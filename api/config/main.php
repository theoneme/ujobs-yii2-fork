<?php
use yii\web\JsonResponseFormatter;
use yii\web\Response;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'name' => 'uJobs',

    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'attribute*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'attribute' => 'attribute.php',
                    ],
                ],
                'labels*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'labels' => 'labels.php',
                    ],
                ],
                'order*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'order' => 'order.php',
                    ],
                ],
                'filter*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'fileMap' => [
                        'formbuilder' => 'filter.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'account*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'account' => 'account.php',
                    ],
                ],
                'notifications*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'notifications' => 'notifications.php',
                    ],
                ],
                'board*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/modules/board/messages',
                    'fileMap' => [
                        'board' => 'board.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => 'job'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'profile'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'attribute'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'attribute-value'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'product'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'test'],
            ],
        ],
        'urlManagerFrontEnd' => [
            'ruleConfig' => [
                'class' => 'frontend\components\LanguageUrlRule'
            ],
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'baseUrl' => 'https://ujobs.me',
            'rules' => [
                'conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/start-conversation',
                'conversation/<id:\d+>' => 'account/inbox/conversation',

                'support-conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/start-conversation',
                'support-conversation/<id:\d+>' => 'account/inbox/support-conversation',

                '<specialty:[\w-+]+>-specialty-tenders' => 'category/specialty-tender',
                '<specialty:[\w-+]+>-specialty-pros' => 'category/specialty-pro',

                '<alias:[\w-]+>-<action:(tender|job|video)>' => '<action>/view',
                '<category_1:[\w-]+>-<action:(category|jobs|tenders|pros)>' => 'category/<action>',

                '<tag:[\w-+]+>-usluga' => 'category/tag',
                '<skill:[\w-+]+>-skill' => 'category/skill',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'formatters' => [
                Response::FORMAT_JSON => [
                    'class' => JsonResponseFormatter::class,
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
        ],
    ],
    'params' => $params,
];
