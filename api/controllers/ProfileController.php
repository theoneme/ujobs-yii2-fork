<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.05.2018
 * Time: 16:46
 */

namespace api\controllers;

use api\models\search\ProfileSearch;
use common\components\UrlAdvanced;
use common\models\user\User;
use common\services\NewMailerService;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class ProfileController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = User::class;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $searchModel = new ProfileSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
            'view' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $model = User::find()->joinWith(['profile'])->where(['user.id' => (int)Yii::$app->request->get('id')]);

                    if (!array_key_exists('primitive-secure', Yii::$app->request->queryParams) || Yii::$app->request->queryParams['primitive-secure'] !== 'bratishka-pokushat') {
                        $model->andWhere('0=1');
                    }

                    return $model->one();
                },
            ]
        ];
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    /**
     * @return \yii\db\ActiveRecord
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        /* @var $model User */
        $model = new $this->modelClass;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->password = uniqid();
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', UrlAdvanced::to(['/account/profile/show', 'id' => $id, 'app_language' => 'ru'], true, 'urlManagerFrontEnd'));

            $content = Yii::t('notifications', 'Greetings! Welcome to uJobs! You can use your account with credentials:', [], 'ru-RU') . '<br>';
            $content .= Yii::t('notifications', 'Login: {login}', ['login' => $model->email], 'ru-RU') . '<br>';
            $content .= Yii::t('notifications', 'Password: {password}', ['password' => $model->password], 'ru-RU') . '<br>';
            $content .= Yii::t('notifications', 'We recommend you to switch this temporary password as fast as possible', [], 'ru-RU');

            $service = new NewMailerService('mailer', $model);
            $service->sendMail([
                'from' => Yii::$app->params['ujobsNoty'],
                'content' => $content,
                'link' => UrlAdvanced::to(['/site/index', 'app_language' => 'ru'], true, 'urlManagerFrontEnd'),
            ]);

        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}