<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.05.2018
 * Time: 18:38
 */

namespace api\controllers;

use yii\rest\ActiveController;
use yii\web\Controller;

/**
 * Class SiteController
 * @package api\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return [];
    }
}
