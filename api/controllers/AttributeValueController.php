<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.05.2018
 * Time: 18:32
 */

namespace api\controllers;

use api\models\search\AttributeSearch;
use api\models\search\AttributeValueSearch;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class AttributeValueController
 * @package api\controllers
 */
class AttributeValueController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = AttributeValue::class;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $searchModel = new AttributeValueSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
            'view' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $model = AttributeValue::find()->joinWith(['translations'])->where(['mod_attribute_value.id' => (int)Yii::$app->request->get('id')])->one();
                    return $model;
                },
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' => ['create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }
}