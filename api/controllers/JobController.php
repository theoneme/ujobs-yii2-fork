<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.05.2018
 * Time: 18:32
 */

namespace api\controllers;

use api\models\search\JobSearch;
use common\models\Job;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\DataFilter;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class JobController
 * @package frontend\controllers
 */
class JobController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = Job::class;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $searchModel = new JobSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' => ['create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }
}