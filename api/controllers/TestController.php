<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.05.2018
 * Time: 16:46
 */

namespace api\controllers;

use api\models\search\ProfileSearch;
use common\components\UrlAdvanced;
use common\models\Test;
use common\models\user\User;
use common\services\NewMailerService;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class TestController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = Test::class;

    /**
     * @return array
     */
    public function actions()
    {
        return [

        ];
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    /**
     * @return Test
     */
    public function actionCreate()
    {
        $params = Yii::$app->getRequest()->getBodyParams();
        /** @var Test $model */
        $model = $this->modelClass::find()->where(['alias' => $params['alias']])->one();

        $response = Yii::$app->getResponse();

        if($model === null) {
            $model = new $this->modelClass;
        }

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->save();
        if($model->save() === true) {
            $response->setStatusCode(201);
        }

        return $model;
    }
}