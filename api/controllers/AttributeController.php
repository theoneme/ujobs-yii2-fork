<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.05.2018
 * Time: 18:32
 */

namespace api\controllers;

use api\models\search\AttributeSearch;
use common\modules\attribute\models\Attribute;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class AttributeController
 * @package api\controllers
 */
class AttributeController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = Attribute::class;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $searchModel = new AttributeSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
            'view' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $model = Attribute::find()->joinWith(['translations'])->where(['mod_attribute.id' => (int)Yii::$app->request->get('id')])->one();
                    return $model;
                },
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' => ['create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }
}