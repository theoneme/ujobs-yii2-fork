<?php

namespace api\models\search;

use common\models\Job;
use common\modules\board\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\validators\NumberValidator;

/**
 * Class ProductSearch
 * @package api\models\search
 */
class ProductSearch extends Product
{
    /**
     * @var boolean
     */
    public $latLongRequired;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            ['category_id', 'arrayOrInt'],
            ['type' ,'string'],
            ['status', 'integer'],
            ['latLongRequired', 'boolean']
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function arrayOrInt($attribute, $params)
    {

        if (!empty($this->{$attribute})) {
            $validator = new NumberValidator();
            $this->{$attribute} = is_array($this->{$attribute}) ? $this->{$attribute} : [$this->{$attribute}];
            foreach ($this->{$attribute} as $key => $value) {
                if (!$validator->validate($value)) {
                    $this->addError($attribute, Yii::t('yii', '{attribute} must be an integer.', ['attribute' => $attribute]));
                }
            }
        }
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');
        $status = $this->status ?? Product::STATUS_ACTIVE;

        $query = Product::find()
            ->joinWith(['translation', 'attachments', 'extra'])
            ->andWhere(['product.status' => $status])
            ->groupBy('product.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'product.user_id' => $this->user_id,
            'type' => $this->type
        ]);

        $query->andFilterWhere(['or',
            ['product.parent_category_id' => $this->category_id],
            ['product.category_id' => $this->category_id],
            ['product.subcategory_id' => $this->category_id]
        ]);
        if ($this->latLongRequired) {
            $query->andWhere(['and',
                ['not', ['product.lat' => null]],
                ['not', ['product.lat' => '']],
                ['not', ['product.long' => null]],
                ['not', ['product.long' =>  '']],
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['pageSize'] ?? 20
            ],
            'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'created_at' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            $query->andWhere('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
