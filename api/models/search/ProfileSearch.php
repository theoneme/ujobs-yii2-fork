<?php

namespace api\models\search;

use common\models\Job;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserAttribute;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class ProfileSearch
 * @package api\models\search
 */
class ProfileSearch extends User
{
    public $specialty;
    public $skill;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            ['specialty', 'safe'],
            ['skill', 'safe']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $query = User::find()
            ->joinWith(['profile.translations', 'userAttributes.attrValueDescriptions'])
//            ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
            ->groupBy('user.id');

        if(!array_key_exists('primitive-secure', $params) || $params['primitive-secure'] !== 'bratishka-pokushat') {
            $query->andWhere('0=1');
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $attributeConditions = [];
        if (!empty($this->specialty)) {
            $attributeConditions[] = ['exists', UserAttribute::find()
                ->select('user_attribute.id')
                ->where('user_attribute.user_id = profile.user_id')
                ->andWhere(['user_attribute.entity_alias' => 'specialty'])
                ->andWhere(['user_attribute.value_alias' => $this->specialty])
            ];
        }
        if (!empty($this->skill)) {
            $attributeConditions[] = ['exists', UserAttribute::find()
                ->select('user_attribute.id')
                ->where('user_attribute.user_id = profile.user_id')
                ->andWhere(['user_attribute.entity_alias' => 'skill'])
                ->andWhere(['user_attribute.value_alias' => $this->skill])
            ];
        }
        if (!empty($attributeConditions)) {
            $query->andWhere(ArrayHelper::merge(['or'], $attributeConditions));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['pageSize'] ?? 20
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        if (!$this->validate()) {
            $query->andWhere('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
