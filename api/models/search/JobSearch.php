<?php

namespace api\models\search;

use common\models\Job;
use yii\data\ActiveDataProvider;

/**
 * Class JobSearch
 * @package api\models\search
 */
class JobSearch extends Job
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $query = Job::find()
            ->joinWith(['translation', 'attachments', 'packages', 'extras', 'extra', 'faq'])
            ->andWhere(['job.status' => self::STATUS_ACTIVE])
            ->groupBy('job.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'job.user_id' => $this->user_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['pageSize'] ?? 20
            ],
            'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'created_at' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            $query->andWhere('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
