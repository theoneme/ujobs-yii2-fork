<?php

namespace api\models\search;

use common\modules\attribute\models\Attribute;
use yii\data\ActiveDataProvider;

/**
 * Class AttributeSearch
 * @package api\models\search
 */
class AttributeSearch extends Attribute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $query = Attribute::find()
            ->joinWith(['translations'])
            ->groupBy('mod_attribute.id');

        $query->andFilterWhere([
            'mod_attribute.id' => $this->id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['pageSize'] ?? 20
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        if (!$this->validate()) {
            $query->andWhere('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
