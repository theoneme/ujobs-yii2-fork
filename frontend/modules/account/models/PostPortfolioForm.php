<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\modules\account\models;

use common\models\Attachment;
use common\models\UserPortfolio;
use common\models\UserPortfolioAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class PostPortfolioForm
 * @package frontend\modules\account\models
 *
 * @property UserPortfolio $portfolio
 */
class PostPortfolioForm extends Model
{
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var integer
     */
    public $subcategory_id;
    /**
     * @var string
     */
    public $tags;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $image = null;
    /**
     * @var string
     */
    public $locale = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var UserPortfolio
     */
    private $_portfolio;
    /**
     * @var array
     */
    private $_attachments = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'categoryRequired' => [['category_id', 'parent_category_id'], 'required'],
            'categoryInteger' => [['category_id', 'parent_category_id', 'subcategory_id'], 'integer'],
            'descriptionRequired' => ['description', 'required'],
            'tagsLength' => ['tags', 'string', 'max' => 260],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages']), 'skipOnEmpty' => false],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            ['description', 'string', 'max' => 65535],
            ['image', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Album Title'),
            'categoryId' => Yii::t('model', 'Category'),
            'parentCategoryId' => Yii::t('model', 'Category'),
            'tags' => Yii::t('model', 'Tags'),
            'description' => Yii::t('model', 'Description'),
        ];
    }

    /**
     *
     */
    public function init()
    {
        if ($this->locale === null) {
            $this->locale = Yii::$app->language;
        }
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->portfolio->attributes = $this->attributes;
        $isSaved = $this->portfolio->save();
        if ($isSaved) {
            $this->portfolio->updateElastic();
        }

        $transaction->commit();

        return $isSaved;
    }

    /**
     * @return array|null
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->portfolio->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->portfolio->attachments;
            }
        }
        return $this->_attachments;
    }

    /**
     * @return UserPortfolio
     */
    public function getPortfolio()
    {
        return $this->_portfolio;
    }

    /**
     * @param UserPortfolio $portfolio
     */
    public function setPortfolio(UserPortfolio $portfolio)
    {
        $this->_portfolio = $portfolio;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);

        if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
            foreach ($input['Attachment'] as $key => $item) {
                if ($item['content'] !== null) {
                    $attachmentObject = new Attachment();
                    $attachmentObject->setAttributes($item);
                    $attachmentObject->entity = 'portfolio';

                    $this->portfolio->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
                }
            }
        }

        $tagAttribute = Attribute::findOne(['alias' => 'portfolio_tag']);
        $this->bindPortfolioAttributes(["attribute_{$tagAttribute->id}" => !empty($this->tags) ? explode(',', $this->tags) : null]);
    }

    /**
     * @param $id
     */
    public function loadPortfolio($id)
    {
        $this->portfolio = UserPortfolio::findOne((int)$id);
        $this->attributes = $this->portfolio->attributes;

        $tags = $this->portfolio->tags;
        $tagsF = array_map(function ($value) {
            /* @var $value UserPortfolioAttribute */
            return $value->getTitle();
        }, $tags);
        $this->tags = implode(', ', $tagsF);
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->portfolio->validateWithRelations()
        );
    }

    /**
     * @param array $attributes
     */
    private function bindPortfolioAttributes(array $attributes)
    {
        foreach ($attributes as $key => $attribute) {
            if (!$attribute) continue;

            $parts = explode('_', $key);

            if (is_array($attribute)) {
                $values = $attribute;
            } else {
                $valueParts = array_filter(explode(',', $attribute));
                $values = $valueParts;
            }

            foreach ($values as $pa) {
                $portfolioAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(UserPortfolioAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                if ($portfolioAttributeObject !== null) {
                    $this->portfolio->bind('userPortfolioAttributes', $portfolioAttributeObject->id)->attributes = $portfolioAttributeObject->attributes;
                }
            }
        }
    }
}