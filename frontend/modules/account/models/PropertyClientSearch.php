<?php

namespace frontend\modules\account\models;

use common\models\PropertyClient;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class PropertyClientSearch
 * @package frontend\modules\account\models
 */
class PropertyClientSearch extends PropertyClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'phone'], 'string', 'max' => 255],
            ['status', 'in', 'range' => [self::STATUS_SENT, self::STATUS_REGISTERED]],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropertyClient::find()->where(['created_by' => Yii::$app->user->identity->getCurrentId()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
