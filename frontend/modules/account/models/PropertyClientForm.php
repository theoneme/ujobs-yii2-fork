<?php

namespace frontend\modules\account\models;

use borales\extensions\phoneInput\PhoneInputValidator;
use common\models\EmailLog;
use common\models\PropertyClient;
use common\models\user\User;
use GeoIp2\Database\Reader;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Yii;

/**
 * Class PropertyClientForm
 * @package frontend\modules\account\models
 */
class PropertyClientForm extends PropertyClient
{
    const SCENARIO_UPDATE = 'update';

    /**
     * @var PropertyClient
     */
    public $client = null;

    /**
     * @var array
     */
    public $houses = [];

    /**
     * @var string
     */
    public $housesString;

    public function beforeValidate()
    {
        $this->phone = "+" . preg_replace("/[^0-9]/", "", $this->phone);
        $phoneUtil = PhoneNumberUtil::getInstance();
        $valid = false;
        try {
            $phone = $phoneUtil->parse($this->phone, null);
            if ($phoneUtil->isValidNumber($phone)) {
                $valid = true;
            }
        } catch (NumberParseException $e) {
        }
        if (!$valid) {
            $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-Country.mmdb');
            $ip = Yii::$app->request->getUserIP();
            if ($ip !== '127.0.0.1' && strlen($ip) > 0) {
                $region = $geoIpReader->country($ip)->country->isoCode;
                if (!empty($region)) {
                    try {
                        $phone = $phoneUtil->parse(preg_replace("/[^0-9]/", "", $this->phone), $region);
                        $this->phone = $phoneUtil->format($phone, PhoneNumberFormat::E164);
                    } catch (NumberParseException $e) {
                    }
                }
            }
        }
        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['phone',
                PhoneInputValidator::class,
                'message' => Yii::t('app', 'Enter phone in full format, including county code. Example +7 (123) 456 78 90')
            ],
            ['housesString', 'string'],
            ['email', 'unique', 'targetAttribute' => ['created_by', 'email'], 'except' => self::SCENARIO_UPDATE],
        ]);
    }

    /**
     * @return bool
     */
    public function submit()
    {
        if ($this->validate()) {
            /* @var $client PropertyClient*/
            $client = $this->client ?? new PropertyClient(['created_by' => Yii::$app->user->identity->getCurrentId()]);
            $client->first_name = $this->first_name;
            $client->last_name = $this->last_name;
            $client->email = $this->email;
            $client->phone = $this->phone;
            $client->info = $this->info;
            $client->mortgage = $this->mortgage;
            $client->housesData = array_filter(
                array_map(
                    function($var){
                        $parts = explode('-', $var, 2);
                        return count($parts) === 2 ? ['entity' => $parts[0], 'entity_id' => $parts[1]] : null;
                    },
                    explode(',', $this->housesString)
                )
            );
            if ($client->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Invitations sent. Thank you for supporting uJobs'));
            }
        }
        Yii::$app->session->setFlash('warning', Yii::t('app', 'Error occurred during sending invitations'));

        return false;
    }
}
