<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.05.2017
 * Time: 12:31
 */

namespace frontend\modules\account\models\history;

use common\components\CurrencyHelper;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use Yii;

/**
 * Class PriceDecisionForm
 * @package frontend\modules\account\models\history
 */
class PriceDecisionForm extends DecisionForm
{
	/**
	 * @var integer
	 */
	public $price;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = array_merge(parent::rules(), [
			['price', 'integer'],
			['price', 'required']
		]);

		unset($rules['contentRequired']);
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'price' => Yii::t('model', 'Price')
		]);
	}

	/**
	 * @return bool
	 */
	public function save()
	{
		/* @var Order $order */
		$order = Order::findOne($this->order_id);
        if ($this->validate() && (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id))) {
            $transaction = Yii::$app->db->beginTransaction();

            $responseHistory = new OrderHistory([
                'type' => $this->t,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $this->order_id,
                'content' => $this->content,
                'custom_data' => json_encode([
                    'price' => [
                        'from' => Yii::$app->params['app_currency_code'],
                        'amount' => $this->price
                    ]
                ])
            ]);
            if($responseHistory->save()) {
                $order->status = $this->h;
                $order->total = $this->price;
                $order->currency_code = Yii::$app->params['app_currency_code'];
                $order->save();

                $this->addNotification($order, $responseHistory, $transaction);

                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }
		return false;
	}
}