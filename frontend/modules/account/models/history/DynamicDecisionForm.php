<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.01.2018
 * Time: 15:57
 */

namespace frontend\modules\account\models\history;

use common\helpers\SecurityHelper;
use common\models\Attachment;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\Url;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Class DocumentDecisionForm
 * @package frontend\modules\account\models\history
 */
class DynamicDecisionForm extends DecisionForm
{
    /**
     * @var array
     */
    public $_attributes = [];
    /**
     * @var array
     */
    public $_labels = [];

    /**
     * DocumentDecisionForm constructor.
     * @param array $config
     * @param array $fields
     */
    public function __construct(array $config = [], array $fields = [])
    {
        $this->_labels = parent::attributeLabels();

        foreach ($fields as $key => $field) {
            $this->_attributes[$key] = null;
            $this->_labels[$key] = $field['label'];
            if (!empty((array)$field['rules'])) {
                foreach ((array)$field['rules'] as $rule) {
                    $this->addRule($rule[0], $rule[1]);
                }
            }
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        } else {
            return parent::__get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_attributes)) {
            $this->_attributes[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param $attributes
     * @param $validator
     * @param array $options
     * @return $this
     */
    public function addRule($attributes, $validator, $options = [])
    {
        $validators = $this->getValidators();
        $validators->append(Validator::createValidator($validator, $this, (array)$attributes, $options));

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        unset($rules['contentRequired']);

        return $rules;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return $this->_labels;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        /* @var Order $order */
        $order = Order::findOne($this->order_id);
        if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
            $transaction = Yii::$app->db->beginTransaction();
            $content = Yii::t('order', 'Answers on requirements:') . PHP_EOL;
            foreach($this->_attributes as $key=>$attribute) {
                $content .= "{$this->_labels[$key]}: {$attribute}" . PHP_EOL;
            }

            $history = new OrderHistory([
                'isBlameable' => false,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'type' => $this->t,
                'order_id' => $order->id,
                'content' => $content,
                'sender_id' => $order->seller_id
            ]);
            if ($history->save()) {
                $this->addNotification($order, $history, $transaction);

                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }
        return false;
    }
}