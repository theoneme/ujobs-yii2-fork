<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.05.2017
 * Time: 17:48
 */

namespace frontend\modules\account\models\history;

use common\models\Review;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use Yii;

/**
 * Class RatingForm
 * @package frontend\modules\account\models\history
 */
class RatingForm extends DecisionForm
{
	/**
	 * @var integer
	 */
	public $rating = 10;
	/**
	 * @var integer
	 */
	public $communication;
	/**
	 * @var integer
	 */
	public $service;
	/**
	 * @var integer
	 */
	public $recommend;

	const SCENARIO_SELLER = 'seller';
	const SCENARIO_CUSTOMER = 'customer';

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			self::SCENARIO_SELLER => ['order_id', 'rating', 'communication', 'service', 'recommend', 'h', 't', 'content', 'type'],
			self::SCENARIO_CUSTOMER => ['order_id', 'rating', 'h', 't', 'content', 'type'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		if($this->scenario === self::SCENARIO_SELLER) {
			$this->communication = 10;
			$this->service = 10;
			$this->recommend = 10;
		}

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return array_merge(parent::rules(), [
			['rating', 'required'],
			[['rating', 'communication', 'service', 'recommend'], 'integer'],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function save()
	{
		if(!$this->validate()) {
			return false;
		}

		/* @var Order $order */
		$order = Order::findOne($this->order_id);
        if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
            $transaction = Yii::$app->db->beginTransaction();

            $history = new OrderHistory();
            $history->attributes = $this->attributes;
            $history->seller_id = $order->seller_id;
            $history->customer_id = $order->customer_id;
            $history->type = $this->t;
            if($history->save()) {
                $order->status = $this->h;
                $order->save();

                $review = new Review();
                $review->attributes = $this->attributes;
                $review->created_for = $this->scenario === self::SCENARIO_SELLER ? $order->seller_id : $order->customer_id;
                $review->text = $this->content;
                $review->type = $this->scenario === self::SCENARIO_SELLER ? Review::TYPE_SELLER_REVIEW : Review::TYPE_CUSTOMER_REVIEW;
                $productId = null;
                switch($order->product_type) {
                    case Order::TYPE_JOB_PACKAGE:
                        $productId = $order->product->job_id;
                        break;
                    case Order::TYPE_SIMPLE_TENDER:
                    case Order::TYPE_TENDER:
                        $productId = $order->product->id;
                        break;
                }
                $review->job_id = $productId;
                if(!$review->save()) {
                    $transaction->rollBack();
                }

                $this->addNotification($order, $history, $transaction);

                $transaction->commit();

                return true;
            }

            $transaction->rollBack();
        }
		return false;
	}
}