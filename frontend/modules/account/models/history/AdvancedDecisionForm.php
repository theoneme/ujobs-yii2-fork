<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.05.2017
 * Time: 19:09
 */

namespace frontend\modules\account\models\history;

use common\helpers\SecurityHelper;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use Yii;

/**
 * Class DecisionForm
 * @package frontend\modules\account\models\history
 */
class AdvancedDecisionForm extends DecisionForm
{
	/**
	 * @var integer
	 */
	public $tbd_at = null;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
        $rules = array_merge(parent::rules(), [
            ['tbd_at', 'required'],
            ['tbd_at', 'integer', 'min' => time(), 'tooSmall' => Yii::t('app', 'Date must be greater than current')]
        ]);

        unset($rules['contentRequired']);
        return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'tbd_at' => Yii::t('model', 'Completion time')
		]);
	}

	/**
	 * @return bool
	 */
	public function beforeValidate()
	{
		$valid = parent::beforeValidate();
		if($this->tbd_at) {
			$this->tbd_at = strtotime($this->tbd_at);
		}

		return $valid;
	}

	/**
	 * @return bool
	 */
	public function save()
	{
	    if(!$this->validate()) {
	        return false;
        }

		/* @var Order $order */
		$order = Order::findOne($this->order_id);
        if ($order !== null && (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id))) {
            $transaction = Yii::$app->db->beginTransaction();

            $history = new OrderHistory();
            $history->attributes = $this->attributes;
            $history->seller_id = $order->seller_id;
            $history->customer_id = $order->customer_id;
            $history->type = $this->t;
            if($history->save()) {
                $order->status = $this->h;
                $order->tbd_at = $this->tbd_at;
                if(!$order->save()) {
                    $transaction->rollBack();

                    return false;
                }

                $this->addNotification($order, $history, $transaction);

                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
        }

        return false;
	}
}