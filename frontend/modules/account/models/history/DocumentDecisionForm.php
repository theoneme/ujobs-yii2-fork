<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 31.05.2017
 * Time: 13:18
 */

namespace frontend\modules\account\models\history;

use common\helpers\SecurityHelper;
use common\models\Attachment;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\Url;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Class DocumentDecisionForm
 * @package frontend\modules\account\models\history
 */
class DocumentDecisionForm extends DecisionForm
{
	/**
	 * @var array
	 */
	private $_attributes = [];
	/**
	 * @var array
	 */
	private $_labels = [];

	/**
	 * DocumentDecisionForm constructor.
	 * @param array $config
	 * @param array $fields
	 */
	public function __construct(array $config = [], array $fields = [])
	{
		$this->_labels = parent::attributeLabels();

		foreach ($fields as $key => $field) {
			$this->_attributes[$key] = null;
			$this->_labels[$key] = $field['label'];
			if (!empty((array)$field['rules'])) {
				foreach ((array)$field['rules'] as $rule) {
					$this->addRule($rule[0], $rule[1]);
				}
			}
		}

		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function __get($name)
	{
		if (array_key_exists($name, $this->_attributes)) {
			return $this->_attributes[$name];
		} else {
			return parent::__get($name);
		}
	}

	/**
	 * @inheritdoc
	 */
	public function __set($name, $value)
	{
		if (array_key_exists($name, $this->_attributes)) {
			$this->_attributes[$name] = $value;
		} else {
			parent::__set($name, $value);
		}
	}

	/**
	 * @param $attributes
	 * @param $validator
	 * @param array $options
	 * @return $this
	 */
	public function addRule($attributes, $validator, $options = [])
	{
		$validators = $this->getValidators();
		$validators->append(Validator::createValidator($validator, $this, (array)$attributes, $options));

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = parent::rules();
		unset($rules['contentRequired']);

		return $rules;
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return $this->_labels;
	}

	/**
	 * @return bool
	 */
	public function save()
	{
		if (!$this->validate()) {
			return false;
		}
		/* @var Order $order */
		$order = Order::findOne($this->order_id);
        if ((Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id))) {
            $transaction = Yii::$app->db->beginTransaction();

            $history = new OrderHistory([
                'isBlameable' => false,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'type' => $this->t,
                'content' => Yii::t('document', 'Your document is formed. You can download it from the link'),
                'order_id' => $order->id,
                'sender_id' => $order->seller_id
            ]);
            if ($history->save()) {
                $order->status = Order::STATUS_COMPLETED;
                $order->save();

                $year = date('Y');
                $month = date('m');
                $dir = Yii::getAlias('@webroot') . "/downloads/documents/{$year}/{$month}/";

                Yii::$app->utility->makeDir($dir);
                $fileName = uniqid() . time() . ".pdf";
                $link = Url::to("/downloads/documents/{$year}/{$month}/{$fileName}");
                $fullName = $dir . $fileName;

                /* @var $pdf Pdf */
                $pdf = Yii::$app->pdf;
                $pdf->cssFile = '@frontend/web/css/new/pdf-style.css';

                $html = Yii::$app->view->render('@frontend/modules/account/views/order/documents/without-anything', [
                    'model' => $this,
                    'time' => time()
                ]);

                @$pdf->output($html, $fullName, Pdf::DEST_FILE);

                $attachment = new Attachment([
                    'entity' => 'order-history',
                    'entity_id' => $history->id,
                    'content' => $link
                ]);
                if (!$attachment->save()) {
                    $transaction->rollBack();

                    return false;
                }
                $this->addNotification($order, $history, $transaction);

                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }
		return false;
	}
}