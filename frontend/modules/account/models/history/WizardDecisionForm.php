<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.01.2018
 * Time: 13:36
 */

namespace frontend\modules\account\models\history;

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use Yii;
use yii\base\Model;
use yii\validators\Validator;

/**
 * Class WizardDecisionForm
 * @package frontend\modules\account\models\history
 */
class WizardDecisionForm extends Model
{
    /**
     * @var array
     */
    public $_attributes = [];
    /**
     * @var array
     */
    public $_labels = [];
    /**
     * @var null
     */
    public $document_view = null;
    /**
     * @var array
     */
    public $steps = [];
    /**
     * @var int
     */
    public $order_id;

    /**
     * WizardDecisionForm constructor.
     * @param array $config
     * @param array $steps
     */
    public function __construct(array $config = [], array $steps = [])
    {
        $this->_labels = parent::attributeLabels();
        $this->steps = $steps;

        foreach ($steps as $key => $step) {
            foreach ($step['requirements'] as $reqKey => $requirement) {
                $this->_attributes[$reqKey] = null;
                $this->_labels[$reqKey] = $requirement['label'];
                foreach ((array)$requirement['rules'] as $rule) {
                    $this->addRule($rule[0], $rule[1]);
                }
            }
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        } else {
            return parent::__get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_attributes)) {
            $this->_attributes[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param $attributes
     * @param $validator
     * @param array $options
     * @return $this
     */
    public function addRule($attributes, $validator, $options = [])
    {
        $validators = $this->getValidators();
        $validators->append(Validator::createValidator($validator, $this, (array)$attributes, $options));

        return $this;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return $this->_labels;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        /* @var Order $order */
        $order = Order::findOne($this->order_id);
        if($order !== null) {
            if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
                $transaction = Yii::$app->db->beginTransaction();
                $content = Yii::t('order', 'Answers on requirements:') . PHP_EOL;
                foreach ($this->_attributes as $key => $attribute) {
                    $content .= "{$this->_labels[$key]}: {$attribute}" . PHP_EOL;
                }

                $history = new OrderHistory([
                    'isBlameable' => false,
                    'seller_id' => $order->seller_id,
                    'customer_id' => $order->customer_id,
                    'type' => OrderHistory::TYPE_POST_REQUIREMENTS,
                    'order_id' => $order->id,
                    'content' => $content,
                    'sender_id' => $order->seller_id
                ]);

                if ($history->save()) {
                    $transaction->commit();
                    return true;
                }

                $transaction->rollBack();
            }
        }

        return false;
    }
}