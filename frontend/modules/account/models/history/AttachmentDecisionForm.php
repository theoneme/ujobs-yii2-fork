<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.05.2017
 * Time: 16:40
 */

namespace frontend\modules\account\models\history;

use common\models\Attachment;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\UnsetArrayValue;
use yii\web\UploadedFile;

/**
 * Class DecisionForm
 * @package frontend\modules\account\models\history
 */
class AttachmentDecisionForm extends DecisionForm
{
    /**
     * @var integer
     */
    public $uploadedFiles = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['uploadedFiles',
                'file',
                'maxFiles' => 0,
                'maxSize' => 1024 * 1024 * 30,
                'tooBig' => Yii::t('app', 'The file "{file}" is too big. Its size cannot exceed 30MB.'),
            ],
        ]);
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        /* @var Order $order */
        $order = Order::findOne($this->order_id);
        if ($order !== null && (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id))) {
            $transaction = Yii::$app->db->beginTransaction();

            $history = new OrderHistory();
            $history->attributes = $this->attributes;
            $history->seller_id = $order->seller_id;
            $history->customer_id = $order->customer_id;
            $history->type = $this->t;
            if ($history->save()) {
                $order->status = $this->h;
                $order->save();

                $path = '/uploads/order-history/' . date('Y') . '/' . date('m') . '/';
                $dir = Yii::getAlias('@webroot') . $path;
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }
                $this->uploadedFiles = UploadedFile::getInstances($this, 'uploadedFiles');
                foreach ($this->uploadedFiles as $file) {
                    $name = $path . $file->baseName . '-' . hash('crc32b', $file->name . time()) . "." . $file->extension;
                    if ($file->saveAs(Yii::getAlias('@webroot') . $name)) {
                        $attachment = new Attachment();
                        $attachment->entity_id = $history->id;
                        $attachment->entity = 'order-history';
                        $attachment->content = $name;
                        if (!$attachment->save()) {
                            $transaction->rollBack();

                            return false;
                        }
                    }
                }

                $this->addNotification($order, $history, $transaction);

                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }

        return false;
    }
}