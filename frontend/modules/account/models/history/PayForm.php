<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.03.2017
 * Time: 14:44
 */

namespace frontend\modules\account\models\history;

use common\components\CurrencyHelper;
use common\modules\store\models\Order;
use common\modules\store\models\PaymentMethod;
use Yii;
use common\models\Payment;

/**
 * Class PayForm
 * @package frontend\modules\account\models\history
 */
class PayForm extends DecisionForm
{
	/**
	 * @var integer
	 */
    public $payment_method_id;

	/**
	 * @var array
	 */
    public $paymentMethods;

	/**
	 * @var string
	 */
    public $customLabel = null;

	/**
	 * @inheritdoc
	 */
    public function init()
    {
        $this->paymentMethods = PaymentMethod::getMethodsByCountry(Yii::$app->user->identity->id, true);
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = array_merge(parent::rules(), [
			['payment_method_id', 'integer'],
			['payment_method_id', 'required']
		]);

		unset($rules['contentRequired']);
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'payment_method_id' => Yii::t('model', 'Payment method')
		]);
	}

	/**
	 * @return bool
	 */
    public function save()
    {
        /* @var Order $order */
        $order = Order::findOne($this->order_id);
        if ($order !== null && (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id))) {
            $payment = new Payment();
            $payment->description = Yii::t('order', 'Paying deposit for service on {site}', ['site' => 'uJobs.me']);
            $payment->type = $order->product_type === Order::TYPE_PRODUCT_TENDER ? Payment::TYPE_PRODUCT_TENDER_PAYMENT : Payment::TYPE_TENDER_PAYMENT;
            $payment->user_id = Yii::$app->user->identity->getCurrentId(); // TODO WTF
            $payment->total = CurrencyHelper::convert($order->currency_code, Yii::$app->params['app_currency_code'], $order->total, true);
            $payment->payment_method_id = $this->payment_method_id;

            if($payment->save()) {
                if ($order->payment !== null) {
                    $oldPayment = $order->payment;
                    $oldPayment->old_order_id = $order->id;
                    $oldPayment->save();
                }

                $order->payment_id = $payment->id;
                $order->payment_method_id = $payment->payment_method_id;
                $order->save();

                return true;
            }
        }

        return false;
    }
}