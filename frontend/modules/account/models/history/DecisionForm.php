<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.05.2017
 * Time: 15:40
 */

namespace frontend\modules\account\models\history;

use common\helpers\SecurityHelper;
use common\models\Job;
use common\models\Notification;
use common\models\user\User;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use Yii;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class DecisionForm
 * @package frontend\modules\account\models\history
 */
class DecisionForm extends Model
{
    /**
     * @var string
     */
    public $content = '';
    /**
     * @var int
     */
    public $seller_id = 0;
    /**
     * @var int
     */
    public $customer_id = 0;
    /**
     * @var int
     */
    public $order_id = 0;
    /**
     * @var string
     */
    public $h = null;
    /**
     * @var string
     */
    public $t = null;
    /**
     * @var string
     */
    public $placeholder = '';
    /**
     * @var string
     */
    public $type = self::DECISION_HISTORY;
    /**
     * @var string
     */
    public $zxToken = null;
    /**
     * @var bool
     */
    public $visible = true;

    const DECISION_HISTORY = 'history';
    const DECISION_TENDER_HISTORY = 'tender-history';
    const DECISION_PRODUCT_HISTORY = 'product-history';
    const DECISION_PRODUCT_TENDER_HISTORY = 'product-tender-history';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'contentRequired' => ['content', 'required'],
            [['order_id', 'customer_id', 'seller_id'], 'integer'],
            ['content', 'string', 'max' => 1200],
            ['placeholder', 'string'],
            ['type', 'string'],
            [['h', 't'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'content' => Yii::t('model', 'Message')
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();

        $this->h = SecurityHelper::decrypt($this->h);
        $this->t = SecurityHelper::decrypt($this->t);

        return $valid;
    }

    /**
     * @return bool
     */
    public function save()
    {
        /* @var Order $order */
        $order = Order::findOne($this->order_id);

        if ((Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id) || Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) && $this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();

            $history = new OrderHistory();
            $history->attributes = $this->attributes;
            $history->seller_id = $order->seller_id;
            $history->customer_id = $order->customer_id;
            $history->type = $this->t;
            if ($history->save()) {
                $order->status = $this->h;
                $order->save();

                $this->addNotification($order, $history, $transaction);
                $transaction->commit();

                return true;
            }
            $transaction->rollBack();
        }

        return false;
    }

    /**
     * @param Order $order
     * @param OrderHistory $history
     * @param Transaction $transaction
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function addNotification(Order $order, OrderHistory $history, Transaction $transaction = null)
    {
        $to = $history->sender_id === $history->seller_id ? $history->customer_id : $history->seller_id;

        $notification = new Notification();
        $notification->from_id = $history->sender_id;
        $notification->to_id = $to;
        $notification->subject = Notification::SUBJECT_ORDER_UPDATED;
        $notification->template = Notification::TEMPLATE_ORDER_UPDATED;
        $notification->custom_data = json_encode([
            'user' => $history->sender->getSellerName(),
            'order' => $order->product->getLabel()
        ]);
        $notification->thumb = $order->product->getThumb('catalog', false);
        $notification->linkRoute = ["/account/order/{$this->type}", 'id' => $order->id];
        $notification->sender = $order->seller->username;
        $notification->withEmail = true;

        if(!$result = $notification->save()) {
            if($transaction !== null) {
                $transaction->rollBack();
            }
        }

        return $result;
    }
}