<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.10.2017
 * Time: 15:28
 */

namespace frontend\modules\account\models;

use common\models\elastic\JobElastic;
use common\models\Job;
use common\models\user\Profile;
use common\models\UserDeactivation;
use common\models\user\User;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use yii\base\Model;
use Yii;

class PauseForm extends Model
{
    /**
     * @var string
     */
    public $action;

    const ACTION_PAUSE = 1;
    const ACTION_RESUME = 0;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * DeactivateForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->profile = Profile::findOne(['user_id' => Yii::$app->user->identity->getId()]);
        $this->action = $this->profile->status === Profile::STATUS_PAUSED;
    }


    /** @inheritdoc */
    public function rules()
    {
        return [
            ['action', 'in', 'range' => [
                self::ACTION_PAUSE,
                self::ACTION_RESUME
            ]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action' => $this->profile->status === Profile::STATUS_PAUSED ? Yii::t('account', 'Vacation mode is turned on') : Yii::t('account', 'Vacation mode is turned off')
        ];
    }

    /**
     * @return bool
     */
    public function pause()
    {
        if (!$this->validate()) {
            return false;
        }

        $profile = $this->profile;
        $profile->updateAttributes(['status' => Profile::STATUS_PAUSED]);
        $profile->updateElastic();
        $jobIDs = Job::find()->where(['status' => Job::STATUS_ACTIVE, 'user_id' => $profile->user_id])->select('id')->column();
        $productIDs = Product::find()->where(['status' => Product::STATUS_ACTIVE, 'user_id' => $profile->user_id])->select('id')->column();

        JobElastic::updateAll(['profile' => [
            'user_id' => $profile->user_id,
            'name' => $profile->getSellerName(),
            'gravatar_email' => $profile->gravatar_email,
            'status' => $profile::STATUS_PAUSED,
            'rating' => $profile->rating
        ]], ['id' => $jobIDs]);

        ProductElastic::updateAll(['profile' => [
            'user_id' => $profile->user_id,
            'name' => $profile->getSellerName(),
            'gravatar_email' => $profile->gravatar_email,
            'status' => $profile::STATUS_PAUSED,
            'rating' => $profile->rating
        ]], ['id' => $productIDs]);

        return true;
    }

    /**
     * @return bool
     */
    public function resume()
    {
        if (!$this->validate()) {
            return false;
        }

        $profile = $this->profile;
        $profile->defineStatus();
        $profile->updateAttributes(['status' => $profile->status]);
        $profile->updateElastic();

        $jobIDs = Job::find()->where(['status' => Job::STATUS_ACTIVE, 'user_id' => $profile->user_id])->select('id')->column();
        $productIDs = Product::find()->where(['status' => Product::STATUS_ACTIVE, 'user_id' => $profile->user_id])->select('id')->column();

        JobElastic::updateAll(['profile' => [
            'user_id' => $profile->user_id,
            'name' => $profile->getSellerName(),
            'gravatar_email' => $profile->gravatar_email,
            'status' => $profile->status,
            'rating' => $profile->rating
        ]], ['id' => $jobIDs]);

        ProductElastic::updateAll(['profile' => [
            'user_id' => $profile->user_id,
            'name' => $profile->getSellerName(),
            'gravatar_email' => $profile->gravatar_email,
            'status' => $profile->status,
            'rating' => $profile->rating
        ]], ['id' => $productIDs]);

        return true;
    }
}