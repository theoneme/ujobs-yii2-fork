<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.11.2016
 * Time: 13:37
 */

namespace frontend\modules\account\models;

use dektrium\user\helpers\Password;
use common\models\user\User;
use yii\base\Model;
use Yii;

class PasswordChangeForm extends Model
{
    /**
     * @var string
     */
    public $old_password;

    /**
     * @var string
     */
    public $new_password;

    /**
     * @var string
     */
    public $confirm_password;

    /**
     * @var User
     */
    public $user;

    public function __construct()
    {
        $this->user = User::findOne(Yii::$app->user->identity->getId());
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            // password rules
            'passwordRequired' => [['new_password', 'confirm_password', 'old_password'], 'required'],
            'passwordLength' => [['new_password', 'confirm_password'], 'string', 'min' => 6],

            'passwordMatchReverse' => ['confirm_password', 'compare', 'compareAttribute' => 'new_password'],

            'passwordValidate' => [
                'old_password',
                function ($attribute) {
                    if ($this->user === null || !Password::validate($this->old_password, $this->user->password_hash)) {
                        $this->addError($attribute, Yii::t('app', 'Invalid Password'));
                    }
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('model', 'Old Password'),
            'new_password' => Yii::t('model', 'New Password'),
            'confirm_password' => Yii::t('model', 'Confirm Password'),
        ];
    }

    /**
     * @return bool
     */
    public function change()
    {
        $user = $this->user;

        $user->setScenario('update');
        $user->password = $this->new_password;

        return $user->save();
    }
}