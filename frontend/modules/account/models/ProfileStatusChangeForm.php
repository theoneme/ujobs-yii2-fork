<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 12:27
 */

namespace frontend\modules\account\models;

use common\models\UserDeactivation;
use common\models\user\User;
use yii\base\Model;
use Yii;

class ProfileStatusChangeForm extends Model
{
    /**
     * @var string
     */
    public $description;

    /**
     * @var User
     */
    public $user;

    public function __construct()
    {
        $this->user = User::findOne(Yii::$app->user->identity->getId());
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            //'descriptionRequired' => [['description'], 'required'],

            'descriptionAdditional' => ['description', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => Yii::t('account', 'Why are you leaving?'),
        ];
    }

    /**
     * @return bool
     */
    public function updateDescription()
    {
        $user = $this->user;

        $user->setScenario('update');
        $user->bio = $this->description;

        $user->save();

        return true;
    }
}