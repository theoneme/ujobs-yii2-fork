<?php

namespace frontend\modules\account\models;

use common\modules\store\models\Order;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Class OrderSearch
 * @package frontend\modules\account\models
 */
class OrderSearch extends ActiveRecord
{
    // Job Order statuses
    const STATUS_ALL = 'all';
    const STATUS_ACTIVE = 'active';
    const STATUS_MISSING_DETAILS = 'missing-details';
    const STATUS_AWAITING_REVIEW = 'awaiting-review';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_REQUIRE_PAYMENT = 'require-payment';

    //Request Order statuses
    const STATUS_TENDER_ALL = 'all';
    const STATUS_TENDER_ACTIVE = 'active';
    const STATUS_TENDER_UNAPPROVED = 'missing-details';
    const STATUS_TENDER_APPROVED = 'awaiting-review';
    const STATUS_TENDER_DELIVERED = 'delivered';
    const STATUS_TENDER_COMPLETED = 'completed';
    const STATUS_TENDER_CANCELLED = 'cancelled';

    //Order response statuses
    const STATUS_RESPONSE_ALL = 'all';
    const STATUS_RESPONSE_MY = 'my-responses';
    const STATUS_RESPONSE_TO_ME = 'responses-on-my-tenders';

    const TYPE_JOB = 0;
    const TYPE_TENDER = 10;
    const TYPE_RESPONSE = 20;
    const TYPE_PRODUCT = 30;
    const TYPE_PRODUCT_RESPONSE = 40;

    public $status;
    public $product_type;
    public $customer_id;
    public $seller_id;
    public $user_id;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                self::STATUS_ALL,
                self::STATUS_ACTIVE,
                self::STATUS_MISSING_DETAILS,
                self::STATUS_AWAITING_REVIEW,
                self::STATUS_DELIVERED,
                self::STATUS_COMPLETED,
                self::STATUS_CANCELLED,
                self::STATUS_REQUIRE_PAYMENT,

                self::STATUS_TENDER_ALL,
                self::STATUS_TENDER_ACTIVE,
                self::STATUS_TENDER_UNAPPROVED,
                self::STATUS_TENDER_APPROVED,
                self::STATUS_TENDER_DELIVERED,
                self::STATUS_TENDER_COMPLETED,
                self::STATUS_TENDER_CANCELLED,

                self::STATUS_RESPONSE_ALL,
                self::STATUS_RESPONSE_MY,
                self::STATUS_RESPONSE_TO_ME
            ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $subQuery = (new Query())->select('*')->from('order_product' . ' t2')->where('order.id=t2.order_id');
        $query = Order::find()
            ->where(['exists', $subQuery])
            ->andFilterWhere(['or', ['customer_id' => $this->customer_id], ['seller_id' => $this->seller_id]])
            ->andFilterWhere(['product_type' => $this->product_type])
            ->orderBy(['order.created_at' => SORT_DESC]);
        $query = $this->addStatusCondition($query, $this->status);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        return $dataProvider;
    }

    /**
     * @param $status
     * @return int|string
     */
    public function getStatusCount($status)
    {
        $subQuery = (new Query())->select('*')->from('order_product' . ' t2')->where('order.id=t2.order_id');
        $query = Order::find()->where(['exists', $subQuery])
            ->andFilterWhere(['or', ['customer_id' => $this->customer_id], ['seller_id' => $this->seller_id]])
            ->andFilterWhere(['product_type' => $this->product_type]);
        return $this->addStatusCondition($query, $status)->count();
    }

    /**
     * @return array
     */
    public static function getJobStatusLabels()
    {
        return [
            self::STATUS_ALL => Yii::t('labels', 'All'),
            self::STATUS_ACTIVE => Yii::t('labels', 'Active'),
            self::STATUS_REQUIRE_PAYMENT => Yii::t('labels', 'Awaiting Payment'),
            self::STATUS_MISSING_DETAILS => Yii::t('labels', 'Missing Details'),
            self::STATUS_AWAITING_REVIEW => Yii::t('labels', 'Awaiting My Review'),
            self::STATUS_DELIVERED => Yii::t('labels', 'Delivered'),
            self::STATUS_COMPLETED => Yii::t('labels', 'Completed'),
            self::STATUS_CANCELLED => Yii::t('labels', 'Cancelled'),
        ];
    }

    /**
     * @return array
     */
    public static function getTenderStatusLabels()
    {
        return [
            self::STATUS_TENDER_ALL => Yii::t('labels', 'All'),
            self::STATUS_TENDER_ACTIVE => Yii::t('labels', 'Active'),
            self::STATUS_TENDER_APPROVED => Yii::t('labels', 'Awaiting Payment'),
            self::STATUS_TENDER_DELIVERED => Yii::t('labels', 'Delivered'),
            self::STATUS_TENDER_COMPLETED => Yii::t('labels', 'Completed'),
            self::STATUS_TENDER_CANCELLED => Yii::t('labels', 'Cancelled'),
        ];
    }

    /**
     * @return array
     */
    public static function getResponseStatusLabels()
    {
        return [
            self::STATUS_RESPONSE_ALL => Yii::t('labels', 'All'),
            self::STATUS_RESPONSE_MY => Yii::t('labels', 'I responded on requests'),
            self::STATUS_RESPONSE_TO_ME => Yii::t('labels', 'Responses on my requests'),
        ];
    }

    /**
     * @return array
     */
    public static function getProductStatusLabels()
    {
        return [
            self::STATUS_ALL => Yii::t('labels', 'All'),
            self::STATUS_ACTIVE => Yii::t('labels', 'Active'),
            self::STATUS_REQUIRE_PAYMENT => Yii::t('labels', 'Awaiting Payment'),
            self::STATUS_MISSING_DETAILS => Yii::t('labels', 'Missing Details'),
            self::STATUS_AWAITING_REVIEW => Yii::t('labels', 'Awaiting My Review'),
            self::STATUS_DELIVERED => Yii::t('labels', 'Delivered'),
            self::STATUS_COMPLETED => Yii::t('labels', 'Completed'),
            self::STATUS_CANCELLED => Yii::t('labels', 'Cancelled'),
        ];
    }

    /**
     * @return array
     */
    public static function getJobStatusList()
    {
        return array_keys(self::getJobStatusLabels());
    }

    /**
     * @return array
     */
    public static function getTenderStatusList()
    {
        return array_keys(self::getTenderStatusLabels());
    }

    /**
     * @return array
     */
    public static function getResponseStatusList()
    {
        return array_keys(self::getResponseStatusLabels());
    }

    /**
     * @return array
     */
    public static function getProductStatusList()
    {
        return array_keys(self::getProductStatusLabels());
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        switch ($this->type) {
            case self::TYPE_JOB:
                $labels = self::getJobStatusLabels();
                break;
            case self::TYPE_TENDER:
                $labels = self::getTenderStatusLabels();
                break;
            case self::TYPE_RESPONSE:
                $labels = self::getResponseStatusLabels();
                break;
            case self::TYPE_PRODUCT:
                $labels = self::getProductStatusLabels();
                break;
        }

        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addStatusCondition($query, $status)
    {
        switch ($this->type) {
            case self::TYPE_JOB:
                $this->addJobStatusCondition($query, $status);
                break;
            case self::TYPE_TENDER:
                $this->addTenderStatusCondition($query, $status);
                break;
            case self::TYPE_RESPONSE:
            case self::TYPE_PRODUCT_RESPONSE:
                $this->addResponseStatusCondition($query, $status);
                break;
            case self::TYPE_PRODUCT:
                $this->addProductStatusCondition($query, $status);
                break;
        }
        return $query;
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addJobStatusCondition($query, $status)
    {
        switch ($status) {
            case self::STATUS_ALL:
                $query->andWhere(['<', 'status', Order::STATUS_TENDER_UNAPPROVED]);
                break;
            case self::STATUS_ACTIVE:
                $query->andWhere(['status' => Order::STATUS_ACTIVE]);
                break;
            case self::STATUS_REQUIRE_PAYMENT:
                $query->andWhere(['status' => Order::STATUS_REQUIRES_PAYMENT]);
                break;
            case self::STATUS_MISSING_DETAILS:
                $query->andWhere(['status' => Order::STATUS_MISSING_DETAILS]);
                break;
            case self::STATUS_AWAITING_REVIEW:
                $query->andWhere(['status' => [Order::STATUS_AWAITING_CUSTOMER_REVIEW, Order::STATUS_AWAITING_SELLER_REVIEW, Order::STATUS_AWAITING_REVIEW]]);
                break;
            case self::STATUS_DELIVERED:
                $query->andWhere(['status' => Order::STATUS_DELIVERED]);
                break;
            case self::STATUS_COMPLETED:
                $query->andWhere(['status' => Order::STATUS_COMPLETED]);
                break;
            case self::STATUS_CANCELLED:
                $query->andWhere(['status' => Order::STATUS_CANCELLED]);
                break;
        }
        return $query;
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addTenderStatusCondition($query, $status)
    {
        switch ($status) {
            case self::STATUS_TENDER_ALL:
                $query->andWhere(['not', ['status' => [
                    Order::STATUS_TENDER_APPROVED,
                    Order::STATUS_TENDER_UNAPPROVED,
                    Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
                    Order::STATUS_CANCELLED
                ]]]);
                break;
            case self::STATUS_TENDER_ACTIVE:
                $query->andWhere(['status' => [
                    Order::STATUS_TENDER_ACTIVE,
                    Order::STATUS_TENDER_PAID,
                    Order::STATUS_TENDER_REQUIRES_WEEKLY_TASK,
                    Order::STATUS_TENDER_REQUIRES_TASK_APPROVE,
                ]]);
                break;
            case self::STATUS_TENDER_UNAPPROVED:
                $query->andWhere(['status' => [
                    Order::STATUS_TENDER_UNAPPROVED,
                    Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
                ]]);
                break;
            case self::STATUS_TENDER_APPROVED:
                $query->andWhere(['status' => [Order::STATUS_TENDER_APPROVED]]);
                break;
            case self::STATUS_TENDER_DELIVERED:
                $query->andWhere(['status' => [Order::STATUS_TENDER_DELIVERED]]);
                break;
            case self::STATUS_TENDER_COMPLETED:
                $query->andWhere(['status' => [
                    Order::STATUS_TENDER_COMPLETED,
                    Order::STATUS_TENDER_AWAITING_REVIEW,
                    Order::STATUS_TENDER_AWAITING_CUSTOMER_REVIEW,
                    Order::STATUS_TENDER_AWAITING_SELLER_REVIEW,
                ]]);
                break;
            case self::STATUS_TENDER_CANCELLED:
                $query->andWhere(['status' => Order::STATUS_CANCELLED]);
                break;
        }
        return $query;
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addProductStatusCondition($query, $status)
    {
        switch ($status) {
            case self::STATUS_ALL:
                $query->andWhere(['between', 'status', 200, 300]);
                break;
            case self::STATUS_ACTIVE:
                $query->andWhere(['status' => Order::STATUS_PRODUCT_SENT]);
                break;
            case self::STATUS_REQUIRE_PAYMENT:
                $query->andWhere(['status' => Order::STATUS_REQUIRES_PAYMENT]);
                break;
            case self::STATUS_MISSING_DETAILS:
                $query->andWhere(['status' => [Order::STATUS_PRODUCT_WAITING_FOR_SEND, Order::STATUS_PRODUCT_WAITING_FOR_PICKUP, Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION]]);
                break;
            case self::STATUS_AWAITING_REVIEW:
                $query->andWhere(['status' => Order::STATUS_PRODUCT_AWAITING_REVIEW]);
                break;
            case self::STATUS_DELIVERED:
                $query->andWhere(['status' => Order::STATUS_PRODUCT_DELIVERED]);
                break;
            case self::STATUS_COMPLETED:
                $query->andWhere(['status' => Order::STATUS_PRODUCT_COMPLETED]);
                break;
            case self::STATUS_CANCELLED:
                $query->andWhere(['status' => Order::STATUS_CANCELLED]);
                break;
        }

        return $query;
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addResponseStatusCondition($query, $status)
    {
        $query->andWhere(['status' => [
            Order::STATUS_TENDER_UNAPPROVED,
            Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
            Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
            Order::STATUS_TENDER_APPROVED,
            Order::STATUS_PRODUCT_TENDER_UNAPPROVED,
            Order::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION,
            Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
            Order::STATUS_PRODUCT_TENDER_APPROVED
        ]]);
        switch ($status) {
            case self::STATUS_RESPONSE_ALL:
                $query->andWhere(['or', ['customer_id' => $this->user_id], ['seller_id' => $this->user_id]]);
                break;
            case self::STATUS_RESPONSE_MY:
                $query->andWhere(['seller_id' => $this->user_id]);
                break;
            case self::STATUS_RESPONSE_TO_ME:
                $query->andWhere(['customer_id' => $this->user_id]);
                break;
        }
        return $query;
    }
}