<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.11.2016
 * Time: 13:37
 */

namespace frontend\modules\account\models;

use common\models\user\Profile;
use common\models\UserDeactivation;
use common\models\user\User;
use yii\base\Model;
use Yii;

class DeactivateForm extends Model
{
    /**
     * @var string
     */
    public $reason;

    /**
     * @var string
     */
    public $reason_additional;

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    public $reasons = [];

    /**
     * DeactivateForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->user = User::findOne(Yii::$app->user->identity->getId());

        $this->reasons = [
            Yii::t('account', 'Account') => [
                Yii::t('account', 'I have another uJobs account'),
                Yii::t('account', 'I want to change my username'),
                Yii::t('account', 'Unsubscribe from uJobs emails'),
                Yii::t('account', 'Other')
            ],
            Yii::t('account', 'Selling') => [
                Yii::t('account', 'Not getting enough orders'),
                Yii::t('account', 'Negative experience with buyers'),
                Yii::t('account', 'I get too many orders'),
                Yii::t('account', 'uJobs is complicated or hard to use'),
                Yii::t('account', 'I`m unhappy with uJobs` policies'),
            ],
            Yii::t('account', 'Buying') => [
                Yii::t('account', 'I can`t find what I need on uJobs'),
                Yii::t('account', 'uJobs is complicated or hard to use'),
                Yii::t('account', 'Negative experience with sellers'),
                Yii::t('account', 'I`m unhappy with uJobs` policies'),
                Yii::t('account', 'Other'),
            ],
            Yii::t('account', 'Something else')
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            // password rules
            'reasonRequired' => [['reason'], 'required'],
            'reasonInteger' => [['reason'], 'integer'],

            'reasonAdditional' => ['reason_additional', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reason' => Yii::t('account', 'Why are you leaving?'),
            'reason_additional' => Yii::t('account', 'Tell us more (optional)')
        ];
    }

    /**
     * @return bool
     */
    public function deactivate()
    {
        if(!$this->validate()) {
            return false;
        }

        $user = $this->user;

        $user->setScenario('update');
        $user->disabled = true;
        $user->blocked_at = time();

        if ($user->save() === true) {
            $user->profile->updateAttributes(['status' => Profile::STATUS_DISABLED]);
            $user->profile->updateElastic();

            $deactivation = new UserDeactivation;
            $deactivation->reason_id = $this->reason;
            $deactivation->reason_additional = $this->reason_additional;
            $deactivation->user_id = $user->id;
            $deactivation->save();

            return true;
        }

        return false;
    }
}