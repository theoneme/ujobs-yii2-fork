<?php

namespace frontend\modules\account\models;

use common\models\Category;
use common\modules\board\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ProfileProductTenderSearch
 * @package frontend\modules\account\models
 */
class ProfileProductTenderSearch extends ActiveRecord
{
    public $price;
    public $user_id;
    public $category_id;
    public $isOwner;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            ['price', 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = null)
    {
        $this->load($params, '');

        $query = Product::find()
            ->select('product.*')
            ->joinWith(['translation', 'attachments', 'user.activeTariff', 'profile', 'category'])
            ->andFilterWhere(['product.category_id' => $this->category_id])
            ->groupBy('product.id');

        $query = $this->applyFilters($query);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageParam' => 'ptender-page',
                'pageSize' => 16,
                'defaultPageSize' => 16
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCategories(){
        $categories = Category::find()
            ->select(['content_translation.title', 'category.id'])
            ->joinWith(['products', 'translation'])
            ->indexBy('id')
            ->groupBy('category.id')
            ->asArray();
        $categories = $this->applyFilters($categories);
        return $categories->column();
    }

    /**
     * @return array|null|ActiveRecord
     */
    public function getPriceRange(){
        $priceQuery = Product::find()
            ->select([
                'min' => new Expression('FLOOR(MIN(CASE WHEN (ratio IS NULL) THEN product.price ELSE product.price * ratio END))'),
                'max' => new Expression('FLOOR(MAX(CASE WHEN (ratio IS NULL) THEN product.price ELSE product.price * ratio END))')
            ])
            ->andFilterWhere(['product.category_id' => $this->category_id])
            ->asArray();
        $priceQuery = $this->applyFilters($priceQuery);
        return $priceQuery->one();
    }

    /**
     * @param $query ActiveQuery
     * @return ActiveQuery
     */
    private function applyFilters($query){
        $query->andWhere(['product.user_id' => $this->user_id])
            ->andFilterWhere(['product.type' => Product::TYPE_TENDER])
            ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = '" . Yii::$app->params['app_currency_code'] . "'");

        if ($this->isOwner) {
            $query->andWhere(['not', ['product.status' => Product::STATUS_DELETED]]);
        }
        else {
            $query->andWhere(['product.status' => Product::STATUS_ACTIVE]);
        }

        if (!empty($this->price)) {
            $query->addSelect(['convertedPrice' => new Expression('FLOOR(CASE WHEN (ratio IS NULL) THEN product.price ELSE product.price * ratio END)')]);
            $query->andFilterHaving(['>=', 'convertedPrice', $this->price['min'] ?? null]);
            $query->andFilterHaving(['<=', 'convertedPrice', $this->price['max'] ?? null]);
        }
        return $query;
    }
}
