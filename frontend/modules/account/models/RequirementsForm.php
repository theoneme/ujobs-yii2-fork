<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.12.2016
 * Time: 14:36
 */

namespace frontend\modules\account\models;

use common\models\UserDeactivation;
use common\models\user\User;
use yii\base\Model;
use Yii;

class RequirementsForm extends Model
{
    /**
     * @var string
     */
    public $requirements;

    /*
     * @var mixed
     */
    public $files;

    /**
     * @var User
     */
    public $user;

    public function __construct()
    {
        $this->user = User::findOne(Yii::$app->user->identity->getId());
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            'requirements' => ['requirements', 'string', 'max' => 600],

            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, doc, pdf, xls, xlsx, docx', 'maxFiles' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'requirements' => Yii::t('model', 'Additional Information'),
        ];
    }

    public function loadRequirement()
    {
//        $history =

        $user = $this->user;

        $user->setScenario('update');
        $user->bio = $this->description;

        $user->save();

        return true;
    }
}