<?php

namespace frontend\modules\account\models;

use common\models\Job;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class JobSearch
 * @package frontend\modules\account\models
 */
class JobSearch extends ActiveRecord
{
    const STATUS_ALL = 'all';
    const STATUS_ACTIVE = 'active';
    const STATUS_REQUIRE_MODERATION = 'require-moderation';
    const STATUS_REQUIRE_MODIFICATION = 'require-modification';
    const STATUS_DISABLED = 'disabled';
    const STATUS_DENIED = 'denied';
    const STATUS_PAUSED = 'paused';
    const STATUS_DRAFT = 'draft';

    public $status;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                self::STATUS_ACTIVE,
                self::STATUS_REQUIRE_MODERATION,
                self::STATUS_REQUIRE_MODIFICATION,
                self::STATUS_DISABLED,
                self::STATUS_DENIED,
                self::STATUS_PAUSED,
                self::STATUS_DRAFT,
                self::STATUS_ALL,
            ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $query = Job::find()
            ->joinWith(['attachments', 'translation'])
            ->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'type' => $this->type])
            ->orderBy(['job.created_at' => SORT_DESC])
            ->groupBy('job.id');
        $query = $this->addStatusCondition($query, $this->status);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_ALL => Yii::t('labels', 'All'),
            self::STATUS_ACTIVE => Yii::t('labels', 'Active'),
            self::STATUS_REQUIRE_MODERATION => Yii::t('labels', 'Awaiting Moderation'),
            self::STATUS_REQUIRE_MODIFICATION => Yii::t('labels', 'Requires Details'),
            self::STATUS_DISABLED => Yii::t('labels', 'Disabled'),
            self::STATUS_DENIED => Yii::t('labels', 'Denied'),
            self::STATUS_PAUSED => Yii::t('labels', 'Paused'),
            self::STATUS_DRAFT => Yii::t('labels', 'Drafts'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ALL,
            self::STATUS_ACTIVE,
            self::STATUS_REQUIRE_MODERATION,
            self::STATUS_REQUIRE_MODIFICATION,
            self::STATUS_DISABLED,
            self::STATUS_DENIED,
            self::STATUS_PAUSED,
            self::STATUS_DRAFT,
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param $status
     * @return int|string
     */
    public function getStatusCount($status)
    {
        $query = Job::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'type' => $this->type]);
        return $this->addStatusCondition($query, $status)->count();
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addStatusCondition($query, $status)
    {
        switch ($status) {
            case self::STATUS_ALL:
                $query->andWhere(['not', ['status' => Job::STATUS_DELETED]]);
                break;
            case self::STATUS_ACTIVE:
                $query->andWhere(['status' => Job::STATUS_ACTIVE]);
                break;
            case self::STATUS_REQUIRE_MODERATION:
                $query->andWhere(['status' => Job::STATUS_REQUIRES_MODERATION]);
                break;
            case self::STATUS_REQUIRE_MODIFICATION:
                $query->andWhere(['status' => Job::STATUS_REQUIRES_MODIFICATION]);
                break;
            case self::STATUS_DISABLED:
                $query->andWhere(['status' => Job::STATUS_DISABLED]);
                break;
            case self::STATUS_DENIED:
                $query->andWhere(['status' => Job::STATUS_DENIED]);
                break;
            case self::STATUS_PAUSED:
                $query->andWhere(['status' => Job::STATUS_PAUSED]);
                break;
            case self::STATUS_DRAFT:
                $query->andWhere(['status' => Job::STATUS_DRAFT]);
                break;
        }
        return $query;
    }
}
