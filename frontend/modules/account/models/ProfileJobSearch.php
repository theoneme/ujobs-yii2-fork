<?php

namespace frontend\modules\account\models;

use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\modules\store\models\Order;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ProfileJobSearch
 * @package frontend\modules\account\models
 */
class ProfileJobSearch extends ActiveRecord
{
    public $price;
    public $tags = [];
    public $user_id;
    public $category_id;
    public $isOwner;
    public $sold;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            ['tags', 'safe'],
            ['price', 'safe'],
            ['sold', 'boolean'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = null)
    {
        $this->load($params, '');

        $query = Job::find()
            ->select('job.*')
            ->andFilterWhere(['job.category_id' => $this->category_id])
            ->joinWith(['translation', 'favourite', 'attachments', 'user.activeTariff', 'profile', 'packages', 'category'])
            ->groupBy('job.id');
        $query = $this->applyFilters($query);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageParam' => 'job-page',
                'pageSize' => 16,
                'defaultPageSize' => 16
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        $tags = JobAttribute::find()
            ->select(['mad_2.title', 'job_attribute.job_id', 'job_attribute.value', 'job_attribute.value_alias'])
            ->joinWith(['job', 'attrValueDescriptions'])
            ->andWhere(['mad_2.locale' => Yii::$app->language, 'job_attribute.entity_alias' => 'tag'])
            ->andFilterWhere(['job.category_id' => $this->category_id])
            ->indexBy('value_alias')
            ->groupBy('job_attribute.value_alias')
            ->asArray();
        $tags = $this->applyFilters($tags);
        return $tags->column();
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $categories = Category::find()
            ->select(['content_translation.title', 'category.id'])
            ->joinWith(['jobs', 'translation'])
            ->indexBy('id')
            ->groupBy('category.id')
            ->asArray();
        $categories = $this->applyFilters($categories);
        return $categories->column();
    }

    /**
     * @return array|null|ActiveRecord
     */
    public function getPriceRange()
    {
        $priceQuery = Job::find()
            ->select([
                'min' => new Expression('FLOOR(MIN(CASE WHEN (ratio IS NULL) THEN job.price ELSE job.price * ratio END))'),
                'max' => new Expression('FLOOR(MAX(CASE WHEN (ratio IS NULL) THEN job.price ELSE job.price * ratio END))')
            ])
            ->andFilterWhere(['job.category_id' => $this->category_id])
            ->asArray();
        $priceQuery = $this->applyFilters($priceQuery);
        return $priceQuery->one();
    }

    /**
     * @param $query ActiveQuery
     * @return ActiveQuery
     */
    private function applyFilters($query)
    {
        $query->andWhere(['job.user_id' => $this->user_id])
            ->andFilterWhere(['job.type' => Job::TYPE_JOB])
            ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = '" . Yii::$app->params['app_currency_code'] . "'");

        if ($this->sold) {
            $query->andWhere(['exists', Order::find()
                ->joinWith(['orderProduct'])
                ->where("order_product.product_id=job_package.id")
                ->andWhere([
                    'order.product_type' => Order::TYPE_JOB_PACKAGE,
                    'order.status' => Order::STATUS_COMPLETED
                ])
            ]);
        }
        if ($this->isOwner) {
            $query->andWhere(['not', ['job.status' => Job::STATUS_DELETED]]);
        } else {
            $query->andWhere(['job.status' => Job::STATUS_ACTIVE]);
        }
        if (!empty($this->price)) {
            $query->addSelect(['convertedPrice' => new Expression('FLOOR(CASE WHEN (ratio IS NULL) THEN job.price ELSE job.price * ratio END)')]);
            $query->andFilterHaving(['>=', 'convertedPrice', $this->price['min'] ?? null]);
            $query->andFilterHaving(['<=', 'convertedPrice', $this->price['max'] ?? null]);
        }
        if (!empty($this->tags)) {
            foreach ($this->tags as $tag) {
                $query->andWhere(['exists', JobAttribute::find()
                    ->from(['ja' => 'job_attribute'])
                    ->where("ja.job_id=job.id")
                    ->andWhere([
                        'ja.entity_alias' => 'tag',
                        'ja.value_alias' => $tag
                    ])
                ]);
            }
        }
        return $query;
    }
}
