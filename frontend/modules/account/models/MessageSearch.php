<?php

namespace frontend\modules\account\models;

use common\models\Conversation;
use common\models\Message;
use common\models\user\User;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class MessageSearch
 * @package frontend\modules\account\models
 */
class MessageSearch extends Message
{
    const STATUS_ALL = 'all';
    const STATUS_UNREAD = 'unread';
    const STATUS_READ = 'read';
    const STATUS_ORDER = 'order';
    const STATUS_STARRED = 'starred';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED = 'deleted';

    const TYPE_CONVERSATIONS = 0;
    const TYPE_MESSAGES = 10;

    public $status;
    public $search;
    public $conversation_id;
    public $order = SORT_DESC;
    public $type = self::TYPE_CONVERSATIONS;
    public $from_id;
    public $limit = 20;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                self::STATUS_ALL,
                self::STATUS_UNREAD,
                self::STATUS_READ,
                self::STATUS_ORDER,
                self::STATUS_STARRED,
                self::STATUS_ARCHIVED,
                self::STATUS_DELETED,
            ]],
            ['search', 'string', 'max' => 255],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['order', 'in', 'range' => [SORT_DESC, SORT_ASC]],
            ['type', 'in', 'range' => [self::TYPE_MESSAGES]],
            ['conversation_id', 'exist', 'targetClass' => Conversation::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $route = $this->from_id === Yii::$app->params['supportId'] ? '/account/inbox/support' : '/account/inbox/index';

        if (!isset($params['status'])) {
            $params['status'] = self::STATUS_ALL;
        }
        $this->load($params, '');

        $query = Message::find()
            ->joinWith(['user.profile', 'attachments', 'conversation', 'conversationMembers', 'offer'])
            ->where(['conversation_member.user_id' => $this->from_id])
            ->andFilterWhere(['message.conversation_id' => $this->conversation_id])
            ->andFilterWhere(['or',
                ['like', 'message.message', $this->search],
                ['like', 'profile.name', $this->search],
                ['like', 'user.username', $this->search],
            ])
            ->orderBy(['message.created_at' => $this->order]);
        $query = $this->addStatusWhereCondition($query, $this->status);
        $countQuery = clone $query;
        $countQuery->limit(null);

        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $this->limit,
            'pageSizeParam' => false,
            'route' => $route,
            'params' => $params
        ]);
        $pages->pageSizeParam = false;
        $query->offset($pages->offset)
            ->limit($pages->limit);

        $conversations = $query->all();
        $data = [
            'items' => array_reverse($conversations),
            'pagination' => $pages
        ];

        if (!$this->validate()) {
            $data = [
                'items' => [],
                'pagination' => $pages
            ];
        }

        return $data;
    }

    /**
     * @param $status
     * @return int|string
     */
    public function getStatusCount($status)
    {
        $query = Message::find()
            ->joinWith(['conversationMembers', 'conversation'])
            ->andWhere(['conversation_member.user_id' => $this->from_id])
            ->andWhere(['message.type' => Message::TYPE_MESSAGE]);
        return $this->addStatusWhereCondition($query, $status)->count();
    }

    /**
     * @param ActiveQuery $query
     * @param $status
     * @return mixed
     */
    private function addStatusWhereCondition($query, $status)
    {
        switch ($status) {
            case self::STATUS_ALL:
                break;
            case self::STATUS_STARRED:
                $query->andWhere(['conversation_member.starred' => true]);
                break;
            case self::STATUS_READ:
                $query->andWhere('message.created_at < conversation_member.viewed_at');
                $query->andWhere(['not', ['message.user_id' => $this->from_id]]);
                break;
            case self::STATUS_UNREAD:
                $query->andWhere('message.created_at >= conversation_member.viewed_at');
                $query->andWhere(['not', ['message.user_id' => $this->from_id]]);
                break;
            case self::STATUS_ORDER:
                $query->andWhere(['conversation.type' => Conversation::TYPE_ORDER]);
                break;
            default:
                $query->andWhere(['conversation_member.status' => $status]);
        }

        return $query;
    }
}
