<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.04.2018
 * Time: 14:21
 */

namespace frontend\modules\account\models;

use common\models\Conversation;
use common\models\ConversationMember;
use common\models\Message;
use common\models\user\User;
use frontend\dto\ConversationItemDTO;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class ConversationSearch
 * @package frontend\modules\account\models
 */
class ConversationSearch extends Message
{
    const STATUS_ALL = 'all';
    const STATUS_UNREAD = 'unread';
    const STATUS_READ = 'read';
    const STATUS_ORDER = 'order';
    const STATUS_STARRED = 'starred';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED = 'deleted';

    public $status;
    public $search;
    public $conversation_id;
    public $order = SORT_DESC;
    public $from_id;
    public $limit = 10;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                self::STATUS_ALL,
                self::STATUS_UNREAD,
                self::STATUS_READ,
                self::STATUS_ORDER,
                self::STATUS_STARRED,
                self::STATUS_ARCHIVED,
                self::STATUS_DELETED,
            ]],
            ['search', 'string', 'max' => 255],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['order', 'in', 'range' => [SORT_DESC, SORT_ASC]],
//            ['type', 'in', 'range' => [self::TYPE_CONVERSATIONS, self::TYPE_MESSAGES]],
            ['conversation_id', 'exist', 'targetClass' => Conversation::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $route = $this->from_id === Yii::$app->params['supportId'] ? '/account/inbox/support' : '/account/inbox/index';

        if (!isset($params['status'])) {
            $params['status'] = self::STATUS_ALL;
        }
        $this->load($params, '');

        $query = Conversation::find()
            ->select('conversation.id, conversation.type, conversation.title, conversation.avatar')
            ->addSelect(['hue' => (new Query)
                ->select("max(id)")
                ->from('message')
                ->where('conversation.id =  message.conversation_id')
            ])
            ->joinWith([
                'lastMessage.user' => function($q) {
                    return $q->select('user.id, user.is_company, user.username');
                },
                'conversationMembers',
                'lastMessage.user.profile',
                'lastMessage.attachments',
                'conversationMembers.user' => function($q) {
                    return $q->from(['cm_u' => 'user']);
                },
                'conversationMembers.user.profile' => function($q) {
                    return $q->from(['cm_p' => 'profile']);
                },
                'conversationMembers.user.profile.translation',
            ])
            ->where(['conversation_member.user_id' => $this->from_id])
            ->andWhere(['message.type' => [Message::TYPE_MESSAGE, Message::TYPE_GROUP_CHAT_INIT]])
            ->andWhere(['exists', Message::find()
                ->where('message.conversation_id = conversation.id')
            ])
            ->orderBy('hue desc')
            ->groupBy('conversation.id')
            ->limit($this->limit);
        $query = $this->addStatusWhereCondition($query, $this->status);
        $countQuery = clone $query;
        $countQuery->limit(null);

        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $this->limit,
            'pageSizeParam' => false,
            'route' => $route,
            'params' => $params
        ]);
        $pages->pageSizeParam = false;
        $query->offset($pages->offset)
            ->limit($pages->limit);

        $conversations = $query->all();
        $data = [
            'items' => array_filter(array_map(function($value) {
                $dto = new ConversationItemDTO($value, $this->from_id);
                return $dto->getData();
            }, $conversations)),
            'pagination' => $pages
        ];

        if (!$this->validate()) {
            $data = [
                'items' => [],
                'pagination' => $pages
            ];
        }

        return $data;
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_ALL => Yii::t('model', 'All'),
            self::STATUS_READ => Yii::t('model', 'Read'),
            self::STATUS_UNREAD => Yii::t('model', 'Unread'),
            self::STATUS_ORDER => Yii::t('model', 'Order'),
            self::STATUS_STARRED => Yii::t('model', 'Starred'),
            self::STATUS_DELETED => Yii::t('model', 'Deleted'),
//            self::STATUS_ARCHIVED => Yii::t('model', 'Archived'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @param $status
     * @return int|string
     */
    public function getStatusCount($status)
    {
        $query = Conversation::find()
            ->joinWith(['conversationMembers', 'messages'])
            ->andWhere(['conversation_member.user_id' => $this->from_id])
            ->groupBy('conversation.id')
            ->andWhere(['message.type' => [Message::TYPE_MESSAGE, Message::TYPE_GROUP_CHAT_INIT]]);
        return $this->addStatusWhereCondition($query, $status)->count();
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @param $type string
     * @return ActiveQuery
     */
    private function addStatusWhereCondition($query, $status)
    {
        if ($status !== self::STATUS_DELETED) {
            $query->andWhere(['not', ['conversation_member.status' => ConversationMember::STATUS_DELETED]]);
        }
        switch ($status) {
            case self::STATUS_ALL:
                break;
            case self::STATUS_STARRED:
                $query->andWhere(['conversation_member.starred' => true]);
                break;
            case self::STATUS_READ:
                $query->andWhere('message.created_at < conversation_member.viewed_at');
                $query->andWhere(['not', ['message.user_id' => $this->from_id]]);
                break;
            case self::STATUS_UNREAD:
                $query->andWhere('message.created_at >= conversation_member.viewed_at');
                $query->andWhere(['not', ['message.user_id' => $this->from_id]]);
                break;
            case self::STATUS_ORDER:
                $query->andWhere(['conversation.type' => Conversation::TYPE_ORDER]);
                break;
            default:
                $query->andWhere(['conversation_member.status' => $status]);
        }
        return $query;
    }
}
