<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.01.2018
 * Time: 18:14
 */

namespace frontend\modules\account\helpers;

use common\models\Job;
use common\models\JobPackage;
use common\modules\store\models\Order;

/**
 * Class InsolvencyFormHelper
 * @package frontend\modules\account\helpers
 */
class DynamicFormHelper
{
    /**
     * @param null $orderId
     * @return array
     */
    public static function getConfig($orderId = null)
    {
        $config = [];
        $order = Order::findOne($orderId);

        if ($order !== null) {
            $product = $order->product;
            $job = null;
            if ($product instanceof JobPackage) {
                $job = $product->job;
            }

            if ($job instanceof Job) {
                $requirements = $job->simpleRequirements;
                foreach ($requirements as $key => $requirement) {
                    if ($requirement->is_required) {
                        $rules = [
                            ["field_{$key}", 'string', 'min' => 2],
                            ["field_{$key}", 'required', 'skipOnEmpty' => false]
                        ];
                    } else {
                        $rules = [
                            ["field_{$key}", 'string', 'min' => 2],
                        ];
                    }
                    $config["field_{$key}"] = [
                        'rules' => $rules,
                        'label' => $requirement->title
                    ];
                }
            }
        }

        return $config;
    }

    /**
     * @param null $orderId
     * @return array
     */
    public static function getAdvancedConfig($orderId = null)
    {
        $config = [];
        $order = Order::findOne($orderId);

        if ($order !== null) {
            $product = $order->product;
            $job = null;
            if ($product instanceof JobPackage) {
                $job = $product->job;
            }

            $fieldIterator = 0;
            if ($job instanceof Job) {
                $steps = $job->requirementSteps;
                foreach ($steps as $key => $step) {
                    $config[$key]['title'] = $step->title;
                    foreach($step->jobRequirements as $reqKey => $requirement) {
                        if ($requirement->is_required) {
                            $rules = [
                                ["field_{$fieldIterator}", 'string', 'min' => 2],
                                ["field_{$fieldIterator}", 'required', 'skipOnEmpty' => false]
                            ];
                        } else {
                            $rules = [
                                ["field_{$fieldIterator}", 'string', 'min' => 2],
                            ];
                        }

                        $config[$key]['requirements']["field_{$fieldIterator}"] = [
                            'rules' => $rules,
                            'label' => $requirement->title
                        ];
                        $fieldIterator++;
                    }
                }
            }
        }

        return $config;
    }
}