<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 31.05.2017
 * Time: 16:33
 */

namespace frontend\modules\account\helpers;

use Yii;

/**
 * Class InsolvencyFormHelper
 * @package frontend\modules\account\helpers
 */
class DivorceFormHelper
{
    /**
     * @param null $orderId
     * @return array
     */
	public static function getConfig($orderId = null)
	{
		return [
			'claimant_firstname' => [
				'rules' => [
					['claimant_firstname', 'required'],
					['claimant_firstname', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Firstname')
			],
			'claimant_lastname' => [
				'rules' => [
					['claimant_lastname', 'required'],
					['claimant_lastname', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Lastname')
			],
			'claimant_middlename' => [
				'rules' => [
					['claimant_middlename', 'required'],
					['claimant_middlename', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Middlename')
			],
			'claimant_birthdate' => [
				'rules' => [
					['claimant_birthdate', 'required'],
					//['claimant_birthdate', 'date', 'format' => 'd-m-Y']
				],
				'label' => Yii::t('document', 'Birthdate')
			],
			'claimant_phone' => [
				'rules' => [
					['claimant_phone', 'required'],
					['claimant_phone', 'string', 'min' => 8]
				],
				'label' => Yii::t('document', 'Phone')
			],
			'claimant_email' => [
				'rules' => [
					['claimant_email', 'required'],
					['claimant_email', 'email']
				],
				'label' => Yii::t('document', 'Email')
			],
			'claimant_address_city' => [
				'rules' => [
					['claimant_address_city', 'required'],
					['claimant_address_city', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'City')
			],
			'claimant_address_street' => [
				'rules' => [
					['claimant_address_street', 'required'],
					['claimant_address_street', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'Street')
			],
			'claimant_address_house' => [
				'rules' => [
					['claimant_address_house', 'required'],
				],
				'label' => Yii::t('document', 'House')
			],
			'claimant_address_housing' => [
				'rules' => [
					['claimant_address_housing', 'safe'],
				],
				'label' => Yii::t('document', 'Housing')
			],
			'claimant_address_flat' => [
				'rules' => [
					['claimant_address_flat', 'integer']
				],
				'label' => Yii::t('document', 'Flat')
			],
			'defendant_firstname' => [
				'rules' => [
					['defendant_firstname', 'required'],
					['defendant_firstname', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Firstname')
			],
			'defendant_lastname' => [
				'rules' => [
					['defendant_lastname', 'required'],
					['defendant_lastname', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Lastname')
			],
			'defendant_middlename' => [
				'rules' => [
					['defendant_middlename', 'required'],
					['defendant_middlename', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Middlename')
			],
			'defendant_birthdate' => [
				'rules' => [
					['defendant_birthdate', 'required'],
					//['defendant_birthdate', 'date', 'format' => 'yyyy-M-d']
				],
				'label' => Yii::t('document', 'Birthdate')
			],
			'defendant_address_city' => [
				'rules' => [
					['defendant_address_city', 'required'],
					['defendant_address_city', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'City')
			],
			'defendant_address_street' => [
				'rules' => [
					['defendant_address_street', 'required'],
					['defendant_address_street', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'Street')
			],
			'defendant_address_house' => [
				'rules' => [
					['defendant_address_house', 'required'],
				],
				'label' => Yii::t('document', 'House')
			],
			'defendant_address_housing' => [
				'rules' => [
					['defendant_address_housing', 'safe'],
				],
				'label' => Yii::t('document', 'Housing')
			],
			'defendant_address_flat' => [
				'rules' => [
					['defendant_address_flat', 'integer']
				],
				'label' => Yii::t('document', 'Flat')
			],
			'marriage_date' => [
				'rules' => [
					['marriage_date', 'required'],
					//['marriage_date', 'date', 'format' => 'yyyy-M-d']
				],
				'label' => Yii::t('document', 'Marriage date')
			],
			'break_year' => [
				'rules' => [
					['break_year', 'required'],
					['break_year', 'integer']
				],
				'label' => Yii::t('document', 'Year of divorce')
			],
			'document_serial' => [
				'rules' => [
					['document_serial', 'required'],
					['document_serial', 'string']
				],
				'label' => Yii::t('document', 'Document serial')
			],
			'document_number' => [
				'rules' => [
					['document_number', 'required'],
					['document_number', 'integer']
				],
				'label' => Yii::t('document', 'Document number')
			],
			'document_city' => [
				'rules' => [
					//['document_city', 'required'],
					['document_city', 'string']
				],
				'label' => Yii::t('document', 'Document city')
			],
			'document_region' => [
				'rules' => [
					//['document_region', 'required'],
					['document_region', 'string']
				],
				'label' => Yii::t('document', 'Document region')
			],
		];
	}
}