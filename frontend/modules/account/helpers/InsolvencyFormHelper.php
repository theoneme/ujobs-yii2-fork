<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 31.05.2017
 * Time: 15:01
 */

namespace frontend\modules\account\helpers;

use Yii;

/**
 * Class InsolvencyFormHelper
 * @package frontend\modules\account\helpers
 */
class InsolvencyFormHelper
{
	/**
	 * @return array
	 */
	public static function getConfig()
	{
		return [
			'claimant_firstname' => [
				'rules' => [
					['claimant_firstname', 'required'],
					['claimant_firstname', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Firstname')
			],
			'claimant_lastname' => [
				'rules' => [
					['claimant_lastname', 'required'],
					['claimant_lastname', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Lastname')
			],
			'claimant_middlename' => [
				'rules' => [
					['claimant_middlename', 'required'],
					['claimant_middlename', 'string', 'min' => 2]
				],
				'label' => Yii::t('document', 'Middlename')
			],
			'claimant_phone' => [
				'rules' => [
					['claimant_phone', 'required'],
					['claimant_phone', 'string', 'min' => 8]
				],
				'label' => Yii::t('document', 'Phone')
			],
			'claimant_email' => [
				'rules' => [
					['claimant_email', 'required'],
					['claimant_email', 'email']
				],
				'label' => Yii::t('document', 'Email')
			],
			'credit' => [
				'rules' => [
					['credit', 'required'],
					['credit', 'number']
				],
				'label' => Yii::t('document', 'Credit')
			],
			'creditor_title' => [
				'rules' => [
					['creditor_title', 'required'],
					['creditor_title', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'Creditor')
			],
			'claimant_address_city' => [
				'rules' => [
					['claimant_address_city', 'required'],
					['claimant_address_city', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'City')
			],
			'claimant_address_street' => [
				'rules' => [
					['claimant_address_street', 'required'],
					['claimant_address_street', 'string', 'min' => 5]
				],
				'label' => Yii::t('document', 'Street')
			],
			'claimant_address_house' => [
				'rules' => [
					['claimant_address_house', 'required'],
				],
				'label' => Yii::t('document', 'House')
			],
			'claimant_address_housing' => [
				'rules' => [
					['claimant_address_housing', 'safe'],
				],
				'label' => Yii::t('document', 'Housing')
			],
			'claimant_address_flat' => [
				'rules' => [
					['claimant_address_flat', 'integer']
				],
				'label' => Yii::t('document', 'Flat')
			]
		];
	}
}