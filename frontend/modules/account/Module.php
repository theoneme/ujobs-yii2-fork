<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.11.2016
 * Time: 16:05
 */

namespace frontend\modules\account;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\account\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/attribute/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@frontend/modules/account/messages',
            'fileMap' => [
                'modules/account/app' => 'app.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/account/' . $category, $message, $params, $language);
    }
}