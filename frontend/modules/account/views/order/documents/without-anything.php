<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 31.05.2017
 * Time: 17:03
 */
use frontend\modules\account\models\history\DocumentDecisionForm;

/* @var $model DocumentDecisionForm */

?>

<div class="wrap-doc">
	<div class="right-head">
		<div class="rh-item">
			___________ <span>суд    Свердловской  области </span><br/>
			___________
		</div>
		<br/>

		<div class="rh-item">
			<span>Истец:</span> <?= Yii::$app->utility->formatName($model->claimant_firstname, $model->claimant_lastname, $model->claimant_middlename) ?>
		</div>
		<div class="rh-item">Зарегистрированный по
			адресу: <?= Yii::$app->utility->formatAddress($model->claimant_address_city, $model->claimant_address_street, $model->claimant_address_house, $model->claimant_address_flat) ?></div>
		<div class="rh-item"><span>Тел</span> <?= $model->claimant_phone ?></div>
		<br/>

		<div class="rh-item">
			<span>Ответчик:</span> <?= Yii::$app->utility->formatName($model->defendant_firstname, $model->defendant_lastname, $model->defendant_middlename) ?>
		</div>
		<div class="rh-item">Зарегистрированная по
			адресу: <?= Yii::$app->utility->formatAddress($model->defendant_address_city, $model->defendant_address_street, $model->defendant_address_house, $model->defendant_address_flat) ?></div>
		<div class="rh-item"><span>Государственная  пошлина:</span> 600 рублей</div>
	</div>

	<div class="clear"></div>

	<div class="title1">
		ИСКОВОЕ ЗАЯВЛЕНИЕ <br/>
		о расторжении брака
	</div>
	<p>
		Я, <?= Yii::$app->utility->formatName($model->claimant_firstname, $model->claimant_lastname, $model->claimant_middlename) ?>, состою в зарегистрированном браке
		с <?= Yii::$app->utility->formatName($model->defendant_firstname, $model->defendant_lastname, $model->defendant_middlename) ?>,
		<?= Yii::$app->formatter->asDate($model->defendant_birthdate, "Y") ?> года рождения, что
		подтверждается Свидетельством о заключении брака серии <?= $model->document_serial ?>
		№<?= $model->document_number ?> выданным в <?= Yii::$app->formatter->asDate($model->marriage_date, "Y") ?> г.
		Комитетом записи актов гражданского состояния Администрации города ________________ _________________ области
		Российской
		Федерации. Совместных детей не имеем.
	</p>

	<p>
		Брачные отношения между мной и ответчицей фактически прекращены с <?= $model->break_year ?> года, совместное
		хозяйство нами
		не ведется, общего бюджета не имеем.
	</p>

	<p>
		В настоящее время я настаиваю на расторжении брака, заключенного между мной
		и <?= Yii::$app->utility->formatName($model->defendant_firstname, $model->defendant_lastname, $model->defendant_middlename) ?>, в связи с тем, что
		совместная жизнь с ответчицей не представляется возможной, ввиду того, что у нас утрачено чувство любви и
		взаимопонимания.
	</p>

	<p>
		Согласно ст. 21 Семейного кодекса РФ расторжение брака производится в судебном порядке в случаях, если один из
		супругов, несмотря на отсутствие у него возражений, уклоняется от расторжения брака в органе записи актов
		гражданского состояния (отказывается подать заявление, не желает явиться для государственной регистрации
		расторжения брака и другое).
	</p>

	<p>
		В соответствии со ст. 22 Семейного кодекса РФ расторжение брака в судебном порядке производится, если судом
		установлено, что дальнейшая совместная жизнь супругов и сохранение семьи невозможны.
	</p>

	<p>
		В соответствии с п.10 Постановления Пленума Верховного Суда РФ от 05.11.1998 г. № 15 «О применении судами
		законодательства при рассмотрении дел о расторжении брака» срок, назначенный для примирения, может быть
		сокращен, если об этом просят стороны, а причины, указанные ими, будут признаны судом уважительными. В этих
		случаях должно быть вынесено мотивированное определение. Определение суда об отложении разбирательства дела для
		примирения супругов не может быть обжаловано в апелляционном и кассационном порядке, так как оно не исключает
		возможности дальнейшего движения дела. Если после истечения назначенного судом срока примирение супругов не
		состоялось и хотя бы один из них настаивает на прекращении брака, суд расторгает брак.
	</p>

	<p>
		Ввиду фактического прекращения между нами семейных отношений с <?= $model->break_year ?> года, невозможности
		дальнейшего
		сохранения семьи, я вынужден обратиться в суд с данным иском, кроме того ответчик уклоняется от расторжения
		брака в органах ЗАГСа.
	</p>

	<p>
		При этом, на основании вышесказанного, считаю, что нет необходимости в сроке для примирения, в связи с чем я
		настаиваю на расторжении брака.
	</p>
	<br/>

	<p>
		<span>На основании изложенного,</span>
	</p>
	<br/>

	<div class="title2">
		ПРОШУ:
	</div>
	<br/>
	<ol class="bold">
		<li>Расторгнуть брак между мной, <?= Yii::$app->utility->formatName($model->claimant_firstname, $model->claimant_lastname, $model->claimant_middlename) ?>,
			и <?= Yii::$app->utility->formatName($model->defendant_firstname, $model->defendant_lastname, $model->defendant_middlename) ?>, с момента вступления решения суда в
			законную
			силу.
		</li>
	</ol>
	<br/>
	<br/>
	Приложение:
	<ol>
		<li>Копия квитанции об оплате госпошлины;</li>
		<li>Копия паспорта;</li>
		<li>Копия Свидетельства о заключении брака серии <?= $model->document_serial ?> № <?= $model->document_number ?>
			;
		</li>
	</ol>
	<div class="end-doc">
		<div class="ed-left">
			<?= Yii::$app->formatter->asDate($time, "«d» MMMM yyyy") ?> года
		</div>
		<div class="ed-right">
			<div class="name"><?= Yii::$app->utility->formatName($model->claimant_firstname, $model->claimant_lastname, $model->claimant_middlename) ?></div>
			<div class="subscr"></div>
		</div>
	</div>
</div>
