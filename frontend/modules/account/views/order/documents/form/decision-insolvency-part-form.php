<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 31.05.2017
 * Time: 14:32
 */

use common\helpers\SecurityHelper;
use frontend\modules\account\models\history\DecisionForm;
use yii\helpers\Html;
use common\modules\store\models\Order;
use kartik\date\DatePicker;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var Order $order
 * @var DecisionForm $formModel
 * @var boolean $autoAccept
 */

$key = uniqid();

?>

<div class="message-block">
	<div class="message-item">
		<?php $form = ActiveForm::begin([
			'options' => [
				'id' => "{$key}-form",
				'enctype' => 'multipart/form-data',
				'class' => 'row send-message decision',
                'data-accept' => (int)$autoAccept
			],
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'action' => Url::to(['/account/order/decision']),
		]); ?>
		<?= Html::input('hidden', 'o', SecurityHelper::encrypt(StringHelper::basename(get_class($formModel)))); ?>
		<?= Html::input('hidden', 'd', $d); ?>
		<?= $form->field($formModel, 'order_id')->hiddenInput(['value' => $order->id])->label(false) ?>
		<?= $form->field($formModel, 'h')->hiddenInput(['value' => $formModel->h])->label(false) ?>
		<?= $form->field($formModel, 't')->hiddenInput(['value' => $formModel->t])->label(false) ?>
		<?= $form->field($formModel, 'zxToken')->hiddenInput(['value' => $formModel->zxToken])->label(false) ?>
		<div class="white-mes-bg">
			<h2 class="text-center">
				<?= Yii::t('document', 'Personal information') ?>
			</h2>
			<div class="row">
				<div class="col-md-4">
					<?= $form->field($formModel, 'claimant_lastname') ?>
				</div>
				<div class="col-md-4">
					<?= $form->field($formModel, 'claimant_firstname') ?>
				</div>
				<div class="col-md-4">
					<?= $form->field($formModel, 'claimant_middlename') ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_phone') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_email') ?>
				</div>
			</div>
			<h2 class="text-center">
				<?= Yii::t('document', 'Credit information') ?>
			</h2>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'credit')->textInput(['type' => 'number']) ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'creditor_title') ?>
				</div>
			</div>
			<h2 class="text-center">
				<?= Yii::t('document', 'Address information') ?>
			</h2>
			<?= $form->field($formModel, 'claimant_address_city') ?>
			<?= $form->field($formModel, 'claimant_address_street') ?>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_address_house') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_address_housing') ?>
				</div>
			</div>
			<?= $form->field($formModel, 'claimant_address_flat')->textInput(['type' => 'number']) ?>
		</div>
		<div class="row">
			<div class="col-md-12 text-right">
				<?= Html::submitInput(Yii::t('account', 'Send'), ['class' => 'btn-small']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>