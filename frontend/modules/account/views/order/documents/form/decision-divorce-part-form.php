<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 31.05.2017
 * Time: 16:40
 */

use common\helpers\SecurityHelper;
use frontend\modules\account\models\history\DecisionForm;
use yii\helpers\Html;
use common\modules\store\models\Order;
use kartik\date\DatePicker;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var Order $order
 * @var DecisionForm $formModel
 * @var boolean $autoAccept
 */

$key = uniqid();

?>

<div class="message-block">
	<div class="message-item">
		<?php $form = ActiveForm::begin([
			'options' => [
				'id' => "{$key}-form",
				'enctype' => 'multipart/form-data',
				'class' => 'row send-message decision',
                'data-accept' => (int)$autoAccept
			],
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'action' => Url::to(['/account/order/decision']),
		]); ?>
		<?= Html::input('hidden', 'o', SecurityHelper::encrypt(StringHelper::basename(get_class($formModel)))); ?>
		<?= Html::input('hidden', 'd', $d); ?>
		<?= $form->field($formModel, 'order_id')->hiddenInput(['value' => $order->id])->label(false) ?>
		<?= $form->field($formModel, 'h')->hiddenInput(['value' => $formModel->h])->label(false) ?>
		<?= $form->field($formModel, 't')->hiddenInput(['value' => $formModel->t])->label(false) ?>
		<?= $form->field($formModel, 'zxToken')->hiddenInput(['value' => $formModel->zxToken])->label(false) ?>
		<div class="white-mes-bg">
			<h2 class="text-center">
				<?= Yii::t('document', 'Claimant personal information') ?>
			</h2>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_lastname') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_firstname') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_middlename') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_birthdate')->textInput([
						'type' => 'date'
					]) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_phone') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_email') ?>
				</div>
			</div>
			<h2 class="text-center">
				<?= Yii::t('document', 'Claimant address information') ?>
			</h2>
			<?= $form->field($formModel, 'claimant_address_city') ?>
			<?= $form->field($formModel, 'claimant_address_street') ?>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_address_house') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'claimant_address_housing') ?>
				</div>
			</div>
			<?= $form->field($formModel, 'claimant_address_flat')->textInput(['type' => 'number']) ?>

			<h2 class="text-center">
				<?= Yii::t('document', 'Defendant personal information') ?>
			</h2>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'defendant_lastname') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'defendant_firstname') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'defendant_middlename') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'defendant_birthdate')->textInput([
						'type' => 'date'
					]) ?>
				</div>
			</div>
			<h2 class="text-center">
				<?= Yii::t('document', 'Defendant address information') ?>
			</h2>
			<?= $form->field($formModel, 'defendant_address_city') ?>
			<?= $form->field($formModel, 'defendant_address_street') ?>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'defendant_address_house') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'defendant_address_housing') ?>
				</div>
			</div>
			<?= $form->field($formModel, 'defendant_address_flat')->textInput(['type' => 'number']) ?>
			<h2 class="text-center">
				<?= Yii::t('document', 'Marriage dates') ?>
			</h2>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'marriage_date')->textInput([
						'type' => 'date'
					]) ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'break_year')->textInput([
						'type' => 'number',
						'min' => 1950,
						'value' => 2000
					]) ?>
				</div>
			</div>
			<h2 class="text-center">
				<?= Yii::t('document', 'Document number and serial') ?>
			</h2>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($formModel, 'document_serial') ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($formModel, 'document_number') ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-right">
				<?= Html::submitInput(Yii::t('account', 'Send'), ['class' => 'btn-small']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>