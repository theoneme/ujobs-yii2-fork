<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.05.2017
 * Time: 13:12
 */

use common\modules\store\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var Order $model
 * @var View $this
 */

?>

<div class="jm-option">
    <div class="jm-img">
        <?= Html::a(Html::img($model->product->getThumb('catalog'), ['class' => 'order-item-thumb']),
            ['/account/order/history', 'id' => $model->id],
            ['data-pjax' => 0]
        ) ?>
    </div>
    <div class="jm-name">
        <?= Html::a($model->product->getLabel(),
            ['/account/order/history', 'id' => $model->id],
            ['data-pjax' => 0]
        ) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Order Date')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->created_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Due On')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->tbd_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Total')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getTotal() ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
       <?= mb_strtoupper(Yii::t('account', 'Status')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getStatusLabel(true) ?>
    </div>
</div>
