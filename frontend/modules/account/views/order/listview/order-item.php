<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.12.2016
 * Time: 18:21
 */

use yii\helpers\Html;
use common\modules\store\models\Order;
use yii\helpers\Url;
use yii\web\View;

/* @var Order $model
 * @var View $this
 */

?>
<td>
    <?= Html::a(Html::img($model->product->getThumb('catalog'), ['class' => 'order-item-thumb', 'alt' => $model->product->getLabel(),'title' => $model->product->getLabel()]),
        ['/account/order/history', 'id' => $model->id],
        ['class' => 'tg-img', 'data-pjax' => 0]
    ) ?>
</td>
<td>
    <?= Html::a($model->product->getLabel(),
        ['/account/order/history', 'id' => $model->id],
        ['class' => 'tg-name', 'data-pjax' => 0]
    ) ?>
</td>
<td>
    <?= Yii::$app->formatter->asDate($model->created_at) ?>
</td>
<td>
    <?= Yii::$app->formatter->asDate($model->tbd_at) ?>
</td>
<td>
    <?= $model->getTotal() ?>
</td>
<td>
    <?= $model->getStatusLabel(true) ?>
</td>