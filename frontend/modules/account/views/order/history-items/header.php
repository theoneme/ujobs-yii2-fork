<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 30.05.2017
 * Time: 13:50
 */

use yii\helpers\Html;
use common\modules\store\models\OrderHistory;

/* @var OrderHistory $history */

?>

<div class="order-step">
	<div class="order-step-head">
		<div class="osh-icon osh-icon-<?= $history->getIcon() ?>"></div>
		<div class="osh-title text-center"><?= $history->getHeader() ?></div>
		<?php if ($subHeader = $history->getSubHeader()) { ?>
			<p class="osh-descr text-center">
				<?= $subHeader ?>
			</p>
		<?php } elseif ($history->content) { ?>
			<p class="osh-descr text-center">
				<?= nl2br(Html::encode($history->content)) ?>
			</p>
		<?php } ?>
		<div class="order-step-date"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
	</div>
</div>