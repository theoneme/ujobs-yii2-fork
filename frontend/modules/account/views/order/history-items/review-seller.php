<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.01.2017
 * Time: 18:13
 */

use common\modules\store\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\rating\StarRating;
use common\modules\store\models\OrderHistory;

/* @var OrderHistory $history */
/* @var Order $model */

?>

<div class="order-step">
    <div class="order-step-body">
        <div class="order-step-review">
            <div class="answer-table">
                <div class="answer-avatar">
                    <?= Html::a(Html::img($model->customer->getThumb('catalog'), [
                        'alt' => Html::encode($model->customer->getSellerName())
                    ]), [
                        '/account/profile/show', 'id' => $model->customer->id, 'class' => 'text-center'
                    ], ['data-pjax' => 0]) ?>
                </div>
                <div class="answer-info">
                    <div class="review-grey row">
                        <div class="my-rev-left">
                            <div class="order-step-buyer"><?= Html::encode($model->customer->getSellerName()) ?> </div>
                            <div class="order-rev-time"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
                            <p>
                                <?= nl2br(Html::encode($history->content)) ?>
                            </p>
                        </div>
                        <div class="my-rev-right">
                            <div class="rev-stars">
                                <?= $this->render('@frontend/widgets/views/rating', [
                                    'rating' => $model->sellerReview->rating / 2,
                                    'size' => 24
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="order-rate-list">
                        <div class="order-rate-line row">
                            <div class="order-rate-name">
                                <?= Yii::t('account', 'Communication with seller') ?>
                            </div>
                            <div class="text-right">
                                <div class="rev-stars">
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $model->sellerReview->communication / 2,
                                        'size' => 18
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="order-rate-line row">
                            <div class="order-rate-name">
                                <?= Yii::t('account', 'Service as Described') ?>
                            </div>
                            <div class="text-right">
                                <div class="rev-stars">
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $model->sellerReview->service / 2,
                                        'size' => 18
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="order-rate-line row">
                            <div class="order-rate-name">
                                <?= Yii::t('account', 'I would order again or recommend to someone') ?>
                            </div>
                            <div class="text-right">
                                <div class="rev-stars">
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $model->sellerReview->recommend / 2,
                                        'size' => 18
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>