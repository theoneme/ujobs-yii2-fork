<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 30.05.2017
 * Time: 13:50
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var Order $model */
/* @var OrderHistory $history */

?>

<div class="order-step">
	<div class="order-step-body">
		<div class="answer-table">
			<div class="answer-avatar">
				<?= Html::a(Html::img($history->sender->getThumb('catalog'), [
					'alt' => Html::encode($history->sender->getSellerName())
				]), [
					'/account/profile/show', 'id' => $history->sender_id, 'class' => 'text-center'
				], ['data-pjax' => 0]) ?>
			</div>
			<?php if ($model->product) {?>
				<div class="answer-info">
					<div class="require-mes">
						<?= Html::a($history->sender->getSellerName(), [
							'/account/profile/show', 'id' => $history->sender_id
						], ['data-pjax' => 0]) ?>
						<p><?= $history->getSubHeader() ?> </p>
					</div>
					<?php if($model->product->getRequirements() && $model->product->getRequirements() != $history->content) { ?>
						<div class="require-answer">
							<p><?= nl2br(Html::encode($model->product->getRequirements())) ?></p>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>