<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.01.2017
 * Time: 20:01
 */

use common\modules\store\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\rating\StarRating;
use common\modules\store\models\OrderHistory;

/* @var OrderHistory $history */
/* @var Order $model */

?>

<div class="order-step">
    <div class="order-step-body">
        <div class="order-step-review">
            <div class="answer-table">
                <div class="answer-avatar">
                    <?= Html::a(Html::img($model->seller->getThumb('catalog'), [
                        'alt' => Html::encode($model->seller->getSellerName())
                    ]), [
                        '/account/profile/show', 'id' => $model->seller->id, 'class' => 'text-center'
                    ], ['data-pjax' => 0]) ?>
                </div>
                <div class="answer-info">
                    <div class="order-step-buyer"><?= Html::encode($model->seller->getSellerName()) ?> </div>
                    <div class="order-rev-time"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
                    <div class="rev-stars">
                        <?= $this->render('@frontend/widgets/views/rating', [
                            'rating' => $model->customerReview->rating / 2,
                            'size' => 22
                        ]) ?>
                    </div>
                    <p>
                        <?= nl2br(Html::encode($history->content)) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>