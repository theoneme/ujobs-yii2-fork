<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 30.05.2017
 * Time: 13:50
 */

use common\models\Attachment;
use yii\helpers\Html;
use common\modules\store\models\OrderHistory;
use yii\helpers\Url;

/* @var OrderHistory $history */

?>

	<div class="order-step">
		<div class="order-step-head">
			<div class="osh-icon osh-icon-<?= $history->getIcon() ?>"></div>
			<div class="osh-title text-center"><?= $history->getHeader() ?></div>
			<?php if ($subHeader = $history->getSubHeader()) { ?>
				<p class="osh-descr text-center">
					<?= $subHeader ?>
				</p>
			<?php } ?>
			<div class="order-step-date"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
		</div>
	</div>
<?php if ($history->content) { ?>
	<div class="order-step">
		<div class="order-step-body">
			<div class="answer-table">
				<div class="answer-avatar">
					<?= Html::a(Html::img($history->sender->getThumb('catalog'), [
						'alt' => Html::encode($history->sender->getSellerName())
					]), [
						'/account/profile/show', 'id' => $history->sender_id, 'class' => 'text-center'
					], ['data-pjax' => 0]) ?>
				</div>
				<div class="answer-info">
					<div class="order-step-buyer"><?= Html::encode($history->sender->getSellerName()) ?></div>
					<p><?= nl2br(Html::encode($history->content)) ?></p>

					<?php if ($history->attachments) { ?>
						<div class="order-attachment">
							<div class="attach-title"><?= Yii::t('account', 'Attachments') ?></div>
							<div class="message-attachments">
								<?php /* @var Attachment $attachment */ ?>
								<?php foreach ($history->attachments as $attachment) { ?>
									<div class="message-attachment">
										<div class="file-container">
                                            <?= Html::a(Html::img($attachment->getThumb()),
                                                Yii::$app->mediaLayer->tryLoadFromAws($attachment->content), [
                                                    'download' => true,
                                                    'data-pjax' => 0
                                            ]) ?>
										</div>
										<?= Html::a('<i class="fa fa-download" aria-hidden="true"></i>' . basename($attachment->content),
                                            Yii::$app->mediaLayer->tryLoadFromAws($attachment->content), [
												'download' => true,
												'data-pjax' => 0
											]) ?>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="order-step-date"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
		</div>
	</div>
<?php } ?>