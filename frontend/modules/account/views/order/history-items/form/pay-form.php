<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.03.2017
 * Time: 15:03
 */

use common\components\CurrencyHelper;
use common\modules\store\models\Order;
use frontend\modules\account\models\history\PayForm;
use yii\helpers\Url;

/* @var PayForm $formModel */
/* @var Order $order */
/* @var mixed $heading */

?>

<div class="order-step">
    <div class="order-step-body">
        <div class="require-answer">
            <div class="alert alert-info text-center">
                <strong>
                    <?php if ($order->product_type === Order::TYPE_PRODUCT_TENDER) { ?>
                        <?= Yii::t('order', 'Thanks for choosing seller with our site.') ?> <br>
                        <?= Yii::t('order', 'To begin, you have to pay deposit equal to {value}, which will be paid to seller when you receive the product.', ['value' => $order->getTotal()]) ?>
                    <?php } else if ($order->product_type === Order::TYPE_SIMPLE_TENDER) { ?>
                        <?= Yii::t('order', 'Thanks for choosing worker with our site.') ?> <br>
                        <?= Yii::t('order', 'To begin, you have to pay deposit equal to {value}, which will be paid to worker when you will accept his job.', ['value' => $order->getTotal()]) ?>
                    <?php } else { ?>
                        <?php if ($formModel->customLabel === null) { ?>
                            <?= Yii::t('order', 'Thanks for choosing worker with our site.') ?> <br>
                            <?= Yii::t('order', 'Firstly, you have to pay deposit equal to {value}, which will be used to reward the worker.', ['value' => $order->getTotal()]) ?>
                            <br>
                            <?= Yii::t('order', 'Payments for worker occur every week, when he completes your tasks.') ?>
                            <br>
                            <?= Yii::t('order', 'If the tasks will not be completed, or will be completed partly, you may send task for revision, or refuse further cooperation.') ?>
                            <br>
                            <?= Yii::t('order', 'You can get your money back to use them, to buy other services or to pay for the work of another freelancer.') ?>
                            <br>
                        <?php } else { ?>
                            <?= $formModel->customLabel ?>
                        <?php } ?>
                    <?php } ?>
                </strong>
            </div>
        </div>
        <?= $this->render('@common/modules/store/views/checkout/checkout-payment-methods', [
            'paymentMethods' => $formModel->paymentMethods,
            'checkoutUrl' => Url::to(['/account/order/pay']),
            'total' => CurrencyHelper::convert($order->currency_code, Yii::$app->params['app_currency_code'], $order->total, true),
            'currency_code' => $order->currency_code,
            'params' => ['order_id' => $order->id]
        ]) ?>
    </div>
</div>