<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.01.2018
 * Time: 15:53
 */

use yii\helpers\Html;

/* @var array $route */

?>

<div class="message-block">
    <div class="message-item">
        <p>
            <?= Yii::t('order', "You have to fill seller`s requirements to continue this order") ?>
        </p>
        <p>
            <?= Html::a(Yii::t('order', 'Fill requirements'), $route, ['data-pjax' => 0]) ?>
        </p>
    </div>
</div>