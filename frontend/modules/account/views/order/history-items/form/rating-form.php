<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.05.2017
 * Time: 18:07
 */

use common\helpers\SecurityHelper;
use frontend\modules\account\models\history\RatingForm;
use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\store\models\Order;

/**
 * @var Order $order
 * @var RatingForm $formModel
 * @var boolean $autoAccept
 */

?>

<div class="message-block">
	<?php $form = ActiveForm::begin([
		'options' => [
			'id' => "rating-form",
			'enctype' => 'multipart/form-data',
			'class' => 'row send-message decision',
			'data-accept' => 0
		],
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'action' => Url::to(['/account/order/decision']),
	]); ?>
	<?= Html::input('hidden', 'o', SecurityHelper::encrypt(StringHelper::basename(get_class($formModel)))); ?>
	<?= Html::input('hidden', 's', SecurityHelper::encrypt($formModel->scenario)); ?>
	<?= $form->field($formModel, 'order_id')->hiddenInput(['value' => $order->id])->label(false) ?>
	<?= $form->field($formModel, 'h')->hiddenInput(['value' => $formModel->h])->label(false) ?>
	<?= $form->field($formModel, 't')->hiddenInput(['value' => $formModel->t])->label(false) ?>
	<?= $form->field($formModel, 'type')->hiddenInput(['value' => $formModel->type])->label(false) ?>
	<?= $form->field($formModel, 'zxToken')->hiddenInput(['value' => $formModel->zxToken])->label(false) ?>
	<div class="white-mes-bg clearfix">
		<div class="col-xs-6 rating-label-sm">
			<?php if ($formModel->scenario === RatingForm::SCENARIO_SELLER) { ?>
				<?= Yii::t('account', 'Rate order') ?>
			<?php } else { ?>
				<?= Yii::t('account', 'Please, leave feedback about the buyer...') ?>
			<?php } ?>
		</div>
		<div class="col-xs-6 text-right">
			<?= StarRating::widget(['model' => $formModel, 'attribute' => 'rating',
				'pluginOptions' => [
					'theme' => 'krajee-uni',
					'filledStar' => '&#x2605;',
					'emptyStar' => '&#x2606;',
					'size' => 'sm',
					'showCaption' => false,
					'min' => 0,
					'max' => 10,
					'step' => 1,
				]
			]); ?>
		</div>
	</div>

	<?php if ($formModel->scenario === RatingForm::SCENARIO_SELLER) { ?>
		<div class="white-mes-bg clearfix">
			<div class="col-xs-6 rating-label-xs">
				<?= Yii::t('account', 'Contact seller') ?>
			</div>
			<div class="col-xs-6 text-right">
				<?= StarRating::widget(['model' => $formModel, 'attribute' => 'communication',
					'pluginOptions' => [
						'theme' => 'krajee-uni',
						'filledStar' => '&#x2605;',
						'emptyStar' => '&#x2606;',
						'size' => 'xs',
						'showCaption' => false,
						'min' => 0,
						'max' => 10,
						'step' => 1,
					]
				]); ?>
			</div>

			<div class="col-xs-6 rating-label-xs">
				<?= Yii::t('account', 'Service as Described') ?>
			</div>
			<div class="col-xs-6 text-right">
				<?= StarRating::widget(['model' => $formModel, 'attribute' => 'service',
					'pluginOptions' => [
						'theme' => 'krajee-uni',
						'filledStar' => '&#x2605;',
						'emptyStar' => '&#x2606;',
						'size' => 'xs',
						'showCaption' => false,
						'min' => 0,
						'max' => 10,
						'step' => 1,
					]
				]); ?>
			</div>

			<div class="col-xs-6 rating-label-xs">
				<?= Yii::t('account', 'Would order again or recommend someone?') ?>
			</div>
			<div class="col-xs-6 text-right">
				<?= StarRating::widget(['model' => $formModel, 'attribute' => 'recommend',
					'pluginOptions' => [
						'theme' => 'krajee-uni',
						'filledStar' => '&#x2605;',
						'emptyStar' => '&#x2606;',
						'size' => 'xs',
						'showCaption' => false,
						'min' => 0,
						'max' => 10,
						'step' => 1,
					]
				]); ?>
			</div>
		</div>
	<?php } ?>
	
	<div class="col-md-12 white-mes-bg">
		<?= $form->field($formModel, 'content', [
			'template' => "{input}\n{error}",
		])->textarea([
			'class' => 'readsym',
			'placeholder' => Yii::t('account', 'Type your review here...')
		]) ?>
		<div class="white-mes-bottom text-left row">
			<div class="counttext">
				<span class="ctspan">0</span> / 300
			</div>
		</div>
	</div>
	<div class="col-md-12 text-right">
		<?= Html::submitInput(Yii::t('account', 'Send'), ['class' => 'btn-small']) ?>
		<br><br>
	</div>
	<?php ActiveForm::end(); ?>
</div>