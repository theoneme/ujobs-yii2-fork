<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.05.2017
 * Time: 12:43
 */

use common\helpers\SecurityHelper;
use frontend\modules\account\models\history\DecisionForm;
use yii\helpers\Html;
use common\modules\store\models\Order;
use kartik\date\DatePicker;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var Order $order
 * @var DecisionForm $formModel
 * @var boolean $autoAccept
 * @var string $key
 */

?>

<div class="message-block">
	<div class="message-item">
		<?php $form = ActiveForm::begin([
			'options' => [
				'id' => "{$key}-form",
				'enctype' => 'multipart/form-data',
				'class' => 'row send-message decision',
                'data-accept' => (int)$autoAccept
			],
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'action' => Url::to(['/account/order/decision']),
		]); ?>
		<?= Html::input('hidden', 'o', SecurityHelper::encrypt(StringHelper::basename(get_class($formModel)))); ?>
		<?= $form->field($formModel, 'order_id')->hiddenInput(['value' => $order->id, 'id' => "{$key}-order_id"])->label(false) ?>
		<?= $form->field($formModel, 'h')->hiddenInput(['value' => $formModel->h, 'id' => "{$key}-h"])->label(false) ?>
		<?= $form->field($formModel, 't')->hiddenInput(['value' => $formModel->t, 'id' => "{$key}-t"])->label(false) ?>
		<?= $form->field($formModel, 'type')->hiddenInput(['value' => $formModel->type, 'id' => "{$key}-type"])->label(false) ?>
		<?= $form->field($formModel, 'zxToken')->hiddenInput(['value' => $formModel->zxToken, 'id' => "{$key}-zxtoken"])->label(false) ?>
		<div class="white-mes-bg">
			<?= $form->field($formModel, 'price', [
				'template' => "{input}\n{error}",
			])->textInput([
				'type' => 'number',
                'id' => "{$key}-price",
				'class' => 'readsym',
				'placeholder' => Yii::t('account', 'Specify your price')
			]) ?>
		</div>
		<div class="white-mes-bg">
			<?= $form->field($formModel, 'content', [
				'template' => "{input}\n{error}",
			])->textarea([
				'class' => 'readsym',
                'id' => "{$key}-content",
				'placeholder' => $formModel->placeholder
			]) ?>
			<div class="white-mes-bottom text-left row">
				<div class="counttext">
					<span class="ctspan">0</span> / 1200
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-right">
				<?= Html::submitInput(Yii::t('account', 'Send'), ['class' => 'btn-small']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>