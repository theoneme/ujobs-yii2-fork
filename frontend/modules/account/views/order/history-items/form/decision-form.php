<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.05.2017
 * Time: 17:08
 */

use common\modules\store\models\Order;

/* @var string $alert */
/* @var array $data */
/* @var Order $order */

?>

<div class="order-step">
	<div class="order-step-body">
		<?php if (isset($data['alert'])) { ?>
			<div class="alert alert-info text-center">
				<strong><?= $data['alert'] ?></strong>
			</div>
		<?php } ?>

		<div class="form-box">
			<div class="time-options">
				<?php $i = 0; foreach ((array)$data['forms'] as $key => $item) { ?>
					<div>
						<input data-class="decision_<?= $key ?>" id="decision_<?= $key ?>" type="radio"
						       value="<?= $key ?>" name="decision" data-f="<?= $key ?>-form">
						<label for="decision_<?= $key ?>">
							<span class="ctext" style="width: 190px"><?= $item['label'] ?></span>
						</label>
					</div>

					<?php $i++ ?>
					<?php if ($i % 2 === 0 && count($data['forms']) % 4 === 0 && $i > 0) { ?>
						<?= "</div><div class='time-options' style='margin-top:5px'>" ?>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
		<p>&nbsp;</p>

		<div class="decision_forms">
			<?php foreach ((array)$data['forms'] as $key => $item) { ?>
				<div class="decision_<?= $key ?> hidden">
					<?= $this->render($item['view'], [
						'formModel' => $item['form'],
						'autoAccept' => $item['autoAccept'],
						'key' => $key,
						'order' => $order
					]); ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>