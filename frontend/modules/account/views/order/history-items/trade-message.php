<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.11.2017
 * Time: 13:47
 */

use common\modules\store\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\store\models\OrderHistory;
use common\models\Attachment;

/* @var OrderHistory $history */
/* @var Order $model */
?>
<div class="order-step">
    <div class="order-step-body">
        <?php if ($alert = $history->getAlert()) { ?>
            <div class="alert alert-info text-center">
                <strong><?= $alert ?></strong>
            </div>
        <?php } ?>
        <div class="answer-table">
            <div class="answer-avatar">
                <?= Html::a(Html::img($history->sender->getThumb('catalog'), [
                    'alt' => Html::encode($history->sender->getSellerName())
                ]), [
                    '/account/profile/show', 'id' => $history->sender_id, 'class' => 'text-center'
                ], ['data-pjax' => 0]) ?>
            </div>
            <div class="answer-info">
                <div class="order-step-buyer"><?= Html::encode($history->sender->getSellerName()) ?></div>
                <p><?= nl2br(Html::encode($history->content)) ?></p>
                <p><?= $history->getSubHeader() ?></p>
            </div>
        </div>
        <div class="order-step-date"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
    </div>
</div>