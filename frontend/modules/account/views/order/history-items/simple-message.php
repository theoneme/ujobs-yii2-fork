<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 30.05.2017
 * Time: 13:50
 */

use common\modules\store\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\store\models\OrderHistory;
use common\models\Attachment;

/* @var OrderHistory $history */
/* @var Order $model */
?>
<div class="order-step">
	<div class="order-step-body">
		<?php if ($alert = $history->getAlert()) { ?>
			<div class="alert alert-info text-center">
				<strong><?= $alert ?></strong>
			</div>
		<?php } ?>
		<div class="answer-table">
			<div class="answer-avatar">
				<?= Html::a(Html::img($history->sender->getThumb('catalog'), [
					'alt' => Html::encode($history->sender->getSellerName())
				]), [
					'/account/profile/show', 'id' => $history->sender_id, 'class' => 'text-center'
				], ['data-pjax' => 0]) ?>
			</div>
			<div class="answer-info">
				<div class="order-step-buyer"><?= Html::encode($history->sender->getSellerName()) ?></div>
				<p><?= nl2br(Html::encode($history->content)) ?></p>

				<?php if ($history->attachments) { ?>
					<div class="order-attachment">
						<div class="attach-title"><?= Yii::t('account', 'Attachments') ?></div>
						<div class="message-attachments">
							<?php /* @var Attachment $attachment */ ?>
							<?php foreach ($history->attachments as $attachment) { ?>
								<div class="message-attachment">
									<div class="file-container">
                                        <?= Html::a(Html::img($attachment->getThumb()),
                                            Yii::$app->mediaLayer->tryLoadFromAws($attachment->content), [
                                                'download' => true,
                                                'data-pjax' => 0
                                        ]) ?>
									</div>
									<?= Html::a('<i class="fa fa-download" aria-hidden="true"></i>' . basename($attachment->content),
                                        Yii::$app->mediaLayer->tryLoadFromAws($attachment->content),
                                        [
											'download' => true,
											'data-pjax' => 0
										]) ?>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="order-step-date"><?= Yii::$app->formatter->asDate($history->created_at) ?></div>
	</div>
</div>