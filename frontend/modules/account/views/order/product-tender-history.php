<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.03.2017
 * Time: 16:17
 */

use common\components\CurrencyHelper;
use common\modules\store\assets\CheckoutAsset;
use frontend\assets\OrderDetailsAsset;
use yii\helpers\Html;
use common\modules\store\models\Order;

OrderDetailsAsset::register($this);
CheckoutAsset::register($this);

/* @var Order $order */
/* @var mixed $historyForm */

$this->title = $order->product ? $order->product->getLabel() : Yii::t('labels', 'Service not found');

?>

	<div class="green-status">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if ($order->status > Order::STATUS_PRODUCT_TENDER_UNAPPROVED) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Seller <br> is found') ?></div>
                        </div>
                    <?php } else { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-close"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Seller <br> is not found') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->status >= ORder::STATUS_PRODUCT_TENDER_APPROVED) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Price <br> approved') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->status >= Order::STATUS_PRODUCT_TENDER_PAID) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Request <br> paid') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->status > Order::STATUS_PRODUCT_TENDER_DELIVERED) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Product <br> delivered') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->status >= Order::STATUS_PRODUCT_TENDER_COMPLETED) { ?>
                        <div class="order-status">
                            <i class="fa fa-check-circle-o"></i>
                            <?= Yii::t('labels', 'Order Completed') ?>
                        </div>
                    <?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<p>&nbsp;</p>
			</div>

			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="order-history">
					<div class="order-details">
						<div class="order-descr-table">
							<div class="order-img-cell">
								<?= $order->product
									? Html::a(Html::img($order->product->getThumb()), $order->product->getUrl(), [
										'class' => 'order-img'
									])
									: '' ?>
							</div>
							<div class="order-info">
								<?= $order->product
									? Html::a(nl2br(Html::encode($order->product->getLabel())), $order->product->getUrl(), [
										'class' => 'order-link'
									])
									: '<span style="color: red">' . Yii::t('labels', 'Service not found') . '</span>'
								?>
								<div class="order-opts">
									<div class="order-opts-item">
										<span><?= Yii::t('model', 'Customer') ?>:</span>
										<?= Html::a($order->customer->getSellerName(),
											$order->customer->getSellerUrl()
										) ?>
									</div>
									<div class="order-opts-item">
										<span><?= Yii::t('model', 'Response') ?>:</span> #<?= $order->id ?>
									</div>
									<div class="order-opts-item">
										<?= Yii::$app->formatter->asDate($order->created_at) ?>
									</div>
								</div>
							</div>
							<div class="order-price text-right"></div>
						</div>
						<div class="order-job-table">
							<div class="order-job-row">
								<div class="order-job-cell">
									<?= Yii::t('model', 'Request') ?>
								</div>
								<div class="order-job-cell">
									<?= Yii::t('model', 'Quantity') ?>
								</div>
								<div class="order-job-cell text-right">
									<?= Yii::t('model', 'Total') ?>
								</div>
							</div>

							<div class="order-job-row">
								<div class="order-job-cell">
									<?= $order->product ? nl2br(Html::encode($order->product->getLabel())) : '<span style="color: red">' . Yii::t('app', 'Service not found') . '</span>' ?>
								</div>
								<div class="order-job-cell">
									<?= $order->orderProduct->quantity ?>
								</div>
								<div class="order-job-cell text-right">
									<?= $order->product->contract_price ?
										Yii::t('app', 'Contract price')
										: CurrencyHelper::convertAndFormat($order->product->currency_code, Yii::$app->params['app_currency_code'], $order->orderProduct->price * $order->orderProduct->quantity, true)
									?>
								</div>
							</div>
							<?php if (!empty($order->orderProduct->extra)) { ?>
								<?php foreach ($order->orderProduct->extra as $extra) { ?>
									<div class="order-job-row">
										<div class="order-job-cell" style="font-weight: 100">
											- <?= Html::encode($extra->title) ?>
										</div>
										<div class="order-job-cell">
											<?= $extra->quantity ?>
										</div>
										<div class="order-job-cell text-right">

										</div>
										<div class="order-job-cell text-right">
											<?= CurrencyHelper::convertAndFormat($extra->currency_code, Yii::$app->params['app_currency_code'], $extra->price * $extra->quantity, true) ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
							<div class="order-total text-right"><?= Yii::t('model', 'Total') ?> <?= $order->getTotal() ?>
							</div>
						</div>
					</div>
					<div class="order-steps">
						<?php foreach ($order->history as $history) { ?>
							<?php if ($history->statusesToViews[$history->type] !== null) { ?>
								<?= $this->render("history-items/" . $history->statusesToViews[$history->type], [
									'model' => $order,
									'history' => $history
								]) ?>
							<?php } ?>
						<?php } ?>
					</div>
					<?= $historyForm ?>
				</div>
			</div>

			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="order-issues text-center">
                    <p class="text-center"><?= Yii::t('order', 'Have troubles with order?') ?></p>

                    <a class="blue-but" href=""><?= Yii::t('order', 'Solve them now') ?></a>
				</div>
				<p>&nbsp;</p>

				<div class="cart-fair-play" style="border-top: 1px solid #ddd">
					<?= Html::a(Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
						['/page/static', 'alias' => 'fair-play'],
						[
							'data-pjax' => 0,
							'class' => 'image-container'
						]
					) ?>
					<div class="post-mes">
						<?= Html::a(Yii::t('app', 'We guarantee the execution of each order or we will refund your money back'),
							['/page/static', 'alias' => 'fair-play'],
							[
								'data-pjax' => 0,
								'class' => 'message-container'
							]
						) ?>
					</div>
				</div>
                <div class="no-fair-play text-center hidden-xs">
                    <div class="no-pair-play-header">
                        <span>!</span>
                        <?= Yii::t('order','Customers');?>
                    </div>
                    <div class="no-pair-play-text">
                        <?= Yii::t('order', 'Dear customers! If someone offer to you pay service outside uJobs, remember that in this case {secure} becomes unavailable, so the probability of losing money and time increases.', [
                            'secure' => Html::a(Yii::t('app', 'Fair play'), ['/page/static', 'alias' => 'fair-play'], ['data-pjax' => 0])
                        ]); ?>
                    </div>
                </div>
			</div>
		</div>
	</div>

<?php $script = <<<JS
    $('body,html').animate({
        scrollTop: $('.order-step').last().offset().top - 105
    }, 800);

    $(document).on('change', '#file-input', function(){
        var names = $.map($(this).prop('files'), function(val) { return val.name; });
        $(this).parent().siblings('mark').html('(' + names.length + ') ' + names.join(', '));
    });

    $(document).on('change', '.time-options input[type=radio]', function() {
		var target = $(this).data('class'),
			targetObject = $('.' + target),
			isAutoAcceptable = targetObject.find('form').data('accept');

        $('.decision_forms > div').addClass('hidden');
        if(isAutoAcceptable != 1) {
        	targetObject.removeClass('hidden');
        }
	});

	$(document).on('click', 'input[name=decision]', function() {
        var formIdentity = $(this).data('f'),
        form = $('form#' + formIdentity),
        accept = form.data('accept');

		if(accept === 1) {
			form.submit();
		}
	});

	$(document).on("beforeSubmit", 'form.decision', function () {
		$('.order-history form input[type=\'submit\']').attr('disabled', 'disabled');
        //var data = $(this).serialize();
        //data += '&s=true';
        //
        //$.ajax({
         //   type: 'POST',
         //   url: $(this).attr('action'),
         //   data: data,
         //   success: function (result) {
         //       if (result.success == true) {
         //          $.pjax.reload({container: '#history-pjax'});
         //       } else {
         //       	alertCall('top', 'error', 'error');
         //       }
         //   }
        //});
        //
	    //return false;
	});
JS;

$this->registerJs($script);