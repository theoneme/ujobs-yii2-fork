<?php
use common\models\user\Profile;
use yii\helpers\Html;

/* @var $profile Profile */
/* @var $accessInfo array */
if (isGuest()) {
    $offerModal = '#ModalSignup';
} else {
    if ($accessInfo['allowed'] === true) {
        $offerModal = '#ModalRequest';
    } else {
        $offerModal = '#tariffModal';
    }
}
?>

<div class='bf-item'>
    <div class='request-job text-center'>
        <?= Html::img($profile->getThumb(),['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
        <strong><?= Yii::t('account', 'Need something else?') ?></strong>
        <br>
        <span><?= Yii::t('account', 'You can order individual service') ?></span>
        <div class='button-block'>
            <?= Html::a(Yii::t('account', 'Request service'), null, [
                'class' => 'btn-middle modal-dismiss',
                'data-toggle' => 'modal',
                'data-target' => $offerModal
            ]) ?>
        </div>
    </div>
</div>
