<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\helpers\YandexHelper;
use common\models\Message;
use common\models\Offer;
use common\models\user\Profile;
use common\models\UserAttribute;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use frontend\modules\account\components\ProfileSidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $offersDataProvider ActiveDataProvider
 * @var $sellingJobsDataProvider ActiveDataProvider
 * @var $soldJobsDataProvider ActiveDataProvider
 * @var $tendersDataProvider ActiveDataProvider
 * @var $sellingProductsDataProvider ActiveDataProvider
 * @var $soldProductsDataProvider ActiveDataProvider
 * @var $ptendersDataProvider ActiveDataProvider
 * @var $reviewDataProvider ActiveDataProvider
 * @var $message Message
 * @var $offer Offer
 * @var $reviewsAverage integer
 * @var $reviewsCount integer
 * @var $totalSoldJobs integer
 * @var $totalSoldProducts integer
 * @var $contactAccessInfo array
 * @var UserAttribute $cityAttribute
 */

WorkerAsset::register($this);
SelectizeAsset::register($this);

$totalProducts = $sellingProductsDataProvider->totalCount + $soldProductsDataProvider->totalCount;
$totalJobs = $sellingJobsDataProvider->totalCount + $soldJobsDataProvider->totalCount;
?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($profile->gravatar_email) { ?>
                            <?= Html::img($profile->getThumb(), ['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($profile->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($profile->user->isOnline()) { ?>
                                <span class="status isonline"><i class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                            <?php } else { ?>
                                <span class="status isoffline"><i class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                            <?php } ?>
                        </div>
                        <div class="wmh-rate flex">
                            <?= $this->render('@frontend/widgets/views/rating', [
                                'rating' => $profile->rating / 2,
                                'size' => 14
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <?= Html::a(Yii::t('account', 'View As A Buyer'),
                        ['/account/profile/show-as-customer', 'id' => $profile->user_id],
                        ['class' => 'button green no-size']
                    ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= ProfileSidebar::widget([
                    'profile' => $profile,
                    'editable' => true,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'jobs' => $sellingJobsDataProvider->totalCount
                            ? ['link' => '#jobs', 'label' => Yii::t('app', 'Services') . " ({$sellingJobsDataProvider->totalCount})"]
                            : false,
                        'products' => $sellingProductsDataProvider->totalCount
                            ? ['link' => '#products', 'label' => Yii::t('app', 'Products') . " ({$sellingProductsDataProvider->totalCount})"]
                            : false,
                        'reviews' => $reviewDataProvider->totalCount
                            ? ['link' => '#reviews', 'label' => Yii::t('app', 'Reviews') . " ({$reviewDataProvider->totalCount})"]
                            : false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs">
                <div class="hidden-xs">
                    <div class="gigs-title no-padding prod-with-tabs" id="job-list-header">
                        <div class="flex flex-justify-between">
                            <div class="prod-tit">
                                <?= Yii::t('account', 'Your jobs') ?>
                            </div>
                            <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                            <ul class="nav nav-tabs product-tabs">
                                <li class="active">
                                    <?= Html::a(
                                        Yii::t('account', '{amount} on sale', ['amount' => $sellingJobsDataProvider->totalCount]),
                                        '#selling-jobs',
                                        ['data-toggle' => 'tab']
                                    )?>
                                </li>
                                <li>
                                    <?= Html::a(
                                        Yii::t('account', '{amount} sold', ['amount' => $totalSoldJobs]),
                                        '#sold-jobs',
                                        ['data-toggle' => 'tab']
                                    )?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php Pjax::begin(['enablePushState' => true, 'id' => 'jobs-pjax']); ?>
                        <div class="tab-content">
                            <div id="selling-jobs" class="tab-pane fade in active">
                                <?= ListView::widget([
                                    'dataProvider' => $sellingJobsDataProvider,
                                    'itemView' => 'listview/job',
                                    'viewParams' => ['editable' => true],
                                    'options' => [
                                        'class' => 'max-width gigs-list relative',
                                        'id' => 'pro-selling-jobs',
                                    ],
                                    'itemOptions' => [
                                        'class' => 'bf-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $sellingJobsDataProvider->pagination,
                                ]) ?>
                            </div>
                            <div id="sold-jobs" class="tab-pane fade">
                                <?= ListView::widget([
                                    'dataProvider' => $soldJobsDataProvider,
                                    'itemView' => 'listview/job',
                                    'viewParams' => ['editable' => true],
                                    'options' => [
                                        'class' => 'max-width gigs-list relative',
                                        'id' => 'pro-sold-jobs',
                                    ],
                                    'itemOptions' => [
                                        'class' => 'bf-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $soldJobsDataProvider->pagination,
                                ]) ?>
                            </div>
                        </div>
                    <?php Pjax::end(); ?>
                    <div class="gigs-title" id="tender-list-header">
                        <?= Yii::t('account', 'Your requests') ?>
                        <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                    </div>
                    <?php Pjax::begin(['enablePushState' => true, 'id' => 'tenders-pjax', 'scrollTo' => 1]); ?>
                    <?= ListView::widget([
                        'dataProvider' => $tendersDataProvider,
                        'itemView' => 'listview/job',
                        'viewParams' => ['editable' => true],
                        'options' => [
                            'class' => 'max-width gigs-list relative',
                            'id' => 'pro-tenders'
                        ],
                        'itemOptions' => [
                            'class' => 'bf-item'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Requests") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                    <?= LinkPager::widget([
                        'pagination' => $tendersDataProvider->pagination,
                    ]) ?>
                    <?php Pjax::end(); ?>

                    <div class="gigs-title no-padding prod-with-tabs" id="product-list-header">
                        <div class="flex flex-justify-between">
                            <div class="prod-tit">
                                <?= Yii::t('account', 'Your products') ?>
                            </div>
                            <?php if ($profile->is_store) { ?>
                                <?= Html::a(Yii::t('app', 'Page of this store'), ['/board/category/store', 'store_id' => $profile->user_id], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                            <?php } else { ?>
                                <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                            <?php } ?>
                            <ul class="nav nav-tabs product-tabs">
                                <li class="active">
                                    <?= Html::a(
                                        Yii::t('account', '{amount} on sale', ['amount' => $sellingProductsDataProvider->totalCount]),
                                        '#selling-products',
                                        ['data-toggle' => 'tab']
                                    )?>
                                </li>
                                <li>
                                    <?= Html::a(
                                        Yii::t('account', '{amount} sold', ['amount' => $totalSoldProducts]),
                                        '#sold-products',
                                        ['data-toggle' => 'tab']
                                    )?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php Pjax::begin(['enablePushState' => true, 'id' => 'products-pjax', 'scrollTo' => 1]); ?>
                        <div class="tab-content">
                            <div id="selling-products" class="tab-pane fade in active">
                                <?= ListView::widget([
                                    'dataProvider' => $sellingProductsDataProvider,
                                    'itemView' => 'listview/product',
                                    'viewParams' => ['editable' => true],
                                    'options' => [
                                        'class' => 'max-width gigs-list relative',
                                        'id' => 'pro-selling-products'
                                    ],
                                    'itemOptions' => [
                                        'class' => 'bf-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any Products on sale") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $sellingProductsDataProvider->pagination,
                                ]) ?>
                            </div>
                            <div id="sold-products" class="tab-pane fade">
                                <?= ListView::widget([
                                    'dataProvider' => $soldProductsDataProvider,
                                    'itemView' => 'listview/product',
                                    'viewParams' => ['editable' => true],
                                    'options' => [
                                        'class' => 'max-width gigs-list relative',
                                        'id' => 'pro-sold-products'
                                    ],
                                    'itemOptions' => [
                                        'class' => 'bf-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any sold Products") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $soldProductsDataProvider->pagination,
                                ]) ?>
                            </div>
                        </div>
                    <?php Pjax::end(); ?>

                    <div class="gigs-title" id="ptender-list-header">
                        <?= Yii::t('account', 'Your product requests') ?>
                        <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                    </div>
                    <?php Pjax::begin(['enablePushState' => true, 'id' => 'ptenders-pjax', 'scrollTo' => 1]); ?>
                    <?= ListView::widget([
                        'dataProvider' => $ptendersDataProvider,
                        'itemView' => 'listview/product',
                        'viewParams' => ['editable' => true],
                        'options' => [
                            'class' => 'max-width gigs-list relative',
                            'id' => 'pro-product-tenders'
                        ],
                        'itemOptions' => [
                            'class' => 'bf-item'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any active Products") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                    <?= LinkPager::widget([
                        'pagination' => $ptendersDataProvider->pagination,
                    ]) ?>
                    <?php Pjax::end(); ?>

                    <?php if ($offersDataProvider->totalCount) { ?>
                        <div class="gigs-title" id="offer-list-header"><?= Yii::t('account', 'Available individual jobs') ?></div>
                        <div class="relative">
                            <?= ListView::widget([
                                'dataProvider' => $offersDataProvider,
                                'itemView' => 'listview/offer',
                                'viewParams' => ['editable' => true],
                                'options' => [
                                    'class' => 'max-width gigs-list relative',
                                    'id' => 'pro-offers'
                                ],
                                'itemOptions' => [
                                    'class' => 'bf-item'
                                ],
                                'emptyText' => '',
                                'layout' => "{items}"
                            ]) ?>
                        </div>
                    <?php } ?>

                    <?php if ($reviewDataProvider->getTotalCount()) { ?>
                        <div class="gigs-title" id="review-list-header">
                            <div class="flex">
                                <div>
                                    <?= Yii::t('account', 'Reviews') ?>&nbsp;&nbsp;
                                </div>
                                <div>
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $reviewsAverage / 2,
                                        'size' => 14
                                    ]) ?>
                                </div>
                                <div>
                                    &nbsp;<?= round($reviewsAverage / 2, 1, PHP_ROUND_HALF_UP) ?>
                                </div>
                            </div>
                        </div>
                        <div class="body-rate">
                            <?= ListView::widget([
                                'dataProvider' => $reviewDataProvider,
                                'itemView' => 'listview/review-item',
                                'options' => [
                                    'class' => ''
                                ],
                                'itemOptions' => [
                                    'class' => 'comment'
                                ],
                                'layout' => "{items}"
                            ]) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <?php if ($totalJobs) { ?>
                        <div id="jobs" class="gigs-title no-padding prod-with-tabs">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit">
                                    <?= Yii::t('account', 'Your jobs') ?>
                                </div>
                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingJobsDataProvider->totalCount]),
                                            '#selling-jobs-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldJobs]),
                                            '#sold-jobs-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'jobs-pjax', 'scrollTo' => 1]); ?>
                            <div class="tab-content">
                                <div id="selling-jobs-mob" class="tab-pane fade in active">
                                    <?= ListView::widget([
                                        'dataProvider' => $sellingJobsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                                        'options' => [
                                            'class' => 'max-width gigs-list relative',
                                            'id' => 'pro-selling-jobs-mobile',
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $sellingJobsDataProvider->pagination,
                                    ]) ?>
                                </div>
                                <div id="sold-jobs-mob" class="tab-pane fade">
                                    <?= ListView::widget([
                                        'dataProvider' => $soldJobsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                                        'options' => [
                                            'class' => 'max-width gigs-list relative',
                                            'id' => 'pro-sold-jobs-mobile',
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $soldJobsDataProvider->pagination,
                                    ]) ?>
                                </div>
                            </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>

                    <?php if ($tendersDataProvider->getTotalCount()) { ?>
                        <div id="tenders" class="gigs-title"><?= Yii::t('account', 'Your requests') ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $tendersDataProvider,
                            'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                            'options' => [
                                'class' => 'gigs-list relative',
                                'id' => 'pro-tenders-mobile'
                            ],
                            'itemOptions' => [
                                'class' => 'mob-work-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Requests") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    <?php } ?>

                    <?php if ($totalProducts) { ?>
                        <div id="products" class="gigs-title no-padding prod-with-tabs">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit"><?= Yii::t('account', 'Products from user {user}', ['user' => $profile->getSellerName()]) ?></div>

                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingProductsDataProvider->totalCount]),
                                            '#selling-products-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldProducts]),
                                            '#sold-products-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'products-pjax', 'scrollTo' => 1]); ?>
                        <div class="tab-content">
                            <div id="selling-products-mob" class="tab-pane fade in active">
                                <?= ListView::widget([
                                    'dataProvider' => $sellingProductsDataProvider,
                                    'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                                    'options' => [
                                        'class' => 'gigs-list relative',
                                        'id' => 'pro-selling-products-mob'
                                    ],
                                    'itemOptions' => [
                                        'class' => 'mob-work-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any Products on sale") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $sellingProductsDataProvider->pagination,
                                ]) ?>
                            </div>
                            <div id="sold-products-mob" class="tab-pane fade">
                                <?= ListView::widget([
                                    'dataProvider' => $soldProductsDataProvider,
                                    'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                                    'options' => [
                                        'class' => 'gigs-list relative',
                                        'id' => 'pro-sold-products-mob'
                                    ],
                                    'itemOptions' => [
                                        'class' => 'mob-work-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any sold Products") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $soldProductsDataProvider->pagination,
                                ]) ?>
                            </div>
                        </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>
                    <?php if ($ptendersDataProvider->totalCount) { ?>
                        <div id="ptenders" class="gigs-title"><?= Yii::t('account', 'Your product requests') ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $ptendersDataProvider,
                            'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                            'options' => [
                                'class' => 'gigs-list relative',
                                'id' => 'pro-product-tenders-mobile'
                            ],
                            'itemOptions' => [
                                'class' => 'mob-work-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any active Products") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    <?php } ?>

                    <?php if ($reviewDataProvider->getTotalCount()) { ?>
                        <div id="reviews">
                            <div class="gigs-title" id="review-list-header">
                                <div class="flex">
                                    <div>
                                        <?= Yii::t('account', 'Reviews') ?>&nbsp;&nbsp;
                                    </div>
                                    <div>
                                        <?= $this->render('@frontend/widgets/views/rating', [
                                            'rating' => $reviewsAverage / 2,
                                            'size' => 14
                                        ]) ?>
                                    </div>
                                    <div>
                                        &nbsp;<?= round($reviewsAverage / 2, 1, PHP_ROUND_HALF_UP) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="body-rate">
                                <?= ListView::widget([
                                    'dataProvider' => $reviewDataProvider,
                                    'itemView' => 'listview/review-item',
                                    'options' => [
                                        'class' => ''
                                    ],
                                    'itemOptions' => [
                                        'class' => 'comment'
                                    ],
                                    'layout' => "{items}"
                                ]) ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger"
                            id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn"
                            id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

<?php $cartUrl = Url::to(['/store/cart/add-offer']);
$cartText = Yii::t('account', 'Individual order added to cart');
$script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    
    menuMobileItems.css('width', perc);
    
    $("#products-pjax, #jobs-pjax, #ptenders-pjax, #tenders-pjax").on('pjax:complete', function(e) {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 200
        }, 500);
    });
    
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x" // horizontal scrollbar
    });
    
    $(document).on('click', '.menu-worker-mobile a', function() {
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        return false;
    });

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });
    $(document).on('click', '.add-to-cart', function() {
        let id = $(this).attr('data-id');
        let self = $(this);
        $.ajax({
            url: '$cartUrl',
            type: 'post',
            data: {
                id: id
            },
            success: function(response) {
                $('#cart-head-container').replaceWith(response);
                alertCall('top', 'success', '$cartText');
            },
        });
        return false;
    });

    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script);