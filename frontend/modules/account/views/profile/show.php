<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\models\Message;
use common\models\Offer;
use common\models\user\Profile;
use frontend\assets\WorkerAsset;
use frontend\modules\account\components\ProfileSidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $sellingJobsDataProvider ActiveDataProvider
 * @var $soldJobsDataProvider ActiveDataProvider
 * @var $tendersDataProvider ActiveDataProvider
 * @var $sellingProductsDataProvider ActiveDataProvider
 * @var $soldProductsDataProvider ActiveDataProvider
 * @var $ptendersDataProvider ActiveDataProvider
 * @var $languageDataProvider ActiveDataProvider
 * @var $skillDataProvider ActiveDataProvider
 * @var $specialtyDataProvider ActiveDataProvider
 * @var $certificateDataProvider ActiveDataProvider
 * @var $educationDataProvider ActiveDataProvider
 * @var $portfolioDataProvider ActiveDataProvider
 * @var $reviewDataProvider ActiveDataProvider
 * @var $articlesDataProvider ActiveDataProvider
 * @var $message Message
 * @var $offer Offer
 * @var $reviewsAverage integer
 * @var $reviewsCount integer
 * @var $totalSoldJobs integer
 * @var $totalSoldProducts integer
 * @var $contactAccessInfo array
 */

WorkerAsset::register($this);

$isOwner = hasAccess($profile->user_id);
$totalProducts = $sellingProductsDataProvider->totalCount + $soldProductsDataProvider->totalCount;
$totalJobs = $sellingJobsDataProvider->totalCount + $soldJobsDataProvider->totalCount;
?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($profile->gravatar_email) { ?>
                            <?= Html::img($profile->getThumb(), ['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($profile->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($profile->user->isOnline()) { ?>
                                <span class="status isonline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                            <?php } else { ?>
                                <span class="status isoffline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                            <?php } ?>
                        </div>
                        <div class="wmh-rate flex">
                            <?= $this->render('@frontend/widgets/views/rating', [
                                'rating' => $profile->rating / 2,
                                'size' => 14
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <?php if (isGuest()) {
                        echo Html::a(Yii::t('account', 'Contact a freelancer'), null, [
                            'class' => 'button green no-size',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalLogin'
                        ]);
                    } else {
                        if (hasAccess($profile->user_id)) {
                            echo Html::a(Yii::t('account', 'View As A Owner'),
                                ['/account/profile/show', 'id' => $profile->user_id],
                                ['class' => 'button green no-size']
                            );
                        } else {
                            echo Html::a(Yii::t('account', 'Contact a freelancer'),
                                ['/account/inbox/start-conversation', 'user_id' => $profile->user_id],
                                ['class' => 'button green no-size']
                            );
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= ProfileSidebar::widget([
                    'profile' => $profile,
                    'editable' => false,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'jobs' => $sellingJobsDataProvider->totalCount
                            ? ['link' => '#jobs', 'label' => Yii::t('app', 'Services') . " ({$sellingJobsDataProvider->totalCount})"]
                            : false,
                        'products' => $sellingProductsDataProvider->totalCount
                            ? ['link' => '#products', 'label' => Yii::t('app', 'Products') . " ({$sellingProductsDataProvider->totalCount})"]
                            : false,
                        'reviews' => $reviewDataProvider->totalCount
                            ? ['link' => '#reviews', 'label' => Yii::t('app', 'Reviews') . " ({$reviewDataProvider->totalCount})"]
                            : false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs">
                <div class="hidden-xs">
                    <?php
                    if ($profile->is_store ? $totalJobs : $totalJobs || count($profile->skills) || $profile->is_freelancer) { ?>
                        <div class="gigs-title no-padding prod-with-tabs" id="job-list-header">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit">
                                    <?= Yii::t('account', 'Job offers from user {user}', ['user' => $profile->getSellerName()]) ?>
                                </div>
                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingJobsDataProvider->totalCount]),
                                            '#selling-jobs',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldJobs]),
                                            '#sold-jobs',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'jobs-pjax']); ?>
                            <div class="tab-content">
                                <div id="selling-jobs" class="tab-pane fade in active">
                                    <?= ListView::widget([
                                        'dataProvider' => $sellingJobsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/job-list',
                                        'options' => [
                                            'class' => 'max-width gigs-list relative',
                                            'id' => 'pro-selling-jobs',
                                        ],
                                        'itemOptions' => [
                                            'class' => 'bf-item'
                                        ],
                                        'emptyText' =>
                                            $profile->status === Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>'
                                        ,
                                        'layout' => '{items}' .
                                            ($profile->status === Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : ''
                                            )
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $sellingJobsDataProvider->pagination,
                                    ]) ?>
                                </div>
                                <div id="sold-jobs" class="tab-pane fade">
                                    <?= ListView::widget([
                                        'dataProvider' => $soldJobsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/job-list',
                                        'options' => [
                                            'class' => 'max-width gigs-list relative',
                                            'id' => 'pro-sold-jobs',
                                        ],
                                        'itemOptions' => [
                                            'class' => 'bf-item'
                                        ],
                                        'emptyText' =>
                                            $profile->status === Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>'
                                        ,
                                        'layout' => '{items}' .
                                            ($profile->status === Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : ''
                                            )
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $soldJobsDataProvider->pagination,
                                    ]) ?>
                                </div>
                            </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>

                    <?php if ($tendersDataProvider->totalCount) { ?>
                        <div class="gigs-title" id="tender-list-header">
                            <?= Yii::t('account', 'Requests from user {user}', ['user' => $profile->getSellerName()]) ?>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'tenders-pjax']); ?>
                        <?= ListView::widget([
                            'dataProvider' => $tendersDataProvider,
                            'itemView' => '@frontend/modules/filter/views/job-list',
                            'options' => [
                                'class' => 'gigs-list relative',
                                'id' => 'pro-tenders'
                            ],
                            'itemOptions' => [
                                'class' => 'bf-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Requests") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $tendersDataProvider->pagination,
                        ]) ?>
                        <?php Pjax::end(); ?>
                    <?php } ?>

                    <?php if ($totalProducts) { ?>
                        <div class="gigs-title no-padding prod-with-tabs" id="product-list-header">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit">
                                    <?= Yii::t('account', 'Products from user {user}', ['user' => $profile->getSellerName()]) ?>
                                </div>
                                <?php if ($profile->is_store) { ?>
                                    <?= Html::a(Yii::t('app', 'Page of this store'), ['/board/category/store', 'store_id' => $profile->user_id], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                                <?php } ?>
                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingProductsDataProvider->totalCount]),
                                            '#selling-products',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldProducts]),
                                            '#sold-products',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'products-pjax']); ?>
                            <div class="tab-content">
                                <div id="selling-products" class="tab-pane fade in active">
                                    <?= ListView::widget([
                                        'dataProvider' => $sellingProductsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/product-list',
                                        'options' => [
                                            'class' => 'gigs-list relative',
                                            'id' => 'pro-selling-products'
                                        ],
                                        'itemOptions' => [
                                            'class' => 'bf-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any Products on sale") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $sellingProductsDataProvider->pagination,
                                    ]) ?>
                                </div>
                                <div id="sold-products" class="tab-pane fade">
                                    <?= ListView::widget([
                                        'dataProvider' => $soldProductsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/product-list',
                                        'options' => [
                                            'class' => 'gigs-list relative',
                                            'id' => 'pro-sold-products'
                                        ],
                                        'itemOptions' => [
                                            'class' => 'bf-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any sold Products") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $soldProductsDataProvider->pagination,
                                    ]) ?>
                                </div>
                            </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>

                    <?php if ($ptendersDataProvider->totalCount) { ?>
                        <div class="gigs-title" id="ptender-list-header">
                            <?= Yii::t('account', 'Product requests from user {user}', ['user' => $profile->getSellerName()]) ?>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'ptenders-pjax']); ?>
                        <?= ListView::widget([
                            'dataProvider' => $ptendersDataProvider,
                            'itemView' => '@frontend/modules/filter/views/product-list',
                            'options' => [
                                'class' => 'max-width gigs-list relative',
                                'id' => 'pro-product-tenders'
                            ],
                            'itemOptions' => [
                                'class' => 'bf-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any active Products") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $ptendersDataProvider->pagination,
                        ]) ?>
                        <?php Pjax::end(); ?>
                    <?php } ?>

                    <?php if ($reviewDataProvider->getTotalCount()) { ?>
                        <div class="gigs-title" id="review-list-header">
                            <div class="flex">
                                <div>
                                    <?= Yii::t('account', 'Reviews') ?>&nbsp;&nbsp;
                                </div>
                                <div>
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $reviewsAverage / 2,
                                        'size' => 14
                                    ]) ?>
                                </div>
                                <div>
                                    &nbsp;<?= round($reviewsAverage / 2, 1, PHP_ROUND_HALF_UP) ?>
                                </div>
                            </div>
                        </div>
                        <div class="body-rate">
                            <?= ListView::widget([
                                'dataProvider' => $reviewDataProvider,
                                'itemView' => 'listview/review-item',
                                'options' => [
                                    'class' => ''
                                ],
                                'itemOptions' => [
                                    'class' => 'comment'
                                ],
                                'layout' => "{items}"
                            ]) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <?php if ($totalJobs) { ?>
                        <div id="jobs" class="gigs-title no-padding prod-with-tabs">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit">
                                    <?= Yii::t('account', 'Job offers from user {user}', ['user' => $profile->getSellerName()]) ?>
                                </div>
                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingJobsDataProvider->totalCount]),
                                            '#selling-jobs-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldJobs]),
                                            '#sold-jobs-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'jobs-pjax', 'scrollTo' => 1]); ?>
                            <div class="tab-content">
                                <div id="selling-jobs-mob" class="tab-pane fade in active">
                                    <?= ListView::widget([
                                        'dataProvider' => $sellingJobsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                                        'options' => [
                                            'class' => 'max-width gigs-list relative',
                                            'id' => 'pro-selling-jobs-mobile',
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' =>
                                            $profile->status == Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>'
                                        ,
                                        'layout' => "{items}" .
                                            ($profile->status == Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : ""
                                            )
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $sellingJobsDataProvider->pagination,
                                    ]) ?>
                                </div>
                                <div id="sold-jobs-mob" class="tab-pane fade">
                                    <?= ListView::widget([
                                        'dataProvider' => $soldJobsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                                        'options' => [
                                            'class' => 'max-width gigs-list relative',
                                            'id' => 'pro-sold-jobs-mobile',
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' =>
                                            $profile->status == Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>'
                                        ,
                                        'layout' => "{items}" .
                                            ($profile->status == Profile::STATUS_ACTIVE && !$isOwner
                                                ? $this->render('_offer_block', ['profile' => $profile, 'accessInfo' => $contactAccessInfo])
                                                : ""
                                            )
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $soldJobsDataProvider->pagination,
                                    ]) ?>
                                </div>
                            </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>

                    <?php if ($tendersDataProvider->totalCount) { ?>
                        <div id="services" class="gigs-title"><?= Yii::t('account', 'Requests from user {user}', ['user' => $profile->getSellerName()]) ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $tendersDataProvider,
                            'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                            'options' => [
                                'class' => 'gigs-list relative',
                                'id' => 'pro-tenders'
                            ],
                            'itemOptions' => [
                                'class' => 'mob-work-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Requests") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    <?php } ?>

                    <?php if ($totalProducts) { ?>
                        <div id="products" class="gigs-title no-padding prod-with-tabs">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit"><?= Yii::t('account', 'Products from user {user}', ['user' => $profile->getSellerName()]) ?></div>

                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingProductsDataProvider->totalCount]),
                                            '#selling-products-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldProducts]),
                                            '#sold-products-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'products-pjax', 'scrollTo' => 1]); ?>
                            <div class="tab-content">
                                <div id="selling-products-mob" class="tab-pane fade in active">
                                    <?= ListView::widget([
                                        'dataProvider' => $sellingProductsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                                        'options' => [
                                            'class' => 'gigs-list relative',
                                            'id' => 'pro-selling-products-mob'
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any Products on sale") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $sellingProductsDataProvider->pagination,
                                    ]) ?>
                                </div>
                                <div id="sold-products-mob" class="tab-pane fade">
                                    <?= ListView::widget([
                                        'dataProvider' => $soldProductsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                                        'options' => [
                                            'class' => 'gigs-list relative',
                                            'id' => 'pro-sold-products-mob'
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any sold Products") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $soldProductsDataProvider->pagination,
                                    ]) ?>
                                </div>
                            </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>
                    <?php if ($ptendersDataProvider->totalCount) { ?>
                        <div class="gigs-title"><?= Yii::t('account', 'Product requests from user {user}', ['user' => $profile->getSellerName()]) ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $ptendersDataProvider,
                            'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                            'options' => [
                                'class' => 'gigs-list relative',
                                'id' => 'pro-product-tenders'
                            ],
                            'itemOptions' => [
                                'class' => 'mob-work-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any active Products") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    <?php } ?>

                    <?php if ($reviewDataProvider->getTotalCount()) { ?>
                        <div id="reviews">
                            <div class="gigs-title" id="review-list-header">
                                <div class="flex">
                                    <div>
                                        <?= Yii::t('account', 'Reviews') ?>&nbsp;&nbsp;
                                    </div>
                                    <div>
                                        <?= $this->render('@frontend/widgets/views/rating', [
                                            'rating' => $reviewsAverage / 2,
                                            'size' => 14
                                        ]) ?>
                                    </div>
                                    <div>
                                        &nbsp;<?= round($reviewsAverage / 2, 1, PHP_ROUND_HALF_UP) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="body-rate">
                                <?= ListView::widget([
                                    'dataProvider' => $reviewDataProvider,
                                    'itemView' => 'listview/review-item',
                                    'options' => [
                                        'class' => ''
                                    ],
                                    'itemOptions' => [
                                        'class' => 'comment'
                                    ],
                                    'layout' => "{items}"
                                ]) ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalRequest" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('account', 'Individual job') ?></div>
                </div>
                <div class="modal-body">
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'request-modal-pjax', 'scrollTo' => false]); ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'request-form',
                        'action' => ['/account/inbox-ajax/request', 'user_id' => $profile->user->id],
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'class' => 'row send-message',
                        ]
                    ]); ?>
                    <div>
                        <div class="chover">
                            <?= Html::radio('Offer[type]', true, ['class' => 'radio-checkbox', 'value' => Offer::TYPE_REQUEST, 'id' => 'request_type_1']) ?>
                            <label for="request_type_1"><?= Yii::t('app', 'I am customer and I want to request individual job.') ?></label>
                        </div>
                        <div class="chover">
                            <?= Html::radio('Offer[type]', false, ['class' => 'radio-checkbox', 'value' => Offer::TYPE_OFFER, 'id' => 'request_type_2']) ?>
                            <label for="request_type_2"><?= Yii::t('app', 'I am worker and I want to offer individual job.') ?></label>
                        </div>
                    </div>
                    <?= $form->field($offer, 'subject', [
                        'template' => "{input}\n{error}",
                    ])->textInput([
                        'id' => 'offer-request-subject',
                        'placeholder' => Yii::t('account', 'Set request subject')
                    ]) ?>
                    <div class="white-mes-bg">
                        <?= $form->field($message, 'message', ['template' => "{input}\n{error}"])->textarea([
                            'class' => 'readsym',
                            'placeholder' => Yii::t('account', 'Type your message here...')
                        ]) ?>
                        <div class="white-mes-bottom text-left clearfix">
                            <div class="counttext">
                                <span class="ctspan">0</span> / 1200
                            </div>
                            <div class="fileup">
                                <label class="file-upload">
                                        <span class="button text-center hint--top-right"
                                              data-hint="<?= Yii::t('account', 'Attach files. {mb}MB max.', ['mb' => 30]) ?>">
                                            <i class="fa fa-paperclip" aria-hidden="true"></i>
                                        </span>
                                    <?= $form->field($message, 'uploadedFiles[]')->fileInput([
                                        'id' => 'file-input',
                                        'multiple' => true
                                    ])->label(false) ?>
                                    <mark style="background-color: #fff"></mark>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <?= $form->field($offer, 'price', [
                        'template' => "{input}\n{error}",
                        'options' => ['class' => 'form-group offer-price']
                    ])->textInput([
                        'placeholder' => Yii::t('account', 'Budget')
                    ]) ?>
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
                    <?php ActiveForm::end(); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
            <div class="spinner-bg">
                <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            </div>
        </div>
    </div>

<?php if ($contactAccessInfo['allowed'] == false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>

<?php
$requestSuccessText = Yii::t('account', 'Request successfully sent');
$script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100/menuMobileItems.length+'%';
    let anchor = window.location.hash;
    
    menuMobileItems.css('width',perc);

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if($(window).width()>992){
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display','block');
            } else {
                $('.worker-mobile-head').css('display','none');
            }
        }
    });

    $("#products-pjax, #jobs-pjax, #ptenders-pjax, #tenders-pjax").on('pjax:complete', function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 200
        }, 500);
	});
    $(".menu-worker-mobile").mCustomScrollbar({
        axis:"x" 
    });
    $(document).on('click','.menu-worker-mobile a',function(){
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        
        return false;
    });
    $(document).on('beforeSubmit', '#request-form', function () {
        let self = $(this);
        let formData = new FormData($(this)[0]);
        let spinner = $('#ModalRequest').find('.spinner-bg');
        spinner.show();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            
            success: function (result) {
                if (result.success) {
                    alertCall('top', 'success', '$requestSuccessText');
                    self.closest('.modal').modal('hide');
                    $.pjax.reload({container: '#request-modal-pjax'});
                }
                spinner.hide();
            }
        });
        return false;
    });

    $(document).on('change', '#file-input', function(){
        let names = $.map($(this).prop('files'), function(val) { return val.name; });
        $(this).parent().siblings('mark').html('(' + names.length + ') ' + names.join(', '));
    });
    
    
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script);