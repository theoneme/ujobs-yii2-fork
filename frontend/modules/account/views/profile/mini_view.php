<?php

use common\models\Subscription;
use common\models\user\Profile;
use common\models\user\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var User $user
 * @var array $accessInfo
 * @var View $this
 * @var array $skills
 * @var array $languages
 */
$tariffIcon = '';
if (!empty($user->activeTariff)) {
    $pathInfo = pathinfo($user->activeTariff->tariff->icon);
    $tariffIcon = Html::img($pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . '-profile.' . $pathInfo['extension'], ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]);
}
?>
<div class="short-author">
    <div class="status-site">
        <?php if ($user->isOnline()) { ?>
            <span class="status isonline"><i class="fa fa-circle"></i><?= Yii::t('account', 'Online') ?></span>
        <?php } else { ?>
            <span class="status isoffline"><i class="fa fa-circle"></i><?= Yii::t('account', 'Offline') ?></span>
        <?php } ?>
    </div>
    <div class="author-top text-center">
        <div class="author-photo">
            <?= Html::a(
                Html::img($user->getThumb('catalog'), ['alt' => $user->getSellerName(), 'title' => $user->getSellerName()]) . $tariffIcon,
                ['/account/profile/show', 'id' => $user->id]
            ) ?>
        </div>
        <?= Html::a(Html::encode($user->getSellerName()), ['/account/profile/show', 'id' => $user->id], ['class' => 'author-name']) ?>
        <!--<div class="author-medal">Top Rated Seller</div>-->
    </div>
    <div class="author-about">
        <div class="wc-item wc-rate">
            <?= Yii::t('account', 'Rating') ?>
            <span class="text-right">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $user->profile->rating / 2,
                    'size' => 14
                ]) ?>
            </span>
        </div>

        <?php if (count($skills)) { ?>
            <div class="wc-item skills clearfix">
                <?= Yii::t('account', 'Skills') ?>
                <span class="text-right"><?= implode(',<br>',$skills) ?></span>
            </div>
        <?php } ?>

        <?php if (count($user->profile->educationsArray)) { ?>
            <div class="wc-item education clearfix">
                <?= Yii::t('account', 'Education') ?>
                <span class="text-right"><?= implode(',<br> ', ArrayHelper::getColumn($user->profile->educationsArray, 'title')) ?></span>
            </div>
        <?php } ?>

        <?php if (count($user->profile->certificatesArray)) { ?>
            <div class="wc-item certificates clearfix">
                <?= Yii::t('account', 'Certificates') ?>
                <span class="text-right"><?= implode(',<br> ', ArrayHelper::getColumn($user->profile->certificatesArray, 'title')) ?></span>
            </div>
        <?php } ?>

        <?php if (count($languages)) { ?>
            <div class="wc-item languages clearfix">
                <?= Yii::t('account', 'Languages') ?>
                <span class="text-right"><?= implode(',<br> ', $languages) ?></span>
            </div>
        <?php } ?>
        <?php if ($soldCount = $user->profile->getSoldServicesCount()) { ?>
            <div class="wc-item basket">
                <?= Yii::t('account', 'Sold through service') ?>
                <span class="text-right"><?= $soldCount ?></span>
            </div>
        <?php } ?>
        <?php if ($user->profile->getFrom()) { ?>
            <div class="wc-item location">
                <?= Yii::t('account', 'From') ?>
                <span class="text-right"><?= $user->profile->getFrom() ?></span>
            </div>
        <?php } ?>
        <div class="wc-item response-time">
            <?= Yii::t('account', 'Avg. Response Time') ?> <span class="text-right"> <?= Yii::t('account', '{count, plural, one{# hour} other{# hours}}', ['count' => 1])?> </span>
        </div>
    </div>
    <div class="author-intro">
        <p class="short-descr-desktop">
            <?= nl2br(Html::encode($user->profile->getDescription())) ?>
        </p>
        <?php if (nl2br(Html::encode($user->profile->getDescription()))) { ?>
            <a class="mob-show-info" data-expanded="0" href="#"><?= Yii::t('app', 'See more') ?></a>
        <?php } ?>
    </div>
    <!--<a class="other-products">
        <?= Yii::t('app', 'Other offers from the seller'); ?>
    </a>-->
    <div class="author-contact text-center">
        <?php if (isGuest()) {
            echo Html::a(Yii::t('account', 'Contact Me'), null, [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-target' => '#ModalLogin'
            ]);
        } else {
            if ($accessInfo['allowed'] === true) { ?>
                <?= Html::a(Yii::t('account', 'Contact Me'), ['/account/inbox/start-conversation', 'user_id' => $user->id], [
                    'class' => 'btn-big'
                ]); ?>
            <?php } else { ?>
                <?= Html::a(Yii::t('account', 'Contact Me'), null, [
                    'class' => 'btn-big',
                    'data-toggle' => 'modal',
                    'data-target' => '#tariffContactModal'
                ]); ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>
