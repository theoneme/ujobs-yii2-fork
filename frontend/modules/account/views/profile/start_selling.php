<?php

use common\models\Job;
use common\models\Page;
use common\models\user\Profile;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

\frontend\assets\WorkerAsset::register($this);

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $page Page
 * @var $cheapest Job
 * @var $mostExpensive Job
 * @var $profilesDataProvider ActiveDataProvider
 * @var $buyersDataProvider ActiveDataProvider
 */

$this->title = Yii::t('account', 'Start Selling');
?>

<div class="video-block">
    <div class="video hidden-xs">
        <video src="/images/new/cover_video2.mp4" poster="/images/new/poster.jpg" autoplay="" loop="" muted=""
               preload="auto" style="opacity: 0.824561;">
        </video>
    </div>
    <div class="video-title text-center">
        <h1 class="cat-title text-center"><?= Yii::t('account', 'Work Your Way') ?></h1>
        <div class="video-descr"><?= Yii::t('account', 'You bring the skill. We\'ll make earning easy.') ?></div>
        <?php if (Yii::$app->user->isGuest) {
            echo Html::a(Yii::t('account', 'Become a Seller'), null, [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-target' => '#ModalSignup'
            ]);
        } else {
            echo Html::a(Yii::t('account', 'Become a Seller'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id], [
                'class' => 'btn-big'
            ]);
        } ?>
    </div>
    <div class="cover-img visible-xs">
        <img src="/images/new/cover-mobile.jpg" alt="ujobs-people">
    </div>
</div>
<div class="video-bottom row">
    <div class="container-fluid">
        <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
            <?= Yii::t('account', 'Order every') ?><br/>
            <span><?= Yii::t('account', '{0} sec', 5) ?></span>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
            <?= Yii::t('account', 'Projects Completed') ?><br/>
            <span>200 000</span>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
            <?= Yii::t('account', 'Price Range') ?><br/>
            <span>
                <?= $cheapest->getPrice() . " - " . $mostExpensive->getPrice() ?>
            </span>
        </div>
    </div>
</div>

<div class="bs-block white-block hidden-xs">
    <h2 class="text-center"><?= Yii::t('account', 'JOIN OUR GROWING FREELANCE COMMUNITY') ?></h2>
    <?php if (!isPagespeed()) { ?>
        <?= ListView::widget([
            'dataProvider' => $profilesDataProvider,
            'itemView' => 'listview/ss_profile',
            'options' => [
                'class' => 'container-fluid'
            ],
            'itemOptions' => [
                'class' => ''
            ],
            'viewParams' => [
                'count' => $profilesDataProvider->getTotalCount(),
                'keywords' => $page->translation->seo_keywords
            ],
            'layout' => "{items}"
        ]) ?>
    <?php } ?>
</div>

<div class="mobile-people-over visible-xs">
    <?php if (!isPagespeed()) { ?>
        <?= ListView::widget([
            'dataProvider' => $profilesDataProvider,
            'itemView' => 'listview/ss_profile_mobile',
            'options' => [
                'class' => '',
                'id' => 'mobile-people'
            ],
            'itemOptions' => [
                'class' => 'top-mobile-seller'
            ],
            'viewParams' => [
                'keywords' => $page->translation->seo_keywords
            ],
            'layout' => "{items}"
        ]) ?>
    <?php } ?>
</div>

<div class="ss-grey-block">
    <div class="container-fluid">
        <h2 class="text-center"><?= Yii::t('account', 'HOW IT WORKS') ?></h2>
        <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
            <div class="how-work-title text-center">
                1. <?= Yii::t('account', 'Publish job') ?>
            </div>
            <p class="text-center">
                <?= Yii::t('account', 'Sign up for free, set up your profile and offer your services to our global audience.') ?>
            </p>
        </div>
        <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
            <div class="how-work-title text-center">
                2. <?= Yii::t('account', 'Deliver Great Work') ?>
            </div>
            <p class="text-center">
                <?= Yii::t('account', 'Get notified when you get an order and use our system to discuss details with customers.') ?>
            </p>
        </div>
        <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
            <div class="how-work-title text-center">
                3. <?= Yii::t('account', 'Get Paid') ?>
            </div>
            <p class="text-center">
                <?= Yii::t('account', 'You will receive payment on time, we guarantee it. The reward will be credited to your balance after completing the order.') ?>
            </p>
        </div>
    </div>
</div>

<div class="white-block">
    <div class="container-fluid">
        <h2 class="text-center"><?= Yii::t('account', 'HISTORY OF OUR PROFESSIONALS') ?></h2>
        <?php if (!isPagespeed()) { ?>
            <?= ListView::widget([
                'dataProvider' => $buyersDataProvider,
                'itemView' => 'listview/ss_buyer',
                'options' => [
                    'class' => 'row'
                ],
                'itemOptions' => [
                    'class' => 'col-md-6 col-sm-6 col-xs-12 buyer-stories'
                ],
                'viewParams' => [
                    'keywords' => $page->translation->seo_keywords
                ],
                'layout' => "{items}"
            ]) ?>
        <?php } ?>
    </div>
</div>

<div class="ss-grey-block">
    <div class="container-fluid">
        <h2 class="text-center"><?= Yii::t('account', 'Q&A') ?></h2>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 qa">
                <div class="faq-item">
                    <div class="faq-head">
                        <?= Yii::t('account', 'What services can I offer?') ?>
                    </div>
                    <div class="faq-body">
                        <p>
                            <?= Yii::t('account', 'Be creative! Think about what you can do best. Offer any services if they are legitimate and comply with our rules. The site presents more than 100 categories for placement of works. Study them: maybe this will give you an idea.') ?>
                        </p>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="faq-head">
                        <?= Yii::t('account', 'How much can I earn?') ?>
                    </div>
                    <div class="faq-body">
                        <p>
                            <?= Yii::t('account', 'It totally depends on you. You can work without time limits. Many Performers stay on Ujobs during the working day, some combine their permanent job with freelancing to get extra money. Average tariffs range from 500 - 3000 rubles per hour, the price depends on the client and your qualification.') ?>
                        </p>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="faq-head">
                        <?= Yii::t('account', 'How much does it cost? Wich is the commission?') ?>
                    </div>
                    <div class="faq-body">
                        <p>
                            <?= Yii::t('account', 'You can join Ujobs absolutely free of charge. No subscription or other costs. You get {percent1}% of each transaction. Our commission is {percent2}% of the amount of payment made, and it should be included in the cost that you set for your work.', ['percent1' => 80, 'percent2' => 20]) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 qa">
                <div class="faq-item">
                    <div class="faq-head">
                        <?= Yii::t('account', 'How much time will I have to invest?') ?>
                    </div>
                    <div class="faq-body">
                        <p>
                            <?= Yii::t('account', 'It’s very flexible. You need to put in some time and effort in the beginning to learn the marketplace and then you can decide for yourself what amount of work you want to do.') ?>
                        </p>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="faq-head">
                        <?= Yii::t('account', 'How should I offer my service?') ?>
                    </div>
                    <div class="faq-body">
                        <p>
                            <?= Yii::t('account', 'In the standard services section, you can assign any price from {minPrice} to {maxPrice} and offer three service options with different cost. When setting tariffs, be as realistic as possible, and think about whether customers are ready to buy services for your price.') ?>
                        </p>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="faq-head">
                        <?= Yii::t('account', 'How do I get paid?') ?>
                    </div>
                    <div class="faq-body">
                        <p>
                            <?= Yii::t('account', 'Once you complete a buyer’s order, the money is transferred to your account. You don`t need to chase clients for payments and wait 60 or 90 days for a check.') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="white-block">
    <div class="container-fluid">
        <div class="get-started text-center">
            <p class="text-center"><?= Yii::t('account', 'Sign up and create your first job') ?></p>
            <?php if (Yii::$app->user->isGuest) {
                echo Html::a(Yii::t('account', 'Get Started'), null, [
                    'class' => 'btn-big',
                    'data-toggle' => 'modal',
                    'data-target' => '#ModalSignup'
                ]);
            } else {
                echo Html::a(Yii::t('account', 'Get Started'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id], [
                    'class' => 'btn-big'
                ]);
            } ?>
        </div>
    </div>
</div>