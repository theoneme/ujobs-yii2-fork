<?php
use common\models\user\Profile;
use yii\helpers\Html;

/* @var Profile $model */
/* @var boolean $editable */
/* @var integer $index */

?>

<div class="seller-slide">
    <img src="<?= $model->getThumb('catalog') ?>"
         alt="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>">
    <div class="bs-someinfo text-left">
        <div class="bs-who"><?= \yii\helpers\StringHelper::truncate(Html::encode($model->translation->content ?? $model->bio), 40) ?></div>
        <div class="bs-name"><?= Html::encode($model->getSellerName()) ?></div>
    </div>
    <a class="show-stat"><?= Yii::t('account', 'View Statistics') ?></a>
</div>
<div class="statistics text-center row">
    <div class="stat-close text-right col-xs-12">
        <a>&times;</a>
    </div>
    <div class="col-xs-6 stat-item text-center">
        <span>5,061,990</span><br/>
        <?= Yii::t('account', 'Projects Completed') ?>
    </div>
    <div class="col-xs-6 stat-item text-center">
        <span><?= $model->getCategoriesCount() ?></span><br/>
        <?= Yii::t('account', 'Subcategories') ?>
    </div>
    <div class="col-xs-6 stat-item text-center">
        <span><?= random_int(1, 100) ?></span><br/>
        <?= Yii::t('account', 'Buyer origin country') ?>
    </div>
    <div class="col-xs-6 stat-item text-center">
        <span><?= Yii::$app->formatter->asDecimal($model->getMinPrice(), 0) . " р. - " . Yii::$app->formatter->asDecimal($model->getMaxPrice(), 0) . " р." ?></span>
        <br/>
        <?= Yii::t('account', 'Price Range') ?>
    </div>
    <?php if (Yii::$app->user->isGuest) {
        echo Html::a(Yii::t('account', 'Start Selling'), null, [
            'class' => 'btn-big',
            'data-toggle' => 'modal',
            'data-target' => '#ModalSignup'
        ]);
    } else {
        echo Html::a(Yii::t('account', 'Start Selling'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id], [
            'class' => 'btn-big'
        ]);
    } ?>
</div>