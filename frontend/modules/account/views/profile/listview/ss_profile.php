<?php
use common\models\user\Profile;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var Profile $model */
/* @var boolean $editable */
/* @var integer $index */
/* @var integer $count */
/* @var integer $keywords */
?>

    <div class="col-md-3 col-sm-4 col-xs-6 buy-sell-over">
        <a class="buy-sell" data-toggle="modal" data-target="#profile-<?= $model->user_id ?>">
            <img src="<?= $model->getThumb('catalog') ?>"
                 alt="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>"
                 title="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>">
            <div class="bs-someinfo text-left">
                <div class="bs-who"><?= StringHelper::truncate(Html::encode($model->translation->content ?? $model->bio), 40) ?></div>
                <div class="bs-name"><?= Html::encode($model->getSellerName()) ?></div>
            </div>
        </a>
    </div>

    <div class="modal fade top-modal bs-example-modal-lg" id="profile-<?= $model->user_id ?>" tabindex="-1"
         role="dialog" aria-hidden="true">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-body row">
                    <div style="background:#fff url(<?= $model->getThumb() ?>) top center / cover"
                         class="col-md-8 col-sm-8 col-xs-12 top-photo-col">
                        <div class="top-about">
                            <blockquote>
                                <?= StringHelper::truncate(Html::encode($model->translation->content ?? $model->bio), 200) ?>
                            </blockquote>
                            <cite>
                                <strong><?= Html::encode($model->getSellerName()) ?></strong>
                                |
                                <?= Yii::t('account', 'Top rated seller, completed {0} orders.', 15733) ?>
                            </cite>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 top-info-col">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <h2 class="text-center"><?= Html::encode($model->getSellerName()) ?></h2>
                        <div class="cat-options">
                            <div class="cat-option-item">
                                <span><?= Yii::$app->formatter->asDecimal(random_int(100, 9999), 0) ?></span><br/>
                                <?= Yii::t('account', 'Projects Completed') ?>
                            </div>
                            <div class="cat-option-item">
                                <span><?= $model->getCategoriesCount() ?></span><br/>
                                <?= Yii::t('account', 'Subcategories') ?>
                            </div>
                            <div class="cat-option-item">
                                <span><?= random_int(1, 100) ?></span><br/>
                                <?= Yii::t('account', 'Buyer origin country') ?>
                            </div>
                            <div class="cat-option-item">
                            <span>
                                <?= Yii::$app->formatter->asDecimal($model->getMinPrice(), 0) . " р. - " . Yii::$app->formatter->asDecimal($model->getMaxPrice(), 0) . " р." ?>
                            </span>
                                <br/>
                                <?= Yii::t('account', 'Price Range') ?>
                            </div>
                        </div>
                        <?php if (Yii::$app->user->isGuest) {
                            echo Html::a(Yii::t('account', 'Start Selling'), null, [
                                'class' => 'btn-big',
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalSignup'
                            ]);
                        } else {
                            echo Html::a(Yii::t('account', 'Start Selling'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id], [
                                'class' => 'btn-big'
                            ]);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if (($index + 1) == $count) { ?>
    <div class="col-md-3 col-sm-4 col-xs-6 buy-sell-over">
        <div class="buy-sell">
            <div class="whats-skill text-center">
                <i class="fa fa-heart"></i>
                <br/>
                <?= Yii::t('account', 'WHAT\'S <br/>YOUR SKILL?') ?>
                <?php if (Yii::$app->user->isGuest) {
                    echo Html::a(Yii::t('account', 'Become a Seller'), null, [
                        'class' => 'btn-big',
                        'data-toggle' => 'modal',
                        'data-target' => '#ModalSignup'
                    ]);
                } else {
                    echo Html::a(Yii::t('account', 'Become a Seller'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id], [
                        'class' => 'btn-big'
                    ]);
                } ?>
            </div>
        </div>
    </div>
<?php } ?>