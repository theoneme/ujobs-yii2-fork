<?php

use common\models\Offer;
use yii\helpers\Html;

/* @var $model Offer */
/* @var $index integer */
/* @var $editable boolean */

?>

<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img($model->from->getThumb('catalog'),['alt' => $model->from->getSellerName(), 'title' => $model->from->getSellerName()]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'by') ?>
            <?= Html::a(Html::encode($model->from->getSellerName()), $model->from->getSellerUrl(), ['data-pjax' => 0, 'title' => $model->from->getSellerName()]) ?>
        </div>
        <div class="rate">
            <?= $this->render('@frontend/widgets/views/rating', [
                'rating' => $model->from->profile->rating / 2,
                'size' => 10
            ]) ?>
        </div>
    </div>
</div>
<div class="bf-img" style="margin-top: 35px;">
    <h3 class="bf-offer-descr">
        <?= $model->description ?>
    </h3>
</div>
<div class="bf-cart text-center">
    <?= Html::a(Yii::t('account', 'Add to Cart'), null, [
        'class' => 'btn-small add-to-cart',
        'data-pjax' => 0,
        'data-id' => $model->id,
    ]); ?>
    <?php if ($editable === true) { ?>
        <?= Html::a(Yii::t('account', 'Delete'),
            ["/offer/delete", 'id' => $model->id],
            ['class' => 'btn-small red', 'data-method' => 'post', 'data-confirm' => Yii::t('account', 'Are you sure?'), 'data-pjax' => 0]
        ) ?>
    <?php } ?>
</div>
<div class="block-bottom">
    <div class="block-price">
        <?= $model->getPrice() ?>
    </div>
</div>