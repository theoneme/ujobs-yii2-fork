<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 15:24
 */

use common\models\Review;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var $model Review
 */

?>

<div class="this-comment">
    <div class="photo-com">
        <div class="text-center">
            <?php
            if ($model->createdBy->profile->gravatar_email) {
                echo Html::img($model->createdBy->profile->getThumb('catalog'),['alt' => $model->createdBy->username, 'title' => $model->createdBy->username]);
            } elseif ($model->createdBy->profile->name) {
                echo StringHelper::truncate(Html::encode($model->createdBy->profile->name), 1, '');
            } else {
                echo StringHelper::truncate(Html::encode($model->createdBy->username), 1, '');
            }
            ?>
        </div>
    </div>
    <div class="info-com text-left">
        <div class="who-com">
            <?= Html::a(Html::encode($model->createdBy->profile->name ? $model->createdBy->profile->name : $model->createdBy->username), Url::to([
                '/account/profile/show',
                'id' => $model->created_by
            ])) ?>
            <div class="com-stars">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $model->rating / 2,
                    'size' => 14
                ]) ?>
            </div>
        </div>
        <div class="com-text">
            <p><?= Html::encode($model->text) ?></p>
        </div>
        <div class="com-time"><?= Yii::$app->formatter->asDate($model->created_at) ?></div>
    </div>
</div>