<?php
use common\models\user\Profile;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var Profile $model */
/* @var boolean $editable */
/* @var integer $index */
?>

<div class="col-md-6 col-sm-6 col-xs-12 buyer-photo visible-xs">
    <img src="<?= $model->getThumb() ?>"
         alt="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>"
         title="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>">
</div>
<?php if ($index % 4 < 2) { ?>
    <div class="col-md-6 col-sm-6 col-xs-12 buyer-photo hidden-xs">
        <img src="<?= $model->getThumb() ?>"
             alt="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>"
             title="<?= isset($keywords) ? Yii::$app->utility->getElementFromArrayModule($keywords, $index, ',') : e($model->getSellerName()) ?>">
    </div>
<?php } ?>
<div class="col-md-6 col-sm-6 col-xs-12 buyer-text">
    <blockquote>
        "<?= StringHelper::truncate(Html::encode($model->translation->content ?? $model->bio), 160) ?>"
        <cite>
            <?= Html::encode($model->getSellerName()) ?>
        </cite>
    </blockquote>
</div>
<?php if ($index % 4 >= 2) { ?>
    <div class="col-md-6 col-sm-6 col-xs-12 buyer-photo hidden-xs">
        <img src="<?= $model->getThumb() ?>" alt="<?= Html::encode($model->getSellerName()) ?>" title="<?= Html::encode($model->getSellerName()) ?>">
    </div>
<?php } ?>

