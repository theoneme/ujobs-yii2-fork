<?php

use common\models\Job;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var Job $model
 * @var boolean $editable
 */

$type = $model->type === Job::TYPE_ADVANCED_TENDER ? Job::TYPE_TENDER : $model->type;
?>

    <div class="bf-alias" href="<?= Url::to(["/$type/view", 'alias' => $model->alias]) ?>">
        <div class="bf-img">
            <?= Html::img($model->getThumb('catalog'), ['alt' => $model->getLabel()]) ?>
        </div>
        <div class="bf-descr">
            <?= Html::encode($model->getLabel()) ?>
        </div>
        <?php if ($model->type === Job::TYPE_JOB) { ?>
            <div class="markers">
                <div class="text-center stick feat"><?= Yii::t('app', 'Favorites') ?></div>
            </div>
        <?php } ?>
    </div>
    <div class="block-bottom">
        <?= Html::a('<i class="fa fa-heart" aria-hidden="true"></i>', '#', [
                'class' => 'like hint--top',
                'data-hint' => Yii::t('app', 'Favourite'),
            ]
        ) ?>
        <?= Html::a("<span>" . Yii::t('app', 'starting at') . "</span>" . $model->getPrice(),
            Url::to(["/$type/view", 'alias' => $model->alias]),
            ['class' => 'block-price']
        ) ?>
    </div>
<?php if ($editable === true) { ?>
    <div class="job-owner-bg"></div>
    <div class="job-owner-buttons">
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Edit'),
                ["/{$type}/update", 'id' => $model->id],
                ['class' => 'btn-middle text-center', 'data-pjax' => 0]
            ) ?>
        </div>
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'View'),
                ["/{$type}/view", 'alias' => $model->alias],
                ['class' => 'btn-middle blue text-center', 'data-pjax' => 0]
            ) ?>
        </div>
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-times" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Delete'),
                ["/{$type}/delete", 'id' => $model->id],
                ['class' => 'btn-middle red text-center', 'data-method' => 'post', 'data-confirm' => Yii::t('account', 'Are you sure?'), 'data-pjax' => 0]
            ) ?>
        </div>
        <?php if ($model->status == Job::STATUS_ACTIVE) { ?>
            <div class="job-owner-button">
                <?= Html::a('<i class="fa fa-pause" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Pause'),
                    ["/{$type}/pause", 'id' => $model->id],
                    ['class' => 'btn-middle dark text-center', 'data-pjax' => 0]
                ) ?>
            </div>
        <?php } ?>
        <?php if ($model->status == Job::STATUS_PAUSED) { ?>
            <div class="job-owner-button">
                <?= Html::a('<i class="fa fa-play" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Resume'),
                    ["/{$type}/resume", 'id' => $model->id],
                    ['class' => 'btn-middle text-center', 'data-pjax' => 0]
                ) ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>