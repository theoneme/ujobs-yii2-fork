<?php

use common\modules\board\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var Product $model
 * @var boolean $editable
 */

$type = $model->type;
?>

    <div class="bf-alias" href="<?= Url::to(["/board/$type/view", 'alias' => $model->alias]) ?>">
        <div class="bf-img">
            <img src="<?= $model->getThumb('catalog') ?>" alt="<?= $model->getLabel() ?>">
        </div>
        <div class="bf-descr">
            <?= Html::encode($model->getLabel()) ?>
        </div>
    </div>
    <div class="block-bottom">
        <?= Html::a($model->getPrice(), ["/board/$type/view", 'alias' => $model->alias], ['class' => 'block-price']) ?>
    </div>

<?php if ($editable === true) { ?>
    <div class="job-owner-bg"></div>
    <div class="job-owner-buttons">
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Edit'),
                ["/board/{$type}/update", 'id' => $model->id],
                ['class' => 'btn-middle text-center', 'data-pjax' => 0]
            ) ?>
        </div>
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'View'),
                ["/board/{$type}/view", 'alias' => $model->alias],
                ['class' => 'btn-middle blue text-center', 'data-pjax' => 0]
            ) ?>
        </div>
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-times" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Delete'),
                ["/board/{$type}/delete", 'id' => $model->id],
                ['class' => 'btn-middle red text-center', 'data-method' => 'post', 'data-confirm' => Yii::t('account', 'Are you sure?'), 'data-pjax' => 0]
            ) ?>
        </div>
        <?php if ($model->status === Product::STATUS_ACTIVE) { ?>
            <div class="job-owner-button">
                <?= Html::a('<i class="fa fa-pause" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Pause'),
                    ["/board/{$type}/pause", 'id' => $model->id],
                    ['class' => 'btn-middle dark text-center', 'data-pjax' => 0]
                ) ?>
            </div>
        <?php } ?>
        <?php if ($model->status === Product::STATUS_PAUSED) { ?>
            <div class="job-owner-button">
                <?= Html::a('<i class="fa fa-play" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Resume'),
                    ["/board/{$type}/resume", 'id' => $model->id],
                    ['class' => 'btn-middle text-center', 'data-pjax' => 0]
                ) ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>