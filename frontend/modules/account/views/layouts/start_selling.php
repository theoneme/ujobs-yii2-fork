<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\StartSellingAsset;
use frontend\components\AccountConfirmModalWidget;
use frontend\components\HeaderCategories;
use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderRecoverModalWidget;
use frontend\components\HeaderSignupModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

StartSellingAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>
<?= \frontend\components\MobileMenu::widget() ?>
<div class="mainblock">
    <header>
        <?= $this->render('@frontend/views/layouts/_header') ?>
        <?= HeaderCategories::widget() ?>
    </header>
    <div class="all-content">
        <?= $content ?>
        <div class="mfooter"></div>
    </div>
</div>

<?= $this->render('@frontend/views/layouts/_footer') ?>
<?= HeaderLoginModalWidget::widget() ?>
<? //= NotyAlert::widget() ?>
<?= HeaderSignupModalWidget::widget() ?>
<?= AccountConfirmModalWidget::widget() ?>
<?= HeaderRecoverModalWidget::widget() ?>

<?php $this->registerJs("
var slickDir = false;
if($('body').hasClass('rtl')){
    slickDir = true;
}
    $('#mobile-people').slick({
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        rtl: slickDir,
        pauseOnHover:false
    });
    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
    $('a.show-stat').on('click',function (e) {
        e.preventDefault();
        $(this).parents('.top-mobile-seller').find('.seller-slide').addClass('blur');
        $(this).parents('.top-mobile-seller').find('.statistics').addClass('open');
        $('#mobile-people').slick('slickPause');
        $('#mobile-people .slick-dots').css('z-index',-1);
    });

    $('.stat-close a').on('click',function (e) {
        e.preventDefault();
        $(this).parents('.top-mobile-seller').find('.seller-slide').removeClass('blur');
        $(this).parents('.top-mobile-seller').find('.statistics').removeClass('open');
        $('#mobile-people').slick('slickPlay');
        $('#mobile-people .slick-dots').css('z-index',1);
    });
"); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
