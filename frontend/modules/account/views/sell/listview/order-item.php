<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.01.2017
 * Time: 19:04
 */

use yii\helpers\Html;
use common\modules\store\models\Order;
use yii\web\View;

/* @var Order $model
 * @var View $this
 */
$baseUrl = in_array($model->product_type, [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER]) ? '/account/order/tender-history' : '/account/order/history';
?>
<td>
    <?= Html::a(Html::img($model->product->getThumb('catalog'), ['alt' => $model->product->getLabel(),'title' => $model->product->getLabel()]), [$baseUrl, 'id' => $model->id], ['class'=>'tg-img', 'data-pjax' => 0]) ?>
</td>

<td>
    <?= Html::a($model->product->getLabel(), [$baseUrl, 'id' => $model->id], ['class'=>'tg-name', 'data-pjax' => 0]) ?>
</td>
<td>
    <?= Yii::$app->formatter->asDate($model->created_at) ?>
</td>
<td>
    <?= Yii::$app->formatter->asDate($model->tbd_at) ?>
</td>
<td>
    <?= $model->getTotal() ?>
</td>
<td>
    <?= $model->getStatusLabel(true) ?>
</td>