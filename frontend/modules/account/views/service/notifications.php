<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.01.2017
 * Time: 12:14
 */

use common\models\user\User;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var mixed $notifications */

?>

<div class="profile greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12 prof-aside">
                <div class="prof-aside-block row">
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <?php
                        /* @var $user User */
                        $user = Yii::$app->user->identity;
                        if ($user->profile->gravatar_email) {
                            ?>
                            <div class="prof-avatar text-center" style="background: url(<?= $user->profile->getThumb() ?>); background-size: cover;"></div>
                        <?php } else { ?>
                            <div class="prof-avatar text-center">
                                <?= StringHelper::truncate(Html::encode($user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <div class="prof-right-al">
                            <?= Html::a(Yii::t('account', 'My Favorites'), '#') ?>
                            <?= Html::a(Yii::t('account', 'Start Selling'), ['/entity/global-create']) ?>
                        </div>
                    </div>
                </div>
                <div class="prof-aside-block row aside-post text-center">
                    <div class="col-md-12">
                        <div class="post-aside-title"><?= Yii::t('account', 'Need Money?') ?></div>
                        <div class="post-mes">
                            <?= Yii::t('account', 'Post the services that you can make and receive orders') ?>
                        </div>
                        <div class="post-aside-img post-aside-money-icon"></div>
                        <?= Html::a(Yii::t('account', 'Start Earning'), ['/entity/global-create'], ['class' => 'btn-big']) ?>
                    </div>
                </div>
                <div class="prof-aside-block row aside-post text-center">
                    <div class="col-md-12">
                        <div class="post-aside-title"><?= Yii::t('account', 'What service are you looking for?') ?></div>
                        <div class="post-mes">
                            <?= Yii::t('account', 'Place an order and receive offers from qualified professionals') ?>
                        </div>
                        <div class="post-aside-img"></div>
                        <?= Html::a(Yii::t('account', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'btn-big']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <ul class="nav nav-tabs dashboards-tabs">
                    <li>
                        <a href="<?= Url::to(['/account/service/dashboard']) ?>">TO-DOS
                            <div class="not-count text-center">1</div>
                        </a>
                    </li>
                    <li class="active">
                        <a href="<?= Url::to(['/account/service/notifications']) ?>"><?= Yii::t('account', 'Notifications') ?>
                            <div class="not-count text-center"><?= $unreadNotifications ?></div>
                        </a>
                    </li>
                </ul>

                <div class="tab-pane fade in" id="tab2">
                    <?php if ($notifications == null) { ?>
                        <div class="empty-noti text-center">
                            <div class="bell text-center">
                                <i class="fa fa-bell-slash-o" aria-hidden="true"></i>
                            </div>
                            <div class="en-title text-center"><?= Yii::t('account', 'No Notifications') ?></div>
                            <div class="en-text"><?= Yii::t('account', 'Browse our catalog of works<br/>or offer your services on uJobs.') ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <?php foreach ($notifications as $notification) { ?>
                            <?= $this->render('@frontend/components/views/listview/' . $notification->subjectsToViews[$notification->subject], [
                                'model' => $notification
                            ]) ?>
                        <?php } ?>
                        <?=\yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                        ]);?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>