<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 16:40
 */

use common\models\user\User;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var integer $unreadNotifications
 */

?>

<div class="profile greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12 prof-aside">
                <div class="prof-aside-block row">
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <?php
                        /* @var $user User */
                        $user = Yii::$app->user->identity;
                        if ($user->profile->gravatar_email) {
                            ?>
                            <div class="prof-avatar text-center" style="background: url(<?= $user->profile->getThumb() ?>); background-size: cover;"></div>
                        <?php } else { ?>
                            <div class="prof-avatar text-center">
                                <?= StringHelper::truncate(Html::encode($user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <div class="prof-right-al">
                            <?= Html::a(Yii::t('account', 'My Favorites'), ['/account/favourite/index']) ?>
                            <?= Html::a(Yii::t('account', 'Invite friend'), ['/referral/index']) ?>
                            <?= Html::a(Yii::t('account', 'Start Selling'), ['/entity/global-create']) ?>
                        </div>
                    </div>
                </div>
                <div class="prof-aside-block row aside-post text-center">
                    <div class="col-md-12">
                        <div class="post-aside-title"><?= Yii::t('account', 'Need Money?') ?></div>
                        <div class="post-mes">
                            <?= Yii::t('account', 'Post the services that you can make and receive orders') ?>
                        </div>
                        <div class="post-aside-img post-aside-money-icon"></div>
                        <?= Html::a(Yii::t('account', 'Start Earning'), ['/entity/global-create'], ['class' => 'btn-big']) ?>
                    </div>
                </div>
                <div class="prof-aside-block row aside-post text-center">
                    <div class="col-md-12">
                        <div class="post-aside-title"><?= Yii::t('account', 'Invite friend') ?></div>
                        <div class="post-mes">
                            <?= Yii::t('account', 'And earn {0} during the year from all his transactions on the service.', ['5%']) ?>
                        </div>
                        <div class="post-aside-img referral-icon"></div>
                        <?= Html::a(Yii::t('account', 'Learn more'), ['/referral/index'], ['class' => 'btn-big']) ?>
                    </div>
                </div>
                <div class="prof-aside-block row aside-post text-center">
                    <div class="col-md-12">
                        <div class="post-aside-title"><?= Yii::t('account', 'What service are you looking for?') ?></div>
                        <div class="post-mes">
                            <?= Yii::t('account', 'Place an order and receive offers from qualified professionals') ?>
                        </div>
                        <div class="post-aside-img"></div>
                        <?= Html::a(Yii::t('account', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'btn-big']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <ul class="nav nav-tabs dashboards-tabs">
                    <li class="active">
                        <a href="<?= Url::to(['/account/service/dashboard']) ?>">TO-DOS
                            <div class="not-count text-center">1</div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/account/service/notifications']) ?>"><?= Yii::t('account', 'Notifications') ?>
                            <div class="not-count text-center"><?= $unreadNotifications ?></div>
                        </a>
                    </li>
                </ul>
                <div class="tab-content dashboards">
                    <div class="tab-pane fade in active" id="tab1">
                        <div class="notis-table">
                            <div class="notis-item">
                                <!--<div class="notis-ico">
                                    <div></div>
                                </div>-->
                                <div class="notis-text">
                                    <a href="<?= Url::to(['/account/service/account-settings']) ?>"> <?= Yii::t('account', 'Your user profile is incomplete. Please edit it') ?> </a>
                                    <!--                                    <p> .. .. .. .. </p>-->
                                </div>
                                <div class="notis-but">
                                    <a href="<?= Url::to(['/account/service/account-settings']) ?>" class="btn-middle"><?= Yii::t('account', 'Update Now') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>