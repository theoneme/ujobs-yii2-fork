<?php

use common\models\Company;
use common\models\CompanyMember;
use frontend\assets\CompanyAsset;
use frontend\assets\SelectizeAsset;
use frontend\modules\account\components\AccountSettingsLeftMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $company Company
 * @var $newMember CompanyMember
 */

$this->title = Yii::t('account', '«{company}» company members', ['company' => $company->getSellerName()]);
$availableRoles = array_diff_key(CompanyMember::getRoleLabels(), [CompanyMember::ROLE_OWNER => 0]);
CompanyAsset::register($this);
SelectizeAsset::register($this);
?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'company']) ?>
            </div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left">
                        <?= $this->title ?>
                    </h1>
                    <div class="company-member-add-container clearfix">
                        <?= Html::a(Yii::t('app', 'Add member'), null, ['class' => 'company-member-add btn-middle revert'])?>
                        <?php $form = ActiveForm::begin([
                            'action' => ['/account/service/company-member-add'],
                            'id' => 'member-form',
                            'options' => [
                                'class' => 'company-member-add-form'
                            ]
                        ]);?>
                            <?= Html::activeHiddenInput($newMember, 'company_id', ['value' => $company->id])?>
                            <div class="row">
                                <div class="col-md-7 selectize-input-container">
                                    <?= $form->field($newMember, 'user_id')->dropDownList([], ['class' => 'company-user-selectize'])->label(Yii::t('app', 'User'))?>
                                </div>
                                <div class="col-md-5">
                                    <?= $form->field($newMember, 'role')->dropDownList($availableRoles)?>
                                </div>
                            </div>
                            <div class="buttons pull-right">
                                <?= Html::a(Yii::t('app', 'Cancel'), null, ['class' => 'company-member-add-cancel btn-middle revert'])?>
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn-middle'])?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <?php if (count($company->companyMembers)) { ?>
                        <ul class="company-list company-member-list">
                            <li class="company-item">
                                <div class="company-title">
                                    <?= Yii::t('app', 'Company member')?>
                                </div>
                                <div class="company-role">
                                    <?= Yii::t('app', 'Company role')?>
                                </div>
                                <div class="company-status">
                                    <?= Yii::t('app', 'Status')?>
                                </div>
                                <div class="company-actions">
                                </div>
                            </li>
                            <?php foreach ($company->companyMembers as $member) { ?>
                                <li class="company-item">
                                    <div class="company-title">
                                        <div class="company-title-label">
                                            <?= Html::a(
                                                $member->user->getSellerName(),
                                                ['/account/profile/show', 'id' => $member->user_id]
                                            )?>
                                        </div>
                                    </div>
                                    <div class="company-role">
                                        <div class="company-role-label"><?= $member->getRoleLabel()?></div>
                                        <?php
                                        if ($member->role !== CompanyMember::ROLE_OWNER) {
                                            echo Html::dropDownList(
                                                'role',
                                                $member->role,
                                                $availableRoles,
                                                ['data-member' => $member->id, 'class' => 'company-role-dropdown form-control']
                                            );
                                        }
                                        ?>
                                    </div>
                                    <div class="company-status">
                                        <div class="company-status-label"><?= $member->getStatusLabel()?></div>
                                    </div>
                                    <div class="company-actions action-buttons">
                                        <?php
                                        if ($member->role !== CompanyMember::ROLE_OWNER && $member->user_id !== userId()) {
                                            echo Html::a(
                                                '<i class="fa fa-pencil"></i>',
                                                null,
                                                ['class' => 'company-member-edit hint--top', 'data-hint' => Yii::t('app', 'Edit')]
                                            );
                                            echo Html::a(
                                                '<i class="fa fa-user-times"></i>',
                                                ['/account/service/company-member-dismiss', 'id' => $member->id],
                                                ['class' => 'hint--top', 'data-hint' => Yii::t('app', 'Dismiss'), 'data-confirm' => Yii::t('app', 'Are you sure you want to dismiss this member?')]
                                            );
                                        }
                                        ?>
                                    </div>
                                    <div class="company-actions edit-buttons">
                                        <?php
                                        if ($member->role !== CompanyMember::ROLE_OWNER) {
                                            echo Html::a(
                                                '<i class="fa fa-save"></i>',
                                                null,
                                                ['class' => 'company-member-save hint--top', 'data-hint' => Yii::t('app', 'Save')]
                                            );
                                            echo Html::a(
                                                '<i class="fa fa-times"></i>',
                                                null,
                                                ['class' => 'company-member-cancel hint--top', 'data-hint' => Yii::t('app', 'Cancel')]
                                            );
                                        }
                                        ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$memberEditUrl = Url::to(['/account/service/company-member-edit']);
$sellersUrl = Url::to(['/ajax/sellers-list']);
$script = <<<JS
    $(document).on('click', '.company-member-edit', function(){
        $(this).closest('.company-item').addClass('editing');
        return false;
    });
    $(document).on('click', '.company-member-cancel', function(){
        $(this).closest('.company-item').removeClass('editing');
        return false;
    });
    $(document).on('click', '.company-member-add', function(){
        $(this).closest('.company-member-add-container').addClass('editing');
        return false;
    });
    $(document).on('click', '.company-member-add-cancel', function(){
        $(this).closest('.company-member-add-container').removeClass('editing');
        return false;
    });
    $(document).on('click', '.company-member-save', function(){
        let input = $(this).closest('.company-item').find('.company-role-dropdown');
        let label = $(this).closest('.company-item').find('.company-role-label');
        $.post('$memberEditUrl',
            {id: input.data('member'), role: input.val()},
            function(data) {
                if (data.success === true) {
                    label.html(data.role);
                    label.closest('.company-item').removeClass('editing');
                    alertCall('top', 'success', data.message);
                } else {
                    alertCall('top', 'error', data.message);
                }
            },
            "json"
        );
        return false;
    });
    
    $(".company-user-selectize").selectize({
        valueField: "id",
        labelField: "value",
        searchField: ["value", "email", 'phone'],
        persist: false,
        create: false,
        maxItems: 1,
	    maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post(
                "$sellersUrl", 
                {query: encodeURIComponent(query)},
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        },
        render: {
	        option: function(data, escape) {
	            return '<div class="option custom-selectize-option flex flex-v-center" data-value="' + data.id + '">' +
                           '<img src="' + data.img + '">' +
                           '<span class="selectize-option-title">' + data.value + '</span>' + 
                       '</div>';
	        }
	    },
    });
JS;
$this->registerJs($script);
?>