<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 17:51
 */

/* @var DeactivateForm $deactivateModel */
/* @var PasswordChangeForm $passwordChangeModel */
/* @var PauseForm $pauseModel */

use frontend\modules\account\models\DeactivateForm;
use frontend\modules\account\models\PasswordChangeForm;

use frontend\modules\account\models\PauseForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= \frontend\modules\account\components\AccountSettingsLeftMenu::widget(['active' => 'account-actions']) ?>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left"><?= Yii::t('account', 'Account Actions') ?></h1>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => Url::to(['/account/service-ajax/process-password-change'])
                    ]) ?>
                    <div class="group-set">
                        <div class="formgig-block">
                            <div class="row set-item">
                                <div class="col-md-12">
                                    <div class="set-title"><?= Yii::t('account', 'Change Password') ?></div>
                                </div>
                            </div>
                            <div class="row set-item">
                                <?= $form->field($passwordChangeModel, 'old_password', [
                                    'template' => '
                                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">{label}</div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                                {input}{error}
                                            </div>'
                                ])->passwordInput() ?>
                            </div>
                            <div class="row set-item">
                                <?= $form->field($passwordChangeModel, 'new_password', [
                                    'template' => '
                                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">{label}</div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                                {input}{error}
                                            </div>'
                                ])->passwordInput() ?>
                            </div>
                            <div class="row set-item">
                                <?= $form->field($passwordChangeModel, 'confirm_password', [
                                    'template' => '
                                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">{label}</div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                                {input}{error}
                                            </div>'
                                ])->passwordInput() ?>
                            </div>
                            <?= Html::submitInput(Yii::t('account', 'Save Changes'), ['class' => 'bright']) ?>
                            <div class="clear"></div>
                            <div class="row set-item"></div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Protect yourself') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Think of a strong password (capital letters, numbers, special characters)')?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => Url::to(['/account/service-ajax/process-vacation'])
                    ]) ?>
                    <div class="group-set">
                        <div class="formgig-block">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title"><?= Yii::t('account', 'Vacation mode') ?></div>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                    <?= $form->field($pauseModel, 'action', [
                                        'template' => '{input}{label}',
                                        'options' => ['class' => 'onoffswitch']
                                    ])
                                        ->checkbox(['class' => 'onoffswitch-checkbox', 'id' => 'myonoffswitch'], false)
                                        ->label('<span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span>', ['class' => 'onoffswitch-label'])
                                    ?>
                                    <div class="vac-text"><?= $pauseModel->getAttributeLabel('action') ?></div>
                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Do you want to pause your activities on uJobs?') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'When you turn on vacation mode, all your jobs, products and requests will disappear from the catalog. When you decide to return to work, you will just have to turn off the mode and everything will return to their places in the catalogs.')?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => Url::to(['/account/service-ajax/process-deactivation'])
                    ]) ?>
                    <div class="group-set">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                <div class="set-title"><?= Yii::t('account', 'Account Deactivation') ?></div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row set-item">
                                <?= $form->field($deactivateModel, 'reason', [
                                    'template' => '
                                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">{label}</div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                                {input}{error}
                                            </div>'
                                ])->dropDownList($deactivateModel->reasons, ['class' => 'reason form-control', 'prompt' => ' -- ' . Yii::t('account', 'Choose reason')]) ?>
                            </div>
                            <div class="row set-item addinp">
                                <?= $form->field($deactivateModel, 'reason_additional', [
                                    'template' => '
                                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">{label}</div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                                {input}{error}
                                            </div>'
                                ])->textarea() ?>
                            </div>
                            <?= Html::submitInput(Yii::t('account', 'Deactivate Account'), ['class' => 'bright inpd', 'disabled' => 'disabled']) ?>
                            <div class="row set-item"></div>

                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'What happens when you deactivate your account?') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <ul>
                                        <li>
                                            <?= Yii::t('account', "Your profile and your works will not be shown on Ujobs anymore.") ?>
                                            (<?= Yii::t('account', 'People who will try to view your profile or works will get an "Unavailable Page" message.')?>)
                                        </li>
                                        <li>
                                            <?= Yii::t('account', 'All open orders will be canceled, and money will be returned.') ?>
                                        </li>
                                        <li>
                                            <?= Yii::t('account', "You will not be able to reactivate your works") ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $script = <<<JS
    $(document).on('change', '.reason', function () {
        $('.addinp').show();
        $('.inpd').removeAttr('disabled');
    });

    $(document).on('change', '#myonoffswitch', function() {
        $(this).closest('form').submit();
    });
JS;
$this->registerJs($script);

