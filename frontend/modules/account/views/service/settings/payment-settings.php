<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 18:08
 */

use common\models\WithdrawalWallet;
use frontend\assets\JqueryMaskAsset;
use frontend\modules\account\components\AccountSettingsLeftMenu;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\Profile;
use common\models\user\User;

/**
 * @var Profile $profile
 * @var User $user
 * @var View $this
 * @var array $withdrawalTypesData
 */

JqueryMaskAsset::register($this);
$fieldTemplate = '
    <div class="col-md-4 col-sm-4 col-xs-4 leftset">
        <div class="set-title">{label}</div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-8 rightset">
        {input}{error}
    </div>
';
?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'payment-settings']) ?>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="settings-content payment-settings">
                    <h1 class="text-left profileh" style="margin-bottom: 0"><?= Yii::t('account', 'Payment Information') ?></h1>
                    <?php $form = ActiveForm::begin([
                        'id' => 'payment-settings-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => Url::to(['/account/service-ajax/process-payment-settings'])
                    ]) ?>

                        <div class="group-set">
<!--                            <h2 class="profileh text-left" style="margin-bottom: 5px">--><?//= Yii::t('account', 'Payment Information') ?><!--</h2>-->
                            <h3 class="profileh"><?= Yii::t('account', 'Specify how you prefer to receive money') ?></h3>
                            <?php foreach ($withdrawalTypesData['preferredTypes'] as $type => $label) {
                                $wallet = $profile->withdrawalWallets[$type] ?? new WithdrawalWallet(['type' => $type]);
                            ?>
                                <div class="formgig-block">
                                    <div class="field-trigger">
                                        <?= Html::hiddenInput("{$type}-trig", 0) ?>
                                        <?= Html::checkbox('phone-trig', !empty($wallet->number), [
                                            'id' => "{$type}-trig",
                                            'data-role' => 'field-expander',
                                            'data-target' => "{$type}-payment"
                                        ]) ?>
                                        <label for="<?= "{$type}-trig"?>"><?= $label ?></label>
                                    </div>
                                    <div class="row <?= empty($wallet->number) ? 'hidden' : '' ?>">
                                        <div class="wallet-type">
                                            <?= $label?>
                                        </div>
                                        <?= $form->field($wallet, "[$type]id")->hiddenInput()->label(false)->error(false)?>
                                        <?= $form->field($wallet, "[$type]type")->hiddenInput()->label(false)->error(false)?>
                                        <?= $form->field($wallet, "[$type]number", ['template' => $fieldTemplate])->textInput([
                                            'class' => 'form-control ' . $wallet->getTypeMask(),
                                            'placeholder' => $wallet->getFieldPlaceholder('number')
                                        ]) ?>
                                        <?php foreach ($wallet->getCustomDataFields() as $field) {
                                            echo $form->field($wallet, "[$type][customDataArray]$field", ['template' => $fieldTemplate])->textInput([
                                                'value' => $wallet->customDataArray[$field] ?? '',
                                                'placeholder' => $wallet->getFieldPlaceholder($field)
                                            ])->label($wallet->getAttributeLabel($field));
                                        }?>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?=Yii::t('app', 'Enter {what}', ['what' => $label])?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?=Yii::t('account', 'For example: {example}', ['example' => $wallet->getTypeExample()])?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="more-container">
                                <a class="show-more"><?= Yii::t('app', 'Show more')?></a>
                                <div class="more">
                                    <?php foreach ($withdrawalTypesData['otherTypes'] as $type => $label) {
                                        $wallet = $profile->withdrawalWallets[$type] ?? new WithdrawalWallet(['type' => $type]);
                                    ?>
                                        <div class="formgig-block">
                                            <div class="field-trigger">
                                                <?= Html::hiddenInput("{$type}-trig", 0) ?>
                                                <?= Html::checkbox('phone-trig', !empty($wallet->number), [
                                                    'id' => "{$type}-trig",
                                                    'data-role' => 'field-expander',
                                                    'data-target' => "{$type}-payment"
                                                ]) ?>
                                                <label for="<?= "{$type}-trig"?>"><?= $label ?></label>
                                            </div>
                                            <div class="row <?= empty($wallet->number) ? 'hidden' : '' ?>">
                                                <div class="wallet-type">
                                                    <?= $label?>
                                                </div>
                                                <?= $form->field($wallet, "[$type]id")->hiddenInput()->label(false)->error(false)?>
                                                <?= $form->field($wallet, "[$type]type")->hiddenInput()->label(false)->error(false)?>
                                                <?= $form->field($wallet, "[$type]number", ['template' => $fieldTemplate])->textInput([
                                                    'class' => 'form-control ' . $wallet->getTypeMask(),
                                                    'placeholder' => $wallet->getFieldPlaceholder('number')
                                                ]) ?>
                                                <?php foreach ($wallet->getCustomDataFields() as $field) {
                                                    echo $form->field($wallet, "[$type][customDataArray]$field", ['template' => $fieldTemplate])->textInput([
                                                        'value' => $wallet->customDataArray[$field] ?? '',
                                                        'placeholder' => $wallet->getFieldPlaceholder($field)
                                                    ])->label($wallet->getAttributeLabel($field));
                                                }?>
                                            </div>
                                            <div class="notes col-md-6 col-md-offset-12">
                                                <div class="notes-head">
                                                    <div class="lamp text-center">
                                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="notes-title"><?=Yii::t('app', 'Enter {what}', ['what' => $label])?></div>
                                                </div>
                                                <div class="notes-descr">
                                                    <p>
                                                        <?=Yii::t('account', 'For example: {example}', ['example' => $wallet->getTypeExample()])?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row set-item"></div>
                        <?= Html::submitInput(Yii::t('account', 'Save Changes'), ['class' => 'bright']) ?>
                    <?php ActiveForm::end(); ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$masks = json_encode(WithdrawalWallet::getMasks());
$script = <<<JS
    let masks = $masks;
    $.each(masks, function(index, value ){
        $('.' + index).mask(value);
    });
    $(document).on('beforeSubmit', '#payment-settings-form', function() {
        $.each(masks, function(index, value ){
            $('.' + index).each(function(){
                $(this).val($(this).val().replace(/[^0-9a-zA-Z]/g, ''))
            });
        });
    });
    $('[data-role=field-expander]').on('change', function() {
         $(this).closest('.field-trigger').next('.row').toggleClass('hidden');
         if($(this).is(':checked') === false) {
             $(this).closest('.field-trigger').next('.row').find('input').val('');
         }
    });
JS;
$this->registerJs($script);

$this->registerJs("
if(typeof fbq != 'undefined') {
    fbq('track', 'AddPaymentInfo');
}
", \yii\web\View::POS_LOAD);