<?php

use common\models\CompanyMember;
use frontend\assets\CompanyAsset;
use frontend\modules\account\components\AccountSettingsLeftMenu;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $members CompanyMember[]
 */

$this->title = Yii::t('account', 'My companies');
CompanyAsset::register($this);
?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'companies']) ?>
            </div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left">
                        <?= $this->title ?>
                    </h1>
                    <?php if (count($members)) { ?>
                        <ul class="company-list">
                            <li class="company-item">
                                <div class="company-title">
                                    <?= Yii::t('app', 'Company')?>
                                </div>
                                <div class="company-role">
                                    <?= Yii::t('app', 'Company role')?>
                                </div>
                                <div class="company-actions">
                                </div>
                            </li>
                            <?php foreach ($members as $member) { ?>
                                <li class="company-item">
                                    <div class="company-title">
                                        <div class="company-title-label"><?= Html::a($member->company->getSellerName(), ['/company/profile/show', 'id' => $member->company_id])?></div>
                                    </div>
                                    <div class="company-role">
                                        <div class="company-role-label"><?= $member->getRoleLabel()?></div>
                                    </div>
                                    <div class="company-actions">
                                        <?php
                                        if ($member->isAllowedToManage()) {
                                            echo Html::a(
                                                '<i class="fa fa-group"></i>',
                                                ['/account/service/company', 'id' => $member->company_id],
                                                ['class' => 'hint--top', 'data-hint' => Yii::t('app', 'Company members')]
                                            );
                                        }
                                        if ($member->role !== CompanyMember::ROLE_OWNER) {
                                            echo Html::a(
                                                '<i class="fa fa-sign-out"></i>',
                                                ['/account/service/company-leave', 'id' => $member->company_id],
                                                ['class' => 'hint--top', 'data-hint' => Yii::t('app', 'Leave company'), 'data-confirm' => Yii::t('app', 'Are you sure you want to leave the company?')]
                                            );
                                        }
                                        ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

