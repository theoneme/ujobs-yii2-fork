<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 18:09
 */

use common\helpers\FileInputHelper;
use common\models\ContentTranslation;
use common\models\user\Profile;
use common\models\user\Token;
use common\models\UserAttribute;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\modules\account\components\AccountSettingsLeftMenu;
use kartik\file\FileInput;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var ActiveDataProvider $languageDataProvider */
/* @var ActiveDataProvider $skillDataProvider */
/* @var ActiveDataProvider $specialtyDataProvider */
/* @var Profile $model */
/* @var array $userLanguageLocales */
/* @var UserAttribute $cityAttribute */

CropperAsset::register($this);
SelectizeAsset::register($this);
?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'public-profile-settings']) ?>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="text-left profileh"><?= Yii::t('account', 'Public Profile Settings') ?></h1>
                    <?php if ($model->user->confirmed_at === null) { ?>
                        <div class="formgig-block">
                            <div class="row set-item">
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title title-big"><?= Yii::t('account', 'Account confirmation') ?></div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                    <?php if (!empty($model->user->email)) { ?>
                                        <?= Html::a(Yii::t('account', 'Send confirmation by {what}', ['what' => 'email']), '/site/index', [
                                            'class' => 'btn-middle confirm-method',
                                            'data-target' => Token::TARGET_EMAIL,
                                            'data-type' => 'email'
                                        ]) ?>
                                        <div class="confirm-block" data-type="email"></div>
                                    <?php } ?>
                                    <?php if (!empty($model->user->phone)) { ?>
                                        <?= Html::a(Yii::t('account', 'Send confirmation by {what}', ['what' => 'sms']), null, [
                                            'class' => 'btn-middle confirm-method',
                                            'data-target' => Token::TARGET_PHONE,
                                            'data-type' => 'sms'
                                        ]) ?>
                                        <div class="confirm-block" data-type="sms">
                                            <p><?= Yii::t('app', 'Enter the confirmation code') ?></p>
                                            <?= Html::beginForm('/user/registration/confirm', 'get', ['class' => 'form-edescr']); ?>
                                            <?= Html::hiddenInput('id', $model->user_id); ?>
                                            <?= Html::textInput('code', null, ['class' => 'token-input', 'placeholder' => Yii::t('app', 'Confirmation code')]) ?>
                                            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-middle']) ?>
                                            <?= Html::endForm(); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Confirm your account') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'You need to confirm your account before we will publish your profile in catalog') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'action' => Url::to(['/account/service-ajax/process-public-profile-settings']),
                        'options' => [
                            'class' => 'form-edescr',
                            'id' => 'public-profile-form'
                        ]
                    ]) ?>

                    <div class="formgig-block">
                        <div class="row set-item">
                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                <div class="set-title title-big"><?= Yii::t('account', 'Profile Photo') ?></div>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                <?php $image_suffix = preg_match('/^http(s)?:\/\/.*/', $model->gravatar_email) ? '' : '?' . time() ?>
                                <?= FileInput::widget(
                                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                        'id' => 'file-upload-input',
                                        'name' => 'uploaded_images',
                                        'pluginOptions' => [
                                            'uploadUrl' => Url::toRoute('/image/upload-profile'),
                                            'overwriteInitial' => true,
                                            'initialPreview' => !empty($model->gravatar_email) ? $model->getThumb() : false,
                                            'initialPreviewConfig' => !empty($model->gravatar_email) ? [[
                                                'caption' => basename($model->gravatar_email),
                                                'url' => Url::toRoute('/image/delete'),
                                                'key' => $model->user_id
                                            ]] : [],
                                            'otherActionButtons' =>
                                                '<button type="button" class="btn btn-sm btn-default crop-button" data-ratio="1" title="' . Yii::t('app', 'Crop image') . '">
                                                    <i class="fa fa-crop" aria-hidden="true"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-default crop-button" data-ratio="1" title="' . Yii::t('app', 'Rotate image') . '">
                                                    <i class="fa fa-rotate-left" aria-hidden="true"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-default crop-button" data-ratio="1" title="' . Yii::t('app', 'Rotate image') . '">
                                                    <i class="fa fa-rotate-right" aria-hidden="true"></i>
                                                </button>',
                                        ]
                                    ])
                                ) ?>
                            </div>
                            <?= $form->field($model, 'gravatar_email')->hiddenInput([
                                'id' => 'gravatar-input',
                                'value' => $model->gravatar_email,
                                'data-key' => $model->user_id
                            ])->label(false); ?>
                        </div>
                        <div class="notes col-md-6 col-md-offset-12">
                            <div class="notes-head">
                                <div class="lamp text-center">
                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                </div>
                                <div class="notes-title"><?= Yii::t('account', 'Upload your photo') ?></div>
                            </div>
                            <div class="notes-descr">
                                <p>
                                    <?= Yii::t('account', 'Your photo. If you want to increase chances to be chosen by customers - please upload it.') ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="">
                        <div class="formgig-block">
                            <div class="leftset">
                                <div class="set-title title-big">
                                    <label><?= Yii::t('account', 'What do you want to do on uJobs?') ?></label>
                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Your targets on uJobs?') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'You specify the necessary information, when you choose what you want to do. You can always return to this step if you decide to expand your activities on the service') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="reason clearfix">
                            <div class="reason-check">
                                <div class="formgig-block">
                                    <div class="leftset">
                                        <?= Html::activeCheckbox($model, 'is_customer', ['id' => 'buy', 'label' => false]) ?>
                                        <?= Html::label(Yii::t('account', 'Order services or buy Goods'), 'buy') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="reason clearfix">
                            <div class="reason-check">
                                <div class="formgig-block">
                                    <div class="leftset">
                                        <?= Html::activeCheckbox($model, 'is_product_seller', ['id' => 'sell', 'label' => false]) ?>
                                        <?= Html::label(Yii::t('account', 'Sell Goods. Find Buyers for My Product'), 'sell') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="reason-options">
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title"><?= Yii::t('account', 'City') ?></div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset selectize-input-container city-selectize">
                                    <div class="profile-location-title-string">
                                        <?php $locationString = $model->getFrom() ?>
                                        <strong>
                                            <?= (!empty($locationString) ? $locationString : Yii::t('account', 'Enter city')) . '&nbsp;' . Html::a('<i class="fa fa-pencil"></i>', null, ['id' => 'edit-city']) ?>
                                        </strong>
                                    </div>
                                    <div id="city-form" style="display: none">
                                        <?= $form->field($model, 'city')->hiddenInput(['id' => 'city'])->label(false)->error(false) ?>
                                        <?= Html::activeHiddenInput($model, 'lat', ['id' => 'profile-lat']) ?>
                                        <?= Html::activeHiddenInput($model, 'long', ['id' => 'profile-long']) ?>
                                        <?= Html::activeHiddenInput($model, '[locationData]country', ['id' => 'profile-location-country', 'value' => false]) ?>
                                        <?= Html::activeHiddenInput($model, '[locationData]city', ['id' => 'profile-location-city']) ?>
                                        <?= Html::textInput('location_input', null,
                                            [
                                                'id' => 'profile-location-input',
                                                'placeholder' => Yii::t('app', 'Enter your city and choose it from dropdown'),
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="reason clearfix">
                            <div class="reason-check">
                                <div class="formgig-block">
                                    <div class="leftset">
                                        <?= Html::activeCheckbox($model, 'is_service_seller', ['id' => 'work', 'label' => false]) ?>
                                        <?= Html::label(Yii::t('account', 'Offer your services, find new customer'), 'work') ?>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('account', 'Professional') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'In order to get you into the Professional directory, you need to describe what you can do, what skills you have and in which languages you can communicate with the customer') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="reason-options">
                                <div class="formgig-block">
                                    <div class="row set-item">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title"><?= Yii::t('account', 'I Can Communicate In') ?></div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <?= \yii\widgets\ListView::widget([
                                                'dataProvider' => $languageDataProvider,
                                                'itemView' => 'listview/language',
                                                'viewParams' => ['editable' => true],
                                                'emptyText' => Yii::t('account', 'No Languages Specified'),
                                                'options' => [
                                                    'id' => 'language-listview'
                                                ],
                                                'itemOptions' => [
                                                    'class' => 'worker-info-item'
                                                ],
                                                'layout' => "{items}"
                                            ]) ?>
                                            <div id="language-container"></div>
                                            <a href="#" class="add-lang" id="load-language">
                                                <i class="fa fa-plus"></i> <?= Yii::t('account', 'Add Language') ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('app', 'Languages') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'You can make up to four selections.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="formgig-block">
                                    <div class="row set-item">
                                        <?= $form->field($model, 'name', [
                                            'template' => '
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label} ' . ' ' . Yii::t('account', 'in ' . Yii::$app->params['languages'][Yii::$app->language]) . '</div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                        ]) ?>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('model', 'Your Name') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'Enter a name that will be displayed to other users') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="formgig-block">
                                    <div class="row set-item">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label>
                                                    <?= Yii::t('account', 'Something About You') . ' (' . mb_strtoupper(Yii::$app->params['supportedLocales'][Yii::$app->language]) . ')' ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <?= $form->field($model, 'bio', [
                                                'template' => '{input} <div class="counttext"><span class="ctspan">0</span> / 300 ' . Yii::t('app', 'Max') . '</div> {error}'
                                            ])->textarea([
                                                'class' => 'readsym',
                                                'placeholder' => Yii::t('account', 'Example: "I am writing articles. I can maintain a group on the social networks " or " I\'m a designer. I can help in creating unique business cards and logos. I work in PhotoShop programs since 2011."')
                                            ])->label(false); ?>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('account', 'Write about yourself') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'Tell us what you do and how you can be useful for our customers, what\'s your experience') . '. ' . Yii::t('account', '{count} characters', ['count' => '0-300']) ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($userLanguageLocales as $locale => $language) {
                                    $translation = $model->translations[$locale] ?? new ContentTranslation() ?>
                                    <div class="formgig-block profile-translations-block">
                                        <div class="row set-item">
                                            <div class="col-md-12 col-sm-12 col-xs-12 leftset">
                                                <a class="translation-toggle">
                                                    <i class="fa fa-<?= $translation->isNewRecord ? 'plus' : 'pencil' ?>"></i>
                                                    <?= Yii::t('account',
                                                        $translation->isNewRecord ? 'Add information about yourself {language}' : 'Change information about yourself {language}',
                                                        ['language' => Yii::t('account', 'in ' . Yii::$app->params['languages'][$locale])]
                                                    ) ?>
                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-xs-4 leftset hidden">
                                                <div class="set-title">
                                                    <?= Yii::t('model', 'Your Name') . ' ' . Yii::t('account', 'in ' . Yii::$app->params['languages'][$locale]) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-xs-8 rightset hidden">
                                                <!--                                                --><?php //= $form->field($translation, "[$locale]id")->hiddenInput(['disabled' => $translation->isNewRecord, 'value' => $translation->isNewRecord ? null : $translation->id])->label(false); ?>
                                                <?= $form->field($translation, "[$locale]title")->textInput(['disabled' => $translation->isNewRecord])->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row set-item">
                                            <div class="col-md-3 col-sm-4 col-xs-4 leftset hidden">
                                                <div class="set-title">
                                                    <?= Yii::t('account', 'Something About You') . ' (' . mb_strtoupper(Yii::$app->params['supportedLocales'][$locale]) . ')' ?>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-xs-8 rightset hidden">
                                                <?= $form->field($translation, "[$locale]content", [
                                                    'template' => '{input} <div class="counttext"><span class="ctspan">0</span> / 300 ' . Yii::t('app', 'Max') . '</div> {error}'
                                                ])->textarea(['disabled' => $translation->isNewRecord, 'class' => 'readsym'])->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('account', 'Information in another languages') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <p>
                                                    <?= Yii::t('account',
                                                        'Fill in information about yourself {language} so that customers from other countries can see your profile',
                                                        ['language' => Yii::t('account', 'in ' . Yii::$app->params['languages'][$locale])]
                                                    ) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="formgig-block">
                                    <div class="row set-item">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title"><?= Yii::t('account', 'I have skills') ?></div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <?= \yii\widgets\ListView::widget([
                                                'dataProvider' => $skillDataProvider,
                                                'itemView' => 'listview/skill',
                                                'viewParams' => ['editable' => true],
                                                'emptyText' => Yii::t('account', 'No Skills Specified'),
                                                'options' => [
                                                    'id' => 'language-listview'
                                                ],
                                                'itemOptions' => [
                                                    'class' => 'worker-info-item'
                                                ],
                                                'layout' => "{items}"
                                            ]) ?>
                                            <div id="skill-container"></div>
                                            <a href="#" class="add-lang" id="load-skill">
                                                <i class="fa fa-plus"></i> <?= Yii::t('account', 'Add Skill') ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('app', 'Skills') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'You can make up to four selections.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="reason clearfix">
                            <div class="reason-check">
                                <div class="formgig-block">
                                    <div class="row freelancer-block">
                                        <div class="col-md-12 leftset">
                                            <?= Html::activeCheckbox($model, 'is_freelancer', ['id' => 'freelancer-checkbox', 'label' => false]) ?>
                                            <?= Html::label(Yii::t('account', 'I want to be a remote worker'), 'freelancer-checkbox') ?>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('account', 'I have specialties') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'If you wanna be a freelancer, write list of specialties you are willing to work, how many hours per week and what salary you expect') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="reason-options">

                                <div class="formgig-block">
                                    <div class="row set-item specialties-block">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title"><?= Yii::t('account', 'I have specialties') ?></div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <?= \yii\widgets\ListView::widget([
                                                'dataProvider' => $specialtyDataProvider,
                                                'itemView' => 'listview/specialty',
                                                'viewParams' => ['editable' => true],
                                                'emptyText' => '',
                                                'options' => [
                                                    'id' => 'language-listview'
                                                ],
                                                'itemOptions' => [
                                                    'class' => 'worker-info-item'
                                                ],
                                                'layout' => "{items}"
                                            ]) ?>
                                            <div id="specialty-container"></div>
                                            <a href="#" class="add-lang" id="load-specialty">
                                                <i class="fa fa-plus"></i> <?= Yii::t('account', 'Add Specialty') ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('app', 'Specialty') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'You can make up to four selections.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= Html::submitInput(Yii::t('account', 'Save Changes'), [
                            'id' => 'submit-button',
                            'class' => 'bright'
                        ]) ?>
                        <?php ActiveForm::end(); ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('@frontend/views/common/crop-bg.php')?>

<?php
$countLanguages = count($model->languages);
$countSkills = count($model->skills);
$countSpecialties = count($model->specialties);
$languageUrl = Url::to(['/account/profile-ajax/render-public-language']);
$skillUrl = Url::to(['/account/profile-ajax/render-public-skill']);
$specialtyUrl = Url::to(['/account/profile-ajax/render-public-specialty']);
$specialtyListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$cityListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'city']);
$confirmUrl = Url::to(['/user/registration/send-confirmation']);
$apiKey = Yii::$app->params['googleAPIKey'];
$language = Yii::$app->params['supportedLocales'][Yii::$app->language];
if ($language === 'ua') {
    $language = 'uk';
}
$script = <<<JS
    let languages = $countLanguages;
    let skills = $countSkills;
    let specialties = $countSpecialties;
    
    $(document).on('change','.reason-check input:checkbox',function(){
        if($(this).prop('checked') === false){
            $(this).closest('.reason').find('.reason-options').slideUp();
        } else {
            $(this).prop('checked',true);
            $(this).closest('.reason').find('.reason-options').slideDown();
        }
    });
    $('.reason-check input:checkbox').trigger('change');

    $(document).on('click', '#load-language', function(e) {
        e.preventDefault();
        $.get('$languageUrl',
        {iterator: languages},
        function(data) {
            if(data.success === true) {
                languages++;
                $('#language-container').append(data.html);
            } else {
                alertCall('top', 'error', data.message);
            }
        }, 'json');
    });

    $(document).on('click', '#load-skill', function(e) {
        e.preventDefault();
        $.get('$skillUrl',
        {iterator: skills},
        function(data) {
            if(data.success === true) {
                skills++;
                $('#skill-container').append(data.html);
            } else {
                alertCall('top', 'error', data.message);
            }
        }, 'json');
    });

    $(document).on('click', '#load-specialty', function(e) {
        e.preventDefault();
        $.get('$specialtyUrl',
        {iterator: specialties},
        function(data) {
            if(data.success === true) {
                specialties++;
                $('#specialty-container').append(data.html);
            } else {
                alertCall('top', 'error', data.message);
            }
        }, 'json');
    });

    $(document).on('click', '.del-lang', function() {
        //languages--;
    });
    
    let hasFileUploadError = false;
    $('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
        let response = data.response;
        $('#gravatar-input').val(response.uploadedPath).data('key', response.imageKey);
    }).on('filedeleted', function(event, key) {
        $('#gravatar-input').val(null);
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
             $('#submit-button').trigger('click');
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });
    
    $('#submit-button').on('click', function(e) {
        if ($('#file-upload-input').fileinput('getFilesCount') > 0) {
            $('#file-upload-input').fileinput('upload');
            return false;
        }

        return true;
    });

    $('.selectize-input-container.specialty-selectize select:not(.selectized)').selectize({
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        'load': function(query, callback) {
            if (!query.length)
                return callback();
            $.post('$specialtyListUrl', {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        }
    });

    $(document).on('change', '#freelancer-checkbox', function(){
        if ($(this).prop('checked')) {
            $('.freelancer-block').hide();
            $('.specialties-block').show();
        }
    });
    
    $(document).on('click', '.confirm-method', function() {
        let target = $(this).data('target');
        let type = $(this).data('type');
        $.get('$confirmUrl', {target: target}, function(data){
            if(data.success) {
                $('.confirm-method').hide();
                $('.confirm-block[data-type="' + type + '"]').show();
            }
            alertCall('top', data.messageType, data.message);
        });
        return false;
    });
    
    $(document).on('click', '.translation-toggle', function() {
        $(this).closest('.formgig-block').find('.rightset, .leftset').toggleClass('hidden').find('input, textarea').prop('disabled', false);
        return false;
    });
    
    var googleAutocomplete;
    if (typeof (googleAutocomplete) === 'undefined') {
        $.getScript('https://maps.googleapis.com/maps/api/js?key={$apiKey}&libraries=places&language={$language}', function() {
            initAutocomplete();
        });
    } else {
        initAutocomplete();
    }
    
    function initAutocomplete() {
        googleAutocomplete = new google.maps.places.Autocomplete(
            $("#profile-location-input").get(0),
            {types: ['(cities)']}
        );

        googleAutocomplete.addListener('place_changed', function() {
            // Get the place details from the autocomplete object.
            let place = googleAutocomplete.getPlace();
            let country = '';
            let city = '';
            if (typeof place.address_components !== "undefined") {
                for (let i = 0; i < place.address_components.length; i++) {
                    let addressType = place.address_components[i].types[0];
                    switch (addressType) {
                        case 'locality': 
                            city = place.address_components[i].long_name;
                            break;
                        case 'country': 
                            country = place.address_components[i].long_name;
                            break;
                    }
                }
            }
            $('#profile-lat').val(place.geometry.location.lat());
            $('#profile-long').val(place.geometry.location.lng());
            $('#profile-location-city').val(city);
            $('#profile-location-country').val(country);
        });
    }
    
    $(document).on('click', '#edit-city', function(e) {
        $('#city-form').toggle();
        return false;
    });
JS;

$this->registerJs($script);