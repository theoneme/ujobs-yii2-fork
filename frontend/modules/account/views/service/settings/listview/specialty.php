<?php

use common\models\forms\UserSpecialtyForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var integer $index */
/* @var UserSpecialtyForm $model */

?>

    <div class="lang-com-item">
        <div class="del-lang">
            <i class="fa fa-times-circle" aria-hidden="true"></i>
        </div>
        <div class="input-auto">
            <div id="container-specialty-<?= $index ?>">
                <?= Html::activeTextInput($model, "[{$index}]title", ['id' => "userspecialtyform-{$index}-title"]) ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="input-auto">
            <div id="container-salary-<?= $index ?>">
                <?= Html::activeTextInput($model, "[{$index}]salary", ['placeholder' => Yii::t('account', 'Monthly payment'), 'type' => 'number']) ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="input-auto" style="width:10px; line-height: 30px;">
            руб.
        </div>
    </div>

<?php $specialtyUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$specialtyMessage = json_encode(Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Specialty")]));
$salaryMessage = json_encode(Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Salary")]));

$script = <<<JS
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userspecialtyform-{$index}-title",
        name: "UserSpecialtyForm[{$index}][title]",
        input: "#userspecialtyform-{$index}-title",
        container: "#container-specialty-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: $specialtyMessage,
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userspecialtyform-{$index}-salary",
        name: "UserSpecialtyForm[{$index}][salary]",
        input: "#userspecialtyform-{$index}-salary",
        container: "#container-salary-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: $salaryMessage
            });
        }
    });
    
    new autoComplete({
        selector: "#userspecialtyform-{$index}-title",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$specialtyUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script, View::POS_LOAD);

