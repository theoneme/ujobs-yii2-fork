<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.12.2016
 * Time: 17:39
 */

use yii\helpers\Url;
use yii\helpers\Html;

/* @var integer $index */

?>

<div class="lang-com-item">
    <div class="input-auto">
        <div id="">
            <?= Html::activeDropDownList($model, "[{$index}]level", [
                'Россия' => 'Россия', 'Украина' => 'Украина',
            ]
            ) ?>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="input-auto">
        <div id="">
            <?= Html::activeDropDownList($model, "[{$index}]level", [
                'Донецк' => 'Донецк', 'Токио' => 'Токио',
            ]
            ) ?>
            <div class="help-block"></div>
        </div>
    </div>
</div>


