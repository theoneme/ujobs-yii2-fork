<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.12.2016
 * Time: 17:39
 */

use common\models\forms\UserLanguageForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var integer $index */
/* @var UserLanguageForm $model */

?>

    <div class="lang-com-item">
        <div class="del-lang">
            <i class="fa fa-times-circle" aria-hidden="true"></i>
        </div>
        <div class="input-auto">
            <div id="container-language-<?= $index ?>">
                <?= Html::activeTextInput($model, "[{$index}]title", ['id' => "userlanguageform-{$index}-title"]) ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="input-auto">
            <div id="container-level-<?= $index ?>">
                <?= Html::activeDropDownList($model, "[{$index}]level", [
                    'Unspecified' => Yii::t('account', 'Unspecified'),
                    'Basic' => Yii::t('account', 'Basic'),
                    'Conversational' => Yii::t('account', 'Conversational'),
                    'Fluent' => Yii::t('account', 'Fluent'),
                    'Native' => Yii::t('account', 'Native')
                ], ['class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>
        </div>
    </div>

<?php $languageUrl = Url::to(['/ajax/attribute-list', 'alias' => 'language']);
$languageMessage = json_encode(Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Language")]));
$levelMessage = json_encode(Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Level")]));

$script = <<<JS
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userlanguageform-{$index}-title",
        name: "UserLanguageForm[{$index}][title]",
        input: "#userlanguageform-{$index}-title",
        container: "#container-language-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred) {
            yii.validation.required(value, messages, {
                message: {$languageMessage}
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userlanguageform-{$index}-level",
        name: "UserLanguageForm[{$index}][level]",
        input: "#userlanguageform-{$index}-level",
        container: "#container-level-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred) {
            yii.validation.required(value, messages, {
                message: {$levelMessage}
            });
        }
    });
    
    new autoComplete({
        selector: "#userlanguageform-{$index}-title",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$languageUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script, View::POS_LOAD);

