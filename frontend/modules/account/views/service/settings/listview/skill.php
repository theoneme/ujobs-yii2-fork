<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.01.2017
 * Time: 12:59
 */

use common\models\forms\UserSkillForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;

/* @var integer $index */
/* @var UserSkillForm $model */

?>

    <div class="lang-com-item">
        <div class="del-lang">
            <i class="fa fa-times-circle" aria-hidden="true"></i>
        </div>
        <div class="input-auto">
            <div id="container-skill-<?= $index ?>">
                <?= Html::activeTextInput($model, "[{$index}]title", ['id' => "userskillform-{$index}-title"]) ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="input-auto">
            <div id="container-slevel-<?= $index ?>">
                <?= Html::activeDropDownList($model, "[{$index}]level", [
                    'Beginner' => Yii::t('account', 'Beginner'),
                    'Intermediate' => Yii::t('account', 'Intermediate'),
                    'Expert' => Yii::t('account', 'Expert'),
                ], ['class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>
        </div>
    </div>

<?php $skillUrl = Url::to(['/ajax/attribute-list', 'alias' => 'skill']);
$skillMessage = json_encode(Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Title")]));
$levelMessage = json_encode(Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Level")]));

$script = <<<JS
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userskillform-{$index}-title",
        name: "UserSkillForm[{$index}][title]",
        input: "#userskillform-{$index}-title",
        container: "#container-skill-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: $skillMessage,
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userskillform-{$index}-level",
        name: "UserSkillForm[{$index}][level]",
        input: "#userskillform-{$index}-level",
        container: "#container-slevel-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: $levelMessage
            });
        }
    });
    
    new autoComplete({
        selector: "#userskillform-{$index}-title",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$skillUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script, View::POS_LOAD);