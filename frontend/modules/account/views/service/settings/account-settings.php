<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 18:08
 */

use frontend\modules\account\components\AccountSettingsLeftMenu;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\Profile;
use common\models\user\User;

/* @var Profile $profile */
/* @var User $user */
/* @var mixed $telegramLink */
/* @var string $skypeHash */

?>

<div class="settings greybg">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 col-sm-3 col-xs-12">
				<?= AccountSettingsLeftMenu::widget(['active' => 'account-settings']) ?>
			</div>
			<div class="col-md-7 col-sm-9 col-xs-12">
				<div class="settings-content">
					<h1 class="profileh text-left"><?= Yii::t('account', 'How to contact you') ?></h1>
					<?php $form = ActiveForm::begin([
						'enableAjaxValidation' => true,
						'enableClientValidation' => false,
						'action' => Url::to(['/account/service-ajax/process-account-settings'])
					]) ?>
					<div class="group-set">
                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('public_email-trig', 0) ?>
                                <?= Html::checkbox('public_email-trig', !empty($profile->public_email), [
                                    'id' => 'public_email-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-public_email'
                                ]) ?>
                                <label for="public_email-trig"><?= Yii::t('account', 'Email') ?></label>
                            </div>
                            <div class="row <?= empty($profile->public_email) ? 'hidden' : '' ?>">
                                <?= $form->field($profile, 'public_email', [
                                    'template' => '<div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Email') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your email. We will be able to send you notifications about feedback from users on your service or product. So you to always be aware of the events and you not lose customers.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('phone-trig', 0) ?>
                                <?= Html::checkbox('phone-trig', !empty($profile->phone), [
                                    'id' => 'phone-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-phone'
                                ]) ?>
                                <label for="phone-trig"><?= Yii::t('account', 'Phone') ?></label>
                            </div>
                            <div class="row <?= empty($profile->phone) ? 'hidden' : '' ?>">
                                <?= $form->field($user, 'phone', [
                                    'template' => '<div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Phone') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your phone and we can send you a notification by SMS. It will also facilitate communication with customers.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('skype-trig', 0) ?>
                                <?= Html::checkbox('skype-trig', !empty($profile->skype), [
                                    'id' => 'skype-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-skype'
                                ]) ?>
                                <label for="skype-trig"><?= Yii::t('account', 'Skype') ?></label>
                            </div>
                            <div class="row <?= empty($profile->skype) ? 'hidden' : '' ?>" id="profile-skype">
                                <?= $form->field($profile, 'skype', [
                                    'template' => '
                                        <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Skype') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your {provider} account and we will be able to contact you if main contacts are unavailable', ['provider' => 'Skype']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('whatsapp-trig', 0) ?>
                                <?= Html::checkbox('whatsapp-trig', !empty($profile->whatsapp), [
                                    'id' => 'whatsapp-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-whatsapp'
                                ]) ?>
                                <label for="whatsapp-trig"><?= Yii::t('account', 'WhatsApp') ?></label>
                            </div>
                            <div class="row <?= empty($profile->whatsapp) ? 'hidden' : '' ?>">
                                <?= $form->field($profile, 'whatsapp', [
                                    'template' => '<div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'WhatsApp') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your {provider} account and we will be able to contact you if main contacts are unavailable', ['provider' => 'WhatsApp']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('viber-trig', 0) ?>
                                <?= Html::checkbox('viber-trig', !empty($profile->viber), [
                                    'id' => 'viber-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-viber'
                                ]) ?>
                                <label for="viber-trig"><?= Yii::t('account', 'Viber') ?></label>
                            </div>
                            <div class="row <?= empty($profile->viber) ? 'hidden' : '' ?>">
                                <?= $form->field($profile, 'viber', [
                                    'template' => '<div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Viber') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your {provider} account and we will be able to contact you if main contacts are unavailable', ['provider' => 'Viber']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('telegram-trig', 0) ?>
                                <?= Html::checkbox('telegram-trig', !empty($profile->viber), [
                                    'id' => 'telegram-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-telegram'
                                ]) ?>
                                <label for="telegram-trig"><?= Yii::t('account', 'Telegram') ?></label>
                            </div>
                            <div class="row <?= empty($profile->telegram) ? 'hidden' : '' ?>">
                                <?= $form->field($profile, 'telegram', [
                                    'template' => '<div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Telegram') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your {provider} account and we will be able to contact you if main contacts are unavailable', ['provider' => 'Telegram']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block no-padding expandable-field">
                            <div class="field-trigger">
                                <?= Html::hiddenInput('facebook_messenger-trig', 0) ?>
                                <?= Html::checkbox('facebook_messenger-trig', !empty($profile->facebook_messenger), [
                                    'id' => 'facebook_messenger-trig',
                                    'data-role' => 'field-expander',
                                    'data-target' => '#profile-facebook_messenger'
                                ]) ?>
                                <label for="facebook_messenger-trig"><?= Yii::t('account', 'Facebook Messenger') ?></label>
                            </div>
                            <div class="row <?= empty($profile->facebook_messenger) ? 'hidden' : '' ?>">
                                <?= $form->field($profile, 'facebook_messenger', [
                                    'template' => '<div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">{label}</div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                            {input}{error}
                                        </div>'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('account', 'Facebook Messenger') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('account', 'Write your {provider} account and we will be able to contact you if main contacts are unavailable', ['provider' => 'Facebook Messenger']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
					</div>
                    <?php if (!$user->is_company) { ?>
                        <div class="group-set">
                            <h2 class="profileh text-left"><?= Yii::t('account', 'Notifications Settings') ?></h2>

                            <div class="formgig-block no-padding">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-6 set-title">
                                        <label class="control-label">
                                            <?= Yii::t('account', 'Notify by Email') ?>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-6 set-title">
                                        <div class="chover">
                                            <?= Html::hiddenInput('Profile[email_notifications]', 0) ?>
                                            <?= Html::checkbox('Profile[email_notifications]', $profile->email_notifications, ['id' => 'email_notifications']) ?>
                                            <label for="email_notifications"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Do you want to enable email notifications?') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'Check this item if you want to be notified about the responses on your services, products and requests to your email.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block no-padding">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-6 set-title">
                                        <label class="control-label">
                                            <?= Yii::t('account', 'Notify by SMS') ?>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-6 set-title">
                                        <div class="chover">
                                            <?= Html::hiddenInput('Profile[sms_notifications]', 0) ?>
                                            <?= Html::checkbox('Profile[sms_notifications]', $profile->sms_notifications, ['id' => 'sms_notifications']) ?>
                                            <label for="sms_notifications"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Do you want to enable SMS notifications?') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'Check this item if you want to be notified about the responses on your services, products and requests to your phone.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="group-set">
                            <h2 class="profileh text-left"><?= Yii::t('account', 'Additional notifications') ?></h2>

                            <div class="formgig-block no-padding">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-6 set-title">
                                        <label class="control-label">
                                            <?= Yii::t('account', 'Notify in telegram') ?>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-6 set-title">
                                        <?= $telegramLink ?>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Do you want to receive notifications in the telegram?') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'Check this item if you want to be notified about the responses on your services, products and requests in telegram.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block no-padding">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-6 set-title">
                                        <label class="control-label">
                                            <?= Yii::t('account', 'Notify in browsers') ?>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-6 set-title">
                                        <a class="js-push-button"><?= Yii::t('account', 'Subscribe') ?></a>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Do you want to receive notifications in the browser?') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'Check this item if you want to be notified about the responses on your services, products and requests in the browser.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
<!--                            <div class="formgig-block no-padding">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-4 col-sm-4 col-xs-6 set-title">-->
<!--                                        <label class="control-label">-->
<!--                                            --><?//= Yii::t('account', 'Notify in skype') ?>
<!--                                        </label>-->
<!--                                    </div>-->
<!--                                    --><?php //if($skypeHash !== null) { ?>
<!--                                        <div class="col-md-3 col-sm-3 col-xs-4 set-title">-->
<!--                                            <a href="https://join.skype.com/bot/e44c207a-078b-4659-91e5-0756bd7a20dd">--><?//= Yii::t('account', 'Subscribe') ?><!--</a>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-5 col-sm-3 col-xs-4 set-title" style="word-break: break-all">-->
<!--                                            --><?//= Yii::t('account', 'Send this code to our Skype bot: <br> {code}', ['code' => $skypeHash]) ?>
<!--                                        </div>-->
<!--                                    --><?php //} else { ?>
<!--                                        <div class="col-md-4 col-sm-4 col-xs-4 set-title">-->
<!--                                            <a href="--><?//= Url::to(['/skype/default/unsubscribe', 'code' => $user->skype_uid])?><!-- ">--><?//= Yii::t('account', 'Unsubscribe') ?><!--</a>-->
<!--                                        </div>-->
<!--                                    --><?php //} ?>
<!--                                </div>-->
<!--                                <div class="notes col-md-6 col-md-offset-12">-->
<!--                                    <div class="notes-head">-->
<!--                                        <div class="lamp text-center">-->
<!--                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>-->
<!--                                        </div>-->
<!--                                        <div class="notes-title">--><?//= Yii::t('account', 'Do you want to receive notifications in the skype?') ?><!--</div>-->
<!--                                    </div>-->
<!--                                    <div class="notes-descr">-->
<!--                                        <p>-->
<!--                                            --><?//= Yii::t('account', 'Check this item and send code to our bot in skype if you want to be notified about the responses on your services, products and requests in skype.') ?>
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    <?php } ?>
					<div class="row set-item"></div>
					<?= Html::submitInput(Yii::t('account', 'Save Changes'), ['class' => 'bright']) ?>
					<?php ActiveForm::end(); ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $script = <<<JS
    $('[data-role=field-expander]').on('change', function() {
         $(this).closest('.field-trigger').next('.row').toggleClass('hidden');
         if($(this).is(':checked') === false) {
             $(this).closest('.field-trigger').next('.row').find('input').val('');
         }
    });
JS;

$this->registerJs($script);