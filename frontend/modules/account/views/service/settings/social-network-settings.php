<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 18:08
 */

use frontend\modules\account\components\AccountSettingsLeftMenu;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\Profile;
use common\models\user\User;
use dektrium\user\widgets\Connect;

/* @var Profile $profile */
/* @var User $user */

?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'social-network-settings']) ?>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left"><?= Yii::t('account', 'Social Network') ?></h1>
                    <?php $auth = Connect::begin([
                        'baseAuthUrl' => ['/user/security/auth'],
                        'accounts' => $profile->user->accounts,
                        'autoRender' => false,
                        'popupMode' => false,
                    ]) ?>
                    <?php foreach ($auth->getClients() as $client): ?>
                    <div class="formgig-block">
                        <div class="row set-item">
                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                <div class="set-title"><?= $client->getTitle() ?></div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                <?= $auth->isConnected($client) ?
                                    Html::a('<i class="fa fa-' . $client->getName() . '" aria-hidden="true"></i>&nbsp;&nbsp;' . Yii::t('account', 'Disconnect from {network}', ['network' => $client->getTitle()]),
                                        $auth->createClientUrl($client), [
                                            'class' => 'grey-but text-center red',
                                            'data-method' => 'post'
                                        ])
                                    :
                                    Html::a('<i class="fa fa-' . $client->getName() . '" aria-hidden="true"></i>&nbsp;&nbsp;' . Yii::t('account', 'Connect to {network}', ['network' => $client->getTitle()]),
                                        $auth->createClientUrl($client), [
                                            'class' => 'grey-but text-center',
                                        ])
                                ?>
                            </div>
                        </div>
                        <div class="notes col-md-6 col-md-offset-12">
                            <div class="notes-head">
                                <div class="lamp text-center">
                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                </div>
                                <div class="notes-title"><?= $client->getTitle() ?></div>
                            </div>
                            <div class="notes-descr">
                                <p>
                                    <?= $auth->isConnected($client) ?
                                        Yii::t('account', 'Detach {network} account from this profile. You will not be able to log in quickly through the selected social network.', ['network' => $client->getTitle()])
                                        :
                                        Yii::t('account', 'Connect on the {network} for a quick entrance through this social network', ['network' => $client->getTitle()])
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php Connect::end() ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>