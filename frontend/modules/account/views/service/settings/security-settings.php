<?php
use frontend\modules\account\components\AccountSettingsLeftMenu;

?>

<div class="settings greybg">
    <div class="container-fluid2">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'security-settings']) ?>
            </div>
            <div class="col-md-10 col-sm-9 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left"><?=Yii::t('account', 'Security Settings')?></h1>
                    <div class="start">
                        <div class="row set-item">
                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                <div class="set-title"><?=Yii::t('account', 'SECURITY QUESTION')?></div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 rightset">
                                <p>
                                    <?=Yii::t('account', 'Creating a security question, you increase the level of protection for the withdrawal of funds or changing the password.')?>
                                </p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 rightset">
                            <!--УБРАЛ У КНОПКИ КЛАСС to-edit, КОГДА БУДЕТ ЭТОТ ФУНКЦИОНАЛ ГОТОВ - ДОБАВИТЬ КЛАСС ОБРАТНО-->
                                <a class="btn-small brighr" href="#">
                                    <?=Yii::t('account', 'Change')?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <form class="edit">
                        <div class="row set-item">
                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">ANSWER SECURITY QUESTION</div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                <p>
                                    WHAT WAS THE NAME OF YOUR FIRST PET?
                                </p>
                                <input type="text" name="curquest" placeholder="Your Answer">
                            </div>
                        </div>
                        <div class="row set-item">
                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">SET SECURITY QUESTION</div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8 rightset reason-sel">
                                <select>
                                    <option value="school">What was the name of your elementary school?</option>
                                    <option value="pet">What was the name of your first pet?</option>
                                    <option value="nickname">What was your childhood nickname?</option>
                                    <option value="parent">In what city did your parents meet?</option>
                                    <option value="friend"> What is the name of your favorite childhood friend?</option>
                                </select>
                                <input type="text" name="answer" placeholder="Some Answer">
                            </div>
                        </div>
                        <div class="row set-item">
                            <div class="col-md-12 col-sm-12 col-xs-12 sec-alias">
                                <a class="to-start" href="">Cancel</a>
                                <a class="to-remove" href="">Remove</a>
                                <a class="to-forgot" href="" data-toggle="modal" data-target="#forgot">I Forgot</a>
                                <input class="btn-small"
                                       type="submit" value="Submit">
                            </div>
                        </div>
                    </form>
                    <form class="remove">
                        <div class="row set-item">
                            <div class="col-md-4 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">ANSWER SECURITY QUESTION</div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-8 rightset">
                                <p>
                                    WHAT WAS THE NAME OF YOUR FIRST PET?
                                </p>
                                <input type="text" name="curquest" placeholder="Your Answer">
                            </div>
                        </div>
                        <div class="row set-item">
                            <div class="col-md-12 col-sm-12 col-xs-12 sec-alias">
                                <a class="to-start" href="">Cancel</a>
                                <a class="to-forgot" href="" data-toggle="modal" data-target="#forgot">I Forgot</a>
                                <input class="btn-small" type="submit" value="Remove">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="form-title">FORGOT YOUR ANSWER?</div>
            </div>
            <div class="modal-body">
                In case you forgot your answer, please contact our <a href="">customer support</a> for account verification.
            </div>
        </div>
    </div>
</div>