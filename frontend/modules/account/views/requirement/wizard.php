<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.01.2018
 * Time: 16:18
 */
use frontend\modules\account\models\history\WizardDecisionForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $model WizardDecisionForm
 */

\frontend\assets\AccountAsset::register($this);

$this->title = Yii::t('order', 'Fill requirements');
?>
    <div class="grey-steps hidden-xs">
        <div class="container-fluid">
            <ul class="gig-steps cart-steps">
                <?php foreach ($model->steps as $key => $step) { ?>
                    <li class="<?= $key === 0 ? 'active' : '' ?>">
                        <?= Html::a($step['title'], '#') ?>
                    </li>
                <?php } ?>
                <li>
                    <?= Html::a(Yii::t('order', 'Finish'), '#') ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid" style="margin-top: 10px">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'data-step' => 1,
                        'data-final-step' => count($model->steps) + 1
                    ],
                    'id' => 'requirement-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnChange' => false,
                    'validateOnBlur' => false,
                ]); ?>
                <?= Html::hiddenInput('id', $model->order_id, ['id' => 'order-id']) ?>
                <div class="tab-content nostyle-tab-content">
                    <?php foreach ($model->steps as $key => $step) { ?>
                        <div class="settings-content step tab-pane fade in <?= $key === 0 ? 'active' : '' ?>" id="step<?= $key + 1?>"
                             data-step="<?= $key + 1?>">
                            <div>
                                <div class="profileh"><?= $step['title'] ?></div>
                            </div>
                            <?php foreach ($step['requirements'] as $reqKey => $requirement) { ?>
                                <div class="formgig-block">
                                    <div class="row">
                                        <?= $form->field($model, $reqKey, [
                                            'template' => '
                                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                                        <div class="set-title">
                                                            {label}
                                                            <div class="mobile-question" data-toggle="modal"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                                        {input}
                                                        {error}
                                                    </div>'
                                        ])->textInput()
                                        ?>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title">PH</div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                PH
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('app', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="settings-content tab-pane fade in" id="step<?= count($model->steps) + 1?>" data-step="<?= count($model->steps) + 1?>">
                        <div class="publish-title text-center">
                            <?= Yii::t('app', 'It\'s almost ready...') ?>
                        </div>
                        <div class="publish-text text-center">
                            <?= Yii::t('order', "Click continue to finish this wizard and send answers to seller.") ?>
                        </div>
                        <div class="create-gig-block">
                            <div>
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Continue'), ['class' => 'btn-mid fright']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php $userId = Yii::$app->user->identity->getId();
$ajaxCreateRoute = Url::toRoute('/job/ajax-create');

$script = <<<JS
    $('body').on('click','.mobile-question',function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click','.mobile-note-btn a',function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });

	let uid = $userId;

	$("#requirement-form").on("beforeValidateAttribute", function (event, attribute, messages) {
		let current_step = $(this).data("step"),
		    input_step = $(attribute.container).closest(".step").data("step");
		if (current_step !== input_step) {
			return false;
		}
	}).on("afterValidate", function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
			let current_step = $(this).data("step");

            $("html, body").animate({
                scrollTop: $(this).find("div[data-step=\'" + current_step + "\'] .has-error").first().offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function (event) {
		let current_step = $(this).data("step"),
		    self = $(this);

		if (current_step !== $(this).data("final-step")) {
            self.find("div[data-step=\'" + current_step + "\']").removeClass("active");
            $(".gig-steps li").removeClass("active");
            current_step++;
            self.data("step", current_step);
            self.find("div[data-step=\'" + current_step + "\']").addClass("active");
            $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            
            return false;
		}
	});

    $(".prev-step").on("click", function() {
    	let requirementForm = $("#requirement-form"),
		    current_step = requirementForm.data("step");

		if (current_step > 1) {
			requirementForm.find("div[data-step=\'" + current_step + "\']").removeClass("active");
			$(".gig-steps li").removeClass("active");
			current_step--;
			requirementForm.data("step", current_step);
			requirementForm.find("div[data-step=\'" + current_step + "\']").addClass("active");
			$(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
			$("html, body").animate({
				scrollTop: 0
			}, "slow");
		}
		return false;
    });
JS;

$this->registerJs($script);