<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.05.2017
 * Time: 11:57
 */

use yii\helpers\Html;
use common\modules\store\models\Order;
use yii\web\View;

/* @var Order $model
 * @var View $this
 */
$baseUrl = in_array($model->product_type, [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER]) ? '/account/order/tender-history' : '/account/order/history';
?>

<div class="jm-option">
    <div class="jm-img">
        <?= $model->product ? Html::a(Html::img($model->product->getThumb('catalog')), [$baseUrl, 'id' => $model->id], ['data-pjax' => 0]) : '' ?>
    </div>
    <div class="jm-name">
        <?= Html::a($model->product ? $model->product->getLabel() : '<span style="color: red">' . Yii::t('account', 'Request not found') . '</span>', [$baseUrl, 'id' => $model->id], ['data-pjax' => 0])?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Order Date')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->created_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Updated At')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->updated_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Total')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getTotal() ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Status')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getStatusLabel(true) ?>
    </div>
</div>
