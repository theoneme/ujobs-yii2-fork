<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.01.2017
 * Time: 19:03
 */

use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\web\View;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('account', 'Current orders');

/* @var mixed $tabHeader
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 */

?>

<div class="inbox-background">
    <div class="profile">
        <div class="container-fluid">
            <?php Pjax::begin([
                'id' => 'sales-pjax',
                'scrollTo' => false
            ]); ?>
            <form class="search-gigs text-right">
                <input type="search" placeholder="<?= Yii::t('account', 'Search My History...') ?>" name="search">
                <a class="btn-search" onclick="$(this).closest('form').submit();"></a>
            </form>
            <h1 class="profileh text-left"><?= Yii::t('account', 'Current orders') ?></h1>

            <div class="inbox row">
                <div class="col-md-12">
                    <nav class="nav-jobs-table">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profa-menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="clearfix"></div>
                        <div id="profa-menu" class="collapse navbar-collapse">
                            <ul class="nav nav-tabs dashboards-tabs">
                                <?= $tabHeader ?>
                            </ul>
                        </div>
                    </nav>
                    <div class="all-messages allgigs-tabs hidden-xs">
                        <div class="over-table-viewgigs">
                            <div class="head-viewgigs row">
                                <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                    <a class="grey-but text-center del-gigs delete-messages" data-pjax="0">
                                        <?= mb_strtoupper(Yii::t('account', 'Delete')) ?>
                                    </a>
                                </div>
                            </div>
                            <table class="table-viewgigs">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Order Date')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Updated At')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Total')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Status')) ?>
                                    </td>
                                </tr>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => 'listview/order-item',
                                    'options' => [
                                        'tag' => false,
                                        'id' => 'messages-list'
                                    ],
                                    'itemOptions' => [
                                        'tag' => 'tr',
                                        'class' => 'messages-row'
                                    ],
                                    'emptyTextOptions' => [
                                        'tag' => 'tr',
                                        'class' => 'messages-row'
                                    ],
                                    'emptyText' => "<td colspan='6'>" . Yii::t('yii', 'No results found.') . "</td>",
                                    'layout' => "{items}"
                                ]) ?>
                            </table>
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                            ]) ?>
                        </div>
                    </div>
                    <div class="jobs-table-mobile visible-xs">
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'listview/order-item-mobile',
                            'options' => [
                                'tag' => false,
                                'id' => 'messages-list'
                            ],
                            'itemOptions' => [
                                'tag' => 'div',
                                'class' => 'jobMobile-item'
                            ],
                            'emptyTextOptions' => [
                                'tag' => 'div',
                                'class' => 'jobMobile-item'
                            ],
                            'emptyText' => "<div class='imobile-empty'>" . Yii::t('yii', 'No results found.') . "</div>",
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $dataProvider->pagination,
                        ]) ?>
                    </div>
                </div>
            </div>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>