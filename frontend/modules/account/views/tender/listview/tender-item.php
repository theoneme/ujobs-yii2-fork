<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.12.2016
 * Time: 18:52
 */

use yii\helpers\Html;
use common\models\Job;
use yii\helpers\Url;
use yii\web\View;

/* @var Job $model
 * @var View $this
 */

?>
<td>
    <?= Html::a(Html::img($model->getThumb('catalog'), ['alt' => $model->translation->title,'title' => $model->translation->title]),
        ['/tender/view', 'alias' => $model->alias],
        ['class' => 'tg-img', 'data-pjax' => 0]) ?>
</td>
<td>
    <?= Html::a($model->translation->title,
        ['/tender/view', 'alias' => $model->alias],
        ['class' => 'tg-name', 'data-pjax'=> 0]) ?>
</td>
<td><?= $model->click_count ?></td>
<td><?= $model->views_count ?></td>
<td><?= $model->orders_count ?></td>
<td><?= $model->cancellations_count ?></td>
<td>
    <div class="tg-opt text-left">
        <div class="tg-optbut text-center">
            <i class="fa fa-caret-down"></i>
        </div>
        <div class="tg-opt-block">
            <a data-pjax="0" href="<?=Url::to(['/tender/update', 'id' => $model->id])?>"><?=Yii::t('account', 'Edit')?></a>
            <?php if($model->status == Job::STATUS_ACTIVE) { ?>
                <a data-pjax="0" href="<?=Url::to(['/tender/pause', 'id' => $model->id])?>"><?=Yii::t('account', 'Pause')?></a>
            <?php } ?>
            <?php if($model->status == Job::STATUS_PAUSED) { ?>
                <a data-pjax="0" href="<?=Url::to(['/tender/resume', 'id' => $model->id])?>"><?=Yii::t('account', 'Resume')?></a>
            <?php } ?>
            <a data-pjax="0" data-method="post" href="<?=Url::to(['/tender/delete', 'id' => $model->id])?>" data-confirm="<?=Yii::t('account', 'Are you sure?')?>" class="gig-del"><?=Yii::t('account', 'Delete')?></a>
        </div>
    </div>
</td>