<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.12.2016
 * Time: 18:31
 */

use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\web\View;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('account', 'Manage Jobs');

/* @var mixed $tabHeader
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 */

?>

<div class="inbox-background">
	<div class="profile">
		<div class="container-fluid">
			<?php Pjax::begin([
				'id' => 'jobs-pjax',
				'scrollTo' => false
			]); ?>
			<form class="search-gigs text-right">
				<input type="search" placeholder="<?= Yii::t('account', 'Search My History...') ?>" name="search">
				<a class="btn-search" onclick="$(this).closest('form').submit();"></a>
			</form>
			<h1 class="profileh text-left"><?= Yii::t('account', 'Manage Jobs') ?></h1>

			<div class="inbox row">
                <div class="col-md-12">
                    <nav class="nav-jobs-table">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profa-menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="clearfix"></div>
                        <div id="profa-menu" class="collapse navbar-collapse">
                            <ul class="nav nav-tabs dashboards-tabs">
                                <?= $tabHeader ?>
                            </ul>
                        </div>
                    </nav>
                    <div class="all-messages allgigs-tabs hidden-xs">
                        <div class="over-table-viewgigs">
                            <div class="head-viewgigs row">
                                <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                    <a class="grey-but text-center del-gigs delete-messages" data-pjax="0">
                                        <?= mb_strtoupper(Yii::t('account', 'Delete')) ?>
                                    </a>
                                </div>
                            </div>
                            <table class="table-viewgigs">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Clicks')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Views')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Language')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Publish date')) ?>
                                    </td>
                                    <td></td>
                                </tr>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => 'listview/job-item',
                                    'options' => [
                                        'tag' => false,
                                        'id' => 'job-list'
                                    ],
                                    'itemOptions' => [
                                        'tag' => 'tr',
                                        'class' => 'messages-row'
                                    ],
                                    'emptyTextOptions' => [
                                        'tag' => 'tr',
                                        'class' => 'messages-row'
                                    ],
                                    'emptyText' => "<td colspan='7'>" . Yii::t('yii', 'No results found.') . "</td>",
                                    'layout' => "{items}"
                                ]) ?>
                            </table>
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                            ]) ?>
                        </div>
                    </div>
                    <div class="jobs-table-mobile jm-edit visible-xs">
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'listview/job-item-mobile',
                            'options' => [
                                'tag' => false,
                                'id' => 'job-list'
                            ],
                            'itemOptions' => [
                                'tag' => 'div',
                                'class' => 'jobMobile-item'
                            ],
                            'emptyTextOptions' => [
                                'tag' => 'div',
                                'class' => 'jobMobile-item'
                            ],
                            'emptyText' => "<div class='imobile-empty'>" . Yii::t('yii', 'No results found.') . "</div>",
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $dataProvider->pagination,
                        ]) ?>
                    </div>
                </div>
			</div>
			<?php Pjax::end() ?>
		</div>
	</div>
</div>