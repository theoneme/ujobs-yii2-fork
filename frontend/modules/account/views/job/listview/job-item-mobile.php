<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.05.2017
 * Time: 9:52
 */
use yii\helpers\Html;
use common\models\Job;
use yii\helpers\Url;
use yii\web\View;
/* @var Job $model
 * @var View $this
 */
?>
    <div class="jm-option">
        <div class="jm-img">
            <?= Html::a(Html::img($model->getThumb('catalog')),
                ['/job/view', 'alias' => $model->alias])?>
        </div>
        <div class="jm-name">
            <?= Html::a($model->translation->title, [
                '/job/view', 'alias' => $model->alias
            ],[
                'data-pjax' => 0
            ]) ?>
            <div class="tg-opt text-left">
                <div class="tg-optbut text-center">
                    <i class="fa fa-caret-down"></i>
                </div>
                <div class="tg-opt-block">
                    <a data-pjax="0" href="<?=Url::to(['/job/update', 'id' => $model->id])?>"><?=Yii::t('account', 'Edit')?></a>
                    <a data-method="post" data-pjax="0" href="<?=Url::to(['/job/delete', 'id' => $model->id])?>" data-confirm="<?=Yii::t('account', 'Are you sure?')?>" class="gig-del"><?=Yii::t('account', 'Delete')?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="jm-option">
        <div class="jm-optionName">
            <?= mb_strtoupper(Yii::t('account', 'Clicks')) ?>
        </div>
        <div class="jm-optionParam">
            <?= $model->click_count ?>
        </div>
    </div>
    <div class="jm-option">
        <div class="jm-optionName">
            <?= mb_strtoupper(Yii::t('account', 'Views')) ?>
        </div>
        <div class="jm-optionParam">
            <?= $model->views_count ?>
        </div>
    </div>
    <div class="jm-option">
        <div class="jm-optionName">
            <?= mb_strtoupper(Yii::t('account', 'Language')) ?>
        </div>
        <div class="jm-optionParam">
            <?= Yii::t('app', Yii::$app->params['languages'][$model->locale]) ?>
        </div>
    </div>
    <div class="jm-option">
        <div class="jm-optionName">
            <?= mb_strtoupper(Yii::t('account', 'Publish date')) ?>
        </div>
        <div class="jm-optionParam">
            <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
        </div>
    </div>