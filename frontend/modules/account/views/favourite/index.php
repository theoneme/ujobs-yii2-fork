<?php

use frontend\assets\StartSellingAsset;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var ActiveDataProvider $similarDataProvider
 */

$this->title = Yii::t('account', 'Favourites');
StartSellingAsset::register($this);

?>
    <div class="profile greybg">
        <div class="container-fluid2">
            <div class="collection-header">
                <h1 class="profileh text-left">
                    <?= Yii::t('account', 'Jobs I Like') ?>
                    <span class="colh-count">
                    (<?= $dataProvider->totalCount . ' ' . Yii::t('account', '{count, plural, one{Job} few{Jobs} many{Jobs} other{Jobs}}', ['count' => $dataProvider->totalCount]) ?>)
                </span>
                </h1>
                <p>
                    <?= Yii::t('account', 'Welcome to your favorites collection. If you add the work to your favorites, it will appear here.') ?>
                </p>
                <!--            --><?php //= Html::a('<i class="fa fa-shopping-cart" aria-hidden="true"></i>' . Yii::t('account', 'Add Collection To Cart'), null, [
                //                'class' => 'grey-but text-center',
                //                'style' => 'padding-left: 36px; text-align: left; width: 250px;'
                //            ])?>
            </div>
            <!--        <ul class="nav nav-tabs tab-gigs">-->
            <!--            <li class="active"><a href="#tab1" data-toggle="tab">All Gig</a></li>-->
            <!--            <li><a href="#tab2" data-toggle="tab">Active Gigs</a></li>-->
            <!--        </ul>-->
            <div class="fav-page">
                <div class="" id="tab1">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '@frontend/modules/filter/views/job-list',
                        'options' => [
                            'class' => 'block-flex',
                            'id' => 'favorites-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'bf-item'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It looks like you haven't added any job to favourite yet") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?php if ($similarDataProvider->totalCount) { ?>
    <div class="work-sliders">
        <div class="ws-block container-fluid-work">
            <div class="ws-title"><?= Yii::t('account', 'SIMILAR TO JOBS IN FAVOURITES') ?></div>
            <?= ListView::widget([
                'dataProvider' => $similarDataProvider,
                'itemView' => '@frontend/components/job/views/listview/proposition-item',
                'options' => [
                    'class' => 'ws-content',
                    'id' => 'collected'
                ],
                'itemOptions' => [
                    'class' => 'slideitem-over'
                ],
                'emptyText' => '',
                'layout' => "{items}"
            ]) ?>
        </div>
    </div>
<?php } ?>

<?php $this->registerJs("
var slickDir = false;
if($('body').hasClass('rtl')){
    slickDir = true;
}
    $('#collected').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        focusOnSelect: false,
        rtl: slickDir,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
");