<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.11.2016
 * Time: 14:00
 */

/* @var integer $key */
/* @var string $route */
/* @var string $formid */
/* @var UserEducation $model */

use common\models\UserEducation;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => $route,
    'options' => [
        'class' => 'form-eform education-form visible-all',
        'id' => "education-form_{$formid}",
        'data-pjax-container' => 'education-pjax'
    ]
]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'country_title')->dropDownList([
                'Ukraine' => Yii::t('app', 'Ukraine'),
                'Russia' => Yii::t('app', 'Russia')
            ], ['prompt' => '-- ' . Yii::t('account', 'Country')])->label(false) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('account', 'University Title')])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-6 width480">
            <?= $form->field($model, 'degree')->textInput(['placeholder' => Yii::t('account', 'Degree')])->label(false) ?>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-6 width480">
            <?= $form->field($model, 'specialization')->textInput(['placeholder' => Yii::t('account', 'Specialization')])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'year_start')->dropDownList(array_combine(range(date('Y'), 1950), range(date('Y'), 1950)), [
                'prompt' => '-- ' . Yii::t('account', 'Year Start')
            ])->label(false) ?>
        </div>
        <!--<div class="col-md-1 col-sm-1 col-xs-2 fromto">
            &#8212;
        </div>-->
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'year_end')->dropDownList(array_combine(range(date('Y'), 1950), range(date('Y'), 1950)), [
                'prompt' => '-- ' . Yii::t('account', 'Year End')
            ])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-right">
            <?= Html::a(Yii::t('account', 'Cancel'), '#', [
                'class' => 'grey-but text-center grey-small closeeditt',
                'data-target' => "education-form_{$formid}",
                'data-key' => $key,
                'data-list' => 'education-listview',
                'data-entity' => 'educations',
            ]) ?>
            <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>