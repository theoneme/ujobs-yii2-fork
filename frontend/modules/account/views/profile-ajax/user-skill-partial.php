<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 18:47
 */

/* @var integer $key */
/* @var string $route */
/* @var string $formid */
/* @var UserSkillForm $model */

use common\models\forms\UserSkillForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => $route,
    'options' => [
        'class' => 'form-eform skill-form visible-all',
        'id' => "skill-form_{$formid}",
        'data-pjax-container' => 'skill-pjax'
    ]
]) ?>
<div>
    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
    <div class="input-auto autocomplete">
        <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('account', 'Skill'), 'id' => "skill_input_{$formid}"])->label(false) ?>
    </div>
    <?= $form->field($model, 'level')->dropDownList([
        'Beginner' => Yii::t('account', 'Beginner'),
        'Intermediate' => Yii::t('account', 'Intermediate'),
        'Expert' => Yii::t('account', 'Expert'),
    ], ['prompt' => '-- ' . Yii::t('account', 'Level of experience')])->label(false); ?>
</div>
<div>
    <div class="text-right">
        <?= Html::a(Yii::t('account', 'Cancel'), '#', [
            'class' => 'grey-but text-center grey-small closeeditt',
            'data-target' => "skill-form_{$formid}",
            'data-key' => $key,
            'data-list' => 'skill-listview',
            'data-entity' => 'skills',
        ]) ?>
        <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$skillSearchUrl = Url::to(['/ajax/attribute-list', 'alias' => 'skill']);
$script = <<<JS
    new autoComplete({
        selector: "#skill_input_{$formid}",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$skillSearchUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script); ?>
