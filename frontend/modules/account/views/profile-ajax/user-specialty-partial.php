<?php

use common\models\forms\UserSpecialtyForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var integer $key */
/* @var string $route */
/* @var UserSpecialtyForm $model */
/* @var integer $formid */

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => $route,
    'options' => [
        'class' => 'form-eform specialty-form visible-all',
        'id' => "specialty-form_{$formid}",
        'data-pjax-container' => 'specialty-pjax'
    ]
]) ?>
<div>
    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
    <div class="text-left">
        <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('account', 'Skill'), 'id' => "specialty_input_{$formid}"])->label(false) ?>
    </div>
    <?= $form->field($model, 'salary')->textInput(['placeholder' => Yii::t('account', 'Monthly payment'), 'type' => 'number'])->label(false) ?>
</div>
<div>
    <div class="text-right">
        <?= Html::a(Yii::t('account', 'Cancel'), '#', [
            'class' => 'grey-but text-center grey-small closeeditt',
            'data-target' => "specialty-form_{$formid}",
            'data-key' => $key,
            'data-list' => 'specialty-listview',
            'data-entity' => 'specialties',
        ]) ?>
        <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$specialtyListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$script = <<<JS
    new autoComplete({
        selector: "#specialty_input_{$formid}",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$specialtyListUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script); ?>
