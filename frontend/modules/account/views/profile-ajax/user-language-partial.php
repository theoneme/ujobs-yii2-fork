<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 13:38
 */

/* @var integer $formid */
/* @var integer $key */
/* @var string $route */
/* @var UserLanguageForm $model */

use common\models\forms\UserLanguageForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => $route,
    'options' => [
        'class' => 'form-eform lang-form visible-all',
        'id' => "lang-form_{$formid}",
        'data-pjax-container' => 'language-pjax'
    ]
]) ?>
<div>
    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
    <div class="input-auto">
        <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('account', 'Language'), 'id' => "language_input_{$formid}"])->label(false) ?>
    </div>
    <div>
        <?= $form->field($model, 'level')->dropDownList([
            'Basic' => Yii::t('account', 'Basic'),
            'Conversational' => Yii::t('account', 'Conversational'),
            'Fluent' => Yii::t('account', 'Fluent'),
            'Native' => Yii::t('account', 'Native')
        ], ['prompt' => '-- ' . Yii::t('account', 'Proficiency')])->label(false); ?>
    </div>
</div>
<div>
    <div class="text-right">
        <?= Html::a(Yii::t('account', 'Cancel'), '#', [
            'class' => 'grey-but text-center grey-small closeeditt',
            'data-target' => "lang-form_{$formid}",
            'data-key' => $key,
            'data-list' => 'language-listview',
            'data-entity' => 'languages',
        ]) ?>
        <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$languageSearchUrl = Url::to(['/ajax/attribute-list', 'alias' => 'language']);
$script = <<<JS
    new autoComplete({
        selector: "#language_input_{$formid}",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$languageSearchUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script); ?>
