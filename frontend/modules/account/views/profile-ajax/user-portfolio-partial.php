<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.11.2016
 * Time: 16:45
 */

/* @var integer $key */
/* @var string $route */
/* @var string $formid */
/* @var UserPortfolio $model */

use common\models\UserPortfolio;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => $route,
    'options' => [
        'class' => 'form-eform portfolio-form visible-all',
        'id' => "portfolio-form_{$formid}"
    ]
]) ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('account', 'Description')])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'description')->textInput(['placeholder' => Yii::t('account', 'e.g. http://MyPortfolio.com')])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-right">
            <?= Html::a(Yii::t('account', 'Cancel'), '#', [
                'class' => 'grey-but text-center grey-small closeeditt',
                'data-target' => "portfolio-form_{$formid}",
                'data-key' => $key,
                'data-entity' => 'portfolios',
                'data-list' => 'portfolio-listview'
            ]) ?>
            <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>