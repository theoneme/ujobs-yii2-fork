<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 19:26
 */

/* @var integer $key */
/* @var string $route */
/* @var string $formid */
/* @var UserCertificate $model */

use common\models\UserCertificate;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => $route,
    'options' => [
        'class' => 'form-eform certificate-form visible-all',
        'id' => "certificate-form_{$formid}",
        'data-pjax-container' => 'certificate-pjax'
    ]
]) ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('account', 'Certificate Or Award')])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 width480">
            <?= $form->field($model, 'description')->textInput(['placeholder' => Yii::t('account', 'Certified From (E.G. Adobe)')])->label(false) ?>
        </div>
        <div class="col-md-6 width480">
            <?= $form->field($model, 'year')->dropDownList(array_combine(range(date('Y'), 1950), range(date('Y'), 1950)), [
                'prompt' => '-- ' . Yii::t('account', 'Year')
            ])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-right">
            <?= Html::a(Yii::t('account', 'Cancel'), '#', [
                'class' => 'grey-but text-center grey-small closeeditt',
                'data-entity' => 'certificates',
                'data-target' => "certificate-form_{$formid}",
                'data-key' => $key,
                'data-list' => 'certificate-listview'
            ]) ?>
            <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>