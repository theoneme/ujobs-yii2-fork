<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.09.2017
 * Time: 14:15
 */

use common\models\Job;
use common\models\UserPortfolio;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $model UserPortfolio */
/* @var $index integer */
/* @var $metaKeywords string */
/* @var $editable boolean */

$category = $model->category;
$route = $category->type === 'product_category'
    ? ['/category/jobs', 'category_1' => $category->translation->slug]
    : ['/board/category/products', 'category_1' => $category->translation->slug]

?>
<div class="pg-over">
    <div class="block-top">
        <div class="block-top-ava">
            <?= Html::img($model->getSellerThumb('catalog'),['alt' => $model->getSellerName(), 'title' => $model->getSellerName()]) ?>
        </div>
        <div class="bt-name-rate">
            <div class="by">
                <?= Yii::t('app', 'by') ?>
                <?= Html::a(Html::encode(StringHelper::truncateWords($model->getSellerName(), 1)), $model->getSellerUrl(), ['data-pjax' => 0]) ?>
            </div>
        </div>
    </div>
    <div class="bf-alias">
        <?= Html::a(
            Html::img($model->getThumb('catalog'),['alt' => $model->title, 'title' => $model->title]),
            ['/account/portfolio/view', 'id' => $model->id],
            ['class' => 'bf-img album-show', 'data-pjax' => 0]
        )?>
        <div class=" bf-descr">
            <a class="album-title" href="#"><?= $model->title ?></a>
        </div>
        <div class="cat-related">
            <?= Html::a($model->category->translation->title, $route) ?>
        </div>
    </div>
    <div class="block-bottom">
        <div class="album-opt like-over">
            <i class="fa fa-thumbs-up"></i><span><?= count($model->likes) ?></span>
            <?php if (count($model->likes) > 0) { ?>
                <div class="people-likes">
                    <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                    <?php foreach ($model->likeUsers as $likeProfile) { ?>
                        <a href="<?= Url::to(['/account/profile/show', 'id' => $likeProfile->user_id]) ?>">
                            <img src="<?= $likeProfile->getThumb('catalog') ?>"
                                 alt="<?= $likeProfile->getSellerName() ?>"
                                 title="<?= $likeProfile->getSellerName() ?>">
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="album-opt">
            <i class="fa fa-eye"></i><span><?= $model->views_count ?></span>
        </div>
    </div>
    <?php if ($editable === true) { ?>
        <div class="job-owner-bg"></div>
        <div class="job-owner-buttons">
            <div class="job-owner-button">
                <?= Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Edit'),
                    ["/account/portfolio/update", 'id' => $model->id],
                    ['class' => 'btn-middle text-center']
                ) ?>
            </div>
            <div class="job-owner-button">
                <?= Html::a(
                    '<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'View'),
                    ['/account/portfolio/view', 'id' => $model->id],
                    ['class' => 'btn-middle blue album-show text-center', 'data-pjax' => 0]
                )?>
            </div>
            <div class="job-owner-button">
                <?= Html::a('<i class="fa fa-times" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Delete'),
                    ["/account/portfolio/delete", 'id' => $model->id],
                    ['class' => 'btn-middle red text-center', 'data-method' => 'post', 'data-confirm' => Yii::t('account', 'Are you sure?')]
                ) ?>
            </div>
        </div>
    <?php } ?>
</div>
