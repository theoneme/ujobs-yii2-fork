<?php

use common\helpers\FileInputHelper;
use common\helpers\ImperaviHelper;
use common\models\Attachment;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\components\WizardSteps;
use frontend\modules\account\models\PostPortfolioForm;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostPortfolioForm
 * @var $exampleModel common\models\Job
 * @var $action string
 * @var $locales array
 * @var $currencySymbols array
 * @var $currencies array
 * @var $this View
 */
CropperAsset::register($this);
SelectizeAsset::register($this);

?>
    <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="mobile-note-title"></div>
                </div>
                <div class="modal-body">
                    <div class="mobile-note-text"></div>
                    <div class="mobile-note-btn text-center">
                        <a class="button big green" href="#"><?= Yii::t('app', 'Continue') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('account', 'Description'),
    2 => Yii::t('account', 'Gallery'),
    3 => Yii::t('account', 'Publish'),
], 'step' => 1]); ?>

    <div class="profile">
        <div class="container-fluid">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a>
                        <?= Yii::t('app', Yii::$app->params['languages'][$model->locale]) ?>
                    </a>
                </li>
            </ul>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-step' => 1,
                            'data-final-step' => 3
                        ],
                        'id' => 'portfolio-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]); ?>
                    <?= Html::hiddenInput('id', $model->portfolio->id, ['id' => 'portfolio-id']) ?>
                    <?= Html::activeHiddenInput($model, 'locale') ?>
                    <div class="tab-content nostyle-tab-content">
                        <div class="settings-content step tab-pane fade in active" id="step1" data-step="1">
                            <div class="gig-head row">
                                <div class="profileh"><?= Yii::t('account', 'Description') ?></div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "title", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                        <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('account', 'Max') . '</div>
                                    </div>'])->textarea([
                                        'class' => 'readsym bigsymb',
                                        'placeholder' => Yii::t('account', 'Name your album'),
                                        'data-action' => 'category-suggest'
                                    ])
                                    ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Name your album') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'This is your album`s title. Choose wisely, you can only use 80 characters.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "description", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->widget(Widget::class, ArrayHelper::merge(ImperaviHelper::getDefaultConfig(), [
                                        'settings' => [
                                            'buttonsHide' => [
                                                'deleted',
                                                'horizontalrule',
                                                'link',
                                            ]
                                        ]
                                    ])); ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Description') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'Try to describe your album with as many details as possible.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label class="control-label" for="postproductform-category_id">
                                                    <?= Yii::t('account', 'Category') ?>
                                                </label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                                        'prompt' => ' -- ' . Yii::t('account', 'Select An Upper Category'),
                                                        'class' => 'form-control'
                                                    ])->label(false); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postportfolioform-parent_category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('account', 'Select A Category'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helper'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ]
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-md-offset-6 col-xs-12 select-margin hidden 3rd-level">
                                                    <?= $form->field($model, 'subcategory_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postportfolioform-category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('account', 'Select A Subcategory'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helperx'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                                        ],
                                                        'pluginEvents' => [
                                                            "depdrop:change" => "function(event, id, value, count) { 
                                                                let container = $(this).closest('.3rd-level');
                                                                if(count > 0) {
                                                                    container.removeClass('hidden');
                                                                } else {
                                                                    container.addClass('hidden');
                                                                }
                                                            }",
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ]
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helperx', $model->subcategory_id, ['id' => 'depdrop-helperx']); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="suggestions-block" class="col-md-12">

                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('account', 'Choose category and subcategory') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'Choose category and subcategory where your album will be published.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, 'tags', [
                                        'template' => '
								    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
								        <div class="set-title">
								            {label}<div class="mobile-question" data-toggle="modal"></div>
								        </div>
								    </div>
								    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
								        {input}{error}
								        <div class="counttext"> ' . Yii::t('app', 'We recommend using at least 3 tags') . '</div>
								    </div>
								'
                                    ])->textInput([
                                        'id' => 'tags',
                                        'class' => 'readsym'
                                    ]) ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('account', 'Tags, tags, tags!') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('account', 'These are the words by which customers can find your album.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('account', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('account', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade in file-input-step" id="step2" data-step="2">
                            <div class="gig-head row">
                                <div class="profileh"><?= Yii::t('account', 'Gallery') ?></div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label"
                                                   for="postproductform-uploaded_images"><?= Yii::t('account', 'Cover of your album') ?></label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="cgp-upload-files">
                                            <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                'id' => 'file-upload-input',
                                                'pluginOptions' => [
                                                    'overwriteInitial' => true,
                                                    'initialPreview' => $model->image ? $model->portfolio->getThumb() : [],
                                                    'initialPreviewConfig' => !empty($model->image) ? [[
                                                        'caption' => basename($model->image),
                                                        'url' => Url::toRoute('/image/delete'),
                                                        'key' => $model->portfolio->user_id
                                                    ]] : [],
                                                ]
                                            ])) ?>
                                            <?= $form->field($model, 'image')->hiddenInput([
                                                'id' => 'image-input',
                                                'value' => $model->image,
                                                'data-key' => $model->portfolio->user_id
                                            ])->label(false); ?>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('account', 'Photos') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('account', 'It’s photo time! Upload cover for your album.') ?>
                                            </p>
                                            <ul>
                                                <li><?= Yii::t('account', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                                <li><?= Yii::t('account', 'Dimensions: more - better') ?></li>
                                                <li><?= Yii::t('account', 'Max File size: {count} MB', ['count' => 5]) ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <div class="additional-gig-item">
                                    <?php foreach ($model->attachments as $key => $value) { ?>
                                        <?php /** @var Attachment $value */ ?>
                                        <div class="cgb-optbox">
                                            <div class="row line-add-extra">
                                                <div class="col-xs-12">
                                                    <?= $form->field($value, "[{$key}]description")->textarea([
                                                        'placeholder' => Yii::t('account', 'Description for extra image')
                                                    ])->label(false) ?>
                                                </div>
                                                <div class="col-xs-12 extra-recommend chetotam"
                                                     data-chetotam="<?= $key ?>">
                                                    <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                        'id' => "file-upload-input-{$key}",
                                                        'options' => ['class' => 'file-upload-input-extra'],
                                                        'pluginOptions' => [
                                                            'overwriteInitial' => true,
                                                            'initialPreview' => $value->content ? $value->getThumb() : [],
                                                            'initialPreviewConfig' => !empty($value->content) ? [[
                                                                'caption' => basename($value->content),
                                                                'url' => Url::toRoute('/image/delete'),
                                                                'key' => "image_init_{$value->id}"
                                                            ]] : [],
                                                        ]
                                                    ])) ?>
                                                </div>
                                            </div>
                                            <div class="delete-extra">
                                                <i class="fa fa-times"></i>
                                                <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue', 'data-id' => $value->id]) ?>
                                            </div>
                                            <hr>
                                        </div>
                                    <?php } ?>
                                    <div class="extra-container"></div>
                                    <div class="additional-gig-head" style="display: block;">
                                        <i class="fa fa-plus"></i>
                                        <a class="load-portfolio-extra"
                                           href=""><?= Yii::t('account', 'Add Extra Image') ?></a>
                                    </div>
                                    <div class="images-container">
                                        <?php foreach ($model->attachments as $key => $image) {
                                            /* @var $image Attachment */
                                            echo $form->field($image, "[{$key}]content")->hiddenInput([
                                                'value' => $image->content,
                                                'data-key' => 'image_init_' . $image->id
                                            ])->label(false);
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block row">
                                <div class="col-md-12">
                                    <?= Html::button(Yii::t('account', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                    <?= Html::submitButton(Yii::t('account', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade in" id="step3" data-step="3">
                            <div class="publish-title text-center">
                                <?= Yii::t('app', 'It\'s almost ready...') ?>
                            </div>
                            <div class="publish-text text-center">
                                <?= Yii::t('account', "Let`s publish your album.") ?>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= !empty($locales) ? Html::button(Yii::t('app', 'Next language'), ['class' => 'btn-mid fright next-language']) : '' ?>
                                <?= Html::submitButton(Yii::t('account', 'Publish album'), ['class' => 'btn-mid fright']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
if (!empty($locales)) {
    echo Html::beginForm(['/account/portfolio/create'], 'post', ['id' => 'portfolio-locale-form']);
    foreach ($locales as $locale) {
        echo Html::hiddenInput('locales[]', $locale);
    }
    echo Html::endForm();
}
?>

<?= $this->render('@frontend/views/common/crop-bg.php')?>

<?php $userId = Yii::$app->user->identity->getId();
$attachmentCount = count($model->attachments);
$categorySuggestRoute = Url::toRoute('/ajax/category-suggest');
$renderExtraRoute = Url::to(['/account/portfolio/render-extra']);
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'portfolio_tag']);
$addTagMessage = Yii::t('account', 'Add tag');
$ajaxCreateRoute = Url::toRoute('/account/portfolio/ajax-create');

$script = <<<JS
	let uid = $userId,
	    iterator = $attachmentCount,
	    attachments = $attachmentCount;

    $(document).on('click', '.mobile-question', function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click', '.mobile-note-btn a', function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });
	
	$(document).on("click", ".load-portfolio-extra", function(e) {
	    e.preventDefault();
	    let container = $(this).closest(".formgig-block").find(".extra-container");
	    $.post('$renderExtraRoute', {
	        iterator: iterator
	    }, function(data) {
	        if (data.success === true) {
	            container.append(data.html);
	        } else {
	            alertCall('top', 'error', data.message);
	        }
	    }, "json");
	    iterator++;
	});
	
	let suggestTimer = null;
    $(document).on("keyup", "[data-action=category-suggest]", function() {
        let self = $(this);
        clearTimeout(suggestTimer);
        suggestTimer = setTimeout(function() {
            let val = self.val();
            $.get('{$categorySuggestRoute}',
                {entity: 'job', request: val},
                function(data) {
                    if (data.success === true) {
                        $('#suggestions-block').html(data.html);
                    } else {
                        $('#suggestions-block').html('');
                    }
                },
                "json"
            );
        }, 700);
    });
    
    $(document).on('click', '[data-action=suggestion-select]', function() {
        let parentCategoryInput = $('#postportfolioform-parent_category_id');
        let categoryInput = $('#postportfolioform-category_id');
        let subcategoryInput = $('#postportfolioform-subcategory_id');
        let pId = $(this).data('p-id');
        let cId = $(this).data('c-id');
        let sId = $(this).data('s-id');
        
        categoryInput.attr('data-c-id', cId);
        subcategoryInput.attr('data-s-id', sId);
        parentCategoryInput.val(pId).trigger('change');
        
        return false;
    });
    
    $('#postportfolioform-category_id').on('depdrop:afterChange', function(event, id, value) {
        let cId = $(this).attr('data-c-id');
        if(cId) {
            $('#postportfolioform-category_id').val(cId).trigger('change');
        }
    });
    
    $('#postportfolioform-subcategory_id').on('depdrop:afterChange', function(event, id, value) {
        let sId = $(this).attr('data-s-id');
        if(sId) {
            $('#postportfolioform-subcategory_id').val(sId).trigger('change');
        }
    });

	$(document).on("click", ".link-blue", function() {
	    let id = $(this).data('id');
	    $(this).closest(".cgb-optbox").remove();
	    $(".images-container").find("input[data-key=image_init_" + id + "]").remove();
	});

	$("#portfolio-form").on("beforeValidateAttribute", function(event, attribute, messages) {
	    let current_step = $(this).data("step");
	    let input_step = $(attribute.container).closest(".step").data("step");
	    if (current_step !== input_step) {
	        return false;
	    }
	}).on("afterValidate", function(event, messages, errorAttributes) {
	    if (errorAttributes.length) {
	        let current_step = $(this).data("step");
	        $.each(errorAttributes, function(k, v) {
	            $(v.container).closest(".faqedit-block").addClass("feb-open");
	        });
	        $("html, body").animate({
	            scrollTop: $(this).find("div[data-step='" + current_step + "'] .has-error").first().offset().top - 180
	        }, 1000);
	    }
	}).on("beforeSubmit", function(event) {
	    let current_step = $(this).data("step");
	    let self = $(this);
	    if ($(this).find("div[data-step='" + current_step + "']").hasClass("file-input-step")) {
	        let returnValue = true;
	        $(".file-upload-input, .file-upload-input-extra").each(function(index) {
	            if ($(this).fileinput("getFilesCount") > 0) {
	                $(this).fileinput("upload");
	                returnValue = false;
	                return false;
	            }
	        });
	        if (!returnValue) {
	            return false;
	        }
	    }
	    if (current_step !== $(this).data("final-step")) {
	        self.find("div[data-step='" + current_step + "']").removeClass("active");
	        $(".gig-steps li").removeClass("active");
	        current_step++;
	        self.data("step", current_step);
	        self.find("div[data-step='" + current_step + "']").addClass("active");
	        $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
	        $("html, body").animate({
	            scrollTop: 0
	        }, "slow");
	        return false;
	    }
	});

	$(".prev-step").on("click", function() {
	    let jobForm = $("#portfolio-form");
	    let current_step = jobForm.data("step");
	    if (current_step > 1) {
	        jobForm.find("div[data-step='" + current_step + "']").removeClass("active");
	        $(".gig-steps li").removeClass("active");
	        current_step--;
	        jobForm.data("step", current_step);
	        jobForm.find("div[data-step='" + current_step + "']").addClass("active");
	        $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
	        $("html, body").animate({
	            scrollTop: 0
	        }, "slow");
	    }
	    return false;
	});

	$('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
	    let response = data.response;
	    $('#image-input').val(response.uploadedPath).data('key', response.imageKey);
	    $("#portfolio-form").submit();
	}).on('filedeleted', function(event, key) {
	    $('#image-input').val(null);
	});

	let hasFileUploadError = false;
	$(document).on("fileuploaded", ".file-upload-input-extra", function(event, data, previewId, index) {
	    let response = data.response;
	    let id = $(this).closest('.chetotam').data('chetotam');
	    $('[name="Attachment[' + id + '][content]"]').remove();
	    $(".images-container").append("<input name='Attachment[" + id + "][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
	}).on("filedeleted", ".file-upload-input-extra", function(event, key) {
	    $(".images-container").find("input[data-key='" + key + "']").remove();
	}).on("filebatchuploadcomplete", ".file-upload-input-extra", function() {
        if (hasFileUploadError === false) {
            $("#portfolio-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", ".file-upload-input-extra", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });

	$("#tags").selectize({
	    valueField: "value",
	    labelField: "value",
	    searchField: ["value"],
	    plugins: ["remove_button"],
	    persist: false,
	    create: true,
	    maxOptions: 10,
	    render: {
	        option_create: function(data, escape) {
	            return "<div class=\"create\">$addTagMessage&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
	        }
	    },
	    "load": function(query, callback) {
	        if (!query.length)
	            return callback();
	        $.post("$tagListUrl", {
	                query: encodeURIComponent(query)
	            },
	            function(data) {
	                callback(data);
	            }).fail(function() {
	            callback();
	        });
	    }
	})

JS;

$this->registerJs($script);