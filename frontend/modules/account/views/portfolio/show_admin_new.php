<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\models\Message;
use common\models\Offer;
use frontend\assets\PortfolioAsset;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use common\models\user\Profile;
use frontend\modules\account\components\ProfileSidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $newPortfolioDataProvider ActiveDataProvider
 * @var $reviewDataProvider ActiveDataProvider
 * @var $message Message
 * @var $offer Offer
 * @var $reviewsAverage integer
 * @var $reviewsCount integer
 * @var $contactAccessInfo array
 */
WorkerAsset::register($this);
SelectizeAsset::register($this);
PortfolioAsset::register($this);

?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($profile->gravatar_email) { ?>
                            <?= Html::img($profile->getThumb(), ['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($profile->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($profile->user->isOnline()) { ?>
                                <span class="status isonline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                            <?php } else { ?>
                                <span class="status isoffline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                            <?php } ?>
                        </div>
                        <div class="wmh-rate">
                            <?= $this->render('@frontend/widgets/views/rating', [
                                'rating' => $profile->rating / 2,
                                'size' => 14
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <?= Html::a(Yii::t('account', 'View As A Buyer'),
                        ['/account/portfolio/show-as-customer', 'id' => $profile->user_id],
                        ['class' => 'button green no-size']
                    ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= ProfileSidebar::widget([
                    'profile' => $profile,
                    'editable' => true,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'portfolios' => $newPortfolioDataProvider->totalCount
                            ? ['link' => '#portfolios', 'label' => Yii::t('app', 'Portfolio') . " ({$newPortfolioDataProvider->totalCount})"]
                            : false,
                        'reviews' => $reviewDataProvider->totalCount
                            ? ['link' => '#reviews', 'label' => Yii::t('app', 'Reviews') . " ({$reviewDataProvider->totalCount})"]
                            : false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs">
                <div class="hidden-xs">
                    <div class="gigs-title" id="portfolio-list-header">
                        <?= Yii::t('account', 'Your portfolio') ?>
                        <?= Html::a(Yii::t('account', 'Post portfolio'), ['/account/portfolio/create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $newPortfolioDataProvider,
                        'viewParams' => ['editable' => $editable],
                        'itemView' => 'listview/portfolio-assymetric',
                        'options' => [
                            'class' => 'portfolio-grid',
                            'id' => 'pro-portfolios',
                        ],
                        'itemOptions' => [
                            'class' => 'pg-item'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have portfolio") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                </div>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="portfolios">
                        <div class="gigs-title"><?= Yii::t('account', 'Your portfolio') ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $newPortfolioDataProvider,
                            'viewParams' => ['editable' => $editable],
                            'itemView' => 'listview/portfolio-assymetric',
                            'options' => [
                                'class' => 'portfolio-grid',
                                'id' => 'pro-portfolios-mobile',
                            ],
                            'itemOptions' => [
                                'class' => 'pg-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger"
                            id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn"
                            id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="album-modal">

    </div>

<?php
$cartUrl = Url::to(['/store/cart/add-offer']);
$attributeListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$cartText = Yii::t('account', 'Individual order added to cart');

$script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    var masonryOrient = true;
    if($('body').hasClass('rtl')){
        masonryOrient = false;
    }
    menuMobileItems.css('width', perc);
    
    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });
    
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x"
    });
    $('.portfolio-grid').masonry({
        itemSelector: '.pg-item',
        originLeft: masonryOrient
    });
 
    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script, \yii\web\View::POS_LOAD);