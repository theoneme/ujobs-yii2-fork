<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.09.2017
 * Time: 13:52
 */

use common\models\Attachment;
use common\models\UserPortfolio;
use frontend\components\SocialShareWidget;
use frontend\models\PostCommentForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var ActiveDataProvider $otherPortfoliosDataProvider */
/* @var ActiveDataProvider $commentsDataProvider */
/* @var PostCommentForm $postCommentForm */
/* @var UserPortfolio $model */
/* @var null|string $leftLink */
/* @var null|string $rightLink */
/* @var $returnUrl string */

$category = $model->category;
$route = $category->type === 'product_category' ? ['/category/jobs', 'category_1' => $category->translation->slug] : ['/board/category/products', 'category_1' => $category->translation->slug];

$hasLike = $model->hasLike();

?>

<?php if ($leftLink !== null) { ?>
    <a href="<?= $leftLink ?>" class="album-prev album-arrow">
        <svg class="icon icon-arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 7.6 12">
            <polygon class="icon-arrow__direction" points="7.5,10.5 3.1,6 7.6,1.6 6,0 0,6 6,12 "></polygon>
        </svg>
    </a>
<?php } ?>
<?php if ($rightLink !== null) { ?>
    <a href="<?= $rightLink ?>" class="album-next album-arrow">
        <svg class="icon icon-arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 7.6 12">
            <polygon class="icon-arrow__direction" points="0,1.5 4.5,6 0,10.4 1.6,12 7.6,6 1.6,0 "></polygon>
        </svg>
    </a>
<?php } ?>
    <a href="<?= $returnUrl ?>" class="album-go-back">
        <svg class="icon icon-arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 7.6 12">
            <polygon class="icon-arrow__direction" points="7.5,10.5 3.1,6 7.6,1.6 6,0 0,6 6,12 "></polygon>
        </svg>
        <span><?= Yii::t('app', 'Back')?></span>
    </a>
    <div class="album-watch text-center">
        <div class="album-content text-left">
            <div class="visible-xs album-mobile-author">
                <div class="album-side-block">
                    <div class="album-follow">
                        <div class="af-ava">
                            <?= Html::img($model->getSellerThumb('catalog'), ['alt' => $model->getSellerName(),'title' => $model->getSellerName()]) ?>
                        </div>
                        <div class="af-name">
                            <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl()) ?>
                        </div>
                        <div class="af-button text-right">
                            <?= Html::a(Yii::t('account', 'Watch profile'), $model->getSellerUrl(), ['class' => 'button green small']) ?>
                        </div>
                    </div>
                </div>
                <div class="album-side-block">
                    <div class="album-title"><?= $model->title ?></div>
                    <div class="cat-related">
                        <?= Html::a($model->category->translation->title, $route) ?>
                    </div>
                    <div class="album-options">
                        <div class="album-opt like-over">
                            <i class="fa fa-thumbs-up"></i><span><?= count($model->likes) ?></span>
                            <?php if (count($model->likes) > 0) { ?>
                                <div class="people-likes">
                                    <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                                    <?php foreach ($model->likeUsers as $likeProfile) { ?>
                                        <a href="<?= Url::to(['/account/profile/show', 'id' => $likeProfile->user_id]) ?>">
                                            <img src="<?= $likeProfile->getThumb('catalog') ?>"
                                                 alt="<?= $likeProfile->getSellerName() ?>"
                                                 title="<?= $likeProfile->getSellerName() ?>">
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="album-opt">
                            <i class="fa fa-eye"></i><span><?= $model->views_count ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="album-main-side">
                <div class="photo">
                    <?= Html::img($model->getThumb('catalog'), ['class' => 'lazy-load', 'data-src' => $model->getThumb(),'alt' => $model->title, 'title' => $model->title]) ?>
                </div>
                <?php if (!empty($model->attachments)) { ?>
                    <?php foreach ($model->attachments as $key => $attachment) { ?>
                        <?php /* @var Attachment $attachment */ ?>
                        <div class="photo">
                            <?= Html::img($attachment->getThumb('catalog'), ['class' => 'lazy-load', 'data-src' => $attachment->getThumb(),'alt' => $model->title, 'title' => $model->title]) ?>
                        </div>
                        <?php if ($attachment->description) { ?>
                            <p class="portfolio-description"><?= Html::encode($attachment->description) ?></p>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <?php if (!Yii::$app->user->isGuest && !$hasLike && !hasAccess($model->user_id)) { ?>
                    <div class="like-album-big text-center">
                        <div>
                            <a href="#" class="post-like text-center" data-entity="portfolio" data-id="<?= $model->id ?>">
                                <svg class="like-svg-over" xmlns="http://www.w3.org/2000/svg"
                                     preserveAspectRatio="xMidYMid" width="35" height="35" viewBox="0 0 90 90">
                                    <path d="M89.999 45 C89.999 46.6 85.8 48 85.7 49.6 C85.481 51.2 89.2 53.5 88.9 55 C88.521 56.6 84.2 57 83.6 58.5 C83.096 60.1 86.2 63.1 85.5 64.5 C84.849 66 80.5 65.4 79.6 66.8 C78.791 68.1 81.2 71.8 80.2 73.1 C79.177 74.3 75.1 72.8 73.9 73.9 C72.794 75.1 74.3 79.2 73.1 80.2 C71.802 81.2 68.1 78.8 66.8 79.7 C65.411 80.5 66 84.9 64.5 85.6 C63.087 86.3 60 83.1 58.5 83.6 C57.008 84.2 56.6 88.5 55 88.9 C53.459 89.2 51.2 85.5 49.6 85.7 C48.002 85.8 46.6 90 45 90 C43.370 90 42 85.8 40.4 85.7 C38.799 85.5 36.5 89.2 35 88.9 C33.403 88.5 33 84.2 31.5 83.6 C29.953 83.1 26.9 86.3 25.5 85.6 C24.013 84.9 24.6 80.5 23.2 79.7 C21.863 78.8 18.2 81.2 16.9 80.2 C15.680 79.2 17.2 75.1 16.1 73.9 C14.927 72.8 10.8 74.3 9.8 73.1 C8.811 71.8 11.2 68.1 10.3 66.8 C9.491 65.4 5.1 66 4.4 64.5 C3.748 63.1 6.9 60.1 6.4 58.5 C5.837 57 1.5 56.6 1.1 55 C0.761 53.5 4.5 51.2 4.3 49.6 C4.157 48 -0 46.6 -0 45 C-0.005 43.4 4.2 42 4.3 40.4 C4.513 38.8 0.8 36.5 1.1 35 C1.473 33.4 5.8 33 6.4 31.5 C6.898 30 3.7 26.9 4.4 25.5 C5.144 24 9.5 24.6 10.3 23.2 C11.203 21.9 8.8 18.2 9.8 16.9 C10.817 15.7 14.9 17.2 16.1 16.1 C17.200 14.9 15.7 10.8 16.9 9.8 C18.191 8.8 21.9 11.2 23.2 10.4 C24.583 9.5 24 5.2 25.5 4.5 C26.907 3.8 30 6.9 31.5 6.4 C32.986 5.8 33.4 1.5 35 1.1 C36.535 0.8 38.8 4.5 40.4 4.3 C41.991 4.2 43.4 0 45 0 C46.623 0 48 4.2 49.6 4.3 C51.195 4.5 53.5 0.8 55 1.1 C56.591 1.5 57 5.8 58.5 6.4 C60.041 6.9 63.1 3.8 64.5 4.5 C65.981 5.2 65.4 9.5 66.8 10.4 C68.131 11.2 71.8 8.8 73.1 9.8 C74.314 10.8 72.8 14.9 73.9 16.1 C75.067 17.2 79.2 15.7 80.2 16.9 C81.183 18.2 78.8 21.9 79.6 23.2 C80.503 24.6 84.8 24 85.6 25.5 C86.246 26.9 83.1 30 83.6 31.5 C84.157 33 88.5 33.4 88.9 35 C89.233 36.5 85.5 38.8 85.7 40.4 C85.837 42 90 43.4 90 45 Z"></path>
                                </svg>
                                <svg class="like-svg" xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                     viewBox="0.5 0.5 16 16">
                                    <path d="M.5 7.5h3v8h-3zM7.207 15.207c.193.19.425.29.677.293H12c.256 0 .512-.098.707-.293l2.5-2.5c.19-.19.288-.457.293-.707V8.5c0-.553-.445-1-1-1h-5L11 5s.5-.792.5-1.5v-1c0-.553-.447-1-1-1l-1 2-4 4v6l1.707 1.707z"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                <?php } ?>
                <div class="album-bottom-info">
                    <div class="album-info-head">
                        <div class="album-head-opts">
                            <div class="album-head-right">
                                <!--                    <a href="#"><i class="fa fa-calendar-plus-o"></i><span>Добавить в коллекцию</span></a>-->
                                <a href="#" class="clipboard-copy"><i class="fa fa-link"></i><span>
                                        <?= Yii::t('account', 'Copy link') ?></span>
                                </a>
                            </div>
                        </div>
                        <div class="album-info-author">
                            <div class="album-info-left">
                                <div class="album-info-author-table">
                                    <div class="album-author-ava">
                                        <div>
                                            <?= Html::img($model->getSellerThumb('catalog'), ['alt' => $model->getSellerName(),'title' => $model->getSellerName()]) ?>
                                        </div>
                                    </div>
                                    <div class="album-author-contact">
                                        <a href="#" class="album-author-name">
                                            <?= Html::encode($model->getSellerName()) ?>
                                        </a>
                                        <div class="album-author-address">
                                            <i class="fa fa-map-marker"></i><?= Html::encode($model->getSellerName()) ?>
                                        </div>
                                        <div class="album-author-buttons">
                                            <?= Html::a(Yii::t('account', 'Watch profile'), $model->getSellerUrl(), ['class' => 'button text-center green middle']) ?>
                                            <?= Html::a(Yii::t('account', 'Message'),
                                                ['/account/inbox/start-conversation', 'user_id' => $model->user_id],
                                                ['class' => 'button text-center white middle']
                                            ) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="album-info-right text-right">
                                <?= Html::a(Yii::t('account', 'View full profile'), $model->getSellerUrl(), ['class' => 'full-profile']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-other-albums visible-xs">
                        <div class="other-albums-table">
                            <?php
                            foreach ($otherPortfoliosDataProvider->getModels() as $item) {
                                echo Html::a(
                                    Html::img($item->getThumb('catalog')),
                                    ['/account/portfolio/view', 'id' => $item->id],
                                    ['class' => 'album-show', 'data-pjax' => 0, 'data-id' => $item->id]
                                );
                            } ?>
                        </div>
                        <?= Html::a(Yii::t('account', 'View full profile'), $model->getSellerUrl(), ['class' => 'full-profile-mobile text-center']) ?>
                    </div>
                    <div class="other-albums hidden-xs">
                        <?= ListView::widget([
                            'dataProvider' => $otherPortfoliosDataProvider,
                            'itemView' => '../listview/portfolio',
                            'viewParams' => ['editable' => false],
                            'options' => [
                                'class' => 'max-width gigs-list relative block-flex',
                                'id' => 'pro-portfolios',
                            ],
                            'itemOptions' => [
                                'class' => 'bf-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center" style="color:#fff; height:380px;">' . Yii::t('account', "It seems this user does not have any other projects") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    </div>
                </div>
                <div class="clearfix">
                    <div id="reviews" class="work-section ">
                        <div class="head-work">
                            <div class="rc-text"><?= Yii::t('account', 'Comments') ?></div>
                            <div class="rev-rate">(<?= $commentsDataProvider->getTotalCount() ?>)</div>
                        </div>
                        <div class="album-small-block clearfix">
                            <?= ListView::widget([
                                'dataProvider' => $commentsDataProvider,
                                'itemView' => '@frontend/views/news/listview/comment-item',
                                'emptyText' => Yii::t('account', 'No comments yet'),
                                'options' => [
                                    'class' => ''
                                ],
                                'itemOptions' => [
                                    'class' => 'comment'
                                ],
                                'layout' => "{items}"
                            ]) ?>
                            <?php if (!Yii::$app->user->isGuest) { ?>

                                <?php $form = ActiveForm::begin([
                                    'id' => 'comment-form',
                                    'action' => Url::to(['/comment/create'])
                                ]); ?>
                                <?= $form->field($postCommentForm, 'entityId')->hiddenInput(['value' => $model->id])->label(false) ?>
                                <?= $form->field($postCommentForm, 'comment')->textarea() ?>
                                <?= $form->field($postCommentForm, 'name', [
                                    'template' => '
                                <div class="row">
                                    <div class="col-md-6">
                                        {label}{input}{error}
                                    </div>
                                </div>
                            '
                                ])->textInput() ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $form->field($postCommentForm, 'email')->textInput() ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= Html::submitButton(Yii::t('app', 'Post Comment'), ['class' => 'btn-middle bright']) ?>
                                        </div>
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="album-small-block">
                            <div class="album-sb-head"><?= Yii::t('account', 'Basic info') ?></div>
                            <div class="album-sb-body">
                                <p>
                                    <?= preg_replace('/<a\s*.*?\s*href=\"(.*?)\".*?>(.*?)<\/a>/', "\\1", $model->description) ?? Yii::t('account', 'No description') ?>
                                </p>
                                <div class="album-publish"><?= Yii::t('account', 'Publish date: {date}', ['date' => Yii::$app->formatter->asDate($model->created_at)]) ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="album-small-block">
                            <div class="album-sb-head"><?= Yii::t('account', 'Tags') ?></div>
                            <div class="album-sb-body">
                                <div class="tags">
                                    <?php if (count($model->tags) > 0) { ?>
                                        <?php foreach ($model->tags as $tag) {
                                            echo Html::a('<h2>' . Html::encode($tag->attrValueDescription->title) . '</h2>', ['/category/portfolio-tag', 'portfolio_tag' => $tag->value_alias]);
                                        } ?>
                                    <?php } else { ?>
                                        <p><?= Yii::t('account', 'No tags specified') ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="album-author-side">
                <div class="sticky-top">
                    <div class="album-side-block">
                        <div class="album-follow">
                            <div class="af-button">
                                <div class="af-name">
                                    <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['style' => 'color: #000;text-decoration: underline']) ?>
                                </div>
                                <?= Html::a(Yii::t('account', 'Watch profile'), $model->getSellerUrl(), ['class' => 'button green middle text-center']) ?>
                            </div>
                            <div class="af-ava">
                                <?= Html::img($model->getSellerThumb('catalog'), ['alt' => $model->getSellerName(),'title' => $model->getSellerName()]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="album-side-block">
                        <div class="album-title"><?= $model->title ?></div>
                        <div class="cat-related">
                            <?= Html::a($model->category->translation->title, $route) ?>
                        </div>
                        <div class="album-options">
                            <div class="album-opt like-over">
                                <i class="fa fa-thumbs-up"></i><span><?= count($model->likes) ?></span>
                                <?php if (count($model->likes) > 0) { ?>
                                    <div class="people-likes">
                                        <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                                        <?php foreach ($model->likeUsers as $likeProfile) { ?>
                                            <a href="<?= Url::to(['/account/profile/show', 'id' => $likeProfile->user_id]) ?>">
                                                <img src="<?= $likeProfile->getThumb('catalog') ?>"
                                                     alt="<?= $likeProfile->getSellerName() ?>"
                                                     title="<?= $likeProfile->getSellerName() ?>">
                                            </a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="album-opt">
                                <i class="fa fa-eye"></i><span><?= $model->views_count ?></span>
                            </div>
                        </div>
                        <div class="album-publish"><?= Yii::t('account', 'Publish date: {date}', ['date' => Yii::$app->formatter->asDate($model->created_at)]) ?></div>
                    </div>
                    <!--            <div class="album-side-block">-->
                    <!--                            <div class="tools-icons">-->
                    <!--                                <div class="tools-item">-->
                    <!--                                    <img src="/images/new/photoshop.svg" alt="photoshop">-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                            <div class="tools-list">-->
                    <!--                                <a href="#">text1</a>,-->
                    <!--                                <a href="#">text text 2</a>,-->
                    <!--                                <a href="#">text3text3 text3</a>-->
                    <!--                            </div>-->
                    <!--            </div>-->
                    <?php if ($hasLike) { ?>
                        <p>&nbsp;</p>
                        <div class="markers text-center">
                            <div class="stick text-center feat active"><?= Yii::t('account', 'You like this project') ?></div>
                        </div>
                    <?php } ?>

                    <div class="like-album">
                        <?php if ($hasLike) {
                            echo Html::a(
                                '<i class="fa fa-thumbs-up"></i>' . Yii::$app->formatter->asDate($model->myLike->created_at),
                                null,
                                ['class' => 'btn-big', 'disabled' => true]
                            );
                        } else {
                            if (!Yii::$app->user->isGuest) {
                                if (!hasAccess($model->user_id)) {
                                    echo Html::a(
                                        '<i class="fa fa-thumbs-up"></i> ' . Yii::t('account', 'Like project'),
                                        null,
                                        ['class' => 'button white big post-like text-center', 'data-id' => $model->id, 'data-entity' => 'portfolio']
                                    );
                                }

                            } else {
                                echo Html::a(
                                    '<i class="fa fa-thumbs-up"></i>' . Yii::t('account', 'Like project'),
                                    null,
                                    [
                                        'class' => 'button white big login-to-like',
//                                        'data-toggle' => 'modal',
//                                        'data-target' => '#ModalLogin'
                                    ]
                                );
                            }
                        } ?>
                    </div>

                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::to(['/account/portfolio/view', 'id' => $model->id, 'invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                            'twitterMessage' => Yii::t('account', 'Check out this portfolio album on {site}:', ['site' => Yii::$app->name]),
                            'mailItem' => [
                                'label' => $model->getLabel(),
                                'thumb' => $model->getThumb('catalog'),
                                'name' => $model->getSellerName()
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$currentUrl = Url::to(['/account/portfolio/view', 'id' => $model->id], true);
$postCommentUrl = Url::to(['/comment/create-ajax']);
$postLikeUrl = Url::to(['/like/like-ajax']);
$thankYouText = Yii::t('account', 'Thank You!');
$script = <<<JS
    let sent = 0;

    $('.clipboard-copy').on('click', function() {
        clipboard.copy({
            'text/plain': "{$currentUrl}",
        });
    });
    
    $('.login-to-like').on('click', function() {
         // $('.album-modal').hide();
         $('#ModalLogin').css('background-color', 'rgba(0, 0, 0, 0.75)').modal('show');
         $('html,body').css('overflow-y', 'auto');
    });

    $('.album-modal').scroll(function() {
        var paddingTop = parseInt($('.album-watch').css('padding-top'));
        var paddingElB = parseInt($('.sticky-bottom').css('padding-bottom'));
        var paddingElT = parseInt($('.sticky-bottom').css('padding-top'));
        var marginBottom = parseInt($('.album-content').css('margin-bottom'));
        if ($(this).scrollTop() > paddingTop) {
            $('.sticky-top').css('position', 'fixed');
        } else {
            $('.sticky-top').css('position', 'absolute');
        }
        if (($('.album-watch').height() - $(this).scrollTop() - $(window).height()) < marginBottom) {
            $('.sticky-bottom').css('position', 'absolute');
        } else {
            $('.sticky-bottom').css('position', 'fixed');
        }
    });

    $('#comment-form').on('beforeSubmit', function() {
        let data = $(this).serialize();
        data = data.concat('&PostCommentForm[entity]=portfolio');
        $.post("{$postCommentUrl}", data, function(response) {
            if (response.success === true) {
                let item = $('.pg-item a[data-id={$model->id}]');

                if (item.length > 0) {
                    item[0].click();
                }
            }
        });

        return false;
    });

    $('.post-like').on('click', function() {
        if(sent === 0) {
            let entity = $(this).data('entity'),
                id = $(this).data('id');

            $.post("{$postLikeUrl}", {
                entity: entity,
                entity_id: id
            }, function(response) {                 
                sent = 1;
                if (response.success === true) {
                    $('a.post-like').html("{$thankYouText}");
                }
            });
        }

        return false;
    });
    
    $('a.social-share').on('click', function() {
        let ww = $(window).width(),
            wh = $(window).height(),
            params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no' + ',width=' + ww * 0.6 + ',height=' + wh * 0.7 + ',left=' + ww * 0.2 + ',top=' + wh * 0.15;
        window.open($(this).attr('href'), $(this).data('hint'), params);
        
        return false;
    });
    
    $.each($('img.lazy-load'), function() {
        $(this).attr('src', $(this).data('src'));
    }); 
JS;

$this->registerJs($script);
