<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.01.2017
 * Time: 13:05
 */

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\attachment;
use yii\helpers\Url;

/* @var attachment $model */
/* @var integer $iterator */
?>

    <div class="cgb-optbox">
        <div class="row line-add-extra">
            <div class="col-xs-12">
                <div id="container-description-<?= $iterator ?>">
                    <?= Html::activeTextarea($model, "[{$iterator}]description", [
                        'placeholder' => Yii::t('account', 'Description of your extra image'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
            <div class="col-xs-12 extra-recommend">
                <div id="container-content-<?= $iterator ?>" data-chetotam="<?= $iterator?>" class="chetotam">
                    <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'id' => "file-upload-input-extra-{$iterator}",
                        'options' => ['class' => 'file-upload-input-extra'],
                        'pluginOptions' => [
                            'overwriteInitial' => true,
                            'initialPreview' => $model->content ? $model->getThumb() : [],
                            'initialPreviewConfig' => !empty($model->content) ? [[
                                'caption' => basename($model->content),
                                'url' => Url::toRoute('/image/delete'),
                                'key' => $model->user_id
                            ]] : [],
                        ]
                    ])) ?>
                </div>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="delete-extra">
            <i class="fa fa-times"></i>
            <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue']) ?>
        </div>
        <hr>
    </div>
<?php $this->registerJs('
    $("#portfolio-form").yiiActiveForm("add", {
        id: "attachment-' . $iterator . '-description",
        name: "attachment[' . $iterator . '][description]",
        input: "#attachment-' . $iterator . '-description",
        container: "#container-description-' . $iterator . '",
        error: ".help-block",
        enableAjaxValidation: true,
        validateOnChange: false,
        validateOnBlur: false
    });
');