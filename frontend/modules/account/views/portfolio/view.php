<?php

use common\models\UserPortfolio;
use frontend\assets\PortfolioAsset;
use frontend\assets\WorkerAsset;
use frontend\models\PostCommentForm;
use yii\data\ActiveDataProvider;
use yii\web\View;

/**
 * @var View $this
 * @var UserPortfolio $model
 * @var $otherPortfoliosDataProvider ActiveDataProvider
 * @var $commentsDataProvider ActiveDataProvider
 * @var $postCommentForm PostCommentForm
 * @var $leftLink string
 * @var $rightLink string
 * @var $returnUrl string
 */
WorkerAsset::register($this);
PortfolioAsset::register($this);

?>
    <div class="album-modal" style="display: block">
        <?= $this->render('partial/modal', [
            'model' => $model,
            'otherPortfoliosDataProvider' => $otherPortfoliosDataProvider,
            'postCommentForm' => $postCommentForm,
            'commentsDataProvider' => $commentsDataProvider,
            'leftLink' => $leftLink,
            'rightLink' => $rightLink,
            'returnUrl' => $returnUrl
        ])?>
    </div>
    <div style="height: 1500px;">

    </div>
<?php

$script = <<<JS
    function goBack() {
        window.location.href = '$returnUrl';
    }

    // $('.album-modal').show();
    $('html,body').css('overflow-y', 'hidden');
        
    $(document).on('click', '.album-modal', function() {
        goBack();
    });

    $(document).on('click', '.album-content,.album-arrow', function(e) {
        e.stopPropagation();
    });
JS;

$this->registerJs($script, View::POS_LOAD);