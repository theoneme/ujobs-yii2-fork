<?php

use common\models\PaymentHistory;

/**
 * @var $model PaymentHistory
 * @var $walletName string
 * @var $walletLetter string
 * @var $currency_code string
 */
?>

<td width="50px" class="hidden-xs">
    <div class="wallet-avatar text-center">
        <?= $walletLetter?>
    </div>
</td>
<td>
    <div class="sum">
        <?= $model->getColoredAmount($currency_code) ?>
        <span class="doing"><?= $model->description ?? $model->getHeader() ?></span>
    </div>
</td>
<td>
    <div class="time text-right"><?= Yii::$app->formatter->asDate($model->created_at) ?></div>
</td>
