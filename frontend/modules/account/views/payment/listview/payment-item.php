<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 18:54
 */

use common\models\PaymentHistory;
use yii\web\View;

/* @var PaymentHistory $model
 * @var View $this
 */


?>

<td>
    <?= Yii::$app->formatter->asDate($model->created_at) ?>
</td>
<td>
    <?= $model->description ?>
</td>
<td>
    <?= $model->getColoredAmount() ?>
</td>