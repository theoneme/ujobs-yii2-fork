<?php

use common\components\CurrencyHelper;
use common\models\PaymentHistory;
use common\models\UserWallet;
use common\models\WalletTransferRequest;
use frontend\models\WithdrawalRequestForm;
use kartik\form\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/**
 * @var View $this
 * @var UserWallet $wallet
 * @var UserWallet[] $otherWallets
 * @var WalletTransferRequest $transferRequest
 * @var WithdrawalRequestForm $withdrawalForm
 * @var array $confirmationMethods
 * @var bool $active
 */


?>

<div id="wallet-<?= $wallet->id?>" class="wallet-content tab-pane fade <?= $active ? 'in active' : ''?>">
    <div class="wallet-title">
        <?= $wallet->getName() . ' ' . Yii::t('account', 'wallet')?>
        <?php if ($wallet->currency_code === 'CTY') {?>
            <span class="hint--top" data-hint="<?= Yii::t('account', 'This is Cointy bonus account') ?>">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        <?php }?>
    </div>
    <div class="head-wallet">
        <div class="wallet-circle">
            <div class="text-center">
                <?= $wallet->getLetter()?>
            </div>
        </div>
        <div class="wallet-title-info">
            <div class="title-sum">
                <span class="money"><?= CurrencyHelper::format($wallet->currency_code, $wallet->balance)?></span>
            </div>
            <div class="title-number"><?= $wallet->getName() . $wallet->id?></div>
        </div>
    </div>
    <div class="row sell-statistic-box">
        <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
            <a href="#" class="stat-alias">
                <div class="sellstat-title text-center"><?= Yii::t('app','Income');?></div>
                <?= CurrencyHelper::format($wallet->currency_code, $wallet->getPaymentHistoryTotal(PaymentHistory::TYPE_BALANCE_CHANGE_POS)) ?>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
            <a href="#" class="stat-alias">
                <div class="sellstat-title text-center"><?= Yii::t('app','Expenses');?></div>
                <?= CurrencyHelper::format($wallet->currency_code, $wallet->getPaymentHistoryTotal(PaymentHistory::TYPE_BALANCE_CHANGE_NEG)) ?>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
            <a href="#" class="stat-alias">
                <div class="sellstat-title text-center"><?= Yii::t('app','Current balance');?></div>
                <?= CurrencyHelper::format($wallet->currency_code, $wallet->balance) ?>
            </a>
        </div>
    </div>
    <?php if(Yii::$app->user->identity->isAllowedToUseWallets()) { ?>
        <div class="wallet-btns">
            <?php if ($wallet->currency_code !== 'CTY') { ?>
                <div class="dropdown">
                    <button class="button white big dropdown-toggle" type="button" data-toggle="dropdown" data-target="withdraw-<?= $wallet->id?>">
                        <?=Yii::t('account', 'Withdrawal request')?>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu message-item withdrawal-form-container" id="withdraw-<?= $wallet->id?>">
                        <li>
                            <?php
                            if(!empty($paymentData)) {
                                $form = ActiveForm::begin([
                                    'type' => ActiveForm::TYPE_INLINE,
                                    'id' => 'withdrawal-form-' . $wallet->id,
                                    'action' => Url::to(['/account/payment/withdrawal-request'])
                                ]); ?>
                                <?= $form->field($withdrawalForm, 'amount')?>
                                <?= $form->field($withdrawalForm, 'targetMethod')->dropDownList($paymentData, ['prompt' => Yii::t('app', 'Make choice'), 'class' => 'target-method'])?>
                                <?= $form->field($withdrawalForm, 'targetAccount')->hiddenInput(['class' => ' target-account'])->label(false);?>
                                <?= $form->field($withdrawalForm, 'currency_code')->hiddenInput(['value' => $wallet->currency_code])->label(false);?>
                                <div class="form-group">
                                    <?=Html::submitButton(Yii::t('account', 'Request withdrawal'), ['class' => 'btn btn-primary'])?>
                                </div>
                                <?php ActiveForm::end();
                            } else { ?>
                                <div class="alert alert-info">
                                    <?= Yii::t('account', 'Please, fill payment information ({link}) in your account settings first', [
                                        'link' => Html::a(Yii::t('account', 'Link'), ['/account/service/payment-settings'])
                                    ])?>
                                </div>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            <?php }
            if ($wallet->currency_code === 'RUB') { ?>
                <div class="dropdown">
                    <button class="button white big dropdown-toggle" type="button" data-toggle="dropdown" data-target="replenish-<?= $wallet->id?>">
                        <?=Yii::t('app', 'Account replenishment')?>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu message-item" id="replenish-<?= $wallet->id?>">
                        <li>
                            <?= Html::beginForm(['/store/checkout/replenish-checkout'], 'post', [
                                'class' => 'form-inline replenish-form',
                                'id' => 'replenish-form-' . $wallet->id,
                            ])?>
                            <div class="form-group">
                                <?= Html::hiddenInput('currency_code', $wallet->currency_code)?>
                                <?= Html::textInput('amount', null, [
                                    'placeholder' => Yii::t('model', 'Amount'),
                                    'style' => 'width: auto',
                                    'class' => 'amount-input'
                                ])?>
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Replenish'), ['class' => 'btn btn-primary']);?>
                            </div>
                            <?= Html::endForm()?>
                        </li>
                    </ul>
                </div>
            <?php }
            if ($wallet->currency_code !== 'CTY') {
                if (count($otherWallets)) { ?>
                    <div class="dropdown position-static">
                        <button class="button white big dropdown-toggle" type="button" data-toggle="dropdown" data-target="transfer-<?= $wallet->id?>">
                            <?=Yii::t('app', 'Money transfer')?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu message-item" id="transfer-<?= $wallet->id?>">
                            <li>
                                <?= Html::beginForm(['/account/payment/transfer'], 'post', [
                                    'class' => 'form-inline transfer-form',
                                    'id' => 'transfer-form-' . $wallet->id,
                                ])?>
                                <?= Html::hiddenInput('from_id', $wallet->id)?>
                                <div class="form-group">
                                    <?= Html::textInput('amount', null, [
                                        'placeholder' => Yii::t('account', 'Write-off amount'),
                                        'style' => 'width: auto',
                                        'class' => 'amount-input',
                                        'data-currency' => $wallet->currency_code
                                    ])?>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <?= Html::textInput('ratio', null, [
                                        'placeholder' => Yii::t('account', 'Exchange rate'),
                                        'style' => 'width: auto',
                                        'class' => 'ratio-input',
                                        'disabled' => true
                                    ])?>
                                </div>
                                <div class="form-group">
                                    <?= Html::textInput('amount_to', null, [
                                        'placeholder' => Yii::t('account', 'Crediting amount'),
                                        'style' => 'width: auto',
                                        'class' => 'amount-to-input',
                                        'data-currency' => ''
                                    ])?>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control transfer-wallet-select" name="to_id" style="width: auto">
                                        <option data-currency="" value=""><?= Yii::t('account', 'Select wallet')?></option>
                                        <?php foreach ($otherWallets as $otherWallet) {
                                            echo '<option data-currency="' . $otherWallet->currency_code . '" value="' . $otherWallet->id . '">' . $otherWallet->getName() .  $otherWallet->id . '</option>';
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']);?>
                                </div>
                                <?= Html::endForm()?>
                            </li>
                        </ul>
                    </div>
                <?php }
            }
            else { ?>
                <div class="dropdown position-static">
                    <button class="button white big dropdown-toggle" type="button" data-toggle="dropdown" data-target="transfer-request-<?= $wallet->id?>">
                        <?=Yii::t('app', 'Money transfer')?>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu message-item transfer-request-form-container" id="transfer-request-<?= $wallet->id?>">
                        <li>
                            <?php
                            if(!empty($confirmationMethods)) {
                                $form = ActiveForm::begin([
                                    'type' => ActiveForm::TYPE_INLINE,
                                    'id' => 'transfer-request-form' . $wallet->id,
                                    'action' => Url::to(['/account/payment/transfer-request'])
                                ]); ?>
                                <?= $form->field($transferRequest, 'amount')?>
                                <?= $form->field($transferRequest, 'wallet_to_id')->textInput(['style' => 'min-width: 220px;'])?>
                                <?= $form->field($transferRequest, 'type')->dropDownList($confirmationMethods, [
                                    'prompt' => Yii::t('app', 'Select confirmation method'),
                                    'class' => 'confirmation-method'
                                ])?>
                                <?= $form->field($transferRequest, 'wallet_from_id')->hiddenInput(['value' => $wallet->id])->label(false);?>
                                <div class="form-group">
                                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary'])?>
                                </div>
                                <?php ActiveForm::end();
                            } else {
                                echo '<div class="alert alert-info" style="white-space: normal;">' .
                                    Yii::t('account',
                                        'You do not have any options for sending a confirmation code, please go to the {link} page and specify the Email or connect Skype or Telegram notifications.',
                                        ['link' => Html::a(Yii::t('account', 'Settings'), ['/account/service/account-settings'], ['data-pjax' => 0])]
                                    ) .
                                    '</div>';
                            } ?>
                        </li>
                    </ul>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <div class="inbox row margin-30">
        <div class="col-md-12">
            <div class="all-messages">
                <div class="over-table-viewgigs">
                    <table class="table-money">
                        <?php
                        $dataProvider = new ArrayDataProvider(['allModels' => $wallet->paymentHistories])?>
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'payment-item-new',
                            'viewParams' => ['walletLetter' => $wallet->getLetter(), 'walletName' => $wallet->getName(), 'currency_code' => $wallet->currency_code],
                            'options' => [
                                'tag' => false,
                                'id' => 'messages-list'
                            ],
                            'itemOptions' => [
                                'tag' => 'tr',
                                'class' => 'messages-row'
                            ],
                            'emptyTextOptions' => [
                                'tag' => 'tr',
                                'class' => 'messages-row'
                            ],
                            'emptyText' => "<td colspan='3'>" . Yii::t('yii', 'No results found.') . "</td>",
                            'layout' => "{items}"
                        ]) ?>
                    </table>
                    <?= LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>