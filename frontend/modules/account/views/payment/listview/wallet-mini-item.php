<?php

use common\components\CurrencyHelper;
use common\models\UserWallet;
use yii\web\View;

/**
 * @var UserWallet $wallet
 * @var View $this
 * @var bool $active
 */


?>

<li class="<?= $active ? 'active' : ''?>">
    <a href="#wallet-<?= $wallet->id?>" data-toggle="tab">
        <div class="wallet-circle">
            <div class="text-center">
                <?= $wallet->getLetter()?>
            </div>
        </div>
        <div class="wallet-title-info">
            <div class="title-sum">
                <span class="money">
                    <?= CurrencyHelper::format($wallet->currency_code, $wallet->balance)?>
                    <?php if ($wallet->currency_code === 'CTY') {?>
                        <span class="hint--top" data-hint="<?= Yii::t('account', 'This is Cointy bonus account') ?>">
                            <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                        </span>
                    <?php }?>
                </span>
            </div>
            <div class="title-number"><?= $wallet->getName() . $wallet->id?></div>
        </div>
    </a>
</li>