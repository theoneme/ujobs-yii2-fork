<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.07.2017
 * Time: 14:47
 */

use common\components\CurrencyHelper;
use common\models\UserWallet;
use common\models\WalletTransferRequest;
use frontend\components\BannerWidget;
use frontend\components\SocialShareWidget;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\data\ActiveDataProvider;
use frontend\models\WithdrawalRequestForm;

$this->title = Yii::t('account', 'Manage Payments');

/* @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserWallet[] $wallets
 * @var array $paymentData
 * @var array $paymentDataLong
 * @var array $confirmationMethods
 * @var array $currencyRatios
 * @var WithdrawalRequestForm $withdrawalForm
 * @var WalletTransferRequest $transferRequest
 */

$currencies = ['USD', 'EUR', 'RUB', 'UAH', 'CNY', 'GEL', 'CTY'];
?>

    <div class="inbox-background">
        <div class="profile">
            <div class="container-fluid">
                <?php Pjax::begin([
                    'id' => 'payments-pjax',
                    'scrollTo' => false,
                    'formSelector' => false
                ]); ?>
                <form class="search-gigs text-right">
                    <input type="search" placeholder="<?= Yii::t('account', 'Search My History...') ?>" name="search">
                    <a class="btn-search" onclick="$(this).closest('form').submit();"></a>
                </form>
                <h1 class="profileh text-left"><?= Yii::t('account', 'Manage Payments') ?></h1>

                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <ul class="ul-wallet no-markers">
                            <?php
                            foreach ($currencies as $code) {
                                $wallet = $wallets[$code] ?? null;
                                if ($wallet) {
                                    echo $this->render('listview/wallet-mini-item', ['wallet' => $wallet, 'active' => reset($wallets) === $wallet]);
                                }
                            } ?>
                        </ul>
                        <div class="prof-aside-block waller-referral">
                            <div class="col-md-12">
                                <p>
                                    <?= Yii::t('account', 'Cointy (CTY) is a bonus account to which we pay you bonuses for useful activities on our site.')?>
                                    <?= Html::a(Yii::t('app', 'Read more'), ['/news/view', 'alias' => 'bonusnaja-sistema-cointy'])?>
                                </p>
                            </div>
                            <div class="col-md-12"><p><?= Yii::t('app', 'Share with more friends and earn bonus now!')?></p></div>
                            <div class="work-share text-center">
                                <?= SocialShareWidget::widget([
                                    'url' => Url::to(['/site/index', 'invited_by' => Yii::$app->user->identity->getCurrentId()], true),
                                    'twitterMessage' => Yii::t('app', 'I\'m tired of keeping uJobs a secret. Thousands of services at an unbeatable value! Discover uJobs:'),
                                    'showLink' => true
                                ])?>
                            </div>
                        </div>
                        <?= BannerWidget::widget()?>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="tab-content">
                            <?php foreach ($currencies as $code) {
                                $wallet = $wallets[$code] ?? null;
                                if ($wallet) {
                                    echo $this->render('listview/wallet-item', [
                                        'wallet' => $wallet,
                                        'transferRequest' => $transferRequest,
                                        'withdrawalForm' => $withdrawalForm,
                                        'paymentData' => $paymentData,
                                        'confirmationMethods' => $confirmationMethods,
                                        'otherWallets' => array_diff_key($wallets, [$wallet->currency_code => 0, 'CTY' => 0]),
                                        'active' => reset($wallets) === $wallet
                                    ]);
                                }
                            } ?>
                        </div>
                    </div>
                </div>

                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <div class="modal fade transfer-confirm-modal" id="TransferConfirmModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('account', 'Operation confirmation')?></div>
                </div>
                <div class="modal-body">
                    <p>
                        <?= Yii::t('account', 'Transfer from wallet') . ' <span id="confirm-wallet-from"></span>'?>
                    </p>
                    <p>
                        <?= Yii::t('account', 'To wallet') . ' <span id="confirm-wallet-to"></span>'?>
                    </p>
                    <p>
                        <?= Yii::t('account', 'Amount') . ': <span id="confirm-amount"></span>'?>
                    </p>
                    <?= Html::beginForm('/account/payment/transfer-confirm', 'get', ['class' => 'formbot transfer-confirm-form']); ?>
                        <?= Html::hiddenInput('id', null, ['id' => 'transfer-confirm-id']); ?>
                        <?= Html::textInput('token', null, ['placeholder' => Yii::t('app', 'Confirmation code')]) ?>
                        <?= Html::submitButton(Yii::t('account', 'Confirm transfer'), ['class' => 'btn-big width100']) ?>
                    <?= Html::endForm(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
$amountError = Yii::t('app', 'Amount must be integer number');
$exchangeRateText = Yii::t('account', 'Exchange rate');
$paymentDataLong = json_encode($paymentDataLong);
$script = <<<JS
    var currencyRatios = $currencyRatios;
    var paymentDataLong = $paymentDataLong;
    $(document).on("submit", ".withdrawal-form-container form, .transfer-form", function(e) {
        e.preventDefault();
        var action = $(this).attr("action");
        var data = $(this).serialize();

        $.ajax({
            url: action,
            data: data,
            type: "post",
            success: function(response) {
                if (response.success === true) {
                    $.pjax.reload({container: "#payments-pjax"});
                    alertCall("top", "success", response.message);
                } else {
                    alertCall("top", "warning", response.message);
                }
            }
        });
    });

    $(document).on("submit", ".replenish-form, .transfer-form", function(e) {
        var amount = $(this).find(".amount-input").val();
        if (!amount.match(/^[0-9]+$/)){
            $(this).find(".amount-input").siblings(".help-block").html("$amountError");
            $(this).addClass("has-error");
            return false;
        }
    });
    
    $(document).on("submit", ".transfer-request-form-container form", function(e) {
        $.ajax({
            url: $(this).attr("action"),
            data: $(this).serialize(),
            type: "post",
            success: function(response) {
                if (response.success === true) {
                    $('#transfer-confirm-id').val(response.request_id);
                    $('#confirm-wallet-from').html(response.wallet_from);
                    $('#confirm-wallet-to').html(response.wallet_to);
                    $('#confirm-amount').html(response.transfer_amount);
                    $('#TransferConfirmModal').modal('show');
                } else {
                    alertCall("top", "warning", response.message);
                }
            }
        });
        return false;
    });
    
    $(document).on("submit", ".transfer-confirm-form", function(e) {
        var action = $(this).attr("action");
        var data = $(this).serialize();

        $.ajax({
            url: action,
            data: data,
            type: "post",
            success: function(response) {
                if (response.success === true) {
                    $('#TransferConfirmModal').modal('hide');
                    $.pjax.reload({container: "#payments-pjax"});
                    alertCall("top", "success", response.message);
                } else {
                    alertCall("top", "warning", response.message);
                }
            }
        });
        return false;
    });
    
    $(document).on("change", ".transfer-wallet-select", function() {
        var currencyFrom = $(this).closest("form").find(".amount-input").data('currency');
        var currencyTo = $(this).find("option:selected").data('currency');
        if (currencyTo) {
            $(this).closest("form").find(".ratio-input").prop('placeholder', currencyRatios[currencyTo][currencyFrom] + ' ' + currencyFrom + '/' + currencyTo);
            $(this).closest("form").find(".amount-to-input").data('currency', currencyTo);
        }
        else {
            $(this).closest("form").find(".ratio-input").prop('placeholder', '$exchangeRateText');
            $(this).closest("form").find(".amount-to-input").data('currency', '');
        }
        $('.amount-input').trigger('keyup');
    });
    
    $(document).on("keyup", ".amount-input", function() {
        var currencyFrom = $(this).data('currency');
        var currencyTo = $(this).closest('form').find('.amount-to-input').data('currency');
        if (currencyTo && $(this).val().match(/^[0-9]+$/)) {
            $(this).closest("form").find(".amount-to-input").val(Math.floor(parseFloat($(this).val()) / parseFloat(currencyRatios[currencyTo][currencyFrom])));
        }
        else {
            $(this).closest("form").find(".amount-to-input").val('');
        }
    });
    
    $(document).on("keyup", ".amount-to-input", function() {
        var currencyFrom = $(this).data('currency');
        var currencyTo = $(this).closest('form').find('.amount-input').data('currency');
        if (currencyTo && $(this).val().match(/^[0-9]+$/)) {
            $(this).closest("form").find(".amount-input").val(Math.ceil(parseFloat($(this).val()) * parseFloat(currencyRatios[currencyFrom][currencyTo])));
        }
        else {
            $(this).closest("form").find(".amount-input").val('');
        }
    });

    $(document).on("change", ".target-method", function() {
        $(this).closest("form").find(".target-account").val(paymentDataLong[$(this).val()]);
    });
JS;
$this->registerJs($script);