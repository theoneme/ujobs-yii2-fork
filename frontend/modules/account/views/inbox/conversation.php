<?php

use common\models\Conversation;
use common\models\Message;
use common\models\Offer;
use common\models\user\User;
use frontend\modules\account\models\ConversationSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var array $conversationData
 * @var ConversationSearch $conversationSearch
 * @var array $data
 * @var boolean $isSupport
 * @var array $languages
 * @var Message $message
 * @var Message $offerMessage
 * @var boolean $showOfferActions
 */
$this->title = Yii::t('account', 'Inbox');
$sendMessageUrl = Url::to(['/account/inbox-ajax/send-message', 'conversation_id' => $conversationData['conversationId'], 'support' => $isSupport ? true : null]);
?>
    <div class="inbox-background">
        <div class="profile">
            <div class="container-fluid">
                <?php Pjax::begin([
                    'id' => 'messages-pjax',
                    'scrollTo' => false
                ]); ?>
                <h1 class="profileh text-left"><?= Yii::t('account', 'Inbox') ?></h1>
                <div class="inbox row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="prof-aside-block welcome-left hidden-xs">
                            <div class="welcome-left-title">
                                <?= Yii::t('app', 'Don\'t have time to correspond?') ?>
                            </div>
                            <p>
                                <?= Yii::t('app', 'Please post a request and professionals will send you their proposals') ?>
                            </p>
                            <?= Html::a(Yii::t('account', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'btn-middle text-center', 'data-pjax' => 0]) ?>
                        </div>
                        <nav class="profa-menu">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target="#profa-menu">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="clearfix"></div>
                            <div id="profa-menu" class="collapse navbar-collapse">
                                <ul class="nav nav-tabs">
                                    <?php foreach (ConversationSearch::getStatusLabels() as $status => $label) { ?>
                                        <li class="<?= $conversationSearch->status === $status ? 'active' : '' ?>">
                                            <?= Html::a($label . " (" . $conversationSearch->getStatusCount($status) . ")",
                                                [$isSupport ? '/account/inbox/support' : '/account/inbox/index', 'status' => $status]
                                            ) ?>
                                        </li>
                                    <?php } ?>
                                    <li class="contact-support">
                                        <?= Html::a(
                                            Yii::t('app', 'Contact Support'),
                                            in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])
                                                ? ['/account/inbox/support']
                                                : ['/account/inbox/start-conversation', 'user_id' => Yii::$app->params['supportId']],
                                            ['data-pjax' => 0]
                                        ) ?>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="no-fair-play text-center hidden-xs">
                            <div class="no-pair-play-header">
                                <span>!</span>
                                <?= Yii::t('account', 'Customers'); ?>
                            </div>
                            <div class="no-pair-play-text">
                                <?= Yii::t('account', 'Dear customers! If you are offered to pay for a service outside of uJobs, remember that in this case the '); ?>
                                <?= Html::a(Yii::t('app', 'Fair play'), ['/page/static', 'alias' => 'fair-play'], ['data-pjax' => 0]) ?>
                                <?= Yii::t('account', 'ceases to operate, hence the probability of losing money and time increases.'); ?>
                            </div>
                        </div>
                        <?php if (Yii::$app->language === 'ru-RU') { ?>
                            <div class="hidden-xs y-inbox-block" id="yandex_rtb_R-A-243385-1"></div>
                        <?php } ?>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="tab-content allgigs inbox-margin0">
                            <div class="tab-pane fade in active" id="write">
                                <div class="over-table-viewgigs">
                                    <div class="write-block">
                                        <div class="mes-who">
                                            <div class="write-avatar">
                                                <div class="wavatar-block text-center">
                                                    <img src="<?= $conversationData['image'] ?>" alt="<?= Html::encode($conversationData['name']) ?>">
                                                </div>
                                                <?= $conversationData['route']
                                                        ? Html::a(Html::encode($conversationData['name']), $conversationData['route'], ['data-pjax' => 0])
                                                        : '<div class="write-avatar-name">' . Html::encode($conversationData['name']) . '</div>'
                                                ?>
                                            </div>
                                            <?php if (in_array($conversationData['type'], [Conversation::TYPE_DEFAULT, Conversation::TYPE_ORDER], true)) { ?>
                                                <div class="contact-user-info hidden-xs">
                                                    <div class="contact-user-info-item">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <?= Yii::t('account', 'Avg. Response Time') ?>
                                                        <span>5 <?= Yii::t('account', 'Hrs') ?>.</span>
                                                    </div>
                                                    <?php if (count($languages) > 0) { ?>
                                                        <div class="contact-user-info-item">
                                                            <i class="fa fa-comments" aria-hidden="true"></i>
                                                            <?= Yii::t('account', 'Speaks in languages:') . " "; ?>
                                                            <?php foreach ($languages as $language => $level) {
                                                                echo Html::tag('span', $language) . ($level ? " (" . Yii::t('account', $level) . ")" : '');
                                                            } ?>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="contact-user-info-item">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                        <?= Yii::t('account', 'Positive ratings') ?> <span>98%</span>
                                                    </div>
                                                </div>
                                            <?php } else if ($conversationData['type'] === Conversation::TYPE_GROUP) { ?>
                                                <div class="contact-user-info hidden-xs">
                                                    <div class="contact-user-info-item">
                                                        <i class="fa fa-users" aria-hidden="true"></i>
                                                        <?= Yii::t('account', 'Conversation members') ?>:
                                                        <?= implode(', ', $conversationData['members']) ?>
                                                    </div>
                                                    <?php if ($conversationData['isDeleted'] === false) { ?>
                                                        <div class="contact-user-info-item">
                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                            <?= Html::a(Yii::t('account', 'Add member to chat'), '#', [
                                                                'data-action' => 'add-member-to-group-chat',
                                                                'data-id' => $conversationData['conversationId']
                                                            ]) ?>
                                                        </div>
                                                        <div class="contact-user-info-item">
                                                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                                                            <?= Html::a(Yii::t('account', 'Leave group chat'), ['/account/inbox/leave-group-chat', 'id' => $conversationData['conversationId']], ['data-pjax' => 0]) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } else if ($conversationData['type'] === Conversation::TYPE_INFO){ ?>
                                                <div class="contact-user-info hidden-xs">
                                                    <div class="contact-user-info-item">
                                                        <i class="fa fa-users" aria-hidden="true"></i>
                                                        <?= Yii::t('account', 'Conversation members') ?>:
                                                        <?= implode(', ', $conversationData['members']) ?>
                                                    </div>
                                                    <?php if ($conversationData['isDeleted'] === false) { ?>
                                                        <div class="contact-user-info-item">
                                                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                                                            <?= Html::a(Yii::t('account', 'Leave info chat'), ['/account/inbox/leave-group-chat', 'id' => $conversationData['conversationId']], ['data-pjax' => 0]) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!--                                        --><? //= Html::a(Yii::t('account', 'Nevermore'), '#', [
                                        //                                            'data-id' => $conversationData['conversationId'],
                                        //                                            'class' => 'load-more'
                                        //                                        ]) ?>
                                        <?php if (!empty($data)) { ?>
                                            <?php foreach ($data['items'] as $item) { ?>
                                                <?= $this->render('conversation_message', [
                                                    'model' => $item,
                                                    'from_id' => $conversationSearch->from_id
                                                ]); ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($conversationData['isDeleted'] === false && $conversationData['offer'] !== null && $buttons = $conversationData['offer']->getStatusButtons()) { ?>
                                            <div class="conversation-message offer-actions" style="text-align: center;">
                                                <div class="clearfix">
                                                    <?= $buttons ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($conversationData['isDeleted'] === false) { ?>
                                            <div class="message-block">
                                                <?php if (!$conversationData['offer'] || in_array($conversationData['offer']->status, [Offer::STATUS_SENT, Offer::STATUS_ACCEPTED])) { ?>
                                                    <div class="message-item">
                                                        <?php if (in_array($conversationData['type'], [Conversation::TYPE_DEFAULT, Conversation::TYPE_ORDER], true)) { ?>
                                                            <?php if ($conversationData['isOnline']) { ?>
                                                                <div class="user-online text-right">
                                                                    <?= Html::encode($conversationData['name']) ?>
                                                                    <span class="status online"><?= Yii::t('account', 'Online') ?></span>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="user-offline text-right">
                                                                    <?= Html::encode($conversationData['name']) ?>
                                                                    <span class="status offline"><?= Yii::t('account', 'Offline') ?></span>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>

                                                        <?php $form = ActiveForm::begin([
                                                            'action' => $sendMessageUrl,
                                                            'options' => [
                                                                'id' => 'conversation-form',
                                                                'enctype' => 'multipart/form-data',
//                                                            'data-pjax' => 1,
                                                                'class' => 'row send-message',
                                                            ]
                                                        ]); ?>
                                                        <div class="col-md-12 white-mes-bg">
                                                            <?= $form->field($message, 'message', [
                                                                'template' => "{input}\n{error}",
                                                            ])->textarea([
                                                                'class' => 'readsym',
                                                                'id' => 'conversation-message-message',
                                                                'placeholder' => Yii::t('account', 'Type your message here...')
                                                            ]) ?>
                                                            <div class="white-mes-bottom text-left row">
                                                                <div class="counttext">
                                                                    <span class="ctspan">0</span> / 1200
                                                                </div>
                                                                <div class="fileup">
                                                                    <label class="file-upload">
                                                                    <span class="button text-center hint--top-right"
                                                                          data-hint="<?= Yii::t('account', 'Attach files. {mb}MB max.', ['mb' => 30]) ?>">
                                                                        <i class="fa fa-paperclip"
                                                                           aria-hidden="true"></i>
                                                                    </span>
                                                                        <?= $form->field($message, 'uploadedFiles[]')->fileInput([
                                                                            'id' => 'message-uf',
                                                                            'class' => 'file-input',
                                                                            'multiple' => true
                                                                        ])->label(false) ?>
                                                                        <mark style="background-color: #fff"></mark>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?= Html::submitInput(Yii::t('account', 'Send'), ['class' => 'btn-small bright']); ?>
                                                        <?php if ($showOfferActions === true) { ?>
                                                            <?= Html::a(Yii::t('account', 'Offer a Job'), null, [
                                                                'class' => 'btn-small text-center grey offer-job modal-dismiss',
                                                                'data-toggle' => 'modal',
                                                                'data-target' => '#ModalOffer'
                                                            ]); ?>
                                                            <?= Html::a(Yii::t('account', 'Request a Job'), null, [
                                                                'class' => 'btn-small text-center grey offer-job modal-dismiss',
                                                                'data-toggle' => 'modal',
                                                                'data-target' => '#ModalRequest'
                                                            ]); ?>
                                                        <?php } ?>
                                                        <?php ActiveForm::end(); ?>
                                                    </div>
                                                    <div class="spinner-bg">
                                                        <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } else { ?>
                                            <div class="conversation-message" style="border: 1px solid #dddddd;">
                                                <div class="message-body">
                                                    <?= Yii::t('account', 'You have left this conversation') ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($showOfferActions === true) { ?>
                    <?php if (!isset($conversationData['offer']) || $conversationData['offer']->status !== Offer::STATUS_PAID) { ?>
                        <div class="modal fade" id="ModalOffer" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" style="width: 600px;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <div class="form-title"><?= Yii::t('account', 'Individual job') ?></div>
                                    </div>
                                    <div class="modal-body">
                                        <?php $form = ActiveForm::begin([
                                            'id' => 'offer-form',
                                            'action' => ['/account/inbox-ajax/offer', 'user_id' => $conversationData['userId']],
                                            'options' => [
                                                'enctype' => 'multipart/form-data',
                                                'class' => 'row send-message',
                                                'data-pjax' => 0
                                            ]
                                        ]); ?>
                                        <div>
                                            <div class="chover">
                                                <?= Html::radio('Offer[type]', false, ['class' => 'radio-checkbox', 'value' => Offer::TYPE_REQUEST, 'id' => 'offer_type_1']) ?>
                                                <label for="offer_type_1"><?= Yii::t('app', 'I am customer and I want to request individual job.') ?></label>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('Offer[type]', true, ['class' => 'radio-checkbox', 'value' => Offer::TYPE_OFFER, 'id' => 'offer_type_2']) ?>
                                                <label for="offer_type_2"><?= Yii::t('app', 'I am worker and I want to offer individual job.') ?></label>
                                            </div>
                                        </div>
                                        <?= $form->field($offer, 'subject', [
                                            'template' => "{input}\n{error}",
                                        ])->textInput([
                                            'id' => 'offer-offer-subject',
                                            'placeholder' => Yii::t('account', 'Set offer subject')
                                        ]) ?>
                                        <div class="white-mes-bg">
                                            <?= $form->field($offerMessage, 'message', ['template' => "{input}\n{error}"])->textarea([
                                                'class' => 'readsym',
                                                'id' => 'offer-message-message',
                                                'placeholder' => Yii::t('account', 'Type your message here...')
                                            ]) ?>
                                            <div class="white-mes-bottom text-left clearfix">
                                                <div class="counttext">
                                                    <span class="ctspan">0</span> / 1200
                                                </div>
                                                <div class="fileup">
                                                    <label class="file-upload">
                                                    <span class="button text-center hint--top-right"
                                                          data-hint="<?= Yii::t('account', 'Attach files. {mb}MB max.', ['mb' => 30]) ?>">
                                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                    </span>
                                                        <?= $form->field($offerMessage, 'uploadedFiles[]')->fileInput([
                                                            'id' => 'offer-uf',
                                                            'class' => 'file-input',
                                                            'multiple' => true
                                                        ])->label(false) ?>
                                                        <mark style="background-color: #fff"></mark>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?= $form->field($offer, 'price', [
                                            'template' => "{input}\n{error}",
                                            'options' => ['class' => 'form-group offer-price']
                                        ])->textInput([
                                            'id' => 'offer-offer-price',
                                            'placeholder' => Yii::t('account', 'Price')
                                        ]) ?>
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                                <div class="spinner-bg">
                                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="ModalRequest" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" style="width: 600px;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <div class="form-title"><?= Yii::t('account', 'Individual job') ?></div>
                                    </div>
                                    <div class="modal-body">
                                        <?php $form = ActiveForm::begin([
                                            'id' => 'request-form',
                                            'action' => ['/account/inbox-ajax/request', 'user_id' => $conversationData['userId']],
                                            'options' => [
                                                'enctype' => 'multipart/form-data',
                                                'class' => 'row send-message',
                                            ]
                                        ]); ?>
                                        <div>
                                            <div class="chover">
                                                <?= Html::radio('Offer[type]', true, ['class' => 'radio-checkbox', 'value' => Offer::TYPE_REQUEST, 'id' => 'request_type_1']) ?>
                                                <label for="request_type_1"><?= Yii::t('app', 'I am customer and I want to request individual job.') ?></label>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('Offer[type]', false, ['class' => 'radio-checkbox', 'value' => Offer::TYPE_OFFER, 'id' => 'request_type_2']) ?>
                                                <label for="request_type_2"><?= Yii::t('app', 'I am worker and I want to offer individual job.') ?></label>
                                            </div>
                                        </div>
                                        <?= $form->field($offer, 'subject', [
                                            'template' => "{input}\n{error}",
                                        ])->textInput([
                                            'id' => 'offer-request-subject',
                                            'placeholder' => Yii::t('account', 'Set request subject')
                                        ]) ?>
                                        <div class="white-mes-bg">
                                            <?= $form->field($offerMessage, 'message', ['template' => "{input}\n{error}"])->textarea([
                                                'class' => 'readsym',
                                                'id' => 'request-message-message',
                                                'placeholder' => Yii::t('account', 'Type your message here...')
                                            ]) ?>
                                            <div class="white-mes-bottom text-left clearfix">
                                                <div class="counttext">
                                                    <span class="ctspan">0</span> / 1200
                                                </div>
                                                <div class="fileup">
                                                    <label class="file-upload">
                                                    <span class="button text-center hint--top-right"
                                                          data-hint="<?= Yii::t('account', 'Attach files. {mb}MB max.', ['mb' => 30]) ?>">
                                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                    </span>
                                                        <?= $form->field($offerMessage, 'uploadedFiles[]')->fileInput([
                                                            'id' => 'request-uf',
                                                            'class' => 'file-input',
                                                            'multiple' => true
                                                        ])->label(false) ?>
                                                        <mark style="background-color: #fff"></mark>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?= $form->field($offer, 'price', [
                                            'template' => "{input}\n{error}",
                                            'options' => ['class' => 'form-group offer-price']
                                        ])->textInput([
                                            'id' => 'request-offer-price',
                                            'type' => 'number',
                                            'placeholder' => Yii::t('account', 'Budget')
                                        ]) ?>
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                                <div class="spinner-bg">
                                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>

<?php $leaveGroupChatUrl = Url::to(['/messenger/ajax/leave-group-chat', 'id' => $conversationData['conversationId']]);
$script = <<<JS
    let conversationMessage = $('.conversation-message');

    if (conversationMessage.length) {
        let target = conversationMessage.last().offset().top;
        $('body,html').animate({
            scrollTop: target - 105
        }, 800);
    }
    $(document).on('change', '.file-input', function() {
        let names = $.map($(this).prop('files'), function(val) {
            return val.name;
        });
        $(this).parent().siblings('mark').html('(' + names.length + ') ' + names.join(', '));
    });

    $(document).on('beforeSubmit', '#offer-form, #request-form', function() {
        let self = $(this);
        let formData = new FormData($(this)[0]);
        self.closest('.modal').find('.spinner-bg').show();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(result) {
                if (result.success) {
                    alertCall('top', 'success', result.message);
                    if (result.redirect) {
                        $.pjax.reload({
                            container: '#messages-pjax',
                            url: result.redirect
                        });
                    } else {
                        $.pjax.reload({
                            container: '#messages-pjax'
                        });
                    }
                    self.closest('.modal').modal('hide');
                } else {
                    alertCall('top', 'error', result.message);
                }
                self.closest('.modal').find('.spinner-bg').hide();
            }
        });
        
        return false;
    });

    $(document).on('pjax:success', function() {
        conversationMessage = $('.conversation-message');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        if (conversationMessage.length) {
            $('body,html').animate({
                scrollTop: conversationMessage.last().offset().top - 105
            }, 800);
        }
        if ($("#yandex_rtb_R-A-243385-1").length) {
            Ya.Context.AdvManager.render({
                blockId: "R-A-243385-1",
                renderTo: "yandex_rtb_R-A-243385-1",
                async: true
            });
        }
        if ($("#yandex_rtb_R-A-243363-1").length) {
            Ya.Context.AdvManager.render({
                blockId: "R-A-243363-1",
                renderTo: "yandex_rtb_R-A-243363-1",
                async: true
            });
        }
    });

    $(document).on('beforeSubmit', '#conversation-form', function() {
        let self = $(this);
        let formData = new FormData($(this)[0]);
        $('.message-block .spinner-bg').show();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(result) {
                if (result.success) {
                    alertCall('top', 'success', result.message);
                    if (result.redirect) {
                        $.pjax.reload({
                            container: '#messages-pjax',
                            url: result.redirect
                        });
                    } else {
                        if ($('.conversation-message:not(.offer-actions)').length) {
                            $('.conversation-message:not(.offer-actions)').last().after(result.item);
                        }
                        else {
                            $('.mes-who').after(result.item);
                        }
                    }
                } else {
                    alertCall('top', 'error', result.message);
                }
                self[0].reset();
                $('.message-block .spinner-bg').hide();
            }
        });
        return false;
    });

    $(document).on('click', 'a.offer-action', function() {
        $.post($(this).attr('href'), function() {
            $.pjax.reload({
                container: '#messages-pjax'
            });
        });
        return false;
    });
JS;

$this->registerJs($script);