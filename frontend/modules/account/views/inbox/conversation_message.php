<?php
use common\models\Message;
use common\models\Offer;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Message $model
 * @var integer $from_id
 */

echo $this->render('type/' . $model->getTypeView(), [
    'model' => $model,
    'from_id' => $from_id
]);