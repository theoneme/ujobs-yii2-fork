<?php

use common\models\user\User;
use frontend\modules\account\models\ConversationSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ConversationSearch $model
 * @var ActiveDataProvider $dataProvider
 * @var boolean $isSupport
 */

$this->title = Yii::t('account', 'Inbox');
$from_id = $isSupport ? Yii::$app->params['supportId'] : Yii::$app->user->identity->getId();
?>

    <div class="inbox-background">
        <div class="profile">
            <div class="container-fluid">
                <?php Pjax::begin([
                    'id' => 'messages-pjax',
                    'scrollTo' => false
                ]); ?>
                <form class="search-gigs text-right">
                    <input type="search" placeholder="<?= Yii::t('account', 'Search My History...') ?>" name="search">
                    <a class="btn-search" onclick="$(this).closest('form').submit();"></a>
                </form>
                <h1 class="profileh text-left"><?= Yii::t('account', 'Inbox') ?></h1>
                <div class="inbox row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="prof-aside-block welcome-left hidden-xs">
                            <div class="welcome-left-title">
                                <?= Yii::t('app', 'Don\'t have time to correspond?') ?>
                            </div>
                            <p>
                                <?= Yii::t('app', 'Please post a request and professionals will send you their proposals') ?>
                            </p>
                            <?= Html::a(Yii::t('account', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'btn-middle text-center', 'data-pjax' => 0]) ?>
                        </div>
                        <nav class="profa-menu">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target="#profa-menu">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="clearfix"></div>
                            <div id="profa-menu" class="collapse navbar-collapse">
                                <ul class="nav nav-tabs">
                                    <?php foreach (ConversationSearch::getStatusLabels() as $status => $label) { ?>
                                        <li class="<?= $model->status === $status ? 'active' : '' ?>">
                                            <?= Html::a($label . " (" . $model->getStatusCount($status) . ")",
                                                [$isSupport ? '/account/inbox/support' : '/account/inbox/index', 'status' => $status]
                                            ) ?>
                                        </li>
                                    <?php } ?>
                                    <li class="contact-support">
                                        <?= Html::a(
                                            Yii::t('app', 'Contact Support'),
                                            in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])
                                                ? ['/account/inbox/support']
                                                : ['/account/inbox/start-conversation', 'user_id' => Yii::$app->params['supportId']],
                                            ['data-pjax' => 0]
                                        ) ?>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="no-fair-play text-center hidden-xs">
                            <div class="no-pair-play-header">
                                <span>!</span>
                                <?= Yii::t('account', 'Customers'); ?>
                            </div>
                            <div class="no-pair-play-text">
                                <?= Yii::t('account', 'Dear customers! If you are offered to pay for a service outside of uJobs, remember that in this case the '); ?>
                                <?= Html::a(Yii::t('app', 'Fair play'), ['/page/static', 'alias' => 'fair-play'], ['data-pjax' => 0]) ?>
                                <?= Yii::t('account', 'ceases to operate, hence the probability of losing money and time increases.'); ?>
                            </div>
                        </div>
                        <?php if (Yii::$app->language === 'ru-RU') { ?>
                            <div class="hidden-xs y-inbox-block" id="yandex_rtb_R-A-243363-1"></div>
                        <?php } ?>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="all-messages inbox-margin0 hidden-xs">
                            <div class="over-table-viewgigs">
                                <div class="head-viewgigs row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <?= mb_strtoupper($model->getStatusLabel()) ?>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <a class="grey-but text-center del-gigs delete-conversations" data-pjax="0">
                                            <?= mb_strtoupper(Yii::t('account', 'Delete')) ?>
                                        </a>
                                    </div>
                                </div>
                                <table class="table-viewgigs">
                                    <tr>
                                        <td>
                                            <div class="chover">
                                                <input class="gigall" id="gigall1" type="checkbox" name="gigall"/>
                                                <label for="gigall1"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <?= mb_strtoupper(Yii::t('account', 'Conversation')) ?>
                                        </td>
                                        <td>
                                            <?= mb_strtoupper(Yii::t('account', 'Last message')) ?>
                                        </td>
                                        <td>
                                            <?= mb_strtoupper(Yii::t('account', 'Updated')) ?>
                                        </td>
                                    </tr>
                                    <?php if (!empty($data)) { ?>
                                        <?php foreach ($data['items'] as $item) { ?>
                                            <tr class="messages-row">
                                                <?= $this->render('index_message', [
                                                    'model' => $item
                                                ]); ?>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <tr class="messages-row">
                                            <td colspan="4"><?= Yii::t('yii', 'No results found.') ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                <?php if (!empty($data)) { ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $data['pagination'],
                                    ]); ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="messages-mobile visible-xs">
                            <?php if (!empty($data)) { ?>
                                <?php foreach ($data['items'] as $item) { ?>
                                    <?= $this->render('index_message_mobile', [
                                        'model' => $item
                                    ]); ?>
                                <?php } ?>
                                <?= LinkPager::widget([
                                    'pagination' => $data['pagination'],
                                ]); ?>
                            <?php } else { ?>
                                <div class='imobile-empty'><?= Yii::t('yii', 'No results found.') ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
<?php
$starUrl = Url::to(['/account/inbox-ajax/star']);
$deleteUrl = Url::to(['/account/inbox-ajax/delete']);
$deleteMessage = Yii::t('account', 'Conversation deleted');
$script = <<<JS
    $(document).on('pjax:success', function(event, data, status, xhr, options) {
        if ($("#yandex_rtb_R-A-243363-1").length){
            Ya.Context.AdvManager.render({
                blockId: "R-A-243363-1",
                renderTo: "yandex_rtb_R-A-243363-1",
                async: true
            });
        }
        if ($("#yandex_rtb_R-A-243385-1").length) {
            Ya.Context.AdvManager.render({
                blockId: "R-A-243385-1",
                renderTo: "yandex_rtb_R-A-243385-1",
                async: true
            });
        }
    });
    
    $(document).on('click', '.inbox-star', function(){
        var id = $(this).data('id');
        var self = $(this);
        $.ajax({
            url: '$starUrl',
            type: 'post',
            data: {id: id},
            success: function() {
                self.toggleClass('starred');
            },
        });
        return false;
    });

    $(document).on('click', '.delete-conversations', function(){
        var ids = $('.table-viewgigs .messages-row input:checkbox:checked').map(function(){
          return $(this).val();
        }).get();
        $.ajax({
            url: '$deleteUrl',
            type: 'post',
            data: {ids: ids},
            success: function(response) {
                $.pjax.reload({container: '#messages-pjax'});
                alertCall('top', 'success', '$deleteMessage');
            },
        });
        return false;
    });
JS;

$this->registerJs($script);