<?php

use common\modules\store\assets\StoreAsset;
use yii\helpers\Url;

/**
 * @var $paymentMethods array
 * @var $offer_id integer
 * @var $currency_code string
 * @var $total integer
 */

StoreAsset::register($this);
?>

<div class="offer-payment-form">
    <?= $this->render('@common/modules/store/views/checkout/checkout-payment-methods', [
        'paymentMethods' => $paymentMethods,
        'checkoutUrl' => Url::to(['/store/checkout/offer']),
        'total' => $total,
        'currency_code' => $currency_code,
        'params' => ['id' => $offer_id],
    ]) ?>
</div>
