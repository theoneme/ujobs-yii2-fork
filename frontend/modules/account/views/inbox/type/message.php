<?php
use common\models\Message;
use common\models\Offer;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var Message $model
 * @var integer $from_id
 */

if ($model->user_id == $from_id) {
    $from = Yii::t('account', 'Me');
    $whose = 'my';
} else {
    $from = $model->user->getSellerName();
    $whose = 'their';
}
?>

<div class="conversation-message <?= $whose ?>" id="message-<?= $model->id ?>">
    <div class="message-body">
        <div class="message-avatar">
            <?= Html::a(Html::img($model->user->getThumb()), $model->user->getSellerUrl(), [
                'data-pjax' => 0, 'class' => 'text-center'
            ]) ?>
        </div>
        <div class="message-text dont-break-out">
            <h4>
                <?= Html::a($from, $model->user->getSellerUrl(), [
                    'data-pjax' => 0
                ]) ?>
            </h4>
            <?= nl2br(e($model->message)); ?>
        </div>
    </div>
    <div class="message-time text-right">
        <?= $model->created_at ?>
    </div>
    <?php if (count($model->attachments)) { ?>
        <div class="message-attachments-label">
            <?= Yii::t('app', 'Attachments') ?>
        </div>
        <div class="message-attachments">
            <?php foreach ($model->attachments as $attachment) { ?>
                <div class="message-attachment">
                    <div class="file-container">
                        <?= Html::a(Html::img($attachment->getThumb()),
                            Yii::$app->mediaLayer->tryLoadFromAws($attachment->content), [
                            'download' => true,
                            'data-pjax' => 0
                        ]) ?>
                    </div>
                    <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i>' . basename($attachment->content),
                        Yii::$app->mediaLayer->tryLoadFromAws($attachment->content), [
                            'download' => true,
                            'data-pjax' => 0
                        ]) ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if ($model->offer !== null) { ?>
        <div class="message-details">
            <div class="details-text">
                <?= Yii::t('account', 'Individual job ' . strtolower(Offer::$typeString[$model->offer->type]) . ' description') ?>
            </div>
            <div class="mes-spec-list">
                <div class="mes-spec-item">
                    <i class="fa fa-database"></i>
                    <?= Yii::t('account', 'Budget') . ': ' . $model->offer->getPrice() ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>