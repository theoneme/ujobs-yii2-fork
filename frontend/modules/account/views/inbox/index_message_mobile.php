<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.05.2017
 * Time: 13:41
 */

use common\models\Message;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Message $model
 * @var string $status
 * @var integer $user_id
 * @var boolean $support
 */

?>

<div class="imobile-item">
    <div class="imobile-avatar">
        <?= Html::a(Html::img($model['image']), $model['route'], [
            'data-pjax' => 0
        ]) ?>
    </div>
    <div class="imobile-info">
        <div class="imobile-sender">
            <?= Html::a(($model['icon'] ? Html::tag('i', '&nbsp;', ['class' => $model['icon']]) : null) . $model['name'], $model['route'], [
                'data-pjax' => 0
            ]) ?>
        </div>
        <div class="imobile-shortmes">
            <?php if (strlen($model['text']) > 1) { ?>
                <a href="<?= Url::to($model['route']) ?>" data-pjax="0">
                    <?= StringHelper::truncate($model['text'], 110) ?>
                </a>
            <?php } else if (!empty($model['attachments'])) { ?>
                <a href="<?= Url::to($model['route']) ?>" data-pjax="0">
                    <?= Yii::t('app', 'Attachment') ?>
                </a>
            <?php } else if (strpos($model['text'], '.') !== false) { ?>
                <a href="<?= Url::to($model['route']) ?>" data-pjax="0">
                    <?= Yii::t('app', 'System message (individual job)') ?>
                </a>
            <?php } ?>
        </div>
    </div>
    <span class="hint--top imobile-trash" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="#">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
    </span>
</div>