<?php
use frontend\dto\ConversationItemDTO;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var ConversationItemDTO $model
 * @var string $status
 * @var integer $user_id
 * @var boolean $support
 */

?>
<td class="check-star">
    <?php if ($model['lastMessageId'] !== null) { ?>
        <div class="chover">
            <input id="message-<?= $model['lastMessageId'] ?>" type="checkbox" name="message_ids"
                   value="<?= $model['conversationId'] ?>"/>
            <label for="message-<?= $model['lastMessageId'] ?>"></label>
        </div>
    <?php } ?>

    <div class="inbox-star <?= $model['starred'] ? 'starred' : '' ?>" data-id="<?= $model['conversationId'] ?>">
        <i class="fa fa-star-o fa-lg" aria-hidden="true"></i>
        <i class="fa fa-star fa-lg" aria-hidden="true"></i>
    </div>
</td>
<td class="inbox-sender">
    <div class="sender-avatar">
        <?= Html::a(Html::img($model['image']), $model['route'], [
            'class' => 'tg-img text-center',
            'data-pjax' => 0
        ]) ?>
    </div>
    <div class="sender-name">
        <?= Html::a(($model['icon'] ? Html::tag('i', '&nbsp;', ['class' => $model['icon']]) : null) . $model['name'], $model['route'], [
            'class' => 'tg-name',
            'data-pjax' => 0
        ]) ?>
    </div>
</td>
<td>
    <?php if (strlen($model['text']) > 1) { ?>
        <a href="<?= Url::to($model['route']) ?>" class="tg-name-new dont-break-out flex flex-v-center" data-pjax="0">
            <div class="sender-mini-avatar"><?= Html::img($model['messageImage']) ?></div>
            <div><?= StringHelper::truncate($model['text'], 110) ?></div>
        </a>
    <?php } else if (!empty($model['attachments'])) { ?>
        <a href="<?= Url::to($model['route']) ?>" class="tg-name-new dont-break-out flex flex-v-center" data-pjax="0">
            <div class="sender-mini-avatar"><?= Html::img($model['messageImage']) ?></div>
            <div><?= Yii::t('app', 'Attachment') ?></div>
        </a>
    <?php } else if (strpos($model['text'], '.') !== false) { ?>
        <a href="<?= Url::to($model['route']) ?>" class="tg-name-new dont-break-out flex flex-v-center" data-pjax="0">
            <div class="sender-mini-avatar"><?= Html::img($model['messageImage']) ?></div>
            <div><?= Yii::t('app', 'System message (individual job)') ?></div>
        </a>
    <?php } ?>
</td>
<td>
    <?= $model['createdAt'] ?>
</td>
