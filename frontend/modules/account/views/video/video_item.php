<?php

use common\models\Video;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model Video
 * @var $index integer
 * @var boolean $editable
 */

?>

<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img($model->user->getThumb('catalog'),['alt' => $model->user->getSellerName(), 'title' => $model->user->getSellerName()]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'by') ?>
            <?= Html::a(Html::encode($model->user->getSellerName()), $model->user->getSellerUrl(), ['data-pjax' => 0]) ?>
        </div>
    </div>
</div>
<div class="bf-alias" data-pjax="0">
    <div class="bf-img bf-video-img">
        <?= Html::img($model->getThumb(), [
            'alt' => $model->title,
            'title' => $model->title,
        ]) ?>
    </div>
    <div class="bf-descr">
        <?= Html::encode($model->title) ?>
    </div>
</div>
<div class="block-bottom">
    <div class="markers"></div>
    <?php echo Html::a($model->getPriceString(),
        ["/videos/view", 'alias' => $model->alias],
        ['class' => 'block-price', 'data-pjax' => 0]
    ) ?>
</div>
<?php if ($editable === true) { ?>
    <div class="job-owner-bg"></div>
    <div class="job-owner-buttons">
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Edit'),
                ["/video/update", 'id' => $model->id],
                ['class' => 'btn-middle text-center', 'data-pjax' => 0]
            ) ?>
        </div>
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'View'),
                ["/video/view", 'alias' => $model->alias],
                ['class' => 'btn-middle blue text-center', 'data-pjax' => 0]
            ) ?>
        </div>
        <div class="job-owner-button">
            <?= Html::a('<i class="fa fa-times" aria-hidden="true"></i>&nbsp;' . Yii::t('account', 'Delete'),
                ["/video/delete", 'id' => $model->id],
                ['class' => 'btn-middle red text-center', 'data-method' => 'post', 'data-confirm' => Yii::t('account', 'Are you sure?'), 'data-pjax' => 0]
            ) ?>
        </div>
    </div>
<?php } ?>