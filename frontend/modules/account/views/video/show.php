<?php

use common\models\user\Profile;
use frontend\assets\WorkerAsset;
use frontend\modules\account\components\ProfileSidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;
use yii\widgets\LinkPager;
use yii\widgets\ListView;


/**
 * @var View $this
 * @var Profile $profile
 * @var $reviewDataProvider ActiveDataProvider
 * @var $videosDataProvider ActiveDataProvider
 * @var $contactAccessInfo array
 */
WorkerAsset::register($this);

$isOwner = hasAccess($profile->user_id);

?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($profile->gravatar_email) { ?>
                            <?= Html::img($profile->getThumb(), ['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($profile->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($profile->user->isOnline()) { ?>
                                <span class="status isonline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                            <?php } else { ?>
                                <span class="status isoffline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                            <?php } ?>
                        </div>
                        <div class="wmh-rate flex">
                            <?= $this->render('@frontend/widgets/views/rating', [
                                'rating' => $profile->rating / 2,
                                'size' => 14
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <?php if (isGuest()) {
                        echo Html::a(Yii::t('account', 'Contact a freelancer'), null, [
                            'class' => 'button green no-size',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalLogin'
                        ]);
                    } else {
                        if (hasAccess($profile->user_id)) {
                            echo Html::a(Yii::t('account', 'View As A Owner'),
                                ['/account/profile/show', 'id' => $profile->user_id],
                                ['class' => 'button green no-size']
                            );
                        } else {
                            echo Html::a(Yii::t('account', 'Contact a freelancer'),
                                ['/account/inbox/start-conversation', 'user_id' => $profile->user_id],
                                ['class' => 'button green no-size']
                            );
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= ProfileSidebar::widget([
                    'profile' => $profile,
                    'editable' => false,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'videos' => $videosDataProvider->totalCount
                            ? ['link' => '#videos', 'label' => Yii::t('app', 'Videos') . " ({$videosDataProvider->totalCount})"]
                            : false,
                        'reviews' => $reviewDataProvider->totalCount
                            ? ['link' => '#reviews', 'label' => Yii::t('app', 'Reviews') . " ({$reviewDataProvider->totalCount})"]
                            : false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs">
                <div class="hidden-xs">
                    <div class="gigs-title" id="video-list-header">
                        <?= Yii::t('account', 'Videos from user {user}', ['user' => $profile->getSellerName()]) ?>
                    </div>
                        <?= ListView::widget([
                            'dataProvider' => $videosDataProvider,
                            'itemView' => '@frontend/components/catalog/views/video-list',
                            'options' => [
                                'class' => 'max-width gigs-list relative',
                                'id' => 'pro-videos',
                            ],
                            'itemOptions' => [
                                'class' => 'bf-item bf-item-video'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any videos") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $videosDataProvider->pagination,
                        ]) ?>
                </div>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="videos">
                        <div class="gigs-title"><?= Yii::t('account', 'Videos from user {user}', ['user' => $profile->getSellerName()]) ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $videosDataProvider,
                            'itemView' => '@frontend/components/catalog/views/video-list-mobile',
                            'options' => [
                                'class' => 'max-width gigs-list relative',
                                'id' => 'pro-videos-mobile',
                            ],
                            'itemOptions' => [
                                'class' => 'mob-work-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any videos") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ($contactAccessInfo['allowed'] == false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>

<?php
$script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100/menuMobileItems.length+'%';
    let anchor = window.location.hash;
    
    menuMobileItems.css('width',perc);

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if($(window).width()>992){
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display','block');
            } else {
                $('.worker-mobile-head').css('display','none');
            }
        }
    });
    $(".menu-worker-mobile").mCustomScrollbar({
        axis:"x" 
    });
    $(document).on('click','.menu-worker-mobile a',function(){
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        
        return false;
    });
    
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script);