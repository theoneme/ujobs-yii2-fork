<?php

use common\models\user\Profile;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use frontend\modules\account\components\ProfileSidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/**
 * @var View $this
 * @var Profile $profile
 * @var $videosDataProvider ActiveDataProvider
 * @var $reviewDataProvider ActiveDataProvider
 * @var $contactAccessInfo array
 */

WorkerAsset::register($this);
SelectizeAsset::register($this);

?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($profile->gravatar_email) { ?>
                            <?= Html::img($profile->getThumb(), ['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($profile->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($profile->user->isOnline()) { ?>
                                <span class="status isonline"><i class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                            <?php } else { ?>
                                <span class="status isoffline"><i class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                            <?php } ?>
                        </div>
                        <div class="wmh-rate flex">
                            <?= $this->render('@frontend/widgets/views/rating', [
                                'rating' => $profile->rating / 2,
                                'size' => 14
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <?= Html::a(Yii::t('account', 'View As A Buyer'),
                        ['/account/video/show-as-customer', 'id' => $profile->user_id],
                        ['class' => 'button green no-size']
                    ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= ProfileSidebar::widget([
                    'profile' => $profile,
                    'editable' => true,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'videos' => $videosDataProvider->totalCount
                            ? ['link' => '#videos', 'label' => Yii::t('app', 'Videos') . " ({$videosDataProvider->totalCount})"]
                            : false,
                        'reviews' => $reviewDataProvider->totalCount
                            ? ['link' => '#reviews', 'label' => Yii::t('app', 'Reviews') . " ({$reviewDataProvider->totalCount})"]
                            : false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs">
                <div class="hidden-xs">
                    <div class="gigs-title" id="video-list-header">
                        <?= Yii::t('account', 'Your videos') ?>
                        <?= Html::a(Yii::t('app', 'Post Video'), ['/video/create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $videosDataProvider,
                        'itemView' => 'video_item',
                        'viewParams' => ['editable' => true],
                        'options' => [
                            'class' => 'max-width gigs-list relative',
                            'id' => 'pro-videos',
                        ],
                        'itemOptions' => [
                            'class' => 'bf-item bf-item-video'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any videos") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                    <?= LinkPager::widget([
                        'pagination' => $videosDataProvider->pagination,
                    ]) ?>
                </div>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="videos" class="gigs-title"><?= Yii::t('account', 'Your videos') ?></div>
                    <?= ListView::widget([
                        'dataProvider' => $videosDataProvider,
                        'itemView' => '@frontend/components/catalog/views/video-list-mobile',
                        'options' => [
                            'class' => 'max-width gigs-list relative',
                            'id' => 'pro-videos-mobile',
                        ],
                        'itemOptions' => [
                            'class' => 'mob-work-item'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any videos") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn" id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

<?php
$script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    
    menuMobileItems.css('width', perc);
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x" // horizontal scrollbar
    });
    
    $(document).on('click', '.menu-worker-mobile a', function() {
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        return false;
    });

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });

    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script);