<?php

use common\models\PropertyClient;
use frontend\assets\SelectizeAsset;
use frontend\modules\account\components\AccountSettingsLeftMenu;
use frontend\modules\account\models\PropertyClientForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var PropertyClientForm $model
 * @var View $this
 */

SelectizeAsset::register($this);
$this->title = Yii::t('account', 'Register client');
?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'realtor-register-client']) ?>
            </div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left"><?= $this->title ?></h1>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
//                        'action' => Url::to(['/account/realtor/register-client']),
                        'options' => [
                            'class' => 'form-edescr',
                            'id' => 'public-profile-form'
                        ]
                    ]) ?>
                        <div class="formgig-block">
                            <div class="leftset">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'first_name')->textInput() ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'last_name')->textInput() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="leftset">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'email')->textInput() ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'phone')->textInput() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="leftset">
                                <?= $form->field($model, 'housesString')
                                    ->textInput(['id' => 'houses', 'value' => '', 'class' => 'readsym'])
                                    ->label(Yii::t('model', 'Houses'))
                                ?>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="leftset">
                                <?= $form->field($model, 'info')->textarea() ?>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="leftset selectize-input-container">
                                <?= Html::activeCheckbox($model, 'mortgage', ['id' => 'mortgage', 'label' => false]) ?>
                                <?= Html::label(Yii::t('model', 'Mortgage'), 'mortgage') ?>
                            </div>
                        </div>
                        <?= Html::submitInput(Yii::t('account', 'Save Changes'), [
                            'id' => 'submit-button',
                            'class' => 'bright'
                        ]) ?>
                    <?php ActiveForm::end(); ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$options = json_encode(array_map(
    function($var) {return ['value' => $var->entity . '-' . $var->entity_id, 'label' => $var->getLabel()];},
    $model->client->houses ?? []
));
$values = json_encode(array_map(
    function($var) {return $var->entity . '-' . $var->entity_id;},
    $model->client->houses ?? []
));
$houseListUrl = Url::to(['/ajax/house-list']);
$script = <<<JS
    $("#houses").selectize({
        options: $options,
        items: $values,
        valueField: "value",
        labelField: "label",
        searchField: ["label"],
        plugins: ["remove_button"],
        persist: false,
        create: false,
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post(
                "$houseListUrl", 
                {query: encodeURIComponent(query)},
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        },
        render: {
	        option: function(data, escape) {
	            return '<div class="option custom-selectize-option flex flex-v-center" data-value="' + data.value + '">' +
                           '<img src="' + data.image + '">' +
                           '<span class="selectize-option-title">' + data.label + '</span>' + 
                       '</div>';
	        }
	    },
    });
JS;

$this->registerJs($script);