<?php

use common\models\PropertyClient;
use yii\helpers\Html;

/**
 * @var PropertyClient $model
 */

?>


<div class="cell-friends"><?= $model->first_name . ' ' . $model->last_name ?></div>
<div class="cell-friends"><?= $model->email ?></div>
<div class="cell-friends"><?= $model->phone ?></div>
<div class="cell-friends"><?= $model->getStatusLabel() ?></div>
<div class="cell-friends"><?= Yii::$app->formatter->asDatetime($model->updated_at, 'dd.MM.y HH:mm') ?></div>
<div class="cell-friends action-cell">
    <?php
    echo Html::a(
        '<i class="fa fa-pencil"></i>',
        ['/account/realtor/edit-client', 'id' => $model->id],
        ['class' => 'realtor-client-edit hint--top', 'data-hint' => Yii::t('app', 'Edit'), 'data-pjax' => 0]
    );
    if ($model->user_id !== null) {
        echo Html::a(
            '<i class="fa fa-envelope"></i>',
            ['/account/inbox/start-conversation', 'user_id' => $model->user_id],
            ['class' => 'realtor-client-contact hint--top', 'data-hint' => Yii::t('account', 'Contact client'), 'data-pjax' => 0]
        );
    }
    ?>
</div>