<?php

use common\models\PropertyClient;
use frontend\modules\account\components\AccountSettingsLeftMenu;
use frontend\modules\account\models\PropertyClientSearch;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var PropertyClientSearch $searchModel
 * @var View $this
 */

$this->registerCssFile('/css/new/friends.css');
$this->title = Yii::t('account', 'My clients');
?>

<div class="settings greybg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12 col-xs-12">
                <?= AccountSettingsLeftMenu::widget(['active' => 'realtor-index']) ?>
            </div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="settings-content">
                    <h1 class="profileh text-left"><?= $this->title ?></h1>
                    <?php Pjax::begin(); ?>
                        <?php $form = ActiveForm::begin([
                            'id' => 'filter-form',
                            'action' => ['/account/realtor/index'],
                            'method' => 'get',
                            'options' => [
                                'data-pjax' => true
                            ]
                        ]); ?>
                            <div class="table-friends">
                                <div class='row-friends'>
                                    <div class='cell-friends'><?= Yii::t('app', 'Name') ?></div>
                                    <div class='cell-friends'><?= Yii::t('app', 'Email') ?></div>
                                    <div class='cell-friends'><?= Yii::t('app', 'Phone') ?></div>
                                    <div class='cell-friends'><?= Yii::t('app', 'Status') ?></div>
                                    <div class='cell-friends'><?= Yii::t('app', 'Updated') ?></div>
                                    <div class='cell-friends'></div>
                                </div>
                                <div class='row-friends'>
                                    <div class='cell-friends'></div>
                                    <div class='cell-friends'>
                                        <?= Html::activeTextInput($searchModel, 'email')?>
                                    </div>
                                    <div class='cell-friends'>
                                        <?= Html::activeTextInput($searchModel, 'phone')?>
                                    </div>
                                    <div class='cell-friends'>
                                        <?= Html::activeDropDownList($searchModel, 'status', PropertyClient::getStatusLabels(), [
                                            'class' => 'form-control',
                                            'prompt' => '-- ' . Yii::t('app', 'Choose invitation`s status')
                                        ]) ?>
                                    </div>
                                    <div class='cell-friends'></div>
                                    <div class='cell-friends'></div>
                                </div>

                                <?= ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => 'client-item',
                                    'options' => [
                                        'tag' => false
                                    ],
                                    'itemOptions' => [
                                        'class' => 'row-friends'
                                    ],
                                    'emptyText' => Html::tag('p', Yii::t('app', 'No records found')),
                                    'layout' => '{items}{pager}',
                                ])?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
    $(document).on('change', '#filter-form input, #filter-form select', function() {
        $('#filter-form').submit();
    });
    $(document).on('click', '.cell-friends.action-cell a.realtor-client-contact:not([data-live])', function () {
        let self = $(this);
        if (!self.data('live')) {
            $.post(self.attr('href'), function(response) {
                if (response.conversation_id) {
                    self.attr('data-live', "1");
                    self.attr('data-dialog-id', response.conversation_id);
                    self.trigger('click');
                }
            });
        }
        return false;
    });
JS;

$this->registerJs($script);