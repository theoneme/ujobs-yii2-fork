<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.12.2016
 * Time: 18:52
 */

use common\modules\store\models\Order;
use yii\helpers\Html;
use common\models\Job;
use yii\helpers\Url;
use yii\web\View;

/* @var Order $model
 * @var View $this
 */

?>
<td>
    <?= Html::a(Html::img($model->product->getThumb(), ['alt' => $model->product->getLabel(),'title' => $model->product->getLabel()]),
        ['/account/order/tender-history', 'id' => $model->id],
        ['class' => 'tg-img', 'data-pjax' => 0]) ?>
</td>
<td>
    <?= Html::a($model->product->getLabel(),
        ['/account/order/tender-history', 'id' => $model->id],
        ['class' => 'tg-name', 'data-pjax'=> 0]) ?>
</td>
<td><?= Yii::$app->formatter->asDatetime($model->created_at)?></td>
<td><?= Yii::$app->formatter->asDatetime($model->updated_at)?></td>
<td><?= $model->getStatusLabel(true) ?></td>
<td>
<!--    <div class="tg-opt text-left">-->
<!--        --><?//= Html::a('<i class="fa fa-close"></i>', null) ?>
<!--    </div>-->
</td>