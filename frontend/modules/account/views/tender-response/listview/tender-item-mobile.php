<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.05.2017
 * Time: 12:59
 */

use yii\helpers\Html;
use common\models\Job;
use yii\helpers\Url;
use yii\web\View;

/* @var Job $model
 * @var View $this
 */

?>
<div class="jm-option">
    <div class="jm-img">
        <?= Html::a(Html::img($model->product->getThumb()),
            ['/account/order/tender-history', 'id' => $model->id],
            ['data-pjax' => 0]) ?>
    </div>
    <div class="jm-name">
        <?= Html::a($model->product->getLabel(),
            ['/account/order/tender-history', 'id' => $model->id],
            ['data-pjax'=> 0]) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Order Date')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->created_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Updated At')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->updated_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Status')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getStatusLabel(true) ?>
    </div>
</div>