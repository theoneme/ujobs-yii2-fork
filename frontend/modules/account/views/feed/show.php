<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\models\Job;
use common\models\Message;
use common\models\Offer;
use common\modules\board\models\Product;
use frontend\assets\ArticleCatalogAsset;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use common\models\user\Profile;
use frontend\components\ScrollPager;
use frontend\modules\account\components\ProfileSidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $newPortfolioDataProvider ActiveDataProvider
 * @var $reviewDataProvider ActiveDataProvider
 * @var $articlesDataProvider ActiveDataProvider
 * @var $message Message
 * @var $offer Offer
 * @var $reviewsAverage integer
 * @var $reviewsCount integer
 * @var $contactAccessInfo array
 * @var $userEntities array
 */

WorkerAsset::register($this);
SelectizeAsset::register($this);
ArticleCatalogAsset::register($this);

?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($profile->gravatar_email) { ?>
                            <?= Html::img($profile->getThumb(), ['alt' => $profile->getSellerName(),'title' => $profile->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($profile->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($profile->user->isOnline()) { ?>
                                <span class="status isonline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                            <?php } else { ?>
                                <span class="status isoffline"><i
                                            class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                            <?php } ?>
                        </div>
                        <div class="wmh-rate">
                            <?php if ($profile->rating) { ?>
                                <div class="rate">
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $profile->rating / 2,
                                        'size' => 14
                                    ]) ?>
                                </div>
                            <?php } ?>
                            <div class="count-rate">
                                <strong><?= round($profile->rating / 2, 1, PHP_ROUND_HALF_UP) ?></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <?php if (isGuest()) {
                        echo Html::a(Yii::t('account', 'Contact a freelancer'), null, [
                            'class' => 'button green no-size',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalLogin'
                        ]);
                    } else {
                        if (hasAccess($profile->user_id)) {
                            echo Html::a(Yii::t('account', 'View As A Owner'),
                                ['/account/profile/show', 'id' => $profile->user_id],
                                ['class' => 'button green no-size']
                            );
                        } else {
                            echo Html::a(Yii::t('account', 'Contact a freelancer'),
                                ['/account/inbox/start-conversation', 'user_id' => $profile->user_id],
                                ['class' => 'button green no-size']
                            );
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <div class="sticky-container">
                    <?= ProfileSidebar::widget([
                        'profile' => $profile,
                        'editable' => false,
                        'menuItems' => [
                            'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                            'articles' => $articlesDataProvider->totalCount
                                ? ['link' => '#articles', 'label' => Yii::t('app', 'Articles') . " ({$articlesDataProvider->totalCount})"]
                                : false,
                            'reviews' => $reviewDataProvider->totalCount
                                ? ['link' => '#reviews', 'label' => Yii::t('app', 'Reviews') . " ({$reviewDataProvider->totalCount})"]
                                : false,
                        ]
                    ]) ?>
                </div>
            </div>
            <div class="worker-gigs hidden-xs">
                <div id="article-list-header">
                    <?= ListView::widget([
                        'dataProvider' => $articlesDataProvider,
                        'itemView' => '@frontend/modules/filter/views/page-list',
                        'layout' => "{items}{pager}",
                        'options' => [
                            'tag' => 'div',
                            'class' => 'cat-middle-section',
                            'data-scroll-container' => true,
                            'id' => 'pro-articles',
                        ],
                        'itemOptions' => [
                            'tag' => 'div',
                            'class' => 'cat-middle-block',
                            'data-scroll-item' => true
                        ],
                        'pager' => [
                            'class' => ScrollPager::class,
                            'loadUrl' => Url::to(['/account/feed/load-more', 'user_id' => $profile->user_id])
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any published articles") . '</div>',
                    ]); ?>
                </div>
                <div class="job-section sticky-container">
                    <div class="clearfix">
                    <?php foreach($userEntities as $userEntity) { ?>
                        <?php if($userEntity instanceof Job) { ?>
                            <div class="mob-work-item">
                                <?= $this->render('@frontend/modules/filter/views/job-list-mobile', [
                                    'model' => $userEntity
                                ]) ?>
                            </div>
                        <?php } ?>
                        <?php if($userEntity instanceof Product) { ?>
                            <div class="mob-work-item">
                                <?= $this->render('@frontend/modules/filter/views/product-list-mobile', [
                                    'model' => $userEntity
                                ]) ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    </div>
                </div>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="articles">
                        <div class="gigs-title"><?= Yii::t('account', 'News feed of this user') ?></div>
                        <div class="select-subcat">
                            <?php echo ListView::widget([
                                'dataProvider' => $articlesDataProvider,
                                'id' => 'pages-mobile',
                                'itemView' => '@frontend/modules/filter/views/page-list-mobile',
                                'layout' => "{items}{pager}",
                                'options' => [
                                    'class' => '',
                                    'data-scroll-container-mobile' => true
                                ],
                                'itemOptions' => [
                                    'class' => 'mob-work-item',
                                    'data-scroll-item-mobile' => true
                                ],
                                'pager' => [
                                    'class' => ScrollPager::class,
                                    'loadUrl' => Url::to(['/account/feed/load-more', 'user_id' => $profile->user_id, 'mobile' => true]),
                                    'containerSelector' => '[data-scroll-container-mobile]',
                                    'itemSelector' => '[data-scroll-item-mobile]',
                                ],
                                'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                            ]); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger"
                            id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn"
                            id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="album-modal">

    </div>

<?php
$this->registerJsFile("/js/new/jquery.sticky.js", ['depends' => ['frontend\assets\CommonAsset']]);
$script = <<<JS
    let delay = (function() {
        let timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    
    $(window).scroll(function(){
        delay(function(){
            let top = $('.worker-card').offset().top;
            if ($(window).width() > 992) {
                if ($(document).scrollTop() > top) {
                    $('.worker-mobile-head').css('display', 'block');
                    $('.sticky-container').css('padding-top', '81px');
                } else {
                    $('.worker-mobile-head').css('display', 'none');
                    $('.sticky-container').css('padding-top', 0);
                }
            }
        }, 100);
    });
    
    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
    
    let delay2 = (function() {
        let timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    $(window).resize(function(){
        delay2(function(){
            $(".sticky-container").trigger("sticky_kit:detach");
            if ($(window).width() > 768) {
                $(".sticky-container").stick_in_parent({offset_top: 100,parent: '.worker-info'});
            }
        }, 300);
    });
    
    if ($(window).width() > 768) {
        $(".sticky-container").stick_in_parent({offset_top: 100,parent: '.worker-info'});
    }
JS;

$this->registerJs($script);