<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.01.2018
 * Time: 15:18
 */

namespace frontend\modules\account\components;

use common\models\forms\UserLanguageForm;
use common\models\forms\UserSkillForm;
use common\models\forms\UserSpecialtyForm;
use common\models\Job;
use common\models\Page;
use common\models\Subscription;
use common\models\user\Profile;
use common\models\UserCertificate;
use common\models\UserEducation;
use common\models\UserFriend;
use common\models\UserPortfolio;
use common\models\Video;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\Product;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Class ProfileSidebar
 * @package frontend\modules\account\components
 */
class ProfileSidebar extends Widget
{
    /**
     * @var string
     */
    public $template = 'profile-sidebar';
    /**
     * @var bool
     */
    public $editable = false;
    /**
     * @var Profile
     */
    public $profile;

    /**
     * @var array
     */
    public $menuItems = [];

    /**
     * @return string
     */
    public function run()
    {
        if ($this->editable === false) {
            $attributesCondition = ['mod_attribute_value.status' => AttributeValue::STATUS_APPROVED, 'mad_2.locale' => Yii::$app->language];
            $articlesCondition = ['page.status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]];
            $jobsCondition = ['job.status' => Job::STATUS_ACTIVE];
            $tendersCondition = ['job.status' => Job::STATUS_ACTIVE];
            $productsCondition = ['product.status' => Product::STATUS_ACTIVE];
            $productTendersCondition = ['product.status' => Product::STATUS_ACTIVE];
            $videosCondition = ['video.status' => Video::STATUS_ACTIVE];
            $this->template = 'profile-sidebar';
        } else {
            $attributesCondition = [];
            $articlesCondition = ['not', ['page.status' => Page::STATUS_DELETED]];
            $jobsCondition = ['not', ['job.status' => Job::STATUS_DELETED]];
            $tendersCondition = ['not', ['job.status' => Job::STATUS_DELETED]];
            $productsCondition = ['not', ['product.status' => Product::STATUS_DELETED]];
            $productTendersCondition = ['not', ['product.status' => Product::STATUS_DELETED]];
            $videosCondition = ['not', ['video.status' => Video::STATUS_DELETED]];
            $this->template = 'profile-sidebar-admin';
        }

        $skillQuery = UserSkillForm::findAll(array_merge(['user_id' => $this->profile->user_id, 'entity_alias' => 'skill'], $attributesCondition));
        $languageQuery = UserLanguageForm::findAll(array_merge(['user_id' => $this->profile->user_id, 'entity_alias' => 'language'], $attributesCondition));
        $specialtyQuery = UserSpecialtyForm::findAll(array_merge(['user_id' => $this->profile->user_id, 'entity_alias' => 'specialty'], $attributesCondition));
        $certificateQuery = UserCertificate::find()->where(['user_id' => $this->profile->user_id]);
        $educationQuery = UserEducation::find()->where(['user_id' => $this->profile->user_id]);
        $oldPortfolioQuery = UserPortfolio::find()->where(['user_id' => $this->profile->user_id, 'version' => UserPortfolio::VERSION_OLD]);
        $newPortfolioQuery = UserPortfolio::find()->where(['user_id' => $this->profile->user_id, 'version' => UserPortfolio::VERSION_NEW]);
        $friendsQuery = UserFriend::find()->joinWith(['targetUser'])->where([
            'user_id' => $this->profile->user_id, 'status' => UserFriend::STATUS_APPROVED
        ]);
        $subscribersQuery = Subscription::find()->joinWith(['user'])->where([
            'entity_id' => $this->profile->user_id, 'entity' => Subscription::ENTITY_PROFILE
        ]);

        $jobsCount = Job::find()->where(['job.type' => 'job', 'job.user_id' => $this->profile->user_id])->andWhere($jobsCondition)->groupBy('job.id')->count();
        $tendersCount = Job::find()->where(['type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'job.user_id' => $this->profile->user_id])->andWhere($tendersCondition)->groupBy('job.id')->count();
        $productsCount = Product::find()->where(['type' => Product::TYPE_PRODUCT, 'product.user_id' => $this->profile->user_id])->andWhere($productsCondition)->groupBy('product.id')->count();
        $productTendersCount = Product::find()->where(['type' => Product::TYPE_TENDER, 'product.user_id' => $this->profile->user_id])->andWhere($productTendersCondition)->groupBy('product.id')->count();
        $articlesCount = Page::find()->where(['type' => Page::TYPE_ARTICLE, 'user_id' => $this->profile->user_id])->andWhere($articlesCondition)->count();
        $videosCount = Video::find()->where(['user_id' => $this->profile->user_id])->andWhere($videosCondition)->count();

        $languageDataProvider = new ArrayDataProvider(['allModels' => $languageQuery, 'pagination' => false, 'key' => 'id']);
        $skillDataProvider = new ArrayDataProvider(['allModels' => $skillQuery, 'pagination' => false, 'key' => 'id']);
        $specialtyDataProvider = new ArrayDataProvider(['allModels' => $specialtyQuery, 'pagination' => false, 'key' => 'id']);
        $certificateDataProvider = new ActiveDataProvider(['query' => $certificateQuery, 'pagination' => false]);
        $educationDataProvider = new ActiveDataProvider(['query' => $educationQuery, 'pagination' => false]);
        $oldPortfolioDataProvider = new ActiveDataProvider(['query' => $oldPortfolioQuery, 'pagination' => false]);
        $newPortfolioDataProvider = new ActiveDataProvider(['query' => $newPortfolioQuery, 'pagination' => false]);
        $friendsDataProvider = new ActiveDataProvider(['query' => $friendsQuery, 'pagination' => [
            'pageSize' => 6
        ]]);
        $subscribersDataProvider = new ActiveDataProvider(['query' => $subscribersQuery, 'pagination' => [
            'pageSize' => 6
        ]]);

        $portfolioCount = $newPortfolioDataProvider->totalCount;

        $additionalMenuItems = array_filter([
            'jobs' => $jobsCount
                ? ['link' => ['/account/profile/show', 'id' => $this->profile->user_id, '#' => 'jobs'], 'label' => Yii::t('app', 'Services') . " ({$jobsCount})"]
                : false,
            'products' => $productsCount
                ? ['link' => ['/account/profile/show', 'id' => $this->profile->user_id, '#' => 'products'], 'label' => Yii::t('app', 'Products') . " ({$productsCount})"]
                : false,
            'portfolios' => $portfolioCount
                ? ['link' => ['/account/portfolio/show', 'id' => $this->profile->user_id, '#' => 'portfolios'], 'label' => Yii::t('app', 'Portfolio') . " ({$portfolioCount})"]
                : false,
            'articles' => $articlesCount
                ? ['link' => ['/account/feed/show', 'id' => $this->profile->user_id, '#' => 'articles'], 'label' => Yii::t('app', 'Articles') . " ({$articlesCount})"]
                : false,
            'videos' => $videosCount
                ? ['link' => ['/account/video/show', 'id' => $this->profile->user_id, '#' => 'videos'], 'label' => Yii::t('app', 'Videos') . " ({$videosCount})"]
                : false,
        ]);
        $this->menuItems = array_filter($this->menuItems);
        if (count($this->menuItems) < 4 && count($additionalMenuItems)) {
            foreach ($additionalMenuItems as $key => $item) {
                if (count($this->menuItems) < 4 && !array_key_exists($key, $this->menuItems)) {
                    $this->menuItems[$key] = $item;
                }
            }
        }

        return $this->render($this->template, [
            'profile' => $this->profile,
            'editable' => $this->editable,
            'languageDataProvider' => $languageDataProvider,
            'skillDataProvider' => $skillDataProvider,
            'specialtyDataProvider' => $specialtyDataProvider,
            'certificateDataProvider' => $certificateDataProvider,
            'educationDataProvider' => $educationDataProvider,
            'oldPortfolioDataProvider' => $oldPortfolioDataProvider,
            'newPortfolioDataProvider' => $newPortfolioDataProvider,
            'friendsDataProvider' => $friendsDataProvider,
            'subscribersDataProvider' => $subscribersDataProvider,
            'jobsCount' => $jobsCount,
            'tendersCount' => $tendersCount,
            'productsCount' => $productsCount,
            'productTendersCount' => $productTendersCount,
            'articlesCount' => $articlesCount,
            'videosCount' => $videosCount,
            'portfolioCount' => $portfolioCount,
            'menuItems' => $this->menuItems
        ]);
    }
}