<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.01.2018
 * Time: 15:18
 */

namespace frontend\modules\account\components;

use common\models\user\User;
use common\models\UserAttribute;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class MiniView
 * @package frontend\modules\account\components
 */
class MiniView extends Widget
{
    /**
     * @var string
     */
    public $template = 'mini_view';
    /**
     * @var bool
     */
    public $isMobile = false;
    /**
     * @var User
     */
    public $user;
    public $skills;
    public $languages;
    public $accessInfo;

    /**
     * @return string
     */
    public function run()
    {
        if ((boolean) $this->user->is_company === false) {
            $profileSkills = UserAttribute::find()->joinWith(['attrValueDescriptions'])->where(['user_id' => $this->user->id, 'entity_alias' => 'skill'])->all();
            $skills = ArrayHelper::map($profileSkills, 'id', function($model) {
                return $model->attrValueDescriptions[Yii::$app->language]->title ?? array_values($model->attrValueDescriptions)[0]->title;
            });

            $profileLanguages = UserAttribute::find()->joinWith(['attrValueDescriptions'])->where(['user_id' => $this->user->id, 'entity_alias' => 'language'])->all();
            $languages = ArrayHelper::map($profileLanguages, 'id', function($model) {
                return $model->attrValueDescriptions[Yii::$app->language]->title ?? array_values($model->attrValueDescriptions)[0]->title;
            });

            return $this->render("@frontend/modules/account/views/profile/{$this->template}", [
                'user' => $this->user,
                'skills' => $skills,
                'languages' => $languages,
                'accessInfo' => $this->accessInfo
            ]);
        }

        return $this->render("@frontend/modules/company/views/profile/{$this->template}", [
            'user' => $this->user,
            'accessInfo' => $this->accessInfo
        ]);
    }
}