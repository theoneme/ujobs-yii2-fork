<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 11:41
 */

namespace frontend\modules\account\components;

use common\models\Company;
use common\models\CompanyMember;
use common\models\user\User;
use common\models\UserAttribute;
use common\modules\attribute\models\AttributeValue;
use yii\base\Widget;
use Yii;
use yii\helpers\Url;

/**
 * Class AccountSettingsLeftMenu
 * @package frontend\modules\account\components
 */
class AccountSettingsLeftMenu extends Widget
{
    public $template = 'account-settings-left-menu';
    public $active = null;

    /**
     * @return string
     */
    public function run()
    {
        $links = [
            'public-settings' => [
                'title' => Yii::t('account', 'Public Profile Settings'),
                'active' => $this->active === 'public-profile-settings',
                'href' => Url::to(['/account/service/public-profile-settings'])
            ],
            'account-settings' => [
                'title' => Yii::t('account', 'Account Settings'),
                'active' => $this->active === 'account-settings',
                'href' => Url::to(['/account/service/account-settings'])
            ],
            'payment-settings' => [
                'title' => Yii::t('account', 'Payment Settings'),
                'active' => $this->active === 'payment-settings',
                'href' => Url::to(['/account/service/payment-settings'])
            ],
            'social-settings' => [
                'title' => Yii::t('account', 'Social Network Settings'),
                'active' => $this->active === 'social-network-settings',
                'href' => Url::to(['/account/service/social-network-settings'])
            ],
//            [
//                'title' => Yii::t('account', 'Security Settings'),
//                'active' => $this->active == 'security-settings' ? true : false,
//                'href' => Url::to(['/account/service/security-settings'])
//            ],
            'account-actions' => [
                'title' => Yii::t('account', 'Account Actions'),
                'active' => $this->active === 'account-actions',
                'href' => Url::to(['/account/service/account-actions'])
            ],
        ];
        $hasCompanies = CompanyMember::find()->where(['user_id' => userId(), 'status' => CompanyMember::STATUS_ACTIVE])->exists();
        if ($hasCompanies === true) {
            $links['companies'] = [
                'title' => Yii::t('account', 'My companies'),
                'active' => $this->active === 'companies',
                'href' => Url::to(['/account/service/companies'])
            ];
        }

        $isRealtor = UserAttribute::find()
            ->where([
                'entity_alias' => 'specialty',
                'value_alias' => 'rieltor',
                'user_id' => Yii::$app->user->identity->getCurrentId()
            ])
            ->exists();
        if ($isRealtor === true) {
            $links['realtor-index'] = [
                'title' => Yii::t('account', 'My clients'),
                'active' => $this->active === 'realtor-index',
                'href' => Url::to(['/account/realtor/index'])
            ];
            $links['realtor-register-client'] = [
                'title' => Yii::t('account', 'Register client'),
                'active' => $this->active === 'realtor-register-client',
                'href' => Url::to(['/account/realtor/register-client'])
            ];
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if($user->company_user_id !== null && $user->companyUser->company !== null) {
            $member = $user->companyUser->company->companyMembers[userId()];
            if ($member->isAllowedToManage()) {
                $links['company'] = [
                    'title' => Yii::t('app', 'Add member'),
                    'active' => $this->active === 'company',
                    'href' => Url::to(['/account/service/company', 'id' => $user->companyUser->company->id])
                ];
                $links['public-settings'] = [
                    'title' => Yii::t('account', 'Company Profile Settings'),
                    'active' => $this->active === 'company-profile-settings',
                    'href' => Url::to(['/company/service/public-profile-settings'])
                ];
            }
            else {
                unset($links['public-settings']);
                unset($links['account-settings']);
            }
            if ($member->role !== CompanyMember::ROLE_OWNER) {
                unset($links['payment-settings']);
            }
            unset($links['social-settings']);
            unset($links['account-actions']);
        }

        return $this->render($this->template, [
            'links' => $links
        ]);
    }
}