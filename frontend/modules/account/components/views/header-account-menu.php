<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 17:17
 */

?>

<div class="nav-main-prof hidden-xs">
    <div class="container-fluid">
        <div class="row">
            <ul class="menu-work">
                <?php foreach($items as $item) { ?>
                    <li>
                        <a <?= $item['active'] ? 'class="active"' : '' ?> href="<?=$item['href']?>"><?=$item['title']?></a>
                        <?php if(isset($item['children'])) { ?>
                            <ul>
                            <?php foreach($item['children'] as $child) { ?>
                                <li><a <?= $child['active'] ? 'class="active"' : '' ?> href="<?=$child['href']?>"><?=$child['title']?></a></li>
                            <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>