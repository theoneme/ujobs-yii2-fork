<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.01.2018
 * Time: 15:18
 */

use common\models\ContentTranslation;
use common\models\user\Profile;
use dektrium\user\widgets\Connect;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var array $menuItems
 * @var ArrayDataProvider $languageDataProvider
 * @var ArrayDataProvider $skillDataProvider
 * @var ArrayDataProvider $specialtyDataProvider
 * @var ActiveDataProvider $certificateDataProvider
 * @var ActiveDataProvider $educationDataProvider
 * @var ActiveDataProvider $newPortfolioDataProvider
 * @var ActiveDataProvider $oldPortfolioDataProvider
 * @var integer $jobsCount
 * @var integer $articlesCount
 * @var integer $videosCount
 * @var integer $tendersCount
 * @var integer $productsCount
 * @var integer $productTendersCount
 * @var Profile $profile
 * @var View $this
 */

$tariffIcon = '';
if ($profile->user->activeTariff !== null) {
    $pathInfo = pathinfo($profile->user->activeTariff->tariff->icon);
    $tariffIcon = Html::img($pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . '-profile.' . $pathInfo['extension'], ['class' => 'tariff-icon']);
}
?>

    <div class="worker-info-content">
        <div class="worker-info-block">
            <div class="status-site">
                <?php if ($profile->user->isOnline()) { ?>
                    <span class="status isonline">
                        <i class="fa fa-circle"></i>
                        <?= Yii::t('labels', 'Online') ?>
                    </span>
                <?php } else { ?>
                    <span class="status isoffline">
                        <i class="fa fa-circle"></i>
                        <?= Yii::t('labels', 'Offline') ?>
                    </span>
                <?php } ?>
            </div>
            <?php if ($profile->gravatar_email) { ?>
                <div class="hw-avatar text-center"
                     style="background: url(<?= $profile->getThumb() ?>); background-size: cover; margin: 0 auto;">
                    <?= $tariffIcon ?>
                </div>
            <?php } else { ?>
                <div class="hw-avatar text-center" style="margin: 0 auto">
                    <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                    <?= $tariffIcon ?>
                </div>
            <?php } ?>
            <div class="worker-name text-center"><?= Html::encode($profile->getSellerName()) ?></div>
            <div class="worker-status text-center"><?= Html::encode($profile->status_text) ?></div>
            <div class="worker-rate text-center clearfix">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $profile->rating / 2,
                    'size' => 14
                ]) ?>
                <div class="count-rate">
                    <strong><?= round($profile->rating / 2, 1, PHP_ROUND_HALF_UP) ?></strong>
                </div>
            </div>
            <div class="worker-actions">
                <?= Html::a(Yii::t('account', 'View As A Buyer'),
                    ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show-as-customer', 'id' => Yii::$app->user->identity->getId()],
                    ['class' => 'button green no-size']
                ) ?>
            </div>
        </div>
        <!--
        В этом блоке-выведите категории в которых размещены работы-->
        <!--                    <div class="worker-info-block">-->
        <!--                        <div class="pro-img"><img class="tariff-icon" src="/images/new/icons/pro-icon.png" alt=""></div>-->
        <!--                        <div class="verify-text text-left">-->
        <?php //= Yii::t('app','Verified by uJobs in') ?><!--</div>-->
        <!--                        <ul class="verify-cat">-->
        <!--                            <li>Logo Design</li>-->
        <!--                        </ul>-->
        <!--                    </div>-->
        <div class="worker-info-block">
            <div class="worker-card">
                <div class="wc-item location">
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'location-pjax']); ?>
                    <div class="flex flex-justify-between">
                        <div>
                            <?= Yii::t('account', 'From') ?>
                        </div>
                        <div class="text-right">
                            <strong>
                                <?php $locationString = $profile->getFrom() ?>
                                <?= (!empty($locationString) ? $locationString : Yii::t('account', 'Enter city')) . '&nbsp;' . Html::a('<i class="fa fa-pencil"></i>', null, ['id' => 'edit-city']) ?>
                            </strong>
                        </div>
                    </div>
                    <div>
                        <?php $form = ActiveForm::begin([
                            'action' => ['/account/profile-ajax/update-profile'],
                            'id' => 'city-form',
                            'options' => [
                                'data-pjax-container' => 'location-pjax'
                            ]
                        ]) ?>
                        <div class="row">
                            <div class="col-md-8 selectize-input-container city-selectize">
                                <?= Html::activeHiddenInput($profile, 'lat', ['id' => 'profile-lat']) ?>
                                <?= Html::activeHiddenInput($profile, 'long', ['id' => 'profile-long']) ?>
                                <?= Html::activeHiddenInput($profile, '[locationData]country', ['id' => 'profile-location-country', 'value' => false]) ?>
                                <?= Html::activeHiddenInput($profile, '[locationData]city', ['id' => 'profile-location-city']) ?>
                                <?= Html::textInput('location_input', null,
                                    [
                                        'id' => 'profile-location-input',
                                        'placeholder' => Yii::t('app', 'Enter your city and choose it from dropdown'),
                                    ]
                                ); ?>
                            </div>
                            <div class="col-md-4">
                                <?= Html::submitButton(Yii::t('account', 'Save'), ['class' => 'btn-small', 'style' => 'margin: 0; height: 35px']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
                <div class="wc-item member">
                    <?= Yii::t('account', 'Member Since') ?>
                    <span class="text-right"><?= Yii::$app->formatter->asDate($profile->user->created_at) ?></span>
                </div>
                <?php if ($soldServices = $profile->getSoldServicesCount()) { ?>
                    <div class="wc-item basket">
                        <?= Yii::t('app', 'Sold through service') ?>
                        <span class="text-right"><?= $soldServices ?></span>
                    </div>
                <?php } ?>
                <div class="wc-item response-time">
                    <?= Yii::t('account', 'Avg. Response Time') ?>
                    <span class="text-right"> <?= Yii::t('account', '{count, plural, one{# hour} other{# hours}}', ['count' => 1]) ?> </span>
                </div>
                <div class="wc-item delivery">
                    <?= Yii::t('account', 'Last Delivery') ?>
                    <span class="text-right"><?= Yii::t('account', 'about {count, plural, one{# hour} other{# hours}}', ['count' => 7]) ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="visible-xs">
        <ul class="menu-worker-mobile">
            <?php foreach ($menuItems as $menuItem) { ?>
                <li class="text-center">
                    <?= Html::a($menuItem['label'], $menuItem['link'], ['class' => array_key_exists('active', $menuItem) ? 'active' : '']) ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="worker-info-mobile">
        <div class="worker-info-content">
            <div class="worker-info-block">
                <div>
                    <div class="worker-info-title">
                        <?= Yii::t('account', 'Settings') ?>:
                    </div>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/service/public-profile-settings']) ?>"><?= Yii::t('account', 'Public Profile Settings') ?></a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/service/account-settings']) ?>"><?= Yii::t('account', 'Account Settings') ?></a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/payment/index']) ?>"><?= Yii::t('account', 'Your balance') ?></a>
                </div>
            </div>
            <div class="worker-info-block">
                <div>
                    <div class="worker-info-title">
                        <?= Yii::t('account', 'My offers') ?>
                    </div>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'job-list-header']) ?>">
                        <?= Yii::t('account', 'Services I offer') . ($jobsCount ? " ({$jobsCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'product-list-header']) ?>">
                        <?= Yii::t('account', 'Products I sell') . ($productsCount ? " ({$productsCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'tender-list-header']) ?>">
                        <?= Yii::t('account', 'My service requests') . ($tendersCount ? " ({$tendersCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'ptender-list-header']) ?>">
                        <?= Yii::t('account', 'Products I`m ready to buy') . ($productTendersCount ? " ({$productTendersCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/portfolio/show', 'id' => $profile->user_id, '#' => 'portfolio-list-header']) ?>">
                        <?= Yii::t('account', 'My portfolio') . ($newPortfolioDataProvider->totalCount ? ' (' . $newPortfolioDataProvider->totalCount . ')' : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/feed/show', 'id' => $profile->user_id, '#' => 'article-list-header']) ?>">
                        <?= Yii::t('account', 'My news feed') . ($articlesCount ? " ({$articlesCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link" href="<?= Url::to(['/account/video/show', 'id' => $profile->user_id, '#' => 'video-list-header']) ?>">
                        <?= Yii::t('account', 'My videos') . ($videosCount ? " ({$videosCount})" : '') ?>
                    </a>
                </div>
            </div>
            <div class="worker-info-block" id="about">
                <a class="edit-alias text-right edit-descr" href=""><?= Yii::t('account', 'Edit Description') ?></a>
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'description-pjax', 'scrollTo' => false]); ?>
                <div class="worker-info-title">
                    <?= Yii::t('account', 'Description') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'Tell us more about yourself. Buyers are also interested in learning about you as a person.') ?>">
                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <?php
                $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'action' => Url::to(['/account/profile-ajax/update-profile']),
                    'options' => [
                        'class' => 'form-edescr',
                        'id' => 'description-form',
                        'data-pjax-container' => 'description-pjax'
                    ]
                ]);
                $locale = Yii::$app->language;
                $translation = $profile->translations[$locale] ?? new ContentTranslation();
                ?>
                <?= $form->field($translation, "[$locale]content")->textarea(['autofocus' => 'autofocus', 'value' => Html::encode($profile->getDescription())])->label(false); ?>
                <?= Html::a(Yii::t('account', 'Cancel'), '#', ['class' => 'grey-but text-center grey-small closeedit']) ?>
                <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
                <?php ActiveForm::end(); ?>
                <p class="prof-descr"><?= nl2br(Html::encode($profile->getDescription())) ?></p>
                <?php Pjax::end(); ?>
            </div>
            <?php if ($friendsDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Friends') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $friendsDataProvider,
                        'itemView' => 'listview/friend',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Languages Specified'),
                        'options' => [
                            'id' => 'user-friends',
                            'class' => 'profile-sidebar-friends flex flex-wrap'
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'layout' => "{items}"
                    ]) ?>

                    <?php if($friendsDataProvider->getTotalCount() > 6) { ?>
                        <div class="profile-sidebar-friends-more-friends text-right">
                            <?= Yii::t('account', 'And {count, plural, one{# more friend} other{# more friends}}', ['count' => $friendsDataProvider->getTotalCount() - 6]); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($subscribersDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Subscribers') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $subscribersDataProvider,
                        'itemView' => 'listview/subscribtion',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Languages Specified'),
                        'options' => [
                            'id' => 'user-friends',
                            'class' => 'profile-sidebar-friends flex flex-wrap'
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'layout' => "{items}"
                    ]) ?>

                    <?php if($subscribersDataProvider->getTotalCount() > 6) { ?>
                        <div class="profile-sidebar-friends-more-friends text-right">
                            <?= Yii::t('account', 'And {count, plural, one{# more subscriber} other{# more subscribers}}', ['count' => $subscribersDataProvider->getTotalCount() - 6]); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="worker-info-block">
                <a id="load-language" class="edit-alias text-right" href="#" data-entity="languages">
                    <?= Yii::t('account', 'Add New') ?>
                </a>
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'language-pjax', 'scrollTo' => false]); ?>
                <div class="worker-info-title"><?= Yii::t('account', 'Languages') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'You can make up to four selections.') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <div data-role='container' id="language-container"></div>
                <?= ListView::widget([
                    'dataProvider' => $languageDataProvider,
                    'itemView' => 'listview/language',
                    'viewParams' => ['editable' => true],
                    'emptyText' => Yii::t('account', 'No Languages Specified'),
                    'options' => [
                        'id' => 'language-listview'
                    ],
                    'itemOptions' => [
                        'class' => 'worker-info-item'
                    ],
                    'layout' => "{items}"
                ]) ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-block">
                <a id="load-skill" class="edit-alias text-right edit-form" href="#" data-entity="skills">
                    <?= Yii::t('account', 'Add New') ?>
                </a>
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'skill-pjax']); ?>
                <div class="worker-info-title"><?= Yii::t('account', 'Skills') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'Let your buyers know your skills. Skills gained through your previous jobs, hobbies or even everyday life.') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <div data-role='container' id="skill-container"></div>
                <?= ListView::widget([
                    'dataProvider' => $skillDataProvider,
                    'itemView' => 'listview/skill',
                    'viewParams' => ['editable' => true],
                    'emptyText' => Yii::t('account', 'No Skills Specified'),
                    'options' => [
                        'id' => 'skill-listview',
                    ],
                    'itemOptions' => [
                        'class' => 'worker-info-item'
                    ],
                    'layout' => "{items}"
                ]) ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-block">
                <a id="load-specialty" class="edit-alias text-right edit-form" href="#" data-entity="specialties">
                    <?= Yii::t('account', 'Add New') ?>
                </a>
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'specialty-pjax']); ?>
                <div class="worker-info-title"><?= Yii::t('account', 'Specialties') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'List your specialties. For each specialty specify monthly payment that you would like to receive.') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <div data-role='container' id="specialty-container"></div>
                <?= ListView::widget([
                    'dataProvider' => $specialtyDataProvider,
                    'itemView' => 'listview/specialty',
                    'viewParams' => ['editable' => true],
                    'emptyText' => Yii::t('account', 'No Specialties Specified'),
                    'options' => [
                        'id' => 'specialty-listview'
                    ],
                    'itemOptions' => [
                        'class' => 'worker-info-item'
                    ],
                    'layout' => "{items}"
                ]) ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-block">
                <a id="load-education" class="edit-alias text-right edit-form" href="#" data-entity="educations">
                    <?= Yii::t('account', 'Add New') ?>
                </a>
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'education-pjax']); ?>
                <div class="worker-info-title"><?= Yii::t('account', 'Education') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'Describe your educational background. It will help buyers get to know you better!') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <div data-role='container' id="education-container"></div>
                <?= ListView::widget([
                    'dataProvider' => $educationDataProvider,
                    'itemView' => 'listview/education',
                    'viewParams' => ['editable' => true],
                    'emptyText' => Yii::t('account', 'No Educations Specified'),
                    'options' => [
                        'id' => 'education-listview'
                    ],
                    'itemOptions' => [
                        'class' => 'worker-info-item'
                    ],
                    'layout' => "{items}"
                ]) ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-block">
                <a id="load-certificate" class="edit-alias text-right edit-form" href="#" data-entity="certificates">
                    <?= Yii::t('account', 'Add New') ?>
                </a>
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'certificate-pjax']); ?>
                <div class="worker-info-title"><?= Yii::t('account', 'Certification') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'Listing your honors and awards can help you stand out from other sellers.') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <div data-role='container' id="certificate-container"></div>
                <?= ListView::widget([
                    'dataProvider' => $certificateDataProvider,
                    'itemView' => 'listview/certificate',
                    'viewParams' => ['editable' => true],
                    'emptyText' => Yii::t('account', 'No Certificates Specified'),
                    'options' => [
                        'id' => 'certificate-listview'
                    ],
                    'itemOptions' => [
                        'class' => 'worker-info-item'
                    ],
                    'layout' => "{items}"
                ]) ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-block">
                <a class="edit-alias text-right"
                   href="<?= Url::to(['/account/portfolio/create']) ?>"><?= Yii::t('account', 'Add New') ?></a>
                <div class="worker-info-title"><?= Html::a(Yii::t('account', 'Portfolio'), ['/account/portfolio/show', 'id' => $profile->user_id]) ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'You can share your Blog, Website or your Portfolio on another Website.') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <?php if ($oldPortfolioDataProvider->getTotalCount() === 0 && $newPortfolioDataProvider->getTotalCount() === 0) { ?>
                    <div class="empty"><?= Yii::t('account', 'Portfolio is empty') ?></div>
                <?php } else { ?>
                    <?= ListView::widget([
                        'dataProvider' => $oldPortfolioDataProvider,
                        'itemView' => 'listview/portfolio',
                        'viewParams' => ['editable' => false],
                        'emptyText' => false,
                        'options' => [
                            'id' => 'portfolio-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'worker-info-item'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                    <?= ListView::widget([
                        'dataProvider' => $newPortfolioDataProvider,
                        'itemView' => 'listview/portfolio-new',
                        'viewParams' => ['editable' => false],
                        'emptyText' => false,
                        'options' => [
                            'id' => 'portfolio-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'worker-info-item'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                <?php } ?>
            </div>

            <div class="worker-info-block">
                <div class="worker-info-title"><?= Yii::t('account', 'Social Network') ?>
                    <span class="hint--top"
                          data-hint="<?= Yii::t('account', 'Binding account to social networks confirms the authenticity of your profile. You can link your account to multiple services.') ?>">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                </span>
                </div>
                <ul class="check">
                    <?php
                    $auth = Connect::begin([
                        'baseAuthUrl' => ['/user/security/auth'],
                        'accounts' => $profile->user->accounts,
                        'autoRender' => false,
                        'popupMode' => false,
                    ]);
                    foreach ($auth->getClients() as $client) {
                        echo $auth->isConnected($client) ? "<li>{$client->getTitle()}</li>" : "<li class='addsoc'><a href='" . $auth->createClientUrl($client) . "'>{$client->getTitle()}</a></li>";
                    }
                    Connect::end();
                    ?>
                </ul>
            </div>
        </div>
    </div>

<?php
$languageUrl = Url::to(['/account/profile-ajax/render-language']);
$skillUrl = Url::to(['/account/profile-ajax/render-skill']);
$specialtyUrl = Url::to(['/account/profile-ajax/render-specialty']);
$certificateUrl = Url::to(['/account/profile-ajax/render-certificate']);
$educationUrl = Url::to(['/account/profile-ajax/render-education']);
$portfolioUrl = Url::to(['/account/profile-ajax/render-portfolio']);
$specialtyListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);

$languagesCount = $languageDataProvider->totalCount;
$skillsCount = $skillDataProvider->totalCount;
$specialtiesCount = $specialtyDataProvider->totalCount;
$certificatesCount = $certificateDataProvider->totalCount;
$educationsCount = $educationDataProvider->totalCount;
$portfolioCount = $newPortfolioDataProvider->totalCount;

$apiKey = Yii::$app->params['googleAPIKey'];
$language = Yii::$app->params['supportedLocales'][Yii::$app->language];
if ($language === 'ua') {
    $language = 'uk';
}
$script = <<<JS
    let counters = {
        languages: $languagesCount,
        skills: $skillsCount,
        specialties: $specialtiesCount,
        certificates: $certificatesCount,
        educations: $educationsCount
    };

    let dataUrls = {
        languages: '$languageUrl',
        skills: '$skillUrl',
        specialties: '$specialtyUrl',
        certificates: '$certificateUrl',
        educations: '$educationUrl'
    };      
    let countSkill = $('.skill-list li').length;
    let counter = 0;

    $(document).on('beforeSubmit', '#country-form, #city-form, #description-form, .lang-form, .skill-form, .specialty-form, .certificate-form, .education-form', function() {
        let form = $(this),
            container = $(this).data('pjax-container');

        if (form.find('.has-error').length) {
            return false;
        }
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function(response) {
                if (response.result === true) {
                    $.pjax.reload({
                        container: "#" + container,
                        async: false
                    });
                }
            }
        });
        return false;
    });
    
    $(document).on('click', '#load-language, #load-specialty, #load-certificate, #load-education, #load-skill', function() {
        let entity = $(this).data('entity');
        let self = $(this);
        $.get(dataUrls[entity], {
            iterator: counters[entity]
        }, function(data) {
            if (data.success === true) {
                counters[entity]++;
                self.parent().find('[data-role=container]').append(data.html);
            } else {
                alertCall('top', 'error', data.message);
            }
        }, 'json');
        
        return false;
    });
    
    $(document).on('click', '.lang-delete, .skill-delete, .specialty-delete, .cert-delete, .edu-delete', function() {
        let entity = $(this).data('entity');
        $('#confirm-modal').modal().one('click', '#delete', function(e) {
            counters[entity]--;
        });
        
        return false;
    });
    
    $(document).on('click', '#language-pjax .closeeditt, #skill-pjax .closeeditt, #specialty-pjax .closeeditt, #certificate-pjax .closeeditt, #education-pjax .closeeditt', function() {
        let entity = $(this).data('entity');
        counters[entity]--;
        
        return false;
    });
    
    var googleAutocomplete;
    if (typeof (googleAutocomplete) === 'undefined') {
        $.getScript('https://maps.googleapis.com/maps/api/js?key={$apiKey}&libraries=places&language={$language}', function() {
            initAutocomplete();
        });
    } else {
        initAutocomplete();
    }
    
    function initAutocomplete() {
        googleAutocomplete = new google.maps.places.Autocomplete(
            $("#profile-location-input").get(0),
            {types: ['(cities)']}
        );

        googleAutocomplete.addListener('place_changed', function() {
            // Get the place details from the autocomplete object.
            let place = googleAutocomplete.getPlace();
            let country = '';
            let city = '';
            if (typeof place.address_components !== "undefined") {
                for (let i = 0; i < place.address_components.length; i++) {
                    let addressType = place.address_components[i].types[0];
                    switch (addressType) {
                        case 'locality': 
                            city = place.address_components[i].long_name;
                            break;
                        case 'country': 
                            country = place.address_components[i].long_name;
                            break;
                    }
                }
            }
            $('#profile-lat').val(place.geometry.location.lat());
            $('#profile-long').val(place.geometry.location.lng());
            $('#profile-location-city').val(city);
            $('#profile-location-country').val(country);
        });
    }
    
    $("#location-pjax").on('pjax:complete', function(e) {
        initAutocomplete();
	});
    
    $(document).on('click', '.closeeditt', function(e) {
        let identity = $(this).attr('data-target'),
            key = $(this).attr('data-key'),
            list = $(this).attr('data-list');

        $('#' + identity).remove();
        $('#' + list + ' [data-key=' + key + ']').show();
        
        return false;
    });
    $(document).on('click', '.edit-descr', function (e) {
        let profDescr = $('.prof-descr');
        $('.form-edescr').show();
        profDescr.hide();
        if (profDescr.html() != '') {
            $('.form-edescr textarea').val(profDescr.html());
        }
        return false;
    });
    
    $(document).on('click', '#edit-city', function(e) {
        $('#city-form').toggle();
        return false;
    });
    
    $('.selectize-input-container.specialty-selectize select:not(.selectized)').selectize({
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        'load': function(query, callback) {
            if (!query.length)
                return callback();
            $.post('$specialtyListUrl', {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    });
    
    if (countSkill > 4) {
        let skillHide = countSkill - 4;
        $('.skill-list').after('<a class=\'more-less moreSkill\'>+ See ' + skillHide + ' more</a>');
    }
    
    $('.skill-list div').each(function() {
        if (counter >= 4) {
            $(this).hide();
        }
        counter++;
    });
    
    $(document).on('click', '.moreSkill', function() {
        $('.skill-list div').show();
        $(this).hide();
        
        return false;
    });   
JS;

$this->registerJs($script);