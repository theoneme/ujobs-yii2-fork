<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.01.2018
 * Time: 15:18
 */

use common\models\Subscription;
use common\models\user\Profile;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var array $menuItems
 * @var ArrayDataProvider $languageDataProvider
 * @var ArrayDataProvider $skillDataProvider
 * @var ArrayDataProvider $specialtyDataProvider
 * @var ActiveDataProvider $certificateDataProvider
 * @var ActiveDataProvider $educationDataProvider
 * @var ActiveDataProvider $newPortfolioDataProvider
 * @var ActiveDataProvider $oldPortfolioDataProvider
 * @var ActiveDataProvider $subscribersDataProvider
 * @var ActiveDataProvider $friendsDataProvider
 * @var integer $jobsCount
 * @var integer $articlesCount
 * @var integer $videosCount
 * @var integer $tendersCount
 * @var integer $productsCount
 * @var integer $productTendersCount
 * @var Profile $profile
 */

$tariffIcon = '';
if ($profile->user->activeTariff !== null) {
    $pathInfo = pathinfo($profile->user->activeTariff->tariff->icon);
    $tariffIcon = Html::img($pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . '-profile.' . $pathInfo['extension'], ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]);
}

?>

    <div class="worker-info-content">
        <div class="worker-info-block">
            <div class="status-site">
                <?php if ($profile->user->isOnline()) { ?>
                    <span class="status isonline"><i
                                class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?></span>
                <?php } else { ?>
                    <span class="status isoffline"><i
                                class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?></span>
                <?php } ?>
            </div>
            <?php if ($profile->gravatar_email) { ?>
                <div class="hw-avatar text-center"
                     style="background: url(<?= $profile->getThumb() ?>); background-size: cover; margin: 0 auto;">
                    <?= $tariffIcon ?>
                </div>
            <?php } else { ?>
                <div class="hw-avatar text-center" style="margin: 0 auto">
                    <?= StringHelper::truncate(Html::encode($profile->user->username), 1, '') ?>
                    <?= $tariffIcon ?>
                </div>
            <?php } ?>
            <div class="worker-name text-center"><?= Html::encode($profile->getSellerName()) ?></div>
            <div class="worker-status text-center"><?= Html::encode($profile->status_text) ?></div>
            <div class="worker-rate text-center clearfix">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $profile->rating / 2,
                    'size' => 14
                ]) ?>
                <div class="count-rate">
                    <strong><?= round($profile->rating / 2, 1, PHP_ROUND_HALF_UP) ?></strong>
                </div>
            </div>
            <?php if ($profile->user_id !== userId()) { ?>
                <div class="worker-actions">
                    <?= Html::a(Yii::t('account', 'Contact a freelancer'),
                        ['/account/inbox/start-conversation', 'user_id' => $profile->user_id],
                        ['class' => 'button green no-size']
                    ); ?>
                </div>
                <div class="worker-additional-actions flex flex-justify-between">
                    <?php
                    if (!Yii::$app->user->isGuest) {
                        if (Yii::$app->user->identity->isSubscribedTo(Subscription::ENTITY_PROFILE, $profile->user_id)) {
                            echo Html::a(
                                Yii::t('app', 'Unsubscribe'),
                                null,
                                ['class' => 'button gray small active', 'data-id' => $profile->user_id, 'data-entity' => Subscription::ENTITY_PROFILE, 'data-action' => 'subscribe']
                            );
                        } else {
                            echo Html::a(
                                Yii::t('app', 'Subscribe'),
                                null,
                                ['class' => 'button gray small', 'data-id' => $profile->user_id, 'data-entity' => Subscription::ENTITY_PROFILE, 'data-action' => 'subscribe']
                            );
                        }
                    } ?>

                    <?php
                    if (!Yii::$app->user->isGuest) {
                        if (Yii::$app->user->identity->hasFriendship($profile->user_id) === true) {
                            echo Html::a(
                                Yii::t('app', 'Remove friend'),
                                null,
                                ['class' => 'button gray small active', 'data-id' => $profile->user_id, 'data-action' => 'friendship']
                            );
                        } else {
                            echo Html::a(
                                Yii::t('app', 'Add to friends'),
                                null,
                                ['class' => 'button gray small', 'data-id' => $profile->user_id, 'data-action' => 'friendship']
                            );
                        }
                    } ?>
                </div>
            <?php } else { ?>
                <div class="worker-actions">
                    <?= Html::a(Yii::t('account', 'View As A Owner'),
                        ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show', 'id' => Yii::$app->user->identity->getId()],
                        ['class' => 'button green no-size']
                    ) ?>
                </div>
            <?php } ?>
        </div>
        <!--
        В этом блоке-выведите категории в которых размещены работы-->
        <!--                    <div class="worker-info-block">-->
        <!--                        <div class="pro-img"><img class="tariff-icon" src="/images/new/icons/pro-icon.png" alt=""></div>-->
        <!--                        <div class="verify-text text-left">-->
        <?php //= Yii::t('app','Verified by uJobs in') ?><!--</div>-->
        <!--                        <ul class="verify-cat">-->
        <!--                            <li>Logo Design</li>-->
        <!--                        </ul>-->
        <!--                    </div>-->
        <div class="worker-info-block">
            <div class="worker-card">
                <?php $from = $profile->getFrom();
                if ($from) { ?>
                    <div class="wc-item location">
                        <div class="flex flex-justify-between">
                            <div>
                                <?= Yii::t('account', 'From') ?>
                            </div>
                            <div class="text-right">
                                <strong>
                                    <?= $from ?>
                                </strong>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="wc-item member">
                    <?= Yii::t('account', 'Member Since') ?>
                    <span class="text-right"><?= Yii::$app->formatter->asDate($profile->user->created_at) ?></span>
                </div>
                <?php if ($soldServices = $profile->getSoldServicesCount()) { ?>
                    <div class="wc-item basket">
                        <?= Yii::t('account', 'Sold through service') ?>
                        <span class="text-right"><?= $soldServices ?></span>
                    </div>
                <?php } ?>
                <div class="wc-item response-time">
                    <?= Yii::t('account', 'Avg. Response Time') ?>
                    <span class="text-right"> <?= Yii::t('account', '{count, plural, one{# hour} other{# hours}}', ['count' => 1]) ?> </span>
                </div>
                <div class="wc-item delivery">
                    <?= Yii::t('account', 'Last Delivery') ?>
                    <span class="text-right"><?= Yii::t('account', 'about {count, plural, one{# hour} other{# hours}}', ['count' => 7]) ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="visible-xs">
        <ul class="menu-worker-mobile">
            <?php foreach ($menuItems as $menuItem) { ?>
                <li>
                    <?= Html::a($menuItem['label'], $menuItem['link'], ['class' => array_key_exists('active', $menuItem) ? 'active' : '']) ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="worker-info-mobile">
        <div class="worker-info-content">
            <div class="worker-info-block">
                <div>
                    <div class="worker-info-title">
                        <?= Yii::t('account', 'My offers') ?>
                    </div>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'job-list-header']) ?>">
                        <?= Yii::t('account', 'Job offers from this user') . ($jobsCount ? " ({$jobsCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'product-list-header']) ?>">
                        <?= Yii::t('account', 'Products of this user') . ($productsCount ? " ({$productsCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'tender-list-header']) ?>">
                        <?= Yii::t('account', 'Requests of this user') . ($tendersCount ? " ({$tendersCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/account/profile/show', 'id' => $profile->user_id, '#' => 'ptender-list-header']) ?>">
                        <?= Yii::t('account', 'Product requests of this user') . ($productTendersCount ? " ({$productTendersCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/portfolio/show', 'id' => $profile->user_id, '#' => 'portfolio-list-header']) ?>">
                        <?= Yii::t('account', 'Portfolio of this user') . ($newPortfolioDataProvider->totalCount ? ' (' . $newPortfolioDataProvider->totalCount . ')' : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/account/feed/show', 'id' => $profile->user_id, '#' => 'article-list-header']) ?>">
                        <?= Yii::t('account', 'News feed of this user') . ($articlesCount ? " ({$articlesCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link" href="<?= Url::to(['/account/video/show', 'id' => $profile->user_id, '#' => 'video-list-header']) ?>">
                        <?= Yii::t('account', 'Videos of this user') . ($videosCount ? " ({$videosCount})" : '') ?>
                    </a>
                </div>
            </div>
            <div class="worker-info-block" id="about">
                <div class="worker-info-title">
                    <?= Yii::t('account', 'Description') ?>
                    <!--
                                <span class="hint--top" data-hint="<?= Yii::t('account', 'Tell us more about yourself. Buyers are also interested in learning about you as a person.') ?>">
                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                            </span>-->
                </div>
                <p class="prof-descr"><?= nl2br(Html::encode($profile->getDescription())) ?></p>
            </div>
            <?php if ($friendsDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Friends') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $friendsDataProvider,
                        'itemView' => 'listview/friend',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Languages Specified'),
                        'options' => [
                            'id' => 'user-friends',
                            'class' => 'profile-sidebar-friends flex flex-wrap'
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'layout' => "{items}"
                    ]) ?>

                    <?php if ($friendsDataProvider->getTotalCount() > 6) { ?>
                        <div class="profile-sidebar-friends-more-friends text-right">
                            <?= Yii::t('account', 'And {count, plural, one{# more friend} other{# more friends}}', ['count' => $friendsDataProvider->getTotalCount() - 6]); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($subscribersDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Subscribers') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $subscribersDataProvider,
                        'itemView' => 'listview/subscribtion',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Languages Specified'),
                        'options' => [
                            'id' => 'user-friends',
                            'class' => 'profile-sidebar-friends flex flex-wrap'
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'layout' => "{items}"
                    ]) ?>

                    <?php if ($subscribersDataProvider->getTotalCount() > 6) { ?>
                        <div class="profile-sidebar-friends-more-friends text-right">
                            <?= Yii::t('account', 'And {count, plural, one{# more subscriber} other{# more subscribers}}', ['count' => $subscribersDataProvider->getTotalCount() - 6]); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($languageDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Languages') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $languageDataProvider,
                        'itemView' => 'listview/language',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Languages Specified'),
                        'options' => [
                            'id' => 'language-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'worker-info-item'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($skillDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Skills') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $skillDataProvider,
                        'itemView' => 'listview/skill',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Skills Specified'),
                        'options' => [
                            'id' => 'skill-listview',
                            'tag' => 'ul',
                            'class' => 'skill-list'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($specialtyDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Specialties') ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $specialtyDataProvider,
                        'itemView' => 'listview/specialty',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Specialties Specified'),
                        'options' => [
                            'id' => 'specialty-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'worker-info-item'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($educationDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Education') ?>

                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $educationDataProvider,
                        'itemView' => 'listview/education',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Educations Specified'),
                        'options' => [
                            'id' => 'education-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'worker-info-item'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($certificateDataProvider->getTotalCount() > 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Yii::t('account', 'Certification') ?>

                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $certificateDataProvider,
                        'itemView' => 'listview/certificate',
                        'viewParams' => ['editable' => false],
                        'emptyText' => Yii::t('account', 'No Certificates Specified'),
                        'options' => [
                            'id' => 'certificate-listview'
                        ],
                        'itemOptions' => [
                            'class' => 'worker-info-item'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($oldPortfolioDataProvider->getTotalCount() !== 0 || $newPortfolioDataProvider->getTotalCount() !== 0) { ?>
                <div class="worker-info-block">
                    <div class="worker-info-title"><?= Html::a(Yii::t('account', 'Portfolio'), ['/account/portfolio/show', 'id' => $profile->user_id]) ?>
                    </div>

                    <?php if ($oldPortfolioDataProvider->getTotalCount() === 0 && $newPortfolioDataProvider->getTotalCount() === 0) { ?>
                        <div class="empty"><?= Yii::t('account', 'Portfolio is empty') ?></div>
                    <?php } else { ?>
                        <?= ListView::widget([
                            'dataProvider' => $oldPortfolioDataProvider,
                            'itemView' => 'listview/portfolio',
                            'viewParams' => ['editable' => false],
                            'emptyText' => false,
                            'options' => [
                                'id' => 'portfolio-listview'
                            ],
                            'itemOptions' => [
                                'class' => 'worker-info-item'
                            ],
                            'layout' => "{items}"
                        ]) ?>
                        <?= ListView::widget([
                            'dataProvider' => $newPortfolioDataProvider,
                            'itemView' => 'listview/portfolio-new',
                            'viewParams' => ['editable' => false],
                            'emptyText' => false,
                            'options' => [
                                'id' => 'portfolio-listview'
                            ],
                            'itemOptions' => [
                                'class' => 'worker-info-item'
                            ],
                            'layout' => "{items}"
                        ]) ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>

<?php $subscribeUrl = Url::to(['/subscription/toggle']);
$friendshipUrl = Url::to(['/friend/toggle']);
$subscribeText = Yii::t('app', 'Subscribe');
$unsubscribeText = Yii::t('app', 'Unsubscribe');
$friendshipSentText = Yii::t('app', 'Cancel request');
$friendshipText = Yii::t('app', 'Add to friends');

$script = <<<JS
    let countSkill = $('.skill-list li').length;
    let counter = 0;
    let sent = 0;
    
     if (countSkill > 4) {
        let skillHide = countSkill - 4;
        $('.skill-list').after('<a class=\'more-less moreSkill\'>+ See ' + skillHide + ' more</a>');
    }
    
    $('.skill-list div').each(function() {
        if (counter >= 4) {
            $(this).hide();
        }
        counter++;
    });
    
    $(document).on('click', '.moreSkill', function() {
        $('.skill-list div').show();
        $(this).hide();
        
        return false;
    });  
    
    $('[data-action="subscribe"]').on('click', function() {
        if(sent === 0) {
            let entity = $(this).data('entity'),
                id = $(this).data('id'),
                self = $(this);

            $.post("{$subscribeUrl}", {
                entity: entity,
                entity_id: id
            }, function(response) {
                if (response.success === true) {
                    self.toggleClass('active');
                    self.html('</i>&nbsp;' + (self.hasClass('active') ? '{$unsubscribeText}' : '{$subscribeText}'));
                }
            });
        }

        return false;
    });
    
    $('[data-action="friendship"]').on('click', function() {
        if(sent === 0) {
            let entity = $(this).data('entity'),
                id = $(this).data('id'),
                self = $(this);

            $.post("{$friendshipUrl}", {
                entity: entity,
                entity_id: id
            }, function(response) {
                if (response.success === true) {
                    self.toggleClass('active');
                    self.html('</i>&nbsp;' + (self.hasClass('active') ? '{$friendshipText}' : '{$friendshipText}'));
                    if(response.message.length > 0) {
                        alertCall('top', 'success', response.message);
                    }
                } else {
                    alertCall('top', 'warning', response.message);
                }
            });
        }

        return false;
    });
JS;

$this->registerJs($script);