<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 15:25
 */

use common\models\forms\UserLanguageForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var UserLanguageForm $model */
/* @var boolean $editable */

?>

<div class="d-inline"><?= Html::encode($model->title) ?></div> - <span><?= Yii::t('account', Html::encode($model->level)) ?></span>

<?php if ($editable) { ?>
    <div class="edit-prof-icons">
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Edit') ?>">
        <a href="<?= Url::to(['/account/profile-ajax/render-language', 'id' => $model->id]) ?>"
           class="epi-update lang-update" data-id="<?= $model->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        </span>
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="<?= Url::to(['/account/user-language/delete']) ?>" class="lang-delete" data-entity="languages" data-id="<?= $model->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        </span>
    </div>

    <?php $this->registerJs("
        $('.lang-update').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href'),
            	langIdentity = $(this).data('id');
            $.get(link, function(data){
                if(data.success == true) {
                    $('#language-listview div[data-key=' + langIdentity + ']').hide();
                    $('#language-container').append(data.html);
                } else {
                    alertCall('top', 'error', data.message);
                }
			}, 'json');
			
			return false;
        });

        $('.lang-delete').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href'),
            	langIdentity = $(this).data('id');

            $('#confirm-modal').modal()
                .one('click', '#delete', function (e) {
                    $.post(link, {id: langIdentity}, function(data) {
                        if(data.success == true) {
                            //console.log(languages);
                            //languages--;
                            $.pjax({container: '#language-pjax'});

                        } else {
                            alertCall('top', 'error', data.message);
                        }
                    }, 'json');
                });
        });
    ", \yii\web\View::POS_READY, 'lang-edit') ?>
<?php } ?>

