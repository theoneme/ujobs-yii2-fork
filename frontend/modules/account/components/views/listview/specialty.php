<?php

use common\components\CurrencyHelper;
use common\models\forms\UserSpecialtyForm;
use common\models\UserSpecialty;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var UserSpecialtyForm $model */
/* @var boolean $editable */

?>

<?= Html::encode($model->title) ?> - <span><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], intval($model->salary)) ?></span>

<?php if ($editable) { ?>
    <div class="edit-prof-icons">
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Edit') ?>">
        <a href="<?= Url::to(['/account/profile-ajax/render-specialty', 'id' => $model->id]) ?>"
           class="epi-update specialty-update" data-id="<?= $model->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        </span>
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="<?= Url::to(['/account/user-specialty/delete']) ?>" class="specialty-delete" data-entity="specialties"
           data-id="<?= $model->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        </span>
    </div>

    <?php $this->registerJs("
        $('.specialty-update').on('click', function(e) {
            e.preventDefault();
            var specialtyIdentity = $(this).data('id'),
            	link = $(this).attr('href');
            $.get(link, function(data){
                if(data.success == true) {
                    $('#specialty-listview div[data-key=' + specialtyIdentity + ']').hide();
                    $('#specialty-container').append(data.html);
                } else {
                    alertCall('top', 'error', data.message);
                }
			}, 'json');
        });

        $('.specialty-delete').on('click', function(e) {
            e.preventDefault();
            var specialtyIdentity = $(this).data('id'),
            	link = $(this).attr('href');

            $('#confirm-modal').modal()
                .one('click', '#delete', function (e) {
                    $.post(link, {id: specialtyIdentity}, function(data){
                        if(data.success == true) {
                            $.pjax({container: '#specialty-pjax'});

                        } else {
                            alertCall('top', 'error', data.message);
                        }
                    }, 'json');
                });
        });
    ", View::POS_READY, 'specialty-edit') ?>
<?php } ?>
