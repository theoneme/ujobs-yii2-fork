<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.04.2018
 * Time: 16:19
 */

use common\models\UserFriend;
use yii\helpers\Url;

/** @var UserFriend $model */

?>
<div class="profile-sidebar-friend text-center">
    <a href="<?= Url::to(['/account/profile/show', 'id' => $model->user->id]) ?>" class="profile-sidebar-friend-name">
        <img src="<?= $model->user->getThumb('catalog') ?>"
             alt="<?= $model->user->getSellerName() ?>"
             title="<?= $model->user->getSellerName() ?>">
        <div><?= $model->user->getSellerName() ?></div>
    </a>
</div>