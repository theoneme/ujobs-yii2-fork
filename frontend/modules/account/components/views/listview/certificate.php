<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 19:25
 */

use common\models\UserCertificate;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var UserCertificate $model */
/* @var boolean $editable */

?>

<div>
    <?= Html::encode($model->title) ?>
</div><div class="d-inline"><span> <?= Html::encode($model->description) ?></span></div> - <?= Html::encode($model->year) ?>
<?php if ($editable) { ?>
    <div class="edit-prof-icons">
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Edit') ?>">
        <a href="<?= Url::to(['/account/profile-ajax/render-certificate', 'id' => $model->id]) ?>"
           class="epi-update cert-update" data-id="<?= $model->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        </span>
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="<?= Url::to(['/account/user-certificate/delete']) ?>" class="cert-delete" data-entity="certificates" data-id="<?= $model->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        </span>
    </div>

    <?php $this->registerJs("
        $('.cert-update').on('click', function(e) {
            e.preventDefault();
            var certIdentity = $(this).data('id'),
            	link = $(this).attr('href');
            $.get(link, function(data){
                if(data.success == true) {
                    $('#certificate-listview div[data-key=' + certIdentity + ']').hide();
                    $('#certificate-container').append(data.html);
                } else {
                    alertCall('top', 'error', data.message);
                }
			}, 'json');
        });

        $('.cert-delete').on('click', function(e) {
            e.preventDefault();
            var certIdentity = $(this).data('id'),
            	link = $(this).attr('href');

            $('#confirm-modal').modal()
                .one('click', '#delete', function (e) {
                    $.post(link, {id: certIdentity}, function(data){
                        if(data.success == true) {
                            //certificates--;
                            $.pjax({container: '#certificate-pjax'});
                        } else {
                            alertCall('top', 'error', data.message);
                        }
                    }, 'json');
                });
        });
    ", \yii\web\View::POS_READY, 'cert-edit') ?>
<?php } ?>

