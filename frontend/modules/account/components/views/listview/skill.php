<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.11.2016
 * Time: 18:52
 */

use common\models\forms\UserSkillForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var UserSkillForm $model */
/* @var boolean $editable */

?>

<?php
if (!$editable) {
    echo '<li>' . Html::encode($model->title) . '</li>';
} else { ?>
<div class="d-inline"> <?= Html::encode($model->title) ?>
</div><?= $editable ? ' - <span>' . Yii::t('account', Html::encode($model->level)) . '</span>' : '' ?>
    <div class="edit-prof-icons">
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Edit') ?>">
        <a href="<?= Url::to(['/account/profile-ajax/render-skill', 'id' => $model->id]) ?>"
           class="epi-update skill-update" data-id="<?= $model->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        </span>
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="<?= Url::to(['/account/user-skill/delete']) ?>" class="skill-delete" data-entity="skills" data-id="<?= $model->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        </span>
    </div>

    <?php $this->registerJs("
        $('.skill-update').on('click', function(e) {
            e.preventDefault();
            var skillIdentity = $(this).data('id'),
            	link = $(this).attr('href');
            $.get(link, function(data){
                if(data.success == true) {
                    $('#skill-listview div[data-key=' + skillIdentity + ']').hide();
                    $('#skill-container').append(data.html);
                } else {
                    alertCall('top', 'error', data.message);
                }
			}, 'json');
        });

        $('.skill-delete').on('click', function(e) {
            e.preventDefault();
            var skillIdentity = $(this).attr('data-id'),
            	link = $(this).attr('href');

            $('#confirm-modal').modal()
                .one('click', '#delete', function (e) {
                    $.post(link, {id: skillIdentity}, function(data){
                        if(data.success == true) {
                            //skills--;
                            $.pjax({container: '#skill-pjax'});

                        } else {
                            alertCall('top', 'error', data.message);
                        }
                    }, 'json');
                });
        });
    ", \yii\web\View::POS_READY, 'skill-edit') ?>
<?php } ?>
