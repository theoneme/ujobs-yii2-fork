<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.04.2018
 * Time: 15:11
 */
use common\models\UserFriend;
use yii\helpers\Url;

/** @var UserFriend $model */

?>
<div class="profile-sidebar-friend text-center">
    <a href="<?= Url::to(['/account/profile/show', 'id' => $model->targetUser->id]) ?>" class="profile-sidebar-friend-name dont-break-out">
        <img src="<?= $model->targetUser->getThumb('catalog') ?>"
             alt="<?= $model->targetUser->getSellerName() ?>"
             title="<?= $model->targetUser->getSellerName() ?>">
        <div><?= $model->targetUser->getSellerName() ?></div>
    </a>
</div>