<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.11.2016
 * Time: 12:12
 */

use common\models\UserEducation;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var UserEducation $model */
/* @var boolean $editable */

?>

<div>
    <?= Html::encode($model->degree) ?> - <?= Html::encode($model->specialization) ?>
</div><span> <?= Html::encode($model->title) ?>, <?= Html::encode($model->country_title) ?>
    , <?= Html::encode($model->year_start) ?>
    - <?= $model->year_end ? Html::encode($model->year_end) : '...' ?></span>
<?php if ($editable) { ?>
    <div class="edit-prof-icons">
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Edit') ?>">
        <a href="<?= Url::to(['/account/profile-ajax/render-education', 'id' => $model->id]) ?>"
           class="epi-update edu-update" data-id="<?= $model->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        </span>
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="<?= Url::to(['/account/user-education/delete']) ?>" class="edu-delete" data-entity="educations" data-id="<?= $model->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        </span>
    </div>

    <?php $this->registerJs("
        $('.edu-update').on('click', function(e) {
            e.preventDefault();
            var eduIdentity = $(this).data('id'),
            	link = $(this).attr('href');
            $.get(link, function(data){
                if(data.success == true) {
                    $('#education-listview div[data-key=' + eduIdentity + ']').hide();
                    $('#education-container').append(data.html);
                } else {
                    alertCall('top', 'error', data.message);
                }
			}, 'json');
        });

        $('.edu-delete').on('click', function(e) {
            e.preventDefault();
            var eduIdentity = $(this).data('id'),
            	link = $(this).attr('href');

            $('#confirm-modal').modal()
                .one('click', '#delete', function (e) {
                    $.post(link, {id: eduIdentity}, function(data){
                        if(data.success == true) {
                            //educations--;
                            $.pjax({container: '#education-pjax'});
                        } else {
                            alertCall('top', 'error', data.message);
                        }
                    }, 'json');
                });
        });
    ", \yii\web\View::POS_READY, 'edu-edit') ?>
<?php } ?>

