<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.09.2017
 * Time: 21:37
 */
use common\models\UserPortfolio;
use yii\helpers\Html;

/* @var UserPortfolio $model */
/* @var boolean $editable */

?>

<div>
    <?= Html::a(Html::encode($model->title), ['/account/portfolio/view', 'id' => $model->id]) ?>
</div>
