<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.11.2016
 * Time: 12:12
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var \common\models\UserPortfolio $model */
/* @var boolean $editable */

?>

<div>
    <?= Html::encode($model->title) ?>
</div><span><?= Html::encode($model->description) ?></span>

<?php if ($editable) { ?>
    <div class="edit-prof-icons">
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Edit') ?>">
        <a href="<?= Url::to(['/account/profile-ajax/render-portfolio', 'id' => $model->id]) ?>"
           class="epi-update portfolio-update" data-id="<?= $model->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        </span>
        <span class="hint--top" data-hint="<?= Yii::t('account', 'Delete') ?>">
        <a href="<?= Url::to(['/account/user-portfolio/delete']) ?>" class="portfolio-delete"
           data-id="<?= $model->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        </span>
    </div>

    <?php $this->registerJs("
        $('.portfolio-update').on('click', function(e) {
            e.preventDefault();
            var portIdentity = $(this).data('id'),
            	link = $(this).attr('href');
            $.get(link, function(data){
                if(data.success == true) {
                    $('#portfolio-listview div[data-key=' + portIdentity + ']').hide();
                    $('#portfolio-container').append(data.html);
                } else {
                    alertCall('top', 'error', data.message);
                }
			}, 'json');
        });

        $('.portfolio-delete').on('click', function(e) {
            e.preventDefault();
            var portIdentity = $(this).data('id'),
            	link = $(this).attr('href');

            $('#confirm-modal').modal()
                .one('click', '#delete', function (e) {
                    $.post(link, {id: portIdentity}, function(data){
                        if(data.success == true) {
                            //portfolio--;
                            $.pjax({container: '#portfolio-pjax'});

                        } else {
                            alertCall('top', 'error', data.message);
                        }
                    }, 'json');
                });
        });
    ", \yii\web\View::POS_READY, 'portfolio-edit') ?>
<?php } ?>
