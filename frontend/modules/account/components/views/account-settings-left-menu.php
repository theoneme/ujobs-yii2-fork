<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.11.2016
 * Time: 11:40
 */
use yii\helpers\Html;

/* @var array $links */

?>

<nav class="profa-menu">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profa-menu">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<div class="other-settings visible-xs"><?= Yii::t('account', 'Settings') ?></div>
	<div class="clear"></div>
	<div class="collapse navbar-collapse" id="profa-menu">
		<ul class="nav nav-tabs">
			<?php foreach ($links as $link) { ?>
				<li><a <?= $link['active'] ? 'class="active"' : '' ?>
							href="<?= $link['href'] ?>"><?= $link['title'] ?></a></li>
			<?php } ?>
		</ul>
	</div>
</nav>
<!--<div class="prof-aside-block row aside-post text-center">
	<div class="col-md-12">
		<div class="post-aside-title"><?= Yii::t('account', 'Need Money?') ?></div>
		<div class="post-mes">
			<?= Yii::t('account', 'Post the services that you can make and receive orders') ?>
		</div>
		<div class="post-aside-img post-aside-money-icon"></div>
		<?= Html::a(Yii::t('account', 'Post Service'), ['/entity/global-create'], [
			'class' => 'btn-middle',
			'style' => 'line-height: 15px; padding: 5px 10px; white-space: normal;'
		]) ?>
	</div>
</div>-->

