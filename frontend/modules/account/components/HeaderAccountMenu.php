<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 11:41
 */

namespace frontend\modules\account\components;

use frontend\modules\account\models\JobSearch;
use frontend\modules\account\models\OrderSearch;
use yii\base\Widget;
use Yii;
use yii\helpers\Url;

/**
 * Class HeaderAccountMenu
 * @package frontend\modules\account\components
 */
class HeaderAccountMenu extends Widget
{
    public $template = 'header-account-menu';
    public $active = null;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->active = Yii::$app->controller->action->id;

        $links = [
            [
                'href' => Url::to(['/account/profile/show', 'id' => Yii::$app->user->identity->getId()]),
                'title' => Yii::t('account', 'My Profile'),
                'active' => $this->active == 'show' ? true : false,
            ],
            [
                'href' => Url::to(['/account/service/dashboard']),
                'title' => Yii::t('account', 'Dashboard'),
                'active' => $this->active == 'dashboard' ? true : false,
            ],
            [
                'href' => '#',
                'title' => Yii::t('account', 'My Services') . '&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>',
                'active' => in_array($this->active, ['selling', 'manage-sales', 'buyer-requests', 'my-jobs', 'create-job']) ? true : false,
                'children' => [
                    [
                        'href' => Url::to(['/account/sell/list', 'status' => OrderSearch::STATUS_ACTIVE]),
                        'title' => Yii::t('account', 'Manage Sales'),
                        'active' => $this->active == 'manage-sales' ? true : false,
                    ],
                    /*[
                        'href' => '#',
                        'title' => Yii::t('account', 'Buyer Requests'),
                        'active' => $this->active == 'buyer-requests' ? true : false,
                    ],*/
                    [
                        'href' => Url::to(['/account/job/list', 'status' => JobSearch::STATUS_ACTIVE]),
                        'title' => Yii::t('account', 'My Jobs'),
                        'active' => $this->active == 'my-jobs' ? true : false,
                    ],
                    [
                        'href' => Url::to(['/entity/global-create']),
                        'title' => Yii::t('account', 'Create Job'),
                        'active' => $this->active == 'create-job' ? true : false,
                    ],
                ]
            ],
            [
                'href' => '#',
                'title' => Yii::t('account', 'My Requests') . '&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>',
                'active' => in_array($this->active, ['requests', 'manage-requests', 'create-request']) ? true : false,
                'children' => [
                    [
                        'href' => Url::to(['/account/tender-response/list', 'status' => OrderSearch::STATUS_RESPONSE_TO_ME]),
                        'title' => Yii::t('account', 'Responses on requests'),
                        'active' => $this->active == 'responses-on-my-tenders' ? true : false,
                    ],
                    [
                        'href' => Url::to(['/account/tender/list', 'status' => JobSearch::STATUS_ACTIVE]),
                        'title' => Yii::t('account', 'Manage Requests'),
                        'active' => $this->active == 'manage-requests' ? true : false,
                    ],
                    [
                        'href' => Url::to(['/entity/global-create', 'type' => 'tender-service']),
                        'title' => Yii::t('account', 'Create Request'),
                        'active' => $this->active == 'create-request' ? true : false,
                    ],
                ]
            ],
            [
                'href' => '#',
                'title' => Yii::t('account', 'Contact us') . '&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>',
                'active' => in_array($this->active, ['Contact us', 'my-buyers', 'my-sellers']) ? true : false,
                'children' => [
                    [
                        'href' => '#',
                        'title' => Yii::t('account', 'My Buyers'),
                        'active' => $this->active == 'my-buyers' ? true : false,
                    ],
                    [
                        'href' => '#',
                        'title' => Yii::t('account', 'My Sellers'),
                        'active' => $this->active == 'my-sellers' ? true : false,
                    ],
                ]
            ],
            [
                'href' => Url::to(['/account/service/account-settings']),
                'title' => Yii::t('account', 'Settings'),
                'active' => in_array($this->active, ['settings', 'account-actions', 'account-settings', 'public-profile-settings', 'security-settings']) ? true : false,
            ],
            [
                'href' => Url::to(['/account/payment/index']),
                'title' => Yii::t('account', 'Your balance'),
                'active' => $this->active == 'index' ? true : false,
            ],
        ];

        return $this->render($this->template, ['items' => $links]);
    }
}