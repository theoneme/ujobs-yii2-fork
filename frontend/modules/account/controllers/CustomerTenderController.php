<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.01.2017
 * Time: 18:56
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\modules\store\models\Order;
use frontend\modules\account\models\OrderSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;

/**
 * Class CustomerTenderController
 * @package frontend\modules\account\controllers\
 */
class CustomerTenderController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['list'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $searchModel = new OrderSearch([
            'customer_id' => Yii::$app->user->identity->getCurrentId(),
            'product_type' => [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER],
            'type' => OrderSearch::TYPE_TENDER
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@frontend/modules/account/views/tender-orders/orders', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param $searchModel OrderSearch
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = OrderSearch::getTenderStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a($label . "&nbsp;({$searchModel->getStatusCount($status)})", ['/account/customer-tender/list', 'status' => $status]),
                ['class' => $status === $searchModel->status ? 'active' : '']
            );
        }
        return $html;
    }
}