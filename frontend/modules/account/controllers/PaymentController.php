<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 17:55
 */

namespace frontend\modules\account\controllers;

use common\components\CurrencyHelper;
use common\controllers\FrontEndController;
use common\models\PaymentHistory;
use common\models\user\User;
use common\models\UserWallet;
use common\models\UserWithdrawal;
use common\models\WalletTransferRequest;
use frontend\models\WithdrawalRequestForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Response;

/**
 * Class PaymentController
 * @package frontend\modules\account\controllers
 */
class PaymentController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['withdrawal-request', 'transfer', 'transfer-request', 'transfer-confirm'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToUseWallets();
                        }
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to access this page'));
                    if(isGuest()) {
                        return $this->redirect(['/site/index']);
                    }

                    return $this->redirect(['/account/payment/index']);
                },
            ],
        ];
    }

    public function actionIndex()
    {
        /* @var $user User*/
        $user = Yii::$app->user->identity->company_user_id ? Yii::$app->user->identity->companyUser : Yii::$app->user->identity;
        $wallets = $user->wallets;
        foreach (['RUB', 'USD'] as $code) {
            if (!isset($wallets[$code])) {
                $wallet = new UserWallet(['user_id' => $user->id, 'currency_code' => $code, 'balance' => 0]);
                $wallet->save();
                $wallets = array_merge([$code => $wallet], $wallets);
            }
        }
        return $this->render('historyNew', [
            'wallets' => $wallets,
            'paymentData' => WithdrawalRequestForm::getUserPaymentData(),
            'paymentDataLong' => WithdrawalRequestForm::getUserPaymentDataLong(),
            'withdrawalForm' => new WithdrawalRequestForm(),
            'currencyRatios' => Json::encode(CurrencyHelper::getRatios()),
            'transferRequest' => new WalletTransferRequest(),
            'confirmationMethods' => WalletTransferRequest::getConfirmationMethods()
        ]);
    }

    public function actionWithdrawalRequest()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $output = [
                'success' => false,
                'message' => Yii::t('account', 'Something went wrong')
            ];

            $input = Yii::$app->request->post();

            $model = new WithdrawalRequestForm();

            if ($model->load($input)) {
                /* @var $user User*/
                $user = Yii::$app->user->identity->company_user_id ? Yii::$app->user->identity->companyUser : Yii::$app->user->identity;
                $sum = UserWithdrawal::find()->where([
                    'status' => UserWithdrawal::STATUS_WAITING,
                    'user_id' => $user->id,
                    'currency_code' => $model->currency_code
                ])->sum('amount');
                if (($sum + $model->amount) <= $user->wallets[$model->currency_code]->balance && $model->save()) {
                    $output['success'] = true;
                    $output['message'] = Yii::t('account', 'Withdrawal request sent');
                } else {
                    $output['message'] = Yii::t('account', 'You don`t have enough money on balance. If you have sent withdrawal request recently - wait until it is processed.');
                }
            }

            return $output;
        }

        return $this->redirect(['/account/payment/index']);
    }

    public function actionTransfer()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $output = [
                'success' => false,
                'message' => Yii::t('account', 'Something went wrong')
            ];

            $input = Yii::$app->request->post();
            if (isset($input['from_id']) && isset($input['to_id'])) {
                /**
                 * @var $walletFrom UserWallet
                 * @var $walletTo UserWallet
                 */
                $walletFrom = UserWallet::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'id' => $input['from_id']])->one();
                $walletTo = UserWallet::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'id' => $input['to_id']])->one();
                if ($walletFrom !== null && $walletTo !== null) {
                    if ($input['amount'] <= $walletFrom->balance) {
                        $ratios = CurrencyHelper::getRatios();
                        $amountTo = floor($input['amount'] / $ratios[$walletTo->currency_code][$walletFrom->currency_code]);
                        if ($amountTo > 0) {
                            // Прибавляем на одном кошельке
                            $paymentHistory = new PaymentHistory();
                            $paymentHistory->amount = $amountTo;
                            $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_POS;
                            $paymentHistory->template = PaymentHistory::TEMPLATE_MONEY_TRANSFER;
                            $paymentHistory->type = PaymentHistory::TYPE_OPERATION_TRANSFER;
                            $paymentHistory->user_id = $walletTo->user_id;
                            $paymentHistory->currency_code = $walletTo->currency_code;
                            $paymentHistory->save();
                            // Отнимаем на другом
                            $paymentHistory = new PaymentHistory();
                            $paymentHistory->amount = $input['amount'];
                            $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_NEG;
                            $paymentHistory->template = PaymentHistory::TEMPLATE_MONEY_TRANSFER;
                            $paymentHistory->type = PaymentHistory::TYPE_OPERATION_TRANSFER;
                            $paymentHistory->user_id = $walletFrom->user_id;
                            $paymentHistory->currency_code = $walletFrom->currency_code;
                            $paymentHistory->save();
                            $output['success'] = true;
                            $output['message'] = Yii::t('account', 'Money successfully transferred');
                        } else {
                            $output['message'] = Yii::t('account', 'Transfer amount must be greater than 0');

                        }
                    } else {
                        $output['message'] = Yii::t('account', 'Your account has not enough money to transfer');
                    }
                } else {
                    $output['message'] = Yii::t('account', 'Wallet not found');
                }
            } else {
                $output['message'] = Yii::t('account', 'Wallet number not specified');
            }

            return $output;
        }

        return $this->redirect(['/account/payment/index']);
    }

    /**
     * @return array|Response
     */
    public function actionTransferRequest()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $output = [
                'success' => false,
                'message' => Yii::t('account', 'Something went wrong')
            ];

            $transferRequest = new WalletTransferRequest();
            $transferRequest->load(Yii::$app->request->post());
            if ($transferRequest->save()) {
                $output['success'] = true;
                $output['request_id'] = $transferRequest->id;
                $output['wallet_from'] = $transferRequest->walletFrom->getName() . $transferRequest->walletFrom->id;
                $output['wallet_to'] = $transferRequest->walletTo->getName() . $transferRequest->walletTo->id;
                $output['transfer_amount'] = CurrencyHelper::format($transferRequest->walletFrom->currency_code, $transferRequest->amount);
            } else {
                $output['message'] = reset($transferRequest->errors);
            }

            return $output;
        }

        return $this->redirect(['/account/payment/index']);
    }

    /**
     * @return array|Response
     */
    public function actionTransferConfirm()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $output = [
                'success' => false,
                'message' => Yii::t('account', 'Something went wrong')
            ];
            /* @var $transferRequest WalletTransferRequest */
            $transferRequest = WalletTransferRequest::find()->where(['id' => Yii::$app->request->post('id')])->one();
            if ($transferRequest !== null) {
                if ($transferRequest->token === Yii::$app->request->post('token')) {
                    $transferRequest->status = WalletTransferRequest::STATUS_CONFIRMED;
                    $transferRequest->save();
                    $output['success'] = true;
                    $output['message'] = Yii::t('account', 'Money successfully transferred');
                } else {
                    $output['message'] = Yii::t('account', 'Invalid confirmation code');
                }
            } else {
                $output['message'] = Yii::t('account', 'Wallet not found');
            }

            return $output;
        }

        return $this->redirect(['/account/payment/index']);
    }
}