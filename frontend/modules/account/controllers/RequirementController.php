<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.01.2018
 * Time: 13:06
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use frontend\modules\account\helpers\DynamicFormHelper;
use frontend\modules\account\models\history\WizardDecisionForm;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class RequirementController
 * @package frontend\modules\account\controllers
 */
class RequirementController extends FrontEndController
{
    /**
     * @param $order_id
     * @return array|string
     */
    public function actionPost($order_id)
    {
        $input = Yii::$app->request->post();
        $model = new WizardDecisionForm([], DynamicFormHelper::getAdvancedConfig($order_id));
        $model->order_id = $order_id;

        if (empty($input)) {
            return $this->render('wizard', [
                'model' => $model,
            ]);
        } else {
            if (Yii::$app->request->isAjax) {
                $model->load($input);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                $model->load($input);
                if ($model->save()) {
                    return $this->redirect(['/account/order/history', 'id' => $model->order_id]);
                } else {
                    return $this->render('wizard', [
                        'model' => $model,
                    ]);
                }
            }
        }
    }
}