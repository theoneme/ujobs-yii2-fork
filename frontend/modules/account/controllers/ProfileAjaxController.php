<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.11.2016
 * Time: 16:10
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\ContentTranslation;
use common\models\forms\UserLanguageForm;
use common\models\forms\UserSkillForm;
use common\models\forms\UserSpecialtyForm;
use common\models\user\Profile;
use common\models\UserAttribute;
use common\models\UserCertificate;
use common\models\UserEducation;
use common\models\UserPortfolio;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProfileAjaxController extends FrontEndController
{
    const MAX_SKILLS_AVAILABLE = 10;
    const MAX_CERTIFICATES_AVAILABLE = 10;
    const MAX_EDUCATIONS_AVAILABLE = 10;
    const MAX_PORTFOLIO_AVAILABLE = 10;
    const MAX_SPECIALTIES_AVAILABLE = 10;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'update-profile',
                        'render-language',
                        'render-public-language',
                        'render-public-skill',
                        'render-skill',
                        'render-public-specialty',
                        'render-specialty',
                        'render-certificate',
                        'render-education',
                        'render-portfolio'
                    ], 'roles' => ['@']],
                ],
            ],
        ];
    }

    /**
     * @return array|bool
     */
    public function actionUpdateProfile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var Profile $model */
        $model = Profile::findOne(Yii::$app->user->identity->getId());
        if($model !== null) {
            $input = Yii::$app->request->post();

            if (isset($input['ajax'])) {
                $model->load($input);
                return ActiveForm::validate($model);
            }

            $model->load($input);
            if ($model->save()) {
                if (isset($input['ContentTranslation'][Yii::$app->language]['content'])) {
                    $translation = $model->translations[Yii::$app->language] ?? new ContentTranslation(['entity' => 'profile', 'entity_id' => $model->user_id, 'locale' => Yii::$app->language]);
                    $translation->content = $input['ContentTranslation'][Yii::$app->language]['content'];
                    if ($translation->isNewRecord) {
                        $translation->title = $model->getSellerName();
                    }
                    $translation->save();
                };
                if (!empty($input['city_attribute_id']) && $cityAttributeValue = AttributeValue::findOne((int)$input['city_attribute_id'])) {
                    $cityAttribute = UserAttribute::findOrCreate([
                        'user_id' => $model->user_id,
                        'entity_alias' => 'city',
                        'attribute_id' => $cityAttributeValue->attribute_id,
                        'locale' => 'ru-RU',
                    ], false);
                    $cityAttribute->value = (string)$cityAttributeValue->id;
                    $cityAttribute->value_alias = $cityAttributeValue->alias;
                    $cityAttribute->save();
                }

                return ['result' => true];
            }
        }

        return ['result' => false];
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderEducation($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $route = Url::to(['/account/user-education/create']);

        if ($iterator < self::MAX_EDUCATIONS_AVAILABLE) {
            $model = new UserEducation();
            if ($id != null) {
                $model = UserEducation::find()->where(['id' => $id, 'user_id' => Yii::$app->user->identity->getId()])->one();
                $route = Url::to(['/account/user-education/update', 'id' => $model->id]);
            }

            $output = [
                'html' => $this->renderAjax('user-education-partial', [
                    'model' => $model,
                    'key' => (int)$id,
                    'formid' => (int)$iterator,
                    'route' => $route
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} educations", ['count' => self::MAX_EDUCATIONS_AVAILABLE])
        ];
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderLanguage($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $route = Url::to(['/account/user-language/create']);

        if ($iterator < UserLanguageForm::MAX_LANGUAGES_AVAILABLE) {
            $model = new UserLanguageForm();
            if ($id != null) {
                $model = UserLanguageForm::findOne(['user_attribute.id' => $id, 'user_id' => Yii::$app->user->identity->getId()]);
                $route = Url::to(['/account/user-language/update', 'id' => $model->id]);
            }
            $output = [
                'html' => $this->renderAjax('user-language-partial', [
                    'model' => $model,
                    'key' => (int)$id,
                    'formid' => (int)$iterator,
                    'route' => $route
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} languages", ['count' => UserLanguageForm::MAX_LANGUAGES_AVAILABLE])
        ];
    }

    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicLanguage($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($iterator < UserLanguageForm::MAX_LANGUAGES_AVAILABLE) {
            $model = new UserLanguageForm();

            $output = [
                'html' => $this->renderAjax('@frontend/modules/account/views/service/settings/listview/language', [
                    'model' => $model,
                    'index' => (int)$iterator,
                    'isNew' => true
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} languages", ['count' => UserLanguageForm::MAX_LANGUAGES_AVAILABLE])
        ];
    }

    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicSkill($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new UserSkillForm();

        $output = [
            'html' => $this->renderAjax('@frontend/modules/account/views/service/settings/listview/skill', [
                'model' => $model,
                'index' => (int)$iterator,
                'isNew' => true
            ]),
            'success' => true
        ];

        return $output;
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderSkill($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $route = Url::to(['/account/user-skill/create']);

        if ($iterator < self::MAX_SKILLS_AVAILABLE) {
            $model = new UserSkillForm();
            if ($id != null) {
                $model = UserSkillForm::findOne(['user_attribute.id' => $id, 'user_id' => Yii::$app->user->identity->getId()]);
                $route = Url::to(['/account/user-skill/update', 'id' => $model->id]);
            }

            $output = [
                'html' => $this->renderAjax('user-skill-partial', [
                    'model' => $model,
                    'key' => (int)$id,
                    'formid' => (int)$iterator,
                    'route' => $route
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} skills", ['count' => self::MAX_SKILLS_AVAILABLE])
        ];
    }

    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicSpecialty($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new UserSpecialtyForm();

        $output = [
            'html' => $this->renderAjax('@frontend/modules/account/views/service/settings/listview/specialty', [
                'model' => $model,
                'index' => (int)$iterator,
                'isNew' => true
            ]),
            'success' => true
        ];

        return $output;
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderSpecialty($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $route = Url::to(['/account/user-specialty/create']);

        if ($iterator < self::MAX_SPECIALTIES_AVAILABLE) {
            $model = new UserSpecialtyForm();
            if ($id != null) {
                $model = UserSpecialtyForm::findOne(['user_attribute.id' => $id, 'user_id' => Yii::$app->user->identity->getId()]);
                $route = Url::to(['/account/user-specialty/update', 'id' => $model->id]);
            }

            $output = [
                'html' => $this->renderAjax('user-specialty-partial', [
                    'model' => $model,
                    'key' => (int)$id,
                    'formid' => (int)$iterator,
                    'route' => $route
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} specialties", ['count' => self::MAX_SPECIALTIES_AVAILABLE])
        ];
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderCertificate($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $route = Url::to(['/account/user-certificate/create']);

        if ($iterator < self::MAX_CERTIFICATES_AVAILABLE) {
            $model = new UserCertificate();
            if ($id != null) {
                $model = UserCertificate::find()->where(['id' => $id, 'user_id' => Yii::$app->user->identity->getId()])->one();
                $route = Url::to(['/account/user-certificate/update', 'id' => $model->id]);
            }

            $output = [
                'html' => $this->renderAjax('user-certificate-partial', [
                    'model' => $model,
                    'key' => (int)$id,
                    'formid' => (int)$iterator,
                    'route' => $route
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} certificates", ['count' => self::MAX_CERTIFICATES_AVAILABLE])
        ];
    }

    public function actionRenderPortfolio($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $route = Url::to(['/account/user-portfolio/create']);

        if ($iterator < self::MAX_PORTFOLIO_AVAILABLE) {
            $model = new UserPortfolio();
            if ($id != null) {
                $model = UserPortfolio::find()->where(['id' => $id, 'user_id' => Yii::$app->user->identity->getId()])->one();
                $route = Url::to(['/account/user-portfolio/update', 'id' => $model->id]);
            }

            $output = [
                'html' => $this->renderAjax('user-portfolio-partial', [
                    'model' => $model,
                    'key' => (int)$id,
                    'formid' => (int)$iterator,
                    'route' => $route
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('account', "You can't set more than {count} portfolios", ['count' => self::MAX_PORTFOLIO_AVAILABLE])
        ];
    }
}