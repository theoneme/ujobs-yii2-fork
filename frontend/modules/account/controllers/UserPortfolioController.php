<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.11.2016
 * Time: 17:31
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\UserPortfolio;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class UserPortfolioController
 * @package frontend\modules\account\controllers
 */
class UserPortfolioController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete'
                        ],
                        'roles' => ['@'],

                    ]
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new UserPortfolio();

        Yii::$app->response->format = Response::FORMAT_JSON;
        $input = Yii::$app->request->post();

        if (isset($input['ajax'])) {
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        } else {
            $model->load($input);

            if ($model->save()) {
                return [
                    'result' => true
                ];
            }
        }
        return false;
    }

    public function actionUpdate($id)
    {
        /* @var UserPortfolio $model */
        $model = UserPortfolio::findOne($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $input = Yii::$app->request->post();

        if (isset($input['ajax'])) {
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        } else {
            $model->load($input);

            if ($model->save()) {
                return [
                    'result' => true
                ];
            }
        }
        return false;
    }

    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');

        /* @var UserPortfolio $model */
        $model = UserPortfolio::findOne((int)$id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model) {
            $model->delete();

            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'message' => 'PH'
        ];
    }
}