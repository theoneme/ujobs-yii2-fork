<?php

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Job;
use common\models\Message;
use common\models\Offer;
use common\models\Page;
use common\models\Review;
use common\models\user\Profile;
use common\models\Video;
use common\modules\board\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Class VideoController
 * @package frontend\modules\account\controllers
 */
class VideoController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'load-more' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['show', 'load-more', 'show-as-customer'], 'roles' => ['@', '?']],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionShow($id)
    {
        return $this->prepareShowPage($id, false);
    }

    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShowAsCustomer($id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage($id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Profile $profile */
        $profile = Profile::find()->where(['profile.user_id' => $id])->one();

        if ($profile === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested profile is not found'));
            return $this->redirect(['/category/pro-catalog']);
        }

        $this->registerMetaTags($profile);
        $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Videos of user {user} on site uJobs.me", ['user' => $profile->getSellerName()]);

        if (!$as_customer && hasAccess($profile->user_id)) {
            $template = 'show_admin';
            $editable = true;
        }

        $videoQuery = Video::find()->where(['user_id' => $profile->user_id])->orderBy('updated_at desc');

        if (!$editable) {
            $videoQuery->andWhere(['video.status' => Video::STATUS_ACTIVE]);
            $contactAccessInfo = isGuest() ? ['allowed' => false] : Yii::$app->user->identity->isAllowedToCreateConversation($profile->user);
        } else {
            $videoQuery->andWhere(['not', ['video.status' => Video::STATUS_DELETED]]);
        }

        $videosDataProvider = new ActiveDataProvider(['query' => $videoQuery, 'pagination' => ['pageParam' => 'video-page', 'pageSize' => 20]]);

        $reviewCondition = ['created_for' => $profile->user_id, 'type' => Review::TYPE_SELLER_REVIEW];
        if (hasAccess($profile->user_id)) {
            $reviewCondition['is_fake'] = false;
        }

        $reviewQuery = Review::find()->joinWith(['createdBy'])->where($reviewCondition);
        $reviewDataProvider = new ActiveDataProvider(['query' => $reviewQuery, 'pagination' => false]);

        return $this->render($template, [
            'profile' => $profile,
            'editable' => $editable,
            'videosDataProvider' => $videosDataProvider,
            'message' => new Message(),
            'offer' => new Offer(),
            'contactAccessInfo' => $contactAccessInfo,
            'reviewDataProvider' => $reviewDataProvider
        ]);
    }
}