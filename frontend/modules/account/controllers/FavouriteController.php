<?php

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Job;
use common\models\UserFavourite;
use common\models\UserFavouriteProduct;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Class FavouriteController
 * @package frontend\modules\account\controllers
 */
class FavouriteController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'toggle' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'toggle',
                            'toggle-product',
                        ],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = Job::find()->joinWith(['translation', 'category', 'extra', 'attachments', 'favourites', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations', 'packages'])
            ->where(['user_favourite.user_id' => Yii::$app->user->identity->id])
            ->andWhere(['not', ['job.status' => Job::STATUS_DELETED]])
            ->groupBy('job.id');
        $dataProvider = new ActiveDataProvider(['query' => $query]);
        $query2 = clone $query;
        $jobs = $query2->select('job.id, job.category_id, job.user_id')->asArray()->all();
//        $similarCategoryIds = $query2->select('job.category_id')->groupBy('job.category_id')->column();
//        $favouritesIds = $query2->select('job.id')->groupBy('job.id')->column();

        $similarQuery = Job::find()->joinWith(['translation', 'packages', 'category', 'extra', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
            ->where([
                'job.category_id' => ArrayHelper::getColumn($jobs, 'category_id'),
                'job.status' => Job::STATUS_ACTIVE,
                'job.type' => 'job'
            ])->andWhere(['not', ['job.id' => ArrayHelper::getColumn($jobs, 'id')]])
            ->groupBy('job.id')
            ->limit(10);
        $similarDataProvider = new ActiveDataProvider(['query' => $similarQuery, 'pagination' => false]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'similarDataProvider' => $similarDataProvider,
        ]);
    }

    /**
     * Add to or Delete from favourites
     */
    public function actionToggle()
    {
        $job_id = Yii::$app->request->post('job_id');
        $model = UserFavourite::find()->where([
            'user_id' => Yii::$app->user->identity->id,
            'job_id' => $job_id
        ])->one();
        if ($model !== null) {
            $model->delete();
        } else {
            $model = new UserFavourite();
            $model->user_id = Yii::$app->user->identity->id;
            $model->job_id = $job_id;
            $model->save();
        }
    }

    /**
     * Add to or Delete from favourites
     */
    public function actionToggleProduct()
    {
        $product_id = Yii::$app->request->post('product_id');
        $model = UserFavouriteProduct::find()->where([
            'user_id' => Yii::$app->user->identity->id,
            'product_id' => $product_id
        ])->one();
        if ($model !== null) {
            $model->delete();
        } else {
            $model = new UserFavouriteProduct();
            $model->user_id = Yii::$app->user->identity->id;
            $model->product_id = $product_id;
            $model->save();
        }
    }
}