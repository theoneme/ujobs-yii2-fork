<?php

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Job;
use common\models\Message;
use common\models\Offer;
use common\models\Page;
use common\models\Review;
use common\models\user\Profile;
use common\modules\board\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Class FeedController
 * @package frontend\modules\account\controllers
 */
class FeedController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'load-more' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['show', 'load-more', 'show-as-customer'], 'roles' => ['@', '?']],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionShow($id)
    {
        return $this->prepareShowPage($id, false);
    }

    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShowAsCustomer($id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage($id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Profile $profile */
        $profile = Profile::find()->where(['profile.user_id' => $id])->one();

        if ($profile === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested profile is not found'));
            return $this->redirect(['/category/pro-catalog']);
        }

        $this->registerMetaTags($profile);
        $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "News feed of user {user} on site uJobs.me", ['user' => $profile->getSellerName()]);

        if (!$as_customer && hasAccess($profile->user_id)) {
            $template = 'show_admin_new';
            $editable = true;
        }

        $articleQuery = Page::find()->joinWith(['translation'])->where(['type' => Page::TYPE_ARTICLE, 'user_id' => $profile->user_id])->orderBy('publish_date desc');
        $jobsQuery = Job::find()
            ->joinWith(['translation', 'favourite', 'attachments', 'user', 'profile', 'packages'])
            ->where(['job.type' => 'job', 'job.user_id' => $profile->user_id, 'job.status' => Job::STATUS_ACTIVE])
            ->groupBy('job.id')
            ->limit(10);
        $productsQuery =  Product::find()
            ->joinWith(['translation', 'attachments', 'user', 'profile'])
            ->where(['product.type' => 'product', 'product.user_id' => $profile->user_id, 'product.status' => Product::STATUS_ACTIVE])
            ->groupBy('product.id')
            ->limit(10);

        if (!$editable) {
            $jobsQuery->andWhere(['job.status' => Job::STATUS_ACTIVE]);
            $productsQuery->andWhere(['product.status' => Product::STATUS_ACTIVE]);
            $articleQuery->andWhere(['page.status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]]);
            $contactAccessInfo = isGuest() ? ['allowed' => false] : Yii::$app->user->identity->isAllowedToCreateConversation($profile->user);
        } else {
            $jobsQuery->andWhere(['not', ['job.status' => Job::STATUS_DELETED]]);
            $productsQuery->andWhere(['not', ['product.status' => Product::STATUS_DELETED]]);
            $articleQuery->andWhere(['not', ['page.status' => Page::STATUS_DELETED]]);
        }

        $articlesDataProvider = new ActiveDataProvider(['query' => $articleQuery, 'pagination' => ['pageParam' => 'article-page', 'pageSize' => 5]]);
        $userEntities = $jobsQuery->all();
        $userEntities = array_merge($userEntities, $productsQuery->all());
        shuffle($userEntities);

        $reviewCondition = ['created_for' => $profile->user_id, 'type' => Review::TYPE_SELLER_REVIEW];
        if (hasAccess($profile->user_id)) {
            $reviewCondition['is_fake'] = false;
        }

        $reviewQuery = Review::find()->joinWith(['createdBy'])->where($reviewCondition);
        $reviewDataProvider = new ActiveDataProvider(['query' => $reviewQuery, 'pagination' => false]);

        return $this->render($template, [
            'profile' => $profile,
            'editable' => $editable,
            'articlesDataProvider' => $articlesDataProvider,
            'message' => new Message(),
            'offer' => new Offer(),
            'contactAccessInfo' => $contactAccessInfo,
            'userEntities' => $userEntities,
            'reviewDataProvider' => $reviewDataProvider
        ]);
    }

    /**
     * @param $user_id
     * @param bool $mobile
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionLoadMore($user_id, $mobile = false)
    {
        $template = $mobile ? 'page-list-mobile' : 'page-list';
        $itemSelector = $mobile ? 'data-scroll-item-mobile' : 'data-scroll-item';
        $container = $mobile ? 'mob-work-item' : 'cat-middle-block';
        $articles = Page::find()
            ->joinWith(['translation'])
            ->where(['page.type' => Page::TYPE_ARTICLE, 'user_id' => $user_id])
            ->andWhere(['page.status' => Page::STATUS_ACTIVE])
            ->limit(Yii::$app->request->post('limit', 5))
            ->offset(Yii::$app->request->post('offset', 0))
            ->all();
        return array_reduce($articles, function($carry, $var) use ($template, $itemSelector, $container){
            return $carry . '<div class="' . $container . '" ' . $itemSelector . '>' . $this->renderPartial('@frontend/modules/filter/views/' . $template, ['model' => $var]) . '</div>';
        }, '');
    }
}