<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2017
 * Time: 17:50
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Attachment;
use common\models\Category;
use common\models\Comment;
use common\models\Message;
use common\models\Offer;
use common\models\Review;
use common\models\user\Profile;
use common\models\UserPortfolio;
use common\services\CounterService;
use frontend\models\PostCommentForm;
use frontend\modules\account\models\PostPortfolioForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class PortfolioController
 * @package frontend\modules\account\controllers
 */
class PortfolioController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['render-extra'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['show', 'view-ajax', 'view', 'show-as-customer'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['create', 'ajax-create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToPost() && UserPortfolio::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['view-ajax', 'render-extra', 'ajax-create'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $referrer = Yii::$app->request->referrer;
        if ($action->id == 'view' && preg_match('/^http(s)?:\/\/ujobs.me.*/ui', $referrer) && !preg_match("/.*\/account\/portfolio\/view.*/ui", $referrer)) {
            Url::remember($referrer, 'portfolioReturnUrl');
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShow($id, $share = false)
    {
        return $this->prepareShowPage($id, false, $share);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @param $share
     * @return string
     */
    private function prepareShowPage($id, $as_customer = false, $share)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Profile $profile */
        $profile = Profile::find()->joinWith(['user'])->where(['profile.user_id' => $id])->one();

        if ($profile === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested profile is not found'));
            return $this->redirect(['/category/pro-catalog']);
        }

        if ((boolean)$profile->user->is_company === true) {
            return $this->redirect(['/company/portfolio/show', 'id' => $profile->user->company->id]);
        }

        if (!$as_customer && hasAccess($profile->user_id)) {
            $template = 'show_admin_new';
            $editable = true;
        }

        $portfolioQuery = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_portfolio.user_id' => $profile->user_id]);
        if (!$editable) {
            $portfolioQuery->andWhere(['user_portfolio.status' => UserPortfolio::STATUS_ACTIVE]);
            $contactAccessInfo = isGuest() ? ['allowed' => false] : Yii::$app->user->identity->isAllowedToCreateConversation($profile->user);
        } else {
            $portfolioQuery->andWhere(['not', ['user_portfolio.status' => UserPortfolio::STATUS_DELETED]]);
        }

        $newPortfolioQuery = UserPortfolio::find()->joinWith(['profile'])->where(['user_portfolio.user_id' => $profile->user_id, 'version' => UserPortfolio::VERSION_NEW]);
        $newPortfolioDataProvider = new ActiveDataProvider(['query' => $newPortfolioQuery, 'pagination' => false]);

        if ($share === false) {
            $albums = implode(', ', array_map(function ($value) {
                return $value->title;
            }, $newPortfolioDataProvider->getModels()));
            Yii::$app->opengraph->description = Yii::t('account', 'View photos in albums {albums}', ['albums' => $albums]);
            $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Portfolio page of user {user} on site uJobs.me", ['user' => $profile->getSellerName()]);
            $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('account', 'View photos in albums {albums}', ['albums' => $albums])]);
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('account', 'Portfolio of user {user}', ['user' => $profile->getSellerName()])]);
            if (!strstr($profile->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png')) {
                Yii::$app->opengraph->image = Url::to($profile->getThumb(), true);
            }
        } else {
            $portfolio = UserPortfolio::findOne($share);
            if ($portfolio !== null) {
                Yii::$app->opengraph->description = Html::encode(strip_tags($portfolio->description));
                $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Portfolio album {album}", ['album' => Html::encode($portfolio->title)]);
                $this->view->registerMetaTag(['name' => 'description', 'content' => Html::encode(strip_tags($portfolio->description))]);
                if (!strstr($profile->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png')) {
                    Yii::$app->opengraph->image = Url::to($portfolio->getThumb(), true);
                }
            }
        }

        $reviewCondition = ['created_for' => $profile->user_id, 'type' => Review::TYPE_SELLER_REVIEW];
        if (hasAccess($profile->user_id)) {
            $reviewCondition['is_fake'] = false;
        }

        $reviewQuery = Review::find()->joinWith(['createdBy'])->where($reviewCondition);
        $reviewDataProvider = new ActiveDataProvider(['query' => $reviewQuery, 'pagination' => false]);

        return $this->render($template, [
            'profile' => $profile,
            'editable' => $editable,
            'newPortfolioDataProvider' => $newPortfolioDataProvider,
            'reviewDataProvider' => $reviewDataProvider,
            'message' => new Message(),
            'offer' => new Offer(),
            'contactAccessInfo' => $contactAccessInfo
        ]);
    }

    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShowAsCustomer($id, $share = false)
    {
        return $this->prepareShowPage($id, true, $share);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /* @var UserPortfolio $model */
        $model = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username, user.is_company');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers'])
            ->where(['user_portfolio.id' => $id])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $otherPortfoliosQuery = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username, user.is_company');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers'])
            ->where(['not', ['user_portfolio.id' => $model->id]])
            ->andWhere(['user_portfolio.user_id' => $model->user_id, 'version' => UserPortfolio::VERSION_NEW])
            ->groupBy('user_portfolio.id')
            ->limit(4);
        $commentsQuery = Comment::find()->where(['entity' => 'portfolio', 'entity_id' => $model->id]);

        $otherPortfoliosDataProvider = new ActiveDataProvider(['query' => $otherPortfoliosQuery, 'pagination' => false]);
        $commentsDataProvider = new ActiveDataProvider(['query' => $commentsQuery, 'pagination' => false]);

        $postCommentForm = new PostCommentForm();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'portfolio_views', Yii::$app->session);
        $counterServiceView->process();

        $left = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['<', 'id', $model->id])->orderBy('id desc')->one();
        $right = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['>', 'id', $model->id])->orderBy('id asc')->one();
        $leftLink = $left !== null ? Url::to(['/account/portfolio/view', 'id' => $left->id]) : null;
        $rightLink = $right !== null ? Url::to(['/account/portfolio/view', 'id' => $right->id]) : null;
        $returnUrl = Url::previous('portfolioReturnUrl') ?? Url::to(['/account/portfolio/show', 'id' => $model->user_id]);

        Yii::$app->opengraph->description = Html::encode(strip_tags($model->description));
        $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Portfolio album {album}", ['album' => Html::encode($model->title)]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => Html::encode(strip_tags($model->description))]);
        if (!strstr($model->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png')) {
            Yii::$app->opengraph->image = Url::to($model->getThumb(), true);
        }

        return $this->render('view', [
            'model' => $model,
            'otherPortfoliosDataProvider' => $otherPortfoliosDataProvider,
            'postCommentForm' => $postCommentForm,
            'commentsDataProvider' => $commentsDataProvider,
            'leftLink' => $leftLink,
            'rightLink' => $rightLink,
            'returnUrl' => $returnUrl
        ]);
    }

    /**
     * @return array
     */
    public function actionViewAjax()
    {
        $id = Yii::$app->request->post('id', null);
        $catalog = (int)Yii::$app->request->post('catalog', 0);

        $left = $leftLink = $right = $rightLink = null;

        $this->layout = null;
        /* @var UserPortfolio $model */
        $model = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers', 'category', 'category.translation'])
            ->where(['user_portfolio.id' => $id])
            ->one();

        if ($model === null) {
            return ['success' => false];
        }

        $otherPortfoliosQuery = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers', 'category', 'category.translation'])
            ->where(['not', ['user_portfolio.id' => $model->id]])
            ->andWhere(['user_portfolio.user_id' => $model->user_id, 'version' => UserPortfolio::VERSION_NEW])
            ->groupBy('user_portfolio.id')
            ->limit(4);
        $commentsQuery = Comment::find()
            ->where(['entity' => 'portfolio', 'entity_id' => $model->id]);

        $otherPortfoliosDataProvider = new ActiveDataProvider(['query' => $otherPortfoliosQuery, 'pagination' => false]);
        $commentsDataProvider = new ActiveDataProvider(['query' => $commentsQuery, 'pagination' => false]);

        $postCommentForm = new PostCommentForm();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'portfolio_views', Yii::$app->session);
        $counterServiceView->process();

        if ($catalog === 0) {
            $left = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['<', 'id', $model->id])->orderBy('id desc')->one();
            $right = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['>', 'id', $model->id])->orderBy('id asc')->one();
            $leftLink = $left !== null ? Url::to(['/account/portfolio/view', 'id' => $left->id]) : null;
            $rightLink = $right !== null ? Url::to(['/account/portfolio/view', 'id' => $right->id]) : null;
        }
        $returnUrl = Url::previous('portfolioReturnUrl') ?? Url::to(['/account/portfolio/show', 'id' => $model->user_id]);

        return [
            'success' => true,
            'html' => $this->renderAjax('partial/modal', [
                'model' => $model,
                'profile' => $model->profile,
                'otherPortfoliosDataProvider' => $otherPortfoliosDataProvider,
                'postCommentForm' => $postCommentForm,
                'commentsDataProvider' => $commentsDataProvider,
                'leftLink' => $leftLink,
                'rightLink' => $rightLink,
                'catalog' => $catalog,
                'returnUrl' => $returnUrl
            ])
        ];
    }

    public function actionCreate()
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $input = Yii::$app->request->post();
        $availableLocales = Yii::$app->params['languages'];
        if (!$input) { // Нулевой шаг - выбор локалей
            return $this->render('locale-step', ['locales' => $availableLocales]);
        } else {
            $model = new PostPortfolioForm();
            $model->portfolio = new UserPortfolio();

            $locales = !empty($input['locales']) ? $input['locales'] : [];
            $locales = array_filter($locales, function ($locale) use ($availableLocales) {
                return isset($availableLocales[$locale]);
            });
            if ($locales) {
                $model->locale = array_shift($locales);

                $rootCategoriesList = ArrayHelper::map(
                    Category::find()
                        ->andWhere(['lvl' => 1])
                        ->joinWith(['translation'])
                        ->andWhere(['type' => ['job_category', 'product_category']])
                        ->all(),
                    'id',
                    'translation.title',
                    function ($value) {
                        return $value->type === 'product_category' ? Yii::t('account', 'Product categories') : Yii::t('account', 'Job categories');
                    }
                );

                return $this->render('_form', [
                    'model' => $model,
                    'rootCategoriesList' => $rootCategoriesList,
                    'action' => 'create',
                    'locales' => $locales
                ]);
            } else { // Иначе попадаем на валиацию\сабмит
                if (Yii::$app->request->isAjax) {
                    $model->myLoad($input);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $model->validateAll();
                } else {
                    if (is_numeric($input['id'])) {
                        $model->loadPortfolio($input['id']);
                    }
                    $model->myLoad($input);

                    if ($model->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('account', 'Portfolio has been created. Thank you for posting it!'));
                        return $this->redirect(['/account/portfolio/view', 'id' => $model->portfolio->id]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred during portfolio creation.'));
                        return null;
                    }
                }
            }
        }
    }

    /**
     * Creates a new Portfolio model on ajax request.
     * @return mixed
     */
    public function actionAjaxCreate()
    {
        $input = Yii::$app->request->post();
        $response = ['success' => false];
        if ($input) {
            $model = new PostPortfolioForm();
            $model->portfolio = new UserPortfolio();
            $model->myLoad($input);

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('account', 'Portfolio has been created. Thank you for posting it!'));
                $response['success'] = true;
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred during portfolio creation.'));
            }
        }

        return $response;
    }

    /**
     * @param $id
     * @return array|string|Response
     */
    public function actionUpdate($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $model = new PostPortfolioForm();
        $model->loadPortfolio($id);
        $input = Yii::$app->request->post();
        if ($input) {
            if (Yii::$app->request->isAjax) {
                $model->myLoad($input);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model->validateAll();
            } else {
                $model->myLoad($input);

                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('account', 'Portfolio has been updated.'));
                    return $this->redirect(['/account/portfolio/view', 'id' => $model->portfolio->id]);
                }
            }
        }

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => ['job_category', 'product_category']])
                ->all(),
            'id',
            'translation.title',
            function ($value) {
                return $value->type === 'product_category' ? Yii::t('account', 'Product categories') : Yii::t('account', 'Job categories');
            }
        );

        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'action' => 'update',
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $portfolio = UserPortfolio::findOne($id);
        $portfolio->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return array
     */
    public function actionRenderExtra()
    {
        return [
            'html' => $this->renderAjax('partial/portfolio-attachment-partial', [
                'model' => new Attachment(),
                'iterator' => Yii::$app->request->post('iterator', 0)
            ]),
            'success' => true
        ];
    }
}