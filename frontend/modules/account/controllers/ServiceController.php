<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.11.2016
 * Time: 16:10
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\CompanyMember;
use common\models\forms\UserLanguageForm;
use common\models\forms\UserSkillForm;
use common\models\forms\UserSpecialtyForm;
use common\models\Notification;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserAttribute;
use common\models\WithdrawalWallet;
use common\services\AttributeToFormService;
use frontend\modules\account\models\DeactivateForm;
use frontend\modules\account\models\PasswordChangeForm;
use frontend\modules\account\models\PauseForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ServiceController
 * @package frontend\modules\account\controllers
 */
class ServiceController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'dashboard',
                        'account-settings',
                        'account-actions',
                        'security-settings',
                        'public-profile-settings',
                        'payment-settings',
                        'social-network-settings',
                        'notifications',
                        'companies',
                        'company',
                        'company-leave',
                        'company-member-dismiss',
                        'company-member-edit',
                        'company-member-add',
                    ], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['company-member-edit'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionDashboard()
    {
        $userId = Yii::$app->user->identity->getCurrentId();
        $this->view->title = Yii::t('account', 'Dashboard');
        $unreadNotifications = Notification::find()->joinWith(['from.profile'])->where(['to_id' => $userId, 'is_read' => false])->select('id')->count();

        return $this->render('dashboard', [
            'unreadNotifications' => $unreadNotifications
        ]);
    }

    /**
     * @return mixed
     */
    public function actionNotifications()
    {
        $userId = Yii::$app->user->identity->getCurrentId();
        $this->view->title = Yii::t('account', 'Notifications');

        $unreadNotifications = Notification::find()->joinWith(['from.profile'])->where(['to_id' => $userId, 'is_read' => false])->select('id')->count();

        Notification::updateAll(['is_read' => true], ['to_id' => $userId]);

        $notifications = Notification::find()->joinWith(['from.profile'])->where(['to_id' => $userId])->orderBy('created_at desc');
        $pages = new Pagination(['totalCount' => $notifications->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;

        $notifications = $notifications
            ->limit($pages->limit)
            ->offset($pages->offset)
            ->all();

        return $this->render('notifications', [
            'notifications' => $notifications,
            'unreadNotifications' => $unreadNotifications,
            'pages' => $pages
        ]);
    }

    /**
     * @return mixed
     */
    public function actionPublicProfileSettings()
    {
        $this->view->title = Yii::t('account', 'Public Profile Settings');

        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id !== null && $user->companyUser->company !== null) {
            if ($user->companyUser->company->companyMembers[userId()]->isAllowedToManage()) {
                return $this->redirect(['/company/service/public-profile-settings']);
            }
            else {
                return $this->redirect(['/account/service/companies']);
            }
        }
        $profile = $user->profile;

        $languageQuery = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescriptions'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'language'])->all();
        $languages = AttributeToFormService::process($languageQuery, UserLanguageForm::class);
        $languageDataProvider = new ArrayDataProvider(['allModels' => $languages, 'pagination' => false]);

        $skillQuery = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescriptions'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'skill'])->all();
        $skills = AttributeToFormService::process($skillQuery, UserSkillForm::class);
        $skillDataProvider = new ArrayDataProvider(['allModels' => $skills, 'pagination' => false]);

        $specialtyQuery = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescriptions'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'specialty'])->all();
        $specialties = AttributeToFormService::process($specialtyQuery, UserSpecialtyForm::class);
        $specialtyDataProvider = new ArrayDataProvider(['allModels' => $specialties, 'pagination' => false]);

        $userLanguageLocales = array_diff_key(Yii::$app->params['languages'], [Yii::$app->language => 0]);
        $cityAttribute = UserAttribute::find()->joinWith(['attrValueDescription'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'city'])->one();

        return $this->render('settings/public-profile-settings-new', [
            'model' => $profile,
            'languageDataProvider' => $languageDataProvider,
            'skillDataProvider' => $skillDataProvider,
            'specialtyDataProvider' => $specialtyDataProvider,
            'userLanguageLocales' => $userLanguageLocales,
            'cityAttribute' => $cityAttribute
        ]);
    }

    /**
     * @return mixed
     */
    public function actionAccountSettings()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id !== null && $user->companyUser->company !== null && !$user->companyUser->company->companyMembers[userId()]->isAllowedToManage()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to edit company profile'));
            return $this->redirect(['/account/service/companies']);
        }
        $this->view->title = Yii::t('account', 'Account Settings');

        $profile = Profile::findOne(Yii::$app->user->identity->getCurrentId());
        $user = $profile->user;

        $botName = Yii::$app->params['telegramBotName'];
        if (!$telegramUid = $user->telegram_uid) {
            $telegramLink = Html::a(Yii::t('account', 'Subscribe'), "https://telegram.me/{$botName}?start={$user->id}");
        } else {
            $telegramLink = Html::a(Yii::t('account', 'Unsubscribe'), "https://telegram.me/{$botName}?stop={$user->id}");
        }

        if (!$skypeUid = $user->skype_uid) {
            $skypeHash = "gzygom_" . Yii::$app->cacheLayer->getUserRandomString();
            $user->skype_hash = $skypeHash;
            $user->save();
        } else {
            $skypeHash = null;
        }

        $this->view->registerLinkTag(['rel' => 'manifest', 'href' => '/manifest.json']);

        return $this->render('settings/account-settings', [
            'profile' => $profile,
            'user' => $user,

            'telegramLink' => $telegramLink,
            'skypeHash' => $skypeHash
        ]);
    }

    /**
     * @return mixed
     */
    public function actionPaymentSettings()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id !== null && $user->companyUser->company !== null && $user->companyUser->company->companyMembers[userId()]->role !== CompanyMember::ROLE_OWNER) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to edit company payment settings'));
            return $this->redirect(['/account/service/companies']);
        }
        $this->view->title = Yii::t('account', 'Payment Settings');

        $profile = Profile::findOne(Yii::$app->user->identity->getCurrentId());
        $user = $profile->user;

        return $this->render('settings/payment-settings', [
            'profile' => $profile,
            'user' => $user,
            'withdrawalTypesData' => WithdrawalWallet::getTypesByCountry($profile->user_id),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSocialNetworkSettings()
    {
        $this->view->title = Yii::t('account', 'Social Network Settings');

        $profile = Profile::findOne(Yii::$app->user->identity->getId());
        $user = $profile->user;

        return $this->render('settings/social-network-settings', [
            'profile' => $profile,
            'user' => $user
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSecuritySettings()
    {
        $this->view->title = Yii::t('account', 'Security Settings');

        return $this->render('settings/security-settings', []);
    }

    /**
     * @return mixed
     */
    public function actionAccountActions()
    {
        $this->view->title = Yii::t('account', 'Account Actions');

        $passwordChangeModel = new PasswordChangeForm();
        $deactivateModel = new DeactivateForm();
        $pauseModel = new PauseForm();

        return $this->render('settings/account-actions', [
            'passwordChangeModel' => $passwordChangeModel,
            'deactivateModel' => $deactivateModel,
            'pauseModel' => $pauseModel
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCompanies()
    {
        $members = CompanyMember::find()->joinWith(['company'])->where(['company_member.user_id' => userId(), 'company_member.status' => CompanyMember::STATUS_ACTIVE])->all();
        return $this->render('settings/companies', [
            'members' => $members
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCompany($id)
    {
        /* @var $company Company */
        $company = Company::find()->joinWith(['companyMembers'])->where(['company.id' => $id])->one();
        if ($company !== null) {
            if (isset($company->companyMembers[userId()]) && $company->companyMembers[userId()]->isAllowedToManage()) {
                return $this->render('settings/company', [
                    'company' => $company,
                    'newMember' => new CompanyMember()
                ]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to manage company members'));
                return $this->redirect(['/account/service/companies']);
            }
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Requested page is not found'));
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionCompanyLeave($id)
    {
        /* @var $company Company */
        $company = Company::find()->joinWith(['companyMembers'])->where(['company.id' => $id])->one();
        if ($company !== null && isset($company->companyMembers[userId()]) && $company->companyMembers[userId()]->role !== CompanyMember::ROLE_OWNER) {
            if ($company->companyMembers[userId()]->user->company_user_id === $company->user_id) {
                $company->companyMembers[userId()]->user->updateAttributes(['company_user_id' => null]);
            }
            $company->companyMembers[userId()]->delete();
            Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully left the company'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Only company members can leave the company'));
        }
        return $this->redirect(['/account/service/companies']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionCompanyMemberDismiss($id)
    {
        /* @var $member CompanyMember */
        $member = CompanyMember::find()->joinWith(['company'])->where(['company_member.id' => $id])->one();
        if ($member !== null) {
            if ($member->user_id !== userId() && $member->role !== CompanyMember::ROLE_OWNER && isset($member->company->companyMembers[userId()]) && $member->company->companyMembers[userId()]->isAllowedToManage()) {
                if ($member->user->company_user_id === $member->company->user_id) {
                    $member->user->updateAttributes(['company_user_id' => null]);
                }
                $member->delete();
                return $this->redirect(['/account/service/company', 'id' => $member->company_id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to manage this member'));
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Company member not found'));
        }
        return $this->redirect(['/account/service/companies']);
    }

    /**
     * @return array
     */
    public function actionCompanyMemberEdit()
    {
        $id = Yii::$app->request->post('id');
        $response = [
            'success' => false,
            'message' => Yii::t('app', 'Company member not found')
        ];
        if ($id) {
            /* @var $member CompanyMember */
            $member = CompanyMember::find()->joinWith(['company'])->where(['company_member.id' => $id])->one();
            if ($member !== null) {
                if ($member->user_id !== userId() && $member->role !== CompanyMember::ROLE_OWNER && isset($member->company->companyMembers[userId()]) && $member->company->companyMembers[userId()]->isAllowedToManage()) {
                    $roles = array_diff_key(CompanyMember::getRoleLabels(), [CompanyMember::ROLE_OWNER => 0]);
                    $role = Yii::$app->request->post('role');
                    if (array_key_exists($role, $roles)) {
                        $member->updateAttributes(['role' => $role]);
                        $response = [
                            'success' => true,
                            'message' => Yii::t('app', 'Member role successfully changed'),
                            'role' => $member->getRoleLabel()
                        ];
                    } else {
                        $response['message'] = Yii::t('app', 'This role cannot be set');
                    }
                } else {
                    $response['message'] = Yii::t('app', 'You are not allowed to manage this member');
                }
            }
        }

        return $response;
    }

    /**
     * @return Response
     */
    public function actionCompanyMemberAdd()
    {
        /* @var $member CompanyMember */
        $member = new CompanyMember();
        $error = Yii::t('app', 'An error occurred while adding a company member');
        if ($member->load(Yii::$app->request->post())) {
            if (isset($member->company->companyMembers[userId()]) && $member->company->companyMembers[userId()]->isAllowedToManage()) {
                if ($member->role !== CompanyMember::ROLE_OWNER) {
                    if ($member->save()) {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'New company member successfully added'));
                        return $this->redirect(['/account/service/company', 'id' => $member->company_id]);
                    }
                }
            } else {
                $error = Yii::t('app', 'You are not allowed to add members to the company');
            }
        }

        Yii::$app->session->setFlash('error', $error);
        return $this->redirect(['/account/service/companies']);
    }
}