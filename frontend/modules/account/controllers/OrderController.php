<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.12.2016
 * Time: 16:30
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Job;
use common\models\JobPackage;
use common\models\Payment;
use common\models\PaymentHistory;
use common\models\Review;
use common\modules\board\models\history\ShippingDecisionForm;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\helpers\DivorceFormHelper;
use frontend\modules\account\helpers\DynamicFormHelper;
use frontend\modules\account\models\history\AdvancedDecisionForm;
use frontend\modules\account\models\history\AttachmentDecisionForm;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\DocumentDecisionForm;
use frontend\modules\account\models\history\DynamicDecisionForm;
use frontend\modules\account\models\history\PayForm;
use frontend\modules\account\models\history\PriceDecisionForm;
use frontend\modules\account\models\history\RatingForm;
use frontend\modules\account\models\OrderSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class OrderController
 * @package frontend\modules\account\controllers
 */
class OrderController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['decision', 'pay'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'list',
                        'history',
                        'tender-history',
                        'product-history',
                        'product-tender-history',
                        'pay',
                        'decision',
                    ], 'roles' => ['@']],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function beforeAction($action)
    {
        if(!Yii::$app->request->isAjax) {
            $this->layout = '@frontend/views/layouts/account_layout';
        }

        $formTokenName = 'zxToken';

        if ($formTokenValue = Yii::$app->request->post($formTokenName)) {
            $sessionTokenValue = Yii::$app->session->get($formTokenName);
            if ($formTokenValue !== $sessionTokenValue) {
                throw new BadRequestHttpException('The form token could not be verified.');
            }

            if (!Yii::$app->request->isAjax) {
                Yii::$app->session->remove($formTokenName);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $searchModel = new OrderSearch(['customer_id' => Yii::$app->user->identity->getCurrentId(), 'type' => OrderSearch::TYPE_JOB]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('orders', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param $searchModel OrderSearch
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = OrderSearch::getJobStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a("{$label}&nbsp;({$searchModel->getStatusCount($status)})", ['/account/order/list', 'status' => $status]),
                ['class' => $status === $searchModel->status ? 'active' : '']
            );
        }

        return $html;
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ForbiddenHttpException
     */
    public function actionHistory($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';
        /* @var Order $order */
        $order = Order::find()->joinWith(['history'])->where(['order.id' => $id])->one();
        if (!in_array($order->product_type, [Order::TYPE_JOB_PACKAGE, Order::TYPE_JOB, Order::TYPE_OFFER])) {
            throw new ForbiddenHttpException();
        }

        if (hasAccess($order->customer_id) || hasAccess($order->seller_id)) {
            $role = hasAccess($order->customer_id) ? Order::ROLE_CUSTOMER : Order::ROLE_SELLER;
            $historyForm = $this->renderHistoryForm($order);
            $historyTemplate = 'history';
            return $this->render($historyTemplate, [
                'order' => $order,
                'role' => $role,
                'historyForm' => $historyForm
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested order is not found.'));

        return $this->redirect(['/site/index']);
    }

    /**
     * @param Order $order
     * @return null|string
     */
    protected function renderHistoryForm(Order $order)
    {
        $formToken = FormHelper::generateTokenInput();
        $output = null;

        if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
            switch ($order->status) {
                case Order::STATUS_MISSING_DETAILS:
                case Order::STATUS_ACTIVE:
                    $output = $this->generateRequirementForm($order, $formToken);

                    break;
                case Order::STATUS_DELIVERED:
                    $data = [
                        'alert' => Yii::t('order', 'Worker marked this order as completed. Your decision?'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_AWAITING_REVIEW),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_JOB_ACCEPT),
                                    'content' => 'ph',
                                    'placeholder' => Yii::t('order', 'You may add a comment'),
                                    'zxToken' => $formToken
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Accept work')
                            ],
                            'deny' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_ACTIVE),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_JOB_DENY),
                                    'placeholder' => Yii::t('order', 'What is wrong with order?'),
                                    'zxToken' => $formToken
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Needs adjustments')
                            ],
                        ]
                    ];
                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;
                case Order::STATUS_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }
                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_SELLER),
                        'scenario' => RatingForm::SCENARIO_SELLER,
                        'zxToken' => $formToken,
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);

                    break;
            }
        } elseif (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id)) {
            switch ($order->status) {
                case Order::STATUS_MISSING_DETAILS:
                    $postRequirementsExist = OrderHistory::find()->where(['type' => OrderHistory::TYPE_POST_REQUIREMENTS, 'order_id' => $order->id])->exists();

                    if ($postRequirementsExist || $order->created_at < (time() - 3600 * 24)) {
                        $data = [
                            'alert' => Yii::t('order', 'It seems customer sent requirements for this order. What is your decision?'),
                            'forms' => [
                                'accept' => [
                                    'form' => new AdvancedDecisionForm([
                                        'h' => SecurityHelper::encrypt(Order::STATUS_ACTIVE),
                                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REQUIREMENTS_ACCEPT),
                                        'placeholder' => Yii::t('order', 'When are you planning to finish this order?'),
                                        'zxToken' => $formToken
                                    ]),
                                    'autoAccept' => false,
                                    'view' => 'decision-advanced-part-form',
                                    'label' => Yii::t('order', 'All details are clarified, I can start')
                                ],
                                'deny' => [
                                    'form' => new DecisionForm([
                                        'h' => SecurityHelper::encrypt(Order::STATUS_MISSING_DETAILS),
                                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REQUIREMENTS_DENY),
                                        'placeholder' => Yii::t('order', 'What is missing in additional details?'),
                                        'zxToken' => $formToken
                                    ]),
                                    'autoAccept' => false,
                                    'view' => 'decision-part-form',
                                    'label' => Yii::t('order', 'Require more info')
                                ],
                                'cancel' => [
                                    'form' => new DecisionForm([
                                        'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_CANCEL),
                                        'placeholder' => Yii::t('order', 'Enter the reason for canceling the order'),
                                        'zxToken' => $formToken
                                    ]),
                                    'autoAccept' => false,
                                    'view' => 'decision-part-form',
                                    'label' => Yii::t('order', 'Refuse order')
                                ]
                            ]
                        ];

                        $output = $this->renderPartial('history-items/form/decision-form', [
                            'data' => $data,
                            'order' => $order,
                        ]);
                    } else {
                        $formModel = new DecisionForm([
                            'h' => SecurityHelper::encrypt(Order::STATUS_MISSING_DETAILS),
                            't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                            'placeholder' => Yii::t('order', 'Type your message here...'),
                            'zxToken' => $formToken
                        ]);

                        $output = $this->renderPartial('history-items/form/decision-part-form', [
                            'formModel' => $formModel,
                            'order' => $order,
                            'autoAccept' => false
                        ]);
                    }

                    break;
                case Order::STATUS_ACTIVE:
                    $data = [
                        'alert' => Yii::t('order', 'You may write a simple message, or inform customer that you have completed order'),
                        'forms' => [
                            'done' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_DELIVERED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_DELIVERY),
                                    'placeholder' => Yii::t('order', 'Send a message when you finish the order. You also may attach photos or other files, especially if it was online job.'),
                                    'zxToken' => $formToken
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Inform about order completion')
                            ],
                            'message' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ],
                        ]
                    ];
                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;
                case Order::STATUS_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }

                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_BUYER),
                        'scenario' => RatingForm::SCENARIO_CUSTOMER,
                        'zxToken' => $formToken
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);

                    break;
            }
        }

        return $output;
    }

    /**
     * @param Order $order
     * @param $zxToken
     * @return string
     */
    private function generateRequirementForm(Order $order, $zxToken)
    {
        switch ($order->product_subtype) {
            case 'lawyer_family':
                $formModel = new DocumentDecisionForm([
                    'h' => SecurityHelper::encrypt(Order::STATUS_ACTIVE),
                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_POST_REQUIREMENTS),
                    'zxToken' => $zxToken
                ], DivorceFormHelper::getConfig($order->id));
                $output = $this->renderPartial('documents/form/decision-divorce-part-form', [
                    'formModel' => $formModel,
                    'order' => $order,
                    'd' => SecurityHelper::encrypt('DivorceFormHelper'),
                    'autoAccept' => false
                ]);

                break;

            default:
                $product = $order->product;
                $job = null;
                $requirementsType = Job::REQUIREMENTS_TYPE_DEFAULT;
                if ($product instanceof JobPackage) {
                    $job = $product->job;
                    $requirementsType = $job->requirements_type;
                }
                switch ($requirementsType) {
                    case Job::REQUIREMENTS_TYPE_SIMPLE:
                        $postRequirementsExist = OrderHistory::find()->where(['type' => OrderHistory::TYPE_POST_REQUIREMENTS, 'order_id' => $order->id])->exists();
                        if ($postRequirementsExist) {
                            $formModel = new AttachmentDecisionForm([
                                'h' => SecurityHelper::encrypt($order->status),
                                't' => SecurityHelper::encrypt(OrderHistory::TYPE_POST_REQUIREMENTS),
                                'placeholder' => Yii::t('order', 'Type your message here...'),
                                'zxToken' => $zxToken
                            ]);
                            $output = $this->renderPartial('history-items/form/decision-attachment-part-form', [
                                'formModel' => $formModel,
                                'order' => $order,
                                'autoAccept' => false
                            ]);
                        } else {
                            $formModel = new DynamicDecisionForm([
                                'h' => SecurityHelper::encrypt($order->status),
                                't' => SecurityHelper::encrypt(OrderHistory::TYPE_POST_REQUIREMENTS),
                                'zxToken' => $zxToken
                            ], DynamicFormHelper::getConfig($order->id));

                            $output = $this->renderPartial('history-items/form/decision-dynamic-part-form', [
                                'formModel' => $formModel,
                                'order' => $order,
                                'd' => SecurityHelper::encrypt('DynamicFormHelper'),
                                'autoAccept' => false
                            ]);
                        }

                        break;
                    case Job::REQUIREMENTS_TYPE_ADVANCED:
                        $postRequirementsExist = OrderHistory::find()->where(['type' => OrderHistory::TYPE_POST_REQUIREMENTS, 'order_id' => $order->id])->exists();
                        if ($postRequirementsExist) {
                            $formModel = new AttachmentDecisionForm([
                                'h' => SecurityHelper::encrypt($order->status),
                                't' => SecurityHelper::encrypt(OrderHistory::TYPE_POST_REQUIREMENTS),
                                'placeholder' => Yii::t('order', 'Type your message here...'),
                                'zxToken' => $zxToken
                            ]);
                            $output = $this->renderPartial('history-items/form/decision-attachment-part-form', [
                                'formModel' => $formModel,
                                'order' => $order,
                                'autoAccept' => false
                            ]);
                        } else {
                            $output = $this->renderPartial('history-items/form/requirements', [
                                'route' => ['/account/requirement/post', 'order_id' => $order->id],
                            ]);
                        }

                        break;
                    default:
                        $formModel = new AttachmentDecisionForm([
                            'h' => SecurityHelper::encrypt($order->status),
                            't' => SecurityHelper::encrypt(OrderHistory::TYPE_POST_REQUIREMENTS),
                            'placeholder' => Yii::t('order', 'Type your message here...'),
                            'zxToken' => $zxToken
                        ]);
                        $output = $this->renderPartial('history-items/form/decision-attachment-part-form', [
                            'formModel' => $formModel,
                            'order' => $order,
                            'autoAccept' => false
                        ]);
                }

                break;
        }

        return $output;
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ForbiddenHttpException
     */
    public function actionTenderHistory($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';
        /* @var Order $order */
        $order = Order::find()->joinWith(['history'])->where(['order.id' => $id])->one();

        if (!in_array($order->product_type, [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER])) {
            throw new ForbiddenHttpException();
        }

        if (hasAccess($order->customer_id) || hasAccess($order->seller_id)) {
            $role = hasAccess($order->customer_id) ? Order::ROLE_CUSTOMER : Order::ROLE_SELLER;
            $historyForm = $this->renderTenderHistoryForm($order);
            $historyTemplate = 'tender-history';
            return $this->render($historyTemplate, [
                'order' => $order,
                'role' => $role,
                'historyForm' => $historyForm
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested order is not found.'));

        return $this->redirect(['/site/index']);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ForbiddenHttpException
     */
    public function actionProductTenderHistory($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';
        /* @var Order $order */
        $order = Order::find()->joinWith(['history'])->where(['order.id' => $id])->one();

        if ($order->product_type !== Order::TYPE_PRODUCT_TENDER) {
            throw new ForbiddenHttpException();
        }

        if (hasAccess($order->customer_id) || hasAccess($order->seller_id)) {
            $role = hasAccess($order->customer_id) ? Order::ROLE_CUSTOMER : Order::ROLE_SELLER;
            $historyForm = $this->renderProductTenderHistoryForm($order);
            $historyTemplate = 'product-tender-history';
            return $this->render($historyTemplate, [
                'order' => $order,
                'role' => $role,
                'historyForm' => $historyForm
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested order is not found.'));

        return $this->redirect(['/site/index']);
    }


    /**
     * @param Order $order
     * @return null|string
     */
    protected function renderProductTenderHistoryForm(Order $order)
    {
        $formToken = FormHelper::generateTokenInput();
        $output = null;

        if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
            switch ($order->status) {
                case Order::STATUS_PRODUCT_TENDER_UNAPPROVED:
                    $data = [
                        'alert' => Yii::t('order', 'You can approve or reject the seller. To clarify the necessary information, we recommend using the function of sending a message.'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_APPROVED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_SELLER_APPROVED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Approve seller')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_DENIED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Deny seller')
                            ],
                            'message' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ]
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;
                case Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION:
                    $data = [
                        'alert' => Yii::t('order', 'You can approve or reject the seller. To clarify the necessary information, we recommend using the function of sending a message.'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_APPROVED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_SELLER_APPROVED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Approve seller')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_SELLER_DENIED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Deny seller')
                            ],
                            'message' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ],
                            'price' => [
                                'form' => new PriceDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-price-part-form',
                                'label' => Yii::t('order', 'Change conditions')
                            ]
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_PRODUCT_TENDER_APPROVED:
                    $tenderPayForm = new PayForm();

                    $output = $this->renderPartial('history-items/form/pay-form', [
                        'order' => $order,
                        'formModel' => $tenderPayForm,
                    ]);
                    break;
                case Order::STATUS_PRODUCT_TENDER_PAID:
                    $alert = Yii::t('order', 'Now you can reach an agreement about shipping or details of purchase.');

                    $formModel = new AttachmentDecisionForm([
                        'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_ACTIVE),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_PRODUCT_DETAILS),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                        'alert' => $alert,
                        'autoAccept' => false
                    ]);

                    break;
                case Order::STATUS_PRODUCT_TENDER_ACTIVE:
                    $formModel = new DecisionForm([
                        'h' => SecurityHelper::encrypt($order->status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                        'autoAccept' => false
                    ]);
                    break;
                case Order::STATUS_PRODUCT_TENDER_DELIVERED:
                    $status = Order::STATUS_PRODUCT_TENDER_AWAITING_REVIEW;
                    $type = OrderHistory::TYPE_TENDER_REPORT_ACCEPT_HEADER;

                    $data = [
                        'alert' => Yii::t('order', 'Seller marked this order as completed. Your decision?'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($status),
                                    't' => SecurityHelper::encrypt($type),
                                    'content' => 'ph',
                                    'placeholder' => Yii::t('order', 'You may add a comment'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Accept product')
                            ],
                            'deny' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_ACTIVE),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_PRODUCT_REPORT_DENY_HEADER),
                                    'placeholder' => Yii::t('order', 'What is wrong with order?'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Something wrong')
                            ],
                        ]
                    ];
                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;

                case Order::STATUS_PRODUCT_TENDER_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }

                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_PRODUCT_TENDER_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_SELLER),
                        'scenario' => RatingForm::SCENARIO_SELLER,
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);
                    break;
            }
        } elseif (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id)) {
            switch ($order->status) {
                case Order::STATUS_PRODUCT_TENDER_UNAPPROVED:
                case Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION:
                    $formModel = new DecisionForm([
                        'h' => SecurityHelper::encrypt($order->status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);
                    break;
                case Order::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION:
                    $data = [
                        'alert' => Yii::t('order', 'You may accept current price, or propose your price'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_UNAPPROVED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE_ACCEPT),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Accept price')
                            ],
                            'price' => [
                                'form' => new PriceDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-price-part-form',
                                'label' => Yii::t('order', 'Change conditions')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CANCEL),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Cancel')
                            ],
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_PRODUCT_TENDER_ACTIVE:
                    $data = [
                        'alert' => Yii::t('order', 'Mark this order as completed when product is sent or handed over to a customer.'),
                        'forms' => [
                            'done' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_DELIVERED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_PRODUCT_REPORT),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                                    'placeholder' => Yii::t('order', 'You can add a comment. You also may attach photos or other files.'),
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Complete order')
                            ],
                            'message' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ],
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_PRODUCT_TENDER_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }

                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_PRODUCT_TENDER_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_BUYER),
                        'scenario' => RatingForm::SCENARIO_CUSTOMER,
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);
                    break;
            }
        }

        return $output;
    }

    /**
     * @param Order $order
     * @return null|string
     */
    protected function renderTenderHistoryForm(Order $order)
    {
        $formToken = FormHelper::generateTokenInput();
        $output = null;

        if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
            switch ($order->status) {
                case Order::STATUS_TENDER_UNAPPROVED:
                    $data = [
                        'alert' => Yii::t('order', 'You can approve or reject the worker. To clarify the necessary information, we recommend using the function of sending a message.'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_APPROVED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_APPROVED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Approve worker')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_DENIED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Deny worker')
                            ],
                            'message' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY,
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ]
                        ]
                    ];
                    if ($order->status === Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION) {
                        $data['price'] = [
                            'form' => new PriceDecisionForm([
                                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION),
                                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                                'placeholder' => Yii::t('order', 'Type your message here...'),
                                'zxToken' => $formToken,
                                'type' => DecisionForm::DECISION_TENDER_HISTORY,
                            ]),
                            'autoAccept' => false,
                            'view' => 'decision-price-part-form',
                            'label' => Yii::t('order', 'Send message')
                        ];
                    }

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION:
                    $data = [
                        'alert' => Yii::t('order', 'You can approve or reject the worker. To clarify the necessary information, we recommend using the function of sending a message.'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_APPROVED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_APPROVED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Approve worker')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_DENIED_HEADER),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Deny worker')
                            ],
                            'message' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ],
                            'price' => [
                                'form' => new PriceDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-price-part-form',
                                'label' => Yii::t('order', 'Change conditions')
                            ]
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_TENDER_APPROVED:
                    $tenderPayForm = new PayForm();

                    $output = $this->renderPartial('history-items/form/pay-form', [
                        'order' => $order,
                        'formModel' => $tenderPayForm,
                    ]);

                    break;
                case Order::STATUS_TENDER_PAID:
                    $alert = Yii::t('order', 'Describe task in details');

                    if ($order->product_type !== Order::TYPE_SIMPLE_TENDER) {
                        $deposit = (int)((Payment::find()->where(['old_order_id' => $order->id])->sum('total') + $order->payment->total) * 0.8);
                        $diff = $deposit - PaymentHistory::find()->where(['order_id' => $order->id, 'type' => PaymentHistory::TYPE_OPERATION_REVENUE])->sum('amount');
                        $diff -= (int)(($order->total / 30) * 7 * 0.8);
                        if ($diff > 0) {
                            $alert = Yii::t('order', 'Describe weekly task in details');
                        } else {
                            $data = [
                                'alert' => Yii::t('order', 'Your deposit is expired. You have to pay new deposit for next month to continue working with this seller.'),
                                'forms' => [
                                    'pay' => [
                                        'form' => new PayForm(),
                                        'view' => 'pay-form',
                                        'label' => Yii::t('order', 'Continue working'),
                                        'autoAccept' => false,
                                    ],
                                    'done' => [
                                        'form' => new DecisionForm([
                                            'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_AWAITING_REVIEW),
                                            't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_REPORT_ACCEPT_HEADER),
                                            'content' => 'ph',
                                            'zxToken' => $formToken,
                                            'type' => DecisionForm::DECISION_TENDER_HISTORY
                                        ]),
                                        'autoAccept' => true,
                                        'view' => 'decision-part-form',
                                        'label' => Yii::t('order', 'Finish working')
                                    ],
                                ]
                            ];
                            $output = $this->renderPartial('history-items/form/decision-form', [
                                'data' => $data,
                                'order' => $order,
                            ]);

                            return $output;
                        }
                    }

                    $formModel = new AttachmentDecisionForm([
                        'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_REQUIRES_TASK_APPROVE),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WEEKLY_TASK),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                        'alert' => $alert,
                        'autoAccept' => false
                    ]);

                    break;
                case Order::STATUS_TENDER_REQUIRES_TASK_APPROVE:
                case Order::STATUS_TENDER_ACTIVE:
                    $formModel = new DecisionForm([
                        'h' => SecurityHelper::encrypt($order->status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                        'autoAccept' => false
                    ]);
                    break;
                case Order::STATUS_TENDER_DELIVERED:
                    $status = $order->product_type === Order::TYPE_TENDER ? Order::STATUS_TENDER_PAID : Order::STATUS_TENDER_AWAITING_REVIEW;
                    $type = $order->product_type === Order::TYPE_TENDER ? OrderHistory::TYPE_TENDER_WEEKLY_REPORT_ACCEPT_HEADER : OrderHistory::TYPE_TENDER_REPORT_ACCEPT_HEADER;

                    $data = [
                        'alert' => Yii::t('order', 'Worker marked this order as completed. Your decision?'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($status),
                                    't' => SecurityHelper::encrypt($type),
                                    'content' => 'ph',
                                    'placeholder' => Yii::t('order', 'You may add a comment'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY,
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Accept work')
                            ],
                            'deny' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_ACTIVE),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WEEKLY_REPORT_DENY_HEADER),
                                    'placeholder' => Yii::t('order', 'What is wrong with order?'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY,
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Needs adjustments')
                            ],
                        ]
                    ];
                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;

                case Order::STATUS_TENDER_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }

                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_TENDER_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_SELLER),
                        'scenario' => RatingForm::SCENARIO_SELLER,
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_TENDER_HISTORY
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);
                    break;
            }
        } elseif (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id)) {
            switch ($order->status) {
                case Order::STATUS_TENDER_UNAPPROVED:
                case Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION:
                    $formModel = new DecisionForm([
                        'h' => SecurityHelper::encrypt($order->status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                        'autoAccept' => false
                    ]);

                    break;
                case Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION:
                    $data = [
                        'alert' => Yii::t('order', 'You may accept current price, or propose your price'),
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_UNAPPROVED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE_ACCEPT),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY,
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Accept price')
                            ],
                            'price' => [
                                'form' => new PriceDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-price-part-form',
                                'label' => Yii::t('order', 'Change conditions')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CANCEL),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Cancel')
                            ],
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_TENDER_REQUIRES_TASK_APPROVE:
                    $alert = Yii::t('order', 'It seems customer set weekly task. If you do not have additional questions regards task, you can start job. You can also ask customer for additional information, or cancel this order.');
                    if ($order->product_type === Order::TYPE_SIMPLE_TENDER) {
                        $alert = Yii::t('order', 'It seems customer sent details of this request.<br> If you do not have additional questions regards task, you can start job. <br> You can also ask customer for additional information, or cancel this order.');
                    }
                    $data = [
                        'alert' => $alert,
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_ACTIVE),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WEEKLY_TASK_ACCEPT),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Start job')
                            ],
                            'message' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ],
                            'deny' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CANCEL),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY,
                                    'placeholder' => Yii::t('order', 'What is the reason of your refusal?')
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Refuse')
                            ],
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_TENDER_ACTIVE:
                    $data = [
                        'alert' => Yii::t('order', 'You may write a simple message, or inform customer that you have completed order'),
                        'forms' => [
                            'done' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_DELIVERED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WEEKLY_REPORT),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY,
                                    'placeholder' => Yii::t('order', 'Send a message when you finish the order. You also may attach photos or other files, especially if it was online job.'),
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Complete task')
                            ],
                            'message' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_TENDER_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ],
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order
                    ]);

                    break;
                case Order::STATUS_TENDER_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }

                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_TENDER_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_BUYER),
                        'scenario' => RatingForm::SCENARIO_CUSTOMER,
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_TENDER_HISTORY,
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);

                    break;
            }
        }

        return $output;
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionProductHistory($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';
        /** @var Order $order */
        $order = Order::find()->joinWith(['history'])->where(['order.id' => $id])->one();

        if ($order->product_type !== Order::TYPE_PRODUCT) {
            throw new ForbiddenHttpException();
        }

        if (hasAccess($order->customer_id) || hasAccess($order->seller_id)) {
            $role = hasAccess($order->customer_id) ? Order::ROLE_CUSTOMER : Order::ROLE_SELLER;
            $historyForm = $this->renderProductHistoryForm($order);
            $historyTemplate = 'product-history';
            return $this->render($historyTemplate, [
                'order' => $order,
                'role' => $role,
                'historyForm' => $historyForm
            ]);
        }

        throw new NotFoundHttpException();
    }

    /**
     * @param Order $order
     * @return null|string
     */
    protected function renderProductHistoryForm(Order $order)
    {
        $formToken = FormHelper::generateTokenInput();
        $output = null;

        if (Yii::$app->user->identity->isAllowedToUpdateOrder($order->customer_id)) {
            switch ($order->status) {
                case Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION:
                    $data = [
                        'alert' => Yii::t('order', 'What shipping method do you prefer?'),
                        'forms' => [
                            'shipping' => [
                                'form' => new ShippingDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_WAITING_FOR_SEND),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_PRODUCT_SET_SHIPPING),
                                    'placeholder' => Yii::t('order', 'You may post some additional information. For example: "I`am not at home on saturdays and sundays"'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-address-form',
                                'label' => Yii::t('order', 'Shipping'),
                            ],
                            'pickup' => [
                                'form' => new ShippingDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_WAITING_FOR_PICKUP),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_PRODUCT_SET_SHIPPING),
                                    'shipping_type' => 'pickup',
                                    'placeholder' => Yii::t('order', 'You can post, when do you plan to pickup product from seller (day and time)'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-address-form',
                                'label' => Yii::t('order', 'Pickup')
                            ],
                        ]
                    ];
                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;
                case Order::STATUS_PRODUCT_SENT:
                case Order::STATUS_PRODUCT_WAITING_FOR_PICKUP:
                    $data = [
                        'forms' => [
                            'accept' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_AWAITING_REVIEW),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_ACCEPT_PRODUCT),
                                    'content' => 'ph',
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => true,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Accept product')
                            ],
                            'deny' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_DENIED),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_DENY_PRODUCT),
                                    'placeholder' => Yii::t('order', 'What is wrong with product?'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'Deny product')
                            ],
                            'message' => [
                                'form' => new DecisionForm([
                                    'h' => SecurityHelper::encrypt($order->status),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                                    'placeholder' => Yii::t('order', 'Type your message here...'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'Send message')
                            ]
                        ]
                    ];
                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;
                case Order::STATUS_PRODUCT_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }
                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_PRODUCT_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_SELLER),
                        'scenario' => RatingForm::SCENARIO_SELLER,
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);

                    break;
            }
        } elseif (Yii::$app->user->identity->isAllowedToUpdateOrder($order->seller_id)) {
            switch ($order->status) {
                case Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION:
                    $formModel = new AttachmentDecisionForm([
                        'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                        'placeholder' => Yii::t('order', 'Type your message here...'),
                        'zxToken' => $formToken
                    ]);

                    $output = $this->renderPartial('history-items/form/decision-attachment-part-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                        'autoAccept' => false
                    ]);

                    break;
                case Order::STATUS_PRODUCT_WAITING_FOR_SEND:
                    $data = [
                        'alert' => Yii::t('order', 'Customer chose shipping method. Is everything ok?'),
                        'forms' => [
                            'ship' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_SENT),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_PRODUCT_SEND_PRODUCT),
                                    'placeholder' => Yii::t('order', 'Send product and then send message via this form. You can also attach documents about shipping.'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-attachment-part-form',
                                'label' => Yii::t('order', 'I send product')
                            ],
                            'deny' => [
                                'form' => new AttachmentDecisionForm([
                                    'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION),
                                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_PRODUCT_SET_SHIPPING_DENY),
                                    'placeholder' => Yii::t('order', 'Type here, what is wrong with customer`s information.'),
                                    'zxToken' => $formToken,
                                    'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                                ]),
                                'autoAccept' => false,
                                'view' => 'decision-part-form',
                                'label' => Yii::t('order', 'I don`t have all information')
                            ],
                        ]
                    ];

                    $output = $this->renderPartial('history-items/form/decision-form', [
                        'data' => $data,
                        'order' => $order,
                    ]);

                    break;
                case Order::STATUS_PRODUCT_AWAITING_REVIEW:
                    $existingReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
                    if ($existingReview !== null) {
                        break;
                    }

                    $otherReview = Review::findOne(['order_id' => $order->id, 'type' => Review::TYPE_SELLER_REVIEW]);
                    $status = $otherReview === null ? $order->status : Order::STATUS_PRODUCT_COMPLETED;
                    $formModel = new RatingForm([
                        'h' => SecurityHelper::encrypt($status),
                        't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_BUYER),
                        'scenario' => RatingForm::SCENARIO_CUSTOMER,
                        'zxToken' => $formToken,
                        'type' => DecisionForm::DECISION_PRODUCT_HISTORY
                    ]);

                    $output = $this->renderPartial('history-items/form/rating-form', [
                        'formModel' => $formModel,
                        'order' => $order,
                    ]);

                    break;
            }
        }

        return $output;
    }

    /**
     * @return array|Response
     */
    public function actionDecision()
    {
        $namespaces = [
            'frontend\modules\account\models\history\\',
            'common\modules\board\models\history\\'
        ];
        $classPath = null;

        $input = Yii::$app->request->post();
        $className = SecurityHelper::decrypt($input['o']);
        $scenario = array_key_exists('s', $input) ? SecurityHelper::decrypt($input['s']) : 'default';
        $dynamicHelper = array_key_exists('d', $input) ? SecurityHelper::decrypt($input['d']) : null;
        foreach ($namespaces as $namespace) {
            if (class_exists("$namespace{$className}")) {
                $classPath = "$namespace{$className}";
                break;
            }
        }

        if ($classPath !== null) {
            /* @var mixed $model */
            if ($dynamicHelper !== null) {
                $orderId = $input['DynamicDecisionForm']['order_id'];
                /* @var mixed $helper */
                $helper = "frontend\modules\account\helpers\\{$dynamicHelper}";
                $model = new $classPath(['scenario' => $scenario], $helper::getConfig($orderId));
            } else {
                $model = new $classPath(['scenario' => $scenario]);
            }

            if (Yii::$app->request->isAjax) {
                $model->load($input);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            $model->load($input);
            $model->save();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return array
     */
    public function actionPay()
    {
        $response = ['success' => false, 'error' => Yii::t('app', 'An error is encountered')];
        /* @var $order Order */
        $input = Yii::$app->request->post();
        if ($input) {
            $tenderPayForm = new PayForm();
            $tenderPayForm->load($input, '');

            $order = Order::find()->where(['id' => $tenderPayForm->order_id])->one();
            if ($order !== null) {
                $tenderPayForm->seller_id = $order->seller_id;
                $tenderPayForm->customer_id = $order->customer_id;
                $tenderPayForm->order_id = $order->id;

                if ($tenderPayForm->save()) {
                    $order = Order::find()->where(['id' => $tenderPayForm->order_id])->one();
                    $payment = $order->payment;
                    $response = [
                        'success' => true,
                        'form' => $this->renderPartial('@frontend/views/payments_forms/' . $payment->paymentMethod->code, [
                            'payment' => $payment
                        ])
                    ];
                }
            }
        }

        return $response;
    }
}