<?php

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\forms\UserSpecialtyForm;
use common\models\UserSpecialty;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class UserSpecialtyController
 * @package frontend\modules\account\controllers
 */
class UserSpecialtyController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'update',
                            'delete'
                        ],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array|bool
     */
    public function actionCreate()
    {
        $model = new UserSpecialtyForm();

        Yii::$app->response->format = Response::FORMAT_JSON;
        $input = Yii::$app->request->post();

        if (isset($input['ajax'])) {
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        $model->load($input);
        if ($model->save()) {
//                $model->user->profile->updateElastic();
            return ['result' => true];
        }

        return false;
    }

    /**
     * @param null $id
     * @return array|bool
     */
    public function actionUpdate($id)
    {
        /* @var UserSpecialtyForm $model */
        $model = UserSpecialtyForm::findOne(['user_attribute.id' => $id, 'user_id' => Yii::$app->user->identity->getId()]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $input = Yii::$app->request->post();

        if (isset($input['ajax'])) {
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }
        $model->load($input);

        if ($model->save()) {
            return ['result' => true];
        }

        return false;
    }

    /**
     * @return array
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');

        /* @var UserSpecialtyForm $model */
        $model = UserSpecialtyForm::findOne(['user_attribute.id' => (int)$id, 'user_id' => Yii::$app->user->identity->getId()]);
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model) {
            $model->delete();
//            $model->user->profile->updateElastic();
            return ['success' => true];
        }

        return ['success' => false, 'message' => 'PH'];
    }
}