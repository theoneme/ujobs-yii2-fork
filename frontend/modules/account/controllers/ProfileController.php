<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.11.2016
 * Time: 16:08
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Job;
use common\models\Message;
use common\models\Offer;
use common\models\Page;
use common\models\Review;
use common\models\user\Profile;
use common\models\UserAttribute;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use frontend\modules\account\models\ProfileJobSearch;
use frontend\modules\account\models\ProfileProductSearch;
use frontend\modules\account\models\ProfileProductTenderSearch;
use frontend\modules\account\models\ProfileTenderSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class ProfileController
 * @package frontend\modules\account\controllers
 */
class ProfileController extends FrontEndController
{
    /**
     * @param $id
     * @return string
     */
    public function actionShow($id)
    {
        return $this->prepareShowPage($id);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage($id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Profile $profile */
        $profile = Profile::find()->joinWith(['user'])->where(['profile.user_id' => $id])->one();
        if ($profile === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested profile is not found'));
            return $this->redirect(['/category/pro-catalog']);
        }

        if ((boolean)$profile->user->is_company === true) {
            return $this->redirect(['/company/profile/show', 'id' => $profile->user->company->id]);
        }

//        if ((boolean)$profile->user->is_company === true) { // Выглядит как бред
//            return $this->redirect(['/company/portfolio/show', 'id' => $profile->user->company->id]);
//        }

        $this->registerMetaTags($profile);

        if (!$as_customer && hasAccess($profile->user_id)) {
            $template = 'show_admin_new';
            $editable = true;
        }

        $offersQuery = Offer::find()->joinWith(['from', 'from.profile'])->where(['to_id' => $id, 'offer.status' => Offer::STATUS_ACCEPTED]);

        if (!$editable) {
            $contactAccessInfo = isGuest() ? ['allowed' => false] : Yii::$app->user->identity->isAllowedToCreateConversation($profile->user);
        }

        $reviewCondition = ['created_for' => $profile->user_id, 'type' => Review::TYPE_SELLER_REVIEW];
        if (hasAccess($profile->user_id)) {
            $reviewCondition['is_fake'] = false;
        }

        $reviewQuery = Review::find()->joinWith(['createdBy.profile'])->where($reviewCondition);
        $reviewsCount = Review::find()->joinWith(['createdBy'])->where($reviewCondition)->count();
        $reviewsAverage = Review::find()->joinWith(['createdBy'])->where($reviewCondition)->average('rating');

        $jobSearchModel = new ProfileJobSearch(['isOwner' => $editable, 'user_id' => $profile->user_id]);
        $sellingJobsDataProvider = $jobSearchModel->search(['sold' => false]);
        $soldJobsDataProvider = $jobSearchModel->search(['sold' => true]);

        $tenderSearchModel = new ProfileTenderSearch(['isOwner' => $editable, 'user_id' => $profile->user_id]);
        $tendersDataProvider = $tenderSearchModel->search();

        $productSearchModel = new ProfileProductSearch(['isOwner' => $editable, 'user_id' => $profile->user_id]);
        $sellingProductsDataProvider = $productSearchModel->search(['sold' => false]);
        $soldProductsDataProvider = $productSearchModel->search(['sold' => true]);

        $ptenderSearchModel = new ProfileProductTenderSearch(['isOwner' => $editable, 'user_id' => $profile->user_id]);
        $ptendersDataProvider = $ptenderSearchModel->search();

        $reviewDataProvider = new ActiveDataProvider(['query' => $reviewQuery, 'pagination' => false]);
        $offersDataProvider = new ActiveDataProvider(['query' => $offersQuery, 'pagination' => false]);

        $cityAttribute = UserAttribute::find()->joinWith(['attrValueDescription'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'city'])->one();

        $totalSoldJobs = Order::find()
            ->joinWith(['orderProduct'])
            ->where(["order.seller_id" => $profile->user_id])
            ->andWhere(['order.product_type' => Order::TYPE_JOB_PACKAGE, 'order.status' => Order::STATUS_COMPLETED])
            ->count();

        $totalSoldProducts = Order::find()
            ->joinWith(['orderProduct'])
            ->where(["order.seller_id" => $profile->user_id])
            ->andWhere(['order.product_type' => Order::TYPE_PRODUCT, 'order.status' => Order::STATUS_PRODUCT_COMPLETED])
            ->count();

        return $this->render($template, [
            'profile' => $profile,
            'editable' => $editable,
            'sellingJobsDataProvider' => $sellingJobsDataProvider,
            'soldJobsDataProvider' => $soldJobsDataProvider,
            'tendersDataProvider' => $tendersDataProvider,
            'sellingProductsDataProvider' => $sellingProductsDataProvider,
            'soldProductsDataProvider' => $soldProductsDataProvider,
            'ptendersDataProvider' => $ptendersDataProvider,
            'reviewDataProvider' => $reviewDataProvider,
            'offersDataProvider' => $offersDataProvider,
            'totalSoldJobs' => $totalSoldJobs,
            'totalSoldProducts' => $totalSoldProducts,
            'reviewsCount' => $reviewsCount,
            'reviewsAverage' => $reviewsAverage,
            'message' => new Message(),
            'offer' => new Offer(),
            'contactAccessInfo' => $contactAccessInfo,
            'cityAttribute' => $cityAttribute,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionShowAsCustomer($id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionStartSelling()
    {
        $this->layout = "start_selling";
        $page = Page::find()->joinWith(['translation'])->where(['page.alias' => 'start-selling'])->one();
        $this->registerMetaTags($page);

        $profiles = Profile::find()
            ->joinWith(['user', 'jobs', 'translation'])
            ->where(['profile.status' => Profile::STATUS_ACTIVE])
            ->andWhere(['exists', Job::find()
                ->select('job.id')
                ->where("job.user_id = user.id")
                ->andWhere(['job.type' => 'job'])
            ])
            ->andWhere(['not', ['gravatar_email' => null]])
            ->orderBy(new Expression('rand()'))
            ->groupBy('profile.user_id')
            ->limit(7);
        $buyers = Profile::find()
            ->joinWith(['user', 'translation'])
            ->where(['and',
                ['not', ['pct.title' => null]],
                ['not', ['pct.title' => '']],
                ['not', ['pct.content' => null]],
                ['not', ['pct.content' => '']],
                ['not', ['gravatar_email' => null]],
            ])
            ->orderBy(new Expression('rand()'))
            ->groupBy('profile.user_id')
            ->limit(4);

        $profilesDataProvider = new ActiveDataProvider(['query' => $profiles, 'pagination' => false]);
        $buyersDataProvider = new ActiveDataProvider(['query' => $buyers, 'pagination' => false]);

        $cheapest = Job::find()->select('price')->where(['type' => 'job', 'status' => Job::STATUS_ACTIVE])->orderBy('price asc')->one();
        $mostExpensive = Job::find()->select('price')->where(['type' => 'job', 'status' => Job::STATUS_ACTIVE])->orderBy('price desc')->one();

        return $this->render('start_selling', [
            'page' => $page,
            'profilesDataProvider' => $profilesDataProvider,
            'buyersDataProvider' => $buyersDataProvider,

            'cheapest' => $cheapest,
            'mostExpensive' => $mostExpensive
        ]);
    }
}