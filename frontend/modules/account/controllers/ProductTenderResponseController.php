<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.03.2017
 * Time: 16:37
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\modules\store\models\Order;
use frontend\modules\account\models\OrderSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;

class ProductTenderResponseController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['list'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }


    /** TABS BEGIN */
    public function actionList()
    {
        $searchModel = new OrderSearch([
            'user_id' => userId(),
            'product_type' => Order::TYPE_PRODUCT_TENDER,
            'type' => OrderSearch::TYPE_RESPONSE
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('tenders', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param $searchModel OrderSearch
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = OrderSearch::getResponseStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a($label . "&nbsp;(" . $searchModel->getStatusCount($status) . ")", ['/account/product-tender-response/list', 'status' => $status]),
                ['class' => $status == $searchModel->status ? 'active' : '']
            );
        }
        return $html;
    }
    /** TABS END */
}