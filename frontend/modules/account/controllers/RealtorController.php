<?php

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\ContentTranslation;
use common\models\PropertyClient;
use common\models\UserAttribute;
use frontend\modules\account\models\PropertyClientForm;
use frontend\modules\account\models\PropertyClientSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class RealtorController
 * @package frontend\modules\account\controllers
 */
class RealtorController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'register-client', 'edit-client'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return UserAttribute::find()
                                ->where([
                                    'entity_alias' => 'specialty',
                                    'value_alias' => 'rieltor',
                                    'user_id' => Yii::$app->user->identity->getCurrentId()
                                ])
                                ->exists();
                        }
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('warning', Yii::t('app', 'You are not allowed to access this page'));
                    return $this->redirect(isGuest() ? ['/site/index', 'type' => 10, 'showSignupModal' => 1] : ['/account/service/public-profile-settings']);
                },
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PropertyClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionRegisterClient()
    {
        $model = new PropertyClientForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->identity->getCurrentId();
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                $model->submit();
                return $this->redirect(['/account/realtor/index']);
            }
        }

        return $this->render('register-client-form', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionEditClient($id)
    {
        /* @var PropertyClient $client*/
        $client = PropertyClient::find()->where(['id' => $id, 'created_by' => Yii::$app->user->identity->getCurrentId()])->one();
        if ($client !== null) {
            $model = new PropertyClientForm();
            $model->setAttributes($client->attributes);
            $model->scenario = PropertyClientForm::SCENARIO_UPDATE;
            $model->client = $client;

            if ($model->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
                else {
                    $model->submit();
                    return $this->redirect(['/account/realtor/index']);
                }
            }

            return $this->render('register-client-form', [
                'model' => $model
            ]);
        }
        else {
            throw new NotFoundHttpException(Yii::t('app', 'Requested page is not found'));
        }
    }
}