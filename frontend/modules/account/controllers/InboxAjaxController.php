<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.04.2018
 * Time: 19:30
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Attachment;
use common\models\Conversation;
use common\models\ConversationMember;
use common\models\Message;
use common\models\Offer;
use common\models\user\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class InboxAjaxController
 * @package frontend\modules\account\controllers
 */
class InboxAjaxController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'accept-offer' => ['post'],
                    'decline-offer' => ['post'],
                    'cancel-offer' => ['post'],
                    'star' => ['post'],
                    'request' => ['post'],
                    'offer' => ['post'],
                    'send-message' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['request', 'offer', 'delete', 'star', 'accept-offer', 'decline-offer', 'cancel-offer', 'send-message']
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['request', 'offer', 'delete', 'star', 'accept-offer', 'decline-offer', 'cancel-offer', 'send-message'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $members
     * @param int $type
     * @return Conversation
     */
    private function createConversation($members, $type = Conversation::TYPE_DEFAULT)
    {
        $conversation = new Conversation();
        $conversation->type = $type;
        foreach ($members as $member) {
            $conversation->bind('conversationMembers')->attributes = ['user_id' => $member];
        }
        $conversation->save();

        return $conversation;
    }

    /**
     * @param $conversation_id
     * @param bool $support
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionSendMessage($conversation_id, $support = false)
    {
        $response = [
            'success' => false,
            'message' => Yii::t('app', 'Error occurred during sending message'),
        ];

        if (!$support || in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])) {
            /* @var $conversation Conversation */
            $user_id = Yii::$app->user->identity->getCurrentId();
            $conversation = Conversation::findByMembers([$support ? Yii::$app->params['supportId'] : $user_id])->joinWith(['conversationMembers'])->andWhere(['conversation.id' => $conversation_id])->one();

            if ($conversation !== null) {
                $redirect = false;
                if ($conversation->type === Conversation::TYPE_INFO && $conversation->created_by !== $user_id) {
                    $conversation_id = Conversation::findByMembers([$conversation->created_by, $user_id])
                        ->select('conversation.id')
                        ->andWhere(['type' => Conversation::TYPE_DEFAULT])
                        ->scalar();
                    if ($conversation_id === false) {
                        $conversation = $this->createConversation([$conversation->created_by, $user_id]);
                        $conversation_id = $conversation->id;
                    }
                    $redirect = true;
                }
                $message = new Message();
                if ($message->load(Yii::$app->request->post())) {
                    if ($support) {
                        $message->detachBehavior('blameable');
                        $message->user_id = Yii::$app->params['supportId'];
                    }
                    $message->conversation_id = $conversation_id;
                    if ($message->save()) {
                        $this->processMessageAttachments($message);
                        $message->created_at = Yii::$app->formatter->asDatetime($message->created_at, 'dd.MM.y HH:mm');
                        $message->updated_at = Yii::$app->formatter->asDatetime($message->updated_at, 'dd.MM.y HH:mm');
                        $response['success'] = true;
                        $response['message'] = Yii::t('app', 'Message sent');
                        if ($redirect === true) {
                            $response['redirect'] = Url::to(['/account/inbox/conversation', 'id' => $conversation_id, '#' => 'message-' . $message->id]);
                        }
                        else {
                            $response['item'] = $this->renderPartial('../inbox/conversation_message', ['model' => $message, 'from_id' => $message->user_id]);
                        }
                    }
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Requested conversation is not found'));
                return $this->redirect(['/account/inbox']);
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested conversation is not found'));
            return $this->redirect(['/account/inbox']);
        }

        return $response;
    }

    /**
     * @param integer $user_id
     * @param integer $type
     * @return mixed
     */
    protected function createIndividual($user_id, $type)
    {
        $type = (int)$type;
        $response = [
            'success' => false,
            'message' => $type === Offer::TYPE_REQUEST
                ? Yii::t('account', 'Error occurred during sending request')
                : Yii::t('account', 'Error occurred during sending offer')
        ];
        $identity_id = Yii::$app->user->identity->getCurrentId();

        /** @var $user User */
        $user = User::find()->where(['id' => $user_id])->one();

        if ($user !== null && $user->id !== $identity_id) {
            $accessInfo = Yii::$app->user->identity->isAllowedToCreateConversation($user);
            if (!$accessInfo['allowed']) {
                Yii::$app->session->setFlash('error', $accessInfo['body']
                    ? $accessInfo['body']
                    : Yii::t('app', 'You cannot create new conversation today'));
                return $this->redirect(['/account/inbox']);
            }

            /** @var $conversation Conversation */
            $conversation = $this->createConversation([$user_id, $identity_id], Conversation::TYPE_ORDER);
            $message = new Message();

            $post = Yii::$app->request->post();

            if ($message->load($post)) {
                $message->conversation_id = $conversation->id;
                $message->bind('offer')->attributes = [
                    'type' => $type,
                    'from_id' => $type === Offer::TYPE_REQUEST ? $user->id : $identity_id,
                    'to_id' => $type === Offer::TYPE_REQUEST ? $identity_id : $user->id,
                    'price' => $post['Offer']['price'],
                    'description' => $message->message,
                    'subject' => $post['Offer']['subject']
                ];

                if ($message->save()) {
                    $this->processMessageAttachments($message);
                    $response = [
                        'success' => true,
                        'message' => $type === Offer::TYPE_REQUEST
                            ? Yii::t('account', 'Request successfully sent')
                            : Yii::t('account', 'Offer successfully sent'),
                        'redirect' => Url::to(['/account/inbox/conversation', 'id' => $conversation->id])
                    ];
                }
            }
        }

        return $response;
    }

    /**
     * @param $user_id
     * @return bool
     */
    public function actionRequest($user_id)
    {
        $post = Yii::$app->request->post();
        $type = $post['Offer']['type'] ?? Offer::TYPE_REQUEST;

        return $this->createIndividual($user_id, $type);
    }

    /**
     * @param $user_id
     * @return bool
     */
    public function actionOffer($user_id)
    {
        $post = Yii::$app->request->post();
        $type = $post['Offer']['type'] ?? Offer::TYPE_OFFER;

        return $this->createIndividual($user_id, $type);
    }

    /**
     * @param $message
     */
    private function processMessageAttachments($message)
    {
        $path = '/uploads/inbox/' . date('Y') . '/' . date('m') . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $files = UploadedFile::getInstances($message, 'uploadedFiles');
        foreach ($files as $file) {
            $name = $path . $file->baseName . '-' . hash('crc32b', $file->name . time()) . "." . $file->extension;
            if ($file->saveAs(Yii::getAlias('@webroot') . $name)) {
                $attachment = new Attachment();
                $attachment->entity_id = $message->id;
                $attachment->entity = 'message';
                $attachment->content = $name;
                $attachment->save();
            }
        }
    }

    /**
     * @return array
     */
    public function actionDelete()
    {
        $response = ['success' => true];
        $ids = Yii::$app->request->post('ids');

        if (is_array($ids)) {
            $rowsDeleted = ConversationMember::updateAll(['status' => ConversationMember::STATUS_DELETED], [
                'conversation_id' => $ids,
                'user_id' => Yii::$app->user->identity->getCurrentId()
            ]);

            if($rowsDeleted > 0) {
                return $response;
            }
        }

        return ['success' => false];
    }


    /**
     * @return array
     */
    public function actionStar()
    {
        $response = ['success' => true];

        /** @var ConversationMember $member */
        $member = ConversationMember::find()->where([
            'conversation_id' => Yii::$app->request->post('id', null),
            'user_id' => Yii::$app->user->identity->getCurrentId()
        ])->one();

        if($member !== null) {
            $rowsUpdated = $member->updateAttributes(['starred' => !$member->starred]);

            if($rowsUpdated > 0) {
                return $response;
            }
        }

        return ['success' => false];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionAcceptOffer($id)
    {
        $response = ['success' => true];

        /** @var $offer Offer */
        $offer = Offer::find()->where(['id' => $id, 'status' => Offer::STATUS_SENT])->one();
        if ($offer !== null) {
            if ((hasAccess($offer->from_id) && $offer->type === Offer::TYPE_REQUEST) || (hasAccess($offer->to_id) && $offer->type === Offer::TYPE_OFFER)) {
                $rowsUpdated = $offer->updateAttributes(['status' => Offer::STATUS_ACCEPTED]);
                $statusMessageCreated = $offer->createStatusMessage();

                if($rowsUpdated > 0 && $statusMessageCreated === true) {
                    return $response;
                }
            }
        }

        return ['success' => false];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDeclineOffer($id)
    {
        $response = ['success' => true];

        /** @var $offer Offer */
        $offer = Offer::find()->where(['id' => $id, 'status' => Offer::STATUS_SENT])->one();
        if ($offer !== null) {
            if ((hasAccess($offer->from_id) && $offer->type === Offer::TYPE_REQUEST) || (hasAccess($offer->to_id) && $offer->type === Offer::TYPE_OFFER)) {
                $rowsUpdated = $offer->updateAttributes(['status' => Offer::STATUS_DECLINED]);
                $statusMessageCreated = $offer->createStatusMessage();

                if($rowsUpdated > 0 && $statusMessageCreated === true) {
                    return $response;
                }
            }
        }

        return ['success' => false];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionCancelOffer($id)
    {
        $response = ['success' => true];

        /** @var $offer Offer */
        $offer = Offer::find()->where(['id' => $id, 'status' => Offer::STATUS_SENT])->one();
        if ($offer !== null) {
            if ((hasAccess($offer->from_id) && $offer->type === Offer::TYPE_OFFER) || (hasAccess($offer->to_id) && $offer->type === Offer::TYPE_REQUEST)) {
                $rowsUpdated = $offer->updateAttributes(['status' => Offer::STATUS_CANCELED]);
                $statusMessageCreated = $offer->createStatusMessage();

                if($rowsUpdated > 0 && $statusMessageCreated === true) {
                    return $response;
                }
            }
        }

        return ['success' => false];
    }
}