<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.11.2016
 * Time: 16:10
 */

namespace frontend\modules\account\controllers;

use common\behaviors\LinkableBehavior;
use common\controllers\FrontEndController;
use common\models\CompanyMember;
use common\models\ContentTranslation;
use common\models\Notification;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserAttribute;
use common\modules\attribute\models\AttributeValue;
use frontend\modules\account\models\DeactivateForm;
use frontend\modules\account\models\PasswordChangeForm;
use frontend\modules\account\models\PauseForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ServiceAjaxController
 * @package common\modules\account\controllers
 */
class ServiceAjaxController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'process-password-change',
                        'process-deactivation',
                        'process-account-settings',
                        'process-public-profile-settings',
                        'process-payment-settings',
                        'process-social-network-settings',
                        'process-vacation',
                        'accept-company-invitation',
                        'decline-company-invitation'
                    ], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['accept-company-invitation', 'decline-company-invitation'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array|Response
     */
    public function actionProcessPublicProfileSettings()
    {
        /* @var Profile $model */
        $model = Profile::findOne(Yii::$app->user->identity->getId());
        if ($model !== null) {
            $input = Yii::$app->request->post();
            if (Yii::$app->request->isAjax) {
                $model->load($input);
                $model->user->load($input);
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model, $model->user);
            }

            if ($input != null) {
                $model->load($input);
                $model->userSkills = $input['UserSkillForm'] ?? [];
                $model->userLanguages = $input['UserLanguageForm'] ?? [];
                $model->userSpecialties = $input['UserSpecialtyForm'] ?? [];

                if ($model->save()) {
                    $input['ContentTranslation'][Yii::$app->language] = ['title' => $model->name, 'content' => $model->bio];
                    foreach ($input['ContentTranslation'] as $locale => $attributes) {
                        $translation = $model->translations[$locale] ?? new ContentTranslation(['entity' => 'profile', 'entity_id' => $model->user_id, 'locale' => $locale]);
                        $translation->load($attributes, '');
                        $translation->save();
                    }

                    if (!empty($input['city_attribute_id']) && $cityAttributeValue = AttributeValue::findOne((int)$input['city_attribute_id'])) {
                        $cityAttribute = UserAttribute::findOrCreate([
                            'user_id' => $model->user_id,
                            'entity_alias' => 'city',
                            'attribute_id' => $cityAttributeValue->attribute_id,
                            'locale' => 'ru-RU',
                        ], false);
                        $cityAttribute->value = (string)$cityAttributeValue->id;
                        $cityAttribute->value_alias = $cityAttributeValue->alias;
                        $cityAttribute->save();
                    }

                    Yii::$app->session->setFlash('success', Yii::t('account', 'Your account is updated'));
                }
            }
        }
        return $this->redirect(Url::to(['/account/service/account-settings']));
    }

    /**
     * @param $redirect
     * @return array|bool|Response
     */
    private function updateProfile($redirect)
    {
        /* @var Profile $model */
        $model = Profile::findOne(Yii::$app->user->identity->getCurrentId());
        if ($model !== null) {
            $input = Yii::$app->request->post();

            $model->load($input);
            $model->user->load($input);

            $model->user->phone = preg_replace("/[^0-9]/", "", $model->user->phone);
            if (!empty($model->user->phone)) {
                $model->user->phone =  "+" . $model->user->phone;
            }
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model, $model->user);
            }

            if (!empty($input['WithdrawalWallet'])) {
                $model->attachBehavior('linkableTemp', [
                    'class' => LinkableBehavior::class,
                    'relations' => ['withdrawalWallets'],
                ]);
                foreach ($input['WithdrawalWallet'] as $item) {
                    if (!empty($item['number'])) {
                        $model->bind('withdrawalWallets', $item['id'])->attributes = $item;
                    }
                }
            }

            if ($model->save() && $model->user->save()) {
                Yii::$app->session->setFlash('success', Yii::t('account', 'Your account is updated'));
            }

            return $this->redirect(Url::to($redirect));
        }

        return false;
    }

    /**
     * @return array|Response
     */
    public function actionProcessAccountSettings()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id !== null && $user->companyUser->company !== null && !$user->companyUser->company->companyMembers[userId()]->isAllowedToManage()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to edit company profile'));
            return $this->redirect(['/account/service/companies']);
        }
//        return $this->updateProfile(['/account/profile/show', 'id' => Yii::$app->user->identity->getId()]);
        return $this->updateProfile(['/account/service/account-settings']);
    }

    /**
     * @return array|Response
     */
    public function actionProcessPaymentSettings()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id !== null && $user->companyUser->company !== null && $user->companyUser->company->companyMembers[userId()]->role !== CompanyMember::ROLE_OWNER) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to edit company payment settings'));
            return $this->redirect(['/account/service/companies']);
        }
        return $this->updateProfile(['/account/service/payment-settings']);
    }

    /**
     * @return array|Response
     */
    public function actionProcessPasswordChange()
    {
        $passwordChangeModel = new PasswordChangeForm();

        if (Yii::$app->request->isAjax) {
            $passwordChangeModel->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($passwordChangeModel);
        }

        if ($input = Yii::$app->request->post()) {
            $passwordChangeModel->load($input);

            if ($passwordChangeModel->change()) {
                Yii::$app->session->setFlash('success', Yii::t('account', 'Your password is successfully changed'));
            }
        }

        return $this->redirect(Url::to(['/account/service/account-actions']));
    }

    /**
     * @return array|Response
     */
    public function actionProcessDeactivation()
    {
        $deactivateModel = new DeactivateForm();

        if (Yii::$app->request->isAjax) {
            $deactivateModel->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($deactivateModel);
        }

        if ($input = Yii::$app->request->post()) {
            $deactivateModel->load($input);

            if ($deactivateModel->deactivate()) {
                Yii::$app->session->setFlash('success', Yii::t('account', 'Your account is deactivated'));
            }
        }

        return $this->redirect(Url::to(['/account/service/account-actions']));
    }

    /**
     * @return array|Response
     */
    public function actionProcessVacation()
    {
        $pauseModel = new PauseForm();

        if (Yii::$app->request->isAjax) {
            $pauseModel->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($pauseModel);
        }

        if ($input = Yii::$app->request->post()) {
            $pauseModel->load($input);

            if ((int)$pauseModel->action === PauseForm::ACTION_RESUME) {
                $pauseModel->resume();
                Yii::$app->session->setFlash('success', Yii::t('account', 'Your account is resumed'));
            } else if ((int)$pauseModel->action === PauseForm::ACTION_PAUSE) {
                $pauseModel->pause();
                Yii::$app->session->setFlash('success', Yii::t('account', 'Your account is on vacation mode'));
            }
        }

        return $this->redirect(Url::to(['/account/service/account-actions']));
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionAcceptCompanyInvitation()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            $companyId = Yii::$app->request->post('entityId');
            $inviteId = Yii::$app->request->post('inviteId');

            $result = ['success' => false, 'message' => 'An unexpected error has occured'];

            if ($companyId !== null) {
                $hasAccess = CompanyMember::find()
                    ->where(['user_id' => userId(), 'company_id' => (int)$companyId, 'status' => CompanyMember::STATUS_INVITED])
                    ->exists();

                /** @var Notification $inviteNotification */
                $inviteNotification = Notification::find()
                    ->where(['to_id' => userId(), 'id' => $inviteId, 'subject' => Notification::SUBJECT_COMPANY_INVITE])
                    ->one();

                if ($hasAccess === true && $inviteNotification !== null) {
                    CompanyMember::updateAll([
                        'status' => CompanyMember::STATUS_ACTIVE
                    ], [
                        'user_id' => userId(),
                        'company_id' => (int)$companyId,
                        'status' => CompanyMember::STATUS_INVITED
                    ]);

                    $updatedCustomData = array_merge($inviteNotification->customDataArray, [
                        'action' => Notification::ACTION_ACCEPT
                    ]);
                    $inviteNotification->updateAttributes(['custom_data' => json_encode($updatedCustomData)]);

                    return [
                        'success' => true,
                        'message' => Yii::t('notifications', 'You have accepted invitation')
                    ];
                }
            }

            return $result;
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionDeclineCompanyInvitation()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            $companyId = Yii::$app->request->post('entityId');
            $inviteId = Yii::$app->request->post('inviteId');

            $result = ['success' => false, 'message' => 'An unexpected error has occured'];

            if ($companyId !== null) {
                $hasAccess = CompanyMember::find()
                    ->where(['user_id' => userId(), 'company_id' => (int)$companyId, 'status' => CompanyMember::STATUS_INVITED])
                    ->exists();

                /** @var Notification $inviteNotification */
                $inviteNotification = Notification::find()
                    ->where(['to_id' => userId(), 'id' => $inviteId, 'subject' => Notification::SUBJECT_COMPANY_INVITE])
                    ->one();

                if ($hasAccess === true && $inviteNotification !== null) {
                    CompanyMember::deleteAll([
                        'user_id' => userId(),
                        'company_id' => (int)$companyId,
                        'status' => CompanyMember::STATUS_INVITED
                    ]);

                    $updatedCustomData = array_merge($inviteNotification->customDataArray, [
                        'action' => Notification::ACTION_DECLINE
                    ]);
                    $inviteNotification->updateAttributes(['custom_data' => json_encode($updatedCustomData)]);

                    return [
                        'success' => true,
                        'message' => Yii::t('notifications', 'You have declined invitation')
                    ];
                }
            }

            return $result;
        }

        throw new BadRequestHttpException('Should be called other way');
    }
}