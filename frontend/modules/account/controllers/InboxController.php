<?php

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Conversation;
use common\models\ConversationMember;
use common\models\Message;
use common\models\Offer;
use common\models\user\User;
use common\models\UserAttribute;
use frontend\dto\LightConversationItemDTO;
use frontend\modules\account\models\ConversationSearch;
use frontend\modules\account\models\MessageSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class InboxController
 * @package frontend\modules\account\controllers
 */
class InboxController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ConversationSearch(['from_id' => Yii::$app->user->identity->getCurrentId()]);
        $data = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'isSupport' => false,
            'model' => $searchModel,
            'data' => $data,
        ]);
    }

    /**
     * @return string
     */
    public function actionSupport()
    {
        if (in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])) {
            $searchModel = new ConversationSearch(['from_id' => Yii::$app->params['supportId']]);
            $data = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'isSupport' => true,
                'model' => $searchModel,
                'data' => $data,
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));

        return $this->redirect(['/account/inbox']);
    }

    /**
     * @param $members
     * @param int $type
     * @return Conversation
     */
    private function createConversation($members, $type = Conversation::TYPE_DEFAULT)
    {
        $conversation = new Conversation();
        $conversation->type = $type;
        foreach ($members as $member) {
            $conversation->bind('conversationMembers')->attributes = ['user_id' => $member];
        }
        $conversation->save();

        return $conversation;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function actionStartConversation($user_id)
    {
        /* @var $user User */
        /* @var $conversation Conversation */
        $user = User::find()->where(['id' => $user_id])->one();
        $identity_id = Yii::$app->user->identity->getCurrentId();
        if ($user === null || $user->id == $identity_id) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested conversation is not found'));
            return $this->redirect(['/account/inbox']);
        }
        $accessInfo = Yii::$app->user->identity->isAllowedToCreateConversation($user);
        if (!$accessInfo['allowed']) {
            Yii::$app->session->setFlash('error', $accessInfo['body'] ?? Yii::t('app', 'You cannot create new conversation today'));
            return $this->redirect(['/account/inbox']);
        }

        $conversation = Conversation::findByMembers([$user_id, $identity_id])->andWhere(['type' => Conversation::TYPE_DEFAULT])->one();
        if ($conversation === null) {
            $conversation = $this->createConversation([$user_id, $identity_id]);
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['conversation_id' => $conversation->id];
        }
        else {
            return $this->redirect(['/account/inbox/conversation', 'id' => $conversation->id]);
        }
    }

    /**
     * @param $user_id
     * @return \yii\web\Response
     */
    public function actionStartSupportConversation($user_id)
    {
        /* @var $user User */
        /* @var $conversation Conversation */
        if (!in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));
            return $this->redirect(['/account/inbox']);
        }
        $user = User::find()->where(['id' => $user_id])->one();
        $identity_id = Yii::$app->params['supportId'];
        if ($user === null || $user->id == $identity_id) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested conversation is not found'));
            return $this->redirect(['/account/inbox']);
        }

        $conversation = Conversation::findByMembers([$user_id, $identity_id])->andWhere(['type' => Conversation::TYPE_DEFAULT])->one();
        if ($conversation === null) {
            $conversation = $this->createConversation([$user_id, $identity_id]);
        }

        return $this->redirect(['/account/inbox/support-conversation', 'id' => $conversation->id]);
    }

    /**
     * @param $conversation_id
     * @param $identity_id
     * @return string|\yii\web\Response
     */
    private function prepareConversationPage($conversation_id, $identity_id)
    {
        /** @var $conversation Conversation */
        $conversation = Conversation::find()->joinWith(['conversationMembers'])->where(['conversation.id' => $conversation_id])->one();
        $identity_id = (int)$identity_id;
        if ($conversation !== null && isset($conversation->conversationMembers[$identity_id])) {
            $languages = [];
            $isSupport = $identity_id === Yii::$app->params['supportId'];

            $conversation->conversationMembers[$identity_id]->updateAttributes(['viewed_at' => time()]);

            $messageSearch = new MessageSearch(['from_id' => $identity_id, 'limit' => null]);
            $data = $messageSearch->search([
                'status' => MessageSearch::STATUS_ALL,
                'conversation_id' => $conversation->id,
                'type' => MessageSearch::TYPE_MESSAGES
            ]);
            $conversationSearch = new ConversationSearch(['from_id' => $identity_id]);

            $conversationItemDto = new LightConversationItemDTO($conversation, $identity_id);
            $conversationData = $conversationItemDto->getData();
            $conversationData['offer'] = $conversation->firstMessage->offer ?? null;
            if (in_array($conversationData['type'], [Conversation::TYPE_DEFAULT, Conversation::TYPE_ORDER], true)) {
                $otherMember = $conversation->getOtherMember($identity_id);
                if ($otherMember !== null) {
                    $profileLanguages = UserAttribute::find()->joinWith(['attrValueDescriptions'])->where(['user_id' => $otherMember->user_id, 'entity_alias' => 'language'])->all();
                    $languages = ArrayHelper::map($profileLanguages, function ($model) {
                        return $model->attrValueDescriptions[Yii::$app->language]->title ?? array_values($model->attrValueDescriptions)[0]->title;
                    }, function ($model) {
                        return $model->customDataArray['level'] ?? null;
                    });

                    $conversationData['userId'] = $otherMember->user_id;
                }

                $showOfferActions = !isset($conversation->conversationMembers[Yii::$app->params['supportId']]);
            } else {
                $showOfferActions = false;
            }

            return $this->render('conversation', [
                'conversationData' => $conversationData,
                'isSupport' => $isSupport,
                'conversationSearch' => $conversationSearch,
                'data' => $data,
                'message' => new Message(),
                'offerMessage' => new Message(),
                'offer' => new Offer(),
                'languages' => $languages,
                'showOfferActions' => $showOfferActions
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested conversation is not found'));

        return $this->redirect(['/account/inbox']);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionConversation($id)
    {
        return $this->prepareConversationPage($id, Yii::$app->user->identity->getCurrentId());
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionSupportConversation($id)
    {
        if (in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])) {
            return $this->prepareConversationPage($id, Yii::$app->params['supportId']);
        }
        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));

        return $this->redirect(['/account/inbox']);
    }


    /**
     * @param int $id
     * @return array|\yii\web\Response
     */
    public function actionLeaveGroupChat(int $id)
    {
        if ($id !== null) {
            /** @var ConversationMember $conversationMember */
            $conversationMember = ConversationMember::find()
                ->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'conversation_id' => $id, 'status' => ConversationMember::STATUS_ACTIVE])
                ->one();

            if ($conversationMember !== null) {
                $updatedRows = $conversationMember->updateAttributes(['status' => ConversationMember::STATUS_DELETED]);

                if ($updatedRows > 0) {
                    Yii::$app->session->setFlash('success', Yii::t('messenger', 'You have left group chat'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));
                }
            }
        }

        return $this->redirect(['/account/inbox/index']);
    }
}