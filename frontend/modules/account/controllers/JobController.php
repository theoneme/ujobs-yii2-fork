<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.12.2016
 * Time: 17:50
 */

namespace frontend\modules\account\controllers;

use common\controllers\FrontEndController;
use common\models\Job;
use frontend\modules\account\models\JobSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;

/**
 * Class JobController
 * @package frontend\modules\account\controllers
 */
class JobController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['list'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $searchModel = new JobSearch(['type' => Job::TYPE_JOB]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('jobs', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param $searchModel JobSearch
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = JobSearch::getStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a($label . "&nbsp;(" . $searchModel->getStatusCount($status) . ")", ['/account/job/list', 'status' => $status]),
                ['class' => $status == $searchModel->status ? 'active' : '']
            );
        }
        return $html;
    }
}