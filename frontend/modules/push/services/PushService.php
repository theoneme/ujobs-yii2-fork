<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.07.2017
 * Time: 16:15
 */

namespace frontend\modules\push\services;

use common\models\user\User;
use console\jobs\PushNotificationJob;
use Minishlink\WebPush\WebPush;
use Yii;

/**
 * Class PushService
 * @package frontend\modules\push\services
 */
class PushService
{
    /**
     * @var User
     */
    private $receiver;

    /**
     * @var array
     */
    private $auth = ['VAPID' => [
        'subject' => 'mailto:devouryo@gmail.com',
        'publicKey' => 'BMuUgP6qOgp0onw4MyAN2vvESGyNC-WPtbfzUgRJ-xMon7ouL3NkL8Mzfwe4oafPxOcOqQLlk6OFcyNbaiZIk2E',
        'privateKey' => '53TxV0smrnRzQthVB71eUaPlRkIZLNkYAyIwMEsQxjo',
    ]];

    /**
     * @var WebPush
     */
    private $push;

    /**
     * PushService constructor.
     * @param User $receiver
     */
    public function __construct(User $receiver)
    {
        $this->receiver = $receiver;
        $this->push = new WebPush($this->auth);
    }

    /**
     * @param array $params
     * @return bool
     */
    public function sendNotification(array $params)
    {
        $subject = array_key_exists('heading', $params) ? "{$params['heading']}: " : '';
        $text = "{$subject}{$params['content']}. " . Yii::t('notifications', 'View by link: {link}', ['link' => $params['link']], $this->receiver->site_language);

        if(Yii::$app->params['notificationQueue'] === true) {
            return Yii::$app->queue->push(new PushNotificationJob([
                'pushId' => $this->receiver->push_id,
                'pushKey' => $this->receiver->push_key,
                'pushToken' => $this->receiver->push_token,
                'text' => $text
            ]));
        } else {
            return (bool)$this->push->sendNotification(
                $this->receiver->push_id,
                $text,
                $this->receiver->push_key,
                $this->receiver->push_token,
                true
            );
        }
    }
}