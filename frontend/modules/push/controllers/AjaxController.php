<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.07.2017
 * Time: 16:32
 */

namespace frontend\modules\push\controllers;

use common\models\Notification;
use common\models\user\User;
use frontend\modules\skype\commands\StartCommand;
use frontend\modules\skype\services\SkypeFactory;
use SkypeBot\Entity\MessagePayload;
use SkypeBot\SkypeBot;
use Yii;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\helpers\VarDumper;
use yii\web\MethodNotAllowedHttpException;

/**
 * Class AjaxController
 * @package frontend\modules\push\controllers
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['subscribe', 'fetch-notification', 'unsubscribe'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     *
     */
    public function actionSubscribe()
    {
        if (Yii::$app->request->isAjax) {
            $input = json_decode(Yii::$app->request->rawBody, true);

            if (!empty($input)) {
                $user = User::findOne(Yii::$app->user->identity->getId());

                if ($user !== null) {
                    $user->push_id = $input['endpoint'];
                    $user->push_key = $input['key'];
                    $user->push_token = $input['token'];

                    return $user->save();
                }
            }
        }

        return new MethodNotAllowedHttpException();
    }

    /**
     * @return bool|MethodNotAllowedHttpException
     */
    public function actionFetchNotification()
    {
        if (1) {
            $input = json_decode(Yii::$app->request->rawBody, true);

            if (!empty($input)) {
                /* @var User $user */
                $user = User::findOne(['push_id' => $input['endpoint']]);

                if ($user !== null) {
                    /* @var Notification $notification */
                    $notification = $user->lastSimpleNotification;
                    if ($notification !== null) {
                        $data = [
                            'notification' => [
                                'title' => Yii::t('notifications', 'You have new notification from {site}', ['site' => 'uJobs'], $user->site_language),
                                'message' => $notification->getHeader($user->site_language),
                                'icon' => Url::to('/images/new/logo.png', true),
                                'data' => Url::to($notification->link, true)
                            ]
                        ];

                        return json_encode($data);
                    }
                }
            }
        }

        return new MethodNotAllowedHttpException();
    }

    /**
     *
     */
    public function actionUnsubscribe()
    {
        if (Yii::$app->request->isAjax) {
            $user = User::findOne(Yii::$app->user->identity->getId());

            if ($user !== null) {
                $user->push_id = null;
                $user->push_key = null;
                $user->push_token = null;

                return $user->save();
            }
        }

        return new MethodNotAllowedHttpException();
    }
}