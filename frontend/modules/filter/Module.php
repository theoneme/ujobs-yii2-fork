<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:03
 */

namespace frontend\modules\filter;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\filter\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/filter/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@frontend/modules/filter/messages',
            'fileMap' => [
                'modules/filter/filter' => 'filter.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/filter/' . $category, $message, $params, $language);
    }
}