<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 15:13
 */

use common\models\user\Profile;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $model Profile */
/* @var $index integer */
/* @var $metaKeywords string */

$tariffTitle = $model->getActiveTariffTitle();
$tariffIcon = $model->getActiveTariffIcon();

?>

<div class="mob-work-table">
    <a data-pjax="0" class="mob-work-img" href="<?= Url::to(['/account/profile/show', 'id' => $model->user_id]) ?>">
        <?= Html::img($model->getThumb('catalog'), [
            'alt' => $model->getSellerName(),
            'title' => $model->getSellerName(),
        ]) ?>
        <?= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : '' ?>
    </a>
    <div class="mob-work-info">
        <?= Html::a(StringHelper::truncate(Html::encode($model->getDescription()), 110, '...', 'UTF-8'), [
            '/user/profile/show',
            'id' => $model->user_id
        ], [
            'class' => 'bf-descr',
            'data-pjax' => 0
        ]) ?>
        <div class="markers">
            <div class="stick text-center feat"><?= Yii::t('app', 'Favorites') ?></div>
        </div>
        <?php if ($model->rating) { ?>
            <div class="rate">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $model->rating / 2,
                    'size' => 14
                ]) ?>
            </div>
        <?php } ?>

        <div class="mob-block-bottom">
            <div class="by">
                <?= Yii::t('app', 'by') ?>
                <?= Html::a(Html::encode($model->getSellerName()), ['/account/profile/show', 'id' => $model->user_id], ['data-pjax' => 0]) ?>
            </div>
        </div>
    </div>
</div>