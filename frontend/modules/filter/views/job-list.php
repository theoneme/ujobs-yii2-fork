<?php

use common\models\elastic\JobElastic;
use common\models\Job;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Job|JobElastic */
/* @var $index integer */

$type = $model->type === Job::TYPE_ADVANCED_TENDER ? Job::TYPE_TENDER : $model->type;
$price = $model->contract_price ? '<span>' . Yii::t('app', 'contract') . '</span>' : $model->getPrice();
if ($model->type === Job::TYPE_ADVANCED_TENDER && !$model->contract_price) {
    $price .= ' ' . Yii::t('app', 'per month');
}

$tariffIcon = $model->getActiveTariffIcon();

?>

<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img($model->getSellerThumb('catalog'),['alt' => $model->getSellerName(), 'title' => $model->getSellerName()]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'by') ?>
            <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['data-pjax' => 0, 'title' => $model->getSellerName()]) ?>
        </div>
        <?php if ($model->type === Job::TYPE_JOB) { ?>
            <div class="rate">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $model->rating / 2,
                    'size' => 10
                ]) ?>
            </div>
            <?php if (isset($model->count_orders) && $model->count_orders > 0) {
                echo '<div class="rate">&nbsp;(' . $model->count_orders . ')</div>';
            } ?>
        <?php } ?>
    </div>
</div>
<a class="bf-alias" data-pjax="0" href="<?= Url::to(["/{$type}/view", 'alias' => $model->alias]) ?>">
    <div class="bf-img">
        <?= Html::img($model->getThumb('catalog'), [
            'alt' => $model->getLabel(),
            'title' => $model->getLabel(),
        ]) ?>
        <?= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon'],['alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : '' ?>
    </div>
    <div class="bf-descr">
        <?= Html::encode($model->getLabel()) ?>
    </div>
</a>
<div class="block-bottom">
    <?php if (!Yii::$app->user->isGuest && $model->type == Job::TYPE_JOB) {
        echo Html::a('<i class="fa fa-heart" aria-hidden="true"></i>', null, [
                'class' => 'like hint--top toggle-favourite ' . ($model->isFavourite() ? 'active' : ''),
                'data-hint' => Yii::t('app', 'Favourite'),
                'data-hint-add' => Yii::t('app', 'Favourite'),
                'data-hint-remove' => Yii::t('app', 'Favourite'),
                'data-id' => $model->id,
                'data-type' => 'job'
            ]
        );
    } ?>
    <div class="markers">
        <div class="stick text-center new <?= ($model->isTenderNew() ? 'active' : '') ?>"><?= Yii::t('app', 'New') ?></div>
    </div>
    <?php echo Html::a((count($model->packages) > 1 ? "<span>" . Yii::t('app', 'starting at') . "</span>" : '') . $price,
        ["/{$type}/view", 'alias' => $model->alias],
        ['class' => "block-price" . ($model->contract_price ? ' contract-price' : ''), 'data-pjax' => 0]
    ) ?>
</div>