<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.09.2017
 * Time: 15:51
 */

use common\models\elastic\UserPortfolioElastic;
use common\models\UserPortfolio;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model UserPortfolioElastic|UserPortfolio */
/* @var $index integer */
/* @var $metaKeywords string */
/* @var $editable boolean */


$category = $model->category;
$route = $category['type'] === 'product_category' ? ['/board/category/products', 'category_1' => $category['alias']] : ['/category/jobs', 'category_1' => $category['alias']];
$tariffIcon = $model->getActiveTariffIcon();
$tariffTitle = $model->getActiveTariffTitle();

?>
<div class="pg-over">
    <div class="block-top">
        <div class="block-top-ava">
            <?= Html::img($model->getSellerThumb('catalog'),['alt' => $model->getSellerName(), 'title' => $model->getSellerName()]) ?>
        </div>
        <div class="bt-name-rate">
            <div class="by">
                <?= Yii::t('app', 'by') ?>
                <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['data-pjax' => 0, 'title' => $model->getSellerName()]) ?>
            </div>
        </div>
    </div>
    <div class="bf-alias">
        <?= Html::a(
            Html::img($model->getThumb('catalog'),['alt' => $model->title, 'title' => $model->title]) . ($tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : ''),
            ['/account/portfolio/view', 'id' => $model->id],
            ['class' => 'bf-img album-show', 'data-pjax' => 0]
        )?>
        <div class="bf-descr">
            <?= Html::a(
                Html::encode($model->title),
                ['/account/portfolio/view', 'id' => $model->id],
                ['class' => 'album-title album-show', 'data-pjax' => 0]
            )?>
        </div>
        <div class="cat-related">
            <?= Html::a($model->category['title'], $route, ['data-pjax' => 0]) ?>
        </div>
    </div>

    <div class="block-bottom">
        <div class="album-opt like-over">
            <i class="fa fa-thumbs-up"></i><span><?= $model->likes_count ?></span>
            <?php if($model->likes_count > 0) { ?>
                <div class="people-likes">
                    <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                    <?php foreach ($model->likes as $userId => $likeProfile) {
                        echo Html::a(
                            Html::img($model->getLikeUserThumb($userId, 'catalog'), ['alt' => $likeProfile['name'], 'title' => $likeProfile['name']]),
                            ['/account/profile/show', 'id' => $userId],
                            ['data-pjax' => 0]
                        )?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="album-opt">
            <i class="fa fa-eye"></i><span><?= $model->views_count ?></span>
        </div>
    </div>
</div>