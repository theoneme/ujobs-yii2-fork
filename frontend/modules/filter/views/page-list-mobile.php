<?php

use common\models\elastic\PageElastic;
use common\models\Page;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $model Page|PageElastic */
/* @var $index integer */

//$tariffIcon = $model->getActiveTariffIcon();
//$tariffTitle = $model->getActiveTariffTitle();

?>

<div class="mob-news-header">
    <div class="mob-news-u-img">
        <?= Html::img($model->getSellerThumb())?>
    </div>
    <div class="mob-news-author">
        <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['data-pjax' => 0]) ?>
    </div>
    <div class="mob-news-date">
        <?= Yii::$app->formatter->asDate($model->publish_date) ?>
    </div>
</div>
<a class="mob-news-img" data-pjax="0" href="<?= $model->getUrl() ?>">
    <?= Html::img($model->getThumb(), ['alt' => $model->getLabel(), 'title' => $model->getLabel()]) ?>
    <?//= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon']) : '' ?>
</a>
<div class="mob-news-info">
    <?= Html::a(Html::encode($model->getLabel()),
        $model->getUrl(),
        ['class' => 'mob-news-title', 'data-pjax' => 0]
    ) ?>
    <div class="mob-news-text">
        <?= StringHelper::truncate(strip_tags($model->getContent()), 100)?>
    </div>
</div>
<div class="mob-news-bottom">
<!--    <div class="mob-news-comments">-->
<!--        --><?//=Yii::t('app', '{count} comments', ['count' => count($model->comments)])?>
<!--    </div>-->
    <div class="album-opt like-over">
        <i class="fa fa-thumbs-up"></i>
        <span><?= count($model->likes) ?></span>
        <?php if(count($model->likes) > 0) { ?>
            <div class="people-likes">
                <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                <?php foreach ($model->likes as $userId => $like) {
                    echo Html::a(
                        Html::img($model->getLikeUserThumb($userId, 'catalog'), ['alt' => $model->getLikeSellerName($userId), 'title' => $model->getLikeSellerName($userId)]),
                        ['/account/profile/show', 'id' => $userId]
                    );
                } ?>
            </div>
        <?php } ?>
    </div>
    <div class="album-opt">
        <i class="fa fa-eye"></i>
        <span><?= $model->views ?></span>
    </div>
</div>