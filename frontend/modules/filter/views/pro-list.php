<?php

use common\models\user\Profile;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $model Profile */

$tariffIcon = $model->getActiveTariffIcon();

?>
<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img(Yii::$app->mediaLayer->getThumb($model->gravatar_email, 'catalog'),['alt' => $model->getSellerName(), 'title' => $model->getSellerName()]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'by') ?>
            <?= Html::a(Html::encode($model->getSellerName()), [
                '/account/profile/show', 'id' => $model->user_id
            ], ['data-pjax' => 0, 'title' => $model->getSellerName()]) ?>
        </div>
        <div class="rate">
            <?= $this->render('@frontend/widgets/views/rating', [
                'rating' => $model->rating / 2,
                'size' => 10
            ]) ?>
        </div>
        <?php if (isset($model->sold_items) && $model->sold_items > 0) {
            echo '<div class="rate">&nbsp;(' . $model->sold_items . ')</div>';
        } ?>
    </div>
</div>
<a data-pjax="0" class="bf-alias" href="<?= Url::to(['/account/profile/show', 'id' => $model->user_id]) ?>">
    <div class="bf-img">
        <?= Html::img($model->getThumb('catalog'), [
            'alt' => $model->getSellerName(),
            'title' => $model->getSellerName(),
        ]) ?>
        <?= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : '' ?>
    </div>
    <div class="bf-descr">
        <?= $model->getBio() ?>
    </div>
</a>