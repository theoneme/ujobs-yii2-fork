<?php

use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Product|ProductElastic */
/* @var $index integer */

$type = $model->type;
$price = $model->contract_price ? '<span>' . Yii::t('app', 'contract') . '</span>' : $model->getPrice();

$tariffIcon = $model->getActiveTariffIcon();
$tariffTitle = $model->getActiveTariffTitle();
?>

<?php if (!Yii::$app->user->isGuest && $type == Product::TYPE_PRODUCT) {
    echo Html::a('<i class="fa fa-heart" aria-hidden="true"></i>', null, [
            'class' => 'like hint--top toggle-favourite ' . ($model->isFavourite() ? 'active' : ''),
            'data-hint' => Yii::t('app', 'Favourite'),
            'data-hint-add' => Yii::t('app', 'Favourite'),
            'data-hint-remove' => Yii::t('app', 'Favourite'),
            'data-id' => $model->id,
            'data-type' => 'product'
        ]
    );
} ?>
<div class="mob-work-table">
    <a class="mob-work-img" data-pjax="0" href="<?= Url::to(["/board/{$type}/view", 'alias' => $model->alias]) ?>">
        <?= Html::img($model->getThumb('catalog'), [
            'alt' => $model->getLabel(),
            'title' => $model->getLabel(),
        ]) ?>
        <?= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : '' ?>
    </a>

    <div class="mob-work-info">
        <?= Html::a(Html::encode($model->getLabel()),
            ["/board/product/view", 'alias' => $model->alias],
            ['class' => 'bf-descr', 'data-pjax' => 0]
        ) ?>
        <div class="mob-block-bottom">
            <div class="by">
                <?= Yii::t('app', 'by') ?>
                &nbsp;<?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['data-pjax' => 0]) ?>
            </div>
            <?= Html::a($price,
                ["/board/{$type}/view", 'alias' => $model->alias],
                ['class' => 'block-price', 'data-pjax' => 0]
            ) ?>
        </div>
    </div>
</div>