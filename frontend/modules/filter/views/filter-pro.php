<?php
use common\models\Category;
use frontend\components\pro\RecommendedPros;
use frontend\components\pro\SimilarPros;
use yii\widgets\ListView;

/* @var $category Category */
/* @var $parentCategory Category|null */
/* @var $categories Category[] */
/* @var $prices array */
/* @var $priceSliderPoints array */
/* @var $sortData array */
/* @var $paginationData array */
/* @var $entityToRouteMatches array */
/* @var $attributeRemoveLinks array */
/* @var $showPriceSlider bool */
/* @var $filtersHtml string */
/* @var $categoryHeader string */
/* @var $type string */
/* @var $request string */
/* @var $entityTitle string */
/* @var $breadcrumbs array */
/* @var $similarCategories null|array */
/* @var $showSecureBanner boolean */
?>

<?= $this->render('top-filter-section', [
    'type' => $type,
    'entitySwitcher' => false,
    'attributeRemoveLinks' => $attributeRemoveLinks,
    'sortData' => $sortData,
    'paginationData' => $paginationData,
    'request' => $request,
]) ?>
<aside>
    <?= $this->render('left-filter-section', [
        'category' => $category,
        'categories' => $categories,
        'categoryHeader' => $categoryHeader,
        'showPriceSlider' => false,
        'filtersHtml' => $filtersHtml,
        'attributeRemoveLinks' => $attributeRemoveLinks,
        'showSecureBanner' => $showSecureBanner,
        'showRequestBanner' => $showRequestBanner ?? false,
    ]) ?>
</aside>
<div class="cat-list">
    <?php if (!isPagespeed()) {
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'pro-list',
            'layout' => "<div class='block-flex'>{items}</div>{pager}",
            'options' => [
                'tag' => 'div',
                'class' => ''
            ],
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'bf-item'
            ],
            'emptyText' =>
                '<p class="catalog-empty-text">' .
                Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => $entityTitle]) .
                '</p>',
        ]);
    }
    if ($dataProvider->totalCount <= 8) {
        echo SimilarPros::widget(['request' => $entityTitle, 'sliderCount' => 5]);
        echo RecommendedPros::widget(['category' => $parentCategory, 'similarCategories' => $similarCategories, 'sliderCount' => 5]);
    } ?>

</div>