<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.08.2016
 * Time: 14:22
 */

use common\models\Category;
use common\models\JobAttribute;
use frontend\assets\SelectizeAsset;
use frontend\assets\SlickAsset;
use frontend\components\BannerWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var mixed $filtersHtml */
/* @var Category $category */
/* @var Category[] $categories */
/* @var $relatedTags JobAttribute[] */
/* @var $tagAttribute JobAttribute */
/* @var bool $showPriceSlider */
/* @var array $prices */
/* @var array $priceSliderPoints */
/* @var $tagsFirst bool */
/* @var $attributeRemoveLinks array */
/* @var $categoryHeader string */
/* @var $showSecureBanner boolean */
/* @var $showRequestBanner boolean */

SelectizeAsset::register($this);
SlickAsset::register($this);

?>


<?php if (!empty($attributeRemoveLinks)) { ?>
    <div class="checked-filter clearfix">
        <?php foreach ($attributeRemoveLinks as $key => $attribute) { ?>
            <?php foreach ($attribute['values'] as $value) { ?>
                <?= Html::a("<i class='fa fa-times' aria-hidden='true'></i>{$value['value']}", $value['url']) ?>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>
    <div class="aside-body">
        <div class="text-right">
            <?php if ($attributeRemoveLinks) { ?>
                <?= Html::a(Yii::t('app', 'Clear All'), Url::canonical()); ?>
            <?php } ?>
        </div>
        <div class="aside-block">
            <ul class="cats no-markers">
                <li>
                    <h2 class="text-left"><?= $categoryHeader ?></h2>
                    <?php foreach ($categories as $group => $items) { ?>
                        <?php /** @var array $items */ ?>
                        <?php if (count($categories) > 1) { ?>
                            <h3><?= $group === 'job_category' ? Yii::t('app', 'Job Categories') : Yii::t('app', 'Product Categories') ?></h3>
                        <?php } ?>
                        <div class="ml <?= (count($items) > 6) ? "hide-filter hf-categories" : "" ?>">
                            <ul>
                                <?php foreach ($items as $key => $category_item) { ?>
                                    <li class="<?= isset($category_item['active']) ? "active" : "" ?>">
                                        <?= Html::tag($category_item['options']['tag'], $category_item['label'] . ($category_item['count'] ? " ({$category_item['count']})" : ''), $category_item['linkOptions']) ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php if (count($items) > 6) {
                            echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($items) - 6)]), null, [
                                'class' => 'more-less more-cats',
                                'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($items) - 6)]),
                                'data-less' => Yii::t('app', 'See less'),
                            ]) ?>
                        <?php } ?>
                    <?php } ?>

                    <?php if (count($categories) > 6) {
                        echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($categories) - 6)]), null, [
                            'class' => 'more-less more-cats',
                            'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($categories) - 6)]),
                            'data-less' => Yii::t('app', 'See less'),
                        ]) ?>
                    <?php } ?>
                </li>
            </ul>
        </div>
        <?php if ($showRequestBanner) { ?>
            <div class="prof-aside-block welcome-left">
                <div class="welcome-left-title">
                    <?= Yii::t('app', 'Don\'t have time to search?')?>
                </div>
                <p>
                    <?= Yii::t('app', 'Please post a request and professionals will send you their proposals')?>
                </p>
                <?= Html::a(Yii::t('account', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'btn-middle text-center', 'data-pjax' => 0]) ?>
            </div>
        <?php } ?>
        <?= Html::beginForm('', 'post', [
            'id' => 'filter_form',
        ]) ?>
            <div id="filters">
                <?= $filtersHtml ?>
            </div>
            <?php if ($showPriceSlider) { ?>
                <?= Html::hiddenInput('price', isset(Yii::$app->request->queryParams['price']) ? $priceSliderPoints['min'] . '-' . $priceSliderPoints['max'] : '', ['id' => 'price-holder']) ?>
                <div class="aside-block">
                    <div class="fi-title"><?= Yii::t('filter', 'Price') ?></div>
                    <div class="flex">
                        <div class="price-label-wrap">
                            <input class="price-label text-center" id="min-price" value="<?= $priceSliderPoints['min'] ?>">
                        </div>
                        <div class="price-label-wrap">
                            <input class="price-label text-center" id="max-price" value="<?= $priceSliderPoints['max'] ?>">
                        </div>
                    </div>

                    <div class="noUi-price-over">
                        <div id="slider-price"></div>
                    </div>
                </div>
            <?php } ?>
        <?= Html::endForm() ?>
    </div>
    <?= BannerWidget::widget() ?>
    <?php if (Yii::$app->language === 'ru-RU') {?>
        <div id="yandex_rtb_R-A-243363-1"></div>
    <?php } else { ?>
        <ins class="adsbygoogle"
             style="display:inline-block;width:240px;height:400px"
             data-ad-client="ca-pub-7280089675102373"
             data-ad-slot="7816114377">
        </ins>
    <?php } ?>

<?php $canonicalUrl = Url::canonical();
$attrListUrl = Url::to(['/ajax/attribute-list']);
$currencyCode = Yii::$app->params['currencies'][Yii::$app->params['app_currency_code']]['sign'];
$script = <<<JS
	let sliderTimer = null;

    function applyFilter(url) {
        let baseUrl = "$canonicalUrl";
        if(url.length > 0) {
            if(baseUrl.indexOf('?') > -1) {
                baseUrl += '&' + url;
            } else {
                baseUrl += '?' + url;
            }         
        } 

        $.pjax.reload({container: '#catalog-pjax', url: baseUrl});
    }

    function processFilterForms(object) {
        let inputs = $('#filter_form input:not(.price-label):not([name=_csrf-frontend]):not([value=""]), #top_filter_form input:not([name=_csrf-frontend])'),
            dropdowns = $('#filter_form select');
        $.each(dropdowns, function(index, value) {
            if($(this).find('option:selected') && $(this).find('option:selected').val()) {
                inputs.push($(this)[0]);
            }
        }).promise().done(function() {
            let url = inputs.filter(function(index, element) {
				return $(element).val() !== "";
			}).serialize();
            
            applyFilter(url);
        });
    }

    $('#filters input[type=radio], #filters input[type=checkbox], #filters select').change(function() {
        processFilterForms($(this));
    });

    $('.show-all').on('click', function(){
        $(this).hide();
        $('a.tag-but').css('display', 'inline-block');
        return false;
    });

    $(".selectize-input-container select:not(.selectized)").each(function(){
        let url = '$attrListUrl?alias=' + $(this).data('alias');
        $(this).selectize({
            valueField: "alias",
            labelField: "value",
            searchField: ["value"],
            maxOptions: 10,
            "load": function(query, callback) {
                if (!query.length) {
                    return callback();
                }
                $.post(url, {query: encodeURIComponent(query)}, function(data) {
                    callback(data);
                }).fail(function() {
                    callback();
                });
            }
        });
    });
JS;

$this->registerJs($script, yii\web\View::POS_READY, 'filters'); ?>

<?php if ($showPriceSlider) {
    $leftCurrencySymbol = $currency->symbol_left;
    $rightCurrencySymbol = $currency->symbol_right;
    $maxPrice = $prices['max'];
    $minPrice = $prices['min'];
    $maxSliderPoint = $priceSliderPoints['max'];
    $minSliderPoint = $priceSliderPoints['min'];
    $priceOffset1 = (int)($prices['max'] - $prices['min']) * 0.1;
    $priceOffset2 = (int)($prices['max'] - $prices['min']) * 0.3;
    $priceBorder1 = $minPrice + $priceOffset1;
    $priceBorder2 = $minPrice + $priceOffset2;

    $script = <<<JS
    let priceSlider = document.getElementById('slider-price'),
        inputs = [document.getElementById('min-price'), document.getElementById('max-price')];

    var priceDir = 'ltr';
    if($('body').hasClass('rtl')){
        priceDir = 'rtl';
    }

    function updatePriceLabels(min, max) {
        $('#min-price').val(min);
        $('#max-price').val(max);

        if(typeof max === 'string') {
            max = max.replace(/[^0-9]/g, '');
        }
        if(typeof min === 'string') {
            min = min.replace(/[^0-9]/g, '');
        }
        $('#price-holder').val(min + '-' + max);
    }

    function setSliderHandle(instance, i, value) {
        let r = [null, null];
        r[i] = value;

        instance.noUiSlider.set(r);

        let values = instance.noUiSlider.get();
        updatePriceLabels(values[0], values[1]);

		clearTimeout(sliderTimer);
        sliderTimer = setTimeout(function() {
            processFilterForms($(this));
        }, 300);
    }

    function initPriceSlider(instance, inputs) {
        noUiSlider.create(instance, {
            connect: true,
            behaviour: 'tap',
            direction: priceDir,
            start: [$minSliderPoint, $maxSliderPoint],
            range: {
                'min': [$minPrice],
                '60%': [$priceBorder1],
                '80%': [$priceBorder2],
                'max': [$maxPrice]
            },
            format: {
                to: function(value) {
                    result = '';
                    if(value === {$minPrice}) {
                        result = '<';
                    }
                    if(value === {$maxPrice}) {
                        result = '>';
                    }
                    
                    return result + ' {$leftCurrencySymbol}' + parseInt(value) + '{$rightCurrencySymbol}';
                },
                from: function(value) {
                    return parseInt(value.replace(/[^0-9]/g, ''));
                },
            }
        });

        instance.noUiSlider.on('end', function(values) {
            updatePriceLabels(values[0], values[1]);

            clearTimeout(sliderTimer);
            sliderTimer = setTimeout(function() {
                processFilterForms($(this));
            }, 300);
        });

        instance.noUiSlider.on('update', function(values, handle) {
            inputs[handle].value = values[handle];
        });

        inputs.forEach(function(input, handle){
            input.addEventListener('change', function() {
                setSliderHandle(instance, handle, this.value);
            });
        });
    }

    initPriceSlider(priceSlider, inputs);
JS;

    $this->registerJs($script);
} ?>