<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 15:01
 */

use common\models\Job;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Job */
/* @var $index integer */

$type = $model->type == Job::TYPE_ADVANCED_TENDER ? Job::TYPE_TENDER : $model->type;
$price = $model->contract_price ? '<span>' . Yii::t('app', 'contract') . '</span>' : $model->getPrice();
if ($model->type == Job::TYPE_ADVANCED_TENDER && !$model->contract_price) {
    $price .= ' ' . Yii::t('app', 'per month');
}

$tariffIcon = $model->getActiveTariffIcon();
$tariffTitle = $model->getActiveTariffTitle();

?>

<?php
if (!Yii::$app->user->isGuest && $model->type == Job::TYPE_JOB) {
    echo Html::a('<i class="fa fa-heart" aria-hidden="true"></i>', null, [
            'class' => 'like hint--top toggle-favourite ' . ($model->isFavourite() ? 'active' : ''),
            'data-hint' => Yii::t('app', 'Favourite'),
            'data-hint-add' => Yii::t('app', 'Favourite'),
            'data-hint-remove' => Yii::t('app', 'Favourite'),
            'data-id' => $model->id,
            'data-type' => 'job'
        ]
    );
}
?>
<div class="mob-work-table">
    <a class="mob-work-img" data-pjax="0" href="<?= Url::to(["/{$type}/view", 'alias' => $model->alias]) ?>">
        <?= Html::img($model->getThumb('catalog'), [
            'alt' => $model->getLabel(),
            'title' => $model->getLabel(),
        ]) ?>
        <?= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : '' ?>
    </a>
    <div class="mob-work-info">
        <?= Html::a(Html::encode($model->getLabel()),
            ["/{$type}/view", 'alias' => $model->alias],
            ['class' => 'bf-descr', 'data-pjax' => 0]
        ) ?>
        <?php if ($model->rating) { ?>
            <div class="rate">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $model->rating / 2,
                    'size' => 14
                ]) ?>
            </div>
        <?php } ?>

        <div class="mob-block-bottom">
            <div class="by">
                <?= Yii::t('app', 'by') ?>
                &nbsp;<?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['data-pjax' => 0]) ?>
            </div>
            <?= Html::a($price,
                ["/{$type}/view", 'alias' => $model->alias],
                ['class' => 'block-price', 'data-pjax' => 0]
            ) ?>
        </div>
    </div>
</div>