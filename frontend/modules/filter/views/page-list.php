<?php

use common\models\elastic\PageElastic;
use common\models\Page;
use yii\helpers\Html;

/* @var $model Page|PageElastic */
/* @var $index integer */

//$tariffIcon = $model->getActiveTariffIcon();
//$tariffTitle = $model->getActiveTariffTitle();

?>

<div class="cmd-news-head">
    <div class="cmd-avatar">
        <div>
            <?= Html::img($model->getSellerThumb(),['alt' => $model->getSellerName(), 'title' => $model->getSellerName()])?>
        </div>
    </div>
    <div class="cmd-head-info">
        <div class="cmd-head-title">
            <?= Html::a($model->getSellerName(), $model->getSellerUrl(), ['data-pjax' => 0])?>
        </div>
        <div class="cmd-head-time">
            <?= Yii::$app->formatter->asDatetime($model->publish_date)?>
        </div>
    </div>
</div>
<?= Html::a(Html::img($model->getThumb(),['alt' => $model->getLabel(), 'title' => $model->getLabel()]), $model->getUrl(), ['class' => 'cmd-img text-center', 'data-pjax' => 0])?>
<div class="cmd-info-title">
    <?= Html::a($model->getLabel(), $model->getUrl(), ['data-pjax' => 0])?>
</div>
<div class="cmd-info">
    <?= strip_tags($model->getContent(), '<p>')?>
</div>
<?= Html::a(Yii::t('app', 'More'), $model->getUrl(), ['class' => 'cat-news-more link-blue', 'data-pjax' => 0])?>
<div class="clearfix">
    <div class="album-opt like-over pull-right">
        <i class="fa fa-thumbs-up"></i>
        <span><?= count($model->likes) ?></span>
        <?php if(count($model->likes) > 0) { ?>
            <div class="people-likes">
                <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                <?php foreach ($model->likes as $userId => $like) {
                    echo Html::a(
                        Html::img($model->getLikeUserThumb($userId, 'catalog'), ['alt' => $model->getLikeSellerName($userId), 'title' => $model->getLikeSellerName($userId)]),
                        ['/account/profile/show', 'id' => $userId], ['data-pjax' => 0]
                    );
                } ?>
            </div>
        <?php } ?>
    </div>
    <div class="album-opt pull-right">
        <i class="fa fa-eye"></i>
        <span><?= $model->views ?></span>
    </div>
</div>