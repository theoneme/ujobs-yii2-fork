<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $entitySwitcher bool */
/* @var $attributeRemoveLinks array */
/* @var $sortData array */
/* @var $paginationData array */
/* @var $type string */
/* @var $request string */

?>

<div class="container-fluid">
    <div class="filter-top row">
        <?= Html::beginForm(Url::canonical(), 'post', [
            'id' => 'top_filter_form'
        ]) ?>
        <div id="catalog-search">
            <?= Html::textInput('request', $request, [
                'class' => 'form-control autocomplete',
                'placeholder' => Yii::t('app', 'Search'),
            ]) ?>
            <button type="submit" class="btn-middle">
                <?= Yii::t('app', 'Search') ?>
            </button>
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>
        <div id="catalog-entity-create">
            <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle btn-hov-dark', 'data-pjax' => 0]) ?>
        </div>
        <div id="catalog-sort" class="text-right">
            <?= Html::hiddenInput('sort_by', isset(Yii::$app->request->queryParams['sort_by']) ? $sortData['sort_by']['value'] : '', ['id' => 'sort-by']) ?>
            <?= Html::hiddenInput('page', isset(Yii::$app->request->queryParams['page']) ? $paginationData['page'] : '', ['id' => 'page']) ?>
            <?= Html::hiddenInput('per-page', isset(Yii::$app->request->queryParams['per-page']) ? $paginationData['per-page'] : '', ['id' => 'per-page']) ?>

            <?php if (count($sortData['sortTitles']) > 1) { ?>
                <div class="ftr-text"><?= Yii::t('app', 'Sort By') ?>:</div>
                <div class="ftr-choose" id="sort-selector">
                    <div class="ftr-current">
                        <a href="javascript:void(0)"><?= $sortData['sort_by']['title'] ?></a>
                    </div>
                    <ul data-target="sort-by" data-selector="sort-selector" class="ftr-list no-markers">
                        <?php foreach ($sortData['sortTitles'] as $sort => $title) {
                            echo Html::tag('li',
                                Html::a($title, null, ['data-entity' => $sort])
                            );
                        } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
        <?= Html::endForm() ?>
    </div>
</div>

<?php $searchUrl = Url::to(['/ajax/catalog-search', 'type' => $type]);
$script = <<<JS
    $('#top_filter_form').submit(function() {
        processFilterForms($(this));
        return false;
    });

    $('#sort-selector li a, #sort-dir-selector li a').click(function(e) {
        e.preventDefault();
        let target = $(this).parent().parent().attr('data-target'),
            selector = $(this).parent().parent().attr('data-selector'),
            entity = $(this).attr('data-entity');

        $('#' + target).val(entity);
        processFilterForms($(this));
    });

    $('.ftr-current a').on('click', function(e) {
        e.preventDefault();
        $(this).parents('.ftr-choose').toggleClass('open');
        return false;
    });

    $('.ftr-list').mouseleave(function() {
        $(this).parents('.ftr-choose').removeClass('open');
    });
    
    new autoComplete({
        selector: "#catalog-search input",
        minChars: 2,
        autoFocus: false,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$searchUrl}', { request: request }, function(data) { 
                let autoCSource = [];
                if (data.success === true) {
                    $.each(data.data, function(key, value) {
                        autoCSource.push(value.label);
                    });
                }
                response(autoCSource); 
            });     
        },
    });
JS;

$this->registerJs($script, yii\web\View::POS_READY, 'top-filter'); ?>
