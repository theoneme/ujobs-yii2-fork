<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.09.2017
 * Time: 15:38
 */

use common\models\Category;
use common\models\JobAttribute;
use yii\data\ActiveDataProvider;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/* @var $category Category */
/* @var $parentCategory Category|null */
/* @var $categories Category[] */
/* @var $relatedTags JobAttribute[] */
/* @var $tagAttribute JobAttribute */
/* @var $prices array */
/* @var $priceSliderPoints array */
/* @var $entityToRouteMatches array */
/* @var $attributeRemoveLinks array */
/* @var $paginationData array */
/* @var $sortData array */
/* @var $showPriceSlider bool */
/* @var $tagsFirst bool */
/* @var $filtersHtml string */
/* @var $metaKeywords string */
/* @var $categoryHeader string */
/* @var $request string */
/* @var $dataProvider ActiveDataProvider */
/* @var $type string */
/* @var $entityTitle string */
/* @var $breadcrumbs array */
/* @var $similarCategories null|array */
/* @var $showSecureBanner boolean */
/* @var $showRequestBanner boolean */

?>

<?= $this->render('top-filter-section', [
    'type' => $type,
    'entitySwitcher' => true,
    'attributeRemoveLinks' => $attributeRemoveLinks,
    'sortData' => $sortData,
    'paginationData' => $paginationData,
    'request' => $request,
]) ?>

<aside>
    <?= $this->render('left-filter-section', [
        'category' => $category,
        'categories' => $categories,
        'categoryHeader' => $categoryHeader,
        'prices' => [],
        'showPriceSlider' => false,
        'priceSliderPoints' => [],
        'filtersHtml' => $filtersHtml,
        'attributeRemoveLinks' => $attributeRemoveLinks,
        'showSecureBanner' => $showSecureBanner,
        'showRequestBanner' => $showRequestBanner ?? false,
    ]) ?>
    <ins class="adsbygoogle"
         style="display:inline-block;width:240px;height:400px"
         data-ad-client="ca-pub-7280089675102373"
         data-ad-slot="3655052679">
    </ins>
</aside>

<div class="cat-list">
    <?php echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => 'portfolio-list',
        'layout' => "<div class='clearfix'>{items}</div>",
        'options' => [
            'tag' => 'div',
            'class' => 'portfolio-grid',
            'id' => 'portfolio-grid'
        ],
        'itemOptions' => [
            'class' => 'pg-item'
        ],
        'emptyText' =>
            '<p class="catalog-empty-text">' .
            Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => $entityTitle]) .
            '</p>',
    ]); ?>

    <?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]) ?>
</div>