<?php

use common\models\Category;
use common\models\JobAttribute;
use frontend\components\ScrollPager;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;

/* @var $category Category */
/* @var $parentCategory Category|null */
/* @var $categories Category[] */
/* @var $relatedTags JobAttribute[] */
/* @var $tagAttribute JobAttribute */
/* @var $prices array */
/* @var $priceSliderPoints array */
/* @var $entityToRouteMatches array */
/* @var $attributeRemoveLinks array */
/* @var $paginationData array */
/* @var $sortData array */
/* @var $showPriceSlider bool */
/* @var $tagsFirst bool */
/* @var $filtersHtml string */
/* @var $metaKeywords string */
/* @var $categoryHeader string */
/* @var $request string */
/* @var $dataProvider ActiveDataProvider */
/* @var $jobsDataProvider ActiveDataProvider */
/* @var $type string */
/* @var $entityTitle string */
/* @var $breadcrumbs array */
/* @var $similarCategories null|array */
/* @var $this View */
/* @var $showSecureBanner boolean */
/* @var $showRequestBanner boolean */

?>

<?= $this->render('top-filter-section', [
    'type' => $type,
    'entitySwitcher' => true,
    'attributeRemoveLinks' => $attributeRemoveLinks,
    'sortData' => $sortData,
    'paginationData' => $paginationData,
    'request' => $request,
]) ?>


<div class="clearfix">
    <aside>
        <?= $this->render('left-filter-section', [
            'category' => $category,
            'categories' => $categories,
            'categoryHeader' => $categoryHeader,
            'prices' => $prices,
            'showPriceSlider' => $showPriceSlider,
            'priceSliderPoints' => $priceSliderPoints,
            'filtersHtml' => $filtersHtml,
            'attributeRemoveLinks' => $attributeRemoveLinks,
            'showSecureBanner' => $showSecureBanner,
            'showRequestBanner' => $showRequestBanner ?? false,
        ]) ?>
    </aside>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => 'page-list',
        'layout' => "{items}{pager}",
        'options' => [
            'tag' => 'div',
            'class' => 'cat-middle-section',
            'data-scroll-container' => true,
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'cat-middle-block',
            'data-scroll-item' => true
        ],
        'pager' => [
            'class' => ScrollPager::class,
            'loadUrl' => Url::current(['/category/article-more', 'category_id' => $category->id])
        ],
        'emptyText' =>
            '<p class="catalog-empty-text">' .
                Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => $entityTitle]) .
            '</p>'
    ]); ?>
    <div class="job-section">
        <?= ListView::widget([
            'dataProvider' => $jobsDataProvider,
            'id' => 'jobs-mobile',
            'itemView' => 'job-list-mobile',
            'layout' => "<div class='clearfix'>{items}</div>",
            'options' => [
                'class' => ''
            ],
            'itemOptions' => [
                'class' => 'mob-work-item'
            ],
            'emptyText' => ''
        ]); ?>
        <ins class="adsbygoogle"
             style="display:inline-block;width:100%;height:280px"
             data-ad-client="ca-pub-7280089675102373"
             data-ad-slot="8279148809">
        </ins>
    </div>
</div>