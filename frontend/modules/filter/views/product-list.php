<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.07.2017
 * Time: 14:30
 */

use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Product | ProductElastic */
/* @var $index integer */

$price = $model->contract_price ? '<span>' . Yii::t('app', 'contract') . '</span>' : $model->getPrice();
$type = $model->type;

$tariffIcon = $model->getActiveTariffIcon();

?>

<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img($model->getSellerThumb('catalog'),['alt' => $model->getSellerName(), 'title' => $model->getSellerName()]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'by') ?>
            <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['data-pjax' => 0, 'title' => $model->getSellerName()]) ?>
        </div>
    </div>
</div>
<a class="bf-alias" data-pjax="0" href="<?= Url::to(["/board/{$type}/view", 'alias' => $model->alias]) ?>">
    <div class="bf-img">
        <?= Html::img($model->getThumb('catalog'), [
            'alt' => Html::encode($model->getLabel()),
            'title' => Html::encode($model->getLabel()),
        ]) ?>
        <?= $tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : '' ?>
    </div>
    <div class="bf-descr">
        <?= Html::encode($model->getLabel(), false) ?>
    </div>
</a>
<div class="block-bottom">
    <?php if (!Yii::$app->user->isGuest && $type === Product::TYPE_PRODUCT) {
        echo Html::a('<i class="fa fa-heart" aria-hidden="true"></i>', null, [
                'class' => 'like hint--top toggle-favourite ' . ($model->isFavourite() ? 'active' : ''),
                'data-hint' => Yii::t('app', 'Favourite'),
                'data-hint-add' => Yii::t('app', 'Favourite'),
                'data-hint-remove' => Yii::t('app', 'Favourite'),
                'data-id' => $model->id,
                'data-type' => 'product'
            ]
        );
    } ?>
    <div class="markers">
        <div class="stick text-center new <?= ($model->isNew() ? 'active' : '') ?>"><?= Yii::t('app', 'New') ?></div>
    </div>
    <?php echo Html::a($price, ["/board/{$type}/view", 'alias' => $model->alias], ['class' => 'block-price', 'data-pjax' => 0]); ?>
</div>