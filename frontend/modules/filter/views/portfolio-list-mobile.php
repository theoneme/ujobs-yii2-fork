<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.09.2017
 * Time: 13:10
 */

use common\models\Job;
use yii\helpers\Html;

/* @var $model Job */
/* @var $index integer */

$tariffIcon = $model->getActiveTariffIcon();
$tariffTitle = $model->getActiveTariffTitle();

?>

<div class="mob-work-table">
    <?= Html::a(
        Html::img($model->getThumb('catalog')) . ($tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon']) : ''),
        ['/account/portfolio/view', 'id' => $model->id],
        ['class' => 'mob-work-img album-show', 'data-pjax' => 0]
    )?>

    <div class="mob-work-info">
        <?= Html::a(
            Html::encode($model->title),
            ['/account/portfolio/view', 'id' => $model->id],
            ['class' => 'bf-descr album-show', 'data-pjax' => 0, 'data-id' => $model->id]
        ) ?>

        <div class="mob-block-bottom">
            <div class="by">
                <?= Yii::t('app', 'by') ?>
                &nbsp;<?= Html::a(Html::encode($model->getSellerName()), ['/account/profile/show', 'id' => $model->user_id], ['data-pjax' => 0]) ?>
            </div>
        </div>
    </div>
</div>