<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.07.2017
 * Time: 14:29
 */

use common\models\Category;
use common\models\JobAttribute;
use common\modules\store\models\Currency;
use frontend\components\product\NewProducts;
use frontend\components\product\RecommendedProducts;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

/* @var $category Category */
/* @var $parentCategory Category|null */
/* @var $categories Category[] */
/* @var $relatedTags JobAttribute[] */
/* @var $tagAttribute JobAttribute */
/* @var $prices array */
/* @var $priceSliderPoints array */
/* @var $entityToRouteMatches array */
/* @var $attributeRemoveLinks array */
/* @var $paginationData array */
/* @var $sortData array */
/* @var $showPriceSlider bool */
/* @var $tagsFirst bool */
/* @var $filtersHtml string */
/* @var $metaKeywords string */
/* @var $categoryHeader string */
/* @var $request string */
/* @var $dataProvider ActiveDataProvider */
/* @var $type string */
/* @var $entityTitle string */
/* @var $breadcrumbs array */
/* @var $similarCategories null|array */
/* @var $extra string */
/* @var $showSecureBanner boolean */
/* @var $showRequestBanner boolean */
/* @var $currency Currency */

?>

<?= $this->render('top-filter-section', [
    'type' => $type,
    'entitySwitcher' => true,
    'attributeRemoveLinks' => $attributeRemoveLinks,
    'sortData' => $sortData,
    'paginationData' => $paginationData,
    'request' => $request
]) ?>

    <aside>
        <?= $this->render('left-filter-section', [
            'category' => $category,
            'categories' => $categories,
            'categoryHeader' => $categoryHeader,
            'prices' => $prices,
            'showPriceSlider' => $showPriceSlider,
            'priceSliderPoints' => $priceSliderPoints,
            'filtersHtml' => $filtersHtml,
            'attributeRemoveLinks' => $attributeRemoveLinks,
            'showSecureBanner' => $showSecureBanner,
            'currency' => $currency,
            'showRequestBanner' => $showRequestBanner ?? false,
        ]) ?>
    </aside>

    <div class="cat-list">
        <?php if ($extra !== null) { ?>
            <?= $extra ?>
        <?php } ?>

        <?php echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'product-list',
            'layout' => "<div class='block-flex'>{items}</div>{pager}",
            'options' => [
                'tag' => 'div',
                'class' => ''
            ],
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'bf-item'
            ],
            'emptyText' =>
                '<p class="catalog-empty-text">' .
                Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => $entityTitle]) .
                '</p>' .
                ($dataProvider->totalCount ? '' : NewProducts::widget(['entity' => $type])),
        ]);

        if ($dataProvider->totalCount <= 8) {
            echo RecommendedProducts::widget([
                'category' => $parentCategory,
                'similarCategories' => $similarCategories,
                'entity' => $type,
                'sliderCount' => 5
            ]);
        } ?>
    </div>