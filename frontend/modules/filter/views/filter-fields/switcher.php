<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.10.2017
 * Time: 14:44
 */

use frontend\modules\elasticfilter\components\BaseFilter;
use yii\helpers\Html;

/** @var $item array */
/** @var $attribute_id integer */
/** @var $filter_type string */

?>

<div class="filter-fair-play">
    <div class="filter-fair-play-text">
        <?= Yii::t('app', 'Show Fair Play first') ?>
        <?= Html::a(Yii::t('app', 'Learn more'), ['/page/static', 'alias' => 'fair-play'], ['class' => 'link-blue', 'target' => '_blank']) ?>
    </div>
    <div class="onoffswitch field-myonoffswitch">
        <?= Html::tag('input', null, [
            'id' => $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_myonoffswitch" : "attr_myonoffswitch",
            'class' => 'onoffswitch-checkbox',
            'checked' => $item['checked'] === 'with_secure' ? 'checked' : false,
            'type' => 'checkbox',
            'value' => 'with_secure',
            'name' => $item['alias']
        ]); ?>
        <label class="onoffswitch-label" for="<?= $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_myonoffswitch" : "attr_myonoffswitch"?>">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
</div>
