<?php

use yii\helpers\Html;

/* @var $item array */
/* @var $attribute_id integer */

$module = Yii::$app->controller->module;
if ($module !== null && $module->id === 'board') {
    $route = "/board/category/tag";
} else {
    $route = "/category/{$item['alias']}";
}

?>
<?php if (!empty($item['values'])) { ?>
    <div class="aside-block">
        <div class="fi-title"><?= $item['title'] ?></div>
        <div class="tags-block">
            <?php foreach ($item['values'] as $k => $v) {
                if ($v['count'] > 0) {
                    $paramName = in_array($item['alias'], ['specialty-tender', 'specialty-pro']) ? 'specialty' : $item['alias'];
                    echo Html::a($v['title'] . (isset($v['count']) ? " <span class='grey'>(" . $v['count'] . ")</span>" : ''),
                        [$route, $paramName => $k],
                        ['class' => 'tag-but', 'data-pjax' => 0]
                    );
                }
            } ?>
        </div>
        <?php if (count($item['values']) > 5) { ?>
            <?= Html::a(Yii::t('filter', 'Show all'), '#', ['class' => 'show-all']); ?>
        <?php } ?>
    </div>
<?php } ?>