<?php

use frontend\modules\elasticfilter\components\BaseFilter;
use yii\helpers\Html;

/** @var $item array */
/** @var $attribute_id integer */
/** @var $filter_type string */

?>

<?php if (!empty($item['values'])) { ?>
    <div class="aside-block">
        <div class="fi-title"><?= $item['title'] ?></div>
        <?php foreach ($item['values'] as $k => $v) { ?>
            <div class="chover">
                <?= Html::tag('input', null, [
                    'id' => $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_{$attribute_id}_{$k}" : "attr_{$attribute_id}_{$k}",
                    'checked' => in_array($k, (array)$item['checked']),
                    'type' => 'radio',
                    'value' => $k,
                    'name' => "{$item['alias']}[]"
                ]); ?>
                <label for="<?= $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_{$attribute_id}_{$k}" : "attr_{$attribute_id}_{$k}" ?>">
                    <span class="ctext"><?= $v['title'] ?></span>
                    <?php if (isset($v['count'])) { ?>
                        <span class="item-count">(<?= $v['count'] ?>)</span>
                    <?php } ?>
                </label>
            </div>
        <?php } ?>
    </div>
<?php } ?>
