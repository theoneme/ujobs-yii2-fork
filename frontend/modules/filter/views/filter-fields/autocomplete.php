<?php

use frontend\modules\elasticfilter\components\BaseFilter;
use yii\web\View;

/** @var $item array */
/** @var $attribute_id integer */
/** @var $this View */
/** @var $filter_type string */
$id = $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_{$item['alias']}" : "attr_{$item['alias']}";
?>
<div class="aside-block selectize-input-container">
    <label for="<?= $id ?>" class="fi-title"><?= $item['title'] ?></label>
    <select id="<?= $id ?>" name="<?= $item['alias'] ?>" data-alias="<?= $item['alias'] ?>">
        <option value=""><?= Yii::t('app', 'Start typing «{attribute}»', ['attribute' => mb_strtolower($item['title'])])?></option>
        <?php foreach ($item['values'] as $key => $value) { ?>
            <option <?= $key == $item['checked'] ? 'selected' : '' ?> value="<?= $key ?>">
                <?= $value['title'] ?>
            </option>
        <?php } ?>
    </select>
</div>