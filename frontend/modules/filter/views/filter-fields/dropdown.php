<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.09.2016
 * Time: 16:33
 */

/* @var $item array */
/* @var $attribute_id integer */
?>

<?php if (!empty($item['values'])) { ?>
    <div class="aside-block">
        <label for="filter_<?= $item['alias'] ?>" class="fi-title"><?= $item['title'] ?></label>
        <select id="filter_<?= $item['alias'] ?>" class="form-control" name="<?= $item['alias'] ?>">
            <option value=""> --</option>
            <?php foreach ($item['values'] as $key => $value) { ?>
                <option <?= $key == $item['checked'] ? 'selected' : '' ?> value="<?= $key ?>">
                    <?= $value['title'] ?>
                    <?php if (isset($value['count'])) { ?>
                        (<?= $value['count'] ?>)
                    <?php } ?>
                </option>
            <?php } ?>
        </select>
    </div>
<?php } ?>