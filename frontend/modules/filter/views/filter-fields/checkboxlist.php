<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.09.2016
 * Time: 16:33
 */
use frontend\modules\elasticfilter\components\BaseFilter;
use yii\helpers\Html;

/** @var integer $attribute_id */
/** @var array $item */
/** @var integer $filter_type */

?>
<?php if (!empty($item['values'])) { ?>
    <div class="aside-block">
        <div class="fi-title"><?= $item['title'] ?></div>
        <div class="ml hide-filter">
            <?php foreach ($item['values'] as $k => $v) { ?>
                <div class="chover">
                    <?= Html::tag('input', null, [
                        'id' => $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_{$attribute_id}_{$k}" : "attr_{$attribute_id}_{$k}",
                        'checked' => in_array($k, (array)$item['checked']),
                        'type' => 'checkbox',
                        'value' => $k,
                        'name' => "{$item['alias']}[]"
                    ]); ?>
                    <label for="<?= $filter_type === BaseFilter::FILTER_TYPE_MOBILE ? "mobile_attr_{$attribute_id}_{$k}" : "attr_{$attribute_id}_{$k}" ?>">
                        <span class="ctext"><?= $v['title'] ?></span>
                        <?php if (isset($v['count'])) { ?>
                            <span class="item-count">(<?= $v['count'] ?>)</span>
                        <?php } ?>
                    </label>
                </div>
            <?php } ?>
        </div>

        <?php if (count($item['values']) > 4) { ?>
            <a class="more-less watch-more" href="#"><?= Yii::t('filter', 'See more') ?></a>
        <?php } ?>
    </div>
<?php } ?>