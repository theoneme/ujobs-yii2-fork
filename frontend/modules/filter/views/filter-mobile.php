<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 16:53
 */

use common\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;

/* @var $category Category */
/* @var $categories Category[] */
/* @var $attributeRemoveLinks array */
/* @var $filtersHtml mixed */
/* @var $showPriceSlider boolean */
/* @var $entityToRouteMatches array */
/* @var $metaKeywords string */
/* @var $showCategorySelect boolean */
/* @var $priceSliderPoints array */
/* @var $showMobileFilter boolean */

?>

<?php if ($showMobileFilter) { ?>
    <div class="filters-mobile">
        <?php if ($attributeRemoveLinks) { ?>
            <div class="checked-filter clearfix">
                <?php foreach ($attributeRemoveLinks as $key => $attribute) { ?>
                    <?php foreach ($attribute['values'] as $value) { ?>
                        <?= Html::a("<i class='fa fa-times' aria-hidden='true'></i>{$value['value']}", $value['url']) ?>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="text-right">
            <?php if ($attributeRemoveLinks) { ?>
                <?= Html::a(Yii::t('app', 'Clear All'), Url::canonical()); ?>
            <?php } ?>
        </div>
        <div class="subcats-mobile-block visible-xs">
            <div class="text-chose-subc text-left visible-xs "><?= Yii::t('app', 'Choose Subcategory') ?></div>
            <?php foreach ($categories as $group => $items) { ?>
                <div class="ml hide-filter">
                    <?php foreach ($items as $key => $category_item) { ?>
                        <?= Html::tag($category_item['options']['tag'], $category_item['label'] . ($category_item['count'] ? " ({$category_item['count']})" : ''), $category_item['linkOptions']) ?>
                    <?php } ?>
                </div>
                <?php if (count($items) > 4) {
                    echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($items) - 4)]), null, [
                        'class' => 'more-less more-cats',
                        'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($items) - 4)]),
                        'data-less' => Yii::t('app', 'See less'),
                    ]) ?>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="show-hide-filters">
            <?= Html::beginForm('', 'post', [
                'id' => 'filter_form_mobile',
            ]) ?>
            <div id="mobile-filters">
                <?= $filtersHtml ?>
            </div>
            <?php if ($showPriceSlider) { ?>
                <?= Html::hiddenInput('price', isset(Yii::$app->request->queryParams['price']) ? $priceSliderPoints['min'] . '-' . $priceSliderPoints['max'] : '', ['id' => 'price-holder-mobile']) ?>
                <div class="aside-block">
                    <div class="fi-title"><?= Yii::t('filter', 'Price') ?></div>
                    <div class="flex mobile-price">
                        <div class="price-label-wrap">
                            <input class="price-label text-center" id="min-price-mobile" value="<?= $priceSliderPoints['min'] ?>">
                        </div>
                        <div class="price-label-wrap">
                            <input class="price-label text-center" id="max-price-mobile" value="<?= $priceSliderPoints['max'] ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="slider-price-mobile"></div>
                    </div>
                </div>
            <?php } ?>
            <?= Html::endForm() ?>
        </div>
        <div class="filter-buttons">
            <a class="btn-middle more-filters" href=""><?= Yii::t('filter', 'More filters') ?></a>
        </div>
        <a class="btn-middle hide-filters" href=""><?= Yii::t('filter', 'Hide filters') ?></a>
    </div>
<?php } ?>
    <div class="select-subcat">
        <?php echo ListView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'jobs-mobile',
            'itemView' => 'job-list-mobile',
            'layout' => "<div class='clearfix'>{items}</div>{pager}",
            'options' => [
                'class' => ''
            ],
            'itemOptions' => [
                'class' => 'mob-work-item'
            ],
            'pager' => [
                'nextPageLabel' => '→',
                'prevPageLabel' => '←',
                'maxButtonCount' => 5,
            ],
            'emptyText' =>
                '<p class="catalog-empty-text">' .
                Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => $entityTitle]) .
                '</p>',
        ]); ?>
    </div>

<?php $script = <<<JS
    $('select#category-mobile').change(function() {
        let url = $(this).find('option:selected').attr('data-href');
        $.pjax.reload({container: '#catalog-pjax', url: url})
    });

    function processMobileFilterForms(object) {
        let inputs = $('#filter_form_mobile input:not(.price-label):not([name=_csrf-frontend]):not([value=\'\']), #top_filter_form input:not([name=_csrf-frontend]):not([value=\'\'])'), 
            dropdowns = $('#filter_form_mobile select');

        $.each(dropdowns, function(index, value) {
            if($(this).find('option:selected') && $(this).find('option:selected').val()) {
                inputs.push($(this)[0]);
            }
        }).promise().done(function() {
            let url = inputs.serialize();
            applyFilter(url);
        });
    }

    $('#mobile-filters input[type=radio], #mobile-filters input[type=checkbox], #mobile-filters select').change(function() {console.log('asdf');
        processMobileFilterForms($(this));
    });

    let priceSliderMobile = document.getElementById('slider-price-mobile'),
    	inputMobs = [document.getElementById('min-price-mobile'), document.getElementById('max-price-mobile')];

	if(priceSliderMobile !== null) {
	    initPriceSlider(priceSliderMobile, inputMobs);
	}
JS;

$this->registerJs($script, View::POS_READY);
?>