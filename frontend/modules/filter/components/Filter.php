<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.08.2016
 * Time: 11:30
 */

namespace frontend\modules\filter\components;

use yii\base\Widget;
use common\models\Category;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use frontend\modules\filter\models\Filter as FilterModel;

class Filter extends BaseFilter
{
    public $showMobileFilter;
    public $sortData;
    public $paginationData;
    public $prices;
    public $priceSliderPoints;
    public $attributeRemoveLinks;
    public $categories;
    public $attributeFilters;
    public $filtersHtml;
    public $dataProvider;

    public function init()
    {
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'requestType' => $this->requestType,
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
        ]);

        $this->request = $this->category->translation->title;
        $this->requestType = $this->category->lvl == 0 ? 'root_category' : 'category';

        $this->model->lightApplyFiltersToCatalog();
        $this->model->applyFiltersToCatalog();

        $this->showMobileFilter = $this->model->showMobileFilter();

        $this->sortData = $this->model->getSortData();
        $this->paginationData = $this->model->getPaginationData();
        $this->prices = $this->model->getPriceRange();
        $this->priceSliderPoints = $this->model->getSliderPoints();
        $this->attributeRemoveLinks = $this->model->buildRemoveFilterLinks();
        $this->categories = $this->model->buildCategoriesMenu();
        $this->attributeFilters = $this->model->buildAttributeFiltersConfig();
        $this->filtersHtml = $this->buildFiltersHtml($this->attributeFilters);

        if (isset($this->prices['min']) && isset($this->prices['max']) && $this->prices['min'] != $this->prices['max']) {
            $this->showPriceSlider = true;
        }

        $query = clone ($this->model->query);
        $query->select('job.*');
        $query->orderBy = null;

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 20,
            ],

            'totalCount' => $query->count()
        ]);
    }

    public function run()
    {
        return $this->render("../../views/{$this->template}", [
            'categories' => $this->categories,
            'category' => $this->category,
            'dataProvider' => $this->dataProvider,

            'categoryHeader' => $this->model->getCategoryHeader(),
            'prices' => $this->prices,
            'priceSliderPoints' => $this->priceSliderPoints,
            'showPriceSlider' => $this->showPriceSlider,
            'showMobileFilter' => $this->showMobileFilter,
            'sortData' => $this->sortData,
            'paginationData' => $this->paginationData,

            'filtersHtml' => $this->filtersHtml,
            'attributeRemoveLinks' => $this->attributeRemoveLinks,

            'type' => $this->type,
        ]);
    }
}