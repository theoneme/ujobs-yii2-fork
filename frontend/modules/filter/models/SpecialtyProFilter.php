<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.03.2017
 * Time: 17:19
 */

namespace frontend\modules\filter\models;

use common\models\Category;
use common\models\Job;
use common\models\UserAttribute;
use Yii;

/**
 * Class SpecialtyProFilter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class SpecialtyProFilter extends SkillFilter
{

    /**
     * @var string
     */
    private $_entityTitle = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['translation', 'jobs', 'jobs.user'])
                ->andWhere(['job.type' => 'job', 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['exists',
                    UserAttribute::find()
                        ->where('user.id = user_attribute.user_id')
                        ->andWhere([
                            'user_attribute.entity' => 'attribute',
                            'user_attribute.entity_content' => 41,
                            'user_attribute.value_alias' => $this->request
                        ])
                ])
                ->orderBy('sort_order asc')
                ->groupBy('category.id')
                ->select('category.id')
                ->limit(7)
                ->column();
        }
        return $this->_similarCategories;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return ['specialty'];
    }

    /**
     * @return string
     */
    public function getEntityTitle(){
        if ($this->_entityTitle === null) {
            /* @var $userSpecialty UserAttribute */
            $userSpecialty = UserAttribute::find()
                ->joinWith('attrValueDescription')
                ->where([
                    'user_attribute.entity' => 'attribute',
                    'user_attribute.entity_content' => 41,
                    'user_attribute.value_alias' => $this->request
                ])->one();
            $this->_entityTitle = $userSpecialty->attrValueDescription->title;
        }
        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with specialty "{specialty}"', ['specialty' => $this->entityTitle]);
    }
}