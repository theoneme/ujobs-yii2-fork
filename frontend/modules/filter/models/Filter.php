<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\filter\models;

use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Setting;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class Filter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class Filter extends Model
{
    /**
     * @var ActiveQuery
     */
    public $query = null;

    /**
     * @var ActiveQuery
     */
    public $lightQuery = null;

    /**
     * @var null
     */
    public $requestType = null;

    /**
     * @var null
     */
    public $request = null;

    /**
     * @var string
     */
    public $type = null;

    /**
     * @var Category
     */
    public $category = null;

    /**
     * @var string
     */
    private $_entityTitle = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'query' => [['query', 'lightQuery'], 'safe'],
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');


        $queryParams = Yii::$app->request->queryParams;
        $priceParam = ArrayHelper::remove($queryParams, 'price');
        if ($priceParam) {
            $priceParts = explode('-', $priceParam);
            $this->query->andWhere(['between', 'job.price', $priceParts[0], $priceParts[1]]);
        }

        $sortByParam = explode(';', ArrayHelper::remove($queryParams, 'sort_by'));
        if ($this->query->modelClass == Job::class) {
            if ($sortByParam && in_array($sortByParam[0], ['price', 'created_at', 'rating'])) {
                $this->query->addOrderBy('job.' . $sortByParam[0] . ' ' . (isset($sortByParam[1]) && in_array($sortByParam[1], ['asc', 'desc']) ? $sortByParam[1] : 'desc'));
            } else {
                if ($this->requestType !== null) {
                    $this->query = $this->applyRelevancy($this->query);
                } else {
                    $this->query->addOrderBy("job.created_at DESC");
                }
            }
        }

        foreach ($queryParams as $key => $queryParam) {
            if (isset($cachedAttributes[$key])) {
                $condition = [
                    'ja.entity_alias' => $key
                ];
                if ($queryParam[0] == 1) {
                    $condition['ja.value'] = $queryParam;
                } else {
                    $condition['ja.value_alias'] = $queryParam;
                }

                $subQuery = (new \yii\db\Query)
                    ->select('*')
                    ->from('job_attribute ja')
                    ->where('ja.job_id = job.id')
                    ->andWhere($condition);
                $this->query->andFilterWhere(['exists', $subQuery]);
            }
        }

        return true;
    }

    /**
     * @param array $exception
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        $this->lightQuery->joinWith = null;
        $this->lightQuery->joinWith(['extra']);
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;
        ArrayHelper::remove($queryParams, $exception);

        foreach ($queryParams as $key => $queryParam) {
            if (isset($cachedAttributes[$key])) {
                $condition = [
                    'ja.entity_alias' => $key
                ];
                if ($queryParam[0] == 1) {
                    $condition['ja.value'] = $queryParam;
                } else {
                    $condition['ja.value_alias'] = $queryParam;
                }

                $subQuery = (new \yii\db\Query)
                    ->select('*')
                    ->from('job_attribute ja')
                    ->where('ja.job_id = job.id')
                    ->andWhere($condition);
                $this->lightQuery->andFilterWhere(['exists', $subQuery]);
            }
        }
        return true;
    }

    /**
     * @param ActiveQuery $query
     * @return null
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;

        foreach ($queryParams as $key => $queryParam) {
            if (isset($cachedAttributes[$key])) {
                $condition = [
                    'ja.entity_alias' => $key
                ];
                if ($queryParam[0] == 1) {
                    $condition['ja.value'] = $queryParam;
                } else {
                    $condition['ja.value_alias'] = $queryParam;
                }

                $subQuery = (new \yii\db\Query)
                    ->select('*')
                    ->from('job_attribute ja')
                    ->where('ja.job_id = job.id')
                    ->andWhere($condition);
                $query->andFilterWhere(['exists', $subQuery]);
            }
        }
        return $query;
    }

    /**
     * @return array
     */
    public function getPriceRange()
    {
        $result = [];

        switch ($this->type) {
            case 'job':
            case 'tender':
                $query = clone $this->lightQuery;
                $query->joinWith = [];

                $cheapest = $query->orderBy('price asc')->one();
                $mostExpensive = $query->orderBy('price desc')->one();

                if ($cheapest) {
                    $result['min'] = $cheapest['price'];
                }

                if ($mostExpensive) {
                    $result['max'] = $mostExpensive['price'];
                }
                break;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getSliderPoints()
    {
        $result = [];

        switch ($this->type) {
            case 'job':
            case 'tender':
                if ($priceParam = Yii::$app->request->get('price')) {
                    $priceParts = explode('-', $priceParam);

                    $result['min'] = (int)$priceParts[0];
                    $result['max'] = (int)$priceParts[1];
                } else {
                    $result = $this->getPriceRange();
                }
                break;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getSortData()
    {
        $queryParams = Yii::$app->request->queryParams;

        $sortByQueryParam = ArrayHelper::remove($queryParams, 'sort_by');

        $sortByParam = $sortByQueryParam ? $sortByQueryParam : 'relevance;desc';
        if (strpos($sortByParam, ';') === false) {
            $sortByParam .= ';desc';
        }

        $sortTitles = [
            'relevance;desc' => Yii::t('filter', 'Relevance'),
            'created_at;desc' => Yii::t('filter', 'Publish date'),
            'price;asc' => Yii::t('filter', 'Price ascending'),
            'price;desc' => Yii::t('filter', 'Price descending'),
            'rating;asc' => Yii::t('filter', 'Rating ascending'),
            'rating;desc' => Yii::t('filter', 'Rating descending'),
        ];

        $sort = [
            'sort_by' => [
                'value' => $sortByParam,
                'title' => $sortTitles[$sortByParam]
            ],
        ];

        return $sort;
    }

    /**
     * @return array
     */
    public function getPaginationData()
    {
        $queryParams = Yii::$app->request->queryParams;

        $pageQueryParam = ArrayHelper::remove($queryParams, 'page');
        $perPageQueryParam = ArrayHelper::remove($queryParams, 'per-page');

        $pageParam = $pageQueryParam ? $pageQueryParam : '';
        $perPageParam = $perPageQueryParam ? $perPageQueryParam : '';

        $pagination = [
            'per-page' => $perPageParam,
            'page' => $pageParam
        ];

        return $pagination;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];

        $enabledFilterAttributes = Setting::find()
            ->andFilterWhere(['like', 'key', 'filter_'])
            ->andWhere(['not', ['value' => 'hide']])
            ->asArray()
            ->all();

        foreach ($enabledFilterAttributes as $element) {
            $attributeIDs[preg_replace("/[^0-9]/", '', $element['key'])] = $element['value'];
        }

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $subQuery = (new Query)
            ->select("count('j2.id')")
            ->from('job j2')
            ->leftJoin('job_attribute ja2', 'j2.id = ja2.job_id')
            ->where('ja2.value_alias = job_attribute.value_alias and ja2.entity_alias = job_attribute.entity_alias')
            ->andWhere([
                'j2.type' => $this->type,
                'j2.status' => Job::STATUS_ACTIVE,
                'j2.category_id' => $childrenIds
            ])
            ->groupBy('ja2.value_alias');
        //$subQuery = $this->lightApplyFiltersToQuery($subQuery);

        $attributeValues = JobAttribute::find()
            ->select('job_attribute.*')
            ->joinWith(['job' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('id, category_id, user_id');
            }, 'attrValueDescription'], true)
            ->addSelect(['relatedJobsCount' => $subQuery])
            ->where([
                'job_attribute.attribute_id' => array_keys($attributeIDs),
                'job.category_id' => $childrenIds,
                'job.type' => $this->type,
                'job.status' => Job::STATUS_ACTIVE
            ])
            ->orderBy('relatedJobsCount desc')
            ->asArray()
            ->all();
        $attributes = $this->buildConfig($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categories = [];
        if ($this->category !== null) {
            if (($this->category->rgt - $this->category->lft) > 1) {
                $mainCategories = Category::find()
                    ->joinWith(['translation'])
                    ->where(['category.id' => $this->category->getChildrenIds()])
                    ->orderBy('sort_order asc')
                    ->all();
            } else {
                /* @var $parent Category */
                $parent = $this->category->parents(1)->one();
                $mainCategories = Category::find()
                    ->joinWith(['translation'])
                    ->where(['category.id' => $parent->getChildrenIds()])
                    ->all();
            }
        }
        $currentUrl = Url::current();
        if (!empty($mainCategories)) {
            foreach ($mainCategories as $mainCategory) {
                $item = [
                    'label' => $mainCategory->translation->title,
                    'url' => Url::to(["/category/{$this->type}s", "category_1" => $mainCategory->translation->slug]),
                    'options' => ['class' => 'list-group-item'],
                    'linkOptions' => ['class' => 'item-a-class'],
                ];

                if ($item['url'] == $currentUrl) {
                    $item['active'] = true;
                }

                $categories[] = $item;
            }
        }

        return $categories;
    }

    /**
     * @param integer $attributeId
     * @param string $type
     * @return array
     */
    public function getCheckedValues($attributeId = null, $type = null)
    {
        $result = [];

        if ($attributeId != null) {
            $queryParams = Yii::$app->request->queryParams;

            if (array_key_exists("at_{$attributeId}", $queryParams)) {
                $result = $queryParams["at_{$attributeId}"];
            }

            if (array_key_exists($attributeId, $queryParams)) {
                $result = $queryParams[$attributeId];
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function buildRemoveFilterLinks()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $route = "/{$controller}/{$action}";

        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;
        $config = [];

        $priceParam = ArrayHelper::remove($queryParams, 'price');
        if ($priceParam) {
            $tempQueryParams = $this->getRouteParamsWithoutKey($route, $queryParams, 'price');

            $config['price']['title'] = Yii::t('filter', 'Price');
            $config['price']['values'][] = [
                'url' => Url::to($tempQueryParams),
                'value' => Yii::t('filter', 'Price') . ": " . $priceParam
            ];
        }

        foreach ($queryParams as $key => $queryParam) {
            if (!in_array($key, $this->getRemoveAttributeLinkExceptions()) && isset($cachedAttributes[$key])) {
                $attribute = $cachedAttributes[$key];
                $config[$attribute['id']]['title'] = $attribute['translation']['title'];
                foreach ((array)$queryParam as $k => $p) {
                    if (count($queryParam) > 1) {
                        $tempQueryParams = $queryParams;
                        unset($tempQueryParams[$key][$k]);
                    } else {
                        $tempQueryParams = $this->getRouteParamsWithoutKey($route, $queryParams, $key);
                    }

                    $attributeValue = AttributeValue::find()->joinWith(['translation'])->where(['mod_attribute_value.alias' => $p])->asArray()->one();

                    $config[$attribute['id']]['values'][] = [
                        'url' => Url::to($tempQueryParams),
                        'value' => ($attributeValue['translation']['title']) ? $attributeValue['translation']['title'] : $attribute['translation']['title']
                    ];
                }
            }
        }
        return $config;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return [];
    }

    /**
     * @param string $route
     * @param array $queryParams
     * @param string $key
     * @return array
     */
    public function getRouteParamsWithoutKey($route, array $queryParams, $key)
    {
        $tempQueryParams = [];
        if (is_array($queryParams) && $key != null) {
            $tempQueryParams = $queryParams;
            unset($tempQueryParams[$key]);
            $tempQueryParams = array_merge((array)$route, $tempQueryParams);
        }

        return $tempQueryParams;
    }

    /**
     * @param array $attributeValues
     * @param array $attributeIDs
     * @return array
     */
    protected function buildConfig($attributeValues = [], $attributeIDs = [])
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');
        $attributes = [];
        if (!empty($attributeValues) && !empty($attributeIDs)) {
            foreach ($attributeValues as $key => $value) {
                $jobAttributeValue = $value['value_alias'] ? $value['value_alias'] : $value['value'];

                if (!isset($attributes[$value['entity_content']]['title'])) {
                    $attribute = $cachedAttributes[$value['entity_alias']];

                    $attributes[$value['entity_content']]['title'] = $attribute['translation']['title'];
                    $attributes[$value['entity_content']]['alias'] = $attribute['alias'] == 'specialty' ? 'specialty-tender' : $attribute['alias'];
                }

                if (!isset($attributes[$value['entity_content']]['type'])) {
                    $attributes[$value['entity_content']]['type'] = $attributeIDs[$value['entity_content']];
                }
                $attributes[$value['entity_content']]['checked'] = $this->getCheckedValues($value['entity_alias']);
                $attributes[$value['entity_content']]['values'][$jobAttributeValue] = [
                    'title' => !empty($value['attrValueDescription']['title']) ? $value['attrValueDescription']['title'] : $value['value'],
                    'count' => $value['relatedJobsCount'] ? $value['relatedJobsCount'] : 0
                ];
            }
        }

        return $attributes;
    }

    /**
     * @param ActiveQuery $query
     * @return mixed
     */
    private function applyRelevancy($query)
    {
        $params = Yii::$app->params[$this->requestType];
        $job_title_like = "(CASE WHEN ct.title like '%" . $this->request . "%' THEN " . $params['job_title_like'] . " ELSE 0 END)";
        $description_length = "(CASE WHEN CHAR_LENGTH(ct.content) > 100 THEN " . $params['description_length'] . " ELSE 0 END)";
        $avatar_exists = "(CASE WHEN (profile.gravatar_email IS NOT NULL) AND (TRIM(profile.gravatar_email) != '') THEN 0 ELSE " . $params['avatar_exists'] . " END)";
        $months_passed = "FLOOR((" . time() . " - job.created_at)/" . 60 * 60 * 24 * 30 . ")";
        $jobs_exists = "
                        (CASE WHEN EXISTS (
                                SELECT j3.id FROM job j3
                                WHERE j3.user_id = job.user_id
                                    AND j3.id != job.id
                                    AND j3.type = '" . $this->type . "'
                                    AND j3.status = " . Job::STATUS_ACTIVE . "
                            )
                            THEN " . $params['jobs_exists'] . "
                            ELSE 0
                            END
                        )
                    ";
        $package_title_like = "
                        (CASE WHEN EXISTS (
                                SELECT jp3.id FROM job_package jp3
                                WHERE jp3.job_id = job.id
                                    AND jp3.title like '%" . $this->request . "%'
                            )
                            THEN " . $params['package_title_like'] . "
                            ELSE 0
                            END
                        )
                    ";
        $description_like = "
                        (CASE WHEN ct.content like '%" . $this->request . "%'
                            OR EXISTS (
                                SELECT jp4.id FROM job_package jp4
                                WHERE jp4.job_id = job.id
                                    AND jp4.description like '%" . $this->request . "%'
                            )
                            THEN " . $params['description_like'] . "
                            ELSE 0
                            END
                        )
                    ";
        $more_packages = "
                        (CASE WHEN (SELECT COUNT(jp5.id) FROM job_package jp5 WHERE jp5.job_id = job.id) > 1
                            THEN " . $params['more_packages'] . "
                            ELSE 0
                            END
                        )
                    ";
        $query->addSelect(new Expression("
                        (" .
            $job_title_like .
            " + " . $package_title_like .
            " + " . $description_like .
            " + " . $jobs_exists .
            " + " . $description_length .
            " + " . $more_packages .
            " + " . $avatar_exists .
            " - " . $months_passed .
            ") AS relevance
                    "));
        $query->addOrderBy("relevance DESC");

        return $query;
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        return $this->_similarCategories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            $this->_entityTitle = $this->category->translation->title;
        }
        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'All in') . ' ' . $this->entityTitle;
    }

    /**
     * @return bool
     */
    public function showMobileFilter()
    {
        return ($this->category->rgt - $this->category->lft == 1);
    }
}