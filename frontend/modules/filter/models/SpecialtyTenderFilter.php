<?php

namespace frontend\modules\filter\models;

use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\modules\attribute\models\Attribute;
use Yii;

/**
 * Class SpecialtyTenderFilter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class SpecialtyTenderFilter extends TagFilter
{
    /**
     * @var string
     */
    private $_entityTitle = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['translation', 'jobs'])
                ->andWhere(['job.type' => $this->type, 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['exists',
                    JobAttribute::find()
                        ->where('job.id = job_attribute.job_id')
                        ->andWhere([
                            'job_attribute.attribute_id' => 41,
                            'job_attribute.value_alias' => $this->request
                        ])
                ])
                ->orderBy('sort_order asc')
                ->select('category.id')
                ->column();
        }
        return $this->_similarCategories;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return ['specialty'];
    }

    /**
     * @return string
     */
    public function getEntityTitle(){
        if ($this->_entityTitle === null) {
            /* @var JobAttribute $specialty */
            $specialty = JobAttribute::find()
                ->where([
                    'job_attribute.attribute_id' => 41,
                    'job_attribute.value_alias' => $this->request
                ])->one();
            $this->_entityTitle = $specialty->value;
        }
        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with specialty "{specialty}"', ['specialty' => $this->entityTitle]);
    }

}