<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.03.2017
 * Time: 18:38
 */

namespace frontend\modules\filter\models;

use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Setting;
use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\models\user\Profile;
use common\models\user\User;
use yii\base\Model;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Filter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class FilterPro extends Model
{
    /**
     * @var ActiveQuery
     */
    public $query = null;

    /**
     * @var ActiveQuery
     */
    public $lightQuery = null;

    /**
     * @var null
     */
    public $requestType = null;

    /**
     * @var null
     */
    public $request = null;

    /**
     * @var string
     */
    public $type = null;

    /**
     * @var Category
     */
    public $category = null;

    /**
     * @var string
     */
    private $_entityTitle = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'query' => [['query', 'lightQuery'], 'safe'],
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;

        $sortByParam = explode(';', ArrayHelper::remove($queryParams, 'sort_by'));
        if ($this->query->modelClass == User::class) {
            if ($sortByParam && in_array($sortByParam[0], ['created_at', 'rating'])) {
                $this->query->orderBy($sortByParam[0] . ' ' . (isset($sortByParam[1]) && in_array($sortByParam[1], ['asc', 'desc']) ? $sortByParam[1] : 'desc'));
            } else {
                $this->query->orderBy("created_at DESC");
            }
        }

        foreach ($queryParams as $key => $queryParam) {
            if (isset($cachedAttributes[$key])) {
                $condition = [
                    'ua.entity_alias' => $key
                ];
                if ($queryParam[0] == 1) {
                    $condition['ua.value'] = $queryParam;
                } else {
                    $condition['ua.value_alias'] = $queryParam;
                }

                $subQuery = (new \yii\db\Query)
                    ->select('*')
                    ->from('user_attribute ua')
                    ->where('ua.user_id = user.id')
                    ->andWhere($condition);
                $this->query->andFilterWhere(['exists', $subQuery]);
            }
        }

        return true;
    }

    /**
     * @param array $exception
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        $this->lightQuery->joinWith = null;
        $this->lightQuery->joinWith(['extra']);
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;
        ArrayHelper::remove($queryParams, $exception);

        foreach ($queryParams as $key => $queryParam) {
            if (isset($cachedAttributes[$key])) {
                $condition = [
                    'ua.entity_alias' => $key
                ];
                if ($queryParam[0] == 1) {
                    $condition['ua.value'] = $queryParam;
                } else {
                    $condition['ua.value_alias'] = $queryParam;
                }

                $subQuery = (new \yii\db\Query)
                    ->select('*')
                    ->from('user_attribute ua')
                    ->where('ua.user_id = user.id')
                    ->andWhere($condition);
                $this->lightQuery->andFilterWhere(['exists', $subQuery]);
            }
        }
        return true;
    }

    /**
     * @param ActiveQuery $query
     * @return null
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;

        foreach ($queryParams as $key => $queryParam) {
            if (isset($cachedAttributes[$key])) {
                $condition = [
                    'ua.entity_alias' => $key
                ];
                if ($queryParam[0] == 1) {
                    $condition['ua.value'] = $queryParam;
                } else {
                    $condition['ua.value_alias'] = $queryParam;
                }

                $subQuery = (new \yii\db\Query)
                    ->select('*')
                    ->from('user_attribute ua')
                    ->where('ua.user_id = u2.id')
                    ->andWhere($condition);
                $query->andFilterWhere(['exists', $subQuery]);
            }
        }
        return $query;
    }

    /**
     * @return array
     */
    public function getSortData()
    {
        $queryParams = Yii::$app->request->queryParams;

        $sortByQueryParam = ArrayHelper::remove($queryParams, 'sort_by');

        $sortByParam = $sortByQueryParam ? $sortByQueryParam : 'created_at;desc';
        if (strpos($sortByParam, ';') === false) {
            $sortByParam .= ';desc';
        }

        $sortTitles = [
            'created_at;desc' => Yii::t('filter', 'Publish date'),
            'rating;asc' => Yii::t('filter', 'Rating ascending'),
            'rating;desc' => Yii::t('filter', 'Rating descending'),
        ];

        $sort = [
            'sort_by' => [
                'value' => $sortByParam,
                'title' => $sortTitles[$sortByParam]
            ],
        ];

        return $sort;
    }

    /**
     * @return array
     */
    public function getPaginationData()
    {
        $queryParams = Yii::$app->request->queryParams;

        $pageQueryParam = ArrayHelper::remove($queryParams, 'page');
        $perPageQueryParam = ArrayHelper::remove($queryParams, 'per-page');

        $pageParam = $pageQueryParam ? $pageQueryParam : '';
        $perPageParam = $perPageQueryParam ? $perPageQueryParam : '';

        $pagination = [
            'per-page' => $perPageParam,
            'page' => $pageParam
        ];

        return $pagination;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];

        $enabledFilterAttributes = Setting::find()
            ->andFilterWhere(['like', 'key', 'filter_'])
            ->andWhere(['not', ['value' => 'hide']])
            ->asArray()
            ->all();

        foreach ($enabledFilterAttributes as $element) {
            $attributeIDs[preg_replace("/[^0-9]/", '', $element['key'])] = $element['value'];
        }

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $attributeValues = UserAttribute::find()
            ->select('user_attribute.id, user_attribute.user_id, user_attribute.entity, user_attribute.entity_content, user_attribute.entity_alias, user_attribute.value, user_attribute.value_alias')
            ->joinWith(['user' => function($q) {
                /* @var ActiveQuery $q */
                $q->select('id');
            }, 'user.jobs' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('id, category_id, user_id, status');
            }, 'attrValueDescription'])
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                ->select("count('u2.id')")
                ->from('user u2')
                ->leftJoin('profile p', 'p.user_id = u2.id')
                ->leftJoin('user_attribute ua2', 'u2.id = ua2.user_id')
                ->where('ua2.value_alias = user_attribute.value_alias and ua2.entity_alias = user_attribute.entity_alias')
                ->andWhere(['p.status' => Profile::STATUS_ACTIVE])
                ->andWhere(['exists', Job::find()
                    ->select('j.id, j.user_id, j.category_id, j.status')
                    ->select('id, user_id, category_id, status')
                    ->from('job j')
                    ->where('j.user_id = u2.id')
                    ->andWhere([
                        'j.status' => Job::STATUS_ACTIVE,
                        'j.category_id' => $childrenIds
                    ])
                ])
                ->groupBy('ua2.value_alias')
            ])
            ->where(['user_attribute.entity' => 'attribute', 'user_attribute.entity_content' => array_keys($attributeIDs)])
            ->andWhere(['job.category_id' => $childrenIds, 'job.status' => Job::STATUS_ACTIVE])
            ->having(['>', 'relatedCount', 0])
            ->orderBy('relatedCount desc')
            ->groupBy('user_attribute.value_alias')
            ->asArray()
            ->all();

        $attributes = $this->buildConfig($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categories = [];

        if ($this->category !== null) {
            if (($this->category->rgt - $this->category->lft) > 1) {
                $mainCategories = Category::find()->joinWith(['translation'])->where(['category.id' => $this->category->getChildrenIds()])->orderBy('sort_order asc')->all();
            } else {
                /* @var $parent Category*/
                $parent = $this->category->parents(1)->one();
                $mainCategories = Category::find()->joinWith(['translation'])->where(['category.id' => $parent->getChildrenIds()])->all();
            }
        }
        $currentUrl = Url::current();
        if (!empty($mainCategories)) {
            foreach ($mainCategories as $mainCategory) {
                $item = [
                    'label' => $mainCategory->translation->title,
                    'url' => Url::to(["/category/{$this->type}s", "category_1" => $mainCategory->alias]),
                    'options' => ['class' => 'list-group-item'],
                    'linkOptions' => ['class' => 'item-a-class'],
                ];

                if ($item['url'] == $currentUrl) {
                    $item['active'] = true;
                }

                $categories[] = $item;
            }
        }

        return $categories;
    }

    /**
     * @param integer $attributeId
     * @param string $type
     * @return array
     */
    public function getCheckedValues($attributeId = null, $type = null)
    {
        $result = [];

        if ($attributeId != null) {
            $queryParams = Yii::$app->request->queryParams;

            if (array_key_exists("at_{$attributeId}", $queryParams)) {
                $result = $queryParams["at_{$attributeId}"];
            }

            if (array_key_exists($attributeId, $queryParams)) {
                $result = $queryParams[$attributeId];
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function buildRemoveFilterLinks()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $route = "/{$controller}/{$action}";

        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');

        $queryParams = Yii::$app->request->queryParams;
        $config = [];

        foreach ($queryParams as $key => $queryParam) {
            if (!in_array($key, $this->getRemoveAttributeLinkExceptions()) && isset($cachedAttributes[$key])) {
                $attribute = $cachedAttributes[$key];
                $config[$attribute['id']]['title'] = $attribute['translation']['title'];
                foreach ((array)$queryParam as $k => $p) {
                    if (count($queryParam) > 1) {
                        $tempQueryParams = $queryParams;
                        unset($tempQueryParams[$key][$k]);
                    } else {
                        $tempQueryParams = $this->getRouteParamsWithoutKey($route, $queryParams, $key);
                    }

                    $attributeValue = AttributeValue::find()->joinWith(['translation'])->where(['mod_attribute_value.alias' => $p])->asArray()->one();

                    $config[$attribute['id']]['values'][] = [
                        'url' => Url::to($tempQueryParams),
                        'value' => ($attributeValue['translation']['title']) ? $attributeValue['translation']['title'] : $attribute['translation']['title']
                    ];
                }
            }
        }
        return $config;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return [];
    }

    /**
     * @param string $route
     * @param array $queryParams
     * @param string $key
     * @return array
     */
    public function getRouteParamsWithoutKey($route, array $queryParams, $key)
    {
        $tempQueryParams = [];
        if (is_array($queryParams) && $key != null) {
            $tempQueryParams = $queryParams;
            unset($tempQueryParams[$key]);
            $tempQueryParams = array_merge((array)$route, $tempQueryParams);
        }

        return $tempQueryParams;
    }

    /**
     * @param array $attributeValues
     * @param array $attributeIDs
     * @return array
     */
    protected function buildConfig($attributeValues = [], $attributeIDs = [])
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $cachedAttributes = $cacheLayer->get('attributes');
        $attributes = [];
        if (!empty($attributeValues) && !empty($attributeIDs)) {
            foreach ($attributeValues as $key => $value) {
                if (!isset($attributes[$value['entity_content']]) || count($attributes[$value['entity_content']]['values']) < 20) {
                    $jobAttributeValue = $value['value_alias'] ? $value['value_alias'] : $value['value'];

                    if (!isset($attributes[$value['entity_content']]['title'])) {
                        $attribute = $cachedAttributes[$value['entity_alias']];

                        $attributes[$value['entity_content']]['title'] = $attribute['translation']['title'];
                        $attributes[$value['entity_content']]['alias'] = $attribute['alias'] == 'specialty' ? 'specialty-pro' : $attribute['alias'];
                    }

                    if (!isset($attributes[$value['entity_content']]['type'])) {
                        $attributes[$value['entity_content']]['type'] = $attributeIDs[$value['entity_content']];
                    }
                    $attributes[$value['entity_content']]['checked'] = $this->getCheckedValues($value['entity_alias']);
                    $attributes[$value['entity_content']]['values'][$jobAttributeValue] = [
                        'title' => !empty($value['attrValueDescription']['title']) ? $value['attrValueDescription']['title'] : $value['value'],
                        'count' => $value['relatedCount'] ? $value['relatedCount'] : 0
                    ];
                }
            }
        }
        return $attributes;
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        return $this->_similarCategories;
    }

    /**
     * @return string
     */
    public function getEntityTitle(){
        if ($this->_entityTitle === null) {
            $this->_entityTitle = $this->category->translation->title;
        }
        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader(){
        return Yii::t('filter', 'All in') . ' ' . $this->entityTitle;
    }

    /**
     * @return bool
     */
    public function showMobileFilter () {
        return ($this->category->rgt - $this->category->lft == 1);
    }
}