<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.03.2017
 * Time: 17:19
 */

namespace frontend\modules\filter\models;

use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Setting;
use common\models\UserAttribute;
use common\models\user\Profile;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Class SkillFilter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class SkillFilter extends FilterPro
{
    /**
     * @var string
     */
    private $_entityTitle = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];

        $enabledFilterAttributes = Setting::find()
            ->andFilterWhere(['like', 'key', 'filter_'])
            ->andWhere(['not', ['value' => 'hide']])
            ->asArray()
            ->all();

        foreach ($enabledFilterAttributes as $element) {
            $attributeIDs[preg_replace("/[^0-9]/", '', $element['key'])] = $element['value'];
        }

        $categories = $this->similarCategories;

        $subQuery = (new Query)
            ->select("count('ua2.user_id')")
            ->from('user_attribute ua2')
            ->where('ua2.value_alias = user_attribute.value_alias and ua2.entity_alias = user_attribute.entity_alias')
            ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
            ->groupBy(['ua2.value_alias']);
        $subQuery = $this->lightApplyFiltersToQuery($subQuery);

        $attributeValues = UserAttribute::find()
            ->select('user_attribute.id, user_attribute.user_id, user_attribute.entity, user_attribute.entity_content, user_attribute.entity_alias, user_attribute.value, user_attribute.value_alias')
            ->joinWith(['attrValueDescription', 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, status');
            }, 'user u2' => function ($q) {
                $q->select('u2.id');
            }])
            ->addSelect(['relatedCount' => $subQuery])
            ->where(['user_attribute.entity' => 'attribute', 'user_attribute.entity_content' => array_keys($attributeIDs)])
            ->orderBy('relatedCount desc')
            ->andWhere(['not', ['value_alias' => $this->request]])
            ->having(['>', 'relatedCount', 2])
            ->groupBy(['user_attribute.value_alias'])
            ->asArray()
            ->all();

        $attributes = $this->buildConfig($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return ['skill'];
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categories = [];

        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()->joinWith(['translation'])->where(['category.id' => $categoryIDs])->all();

        $currentUrl = Url::current();
        if (!empty($mainCategories)) {
            foreach ($mainCategories as $mainCategory) {
                $item = [
                    'label' => $mainCategory->translation->title,
                    'url' => Url::to(["/category/{$this->type}s", "category_1" => $mainCategory->alias]),
                    'options' => ['class' => 'list-group-item'],
                    'linkOptions' => ['class' => 'item-a-class'],
                ];

                if ($item['url'] == $currentUrl) {
                    $item['active'] = true;
                }

                $categories[] = $item;
            }
        }

        return $categories;
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['translation', 'jobs', 'jobs.user'])
                ->andWhere(['job.type' => 'job', 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['exists',
                    UserAttribute::find()
                        ->where('user.id = user_attribute.user_id')
                        ->andWhere([
                            'user_attribute.entity' => 'attribute',
                            'user_attribute.entity_content' => 38,
                            'user_attribute.value_alias' => $this->request
                        ])
                ])
                ->orderBy('sort_order asc')
                ->groupBy('category.id')
                ->select('category.id')
                ->limit(7)
                ->column();
        }
        return $this->_similarCategories;
    }

    /**
     * @return string
     */
    public function getEntityTitle(){
        if ($this->_entityTitle === null) {
            /* @var $userSkill UserAttribute */
            $userSkill = UserAttribute::find()
                ->joinWith('attrValueDescription')
                ->where([
                    'user_attribute.entity' => 'attribute',
                    'user_attribute.entity_content' => 38,
                    'user_attribute.value_alias' => $this->request
                ])->one();
            $this->_entityTitle = $userSkill->attrValueDescription->title;
        }
        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with skill "{skill}"', ['skill' => $this->entityTitle]);
    }

    /**
     * @return bool
     */
    public function showMobileFilter()
    {
        return true;
    }
}