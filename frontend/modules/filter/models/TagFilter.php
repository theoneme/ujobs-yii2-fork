<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\filter\models;

use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Setting;
use Yii;
use yii\db\Query;
use yii\helpers\Url;

/**
 * Class TagFilter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class TagFilter extends Filter
{
    /**
     * @var string
     */
    private $_entityTitle = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];

        $enabledFilterAttributes = Setting::find()
            ->andFilterWhere(['like', 'key', 'filter_'])
            ->andWhere(['not', ['value' => 'hide']])
            ->asArray()
            ->all();

        foreach ($enabledFilterAttributes as $element) {
            $attributeIDs[preg_replace("/[^0-9]/", '', $element['key'])] = $element['value'];
        }

        $categories = $this->similarCategories;

        $subQuery = (new \yii\db\Query)
            ->select("count('job.id')")
            ->from('job')
            ->leftJoin('job_attribute ja2', 'job.id = ja2.job_id')
            ->where('ja2.value_alias = job_attribute.value_alias and ja2.entity_alias = job_attribute.entity_alias')
            ->andWhere([
                'job.type' => $this->type,
                'job.status' => Job::STATUS_ACTIVE,
                'job.category_id' => $categories
            ])
            ->groupBy('ja2.value_alias');
        $subQuery = $this->lightApplyFiltersToQuery($subQuery);

        $attributeValues = JobAttribute::find()
            ->select('job_attribute.id, job_attribute.attribute_id, job_attribute.value, job_attribute.value_alias, job_attribute.entity_alias')
            ->joinWith(['attrValueDescription'])
            ->addSelect(['relatedJobsCount' => $subQuery])
            ->where([
                'job_attribute.attribute_id' => array_keys($attributeIDs),
            ])
            ->andWhere(['not', ['value_alias' => $this->request]])
            ->groupBy('job_attribute.value_alias')
            ->orderBy('relatedJobsCount desc')
            ->having(['>', 'relatedJobsCount', 0])
            ->asArray()
            ->all();

        $attributes = $this->buildConfig($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categories = [];

        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()->joinWith(['translation'])->where(['category.id' => $categoryIDs])->all();

        $currentUrl = Url::current();
        if (!empty($mainCategories)) {
            foreach ($mainCategories as $mainCategory) {
                $item = [
                    'label' => $mainCategory->translation->title,
                    'url' => Url::to(["/category/{$this->type}s", "category_1" => $mainCategory->alias]),
                    'options' => ['class' => 'list-group-item'],
                    'linkOptions' => ['class' => 'item-a-class'],
                ];

                if ($item['url'] == $currentUrl) {
                    $item['active'] = true;
                }

                $categories[] = $item;
            }
        }

        return $categories;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return ['tag'];
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['translation', 'jobs'])
                ->andWhere(['job.type' => $this->type, 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['exists',
                    JobAttribute::find()
                        ->where('job.id = job_attribute.job_id')
                        ->andWhere([
                            'job_attribute.entity_alias' => 'tag',
                            'job_attribute.value_alias' => $this->request
                        ])
                ])
                ->groupBy('category.id')
                ->orderBy('sort_order asc')
                ->select('category.id')
                ->limit(7)
                ->column();
        }
        return $this->_similarCategories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            /* @var JobAttribute $tag */
            $tag = JobAttribute::find()
                ->select('job_attribute.id, job_attribute.value, job_attribute.entity_alias, job_attribute.value_alias')
                ->where([
                    'job_attribute.entity_alias' => 'tag',
                    'job_attribute.value_alias' => $this->request
                ])->one();
            $this->_entityTitle = $tag->value;
        }
        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with tag "{tag}"', ['tag' => $this->entityTitle]);
    }

    /**
     * @return bool
     */
    public function showMobileFilter()
    {
        return true;
    }
}