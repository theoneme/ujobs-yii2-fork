<?php

namespace frontend\modules\skype\controllers;

use common\models\user\User;
use frontend\modules\skype\commands\StartCommand;
use frontend\modules\skype\services\SkypeFactory;
use SkypeBot\Entity\MessagePayload;
use SkypeBot\SkypeBot;
use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\helpers\VarDumper;

/**
 * Default controller for the `telegram` module
 */
class DefaultController extends Controller
{
	/**
	 * @var SkypeBot
	 */
	private $skype = null;

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		if ($action->id == 'hook') {
			$this->enableCsrfValidation = false;
		}

		if ($this->skype === null) {
			$this->skype = SkypeFactory::getInstance();
		}

		return parent::beforeAction($action);
	}

	/**
	 *
	 */
	public function actionHook()
	{
		try {
			$this->skype->getNotificationListener()->setMessageHandler(
				function ($payload) {
					/* @var MessagePayload $payload */
					$message = $payload->getText();
					$command = strstr($message, 'gzygom_') ? 'start' : $message;
					$containsHash = strstr($message, 'gzygom_') ? true : false;

					switch($command) {
						case 'start':
							$startCommand = new StartCommand($message, $containsHash, $payload, $this->skype);
							$startCommand->execute();
							break;
					}
				}
			);
			$this->skype->getNotificationListener()->dispatch();

			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	/**
	 * @param $code
	 * @return \yii\web\Response
	 */
	public function actionUnsubscribe($code)
	{
		if($code !== null) {
			$user = User::findOne(['skype_uid' => $code]);

			if($user !== null) {
				$user->skype_uid = null;
				$user->save();
			}
		}

		return $this->redirect(['/account/service/account-settings']);
	}
}