<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.05.2017
 * Time: 11:51
 */

namespace frontend\modules\skype\commands;
use common\models\user\User;
use frontend\modules\skype\services\SkypeFactory;
use SkypeBot\Command\SendMessage;
use SkypeBot\Entity\MessagePayload;
use SkypeBot\SkypeBot;
use Yii;
use yii\helpers\Url;

/**
 * Start command
 */
class StartCommand
{
	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var boolean
	 */
	private $containsHash = false;

	/**
	 * @var MessagePayload
	 */
	private $payload = null;

	/**
	 * @var SkypeBot
	 */
	private $skype = null;

	/**
	 * @param $message
	 * @param $containsHash
	 * @param $payload
	 */
	public function __construct($message, $containsHash, $payload, $skype)
	{
		$this->message = $message;
		$this->containsHash = $containsHash;
		$this->payload = $payload;
		$this->skype = $skype;
	}

	/**
	 *
	 */
	public function execute()
	{
		switch($this->containsHash) {
			case true:
				$user = User::findOne(['skype_hash' => $this->message]);
				if($user !== null) {
					$user->skype_uid = $this->payload->getConversation()->getId();
					$user->save();
				}

				$response = $this->skype->getApiClient()->call(
					new SendMessage(
						Yii::t('account', "You have successfully subscribed on uJobs notifications via skype"),
						$this->payload->getConversation()->getId()
					)
				);

				break;
			case false:
				$response = $this->skype->getApiClient()->call(
					new SendMessage(
						Yii::t('account', "You tried to subscribe to uJobs notifications via skype, but for this you need to provide an access code. You can get it on {page}", ['page' => Url::to(['/account/service/account-settings'])]),
						$this->payload->getConversation()->getId()
					)
				);

				break;
		}
	}
}