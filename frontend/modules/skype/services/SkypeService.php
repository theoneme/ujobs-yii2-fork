<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.11.2017
 * Time: 15:46
 */

namespace frontend\modules\skype\services;

use common\models\user\User;
use console\jobs\SkypeNotificationJob;
use SkypeBot\Command\SendMessage;
use SkypeBot\SkypeBot;
use Yii;

/**
 * Class SkypeService
 */
class SkypeService
{
    /**
     * @var User
     */
    private $receiver;

    /**
     * @var SkypeBot
     */
    private $skype;

    /**
     * SkypeService constructor.
     * @param User $receiver
     */
    public function __construct(User $receiver)
    {
        $this->receiver = $receiver;
        $this->skype = SkypeFactory::getInstance();
    }

    /**
     * @param array $params
     * @return bool
     */
    public function sendMessage(array $params)
    {
        $subject = array_key_exists('heading', $params) ? "{$params['heading']}: " : '';
        $text = "{$subject}{$params['content']}. " . Yii::t('notifications', 'View by link: {link}', ['link' => $params['link']], $this->receiver->site_language);

        if(Yii::$app->params['notificationQueue'] === true) {
            return Yii::$app->queue->push(new SkypeNotificationJob([
                'chatId' => $this->receiver->skype_uid,
                'text' => $text
            ]));
        } else {
            $response = $this->skype->getApiClient()->call(
                new SendMessage(
                    $text,
                    $this->receiver->skype_uid
                )
            );

            return true;
        }
    }
}