<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.05.2017
 * Time: 12:40
 */

namespace frontend\modules\skype\services;

use SkypeBot\Config;
use SkypeBot\SkypeBot;
use SkypeBot\Storage\FileStorage;
use Yii;

/**
 * Class SkypeFactory
 * @package frontend\modules\skype\services
 */
class SkypeFactory
{
    /**
     * @return SkypeBot
     */
	public static function getInstance()
	{
		$skypeId = Yii::$app->params['skypeId'];
		$skypeSecret = Yii::$app->params['skypeSecret'];

		$storage = new FileStorage(sys_get_temp_dir());
		$config = new Config(
			$skypeId,
			$skypeSecret
		);

		$skype = SkypeBot::init($config, $storage);

		return $skype;
	}
}