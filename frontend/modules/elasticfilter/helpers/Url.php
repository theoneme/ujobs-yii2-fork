<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.07.2017
 * Time: 13:55
 */

namespace frontend\modules\elasticfilter\helpers;

use Yii;
use yii\helpers\Url as BaseUrl;

/**
 * Class Url
 * @package frontend\modules\elasticfilter\helpers
 */
class Url extends BaseUrl
{
    /**
     * @param string $url
     * @param bool $scheme
     * @return mixed
     */
    public static function to($url = '', $scheme = false)
    {
        $result = parent::to($url, $scheme);

        return Yii::$app->utility->cureUrl($result);
    }
}