<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.08.2016
 * Time: 11:30
 */

namespace frontend\modules\elasticfilter\components;

use common\modules\store\models\Currency;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class Filter
 * @package frontend\modules\elasticfilter\components
 */
class FilterJob extends BaseFilter
{
    /**
     * @var bool
     */
    public $searchMode = false;

    public $showMobileFilter;
    public $sortData;
    public $paginationData;
    public $prices;
    public $priceSliderPoints;
    public $attributeRemoveLinks;
    public $categories;
    public $attributeFilters;
    public $filtersHtml;
    public $dataProvider;
    public $parentCategory;
    public $currency;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'requestType' => $this->requestType,
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
        ]);

        $this->request = ArrayHelper::getValue(Yii::$app->request->queryParams, 'request');
        $this->requestType = $this->category->lvl == 0 ? 'root_category' : 'category';

        $this->model->lightApplyFiltersToCatalog();
        $this->model->applyFiltersToCatalog();
        if (!empty($this->request) && $this->category->alias !== 'root' && $this->model->query->count() === 0) {
            Yii::$app->response->redirect(['/category/' . $this->type . '-catalog', 'request' => $this->request])->send();
        }

        $this->showMobileFilter = $this->model->showMobileFilter();
        $this->sortData = $this->model->getSortData();
        $this->paginationData = $this->model->getPaginationData();
        $this->prices = $this->model->getPriceRange();
        $this->priceSliderPoints = $this->model->getSliderPoints();
        $this->attributeRemoveLinks = $this->model->buildRemoveFilterLinks();
        $this->categories = $this->model->buildCategoriesMenu();
        $this->attributeFilters = $this->model->buildAttributeFiltersConfig();
        $this->breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($this->category, $this->model->entityTitle, "/category/{$this->type}s");
        $this->parentCategory = $this->category->lvl > 0 ? $this->category->parents(1)->one() : $this->category;

        $this->currency = Currency::getDb()->cache(function () {
            return Currency::find()->where(['code' => Yii::$app->params['app_currency_code']])->one();
        });

        if (isset($this->prices['min'], $this->prices['max']) && $this->prices['min'] !== $this->prices['max']) {
            $this->showPriceSlider = true;
        }

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        $this->filtersHtml = $this->buildFiltersHtml($this->attributeFilters);

        return $this->render("@frontend/modules/filter/views/{$this->template}", [
            'categories' => $this->categories,
            'category' => $this->category,
            'parentCategory' => $this->parentCategory,
            'dataProvider' => $this->dataProvider,

            'categoryHeader' => $this->model->getCategoryHeader(),
            'prices' => $this->prices,
            'priceSliderPoints' => $this->priceSliderPoints,
            'showPriceSlider' => $this->showPriceSlider,
            'showMobileFilter' => $this->showMobileFilter,
            'sortData' => $this->sortData,
            'paginationData' => $this->paginationData,
            'currency' => $this->currency,

            'filtersHtml' => $this->filtersHtml,
            'attributeRemoveLinks' => $this->attributeRemoveLinks,

            'type' => $this->type,
            'request' => $this->request,
            'entityTitle' => $this->model->entityTitle,
            'similarCategories' => $this->model->similarCategories,

            'searchMode' => $this->searchMode,
            'extra' => $this->extra,

            'showSecureBanner' => true,
            'showRequestBanner' => $this->type === 'job',
        ]);
    }
}