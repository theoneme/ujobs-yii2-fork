<?php

namespace frontend\modules\elasticfilter\components;

use common\models\Job;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class FilterPage
 * @package frontend\modules\elasticfilter\components
 */
class FilterPage extends BaseFilter
{
    /**
     * @var string
     */
    public $template = 'filter-page';

    /**
     * @var string
     */
    public $filterClass = 'frontend\modules\elasticfilter\models\FilterPage';

    /**
     * @var bool
     */
    public $searchMode = false;

    public $showMobileFilter;
    public $sortData;
    public $paginationData;
    public $attributeRemoveLinks;
    public $categories;
    public $attributeFilters;
    public $filtersHtml;
    public $dataProvider;
    public $jobsDataProvider;
    public $parentCategory;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
        ]);

        $this->request = ArrayHelper::getValue(Yii::$app->request->queryParams, 'request');

        $this->model->lightApplyFiltersToCatalog();
        $this->model->applyFiltersToCatalog();
        if (!empty($this->request) && $this->category->alias !== 'root' && $this->model->query->count() == 0) {
            Yii::$app->response->redirect(['/category/article-catalog', 'request' => $this->request])->send();
        }

        $this->showMobileFilter = $this->model->showMobileFilter();
        $this->sortData = $this->model->getSortData();
        $this->paginationData = $this->model->getPaginationData();
        $this->attributeRemoveLinks = $this->model->buildRemoveFilterLinks();
        $this->categories = $this->model->buildCategoriesMenu();
        $this->attributeFilters = $this->model->buildAttributeFiltersConfig();
        $this->breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($this->category, $this->model->entityTitle, "/category/articles");
        $this->parentCategory = $this->category->lvl > 0 ? $this->category->parents(1)->one() : $this->category;

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $categoryIds = null;
        if (!$this->category->isRoot()) {
            $categoryIds = $this->category->isLeaf() ? $this->category->id : $this->category->leaves()->select('id')->column();
        }

        $this->jobsDataProvider = new ActiveDataProvider([
            'query' => Job::find()
                ->joinWith(['translation', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->andFilterWhere([
                    'job.category_id' => $categoryIds,
                    'job.type' => Job::TYPE_JOB,
                    'job.status' => Job::STATUS_ACTIVE,
                    'job.locale' => Yii::$app->language
                ])
                ->orderBy('job.id desc')
                ->groupBy('job.id')
                ->limit(20),
            'pagination' => false,
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        $this->filtersHtml = $this->buildFiltersHtml($this->attributeFilters);

        return $this->render("@frontend/modules/filter/views/{$this->template}", [
            'categories' => $this->categories,
            'category' => $this->category,
            'parentCategory' => $this->parentCategory,
            'dataProvider' => $this->dataProvider,
            'jobsDataProvider' => $this->jobsDataProvider,

            'categoryHeader' => $this->model->getCategoryHeader(),
            'showPriceSlider' => false,
            'prices' => null,
            'priceSliderPoints' => null,
            'showMobileFilter' => $this->showMobileFilter,
            'sortData' => $this->sortData,
            'paginationData' => $this->paginationData,

            'filtersHtml' => $this->filtersHtml,
            'attributeRemoveLinks' => $this->attributeRemoveLinks,

            'type' => $this->type,
            'request' => $this->request,
            'entityTitle' => $this->model->entityTitle,
            'similarCategories' => $this->model->similarCategories,

            'searchMode' => $this->searchMode,

            'showSecureBanner' => true,
            'showRequestBanner' => false,
        ]);
    }
}