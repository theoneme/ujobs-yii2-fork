<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.08.2016
 * Time: 11:30
 */

namespace frontend\modules\elasticfilter\components;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class FilterPro
 * @package frontend\modules\elasticfilter\components
 */
class FilterPro extends BaseFilter
{
    /**
     * @var string
     */
    public $template = 'filter-pro';

    /**
     * @var string
     */
    public $filterClass = 'frontend\modules\elasticfilter\models\FilterPro';

    public $showMobileFilter;
    public $sortData;
    public $paginationData;
    public $prices;
    public $priceSliderPoints;
    public $attributeRemoveLinks;
    public $categories;
    public $attributeFilters;
    public $filtersHtml;
    public $dataProvider;
    public $parentCategory;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'requestType' => $this->requestType,
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
        ]);

        $this->request = ArrayHelper::getValue(Yii::$app->request->queryParams, 'request');
        $this->requestType = $this->category->lvl == 0 ? 'root_category' : 'category';

        $this->model->applyFiltersToCatalog();
        if (!empty($this->request) && $this->category->alias !== 'root' && $this->model->query->count() == 0) {
            Yii::$app->response->redirect(['/category/' . $this->type . '-catalog', 'request' => $this->request])->send();
        }
        $this->showMobileFilter = $this->model->showMobileFilter();

        $this->sortData = $this->model->getSortData();
        $this->paginationData = $this->model->getPaginationData();
        $this->attributeRemoveLinks = $this->model->buildRemoveFilterLinks();

        $this->categories = $this->model->buildCategoriesMenu();
        $this->attributeFilters = $this->model->buildAttributeFiltersConfig();
        $this->breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($this->category, $this->model->entityTitle, "/category/{$this->type}s");
        $this->parentCategory = $this->category->lvl > 0 ? $this->category->parents(1)->one() : $this->category;

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 25,
            ],
            //'totalCount' => $query->count()
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        $this->filtersHtml = $this->buildFiltersHtml($this->attributeFilters);

        return $this->render("@frontend/modules/filter/views/{$this->template}", [
            'categories' => $this->categories,
            'category' => $this->category,
            'parentCategory' => $this->parentCategory,
            'dataProvider' => $this->dataProvider,

            'categoryHeader' => $this->model->getCategoryHeader(),
            'showMobileFilter' => $this->showMobileFilter,
            'showPriceSlider' => $this->showPriceSlider,
            'sortData' => $this->sortData,
            'paginationData' => $this->paginationData,

            'filtersHtml' => $this->filtersHtml,
            'attributeRemoveLinks' => $this->attributeRemoveLinks,

            'type' => $this->type,
            'request' => $this->request,
            'entityTitle' => $this->model->entityTitle,
            'similarCategories' => $this->model->similarCategories,

            'showSecureBanner' => true,
            'showRequestBanner' => true,
        ]);
    }

    /**
     * @return array
     */
    public function getSelectedAttributeNames()
    {
        $selectedAttributes = $this->attributeRemoveLinks;
        unset($selectedAttributes['price']);
        return array_unique(
            array_reduce(
                array_map(function ($item) {
                    return array_map(function ($item2) {
                        return $item2['value'];
                    }, $item['values']);
                }, $selectedAttributes),
                'array_merge',
                []
            )
        );
    }
}