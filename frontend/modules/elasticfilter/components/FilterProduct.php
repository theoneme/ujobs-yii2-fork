<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.07.2017
 * Time: 13:18
 */

namespace frontend\modules\elasticfilter\components;

use common\modules\store\models\Currency;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class FilterProduct
 * @package frontend\modules\elasticfilter\components
 */
class FilterProduct extends BaseFilter
{
    /**
     * @var bool
     */
    public $searchMode = false;
    /**
     * @var string
     */
    public $template = 'filter-product';

    /**
     * @var string
     */
    public $filterClass = 'frontend\modules\elasticfilter\models\FilterProduct';

    public $showMobileFilter;
    public $sortData;
    public $paginationData;
    public $prices;
    public $priceSliderPoints;
    public $attributeRemoveLinks;
    public $categories;
    public $attributeFilters;
    public $filtersHtml;
    public $dataProvider;
    public $storeId;
    public $parentCategory;
    public $currency;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'requestType' => $this->requestType,
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
            'storeId' => $this->storeId
        ]);

        $this->request = ArrayHelper::getValue(Yii::$app->request->queryParams, 'request');
        $this->requestType = $this->category->lvl == 0 ? 'root_category' : 'category';

        $this->model->lightApplyFiltersToCatalog();
        $this->model->applyFiltersToCatalog();
        if (!empty($this->request) && $this->category->alias !== 'product_root' && $this->model->query->count() == 0) {
            Yii::$app->response->redirect(['/board/category/products', 'request' => $this->request])->send();
        }

        $this->showMobileFilter = $this->model->showMobileFilter();
        $this->sortData = $this->model->getSortData();
        $this->paginationData = $this->model->getPaginationData();
        $this->prices = $this->model->getPriceRange();
        $this->priceSliderPoints = $this->model->getSliderPoints();
        $this->attributeRemoveLinks = $this->model->buildRemoveFilterLinks();
        $this->categories = $this->model->buildCategoriesMenu();
        $this->attributeFilters = $this->model->buildAttributeFiltersConfig();

        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $route = "/board/{$controller}/{$action}";

        $this->breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($this->category, $this->model->entityTitle, $route, $this->storeId ? ['store_id' => $this->storeId] : []);
        $this->parentCategory = $this->category->lvl > 0 ? $this->category->parents(1)->one() : $this->category;

        $this->currency = Currency::getDb()->cache(function () {
            return Currency::find()->where(['code' => Yii::$app->params['app_currency_code']])->one();
        });

        if (isset($this->prices['min'], $this->prices['max']) && $this->prices['min'] != $this->prices['max']) {
            $this->showPriceSlider = true;
        }

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        $this->filtersHtml = $this->buildFiltersHtml($this->attributeFilters);

        return $this->render("@frontend/modules/filter/views/{$this->template}", [
            'categories' => $this->categories,
            'category' => $this->category,
            'parentCategory' => $this->parentCategory,
            'dataProvider' => $this->dataProvider,

            'categoryHeader' => $this->model->getCategoryHeader(),
            'prices' => $this->prices,
            'priceSliderPoints' => $this->priceSliderPoints,
            'showPriceSlider' => $this->showPriceSlider,
            'showMobileFilter' => $this->showMobileFilter,
            'sortData' => $this->sortData,
            'paginationData' => $this->paginationData,
            'currency' => $this->currency,

            'filtersHtml' => $this->filtersHtml,
            'attributeRemoveLinks' => $this->attributeRemoveLinks,

            'type' => $this->type,
            'request' => $this->request,
            'entityTitle' => $this->model->entityTitle,
            'similarCategories' => $this->model->similarCategories,

            'searchMode' => $this->searchMode,
            'extra' => $this->extra,

            'showSecureBanner' => false,
            'showRequestBanner' => false,
        ]);
    }
}