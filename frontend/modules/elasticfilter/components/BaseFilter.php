<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.03.2017
 * Time: 14:57
 */

namespace frontend\modules\elasticfilter\components;

use common\models\Category;
use frontend\modules\filter\models\Filter as FilterModel;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class BaseFilter
 * @package frontend\modules\elasticfilter\components
 */
class BaseFilter extends Widget
{
    const FILTER_TYPE_DEFAULT = 'default';
    const FILTER_TYPE_MOBILE = 'mobile';

    const TEMPLATE_DEFAULT = 'filter';
    const TEMPLATE_MOBILE = 'filter-mobile';

    /**
     * @var string
     */
    public $template = self::TEMPLATE_DEFAULT;

    /**
     * @var null
     */
    public $extra = null;
    /**
     * @var string
     */
    public $filterType = self::FILTER_TYPE_DEFAULT;
    /**
     * @var ActiveQuery
     */
    public $query = null;
    /**
     * @var string
     */
    public $type = 'job';
    /**
     * @var Category;
     */
    public $category = null;
    /**
     * @var string
     */
    public $request = null;
    /**
     * @var string
     */
    public $requestType = null;
    /**
     * @var bool
     */
    public $enableCategoryFilter = true;
    /**
     * @var bool
     */
    public $enableAttributeFilter = true;
    /**
     * @var bool
     */
    public $showPriceSlider = false;
    /**
     * @var string
     */
    public $metaKeywords = '';
    /**
     * @var array
     */
    public $breadcrumbs = [];
    /**
     * @var string
     */
    public $filterClass = 'frontend\modules\elasticfilter\models\FilterJob';
    /**
     * @var FilterModel
     */
    protected $model = null;

    /**
     * @return string
     */
    public function run()
    {
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'requestType' => $this->requestType,
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
        ]);

        $this->request = $this->category->translation->title;
        $this->requestType = $this->category->lvl == 0 ? 'root_category' : 'category';

        $this->model->lightApplyFiltersToCatalog();
        $this->model->applyFiltersToCatalog();

        $sortData = $this->model->getSortData();
        $paginationData = $this->model->getPaginationData();
        $prices = $this->model->getPriceRange();
        $priceSliderPoints = $this->model->getSliderPoints();
        $attributeRemoveLinks = $this->model->buildRemoveFilterLinks();
        $categories = $this->model->buildCategoriesMenu();
        $attributeFilters = $this->model->buildAttributeFiltersConfig();
        $filtersHtml = $this->buildFiltersHtml($attributeFilters);

        if (isset($prices['min'], $prices['max']) && $prices['min'] != $prices['max']) {
            $this->showPriceSlider = true;
        }

        $query = clone ($this->model->query);
        $query->select('job.*');
        $query->orderBy = null;

        $dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 24,
            ],

            'totalCount' => $query->count()
        ]);

        return $this->render("@frontend/modules/filter/views/{$this->template}", [
            'categories' => $categories,
            'category' => $this->category,
            'dataProvider' => $dataProvider,

            'prices' => $prices,
            'priceSliderPoints' => $priceSliderPoints,
            'showPriceSlider' => $this->showPriceSlider,
            'sortData' => $sortData,
            'paginationData' => $paginationData,

            'filtersHtml' => $filtersHtml,
            'attributeRemoveLinks' => $attributeRemoveLinks,

            'type' => $this->type,
        ]);
    }

    /**
     * @param array $config
     * @return string
     */
    public function buildFiltersHtml($config)
    {
        $output = '';

        if (is_array($config)) {
            foreach ($config as $key => $value) {
                if ($value['type']) {
                    $output .= $this->render("@frontend/modules/filter/views/filter-fields/{$value['type']}", [
                        'attribute_id' => $key,
                        'item' => $value,
                        'filter_type' => $this->filterType
                    ]);
                }
            }
        }

        return $output;
    }
}