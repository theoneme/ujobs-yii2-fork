<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.10.2017
 * Time: 17:08
 */

namespace frontend\modules\elasticfilter\components;

use common\modules\store\models\Currency;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SearchProduct
 * @package frontend\modules\elasticfilter\components
 */
class SearchProduct extends FilterProduct
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->request = ArrayHelper::getValue(Yii::$app->request->queryParams, 'request');
        $this->model = new $this->filterClass([
            'query' => $this->query,
            'lightQuery' => clone ($this->query),
            'requestType' => $this->requestType,
            'request' => $this->request,
            'type' => $this->type,
            'category' => $this->category,
        ]);

        $this->model->lightApplyFiltersToCatalog();
        $this->model->applyFiltersToCatalog();

        $this->showMobileFilter = $this->model->showMobileFilter();
        $this->sortData = $this->model->getSortData();
        $this->paginationData = $this->model->getPaginationData();
        $this->prices = $this->model->getPriceRange();
        $this->priceSliderPoints = $this->model->getSliderPoints();
        $this->attributeRemoveLinks = $this->model->buildRemoveFilterLinks();
        $this->categories = $this->model->buildCategoriesMenu();
        $this->attributeFilters = $this->model->buildAttributeFiltersConfig();
        $this->filtersHtml = $this->buildFiltersHtml($this->attributeFilters);

        $this->currency = Currency::getDb()->cache(function () {
            return Currency::find()->where(['code' => Yii::$app->params['app_currency_code']])->one();
        });

        if (isset($this->prices['min'], $this->prices['max']) && $this->prices['min'] != $this->prices['max']) {
            $this->showPriceSlider = true;
        }

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->model->query,
            'pagination' => [
                'pageSize' => 24,
            ],
        ]);
    }
}