<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.10.2017
 * Time: 11:45
 */

namespace frontend\modules\elasticfilter\widgets;

use Yii;
use yii\base\Widget;

class EntitySwitcherWidget extends Widget
{
    /**
     * @var int
     */
    public $jobsCount = 0;

    /**
     * @var int
     */
    public $productsCount = 0;

    /**
     * @var null
     */
    public $entity = null;

    /**
     * @var null
     */
    public $request = null;

    /**
     * @return string
     */
    public function run()
    {
        $queryParams = Yii::$app->request->queryParams;

        return $this->render('entity-switcher', [
            'jobsCount' => $this->jobsCount,
            'productsCount' => $this->productsCount,
            'entity' => $this->entity,
            'request' => $this->request,
            'entityUrlParam' => array_key_exists('entity', $queryParams) ? $queryParams['entity'] : null
        ]);
    }
}