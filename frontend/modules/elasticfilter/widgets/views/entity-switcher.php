<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.10.2017
 * Time: 11:48
 */
use yii\helpers\Html;

/* @var integer $jobsCount */
/* @var integer $productsCount */
/* @var string $request */
/* @var string $entityUrlParam */

$jobsButtonClass = $entityUrlParam === 'job' || ($entityUrlParam === null && $jobsCount >= $productsCount) ? '' : 'revert';
$productsButtonClass = $entityUrlParam === 'product' || ($entityUrlParam === null && $jobsCount < $productsCount) ? '' : 'revert';

?>

<?php if ($jobsCount === 0 && $productsCount > 0) { ?>
    <p class="catalog-empty-text">
        <?= Yii::t('app', 'There was found {count, plural, one{# product} few{# products} many{# products} other{# products}} on your request {request}.', ['count' => $productsCount, 'request' => $request]) ?>
    </p>
    <!--    <p class="text-center">-->
    <!--        --><?php //= Html::a(Yii::t('app', 'Services ({count})', ['count' => $jobsCount]), ['/search/search', 'request' => $request, 'entity' => 'job'], ['class' => 'btn-middle revert', 'disabled' => 'disabled']) ?>
    <!--        --><?php //= Html::a(Yii::t('app', 'Products ({count})', ['count' => $productsCount]), ['/search/search', 'request' => $request, 'entity' => 'product'], ['class' => 'btn-middle ']) ?>
    <!--    </p>-->
<?php } else if ($productsCount === 0 && $jobsCount > 0) { ?>
    <p class="catalog-empty-text">
        <?= Yii::t('app', 'There was found {count, plural, one{# service} few{# services} many{# services} other{# services}} on your request {request}.', ['count' => $jobsCount, 'request' => $request]) ?>
    </p>
    <!--    <p class="text-center">-->
    <!--        --><?php //= Html::a(Yii::t('app', 'Services ({count})', ['count' => $jobsCount]), ['/search/search', 'request' => $request, 'entity' => 'job'], ['class' => 'btn-middle ']) ?>
    <!--        --><?php //= Html::a(Yii::t('app', 'Products ({count})', ['count' => $productsCount]), ['/search/search', 'request' => $request, 'entity' => 'product'], ['class' => 'btn-middle revert', 'disabled' => 'disabled']) ?>
    <!--    </p>-->
<?php } else if ($jobsCount === 0 && $productsCount === 0) { ?>

<?php } else { ?>
    <p class="catalog-empty-text">
        <?= Yii::t('app', 'There was found {count, plural, one{# service} few{# services} many{# services} other{# services}} and {pcount, plural, one{# product} few{# products} many{# products} other{# products}} on your request {request}.', ['count' => $jobsCount, 'pcount' => $productsCount, 'request' => $request]) ?>
    </p>
    <p class="text-center">
        <?= Html::a(Yii::t('app', 'Services ({count})', ['count' => $jobsCount]), ['/search/search', 'request' => $request, 'entity' => 'job'], ['class' => "btn-middle {$jobsButtonClass}"]) ?>
        <?= Html::a(Yii::t('app', 'Products ({count})', ['count' => $productsCount]), ['/search/search', 'request' => $request, 'entity' => 'product'], ['class' => "btn-middle {$productsButtonClass}"]) ?>
    </p>
<?php } ?>

