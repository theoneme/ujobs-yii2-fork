<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.03.2017
 * Time: 18:38
 */

namespace frontend\modules\elasticfilter\models;

use common\components\elasticsearch\ActiveQuery;
use common\helpers\ElasticConditionsHelper;
use common\models\elastic\ProfileElastic;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class FilterPro
 * @package frontend\modules\elasticfilter\models
 */
class FilterPro extends BaseFilter
{
    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $queryParams = Yii::$app->request->queryParams;

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $translationsCondition = $this->getSearchQuery($searchParam, 'translations');
            $attributesCondition = $this->getSearchQuery($searchParam, 'attrs');

            $condition = $translationsCondition;
            $condition['bool']['must'][0]['bool']['should'] = array_merge($condition['bool']['must'][0]['bool']['should'], $attributesCondition['bool']['must'][0]['bool']['should']);
            $this->query->query($condition);
        }

        $condition = $this->query->query;

        $onlineParam = ArrayHelper::remove($queryParams, 'online');
        if ($onlineParam === 'online') {
            $onlineUsers = User::find()->where(['>', 'last_action_at', time() - 60 * 15])->select('id')->column();
            $this->query->andFilterWhere(['user_id' => $onlineUsers]);
        }

        // Если указан город - в первую очередь показываем тех у кого совпадает город
        $cityParam = ArrayHelper::remove($queryParams, 'city');
        if ($cityParam) {
            $this->query->addOrderBy($this->getCityCondition('attrs', $cityParam));
        }

        $languageParam = $queryParams['language'] ?? null;
        $this->query->addOrderBy($this->getLanguageCondition('lang', $languageParam));

        // У кого куплен тарифф - выше остальных
//        $this->query->addOrderBy('tariff.price desc');
        // Дальше обычная сортировка в зависимости от выбранного поля сортировки
        $sortByParam = explode(';', ArrayHelper::remove($queryParams, 'sort_by'));

        if ($sortByParam && in_array($sortByParam[0], ['created_at', 'rating'])) {
            $this->query->orderBy($sortByParam[0] . ' ' . (isset($sortByParam[1]) && in_array($sortByParam[1], ['asc', 'desc']) ? $sortByParam[1] : 'desc'));
        } else {
            if (!$searchParam) {
                $this->addRelevanceOrderQuery();
                $this->query->addOrderBy("created_at DESC");
            } else {
                $this->query->addOrderBy('_score DESC');
            }
        }

        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $this->query->query($condition);
        }

        return true;
    }

    /**
     * @param null $exception
     * @return bool
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        return false;
    }

    /**
     * @param ActiveQuery $query
     * @param null $exception
     * @return null
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $condition = $query->query;

        $queryParams = Yii::$app->request->queryParams;
        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $query->query($condition);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getSortData()
    {
        $queryParams = Yii::$app->request->queryParams;
        $sortByQueryParam = ArrayHelper::remove($queryParams, 'sort_by');
        $sortByParam = $sortByQueryParam ? $sortByQueryParam : 'created_at;desc';

        if (strpos($sortByParam, ';') === false) {
            $sortByParam .= ';desc';
        }

        $sortTitles = [
            'relevance;desc' => Yii::t('filter', 'Relevance'),
            'created_at;desc' => Yii::t('filter', 'Publish date'),
            'rating;asc' => Yii::t('filter', 'Rating ascending'),
            'rating;desc' => Yii::t('filter', 'Rating descending'),
        ];

        $sort = [
            'sort_by' => [
                'value' => $sortByParam,
                'title' => $sortTitles[$sortByParam]
            ],
            'sortTitles' => $sortTitles
        ];

        return $sort;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $attributes = Attribute::find()->where(['alias' => ['language', 'skill', 'city', 'specialty']])->indexBy('alias')->all();
        $attributeIDs[$attributes['language']->id] = 'checkboxlist';
        $attributeIDs[$attributes['skill']->id] = 'checkboxlist';
        $attributeIDs[$attributes['specialty']->id] = 'checkboxlist';
        $attributeIDs[$attributes['city']->id] = 'autocomplete';

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $queryParams = Yii::$app->request->queryParams;

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $translationsCondition = $this->getSearchQuery($searchParam, 'translations');
            $attributesCondition = $this->getSearchQuery($searchParam, 'attrs');

            $condition = $translationsCondition;
            $condition['bool']['must'][0]['bool']['should'] = array_merge($condition['bool']['must'][0]['bool']['should'], $attributesCondition['bool']['must'][0]['bool']['should']);
            $query = $condition;
        }

        if ($this->category->lvl !== 0) {
            $query['bool']['must'][] = [
                'nested' => [
                    'path' => 'categories',
                    'query' => [
                        'bool' => [
                            'filter' => [
                                'terms' => [
                                    'categories.id' => $childrenIds,
                                ],
                            ]
                        ]
                    ],
                ]
            ];
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query = ProfileElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs, [], ['language', 'secure']);

        return $attributes;
    }
}