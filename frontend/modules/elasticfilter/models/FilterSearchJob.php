<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\elastic\JobAttributeElastic;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\models\user\Profile;
use common\modules\attribute\models\Attribute;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;

/**
 * Class SearchJobFilter
 * @package frontend\modules\elasticfilter\models
 */
class FilterSearchJob extends FilterJob
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $tagAttribute = Attribute::findOne(['alias' => 'tag']);
        $attributeIDs[$tagAttribute->id] = 'checkboxlist';

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['type' => $this->type]],
            ['term' => ['status' => Job::STATUS_ACTIVE]],
        ];

        $query = JobElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs);
        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()
            ->select('category.id')
            ->addSelect(['relatedCount' => Job::find()
                ->joinWith(['profile'])
                ->select('count(job.id)')
                ->where("job.category_id=category.id")
                ->andWhere(['job.type' => $this->type, 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->joinWith(['translation'])
            ->where(['category.id' => $categoryIDs])
            ->orderBy('relatedCount desc')
            ->all();

        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            $this->_entityTitle = HtmlPurifier::process(Yii::$app->request->get('request'));
        }

        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Related categories for request {request}', ['request' => $this->entityTitle]);
    }

    /**
     * @return bool
     */
    public function showMobileFilter()
    {
        return true;
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = [];
            $request = Yii::$app->request->get('request', null);
            if ($request !== null) {
                $request = HtmlPurifier::process($request);

                $items = JobElastic::find()
                    ->query($this->getSearchQuery($request))
                    ->limit(500)
                    ->column('category_id');

                $this->_similarCategories = array_slice(array_unique($items), 0, 7);
            }
        }
        return $this->_similarCategories;
    }
}