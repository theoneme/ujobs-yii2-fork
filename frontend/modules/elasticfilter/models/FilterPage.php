<?php

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\elastic\PageAttributeElastic;
use common\models\elastic\PageElastic;
use common\models\Page;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeGroup;
use Yii;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class FilterPage
 * @package frontend\modules\elasticfilter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class FilterPage extends BaseFilter
{

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $queryParams = Yii::$app->request->queryParams;

        $onlineParam = ArrayHelper::remove($queryParams, 'online');
        if ($onlineParam === 'online') {
            $onlineUsers = User::find()->where(['>', 'last_action_at', time() - 60 * 15])->select('id')->column();
            $this->query->andFilterWhere(['user_id' => $onlineUsers]);
        }

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $this->query->query($this->getSearchQuery($searchParam, 'translations'));
        }
        // Если указан город - в первую очередь показываем тех у кого совпадает город
        $cityParam = ArrayHelper::remove($queryParams, 'city');
        if ($cityParam) {
            $this->query->addOrderBy($this->getCityCondition('attrs', $cityParam));
        }

        $languageParam = ArrayHelper::remove($queryParams, 'language');
        $this->query->addOrderBy($this->getLanguageCondition('lang', $languageParam));

        // У кого куплен тарифф - выше остальных
//        $this->query->addOrderBy('tariff.price desc');

        // Дальше обычная сортировка в зависимости от выбранного поля сортировки
        if (!$searchParam) {
            $this->query->addOrderBy("publish_date DESC");
        } else {
            $this->query->addOrderBy('_score DESC');
        }

        $condition = $this->query->query ?? [];
        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $this->query->query($condition);
        }
        return true;
    }

    /**
     * @param array $exception
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        $condition = [];
        $queryParams = Yii::$app->request->queryParams;
        ArrayHelper::remove($queryParams, $exception);
        ArrayHelper::remove($queryParams, 'city');

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $condition = $this->getSearchQuery($searchParam, 'translations');
        }

        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $this->lightQuery->query($condition);
        }

        return true;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];
        $query = [];

        $templates = [
            'textbox' => 'checkboxlist',
            'numberbox' => 'checkboxlist',
            'textbox+' => 'checkboxlist',
            'radiolist' => 'checkboxlist'
        ];

        $group = AttributeGroup::find()->joinWith(['attrs'])->where(['mod_attribute_group.id' => $this->category->attribute_set_id])->one();
        if ($group !== null) {
            foreach ($group->attrs as $attr) {
                if (array_key_exists($attr->type, $templates)) {
                    $attributeIDs[$attr->id] = $templates[$attr->type];
                }
            }
        }

        $attributes = Attribute::find()->where(['alias' => [
//            'city',
            'page_tag'
        ]])->indexBy('alias')->all();
//        $attributeIDs[$attributes['city']->id] = 'autocomplete';
        $attributeIDs[$attributes['page_tag']->id] = 'checkboxlist';

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam, 'translations');
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['status' => Page::STATUS_ACTIVE]],
        ];
        $query['bool']['must'][] = [
            ['term' => ['type' => Page::TYPE_ARTICLE]]
        ];

        if ($this->category->lvl !== 0) {
            $query['bool']['must'][] = [
                ['terms' => ['category_id' => $childrenIds]]
            ];
        }
        $query = PageElastic::find()->query($query)->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);
        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @param ActiveQuery $query
     * @param null $exception
     * @return null
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $condition = $query->query;
        $queryParams = Yii::$app->request->queryParams;

        $condition = $this->queryParamsToConditions($queryParams, $condition);
        if (!empty($condition)) {
            $query->query($condition);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getSortData()
    {
        $queryParams = Yii::$app->request->queryParams;
        $sortByParam = ArrayHelper::remove($queryParams, 'sort_by', 'publish_date;desc');

        if (strpos($sortByParam, ';') === false) {
            $sortByParam .= ';desc';
        }

        $sortTitles = [
            'publish_date;desc' => Yii::t('filter', 'Publish date'),
//            'rating;asc' => Yii::t('filter', 'Rating ascending'),
//            'rating;desc' => Yii::t('filter', 'Rating descending'),
        ];

        $sort = [
            'sort_by' => [
                'value' => $sortByParam,
                'title' => $sortTitles[$sortByParam],
            ],
            'sortTitles' => $sortTitles
        ];

        return $sort;
    }

    /**
     * @return mixed
     */
    protected function getCategoryInitialQuery()
    {
        return Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Page::find()
                ->joinWith(['profile', 'category c2'])
                ->select('count(page.id)')
                ->where('(page.category_id = category.id) or (c2.lft > category.lft and c2.rgt < category.rgt and c2.rgt - c2.lft = 1)')
                ->andWhere(['page.type' => Page::TYPE_ARTICLE, 'page.status' => Page::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->orderBy('relatedCount desc');
    }

//    /**
//     * @return mixed
//     */
//    protected function getCategoriesList()
//    {
//        $mainCategories = [];
//        if ($this->category !== null) {
//            if (!$this->category->isLeaf()) {
//                $mainCategories = ($this->getCategoryInitialQuery())
//                    ->joinWith(['translation'])
//                    ->andWhere(['category.id' => $this->category->getChildrenIds()])
//                    ->all();
//            } else {
//                /* @var $parent Category */
//                $parent = $this->category->parents(1)->one();
//                if ($parent !== null) {
//                    $mainCategories = ($this->getCategoryInitialQuery())
//                        ->joinWith(['translation'])
//                        ->andWhere(['category.id' => $parent->getChildrenIds()])
//                        ->all();
//                }
//            }
//        }
//
//        return $mainCategories;
//    }
}