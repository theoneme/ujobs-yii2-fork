<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\elasticfilter\models;

use common\components\CurrencyHelper;
use common\models\Category;
use common\models\elastic\JobAttributeElastic;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\models\JobAttribute;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeGroup;
use Yii;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class FilterJob
 * @package frontend\modules\elasticfilter\models
 */
class FilterJob extends BaseFilter
{
    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $queryParams = Yii::$app->request->queryParams;
        $priceParam = ArrayHelper::remove($queryParams, 'price');
        if ($priceParam) {
            $priceParts = explode('-', $priceParam);
            $minRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], self::MIN_DEFAULT_PRICE);
            $maxRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], self::MAX_DEFAULT_PRICE);

            $this->query->andFilterWhere([
                '>=',
                'default_price',
                $priceParts[0] <= $minRangePrice
                    ? null
                    : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[0])
            ]);
            $this->query->andFilterWhere([
                '<=',
                'default_price',
                $priceParts[1] >= $maxRangePrice
                    ? null
                    : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[1])
            ]);
        }

        $onlineParam = ArrayHelper::remove($queryParams, 'online');
        if ($onlineParam === 'online') {
            $onlineUsers = User::find()->where(['>', 'last_action_at', time() - 60 * 15])->select('id')->column();
            $this->query->andFilterWhere(['user_id' => $onlineUsers]);
        }

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $this->query->query($this->getSearchQuery($searchParam));
        }
        // Если указан город - в первую очередь показываем тех у кого совпадает город
        $cityParam = ArrayHelper::remove($queryParams, 'city');
        if ($cityParam) {
            $this->query->addOrderBy($this->getCityCondition('attrs', $cityParam));
        }

        $languageParam = ArrayHelper::remove($queryParams, 'language');
        $this->query->addOrderBy($this->getLanguageCondition('lang', $languageParam));

        // Дальше обычная сортировка в зависимости от выбранного поля сортировки
        $sortByParam = explode(';', ArrayHelper::remove($queryParams, 'sort_by'));

        if ($sortByParam && in_array($sortByParam[0], ['price', 'created_at', 'rating'])) {
            $sortByParam[0] = str_replace('price', 'default_price', $sortByParam[0]);
            $this->query->addOrderBy($sortByParam[0] . ' ' . (isset($sortByParam[1]) && in_array($sortByParam[1], ['asc', 'desc']) ? $sortByParam[1] : 'desc'));
        } else {
            if (!$searchParam) {
                if ($this->type === 'job') {
                    $this->addRelevanceOrderQuery();
                    $this->query->addOrderBy('tariff.price desc');
                }
                $this->query->addOrderBy("created_at DESC");
            } else {
                $this->query->addOrderBy('_score DESC');
            }
        }

        $condition = $this->query->query ?? [];
        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $this->query->query($condition);
        }

        return true;
    }

    /**
     * @param array $exception
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        $queryParams = Yii::$app->request->queryParams;
        ArrayHelper::remove($queryParams, $exception);
        ArrayHelper::remove($queryParams, 'city');

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $this->lightQuery->query($this->getSearchQuery($searchParam));
        }

        $condition = $this->queryParamsToConditions($queryParams);

        if (!empty($condition)) {
            $this->lightQuery->query($condition);
        }

        return true;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $templates = [
            'textbox' => 'checkboxlist',
            'numberbox' => 'checkboxlist',
            'textbox+' => 'checkboxlist',
            'radiolist' => 'checkboxlist',
            'checkboxlist' => 'checkboxlist',
            'list' => 'checkboxlist'
        ];

        $group = AttributeGroup::find()->joinWith(['attrs'])->where(['mod_attribute_group.id' => $this->category->attribute_set_id])->one();

        if ($group !== null) {
            foreach ($group->attrs as $attr) {
                if (array_key_exists($attr->type, $templates)) {
                    $attributeIDs[$attr->id] = $templates[$attr->type];
                }
            }
        }

        $attributes = Attribute::find()->where(['alias' => ['tag', 'city']])->indexBy('alias')->all();
        $attributeIDs[$attributes['tag']->id] = 'checkboxlist';
        $attributeIDs[$attributes['city']->id] = 'autocomplete';

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['type' => $this->type]],
            ['term' => ['status' => Job::STATUS_ACTIVE]],
        ];
        if ($this->category->lvl !== 0) {
            $query['bool']['must'][] = [
                ['terms' => ['category_id' => $childrenIds]]
            ];
        }

        $query = JobElastic::find()
            ->query($query)
            ->aggregations($customAggregations);

        $query = $this->lightApplyFiltersToQuery($query);
        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @param ActiveQuery $query
     * @param null $exception
     * @return null
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $condition = $query->query;

        $queryParams = Yii::$app->request->queryParams;
        $priceParam = ArrayHelper::remove($queryParams, 'price');

        if ($priceParam) {
            $priceParts = explode('-', $priceParam);
            $minRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], self::MIN_DEFAULT_PRICE);
            $maxRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], self::MAX_DEFAULT_PRICE);

            $condition['bool']['must'][] = [
                'query' => [
                    'range' => [
                        'default_price' => [
                            'gte' => $priceParts[0] <= $minRangePrice
                                ? null
                                : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[0]),
                            'lte' => $priceParts[1] >= $maxRangePrice
                                ? null
                                : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[1]),
                        ]
                    ]
                ]
            ];
        }

        $condition = $this->queryParamsToConditions($queryParams, $condition);
        if (!empty($condition)) {
            $query->query($condition);
        }

        return $query;
    }

    /**
     * @return mixed
     */
    protected function getCategoryInitialQuery()
    {
        $relatedField = 'category_id';
        if ($this->category->lvl === 0) $relatedField = 'parent_category_id';

        return Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Job::find()
                ->joinWith(['profile'])
                ->select('count(job.id)')
                ->where("job.{$relatedField}=category.id")
                ->andWhere(['job.type' => $this->type === Job::TYPE_TENDER ? [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER] : $this->type, 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->orderBy('relatedCount desc');
    }
}