<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\Job;
use common\models\user\Profile;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeGroup;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;

/**
 * Class FilterSearchProduct
 * @package frontend\modules\elasticfilter\models
 */
class FilterSearchProduct extends FilterProduct
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];
        $query = [];

        $templates = [
            'textbox' => 'checkboxlist',
            'numberbox' => 'checkboxlist',
            'textbox+' => 'checkboxlist',
            'radiolist' => 'checkboxlist'
        ];

//        $group = AttributeGroup::find()->joinWith(['attrs'])->where(['mod_attribute_group.id' => $this->category->attribute_set_id])->one();
//        if ($group !== null) {
//            foreach ($group->attrs as $attr) {
//                if (array_key_exists($attr->type, $templates)) {
//                    $attributeIDs[$attr->id] = $templates[$attr->type];
//                }
//            }
//        }

        $tagAttribute = Attribute::findOne(['alias' => 'product_tag']);
        $attributeIDs[$tagAttribute->id] = 'checkboxlist';

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['type' => $this->type]],
            ['term' => ['status' => Job::STATUS_ACTIVE]],
        ];

        $query = ProductElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs);
        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()
            ->select('category.id')
            ->addSelect(['relatedCount' => Product::find()
                ->joinWith(['profile'])
                ->select('count(product.id)')
                ->where("product.category_id=category.id")
                ->andWhere(['product.type' => $this->type, 'product.status' => Product::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->joinWith(['translation'])
            ->where(['category.id' => $categoryIDs])
            ->orderBy('relatedCount desc')
            ->all();

        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            $this->_entityTitle = HtmlPurifier::process(Yii::$app->request->get('request'));
        }

        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Related categories for request {request}', ['request' => $this->entityTitle]);
    }

    /**
     * @return bool
     */
    public function showMobileFilter()
    {
        return true;
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = [];
            $request = Yii::$app->request->get('request', null);
            if ($request !== null) {
                $request = HtmlPurifier::process($request);

                $items = ProductElastic::find()
                    ->query($this->getSearchQuery($request))
                    ->limit(500)
                    ->column('category_id');

                $this->_similarCategories = array_slice(array_unique($items), 0, 7);
            }
        }
        return $this->_similarCategories;
    }
}