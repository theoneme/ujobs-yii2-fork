<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\elasticfilter\models;

use common\components\CurrencyHelper;
use common\models\Category;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeGroup;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use Yii;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class FilterProduct
 * @package frontend\modules\elasticfilter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class FilterProduct extends BaseFilter
{
    const MAX_DEFAULT_PRICE = 100000;

    /**
     * @var integer
     */
    public $storeId;

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $queryParams = Yii::$app->request->queryParams;
        $priceParam = ArrayHelper::remove($queryParams, 'price');
        if ($priceParam) {
            $priceParts = explode('-', $priceParam);
            $minRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], static::MIN_DEFAULT_PRICE);
            $maxRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], static::MAX_DEFAULT_PRICE);

            $this->query->andFilterWhere([
                '>=',
                'default_price',
                $priceParts[0] <= $minRangePrice
                    ? null
                    : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[0])
            ]);
            $this->query->andFilterWhere([
                '<=',
                'default_price',
                $priceParts[1] >= $maxRangePrice
                    ? null
                    : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[1])
            ]);
        }

        $onlineParam = ArrayHelper::remove($queryParams, 'online');
        if ($onlineParam === 'online') {
            $onlineUsers = User::find()->where(['>', 'last_action_at', time() - 60 * 15])->select('id')->column();
            $this->query->andFilterWhere(['user_id' => $onlineUsers]);
        }

        $secureParam = ArrayHelper::remove($queryParams, 'secure');
        if ($secureParam !== null) {
            $this->query->addOrderBy($this->getSecureCondition('helpers', 1));
        }

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $this->query->query($this->getSearchQuery($searchParam));
        }
        $sortByParam = explode(';', ArrayHelper::remove($queryParams, 'sort_by'));
        // Если указан город - в первую очередь показываем тех у кого совпадает город
        $cityParam = ArrayHelper::remove($queryParams, 'city');
        if ($cityParam) {
            $this->query->addOrderBy($this->getCityCondition('attrs', $cityParam));
        }

        $languageParam = ArrayHelper::remove($queryParams, 'language');
        $this->query->addOrderBy($this->getLanguageCondition('helpers', $languageParam));

        // У кого куплен тарифф - выше остальных
//        $this->query->addOrderBy('tariff.price desc');
        // Дальше обычная сортировка в зависимости от выбранного поля сортировки

        if ($sortByParam && in_array($sortByParam[0], ['price', 'created_at', 'rating'])) {
            $sortByParam[0] = str_replace('price', 'default_price', $sortByParam[0]);
            $this->query->addOrderBy($sortByParam[0] . ' ' . (isset($sortByParam[1]) && in_array($sortByParam[1], ['asc', 'desc']) ? $sortByParam[1] : 'desc'));
        } else {
            if (!$searchParam) {
                if ($this->type === Product::TYPE_PRODUCT) {
                    $this->addRelevanceOrderQuery();
                }
                $this->query->addOrderBy("created_at DESC");
            } else {
                $this->query->addOrderBy('_score DESC');
            }
        }

        $condition = $this->query->query ?? [];
        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $this->query->query($condition);
        }

        return true;
    }

    /**
     * @param string $relation
     * @param $param
     * @return array
     */
    protected function getSecureCondition($relation = 'lang', $param)
    {
        return ["{$relation}.secure" => [
            "order" => "desc",
            "nested_path" => $relation,
            "nested_filter" => [
                ["term" => ["{$relation}.secure" => 1]],
            ]
        ]];
    }

    /**
     * @param array $exception
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        $condition = [];
        $queryParams = Yii::$app->request->queryParams;
        ArrayHelper::remove($queryParams, $exception);
        ArrayHelper::remove($queryParams, 'city');

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $condition = $this->getSearchQuery($searchParam);
        }

        $condition = $this->queryParamsToConditions($queryParams, $condition);

        if (!empty($condition)) {
            $this->lightQuery->query($condition);
        }

        return true;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];
        $query = [];

        $templates = [
            'textbox' => 'checkboxlist',
            'numberbox' => 'checkboxlist',
            'textbox+' => 'checkboxlist',
            'radiolist' => 'checkboxlist',
            'checkboxlist' => 'checkboxlist'
        ];

        $group = AttributeGroup::find()->joinWith(['attrs'])->where(['mod_attribute_group.id' => $this->category->attribute_set_id])->one();
        if ($group !== null) {
            foreach ($group->attrs as $attr) {
                if (array_key_exists($attr->type, $templates)) {
                    $attributeIDs[$attr->id] = $templates[$attr->type];
                }
            }
        }

        $attributes = Attribute::find()->where(['alias' => ['city', 'product_tag']])->indexBy('alias')->all();
        $attributeIDs[$attributes['city']->id] = 'autocomplete';
        $attributeIDs[$attributes['product_tag']->id] = 'checkboxlist';

        $childrenIds = array_merge($this->category->children()->select('id')->column(), (array)$this->category->id);

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['status' => Product::STATUS_ACTIVE]],
        ];

        switch ($this->type) {
            case 'product':
                $query['bool']['must'][] = [
                    ['term' => ['type' => Product::TYPE_PRODUCT]]
                ];
                break;
            case 'ptender':
                $query['bool']['must'][] = [
                    ['term' => ['type' => Product::TYPE_TENDER]]
                ];
                break;
        }

        if ($this->category->lvl !== 0) {
            $query['bool']['must'][] = [
                ['terms' => ['category_id' => $childrenIds]]
            ];
        }

        $query = ProductElastic::find()
            ->query($query)
            ->aggregations($customAggregations);

        $query = $this->lightApplyFiltersToQuery($query);
        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs, [], []);

        return $attributes;
    }

    /**
     * @param ActiveQuery $query
     * @param null $exception
     * @return null
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $condition = $query->query;

        $queryParams = Yii::$app->request->queryParams;
        $priceParam = ArrayHelper::remove($queryParams, 'price');
        if ($priceParam) {
            $priceParts = explode('-', $priceParam);
            $minRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], static::MIN_DEFAULT_PRICE);
            $maxRangePrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], static::MAX_DEFAULT_PRICE);

            $condition['bool']['must'][] = [
                'query' => [
                    'range' => [
                        'default_price' => [
                            'gte' => $priceParts[0] <= $minRangePrice
                                ? null
                                : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[0]),
                            'lt' => $priceParts[1] >= $maxRangePrice
                                ? null
                                : CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $priceParts[1])
                        ]
                    ]
                ]
            ];
        }

        $condition = $this->queryParamsToConditions($queryParams, $condition);
        if (!empty($condition)) {
            $query->query($condition);
        }

        return $query;
    }

//    /**
//     * @return array
//     */
//    protected function getSecureCondition()
//    {
//        return ['secure' => true];
//    }

    /**
     * @return mixed
     */
    protected function getCategoryInitialQuery()
    {
        switch($this->category->lvl) {
            case 0:
                $relatedField = 'parent_category_id';
                break;
            case 2:
                $relatedField = $this->category->rgt - $this->category->lft > 1 ? 'subcategory_id' : 'category_id';
                break;
            case 3:
                $relatedField = 'subcategory_id';
                break;
            default:
                $relatedField = 'category_id';
        }
        return Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Product::find()
                ->joinWith(['profile'])
                ->select('count(product.id)')
                ->where("product.{$relatedField}=category.id")
                ->andWhere(['product.type' => $this->type, 'product.status' => Product::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->orderBy('relatedCount desc');
    }
}