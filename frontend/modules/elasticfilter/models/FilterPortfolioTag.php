<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.08.2017
 * Time: 15:03
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\elastic\UserPortfolioAttributeElastic;
use common\models\elastic\UserPortfolioElastic;
use common\models\UserPortfolio;
use common\models\UserPortfolioAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PortfolioTagFilter
 * @package frontend\modules\elasticfilter\models
 */
class FilterPortfolioTag extends FilterPortfolio
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $portfolioTagAttribute = Attribute::findOne(['alias' => 'portfolio_tag']);
        $attributeIDs[$portfolioTagAttribute->id] = 'checkboxlist';

        $categories = $this->similarCategories;

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getAttributeSearchQuery('portfolio', $searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['terms' => ['category_id' => $categories]],
            ['term' => ['version' => UserPortfolio::VERSION_NEW]],
            ['term' => ['status' => UserPortfolio::STATUS_ACTIVE]],
        ];

        $query = UserPortfolioElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);
        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs, [], ['language', 'online', 'secure']);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()->joinWith(['translation'])->where(['category.id' => $categoryIDs])->all();
        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            /* @var UserPortfolioAttribute $tag */
            $tag = AttributeValue::find()
                ->joinWith(['translations', 'relatedAttribute'])
                ->where([
                    'mod_attribute.alias' => 'portfolio_tag',
                    'mod_attribute_value.alias' => $this->request
                ])->one();
            if ($tag !== null) {
                $this->_entityTitle = $tag->translations[Yii::$app->language]->title ?? array_pop($tag->translations)->title;
            }
        }

        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with tag "{tag}"', ['tag' => $this->entityTitle]);
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['translation', 'portfolios'])
                ->andWhere(['user_portfolio.status' => UserPortfolio::STATUS_ACTIVE])
                ->andWhere(['exists',
                    UserPortfolioAttribute::find()
                        ->where('user_portfolio.id = user_portfolio_attribute.user_portfolio_id')
                        ->andWhere([
                            'user_portfolio_attribute.entity_alias' => 'portfolio_tag',
                            'user_portfolio_attribute.value_alias' => $this->request
                        ])
                ])
                ->groupBy('category.id')
                ->orderBy('sort_order asc')
                ->select('category.id')
                ->limit(7)
                ->column();
        }

        return $this->_similarCategories;
    }
}