<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 16:07
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\user\Profile;
use common\modules\attribute\models\AttributeGroup;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class FilterProduct
 * @package frontend\modules\elasticfilter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class FilterProductStore extends FilterProduct
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $attributeIDs = [];
        $query = [];

        $templates = [
            'textbox' => 'checkboxlist',
            'numberbox' => 'checkboxlist',
            'textbox+' => 'checkboxlist',
            'radiolist' => 'checkboxlist'
        ];

        $group = AttributeGroup::find()->joinWith(['attrs'])->where(['mod_attribute_group.id' => $this->category->attribute_set_id])->one();
        if ($group !== null) {
            foreach ($group->attrs as $attr) {
                if (array_key_exists($attr->type, $templates)) {
                    $attributeIDs[$attr->id] = $templates[$attr->type];
                }
            }
        }

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['status' => Product::STATUS_ACTIVE]],
            ['term' => ['user_id' => $this->storeId]],
        ];
        switch ($this->type) {
            case 'product':
                $query['bool']['must'][] = [
                    ['term' => ['type' => Product::TYPE_PRODUCT]]
                ];
                break;
        }
        if ($this->category->lvl !== 0) {
            $query['bool']['must'][] = [
                ['terms' => ['category_id' => $childrenIds]]
            ];
        }
        $query = ProductElastic::find()
            ->query($query)
            ->aggregations($customAggregations);

        $query = $this->lightApplyFiltersToQuery($query);
        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs, [], ['online', 'language']);

        return $attributes;
    }

    /**
     * @param $categories
     * @param null $route
     * @return array
     */
    protected function formatCategories($categories, $route = null)
    {
        $result = [];

        if ($route === null) {
            $route = "/" . Yii::$app->controller->route . "s";
        }

        $currentUrl = Url::current(['_pjax' => null]);

        if (!empty($categories)) {
            foreach ($categories as $mainCategory) {
                $item = [
                    'label' => $mainCategory->translation->title,
                    'options' => ['class' => 'list-group-item', 'tag' => 'a'],
                    'linkOptions' => ['href' => Url::to([$route, "category_1" => $mainCategory->translation->slug, 'store_id' => $this->storeId])],
                    'count' => $mainCategory->relatedCount
                ];

                if ($item['linkOptions']['href'] === $currentUrl) {
                    $item['active'] = true;
                }

                $result[$mainCategory->type][] = $item;
            }
        }

        return $result;
    }

    /**
     * @return mixed
     */
    protected function getCategoryInitialQuery()
    {
        $relatedField = 'category_id';
        if ($this->category->lvl === 0) $relatedField = 'parent_category_id';
        if ($this->category->lvl >= 2) $relatedField = 'subcategory_id';

        return Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Product::find()
                ->joinWith(['profile'])
                ->select('count(product.id)')
                ->where("product.{$relatedField}=category.id")
                ->andWhere(['product.type' => $this->type, 'product.status' => Product::STATUS_ACTIVE, 'product.user_id' => $this->storeId])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->orderBy('relatedCount desc');
    }
}