<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.09.2017
 * Time: 17:11
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Attachment;
use common\models\Category;
use common\models\elastic\UserPortfolioAttributeElastic;
use common\models\elastic\UserPortfolioElastic;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserPortfolio;
use common\modules\attribute\models\Attribute;
use Yii;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class FilterPortfolio
 * @package frontend\modules\elasticfilter\models
 */
class FilterPortfolio extends BaseFilter
{
    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function applyFiltersToCatalog()
    {
        $queryParams = Yii::$app->request->queryParams;

        $onlineParam = ArrayHelper::remove($queryParams, 'online');
        if ($onlineParam === 'online') {
            $onlineUsers = User::find()->where(['>', 'last_action_at', time() - 60 * 15])->select('id')->column();
            $this->query->andFilterWhere(['user_id' => $onlineUsers]);
        }

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $condition['bool']['must'][] = [
                [
                    'query_string' => [
                        'fields' => [
//                            'description',
                            'title'
                        ],
                        'query' => $this->cureSearchString($searchParam)/* . '~'*/,
                        'fuzzy_prefix_length' => 2,
                        'fuzziness' => 2,
                    ]
                ]
            ];
        }

        $sortByParam = explode(';', ArrayHelper::remove($queryParams, 'sort_by'));
        // Если указан город - в первую очередь показываем тех у кого совпадает город
        $cityParam = ArrayHelper::remove($queryParams, 'city');
        if ($cityParam) {
            $this->query->addOrderBy($this->getCityCondition('attrs', $cityParam));
        }

        $languageParam = ArrayHelper::remove($queryParams, 'language');
        $this->query->addOrderBy($this->getLanguageCondition('lang', $languageParam));

        // У кого куплен тарифф - выше остальных
//        $this->query->addOrderBy('tariff.price desc');
        // Дальше обычная сортировка в зависимости от выбранного поля сортировки

        if ($sortByParam && in_array($sortByParam[0], ['created_at', 'rating'])) {
            $this->query->addOrderBy($sortByParam[0] . ' ' . (isset($sortByParam[1]) && in_array($sortByParam[1], ['asc', 'desc']) ? $sortByParam[1] : 'desc'));
        } else {
            if (!$searchParam) {
                $this->addRelevanceOrderQuery();
                $this->query->addOrderBy("created_at DESC");
            } else {
                $this->query->addOrderBy('_score DESC');
            }
        }

        foreach ($queryParams as $key => $queryParam) {
            if (array_key_exists($key, $this->availableAttributes)) {
                $newCondition = $this->getApplyAttributesCondition('attrs', $key, $queryParam);
                $condition['bool']['must'][] = $newCondition['bool']['must'];
            }
        }

        if (!empty($condition)) {
            $this->query->query($condition);
        }

        return true;
    }

    /**
     * @param null $exception
     * @return bool
     */
    public function lightApplyFiltersToCatalog($exception = null)
    {
        return false;
    }

    /**
     * @param ActiveQuery $query
     * @param null $exception
     * @return null
     */
    public function lightApplyFiltersToQuery($query = null, $exception = null)
    {
        $condition = $query->query;
        $queryParams = Yii::$app->request->queryParams;

        foreach ($queryParams as $key => $queryParam) {
            if (array_key_exists($key, $this->availableAttributes)) {
                $condition['bool']['must'][] = $this->getApplyAttributesCondition('attrs', $key, $queryParam);
            }
        }

        if (!empty($condition)) {
            $query->query($condition);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $portfolioTagAttribute = Attribute::findOne(['alias' => 'portfolio_tag']);
        $attributeIDs[$portfolioTagAttribute->id] = 'checkboxlist';

        $childrenIds = array_merge($this->category->getChildrenIds(), (array)$this->category->id);

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query['bool']['must'] = [
                [
                    'query_string' => [
                        'fields' => [
                            'title^4',
                            'description',
                        ],
                        'query' => $searchParam . '~',
                        'fuzzy_prefix_length' => 2,
                        'fuzziness' => 2,
                    ]
                ]
            ];
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['status' => UserPortfolioElastic::STATUS_ACTIVE]],
        ];

        if ($this->category->lvl !== 0) {
            $query['bool']['must'][] = [
                ['terms' => ['category_id' => $childrenIds]]
            ];
        }
        $query = UserPortfolioElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);
        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs, [], ['language', 'secure']);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $mainCategories = $this->getCategoriesList();
        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    protected function getCategoriesList()
    {
        $mainCategories = [];

        if ($this->category !== null) {
            if (!$this->category->isLeaf()) {
                if ($this->category->lvl === 0) {
                    $mainCategories = ($this->getCategoryInitialQuery())->joinWith(['translation'])
                        ->andWhere(['type' => ['job_category', 'product_category'], 'lvl' => 1])
                        ->groupBy('category.id')
                        ->all();

                } else {
                    $mainCategories = ($this->getCategoryInitialQuery())
                        ->joinWith(['translation'])
                        ->andWhere(['category.id' => $this->category->getChildrenIds()])
                        ->all();
                }
            } else {
                /* @var $parent Category */
                $parent = $this->category->parents(1)->one();
                if ($parent !== null) {
                    $mainCategories = ($this->getCategoryInitialQuery())
                        ->joinWith(['translation'])
                        ->andWhere(['category.id' => $parent->getChildrenIds()])
                        ->all();
                }
            }
        }

        return $mainCategories;
    }

    /**
     * @return mixed
     */
    protected function getCategoryInitialQuery()
    {
        $relatedField = 'category_id';
        if ($this->category->lvl === 0) $relatedField = 'parent_category_id';

        return Category::find()
            ->select('category.id, alias, lvl, lft, rgt, root, type')
            ->addSelect(['relatedCount' => UserPortfolio::find()
                ->select('count(user_portfolio.id)')
                ->joinWith(['profile'])
                ->where("user_portfolio.{$relatedField}=category.id")
                ->andWhere(['user_portfolio.version' => UserPortfolio::VERSION_NEW, 'user_portfolio.status' => UserPortfolio::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
                ->andWhere(['exists', Attachment::find()
                    ->where('attachment.entity_id = user_portfolio.id')
                    ->andWhere(['entity' => 'portfolio'])
                ])
            ])
            ->orderBy('relatedCount desc');
    }
}