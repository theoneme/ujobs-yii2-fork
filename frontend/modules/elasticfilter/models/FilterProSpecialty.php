<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.03.2017
 * Time: 17:19
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\elastic\ProfileElastic;
use common\models\elastic\UserAttributeElastic;
use common\models\Job;
use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SpecialtyProFilter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
class FilterProSpecialty extends FilterPro
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];
        $queryParams = Yii::$app->request->queryParams;

        $attributes = Attribute::find()->where(['alias' => ['language', 'skill', 'city', 'specialty']])->indexBy('alias')->all();
        $attributeIDs[$attributes['language']->id] = 'checkboxlist';
        $attributeIDs[$attributes['skill']->id] = 'checkboxlist';
        $attributeIDs[$attributes['specialty']->id] = 'checkboxlist';
        $attributeIDs[$attributes['city']->id] = 'autocomplete';

        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam, 'translations');
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query = ProfileElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);
        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs, [], ['language', 'secure']);

        return $attributes;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            /* @var $userSpecialty UserAttribute */
            $userSpecialty = UserAttribute::find()
                ->joinWith('attrValueDescriptions')
                ->where([
                    'user_attribute.entity_alias' => 'specialty',
                    'user_attribute.value_alias' => $this->request
                ])->one();
            $this->_entityTitle = isset($userSpecialty->attrValueDescriptions[Yii::$app->language]) ? $userSpecialty->attrValueDescriptions[Yii::$app->language]->title : array_values($userSpecialty->attrValueDescriptions)[0]->title;
        }

        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with specialty "{specialty}"', ['specialty' => $this->entityTitle]);
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['jobs'])
                ->andWhere(['job.type' => 'job', 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['exists',
                    UserAttribute::find()
                        ->where('job.user_id = user_attribute.user_id')
                        ->andWhere([
                            'user_attribute.attribute_id' => 41,
                            'user_attribute.value_alias' => $this->request
                        ])
                ])
                ->orderBy('sort_order asc')
                ->groupBy('category.id')
                ->select('category.id')
                ->limit(7)
                ->column();
        }

        return $this->_similarCategories;
    }
}