<?php

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\elastic\JobAttributeElastic;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\models\JobAttribute;
use common\models\user\Profile;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class FilterJobSpecialty
 * @package frontend\modules\elasticfilter\models
 */
class FilterJobSpecialty extends FilterJob
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $tagAttribute = Attribute::findOne(['alias' => 'tag']);
        $attributeIDs[$tagAttribute->id] = 'checkboxlist';
        $attributeIDs[41] = 'checkboxlist';

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getSearchQuery($searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }

        $query['bool']['must'][] = [
            ['term' => ['type' => $this->type]],
            ['term' => ['status' => Job::STATUS_ACTIVE]],
        ];

        $query = JobElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);

        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Job::find()
                ->joinWith(['profile'])
                ->select('count(job.id)')
                ->where("job.category_id=category.id")
                ->andWhere(['job.type' => $this->type, 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
            ])
            ->joinWith(['translation'])
            ->where(['category.id' => $categoryIDs])
            ->orderBy('relatedCount desc')
            ->all();

        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            /* @var AttributeValue $value */
            $value = AttributeValue::find()
                ->joinWith(['translations'])
                ->where(['attribute_id' => 41, 'alias' => $this->request])
                ->one();
            if ($value !== null) {
                $this->_entityTitle = $value->translations[Yii::$app->language]->title ?? array_pop($value->translations)->title;
            }
        }

        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with specialty "{specialty}"', ['specialty' => $this->entityTitle]);
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->joinWith(['jobs'])
                ->andWhere(['job.type' => $this->type, 'job.status' => Job::STATUS_ACTIVE])
                ->andWhere(['exists',
                    JobAttribute::find()
                        ->where('job.id = job_attribute.job_id')
                        ->andWhere([
                            'job_attribute.attribute_id' => 41,
                            'job_attribute.value_alias' => $this->request
                        ])
                ])
                ->orderBy('sort_order asc')
                ->select('category.id')
                ->column();
        }
        return $this->_similarCategories;
    }
}