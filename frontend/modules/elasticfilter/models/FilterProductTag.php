<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.08.2017
 * Time: 15:03
 */

namespace frontend\modules\elasticfilter\models;

use common\models\Category;
use common\models\user\Profile;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ProductTagFilter
 * @package frontend\modules\elasticfilter\models
 * @property $similarCategories array
 */
class FilterProductTag extends FilterProduct
{
    /**
     * @return array
     */
    public function buildAttributeFiltersConfig()
    {
        $query = [];

        $attributes = Attribute::find()->where(['alias' => ['city', 'product_tag']])->indexBy('alias')->all();
        $attributeIDs[$attributes['city']->id] = 'autocomplete';
        $attributeIDs[$attributes['product_tag']->id] = 'checkboxlist';

        $queryParams = Yii::$app->request->queryParams;
        $searchParam = ArrayHelper::remove($queryParams, 'request');
        if ($searchParam) {
            $query = $this->getAttributeSearchQuery('product', $searchParam);
        }

        $customAggregations = [];
        foreach ($attributeIDs as $key => $id) {
            $customAggregations[$key] = $this->getAttributeNestedAggregationCondition($key);
        }
        $query['bool']['must'][] = [
            ['term' => ['type' => $this->type]],
            ['term' => ['status' => Product::STATUS_ACTIVE]],
        ];

        $query = ProductElastic::find()
            ->query($query)
            ->aggregations($customAggregations);
        $query = $this->lightApplyFiltersToQuery($query);

        $attributeValues = $query->search(null, ['search_type' => 'count']);
        $attributes = $this->buildConfigElastic($attributeValues, $attributeIDs);

        return $attributes;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $categoryIDs = $this->similarCategories;
        $mainCategories = Category::find()
            ->select('category.id')
            ->addSelect(['relatedCount' => Product::find()
                ->select('count(product.id)')
                ->where("product.category_id=category.id")
                ->andWhere(['product.type' => $this->type, 'product.status' => Product::STATUS_ACTIVE])
            ])
            ->joinWith(['translation'])
            ->where(['category.id' => $categoryIDs])
            ->orderBy('relatedCount desc')
            ->all();

        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return string
     */
    public function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            /* @var ProductAttribute $tag */
            $tag = AttributeValue::find()
                ->joinWith(['translation', 'relatedAttribute'])
                ->where([
                    'mod_attribute.alias' => 'product_tag',
                    'mod_attribute_value.alias' => $this->request
                ])->one();
            if ($tag !== null) {
                $this->_entityTitle = $tag->translations[Yii::$app->language]->title ?? array_pop($tag->translations)->title;
            }
        }

        return $this->_entityTitle;
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'Categories with tag "{tag}"', ['tag' => $this->entityTitle]);
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        if ($this->_similarCategories === null) {
            $this->_similarCategories = Category::find()
                ->select('category.id')
                ->addSelect(['relatedCount' => Product::find()
                    ->joinWith(['profile'])
                    ->select('count(product.id)')
                    ->where("product.category_id=category.id or product.subcategory_id=category.id")
                    ->andWhere(['product.type' => $this->type, 'product.status' => Product::STATUS_ACTIVE])
                    ->andWhere(['not', ['profile.status' => [Profile::STATUS_PAUSED, Profile::STATUS_DISABLED]]])
                ])
                ->joinWith(['translation', 'products'])
                ->andWhere(['product.status' => Product::STATUS_ACTIVE])
                ->andWhere(['exists',
                    ProductAttribute::find()
                        ->where('product.id = product_attribute.product_id')
                        ->andWhere([
                            'product_attribute.entity_alias' => 'product_tag',
                            'product_attribute.value_alias' => $this->request
                        ])
                ])
                ->groupBy('category.id')
                ->orderBy('relatedCount desc')
                ->limit(7)
                ->column();
        }

        return $this->_similarCategories;
    }
}