<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.10.2017
 * Time: 17:24
 */

namespace frontend\modules\elasticfilter\models;

use common\components\CurrencyHelper;
use common\helpers\ElasticConditionsHelper;
use common\models\Category;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\base\Model;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Filter
 * @package frontend\modules\filter\models
 * @property $entityTitle string
 * @property $similarCategories array
 */
abstract class BaseFilter extends Model
{
    const MIN_DEFAULT_PRICE = 200;
    const MAX_DEFAULT_PRICE = 50000;

    const MODE_TYPE_RESERVED = 'reserved';

    /**
     * @var ActiveQuery
     */
    public $query = null;

    /**
     * @var ActiveQuery
     */
    public $lightQuery = null;

    /**
     * @var null
     */
    public $requestType = null;

    /**
     * @var null
     */
    public $request = null;

    /**
     * @var string
     */
    public $type = null;

    /**
     * @var Category
     */
    public $category = null;

    /**
     * @var array
     */
    public $_similarCategories = null;

    /**
     * @var string
     */
    protected $_entityTitle = null;

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var array
     */
    protected $availableAttributes = [];

    /**
     * @var array
     */
    protected $reservedFiltersToSimple = [
        'reserved_job_tag' => 'tag',
        'reserved_specialty' => 'specialty',
        'reserved_skill' => 'skill',
        'reserved_portfolio_tag' => 'portfolio_tag',
        'reserved_product_tag' => 'product_tag'
    ];

    /**
     * @var array
     */
    protected $searchStringExceptions = [
        'фрилансеры', 'фрилансер', 'исполнитель', 'заказать', 'заказ', 'купить', 'услуга', 'услуги', 'услугу', 'как', 'найти', 'написать', 'сделать', ',', '.'
    ];

    /**
     * BaseFilter constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->routes = [
            'FilterJob' => "/category/{$this->type}s",
            'FilterPro' => "/category/{$this->type}s",
            'FilterPortfolio' => "/category/{$this->type}s",
            'FilterProduct' => "/board/category/{$this->type}s",
            'FilterJobTag' => "/category/jobs",
            'FilterProSkill' => "/category/pros",
            'FilterPortfolioTag' => '/category/portfolios',
            'FilterProductTag' => '/board/category/products',
            'FilterJobSpecialty' => '/category/tenders',
            'FilterProSpecialty' => '/category/pros',
            'FilterProductStore' => '/board/category/store',
            'FilterSearchJob' => "/category/{$this->type}s",
            'FilterSearchProduct' => "/board/category/{$this->type}s",
            'FilterPage' => "/category/articles",
        ];

        $queryParams = Yii::$app->request->queryParams;

        $keys = array_keys($queryParams);
        $filteredKeys = [];

        foreach ($keys as $key) {
//            if(in_array($key, ['city', 'online', 'secure'])) continue;

            $filteredKeys[] = array_key_exists($key, $this->reservedFiltersToSimple) ? $this->reservedFiltersToSimple[$key] : $key;
        }

        $this->availableAttributes = Attribute::find()->joinWith(['translations' => function ($query) {
            return $query->select('entity, entity_content, title, locale');
        }])->select('mod_attribute.id, alias')->where(['mod_attribute.alias' => $filteredKeys])->indexBy('alias')->asArray()->all();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'query' => [['query', 'lightQuery'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function getSliderPoints()
    {
        $result = [];

        if ($priceParam = Yii::$app->request->get('price')) {
            $priceParts = explode('-', $priceParam);

            $result['min'] = (int)$priceParts[0];
            $result['max'] = (int)$priceParts[1];
        } else {
            $result = $this->getPriceRange();
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getPriceRange()
    {
        $result = [];

        $query = clone $this->lightQuery;

        $cheapest = $query->andWhere(['>=', 'default_price', static::MIN_DEFAULT_PRICE])->orderBy('default_price asc')->one();
        $mostExpensive = $query->andWhere(['<=', 'default_price', static::MAX_DEFAULT_PRICE])->orderBy('default_price desc')->one();

        if ($cheapest) {
            $result['min'] = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $cheapest['default_price']);
        }

        if ($mostExpensive) {
            $result['max'] = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $mostExpensive['default_price']);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getSortData()
    {
        $queryParams = Yii::$app->request->queryParams;

        $sortByQueryParam = ArrayHelper::remove($queryParams, 'sort_by');

        $sortByParam = $sortByQueryParam ? $sortByQueryParam : 'relevance;desc';
        if (strpos($sortByParam, ';') === false) {
            $sortByParam .= ';desc';
        }

        $sortTitles = [
            'relevance;desc' => Yii::t('filter', 'Relevance'),
            'created_at;desc' => Yii::t('filter', 'Publish date'),
            'price;asc' => Yii::t('filter', 'Price ascending'),
            'price;desc' => Yii::t('filter', 'Price descending'),
            'rating;asc' => Yii::t('filter', 'Rating ascending'),
            'rating;desc' => Yii::t('filter', 'Rating descending'),
        ];

        $sort = [
            'sort_by' => [
                'value' => $sortByParam,
                'title' => $sortTitles[$sortByParam]
            ],
            'sortTitles' => $sortTitles
        ];

        return $sort;
    }

    /**
     * @return array
     */
    public function getPaginationData()
    {
        $queryParams = Yii::$app->request->queryParams;

        $pageQueryParam = ArrayHelper::remove($queryParams, 'page');
        $perPageQueryParam = ArrayHelper::remove($queryParams, 'per-page');

        $pageParam = $pageQueryParam ? $pageQueryParam : '';
        $perPageParam = $perPageQueryParam ? $perPageQueryParam : '';

        $pagination = [
            'per-page' => $perPageParam,
            'page' => $pageParam
        ];

        return $pagination;
    }

    /**
     * @return array
     */
    public function buildCategoriesMenu()
    {
        $mainCategories = $this->getCategoriesList();
        $categories = $this->formatCategories($mainCategories, $this->routes[(new \ReflectionClass($this))->getShortName()]);

        return $categories;
    }

    /**
     * @return mixed
     */
    protected function getCategoriesList()
    {
        $mainCategories = [];

        if ($this->category !== null) {
            if (!$this->category->isLeaf()) {
                $mainCategories = ($this->getCategoryInitialQuery())
                    ->joinWith(['translation'])
                    ->andWhere(['category.id' => $this->category->getChildrenIds()])
                    ->all();
            } else {
                /* @var $parent Category */
                $parent = $this->category->parents(1)->one();
                if ($parent !== null) {
                    $mainCategories = ($this->getCategoryInitialQuery())
                        ->joinWith(['translation'])
                        ->andWhere(['category.id' => $parent->getChildrenIds()])
                        ->all();
                }
            }
        }

        return $mainCategories;
    }

    /**
     * @return mixed
     */
    protected function getCategoryInitialQuery()
    {
        return Category::find();
    }

    /**
     * @param $categories
     * @param null $route
     * @return array
     */
    protected function formatCategories($categories, $route = null)
    {
        $result = [];

        if ($route === null) {
            $route = "/" . Yii::$app->controller->route . "s";
        }

        $currentUrl = Url::current(['_pjax' => null]);

        if (!empty($categories)) {
            foreach ($categories as $mainCategory) {
                $item = [
                    'label' => $mainCategory->translation->title,
                    'options' => ['class' => 'list-group-item', 'tag' => 'a'],
                    'linkOptions' => ['href' => Url::to([$route, "category_1" => $mainCategory->translation->slug])],
                    'count' => $mainCategory->relatedCount
                ];

                if ($item['linkOptions']['href'] === $currentUrl) {
                    $item['active'] = true;
                }

                $result[$mainCategory->type][] = $item;
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function buildRemoveFilterLinks()
    {
        $queryParams = Yii::$app->request->queryParams;
        $config = [];

        $priceParam = ArrayHelper::remove($queryParams, 'price');
        if ($priceParam) {
            $config['price']['title'] = Yii::t('filter', 'Price');
            $config['price']['values'][] = [
                'url' => Url::current(['price' => null, '_pjax' => null]),
                'value' => Yii::t('filter', 'Price') . ": " . $priceParam
            ];
        }

        foreach ($queryParams as $key => $queryParam) {
            if (!in_array($key, $this->getRemoveAttributeLinkExceptions()) && array_key_exists($key, $this->availableAttributes)) {
                $attribute = $this->availableAttributes[$key];
                $translations = $attribute['translations'];
                $title = array_key_exists(Yii::$app->language, $translations) ? $translations[Yii::$app->language]['title'] : array_pop($translations)['title'];

                $config[$attribute['id']]['title'] = $title;
                foreach ((array)$queryParam as $k => $p) {
                    if (is_array($queryParam)) {
                        $tempQueryParams = [$key => array_replace($queryParam, [$k => null]), '_pjax' => null];
                    } else {
                        $tempQueryParams = [$key => null, '_pjax' => null];
                    }

                    $attributeValue = AttributeValue::find()->joinWith(['translations'])->where(['mod_attribute_value.alias' => $p, 'attribute_id' => $this->availableAttributes[$key]['id']])->asArray()->one();
                    if ($attributeValue !== null) {
                        $valueTitle = $attributeValue['translations'][Yii::$app->language]['title'] ?? array_pop($attributeValue['translations'])['title'];
                    } else {
                        $valueTitle = $this->availableAttributes[$key]['translations'][Yii::$app->language]['title'] ?? array_pop($this->availableAttributes[$key]['translations'])['title'];
                    }

                    $config[$attribute['id']]['values'][] = [
                        'url' => Url::current($tempQueryParams),
                        'value' => $valueTitle ?? $title
                    ];
                }
            }
        }

        return $config;
    }

    /**
     * @return array
     */
    protected function getRemoveAttributeLinkExceptions()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCategoryHeader()
    {
        return Yii::t('filter', 'All in') . ' ' . Html::encode($this->category->meta && $this->category->meta->keyword ? $this->category->meta->keyword : $this->entityTitle);
    }

    /**
     * @return bool
     */
    public function showMobileFilter()
    {
        return true;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    abstract protected function applyFiltersToCatalog();

    /**
     * @param array $exception
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    abstract protected function lightApplyFiltersToCatalog($exception = null);

    /**
     * @param ActiveQuery $query
     * @return null
     * @throws \yii\base\InvalidConfigException
     */
    abstract protected function lightApplyFiltersToQuery($query = null);

    /**
     * @return array
     */
    abstract protected function buildAttributeFiltersConfig();

    /**
     * @param array $attributeValues
     * @param array $attributeIDs
     * @param array $overrides
     * @param array $extraExceptions
     * @return array
     */
    protected function buildConfigElastic($attributeValues = [],
                                          $attributeIDs = [],
                                          array $overrides = [],
                                          array $extraExceptions = ['secure'])
    {
        $attributes = [];

        if (!in_array('secure', $extraExceptions)) {
            $attributes['secure'] = [
                'alias' => 'secure',
                'title' => Yii::t('app', 'Language'),
                'type' => 'switcher',
                'checked' => $this->getCheckedValues('secure'),
                'values' => []
            ];
        }

        if (!in_array('language', $extraExceptions)) {
            $languageValues = [];
            foreach (Yii::$app->params['languages'] as $locale => $title) {
                $languageValues[strtolower($title)] = ['title' => Yii::t('app', $title, [], $locale)];
            }
            $attributes['language'] = [
                'alias' => 'language',
                'title' => Yii::t('app', 'Announcement language'),
                'type' => 'checkboxlist',
                'checked' => $this->getCheckedValues('language'),
                'values' => $languageValues
            ];
        }

        if (!empty($attributeValues) && !empty($attributeIDs)) {
            /** @var array $aggregations */
            $aggregations = $attributeValues['aggregations'];
            $keys = array_keys($aggregations);

            if (!empty($aggregations)) {
                $attributeObjects = Attribute::find()->joinWith(['translations' => function ($query) {
                    /** @var \yii\db\ActiveQuery $query */
                    return $query->select('entity, entity_content, title, locale');
                }])->select('mod_attribute.id, alias')->where(['mod_attribute.id' => $keys])->indexBy('id')->asArray()->all();
                foreach ($aggregations as $key => $aggregation) {
                    if ($aggregation['doc_count'] === 0 && $key !== 42) {
                        continue;
                    }

                    $translations = $attributeObjects[$key]['translations'];
                    $title = array_key_exists(Yii::$app->language, $translations) ? $translations[Yii::$app->language]['title'] : array_pop($translations)['title'];
                    $attributes[$key] = [
                        'alias' => $overrides['alias'][$key] ?? $attributeObjects[$key]['alias'],
                        'title' => $title,
                        'type' => $overrides['type'][$key] ?? $attributeIDs[$key],
                        'checked' => $this->getCheckedValues($attributeObjects[$key]['alias']),
                        'values' => []
                    ];

                    $keys = array_column($aggregation['attrs']['values']['buckets'], 'key');
                    $values = AttributeValue::find()
                        ->joinWith(['translations'])
                        ->where(['alias' => $keys, "mod_attribute_value.attribute_id" => $key])
                        ->groupBy('mod_attribute_value.id')
                        ->indexBy('alias')
                        ->asArray()
                        ->all();

                    foreach ($aggregation['attrs']['values']['buckets'] as $bucket) {
                        $valueKey = $bucket['key'];
                        if ($valueKey === $this->request && $this->requestType === 'reserved') continue;
                        $value = $values[$valueKey] ?? null;
                        if ($value === null) continue;

                        if (array_key_exists(Yii::$app->language, $value['translations'])) {
                            $title = $value['translations'][Yii::$app->language]['title'];
                        } else if (count($value['translations']) > 0) {
                            $title = array_values($value['translations'])[0]['title'];
                        } else continue;

                        $attributes[$key]['values'][$valueKey] = [
                            'title' => Yii::$app->utility->upperFirstLetter($title),
                            'count' => $bucket['doc_count']
                        ];
                    }
                }
            }
        }

        if (!in_array('online', $extraExceptions)) {
            $attributes['online'] = [
                'alias' => 'online',
                'title' => Yii::t('app', 'Online status'),
                'type' => 'checkbox',
                'checked' => $this->getCheckedValues('online'),
                'values' => ['online' => ['title' => Yii::t('app', 'Online')]]
            ];
        }

        return $attributes;
    }

    /**
     * @param null $attributeId
     * @return array
     */
    protected function getCheckedValues($attributeId = null)
    {
        $result = [];

        if ($attributeId !== null) {
            $queryParams = Yii::$app->request->queryParams;

            if (array_key_exists("at_{$attributeId}", $queryParams)) {
                $result = $queryParams["at_{$attributeId}"];
            }

            if (array_key_exists($attributeId, $queryParams)) {
                $result = $queryParams[$attributeId];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getSimilarCategories()
    {
        return $this->_similarCategories;
    }

    /**
     * @return string
     */
    protected function getEntityTitle()
    {
        if ($this->_entityTitle === null) {
            $this->_entityTitle = $this->category->translation->title;
        }
        return $this->_entityTitle;
    }

    /**
     * @return bool
     */
    protected function addRelevanceOrderQuery()
    {
        switch ($this->category->lvl) {
            case 0:
                $this->query->addOrderBy('root_score DESC');
                break;
            case 1:
                $this->query->addOrderBy('upper_category_score DESC');
                break;
            case 2:
            case 3:
                $this->query->addOrderBy('category_score DESC');
                break;
        }

        return true;
    }

    /**
     * @param array $queryParams
     * @param array $condition
     * @param string $relation
     * @return array
     */
    protected function queryParamsToConditions(array $queryParams, $condition = [], $relation = 'attrs')
    {
        foreach ($queryParams as $key => $queryParam) {
            $key = array_key_exists($key, $this->reservedFiltersToSimple) ? $this->reservedFiltersToSimple[$key] : $key;

            if (array_key_exists($key, $this->availableAttributes) && !in_array($key, ['city', 'online', 'secure'])) {
                $newCondition = $this->getApplyAttributesCondition($relation, $key, $queryParam);
                $condition['bool']['must'][] = $newCondition['bool']['must'];
            }
        }

        return $condition;
    }

    /**
     * @param $request
     * @param string $relation
     * @return array
     */
    protected function getSearchQuery($request, $relation = 'translation')
    {
        $request = $this->cureSearchString($request);
        return [
            'bool' => [
                'must' => [
                    0 => [
                        'bool' => [
                            'should' => ElasticConditionsHelper::getNestedSearchQuery($relation, ["{$relation}.title"], $request)
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $request
     * @param string $relation
     * @return array
     */
    protected function getProfileSearchQuery($request, $relation = 'translation')
    {
        $request = $this->cureSearchString($request);
        return [
            'bool' => [
                'must' => [
                    [
                        'bool' => [
                            'should' => ElasticConditionsHelper::getNestedSearchQuery($relation, ["{$relation}.title"], $request)
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $string
     * @return string
     */
    public function cureSearchString($string)
    {
        $string = strlen($string) > 4 ? $string : preg_replace('/\s/', '', $string);
        return trim(str_ireplace($this->searchStringExceptions, '', $string));
    }

    /**
     * @param $entity
     * @param $request
     * @return array
     */
    protected function getAttributeSearchQuery($entity = 'job', $request)
    {
        $request = $this->cureSearchString($request);
        return [
            'bool' => [
                'must' => [
                    [
                        'bool' => [
                            'should' => ElasticConditionsHelper::getNestedSearchQuery($entity, ["{$entity}.title"], $request)
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @param string $relation
     * @param $key
     * @param $queryParam
     * @return array
     */
    protected function getApplyAttributesCondition($relation = 'attrs', $key, $queryParam)
    {
        if (is_array($queryParam)) {
            $condition = [
                'bool' => []
            ];
            foreach ($queryParam as $param) {
                $condition['bool']['must'][] = [
                    'nested' => [
                        'path' => $relation,
                        'query' => [
                            'bool' => [
                                'must' => [
                                    ['term' => ["{$relation}.entity_alias" => $key]],
                                    ['term' => ["{$relation}.value_alias" => $param]]
                                ]
                            ],
                        ]
                    ]
                ];
            }
        } else {
            $condition = [
                'bool' => [
                    'must' => [
                        'nested' => [
                            'path' => $relation,
                            'query' => [
                                'bool' => [
                                    'must' => [
                                        ['term' => ["{$relation}.entity_alias" => $key]],
                                        ['term' => ["{$relation}.value_alias" => $queryParam]]
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $condition;
    }

    /**
     * @param string $relation
     * @param $param
     * @return array
     */
    protected function getCityCondition($relation = 'attrs', $param)
    {
        return ["{$relation}.attribute_id" => [
            "order" => "asc",
            "nested_path" => $relation,
            "nested_filter" => [
                ["term" => ["{$relation}.entity_alias" => "city"]],
                ["term" => ["{$relation}.value_alias" => $param]]
            ]
        ]];
    }

    /**
     * @param string $relation
     * @param $param
     * @return array
     */
    protected function getLanguageCondition($relation = 'lang', $param)
    {
        return ["{$relation}.locale" => [
            "order" => "asc",
            "nested_path" => $relation,
            "nested_filter" => [
                ["term" => ["{$relation}.locale" => $param[0] ?? strtolower(Yii::$app->params['languages'][Yii::$app->language])]],
            ]
        ]];
    }

    /**
     * @param $attributeId
     * @param int $limit
     * @return array
     */
    protected function getAttributeAggregationCondition($attributeId, $limit = 25)
    {
        return [
            'filter' => [
                'bool' => [
                    'must' => [
                        [
                            'term' => [
                                'attribute_id' => $attributeId
                            ]
                        ],
                    ]
                ]
            ],
            'aggs' => [
                'values' => [
                    'terms' => [
                        'field' => 'value_alias',
                        'size' => $limit
                    ],
                ]
            ]
        ];
    }

    /**
     * @param $attributeId
     * @param int $limit
     * @return array
     */
    protected function getAttributeNestedAggregationCondition($attributeId, $limit = 25)
    {
        return [
            'nested' => [
                'path' => 'attrs',
            ],
            'aggs' => [
                'attrs' => [
                    'filter' => [
                        'bool' => [
                            'must' => [
                                [
                                    'term' => [
                                        'attrs.attribute_id' => $attributeId
                                    ]
                                ],
                            ]
                        ]
                    ],
                    'aggs' => [
                        'values' => [
                            'terms' => [
                                'field' => 'attrs.value_alias',
                                'size' => $limit
                            ],
                        ]
                    ]
                ]

            ]
        ];
    }
}