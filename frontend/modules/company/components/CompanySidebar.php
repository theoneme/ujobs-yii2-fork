<?php

namespace frontend\modules\company\components;

use common\models\Company;
use common\models\Job;
use common\models\Page;
use common\models\UserPortfolio;
use common\modules\board\models\Product;
use frontend\modules\company\models\CompanyDescriptionForm;
use frontend\modules\company\models\CompanyStatusForm;
use Yii;
use yii\base\Widget;

/**
 * Class CompanySidebar
 * @package frontend\modules\account\components
 */
class CompanySidebar extends Widget
{
    /**
     * @var string
     */
    public $template = 'company-sidebar';
    /**
     * @var bool
     */
    public $editable = false;
    /**
     * @var Company
     */
    public $company;
    /**
     * @var array
     */
    public $menuItems = [];

    /**
     * @return string
     */
    public function run()
    {
        $isEditable = $this->editable;

        if ($isEditable === false) {
            $articlesCondition = ['page.status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]];
            $jobsCondition = ['job.status' => Job::STATUS_ACTIVE];
            $tendersCondition = ['job.status' => Job::STATUS_ACTIVE];
            $productsCondition = ['product.status' => Product::STATUS_ACTIVE];
            $productTendersCondition = ['product.status' => Product::STATUS_ACTIVE];
            $companyStatusForm = null;
            $companyDescriptionForm = null;
            $this->template = 'company-sidebar';
        } else {
            $articlesCondition = ['not', ['page.status' => Page::STATUS_DELETED]];
            $jobsCondition = ['not', ['job.status' => Job::STATUS_DELETED]];
            $tendersCondition = ['not', ['job.status' => Job::STATUS_DELETED]];
            $productsCondition = ['not', ['product.status' => Product::STATUS_DELETED]];
            $productTendersCondition = ['not', ['product.status' => Product::STATUS_DELETED]];
            $companyStatusForm = new CompanyStatusForm([
                'status' => $this->company->status_text
            ]);
            $companyDescriptionForm = new CompanyDescriptionForm([
                'description' => $this->company->translation->content ?? null,
                'short_description' => $this->company->translation->content_prev ?? null
            ]);
            $this->template = 'company-sidebar';
        }

        $jobsCount = Job::find()->where(['job.type' => 'job', 'job.user_id' => $this->company->user_id])->andWhere($jobsCondition)->groupBy('job.id')->count();
        $tendersCount = Job::find()->where(['type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'job.user_id' => $this->company->user_id])->andWhere($tendersCondition)->groupBy('job.id')->count();
        $productsCount = Product::find()->where(['type' => Product::TYPE_PRODUCT, 'product.user_id' => $this->company->user_id])->andWhere($productsCondition)->groupBy('product.id')->count();
        $productTendersCount = Product::find()->where(['type' => Product::TYPE_TENDER, 'product.user_id' => $this->company->user_id])->andWhere($productTendersCondition)->groupBy('product.id')->count();
        $articlesCount = Page::find()->where(['type' => Page::TYPE_ARTICLE, 'user_id' => $this->company->user_id])->andWhere($articlesCondition)->count();
        $portfoliosCount = UserPortfolio::find()->where(['user_id' => $this->company->user_id, 'version' => UserPortfolio::VERSION_NEW])->count();

        $additionalMenuItems = array_filter([
            'jobs' => $jobsCount
                ? ['link' => ['/account/profile/show', 'id' => $this->company->user_id, '#' => 'jobs'], 'label' => Yii::t('app', 'Services') . " ({$jobsCount})"]
                : false,
            'products' => $productsCount
                ? ['link' => ['/account/profile/show', 'id' => $this->company->user_id, '#' => 'products'], 'label' => Yii::t('app', 'Products') . " ({$productsCount})"]
                : false,
            'articles' => $articlesCount
                ? ['link' => ['/account/feed/show', 'id' => $this->company->user_id, '#' => 'articles'], 'label' => Yii::t('app', 'Articles') . " ({$articlesCount})"]
                : false,
        ]);
        $this->menuItems = array_filter($this->menuItems);
        if (count($this->menuItems) < 4 && count($additionalMenuItems)) {
            foreach ($additionalMenuItems as $key => $item) {
                if (count($this->menuItems) < 4 && !array_key_exists($key, $this->menuItems)) {
                    $this->menuItems[$key] = $item;
                }
            }
        }

        return $this->render($this->template, [
            'company' => $this->company,
            'jobsCount' => $jobsCount,
            'tendersCount' => $tendersCount,
            'productsCount' => $productsCount,
            'productTendersCount' => $productTendersCount,
            'portfoliosCount' => $portfoliosCount,
            'articlesCount' => $articlesCount,
            'menuItems' => $this->menuItems,
            'isEditable' => $isEditable,
            'companyStatusForm' => $companyStatusForm,
            'companyDescriptionForm' => $companyDescriptionForm
        ]);
    }
}