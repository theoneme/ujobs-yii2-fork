<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.01.2018
 * Time: 15:18
 */

use common\models\Company;
use frontend\modules\company\models\CompanyDescriptionForm;
use frontend\modules\company\models\CompanyStatusForm;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var Company $company
 * @var boolean $isEditable
 * @var array $menuItems
 * @var integer $jobsCount
 * @var integer $productsCount
 * @var integer $tendersCount
 * @var integer $productTendersCount
 * @var integer $portfoliosCount
 * @var integer $articlesCount
 * @var CompanyStatusForm $companyStatusForm
 * @var CompanyDescriptionForm $companyDescriptionForm
 */

$tariffIcon = '';
if ($company->user->activeTariff !== null) {
    $pathInfo = pathinfo($company->user->activeTariff->tariff->icon);
    $tariffIcon = Html::img($pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . '-profile.' . $pathInfo['extension'], ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]);
}
?>

    <div class="worker-info-content">
        <div class="worker-info-block">
            <div class="status-site">
                <?php if ($company->user->isOnline()) { ?>
                    <span class="status isonline">
                        <i class="fa fa-circle"></i>
                        <?= Yii::t('labels', 'Online') ?>
                    </span>
                <?php } else { ?>
                    <span class="status isoffline">
                        <i class="fa fa-circle"></i>
                        <?= Yii::t('labels', 'Offline') ?>
                    </span>
                <?php } ?>
            </div>
            <?php if ($company->logo) { ?>
                <div class="hw-avatar text-center"
                     style="background: url(<?= $company->getThumb() ?>); background-size: cover; margin: 0 auto;">
                    <?= $tariffIcon ?>
                </div>
            <?php } else { ?>
                <div class="hw-avatar text-center" style="margin: 0 auto">
                    <?= StringHelper::truncate(Html::encode($company->user->username), 1, '') ?>
                    <?= $tariffIcon ?>
                </div>
            <?php } ?>
            <div class="worker-name text-center"><?= Html::encode($company->getSellerName()) ?></div>
            <div class="worker-status text-center">
                <?php if ($isEditable === true) { ?>
                    <div id="current-company-status">
                        <span id="current-company-status-text">
                            <?= $company->status_text !== null ? Html::encode($company->status_text) : Yii::t('account', 'Change status text') ?>
                        </span>
                        <a href="#" data-action="change-status-text">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </div>
                    <div id="company-status-form-block">
                        <?php $form = ActiveForm::begin([
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                            'action' => Url::to(['/company/profile-ajax/update-status']),
                            'options' => [
                                'id' => 'company-status-form'
                            ]
                        ]) ?>
                        <?= $form->field($companyStatusForm, 'status')->textInput(['autofocus' => 'autofocus'])->label(false); ?>
                        <?= Html::a(Yii::t('account', 'Cancel'), '#', ['class' => 'grey-but text-center grey-small', 'data-action' => 'cancel-change-status']) ?>
                        <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php } else { ?>
                    <?= Html::encode($company->status_text) ?>
                <?php } ?>
            </div>
            <?php if (hasAccess($company->user_id)) { ?>
                <div class="worker-actions">
                    <?php if ($isEditable === true) { ?>
                        <?= Html::a(Yii::t('account', 'View As A Buyer'),
                            ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show-as-customer', 'id' => $company->id],
                            ['class' => 'button green no-size']
                        ) ?>
                    <?php } else { ?>
                        <?= Html::a(Yii::t('account', 'View As A Owner'),
                            ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show', 'id' => $company->id],
                            ['class' => 'button green no-size']
                        ) ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="worker-info-block">
            <div class="worker-card">
                <?php if ($company->getAddress() !== null) { ?>
                    <div class="wc-item location">
                        <div class="flex flex-justify-between">
                            <div>
                                <?= Yii::t('account', 'From') ?>
                            </div>
                            <div class="text-right">
                                <strong>
                                    <?= $company->getAddress() ?>
                                </strong>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="wc-item member">
                    <?= Yii::t('account', 'Member Since') ?>
                    <span class="text-right"><?= Yii::$app->formatter->asDate($company->user->created_at) ?></span>
                </div>
                <div class="wc-item response-time">
                    <?= Yii::t('account', 'Avg. Response Time') ?>
                    <span class="text-right"> <?= Yii::t('account', '{count, plural, one{# hour} other{# hours}}', ['count' => 1]) ?> </span>
                </div>
                <div class="wc-item delivery">
                    <?= Yii::t('account', 'Last Delivery') ?>
                    <span class="text-right"><?= Yii::t('account', 'about {count, plural, one{# hour} other{# hours}}', ['count' => 7]) ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="visible-xs">
        <ul class="menu-worker-mobile">
            <?php foreach ($menuItems as $menuItem) { ?>
                <li>
                    <?= Html::a($menuItem['label'], $menuItem['link'], ['class' => array_key_exists('active', $menuItem) ? 'active' : '']) ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="worker-info-mobile">
        <div class="worker-info-content">
            <?php if ($isEditable === true) { ?>
                <div class="worker-info-block">
                    <div>
                        <div class="worker-info-title">
                            <?= Yii::t('account', 'Settings') ?>:
                        </div>
                    </div>
                    <div>
                        <a class="action-link" href="<?= Url::to(['/account/service/public-profile-settings']) ?>">
                            <?= Yii::t('account', 'Public Profile Settings') ?>
                        </a>
                    </div>
                    <div>
                        <a class="action-link" href="<?= Url::to(['/account/service/account-settings']) ?>">
                            <?= Yii::t('account', 'Account Settings') ?>
                        </a>
                    </div>
                    <div>
                        <a class="action-link" href="<?= Url::to(['/account/payment/index']) ?>">
                            <?= Yii::t('account', 'Your balance') ?>
                        </a>
                    </div>
                </div>
            <?php } ?>
            <div class="worker-info-block" id="about">
                <div id="company-description">
                    <?php if ($isEditable === true) { ?>
                        <a class="edit-alias text-right edit-descr" href="#" data-action="change-description">
                            <?= Yii::t('account', 'Edit Description') ?>
                        </a>
                    <?php } ?>
                    <div class="worker-info-title">
                        <?= Yii::t('account', 'Description') ?>
                    </div>
                    <p class="prof-descr" id="current-company-description-text">
                        <?= nl2br(Html::encode($company->getDescription() ?? Yii::t('account', 'Company description is missing'))) ?>
                    </p>
                </div>
                <?php if ($isEditable === true) { ?>
                    <div id="company-description-form-block">
                        <?php $form = ActiveForm::begin([
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                            'action' => Url::to(['/company/profile-ajax/update-description']),
                            'options' => [
                                'id' => 'company-description-form'
                            ]
                        ]) ?>
                        <?= $form->field($companyDescriptionForm, 'short_description')->textarea(['autofocus' => 'autofocus']) ?>
                        <?= $form->field($companyDescriptionForm, "description")->textarea(['autofocus' => 'autofocus']); ?>
                        <?= Html::a(Yii::t('account', 'Cancel'), '#', ['class' => 'grey-but text-center grey-small', 'data-action' => 'cancel-change-description']) ?>
                        <?= Html::submitInput(Yii::t('account', 'Save'), ['class' => 'btn-small']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php } ?>
            </div>
            <div class="worker-info-block">
                <div>
                    <div class="worker-info-title">
                        <?= Yii::t('account', 'My offers') ?>
                    </div>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/company/job/show', 'id' => $company->id, '#' => 'job-list-header']) ?>">
                        <?= Yii::t('account', 'Services I offer') . ($jobsCount ? " ({$jobsCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/company/product/show', 'id' => $company->id, '#' => 'product-list-header']) ?>">
                        <?= Yii::t('account', 'Products I sell') . ($productsCount ? " ({$productsCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/company/tender/show', 'id' => $company->id, '#' => 'tender-list-header']) ?>">
                        <?= Yii::t('account', 'My service requests') . ($tendersCount ? " ({$tendersCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link anchor-scroll"
                       href="<?= Url::to(['/company/product-tender/show', 'id' => $company->id, '#' => 'ptender-list-header']) ?>">
                        <?= Yii::t('account', 'Products I`m ready to buy') . ($productTendersCount ? " ({$productTendersCount})" : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/company/portfolio/show', 'id' => $company->id, '#' => 'portfolio-list-header']) ?>">
                        <?= Yii::t('account', 'My portfolio') . ($portfoliosCount ? ' (' . $portfoliosCount . ')' : '') ?>
                    </a>
                </div>
                <div>
                    <a class="action-link"
                       href="<?= Url::to(['/company/profile/show', 'id' => $company->id, '#' => 'article-list-header']) ?>">
                        <?= Yii::t('account', 'My news feed') . ($articlesCount ? " ({$articlesCount})" : '') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php if ($isEditable === true) {
    $script = <<<JS
        let statusForm = $('#company-status-form'),
            descriptionForm = $('#company-description-form'),
            currentDescriptionBlock = $('#company-description'),
            currentDescriptionTextSpan = $('#current-company-description-text'),
            currentStatusBlock = $('#current-company-status'),
            currentStatusTextSpan = $('#current-company-status-text');

        $('[data-action="change-status-text"], [data-action="cancel-change-status"]').on('click', function() {
            statusForm.toggle();
            currentStatusBlock.toggle();
            
            return false;
        });
        
        $('[data-action="change-description"], [data-action="cancel-change-description"]').on('click', function() {
            descriptionForm.toggle();
            currentDescriptionBlock.toggle();
            
            return false;
        });

        statusForm.on('beforeSubmit', function() {
            let form = $(this),
                data = form.serialize(),
                url = form.attr('action');
            
            $.post(url, data, function(response) {
                if(response.success === true) {
                    statusForm.toggle();
                    currentStatusBlock.toggle();
                    currentStatusTextSpan.text(response.status);
                }
            });
            
            return false;
        }); 
        
        descriptionForm.on('beforeSubmit', function() {
            let form = $(this),
                data = form.serialize(),
                url = form.attr('action');
            
            $.post(url, data, function(response) {
                if(response.success === true) { 
                    descriptionForm.toggle();
                    currentDescriptionBlock.toggle();
                    currentDescriptionTextSpan.text(response.description);
                }
            });
            
            return false;
        });
JS;

    $this->registerJs($script);
}
