<?php

namespace frontend\modules\company;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\company\controllers';
}