<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\models\Company;
use frontend\assets\PortfolioAsset;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var Company $company
 * @var ActiveDataProvider $newPortfolioDataProvider
 * @var boolean $editable
 */
WorkerAsset::register($this);
SelectizeAsset::register($this);
PortfolioAsset::register($this);

?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($company->logo) { ?>
                            <?= Html::img($company->getThumb(), ['alt' => $company->getSellerName(),'title' => $company->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($company->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($company->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($company->user->isOnline()) { ?>
                                <span class="status isonline">
                                    <i class="fa fa-circle"></i>
                                    <?= Yii::t('labels', 'Online') ?>
                                </span>
                            <?php } else { ?>
                                <span class="status isoffline">
                                    <i class="fa fa-circle"></i>
                                    <?= Yii::t('labels', 'Offline') ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if (hasAccess($company->user_id)) { ?>
                    <div class="right-side">
                        <?php if ($editable === true) { ?>
                            <?= Html::a(Yii::t('account', 'View As A Buyer'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show-as-customer', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } else { ?>
                            <?= Html::a(Yii::t('account', 'View As A Owner'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= \frontend\modules\company\components\CompanySidebar::widget([
                    'company' => $company,
                    'editable' => $editable,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'portfolios' => $newPortfolioDataProvider->totalCount
                            ? ['link' => '#portfolios', 'label' => Yii::t('app', 'Portfolio') . " ({$newPortfolioDataProvider->totalCount})"]
                            : false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs hidden-xs">
                <?php if (!empty($company->banner)) { ?>
                    <div class="company-banner" style='background-image: url(<?= $company->getBanner() ?>)'>

                    </div>
                <?php } ?>
                <div class="gigs-title" id="portfolio-list-header">
                    <?= Yii::t('account', 'Portfolio of company `{company}`', ['company' => $company->getSellerName()]) ?>
                    <?= Html::a(Yii::t('account', 'Post portfolio'), ['/account/portfolio/create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                </div>
                <?= ListView::widget([
                    'dataProvider' => $newPortfolioDataProvider,
                    'viewParams' => ['editable' => $editable],
                    'itemView' => '@frontend/modules/account/views/portfolio/listview/portfolio-assymetric',
                    'options' => [
                        'class' => 'portfolio-grid',
                        'id' => 'pro-portfolios',
                    ],
                    'itemOptions' => [
                        'class' => 'pg-item'
                    ],
                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have portfolio") . '</div>',
                    'layout' => "{items}"
                ]) ?>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="portfolios">
                        <div class="gigs-title"><?= Yii::t('account', 'Portfolio of company `{company}`', ['company' => $company->getSellerName()]) ?></div>
                        <?= ListView::widget([
                            'dataProvider' => $newPortfolioDataProvider,
                            'viewParams' => ['editable' => $editable],
                            'itemView' => '@frontend/modules/account/views/portfolio/listview/portfolio-assymetric',
                            'options' => [
                                'class' => 'portfolio-grid',
                                'id' => 'pro-portfolios-mobile',
                            ],
                            'itemOptions' => [
                                'class' => 'pg-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger"
                            id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn"
                            id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="album-modal">

    </div>

<?php
$cartUrl = Url::to(['/store/cart/add-offer']);
$attributeListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$cartText = Yii::t('account', 'Individual order added to cart');

$script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    var masonryOrient = true;
    if($('body').hasClass('rtl')){
        masonryOrient = false;
    }
    menuMobileItems.css('width', perc);
    
    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });
    
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x"
    });
    $('.portfolio-grid').masonry({
        itemSelector: '.pg-item',
        originLeft: masonryOrient
    });
 
    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script, \yii\web\View::POS_LOAD);