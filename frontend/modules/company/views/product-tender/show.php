<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\models\Company;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var Company $company
 * @var ActiveDataProvider $productsDataProvider
 * @var boolean $editable
 */

WorkerAsset::register($this);
SelectizeAsset::register($this);

?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($company->logo) { ?>
                            <?= Html::img($company->getThumb('catalog'), ['alt' => $company->getSellerName(),'company' => $company->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($company->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($company->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($company->user->isOnline()) { ?>
                                <span class="status isonline">
                                    <i class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?>
                                </span>
                            <?php } else { ?>
                                <span class="status isoffline">
                                    <i class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if (hasAccess($company->user_id)) { ?>
                    <div class="right-side">
                        <?php if ($editable === true) { ?>
                            <?= Html::a(Yii::t('account', 'View As A Buyer'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show-as-customer', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } else { ?>
                            <?= Html::a(Yii::t('account', 'View As A Owner'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= \frontend\modules\company\components\CompanySidebar::widget([
                    'company' => $company,
                    'editable' => $editable,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'products' => ['link' => '#products', 'label' => Yii::t('app', 'Products') . " ({$productsDataProvider->totalCount})"],
                        'reviews' => false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs hidden-xs">
                <?php if (!empty($company->banner)) { ?>
                    <div class="company-banner" style='background-image: url(<?= $company->getBanner() ?>)'>

                    </div>
                <?php } ?>
                <div class="gigs-title" id="ptender-list-header">
                    <?= Yii::t('account', 'Product requests from company `{company}`', ['company' => $company->getSellerName()]) ?>
                    <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                </div>
                <?php Pjax::begin(['enablePushState' => true, 'id' => 'product-tenders-pjax', 'scrollTo' => 1]); ?>
                <?= ListView::widget([
                    'dataProvider' => $productsDataProvider,
                    'itemView' => $editable === true
                        ? '@frontend/modules/account/views/profile/listview/product'
                        : '@frontend/modules/filter/views/product-list',
                    'viewParams' => ['editable' => $editable],
                    'options' => [
                        'class' => 'max-width gigs-list relative',
                        'id' => 'pro-jobs',
                    ],
                    'itemOptions' => [
                        'class' => 'bf-item'
                    ],
                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active product requests") . '</div>',
                    'layout' => "{items}"
                ]) ?>
                <?= LinkPager::widget([
                    'pagination' => $productsDataProvider->pagination,
                ]) ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="products" class="gigs-title">
                        <?= Yii::t('account', 'Product requests from company `{company}`', ['company' => $company->getSellerName()]) ?>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $productsDataProvider,
                        'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                        'options' => [
                            'class' => 'max-width gigs-list relative',
                            'id' => 'pro-jobs-mobile',
                        ],
                        'itemOptions' => [
                            'class' => 'mob-work-item'
                        ],
                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active product requests") . '</div>',
                        'layout' => "{items}"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger"
                            id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn"
                            id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    
    menuMobileItems.css('width', perc);
    
    $("#product-requests-pjax").on('pjax:complete', function(e) {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 200
        }, 500);
    });
    
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x" // horizontal scrollbar
    });
    
    $(document).on('click', '.menu-worker-mobile a', function() {
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        return false;
    });

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });
    
    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script);