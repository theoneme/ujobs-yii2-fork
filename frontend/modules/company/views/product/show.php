<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\models\Company;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var Company $company
 * @var $sellingProductsDataProvider ActiveDataProvider
 * @var $soldProductsDataProvider ActiveDataProvider
 * @var boolean $editable
 * @var $totalSoldProducts integer
 */

WorkerAsset::register($this);
SelectizeAsset::register($this);

$totalProducts = $sellingProductsDataProvider->totalCount + $soldProductsDataProvider->totalCount;
?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($company->logo) { ?>
                            <?= Html::img($company->getThumb('catalog'), ['alt' => $company->getSellerName(),'title' => $company->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($company->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($company->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($company->user->isOnline()) { ?>
                                <span class="status isonline">
                                    <i class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?>
                                </span>
                            <?php } else { ?>
                                <span class="status isoffline">
                                    <i class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if (hasAccess($company->user_id)) { ?>
                    <div class="right-side">
                        <?php if ($editable === true) { ?>
                            <?= Html::a(Yii::t('account', 'View As A Buyer'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show-as-customer', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } else { ?>
                            <?= Html::a(Yii::t('account', 'View As A Owner'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= \frontend\modules\company\components\CompanySidebar::widget([
                    'company' => $company,
                    'editable' => $editable,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'products' => ['link' => '#products', 'label' => Yii::t('app', 'Products') . " ({$sellingProductsDataProvider->totalCount})"],
                        'reviews' => false,
                    ]
                ]) ?>
            </div>
            <div class="worker-gigs hidden-xs">
                <?php if (!empty($company->banner)) { ?>
                    <div class="company-banner" style='background-image: url(<?= $company->getBanner() ?>)'>

                    </div>
                <?php } ?>
                <div class="gigs-title no-padding prod-with-tabs" id="product-list-header">
                    <div class="flex flex-justify-between">
                        <div class="prod-tit">
                            <?= Yii::t('account', 'Products from company `{company}`', ['company' => $company->getSellerName()]) ?>
                        </div>
                        <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0]) ?>
                        <ul class="nav nav-tabs product-tabs">
                            <li class="active">
                                <?= Html::a(
                                    Yii::t('account', '{amount} on sale', ['amount' => $sellingProductsDataProvider->totalCount]),
                                    '#selling-products',
                                    ['data-toggle' => 'tab']
                                )?>
                            </li>
                            <li>
                                <?= Html::a(
                                    Yii::t('account', '{amount} sold', ['amount' => $totalSoldProducts]),
                                    '#sold-products',
                                    ['data-toggle' => 'tab']
                                )?>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php Pjax::begin(['enablePushState' => true, 'id' => 'products-pjax', 'scrollTo' => 1]); ?>
                    <div class="tab-content">
                        <div id="selling-products" class="tab-pane fade in active">
                            <?= ListView::widget([
                                'dataProvider' => $sellingProductsDataProvider,
                                'itemView' => $editable === true
                                    ? '@frontend/modules/account/views/profile/listview/product'
                                    : '@frontend/modules/filter/views/product-list',
                                'viewParams' => ['editable' => $editable],
                                'options' => [
                                    'class' => 'max-width gigs-list relative',
                                    'id' => 'pro-selling-products'
                                ],
                                'itemOptions' => [
                                    'class' => 'bf-item'
                                ],
                                'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any Products on sale") . '</div>',
                                'layout' => "{items}"
                            ]) ?>
                            <?= LinkPager::widget([
                                'pagination' => $sellingProductsDataProvider->pagination,
                            ]) ?>
                        </div>
                        <div id="sold-products" class="tab-pane fade">
                            <?= ListView::widget([
                                'dataProvider' => $soldProductsDataProvider,
                                'itemView' => $editable === true
                                    ? '@frontend/modules/account/views/profile/listview/product'
                                    : '@frontend/modules/filter/views/product-list',
                                'viewParams' => ['editable' => $editable],
                                'options' => [
                                    'class' => 'max-width gigs-list relative',
                                    'id' => 'pro-sold-products'
                                ],
                                'itemOptions' => [
                                    'class' => 'bf-item'
                                ],
                                'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any sold Products") . '</div>',
                                'layout' => "{items}"
                            ]) ?>
                            <?= LinkPager::widget([
                                'pagination' => $soldProductsDataProvider->pagination,
                            ]) ?>
                        </div>
                    </div>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <div id="products" class="gigs-title">
                        <?= Yii::t('account', 'Products from company `{company}`', ['company' => $company->getSellerName()]) ?>
                    </div>
                    <?php if ($totalProducts) { ?>
                        <div id="products" class="gigs-title no-padding prod-with-tabs">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit">
                                    <?= Yii::t('account', 'Products from company `{company}`', ['company' => $company->getSellerName()]) ?>
                                </div>
                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingProductsDataProvider->totalCount]),
                                            '#selling-products-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldProducts]),
                                            '#sold-products-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'products-pjax', 'scrollTo' => 1]); ?>
                            <div class="tab-content">
                                <div id="selling-products-mob" class="tab-pane fade in active">
                                    <?= ListView::widget([
                                        'dataProvider' => $sellingProductsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                                        'options' => [
                                            'class' => 'gigs-list relative',
                                            'id' => 'pro-selling-products-mob'
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any Products on sale") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $sellingProductsDataProvider->pagination,
                                    ]) ?>
                                </div>
                                <div id="sold-products-mob" class="tab-pane fade">
                                    <?= ListView::widget([
                                        'dataProvider' => $soldProductsDataProvider,
                                        'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                                        'options' => [
                                            'class' => 'gigs-list relative',
                                            'id' => 'pro-sold-products-mob'
                                        ],
                                        'itemOptions' => [
                                            'class' => 'mob-work-item'
                                        ],
                                        'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "Looks like this user does not have any sold Products") . '</div>',
                                        'layout' => "{items}"
                                    ]) ?>
                                    <?= LinkPager::widget([
                                        'pagination' => $soldProductsDataProvider->pagination,
                                    ]) ?>
                                </div>
                            </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger"
                            id="delete"><?= Yii::t('account', 'Delete') ?></button>
                    <button type="button" data-dismiss="modal" class="btn"
                            id="cancelform"><?= Yii::t('account', 'Cancel') ?></button>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    
    menuMobileItems.css('width', perc);
    
    $("#products-pjax").on('pjax:complete', function(e) {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 200
        }, 500);
    });
    
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x" // horizontal scrollbar
    });
    
    $(document).on('click', '.menu-worker-mobile a', function() {
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        return false;
    });

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });
    
    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
JS;

$this->registerJs($script);