<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:23
 */

use common\components\CurrencyHelper;
use common\models\Company;
use common\modules\store\models\Currency;
use frontend\assets\CatalogAsset;
use frontend\assets\SelectizeAsset;
use frontend\assets\WorkerAsset;
use frontend\modules\account\models\ProfileJobSearch;
use frontend\modules\company\components\CompanySidebar;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var Company $company
 * @var ActiveDataProvider $sellingJobsDataProvider
 * @var ActiveDataProvider $soldJobsDataProvider
 * @var boolean $editable
 * @var $totalSoldJobs integer
 * @var $categories array
 * @var $tags array
 * @var $priceRange array
 * @var $searchModel ProfileJobSearch
 */

WorkerAsset::register($this);
SelectizeAsset::register($this);
CatalogAsset::register($this);

$totalJobs = $sellingJobsDataProvider->totalCount + $soldJobsDataProvider->totalCount;
?>
    <div class="worker-mobile-head">
        <div class="container-fluid">
            <div class="wmh-content">
                <div class="left-side">
                    <div class="wmh-avatar">
                        <?php if ($company->logo) { ?>
                            <?= Html::img($company->getThumb('catalog'), ['alt' => $company->getSellerName(),'title' => $company->getSellerName()]) ?>
                        <?php } else { ?>
                            <div class="hw-avatar-new text-center">
                                <?= StringHelper::truncate(Html::encode($company->user->username), 1, '') ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="wmh-info">
                        <div class="wmh-name"><?= Html::encode($company->getSellerName()) ?></div>
                        <div class="wmh-status">
                            <?php if ($company->user->isOnline()) { ?>
                                <span class="status isonline">
                                    <i class="fa fa-circle"></i><?= Yii::t('labels', 'Online') ?>
                                </span>
                            <?php } else { ?>
                                <span class="status isoffline">
                                    <i class="fa fa-circle"></i><?= Yii::t('labels', 'Offline') ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if (hasAccess($company->user_id)) { ?>
                    <div class="right-side">
                        <?php if ($editable === true) { ?>
                            <?= Html::a(Yii::t('account', 'View As A Buyer'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show-as-customer', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } else { ?>
                            <?= Html::a(Yii::t('account', 'View As A Owner'),
                                ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/show', 'id' => $company->id],
                                ['class' => 'button green no-size']
                            ) ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="worker-info">
            <div class="worker-side">
                <?= CompanySidebar::widget([
                    'company' => $company,
                    'editable' => $editable,
                    'menuItems' => [
                        'about' => ['link' => '#about', 'label' => Yii::t('app', 'About'), 'active' => true],
                        'jobs' => ['link' => '#jobs', 'label' => Yii::t('app', 'Jobs') . " ({$sellingJobsDataProvider->totalCount})"],
                        'reviews' => false,
                    ]
                ]) ?>
                <div style="padding: 20px;">
                    <div class="text-right">
                        <?= Html::a(Yii::t('app', 'Clear All'), Url::canonical()); ?>
                    </div>
                    <div class="checked-filter clearfix">
                        <?php
                        if (!empty($searchModel->tags)) {
                            foreach ($searchModel->tags as $id => $tag) {
                                if (isset($tags[$tag])) {
                                    echo Html::a(
                                        "<i class='fa fa-times' aria-hidden='true'></i>" . $tags[$tag],
                                        Url::current(['tags' => array_replace($searchModel->tags, [$id => null])])
                                    ) ;
                                }
                            }
                        }
                        if (!empty($searchModel->price['min'])) {
                            echo Html::a(
                                "<i class='fa fa-times' aria-hidden='true'></i> > " . CurrencyHelper::format(Yii::$app->params['app_currency_code'], $searchModel->price['min']),
                                Url::current(['price' => array_replace($searchModel->price, ['min' => null])])
                            ) ;
                        }
                        if (!empty($searchModel->price['max'])) {
                            echo Html::a(
                                "<i class='fa fa-times' aria-hidden='true'></i> < " . CurrencyHelper::format(Yii::$app->params['app_currency_code'], $searchModel->price['max']),
                                Url::current(['price' => array_replace($searchModel->price, ['max' => null])])
                            ) ;
                        }
                        ?>
                    </div>
                    <?php if (count($categories)) { ?>
                        <ul class="cats no-markers">
                            <li>
                                <h2 class="text-left"><?= Yii::t('app', 'Categories') ?></h2>
                                <div class="ml <?= (count($categories) > 6) ? "hide-filter hf-categories" : "" ?>">
                                    <ul>
                                        <?php foreach ($categories as $id => $label) { ?>
                                            <li>
                                                <?= Html::a($label, Url::current(['category_id' => $id])) ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php if (count($categories) > 6) {
                                    echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($categories) - 6)]), null, [
                                        'class' => 'more-less more-cats',
                                        'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($categories) - 6)]),
                                        'data-less' => Yii::t('app', 'See less'),
                                    ]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    <?php } ?>
                    <?= Html::beginForm(Url::canonical(), 'get', [
                        'id' => 'filter_form',
                    ]) ?>
                        <?php if (!empty($tags)) { ?>
                            <div class="aside-block">
                                <div class="fi-title"><?= Yii::t('app', 'Tags') ?></div>
                                <div class="ml hide-filter">
                                    <?php foreach ($tags as $alias => $label) { ?>
                                        <div class="chover">
                                            <?= Html::input('checkbox', "tags[]", $alias, [
                                                'id' => "tag_{$alias}",
                                                'checked' => in_array($alias, $searchModel->tags),
                                            ]); ?>
                                            <?= Html::label('<span class="ctext">' . $label . '</span>', 'tag_' . $alias)?>
                                        </div>
                                    <?php } ?>
                                </div>

                                <?php if (count($tags) > 4) { ?>
                                    <a class="more-less watch-more" href="#"><?= Yii::t('filter', 'See more') ?></a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if (isset($priceRange['min'], $priceRange['max']) && $priceRange['min'] !== $priceRange['max']) { ?>
                            <?= Html::hiddenInput('price[min]', $searchModel->price['min'] ?? null, ['id' => 'min-price']) ?>
                            <?= Html::hiddenInput('price[max]', $searchModel->price['max'] ?? null, ['id' => 'max-price']) ?>
                            <div class="aside-block">
                                <div class="fi-title"><?= Yii::t('filter', 'Price') ?></div>
                                <div class="flex">
                                    <div class="price-label-wrap">
                                        <input class="price-label text-center" id="min-price-slider" value="<?= $priceRange['min'] ?>">
                                    </div>
                                    <div class="price-label-wrap">
                                        <input class="price-label text-center" id="max-price-slider" value="<?= $priceRange['max'] ?>">
                                    </div>
                                </div>

                                <div class="noUi-price-over">
                                    <div id="slider-price"></div>
                                </div>
                            </div>
                        <?php } ?>
                    <?= Html::endForm() ?>
                </div>
            </div>
            <div class="worker-gigs hidden-xs">
                <?php if (!empty($company->banner)) { ?>
                    <div class="company-banner" style='background-image: url(<?= $company->getBanner() ?>)'>

                    </div>
                <?php } ?>
                <div class="gigs-title no-padding prod-with-tabs" id="job-list-header">
                    <div class="flex flex-justify-between">
                        <div class="prod-tit">
                            <?= Yii::t('account', 'Services from company `{company}`', ['company' => $company->getSellerName()]) ?>
                        </div>
                        <?php if ($editable === true) { ?>
                            <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], [
                                'class' => 'btn-middle revert profile-entity-create', 'data-pjax' => 0
                            ]) ?>
                        <?php } ?>
                        <ul class="nav nav-tabs product-tabs">
                            <li class="active">
                                <?= Html::a(
                                    Yii::t('account', '{amount} on sale', ['amount' => $sellingJobsDataProvider->totalCount]),
                                    '#selling-jobs',
                                    ['data-toggle' => 'tab']
                                )?>
                            </li>
                            <li>
                                <?= Html::a(
                                    Yii::t('account', '{amount} sold', ['amount' => $totalSoldJobs]),
                                    '#sold-jobs',
                                    ['data-toggle' => 'tab']
                                )?>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php Pjax::begin(['enablePushState' => true, 'id' => 'jobs-pjax']); ?>
                <div class="tab-content">
                    <div id="selling-jobs" class="tab-pane fade in active">
                        <?= ListView::widget([
                            'dataProvider' => $sellingJobsDataProvider,
                            'itemView' => $editable === true
                                ? '@frontend/modules/account/views/profile/listview/job'
                                : '@frontend/modules/filter/views/job-list',
                            'viewParams' => ['editable' => $editable],
                            'options' => [
                                'class' => 'max-width gigs-list relative',
                                'id' => 'pro-selling-jobs',
                            ],
                            'itemOptions' => [
                                'class' => 'bf-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $sellingJobsDataProvider->pagination,
                        ]) ?>
                    </div>
                    <div id="sold-jobs" class="tab-pane fade">
                        <?= ListView::widget([
                            'dataProvider' => $soldJobsDataProvider,
                            'itemView' => $editable === true
                                ? '@frontend/modules/account/views/profile/listview/job'
                                : '@frontend/modules/filter/views/job-list',
                            'viewParams' => ['editable' => $editable],
                            'options' => [
                                'class' => 'max-width gigs-list relative',
                                'id' => 'pro-sold-jobs',
                            ],
                            'itemOptions' => [
                                'class' => 'bf-item'
                            ],
                            'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                            'layout' => "{items}"
                        ]) ?>
                        <?= LinkPager::widget([
                            'pagination' => $soldJobsDataProvider->pagination,
                        ]) ?>
                    </div>
                </div>
                <?php Pjax::end(); ?>
            </div>
            <div class="worker-info-mobile visible-xs">
                <div class="worker-gigs">
                    <?php if ($totalJobs) { ?>
                        <div id="jobs" class="gigs-title no-padding prod-with-tabs">
                            <div class="flex flex-justify-between">
                                <div class="prod-tit">
                                    <?= Yii::t('account', 'Services from company `{company}`', ['company' => $company->getSellerName()]) ?>
                                </div>
                                <ul class="nav nav-tabs product-tabs">
                                    <li class="active">
                                        <?= Html::a(
                                            Yii::t('account', '{amount} on sale', ['amount' => $sellingJobsDataProvider->totalCount]),
                                            '#selling-jobs-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                    <li>
                                        <?= Html::a(
                                            Yii::t('account', '{amount} sold', ['amount' => $totalSoldJobs]),
                                            '#sold-jobs-mob',
                                            ['data-toggle' => 'tab']
                                        )?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php Pjax::begin(['enablePushState' => true, 'id' => 'jobs-pjax', 'scrollTo' => 1]); ?>
                        <div class="tab-content">
                            <div id="selling-jobs-mob" class="tab-pane fade in active">
                                <?= ListView::widget([
                                    'dataProvider' => $sellingJobsDataProvider,
                                    'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                                    'options' => [
                                        'class' => 'max-width gigs-list relative',
                                        'id' => 'pro-selling-jobs-mobile',
                                    ],
                                    'itemOptions' => [
                                        'class' => 'mob-work-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $sellingJobsDataProvider->pagination,
                                ]) ?>
                            </div>
                            <div id="sold-jobs-mob" class="tab-pane fade">
                                <?= ListView::widget([
                                    'dataProvider' => $soldJobsDataProvider,
                                    'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                                    'options' => [
                                        'class' => 'max-width gigs-list relative',
                                        'id' => 'pro-sold-jobs-mobile',
                                    ],
                                    'itemOptions' => [
                                        'class' => 'mob-work-item'
                                    ],
                                    'emptyText' => '<div class="no-gigs text-center">' . Yii::t('account', "It seems this user does not have any active Jobs") . '</div>',
                                    'layout' => "{items}"
                                ]) ?>
                                <?= LinkPager::widget([
                                    'pagination' => $soldJobsDataProvider->pagination,
                                ]) ?>
                            </div>
                        </div>
                        <?php Pjax::end(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="form-title"><?= Yii::t('account', 'Are you sure?') ?></div>
                </div>
                <div class="modal-footer" style="padding-bottom: 0">
                    <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">
                        <?= Yii::t('account', 'Delete') ?>
                    </button>
                    <button type="button" data-dismiss="modal" class="btn" id="cancelform">
                        <?= Yii::t('account', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    let menuMobileItems = $('.menu-worker-mobile li');
    let perc = 100 / menuMobileItems.length + '%';
    
    menuMobileItems.css('width', perc);
    
    $("#jobs-pjax").on('pjax:complete', function(e) {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 200
        }, 500);
    });
    
    $(".menu-worker-mobile").mCustomScrollbar({
        axis: "x" // horizontal scrollbar
    });
    
    $(document).on('click', '.menu-worker-mobile a', function() {
        let el = $(this).attr('href');
        
        $('.menu-worker-mobile a').removeClass('active');
        $(this).addClass('active');
        $('html,body').animate({
            scrollTop: $(el).offset().top
        }, 800);
        return false;
    });

    $(window).scroll(function() {
        let top = $('.worker-card').offset().top;
        if ($(window).width() > 992) {
            if ($(document).scrollTop() > top) {
                $('.worker-mobile-head').css('display', 'block');
            } else {
                $('.worker-mobile-head').css('display', 'none');
            }
        }
    });
    
    let anchor = window.location.hash;
    if (anchor && $(anchor).length) {
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top - 155
        }, 800);
    }
    
    $(document).on('change', '#filter_form input:not(.price-label):not([name=_csrf-frontend])', function() {
        $('#filter_form').submit();
    });
JS;

$this->registerJs($script);
if (isset($priceRange['min'], $priceRange['max']) && $priceRange['min'] !== $priceRange['max']) {
    $currency = Currency::getDb()->cache(function ($db) {
        return Currency::find()->where(['code' => Yii::$app->params['app_currency_code']])->one();
    });
    $leftCurrencySymbol = $currency->symbol_left;
    $rightCurrencySymbol = $currency->symbol_right;
    $minPrice = $priceRange['min'];
    $maxPrice = $priceRange['max'];
    $minSliderPoint = !empty($searchModel->price['min']) ? $searchModel->price['min'] : $minPrice;
    $maxSliderPoint = !empty($searchModel->price['max']) ? $searchModel->price['max'] : $maxPrice;
    $priceBorder1 = $minPrice + (int)($maxPrice - $minPrice) * 0.1;
    $priceBorder2 = $minPrice + (int)($maxPrice - $minPrice) * 0.3;

    $script = <<<JS
    let priceSlider = $('#slider-price').get(0),
        inputs = [$('#min-price-slider').get(0), $('#max-price-slider').get(0)],
        sliderTimer = null;

    function updatePriceLabels(min, max) {
        $('#min-price-slider').val(min);
        $('#max-price-slider').val(max);

        if(typeof min === 'string') {
            min = min.replace(/[^0-9]/g, '');
        }
        if(typeof max === 'string') {
            max = max.replace(/[^0-9]/g, '');
        }
        $('#min-price').val(min);
        $('#max-price').val(max);
    }

    function setSliderHandle(instance, i, value) {
        let r = [null, null];
        r[i] = value;

        instance.noUiSlider.set(r);

        let values = instance.noUiSlider.get();
        updatePriceLabels(values[0], values[1]);

		clearTimeout(sliderTimer);
        sliderTimer = setTimeout(function() {
            $('#filter_form').submit();
        }, 300);
    }

    function initPriceSlider(instance, inputs) {
        console.log(instance);
        noUiSlider.create(instance, {
            connect: true,
            behaviour: 'tap',
            start: [$minSliderPoint, $maxSliderPoint],
            range: {
                'min': [$minPrice],
                '60%': [$priceBorder1],
                '80%': [$priceBorder2],
                'max': [$maxPrice]
            },
            format: {
                to: function(value) {
                    result = '';
                    if(value === {$minPrice}) {
                        result = '<';
                    }
                    if(value === {$maxPrice}) {
                        result = '>';
                    }
                    
                    return result + ' {$leftCurrencySymbol}' + parseInt(value) + '{$rightCurrencySymbol}';
                },
                from: function(value) {
                    return parseInt(value.replace(/[^0-9]/g, ''));
                },
            }
        });

        instance.noUiSlider.on('end', function(values) {
            updatePriceLabels(values[0], values[1]);

            clearTimeout(sliderTimer);
            sliderTimer = setTimeout(function() {
                $('#filter_form').submit();
            }, 300);
        });

        instance.noUiSlider.on('update', function(values, handle) {
            inputs[handle].value = values[handle];
        });

        inputs.forEach(function(input, index){
            input.addEventListener('change', function() {
                setSliderHandle(instance, index, this.value);
            });
        });
    }

    initPriceSlider(priceSlider, inputs);
JS;

    $this->registerJs($script);
} ?>
