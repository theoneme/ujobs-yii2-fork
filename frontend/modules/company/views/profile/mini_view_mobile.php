<?php

use common\models\user\Profile;
use common\models\user\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var User $user
 * @var array $accessInfo
 */
?>
<div class="work-section visible-xs greybg">
    <div class="mobwork-info-head clearfix">
        <div class="left">
            <div class="mobwork-avatar text-center">
                <?= Html::a(
                    Html::img($user->getThumb('catalog')) . (!empty($user->activeTariff) ? Html::img($user->activeTariff->tariff->icon, ['class' => 'tariff-icon']) : ''),
                    $user->getSellerUrl()
                ) ?>
            </div>
            <div class="mobwork-name">
                <div class="mob-author">
                    <?= Html::a(Html::encode($user->getSellerName()), $user->getSellerUrl(), ['class' => 'author-name']) ?>
                </div>
            </div>
        </div>
        <div class="mob-contactuser">
            <?php
            if (isGuest()) {
                echo Html::a(Yii::t('account', 'Contact Me'), null, [
                    'class' => 'button green middle text-center',
                    'data-toggle' => 'modal',
                    'data-target' => '#ModalLogin'
                ]);
            } else {
                if ($accessInfo['allowed'] === true) {
                    echo Html::a(Yii::t('account', 'Contact Me'), ['/account/inbox/start-conversation', 'user_id' => $user->id], [
                        'class' => 'button green middle text-center'
                    ]);
                } else {
                    echo Html::a(Yii::t('account', 'Contact Me'), null, [
                        'class' => 'button green middle text-center',
                        'data-toggle' => 'modal',
                        'data-target' => '#tariffModal'
                    ]);
                }
            } ?>
        </div>
    </div>
    <div class="mob-more-info">
        <div class="mob-bio">
            <p><?= nl2br(Html::encode($user->company->getShortDescription())) ?></p>
        </div>
        <div class="mob-user-props">
            <?php if ($user->company->getAddress()) { ?>
                <div class="wc-item location">
                    <?= Yii::t('account', 'From') ?>
                    <span class="text-right"><?= $user->company->getAddress() ?></span>
                </div>
            <?php } ?>
            <div class="wc-item response-time">
                <?= Yii::t('account', 'Avg. Response Time') ?> <span class="text-right"> <?= Yii::t('account', '{count, plural, one{# hour} other{# hours}}', ['count' => 1])?> </span>
            </div>
        </div>
        <!--                        <a class="seller-job" href="">-->
        <?php //=Yii::t('app', 'View seller services'); ?><!--</a>-->
    </div>
    <a class="mob-show-info" href="#"
       data-expanded="0"><?= Yii::t('app', 'See more') ?></a>
</div>