<?php

use common\models\user\Profile;
use common\models\user\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var User $user
 * @var array $accessInfo
 */
$tariffIcon = '';
if (!empty($user->activeTariff)) {
    $pathInfo = pathinfo($user->activeTariff->tariff->icon);
    $tariffIcon = Html::img($pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . '-profile.' . $pathInfo['extension'], ['class' => 'tariff-icon']);
}
?>
<div class="short-author">
    <div class="status-site">
        <?php if ($user->isOnline()) { ?>
            <span class="status isonline"><i class="fa fa-circle"></i><?= Yii::t('account', 'Online') ?></span>
        <?php } else { ?>
            <span class="status isoffline"><i class="fa fa-circle"></i><?= Yii::t('account', 'Offline') ?></span>
        <?php } ?>
    </div>
    <div class="author-top text-center">
        <div class="author-photo">
            <?= Html::a(
                Html::img($user->getThumb('catalog'), ['alt' => $user->getSellerName(), 'title' => $user->getSellerName()]) . $tariffIcon,
                $user->getSellerUrl()
            ) ?>
        </div>
        <?= Html::a(Html::encode($user->getSellerName()), $user->getSellerUrl(), ['class' => 'author-name']) ?>
        <!--<div class="author-medal">Top Rated Seller</div>-->
    </div>
    <div class="author-about">
        <?php if ($user->company->getAddress()) { ?>
            <div class="wc-item location">
                <div class="flex">
                    <div>
                        <?= Yii::t('account', 'From') ?>
                    </div>
                    <div class="text-right">
                        <strong>
                            <?= $user->company->getAddress() ?>
                        </strong>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="author-intro">
        <p class="short-descr-desktop">
            <?= $user->company->getShortDescription() ?>
        </p>
        <?php if ($user->company->getShortDescription()) { ?>
            <a class="mob-show-info" data-expanded="0" href="#"><?= Yii::t('app', 'See more') ?></a>
        <?php } ?>
    </div>
    <!--<a class="other-products">
        <?= Yii::t('app', 'Other offers from the seller'); ?>
    </a>-->
    <div class="author-contact text-center">
        <?php if (isGuest()) {
            echo Html::a(Yii::t('account', 'Contact Me'), null, [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-target' => '#ModalLogin'
            ]);
        } else {
            if ($accessInfo['allowed'] === true) { ?>
                <?= Html::a(Yii::t('account', 'Contact Me'), ['/account/inbox/start-conversation', 'user_id' => $user->id], [
                    'class' => 'btn-big'
                ]); ?>
            <?php } else { ?>
                <?= Html::a(Yii::t('account', 'Contact Me'), null, [
                    'class' => 'btn-big',
                    'data-toggle' => 'modal',
                    'data-target' => '#tariffContactModal'
                ]); ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>
