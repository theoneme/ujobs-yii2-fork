<?php

use common\helpers\FileInputHelper;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\components\GmapsInputWidget;
use frontend\components\WizardSteps;
use frontend\modules\company\models\PostCompanyForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostCompanyForm
 * @var $exampleModel common\models\Job
 * @var $action string
 * @var $currencySymbols array
 * @var $currencies array
 * @var $measures array
 */
CropperAsset::register($this);
SelectizeAsset::register($this);
$this->title = Yii::t('app', 'Create Company');
?>

<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('app', 'Description'),
    2 => Yii::t('app', 'Photo and details'),
    4 => Yii::t('app', 'Publish')
], 'step' => 1]); ?>
    <div class="profile">
        <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true"
             style="padding-right: 17px;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div class="mobile-note-title"></div>
                    </div>
                    <div class="modal-body">
                        <div class="mobile-note-text"></div>
                        <div class="mobile-note-btn text-center">
                            <a class="button big green" href="#"><?= Yii::t('app', 'Continue') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-step' => 1,
                            'data-final-step' => 3
                        ],
                        'id' => 'company-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]); ?>
                    <?= Html::hiddenInput('id', $model->company->id, ['id' => 'company-id']) ?>
                    <div class="tab-content nostyle-tab-content">
                        <div class="settings-content step tab-pane fade in active" id="step1" data-step="1">
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "title", [
                                        'template' => '
                                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">
                                                    {label}
                                                    <div class="mobile-question" data-toggle="modal"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                                {input}
                                                {error}
                                                <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('app', 'Max') . '</div>
                                            </div>'
                                    ])->textarea([
                                        'class' => 'readsym bigsymb',
                                        'placeholder' => Yii::t('app', 'Specify the name of your company, for example, UHOME')
                                    ])
                                    ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title">
                                            <?= Yii::t('model', 'Company Name') ?>
                                        </div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Text length in this field should not exceed 80 characters.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "short_description", [
                                        'template' => '
                                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">
                                                    {label}<div class="mobile-question" data-toggle="modal"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                                {input}
                                                {error}
                                            </div>'
                                    ])->textarea([
                                        'placeholder' => Yii::t('app', 'Add a concise description of the company. For example: “Modern houses construction at affordable prices.”')
                                    ]); ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title">
                                            <?= Yii::t('model', 'Short Description') ?>
                                        </div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'With 1-2 sentences briefly describe the main areas of work. Do not go into details - to create detailed information we have created the column “Company description”.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "description", [
                                        'template' => '
                                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">
                                                    {label}<div class="mobile-question" data-toggle="modal"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                                {input}
                                                {error}
                                            </div>'
                                    ])->textarea(['style' => 'height: 200px;']); ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title">
                                            <?= Yii::t('model', 'Company Description') ?>
                                        </div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'We advise preparing a beautiful selling description without grammatical, spelling, punctuation mistakes. So you are more likely to interest new customers.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('app', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane step fade in form-gig file-input-step" id="step2"
                             data-step="2">
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label" for="postcompanyform-address">
                                                <?= $model->getAttributeLabel('address') ?>
                                            </label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <?= GmapsInputWidget::widget(['model' => $model, 'inputOptions' => [
                                            'placeholder' => Yii::t('app', 'Moscow, Novy Arbat 12')]
                                        ]); ?>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title">
                                                <?= Yii::t('model', 'Company Address') ?>
                                            </div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('app', 'Set your company address') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, 'activities', ['template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>'
                                    ])->textInput([
                                        'id' => 'activities',
                                        'class' => 'readsym'
                                    ])->label(Yii::t('app', 'Fields of activity')) ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Fields of activity') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Specify the fields of activity of your company') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label" for="postcompanyform-file-upload-input-logo">
                                                <?= $model->getAttributeLabel('logo') ?>
                                            </label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="cgp-upload-files file-input-container">
                                            <?= FileInput::widget(
                                                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                    'id' => 'file-upload-input-logo',
                                                    'pluginOptions' => [
                                                        'overwriteInitial' => true,
                                                        'initialPreview' => !empty($model->logo) ? Yii::$app->mediaLayer->getThumb($model->logo) : [],
                                                        'initialPreviewConfig' => !empty($model->logo) ? [[
                                                            'caption' => basename($model->logo),
                                                            'url' => Url::toRoute('/image/delete'),
                                                            'key' => 'image_init_' . $model->company->id
                                                        ]] : [],
                                                    ]
                                                ])
                                            ) ?>
                                            <div class="images-container" data-field="logo">
                                                <?= $form->field($model, "logo")->hiddenInput([
                                                    'value' => $model->logo,
                                                    'data-key' => 'image_init_' . $model->company->id
                                                ])->label(false);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title">
                                                <?= Yii::t('model', 'Company Logo') ?>
                                            </div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('app', 'We recommend using equilateral images, centering elements of your logo: all images are automatically cropped by the editor to the shape of the square, and the thumbnail is displayed in a round frame:') ?>
                                            </p>
                                            <ol>
                                                <li><?= Yii::t('app', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                                <li><?= Yii::t('app', 'Quality: always use high resolution pictures.') ?></li>
                                                <li><?= Yii::t('app', 'The recommended file weight should not exceed {count} MB.', ['count' => 1]) ?></li>
                                            </ol>
                                            <p>
                                                <?= Yii::t('app', 'If necessary, you can crop the picture yourself using the editor functionality.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label" for="postcompanyform-file-upload-input-banner">
                                                <?= $model->getAttributeLabel('banner') ?>
                                            </label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="cgp-upload-files file-input-container">
                                            <?= FileInput::widget(
                                                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                    'id' => 'file-upload-input-banner',
                                                    'pluginOptions' => [
                                                        'uploadUrl' => Url::to(['/image/upload-background']),
                                                        'minImageWidth' => 260,
                                                        'minImageHeight' => 100,
                                                        'overwriteInitial' => true,
                                                        'initialPreview' => !empty($model->banner) ? Yii::$app->mediaLayer->getThumb($model->banner) : [],
                                                        'initialPreviewConfig' => !empty($model->banner) ? [[
                                                            'caption' => basename($model->banner),
                                                            'url' => Url::toRoute('/image/delete'),
                                                            'key' => 'image_init_' . $model->company->id
                                                        ]] : [],
                                                        'otherActionButtons' =>
                                                            '<button type="button" class="btn btn-xs btn-default crop-bg-button" title="' . Yii::t('app', 'Crop image') . '">
                                                                <i class="fa fa-crop" aria-hidden="true"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-default crop-bg-button" title="' . Yii::t('app', 'Rotate image') . '">
                                                                <i class="fa fa-rotate-left" aria-hidden="true"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-default crop-bg-button" title="' . Yii::t('app', 'Rotate image') . '">
                                                                <i class="fa fa-rotate-right" aria-hidden="true"></i>
                                                            </button>',
                                                    ]
                                                ])
                                            ) ?>
                                            <div class="images-container" data-field="banner">
                                                <?= $form->field($model, "banner")->hiddenInput([
                                                    'value' => $model->banner,
                                                    'data-key' => 'image_init_' . $model->company->id
                                                ])->label(false);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title">
                                                <?= Yii::t('model', 'Company Banner') ?>
                                            </div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('app', 'For the banner it\'s better to choose pictures with proportions of 3:1 (the optimal ratio of width and height):') ?>
                                            </p>
                                            <ol>
                                                <li><?= Yii::t('app', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                                <li><?= Yii::t('app', 'Quality: always use high resolution pictures.') ?></li>
                                                <li><?= Yii::t('app', 'The recommended file weight should not exceed {count} MB.', ['count' => 1]) ?></li>
                                            </ol>
                                            <p>
                                                <?= Yii::t('app', 'If necessary, you can crop the picture yourself using the editor functionality.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade in file-input-step" id="step3" data-step="3">
                            <div class="publish-title text-center">
                                <?= Yii::t('app', 'It\'s almost ready...') ?>
                            </div>
                            <div class="publish-text text-center">
                                <?= Yii::t('app', "It remains only to publish a page of your company in order to attract the attention of new customers. After the moderator checks the information will be available to all users of the site.") ?>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>

                                <?= Html::submitButton(Yii::t('app', 'Publish company'), ['class' => 'btn-mid fright']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

        <?= $this->render('@frontend/views/common/crop-bg.php')?>

    </div>

<?php
$userId = Yii::$app->user->identity->getId();
$ajaxCreateRoute = Url::toRoute('/company/company/ajax-create');
$activityListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'field-of-activity']);
$addMessage = Yii::t('app', 'Add');
$script = <<<JS
    $(document).on('click','.mobile-question',function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click','.mobile-note-btn a',function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });

	let uid = $userId;
       
	$("#company-form").on("beforeValidateAttribute", function (event, attribute, messages) {
		let current_step = $(this).data("step"),
		    input_step = $(attribute.container).closest(".step").data("step");
		if (current_step !== input_step) {
			return false;
		}
	}).on("afterValidate", function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
			let current_step = $(this).data("step");
            $("html, body").animate({
                scrollTop: $(this).find("div[data-step=\'" + current_step + "\'] .has-error").first().offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function (event) {
		let current_step = $(this).data("step"),
		    self = $(this);
		if ($(this).find("div[data-step=\'" + current_step + "\']").hasClass("file-input-step")) {
		    let returnValue = true;
		    $(".file-upload-input").each(function(index){
                if ($(this).fileinput("getFilesCount") > 0) {
                    $(this).fileinput("upload");
                    returnValue = false;
                    return false;
                }
		    });
		    if (!returnValue) {
		        return false;
		    }
        }
		if (current_step !== $(this).data("final-step")) {
            self.find("div[data-step=\'" + current_step + "\']").removeClass("active");
            $(".gig-steps li").removeClass("active");
            current_step++;
            self.data("step", current_step);
            self.find("div[data-step=\'" + current_step + "\']").addClass("active");
            $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
			return false;
		}
	});

    $(".prev-step").on("click", function() {
    	let companyForm = $("#company-form"),
		    current_step = companyForm.data("step");
    	
		if (current_step > 1) {
			companyForm.find("div[data-step=\'" + current_step + "\']").removeClass("active");
			$(".gig-steps li").removeClass("active");
			current_step--;
			companyForm.data("step", current_step);
			companyForm.find("div[data-step=\'" + current_step + "\']").addClass("active");
			$(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
			$("html, body").animate({
				scrollTop: 0
			}, "slow");
		}
		return false;
    });
    
    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        let field = imagesContainer.attr('data-field');
        imagesContainer.find("input").remove();
        imagesContainer.append("<input class='image-source' name='PostCompanyForm[" + field + "]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key='" + key + "']").val('');
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) { 
           $("#company-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) { 
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    
    $("#activities").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class=\"create\">$addMessage&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$activityListUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script);