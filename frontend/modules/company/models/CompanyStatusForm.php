<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.04.2018
 * Time: 15:42
 */

namespace frontend\modules\company\models;

use common\models\Company;
use Yii;
use yii\base\Model;

/**
 * Class CompanyStatusForm
 * @package frontend\modules\company\models
 */
class CompanyStatusForm extends Model
{
    /**
     * @var string
     */
    public $status = null;

    /**
     * @var integer
     */
    public $companyId = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'string', 'max' => 155]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('model', 'Company status text'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if ($this->validate() === false) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $result = Company::updateAll([
            'status_text' => $this->status
        ], [
            'id' => $this->companyId
        ]);

        if ($result > 0) {
            $transaction->commit();
            return true;
        }

        $transaction->rollBack();
        return false;
    }
}