<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.04.2018
 * Time: 16:02
 */

namespace frontend\modules\company\models;

use common\models\Company;
use common\models\ContentTranslation;
use Yii;
use yii\base\Model;
use yii\helpers\StringHelper;

/**
 * Class CompanyStatusForm
 * @package frontend\modules\company\models
 */
class CompanyDescriptionForm extends Model
{
    /**
     * @var string
     */
    public $short_description = null;

    /**
     * @var string
     */
    public $description = null;

    /**
     * @var integer
     */
    public $companyId = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['short_description', 'string', 'max' => 155],
            ['description', 'string', 'max' => 65535],
            [['companyId'], 'exist', 'skipOnError' => false, 'targetClass' => Company::class, 'targetAttribute' => ['companyId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => Yii::t('model', 'Company Description'),
            'short_description' => Yii::t('model', 'Short Description'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if ($this->validate() === false) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $companyTranslation = ContentTranslation::find()
            ->where(['entity' => 'company', 'entity_id' => $this->companyId, 'locale' => Yii::$app->language])
            ->one();

        if($companyTranslation !== null) {
            $companyTranslation->content = $this->description;
            $companyTranslation->content_prev = $this->short_description;

            if( $companyTranslation->save() === true) {
                $transaction->commit();
                return true;
            }
        } else {
            /** @var Company $company */
            $company = Company::find()->where(['id' => $this->companyId])->one();

            $translation = new ContentTranslation([
                'title' => $company->getLabel(),
                'entity' => 'company',
                'entity_id' => $this->companyId,
                'locale' => Yii::$app->language,
                'content_prev' => $this->short_description,
                'content' => $this->description
            ]);

            if($translation->save() === true) {
                $transaction->commit();
                return true;
            }
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_description ?? strip_tags(StringHelper::truncate($this->description, 200));
    }
}