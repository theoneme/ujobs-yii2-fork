<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\modules\company\models;

use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\Company;
use common\models\ContentTranslation;
use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PostCompanyForm
 * @package frontend\models
 *
 * @property Company $company
 */
class PostCompanyForm extends Model
{
    /**
     * @var string
     */
    public $locale;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $short_description;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var string
     */
    public $logo = null;
    /**
     * @var string
     */
    public $banner = null;
    /**
     * @var string
     */
    public $lat = null;
    /**
     * @var string
     */
    public $long = null;
    /**
     * @var string
     */
    public $address = null;
    /**
     * @var string
     */
    public $activities = null;
    /**
     * @var Company
     */
    private $_company;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'descriptionRequired' => ['description', 'required'],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            ['short_description', 'string', 'max' => 200],
            ['description', 'string', 'max' => 65535],
//            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages']), 'skipOnEmpty' => false],
            [['lat', 'long', 'address'], 'string'],
            [['logo', 'banner'], 'string', 'max' => 155],
            ['activities', 'string'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Company Name'),
            'description' => Yii::t('model', 'Company Description'),
            'short_description' => Yii::t('model', 'Short Description'),
            'address' => Yii::t('model', 'Company Address'),
            'logo' => Yii::t('model', 'Company Logo'),
            'banner' => Yii::t('model', 'Company Banner'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!empty($this->validateAll())) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->company->load($this->attributes, '');
        $isSaved = $this->company->save();
        $transaction->commit();

        if ($isSaved) {
            if ($this->lat && $this->long && $this->address) {
                if (!AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->exists()) {
                    (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $this->address]))->save();
                }
            }
        }

        return $isSaved;
    }

    /**
     * @return array|Attachment[]|null
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->company->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->company->attachments;
            }
        }
        return $this->_attachments;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);

        $translationObject = new ContentTranslation([
            'title' => $this->title,
            'content' => $this->description,
            'content_prev' => $this->short_description,
            'locale' => $this->locale ?? Yii::$app->language,
            'entity' => 'company',
        ]);
        $this->company->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;

        $attribute = Attribute::findOne(['alias' => 'field-of-activity']);
        if($attribute !== null) {
            $activities = array_filter(!empty($this->activities) ? explode(',', $this->activities) : []);

            if (!empty($activities)) {
                $this->company->companyActivities = $activities;
//                $linkable = $this->company->behaviors['linkable'];
//                $linkable->relations = array_merge($linkable->relations, ['userAttributes']);
//                $this->company->attachBehavior('linkable', $linkable);
//                foreach($activities as $activity) {
//                    $attributeObject = AttributeValueObjectBuilder::makeAttributeObject(UserAttribute::class, $attribute->id, $activity, ['locale' => $this->locale]);
//                    if($attributeObject !== null) {
//                        $this->company->bind('userAttributes', $attributeObject->id)->attributes = $attributeObject->attributes;
//                    }
//                }
            }
        }
    }

    /**
     * @param $id
     */
    public function loadCompany($id)
    {
        $this->company = Company::findOne((int)$id);
        $this->attributes = $this->company->attributes;
        $this->title = $this->company->translation->title ?? '';
        $this->description = $this->company->translation->content ?? '';
        $this->short_description = $this->company->translation->content_prev ?? '';
        $this->address = $this->company->getAddress();
        $activitiesArray = array_reduce($this->company->userAttributes, function($carry, $value) {
            /* @var $value UserAttribute*/
            if ($value->entity_alias === 'field-of-activity') {
                $carry[] = $value->getTitle();
            }
            return $carry;
        }, []);
        $this->activities = implode(',', $activitiesArray);
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->company->validateWithRelations()
        );
    }
}