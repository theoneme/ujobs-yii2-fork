<?php

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Comment;
use common\models\Company;
use common\models\Message;
use common\models\Offer;
use common\models\UserPortfolio;
use common\services\CounterService;
use frontend\models\PostCommentForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class PortfolioController
 * @package frontend\modules\account\controllers
 */
class PortfolioController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['show', 'view-ajax', 'view', 'show-as-customer'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return UserPortfolio::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShow($id, $share = false)
    {
        return $this->prepareShowPage($id, false, $share);
    }


    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShowAsCustomer($id, $share = false)
    {
        return $this->prepareShowPage($id, true, $share);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @param $share
     * @return string
     */
    private function prepareShowPage($id, $as_customer = false, $share)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Company $company */
        $company = Company::find()->where(['company.id' => $id])->one();

        if ($company === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested company is not found'));
            return $this->redirect(['/category/job-catalog']);
        }

        if (!$as_customer && hasAccess($company->user_id)) {
            $editable = true;
        }

        $portfolioQuery = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_portfolio.user_id' => $company->user_id]);
        if (!$editable) {
            $portfolioQuery->andWhere(['user_portfolio.status' => UserPortfolio::STATUS_ACTIVE]);
        } else {
            $portfolioQuery->andWhere(['not', ['user_portfolio.status' => UserPortfolio::STATUS_DELETED]]);
        }

        $newPortfolioQuery = UserPortfolio::find()->joinWith(['profile', 'category.translation'])->where(['user_portfolio.user_id' => $company->user_id, 'version' => UserPortfolio::VERSION_NEW]);
        $newPortfolioDataProvider = new ActiveDataProvider(['query' => $newPortfolioQuery, 'pagination' => false]);

        if ($share === false) {
            $albums = implode(', ', array_map(function ($value) {
                return $value->title;
            }, $newPortfolioDataProvider->getModels()));
            Yii::$app->opengraph->description = Yii::t('account', 'View photos in albums {albums}', ['albums' => $albums]);
            $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Portfolio page of user {user} on site uJobs.me", ['user' => $company->getSellerName()]);
            $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('account', 'View photos in albums {albums}', ['albums' => $albums])]);
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('account', 'Portfolio of user {user}', ['user' => $company->getSellerName()])]);
            if (!strstr($company->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png')) {
                Yii::$app->opengraph->image = Url::to($company->getThumb(), true);
            }
        } else {
            $portfolio = UserPortfolio::findOne($share);
            if ($portfolio !== null) {
                Yii::$app->opengraph->description = Html::encode(strip_tags($portfolio->description));
                $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Portfolio album {album}", ['album' => Html::encode($portfolio->title)]);
                $this->view->registerMetaTag(['name' => 'description', 'content' => Html::encode(strip_tags($portfolio->description))]);
                if (!strstr($company->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png')) {
                    Yii::$app->opengraph->image = Url::to($portfolio->getThumb(), true);
                }
            }
        }

        return $this->render($template, [
            'company' => $company,
            'editable' => $editable,
            'newPortfolioDataProvider' => $newPortfolioDataProvider,
            'message' => new Message(),
            'offer' => new Offer(),
            'contactAccessInfo' => $contactAccessInfo
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /* @var UserPortfolio $model */
        $model = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers', 'category', 'category.translation'])
            ->where(['user_portfolio.id' => $id])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $otherPortfoliosQuery = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers', 'category', 'category.translation'])
            ->where(['not', ['user_portfolio.id' => $model->id]])
            ->andWhere(['user_portfolio.user_id' => $model->user_id, 'version' => UserPortfolio::VERSION_NEW])
            ->groupBy('user_portfolio.id')
            ->limit(4);
        $commentsQuery = Comment::find()->where(['entity' => 'portfolio', 'entity_id' => $model->id]);

        $otherPortfoliosDataProvider = new ActiveDataProvider(['query' => $otherPortfoliosQuery, 'pagination' => false]);
        $commentsDataProvider = new ActiveDataProvider(['query' => $commentsQuery, 'pagination' => false]);

        $postCommentForm = new PostCommentForm();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'portfolio_views', Yii::$app->session);
        $counterServiceView->process();

        $left = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['<', 'id', $model->id])->orderBy('id desc')->one();
        $right = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['>', 'id', $model->id])->orderBy('id asc')->one();
        $leftLink = $left !== null ? Url::to(['/account/portfolio/view', 'id' => $left->id]) : null;
        $rightLink = $right !== null ? Url::to(['/account/portfolio/view', 'id' => $right->id]) : null;
        $returnUrl = Url::previous('portfolioReturnUrl') ?? Url::to(['/account/portfolio/show', 'id' => $model->user_id]);

        Yii::$app->opengraph->description = Html::encode(strip_tags($model->description));
        $this->view->title = Yii::$app->opengraph->title = Yii::t('account', "Portfolio album {album}", ['album' => Html::encode($model->title)]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => Html::encode(strip_tags($model->description))]);
        if (!strstr($model->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png')) {
            Yii::$app->opengraph->image = Url::to($model->getThumb(), true);
        }

        return $this->render('view', [
            'model' => $model,
            'profile' => $model->profile,
            'otherPortfoliosDataProvider' => $otherPortfoliosDataProvider,
            'postCommentForm' => $postCommentForm,
            'commentsDataProvider' => $commentsDataProvider,
            'leftLink' => $leftLink,
            'rightLink' => $rightLink,
            'returnUrl' => $returnUrl
        ]);
    }

    /**
     * @return array
     */
    public function actionViewAjax()
    {
        $id = Yii::$app->request->post('id', null);
        $catalog = (int)Yii::$app->request->post('catalog', 0);

        $left = $leftLink = $right = $rightLink = null;

        $this->layout = null;
        /* @var UserPortfolio $model */
        $model = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers', 'category', 'category.translation'])
            ->where(['user_portfolio.id' => $id])
            ->one();

        if ($model === null) {
            return ['success' => false];
        }

        $otherPortfoliosQuery = UserPortfolio::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'profile.translation', 'attachments', 'likes', 'likeUsers', 'category', 'category.translation'])
            ->where(['not', ['user_portfolio.id' => $model->id]])
            ->andWhere(['user_portfolio.user_id' => $model->user_id, 'version' => UserPortfolio::VERSION_NEW])
            ->groupBy('user_portfolio.id')
            ->limit(4);
        $commentsQuery = Comment::find()
            ->where(['entity' => 'portfolio', 'entity_id' => $model->id]);

        $otherPortfoliosDataProvider = new ActiveDataProvider(['query' => $otherPortfoliosQuery, 'pagination' => false]);
        $commentsDataProvider = new ActiveDataProvider(['query' => $commentsQuery, 'pagination' => false]);

        $postCommentForm = new PostCommentForm();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'portfolio_views', Yii::$app->session);
        $counterServiceView->process();

        if ($catalog === 0) {
            $left = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['<', 'id', $model->id])->orderBy('id desc')->one();
            $right = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $model->user_id])->andWhere(['>', 'id', $model->id])->orderBy('id asc')->one();
            $leftLink = $left !== null ? Url::to(['/account/portfolio/view', 'id' => $left->id]) : null;
            $rightLink = $right !== null ? Url::to(['/account/portfolio/view', 'id' => $right->id]) : null;
        }
        $returnUrl = Url::previous('portfolioReturnUrl') ?? Url::to(['/account/portfolio/show', 'id' => $model->user_id]);

        return [
            'success' => true,
            'html' => $this->renderAjax('partial/modal', [
                'model' => $model,
                'profile' => $model->profile,
                'otherPortfoliosDataProvider' => $otherPortfoliosDataProvider,
                'postCommentForm' => $postCommentForm,
                'commentsDataProvider' => $commentsDataProvider,
                'leftLink' => $leftLink,
                'rightLink' => $rightLink,
                'catalog' => $catalog,
                'returnUrl' => $returnUrl
            ])
        ];
    }
}