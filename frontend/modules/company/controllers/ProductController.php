<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.03.2018
 * Time: 17:33
 */

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\Job;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use frontend\modules\account\models\ProfileProductSearch;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class ProductController
 * @package frontend\modules\company\controllers
 */
class ProductController extends FrontEndController
{
    /**
     * @param $id
     * @return mixed
     */
    public function actionShow(int $id)
    {
        return $this->prepareShowPage($id, false);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionShowAsCustomer(int $id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage(int $id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Company $company */
        $company = Company::find()->where(['company.id' => $id])->one();

        if ($company === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested company is not found'));
            return $this->redirect(['/category/job-catalog']);
        }

        $this->registerMetaTags($company);

        if (!$as_customer && hasAccess($company->user_id)) {
            $editable = true;
        }

        $searchModel = new ProfileProductSearch(['isOwner' => $editable, 'user_id' => $company->user_id]);
        $sellingProductsDataProvider = $searchModel->search(['sold' => false]);
        $soldProductsDataProvider = $searchModel->search(['sold' => true]);

        $totalSoldProducts = Order::find()
            ->joinWith(['orderProduct'])
            ->where(["order.seller_id" => $company->user_id])
            ->andWhere(['order.product_type' => Order::TYPE_PRODUCT, 'order.status' => Order::STATUS_PRODUCT_COMPLETED])
            ->count();

        return $this->render($template, [
            'company' => $company,
            'editable' => $editable,
            'sellingProductsDataProvider' => $sellingProductsDataProvider,
            'soldProductsDataProvider' => $soldProductsDataProvider,
            'contactAccessInfo' => $contactAccessInfo,
            'totalSoldProducts' => $totalSoldProducts,
        ]);
    }
}