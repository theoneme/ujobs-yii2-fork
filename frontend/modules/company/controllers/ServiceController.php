<?php

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\user\User;
use frontend\modules\company\models\PostCompanyForm;
use Yii;
use yii\filters\AccessControl;

/**
 * Class ServiceController
 * @package frontend\modules\company\controllers
 */
class ServiceController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'public-profile-settings',
                    ], 'roles' => ['@']],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionPublicProfileSettings()
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id === null || $user->companyUser->company === null) {
            return $this->redirect(['/account/service/public-profile-settings']);
        }
        if (!$user->companyUser->company->companyMembers[userId()]->isAllowedToManage()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to edit company profile'));
            return $this->redirect(['/account/service/companies']);
        }
        $model = new PostCompanyForm();
        $model->loadCompany($user->companyUser->company->id);

        return $this->render('public-profile-settings', [
            'model' => $model,
        ]);
    }
}