<?php

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\user\User;
use frontend\modules\company\models\PostCompanyForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class ServiceAjaxController
 * @package common\modules\company\controllers
 */
class ServiceAjaxController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'process-public-profile-settings',
                    ], 'roles' => ['@']],
                ],
            ],
        ];
    }

    /**
     * @return array|Response
     */
    public function actionProcessPublicProfileSettings()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->company_user_id === null || $user->companyUser->company === null) {
            return $this->redirect(['/account/service/public-profile-settings']);
        }
        if (!$user->companyUser->company->companyMembers[userId()]->isAllowedToManage()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to edit company profile'));
            return $this->redirect(['/account/service/companies']);
        }
        $model = new PostCompanyForm();
        $model->loadCompany($user->companyUser->company->id);
        $input = Yii::$app->request->post();
        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->validateAll();
        } else {
            $model->company->status = Company::STATUS_REQUIRES_MODERATION;
            $model->myLoad($input);
            if ($model->save() === true) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Company has been updated!'));
                return $this->redirect(['/company/profile/show', 'id' => $model->company->id]);
            }
        }
        Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred while updating the company profile'));
        return $this->redirect(['/company/service/public-profile-settings']);
    }
}