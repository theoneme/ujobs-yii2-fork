<?php

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\user\User;
use frontend\modules\company\models\CompanyDescriptionForm;
use frontend\modules\company\models\CompanyStatusForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ProfileAjaxController
 * @package frontend\modules\company\controllers
 */
class ProfileAjaxController extends FrontEndController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'switch',
                        'update-status',
                        'update-description'
                    ], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['switch', 'update-status', 'update-description'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionUpdateStatus()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            /** @var User $user */
            $user = Yii::$app->user->identity;

            /** @var Company $company */
            $company = Company::find()->where(['user_id' => $user->company_user_id])->one();
            if ($company !== null) {
                $companyStatusForm = new CompanyStatusForm([
                    'companyId' => $company->id
                ]);

                $input = Yii::$app->request->post();
                $companyStatusForm->load($input);

                if (isset($input['ajax'])) {
                    return ActiveForm::validate($companyStatusForm);
                }

                if ($companyStatusForm->save() === true) {
                    return ['success' => true, 'status' => $companyStatusForm->status];
                }
            }

            return ['success' => false];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionUpdateDescription()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            /** @var User $user */
            $user = Yii::$app->user->identity;

            /** @var Company $company */
            $company = Company::find()->where(['user_id' => $user->company_user_id])->one();
            if ($company !== null) {
                $companyStatusForm = new CompanyDescriptionForm([
                    'companyId' => $company->id
                ]);

                $input = Yii::$app->request->post();
                $companyStatusForm->load($input);

                if (isset($input['ajax'])) {
                    return ActiveForm::validate($companyStatusForm);
                }

                if ($companyStatusForm->save() === true) {
                    return ['success' => true, 'description' => $companyStatusForm->getShortDescription()];
                }
            }

            return ['success' => false];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSwitch()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            $result = ['success' => true];
            $input = Yii::$app->request->post('identity');

            if ($input !== null) {
                $hasAccess = Company::find()
                    ->joinWith(['companyMembers'])
                    ->where(['company.user_id' => (int)$input, 'company_member.user_id' => userId()])
                    ->exists();

                if ($hasAccess === true) {
                    User::updateAll([
                        'company_user_id' => (int)$input,
                    ], ['id' => userId()]);

                    return $result;
                }

                return ['success' => false];
            }

            User::updateAll([
                'company_user_id' => null,
            ], ['id' => userId()]);

            return $result;
        }

        throw new BadRequestHttpException('Should be called other way');
    }
}