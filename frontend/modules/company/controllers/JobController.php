<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.03.2018
 * Time: 17:33
 */

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\Job;
use common\models\JobAttribute;
use common\modules\store\models\Order;
use frontend\modules\account\models\ProfileJobSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class JobController
 * @package frontend\modules\company\controllers
 */
class JobController extends FrontEndController
{
    /**
     * @param $id
     * @return mixed
     */
    public function actionShow(int $id)
    {
        return $this->prepareShowPage($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionShowAsCustomer(int $id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage(int $id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Company $company */
        $company = Company::find()->where(['company.id' => $id])->one();

        if ($company === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested company is not found'));
            return $this->redirect(['/category/job-catalog']);
        }

        $this->registerMetaTags($company);

        if (!$as_customer && hasAccess($company->user_id)) {
            $editable = true;
        }

        $searchModel = new ProfileJobSearch(['isOwner' => $editable, 'user_id' => $company->user_id]);
        $sellingJobsDataProvider = $searchModel->search(array_merge(Yii::$app->request->queryParams, ['sold' => false]));
        $tags = $searchModel->getTags();
        $categories = $searchModel->getCategories();
        $priceRange = $searchModel->getPriceRange();

        $soldJobsDataProvider = $searchModel->search(['sold' => true]);

        $totalSoldJobs = Order::find()
            ->joinWith(['orderProduct'])
            ->where(["order.seller_id" => $company->user_id])
            ->andWhere(['order.product_type' => Order::TYPE_JOB_PACKAGE, 'order.status' => Order::STATUS_COMPLETED])
            ->count();

        return $this->render($template, [
            'company' => $company,
            'editable' => $editable,
            'sellingJobsDataProvider' => $sellingJobsDataProvider,
            'soldJobsDataProvider' => $soldJobsDataProvider,
            'contactAccessInfo' => $contactAccessInfo,
            'totalSoldJobs' => $totalSoldJobs,
            'tags' => $tags,
            'categories' => $categories,
            'priceRange' => $priceRange,
            'searchModel' => $searchModel
        ]);
    }
}