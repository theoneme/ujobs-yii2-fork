<?php

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\Job;
use common\models\Page;
use common\modules\board\models\Product;
use frontend\modules\company\models\PostCompanyForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ProfileController
 * @package frontend\modules\company\controllers
 */
class ProfileController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['form', 'switch'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['show', 'show-as-customer'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['update', 'delete', 'pause', 'resume'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Company::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param int|null $id
     * @return array|string|Response
     */
    public function actionForm(int $id = null)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';
        $input = Yii::$app->request->post();

        $isUpdate = ($id !== null && Company::find()->where(['id' => $id])->exists());
        $model = new PostCompanyForm();
        if ($isUpdate === true) {
            $model->loadCompany($id);
        } else {
            $model->company = new Company();
        }

        if (!empty($input)) {
            if (Yii::$app->request->isAjax) {
                $model->myLoad($input);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model->validateAll();
            } else {
                if (!empty($input['id'])) {
                    $model->loadCompany($input['id']);
                    $model->company->status = Company::STATUS_REQUIRES_MODERATION;
                    $model->myLoad($input);
                    if ($model->save() === true) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Company has been updated!'));
                        return $this->redirect(['/company/profile/show', 'id' => $model->company->id]);
                    }
                } else {
                    $model->myLoad($input);

                    if ($model->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Company has been created!'));
                        return $this->redirect(['/company/profile/show', 'id' => $model->company->id]);
                    }
                }
            }
        }

        return $this->render('_form', [
            'model' => $model,
            'action' => 'create',
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionShow(int $id)
    {
        return $this->prepareShowPage($id, false);
    }

    /**
     * @param $id
     * @param bool $share
     * @return mixed
     */
    public function actionShowAsCustomer(int $id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage(int $id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Company $company */
        $company = Company::find()->where(['company.id' => $id])->one();

        if ($company === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested company is not found'));
            return $this->redirect(['/category/job-catalog']);
        }

        $this->registerMetaTags($company);

        if (!$as_customer && hasAccess($company->user_id)) {
            $editable = true;
        }

        $articleQuery = Page::find()
            ->joinWith(['translation'])
            ->where(['type' => Page::TYPE_ARTICLE, 'user_id' => $company->user_id])
            ->orderBy('publish_date desc');
        $jobsQuery = Job::find()
            ->joinWith(['translation', 'attachments', 'user', 'packages'])
            ->where(['job.type' => Job::TYPE_JOB, 'job.user_id' => $company->user_id])
            ->groupBy('job.id')
            ->limit(10);
        $productsQuery = Product::find()
            ->joinWith(['translation', 'attachments', 'user'])
            ->where(['product.type' => Product::TYPE_PRODUCT, 'product.user_id' => $company->user_id])
            ->groupBy('product.id')
            ->limit(10);
        $tendersQuery = Job::find()
            ->joinWith(['translation', 'attachments', 'user'])
            ->where(['job.type' => Job::TYPE_TENDER, 'job.user_id' => $company->user_id])
            ->groupBy('job.id')
            ->limit(10);
        $ptendersQuery = Product::find()
            ->joinWith(['translation', 'attachments', 'user'])
            ->where(['product.type' => Product::TYPE_TENDER, 'product.user_id' => $company->user_id])
            ->groupBy('product.id')
            ->limit(10);

        if (!$editable) {
            $jobsQuery->andWhere(['job.status' => Job::STATUS_ACTIVE]);
            $productsQuery->andWhere(['product.status' => Product::STATUS_ACTIVE]);
            $tendersQuery->andWhere(['job.status' => Job::STATUS_ACTIVE]);
            $ptendersQuery->andWhere(['product.status' => Product::STATUS_ACTIVE]);
            $articleQuery->andWhere(['page.status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]]);
        } else {
            $jobsQuery->andWhere(['not', ['job.status' => Job::STATUS_DELETED]]);
            $productsQuery->andWhere(['not', ['product.status' => Product::STATUS_DELETED]]);
            $tendersQuery->andWhere(['not', ['job.status' => Job::STATUS_DELETED]]);
            $ptendersQuery->andWhere(['not', ['product.status' => Product::STATUS_DELETED]]);
            $articleQuery->andWhere(['not', ['page.status' => Page::STATUS_DELETED]]);
        }

        $articlesDataProvider = new ActiveDataProvider(['query' => $articleQuery, 'pagination' => ['pageParam' => 'article-page', 'pageSize' => 5]]);
//        $userEntities = $jobsQuery->all();
//        $userEntities = array_merge($userEntities, $productsQuery->all());
//        shuffle($userEntities);
        $userEntities = [
            'Our services' => $jobsQuery->all(),
            'Product for sale' => $productsQuery->all(),
            'Our tenders' => $tendersQuery->all(),
            'Our product tenders' => $ptendersQuery->all(),
        ];
        return $this->render($template, [
            'company' => $company,
            'editable' => $editable,
            'articlesDataProvider' => $articlesDataProvider,
            'contactAccessInfo' => $contactAccessInfo,
            'userEntities' => $userEntities,
        ]);
    }

    /**
     * Deletes an existing Job model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(int $id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Company::STATUS_DELETED]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}