<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.03.2018
 * Time: 17:33
 */

namespace frontend\modules\company\controllers;

use common\controllers\FrontEndController;
use common\models\Company;
use common\models\Job;
use frontend\modules\account\models\ProfileTenderSearch;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class TenderController
 * @package frontend\modules\company\controllers
 */
class TenderController extends FrontEndController
{
    /**
     * @param $id
     * @return mixed
     */
    public function actionShow(int $id)
    {
        return $this->prepareShowPage($id, false);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionShowAsCustomer(int $id)
    {
        return $this->prepareShowPage($id, true);
    }

    /**
     * @param $id
     * @param bool $as_customer
     * @return string
     */
    private function prepareShowPage(int $id, $as_customer = false)
    {
        $template = 'show';
        $editable = false;

        $contactAccessInfo = ['allowed' => true];

        /* @var Company $company */
        $company = Company::find()->where(['company.id' => $id])->one();

        if ($company === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested company is not found'));
            return $this->redirect(['/category/tender-catalog']);
        }

        $this->registerMetaTags($company);

        if (!$as_customer && hasAccess($company->user_id)) {
            $editable = true;
        }

        $searchModel = new ProfileTenderSearch(['isOwner' => $editable, 'user_id' => $company->user_id]);
        $tendersDataProvider = $searchModel->search();

        return $this->render($template, [
            'company' => $company,
            'editable' => $editable,
            'tendersDataProvider' => $tendersDataProvider,
            'contactAccessInfo' => $contactAccessInfo,
        ]);
    }
}