<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2017
 * Time: 12:18
 */


namespace frontend\modules\messenger\dto;

use common\models\Attachment;
use common\models\Conversation;
use common\models\Message;
use Yii;

/**
 * Class MessengerDTO
 * @package frontend\modules\messenger\dto
 */
class MessengerDTO
{
    /**
     * @var Conversation
     */
    private $_conversation;

    /**
     * MessengerDTO constructor.
     * @param Conversation $conversation
     */
    public function __construct(Conversation $conversation)
    {
        $this->_conversation = $conversation;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $conversation = $this->_conversation;

        $user_id = Yii::$app->user->identity->getCurrentId();
        switch ($conversation->type) {
            case Conversation::TYPE_GROUP:
                $otherMembers = array_diff_key($conversation->conversationMembers, [$user_id => 0]);
                $name = $conversation->getLabel();
                $profileRoute = null;
                $viewedAt = max(array_map(function($var){
                    return $var->viewed_at;
                }, $otherMembers));
                $online = array_reduce($otherMembers, function ($carry, $item) {
                    return $carry || $item->user->isOnline();
                }, false);
                break;
            case Conversation::TYPE_INFO:
                $name = $conversation->getLabel();
                $profileRoute = null;
                $viewedAt = 0;
                $online = false;
                break;
            default:
                $targetMember = $conversation->getOtherMember($user_id);
                if ($targetMember === null) {
                    return null;
                }
                $name = $targetMember->user->getSellerName();
                $profileRoute = $targetMember->user->getSellerUrl();
                $viewedAt = $targetMember->viewed_at;
                $online = $targetMember->user->isOnline();
                break;
        }

        $data = [
            'conversationId' => $conversation->id,
            'header' => [
                'title' => $name,
                'online' => $online,
                'dialogRoute' => ['/account/inbox/conversation', 'id' => $conversation->id],
                'profileRoute' => $profileRoute
            ],
            'messages' => array_map(function ($message) use ($viewedAt) {
                /** @var Message $message */
                return [
                    'text' => $message->message,
                    'side' => hasAccess($message->user_id) ? 'right' : 'left',
                    'created_at' => $message->created_at,
                    'sender' => [
                        'name' => $message->user->getSellerName(),
                        'image' => $message->user->getThumb('catalog')
                    ],
                    'attachments' => array_map(function ($attachment) {
                        /** @var Attachment $attachment */
                        $pathInfo = pathinfo($attachment->content);
                        return [
                            'attachment' => $attachment->getThumb(),
                            'type' => in_array($pathInfo['extension'], ['jpg', 'jpeg', 'gif', 'png'])
                                ? 'image'
                                : 'link',
                            'filename' => $pathInfo['filename']
                        ];
                    }, $message->attachments),
                    'unread' => $viewedAt < strtotime($message->created_at)
                ];
            }, array_reverse($conversation->lastMessages))
        ];

        return $data;
    }
}
