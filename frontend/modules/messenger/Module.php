<?php

namespace frontend\modules\messenger;

use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package frontend\modules\messenger
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\messenger\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        \Yii::$app->i18n->translations['messenger*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@frontend/modules/messenger/messages',

            'fileMap' => [
                'messenger' => 'messenger.php',
            ],
        ];
    }
}
