<?php

namespace frontend\modules\messenger\controllers;

use common\controllers\FrontEndController;
use common\helpers\SecurityHelper;
use common\models\Conversation;
use common\models\ConversationMember;
use common\models\user\User;
use frontend\modules\messenger\dto\MessengerDTO;
use frontend\modules\messenger\models\AddMemberToChatForm;
use frontend\modules\messenger\models\GroupChatForm;
use frontend\modules\messenger\models\InfoChatForm;
use Yii;
use yii\db\ActiveQuery;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package frontend\modules\messenger\controllers
 */
class AjaxController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['get-dialog', 'get-info-modal', 'get-group-modal', 'create-info-group', 'create-group', 'add-member-to-chat', 'get-member-modal', 'leave-group-chat'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $dialog_id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetDialog($dialog_id)
    {
        if (Yii::$app->request->isAjax && !isGuest()) {
            /** @var Conversation $conversation */
            $user_id = Yii::$app->user->identity->getCurrentId();
            $conversation = Conversation::find()
                ->joinWith([
                    'lastMessages.user.profile' => function ($q) {
                        /** @var ActiveQuery $q */
                        return $q->select('profile.user_id, profile.name, profile.gravatar_email');
                    },
                    'lastMessages.attachments',
                    'lastMessages.user.profile.translation',
                    'conversationMembers',
                ])
                ->where(['conversation.id' => $dialog_id])
                ->andWhere(['exists', ConversationMember::find()
                    ->where('conversation_member.conversation_id = conversation.id')
                    ->andWhere(['conversation_member.user_id' => $user_id])
                    ->andWhere(['not', ['conversation_member.status' => ConversationMember::STATUS_DELETED]])
                ])
                ->groupBy('conversation.id')
                ->one();

            if ($conversation !== null) {
                $conversation->conversationMembers[$user_id]->updateAttributes(['viewed_at' => time()]);

                $messengerDTO = new MessengerDTO($conversation);
                $data = $messengerDTO->getData();
                return [
                    'success' => true,
                    'window' => $this->renderAjax('window', [
                        'data' => $data
                    ]),
                    'userId' => SecurityHelper::encrypt($user_id),
                    'name' => Yii::$app->user->identity->getSellerName(),
                    'avatars' => $conversation->type !== Conversation::TYPE_INFO
                        ? ArrayHelper::map($conversation->conversationMembers, 'user_id', function ($member) {
                            return $member->user->getThumb('catalog');
                        })
                        : []
                ];
            }

            return [
                'success' => false
            ];
        }

        throw new BadRequestHttpException("Soryan");
    }


    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetInfoModal()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isGet === true) {
            return [
                'html' => $this->renderAjax('info-modal', [
                    'model' => new InfoChatForm()
                ]),
                'success' => true
            ];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @param int|null $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetGroupModal(int $id = null)
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isGet === true) {
            $groupChatForm = new GroupChatForm();

            if ($id !== null) {
                $groupChatForm->loadConversation($id);
            }

            return [
                'html' => $this->renderAjax('group-modal', [
                    'groupChatForm' => $groupChatForm
                ]),
                'success' => true
            ];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetMemberModal(int $id = null)
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isGet === true) {
            $addMemberToChatForm = new AddMemberToChatForm();
            return [
                'html' => $this->renderAjax('group-member-modal', [
                    'addMemberToChatForm' => $addMemberToChatForm,
                    'id' => $id
                ]),
                'success' => true
            ];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionCreateInfoGroup()
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            if (in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])) {
                $form = new InfoChatForm();
                $form->conversation = new Conversation(['type' => Conversation::TYPE_INFO]);

                $input = Yii::$app->request->post();
                $form->myLoad($input);

                if (isset($input['ajax'])) {
                    return ActiveForm::validate($form);
                }

                if ($form->save() === true) {
                    return ['success' => true, 'message' => Yii::t('messenger', 'Info Chat Has Been Created')];
                }

                if (!empty($form->error)) {
                    return ['success' => false, 'message' => $form->error];
                }
            }
            return ['success' => false];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @param int|null $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionCreateGroup(int $id = null)
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            $user_id = Yii::$app->user->identity->getCurrentId();
            $groupChatForm = new GroupChatForm();
            $groupChatForm->conversation = new Conversation(['type' => Conversation::TYPE_GROUP]);

            if ($id !== null) {
                /** @var Conversation $conversation */
                $conversation = Conversation::find()
                    ->joinWith(['conversationMembers'])
                    ->where(['conversation.id' => $id, 'conversation_member.user_id' => $user_id])
                    ->one();
                if ($conversation !== null) {
                    $groupChatForm->loadConversation($id);
                }
            }

            $input = Yii::$app->request->post();
            $groupChatForm->myLoad($input);

            if (isset($input['ajax'])) {
                return $groupChatForm->validateAll();
            }

            if ($groupChatForm->save() === true) {
                return ['success' => true, 'message' => Yii::t('messenger', 'Group Chat Has Been Created')];
            }


            return ['success' => false];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @param int $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionAddMemberToChat(int $id)
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            $user_id = Yii::$app->user->identity->getCurrentId();
            $addMemberToChatForm = new AddMemberToChatForm();

            if ($id !== null) {
                /** @var Conversation $conversation */
                $conversation = Conversation::find()
                    ->joinWith(['conversationMembers'])
                    ->where(['conversation.id' => $id, 'conversation_member.user_id' => $user_id])
                    ->one();
                if ($conversation !== null) {
                    $addMemberToChatForm->loadConversation($id);
                }
            }

            $input = Yii::$app->request->post();
            $addMemberToChatForm->myLoad($input);

            if (isset($input['ajax'])) {
                return $addMemberToChatForm->validateAll();
            }

            if ($addMemberToChatForm->save() === true) {
                return ['success' => true, 'message' => Yii::t('messenger', 'Group Chat Member has been added')];
            }


            return ['success' => false];
        }

        throw new BadRequestHttpException('Should be called other way');
    }

    /**
     * @param int $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionLeaveGroupChat(int $id)
    {
        if (Yii::$app->request->isAjax === true && Yii::$app->request->isPost === true) {
            $user_id = Yii::$app->user->identity->getCurrentId();

            if ($id !== null) {
                /** @var ConversationMember $conversationMember */
                $conversationMember = ConversationMember::find()
                    ->where(['user_id' => $user_id, 'conversation_id' => $id, 'status' => ConversationMember::STATUS_ACTIVE])
                    ->one();

                if ($conversationMember !== null) {
                    $updatedRows = $conversationMember->updateAttributes(['status' => ConversationMember::STATUS_DELETED]);

                    if ($updatedRows > 0) {
                        return ['success' => true, 'message' => Yii::t('messenger', 'You have left group chat')];
                    }
                }
            }

            return ['success' => false];
        }

        throw new BadRequestHttpException('Should be called other way');
    }
}