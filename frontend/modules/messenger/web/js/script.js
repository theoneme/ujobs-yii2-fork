/**
 * Created by Devour on 21.12.2017.
 */

/** @param {jQuery} $ */

/**
 * @constructor 
 */
function ChatHelper() {
    /**
     * @param i
     * @returns {string}
     */
    this.addZero = function (i) {
        return i < 10 ? "0" + i : i;
    };

    /**
     * @param timestamp
     * @returns {string}
     */
    this.timestampToDateTime = function (timestamp) {
        let date = new Date(parseInt(timestamp) * 1000);

        return this.addZero(date.getDate()) + '.'
            + this.addZero(date.getMonth() + 1) + '.'
            + date.getFullYear() + ' '
            + this.addZero(date.getHours()) + ':'
            + this.addZero(date.getMinutes());
    };

    /**
     * @param blockId
     * @param smile
     */
    this.setInputFocus = function (blockId, smile) {
        let p = document.getElementById('input-block-' + blockId),
            s = window.getSelection(),
            r = document.createRange();

        if (smile === null) {
            r.setStart(p, p.childElementCount + p.innerText.length);
            r.setEnd(p, p.childElementCount + p.innerText.length);
        } else {
            r.setStartAfter(smile[0]);
            r.setEndAfter(smile[0]);
            smile.removeAttr('data-new');
        }

        s.removeAllRanges();
        s.addRange(r);
    }
}

/**
 * @constructor
 */
function MediaHandler() {
    this.smilesPath = '/images/smiles/';
    this.assetPath = null;
    this.replaces = null;

    /**
     * @param assetPath
     */
    this.init = function (assetPath) {
        this.assetPath = assetPath;
        this.replaces = {
            ':)': this.assetPath + this.smilesPath + '1.png',
            ':(': this.assetPath + this.smilesPath + '2.png',
            ';)': this.assetPath + this.smilesPath + '3.png',
            ':p': this.assetPath + this.smilesPath + '4.png',
            '8-)': this.assetPath + this.smilesPath + '5.png',
            'XD': this.assetPath + this.smilesPath + '6.png',
            ':O': this.assetPath + this.smilesPath + '8.png',
            ':E': this.assetPath + this.smilesPath + '9.png',
            ';-*': this.assetPath + this.smilesPath + '10.png',
            ':-*': this.assetPath + this.smilesPath + '11.png',
            ':*': this.assetPath + this.smilesPath + '11.png',
            ';(': this.assetPath + this.smilesPath + '12.png',
            ':|': this.assetPath + this.smilesPath + '13.png',
            ':D': this.assetPath + this.smilesPath + '14.png',
            '<3': this.assetPath + this.smilesPath + '15.png',
            '=/': this.assetPath + this.smilesPath + '16.png',
            ':-[': this.assetPath + this.smilesPath + '17.png',
            ':-Х': this.assetPath + this.smilesPath + '18.png',
            ']:->': this.assetPath + this.smilesPath + '19.png',
            '>:)': this.assetPath + this.smilesPath + '20.png',
            '>:(': this.assetPath + this.smilesPath + '21.png',
            '|-)': this.assetPath + this.smilesPath + '22.png',
            'O:-)': this.assetPath + this.smilesPath + '23.png',
        };
    };

    /**
     * @param text
     * @param wrap
     */
    this.applySmiles = function (text, wrap) {
        let replaces = this.replaces;
        let sourceSmiles = Object.keys(replaces).map(function (smile) {
            return '(' + smile
                    .replace('(', "\\\(")
                    .replace(')', "\\\)")
                    .replace('*', '\\\*')
                    .replace('[', '\\\[')
                    .replace(']', '\\\]')
                    .replace('|', '\\\|') + ')';
        });

        let regex = new RegExp(sourceSmiles.join('|'), 'g');

        return text.replace(regex, function (match) {
            return wrap === true ? '<img src="' + replaces[match] + '" data-source-smile="' + match + '" data-new />' : replaces[match];
        });
    };

    /**
     * @param block
     */
    this.removeSmiles = function (block) {
        let smiles = block.find('img');
        $.each(smiles, function () {
            jQuery(this).replaceWith(jQuery(this).data('source-smile'));
        })
    };

    /**
     * @param file
     */
    this.registerCss = function(file) {
        let link = document.createElement( "link" );
        link.href = this.assetPath + '/' + file;
        link.type = "text/css";
        link.rel = "stylesheet";
        link.media = "screen";

        document.getElementsByTagName( "head" )[0].appendChild( link );
    };

    /**
     * @param file
     */
    this.registerJs = function(file) {
        let script = document.createElement('script');
        script.src = this.assetPath + '/' + file;

        document.head.appendChild(script);
    };
}

/**
 * @constructor
 */
function IndependentEvents() {
    this.register = function () {
        jQuery(document).on('click', '[data-action="open-settings"]', function () {
            let chatBlock = jQuery(this).closest('.chatU-block');

            chatBlock.find('.smile-window').hide();
            chatBlock.find('.chat-setting-menu').toggle();

            return false;
        });

        jQuery(document).on('click', '[data-action="smiles"]', function () {
            let chatBlock = jQuery(this).closest('.chatU-block');

            chatBlock.find('.chat-setting-menu').hide();
            chatBlock.find('.smile-window').toggle();

            return false;
        });

        jQuery(document).on('click', '[data-action="open-profile"], [data-action="dialog-maximize"]', function () {
            window.location.href = jQuery(this).attr('href');

            return false;
        });

        jQuery(document).on('click', '[data-action="group-chat-remove-member"]', function () {
            $(this).closest('.flex').remove();

            return false;
        });

        jQuery(document).on('click', '.chatU-header', function (e) {
            if (!jQuery(e.target).hasClass('chat-setting-menu')
                && jQuery(e.target).data('action') !== 'open-profile'
                && jQuery(e.target).data('action') !== 'dialog-maximize'
                && jQuery(e.target).data('action') !== 'create-group-chat'
                && jQuery(e.target).data('action') !== 'create-info-chat'
            /*&& !jQuery(e.target).parents('.chatU-settings').hasClass('chatU-settings')*/) { console.log((e.target));
                let chatBlock = jQuery(this).closest('.chatU-block');
                chatBlock.toggleClass('small').removeClass('active');
                chatBlock.find('.chat-setting-menu').hide();
                chatBlock.find('.smile-window').hide();
            }

            return false;
        });
    }
}

/**
 * @param connectUrl
 * @param dataPaths
 * @param userId
 * @constructor
 */
function Messenger(connectUrl, dataPaths, userId) {
    /**
     * @type {Messenger}
     */
    const self = this;
    /**
     * @type {IndependentEvents}
     */
    this.events = new IndependentEvents();
    /**
     * @type {MediaHandler}
     */
    this.mediaHandler = new MediaHandler();
    /**
     * @type {ChatHelper}
     */
    this.chatHelper = new ChatHelper();
    /**
     * @type {Socket}
     */
    this.socket = null;

    this.dataPaths = JSON.parse(dataPaths);
    this.connectUrl = connectUrl;
    this.userId = userId;

    this.isJoined = false;
    this.assetsLoaded = false;
    this.openedDialogs = {};
    this.availableDialogs = {};
    this.uploadedFiles = {};
    this.galleries = {};

    this.imagesExtensions = ['.gif', '.jpg', '.jpeg', '.png'];

    /**
     * @param assetPath
     */
    this.init = function (assetPath) {
        this.socket = io.connect(this.connectUrl);
        this.mediaHandler.init(assetPath);
        // this.events.init(this.smilesHandler);
    };

    this.registerAssets = function() {
        let styles = [
            'css/lightgallery.min.css',
            'css/chat.css'
        ];
        let scripts = [
            'js/lightgallery.min.js'
        ];

        for (let style of styles) {
            this.mediaHandler.registerCss(style);
        }

        for (let script of scripts) {
            this.mediaHandler.registerJs(script);
        }
    };

    this.registerHandlers = function () {
        let dialogUrl = this.dataPaths.dialog;
        let modalUrl = this.dataPaths.groupModal;
        let infoUrl = this.dataPaths.infoModal;
        let addMemberModal = this.dataPaths.addMemberModal;

        jQuery(document).on('click', 'a[data-live="1"]', function () {
            if(self.assetsLoaded === false) {
                self.registerAssets();

                self.assetsLoaded = true;
            }

            let dialogId = jQuery(this).data('dialog-id');
            if (!self.openedDialogs[dialogId]) {
                $.ajax({
                    url: dialogUrl,
                    type: 'get',
                    data: {dialog_id: dialogId},

                    /**
                     * @param {Object} result
                     * @param {Object} result.avatars
                     * @param {integer} result.userId
                     * @param {string} result.name
                     * @param {string} result.window
                     * @param {boolean} result.success
                     */
                    success: function (result) {
                        if (result.success === true) {
                            if (self.isJoined === false) {
                                self.connect(result.userId, result.name);
                            }

                            jQuery('#dialog-container').append(result.window);
                            self.chatHelper.setInputFocus(dialogId, null);
                            self.scrollWindowToBottom(dialogId);
                            self.joinDialog(dialogId);
                            self.readDialog(dialogId);
                            self.openedDialogs[dialogId] = result.avatars;
                            jQuery('.chatU-block[data-chat-id=' + dialogId + ']').lightGallery({
                                selector: '[data-gallery-item]',
                            });

                            jQuery('.noti-block').hide();
                        }
                    }
                });
            }

            return false;
        });

        let loadGroupModal = function (url) {
            $.ajax({
                url: url,
                type: 'get',
                /**
                 * @param {Object} result
                 * @param {boolean} result.success
                 * @param {string} result.html
                 */
                success: function (result) {
                    if (result.success === true) {
                        let modalContainer = jQuery('#messenger-group-modal-container');
                        $('.noti-block').hide();
                        modalContainer.html(result.html);
                        modalContainer.find('.modal').modal("show")
                    }
                }
            });
        };

        jQuery(document).on('click', 'a[data-action=create-info-chat]', function () {
            loadGroupModal(infoUrl);
            return false;
        });

        jQuery(document).on('click', 'a[data-action=create-group-chat]', function () {
            let dialog = jQuery(this).data('id');
            let url = modalUrl;
            if(dialog > 0) {
                url += '?id=' + dialog;
            }
            loadGroupModal(url);
            return false;
        });

        jQuery(document).on('click', 'a[data-action=add-member-to-group-chat]', function () {
            let dialog = jQuery(this).data('id');
            let url = addMemberModal;
            if(dialog > 0) {
                url += '?id=' + dialog;
            }
            loadGroupModal(url);
            return false;
        });

        jQuery(document).on('keypress', '.like-text', function (e) {
            if (e.which === 13) {
                let inputBlock = jQuery(this);
                let dialogId = jQuery(this).closest('.chatU-block').data('chat-id');
                self.mediaHandler.removeSmiles(inputBlock);
                let text = inputBlock.text();
                self.sendMessage(text, dialogId);
                inputBlock.html('');

                return false;
            }
        });

        jQuery(document).on('click', '[data-action="dialog-close"]', function () {
            let dialogId = parseInt(jQuery(this).data('chat-id'));
            jQuery(this).closest('.chatU-block').remove();
            delete self.openedDialogs[dialogId];
            self.leaveDialog(dialogId);

            return false;
        });

        jQuery(document).on('click', '.smile-window a', function () {
            let smile = self.mediaHandler.applySmiles(jQuery(this).data('smile'), true);
            let dialogId = jQuery(this).closest('.chatU-block').data('chat-id');

            jQuery(this).closest('.chatU-block').find('.like-text').append(smile);
            jQuery(this).closest('.smile-window').hide();

            let pizdes = jQuery(this).closest('.chatU-block').find('[data-new]');
            self.chatHelper.setInputFocus(dialogId, pizdes);

            return false;
        });

        jQuery(document).on('click', '.chatU-block', function (e) {
            let dialogId = jQuery(this).data('chat-id');

            jQuery('.chatU-block').removeClass('active');
            jQuery(this).addClass('active');

            if (jQuery(e.target).parents('a').data('action') !== 'open-settings' && !jQuery(e.target).hasClass('chat-setting-menu')) {
                jQuery(this).find('.chat-setting-menu').hide();
            }
            if (jQuery(e.target).parents('a').data('action') !== 'smiles' && !jQuery(e.target).hasClass('smile-window')) {
                jQuery(this).find('.smile-window').hide();
            }

            self.readDialog(dialogId);
        });

        jQuery(document).on('fileselect', ".chatU-file-upload-input", function() {
            let dialogId = jQuery(this).closest('.chatU-block').data('chat-id');
            self.showLoading(dialogId);
        }).on('filebatchselected', ".chatU-file-upload-input", function () {
            jQuery(this).fileinput("upload");
        }).on('fileuploaded', ".chatU-file-upload-input", function (event, data, previewId, index) {
            let dialogId = jQuery(this).closest('.chatU-block').data('chat-id');
            if(typeof self.uploadedFiles[dialogId] === 'undefined') {
                self.uploadedFiles[dialogId] = [];
            }
            self.uploadedFiles[dialogId].push(data.response.uploadedPath);
        }).on("filebatchuploadcomplete", ".chatU-file-upload-input", function () {
            let dialogId = jQuery(this).closest('.chatU-block').data('chat-id');
            self.sendAttachment('', dialogId, self.uploadedFiles[dialogId]);
            self.uploadedFiles[dialogId] = [];
        });
        jQuery(document).on('click', ".chatU-file-upload", function (event) {
            event.preventDefault();
            jQuery(this).find('.chatU-file-upload-input').trigger('click');
        });
        jQuery(document).on('click', ".chatU-file-upload-input", function (event) {
            event.stopPropagation();
        });

        self.socket.on('joined', function () {
            self.isJoined = true;
        });

        /**
         * @param {Object} data
         * @param {integer} data.roomId
         */
        self.socket.on('readDialog', function (data) {
            let dialogId = data.roomId;
            let messages = jQuery('.chatU-block[data-chat-id= ' + dialogId + ']').find('.chatU-mes-send.send');

            messages.removeClass('send').addClass('read');
        });

        self.socket.on('backgroundMessage', function () {
            let notificationCounter = jQuery('#header-messages-count');
            if (notificationCounter.length > 0) {
                let counter = parseInt(notificationCounter.html());
                counter++;
                notificationCounter.html(counter);
            } else {
                jQuery('#header-messages-load').prepend('<div class="header-cart-amount" id="header-messages-count">1</div>');
            }

            self.resetHeaderDialogs();
        });

        /**
         * @param {Object} data
         * @param {integer} userId
         */
        self.socket.on('message', function (data) {
            let userId = self.userId;
            if (userId !== data.userId) {
                self.appendLeftMessage(data);
            } else {
                self.appendRightMessage(data);
            }
        });

        /**
         * @param {Object} data
         * @param {integer} userId
         */
        self.socket.on('attachment', function (data) {
            let userId = self.userId;
            let dialogId = data.dialogId;
            let chatBlock = jQuery('.chatU-block[data-chat-id=' + dialogId + ']');

            if (userId !== data.userId) {
                self.appendLeftAttachment(data);
            } else {
                self.appendRightAttachment(data);
            }

            if(chatBlock.data("lightGallery")) {
                chatBlock.data("lightGallery").destroy(true);
            }
            chatBlock.lightGallery({
                selector: '[data-gallery-item]'
            });

            self.hideLoading(dialogId);
        });

        self.events.register();
    };

    /**
     * @param userId
     * @param name
     */
    this.connect = function (userId, name) {
        if (self.isJoined === false) {
            self.socket.emit('joinServer', {
                user_id: userId,
                name: name
            });
        }
    };

    /**
     * @param dialogId
     */
    this.joinDialog = function (dialogId) {
        self.socket.emit('joinDialog', {
            room_id: dialogId
        });
    };

    /**
     * @param dialogId
     */
    this.leaveDialog = function (dialogId) {
        self.socket.emit('leaveRoom', {
            room_id: dialogId
        });
    };

    /**
     * @param dialogId
     */
    this.readDialog = function (dialogId) {
        self.socket.emit('readDialog', {
            room_id: dialogId
        });
    };

    /**
     * @param message
     * @param dialogId
     */
    this.sendMessage = function (message, dialogId) {
        self.socket.emit('sendMessage', {
            room_id: dialogId,
            message: message,
        });
    };

    /**
     * @param message
     * @param dialogId
     * @param attachments
     */
    this.sendAttachment = function (message, dialogId, attachments) {
        self.socket.emit('sendAttachment', {
            room_id: dialogId,
            message: message,
            attachments: attachments
        });
    };

    /**
     * @param data
     */
    this.appendLeftMessage = function (data) {
        let avatar = self.openedDialogs[data.dialogId][data.userId];
        let message = self.mediaHandler.applySmiles(data.message, true);
        let html = '<div class="chatU-mes user">'
            + '<div class="chat-mes-ava">'
            + '<img src="' + avatar + '" />'
            + '</div>'
            + '<div class="chat-mes-text">' + message + '</div>'
            + '</div>'
            + '<div class="chatU-mes-info">'
            + self.chatHelper.timestampToDateTime(data.createdAt)
            + '</div>';
        let chatBlock = jQuery('div[data-chat-id=' + data.dialogId + ']');

        chatBlock.find('.chatU-messages-list').append(html);
        self.scrollWindowToBottom(data.dialogId);
        self.switchToOnline();
    };

    /**
     * @param {Object} data
     * @param {integer} data.createdAt
     * @param {integer} data.dialogId
     */
    this.appendRightMessage = function (data) {
        let message = self.mediaHandler.applySmiles(data.message, true);
        let html = '<div class="chatU-mes you">'
            + '<div class="chat-mes-text">' + message + '</div>'
            + '<div class="chatU-mes-send send">&nbsp;'
            + self.chatHelper.timestampToDateTime(data.createdAt)
            + '</div>'
            + '</div>';
        let chatBlock = jQuery('div[data-chat-id=' + data.dialogId + ']');

        chatBlock.find('.chatU-messages-list').append(html);
        self.scrollWindowToBottom(data.dialogId);
    };

    /**
     * @param {Object} data
     * @param {integer} data.createdAt
     * @param {integer} data.dialogId
     * @param {integer} data.userId
     * @param {Object} data.attachments
     */
    this.appendLeftAttachment = function (data) {
        let avatar = self.openedDialogs[data.dialogId][data.userId];
        let attachments = self.renderAttachments(data.attachments);

        let html = '<div class="chatU-mes user">'
            + '<div class="chat-mes-ava">'
            + '<img src="' + avatar + '" />'
            + '</div>'
            + '<div class="chat-mes-attachments">' + attachments + '</div>'
            + '</div>'
            + '<div class="chatU-mes-info">'
            + self.chatHelper.timestampToDateTime(data.createdAt)
            + '</div>';
        let chatBlock = jQuery('div[data-chat-id=' + data.dialogId + ']');

        chatBlock.find('.chatU-messages-list').append(html);
        self.scrollWindowToBottom(data.dialogId);
        self.switchToOnline();
    };

    /**
     * @param {Object} data
     * @param {integer} data.createdAt
     * @param {integer} data.dialogId
     * @param {Object} data.attachments
     */
    this.appendRightAttachment = function (data) {
        let attachments = self.renderAttachments(data.attachments);

        let html = '<div class="chatU-mes you">'
            + attachments
            + '<div class="chatU-mes-send send">&nbsp;'
            + self.chatHelper.timestampToDateTime(data.createdAt)
            + '</div>'
            + '</div>';
        let chatBlock = jQuery('div[data-chat-id=' + data.dialogId + ']');

        chatBlock.find('.chatU-messages-list').append(html);
        self.scrollWindowToBottom(data.dialogId);
    };

    /**
     * @param {Object} attachments
     * @returns {string}
     */
    this.renderAttachments = function(attachments) {
        let html = '';

        for (let key in attachments) {
            if (attachments.hasOwnProperty(key)) {
                let extension = attachments[key].match(/\.([0-9a-z]+)(?:[\?#]|$)/i);
                if (extension.length > 0) {
                    let isImage = jQuery.inArray(extension[0], self.imagesExtensions);
                    if (isImage !== -1) {
                        html += '<div class="chat-mes-attachment text-right"><a data-gallery-item href="' + attachments[key] + '"><img src="' + attachments[key] + '" /></a></div>';
                    } else {
                        let filename = attachments[key].replace(/^.*[\\\/]/, '');
                        html += '<div class="chat-mes-attachment text-right"><a href="' + attachments[key] + '" download><img class="link-attachment-icon" src="/images/new/attachment-icon.png"/>' + filename + '</a></div>';
                    }
                }
            }
        }

        return html;
    };

    /**
     * @param dialogId
     */
    this.scrollWindowToBottom = function (dialogId) {
        setTimeout(function() {
            jQuery('div[data-chat-id=' + dialogId + ']').find('.chatU-body').scrollTop(90000);
        }, 150);
    };

    /**
     * @param dialogId
     */
    this.showLoading = function (dialogId) {
        jQuery('div[data-chat-id=' + dialogId + ']').find('.chatU-loading').removeClass('hidden');
        self.scrollWindowToBottom();
    };

    /**
     * @param dialogId
     */
    this.hideLoading = function(dialogId) {
        jQuery('div[data-chat-id=' + dialogId + ']').find('.chatU-loading').addClass('hidden');
        self.scrollWindowToBottom();
    };

    /**
     * @param dialogId
     */
    this.switchToOnline = function(dialogId) {
        jQuery('div[data-chat-id=' + dialogId + ']').find('.offline').removeClass('offline').addClass('online');
    };

    this.resetHeaderDialogs = function() {
        $('#header-messages-container').html('');
    };
}