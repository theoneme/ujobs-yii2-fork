<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.12.2017
 * Time: 16:34
 */

namespace frontend\modules\messenger\helpers;

use yii\helpers\Html;

/**
 * Class SmileHelper
 * @package frontend\modules\messenger\helpers
 */
class SmileHelper
{
    const SMILES_PATH = '/images/smiles/';

    /**
     * @param $assetPath
     * @param $input
     * @return string
     */
    public static function applySmiles($assetPath, $input)
    {
        $replaces = [
            ':)' => Html::img($assetPath . self::SMILES_PATH . '1.png'),
            ':(' => Html::img($assetPath . self::SMILES_PATH . '2.png'),
            ';)' => Html::img($assetPath . self::SMILES_PATH . '3.png'),
            ':p' => Html::img($assetPath . self::SMILES_PATH . '4.png'),
            '8-)' => Html::img($assetPath . self::SMILES_PATH . '5.png'),
            'XD' => Html::img($assetPath . self::SMILES_PATH . '6.png'),
            ':O' => Html::img($assetPath . self::SMILES_PATH . '8.png'),
            ':E' => Html::img($assetPath . self::SMILES_PATH . '9.png'),
            ';-*' => Html::img($assetPath . self::SMILES_PATH . '10.png'),
            ':-*' => Html::img($assetPath . self::SMILES_PATH . '11.png'),
            ':*' => Html::img($assetPath . self::SMILES_PATH . '11.png'),
            ';(' => Html::img($assetPath . self::SMILES_PATH . '12.png'),
            ':|' => Html::img($assetPath . self::SMILES_PATH . '13.png'),
            ':D' => Html::img($assetPath . self::SMILES_PATH . '14.png'),
            '<3' => Html::img($assetPath . self::SMILES_PATH . '15.png'),
            '=/' => Html::img($assetPath . self::SMILES_PATH . '16.png'),
            ':-[' => Html::img($assetPath . self::SMILES_PATH . '17.png'),
            ':-Х' => Html::img($assetPath . self::SMILES_PATH . '18.png'),
            ']:->' => Html::img($assetPath . self::SMILES_PATH . '19.png'),
            '>:)' => Html::img($assetPath . self::SMILES_PATH . '20.png'),
            '>:(' => Html::img($assetPath . self::SMILES_PATH . '21.png'),
            '|-)' => Html::img($assetPath . self::SMILES_PATH . '22.png'),
            'O:-)' => Html::img($assetPath . self::SMILES_PATH . '23.png'),
        ];

        return strtr($input, $replaces);
    }
}
