<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.04.2018
 * Time: 12:26
 */

namespace frontend\modules\messenger\models;

use common\models\Conversation;
use common\models\ConversationMember;
use common\models\user\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class AddMemberToChatForm
 * @package frontend\modules\messenger\models
 *
 * @property Conversation $conversation
 */
class AddMemberToChatForm extends Model
{
    /**
     * @var integer
     */
    public $member;
    /**
     * @var Conversation
     */
    private $_conversation;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['member', 'membersValidator'],
        ];
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function membersValidator($attribute, $param)
    {
        $member = (int)$this->member;

        if($member < 1) {
            $this->addError($attribute, Yii::t('messenger', 'User is not specified!'));
        }

        $userExists = User::find()->where(['id' => $member])->exists();
        if($userExists === false) {
            $this->addError($attribute, Yii::t('messenger', 'User ID {id} does not exist!', ['id' => $member]));
        }

        $conversationMemberExists = ConversationMember::find()
            ->where(['conversation_id' => $this->conversation->id, 'user_id' => $member])
            ->exists();
        if($conversationMemberExists === true) {
            $this->addError($attribute, Yii::t('messenger', 'This user already exists in this conversation'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member' => Yii::t('messenger', 'Group Chat Member')
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!empty($this->validateAll())) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $member = new ConversationMember([
            'conversation_id' => $this->conversation->id,
            'user_id' => $this->member
        ]);
        if ($member->save() === true) {
            $transaction->commit();

            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->_conversation;
    }

    /**
     * @param Conversation $conversation
     */
    public function setConversation(Conversation $conversation)
    {
        $this->_conversation = $conversation;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);
    }

    /**
     * @param $id
     */
    public function loadConversation($id)
    {
        $this->conversation = Conversation::findOne((int)$id);
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->conversation->validateWithRelations()
        );
    }
}