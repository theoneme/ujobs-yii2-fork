<?php

namespace frontend\modules\messenger\models;

use common\models\Category;
use common\models\Conversation;
use common\models\ConversationMember;
use common\models\Message;
use common\models\user\Profile;
use common\models\UserAttribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\base\Model;

/**
 * Class InfoChatForm
 * @package frontend\modules\messenger\models
 *
 * @property Conversation $conversation
 */
class InfoChatForm extends Model
{
    /**
     * @var integer
     */
    public $title;
    /**
     * @var integer
     */
    public $avatar;
    /**
     * @var string
     */
    public $locale = '';
    /**
     * @var string
     */
    public $categories = '';
    /**
     * @var string
     */
    public $skills = '';
    /**
     * @var string
     */
    public $specialties = '';
    /**
     * @var array
     */
    public $categoriesArray = [];
    /**
     * @var array
     */
    public $skillsArray = [];
    /**
     * @var array
     */
    public $specialtiesArray = [];
    /**
     * @var array
     */
    public $users = [];
    /**
     * @var string
     */
    public $error = '';
    /**
     * @var Conversation
     */
    private $_conversation;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 75],
            ['avatar', 'string', 'max' => 155],
            ['locale', 'required'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
//            ['categories', 'required'],
            ['categories', 'categoriesValidator', 'skipOnEmpty' => true],
            ['skills', 'skillsValidator', 'skipOnEmpty' => true],
            ['specialties', 'specialtiesValidator', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function categoriesValidator($attribute, $param)
    {
        $count = Category::find()->select('id')->where(['id' => $this->categoriesArray])->count();

        if (count($this->categoriesArray) > $count) {
            $this->addError($attribute, Yii::t('yii', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel($attribute)]));
        }
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function skillsValidator($attribute, $param)
    {
        $count = AttributeValue::find()
            ->joinWith(['relatedAttribute'])
            ->select(['mod_attribute_value.id', 'mod_attribute_value.attribute_id', 'mod_attribute_value.alias'])
            ->where(['mod_attribute.alias' => 'skill', 'mod_attribute_value.status' => AttributeValue::STATUS_APPROVED, 'mod_attribute_value.id' => $this->skillsArray])
            ->groupBy('mod_attribute_value.id')
            ->count();

        if (count($this->skillsArray) > $count) {
            $this->addError($attribute, Yii::t('yii', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel($attribute)]));
        }
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function specialtiesValidator($attribute, $param)
    {
        $count = AttributeValue::find()
            ->joinWith(['relatedAttribute'])
            ->select(['mod_attribute_value.id', 'mod_attribute_value.attribute_id', 'mod_attribute_value.alias'])
            ->where(['mod_attribute.alias' => 'specialty', 'mod_attribute_value.status' => AttributeValue::STATUS_APPROVED, 'mod_attribute_value.id' => $this->specialtiesArray])
            ->groupBy('mod_attribute_value.id')
            ->count();

        if (count($this->specialtiesArray) > $count) {
            $this->addError($attribute, Yii::t('yii', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel($attribute)]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('messenger', 'Group Chat Title'),
            'avatar' => Yii::t('messenger', 'Group Chat Avatar'),
            'members' => Yii::t('messenger', 'Group Chat Members'),
            'categories' => Yii::t('app', 'Categories'),
            'skills' => Yii::t('app', 'Skills'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();

        $this->conversation->title = $this->title;
        $this->conversation->avatar = $this->avatar;
        if ($this->conversation->save() === true) {
            $languageAttributes = [
                'en-GB' => 'english',
                'uk-UA' => 'ukrainian',
                'es-ES' => 'spanish',
                'ka-GE' => 'georgian',
                'zh-CN' => 'chinese',
                'ru-RU' => 'russian',
            ];
            $this->users = Profile::find()
                ->joinWith(['user.userAttributes', 'translations', 'userCategories'])
                ->where(['profile.status' => Profile::STATUS_ACTIVE])
                ->andWhere(['or',
                    ['exists', UserAttribute::find()
                        ->from(['ua1' => 'user_attribute'])
                        ->andWhere("profile.user_id = ua1.user_id")
                        ->andWhere(["ua1.entity_alias" => 'skill', 'ua1.value' => $this->skillsArray])
                    ],
                    ['exists', UserAttribute::find()
                        ->from(['ua2' => 'user_attribute'])
                        ->andWhere("profile.user_id = ua2.user_id")
                        ->andWhere(["ua2.entity_alias" => 'specialty', 'ua2.value' => $this->specialtiesArray])
                    ],
                    ['user_category.category_id' => $this->categoriesArray]
                ])
                ->andWhere(['user_attribute.value_alias' => $languageAttributes[$this->locale], 'user_attribute.entity_alias' => 'language'])
                ->all();

            if (!count($this->users)) {
                $this->error = Yii::t('messenger', 'No users found for this query');
                return false;
            }
            $conversationId = $this->conversation->id;
            $membersData = array_map(function($var) use ($conversationId){
                /* @var Profile*/
                return ['user_id' => $var->user_id, 'conversation_id' => $conversationId, 'status' => ConversationMember::STATUS_ACTIVE];
            }, $this->users);
            $membersData[] = ['user_id' => Yii::$app->user->identity->getCurrentId(), 'conversation_id' => $conversationId, 'status' => ConversationMember::STATUS_ACTIVE];
            Yii::$app->db->createCommand()->batchInsert('conversation_member',
                ['user_id', 'conversation_id', 'status'],
                $membersData
            )->execute();
            $message = new Message([
                'conversation_id' => $conversationId,
                'type' => Message::TYPE_GROUP_CHAT_INIT,
                'message' => Yii::t('messenger', 'Welcome to my info chat!')
            ]);
            $message->save();

            $transaction->commit();

            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->_conversation;
    }

    /**
     * @param Conversation $conversation
     */
    public function setConversation(Conversation $conversation)
    {
        $this->_conversation = $conversation;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);
        $this->categoriesArray = !empty($this->categories) ? explode(',', $this->categories) : [];
        $this->skillsArray = !empty($this->skills) ? explode(',', $this->skills) : [];
        $this->specialtiesArray = !empty($this->specialties) ? explode(',', $this->specialties) : [];
    }
}