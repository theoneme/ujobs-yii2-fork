<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.04.2018
 * Time: 14:16
 */

namespace frontend\modules\messenger\models;

use common\models\Conversation;
use common\models\Message;
use common\models\user\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class GroupChatForm
 * @package frontend\modules\messenger\models
 *
 * @property Conversation $conversation
 */
class GroupChatForm extends Model
{
    /**
     * @var integer
     */
    public $title;
    /**
     * @var integer
     */
    public $avatar;
    /**
     * @var array
     */
    public $members = [];
    /**
     * @var Conversation
     */
    private $_conversation;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 75],
            ['avatar', 'string', 'max' => 155],
            ['members', 'membersValidator']
        ];
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function membersValidator($attribute, $param)
    {
        $metIDs = [];

        if (!empty($this->members) && is_array($this->members)) {
            foreach ($this->members as $member) {
                $member = (int)$member;
                if ($member < 1) continue;

                $memberExists = User::find()->where(['id' => $member])->exists();
                if ($memberExists === true) {
                    if ($member !== userId()) {
                        if (in_array($member, $metIDs)) {
                            $this->addError($attribute, Yii::t('messenger', 'User ID {id} met more than once!', ['id' => $member]));
                        } else {
                            $metIDs[] = $member;
                        }
                    } else {
                        $this->addError($attribute, Yii::t('messenger', 'You do not have to add yourself', ['id' => $member]));
                    }
                } else {
                    $this->addError($attribute, Yii::t('messenger', 'User ID {id} does not exist!', ['id' => $member]));
                }
            }
        }

        if (count($metIDs) < 2) {
            $this->addError($attribute, Yii::t('messenger', 'You have to add at least {count, plural, one{# member} other{# members}}', ['count' => 2]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('messenger', 'Group Chat Title'),
            'avatar' => Yii::t('messenger', 'Group Chat Avatar'),
            'members' => Yii::t('messenger', 'Group Chat Members')
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!empty($this->validateAll())) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->conversation->attributes = $this->attributes;
        if ($this->conversation->save() === true) {
            $message = new Message([
                'conversation_id' => $this->conversation->id,
                'type' => Message::TYPE_GROUP_CHAT_INIT,
                'message' => Yii::t('account', 'Welcome to my group chat!')
            ]);
            $message->save();

            $transaction->commit();

            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->_conversation;
    }

    /**
     * @param Conversation $conversation
     */
    public function setConversation(Conversation $conversation)
    {
        $this->_conversation = $conversation;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);

        $this->conversation->bind('conversationMembers')->attributes = ['user_id' => userId()];
        foreach ($this->members as $member) {
            if (is_numeric($member)) {
                $this->conversation->bind('conversationMembers')->attributes = ['user_id' => $member];
            }
        }
    }

    /**
     * @param $id
     */
    public function loadConversation($id)
    {
        $this->conversation = Conversation::findOne((int)$id);
        $this->attributes = $this->conversation->attributes;
        $this->members = array_map(function ($value) {
            return $value->id;
        }, $this->conversation->conversationMembers);
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->conversation->validateWithRelations()
        );
    }
}