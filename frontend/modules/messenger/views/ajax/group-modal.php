<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.04.2018
 * Time: 17:44
 */
use common\helpers\FileInputHelper;
use frontend\modules\messenger\models\GroupChatForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var GroupChatForm $groupChatForm
 */

?>

    <div class="modal fade" id="group-chat-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('messenger', 'Create Group Chat') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'group-chat-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/messenger/ajax/create-group'],
                    ]); ?>

                    <div class="flex flex-v-center messenger-group-form-head">
                        <div class="messenger-group-file-input">
                            <?php if ($groupChatForm->avatar) { ?>
                                <label class="messenger-group-avatar-input" style="background: url(<?= $groupChatForm->avatar ?>"></label>
                            <?php } else { ?>
                                <label class="messenger-group-avatar-input">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                </label>
                            <?php } ?>
                            <div class="hidden">
                                <?= FileInput::widget(
                                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                        'id' => 'group-chat-image',
                                        'options' => ['accept' => 'image/*', 'class' => 'default-form-control'],
                                        'pluginOptions' => [
                                            'overwriteInitial' => true,
                                            'initialPreview' => [],
                                            'initialPreviewConfig' => [],
                                            'allowedFileExtensions' => null,
                                        ]
                                    ])
                                ) ?>

                                <?= $form->field($groupChatForm, 'avatar')->hiddenInput([
                                    'id' => 'avatar-input',
                                    'value' => $groupChatForm->avatar,
                                ])->label(false); ?>
                            </div>
                        </div>
                        <div class="messenger-group-title flex-grown">
                            <?= $form->field($groupChatForm, 'title', [
                                'inputOptions' => [
                                    'placeholder' => Yii::t('messenger', 'Set Group Chat Title'),
                                    'class' => 'transparent'
                                ]
                            ])->label(false) ?>
                        </div>
                    </div>

                    <div class="messenger-group-members-list-header text-left">
                        <?= Yii::t('messenger', 'Group Chat Members') ?>
                    </div>

                    <div class="messenger-group-members-list text-left">
                        <div id="messenger-group-member-list-container" class="messenger-group-member-list-container">
                            <?php if (count($groupChatForm->members) === 0) { ?>
                                <?= Html::tag('span', Yii::t('messenger', 'No members yet'), ['id' => 'messenger-group-member-list-no-member']) ?>
                            <?php } ?>
                        </div>

                        <div id="messenger-group-member-search" style="display:none">
                            <?= Html::input('text', 'new-member', null, ['class' => 'form-control default-form-control', 'id' => 'messenger-group-member-search-input']) ?>
                        </div>

                        <?= $form->field($groupChatForm, 'members[]')->hiddenInput()->label(false); ?>

                        <div class="messenger-group-add-member-button-block">
                            <?= Html::a(Html::tag('i', '&nbsp;', ['class' => 'fa fa-plus']) . Yii::t('messenger', 'Add member'), '#', ['data-action' => 'group-chat-add-member']) ?>
                        </div>
                    </div>

                    <div class="messenger-group-members-actions text-right">
                        <?= Html::button(Yii::t('messenger', 'Cancel'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
                        <?= Html::submitButton(Yii::t('messenger', 'Create Group Chat'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $memberListUrl = Url::to(['/ajax/sellers-list']);
$script = <<<JS
    new autoComplete({
        selector: "#messenger-group-member-search-input",
        minChars: 2,
        autoFocus: false,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$memberListUrl}', {query: request}, function(data) { 
                response(data); 
            });     
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion flex flex-v-center" data-img="' + item.img + '" data-name="' + item.value + '" data-id="' + item.id + '">'
                + '<div class="messenger-group-member-list-item-avatar">'
                + '<img src="' + item.img + '"/>'
                + '</div>'
                + '<div class="messenger-group-member-list-item-name">'
                + item.value.replace(re, "<b>$1</b>")
                + '</div>'
                + '</div>';
        },
        onSelect: function(e, term, item) {
            let html = '<div class="flex flex-v-center">'
                + '<input type="hidden" name="GroupChatForm[members][]" value="' + item.getAttribute('data-id') +'"/>'
                + '<div class="messenger-group-member-list-item-avatar">'
                + '<img src="' + item.getAttribute('data-img') + '"/>'
                + '</div>'
                + '<div class="messenger-group-member-list-item-name flex-grown">'
                + item.getAttribute('data-name')
                + '</div>'
                + '<div class="messenger-group-member-list-item-remove">'
                + '<a href="#" data-action="group-chat-remove-member"><i class="fa fa-close"></i></a>'
                + '</div>'
                + '</div>';
            
            $('#messenger-group-member-list-container').append(html);
            $('#messenger-group-member-list-no-member').hide();
            $('#messenger-group-member-search').hide();
        }
    });

    $("#group-chat-image").on('fileselect', function() {
        $(this).fileinput("upload");
    }).on('fileuploaded', function (event, data, previewId, index) {
        let image = data.response.uploadedPath,
            input = $('.messenger-group-avatar-input');
        input.html('');
        input.css('background-image', 'url(' + image + ')');
        $('#avatar-input').val(image);

    });
    $('.messenger-group-avatar-input').on('click', function (event) {
        event.preventDefault();
        $(this).parent().find('#group-chat-image').click();
    });

    $('[data-action="group-chat-add-member"]').on('click', function () {
        $('#messenger-group-member-search').show();

        return false;
    });
    
    $('#group-chat-form').on('beforeSubmit', function() {
        let url = $(this).attr('action'),
            data = $(this).serialize();
        
        $.post(url, data, function(response) {
            if(response.success === true) {
                alertCall('top', 'success', response.message);
                $('#group-chat-modal').modal('hide');
                $('#header-messages-container').html('');
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
