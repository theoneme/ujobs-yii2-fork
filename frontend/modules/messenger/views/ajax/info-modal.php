<?php

use common\helpers\FileInputHelper;
use frontend\assets\SelectizeAsset;
use frontend\modules\messenger\models\InfoChatForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var InfoChatForm $model
 * @var View $this
 */

SelectizeAsset::register($this);
?>

    <div class="modal fade" id="group-chat-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('messenger', 'Create Info Chat') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'group-chat-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/messenger/ajax/create-info-group'],
                    ]); ?>

                    <div class="flex flex-v-center messenger-group-form-head">
                        <div class="messenger-group-file-input">
                            <label class="messenger-group-avatar-input">
                                <i class="fa fa-camera" aria-hidden="true"></i>
                            </label>
                            <div class="hidden">
                                <?= FileInput::widget(
                                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                        'id' => 'group-chat-image',
                                        'options' => ['accept' => 'image/*', 'class' => 'default-form-control'],
                                        'pluginOptions' => [
                                            'overwriteInitial' => true,
                                            'initialPreview' => [],
                                            'initialPreviewConfig' => [],
                                            'allowedFileExtensions' => null,
                                        ]
                                    ])
                                ) ?>

                                <?= $form->field($model, 'avatar')->hiddenInput(['id' => 'avatar-input'])->label(false); ?>
                            </div>
                        </div>
                        <div class="messenger-group-title flex-grown">
                            <?= $form->field($model, 'title', [
                                'inputOptions' => [
                                    'placeholder' => Yii::t('messenger', 'Set Chat Title'),
                                    'class' => 'transparent'
                                ]
                            ])->label(false) ?>
                        </div>
                    </div>

                    <div class="messenger-group-members-list-header text-left">
                        <?= Yii::t('messenger', 'Info Chat Filters') ?>
                    </div>

                    <div class="text-left">
                        <?= $form->field($model, 'locale')->dropDownList(
                            array_map(function($var){ return Yii::t('app', $var);}, Yii::$app->params['languages'])
                        )->label(Yii::t('app', 'Language')) ?>

                        <?= $form->field($model, 'categories')->textarea([
                            'id' => 'messenger-info-form-categories',
                            'class' => 'readsym'
                        ])->label(Yii::t('app', 'Categories')) ?>

                        <?= $form->field($model, 'skills')->textarea([
                            'id' => 'messenger-info-form-skills',
                            'class' => 'readsym'
                        ])->label(Yii::t('app', 'Skills')) ?>

                        <?= $form->field($model, 'specialties')->textarea([
                            'id' => 'messenger-info-form-specialties',
                            'class' => 'readsym'
                        ])->label(Yii::t('app', 'Specialties')) ?>
                    </div>

                    <div class="messenger-group-members-actions text-right">
                        <?= Html::button(Yii::t('messenger', 'Cancel'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
                        <?= Html::submitButton(Yii::t('messenger', 'Create Info Chat'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php
$categoriesUrl = Url::to(['/ajax/category-list']);
$skillsUrl = Url::to(['/ajax/attribute-list', 'alias' => 'skill']);
$specialtiesUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$script = <<<JS
    $("#messenger-info-form-categories").selectize({
        create: false,
        maxItems: null,
        valueField: "id",
        labelField: "label",
        searchField: ["label"],
        plugins: ["remove_button"],
        persist: false,
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$categoriesUrl", 
                {query: encodeURIComponent(query)},
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        }
    });

    $("#messenger-info-form-skills").selectize({
        create: false,
        maxItems: null,
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$skillsUrl", 
                {query: encodeURIComponent(query)},
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        }
    });
    
    $("#messenger-info-form-specialties").selectize({
        create: false,
        maxItems: null,
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$specialtiesUrl", 
                {query: encodeURIComponent(query)},
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        }
    });

    $("#group-chat-image").on('fileselect', function() {
        $(this).fileinput("upload");
    }).on('fileuploaded', function (event, data, previewId, index) {
        let image = data.response.uploadedPath,
            input = $('.messenger-group-avatar-input');
        input.html('');
        input.css('background-image', 'url(' + image + ')');
        $('#avatar-input').val(image);

    });
    $('.messenger-group-avatar-input').on('click', function (event) {
        event.preventDefault();
        $(this).parent().find('#group-chat-image').click();
    });
    
    $('#group-chat-form').on('beforeSubmit', function() {
        let url = $(this).attr('action'),
            data = $(this).serialize();
        
        $.post(url, data, function(response) {
            if(response.success === true) {
                alertCall('top', 'success', response.message);
                $('#group-chat-modal').modal('hide');
                $('#header-messages-container').html('');
            }
            else {
                if (response.message) {
                    alertCall('top', 'error', response.message);
                }
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
