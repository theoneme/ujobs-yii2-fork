<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.04.2018
 * Time: 12:45
 */

use common\helpers\FileInputHelper;
use frontend\modules\messenger\models\AddMemberToChatForm;
use frontend\modules\messenger\models\GroupChatForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var AddMemberToChatForm $addMemberToChatForm
 */

?>

    <div class="modal fade" id="group-chat-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('messenger', 'Add member to group chat') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'group-member-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/messenger/ajax/add-member-to-chat', 'id' => $id],
                    ]); ?>

                    <div class="messenger-group-members-list-header text-left">
                        <?= Yii::t('messenger', 'Group Chat Member') ?>
                    </div>

                    <div class="messenger-group-members-list text-left">
                        <div id="messenger-group-member-list-container" class="messenger-group-member-list-container">

                        </div>

                        <div id="messenger-group-member-search">
                            <?= Html::input('text', 'new-member', null, ['class' => 'form-control default-form-control', 'id' => 'messenger-group-member-search-input']) ?>
                        </div>
                    </div>

                    <div class="messenger-group-members-actions text-right">
                        <?= Html::button(Yii::t('messenger', 'Cancel'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
                        <?= Html::submitButton(Yii::t('messenger', 'Add Member'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $memberListUrl = Url::to(['/ajax/sellers-list']);
$script = <<<JS
    new autoComplete({
        selector: "#messenger-group-member-search-input",
        minChars: 2,
        autoFocus: false,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$memberListUrl}', {query: request}, function(data) { 
                response(data); 
            });     
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion flex flex-v-center" data-img="' + item.img + '" data-name="' + item.value + '" data-id="' + item.id + '">'
                + '<div class="messenger-group-member-list-item-avatar">'
                + '<img src="' + item.img + '"/>'
                + '</div>'
                + '<div class="messenger-group-member-list-item-name">'
                + item.value.replace(re, "<b>$1</b>")
                + '</div>'
                + '</div>';
        },
        onSelect: function(e, term, item) {
            $('#messenger-group-member-list-container').html('');
            let html = '<div class="flex flex-v-center">'
                + '<input type="hidden" name="AddMemberToChatForm[member]" value="' + item.getAttribute('data-id') +'"/>'
                + '<div class="messenger-group-member-list-item-avatar">'
                + '<img src="' + item.getAttribute('data-img') + '"/>'
                + '</div>'
                + '<div class="messenger-group-member-list-item-name flex-grown">'
                + item.getAttribute('data-name')
                + '</div>'
                + '<div class="messenger-group-member-list-item-remove">'
                + '<a href="#" data-action="group-chat-remove-member"><i class="fa fa-close"></i></a>'
                + '</div>'
                + '</div>';
            
            $('#messenger-group-member-list-container').append(html);
        }
    });
    
    $('#group-member-form').on('beforeSubmit', function() {
        let url = $(this).attr('action'),
            data = $(this).serialize();
        
        $.post(url, data, function(response) {
            if(response.success === true) {
                alertCall('top', 'success', response.message);
                $('#group-chat-modal').modal('hide');
                
                $.pjax.reload({
                    container: '#messages-pjax'
                });
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
