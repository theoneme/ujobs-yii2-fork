<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2017
 * Time: 10:47
 */

use common\helpers\FileInputHelper;
use common\models\user\User;
use frontend\modules\messenger\assets\MessengerAsset;
use frontend\modules\messenger\helpers\SmileHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var array $data */

$assetBaseUrl = Yii::$app->utility->getPublishedAssetPath(MessengerAsset::class);

?>

<div class="chatU-block" data-chat-id="<?= $data['conversationId'] ?>">
    <div class="chatU-header">
        <a class="chatU-user"
           data-action="open-profile"
           href="<?= $data['header']['profileRoute'] === null ? '#' : Url::to($data['header']['profileRoute']) ?>">
            <span><?= $data['header']['title'] ?></span>
            <span class="chatU-status <?= $data['header']['online'] === true ? 'online' : 'offline'?>">
                &nbsp;
            </span>

        </a>
        <div class="chatU-settings">
            <?php if (in_array(Yii::$app->user->identity->access, [User::ROLE_ADMIN, User::ROLE_MODERATOR])) {
                echo Html::a('<i class="fa fa-info-circle"></i>', null, [
                    'data-action' => 'create-info-chat',
                    'class' => 'hint--top',
                    'data-hint' => Yii::t('messenger', 'Create Info Chat')
                ]);
            }?>
            <a data-action="create-group-chat" href="#" class="hint--top" data-hint="<?= Yii::t('messenger', 'Create Group Chat') ?>">
                <i class="fa fa-user-plus"></i>
            </a>
            <!--            <a data-action="open-settings" href="#" class="hint--top"-->
            <!--               data-hint="--><?//= Yii::t('messenger', 'Settings') ?><!--">-->
            <!--                <i class="fa fa-cog"></i>-->
            <!--            </a>-->
            <a data-action="dialog-maximize" href="<?= Url::to($data['header']['dialogRoute']) ?>" class="hint--top" data-hint="<?= Yii::t('messenger', 'Expand dialog') ?>">
                <i class="fa fa-window-maximize"></i>
            </a>
            <a data-action="dialog-close" data-chat-id="<?= $data['conversationId'] ?>" href="#" class="hint--top" data-hint="<?= Yii::t('messenger', 'Close dialog') ?>">
                <i class="fa fa-close"></i>
            </a>
            <div class="chat-setting-menu">
                <div class="chat-setting-group">
                    <a href="#"><?= Yii::t('messenger', 'Add files') ?></a>
                    <a href="#"><?= Yii::t('messenger', 'Create Group Chat') ?></a>
                </div>
                <div class="chat-setting-group">
                    <a href="#"><?= Yii::t('messenger', 'Turn off sound') ?></a>
                    <a href="#"><?= Yii::t('messenger', 'Remove dialog') ?></a>
                    <a href="#"><?= Yii::t('messenger', 'Block messages') ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="chatU-body-over">
        <div class="chatU-body">
            <div class="chatU-messages-list">
                <?php foreach ($data['messages'] as $message) { ?>
                    <?php if ($message['side'] === 'left' && (strlen($message['text']) > 0 || !empty($message['attachments']))) { ?>
                        <div class="chatU-mes user">
                            <div class="chat-mes-ava">
                                <?= Html::img($message['sender']['image']) ?>
                            </div>
                            <?php if (strlen($message['text']) > 0) { ?>
                                <div class="chat-mes-text"><?= SmileHelper::applySmiles($assetBaseUrl, Html::encode(trim($message['text']))) ?></div>
                            <?php } ?>
                            <?php if (!empty($message['attachments'])) { ?>
                                <div class="chat-mes-attachments">
                                    <?php foreach ($message['attachments'] as $attachment) { ?>
                                        <div class="chat-mes-attachment text-right">
                                            <?php if ($attachment['type'] === 'image') { ?>
                                                <a href="<?= $attachment['attachment'] ?>" data-gallery-item>
                                                    <?= Html::img($attachment['attachment']) ?>
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?= $attachment['attachment'] ?>" download>
                                                    <?= Html::img("/images/new/attachment-icon.png", ['class' => 'link-attachment-icon']) ?>
                                                    <?= $attachment['filename'] ?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="chatU-mes-info">
                            <?= $message['created_at'] ?>
                        </div>
                    <?php } else { ?>
                        <?php if (strlen($message['text']) > 0 || !empty($message['attachments'])) { ?>
                            <div class="chatU-mes you">
                                <?php if (strlen($message['text']) > 0) { ?>
                                    <div class="chat-mes-text"><?= SmileHelper::applySmiles($assetBaseUrl, Html::encode(trim($message['text']))) ?></div>
                                <?php } ?>
                                <?php if (!empty($message['attachments'])) { ?>
                                    <?php foreach ($message['attachments'] as $attachment) { ?>
                                        <div class="chat-mes-attachment text-right">
                                            <?php if ($attachment['type'] === 'image') { ?>
                                                <a href="<?= $attachment['attachment'] ?>" data-gallery-item>
                                                    <?= Html::img($attachment['attachment']) ?>
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?= $attachment['attachment'] ?>" download>
                                                    <?= Html::img("/images/new/attachment-icon.png", ['class' => 'link-attachment-icon']) ?>
                                                    <?= $attachment['filename'] ?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="chatU-mes-send <?= $message['unread'] ? 'send' : 'read' ?>">
                                    <?= $message['created_at'] ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="chatU-body-bottom">
                <div class="chatU-loading hidden text-right"><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i></div>
            </div>
        </div>
    </div>
    <div class="chatU-footer">
        <div class="like-text" contenteditable="true" id="input-block-<?= $data['conversationId'] ?>"></div>
        <div class="bottom-panel">
            <a data-action="smiles" class="chatU-footer-icon" href="#"><i class="fa fa-smile-o"></i></a>
            <!--            <a class="chatU-footer-icon" href="#"><i class="fa fa-paperclip"></i></a>-->
            <label class="chatU-file-upload">
                <span class="chatU-footer-icon">
                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                </span>
                <span style="display: none">
                    <?= FileInput::widget(
                        ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                            'options' => ['multiple' => true, 'class' => 'chatU-file-upload-input', 'accept' => false],
                            'id' => 'chatU-file-upload-input-' . $data['conversationId'],
                            'pluginOptions' => [
                                'overwriteInitial' => true,
                                'initialPreview' => [],
                                'initialPreviewConfig' => [],
                                'allowedFileExtensions' => null,
                            ]
                        ])
                    ) ?>
                </span>
            </label>
            <div class="smile-window">
                <a data-smile=":)" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/1.png') ?></a>
                <a data-smile=":(" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/2.png') ?></a>
                <a data-smile=";)" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/3.png') ?></a>
                <a data-smile=":p" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/4.png') ?></a>
                <a data-smile="8-)" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/5.png') ?></a>
                <a data-smile="XD" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/6.png') ?></a>
                <a data-smile=":O" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/8.png') ?></a>
                <a data-smile=":E" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/9.png') ?></a>
                <a data-smile=";-*" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/10.png') ?></a>
                <a data-smile=":-*" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/11.png') ?></a>
                <a data-smile=":*" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/11.png') ?></a>
                <a data-smile=";(" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/12.png') ?></a>
                <a data-smile=":|" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/13.png') ?></a>
                <a data-smile=":D" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/14.png') ?></a>
                <a data-smile="<3" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/15.png') ?></a>
                <a data-smile="=/" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/16.png') ?></a>
                <a data-smile=":-[" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/17.png') ?></a>
                <a data-smile=":-Х" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/18.png') ?></a>
                <a data-smile="]:->" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/19.png') ?></a>
                <a data-smile=">:)" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/20.png') ?></a>
                <a data-smile=">:(" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/21.png') ?></a>
                <a data-smile="|-)" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/22.png') ?></a>
                <a data-smile="O:-)" href="#"><?= Html::img($assetBaseUrl . '/images/smiles/23.png') ?></a>
            </div>
        </div>
    </div>
</div>