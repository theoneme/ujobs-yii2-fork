<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2017
 * Time: 15:10
 */

namespace frontend\modules\messenger\assets;

use yii\web\AssetBundle;

/**
 * Class MessengerAsset
 * @package frontend\modules\messenger\assets
 */
class MessengerAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/messenger/web';
    public $css = [
//        'css/lightgallery.min.css',
//        'css/chat.css',
    ];
    public $js = [
//        'js/lightgallery.min.js',
//        'js/imagesloaded.pkgd.min.js',
        'js/script.js'
    ];
    public $depends = [
        'frontend\assets\CommonAsset'
    ];
}
