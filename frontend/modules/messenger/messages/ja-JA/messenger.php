<?php 
 return [
    'Add Member' => 'メンバを追加する',
    'Add files' => 'ファイルの追加',
    'Add member' => 'メンバを追加する',
    'Add member to group chat' => 'メンバを追加するグループチャット',
    'Block messages' => 'ブロックメッセージ',
    'Cancel' => '消',
    'Close dialog' => 'ダイアログ',
    'Create Group Chat' => 'をグループチャット',
    'Expand dialog' => 'の対話を始める',
    'Group Chat Avatar' => 'アバターなどへのアクセスをブロック',
    'Group Chat Has Been Created' => 'グループチャットの作成',
    'Group Chat Member' => '参加者のグループチャット',
    'Group Chat Member has been added' => '新しい会員を追加したグループチャット',
    'Group Chat Members' => 'グループチャット',
    'Group Chat Title' => 'その名のグループチャット',
    'No members yet' => 'まだ参加者',
    'Remove dialog' => '削除の対話',
    'Set Group Chat Title' => 'の名称を入力し、グループチャット',
    'This user already exists in this conversation' => 'このユーザーが既に追加され、現在の対話',
    'Turn off sound' => 'をオフにして音',
    'User ID {id} does not exist!' => 'ユーザ{id}が存在しない',
    'User ID {id} met more than once!' => 'ユーザ{id}が複数指定された',
    'User is not specified!' => 'ユーザが指定されていない',
    'You do not have to add yourself' => 'する必要はない自分で参加者の一員として、',
    'You have left group chat' => 'ですが、グループチャット',
    'You have to add at least {count, plural, one{# member} other{# members}}' => 'いてくれるものでなければなりませ以上{count, plural, one{#者} other{#参加者}}',
    'Create Info Chat' => 'を情報チャット',
    'Set Chat Title' => 'の名前を指定しゃべり',
    'Info Chat Filters' => 'ヤフーチャット',
    'Welcome to my info chat!' => 'Komemetalさんが生放送を行う情報チャット',
    'No users found for this query' => '申請の際には、ユーザが見つかりませんで',
    'Info Chat Has Been Created' => '情報チャットを作成した',
    '{count, plural, one{# member} other{# members}}' => '{count, plural, one{#者} few{#uchastnka} other{#参加者}}',
];