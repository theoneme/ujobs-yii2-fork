<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.12.2017
 * Time: 17:27
 */

return [
    'color' => null,
    'interactive' => true,
    'sourcePath' => 'frontend/modules/messenger',
    'messagePath' => 'frontend/modules/messenger/messages',
    'languages' => ['ru-RU', 'es-ES', 'uk-UA', 'ka-GE', 'zh-CN'],
    'translator' => 'Yii::t',
    'sort' => true,
    'overwrite' => true,
    'removeUnused' => true,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'format' => 'php',
    'db' => 'db',
    'sourceMessageTable' => '{{%source_message}}',
    'messageTable' => '{{%message}}',
    'catalog' => 'messages',
    'ignoreCategories' => [],
];
