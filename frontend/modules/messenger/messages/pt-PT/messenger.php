<?php 
 return [
    'Add Member' => 'Para adicionar um membro',
    'Add files' => 'Adicionar arquivos',
    'Add member' => 'Para adicionar um membro',
    'Add member to group chat' => 'Adicionar um membro a um grupo de bate-papo',
    'Block messages' => 'Bloquear mensagem',
    'Cancel' => 'Cancelamento',
    'Close dialog' => 'Fechar a janela',
    'Create Group Chat' => 'Criar um grupo de bate-papo',
    'Expand dialog' => 'Expandir o diálogo',
    'Group Chat Avatar' => 'Avatar bate-papo em grupo',
    'Group Chat Has Been Created' => 'O chat em grupo criado',
    'Group Chat Member' => 'Participante do grupo de bate-papo',
    'Group Chat Member has been added' => 'Um novo membro adicionado ao grupo de bate-papo',
    'Group Chat Members' => 'Os participantes do bate-papo em grupo',
    'Group Chat Title' => 'O nome do grupo de bate-papo',
    'No members yet' => 'Ainda não há membros',
    'Remove dialog' => 'Remover o diálogo',
    'Set Group Chat Title' => 'Insira o nome de um grupo de bate-papo',
    'This user already exists in this conversation' => 'Este usuário já foi adicionado no diálogo em curso',
    'Turn off sound' => 'Desligar o som',
    'User ID {id} does not exist!' => 'O usuário {id} não existe',
    'User ID {id} met more than once!' => 'O usuário {id} especificado mais de uma vez',
    'User is not specified!' => 'O usuário não especificado',
    'You do not have to add yourself' => 'Você não precisa adicionar a si mesmo como um participante',
    'You have left group chat' => 'Você deixou um grupo de bate-papo',
    'You have to add at least {count, plural, one{# member} other{# members}}' => 'Você deve adicionar pelo menos {count, plural, one{# participante} other{# participantes}}',
    'Create Info Chat' => 'Criar um chat',
    'Set Chat Title' => 'Especifique o nome do chat',
    'Info Chat Filters' => 'Filtros de informação de chat',
    'Welcome to my info chat!' => 'Bem-vindo à minha informação de bate-papo',
    'No users found for this query' => 'Para este pedido, os usuários não encontrado',
    'Info Chat Has Been Created' => 'Informação chat criado',
    '{count, plural, one{# member} other{# members}}' => '{count, plural, one{# participante} few{# участнка} other{# participantes}}',
];