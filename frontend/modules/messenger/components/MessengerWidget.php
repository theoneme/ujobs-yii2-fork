<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2017
 * Time: 15:31
 */

namespace frontend\modules\messenger\components;

use common\models\Conversation;
use common\models\ConversationMember;
use common\modules\livechat\models\LiveContactEmail;
use frontend\modules\messenger\models\GroupChatForm;
use yii\base\Widget;
use Yii;

/**
 * Class MessengerWidget
 * @package frontend\modules\messenger\components
 */
class MessengerWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'messenger';

    /**
     * @return string
     */
    public function run()
    {
        if(array_key_exists('enableNodeApplications', Yii::$app->params) && Yii::$app->params['enableNodeApplications'] === false) {
            return null;
        }

        $dialogIds =  Conversation::find()
            ->select('conversation.id')
            ->joinWith(['conversationMembers'])
            ->groupBy('conversation.id')
            ->where(['conversation.type' => Conversation::TYPE_DEFAULT, 'conversation_member.user_id' => userId()])
            ->andWhere(['not', ['conversation_member.status' => ConversationMember::STATUS_DELETED]])
            ->scalar();

        return $this->render($this->template, [
            'dialogs' => $dialogIds,
        ]);
    }
}