<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2017
 * Time: 15:34
 */

use common\helpers\SecurityHelper;
use frontend\modules\messenger\assets\MessengerAsset;
use yii\helpers\Url;

$assetBaseUrl = addslashes(MessengerAsset::register($this)->baseUrl);

?>

    <div id="messenger-group-modal-container">
    </div>

    <div class="chatU-content" id="dialog-container">
    </div>

<?php $messengerUrl = Yii::$app->params['chatUrl'];
$dataUrl = Url::to(['/messenger/ajax/get-dialog']);
$dataPaths = json_encode([
    'dialog' => Url::to(['/messenger/ajax/get-dialog']),
    'groupModal' => Url::to(['/messenger/ajax/get-group-modal']),
    'infoModal' => Url::to(['/messenger/ajax/get-info-modal']),
    'addMemberModal' => Url::to(['/messenger/ajax/get-member-modal'])
]);
$userId = Yii::$app->user->identity->getCurrentId();
$encryptedUId = SecurityHelper::encrypt($userId);
$name = addslashes(Yii::$app->user->identity->getSellerName());

$script = <<<JS
    let messenger = new Messenger('$messengerUrl', '$dataPaths', $userId);
    messenger.init("$assetBaseUrl");
    messenger.registerHandlers();
    messenger.connect('$encryptedUId', '$name');
JS;

$this->registerJs($script);
