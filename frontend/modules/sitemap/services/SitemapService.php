<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 03.11.2017
 * Time: 10:02
 */

namespace frontend\modules\sitemap\services;

use frontend\modules\sitemap\interfaces\SitemapDataInterface;

/**
 * Class SitemapService
 * @package frontend\modules\sitemap\services
 */
class SitemapService implements SitemapDataInterface
{
    /**
     * @var GlobalSitemapService
     */
    private $_globalSitemapService;

    /**
     * SitemapService constructor.
     * @param GlobalSitemapService $globalSitemapService
     */
    public function __construct(GlobalSitemapService $globalSitemapService)
    {
        $this->_globalSitemapService = $globalSitemapService;
    }

    /**
     * @param string $entity
     * @return int
     */
    public function getEntityCount($entity)
    {
        return $this->_globalSitemapService->getEntityCount($entity);
    }

    /**
     * @param string $entity
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function getItems($entity, $limit = null, $offset = null)
    {
        return $this->_globalSitemapService->getItems($entity, $limit, $offset);
    }
}