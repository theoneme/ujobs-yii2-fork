<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 03.11.2017
 * Time: 10:02
 */

namespace frontend\modules\sitemap\services;

use common\models\Category;
use common\models\Job;
use common\models\Page;
use common\models\UserPortfolio;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\Product;
use frontend\modules\sitemap\interfaces\SitemapDataInterface;
use frontend\modules\sitemap\mappers\SitemapMapper;
use frontend\modules\sitemap\Sitemap;
use yii\db\ActiveQuery;

/**
 * Class GlobalSitemapService
 * @package frontend\modules\sitemap\services
 */
class GlobalSitemapService implements SitemapDataInterface
{
    /**
     * @param string $entity
     * @return int
     */
    public function getEntityCount($entity)
    {
        $count = 0;

        switch ($entity) {
            case Sitemap::ENTITY_JOB:
                $count = Job::find()->select('id')->where(['status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB])->count();

                break;
            case Sitemap::ENTITY_JOB_TENDER:
                $count = Job::find()->select('id')->where(['status' => Job::STATUS_ACTIVE, 'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER]])->count();

                break;
            case Sitemap::ENTITY_PRODUCT:
                $count = Product::find()->select('id')->where(['status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT])->count();

                break;
            case Sitemap::ENTITY_PRODUCT_TENDER:
                $count = Product::find()->select('id')->where(['status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_TENDER])->count();

                break;
            case Sitemap::ENTITY_SERVICE_TAG:
                $count = AttributeValue::find()->select('id')->where(['status' => AttributeValue::STATUS_APPROVED, 'attribute_id' => 36])->count();

                break;
            case Sitemap::ENTITY_SKILL:
                $count = AttributeValue::find()->select('id')->where(['status' => AttributeValue::STATUS_APPROVED, 'attribute_id' => 38])->count();

                break;
            case Sitemap::ENTITY_PRODUCT_TAG:
                $count = AttributeValue::find()->select('id')->where(['status' => AttributeValue::STATUS_APPROVED, 'attribute_id' => 48])->count();

                break;
            case Sitemap::ENTITY_PAGE:
                $count = Page::find()->select('id')->where(['status' => Page::STATUS_ACTIVE, 'type' => 'default-page'])->count();

                break;
            case Sitemap::ENTITY_ARTICLE:
                $count = Page::find()->select('id')->where(['status' => Page::STATUS_ACTIVE, 'type' => 'article-page'])->count();

                break;
            case Sitemap::ENTITY_PORTFOLIO:
                $count = UserPortfolio::find()->select('id')->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->count();

                break;
            case Sitemap::ENTITY_JOB_CATEGORY:
            case Sitemap::ENTITY_TENDER_CATEGORY:
            case Sitemap::ENTITY_PRO_CATEGORY:
            case Sitemap::ENTITY_PORTFOLIO_CATEGORY:
                $count = Category::find()->select('id')->where(['type' => 'job_category', 'disabled' => false])->count();

                break;
            case Sitemap::ENTITY_PRODUCT_CATEGORY:
            case Sitemap::ENTITY_PRODUCT_TENDER_CATEGORY:
                $count = Category::find()->select('id')->where(['type' => 'product_category', 'disabled' => false])->count();

                break;
        }

        return $count;
    }

    /**
     * @param $entity
     * @param $limit
     * @param $offset
     * @return array
     */
    public function getItems($entity, $limit, $offset)
    {
        $data = [];

        switch ($entity) {
            case Sitemap::ENTITY_JOB:
                $query = Job::find()
                    ->with(['attachments'])
                    ->where(['status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB]);

                break;
            case Sitemap::ENTITY_JOB_TENDER:
                $query = Job::find()
                    ->with(['attachments'])
                    ->where(['status' => Job::STATUS_ACTIVE, 'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER]]);

                break;
            case Sitemap::ENTITY_PRODUCT:
                $query = Product::find()
                    ->with(['attachments'])
                    ->where(['status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT]);

                break;
            case Sitemap::ENTITY_PRODUCT_TENDER:
                $query = Product::find()
                    ->with(['attachments'])
                    ->where(['status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_TENDER]);

                break;
            case Sitemap::ENTITY_SERVICE_TAG:
                $query = AttributeValue::find()
                    ->where(['status' => AttributeValue::STATUS_APPROVED, 'attribute_id' => 36]);

                break;
            case Sitemap::ENTITY_PRODUCT_TAG:
                $query = AttributeValue::find()
                    ->where(['status' => AttributeValue::STATUS_APPROVED, 'attribute_id' => 48]);

                break;
            case Sitemap::ENTITY_SKILL:
                $query = AttributeValue::find()
                    ->where(['status' => AttributeValue::STATUS_APPROVED, 'attribute_id' => 38]);

                break;
            case Sitemap::ENTITY_PAGE:
                $query = Page::find()
                    ->with(['translations' => function (ActiveQuery $query) {
                        return $query->select(['entity', 'entity_id', 'title', 'locale']);
                    }, 'attachments'])
                    ->where(['status' => Page::STATUS_ACTIVE_FOR_SUBSCRIBERS, 'type' => 'default-page']);

                break;
            case Sitemap::ENTITY_ARTICLE:
                $query = Page::find()
                    ->with(['translations' => function (ActiveQuery $query) {
                        return $query->select(['entity', 'entity_id', 'title', 'locale']);
                    }, 'attachments'])
                    ->where(['status' => Page::STATUS_ACTIVE, 'type' => 'article-page']);

                break;
            case Sitemap::ENTITY_PORTFOLIO:
                $query = UserPortfolio::find()
                    ->with(['attachments'])
                    ->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE]);

                break;
            case Sitemap::ENTITY_JOB_CATEGORY:
            case Sitemap::ENTITY_TENDER_CATEGORY:
            case Sitemap::ENTITY_PRO_CATEGORY:
            case Sitemap::ENTITY_PORTFOLIO_CATEGORY:
                $query = Category::find()
                    ->with(['translations' => function (ActiveQuery $query) {
                        return $query->select(['entity', 'entity_id', 'title', 'locale', 'slug']);
                    }])
                    ->where(['type' => 'job_category']);

                break;
            case Sitemap::ENTITY_PRODUCT_CATEGORY:
            case Sitemap::ENTITY_PRODUCT_TENDER_CATEGORY:
                $query = Category::find()
                    ->with(['translations' => function (ActiveQuery $query) {
                        return $query->select(['entity', 'entity_id', 'title', 'locale', 'slug']);
                    }])
                    ->where(['type' => 'product_category']);

                break;
        }

        $items = $query
            ->limit($limit)
            ->offset($offset)
            ->orderBy(['id' => SORT_ASC])
            ->asArray()
            ->all();

        foreach ($items as $item) {
            $data[] = SitemapMapper::getMappedData($item);
        }

        return $data;
    }
}