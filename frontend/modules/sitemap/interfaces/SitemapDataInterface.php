<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.04.2019
 * Time: 14:57
 */

namespace frontend\modules\sitemap\interfaces;

/**
 * Interface SitemapDataInterface
 * @package frontend\modules\sitemap\interfaces
 */
interface SitemapDataInterface
{
    /**
     * @param string $entity
     * @return integer
     */
    public function getEntityCount($entity);

    /**
     * @param string $entity
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getItems($entity, $limit, $offset);
}