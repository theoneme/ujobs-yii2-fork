<?php

namespace frontend\modules\sitemap;

/**
 * Class Sitemap
 * @package frontend\modules\sitemap
 */
class Sitemap extends \yii\base\Module
{
    public const MAX_PER_FILE = 4000;
    public const BATCH_SIZE = 200;

    public const ENTITY_JOB = 'job';
    public const ENTITY_JOB_TENDER = 'tender';
    public const ENTITY_PRODUCT = 'product';
    public const ENTITY_PORTFOLIO = 'portfolio';
    public const ENTITY_PRODUCT_TENDER = 'product_tender';
    public const ENTITY_SERVICE_TAG = 'service_tag';
    public const ENTITY_SKILL = 'skill';
    public const ENTITY_PRODUCT_TAG = 'product_tag';
    public const ENTITY_PAGE = 'page';
    public const ENTITY_ARTICLE = 'article';

    public const ENTITY_JOB_CATEGORY = 'job_category';
    public const ENTITY_TENDER_CATEGORY = 'tender_category';
    public const ENTITY_PRODUCT_CATEGORY = 'product_category';
    public const ENTITY_PRODUCT_TENDER_CATEGORY = 'product_tender_category';
    public const ENTITY_PRO_CATEGORY = 'pro_category';
    public const ENTITY_PORTFOLIO_CATEGORY = 'portfolio_category';

    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\sitemap\controllers';
}
