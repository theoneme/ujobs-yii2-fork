<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.04.2019
 * Time: 16:22
 */

namespace frontend\modules\sitemap\mappers;

use common\interfaces\DataMapperInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\TranslationsMapper;
use yii\helpers\ArrayHelper;

/**
 * Class SitemapMapper
 * @package frontend\modules\sitemap\mappers
 */
class SitemapMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $images = AttachmentsMapper::getMappedData($rawData['attachments'] ?? []);
        $currentTranslation = TranslationsMapper::getMappedData($rawData['translations'] ?? []);
        $translations = TranslationsMapper::getMappedData($rawData['translations'] ?? [], TranslationsMapper::MODE_FULL);

        return [
            'image' => $images[0] ?? null,
            'slug' => ArrayHelper::remove($currentTranslation, 'slug') ?? $rawData['slug'] ?? $rawData['alias'] ?? $rawData['id'],
            'translations' => $translations,
            'changeFreq' => 'weekly',
            'priority' => 0.5,
            'lastMod' => array_key_exists('updated_at', $rawData) ? date(DATE_W3C, $rawData['updated_at']) : null
        ];
    }
}