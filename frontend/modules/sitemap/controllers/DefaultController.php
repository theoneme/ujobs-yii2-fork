<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.02.2017
 * Time: 12:20
 */

namespace frontend\modules\sitemap\controllers;

use common\controllers\FrontEndController;
use frontend\modules\sitemap\services\SitemapService;
use frontend\modules\sitemap\Sitemap;
use Yii;
use yii\base\Module;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class DefaultController
 * @package frontend\modules\sitemap\controllers
 */
class DefaultController extends FrontEndController
{
    /**
     * @var bool
     */
    public $layout = false;
    /**
     * @var SitemapService
     */
    private $_sitemapService;
    /**
     * @var array
     */
    private $_entityToRoute = [
        Sitemap::ENTITY_JOB => ['/job/view'],
        Sitemap::ENTITY_JOB_TENDER => ['/tender/view'],
        Sitemap::ENTITY_PRODUCT => ['/board/product/view'],
        Sitemap::ENTITY_PRODUCT_TENDER => ['/board/tender/view'],
        Sitemap::ENTITY_PORTFOLIO => ['/account/portfolio/view'],
        Sitemap::ENTITY_SERVICE_TAG => ['/category/tag'],
        Sitemap::ENTITY_SKILL => ['/category/skill'],
        Sitemap::ENTITY_PRODUCT_TAG => ['/board/category/tag'],
//        Sitemap::ENTITY_PAGE => ['/page/static'],
        Sitemap::ENTITY_ARTICLE => ['/article/view'],

        Sitemap::ENTITY_PRO_CATEGORY => ['/category/pros'],
        Sitemap::ENTITY_JOB_CATEGORY => ['/category/jobs'],
        Sitemap::ENTITY_TENDER_CATEGORY => ['/category/tenders'],
        Sitemap::ENTITY_PRODUCT_CATEGORY => ['/board/category/products'],
        Sitemap::ENTITY_PRODUCT_TENDER_CATEGORY => ['/board/category/ptenders'],
        Sitemap::ENTITY_PORTFOLIO_CATEGORY => ['/category/portfolios'],
    ];
    /**
     * @var array
     */
    private $_entitySlugAttribute = [
        Sitemap::ENTITY_JOB => 'alias',
        Sitemap::ENTITY_JOB_TENDER => 'alias',
        Sitemap::ENTITY_PRODUCT => 'alias',
        Sitemap::ENTITY_PRODUCT_TENDER => 'alias',
        Sitemap::ENTITY_PORTFOLIO => 'id',
        Sitemap::ENTITY_SERVICE_TAG => 'reserved_job_tag',
        Sitemap::ENTITY_SKILL => 'reserved_skill',
        Sitemap::ENTITY_PRODUCT_TAG => 'reserved_product_tag',
//        Sitemap::ENTITY_PAGE => 'alias',
        Sitemap::ENTITY_ARTICLE => 'alias',

        Sitemap::ENTITY_PRO_CATEGORY => 'category_1',
        Sitemap::ENTITY_JOB_CATEGORY => 'category_1',
        Sitemap::ENTITY_TENDER_CATEGORY => 'category_1',
        Sitemap::ENTITY_PRODUCT_CATEGORY => 'category_1',
        Sitemap::ENTITY_PRODUCT_TENDER_CATEGORY => 'category_1',
        Sitemap::ENTITY_PORTFOLIO_CATEGORY => 'category_1',
    ];

    /**
     * DefaultController constructor.
     * @param string $id
     * @param Module $module
     * @param SitemapService $sitemapService
     * @param array $config
     */
    public function __construct(string $id, Module $module, SitemapService $sitemapService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_sitemapService = $sitemapService;
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->language = 'ru-RU';
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $output = null;

        foreach ($this->_entityToRoute as $entityKey => $route) {
            $count = $this->_sitemapService->getEntityCount($entityKey);
            $maps = $count / Sitemap::MAX_PER_FILE;
            $iterator = 0;
            while ($iterator < $maps) {
                $output .= $this->renderPartial('sitemap', [
                    'url' => Url::to(["/sitemap/default/part", 'entity' => $entityKey, 'index' => $iterator, 'app_language' => 'ru'], true),
                ]);
                $iterator++;
            }
        }

        return $this->render('sitemap-wrapper', [
            'content' => $output
        ]);
    }

    /**
     * @param $entity
     * @param $index
     * @return string
     */
    public function actionPart($entity, $index = 0)
    {
        $output = '';
        $startOffset = $offset = $index * Sitemap::MAX_PER_FILE;

        switch ($entity) {
            case Sitemap::ENTITY_JOB:
            case Sitemap::ENTITY_JOB_TENDER:
            case Sitemap::ENTITY_PRODUCT:
            case Sitemap::ENTITY_PRODUCT_TENDER:
            case Sitemap::ENTITY_SERVICE_TAG:
            case Sitemap::ENTITY_PRODUCT_TAG:
            case Sitemap::ENTITY_SKILL:
            case Sitemap::ENTITY_PAGE:
            case Sitemap::ENTITY_ARTICLE:
            case Sitemap::ENTITY_PORTFOLIO:

            case Sitemap::ENTITY_JOB_CATEGORY:
            case Sitemap::ENTITY_TENDER_CATEGORY:
            case Sitemap::ENTITY_PRODUCT_CATEGORY:
            case Sitemap::ENTITY_PRODUCT_TENDER_CATEGORY:
            case Sitemap::ENTITY_PORTFOLIO_CATEGORY:
            case Sitemap::ENTITY_PRO_CATEGORY:
                while ($offset < $startOffset + Sitemap::MAX_PER_FILE) {
                    $items = $this->_sitemapService->getItems($entity, Sitemap::BATCH_SIZE, $offset);
                    $offset += Sitemap::BATCH_SIZE;

                    foreach ($items as $item) {
                        $output .= $this->renderMultilangItem($this->_entityToRoute[$entity], $item, $this->_entitySlugAttribute[$entity]);
                    }
                }

                break;
        }

        return $this->render('url-wrapper', [
            'content' => $output
        ]);
    }

    /**
     * @param $route
     * @param $data
     * @param string $slugAttribute
     * @return string
     */
    private function renderMultilangItem($route, $data, $slugAttribute = 'slug')
    {
        $mainUrl = Url::to(array_merge($route, [$slugAttribute => $data['slug'], 'app_language' => 'ru']), true);

        $output = <<<xml
            <url>
                <loc>{$mainUrl}</loc>
                <priority>{$data['priority']}</priority>
xml;
        if ($data['image']) {
            $output .= <<<xml
                <image:image><image:loc>{$data['image']}</image:loc></image:image>
xml;
        }
        if ($data['lastMod']) {
            $output .= <<<xml
                <lastmod>{$data['lastMod']}</lastmod>
xml;
        }
        if ($data['changeFreq']) {
            $output .= <<<xml
                <changefreq>{$data['changeFreq']}</changefreq>
xml;
        }
        if (!empty($data['translations'])) {
            foreach ($data['translations'] as $locale => $translation) {
                if ($locale === 'ru-RU') continue;
                $href = Url::to(array_merge($route, [$slugAttribute => $translation['slug'] ?? $data['slug'], 'app_language' => Yii::$app->params['supportedLocales'][$locale]]), true);
                $loc = Yii::$app->params['supportedLocales'][$locale];
                $loc = $loc === 'ua' ? 'uk' : $loc;
                $output .= <<<xml
                    <xhtml:link rel="alternate" hreflang="{$loc}" href="{$href}" />
xml;
            }
        }
        $output .= <<<xml
        </url>
xml;
        return $output;
    }
}