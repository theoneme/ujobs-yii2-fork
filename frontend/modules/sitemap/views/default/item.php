<?php
/**
 * Created by PhpStorm.
 * User: РџРљ
 * Date: 16.02.2017
 * Time: 12:33
 */
use yii\helpers\Url;

/* @var string $image
 * @var string $url
 * @var string $lastmod
 * @var string $priority
 * @var string $changefreq
 */

?>
<url>
    <loc><?= Url::to(array_merge($route, ['app_language' => 'ru']), true) ?></loc>
    <?php if ($image) { ?>
        <image:image>
            <image:loc><?= $image ?></image:loc>
        </image:image>
    <?php } ?>
    <?php if ($lastmod) { ?>
        <lastmod><?= $lastmod ?></lastmod>
    <?php } ?>
    <?php if ($changefreq) { ?>
        <changefreq><?= $changefreq ?></changefreq>
    <?php } ?>
    <priority><?= $priority ?></priority>
    <?php if ($locales) { ?>
        <?php foreach ($locales as $key => $locale) { ?>
            <?php if ($locale === 'ru') continue; ?>
            <xhtml:link rel="alternate" hreflang="<?= $locale ?>" href="<?= Url::to(array_merge($route, ['app_language' => $locale]), true) ?>" />
        <?php } ?>
    <?php } ?>
</url>