<?php
/**
 * Created by PhpStorm.
 * User: РџРљ
 * Date: 16.02.2017
 * Time: 12:33
 */

use yii\helpers\Url;

/* @var string $image
 * @var string $url
 * @var string $lastmod
 * @var string $priority
 * @var string $changefreq
 * @var array $object
 * @var array $route
 * @var string $slugAttribute
 */

?>
<url>
    <loc><?= Url::to(array_merge($route, [$slugAttribute => $object['translation']['ru-RU']['slug'] ?? $object['alias'], 'app_language' => 'ru']), true) ?></loc>
    <?php if ($image) { ?>
        <image:image>
            <image:loc><?= $image ?></image:loc>
        </image:image>
    <?php } ?>
    <?php if ($lastmod) { ?>
        <lastmod><?= $lastmod ?></lastmod>
    <?php } ?>
    <?php if ($changefreq) { ?>
        <changefreq><?= $changefreq ?></changefreq>
    <?php } ?>
    <priority><?= $priority ?></priority>
    <?php if ($object['translations']) { ?>
        <?php foreach ($object['translations'] as $key => $locale) { ?>
            <?php if ($key === 'ru-RU') continue; ?>
            <xhtml:link rel="alternate" hreflang="<?= Yii::$app->params['supportedLocales'][$key] ?>" href="<?= Url::to(array_merge($route, [$slugAttribute => $locale['slug'] ?? $object['alias'], 'app_language' => Yii::$app->params['supportedLocales'][$key]]), true) ?>" />
        <?php } ?>
    <?php } ?>
</url>