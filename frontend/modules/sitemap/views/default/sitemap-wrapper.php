<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.07.2017
 * Time: 12:43
 */

/* @var mixed $content*/

echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?= $content?>
</sitemapindex>