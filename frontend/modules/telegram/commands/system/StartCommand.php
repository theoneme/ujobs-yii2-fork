<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.05.2017
 * Time: 11:50
 */

namespace frontend\modules\telegram\commands\system;

use frontend\modules\telegram\services\TelegramService;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Start command
 */
class StartCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'start';
    /**
     * @var string
     */
    protected $description = 'Start command';
    /**
     * @var string
     */
    protected $usage = '/start';
    /**
     * @var string
     */
    protected $version = '1';

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();

        $chatId = $message->getChat()->getId();
        $identityHash = $message->getText(true);

        if ($identityHash !== '') {
            TelegramService::linkWithTelegramId($identityHash, $chatId);
            $text = Yii::t('notifications', 'You have subscribed on notifications via {provider}', ['provider' => 'Telegram']);
        } else {
            $text = Yii::t('notifications', 'Greetings! You have attempted to subscribe on notifications via {provider}. Please, do this from {link}', ['link' => Html::a(Url::to(['/account/service/account-settings'], true), Url::to(['/account/service/account-settings'], true))]);
        }

        $data = [
            'chat_id' => $chatId,
            'text' => $text,
        ];
        return Request::sendMessage($data);
    }
}