<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.05.2017
 * Time: 16:49
 */

namespace frontend\modules\telegram\commands\system;

use frontend\modules\telegram\services\TelegramService;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
use Yii;

/**
 * Stop command
 */
class StopCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'stop';
    /**
     * @var string
     */
    protected $description = 'Stop command';
    /**
     * @var string
     */
    protected $usage = '/stop';
    /**
     * @var string
     */
    protected $version = '1';

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();

        $chatId = $message->getChat()->getId();
        $identityHash = $message->getText(true);

        if ($identityHash !== '') {
            TelegramService::unlinkFromTelegram($identityHash, $chatId);
        }

        $text = Yii::t('notifications', 'You have unsubscribed from notifications via {provider}, but it`s never too late to come back :)', ['provider' => 'Telegram']);
        $data = [
            'chat_id' => $chatId,
            'text' => $text,
        ];
        return Request::sendMessage($data);
    }
}