<?php

namespace frontend\modules\telegram\controllers;

use frontend\modules\telegram\services\TelegramFactory;
use frontend\modules\telegram\Telegram;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Exception\TelegramLogException;
use Longman\TelegramBot\TelegramLog;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package frontend\modules\telegram\controllers
 */
class DefaultController extends Controller
{
    public $apiKey = null;
    public $botName = null;

    /**
     * @var Telegram
     */
    private $telegram = null;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id == 'hook') {
            $this->enableCsrfValidation = false;
        }

        if ($this->telegram === null) {
            $this->telegram = TelegramFactory::getInstance();
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        try {
            $this->apiKey = Yii::$app->params['telegramKey'];
            $this->botName = Yii::$app->params['telegramBotName'];
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }
    }

    /**
     *
     */
    public function actionDestroy()
    {
        try {
            return $this->telegram->unsetWebhook();
        } catch (TelegramException $e) {
            Yii::error($e->getMessage());
        }

        return false;
    }

    /**
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws TelegramException
     */
    public function actionInit()
    {
        return $this->telegram->setWebHook(Url::to(['/telegram/default/hook'], true));
    }

    /**
     *
     */
    public function actionHook()
    {
        try {
            TelegramLog::initErrorLog(Yii::getAlias('@runtime') . '/log/' . $this->botName . '_error.log');
            // Enable MySQL
            //$telegram->enableMySql($mysql_credentials);
            // Enable MySQL with table prefix
            //$telegram->enableMySql($mysql_credentials, $bot_username . '_');
            $this->telegram->addCommandsPath(Yii::getAlias('@frontend') . '/modules/telegram/commands');
            // Enable admin user(s)
            //$telegram->enableAdmin(your_telegram_id);
            //$telegram->enableAdmins([your_telegram_id, other_telegram_id]);
            // Set custom Upload and Download path
            //$telegram->setDownloadPath('../Download');
            //$telegram->setUploadPath('../Upload');
            $this->telegram->enableLimiter();
            $this->telegram->handle();
        } catch (TelegramException $e) {
            TelegramLog::error($e);
        } catch (TelegramLogException $e) {
        }
    }
}