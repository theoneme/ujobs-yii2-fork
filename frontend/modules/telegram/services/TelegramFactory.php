<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.11.2017
 * Time: 15:09
 */

namespace frontend\modules\telegram\services;

use Longman\TelegramBot\Telegram;
use Yii;

/**
 * Class TelegramFactory
 * @package frontend\modules\telegram\services
 */
class TelegramFactory
{
    /**
     * @return Telegram
     */
    public static function getInstance()
    {
        $telegramKey = Yii::$app->params['telegramKey'];
        $telegramBotName = Yii::$app->params['telegramBotName'];

        return new Telegram($telegramKey, $telegramBotName);
    }
}