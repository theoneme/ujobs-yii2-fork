<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.05.2017
 * Time: 17:24
 */

namespace frontend\modules\telegram\services;

use common\models\user\User;
use console\jobs\TelegramNotificationJob;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Yii;

/**
 * Class TelegramService
 * @package frontend\modules\telegram\services
 */
class TelegramService
{
    /**
     * @var User
     */
    private $receiver;

    /**
     * @var Telegram
     */
    private $telegram;

    /**
     * TelegramService constructor.
     * @param User $receiver
     */
    public function __construct(User $receiver)
    {
        $this->receiver = $receiver;
        $this->telegram = TelegramFactory::getInstance();
    }

    /**
     * @param array $params
     * @return bool
     */
    public function sendMessage(array $params)
    {
        $subject = array_key_exists('heading', $params) ? "{$params['heading']}: " : '';
        $text = "{$subject}{$params['content']}. " . Yii::t('notifications', 'View by link: {link}', ['link' => $params['link']], $this->receiver->site_language);

        if(Yii::$app->params['notificationQueue'] === true) {
            return Yii::$app->queue->push(new TelegramNotificationJob([
                'chatId' => $this->receiver->telegram_uid,
                'text' => $text
            ]));
        } else {
            $response = Request::sendMessage([
                'chat_id' => $this->receiver->telegram_uid,
                'text' => $text,
            ]);

            return $response->isOk();
        }
    }

    /**
     * @param integer $hash
     * @param int $telegramUid
     * @return bool
     */
    public static function linkWithTelegramId($hash = null, $telegramUid = 0)
    {
        if ($telegramUid !== 0 && $hash !== null) {
            $user = User::findOne($hash);
            if ($user !== null) {
                $user->updateAttributes(['telegram_uid' => $telegramUid]);

                return true;
            }
        }

        return false;
    }

    /**
     * @param integer $hash
     * @param int $telegramUid
     * @return bool
     */
    public static function unlinkFromTelegram($hash = null, $telegramUid = 0)
    {
        if ($telegramUid !== 0 && $hash !== null) {
            $user = User::findOne($hash);
            if ($user !== null) {
                $user->updateAttributes(['telegram_uid' => $telegramUid]);

                return true;
            }
        }

        return false;
    }
}