<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.05.2017
 * Time: 12:44
 */

namespace frontend\modules\telegram;

class Telegram extends \Longman\TelegramBot\Telegram
{
    /**
     * Get an object instance of the passed command
     *
     * @param string $command
     *
     * @return \Longman\TelegramBot\Commands\Command|null
     */
    public function getCommandObject($command)
    {
        $which = ['system'];
        $this->isAdmin() && $which[] = 'Admin';
        $which[] = 'user';

        foreach ($which as $auth) {
            $command_namespace = __NAMESPACE__ . '\\commands\\' . $auth . '\\' . $this->ucfirstUnicode($command) . 'command';
            if (class_exists($command_namespace)) {
                return new $command_namespace($this, $this->update);
            }
        }

        return null;
    }
}
