<?php 
 return [
    'All in' => 'Wszystko w kategorii',
    'Categories with skill "{skill}"' => 'Kategorii z umiejętnością "{skill}"',
    'Categories with specialty "{specialty}"' => 'Kategorie ze specjalnością "{specialty}"',
    'Categories with tag "{tag}"' => 'Kategorii z tagiem "{tag}"',
    'Hide filters' => 'Zwiń filtry',
    'More filters' => 'Jeszcze filtry',
    'Price' => 'Cena',
    'Price ascending' => 'Ceny rosnąco',
    'Price descending' => 'Ceny malejąco',
    'Publish date' => 'Nowość',
    'Rating ascending' => 'Oceny rosnąco',
    'Rating descending' => 'Oceny malejąco',
    'Related categories for request {request}' => 'Podobne kategorie na życzenie {request}',
    'Relevance' => 'Znaczenie',
    'See more' => 'Pokaż więcej',
    'Show all' => 'Pokaż wszystkie',
];