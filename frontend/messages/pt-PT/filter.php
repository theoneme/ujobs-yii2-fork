<?php 
 return [
    'All in' => 'Tudo em categorias',
    'Categories with skill "{skill}"' => 'A categoria com a habilidade "{skill}"',
    'Categories with specialty "{specialty}"' => 'Categoria, com especialidade "{specialty}"',
    'Categories with tag "{tag}"' => 'A categoria com a tag "{tag}"',
    'Hide filters' => 'Minimizar os filtros',
    'More filters' => 'Mais filtros',
    'Price' => 'Preço',
    'Price ascending' => 'Preço crescente',
    'Price descending' => 'Preço desc',
    'Publish date' => 'Novidade',
    'Rating ascending' => 'Classificação por ordem crescente',
    'Rating descending' => 'Classificação por ordem decrescente',
    'Related categories for request {request}' => 'Produtos relacionados a pedido de {request}',
    'Relevance' => 'Relevância',
    'See more' => 'Mostrar mais',
    'Show all' => 'Mostrar todos os',
];