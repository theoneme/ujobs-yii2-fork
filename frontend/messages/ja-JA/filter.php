<?php 
 return [
    'All in' => 'すべてのカテゴリ',
    'Categories with skill "{skill}"' => 'カテゴリとスキル"{skill}"',
    'Categories with specialty "{specialty}"' => 'カテゴリ:"{specialty}"',
    'Categories with tag "{tag}"' => 'カテゴリタグには"{tag}"',
    'Hide filters' => '崩壊フィル',
    'More filters' => 'フィル',
    'Price' => '価格',
    'Price ascending' => '物価上昇',
    'Price descending' => '価格降',
    'Publish date' => '新規性',
    'Rating ascending' => '格付けの昇順',
    'Rating descending' => '格付けの降下',
    'Related categories for request {request}' => '関連項目のご請求{request}',
    'Relevance' => '関連性',
    'See more' => '続きを見る',
    'Show all' => '全て表示',
];