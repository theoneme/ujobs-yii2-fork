<?php 
 return [
    'All in' => 'Alla kategorier',
    'Categories with skill "{skill}"' => 'Kategorier med skicklighet "{skill}"',
    'Categories with specialty "{specialty}"' => 'Kategorier: "{specialty}"',
    'Categories with tag "{tag}"' => 'Kategori tag "{tag}"',
    'Hide filters' => 'Kollaps filter',
    'More filters' => 'Filter',
    'Price' => 'Pris',
    'Price ascending' => 'Pris stigande',
    'Price descending' => 'Pris fallande',
    'Publish date' => 'Nyhet',
    'Rating ascending' => 'Betyg stigande',
    'Rating descending' => 'Fallande betyg',
    'Related categories for request {request}' => 'Relaterade kategorier för begäran {request}',
    'Relevance' => 'Ees',
    'See more' => 'Visa mer',
    'Show all' => 'Visa alla',
];