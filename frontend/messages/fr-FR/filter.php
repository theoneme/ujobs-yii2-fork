<?php 
 return [
    'All in' => 'Tous dans la catégorie de',
    'Categories with skill "{skill}"' => 'La catégorie de compétence "{skill}"',
    'Categories with specialty "{specialty}"' => 'La catégorie de spécialité "{specialty}"',
    'Categories with tag "{tag}"' => 'La catégorie avec le tag "{tag}"',
    'Hide filters' => 'Réduire le filtre',
    'More filters' => 'Encore des filtres',
    'Price' => 'Prix',
    'Price ascending' => 'Prix croissant',
    'Price descending' => 'Prix décroissant',
    'Publish date' => 'Nouveauté',
    'Rating ascending' => 'Classement par ordre croissant',
    'Rating descending' => 'Classement par ordre décroissant',
    'Related categories for request {request}' => 'Le préposé de la catégorie sur demande {request}',
    'Relevance' => 'La pertinence',
    'See more' => 'Afficher plus',
    'Show all' => 'Afficher tout',
];