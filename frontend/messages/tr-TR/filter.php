<?php 
 return [
    'All in' => 'Tüm kategoriler',
    'Categories with skill "{skill}"' => 'Kategoriler beceri "{skill}"',
    'Categories with specialty "{specialty}"' => 'Kategoriye özel "{specialty}"',
    'Categories with tag "{tag}"' => 'Kategori ile etiket "{tag}"',
    'Hide filters' => 'Rulo filtreler',
    'More filters' => 'Başka filtreler',
    'Price' => 'Fiyat',
    'Price ascending' => 'Fiyata göre artan',
    'Price descending' => 'Fiyata göre azalan',
    'Publish date' => 'Yenilik',
    'Rating ascending' => 'Puan artan',
    'Rating descending' => 'Beğenilenler azalan',
    'Related categories for request {request}' => 'İlgili kategoriye talep üzerine {request}',
    'Relevance' => 'Alaka',
    'See more' => 'Daha fazla göster',
    'Show all' => 'Tümünü göster',
];