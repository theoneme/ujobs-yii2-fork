<?php 
 return [
    'Address information' => 'Başvuru adresi',
    'Birthdate' => 'Doğum tarihi',
    'City' => 'Şehir',
    'Claimant address information' => 'Başvuru adresi',
    'Claimant personal information' => 'Başvuranın kişisel bilgileri',
    'Credit' => 'Borç',
    'Credit information' => 'Kredi bilgileri',
    'Creditor' => 'Alacaklı',
    'Defendant address information' => 'Davalının adresi',
    'Defendant personal information' => 'Kişisel verileri davalı',
    'Document city' => 'Şehir belge',
    'Document number' => 'Belge numarası',
    'Document number and serial' => 'Seri ve belge no',
    'Document region' => 'Belgenin',
    'Document serial' => 'Serisi belge',
    'Email' => 'E-posta',
    'Firstname' => 'Adı',
    'Flat' => 'Daire',
    'House' => 'Ev',
    'Housing' => 'Gövde',
    'Lastname' => 'Soyadı',
    'Marriage date' => 'Düğün tarihi',
    'Marriage dates' => 'Düğün tarihi',
    'Middlename' => 'İlk, orta',
    'Personal information' => 'Kişisel bilgiler',
    'Phone' => 'Telefon',
    'Street' => 'Sokak',
    'Year of divorce' => 'Yıl boşanma',
    'Your document is formed. You can download it from the link' => 'Belge oluşturulmuştur. Sen linkten indirebilirsiniz',
];