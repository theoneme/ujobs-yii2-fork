<?php 
 return [
    'All in' => 'सभी श्रेणियों',
    'Categories with skill "{skill}"' => 'श्रेणियाँ कौशल के साथ "{skill}"',
    'Categories with specialty "{specialty}"' => 'श्रेणियाँ: क्षेत्र "{specialty}"',
    'Categories with tag "{tag}"' => 'श्रेणी टैग, "{tag}"',
    'Hide filters' => 'पतन फिल्टर',
    'More filters' => 'फिल्टर',
    'Price' => 'कीमत',
    'Price ascending' => 'कीमत के आरोही',
    'Price descending' => 'मूल्य उतरते',
    'Publish date' => 'नवीनता',
    'Rating ascending' => 'रेटिंग आरोही',
    'Rating descending' => 'रेटिंग उतरते',
    'Related categories for request {request}' => 'संबंधित श्रेणियाँ अनुरोध के लिए {request}',
    'Relevance' => 'प्रासंगिकता',
    'See more' => 'दिखाने के और अधिक',
    'Show all' => 'दिखाएँ सभी',
];