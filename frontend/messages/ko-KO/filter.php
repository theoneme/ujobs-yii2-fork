<?php 
 return [
    'All in' => '모든 카테고리',
    'Categories with skill "{skill}"' => '카테고리와 기술을"{skill}"',
    'Categories with specialty "{specialty}"' => '카테고리:"{specialty}"',
    'Categories with tag "{tag}"' => '카테고리 tag"{tag}"',
    'Hide filters' => '붕괴 필터',
    'More filters' => '필터',
    'Price' => '가격',
    'Price ascending' => '가격 오름차순',
    'Price descending' => '가격 내림차순',
    'Publish date' => '참신',
    'Rating ascending' => '오름차순 평가',
    'Rating descending' => '평가 descending',
    'Related categories for request {request}' => '과 관련된 범주에 대한 요청{request}',
    'Relevance' => '성',
    'See more' => '더 보기',
    'Show all' => '모두 보기',
];