<?php 
 return [
    'All in' => 'Усё ў катэгорыі',
    'Categories with skill "{skill}"' => 'Катэгорыі з навыкам "{skill}"',
    'Categories with specialty "{specialty}"' => 'Катэгорыі са спецыяльнасцю "{specialty}"',
    'Categories with tag "{tag}"' => 'Катэгорыі з тэгам "{tag}"',
    'Hide filters' => 'Згарнуць фільтры',
    'More filters' => 'Яшчэ фільтры',
    'Price' => 'Цана',
    'Price ascending' => 'Цане па ўзрастанні',
    'Price descending' => 'Цане па змяншэнні',
    'Publish date' => 'Навізне',
    'Rating ascending' => 'Рэйтынгу па ўзрастанні',
    'Rating descending' => 'Рэйтынгу па змяншэнні',
    'Related categories for request {request}' => 'Спадарожныя катэгорыі па запыце {request}',
    'Relevance' => 'Рэлевантнасці',
    'See more' => 'Паказаць яшчэ',
    'Show all' => 'Паказаць усе',
];