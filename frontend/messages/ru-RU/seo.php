<?php
return [
    ' in {city}' => ' в г.{city}',
    ' with params {attributes}' => ' с параметрами: {attributes}',
    'Request catalog{request}{tattributes}' => 'Каталог заявок{request}{tattributes}',
    '{count, plural, one{# request} other{# requests}} | Send a response, complete the assignment and earn money | Site of professionals uJobs.me'
    => '{count, plural, one{# заказ} few{# заказа} other{# заказов}} | Отправь заявку, выполни задание и заработай | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# request} other{# requests}} and earn on the site of professionals uJobs.me{request}{tattributes}. {countPro, plural, one{# customer} other{# customers}} waiting for your responses!'
    => 'Выбирайте из {count, plural, one{# заказ} few{# заказа} other{# заказов}} и зарабатывайте на сайте профессионалов uJobs.me{request}{tattributes}. {count, plural, one{# заказчик} few{# заказчика} other{# заказчиков}} ждут ваших откликов!',
    'distant work, part-time at home, freelance, freelance market, jobs market{kequest}{tattributes}' => 'удаленная работа, подработка на дому, фриланс, биржа фриланса, биржа заданий{kequest}{tattributes}',
    'Choose a task and earn money' => 'Выбирайте задание и зарабатывайте',
    'Choose orders from {category} category{city}{request}{tattributes}' => 'Выбери заказы в разделе {category}{city}{request}{tattributes}',
    'Catalog of professionals{request}{tattributes}' => 'Каталог профессионалов{request}{tattributes}',
    '{count, plural, one{# professional} other{# professionals}} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} ждут ваших заданий{Request}{Attributes} | Сайт профессионалов uJobs.me',
    '{count, plural, one{# professional} other{# professionals}} from the various spheres awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} из любых сфер ждут ваших заданий{request}{tattributes}. Заказывайте сейчас на сайте профессионалов uJobs.me!',
    'freelancers services, order freelancer services, freelancers exchange, cheap freelancers{kequest}{kattributes}'
    => 'услуги фрилансеров, заказать услуги фрилансеров, биржа фрилансеров, фрилансеры недорого{kequest}{kattributes}',
    'Choose a specialist and assign him for a job' => 'Подбери себе специалиста и поручи ему работу',
    '{category} specialists{city}{request}{tattributes}' => 'Специалисты по {category}{city}{request}{tattributes}',
    '{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} в сфере «{category}»{city} ждут ваших заданий{Request}{Attributes} | Сайт профессионалов uJobs.me',
    '{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} в сфере «{category}»{city} ждут ваших заданий{request}{tattributes}. Заказывайте сейчас на сайте профессионалов uJobs.me!',
    '{category}{city}, order {category}{city}, {category}{city} cheap, {category} freelancer{city}{kequest}{kattributes}'
    => '{category}{city}, заказать {category}{city}, {category}{city} недорого, {category} фрилансер{city}{kequest}{kattributes}',
    'Choose from {count, plural, one{# professional} other{# professionals}} in {category}{city}'
    => 'Выбирайте из {count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} в сфере {category}{city}',
    'Are you looking for {category} to order?' => 'Ищете где заказать {category}?',
    '{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks.'
    => '{count, plural, one{# профессонал} few{# профессонала} other{# профессоналов}} по «{category}»{city} ждут ваших заданий.',
    'On our website you can choose a specialist in «{category}»{city} according to your requirements and with reasonable price.'
    => 'На нашем сайте вы можете выбрать специалиста по «{category}»{city} по вашим требованиям и приемлемой цене.',
    'Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.'
    => 'Оформите заказ в несколько простых шагов, сообщите нужные детали, и выбранный исполнитель возьмется за дело.',
    'Professional will receive a payment only after he completes the job and you approve it.'
    => 'Исполнитель получит оплату только после того, как выполнит работу, а вы примете её.',
    '{count, plural, one{# professional} other{# professionals}} of other services in «{parentCategory}»{city} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} других услуг в сфере «{parentCategory}»{city} ждут ваших заданий{Request} | Сайт профессионалов uJobs.me',
    '{count, plural, one{# professional} other{# professionals}} of other services in «{parentCategory}»{city} awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} других услуг в сфере «{parentCategory}»{city} ждут ваших заданий{request}{tattributes}. Заказывайте сейчас на сайте профессионалов uJobs.me!',
    'other services in {parentCategory}{city}{kequest}{kattributes}' => 'другие услуги в сфере {parentCategory}{city}{kequest}{kattributes}',
    'Order other services in {parentCategory}{city} from {count, plural, one{# freelancer} other{# freelancers}}'
    => 'Закажите другие услуги в сфере {parentCategory}{city} от {count, plural, one{# фрилансер} few{# фрилансера} other{# фрилансеров}}',
    'Are you looking for other services in {parentCategory}{city}?'
    => 'Ищете другие услуги в сфере {parentCategory}{city}?',
    '{count, plural, one{# professional} other{# professionals}}{city} awaiting your response.'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}}{city} ждут ваших заявок.',
    'On our website you can choose a specialist according to your requirements and with reasonable price.'
    => 'На нашем сайте вы можете выбрать специалиста по вашим требованиям и приемлемой цене.',
    '{title} | Skills: {skills} | {count, plural, one{# service} other{# services}} | The site of professionals uJobs.me'
    => '{title} | Навыки: {skills} | {count, plural, one{# услуга} few{# услуги} other{# услуг}} | Сайт профессионалов uJobs.me',
    'Send the order to {title}. Skills: {skills}. {count, plural, one{# service} other{# services}}. More information on the site of professionals uJobs.me.'
    => 'Отправьте заказ пользователю {title}. Навыки: {skills}. {count, plural, one{# услуга} few{# услуги} other{# услуг}}. Подробнее на сайте профессионалов uJobs.me.',
    'Work in {category}{city} | {count, plural, one{# request} other{# requests}} in category «{category}»{city} | Site of professionals uJobs.me'
    => 'Работа в сфере {category}{city} | {count, plural, one{# заказ} few{# заказа} other{# заказов}} в категории «{category}»{city} | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# request} other{# requests}} in category «{category}»{city}{request}{tattributes}. Earn from {minPrice} on the site of professionals uJobs.me.'
    => 'Выберите из {count, plural, one{# заказ} few{# заказа} other{# заказов}} категории «{category}»{city}{request}{tattributes}. Заработайте от {minPrice} на сайте профессионалов uJobs.me.',
    'work {category}{city}, vacancy {category}{city}, freelance {category}{city}{kequest}{kattributes}'
    => 'работа {category}{city}, вакансии {category}{city}, фриланс {category}{city}{kequest}{kattributes}',
    '{count, plural, one{# request} other{# requests}} in «{category}»{city}'
    => '{count, plural, one{# заказ} few{# заказа} other{# заказов}} в сфере «{category}»{city}',
    'Are you looking for a job in {category}{city}?' => 'Ищете работу в сфере {category}{city}?',
    '{countPro, plural, one{# customer} other{# customer}} ready to give you a task in {category}'
    => '{countPro, plural, one{# заказчик} few{# заказчика} other{# заказчиков}} готовы дать вам задание по {category}',
    'You can find a job in category “{category}” on our site according to your skills and abilities'
    => 'На нашем сайте вы можете найти работу в категории “{category}” по вашим навыкам',
    'Respond to request and get task in “{category}”.'
    => 'Откликнитесь на заказ и получите задание по “{category}”.',
    'Complete the job and when the customer approves it receive a payment.'
    => 'Выполните работу и, как только заказчик примет её, вы получите оплату.',
    'Order «{title}» {price} | Site of professionals uJobs.me'
    => 'Заказ «{title}» {price} | Сайт профессионалов uJobs.me',
    '«{title}» service order {price} on the site of professionals uJobs.me. Do the task and earn money!'
        => 'Заказ услуги «{title}» {price} на сайте профессионалов uJobs.me. Выполни задание и заработай!',
    '{title}, order {title}' => '{title}, заказ {title}',
    'Service catalog{request}{tattributes}' => 'Каталог услуг{request}{tattributes}',
    '{count, plural, one{# service} other{# services}} from {countPro, plural, one{# professional} other{# professionals}} | Choose and order now | Site of professionals uJobs.me'
    => '{count, plural, one{# услуга} few{# услуги} other{# услуг}} от {countPro, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} | Выбирайте и заказывайте сейчас | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# service} other{# services}} from {countPro, plural, one{# professional} other{# professionals}} on the site of professionals uJobs.me{request}{tattributes}.'
    => 'Выбирайте из {count, plural, one{# услуга} few{# услуги} other{# услуг}} от {countPro, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} на сайте профессионалов uJobs.me{request}{tattributes}.',
    'Thousands of Professionals ready to fulfill your request(s)' => 'Тысячи профессионалов в своём деле готовы выполнить ваши задания',
    '{category}{city} | {count, plural, one{# service} other{# services}} category «{category}»{city} | Site of professionals uJobs.me'
    => 'Нужны услуги по {category}{city}? | Выбери среди {count, plural, one{# услуги} other{# услуг}} в разделе «{category}» | Заказать услуги в категории «{category}»{city} по цене от {minPrice} | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# service} other{# services}} in category «{category}»{city} at the price from {minPrice} on the site of professionals uJobs.me{request}{tattributes}.'
    => 'Выбирайте из {count, plural, one{# услуга} few{# услуги} other{# услуг}} категории «{category}»{city} по цене от {minPrice} на сайте профессионалов uJobs.me{request}{tattributes}.',
    'order {category}{city}, {category} services{city}, freelancer {category}{city}, order {category} from freelancer{city}, order {category}{city} cheap{kequest}{kattributes}'
    => 'заказать {category}{city}, услуги {category}{city}, фрилансер {category}{city}, заказать {category} у фрилансера{city}, заказать {category}{city} недорого{kequest}{kattributes}',
    'order services{city}, freelancer services{city}, order services{city} cheap{kequest}{kattributes}'
    => 'заказать услуги{city}, услуги фрилансеров{city}, заказать услуги{city} недорого{kequest}{kattributes}',
    'Order {category}{city}{request}{tattributes}' => 'Заказать {category}{city}{request}{tattributes}',
    '{countPro, plural, one{# professional} other{# professionals}} in {category}{city} waiting for your orders.'
    => '{countPro, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} по {category}{city} ждут ваших заявок.',
    'Other services from «{parentCategory}»{city} | Choose from {count, plural, one{# service} other{# services}} | Site of professionals uJobs.me'
    => 'Другие услуги раздела «{parentCategory}»{city} | Выбирайте из {count, plural, one{# услуги} other{# услуг}} | Сайт профессионалов uJobs.me',
    'Choose from {count} other {count, plural, one{service} other{services}} in «{parentCategory}»{city} at the price from {minPrice} on the site of professionals uJobs.me{request}{tattributes}.'
    => 'Выбирайте из {count} других {count, plural, one{# услуги} other{# услуг}} категории «{parentCategory}»{city} по цене от {minPrice} на сайте профессионалов uJobs.me{request}{tattributes}.',
    'Order other services in {parentCategory} from {countPro, plural, one{# freelancer} other{# freelancers}}{city}'
    => 'Закажите другие услуги по {parentCategory} от {countPro, plural, one{# фрилансера} other{# фрилансеров}}',
    'Are you looking for {parentCategory} to order?' => 'Ищете, где заказать другие услуги по {parentCategory}?',
    '{countPro, plural, one{# professional} other{# professionals}} awaiting your response.'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} ждут ваших заявок.',
    'Submit an order in a few simple steps and describe the details, so the chosen professional would get down to business.'
    => 'Оформите заказ в несколько простых шагов, сообщите нужные детали, и выбранный исполнитель возьмется за дело.',
    'Order «{title}» at price {price} | Site of professionals uJobs.me' => 'Заказать «{title}» по цене {price} | Сайт профессионалов uJobs.me',
    'Order the service «{title}» at the price {price} on the site of professionals uJobs.me. Professional {username} waiting for your applications!'
    => 'Закажите услугу «{title}» по цене {price} на сайте профессионалов uJobs.me. Исполнитель {username} ждет ваших заявок!',
    '{title}, service {title}, order {title}' => '{title}, услуга {title}, заказать {title}',
    'Training video catalog about {category} | Choose and order now | Site of professionals uJobs.me' => 'Обучающие видео по {category} | Выбери и закажи сейчас | Сайт профессионалов uJobs.me',
    'Learning videos | Choose and order now | Site of professionals uJobs.me' => 'Обучающее видео | Выбери и закажи сейчас | Сайт профессионалов uJobs.me',
    'Choose learning video on the site of professionals uJobs.me' => 'Выбери обучающее видео на сайте профессионалов uJobs.me',
    'Video catalog' => 'Каталог видео',
    '{title} specialist{city}' => '{title} специалист{city}',
    '{count, plural, one{# professional} other{# professionals}} in «{title}»{city} awaiting your tasks | Site of professionals uJobs.me'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} в сфере «{title}»{city} ждут ваших заданий | Сайт профессионалов uJobs.me',
    '{count, plural, one{# professional} other{# professionals}} in «{title}»{city} awaiting your tasks. Order now on the site of professionals uJobs.me!'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} в сфере «{title}»{city} ждут ваших заданий. Заказывайте сейчас на сайте профессионалов uJobs.me!',
    '{title}{city}, order {title}{city}, {title} cheap, {title} freelancer{city};'
    => '{title}{city}, заказать {title}{city}, {title}{city} недорого, {title} фрилансер{city}',
    'Are you looking for {title} to order{city}?' => 'Ищете, где заказать {title}{city}?',
    '{count, plural, one{# professional} other{# professionals}} in {title}{city} awaiting your response.'
    => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}} по {title}{city} ждут ваших заявок.',
    'On our website you can choose a specialist in {title} according to your requirements and with reasonable price.'
    => 'На нашем сайте вы можете выбрать специалиста по {title} по вашим требованиям и приемлемой цене.',
    '{title}{city} | {count, plural, one{# service} other{# services}} in «{title}»{city} | Site of professionals uJobs.me'
    => 'Заказать {title}{city} | {count, plural, one{# услуга} few{# услуги} other{# услуг}} по «{title}»{city} по цене от {minPrice} | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# service} other{# services}} in «{title}» category at the price from {minPrice} on the site of professionals uJobs.me.'
    => 'Выбирайте из {count, plural, one{# услуга} few{# услуги} other{# услуг}} категории «{title}» по цене от {minPrice} на сайте профессионалов uJobs.me.',
    'order {title}{city}, {title} services{city}, freelancer {title}, order {title} from freelancer{city}'
    => 'заказать {title}{city}, услуги {title}{city}, фрилансер {title}{city}, заказать {title} у фрилансера{city}',
    '{count, plural, one{# service} other{# services}} in {title}{city} awaiting your response.'
    => '{count, plural, one{# услуга} few{# услуги} other{# услуг}} по {title}{city} ждут ваших заявок.',
    'Are you looking for a job such as {title}{city}?' => 'Ищете работу в сфере {title}{city}?',
    '{title} is required | Need {count} {title}{city} salary up to {maxPrice} | Site of professionals uJobs.me'
    => 'Требуется {title} | Нужно {count} {title}{city} зарплата до {maxPrice} | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# vacancy} other{# vacancy}} in «{title}» specialty{city} and earn to {maxPrice} on the site of professionals uJobs.me.'
    => 'Выберите из {count, plural, one{# вакансии} other{# вакансий}} в специальности «{title}»{city} и заработайте до {maxPrice} на сайте профессионалов uJobs.me.',
    'job {title}{city}, vacancies {title}{city}, {title} is required, need {title}{city}'
    => 'работа {title}{city}, вакансии {title}{city}, требуется {title}{city}, нужен {title}{city}',
    'Our Freelance marketplace offers: remote work, flexible hours and the guarantee of timely payment'
    => '"Наша биржа предлагает: работать удаленно, иметь свободный график, гарантию своевременной оплаты "',
    '{count, plural, one{# customer} other{# customers}}{city} ready for give you a task {title}'
    => '{count, plural, one{# заказчик} one{# заказчика} other{# заказчиков}}{city} готовы дать вам задание по {title}',
    'Choose a job from submitted vacancies in {title} choose suitable to you.'
    => 'Выбери работу из представленных вакансий по {title} выберите наиболее подходящее вам.',
    'Respond to request and tell why do the customer must choose you.'
    => 'Откликнитесь на заказ и скажете почему заказчик должны выбрать именно вас',
    'Are you looking for a {title} professional{city}?' => 'Ищете исполнителя по специальности {title}{city}?',
    'Professionals with {title} specialty{city} | Site of professionals uJobs.me'
    => 'Исполнители со специальностью {title}{city} | Сайт профессионалов uJobs.me',
    'Choose from {count, plural, one{# professional} other{# professionals}} «{title}» specialty{city}.'
    => 'Выбирайте из {count, plural, one{# исполнителя} other{# исполнителей}} по специальности «{title}»{city}.',
    'worker {title}{city}, {title} is required, need {title}{city}'
    => 'работник {title}{city}, требуется {title}, нужен {title}{city}',
    'Our Freelance marketplace offers: search for remote workers and ensure timely performance of work'
    => 'Наша биржа фрилансеров предлагает: поиск удаленных работников и гарантию своевременного выполнения работы',
    'Order {title}{city}' => 'Заказать {title}{city}',
    'How to order “{keyword}”{city} on services marketplace uJobs' => 'Как заказать “{keyword}”{city} на бирже услуг uJobs',
    'With our marketplace you can inexpensively order “{keyword}” {city}' => 'С нашей биржей вы сможете недорого заказать услуги по “{keyword}”{city}',
    "Board of private announcements{city}{request}{tattributes}" => "Биржа услуг и товаров{city}{request}{tattributes}",
    "It is easy to buy or sell products{city} from individuals through secure transaction" => "Легко купить или продать товары от частных лиц{city} через безопасную сделку",
    "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} about selling products on marketplace{city} | Easy to buy or sell product on uJobs.me"
    => "{count, plural, =0{частные объявления} one{# частное объявление} few{# частных объявления} other{# частных объявлений}} о продаже товаров на бирже{city} | Легко купить или продать товар на сайте uJobs.me",
    "Private announcements from {countSellers, plural, =0{sellers} one{# seller} other{# sellers}} on free board{city}{request}{tattributes}. Choose new or used product among {count, plural, =0{announcements} one{# announcement} other{# announcements}} cheap on uJobs.me."
    => "Частные объявления от {countSellers, plural, =0{продавцов} one{# продавца} few{# продавцов} other{# продавцов}} на бирже услуг и товаров{city}{request}{tattributes}. Выбрать новый или Б/у товар среди {count, plural, =0{предложений} one{# предложения} other{# предложений}} дешево на uJobs.me.",
    "Private announcements{city}, marketplace of services and goods{city}, free marketplace{city}{kequest}{kattributes}"
    => "Частные объявления{city}, биржа услуг и товаров{city}, бесплатная биржа услуг и товаров{city}{kequest}{kattributes}",
    "How does the marketplace work{city}?" => "Как работает биржа услуг и товаров{city}?",
    "We created a secure transaction to protect your interests."
    => "Мы создали безопасную сделку для защиты ваших интересов.",
    "Choose product and add it to cart"
    => "Выбери подходящий товар и добавь его в корзину.",
    "Pay deposit equal to cost of the product."
    => "Внеси на счет депозит в размере стоимости товара.",
    "Seller receives money right after customer receives product."
    => "Продавец получит деньги сразу после получения товара покупателем.",
    'Buy {category}{city}{request}{tattributes}' => 'Купить {category}{city}{request}{tattributes}',
    'Buy {category}{attributes}{city} through secure transactions' => 'Покупайте товары в {category}{attributes}{city} через безопасную сделку',
    'Buying {category}{attributes}{city} is easy through secure transaction | Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} about selling «{category}»{attributes}{city} on uJobs.me'
    => 'Купить {category} {attributes}{city} легко через безопасную сделку | Выбирайте среди {count, plural, =0{объявления} one{# объявление} few{# объявления} other{# объявления}} о продаже «{category}»{attributes}{city} на uJobs.me',
    'buy {category}{attributes}{city}, sell {category}{attributes}{city}, buy {category}{attributes}{city} cheap{kequest}{kattributes}' => 'купить {category}{attributes}{city}, продать {category}{attributes}{city}, купить {category}{attributes}{city} недорого{kequest}{kattributes}',
    'Looking for a place to buy {category}{attributes}{city} through a secure transaction?' => 'Ищете где купить {category}{attributes}{city} через безопасную сделку?',
    'Choose product in {category}{attributes}{city} and add it to cart' => 'Выбирайте продукт в {category}{attributes}{city} и добавляйте его в корзину',
    'A secure transaction guarantees a 100% refund if you are not satisfied with the product' => 'Безопасная сделка гарантирует 100% возврат денег, если вы не удовлетворены продуктом',
    '{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} buy or sell in category «{category}»{attributes}{city} at the price from {minPrice} on uJobs.me{request}{tattributes}.'
    => '{count, plural, =0{частные объявления} one{# частное объявление} few{# частных объявления} other{# частных объявлений}} покупайте или продавайте в категории «{category}»{attributes}{city} по цене от {minPrice} на uJobs.me{request}{tattributes}.',
    'Ordering "{keyword}" is more advantageous through the marketplace than directly from the performer, for several reasons.'
    => 'Заказывать услуги "{keyword}" выгоднее через биржу, чем напрямую у исполнителя, по нескольким причинам.',
    'The main ones are:' => 'Основные из них:',
    'You pay only for the result, your money is protected when ordering the service. You do not risk losing money if the professional does not perform the job, or does it poorly;'
    => 'Вы платите только за результат, ваши деньги защищены при заказе услуги. Вы не рискуете потерять деньги если Исполнитель не выполнит работу, либо выполнит её некачественно;',
    'A large number of performers in one place allows you to find the right one for your job.'
    => 'Большое количество исполнителей в одном месте позволяет гарантированно найти подходящего для вашего задания.',
    'The process of ordering the service is as follows:' => 'Процесс заказа услуги выглядит следующим образом:',
    'You find a suitable offer "{keyword}" by price, rating, terms of service;'
    => 'Вы находите подходящее предложение "{keyword}" по цене, рейтингу, условиям оказания услуги;',
    'Add the cost of "{keyword}" in the form of a deposit to your account;' => 'Внесите стоимость "{keyword}" в виде депозита на свой счет;',
    'Give the Professional all the details for the task;' => 'Передаете Исполнителю все детали для выполнения задания;',
    'The Professional proceeds to the task. At the same time, he is obliged to make "{keyword}" in the specified period;'
    => 'Исполнитель приступает к выполнению задания. При этом, он обязан сделать "{keyword}" в указанный срок;',
    'Upon completion of the assignment, the Professional notifies you of the implementation and provides the necessary confirmation;'
    => 'По выполнению задания Исполнитель оповещает вас о выполнении и предоставляет необходимое подтверждение;',
    'You check the work. And if everything suits you, take it. Otherwise - send it for revision.'
    => 'Вы проверяете работу. И если вас все устраивает, принимаете её. Иначе - отправляете на доработку.',
    'After the approval of the work by the client, the professional will receive the payment.'
    => 'После приема работы Исполнитель получит деньги с вашего депозита на свой счет.',
    'We created this service to help customers find the best performers' => 'Мы создали данный сервис чтобы помочь заказчикам находить исполнителей',
    'In this section of site you can buy products from categories {randomChildrenCategories} from people and companies with prices starting at {minPrice}'
    => 'В данном разделе сайта вы можете купить товары из категорий {randomChildrenCategories} у частных лиц и предприятий по ценам, начинающимся с {minPrice}',
    'Most of the products are being sold by secure transaction.'
    => 'Большинство товаров продаются через безопасную сделку.',
    'With it it`s guaranteed that you will receive bought product or we will refund your money. If you want to choose products that are available through secure transaction you can use appropriate filter.'
    => 'С ней вы гарантированно получите купленный товар, либо мы вернем Ваши деньги. Если вы хотите выбрать товары, которые доступны только через безопасную сделку, вы можете использовать соответствующий фильтр.',
    'Also you can buy products from other cities, if shipping is available for them.'
    => 'Также вы можете покупать товары из других городов, если для товара возможна доставка.',
    'If you have not found product, that you need, you can post request. So sellers from the entire world will see your request and you will get huge amount of offers about selling this product.'
    => 'Если вы не нашли товар, который вам нужен, вы можете разместить заявку. Тогда продавцы со всего мира увидят ее и вы получите большое количество предложений о продаже нужного товара.',
    'Enjoy the shopping'
    => 'Удачных покупок',
    'Sincerely, uJobs Team'
    => 'С уважением, команда uJobs',
    'Board of private announcements{city}.' => 'Биржа услуг и товаров{city}.',
    'We provide secure transaction for selling products for individuals.'
    => 'Мы предоставляем безопасную сделку частным лицам для продажи товаров.',
    'In category {category}{attributes}{city} are {count} products that you can buy from individuals and companies. There are new and used products with prices starting at {minPrice}.'
    => 'В категории {category}{attributes}{city} {count} товаров, которые вы можете купить у частных лиц и компаний. Представлены как новые, так и Б/у товары с ценами, начинающимся с {minPrice}.',
    'For safety of your money we implemented secure transaction, with it it`s guaranteed that you will receive bought product or we will refund your money. If you want to choose products that are available through secure transaction you can use appropriate filter.'
    => 'Для сохранности ваших денег мы ввели безопасную сделку, с ней вы гарантированно получите купленный товар, или мы вернем деньги. Если вы хотите выбрать товары, которые доступны через безопасную сделку, воспользуйтесь соответствующим фильтром.',
    'You can choose products, that are present not only in your city, but in other cities if shipping is available for product. So you can Buy {category} from other cities.'
    => 'Вы можете выбрать товары, которые доступные не только в вашем городе, но и в других городах, если для них возможна доставка. Таким образом, вы можете покупать товары в категории {category} из других городов.',
    'If you have not found required product in {category}{attributes}{city}, you can post request, so sellers from the entire world can see what product for what price you want to buy. Posting request is absolutely for free.'
    => 'Если вы не нашли необходимый товар в категории {category}{attributes}{city}, вы можете разместить заявку, таким образом продавцы со всего мира увидят какой товар по какой цене вы хотите купить. Размещение заявки абсолютно бесплатно.',
    'In section {category}{attributes} are {count} products that you can buy from individuals and companies{city}. There are new and used products with prices starting at {minPrice}'
    => 'В разделе {category}{attributes} {count} товаров, которые вы можете купить у частных лиц и компаний {city}. Представлены как новые, так и б/у товары по ценам, начинающимся с {minPrice}',
    'You can choose offer that you are interested in by price and quality in your city, or in other city with shipping.'
    => 'Вы можете выбрать предложение, которое вас заинтересует по цене и качеству в вашем городе, или в другом городе если возможна доставка.',
    'We offer you to use secure transaction for money safety, so seller receives money right after you receive product and confirm that it corresponds to the specifications.'
    => 'Мы предлагаем вам воспользоваться безопасной сделкой для сохранности ваших денег, таким образом продавец получит деньги только после того, как вы получите товар и подтвердите его соответствие заявленным характеристикам.',
    'Also you can post request for product you are want to buy, so thousands of sellers from the entire world will see your request and will make offers to you. So you can choose the best offer.'
    => 'Также вы можете разместить заявку на товар, который вы хотите купить, таким образом тысячи продавцов со всего мира ее увидят и пришлют свои предложения. Вы сможете выбрать лучшее из них.',
    'Buy and sell products in {category}{attributes}{city}.'
    => 'Покупайте и продавайте товары в категории {category}{attributes}{city}.',
    'We present private announcements about selling products in {category}{attributes}{city} via secure transaction.'
    => 'Мы представляем частные объявление о продаже товаров в категории {category}{attributes}{city} через безопасную сделку.',
    'We present private announcements about selling products in {category}{attributes}{city}.'
    => 'Мы представляем частные объявление о продаже товаров в категории {category}{attributes}{city}.',
    'Buy {title} {price} on uJobs.me site'
    => 'Купить {title} {price} на сайте uJobs.me',
    'Buy {title}{city}'
        => 'Купить {title}{city}',
    'Secure transaction guarantees 100% money refund, is you are not satisfied by product'
        => 'Безопасная сделка гарантирует 100% возврат денег, если вы не удовлетворены товаром',
    'Buying {title}{attributes}{city} is easy through secure transaction. Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} on uJobs.me'
        => 'Купить {title}{attributes}{city} легко через безопасную сделку. Выбирайте среди {count, plural, =0{объявлений} one{# объявления} other{# объявлений}} на uJobs.me',
    '{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} buy and sell {title}{attributes}{city} by price {minPrice} from individuals and companies on uJobs.me.'
        => '{count, plural, =0{Частные объявления} one{# частное объявление} other{# частных объявлений}} купить и продать {title}{attributes}{city} по цене {minPrice} от продавцов и компаний на uJobs.me.',
    'buy {title}{attributes}{city}, sell {title}{attributes}{city}, buy {title}{attributes}{city} cheap'
        => 'купить {title}{attributes}{city}, продать {title}{attributes}{city}, купить {title}{attributes}{city} дешево',
    'How to buy {title}{attributes}{city} through a secure transaction?'
        => 'Как купить {title}{attributes}{city} через безопасную сделку?',
    'Choose product in {title}{attributes}{city} and add it to cart'
        => 'Выберите товар в {title}{attributes}{city} и добавьте его в корзину',
    'Buy and sell products in {title}{attributes}{city}.'
        => 'Купить и продать товары в {title}{attributes}{city}.',
    'We present private announcements about selling products in {title}{attributes}{city}.'
        => 'Мы представляем частные объявления о продаже товаров в {title}{attributes}{city}.',
    'On this page you can see products with filter {title}{attributes}{city}, which you can buy from individuals and companies.'
        => 'На этой странице представлены товары с фильтром {title}{attributes}{city}, которые вы можете купить от частных лиц и предприятий',
    'On this page you can see products with filter {title}{city}, which you can buy from individuals and companies.'
    => 'На этой странице представлены товары с фильтром {title}{city}, которые вы можете купить от частных лиц и предприятий',
    'For protection of your interests, we implemented secure transaction, so seller will receive money just after customer receives product.'
        => 'Для защиты ваших интересов, мы ввели безопасную сделку, благодаря которой продавец получит деньги только после того, как покупатель получит товар и убедится в его соответствии описанию.',
    'Also some products can be sold with shipping. Agree shipping details with seller before ordering.'
        => 'Также часть товаров продается с возможностью доставки. Перед заказом обязательно уточняйте условия доставки с продавцом.',
    'If you have not found product, that you need, you can post request and specify what product are you ready to buy and for what price. So thousands of sellers from around the world may see your request for buying {title}{attributes}{city} and make offers.'
        => 'Если вы не нашли товар необходимого качества по необходимой цене, вы можете разместить заявку и указать какой товар вы готовый купить и за сколько. Таким образом тысячи продавцов со всего мира смогут увидеть вашу заявку на покупку {title}{attributes}{city} и выставить вам свои предложения.',
    'We provide a free bulletin board{attributes}{city}. On this board there are products from individuals and organizations. You can post your advertisement about selling or buying products for free.'
        => 'Мы предоставляем бирже услуг и товаров {attributes}{city}. На данной бирже представлены товары от частных лиц и организаций. Вы можете сами бесплатно разместить ваше объявление о покупке или продаже товаров и услуг.',
    'Are you looking for a way to make money?' => 'Вы ищете как заработать денег?',
    'We created a section of requests for services and products.' => 'Мы создали раздел заявок на услуги и товары.',
    'Agree with the buyer how he will pay, we advise you to look at secure transaction.' => 'Согласуй с покупателем как он будет оплачивать, советуем посмотреть безопасную сделку.',
    'Choose which request suits you. Offer your work or product, agree on the price.' => 'Выбери какие заявки тебе подходят. Предложи свою работу или товар, согласуй цену.',
    'If you choose to work through secure transaction, then we 100% guarantee that you will receive payment.'
        => 'Если вы выбрали работу через безопасную сделку, то мы 100% гарантируем что вы получите оплату.',
    'Albums of our users'
        => 'Альбомы наших пользователей',
    'Albums of our users{request}{tattributes}'
        => 'Альбомы наших пользователей{request}{tattributes}',
    '{count, plural, =0{albums} one{# album} other{# albums}} from users are waiting for your ratings'
        => '{count, plural, =0{Альбомы} one{# альбом} few{# альбома} other{# альбомов}} пользователей ожидают ваших оценок',
    'You may create your portfolio album for free, or may see photos and examples of works from uJobs users{Request}{Attributes}'
        => 'Вы можете бесплатно создать свой альбом портфолио, или просмотреть фотографии и примеры работ от пользователей uJobs{Request}{Attributes}',
    'Here are {count, plural, =0{albums} one{# album} other{# albums}} with examples of works and photos from {countPro, plural, =0{authors} one{# author} other{# authors}} on uJobs.me{request}{tattributes}'
        => 'В каталоге {count, plural, =0{альбомы} one{# альбом} few{# альбома} other{# альбомов}} с примерами работ и фотографиями от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} на uJobs.me{request}{tattributes}',
    'Create portfolio{kequest}{kattributes}'
        => 'Создать портфолио{kequest}{kattributes}',
    'If you are not registered - do it now! If you are already registered - please log in.'
        => 'Если вы не зарегистрированы - сделайте это сейчас! Если вы уже зарегистрированы - авторизуйтесь.',
    'Use this <a href=\'/account/portfolio/create\'>link</a> and follow all steps of the form.'
        => 'Перейдите по этой <a href=\'/account/portfolio/create\'>ссылке</a> и пройдите все шаги формы.',
    'The more photos and likes your album has - the higher position it takes in catalog. Share your album with your friends!'
        => 'Чем больше фотографий и оценок у вашего альбоме - тем выше его позиция в каталоге. Делитесь вашими альбомами с вашими друзьями!',
    'We bring to your attention photos and examples of works from our users'
        => 'Мы представляем вашему внимания фотографии и примеры работ от наших пользователей',
    'Portfolio albums in category {category}'
        => 'Альбомы в категории {category}',
    'Portfolio albums in category {category}{request}{tattributes}'
        => 'Альбомы в категории {category}{request}{tattributes}',
    '{count, plural, =0{albums} one{# album} other{# albums}} from {countPro, plural, =0{users} one{# user} other{# users}} in category {category}'
        => '{count, plural, =0{Альбомы} one{# альбом} few{# альбома} other{# альбомов}} от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} в категории {category}',
    'View examples of works and photos in category {category} on uJobs.me{Request}{Attributes}'
        => 'Просматривайте примеры работ и фотографии в категории {category} на uJobs.me{Request}{Attributes}',
    '{category} photos and examples of works from {countPro, plural, =0{users} one{# user} other{# users}} on uJobs.me{request}{tattributes}'
        => '{category} фотографии и примеры работ от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} на uJobs.me{request}{tattributes}',
    '{category} photos{kequest}{kattributes}'
        => '{category} фотографии{kequest}{kattributes}',
    'Portfolio albums from users in category {category}'
        => 'Альбомы от пользователей в категории {category}',
    'Here are presented albums in category {category}'
        => 'Здесь представлены альбомы в категории {category}',
    'Portfolio albums with tag {title}'
        => 'Альбомы с тегом {title}',
    '{count, plural, =0{albums} one{# album} other{# albums}} with tag {title}'
        => '{count, plural, =0{Альбомы} one{# альбом} few{# альбома} other{# альбомов}} с тегом {title}',
    'View examples of works and photos with tag {title} on uJobs.me'
        => 'Просматривайте примеры работ и фотографии с тегом {title} на uJobs.me',
    '{title} photos and examples of works from {countPro, plural, =0{users} one{# user} other{# users}} on uJobs.me'
        => '{title} фотографии и примеры работ от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} на uJobs.me',
    '{title} photos'
        => '{title} фотографии',
    'Portfolio albums from users with tag {title}'
        => 'Альбомы от пользователей с тегом {title}',
    'Here are presented albums with tag {title}'
        => 'Здесь представлены альбомы с тегом {title}',
    'Here are photos and examples of works from our users. We hope that this will help you choose the best specialists for your tasks. You can also rate albums and leave comments.'
        => 'Здесь представлены фотографии и примеры работ от наших пользователей. Мы надеемся, что это поможет вам выбирать наилучших специалистов для ваших задач. Вы можете также оценивать альбомы и оставлять комментарии',
    'You can post your portfolio and tell us about the work that you have done.'
        => 'Вы можете разместить свое портфолио и рассказать о работах, которые вы выполняли.',
    'Real work examples have always been the best confirmation of a specialist\'s qualifications.'
        => 'Реальные примеры работ всегда были наилучшим подтверждением квалификации специалиста.',
    'You can post photos of any of your works for free.'
        => 'Вы можете разместить фотографии любых ваших работ абсолютно бесплатно.',
    'Portfolio can be created under the link {link}. Don`t forget to share your portfolio with friends. The more likes your albums have - the higher the positions they occupy in the catalog.'
        => 'Портфолио может быть создано по ссылке {link}. Не забудьте поделиться вашим портфолио с друзьями. Чем больше оценок у ваших альбомов - тем выше позиции они занимают в каталоге.',
    'Users of our portal posted their albums in {title} and awaiting for you to rate them.'
        => 'Пользователи нашего портала разместили свои альбомы в {title} и ожидают ваших оценок.',
    'We hope, that this section will help you to choose the best specialist in {title} for your tasks.'
        => 'Мы надеемся, что этот раздел поможет вам выбрать наилучшего специалиста в {title} для ваших задач',
    'Also you may post your own albums and tell about works that you have done. Real work examples have always been the best confirmation of a specialist\'s qualifications.'
        => 'Вы также можете разместить собственные альбомы и рассказать о работах, которые вы выполняли. Реальные примеры работ всегда были наилучшим подтверждением квалификации работника.',
    'Create portfolio for free'
        => 'Создать портфолио бесплатно',
    'Users of our portal posted their albums in {category} and awaiting for you to rate them.'
    => 'Пользователи нашего портала разместили свои альбомы в {category} и ожидают ваших оценок.',
    'We hope, that this section will help you to choose the best specialist in {category} for your tasks.'
    => 'Мы надеемся, что этот раздел поможет вам выбрать наилучшего специалиста в {category} для ваших задач',
    'Profile page of {title} on site uJobs.me'
        => 'Страница профиля {title} на сайте uJobs.me',
    'I invite you to check services and products i offer on site uJobs.me'
        => 'Приглашаю вас посмотреть товары и услуги которые я предлагаю на uJobs.me',
    'Page of store {store} on site uJobs.me{request}{tattributes}'
        => 'Страница магазина {store} на uJobs.me{request}{tattributes}',
    '{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} about selling products on store page {store}{Request}{Attributes} | Easy to buy or sell product on uJobs.me'
        => '{count, plural, =0{Частные объявления} one{# частное объявление} other{# частные объявления}} о продаже товаров на странице магазина {store}{Request}{Attributes} | Легко покупать и продавать товары на uJobs.me',
    'Private announcements from {store} on site uJobs.me. Choose product among {count, plural, =0{announcements} one{# announcement} other{# announcements}} cheap on uJobs.me{request}{tattributes}.'
        => 'Частные объявление от {store} на uJobs.me. Выбирайте товары среди {count, plural, =0{объявлений} one{# объявления} other{# объявлений}} дешево на uJobs.me{request}{tattributes}.',
    'We provide store page {store}{attributes}{city}.'
        => 'Мы поедставляет страницу магазина {store}{attributes}{city}',
    'In this section of site you can buy products from categories {randomChildrenCategories} in store {store} with prices starting at {minPrice}'
        => 'В данном разделе сайте вы можете купить товары в категориях {randomChildrenCategories} в магазине {store} по ценам, начиная с {minPrice}',
    'With it it`s guaranteed that you will receive bought product or we will refund your money.'
        => 'С ней вы гарантированно получите купленный товар, или возврат денег.',
    'Products in {category} on store page {store}{request}{tattributes}'
        => '{category} в магазине {store}{request}{tattributes}',
    'In category {category}{attributes}{city} are {count} products that you can buy in store {store} with uJobs.me. There are products with prices starting at {minPrice}.'
        => 'В категории {category}{attributes}{city} {count} товаров, которые вы можете купить в магазине {store} через uJobs.me. Цены на товары начинаются с {minPrice}.',
    'Buying {category}{attributes}{city} is easy through secure transaction{Request} | Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} in store {store} about selling «{category}»{attributes}{city} on uJobs.me'
        => 'Покупать {category}{attributes}{city} легко через безопасную сделку{Request} | Выбирайте среди {count, plural, =0{объявлений} one{# объявления} other{# объявлений}} от магазина {store} о продаже «{category}»{attributes}{city} на uJobs.me',
    '{count, plural, one{# request} other{# requests}} | Send an application, complete the assignment and earn money | Site of professionals uJobs.me'
        => '{count, plural, one{# заявка} few{# заявки} other{# заявок}} | Откликнитесь на заявку, выполните поручение и получите деньги | Сайт профессионалов uJobs.me',
    'Search by request {request}' => 'Поиск по запросу {request}',
    'Looking for services related to {request}?' => 'Ищете услуги, связанные с {request}?',
    'Search results by request {request}' => 'Результаты поиска по запросу {request}',
    'On our site you can choose pro by {request}' => 'На нашем сайте вы можете выбрать исполнителя по запросу {request}',
    'Make an order in few simple steps, provide required details, and chosen professional will get to work' => 'Оформите заказ в несколько простых шагов, предоставите требуемые детали, и выбранный профессионал приступит к работе',
    'Worker will get money just after order {request} is completed and accepted by customer' => 'Исполнитель получит деньги сразу после выполнения заказа {order} и его принятия заказчиком',
    'Looking for products related to {request}?' => 'Ищете товары, связанные с {request}?',
    'On our site you can find product {request}' => 'На нашем сайте вы можете найти товар {request}',
    'Make an order in few simple steps' => 'Оформите заказ в несколько простых шагов',
    'Seller will get money just after you receive product {request}' => 'Продавец получит деньги сразу после того, как вы получите товар {request}',
    "Are you looking for job to order?" => 'Ищете где заказать услугу?',
    "{count, plural, one{# professional} other{# professionals}}{city} awaiting your tasks." => '{count, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}}{city} ждут ваших заданий.',
    "On our website you can choose a specialist {city} according to your requirements and with reasonable price." => 'На нашем сайте вы можете выбрать специалиста{city} по вашим требованиям и приемлемой цене.',
    "{countPro, plural, one{# professional} other{# professionals}}{city} waiting for your orders." => '{countPro, plural, one{# исполнитель} few{# исполнителя} other{# исполнителей}}{city} ждут ваших заявок.',
    "Articles from our users" => 'Статьи от наших пользователей',
    "Articles from our users{request}{tattributes}" => 'Статьи от наших пользователей{request}{tattributes}',
    "{count, plural, =0{Articles} one{# article} other{# articles}} from our users are waiting for your ratings"
        => '{count, plural, =0{Статьи} one{# статья} few{# статьи} other{# статей}} от наших пользователей ждут ваших оценок',
    "{count, plural, =0{Articles} one{# article} other{# articles}} from {countPro, plural, =0{authors} one{# author} other{# authors}}{Request}{Attributes} | Read and rate now | Site of professionals uJobs.me"
        => '{count, plural, =0{Статьи} one{# статья} few{# статьи} other{# статей}} от {countPro, plural, =0{авторов} one{# автора} other{# авторов}}{Request}{Attributes} | Читайте и оценивайте сейчас | Сайт профессионалов uJobs.me',
    "Here are {count, plural, =0{articles} one{# article} other{# articles}} from {countPro, plural, =0{authors} one{# author} other{# authors}} on uJobs.me{request}{tattributes}"
        => 'Здесь представлены {count, plural, =0{статьи} one{# статья} few{# статьи} other{# статей}} от {countPro, plural, =0{авторов} one{# автора} other{# авторов}} на uJobs.me{request}',
    "Create article{kequest}{kattributes}" => 'Создать статью{kequest}{kattributes}',
    "Use this <a href='/article/form'>link</a> and follow all steps of the form." => "Перейдите по этой <a href='/article/form'>ссылке</a> и пройдите все шаги формы.",
    "Share your article with your friends!" => 'Делитесь вашими статьями с друзьями!',
    "Articles in category {category}" => 'Статьи в категории {category}',
    "{count, plural, =0{Articles} one{# article} other{# articles}} from {countPro, plural, =0{users} one{# user} other{# users}} in category {category}"
        => '{count, plural, =0{Статьи} one{# статья} few{# статьи} other{# статей}} от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} в категории {category}',
    "{count, plural, =0{Articles} one{# article} other{# articles}} from {countPro, plural, =0{authors} one{# author} other{# authors}} in category {category} | Read and rate now | Site of professionals uJobs.me"
        => '{count, plural, =0{Статьи} one{# статья} few{# статьи} other{# статей}} от {countPro, plural, =0{авторов} one{# автора} other{# авторов}} в категории {category} | Читайте и оценивайте сейчас | Сайт профессионалов uJobs.me',
    "{category} articles from {countPro, plural, =0{users} one{# user} other{# users}} on uJobs.me"
        => '{category} статьи от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} на uJobs.me',
    "{category} articles" => '{category} статьи',
    "Articles with tag {title}" => 'Статьи с тегом {title}',
    "{count, plural, =0{Articles} one{# article} other{# articles}} with tag {title}" => '{count, plural, =0{Статьи} one{# статья} few{# статьи} other{# статей}} с тегом {title}',
    "{count, plural, =0{Articles} one{# article} other{# articles}} from {countPro, plural, =0{authors} one{# author} other{# authors}} with tag {title} | Read and rate now | Site of professionals uJobs.me"
        => '{count, plural, =0{Статьи} one{# статья} few{# статьи} other{# статей}} от {countPro, plural, =0{авторов} one{# автора} other{# авторов}} с тегом {title} | Читайте и оценивайте сейчас | Сайт профессионалов uJobs.me',
    "{title} articles from {countPro, plural, =0{users} one{# user} other{# users}} on uJobs.me" => '{title} статьи от {countPro, plural, =0{пользователей} one{# пользователя} other{# пользователей}} на uJobs.me',
    "{title} articles" => '{title} статьи',
    'at the price {price}' => 'по цене {price}',
    'at the contract price' => 'по договорной цене',
    'Are you looking for a job{city}?' => 'Ищете работу{city}?',
    '{countPro, plural, one{# customer} other{# customer}} ready to give you a task'
        => '{countPro, plural, one{# заказчик} few{# заказчика} other{# заказчиков}} готовы дать вам задание',
    'You can find a job on our site according to your skills and abilities'
        => 'На нашем сайте вы можете найти работу по вашим навыкам',
    'Respond to request and get task.' => 'Откликнитесь на заказ и получите задание.',
    'Articles and news about «{category}»' => 'Статьи и новости по теме «{category}»',
    'Articles and news  about «{category}» from members of the site uJobs.me' => 'Статьи и новости на тему «{category}» от участников сайта ujobs.me',
    '{count, plural, =0{Articles and news} one{# article} other{# articles and news}} about «{category}» from members of the portal uJobs {count, plural, =one{is} other{are}} presented on this page{request}{tattributes}'
        => 'На данной странице представлены {count, plural, =0{статьи и новости} one{# статья} other{# статьи и новости} other{# статей и новостей}} на тему «{category}» от участников портала uJobs{request}{tattributes}',
    '{category} articles; {category} news{kequest}{kattributes}' => 'Статьи {category}; Новости {category}{kequest}{kattributes}',
    'Articles and news about «{title}»' => 'Статьи и новости по теме «{title}»',
    'Articles and news  about «{title}» from members of the site uJobs.me' => 'Статьи и новости на тему «{title}» от участников сайта ujobs.me',
    '{count, plural, =0{Articles and news} one{# article} other{# articles and news}} about «{title}» from members of the portal uJobs {count, plural, =one{is} other{are}} presented on this page'
        => 'На данной странице представлены {count, plural, =0{статьи и новости} one{# статья} other{# статьи и новости} other{# статей и новостей}} на тему «{title}» от участников портала uJobs',
    '{title} articles; {title} news' => 'Статьи {title}; Новости {title}',
    'We prepared the most interesting collections from our participants about «{category}»' => 'Мы подготовили наиболее интересные подборки от наших участников на тему «{category}»',
    'We prepared the most interesting collections from our participants about «{title}»' => 'Мы подготовили наиболее интересные подборки от наших участников на тему «{title}»',
    'On our portal you can not only read but also publish news and articles about «{category}».'
        => 'На нашем портале вы можете не только читать но также опубликовать новости и статьи на тему «{category}».',
    'On our portal you can not only read but also publish news and articles about «{title}».'
        => 'На нашем портале вы можете не только читать но также опубликовать новости и статьи на тему «{title}».',
    'Publishing articles and news will allow you to increase your reputation in the eyes of customers and tell about the features of your business.'
        => 'Публикация статей и новостей позволит вам повысить вашу репутацию в глазах заказчиков и рассказать про особенности вашего бизнеса.',
    'And interesting articles and news from other participants will help you to understand the features of the services and products that they offer and choose the most worthy.'
        => 'А интересные статьи и новости от других участников помогут вам разобраться с особенностями услуг и товаров которые они предлагают и выбрать наиболее достойного.',
    'To ensure that your articles bring the maximum benefit to you, based on our experience, we recommend you:'
        => 'Для того что бы ваши статьи принесли максимальную пользу для вас, на основании нашего опыта мы вам рекомендуем:',

    'Try to write in the first person, telling in detail what you have news and information about «{category}»'
        => 'Старайтесь писать от первого лица, рассказывая в подробностях какие у вас есть новости и информация по теме «{category}»',
    'Try to write in the first person, telling in detail what you have news and information about «{title}»'
        => 'Старайтесь писать от первого лица, рассказывая в подробностях какие у вас есть новости и информация по теме «{title}»',
    'The volume of articles that will allow you to be indexed by search engines should be at least 3 000 characters (one A4 page)'
        => 'Объем статей который позволит вам быть проиндексированный поисковыми системами должен быть не менее 3 000 знаков (одна страница А4)',
    'Place as many articles and news as possible. Do this regularly'
        => 'Размещайте как можно больше статей и новостей. Делайте это регулярно',
    'Try to tell in an unobtrusive manner why you can achieve a better result by «{category}». Direct advertising rarely gives results.'
        => 'Старайтесь рассказывать в ненавязчивой форме , почему именно вы сможете добиться лучшего результата по «{category}». Прямая реклама редко дает результаты.',
    'Try to tell in an unobtrusive manner why you can achieve a better result by «{title}». Direct advertising rarely gives results.'
        => 'Старайтесь рассказывать в ненавязчивой форме , почему именно вы сможете добиться лучшего результата по «{title}». Прямая реклама редко дает результаты.',
    'The specific examples (Life case) about «{category}», where you tell what special features you had while working with this topic, what difficulties you faced and how you solved them, work well.'
        => 'Хорошо работают конкретные примеры (Life case) по теме «{category}», где вы рассказываете какие у вас были особенности в работе с данной темой, с какими сложностями вы сталкивались и как их решили.',
    'The specific examples (Life case) about «{title}», where you tell what special features you had while working with this topic, what difficulties you faced and how you solved them, work well.'
        => 'Хорошо работают конкретные примеры (Life case) по теме «{title}», где вы рассказываете какие у вас были особенности в работе с данной темой, с какими сложностями вы сталкивались и как их решили.',
    'Publish high-quality pictures. Photo or infographics can quickly explain what you want to report.'
        => 'Публикуйте высококачественные картинки. Фотография или инфографика могут быстрее объяснить, что вы хотите сообщить.',
    'Good luck in reading and publishing news and articles on our portal.'
        => 'Удачи вам в чтении и публикации новостей и статей на нашем портале.',
    'And remember that for the publication of articles our ALGORITHM raises your ratings and puts you higher in the issuance on the portal when searching.'
        => 'И помните, что за публикацию статей наш АЛГОРИТМ повышает ваши рейтинги и ставит вас выше в выдаче на портале при поиске.',
    'Sincerely,</br>Support service ujobs.me'
        => 'С уважением,</br>Служба поддержки ujobs.me',
    'Page of company {title} on site uJobs.me' => 'Страница компании {title} на портале uJobs.me',
    'View services and product, requests on services and products, and news from this company. Buying and selling is easy with secure transaction on uJobs.me' => 'Просматривайте предложения услуг и товаров, заявки на услуги и товары, и новости от компании {title}. Покупать и продавать легко через безопасную сделку с uJobs.me',
    '{title}, products {title}, services {title}' => '{title}, товары {title}, услуги {title}',
    'Training video catalog' => 'Каталог обучающего видео',
    'Choose the topic of study that interests you' => 'Выберите интересующую вас тему обучения',
    'Do you want to learn a new specialty?' => 'Хотите изучить новую специальность?',
    'With our video lessons it\'s easy to do' => 'С нашими видеоуроками это просто сделать',
    'Choose the video course of interest' => 'Выберите интересующий видеокурс',
    'Pay and get new knowledge' => 'Оплати и получай новые знания',
    'Training video catalog about {category}' => 'Каталог обучающих видео по {category}',
    'Want to get new knowledge about {category}?' => 'Хотите получить новые знания по {category}?',
    'Choose the course about {category}' => 'Выберите курс по {category}',
    'Make payment for course about {category}' => 'Оплатите курс по {category}',
    'Get new knowledge about {category}' => 'Получайте новые знания по {category}',
    'Choose the video courses you are interested in' => 'Выберите интересующие вас видеокурсы',
    'Order learning video «{title}» | Site of professionals uJobs.me' => 'Заказать обучающее видео «{title}» | Сайт профессионалов uJobs.me',
    'Order learning video «{title}» on the site of professionals uJobs.me.' => 'Заказать обучающее видео «{title}» на сайте профессионалов uJobs.me',
    '{title}, learning video {title}' => '{title}, обучающее видео {title}',
    'Buy {title} for {price} on uJobs.me site'
    => 'Купить {title} по цене {price} на сайте uJobs.me',
    'Service ordering process is as follows:' => 'Процесс заказа услуги выглядит следующим образом:',
    'We are offering {count} options for your request: «{request}». Choose verified professionals on uJobs.me.'
        => 'Предлагаем {count} вариантов по вашему запросу: «{request}». Выберите проверенных исполнителей на сайте uJobs.me.',
    '{request} | {count} Offers{minPrice}' => '{request} | {count} Предложений{minPrice}',
    '{count} Requests{tags}{Attributes}{City}{Request} | Price from {minPrice}' => '{count} Заявки{tags}{Attributes}{City}{Request} | Цена от {minPrice}',
    '{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}' => '{count} {category}{tags}{Attributes}{City}{Request} | Цена от {minPrice}',
    '{count} Services{tags}{Attributes}{City}{Request} | Price from {minPrice}' => '{count} Услуги{tags}{Attributes}{City}{Request} | Цена от {minPrice}',
    '{count} Other Services from «{parentCategory}»{tags}{Attributes}{City}{Request} | Price from {minPrice}' => '{count} Другие Услуги из «{parentCategory}»{tags}{Attributes}{City}{Request} | Цена от {minPrice}',
    '{count} Products{tags}{Attributes}{City}{Request} | Price from {minPrice}' => '{count} Товары{tags}{Attributes}{City}{Request} | Цена от {minPrice}',
    'Contract' => 'Договорная',
    '{Title} (Price {Price})' => '{Title} (Цена {Price})',
    'Price from {minPrice}' => 'Цена от {minPrice}'
];
