<?php 
 return [
    'Address information' => 'عنوان مقدم الطلب',
    'Birthdate' => 'تاريخ الميلاد',
    'City' => 'المدينة',
    'Claimant address information' => 'عنوان مقدم الطلب',
    'Claimant personal information' => 'البيانات الشخصية لمقدم الطلب',
    'Credit' => 'الدين',
    'Credit information' => 'المعلومات الائتمانية',
    'Creditor' => 'المقرض',
    'Defendant address information' => 'المتهم',
    'Defendant personal information' => 'البيانات الشخصية المتهم',
    'Document city' => 'المدينة الوثيقة',
    'Document number' => 'الوثيقة رقم',
    'Document number and serial' => 'سلسلة رقم الوثيقة',
    'Document region' => 'منطقة المستند',
    'Document serial' => 'سلسلة من الوثيقة',
    'Email' => 'البريد الإلكتروني',
    'Firstname' => 'اسم',
    'Flat' => 'شقة',
    'House' => 'البيت',
    'Housing' => 'الحالة',
    'Lastname' => 'اللقب',
    'Marriage date' => 'موعد الزفاف',
    'Marriage dates' => 'موعد الزفاف',
    'Middlename' => 'العائلي',
    'Personal information' => 'المعلومات الشخصية',
    'Phone' => 'الهاتف',
    'Street' => 'الشارع',
    'Year of divorce' => 'السنة من الطلاق',
    'Your document is formed. You can download it from the link' => 'مستند تم إنشاؤه. يمكنك تحميل البرنامج من الرابط',
];