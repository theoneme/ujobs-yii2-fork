<?php 
 return [
    'All in' => 'جميع الفئات',
    'Categories with skill "{skill}"' => 'فئات مع مهارة "{skill}"',
    'Categories with specialty "{specialty}"' => 'فئات: "{specialty}"',
    'Categories with tag "{tag}"' => 'الفئة الوسم "{tag}"',
    'Hide filters' => 'انهيار المرشحات',
    'More filters' => 'المرشحات',
    'Price' => 'السعر',
    'Price ascending' => 'سعر تصاعدي',
    'Price descending' => 'سعر تنازلي',
    'Publish date' => 'الجدة',
    'Rating ascending' => 'تصنيف تصاعدي',
    'Rating descending' => 'تصنيف تنازلي',
    'Related categories for request {request}' => 'فئات ذات صلة طلب {request}',
    'Relevance' => 'أهمية',
    'See more' => 'عرض المزيد',
    'Show all' => 'عرض كل',
];