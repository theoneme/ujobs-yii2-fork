<?php 
 return [
    'All in' => 'หัวข้อทั้งหมด',
    'Categories with skill "{skill}"' => 'หมวดหมู่กับทักษะ"{skill}"',
    'Categories with specialty "{specialty}"' => 'หมวดหมู่:สนาม"{specialty}"',
    'Categories with tag "{tag}"' => 'หมวดหมู่ป้ายกำกับ"{tag}"',
    'Hide filters' => 'ย่อเก็บตัวกรอง',
    'More filters' => 'ตัวกรอง',
    'Price' => 'ราคา',
    'Price ascending' => 'คาการส่งจดหมาย',
    'Price descending' => 'คาการส่งจดหมาย message-&gt;',
    'Publish date' => 'Novelty',
    'Rating ascending' => 'ระดับความชื่นชอบการส่งจดหมาย',
    'Rating descending' => 'ระดับความชื่นชอบการส่งจดหมาย message-&gt;',
    'Related categories for request {request}' => 'เกี่ยวข้องกันหมวดหมู่สำหรับการร้องขอ{request}',
    'Relevance' => 'ชี้แจงว่ามันเกี่ยวข้องหรือ',
    'See more' => 'แสดงมากกว่า',
    'Show all' => 'แสดงทั้งหมด',
];