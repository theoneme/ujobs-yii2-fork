<?php 
 return [
    'All in' => 'Kaikki luokat',
    'Categories with skill "{skill}"' => 'Kategoriat taito "{skill}"',
    'Categories with specialty "{specialty}"' => 'Luokat: - kenttään "{specialty}"',
    'Categories with tag "{tag}"' => 'Category tag "{tag}"',
    'Hide filters' => 'Romahdus suodattimet',
    'More filters' => 'Suodattimet',
    'Price' => 'Hinta',
    'Price ascending' => 'Hinta nouseva',
    'Price descending' => 'Hinta-laskeva',
    'Publish date' => 'Uutuus',
    'Rating ascending' => 'Luokitus nouseva',
    'Rating descending' => 'Luokitus laskeva',
    'Related categories for request {request}' => 'Liittyvät luokat pyynnöstä {request}',
    'Relevance' => 'Merkitystä',
    'See more' => 'Näytä lisää',
    'Show all' => 'Näytä kaikki',
];