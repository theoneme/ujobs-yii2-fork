<?php 
 return [
    'Address information' => 'Osoite hakijan',
    'Birthdate' => 'Syntymäaika',
    'City' => 'Kaupungin',
    'Claimant address information' => 'Osoite hakijan',
    'Claimant personal information' => 'Henkilötiedot hakijan',
    'Credit' => 'Velka',
    'Credit information' => 'Luottotiedot',
    'Creditor' => 'Lainanantaja',
    'Defendant address information' => 'Vastaaja',
    'Defendant personal information' => 'Henkilötietoja vastaaja',
    'Document city' => 'Kaupungin asiakirja',
    'Document number' => 'Asiakirjan numero',
    'Document number and serial' => 'Sarja ja numero, asiakirjan',
    'Document region' => 'Alueen asiakirja',
    'Document serial' => 'Sarjan asiakirja',
    'Email' => 'Sähköposti',
    'Firstname' => 'Nimi',
    'Flat' => 'Huoneisto',
    'House' => 'Talo',
    'Housing' => 'Tapauksessa',
    'Lastname' => 'Sukunimi',
    'Marriage date' => 'Häiden päivämäärä',
    'Marriage dates' => 'Häiden päivämäärä',
    'Middlename' => 'Patronyymi',
    'Personal information' => 'Henkilökohtaisia tietoja',
    'Phone' => 'Puhelin',
    'Street' => 'Street',
    'Year of divorce' => 'Vuonna avioero',
    'Your document is formed. You can download it from the link' => 'Asiakirja on luotu. Voit ladata sen linkin',
];