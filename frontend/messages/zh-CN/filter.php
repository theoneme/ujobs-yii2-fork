<?php 
 return [
    'All in' => '所有的类别的',
    'Categories with skill "{skill}"' => '有能力的类别"{skill}"',
    'Categories with specialty "{specialty}"' => '有专业的类别"{specialty}"',
    'Categories with tag "{tag}"' => '有标记的类别"{tag}"',
    'Checkbox' => null,
    'Checkbox list' => null,
    'Display in filter as' => null,
    'Do not display' => null,
    'Dropdown' => null,
    'Filter settings' => null,
    'Filterable attributes' => null,
    'Hide filters' => '最小化过滤器',
    'Links' => null,
    'More filters' => '其他过滤器',
    'Price' => '价格',
    'Price ascending' => '价格上升',
    'Price descending' => '价格下降',
    'Publish date' => '按新的排名',
    'Radio' => null,
    'Range' => null,
    'Rating ascending' => '评级上升',
    'Rating descending' => '评级下降',
    'Related categories for request {request}' => '请求相关类别{request}',
    'Relevance' => '相关性',
    'Save' => null,
    'See more' => '更多',
    'Show all' => '所有',
];