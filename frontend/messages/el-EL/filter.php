<?php 
 return [
    'All in' => 'Όλα στην κατηγορία',
    'Categories with skill "{skill}"' => 'Κατηγορίες με την ικανότητα "{skill}"',
    'Categories with specialty "{specialty}"' => 'Κατηγορίες με ειδικότητα "{specialty}"',
    'Categories with tag "{tag}"' => 'Κατηγορίες με ετικέτα "{tag}"',
    'Hide filters' => 'Ελαχιστοποίηση φίλτρα',
    'More filters' => 'Ακόμα φίλτρα',
    'Price' => 'Τιμή',
    'Price ascending' => 'Τιμή αύξουσα',
    'Price descending' => 'Τιμή σε φθίνουσα',
    'Publish date' => 'Καινοτομία',
    'Rating ascending' => 'Βαθμολογία αύξουσα',
    'Rating descending' => 'Βαθμολογία κατά φθίνουσα σειρά',
    'Related categories for request {request}' => 'Σχετικές κατηγορίες, κατόπιν αιτήματος {request}',
    'Relevance' => 'Συνάφεια',
    'See more' => 'Δείτε περισσότερα',
    'Show all' => 'Εμφάνιση όλων',
];