<?php 
 return [
    'Address information' => 'Η διεύθυνση του αιτούντος',
    'Birthdate' => 'Ημερομηνία γέννησης',
    'City' => 'Πόλη',
    'Claimant address information' => 'Η διεύθυνση του αιτούντος',
    'Claimant personal information' => 'Τα προσωπικά στοιχεία του αιτούντος',
    'Credit' => 'Το χρέος',
    'Credit information' => 'Πιστωτική πληροφορίες',
    'Creditor' => 'Ο δανειστής',
    'Defendant address information' => 'Η διεύθυνση του εναγομένου',
    'Defendant personal information' => 'Τα προσωπικά στοιχεία του εναγομένου',
    'Document city' => 'Η πόλη του εγγράφου',
    'Document number' => 'Αριθμός εγγράφου',
    'Document number and serial' => 'Σειρά και τον αριθμό του εγγράφου',
    'Document region' => 'Περιοχή του εγγράφου',
    'Document serial' => 'Σειρά εγγράφων',
    'Email' => 'Email',
    'Firstname' => 'Όνομα',
    'Flat' => 'Διαμέρισμα',
    'House' => 'Το σπίτι',
    'Housing' => 'Το περίβλημα',
    'Lastname' => 'Επώνυμο',
    'Marriage date' => 'Η ημερομηνία του γάμου',
    'Marriage dates' => 'Η ημερομηνία του γάμου',
    'Middlename' => 'Πατρώνυμο',
    'Personal information' => 'Προσωπικές πληροφορίες',
    'Phone' => 'Τηλέφωνο',
    'Street' => 'Οδός',
    'Year of divorce' => 'Έτος διαζύγιο',
    'Your document is formed. You can download it from the link' => 'Το έγγραφο διαμορφώνεται. Μπορείτε να το κατεβάσετε από το σύνδεσμο',
];