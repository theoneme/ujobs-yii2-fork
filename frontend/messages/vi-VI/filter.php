<?php 
 return [
    'All in' => 'Tất cả các loại',
    'Categories with skill "{skill}"' => 'Loại với kỹ năng "{skill}"',
    'Categories with specialty "{specialty}"' => 'Loại: trường "{specialty}"',
    'Categories with tag "{tag}"' => 'Loại, nhãn "{tag}"',
    'Hide filters' => 'Sụp đổ bộ lọc',
    'More filters' => 'Bộ lọc',
    'Price' => 'Giá',
    'Price ascending' => 'Giá tăng dần',
    'Price descending' => 'Giá giảm dần',
    'Publish date' => 'Mới lạ',
    'Rating ascending' => 'Giá tăng dần',
    'Rating descending' => 'Đánh giá giảm dần',
    'Related categories for request {request}' => 'Liên quan loại cho yêu cầu {request}',
    'Relevance' => 'Sự liên quan',
    'See more' => 'Show hơn',
    'Show all' => 'Hiển thị tất cả',
];