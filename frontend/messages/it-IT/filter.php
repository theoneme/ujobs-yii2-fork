<?php 
 return [
    'All in' => 'Nella categoria',
    'Categories with skill "{skill}"' => 'Categorie con l\'abilità "{skill}"',
    'Categories with specialty "{specialty}"' => 'Categorie con specialità "{specialty}"',
    'Categories with tag "{tag}"' => 'Categoria con tag "{tag}"',
    'Hide filters' => 'Rotolare i filtri',
    'More filters' => 'Ancora i filtri',
    'Price' => 'Prezzo',
    'Price ascending' => 'Prezzo crescente',
    'Price descending' => 'Costo decrescente',
    'Publish date' => 'Novità',
    'Rating ascending' => 'Valutazione in ordine crescente',
    'Rating descending' => 'Rating decrescente',
    'Related categories for request {request}' => 'Correlati categoria su richiesta {request}',
    'Relevance' => 'Di pertinenza',
    'See more' => 'Mostra più',
    'Show all' => 'Mostra tutti',
];