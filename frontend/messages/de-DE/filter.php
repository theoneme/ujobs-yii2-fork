<?php 
 return [
    'All in' => 'Alle in der Kategorie',
    'Categories with skill "{skill}"' => 'Kategorie mit der Fähigkeit "{skill}"',
    'Categories with specialty "{specialty}"' => 'Kategorie Fachgebiet "{specialty}"',
    'Categories with tag "{tag}"' => 'Kategorie mit dem Tag "{tag}"',
    'Hide filters' => 'Collapse Filter',
    'More filters' => 'Noch Filter',
    'Price' => 'Preis',
    'Price ascending' => 'Preis aufsteigend',
    'Price descending' => 'Preis absteigend',
    'Publish date' => 'Neuheit',
    'Rating ascending' => 'Bewertung aufsteigend',
    'Rating descending' => 'Bewertung absteigend',
    'Related categories for request {request}' => 'Verwandte Kategorien auf Anfrage {request}',
    'Relevance' => 'Relevanz',
    'See more' => 'Karte noch',
    'Show all' => 'Alle anzeigen',
];