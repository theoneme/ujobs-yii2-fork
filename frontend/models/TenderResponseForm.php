<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 07.04.2017
 * Time: 12:14
 */

namespace frontend\models;

use common\models\Attachment;
use yii\base\Model;
use Yii;
use yii\web\UploadedFile;

/**
 * Class TenderResponseForm
 * @package frontend\models
 */
class TenderResponseForm extends Model
{
    const SCENARIO_CONTRACT_PRICE = 1;
    const SCENARIO_NOT_CONTRACT_PRICE = 2;

    public $uploadedFiles;

    /**
     * @var integer
     */
    public $id = null;

    /**
     * @var integer
     */
    public $price = null;

    /**
     * @var string
     */
    public $content = null;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CONTRACT_PRICE] = [
            'id',
            'price',
            'content',
            'uploadedFiles',
        ];
        $scenarios[self::SCENARIO_NOT_CONTRACT_PRICE] = [
            'id',
            'price',
            'content',
            'uploadedFiles',
        ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['price'], 'integer', 'min' => 200],
            [['price'], 'required', 'on' => self::SCENARIO_CONTRACT_PRICE],
            ['content', 'string', 'max' => 555],
            [['price'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['uploadedFiles',
                'file',
                'maxFiles' => 0,
                'maxSize' => 1024 * 1024 * 30,
                'tooBig' => Yii::t('app', 'The file "{file}" is too big. Its size cannot exceed 30MB.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'price' => Yii::t('model', 'Price'),
        ];
    }

    public function save()
    {
        $attachments = [];

        $path = '/uploads/order-history/' . date('Y') . '/' . date('m') . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $this->uploadedFiles = UploadedFile::getInstances($this, 'uploadedFiles');
        foreach ($this->uploadedFiles as $file) {
            $name = $path . $file->baseName . '-' .  hash('crc32b', $file->name . time()) . "." . $file->extension;
            if ($file->saveAs(Yii::getAlias('@webroot') . $name)) {
                $attachment = new Attachment();
                //$attachment->entity_id = $history->id;
                $attachment->entity = 'order-history';
                $attachment->content = $name;
                //$attachment->save();

                $attachments[] = $attachment;
            }
        }

        return $attachments;
    }
}