<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.03.2017
 * Time: 19:06
 */

namespace frontend\models;

use common\models\Invoice;
use common\models\Payment;
use common\models\user\User;
use kartik\mpdf\Pdf;
use yii\base\Model;
use Yii;
use yii\helpers\Url;

/**
 * Class EmailInvoiceForm
 * @package frontend\models
 */
class EmailInvoiceForm extends Model
{
    public $email;
    public $invoiceId;

    /* @var User $user */
    public $user = null;

    public function init()
    {
        if(!isGuest()) {
            $this->email = Yii::$app->user->identity->email;

            $this->user = Yii::$app->user->identity;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            ['invoiceId', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('model', 'Email'),
            'invoiceId' => Yii::t('model', 'Invoice ID'),
        ];
    }

    public function sendMail()
    {
        if($this->user != null) {
            $invoice = Invoice::findOne(['id' => $this->invoiceId, 'user_id' => $this->user->id]);
            if($invoice != null) {
                $mail = Yii::$app->mailer->compose()
                    ->setTo($this->email)
                    ->setFrom(Yii::$app->params['ujobsNoty'])
                    ->setSubject(Yii::t('model', 'Invoice'))
                    ->setTextBody(Yii::t('order', 'Invoice for order payment'))
                    ->attach(Yii::getAlias('@webroot') . $invoice->invoice_file);

                return $mail->send();
            }
        }

        return false;
    }
}