<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 17:10
 */

namespace frontend\models;

use common\components\CurrencyHelper;
use common\models\PaymentHistory;
use common\models\UserWithdrawal;
use common\models\user\Profile;
use common\models\user\User;
use common\models\WithdrawalWallet;
use yii\base\Model;
use Yii;

/**
 * Class WithdrawalRequestForm
 * @package frontend\models
 */
class WithdrawalRequestForm extends Model
{
	public $amount;
    public $currency_code;
	public $targetMethod;
	public $targetAccount;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['amount'], 'integer', 'min' => 1],
			[['amount', 'targetMethod', 'targetAccount', 'currency_code'], 'required']
		];
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

//		$amount = CurrencyHelper::convert(Yii::$app->params['app_currency_code'], 'RUB', $this->amount);
        /* @var $user User*/
        $user = Yii::$app->user->identity->company_user_id ? Yii::$app->user->identity->companyUser : Yii::$app->user->identity;
		if (!isset($user->wallets[$this->currency_code]) && $user->wallets[$this->currency_code]->balance < $this->amount) {
			return false;
		}

		$userWithdrawal = new UserWithdrawal();
		$userWithdrawal->amount = $this->amount;
        $userWithdrawal->currency_code = $this->currency_code;
		$userWithdrawal->target_account = $this->targetAccount;
		$userWithdrawal->target_method = $this->targetMethod;
		if ($userWithdrawal->validate()) {
			$userWithdrawal->save();

			$paymentHistory = new PaymentHistory();
			$paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_INFO;
			$paymentHistory->type = PaymentHistory::TYPE_OPERATION_INFO;
			$paymentHistory->template = PaymentHistory::TEMPLATE_PAYMENT_WITHDRAWAL_REQUEST;
            $paymentHistory->currency_code = $this->currency_code;
			$paymentHistory->amount = $this->amount;
			$paymentHistory->custom_data = json_encode([
			    'amount' => CurrencyHelper::format($this->currency_code, $this->amount)
            ]);
			$paymentHistory->user_id = $userWithdrawal->user_id;
			$paymentHistory->save();

			return true;
		}

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'amount' => Yii::t('model', 'Amount'),
		];
	}

	/**
	 * @return array
	 */
	public static function getUserPaymentData()
	{
		/* @var User $user ; */
		$user = Yii::$app->user->identity;
		$paymentAccounts = array_map(function($var){
		    /* @var $var WithdrawalWallet*/
		    return $var->getTypeLabel() . " - " . $var->number;
        }, $user->withdrawalWallets);

		return $paymentAccounts;
	}

    /**
     * @return array
     */
    public static function getUserPaymentDataLong()
    {
        /* @var User $user ; */
        $user = Yii::$app->user->identity;
        $paymentAccounts = array_map(function($var){
            /* @var $var WithdrawalWallet*/
            $result = $var->getTypeLabel() . '. ' . $var->getAttributeLabel('number') . " - " . $var->number;
            if (!empty($var->customDataArray)) {
                foreach ($var->customDataArray as $type => $value) {
                    $result .= '. ' . $var->getAttributeLabel($type) . " - " . $value;
                }
            }
            return $result;
        }, $user->withdrawalWallets);

        return $paymentAccounts;
    }
}