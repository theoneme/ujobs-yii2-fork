<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 12:28
 */

namespace frontend\models;

use common\models\Comment;
use common\models\user\User;
use yii\base\Model;
use Yii;

/**
 * Class PostCommentForm
 * @package frontend\models
 */
class PostCommentForm extends Model
{
    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $email = null;

    /**
     * @var string
     */
    public $comment = null;

    /**
     * @var string
     */
    public $entity = 'comment';

    /**
     * @var integer
     */
    public $entityId = null;

    /**
     *
     */
    public function init()
    {
        if(!Yii::$app->user->isGuest) {
            /** @var User $user */
            $user = Yii::$app->user->identity;

            $this->name = $user->company_user_id === null ? $user->profile->getSellerName() : $user->companyUser->company->getSellerName();
            $this->email = $user->company_user_id === null ? $user->email : null;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'comment'], 'required'],
            ['entityId', 'integer'],
            ['comment', 'string', 'max' => 800],
            [['name', 'email'], 'string', 'max' => 65],
            ['email', 'email'],
            ['entity', 'in', 'range' => ['comment', 'portfolio']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('model', 'Name'),
            'email' => Yii::t('model', 'Email'),
            'comment' => Yii::t('model', 'Comment'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if(!$this->validate()) {
            return false;
        }

        $comment = new Comment();
        $comment->attributes = $this->attributes;
        $comment->entity = $this->entity;
        $comment->entity_id = $this->entityId;

        return $comment->save();
    }
}