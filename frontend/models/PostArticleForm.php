<?php

namespace frontend\models;

use common\models\Category;
use common\models\ContentTranslation;
use common\models\Page;
use common\models\PageAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PostArticleForm
 * @package frontend\models
 *
 * @property Page $page
 */
class PostArticleForm extends Page
{
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var string
     */
    public $tags = null;
    /**
     * @var Page
     */
    private $_page;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_diff_key(array_merge(parent::rules(), [
            [['title', 'parent_category_id', 'category_id'], 'required'],
            [['category_id'], 'exist', 'skipOnError' => false, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
//            ['title', 'string', 'max' => 200],
//            ['description', 'string'],
            ['tags', 'string', 'max' => 260],
        ]), ['uniqueAlias' => 0]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'tags' => Yii::t('model', 'Tags'),
            'description' => Yii::t('model', 'Text'),
        ]);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!empty($this->validateAll())) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->page->attributes = $this->attributes;
        $this->page->type = Page::TYPE_ARTICLE;
        if (!$this->page->alias) {
            $this->page->alias = Yii::$app->utility->generateSlug($this->title, 4, 50);
        }

        $isSaved = $this->page->save();
        $transaction->commit();
        $this->page->updateElastic();

        return $isSaved;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->_page;
    }

    /**
     * @param Page $page
     */
    public function setPage(Page $page)
    {
        $this->_page = $page;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);

        $this->title = reset($input['ContentTranslation'])['title'];
        foreach ($input['ContentTranslation'] as $value) {
            $value['entity'] = 'page';
            $id = (array_key_exists('id', $value) && is_numeric($value['id'])) ? (int)$value['id'] : null;
            $translation = $this->page->bind('translations', $id, $value['locale']);
            $translation->attributes = $value;
            $translation->setScenario(ContentTranslation::SCENARIO_ARTICLE);
        }

        $tagAttribute = Attribute::findOne(['alias' => 'page_tag']);
        if($tagAttribute !== null) {
            $tags = !empty($this->tags) ? array_filter(explode(',', $this->tags)) : [];
            foreach ($tags as $tag) {
                $pageAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(PageAttribute::class, $tagAttribute->id, $tag, ['locale' => Yii::$app->language]);
                $this->page->bind('extra', $pageAttributeObject->id)->attributes = $pageAttributeObject->attributes;
            }
        }
    }

    /**
     * @param $id
     */
    public function loadPage($id)
    {
        $this->page = Page::findOne((int)$id);
        $this->attributes = $this->page->attributes;
        $this->title = $this->page->translation->title;
        $this->description = $this->page->translation->content;
        $this->parent_category_id = $this->page->category->parent->id;

        $this->tags = implode(', ',
            array_map(function($value) {
                /* @var $value PageAttribute*/
                return $value->getTitle();
            }, $this->page->tags)
        );
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->page->validateWithRelations()
        );
    }
}