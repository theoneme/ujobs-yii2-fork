<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.12.2016
 * Time: 16:11
 */

namespace frontend\models;

use yii\base\Model;
use Yii;

class IndexSearchForm extends Model
{
    public $request;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['request', 'string', 'max' => 80],
//            [['request'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'request' => '',
        ];
    }
}