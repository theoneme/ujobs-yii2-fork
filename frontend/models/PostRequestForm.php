<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.04.2017
 * Time: 14:54
 */

namespace frontend\models;

use common\components\CurrencyHelper;
use common\models\Attachment;
use common\models\ContentTranslation;
use common\models\Job;
use common\models\JobAttribute;
use common\models\JobExtra;
use common\models\JobPackage;
use common\models\JobQa;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\store\models\Currency;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class PostJobForm
 * @package frontend\models
 *
 * @property Job $job
 * @property JobPackage[] $packages
 * @property JobQa[] $faq
 * @property JobExtra[] $extras
 */
class PostRequestForm extends Model
{
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var string
     */
    public $specialties;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var integer
     */
    public $price;
    /**
     * @var boolean
     */
    public $contract_price;
    /**
     * @var integer
     */
    public $percent_bonus;
    /**
     * @var string
     */
    public $period_type;
    /**
     * @var integer
     */
    public $date_start;
    /**
     * @var integer
     */
    public $date_end;
    /**
     * @var string
     */
    public $type;
    /**
     * @var Job
     */
    private $_job;
    /**
     * @var array
     */
    private $_attachments = null;
    /**
     * @var string
     */
    public $currency_code = 'RUB';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'required' => [['category_id', 'parent_category_id', 'description'], 'required'],
            'integer' => [['category_id', 'parent_category_id'], 'integer'],

            ['specialties', 'string', 'max' => 160],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            ['description', 'string', 'max' => 65535],

            'periodType' => ['period_type', 'in', 'range' => [
                Job::PERIOD_START_FROM,
                Job::PERIOD_FINISH_TO,
                Job::PERIOD_RANGE
            ]],
            'type' => ['type', 'in', 'range' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER]],

            'percentBonus' => ['percent_bonus', 'number', 'min' => 0, 'max' => 100],
            'contractPrice' => ['contract_price', 'boolean'],

            [['date_start', 'date_end'], 'date', 'format' => 'php:d-m-Y'],

//            [['date_start', 'date_end'], 'validateDate'],
            ['currency_code', 'validateCurrency', 'skipOnEmpty' => false],
            ['price', 'validatePrice', 'skipOnEmpty' => false],

            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages']), 'skipOnEmpty' => false]
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validatePrice($attribute, $params)
    {
        if (!is_numeric($this->price) && $this->price) {
            $this->addError($attribute, Yii::t('yii', '{attribute} must be a number.', ['attribute' => $this->getAttributeLabel($attribute)]));
        }

        if (CurrencyHelper::convert('RUB', $this->currency_code, 200) > $this->price && !$this->contract_price) {
            $this->addError($attribute, Yii::t('app', 'Price must be greater than {0}', [CurrencyHelper::convert('RUB', $this->currency_code, 200)]));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if (!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateDate($attribute, $params)
    {
        if(strtotime($this->date_start) <= time() - 60 * 60 * 24) {
            $this->addError($attribute, Yii::t('yii', '{attribute} must be greater than "{compareValueOrAttribute}".', [
                'attribute' => $this->getAttributeLabel('date_start'),
                'compareValueOrAttribute' => Yii::$app->formatter->asDate(time())
            ]));
        }

        if(strtotime($this->date_end) <= time() - 60 * 60 * 24) {
            $this->addError($attribute, Yii::t('yii', '{attribute} must be greater than "{compareValueOrAttribute}".', [
                'attribute' => $this->getAttributeLabel('date_end'),
                'compareValueOrAttribute' => Yii::$app->formatter->asDate(time())
            ]));
        }

        if($this->date_start && $this->date_end && strtotime($this->date_start) >= strtotime($this->date_end)) {
            $this->addError('date_end', Yii::t('yii', '{attribute} must be greater than "{compareValueOrAttribute}".', [
                'attribute' => $this->getAttributeLabel('date_end'),
                'compareValueOrAttribute' => $this->date_end
            ]));
        }
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->locale === null) {
            $this->locale = 'ru-RU';
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Title'),
            'category_id' => Yii::t('model', 'Category'),
            'parent_category_id' => Yii::t('model', 'Category'),
            'type' => Yii::t('model', 'Type'),
            'specialties' => Yii::t('model', 'Specialties'),
            'description' => Yii::t('model', 'Description'),
            'price' => Yii::t('model', 'Price'),
            'percent_bonus' => Yii::t('model', 'Percent bonus'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();
        if (empty($this->price)) {
            $this->contract_price = true;
        }
        return $valid;
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $this->job->attributes = $this->attributes;
        $this->job->locale = $this->locale;
        if ($this->job->save()) {
            $transaction->commit();

            $this->job->updateElastic();
            $this->job->profile->updateElastic();

            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @return array|Attachment[]
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->job->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->job->attachments;
            }
        }
        return $this->_attachments;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->_job;
    }

    /**
     * @param Job $job
     */
    public function setJob(Job $job)
    {
        $this->_job = $job;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);

        if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
            foreach ($input['Attachment'] as $key => $item) {
                $attachmentObject = new Attachment();
                $attachmentObject->setAttributes($item);
                $attachmentObject->entity = Job::TYPE_JOB;

                $this->job->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
            }
        }

        $translationObject = new ContentTranslation([
            'title' => $this->title,
            'content' => $this->description,
            'locale' => $this->locale,
            'entity' => Job::TYPE_JOB,
        ]);
        $this->job->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;

        $specialtyAttribute = Attribute::findOne(['alias' => 'specialty']);
        $this->bindJobAttributes(["attribute_{$specialtyAttribute->id}" => !empty($this->specialties) ? explode(',', $this->specialties) : null]);
        if (!empty($input['JobAttribute'])) {
            foreach ($input['JobAttribute'] as $attribute) {
                $this->bindJobAttributes(["attribute_{$attribute['attribute_id']}" => $attribute['value']]);
            }
        }
    }

    /**
     * @param $id
     */
    public function loadJob($id)
    {
        $this->job = Job::findOne((int)$id);
        $this->attributes = $this->job->attributes;
        $this->title = $this->job->translation->title;
        $this->description = $this->job->translation->content;

        $this->specialties = implode(', ', array_map(function ($value) {
            /* @var $value JobAttribute*/
            return $value->getTitle();
        }, $this->job->specialties));
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->job->validateWithRelations()
        );
    }

    /**
     * @param array $attributes
     */
    private function bindJobAttributes(array $attributes)
    {
        foreach ($attributes as $key => $attribute) {
            if (!$attribute) continue;

            $parts = explode('_', $key);

            if (is_array($attribute)) {
                $values = $attribute;
            } else {
                $valueParts = array_filter(explode(',', $attribute));
                $values = $valueParts;
            }

            foreach($values as $pa) {
                $jobAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(JobAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                if($jobAttributeObject !== null) {
                    $this->job->bind('extra', $jobAttributeObject->id)->attributes = $jobAttributeObject->attributes;
                }
            }
        }
    }
}