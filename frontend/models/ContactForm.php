<?php

namespace frontend\models;

use common\models\Attachment;
use common\models\SupportRequest;
use common\models\user\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * Class ContactForm
 * @package frontend\models
 */
class ContactForm extends SupportRequest
{
    public $uploadedFiles;
    public $verifyCode;

    /** @var User $user */
    public $user;

    /** @var string */
    public $firstname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['uploadedFiles', 'file', 'maxFiles' => 3],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            ['firstname', 'string']
        ]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'uploadedFiles' => Yii::t('model', 'Attachments'),
            'verifyCode' => Yii::t('model', 'Verify code'),
        ]);
    }

    /**
     * @return bool
     */
    public function submit()
    {
        if(!empty($this->firstname)) {
            return false;
        }

        $this->save(false);
        $mainMail = Yii::$app->mailer->compose()
            ->setTo(['netproject7@gmail.com', 'info@ujobs.me'])
            ->setFrom(Yii::$app->params['ujobsNoty'])
            ->setSubject('Сообщение с контактной формы')
            ->setTextBody("Имя пользователя: " . $this->name . "\nEmail: " . $this->email . "\nСообщение: " . $this->description);

        $path = '/uploads/contact/' . date('Y') . '/' . date('m') . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $this->uploadedFiles = UploadedFile::getInstances($this, 'uploadedFiles');
        if($this->uploadedFiles) {
            foreach ($this->uploadedFiles as $file) {
                $name = $path . hash('crc32b', $file->name . time()) . "." . $file->extension;
                if ($file->saveAs(Yii::getAlias('@webroot') . $name)) {
                    $attachment = new Attachment();
                    $attachment->entity_id = $this->id;
                    $attachment->entity = 'support_request';
                    $attachment->content = $name;
                    $attachment->save();
                    $mainMail->attach(Yii::getAlias('@webroot') . $attachment->content);
                }
            }
        }

        if($this->user instanceof User) {
            $locale = $this->user->site_language;
        } else {
            $locale = Yii::$app->language;
        }

        $responseMail = Yii::$app->mailer->compose([
            'html' => '@frontend/views/email/contact-form-auto'
        ], [
            'linkLabel' => Yii::t('notifications', 'Post for free', [], $locale),
            'name' => $this->name,
            'locale' => $locale,
            'link' => Url::to(['/entity/global-create'], true),
        ])
            ->setTo([$this->email])
            ->setFrom(Yii::$app->params['ujobsNoty'])
            ->setSubject(Yii::t('notifications', 'Post announcement about selling services or products on uJobs.me for free', [], $locale));

        return $mainMail->send() && $responseMail->send();
    }
}
