<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.03.2017
 * Time: 17:04
 */

namespace frontend\models;

use borales\extensions\phoneInput\PhoneInputBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\components\CurrencyHelper;
use common\models\Invoice;
use common\models\Payment;
use kartik\mpdf\Pdf;
use yii\base\Model;
use Yii;
use yii\helpers\Url;

/**
 * Class InvoiceForm
 * @package frontend\models
 */
class InvoiceForm extends Model
{
    const TYPE_INDIVIDUAL = 'individual';
    const TYPE_LEGAL_ENTITY = 'legal-entity';

    const SCENARIO_INDIVIDUAL = 1;
    const SCENARIO_LEGAL_ENTITY = 2;

    /**
     * @var Invoice
     */
    public $invoice = null;

    /**
     * @var mixed
     */
    public $htmlInvoice;

    /**
     * @var string
     */
    public $template;

    /**
     * @var integer
     */
    public $decision;

    /**
     * @var integer
     */
    public $orderId = null;

    /**
     * @var string
     */
    public $companyName = null;

    /**
     * @var string
     */
    public $inn = null;

    /**
     * @var string
     */
    public $ogrn = null;

    /**
     * @var string
     */
    public $firstName = null;

    /**
     * @var string
     */
    public $lastName = null;

    /**
     * @var string
     */
    public $middleName = null;

    /**
     * @var string
     */
    public $address = null;

    /**
     * @var string
     */
    public $phone = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn'], 'string', 'min' => 8, 'max' => 16],
            ['ogrn', 'string', 'min' => 9, 'max' => 9],
            [['firstName', 'lastName', 'companyName'], 'string', 'min' => 2, 'max' => 40],
            [['address'], 'string', 'min' => 5, 'max' => 60],
            [['orderId', 'decision'], 'integer'],
            [['orderId', 'ogrn', 'inn', 'companyName', 'address', 'phone'], 'required', 'on' => self::SCENARIO_LEGAL_ENTITY],
            [['firstName', 'lastName', 'address', 'orderId', 'phone', 'middleName', 'ogrn'], 'required', 'on' => self::SCENARIO_INDIVIDUAL],

            [['phone'], 'string'],
//            [['phone'], PhoneInputValidator::class],
            [['htmlInvoice'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => PhoneInputBehavior::class,
                'phoneAttribute' => 'phone'
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LEGAL_ENTITY] = [
            'companyName',
            'inn',
            'ogrn',
            'address',
            'orderId',
            'decision',
            'phone'
        ];
        $scenarios[self::SCENARIO_INDIVIDUAL] = [
            'firstName',
            'lastName',
            'middleName',
            'address',
            'ogrn',
            'orderId',
            'decision',
            'phone'
        ];
        return $scenarios;
    }

    public function save()
    {
        $payment = Payment::findOne($this->orderId);

        if ($payment != null) {
            $year = date('Y');
            $month = date('m');
            $dir = Yii::getAlias('@webroot') . "/downloads/documents/{$year}/{$month}/";

            Yii::$app->utility->makeDir($dir);
            $fileName = "{$payment->user_id}_{$payment->id}_" . time() . ".pdf";
            $link = Url::to("/downloads/documents/{$year}/{$month}/{$fileName}");

            $fullName = $dir . $fileName;
            /* @var $pdf Pdf */
            $pdf = Yii::$app->pdf;
            $pdf->mode = Pdf::MODE_UTF8;
            $pdf->cssFile = '@frontend/web/css/new/pdf-style.css';

            @$pdf->output($this->htmlInvoice, $fullName, Pdf::DEST_FILE);

            $this->invoice = new Invoice([
                'payment_id' => $this->orderId,
                'type' => $this->scenario,
                'cost' => CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total),
                'invoice_file' => $link,
            ]);

            if ($this->invoice->save()) {
                return true;
            }

        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function getTemplate()
    {
        switch ($this->scenario) {
            case self::SCENARIO_INDIVIDUAL:
                $this->template = 'individual';
                break;
            case self::SCENARIO_LEGAL_ENTITY:
                $this->template = 'legal-entity';
                break;
            default:
                return false;
        }

        return $this->template;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'companyName' => Yii::t('model', 'Company Name'),
            'inn' => Yii::t('model', 'INN'),
            'ogrn' => Yii::t('model', 'OGRN'),
            'address' => Yii::t('model', 'Address'),
            'firstName' => Yii::t('model', 'Firstname'),
            'lastName' => Yii::t('model', 'Lastname'),
            'middleName' => Yii::t('model', 'Middlename'),
            'phone' => Yii::t('model', 'Phone')
        ];
    }
}