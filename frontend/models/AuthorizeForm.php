<?php

namespace frontend\models;

use common\components\CurrencyHelper;
use common\helpers\AuthorizeHelper;
use common\models\Payment;
use yii\base\Model;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class AuthorizeForm
 * @package frontend\models
 */
class AuthorizeForm extends Model
{
    const SCENARIO_BASIC = 10;
    const SCENARIO_ADVANCED = 20;

    /**
     * @var Payment
     */
    public $payment = null;
    /**
     * @var string
     */
    public $paymentId = null;
    /**
     * @var string
     */
    public $cardNumber = null;
    /**
     * @var string
     */
    public $expirationDate = null;
    /**
     * @var string
     */
    public $year = null;
    /**
     * @var string
     */
    public $month = null;
    /**
     * @var string
     */
    public $cardCode = null;
    /**
     * @var string
     */
    public $firstName = null;
    /**
     * @var string
     */
    public $lastName = null;
    /**
     * @var string
     */
    public $address = null;
    /**
     * @var string
     */
    public $city = null;
    /**
     * @var string
     */
    public $state = null;
    /**
     * @var string
     */
    public $country = null;
    /**
     * @var string
     */
    public $zip = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentId', 'cardNumber', 'cardCode', 'year', 'month'], 'required'],
            [['cardNumber', 'cardCode', 'zip', 'cardCode', 'cardCode'], 'number'],
            ['cardNumber', 'string', 'min' => 13, 'max' => 16],
//            ['expirationDate', 'string', 'length' => 7],
            ['cardCode', 'string', 'min' => 3, 'max' => 4],
            [['firstName', 'lastName'], 'string','max' => 50],
            [['address', 'country'], 'string', 'max' => 60],
            [['city', 'state'], 'string', 'max' => 40],
            [['zip'], 'string', 'max' => 20],
            ['year', 'number', 'min' => date('Y'), 'max' => date('Y') + 50],
            ['month', 'number', 'min' => 1, 'max' => 12],
            [['firstName', 'lastName', 'address', 'city', 'zip', 'state', 'country'], 'required', 'on' => self::SCENARIO_ADVANCED],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cardNumber' => Yii::t('model', 'Card Number'),
            'cardCode' => 'CVV',
            'year' => Yii::t('model', 'Expiration Date'),
            'month' => '',
            'firstName' => Yii::t('model', 'Firstname'),
            'lastName' => Yii::t('model', 'Lastname'),
            'address' => Yii::t('model', 'Address'),
            'city' => Yii::t('model', 'City'),
            'state' => Yii::t('model', 'State/Region'),
            'country' => Yii::t('model', 'Country'),
            'zip' => Yii::t('model', 'Zip Code'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_BASIC] = [
            'paymentId',
            'cardNumber',
            'cardCode',
            'year',
            'month',
        ];
        $scenarios[self::SCENARIO_ADVANCED] = array_merge($scenarios[self::SCENARIO_BASIC], [
            'firstName',
            'lastName',
            'address',
            'city',
            'zip',
            'state',
            'country'
        ]);
        return $scenarios;
    }

    public function save()
    {
        if ($this->validate()) {
            $this->payment = Payment::findOne($this->paymentId);
            if ($this->payment !== null) {
                if ($this->payment->status === Payment::STATUS_PENDING) {
                    return true;
                }
                else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'This payment has already been processed'));
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Payment with such id is not found'));
            }
        }
        else {
            Yii::$app->session->setFlash('error', array_values($this->getErrors())[0]);
        }
        return false;
    }

    public function getRequest() {
        $data = [
            'createTransactionRequest' => [
                'merchantAuthentication' => [
                    'name' => AuthorizeHelper::$login,
                    'transactionKey' => AuthorizeHelper::$password
                ],
                'refId' => $this->paymentId,
                'transactionRequest' => [
                    'transactionType' => 'authCaptureTransaction',
                    'amount' => '5',
                    'payment' => [
                        'creditCard' => [
                            'cardNumber' => $this->cardNumber,
                            'expirationDate' => $this->year . '-' . ($this->month < 10 ? '0' . $this->month : $this->month),
                            'cardCode' => $this->cardCode
                        ]
                    ],
                    'lineItems' => [
                        'lineItem' => [
                            'itemId' => '1',
                            'name' => 'vase',
                            'description' => 'Cannes logo',
                            'quantity' => '18',
                            'unitPrice' => '45.00',
                        ]
                    ],
                    'customer' => [
                        'id' => $this->payment->user_id
                    ],
                ]
            ]
        ];
        $orderData = [];
        switch ($this->payment->type) {
            case Payment::TYPE_ACCESS_PAYMENT:
                $orderData[] = [
                    'itemId' => $this->payment->tariff->id,
                    'name' => $this->payment->tariff->tariff->title,
                    'description' => '',
                    'quantity' => $this->payment->tariff->duration,
                    'unitPrice' => Yii::$app->formatter->asDecimal(CurrencyHelper::convert($this->payment->tariff->currency_code, 'USD', $this->payment->tariff->cost, false), 2),
                ];
                break;
            case Payment::TYPE_ACCOUNT_REPLENISHMENT:
                $orderData[] = [
                    'itemId' => null,
                    'name' => Yii::t('account', 'Account replenishment'),
                    'description' => '',
                    'quantity' => 1,
                    'unitPrice' => Yii::$app->formatter->asDecimal(CurrencyHelper::convert($this->payment->currency_code, 'USD', $this->payment->total, false), 2),
                ];
                break;
            default:
                foreach ($this->payment->orders as $order) {
                    $orderData[] = [
                        'itemId' => $order->orderProduct->id,
                        'name' => StringHelper::truncate(strip_tags($order->product->getLabel()), 27),
                        'description' => StringHelper::truncate(strip_tags($order->product->getDescription()), 250),
                        'quantity' => $order->orderProduct->quantity,
                        'unitPrice' => Yii::$app->formatter->asDecimal(CurrencyHelper::convert($order->product->currency_code, 'USD', $order->product->price, false), 2),
                    ];
                }
                break;
        }

        if ($this->scenario === self::SCENARIO_ADVANCED) {
            $data['createTransactionRequest']['transactionRequest']['billTo'] = [
                "firstName" => $this->firstName,
                "lastName" => $this->lastName,
                "address" => $this->address,
                "city" => $this->city,
                "state" => $this->state,
                "zip" => $this->zip,
                "country" => $this->country
            ];
        }
        return $data;
    }

    /**
     * @return bool|string
     */
    public function getTemplate()
    {
        switch ($this->scenario) {
            case self::SCENARIO_BASIC:
                return '_form_basic';
                break;
            case self::SCENARIO_ADVANCED:
                return '_form_advanced';
                break;
            default:
                return false;
        }
    }
}