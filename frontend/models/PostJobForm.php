<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\models;

use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\Category;
use common\models\ContentTranslation;
use common\models\DynamicForm;
use common\models\Job;
use common\models\JobAttribute;
use common\models\JobExtra;
use common\models\JobPackage;
use common\models\JobQa;
use common\models\Measure;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\store\models\Currency;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PostJobForm
 * @package frontend\models
 *
 * @property Job $job
 * @property JobPackage[] $packages
 * @property JobQa[] $faq
 * @property JobExtra[] $extras
 */
class PostJobForm extends Model
{
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var string
     */
    public $tags;
    /**
     * @var integer
     */
    public $execution_time;
    /**
     * @var bool
     */
    public $three_packages = false;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var string
     */
    public $currency_code = 'RUB';
    /**
     * @var string
     */
    public $requirements = null;
    /**
     * @var string
     */
    public $lat = null;
    /**
     * @var string
     */
    public $long = null;
    /**
     * @var string
     */
    public $address = null;
    /**
     * @var boolean
     */
    public $price_per_unit = 0;
    /**
     * @var string
     */
    public $measure_id = null;
    /**
     * @var integer
     */
    public $units_amount = null;
    /**
     * @var DynamicForm
     */
    public $dynamic_form = null;
    /**
     * @var Job
     */
    private $_job;
    /**
     * @var array
     */
    private $_faq = null;
    /**
     * @var array
     */
    private $_extras = null;
    /**
     * @var array
     */
    private $_jobAttributes = null;
    /**
     * @var array
     */
    private $_packages = null;
    /**
     * @var array
     */
    private $_attachments = null;

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_DRAFT = 'draft';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DRAFT] = [
            'locale',
            'title',
            'description',
            'currency_code',
            'requirements',
            'parent_category_id',
            'category_id',
            'tags',
            'execution_time',
            'three_packages',
            'lat',
            'long',
            'address',
            'price_per_unit',
            'units_amount',
            'measure_id',
        ];
        $scenarios[self::SCENARIO_DEFAULT] = [
            'locale',
            'title',
            'description',
            'currency_code',
            'parent_category_id',
            'requirements',
            'category_id',
            'tags',
            'execution_time',
            'three_packages',
            'lat',
            'long',
            'address',
            'price_per_unit',
            'units_amount',
            'measure_id',
        ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'categoryRequired' => [['category_id', 'parent_category_id'], 'required'],
            'categoryInteger' => [['category_id', 'parent_category_id'], 'integer'],
            'descriptionRequired' => ['description', 'required'],
            'tagsLength' => ['tags', 'string', 'max' => 260],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            ['description', 'string', 'max' => 65535],
            ['requirements', 'string', 'max' => 600],
            ['execution_time', 'integer'],
            ['execution_time', 'in', 'range' => range(1, 200)],
            [['three_packages', 'price_per_unit'], 'boolean'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages']), 'skipOnEmpty' => false],
            ['currency_code', 'validateCurrency', 'skipOnEmpty' => false],
            [['lat', 'long', 'address'], 'string'],
            [['units_amount', 'measure_id'], 'required', 'when' => function ($model) {return $model->price_per_unit === '1';}],
            ['units_amount', 'number', 'min' => 1],
            [['measure_id'], 'exist', 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if(!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Title'),
            'categoryId' => Yii::t('model', 'Category'),
            'parentCategoryId' => Yii::t('model', 'Category'),
            'tags' => Yii::t('model', 'Tags'),
            'description' => Yii::t('model', 'Description'),
            'requirements' => Yii::t('model', 'Requirements'),
            'currency_code' => Yii::t('model', 'Currency for your prices'),
            'execution_time' => Yii::t('model', 'Execution time (days)'),
            'address' => Yii::t('model', 'Address'),
            'units_amount' => Yii::t('model', 'Quantity'),
            'measure_id' => Yii::t('model', 'Measure unit'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!empty($this->validateAll())) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->job->type = Job::TYPE_JOB;
        $this->job->load($this->attributes, '');
//        $this->job->attributes = $this->attributes;
        $this->job->price = $this->packages['basic']->price;

        if ($this->scenario == self::SCENARIO_DRAFT) {
            $this->job->status = Job::STATUS_DRAFT;
        }

//        $this->job->detachBehavior('linkableRequirements');
        $isSaved = $this->job->save();
        $transaction->commit();

        if ($isSaved && $this->scenario != self::SCENARIO_DRAFT) {
            if ($this->lat && $this->long && $this->address) {
                if (!AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->exists()) {
                    (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $this->address]))->save();
                }
            }
            $this->job->defineStatus();
            $this->job->defineRating();
            $this->job->updateElastic();
            $this->job->profile->updateElastic();
        }

        return $isSaved;
    }

    /**
     * @return array|mixed
     */
    public function getFaq()
    {
        if ($this->_faq === null) {
            if ($this->job->isNewRecord) {
                $this->_faq = [];
            } else {
                $this->_faq = $this->job->faq;
            }
        }
        return $this->_faq;
    }

    /**
     * @return array|mixed
     */
    public function getExtras()
    {
        if ($this->_extras === null) {
            if ($this->job->isNewRecord) {
                $this->_extras = [];
            } else {
                $this->_extras = $this->job->extras;
            }
        }
        return $this->_extras;
    }

    /**
     * @return array|mixed
     */
    public function getJobAttributes()
    {
        if ($this->_jobAttributes === null) {
            if ($this->job->isNewRecord) {
                $this->_jobAttributes = [];
            } else {
                $this->_jobAttributes = $this->job->extra;
            }
        }
        return $this->_jobAttributes;
    }

    /**
     * @return array|Attachment[]|null
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->job->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->job->attachments;
            }
        }
        return $this->_attachments;
    }

    /**
     * @return array|null
     */
    public function getPackages()
    {
        if ($this->_packages === null) {
            if ($this->job->isNewRecord) {
                $this->_packages['basic'] = new JobPackage();
                $this->_packages['standart'] = new JobPackage();
                $this->_packages['premium'] = new JobPackage();
            } else {
                $basicPackage = JobPackage::find()->where(['job_id' => $this->job->id, 'type' => 'basic'])->one();
                $this->_packages['basic'] = $basicPackage ? $basicPackage : new JobPackage();

                $standardPackage = JobPackage::find()->where(['job_id' => $this->job->id, 'type' => 'standart'])->one();
                $this->_packages['standart'] = $standardPackage ? $standardPackage : new JobPackage();

                $premiumPackage = JobPackage::find()->where(['job_id' => $this->job->id, 'type' => 'premium'])->one();
                $this->_packages['premium'] = $premiumPackage ? $premiumPackage : new JobPackage();
            }
        }
        return $this->_packages;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->_job;
    }

    /**
     * @param Job $job
     */
    public function setJob(Job $job)
    {
        $this->_job = $job;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);
        $category = Category::findOne($this->subcategory_id ?? $this->category_id);

        if ($category !== null) {
            $dynamicAttributes = DynamicForm::getAttributesListByCategory($category);
            $this->dynamic_form = DynamicForm::initModelFromGroup($dynamicAttributes, []);
            $this->dynamic_form->load($input);
        }
        if ($this->dynamic_form !== null) {
            $this->bindJobAttributes($this->dynamic_form->attributes);
        }
//        $this->price_per_unit = $this->price_per_unit === null ? null : (bool)$this->price_per_unit;

        if (array_key_exists('JobQa', $input) && is_array($input['JobQa'])) {
            foreach ($input['JobQa'] as $key => $item) {
                $faqObject = new JobQa();
                $faqObject->setAttributes($item);

                $this->job->bind('faq', $faqObject->id)->attributes = $faqObject->attributes;
            }
        }

        if (array_key_exists('JobExtra', $input) && is_array($input['JobExtra'])) {
            foreach ($input['JobExtra'] as $key => $item) {
                $extraObject = new JobExtra();
                $extraObject->setAttributes($item);
                $extraObject->currency_code = $this->currency_code;

                $this->job->bind('extras', $extraObject->id)->attributes = $extraObject->attributes;
            }
        }

        if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
            foreach ($input['Attachment'] as $key => $item) {
                $attachmentObject = new Attachment();
                $attachmentObject->setAttributes($item);
                $attachmentObject->entity = Job::TYPE_JOB;

                $this->job->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
            }
        }

        $translationObject = new ContentTranslation([
            'title' => $this->title,
            'content' => $this->description,
            'locale' => $this->locale,
            'entity' => Job::TYPE_JOB,
        ]);
        $this->job->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;

        if (is_array($input['JobPackage'])) {
            foreach ($input['JobPackage'] as $key => $item) {
                $packageObject = $this->packages[$key];
                $item['type'] = $key;
                $packageObject->setAttributes($item);
                $packageObject->currency_code = $this->currency_code;

                if (empty($packageObject->title)) {
                    switch ($key) {
                        case 'basic':
                            $packageObject->title = Yii::t('app', 'Basic');
                            break;
                        case 'standart':
                            $packageObject->title = Yii::t('app', 'Standart');
                            break;
                        case 'premium':
                            $packageObject->title = Yii::t('app', 'Premium');
                            break;
                    }
                }

                if ($this->three_packages || $packageObject->type === 'basic') {
                    $this->job->bind('packages', $packageObject->id, "{$key}")->attributes = $packageObject->attributes;
                }
            }
        }

        $tagAttribute = Attribute::findOne(['alias' => 'tag']);
        if($tagAttribute !== null) {
            $this->bindJobAttributes(["attribute_{$tagAttribute->id}" => !empty($this->tags) ? explode(',', $this->tags) : null]);
            if (!empty($input['JobAttribute'])) {
                foreach ($input['JobAttribute'] as $attribute) {
                    $this->bindJobAttributes(["attribute_{$attribute['attribute_id']}" => $attribute['value']]);
                }
            }
        }
    }

    /**
     * @param $id
     */
    public function loadJob($id)
    {
        $this->job = Job::findOne((int)$id);
        $this->attributes = $this->job->attributes;
        $this->title = $this->job->translation->title;
        $this->description = $this->job->translation->content;

        $tags = $this->job->tags;
        $tagsF = array_map(function($value) {
            /* @var $value JobAttribute*/
            return $value->getTitle();
        }, $tags);
        $this->tags = implode(',', $tagsF);
        $this->three_packages = count($this->job->packages) > 1 ? true : false;
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->job->validateWithRelations()
        );
    }

    /**
     * @param array $attributes
     */
    private function bindJobAttributes(array $attributes)
    {
        foreach ($attributes as $key => $attribute) {
            if (!$attribute) continue;

            $parts = explode('_', $key);

            if (is_array($attribute)) {
                $values = $attribute;
            } else {
                $valueParts = array_filter(explode(',', $attribute));
                $values = $valueParts;
            }

            foreach($values as $pa) {
                $jobAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(JobAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                if($jobAttributeObject !== null) {
                    $this->job->bind('extra', $jobAttributeObject->id)->attributes = $jobAttributeObject->attributes;
                }
            }
        }
    }
}