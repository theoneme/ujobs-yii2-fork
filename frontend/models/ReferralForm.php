<?php

namespace frontend\models;

use common\models\EmailLog;
use common\models\Event;
use common\models\Referral;
use common\models\user\User;
use Yii;
use yii\base\Model;
use yii\validators\EmailValidator;

/**
 * ReferralForm is the model behind the contact form.
 */
class ReferralForm extends Model
{
    public $emails_string;
    public $emails;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['emails_string', 'required'],
            ['emails_string', 'string'],
            ['emails_string', 'checkEmailList', 'skipOnError' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'emails_string' => Yii::t('model', 'Email'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function checkEmailList($attribute, $params)
    {
        $this->emails = explode(',', $this->emails_string);
        $error = null;
        $emailValidator = new EmailValidator();
        foreach ($this->emails as $k => $email) {
            $email = trim($email);
            if (!$emailValidator->validate($email)) {
                $error = Yii::t('app', '{0} is not a valid email', [$email]);
                unset($this->emails[$k]);
            } else if (User::find()->where(['email' => $email])->exists()) {
                $error = Yii::t('app', '{0} - this email address is already registered', [$email]);
                unset($this->emails[$k]);
            } else {
                $referralExists = Referral::find()->where(['email' => $email, 'created_by' => Yii::$app->user->identity->id])->andWhere([
                    'exists',
                    Event::find()->where('event.entity_content = referral.id')->andWhere(['entity' => 'referral', 'code' => Event::REFERRAL_RESENT])
                ])->exists();
                if ($referralExists) {
                    $error = Yii::t('app', '{0} - you have already sent invitation to this email', [$email]);
                    unset($this->emails[$k]);
                }
            }
        }
        if (!count($this->emails)) {
            $this->addError($attribute, $error);
        }
    }

    /**
     * @return bool
     */
    public function submit()
    {
        if ($this->validate()) {
            $mailer = Yii::$app->mailer;
            $mailer->viewPath = '@frontend/views/email';
            foreach ($this->emails as $email) {
                /* @var $user User*/
                $user = Yii::$app->user->identity;
                $email = trim($email);
                $referral = Referral::findOrCreate(['email' => $email, 'created_by' => $user->getCurrentId()], false);
                if ($referral->isNewRecord) {
                    $referral->save();
                } else {
                    (new Event(['entity' => 'referral', 'entity_content' => (string)$referral->id, 'code' => Event::REFERRAL_RESENT]))->save();
                }

                $emailLog = new EmailLog([
                    'email' => $email,
                    'customDataArray' => [
                        'mailer' => 'mailer',
                        'viewPath' => '@frontend/views/email',
                        'view' => 'referral',
                        'subject' => Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]),
                        'receiverEmail' => $email,
                        'user' => $user->getCurrentId(),
                        'language' => $user->site_language
                    ]
                ]);
                $emailLog->save();

                $mailer->compose(['html' => 'referral'], ['user' => $user->company_user_id ? $user->companyUser : $user, 'language' => $user->site_language])
                    ->setTo([$email])
                    ->setFrom(Yii::$app->params['ujobsNoty'])
                    ->setSubject(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]))
                    ->send();
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Invitations sent. Thank you for supporting uJobs'));
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('app', 'Error occurred during sending invitations'));
        }

        return false;
    }
}
