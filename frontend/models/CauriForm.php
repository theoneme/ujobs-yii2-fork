<?php

namespace frontend\models;

use common\components\CurrencyHelper;
use common\helpers\AuthorizeHelper;
use common\models\Payment;
use yii\base\Model;
use Yii;
use yii\helpers\StringHelper;

/**
 * Class CauriForm
 * @package frontend\models
 */
class CauriForm extends Model
{
    /**
     * @var Payment
     */
    public $payment = null;
    /**
     * @var string
     */
    public $paymentId = null;
    /**
     * @var string
     */
    public $userId = null;
    /**
     * @var string
     */
    public $cardNumber = null;
    /**
     * @var string
     */
    public $year = null;
    /**
     * @var string
     */
    public $month = null;
    /**
     * @var string
     */
    public $cardCode = null;
    /**
     * @var string
     */
    public $token = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentId', 'userId', 'cardNumber', 'cardCode', 'year', 'month'], 'required'],
            [['cardNumber', 'cardCode'], 'number'],
            ['cardNumber', 'string', 'min' => 16, 'max' => 19],
            ['cardCode', 'string', 'min' => 3, 'max' => 4],
            ['year', 'number', 'min' => date('Y'), 'max' => date('Y') + 50],
            ['month', 'number', 'min' => 1, 'max' => 12],
            ['token', 'string', 'length' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cardNumber' => Yii::t('model', 'Card Number'),
            'cardCode' => 'CVV',
            'year' => Yii::t('model', 'Expiration Date'),
            'month' => '',
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $this->payment = Payment::findOne($this->paymentId);
            if ($this->payment !== null) {
                if ($this->payment->status === Payment::STATUS_PENDING) {
                    return true;
                }
                else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'This payment has already been processed'));
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Payment with such id is not found'));
            }
        }
        else {
            Yii::$app->session->setFlash('error', array_values($this->getErrors())[0]);
        }
        return false;
    }
}