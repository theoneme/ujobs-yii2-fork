<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.06.2018
 * Time: 15:41
 */

namespace frontend\models\search;

use common\models\Referral;
use common\models\Setting;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class ReferralSearch
 * @package common\models\search
 */
class ReferralSearch extends Referral
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Referral::find()
            ->where(['created_by' => Yii::$app->user->identity->getCurrentId()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
