<?php

namespace frontend\models\search;

use common\models\Job;
use common\models\UserPortfolio;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\Product;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * Class AttributeSearch
 * @package api\models\search
 */
class AttributeValueSearch extends AttributeValue
{
    /**
     * @var string
     */
    public $attributeAlias;

    /**
     * @var string
     */
    public $valueTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['attributeAlias', 'required'],
            ['attributeAlias', 'in', 'range' => self::getAllowedAttributes()],
            ['valueTitle', 'string']
        ];
    }

    /**
     * @return array
     */
    public static function getAllowedAttributes()
    {
        return [
            'tag',
            'product_tag',
            'portfolio_tag',
            'skill',
            'specialty',
//            'language',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $query = AttributeValue::find()
            ->joinWith(['translations' => function ($q) {
                return $q->select('mod_attribute_description.title, mod_attribute_description.entity_content, mod_attribute_description.locale');
            }, 'relatedAttribute'])
            ->select('mod_attribute_value.*')
            ->addSelect(['relatedCount' => $this->getRelatedQuery()])
            ->where([
                'status' => AttributeValue::STATUS_APPROVED
            ])
//            ->asArray()
            ->orderBy('relatedCount desc')
            ->having(['>', 'relatedCount', 1])
            ->groupBy('mod_attribute_value.id');
//            ->limit(50)
//            ->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50
            ],
//            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        if (!$this->validate()) {
            $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['mod_attribute.alias' => $this->attributeAlias]);
        $query->andFilterWhere(['like', 'mod_attribute_description.title', $this->valueTitle]);

        return $dataProvider;
    }

    /**
     * @return Query
     */
    public function getRelatedQuery(){
        switch ($this->attributeAlias) {
            case 'tag':
                return (new Query)
                    ->select("count('ja.id')")
                    ->from('job_attribute ja')
                    ->leftJoin('job j', 'j.id = ja.job_id')
                    ->where('ja.value = mod_attribute_value.id')
                    ->andWhere([
                        'j.status' => Job::STATUS_ACTIVE,
                        'j.type' => Job::TYPE_JOB
                    ]);
                break;
            case 'product_tag':
                return (new Query)
                    ->select("count('pa.id')")
                    ->from('product_attribute pa')
                    ->leftJoin('product p', 'p.id = pa.product_id')
                    ->where('pa.value = mod_attribute_value.id')
                    ->andWhere([
                        'p.type' => Product::TYPE_PRODUCT,
                        'p.status' => Product::STATUS_ACTIVE,
                    ]);
                break;
            case 'portfolio_tag':
                return (new Query)
                    ->select("count('pa.id')")
                    ->from('user_portfolio_attribute pa')
                    ->leftJoin('user_portfolio p', 'p.id = pa.user_portfolio_id')
                    ->where('pa.value = mod_attribute_value.id')
                    ->andWhere([
                        'p.status' => UserPortfolio::STATUS_ACTIVE,
                    ]);
                break;
            case 'skill':
            case 'specialty':
                return (new Query)
                    ->select("count('ua.id')")
                    ->from('user_attribute ua')
                    ->leftJoin('user u', 'u.id = ua.user_id')
                    ->where('ua.value = mod_attribute_value.id');
                break;
            default:
                return (new Query);
        }
    }

    /**
     * @return string
     */
    public function getBaseUrl(){
        switch ($this->attributeAlias) {
            case 'tag':
                return "/category/tag";
                break;
            case 'product_tag':
                return "/board/category/tag";
                break;
            case 'portfolio_tag':
                return "/category/portfolio-tag";
                break;
            case 'skill':
                return "/category/skill";
                break;
            case 'specialty':
                return "/category/specialty-pro";
                break;
            default:
                return null;
        }
    }

    /**
     * @return string
     */
    public function getAttributeParam(){
        switch ($this->attributeAlias) {
            case 'tag':
                return "reserved_job_tag";
                break;
            case 'product_tag':
                return "reserved_product_tag";
                break;
            case 'portfolio_tag':
                return "reserved_portfolio_tag";
                break;
            case 'skill':
                return "reserved_skill";
                break;
            case 'specialty':
                return "reserved_specialty";
                break;
            default:
                return null;
        }
    }
}
