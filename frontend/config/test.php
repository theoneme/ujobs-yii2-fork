<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'authClientCollection' => [
            'class' => \yii\authclient\Collection::class,
            'clients' => [
                'facebook' => [
                    'class' => 'frontend\components\oauth\Facebook',
                    'clientId' => '1063441727071026',
                    'clientSecret' => '216f2f93a87e0b11cd6125952181a5c4',
                    'attributeNames' => ['name', 'email', 'picture'],
                ],
                'google' => [
                    'class'        => 'frontend\components\oauth\Google',
                    'clientId'     => '540071815692-alpv00u5qi19lr17tatkv4ocf1akquqh.apps.googleusercontent.com',
                    'clientSecret' => 'e_pdEfOY4HATX6Mw1d3aG-CF',
                    //'attributeNames' => ['name', 'email', 'picture'],
                ],
            ],
        ],
    ],
    'modules' => [
        'treemanager' => [
            'class' => 'kartik\tree\Module',
        ],
        'user' => [
            'layout' => '@frontend/views/layouts/main',
            'class' => 'dektrium\user\Module',
            'controllerMap' => [
                'registration' => 'frontend\controllers\user\RegistrationController',
                'profile' => 'frontend\controllers\user\ProfileController',
                'profile-ajax' => 'frontend\controllers\user\ProfileAjaxController',
                'settings' => 'frontend\controllers\user\SettingsController',
                'security' => 'frontend\controllers\user\SecurityController',
                'recovery' => 'frontend\controllers\user\RecoveryController'
            ],
            'modelMap' => [
                'Account' => 'common\models\user\Account',
                'User' => 'common\models\user\User',
                'Profile' => 'common\models\user\Profile',
                'Token' => 'common\models\user\Token'
            ],
            'enableUnconfirmedLogin' => true,
            'enableConfirmation' => false,
            'mailer' => [
                'sender' => ['support@ujobs.me' => 'Команда uJobs'],
                'viewPath' => '@frontend/views/email'
            ],
            'rememberFor' => 252000,
        ],
        'account' => [
            'class' => 'frontend\modules\account\Module'
        ],
        'sitemap' => [
            'class' => 'frontend\modules\sitemap\Sitemap',
        ],
        'store' => [
            'class' => 'common\modules\store\Module',
        ],
    ],
    'params' => $params
];
