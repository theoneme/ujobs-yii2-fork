<?php
return [
    'fb_appid' => 1063441727071026,
    'root_category' => [
        'job_title_like' => 3,
        'package_title_like' => 0,
        'description_like' => 0,
        'jobs_exists' => 1,
        'description_length' => 1,
        'more_packages' => 1,
        'avatar_exists' => -3,
    ],
    'category' => [
        'job_title_like' => 3,
        'package_title_like' => 2,
        'description_like' => 1,
        'jobs_exists' => 1,
        'description_length' => 1,
        'more_packages' => 1,
        'avatar_exists' => -3,
    ],
    'tag' => [
        'job_title_like' => 5,
        'package_title_like' => 2,
        'description_like' => 1,
        'jobs_exists' => 1,
        'description_length' => 1,
        'more_packages' => 1,
        'avatar_exists' => -3,
    ],
];
