<?php

use common\modules\board\models\search\ProductSearch;
use frontend\models\search\AttributeValueSearch;
use frontend\modules\account\models\JobSearch;
use frontend\modules\account\models\OrderSearch;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'name' => 'uJobs',
    'language' => 'ru-RU',
    'bootstrap' => [
        'log',
        'store',
        'livechat',
        'board',
        'messenger',
        frontend\components\LanguageSelector::class,
        frontend\components\CurrencySelector::class,
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'treemanager' => [
            'class' => 'kartik\tree\Module',
        ],
        'telegram' => [
            'class' => 'frontend\modules\telegram\Module',
        ],
        'push' => [
            'class' => 'frontend\modules\push\Module',
        ],
        'skype' => [
            'class' => 'frontend\modules\skype\Module',
        ],
        'wizard' => [
            'class' => 'common\modules\wizard\Module',
        ],
        'messenger' => [
            'class' => 'frontend\modules\messenger\Module',
        ],
        'user' => [
            'layout' => '@frontend/views/layouts/main',
            'class' => 'dektrium\user\Module',
            'controllerMap' => [
                'registration' => 'frontend\controllers\user\RegistrationController',
                'profile' => 'frontend\controllers\user\ProfileController',
                'profile-ajax' => 'frontend\controllers\user\ProfileAjaxController',
                'settings' => 'frontend\controllers\user\SettingsController',
                'security' => 'frontend\controllers\user\SecurityController',
                'recovery' => 'frontend\controllers\user\RecoveryController'
            ],
            'modelMap' => [
                'Account' => 'common\models\user\Account',
                'User' => 'common\models\user\User',
                'Profile' => 'common\models\user\Profile',
                'Token' => 'common\models\user\Token'
            ],
            'enableUnconfirmedLogin' => true,
            'enableConfirmation' => false,
            'mailer' => [
                'sender' => ['message@ujobs.me' => 'Команда uJobs'],
                'viewPath' => '@frontend/views/email'
            ],
            'rememberFor' => 252000,
        ],
        'account' => [
            'class' => 'frontend\modules\account\Module'
        ],
        'sitemap' => [
            'class' => 'frontend\modules\sitemap\Sitemap',
        ],
        'store' => [
            'class' => 'common\modules\store\Module',
        ],
        'livechat' => [
            'class' => 'common\modules\livechat\Module'
        ],
        'board' => [
            'class' => 'common\modules\board\Module'
        ],
        'company' => [
            'class' => 'frontend\modules\company\Module'
        ]
    ],
    'components' => [
        'user' => [
            'identityCookie' => [
                'name' => '_frontendUser',
                'path' => '/'
            ],
            'authTimeout' => 60 * 60 * 24 * 10,
        ],
        'pdf' => [
            'class' => kartik\mpdf\Pdf::class,
            'format' => kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => kartik\mpdf\Pdf::DEST_BROWSER,
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'decimalSeparator' => '.',
        ],
        'authClientCollection' => [
            'class' => \yii\authclient\Collection::class,
            'clients' => [
                'facebook' => [
                    'class' => 'frontend\components\oauth\Facebook',
                    'clientId' => '1063441727071026',
                    'clientSecret' => '216f2f93a87e0b11cd6125952181a5c4',
                    'attributeNames' => ['name', 'email', 'picture'],
                ],
                'google' => [
                    'class' => 'frontend\components\oauth\Google',
                    'clientId' => '540071815692-alpv00u5qi19lr17tatkv4ocf1akquqh.apps.googleusercontent.com',
                    'clientSecret' => 'e_pdEfOY4HATX6Mw1d3aG-CF',
                    //'attributeNames' => ['name', 'email', 'picture'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'attribute*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'attribute' => 'attribute.php',
                    ],
                ],
                'labels*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'labels' => 'labels.php',
                    ],
                ],
                'order*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'order' => 'order.php',
                    ],
                ],
                'filter*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'formbuilder' => 'filter.php',
                    ],
                ],
                'account*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'account' => 'account.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'notifications*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'notifications' => 'notifications.php',
                    ],
                ],
                'document*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'document' => 'document.php',
                    ],
                ],
                'seo*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'document' => 'seo.php',
                    ],
                ],
            ],
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => [
                'httponly' => true,
                'lifetime' => 3600 * 8,
            ],
            'timeout' => 3600 * 24 * 30,
            'useCookies' => true,
            'name' => 'frontendSession'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'except' => ['yii\web\HttpException:404', 'yii\i18n\PhpMessageSource::loadFallbackMessages'],
                ],
            ],
        ],
        'errorHandler' => [
//            'errorAction' => 'site/error',
            'errorView' => '@frontend/views/site/error.php',
        ],
        'urlManager' => [
            'ruleConfig' => [
                'class' => 'frontend\components\LanguageUrlRule'
            ],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'rules' => [
                '/rl/<invited_by:\d+>' => 'site/index',
                '/' => 'site/index',
                'sitemap.xml' => 'sitemap/default/index',
                [
                    'pattern' => 'sitemap-<action(list|part)>-<entity:[\w-]+>',
                    'route' => 'sitemap/default/<action>',
                    'suffix' => '.xml',
                ],
                '/nachat-zarabativat' => 'page/start-selling',
                '/stat-frilanserom' => 'page/become-freelancer',
                '/smm-marketing-outsource' => 'page/outsource-marketing',
                '/autsorsing-menedgera-po-prodagam' => 'page/outsource-mpp',
                '/<alias:(privacy-policy|terms-of-service|pay|payment-process|fair-play|pay-any-way|voprosy-i-otvety)>' => 'page/static',
                '/tariffs' => 'page/tariffs',
                '/contact' => 'site/contact',
                '/news' => 'news/list',
                '/catalog-pro' => 'category/pro-catalog',
                '/catalog-job' => 'category/job-catalog',
                '/catalog-tender' => 'category/tender-catalog',
                '/catalog-video' => 'category/video-catalog',
                '/catalog-portfolio' => 'category/portfolio-catalog',
                '/catalog-article' => 'category/article-catalog',

                '/referral_program' => 'referral/index',

                'static/<category_1:[\w-]+>/<category_2:[\w-]+>/<category_3:[\w-]+>/<alias:[\w-]+>' => 'page/static',
                'static/<category_1:[\w-]+>/<category_2:[\w-]+>/<alias:[\w-]+>' => 'page/static',
                'static/<category_1:[\w-]+>/<alias:[\w-]+>' => 'page/static',
                'static/<alias:[\w-]+>' => 'page/static',

                'support-inbox/<status:[\w-]+>/<page:\d+>' => 'account/inbox/support',
                'support-inbox/<status:[\w-]+>' => 'account/inbox/support',
                'support-inbox' => 'account/inbox/support',
                'conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/start-conversation',
                'conversation/<id:\d+>' => 'account/inbox/conversation',
                'account/inbox/<action:(start-conversation|start-support-conversation|leave-group-chat)>' => 'account/inbox/<action>',
                'account/inbox/<status:[\w-]+>/<page:\d+>' => 'account/inbox/index',
                'account/inbox/<status:[\w-]+>' => 'account/inbox/index',
                'account/inbox' => 'account/inbox/index',

                'support-conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/start-support-conversation',
                'support-conversation/<id:\d+>' => 'account/inbox/support-conversation',

                '<alias:(' . implode('|', AttributeValueSearch::getAllowedAttributes()) . ')>/search' => 'attribute-value/search',

                'board/product/list/<status:(' . implode('|', ProductSearch::getStatusList()) . ')>' => 'board/product/list',
                'account/job/<status:(' . implode('|', JobSearch::getStatusList()) . ')>' => 'account/job/list',
                'account/tender/<status:(' . implode('|', JobSearch::getStatusList()) . ')>' => 'account/tender/list',

                'account/order/<status:(' . implode('|', OrderSearch::getJobStatusList()) . ')>' => 'account/order/list',
                'account/sell/<status:(' . implode('|', OrderSearch::getJobStatusList()) . ')>' => 'account/sell/list',
                'account/customer-tender/<status:(' . implode('|', OrderSearch::getTenderStatusList()) . ')>' => 'account/customer-tender/list',
                'account/worker-tender/<status:(' . implode('|', OrderSearch::getTenderStatusList()) . ')>' => 'account/worker-tender/list',
                'account/tender-response/<status:(' . implode('|', OrderSearch::getResponseStatusList()) . ')>' => 'account/tender-response/list',
                'account/product-tender-response/<status:(' . implode('|', OrderSearch::getResponseStatusList()) . ')>' => 'account/product-tender-response/list',

                'news/<alias:[\w-]+>' => 'news/view',
                'article/view/<alias:[\w-]+>' => 'article/view',

                '<reserved_specialty:[\w+-]+>-specialty-tenders-v-<city:[\w-]+>' => 'category/specialty-tender',
                '<reserved_specialty:[\w+-]+>-specialty-tenders' => 'category/specialty-tender',
                '<reserved_specialty:[\w+-]+>-specialty-pros-v-<city:[\w-]+>' => 'category/specialty-pro',
                '<reserved_specialty:[\w+-]+>-specialty-pros' => 'category/specialty-pro',

                '<alias:[\w-]+>-<action:(product)>' => 'board/<action>/view',
                '<alias:[\w-]+>-<action:(tender|job|video)>' => '<action>/view',
                '<alias:[\w-]+>-ptender' => 'board/tender/view',
                '<category_1:[\w-]+>-<action:(category|jobs|tenders|pros)>-v-<city:[\w-]+>' => 'category/<action>',
                '<category_1:[\w-]+>-<action:(category|jobs|tenders|pros|portfolios|articles)>' => 'category/<action>',
                '<category_1:[\w-]+>-<action:(products|ptenders)>-v-<city:[\w-]+>' => 'board/category/<action>',
                '<category_1:[\w-]+>-<action:(products|ptenders)>' => 'board/category/<action>',
                '<category_1:[\w-]+>-<action:(store)>-<store_id:\d+>' => 'board/category/<action>',
                '<action:(store)>-<store_id:\d+>' => 'board/category/<action>',
                '/besplatnaya-doska-obyavleniy-v-<city:[\w-]+>' => 'board/category/products',
                '/besplatnaya-doska-obyavleniy' => 'board/category/products',

                '<reserved_product_tag:[\w+-]+>-kupit-v-<city:[\w-]+>' => 'board/category/tag',
                '<reserved_product_tag:[\w+-]+>-kupit' => 'board/category/tag',
                '<reserved_portfolio_tag:[\w+-]+>-album' => 'category/portfolio-tag',
                '<reserved_job_tag:[\w+-]+>-service-in-<city:[\w-]+>' => 'category/tag',
                '<reserved_job_tag:[\w+-]+>-service' => 'category/tag',
                '<reserved_job_tag:[\w+-]+>-usluga-v-<city:[\w-]+>' => 'category/old-tag',
                '<reserved_job_tag:[\w+-]+>-usluga' => 'category/old-tag',
                '<reserved_skill:[\w+-]+>-skill-v-<city:[\w-]+>' => 'category/skill',
                '<reserved_skill:[\w+-]+>-skill' => 'category/skill',

                '<alias:[\w-]+>-videos' => 'category/videos',

                '<controller:\w+>/<id:\d+>' => '<controller>/view',

                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',

                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        '/js/new/jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend'
        ],
        'utility' => [
            'class' => 'common\components\Utility',
        ],
        'sms' => [
            'class' => 'common\components\Sms',
        ],
        'url' => [
            'class' => 'common\components\Url_helper',
        ],
        'opengraph' => [
            'class' => 'frontend\components\OpenGraph'
        ],
        'cart' => [
            'class' => 'common\modules\store\components\Cart'
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@frontend/views/user'
                ],
            ],
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => !YII_DEBUG,
            'concatCss' => true,
            'minifyCss' => true,
            'concatJs' => true,
            'minifyJs' => true,
            'minifyOutput' => true,
            'webPath' => '@web',
            'basePath' => '@webroot',
            'minifyPath' => '@webroot/minify',
            'jsPosition' => [\yii\web\View::POS_END],
            'forceCharset' => 'UTF-8',
            'expandImports' => true,
            'compressOptions' => ['extra' => true],
        ],
    ],
    'params' => $params,
];
