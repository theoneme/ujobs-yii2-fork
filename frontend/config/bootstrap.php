<?php

use yii\base\Event;

Event::on(\yii\web\User::class, \yii\web\User::EVENT_BEFORE_LOGIN, ['common\models\user\User', 'beforeLogin']);