<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.12.2016
 * Time: 18:26
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class OrderDetailsAsset
 * @package frontend\assets
 */
class OrderDetailsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/order-details.css'
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        CommonAsset::class
    ];
    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/order-details.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}