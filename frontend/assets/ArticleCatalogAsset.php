<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ArticleCatalogAsset
 * @package frontend\assets
 */
class ArticleCatalogAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/article-category.css',
    ];
    public $js = [
        'js/new/jquery.sticky.js',
    ];
    public $depends = [
        CatalogAsset::class
    ];
}