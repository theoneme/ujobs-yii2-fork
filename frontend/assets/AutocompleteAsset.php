<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 18:04
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AutocompleteAsset
 * @package frontend\assets
 */
class AutocompleteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/jquery-ui.min.css',
    ];
    public $js = [
        'js/new/jquery-ui.min.js'
    ];
    public $depends = [
        CommonAsset::class
    ];
}