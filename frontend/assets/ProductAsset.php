<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class ProductAsset
 * @package frontend\assets
 */
class ProductAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/work.css',
        'css/new/jquery.simpleLens.css',
    ];
    public $js = [
        'js/new/jquery.simpleLens.min.js',
        'js/new/jquery.simpleGallery.min.js',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        SlickAsset::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/work.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}