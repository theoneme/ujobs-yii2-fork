<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 21.09.2017
 * Time: 14:44
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class PortfolioAsset
 * @package frontend\assets
 */
class PortfolioAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/portfolio.css',
    ];
    public $js = [
        'js/new/masonry.js',
        'js/new/imagesloaded.min.js'
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/portfolio.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}