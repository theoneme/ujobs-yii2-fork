<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class CropperAsset
 * @package frontend\assets
 */
class CropperAsset extends AssetBundle
{
    public $sourcePath = '@bower/cropper/dist';
    public $css = [
        'cropper.css',
        '/css/new/mycrop.css'
    ];
    public $js = [
        'cropper.js',
        '/js/new/mycrop.js'
    ];
    public $depends = [
        CommonAsset::class,
    ];
}