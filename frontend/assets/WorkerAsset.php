<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 25.11.2016
 * Time: 13:20
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class WorkerAsset
 * @package frontend\assets
 */
class WorkerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/worker.css',
        'css/new/profile.css',
    ];
    public $js = [
        'js/new/clipboard.min.js',
        'js/new/jquery.mCustomScrollbar.concat.min.js'
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        CommonAsset::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = ['css/rtl/worker.css', 'css/new/profile.css'];
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}