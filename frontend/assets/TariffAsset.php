<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 27.02.2017
 * Time: 15:48
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class TariffAsset
 * @package frontend\assets
 */
class TariffAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/tariff.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}