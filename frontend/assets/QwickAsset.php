<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 20.03.2018
 * Time: 15:16
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class QwickAsset
 * @package frontend\assets
 */
class QwickAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/qwick.css',
        'css/new/font-awesome.min.css',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class
    ];
}