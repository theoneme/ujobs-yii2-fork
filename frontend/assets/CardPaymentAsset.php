<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class CardPaymentAsset
 * @package frontend\assets
 */
class CardPaymentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/card-payment.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}