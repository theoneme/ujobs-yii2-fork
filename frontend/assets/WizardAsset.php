<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 20.11.2017
 * Time: 16:50
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class WizardAsset
 * @package frontend\assets
 */
class WizardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/wizard.css',
        'css/new/slick.css',
        'css/new/slick-theme.css',
        'css/new/font-awesome.min.css',
    ];
    public $js = [
        'js/new/slick.js',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class
    ];
}