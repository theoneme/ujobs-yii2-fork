<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets;

use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

/**
 * Class CommonAsset
 * @package frontend\assets
 */
class CommonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/main.css',
        'css/new/mobile.css',
        'css/new/font-awesome.min.css',
        'css/new/addons.css',
        'css/new/auto-complete.css',
    ];
    public $js = [
        'js/new/script.js',
        'js/new/helpers.js',
        'js/new/auto-complete.min.js',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/main.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}