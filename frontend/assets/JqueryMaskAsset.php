<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class JqueryMaskAsset
 * @package frontend\assets
 */
class JqueryMaskAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-mask-plugin/dist';
    public $js = [
        'jquery.mask.min.js',
    ];
    public $depends = [
        CommonAsset::class,
    ];
}