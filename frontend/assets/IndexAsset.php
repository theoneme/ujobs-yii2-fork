<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:13
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class IndexAsset
 * @package frontend\assets
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/index.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/index.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}