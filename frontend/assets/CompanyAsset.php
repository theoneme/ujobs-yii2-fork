<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class CompanyAsset
 * @package frontend\assets
 */
class CompanyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/company.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}