<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.04.2017
 * Time: 17:11
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class NewLandingAsset
 * @package frontend\assets
 */
class NewLandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/landing.css',
        'css/new/font-awesome.min.css',
    ];
    public $js = [
        'js/new/parallax.min.js',
        'js/new/script-landing.js',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/main.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}