<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use airani\bootstrap\BootstrapRtlAsset;
use Yii;

/**
 * Class CatalogAsset
 * @package frontend\assets
 */
class CatalogAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/jquery.nouislider.min.css',
        'css/new/category.css',
    ];
    public $js = [
        'js/new/jquery.nouislider.min.js',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        CommonAsset::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'hi-HI'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/category.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }

}