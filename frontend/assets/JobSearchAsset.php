<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class JobSearchAsset
 * @package frontend\assets
 */
class JobSearchAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/job-search-help.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}