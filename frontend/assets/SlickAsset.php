<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class SlickAsset
 * @package frontend\assets
 */
class SlickAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/slick.css',
        'css/new/slick-theme.css',
    ];
    public $js = [
        'js/new/slick.js',
    ];
    public $depends = [
        YiiAsset::class,
    ];
}