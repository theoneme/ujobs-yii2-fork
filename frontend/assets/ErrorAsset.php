<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 11.09.2017
 * Time: 17:09
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ErrorAsset
 * @package frontend\assets
 */
class ErrorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/error.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}