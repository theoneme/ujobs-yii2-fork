<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class StartSellingAsset
 * @package frontend\assets
 */
class StartSellingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new/start-selling.css',
        'css/new/slick.css',
        'css/new/slick-theme.css',
    ];
    public $js = [
        'js/new/slick.min.js',
    ];
    public $depends = [
        CommonAsset::class
    ];
}