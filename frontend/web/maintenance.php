<!DOCTYPE html>
<html style="width: 100%; height: 100%">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/images/new/favicon.png" type="image/x-icon"/>
        <title>We are on maintenance</title>
    </head>
    <body style="width: 95%; height: 95%">
        <div style="width: 100%; height: 100%">
            <img src="/images/new/under-maintenance.jpg" alt="На ресурсе ведутся технические работы" style="margin: auto; display: block;">
        </div>
    </body>
</html>