/**
 * Created by Единоличница млин on 19.04.2017.
 */
$('.parallax-image').parallax({
    speed: '0.6',
    positionX: '0'
});

$(document).ready(function() {
    $('.example-item').each(function () {
        $(this).height($(this).width());
    })
});

$(window).resize(function() {
    $('.example-item').each(function () {
        $(this).height($(this).width());
    })
});