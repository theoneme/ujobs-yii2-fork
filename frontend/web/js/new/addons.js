/**
 * Created by ПК on 28.11.2016.
 */

function pushNotyError(message) {
    noty({
        layout: "bottomCenter",
        type: 'error',
        text: message,
        textAlign: "center",
        easing: "swing",
        animation: {
            open: {height: "toggle"},
            close: {height: "toggle"},
            easing: "swing",
            speed: 500
        },
        speed: 1500,
        timeout: 5000,
        closable: true,
        closeOnSelfClick: true
    })
}