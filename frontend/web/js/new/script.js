$(function () {
    var sideMenu, sideMenuHeight, menuItems, scrollItems, $body = $('body');

    function reconstructScroller() {
        $(document).trigger("scroll");
        sideMenu = $(".nav-work .menu-work"), sideMenuHeight = sideMenu.outerHeight() + 150, menuItems = sideMenu.find("a"), scrollItems = menuItems.map(function () {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });
    }

    $body.on('click', '.nav-work .menu-work a', function () {
        elementClick = $(this).attr("href");
        destination = $(elementClick).offset().top - 155;
        $("body,html").animate({
            scrollTop: destination
        }, 800);
        return false;
    });
    reconstructScroller();
    $(window).scroll(function () {
        var fromTop = $(this).scrollTop() + sideMenuHeight;
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop) return this;
        });
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";
        $(".menu-work a").removeClass("selected");
        menuItems.parent().end().filter("[href*=\\#" + id + "]").addClass("selected");
    });
});

function slider(sliderClass) {
    sliderClass += ' > div';
    var csl = $(sliderClass).length - 1;
    $(sliderClass).each(function () {
        if ($(this).css('opacity') == 1) {
            var indsl = $(this).index();
            $(sliderClass).eq(indsl).css('opacity', 0);
            if (indsl == csl) {
                $(sliderClass).eq(0).css('opacity', 1);
            } else {
                $(sliderClass).eq(indsl + 1).css('opacity', 1);
            }
        }
    });
}

function menumore() {
    $('.drop-menu.drop100 ul').html('');
    $('#top-menu ul.nav').each(function () {
        var widthli = $(this).find('.drop-menu.drop100').parent().outerWidth(true) + 20;
        $(this).children('li').each(function (index) {
            if (!$(this).children('.drop-menu').hasClass('drop100')) {
                if ($(this).parent().width() <= widthli + $(this).width()) {
                    $(this).siblings('li:last').show();
                    $('<li>' + $(this).find('a:first')[0].outerHTML + '</li>').appendTo($(this).siblings('li:last').find('.drop-menu.drop100 ul:eq(' + ((index % 2) - 1) + ')'));
                    $(this).hide();
                } else {
                    $(this).show();
                    widthli += $(this).width();
                }
            }
        });
    });
}

function headerhide() {
    var width = 20;
    var maxWidth = $('.dark-head .container-fluid').width() - 400;
    $('header .dark-head .nav-autor.modal-dismiss, header .dark-head .nav-right-item, header .dark-head .prof-menu').each(function () {
        width += $(this).outerWidth(true);
    });
    $('header .dark-head .nav-autor:not(.modal-dismiss)').each(function () {
        if (maxWidth <= width + $(this).outerWidth()) {
            $(this).hide();
        } else {
            $(this).show();
            width += $(this).outerWidth();
        }
    });
}

function aside_select() {
    $('.work-opt .ctext').each(function () {
        $(this).width($(this).parents('.work-opt ').width() - 150);
    });
}

function dropHeaderMenu(e, idMenu) {
    e.stopPropagation();
    if ($('.nav-head-profile.open').attr('id') != idMenu) {
        $('.nav-head-profile').removeClass('open');
    }
    $('#' + idMenu).toggleClass('open');
}

$(document).ready(function () {
    $(document.body).on('touchstart', function() {});

    $('body').on('click', '.head-banner .close-banner', function () {
        $(this).closest('.head-banner').slideUp(200);
        if ($(window).width() > 768) {
            setTimeout(function () {
                $('.all-content').css('padding-top', $('.dark-head').height() + 30);
                $('.nav-fixed').css('top', $('.dark-head').height() + 30);
            }, 10);
        } else {
            setTimeout(function () {
                $('.all-content').css('padding-top', $('.dark-head').height());
                $('.nav-fixed').css('top', $('.dark-head').height());
            }, 10);
        }
        return false;
    });

    if ($(window).width() > 768) {
        setTimeout(function () {
            $('.all-content').css('padding-top', $('.dark-head').height() + $('nav.top-menu').height() + ($('div.nav-main-prof').height() / 2) + $('.head-banner').height());
            $('.nav-fixed').css('top', $('.dark-head').height() + $('nav.top-menu').height() + ($('div.nav-main-prof').height() / 2) + $('.head-banner').height());
        }, 10);
    } else {
        setTimeout(function () {
            $('.all-content').css('padding-top', $('.dark-head').height() + $('.head-banner').height());
            $('.nav-fixed').css('top', $('.dark-head').height() + $('.head-banner').height());
        }, 10);
    }

    aside_select();
    $('a.read-more[href="#w-packs"]').on("click", function (e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 155
        }, 500);
        e.preventDefault();
    });
    $("a.btn-white").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top - 90;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        return false;
    });
    var search = 0;
    var carthead = 0;
    var collect = 0;
    var tg_open = 0;
    var marginnotes = 0;
    $body = $('body');
    $footer = $('footer');
    $('.mfooter').css('height', $footer.height() + 50);
    $footer.css('margin-top', -$footer.height() - 50);
    $('.readsym').each(function () {
        $(this).closest('.form-group').find('.ctspan').html($(this).val().length);
        $(this).closest('.white-mes-bg').find('.ctspan').html($(this).val().length);
    });
    if ($(window).width() >= 768) {
        menumore();
        headerhide();
    }

    $body.on('click', '.logon-category-menu', function () {
        var mright = $('.mobile-menu.mRight');
        if (mright.css('opacity') != '1') {
            mright.css('opacity', 1);
        }
        if (!mright.hasClass('mm-show')) {
            mright.addClass('mm-show');
            $('header').addClass('m-main mainRight');
            $('.all-content').addClass('m-main mainRight');
            $('html,body').css('overflow-x', 'hidden');
            mobile = 1;
        }
    });
    $body.on('click', '.m-main', function () {
        $('.mobile-menu').removeClass('mm-show');
        $('header').removeClass('m-main mainRight');
        $('.all-content').removeClass('m-main mainRight');
        $body.css('overflow-y', 'auto');
        $('html,body').css('overflow-x', 'auto');
    });

    $body.on('click', '.trigger-menu', function () {
        var mleft = $('.mobile-menu.mLeft');
        if (mleft.css('opacity') != '1') {
            mleft.css('opacity', 1);
        }
        if (!mleft.hasClass('mm-show')) {
            mleft.addClass('mm-show');
            $('header').addClass('m-main');
            $('.all-content').addClass('m-main');
            $('html,body').css('overflow-x', 'hidden');
            mobile = 1;
        }
    });

    $body.on('click', '.mmhead .btn-small', function () {
        $('.mobile-menu').removeClass('mm-show');
        $('.all-content').removeClass('m-main');
        $('header').removeClass('m-main');
    });

    $body.on('click', 'a.dropdown-toggle', function () {
        window.location.href = $(this).attr('href');
    });
    $body.on('click', '.check-but', function (e) {
        e.preventDefault();
        $('.lang-cur').show();
    });
    $body.on('mouseleave', '.lang-cur', function () {
        $('.lang-cur').hide();
    });
    $body.on('click', '.filter-title', function () {
        if ($(window).width() <= 768) {
            $('.aside-body').slideToggle('1000');
        } else {
            $('.aside-body').show();
        }
    });
    $body.on('click', '.globe', function (e) {
        dropHeaderMenu(e, 'header-language');
    });

    $body.on('click', '.prof-menu', function (e) {
        dropHeaderMenu(e, 'profile-drop');
    });

    $body.on('click', '.close-nav-head-profile', function (e) {
        e.preventDefault();
        $('.nav-head-profile').removeClass('open');
    });

    $body.on('click', '.del-lang', function () {
        $(this).parent().remove();
    });
  //////??????
    /*$body.on('click', '.butlikeradio', function (e) {
        e.preventDefault();
        $(this).hide();
        $('.inputlikeradio').show().focus();
        $('input[name="timepost"]').prop('checked', false);
    });*/
    $('input[type="number"].inputlikeradio').on("focusout", function () {
        if ($(this).val() == "") {
            $(this).hide();
            $('.butlikeradio').show();
        }
    });
    $body.on('click', 'input[name="timepost"]', function () {
        $('input[type="number"].inputlikeradio').hide();
        $('.butlikeradio').show();
    });
    $body.on('keyup', '.readsym', function () {
        $(this).closest('.form-group').find('.ctspan').html($(this).val().length);
        $(this).closest('.white-mes-bg').find('.ctspan').html($(this).val().length);
    });

    $(document).on('change', '.gigall', function () {
        if ($(this).prop('checked')) {
            $(this).parents('.tab-pane, .all-messages').find('tr input[type="checkbox"]').prop("checked", true);
        } else {
            $(this).parents('.tab-pane, .all-messages').find('tr input[type="checkbox"]').prop("checked", false);
        }
    });
    $(document).on('change', '.table-viewgigs input:checkbox', function () {
        if ($(this).prop('checked')) {
            $('.del-gigs').css('display', 'inline-block');
            if ($('.table-viewgigs .messages-row input:checkbox:checked').length == $('.table-viewgigs .messages-row input:checkbox').length) {
                $('.gigall').prop('checked', true);
            }
        } else {
            if (!$('.table-viewgigs input:checkbox:checked').length) {
                $('.del-gigs').hide();
            }
            $('.gigall').prop('checked', false);
        }
    });
    $body.on('click', '.tg-optbut', function () {
        $(this).parents('.tg-opt').addClass('tg-open');
        tg_open = 1;
    });
    $body.on('click', '.tg-optbut', function () {
        $(this).parents('.tg-opt').addClass('tg-open');
        tg_open = 1;
    });
    $body.on('click', '.ap-head', function () {
        if ($(window).width() > 992 && $(this).parents('.aside-price').find('.ap-body').css('display') == 'none') {
            $('.ap-head .fa').show();
            $('.ap-body').hide();
            $(this).parent().children('.ap-body').slideDown(500);
            $(this).children('.fa').hide();
        }
    });
    $body.on('click', '.faq-head', function () {
        if ($(this).parents('.faq-item').hasClass('open')) {
            $(this).parents('.faq-item').removeClass('open');
        } else {
            $('.faq-item').removeClass('open');
            $(this).parents('.faq-item').addClass('open');
        }
    });
    $('ul.gig-steps a[data-toggle="tab"]').on('shown.bs.tab', function () {
        if (navigator.userAgent.search(/Firefox/) <= 0) {
            $('#step2 .form-gig .row-packs').each(function () {
                $(this).children('.notes').css('margin-top', marginnotes);
                marginnotes += $(this).height();
            });
        }
    });
    $body.on('click', '.unlock-packs .blue-but', function (e) {
        e.preventDefault();
        $('.unlock-packs').hide();
        $('.packs3 input:checkbox').prop('checked', true);
    });
    $body.on('change', '.packs3 input:checkbox', function () {
        if ($(this).prop('checked')) {
            $('.unlock-packs').hide();
        } else {
            $('.unlock-packs').show();
        }
    });

    $(document).on('click', '.cart-plus', function (e) {
        e.preventDefault();
        var val = parseInt($(this).siblings('input.count-opt').val());
        if (val < 10000) {
            $(this).siblings('input.count-opt').val(val + 1).change();
        }
    });
    $(document).on('click', '.cart-minus', function (e) {
        e.preventDefault();
        var val = parseInt($(this).siblings('input.count-opt').val());
        if (val > 1) {
            $(this).siblings('input.count-opt').val(val - 1).change();
        }
    });

    $('.top-modal').on('shown.bs.modal', function () {
        $(this).find('.top-photo-col').height($(this).find('.top-info-col').height() + 35);
    });
    $(document).on('click', '#carthead-but', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('.cart-head').show();
    });
    $body.click(function (e) {
        if (!$(e.target).parents('.nav-head-profile').length && !$(e.target).hasClass('nav-head-profile')) {
            $('.nav-head-profile').removeClass('open');
        }
        if (!$(e.target).parents('.noti-block').length && !$(e.target).hasClass('noti-block')) {
            $('.noti-block').hide();
        }
        if (!$(e.target).parents('.cart-head').length && !$(e.target).hasClass('cart-head')) {
            $('.cart-head').hide();
        }
    });
    $body.on('click', '#how-work, .how-work, .close-how', function (e) {
        e.preventDefault();
        $('.how-works').toggleClass('open-how').slideToggle(300);
    });
    $body.on('click', '.close-ref', function (e) {
        e.preventDefault();
        $('.referral-program').slideToggle(300);
    });

    $body.on('click', '.ref-alias', function (e) {
        e.preventDefault();
        $('.referral-program').slideToggle(300);
    });

    $body.on('click', '.more-filters', function (e) {
        e.preventDefault();
        $('.show-hide-filters').addClass('shf-open');
        $(this).hide();
        $('.hide-filters').show();
    });

    $body.on('click', '.hide-filters', function (e) {
        e.preventDefault();
        $('.show-hide-filters').removeClass('shf-open');
        $(this).hide();
        $('.more-filters').show();
    });
    $body.on("click", 'a.anchor-scroll', function () {
        let link = $(this).attr('href'),
            hash = link.split('#')[1];

        if (hash.length > 0) {
            let object = $('#' + hash);
            if(object.length > 0) {
                $('a.anchor-scroll').removeClass('active');
                $(this).addClass('active');
                $('html, body').stop().animate({
                    scrollTop: object.offset().top - 155
                }, 800);
                return false;
            }
        }
    });
    $("img.lazy-load").one("load", function () {
        $(this).attr('src', $(this).data('src'));
    }).each(function () {
        if (this.complete) {
            $(this).trigger('load');
        }
    });
    $body.on('click', '.more-container a.show-more', function () {
        var self = $(this);
        $(this).slideUp(200, function() {
            self.siblings('.more').slideDown();
        });
        return false;
    });
});
$(window).resize(function () {
    aside_select();
    $('.mfooter').css('height', $footer.height() + 50);
    $footer.css('margin-top', -$footer.height() - 50);
    $('.bf-video-img').height($('.bf-video-img').width() / 1.5);
    if ($(window).width() >= 768) {
        $('.aside-body').show();
    } else {
        $('.aside-body').hide();
    }
    if ($(window).width() >= 768) {
        menumore();
        headerhide();
    } else {
        $('#top-menu ul.nav > li').show();
        $('#top-menu ul.nav > li:last-child').hide();
    }
    if ($(window).width() <= 768) {
        $('.messager.desktop').addClass('mobile').removeClass('desktop');
    } else {
        $('.messager.mobile').addClass('desktop').removeClass('mobile');
    }
});
$(document).ready(function () {
    $(document).on('click', '.social-login a', function () {
        var ww = $(window).width();
        var wh = $(window).height();
        var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no' + ',width=' + ww * 0.8 + ',height=' + wh * 0.9 + ',left=' + ww * 0.1 + ',top=' + wh * 0.05;
        window.open($(this).attr('href'), 'Social login', params);
        return false;
    });
    $('input,textarea').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder')).attr('placeholder', '');
    }).blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var alertTypes = {
    'success': 'success',
    'info': 'information',
    'warning': 'warning',
    'error': 'error',
    'danger': 'error'
};

var alertIcons = {
    'success': 'check',
    'info': 'info',
    'warning': 'info',
    'error': 'times',
    'danger': 'times'
};

function alertCall(layout, type, text) {
    if ($('header > .head-banner').length) {
        $('header > .head-banner').slideUp(400, function () {
            $('header > .head-banner').remove();
            alertCall(layout, type, text);
        });
    } else {
        $('header').prepend(
            '<div class="head-banner ' + alertTypes[type] + '" style="display: none">' +
            '    <div class="container-fluid">' +
            '        <div class="hb-body text-center">' +
            '            <div class="hb-img">' +
            '                <i class="fa fa-' + alertIcons[type] + '"></i>' +
            '            </div>' +
            '            <div class="hb-info">' +
            '                <p>' +
            text +
            '                </p>' +
            '            </div>' +
            '        </div>' +
            '        <a class="close-banner" href="#"></a>' +
            '    </div>' +
            '</div>'
        );
        $('header > .head-banner').slideDown();
    }
}

// (function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build({}, self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build({}, k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
// })(jQuery);

