$(document).on('click', '.crop-button', function(){
    let sourceImg = $(this).closest('.file-preview-frame').find('.kv-file-content img');
    initCropper(sourceImg, 100, 100, $(this).data('ratio'));
});

$(document).on('click', '.crop-bg-button', function(){
    let sourceImg = $(this).closest('.file-preview-frame').find('.kv-file-content img');
    initCropper(sourceImg, 260, 100, 260/100);
});

$(document).on('click', '.crop-wide-button', function(){
    let sourceImg = $(this).closest('.file-preview-frame').find('.kv-file-content img');
    initCropper(sourceImg, 150, 100, 150/100);
});

$(document).on('click', '.crop-confirm', function(){
    var img = $('.crop-container > img');
    var cropData = img.cropper('getData', true);
    $('#' + img.data('id').replace( /\./g, "\\\." ))
        .data('x', cropData.x)
        .data('y', cropData.y)
        .data('w', cropData.width)
        .data('h', cropData.height)
        .data('r', cropData.rotate);
    $().cropper('destroy');
    $('.crop-bg').fadeOut();
});

$(document).on('click', '.crop-close', function(){
    $().cropper('destroy');
    $('.crop-bg').fadeOut();
});

$(document).on('click', '.crop-rotate-left', function(){
    $('.crop-container > img').cropper('clear').cropper('rotate', -90).cropper('crop');
    return false;
});

$(document).on('click', '.crop-rotate-right', function(){
    $('.crop-container > img').cropper('clear').cropper('rotate', 90).cropper('crop');
    return false;
});

$(window).resize(function() {
    $().cropper('destroy');
    $('.crop-bg').hide();
});

/**
 * @param sourceImg
 * @param minWidth
 * @param minHeight
 * @param ratio
 */
function initCropper(sourceImg, minWidth, minHeight, ratio) {
    let container = $('.crop-container');
    container.html(sourceImg.get(0).outerHTML);
    $('.crop-bg').show();
    let img = $('.crop-container > img');
    let imgWidth = img.prop('naturalWidth');
    let imgHeight = img.prop('naturalHeight');
    let containerWidth = container.width();
    let containerHeight = container.height();
    let imgRatio = imgWidth/imgHeight;
    let containerRatio = containerWidth/containerHeight;
    if (imgRatio < containerRatio) {
        containerWidth = containerHeight * imgRatio;
    }
    else {
        containerHeight = containerWidth / imgRatio;
    }
    let minCropBoxWidth = minWidth * containerWidth/imgWidth;
    let minCropBoxHeight = minHeight * containerHeight/imgHeight;
    img.data('id', sourceImg.closest('.file-preview-frame').attr('id')).cropper({
        autoCropArea: 1,
        aspectRatio: ratio,
        viewMode: 1,
        movable: false,
        rotatable: true,
        zoomable: false,
        dragMode: 'crop',
        minCropBoxWidth: minCropBoxWidth,
        minCropBoxHeight: minCropBoxHeight
    });
}