var isPushEnabled = false;
const SERVER_API_SUBSCRIBERS = 'https://ujobs.me/push/ajax/subscribe';

function initialiseState() {
    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
        console.warn('Уведомления не поддерживаются браузером.');
        return;
    }

    if (Notification.permission === 'denied') {
        console.warn('Пользователь запретил прием уведомлений.');
        return;
    }

    if (!('PushManager' in window)) {
        console.warn('Push-сообщения не поддерживаются браузером.');
        return;
    }

    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        serviceWorkerRegistration.pushManager.getSubscription()
            .then(function (subscription) {
                var pushButton = document.querySelector('.js-push-button');
                if(pushButton) {
                    pushButton.disabled = false;

                    if (!subscription) {
                        setTimeout(function () {
                            $('.push-block').addClass('fadeIn');
                        }, 2000);

                        $('body').on('click', '.push-block .push-close', function () {
                            $('.push-block').removeClass('fadeIn');
                        });
                        return;
                    }

                    sendSubscriptionToServer(subscription);

                    pushButton.textContent = 'Отписаться от уведомлений';
                    isPushEnabled = true;
                }
            })
            .catch(function (err) {
                console.warn('Ошибка при получении данных о подписчике.', err);
            });
    });
}

function subscribe() {
    var pushButton = document.querySelector('.js-push-button');
    pushButton.disabled = true;

    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        serviceWorkerRegistration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array('BMuUgP6qOgp0onw4MyAN2vvESGyNC-WPtbfzUgRJ-xMon7ouL3NkL8Mzfwe4oafPxOcOqQLlk6OFcyNbaiZIk2E')
        })
            .then(function (subscription) {
                isPushEnabled = true;
                pushButton.textContent = 'Отписаться от уведомлений';
                pushButton.disabled = false;

                return sendSubscriptionToServer(subscription);
            })
            .catch(function (err) {
                if (Notification.permission === 'denied') {
                    console.warn('Пользователь запретил присылать уведомления');
                    pushButton.disabled = true;
                } else {
                    console.error('Невожможно подписаться, ошибка: ', err);
                    pushButton.disabled = false;
                    pushButton.textContent = 'Получать уведомления';
                }
            });
    });
}

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function unsubscribe() {
    var pushButton = document.querySelector('.js-push-button');
    pushButton.disabled = true;

    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
        serviceWorkerRegistration.pushManager.getSubscription().then(
            function (subscription) {
                if (!subscription) {
                    isPushEnabled = false;
                    pushButton.disabled = false;
                    pushButton.textContent = 'Получать уведомления';
                    return;
                }

                var endpoint = subscription.endpoint;
                // TODO: Отправить серверу данные о подписчике,

                subscription.unsubscribe().then(function (successful) {
                    pushButton.disabled = false;
                    pushButton.textContent = 'Получать уведомления';
                    isPushEnabled = false;
                }).catch(function (err) {
                    // TODO: Если при отмене подписки возникла ошибка,

                    console.log('Хьюстон, у нас проблемы с отменой подписки: ', err);
                    pushButton.disabled = false;
                    pushButton.textContent = 'Получать уведомления';
                });
            }).catch(function (err) {
            console.error('Хьюстон, у нас проблемы с получением данных о подписчике: ', err);
        });
    });
}

function sendSubscriptionToServer(subscription) {
    var req = new XMLHttpRequest();
    var url = SERVER_API_SUBSCRIBERS;
    req.open('POST', url, true);
    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    req.setRequestHeader("Content-type", "application/json");
    req.onreadystatechange = function (e) {
        if (req.readyState == 4) {
            if (req.status != 200) {
                console.error("[SW] Erreur :" + e.target.status);
            }
        }
    };
    req.onerror = function (e) {
        console.error("[SW] Erreur :" + e.target.status);
    };

    var key = subscription.getKey('p256dh');
    var token = subscription.getKey('auth');

    req.send(JSON.stringify({
        'endpoint': getEndpoint(subscription),
        'key': key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
        'token': token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null
    }));

    return true;
}

function getEndpoint(pushSubscription) {
    var endpoint = pushSubscription.endpoint;
    var subscriptionId = pushSubscription.subscriptionId;

    if (subscriptionId && endpoint.indexOf(subscriptionId) === -1) {
        endpoint += '/' + subscriptionId;
    }

    return endpoint;
}

window.addEventListener('load', function () {
    var pushButton = document.querySelector('.js-push-button');
    if(pushButton) {
        pushButton.addEventListener('click', function () {
            if (isPushEnabled) {
                unsubscribe();
            } else {
                subscribe();
            }

            $('.push-block').removeClass('fadeIn');
        });
    }

    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/worker.js')
            .then(initialiseState);
    }
});