function FormatHelper() {
    this.formatPrice = function(i) {
        return i.toString().replace(/(?!^)(?=(?:\d{3})+$)/gm, ' ');
    };
}
const formatter = new FormatHelper();

function SlickConfig() {
    this.getContentPropositionsConfig = function(count) {
        return {
            slidesToShow: count,
            slidesToScroll: 1,
            dots: false,
            centerMode: false,
            focusOnSelect: false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: 'unslick'
                }
            ]
        }
    }
}