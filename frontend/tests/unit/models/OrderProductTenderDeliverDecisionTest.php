<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 09.08.2018
 * Time: 13:30
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;
use common\fixtures\OrderProductProductTender;
use common\fixtures\Product as ProductFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProductProductTender as OrderProductProductTenderFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\AttachmentDecisionForm;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\PriceDecisionForm;
use Yii;

/**
 * Class OrderProductTenderDeliverDecisionTest
 * @package frontend\tests\unit\models
 */
class OrderProductTenderDeliverDecisionTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testProductTenderDeliverAccept()
    {
        $status = Order::STATUS_PRODUCT_TENDER_AWAITING_REVIEW;
        $type = OrderHistory::TYPE_TENDER_REPORT_ACCEPT_HEADER;
        $this->_testService->logTestResult('order-product-tender-deliver-accept', 'Заказ на товар - подтверждение доставки', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt($status),
                't' => SecurityHelper::encrypt($type),
                'content' => 'ph',
                'placeholder' => Yii::t('order', 'You may add a comment'),
                'zxToken' => $formToken,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
            ]);

            expect('tender deliver accept', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-tender-deliver-accept', 'Заказ на товар - подтверждение доставки', Test::STATUS_SUCCESS);
    }

    public function testProductTenderDeliverDeny()
    {
        $this->_testService->logTestResult('order-product-tender-deliver-deny', 'Заказ на товар - с товаром что-то не так', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);
        if($order !== null) {
            $model = new AttachmentDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_ACTIVE),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_PRODUCT_REPORT_DENY_HEADER),
                'placeholder' => Yii::t('order', 'What is wrong with order?'),
                'zxToken' => $formToken,
                'content' => 'fixture something broke',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
            ]);

            expect('tender deliver deny', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-tender-deliver-deny', 'Заказ на товар - с товаром что-то не так', Test::STATUS_SUCCESS);
    }

    // tests

    protected function _before()
    {
        $this->tester->haveFixtures([
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product_tender_contract.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender_product_contract_step5.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_product_tender_delivered.php'
            ],
            'orderProduct' => [
                'class' => OrderProductProductTenderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender_product_contract.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}