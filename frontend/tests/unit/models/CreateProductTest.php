<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 05.07.2018
 * Time: 12:57
 */

namespace frontend\tests\unit\models;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\board\models\frontend\PostProductForm;
use common\modules\board\models\Product;
use Yii;

/**
 * Class CreateProductTest
 * @package frontend\tests\unit\models
 */
class CreateProductTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('create-product-validation', 'Товар - проверка валидаторов', Test::STATUS_FAIL);

        $model = new PostProductForm();
        $model->product = new Product();
        $input = [
            'PostProductForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'lat' => '45',
                'long' => '45',

                'measure_id' => null,
                'currency_code' => null,
                'price' => null,
                'amount' => null,
                'address' => null,
                'shipping_type' => 47936,
                'shipping_info' => null,
                'length' => null,
                'width' => null,
                'height' => null,

                'locale' => null,
            ],
            'DynamicForm' => [
                'attribute_50' =>  [49183],
                'attribute_48' => null, // tags
                'attribute_53' => null, // color
                'attribute_45' => null, // brand
                'attribute_46' => null, // material
            ]
        ];

        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        //expect('Locale', $model->validate(['locale']))->false();
        //expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        //expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Amount must not be empty', $model->validate(['amount']))->false();
        expect('Price must not be empty', $model->validate(['price']))->false();

        $input = array_merge($input, [
            'PostProductForm' => [
                'title' => str_repeat('a', 90),
                'description' => str_repeat('a', 70000),
                'shipping_info' => str_repeat('a', 70000),
                'locale' => 'ka-KA',
                'currency_code' => 'HRY',
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Description is too long', $model->validate(['description']))->false();
        expect('Shipping info is too long', $model->validate(['shipping_info']))->false();
        //expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Wrong currency code', $model->validate(['currency_code']))->false();

        $this->_testService->logTestResult('create-product-validation', 'Товар - проверка валидаторов', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product with validation failing
     */
    public function testCreateWrongProduct()
    {
        $this->_testService->logTestResult('create-wrong-product', 'Товар - создание с ошибками', Test::STATUS_FAIL);

        $model = new PostProductForm();
        $model->product = new Product();
        $model->myLoad([
           'PostProductForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'lat' => '45',
                'long' => '45',

                'measure_id' => null,
                'currency_code' => null,
                'price' => null,
                'amount' => null,
                'address' => null,
                'shipping_type' => 47936,
                'shipping_info' => null,
                'length' => null,
                'width' => null,
                'height' => null,

                'locale' => null,
            ],
            'DynamicForm' => [
                'attribute_50' =>  [49183],
                'attribute_48' => null, // tags
                'attribute_53' => null, // color
                'attribute_45' => null, // brand
                'attribute_46' => null, // material
            ]
        ]);

        expect('Product not saving', $model->save())->false();

        $this->_testService->logTestResult('create-wrong-product', 'Товар - создание с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product without errors
     */
    public function testCreateRightProduct()
    {
        $this->_testService->logTestResult('create-right-product', 'Товар - успешное создание', Test::STATUS_FAIL);

        $model = new PostProductForm();
        $model->product = new Product();
        $input = [
           'PostProductForm' => [
                'title' => 'test product title',
                'description' => 'test product text',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                //'subcategory_id' => null,

                'lat' => '45',
                'long' => '45',

                //'measure_id' => null,
                'currency_code' => 'USD',
                'price' => 300,
                'amount' => 2,
                'address' => 'test product address',
                'shipping_type' => 47936,
                'shipping_info' => 'test product shipping_info',
                'length' => 50,
                'width' => 76,
                'height' => 50,

                'locale' => 'en-GB',
            ],
           'DynamicForm' => [
                'attribute_50' =>  [49183], // condition
                'attribute_48' => 'tag1, tag2, tag3', // tags
                'attribute_53' => 'color1, color2, color3', // color
                'attribute_45' => 'brand1, brand2', // brand
                'attribute_46' => 'material1', // material
           ]
        ];

        $model->myLoad($input);

        expect('Product is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-product', 'Товар - успешное создание', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}