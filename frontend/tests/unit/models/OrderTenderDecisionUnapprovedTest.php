<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.07.2017
 * Time: 16:30
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\DecisionForm;
use Yii;

/**
 * Class OrderTenderDecisionUnapprovedTest
 * @package frontend\tests\unit\models
 */
class OrderTenderDecisionUnapprovedTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testTenderDecisionUnapprovedAccept()
    {
        $this->_testService->logTestResult('order-tender-decision-unapproved-accept', 'Заказ на заявку - подтверждение исполнителя', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_APPROVED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_APPROVED_HEADER),
                'zxToken' => $formToken,
                'content' => 'океюшки test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
            ]);

            expect('tender approve', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-decision-unapproved-accept', 'Заказ на заявку - подтверждение исполнителя', Test::STATUS_SUCCESS);
    }

    public function testTenderDecisionUnapprovedDeny()
    {
        $this->_testService->logTestResult('order-tender-decision-unapproved-deny', 'Заказ на заявку - отказ от исполнителя', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_DENIED_HEADER),
                'zxToken' => $formToken,
                'content' => 'нет test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
            ]);

            expect('tender deny', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-decision-unapproved-deny', 'Заказ на заявку - отказ от исполнителя', Test::STATUS_SUCCESS);
    }

    // tests

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender_contract.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_tender_create2.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}