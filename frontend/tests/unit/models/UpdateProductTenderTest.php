<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.07.2018
 * Time: 13:28
 */

namespace frontend\tests\unit\models;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\board\models\frontend\PostRequestForm;
use common\modules\board\models\Product;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Product as ProductFixture;
use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;
use Yii;

/**
 * Class UpdateProductTenderTest
 * @package frontend\tests\unit\models
 */
class UpdateProductTenderTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('update-product-tender-validation', 'Заявка на товар - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $product = Product::findOne(['price' => 99911]);
        $model = new PostRequestForm();
        $model->loadProduct($product->id);
        $input = [
            'PostRequestForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => null,
                'price' => null,
                'contract_price' => null,
                'amount' => null,

                'locale' => null,
            ]
        ];

        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        //expect('Locale', $model->validate(['locale']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Amount must not be empty', $model->validate(['amount']))->false();

        $input = array_merge($input, [
            'PostRequestForm' => [
                'title' => str_repeat('a', 90),
                'description' => str_repeat('a', 70000),
                'locale' => 'ka-KA',
                'currency_code' => 'HRY',
                'amount' => 0,
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Description is too long', $model->validate(['description']))->false();
        //expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Wrong currency code', $model->validate(['currency_code']))->false();
        expect('Wrong amount', $model->validate(['amount']))->false();

        $this->_testService->logTestResult('update-product-tender-validation', 'Заявка на товар - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product tender with validation failing
     */
    public function testUpdateWrongProductTender()
    {
        $this->_testService->logTestResult('update-wrong-product-tender', 'Заявка на товар - редактирование с ошибками', Test::STATUS_FAIL);

        $product = Product::findOne(['price' => '99911']);
        $model = new PostRequestForm();
        $model->loadProduct($product->id);
        $input = [
            'PostRequestForm' => [
                'title' => '',
                'description' => '',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => '',
                'price' => -50,
                'contract_price' => 0,
                'amount' => 0,

                'locale' => 'ru-RU',
            ]
        ];

        $model->myLoad($input);
        expect('Product tender not saving', $model->save())->false();

        $this->_testService->logTestResult('update-wrong-product-tender', 'Заявка на товар - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product tender without errors
     */
    public function testUpdateRightProductTenderContract()
    {
        $this->_testService->logTestResult('update-right-product-tender-contract', 'Заявка на товар - успешное редактирование по договорной цене', Test::STATUS_FAIL);

        $product = Product::findOne(['price' => '99911']);
        $model = new PostRequestForm();
        $model->loadProduct($product->id);
        $input = [
            'PostRequestForm' => [
                'title' => 'test product title',
                'description' => 'test product text',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => 'USD',
                'price' => null,
                'contract_price' => 1,
                'amount' => 1,

                'locale' => 'en-GB',
            ]
        ];
        $model->myLoad($input);

        expect('Product tender is saving', $model->save())->true();

        $this->_testService->logTestResult('update-right-product-tender-contract', 'Заявка на товар - успешное редактирование по договорной цене', Test::STATUS_SUCCESS);
    }

    public function testUpdateRightProductTenderPrice()
    {
        $this->_testService->logTestResult('update-right-product-tender-price', 'Заявка на товар - успешное редактирование по заданной цене', Test::STATUS_FAIL);

        $product = Product::findOne(['price' => '99911']);
        $model = new PostRequestForm();
        $model->loadProduct($product->id);
        $input = [
            'PostRequestForm' => [
                'title' => 'test product title',
                'description' => 'test product text',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => 'USD',
                'price' => 60,
                'contract_price' => 0,
                'amount' => 1,

                'locale' => 'en-GB',
            ]
        ];
        $model->myLoad($input);

        expect('Product tender is saving', $model->save())->true();

        $this->_testService->logTestResult('update-right-product-tender-price', 'Заявка на товар - успешное редактирование по заданной цене', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product_tender.php'
            ],
            'content_translation_product' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}