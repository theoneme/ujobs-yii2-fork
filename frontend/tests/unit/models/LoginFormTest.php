<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.03.2017
 * Time: 23:20
 */

namespace frontend\tests\unit\models;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use Yii;

/**
 * Class LoginFormTest
 * @package frontend\tests\unit\models
 */
class LoginFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    /**
     * @var TestService
     */
    private $_testService = null;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }

    public function testCorrectLogin()
    {
        $this->_testService->logTestResult('login', 'Авторизация', Test::STATUS_FAIL);

        $model = Yii::createObject(LoginForm::class);
        $model->login = 'test_email_1@gmail.com';
        $model->password = '222222';
        $model->rememberMe = true;

        expect('Login succeed', $model->login())->true();

        $this->_testService->logTestResult('login', 'Авторизация', Test::STATUS_SUCCESS);
    }
}