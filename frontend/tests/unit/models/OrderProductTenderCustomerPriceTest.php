<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.08.2018
 * Time: 16:52
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;
use common\fixtures\OrderProductProductTender;
use common\fixtures\Product as ProductFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProductProductTender as OrderProductProductTenderFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\PriceDecisionForm;
use Yii;

/**
 * Class OrderProductTenderCustomerPriceTest
 * @package frontend\tests\unit\models
 */
class OrderProductTenderCustomerPriceTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testProductTenderCustomerPriceAccept()
    {
        $this->_testService->logTestResult('order-product-tender-customer-price-accept', 'Заказ на товар - подтверждение продавца с договорной ценой', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_APPROVED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_SELLER_APPROVED_HEADER),
                'zxToken' => $formToken,
                'content' => 'ph',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
            ]);

            expect('tender approve', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-tender-customer-price-accept', 'Заказ на товар - подтверждение продавца с договорной ценой', Test::STATUS_SUCCESS);
    }

    public function testProductTenderCustomerPriceDeny()
    {
        $this->_testService->logTestResult('order-product-tender-customer-price-deny', 'Заказ на товар - отказ от продавца с договорной ценой', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_SELLER_DENIED_HEADER),
                'content' => 'ph',
                'zxToken' => $formToken,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY
            ]);

            expect('tender deny', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-tender-customer-price-deny', 'Заказ на товар - отказ от продавца с договорной ценой', Test::STATUS_SUCCESS);
    }

    public function testProductTenderCustomerPriceNewPrice()
    {
        $this->_testService->logTestResult('order-product-tender-customer-price-new-price', 'Заказ на товар - покупатель устанавливает цену', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new PriceDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                'placeholder' => Yii::t('order', 'Type your message here...'),
                'zxToken' => $formToken,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_TENDER_HISTORY,
                'price' => '999998'
            ]);

            expect('new price', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-tender-customer-price-new-price', 'Заказ на товар - покупатель устанавливает цену', Test::STATUS_SUCCESS);
    }

    // tests

    protected function _before()
    {
        $this->tester->haveFixtures([
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product_tender_contract.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender_product_contract.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_product_tender_create2.php'
            ],
            'orderProduct' => [
                'class' => OrderProductProductTenderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender_product_contract.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}