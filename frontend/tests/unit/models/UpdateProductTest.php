<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.07.2018
 * Time: 13:27
 */


use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\board\models\frontend\PostProductForm;
use common\modules\board\models\Product;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Product as ProductFixture;
use common\fixtures\ProductAttribute as ProductAttributeFixture;
use common\fixtures\ModAttributeDescriptionProduct as ModAttributeDescriptionProductFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;

/**
 * Class UpdateProductTest
 * @package frontend\tests\unit\models
 */
class UpdateProductTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        //$this->_testService->logTestResult('update-product-validation', 'Товар - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $product = Product::findOne(['shipping_info' => 'test shipping_info product for fixture']);
        $model = new PostProductForm();
        $model->loadProduct($product->id);
        $input = [
            'PostProductForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'lat' => '45',
                'long' => '45',

                'measure_id' => null,
                'currency_code' => null,
                'price' => null,
                'amount' => null,
                'address' => null,
                'shipping_type' => 47936,
                'shipping_info' => null,
                'length' => null,
                'width' => null,
                'height' => null,

                'locale' => null,
            ],
            'DynamicForm' => [
                'attribute_50' =>  [49183],
                'attribute_48' => null, // tags
                'attribute_53' => null, // color
                'attribute_45' => null, // brand
                'attribute_46' => null, // material
            ]
        ];

        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        //expect('Locale', $model->validate(['locale']))->false();
        //expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        //expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Amount must not be empty', $model->validate(['amount']))->false();
        expect('Price must not be empty', $model->validate(['price']))->false();

        $input = array_merge($input, [
            'PostProductForm' => [
                'title' => str_repeat('a', 90),
                'description' => str_repeat('a', 70000),
                'shipping_info' => str_repeat('a', 70000),
                'locale' => 'ka-KA',
                'currency_code' => 'HRY',
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Description is too long', $model->validate(['description']))->false();
        expect('Shipping info is too long', $model->validate(['shipping_info']))->false();
        //expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Wrong currency code', $model->validate(['currency_code']))->false();

        //$this->_testService->logTestResult('update-product-validation', 'Товар - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product with validation failing
     */
    public function testUpdateWrongProduct()
    {
        //$this->_testService->logTestResult('update-wrong-product', 'Товар - редактирование с ошибками', Test::STATUS_FAIL);

        $product = Product::findOne(['shipping_info' => 'test shipping_info product for fixture']);
        $model = new PostProductForm();
        $model->loadProduct($product->id);
        $model->myLoad([
            'PostProductForm' => [
                'title' => '',
                'description' => '',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'lat' => '45',
                'long' => '45',

                'measure_id' => null,
                'currency_code' => 'USD',
                'price' => -100,
                'amount' => 0,
                'address' => '',
                'shipping_type' => 47936,
                'shipping_info' => '',
                'length' => -1,
                'width' => -1,
                'height' => -1,

                'locale' => 'ka-KA',
            ],
            'DynamicForm' => [
                'attribute_50' =>  [49183],
                'attribute_48' => '', // tags
                'attribute_53' => '', // color
                'attribute_45' => '', // brand
                'attribute_46' => '', // material
            ]
        ]);

        expect('Product not saving', $model->save())->false();

        //$this->_testService->logTestResult('update-wrong-product', 'Товар - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product without errors
     */
    public function testUpdateRightProduct()
    {
        //$this->_testService->logTestResult('update-right-product', 'Товар - успешное редактирование', Test::STATUS_FAIL);

        $product = Product::findOne(['shipping_info' => 'test shipping_info product for fixture']);
        $model = new PostProductForm();
        $model->loadProduct($product->id);

        $input = [
            'PostProductForm' => [
                'title' => 'test product title',
                'description' => 'test product text',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                //'subcategory_id' => null,

                'lat' => '45',
                'long' => '45',

                //'measure_id' => null,
                'currency_code' => 'USD',
                'price' => 300,
                'amount' => 2,
                'address' => 'test product address',
                'shipping_type' => 47936,
                'shipping_info' => 'test product shipping_info',
                'length' => 50,
                'width' => 76,
                'height' => 50,

                'locale' => 'en-GB',
            ],
            'DynamicForm' => [
                'attribute_50' =>  [49183], // condition
                'attribute_48' => 'tag11, tag12, tag13', // tags
                'attribute_53' => 'color1, color2, color3', // color
                'attribute_45' => 'brand1, brand2', // brand
                'attribute_46' => 'material1', // material
            ]
        ];

        $model->myLoad($input);

        expect('Product is saving', $model->save())->true();

        //$this->_testService->logTestResult('update-right-product', 'Товар - успешное редактирование', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product.php'
            ],
            'content_translation_product' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
            'product_attribute' => [
                'class' => ProductAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'product_attribute.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value_product.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionProductFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description_product.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}