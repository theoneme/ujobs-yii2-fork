<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 01.06.2017
 * Time: 14:17
 */


namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Job;
use common\models\user\User;
use common\models\user\User as UserModel;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class EmailNewTendersTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    protected $client;

    public function testSendEmailTender()
    {
        $newTenders = Job::find()
            ->where(['type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка'])
            ->all();
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';

        $this->tester->cleanMessages();


        foreach ($newTenders as $key => $tender) {
            /* @var Job $tender */

            $profile = UserModel::findOne(['email' => 'test_email_1@gmail.com']);

            $price = Html::tag('p', Html::tag('b', 'Цена: ') . ($tender->contract_price ? 'Договорная' : $tender->getPrice()));
            $pricePerMonth = ($tender->type == Job::TYPE_ADVANCED_TENDER && !$tender->contract_price) ? Html::tag('p', Html::tag('b', 'В месяц: ') . $tender->getPrice()) : '';

            $cutContent = preg_replace('/^(([\s\S]*?<\/p>){3})[\s\S]*/u', '$1', $tender->translation->content);
            $cutContent .= $price . $pricePerMonth;

            $content = Yii::$app->view->render("@frontend/views/email/problem-items/custom-message", [
                'text' => $cutContent,
                'link' => Html::a('Смотреть подробности', Url::to([
                    '/tender/view',
                    'alias' => $tender->alias,
                    'utm_source' => 'return_email',
                    'utm_medium' => 'email',
                    'utm_campaign' => 'return_email',
                    'utm_content' => 'new-tender-message',
                    'utm_term' => 'Новая заявка'
                ], true), [
                    'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                    'target' => '_blank'
                ])
            ]);

            expect($mailer->compose(['html' => 'new-tenders'], ['content' => $content, 'user' => $profile, 'tender' => $tender])
                ->setTo($profile->email)
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject('Уведомление о новой заявке')
                ->send())->equals(true);
        }
        expect($this->tester->getTo())->equals('test_email_1@gmail.com');
        expect($this->tester->getFrom())->equals('message@ujobs.me');
        expect($this->tester->getSubject())->equals('Уведомление о новой заявке');
        expect($this->tester->getContent())->contains(Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']));
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
        ]);

        Yii::$app->params = array_merge(
            require(__DIR__ . '/../../../../common/config/params.php'),
            require(__DIR__ . '/../../../../common/config/params-local.php'),
            require(__DIR__ . '/../../../config/params.php'),
            require(__DIR__ . '/../../../config/params-local.php')
        );

        Yii::$app->params['app_currency_code'] = 'RUB';
    }
}