<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 15.05.2017
 * Time: 15:00
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobExtra as JobExtraFixture;
use common\fixtures\Order as OrderFixture;
use common\fixtures\OrderExtra as OrderExtraFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderProduct;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Payment as PaymentFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\RatingForm;
use Yii;

/**
 * Class OrderRatingSellerTest
 * @package frontend\tests\unit\models
 */
class OrderRatingSellerTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'jobExtra' => [
                'class' => JobExtraFixture::class,
                'dataFile' => codecept_data_dir() . 'extra.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_rating_seller.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product.php'
            ],
            'orderExtra' => [
                'class' => OrderExtraFixture::class,
                'dataFile' => codecept_data_dir() . 'order_extra.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }

    public function testSellerRating()
    {
        $this->_testService->logTestResult('order-rating-seller', 'Заказ услуги - рейтинг заказчика', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new RatingForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_COMPLETED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_BUYER),
                'scenario' => RatingForm::SCENARIO_CUSTOMER,
                'zxToken' => $formToken,
                'content' => 'норм работа test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'rating' => '8',
                'communication' => '10',
                'service' => '6',
                'recommend' => '8',
            ]);

            expect('Customer review added', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-rating-seller', 'Заказ услуги - рейтинг заказчика', Test::STATUS_SUCCESS);
    }
}