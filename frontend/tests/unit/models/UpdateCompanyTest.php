<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 04.07.2018
 * Time: 14:44
 */
namespace frontend\tests;

use Codeception\Test\Unit;
use common\fixtures\ContentTranslationCompany as ContentTranslationCompanyFixture;
use common\fixtures\ContentTranslationCompany;
use common\fixtures\Company as CompanyFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Company;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use frontend\modules\company\models\PostCompanyForm;
use Yii;


/**
 * Class UpdateCompanyTest
 * @package frontend\tests\unit\models
 */
class UpdateCompanyTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testUpdateCompanyRight()
    {
        $this->_testService->logTestResult('update-right-company', 'Компания - успешное редактирование', Test::STATUS_FAIL);

        $company = Company::findOne(['status_text' => 'test status_text company for fixture']);
        $model = new PostCompanyForm();
        $model->loadCompany($company->id);
        $input = [
            'PostCompanyForm' => [
                'title' => 'fixture test title',
                'short_description' => 'fixture test short description',
                'description' => 'fixture test description',
                'address' => 'fixture test address',
            ],
        ];
        $model->myLoad($input);
        expect('Company is updating', $model->save())->true();

        $this->_testService->logTestResult('update-right-company', 'Компания - успешное редактирование', Test::STATUS_SUCCESS);
    }

    public function testUpdateCompanyWrong()
    {
        $this->_testService->logTestResult('update-wrong-company', 'Компания - редактирование с ошибками', Test::STATUS_FAIL);

        $company = Company::findOne(['status_text' => 'test status_text company for fixture']);
        $model = new PostCompanyForm();
        $model->loadCompany($company->id);
        $input = [
            'PostCompanyForm' => [
                'title' => '',
                'short_description' => 'fixture test short description',
                'description' => '',
                'address' => 'fixture test address',
            ],
        ];
        $model->myLoad($input);
        expect('Wrong title', $model->validate(['title']))->false();
        expect('Wrong description', $model->validate(['description']))->false();
        expect('Company is not updating', $model->save())->false();

        $this->_testService->logTestResult('update-wrong-company', 'Компания - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('update-company-validation', 'Компания - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $company = Company::findOne(['status_text' => 'test status_text company for fixture']);
        $model = new PostCompanyForm();
        $model->loadCompany($company->id);
        $input = [
            'PostCompanyForm' => [
                'title' => null,
                'short_description' => null,
                'description' => null,
                'address' => null,
                'locale' => null,
            ],
        ];
        $model->myLoad($input);
        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        //expect('Locale', $model->validate(['locale']))->false();

        $input = array_merge($input, [
            'PostCompanyForm' => [
                'title' => str_repeat('a', 90),
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();

        $this->_testService->logTestResult('update-company-validation', 'Компания - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'company' => [
                'class' => CompanyFixture::class,
                'dataFile' => codecept_data_dir() . 'company.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCompanyFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_company.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}