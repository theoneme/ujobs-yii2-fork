<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 04.07.2018
 * Time: 14:07
 */
namespace frontend\tests;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\models\Company;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use frontend\modules\company\models\PostCompanyForm;
use Yii;

/**
 * Class CreateCompanyTest
 * @package frontend\tests\unit\models
 */
class CreateCompanyTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;
    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('create-company-validation', 'Компания - проверка валидаторов', Test::STATUS_FAIL);

        $model = new PostCompanyForm();
        $model->company = new Company();
        $input = [
            'PostCompanyForm' => [
                'title' => null,
                'short_description' => null,
                'description' => null,
                'address' => null,
                'locale' => null,
            ],
        ];
        $model->myLoad($input);
        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        //expect('Locale', $model->validate(['locale']))->false();

        $input = array_merge($input, [
            'PostCompanyForm' => [
                'title' => str_repeat('a', 90),
                //'locale' => 'en-EN',
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        //expect('Locale', $model->validate(['locale']))->true();

        $this->_testService->logTestResult('create-company-validation', 'Компания - проверка валидаторов', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create job with validation failing
     */
    public function testCreateWrongCompany()
    {
        $this->_testService->logTestResult('create-wrong-company', 'Компания - создание с ошибками', Test::STATUS_FAIL);

        $model = new PostCompanyForm();
        $model->company = new Company();
        $input = [
            'PostCompanyForm' => [
                'title' => null,
                'short_description' => null,
                'description' => null,
                'address' => null,
            ],
        ];
        $model->myLoad($input);
        expect('Company not saving', $model->save())->false();


        $this->_testService->logTestResult('create-wrong-company', 'Компания - создание с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create job without errors
     */
    public function testCreateRightCompany()
    {
        $this->_testService->logTestResult('create-right-company', 'Компания - успешное создание', Test::STATUS_FAIL);

        $model = new PostCompanyForm();
        $model->company = new Company();
        $input = [
            'PostCompanyForm' => [
                'title' => 'test title',
                'short_description' => 'test short_description',
                'description' => 'test description',
                'address' => 'test address',
            ],
        ];
        $model->myLoad($input);

        expect('Company is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-company', 'Компания - успешное создание', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}