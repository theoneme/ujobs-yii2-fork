<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 10.08.2018
 * Time: 10:53
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;
use common\fixtures\OrderProductProductTender;
use common\fixtures\Product as ProductFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProductProductTender as OrderProductProductTenderFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use common\fixtures\ProductAttribute as ProductAttributeFixture;
use common\fixtures\ModAttributeDescriptionProduct as ModAttributeDescriptionProductFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use frontend\modules\account\models\history\AttachmentDecisionForm;
use frontend\modules\account\models\history\DecisionForm;
use Yii;

/**
 * Class OrderProductSendDecisionTest
 * @package frontend\tests\unit\models
 */
class OrderProductSendDecisionTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testProductSendDecisionShip()
    {
        $this->_testService->logTestResult('order-product-send-ship', 'Товар - отправка товара', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new AttachmentDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_SENT),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_PRODUCT_SEND_PRODUCT),
                'placeholder' => Yii::t('order', 'Send product and then send message via this form. You can also attach documents about shipping.'),
                'zxToken' => $formToken,
                'content' => 'fixture ship',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_HISTORY
            ]);

            expect('product ship', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-send-ship', 'Товар - отправка товара', Test::STATUS_SUCCESS);
    }

    public function testProductSendDecisionDeny()
    {
        $this->_testService->logTestResult('order-product-send-ship-deny', 'Товар - уточнение деталей доставки', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new AttachmentDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_PRODUCT_SET_SHIPPING_DENY),
                'placeholder' => Yii::t('order', 'Type here, what is wrong with customer`s information.'),
                'content' => 'fixture deny ship',
                'zxToken' => $formToken,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_HISTORY
            ]);

            expect('product deny shipment', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-send-ship-deny', 'Товар - уточнение деталей доставки', Test::STATUS_SUCCESS);
    }

    // tests

    protected function _before()
    {
        $this->tester->haveFixtures([
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
            'product_attribute' => [
                'class' => ProductAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'product_attribute.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value_product.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionProductFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description_product.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_for_product_step2.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_product_shipment.php'
            ],
            'orderProduct' => [
                'class' => OrderProductProductTenderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender_product.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}