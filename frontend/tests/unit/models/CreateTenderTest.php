<?php

namespace frontend\tests\unit\models;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\ModAttributeDescription as ModAttributeDescriptionFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\models\Job;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use frontend\models\PostRequestForm;
use Yii;

/**
 * Class CreateTenderTest
 * @package frontend\tests\unit\models
 */
class CreateTenderTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('create-tender-validation', 'Заявка на услугу - проверка валидаторов', Test::STATUS_FAIL);

        $model = new PostRequestForm(['type' => Job::TYPE_TENDER]);
        $model->job = new Job;
        $input = [
            'PostRequestForm' => [
                'title' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'specialties' => null,
                'description' => null,
                'period_type' => null,
                'date_start' => null,
                'date_end' => null,
                'price' => null,
                'contract_price' => null,
            ],
        ];
        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();

        $input = array_merge($input, [
            'PostRequestForm' => [
                'title' => str_repeat('a', 90),
                'specialties' => str_repeat('spec,', 50),
                'percent_bonus' => 120,
                'period_type' => 'help',
                'date_start' => date('php:d-m-Y', time() - 7200),
            ]
        ]);
        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Too many specialties', $model->validate(['specialties']))->false();
        expect('Too big percent bonus', $model->validate(['percent_bonus']))->false();
        expect('Wrong period type', $model->validate(['period_type']))->false();
//        expect('Date start cannot be lesser than now', $model->validate(['date_start']))->false();

        $this->_testService->logTestResult('create-tender-validation', 'Заявка на услугу - проверка валидаторов', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create tender without errors
     */
    public function testCreateTenderRight()
    {
        $this->_testService->logTestResult('create-right-tender', 'Заявка на услугу - успешное создание', Test::STATUS_FAIL);

        $model = new PostRequestForm(['type' => Job::TYPE_TENDER]);
        $model->job = new Job();
        $model->myLoad([
            'PostRequestForm' => [
                'title' => 'Полив цветов',
                'parent_category_id' => 79,
                'category_id' => 87,
                'type' => 'advanced-tender',
                'specialties' => 'specialties11, specialties22',
                'description' => 'Полив цветов по вторникам и пятницам test Andrew is angry',
                'period_type' => 'start',
                'date_start' => '17-03-2017',
                'date_end' => '',
                'price' => '',
                'contract_price' => 1,
            ],
        ]);

        expect('tender is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-tender', 'Заявка на услугу - успешное создание', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create tender with validation failing
     */
    public function testCreateTenderWrong()
    {
        $this->_testService->logTestResult('create-wrong-tender', 'Заявка на услугу - создание с ошибками', Test::STATUS_FAIL);

        $model = new PostRequestForm(['type' => Job::TYPE_TENDER]);
        $model->job = new Job;
        $input = [
            'PostRequestForm' => [
                'title' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'specialties' => null,
                'description' => null,
                'period_type' => null,
                'date_start' => null,
                'date_end' => null,
                'price' => null,
                'contract_price' => null,
            ],
        ];
        $model->myLoad($input);

        expect('tender is saving', $model->save())->false();

        $this->_testService->logTestResult('create-wrong-tender', 'Заявка на услугу - создание с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        Yii::$app->language = 'ru-RU';
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}