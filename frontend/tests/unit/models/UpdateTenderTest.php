<?php

namespace frontend\tests;


use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobTag as JobTagFixture;
use common\fixtures\ModAttributeDescription as ModAttributeDescriptionFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Job;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use frontend\models\PostRequestForm;
use Yii;

class UpdateTenderTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testUpdateTenderRight()
    {
        $this->_testService->logTestResult('update-right-tender', 'Заявка на услугу - успешное редактирование', Test::STATUS_FAIL);

        $job = Job::findOne(['price' => '999998']);
        $model = new PostRequestForm();
        $model->loadJob($job->id);
        $model->myLoad([
            'PostRequestForm' => [
                'title' => 'Полив цветов',
                'parent_category_id' => '79',
                'category_id' => '87',
                'type' => 'tender',
                'specialties' => 'specialty11, specialty22',
                'description' => 'Полив цветов по вторникам и пятницам test Andrew is angry',
                'period_type' => 'start',
                'date_start' => '17-03-2017',
                'date_end' => '',
                'price' => '',
                'contract_price' => 1,
            ],
        ]);
        expect('tender is updating', $model->save())->true();

        $this->_testService->logTestResult('update-right-tender', 'Заявка на услугу - успешное редактирование', Test::STATUS_SUCCESS);
    }

    public function testUpdateTenderWrong()
    {
        $this->_testService->logTestResult('update-wrong-tender', 'Заявка на услугу - редактирование с ошибками', Test::STATUS_FAIL);

        $job = Job::findOne(['price' => '999998']);
        $model = new PostRequestForm();
        $model->loadJob($job->id);
        $model->myLoad([
            'PostRequestForm' => [
                'title' => '',
                'parent_category_id' => '',
                'category_id' => '',
                'specialties' => 'specialty11, specialty22',
                'description' => '',
                'type' => 'tender',
                'period_type' => 'period',
                'date_start' => '17-03-2017',
                'date_end' => '19-03-2017',
                'price' => '1',
                'contract_price' => 0,
            ],
        ]);
        expect('wrong title', $model->validate(['title']))->false();
        expect('wrong parent_category_id', $model->validate(['parent_category_id']))->false();
        expect('wrong categoryId', $model->validate(['category_id']))->false();
        expect('wrong price', $model->validate(['price']))->false();
        expect('tender is not updating', $model->save())->false();

        $this->_testService->logTestResult('update-wrong-tender', 'Заявка на услугу - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('update-tender-validation', 'Заявка на услугу - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $job = Job::findOne(['price' => '999998']);
        $model = new PostRequestForm();
        $model->loadJob($job->id);

        $input = [
            'PostRequestForm' => [
                'title' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'specialties' => null,
                'description' => null,
                'period_type' => null,
                'date_start' => null,
                'date_end' => null,
                'price' => null,
                'contract_price' => null,
                'currency_code' => null,
                'locale' => null,
            ],
        ];
        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        expect('Currency code must not be empty', $model->validate(['currency_code']))->false();
        expect('Locale must not be empty', $model->validate(['locale']))->false();
//        expect('Price must not be empty', $model->validate(['price']))->false();

        $input = array_merge($input, [
            'PostRequestForm' => [
                'title' => str_repeat('a', 90),
                'specialties' => str_repeat('spec,', 50),
                'percent_bonus' => 120,
                'period_type' => 'help',
            ]
        ]);
        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Too many specialties', $model->validate(['specialties']))->false();
        expect('Too big percent bonus', $model->validate(['percent_bonus']))->false();
        expect('Wrong period type', $model->validate(['period_type']))->false();

        $this->_testService->logTestResult('update-tender-validation', 'Заявка на услугу - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description.php'
            ],
            'tag' => [
                'class' => JobTagFixture::class,
                'dataFile' => codecept_data_dir() . 'specialty.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}