<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 19.07.2018
 * Time: 13:07
 */


namespace frontend\tests;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\models\UserPortfolio;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Portfolio as PortfolioFixture;
use common\fixtures\PortfolioAttribute as PortfolioAttributeFixture;
use common\fixtures\ModAttributeDescriptionPortfolio as ModAttributeDescriptionPortfolioFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use frontend\modules\account\models\PostPortfolioForm;
use Yii;

/**
 * Class UpadatePortfolioTest
 * @package frontend\tests\unit\models
 */
class UpdatePortfolioTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('update-portfolio-validation', 'Портфолио - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $portfolio = UserPortfolio::findOne(['title' => 'test title portfolio for fixture']);
        $model = new PostPortfolioForm();
        $model->loadPortfolio($portfolio->id);
        $input = [
            'PostPortfolioForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'locale' => null,
            ],
        ];
        $model->myLoad($input);
        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        expect('Locale', $model->validate(['locale']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();

        $input = array_merge($input, [
            'PostPortfolioForm' => [
                'title' => str_repeat('a', 90),
                'tags' => str_repeat('tag,', 70),
                'locale' => 'en-EN',
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Too many tags', $model->validate(['tags']))->false();
        expect('Wrong locale', $model->validate(['locale']))->false();

        $this->_testService->logTestResult('update-portfolio-validation', 'Портфолио - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create Portfolio with validation failing
     */
    public function testCreateWrongPortfolio()
    {
        $this->_testService->logTestResult('update-wrong-portfolio', 'Портфолио - редактирование с ошибками', Test::STATUS_FAIL);

        $portfolio = UserPortfolio::findOne(['title' => 'test title portfolio for fixture']);
        $model = new PostPortfolioForm();
        $model->loadPortfolio($portfolio->id);
        $input = [
            'PostPortfolioForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => null,
                'category_id' => null,
            ],
        ];
        $model->myLoad($input);
        expect('Portfolio not saving', $model->save())->false();

        $this->_testService->logTestResult('update-wrong-portfolio', 'Портфолио - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create Portfolio without errors
     */
    public function testCreateRightPortfolio()
    {
        $this->_testService->logTestResult('update-right-portfolio', 'Портфолио - успешное редактирование', Test::STATUS_FAIL);

        $portfolio = UserPortfolio::findOne(['title' => 'test title portfolio for fixture']);
        $model = new PostPortfolioForm();
        $model->loadPortfolio($portfolio->id);
        $input = [
            'PostPortfolioForm' => [
                'title' => 'test portfolio title',
                'description' => 'test portfolio text',
                'parent_category_id' => 79,
                'category_id' => 80,
            ],
        ];
        $model->myLoad($input);

        expect('Portfolio is saving', $model->save())->true();

        $this->_testService->logTestResult('update-right-portfolio', 'Портфолио - успешное редактирование', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'portfolio' => [
                'class' => PortfolioFixture::class,
                'dataFile' => codecept_data_dir() . 'portfolio.php'
            ],
            'portfolio_attribute' => [
                'class' => PortfolioAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'portfolio_attribute.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value_portfolio.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionPortfolioFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description_portfolio.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}