<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 15.05.2017
 * Time: 16:29
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobTag as JobTagFixture;
use common\fixtures\ModAttributeDescription as ModAttributeDescriptionFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Job;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use frontend\models\TenderResponseForm;
use Yii;

/**
 * Class TenderResponseTest
 * @package frontend\tests\unit\models
 */
class TenderResponseTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testResponseAdvancedTender()
    {
        $this->_testService->logTestResult('response-advanced-tender', 'Отклик на заявку - долгосрочная работа', Test::STATUS_FAIL);

        $tenderResponseForm = new TenderResponseForm();
        $tenderResponseForm->load([
            'TenderResponseForm' => [
                'content' => 'Хочу выполнять эту работу тест',
            ],
        ]);
        $model = Job::findOne(['alias' => 'zxc-test-tender-1', 'type' => Job::TYPE_ADVANCED_TENDER]);

        $comment = $tenderResponseForm->content;
        $order = new Order([
            'total' => $model->price,
            'customComment' => $comment,
            'is_price_approved' => 0,
            'product_type' => Order::TYPE_TENDER,
            'cartItems' => [$model],
            'seller_id' => userId(),
            'customer_id' => $model->user_id,
            'isBlameable' => false,
            'status' => Order::STATUS_TENDER_UNAPPROVED
        ]);
        $order->save();

        expect('respond on advanced tender', $order->save())->true();

        $this->_testService->logTestResult('response-advanced-tender', 'Отклик на заявку - долгосрочная работа', Test::STATUS_SUCCESS);
    }

    public function testResponseTender()
    {
        $this->_testService->logTestResult('response-tender', 'Отклик на заявку - одноразовая работа', Test::STATUS_FAIL);

        $tenderResponseForm = new TenderResponseForm();
        $tenderResponseForm->load([
            'TenderResponseForm' => [
                'price' => '123456',
                'content' => 'Хочу выполнять эту работу тест',
            ],
        ]);
        $model = Job::findOne(['alias' => 'zxc-test-tender-2', 'type' => Job::TYPE_TENDER]);

        $price = $tenderResponseForm->price;
        $comment = $tenderResponseForm->content;

        $order = new Order([
            'total' => $price,
            'customComment' => $comment,
            'is_price_approved' => 1,
            'product_type' => Order::TYPE_TENDER,
            'cartItems' => [$model],
            'seller_id' => userId(),
            'customer_id' => $model->user_id,
            'isBlameable' => false,
            'status' => Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION
        ]);

        expect('respond on tender', $order->save())->true();

        $this->_testService->logTestResult('response-tender', 'Отклик на заявку - одноразовая работа', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'tag' => [
                'class' => JobTagFixture::class,
                'dataFile' => codecept_data_dir() . 'specialty.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}