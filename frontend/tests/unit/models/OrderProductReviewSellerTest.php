<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 10.08.2018
 * Time: 13:33
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;
use common\fixtures\OrderProductProductTender;
use common\fixtures\Product as ProductFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProductProductTender as OrderProductProductTenderFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use common\fixtures\ProductAttribute as ProductAttributeFixture;
use common\fixtures\ModAttributeDescriptionProduct as ModAttributeDescriptionProductFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\RatingForm;
use Yii;

/**
 * Class OrderProductRevieSellerTest
 * @package frontend\tests\unit\models
 */
class OrderProductRevieSellerTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testProductReviewSeller()
    {
        $this->_testService->logTestResult('order-product-review-seller', 'Товар - отзыв продавца', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new RatingForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_PRODUCT_COMPLETED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_BUYER),
                'scenario' => RatingForm::SCENARIO_CUSTOMER,
                'content' => 'fixture seller comment',
                'zxToken' => $formToken,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_PRODUCT_HISTORY
            ]);

            expect('product review seller', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-product-review-seller', 'Товар - отзыв продавца', Test::STATUS_SUCCESS);
    }

    // tests

    protected function _before()
    {
        $this->tester->haveFixtures([
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
            'product_attribute' => [
                'class' => ProductAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'product_attribute.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value_product.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionProductFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description_product.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_for_product_step5.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_product_review_customer.php'
            ],
            'orderProduct' => [
                'class' => OrderProductProductTenderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender_product.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}