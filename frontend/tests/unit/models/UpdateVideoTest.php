<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.07.2018
 * Time: 16:24
 */


use Codeception\Test\Unit;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Video as VideoFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\models\Video;
use frontend\models\PostVideoForm;
//use Yii;

/**
 * Class CreateVideoTest
 * @package frontend\tests\unit\models
 */
class CreateVideoTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('update-video-validation', 'Видео - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $video = Video::findOne(['title' => 'test title for fixture video']);
        $model = new PostVideoForm();
        $model->loadVideo($video->id);
        $input = [
            'PostVideoForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => null,
                'category_id' => null,
                //'locale' => null,
                'currency_code' => 'USD',
                'price' => null,
                'free' => null,
            ],
        ];
        $model->myLoad($input);
        expect('Title must not be empty', $model->validate(['title']))->false();
        //expect('Locale', $model->validate(['locale']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Price must not be empty', $model->validate(['price']))->false();


        $input = array_merge($input, [
            'PostVideoForm' => [
                'title' => str_repeat('a', 90),
                //'locale' => 'en-EN',
                'price' => 'asd',
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        //expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Wrong price', $model->validate(['price']))->false();

        $this->_testService->logTestResult('update-video-validation', 'Видео - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create Video with validation failing
     */
    public function testCreateWrongVideo()
    {
        $this->_testService->logTestResult('update-wrong-video', 'Видео - редактирование с ошибками', Test::STATUS_FAIL);

        $video = Video::findOne(['title' => 'test title for fixture video']);
        $model = new PostVideoForm();
        $model->loadVideo($video->id);
        $input = [
            'PostVideoForm' => [
                'title' => '',
                'description' => '',
                'parent_category_id' => '',
                'category_id' => '',
                'currency_code' => 'USD',
                'locale' => 'en-GB',
                'price' => '',
                'free' => 1,
            ],
        ];
        $model->myLoad($input);
        expect('Video not saving', $model->save())->false();

        $this->_testService->logTestResult('update-wrong-video', 'Видео - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create Video without errors
     */
    public function testCreateRightVideo()
    {
        $this->_testService->logTestResult('update-right-video', 'Видео - успешное редактирование', Test::STATUS_FAIL);

        $video = Video::findOne(['title' => 'test title for fixture video']);
        $model = new PostVideoForm();
        $model->loadVideo($video->id);
        $input = [
            'PostVideoForm' => [
                'title' => 'test video title',
                'description' => 'test video text',
                'parent_category_id' => 79,
                'category_id' => 80,
                'currency_code' => 'USD',
                'locale' => 'en-GB',
                'price' => 500,
                'free' => 0,
            ],
        ];
        $model->myLoad($input);

        expect('Right title', $model->validate(['title']))->true();
        expect('Right description', $model->validate(['title']))->true();
        expect('Right Locale', $model->validate(['locale']))->true();
        expect('Right parent category', $model->validate(['parent_category_id']))->true();
        expect('Right category', $model->validate(['category_id']))->true();
        expect('Right currency code', $model->validate(['currency_code']))->true();
        expect('Right price', $model->validate(['price']))->true();
        expect('Right free', $model->validate(['free']))->true();
        //expect('Video is saving', $model->save())->true();

        $this->_testService->logTestResult('update-right-video', 'Видео - успешное редактирование', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'video' => [
                'class' => VideoFixture::class,
                'dataFile' => codecept_data_dir() . 'video.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}