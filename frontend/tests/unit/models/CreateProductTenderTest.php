<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.07.2018
 * Time: 12:48
 */

namespace frontend\tests\unit\models;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\board\models\frontend\PostRequestForm;
use common\modules\board\models\Product;
use Yii;

/**
 * Class CreateProductTenderTest
 * @package frontend\tests\unit\models
 */
class CreateProductTenderTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('create-product-tender-validation', 'Заявка на товар - проверка валидаторов', Test::STATUS_FAIL);

        $model = new PostRequestForm();
        $model->product = new Product();
        $input = [
            'PostRequestForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => null,
                'price' => null,
                'contract_price' => null,
                'amount' => null,

                'locale' => null,
            ]
        ];

        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        //expect('Locale', $model->validate(['locale']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Amount must not be empty', $model->validate(['amount']))->false();

        $input = array_merge($input, [
            'PostRequestForm' => [
                'title' => str_repeat('a', 90),
                'description' => str_repeat('a', 70000),
                'locale' => 'ka-KA',
                'currency_code' => 'HRY',
                'amount' => 0,
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Description is too long', $model->validate(['description']))->false();
        //expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Wrong currency code', $model->validate(['currency_code']))->false();
        expect('Wrong amount', $model->validate(['amount']))->false();

        $this->_testService->logTestResult('create-product-tender-validation', 'Заявка на товар - проверка валидаторов', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product tender with validation failing
     */
    public function testCreateWrongProductTender()
    {
        $this->_testService->logTestResult('create-wrong-product-tender', 'Заявка на товар - создание с ошибками', Test::STATUS_FAIL);

        $model = new PostRequestForm();
        $model->product = new Product();
        $input = [
            'PostRequestForm' => [
                'title' => null,
                'description' => null,
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => null,
                'price' => null,
                'contract_price' => null,
                'amount' => null,

                'locale' => null,
            ]
        ];

        $model->myLoad($input);
        expect('Product tender not saving', $model->save())->false();

        $this->_testService->logTestResult('create-wrong-product-tender', 'Заявка на товар - создание с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create product tender without errors
     */
    public function testCreateRightProductTenderContract()
    {
        $this->_testService->logTestResult('create-right-product-tender-contract', 'Заявка на товар - успешное создание по договорной цене', Test::STATUS_FAIL);

        $model = new PostRequestForm();
        $model->product = new Product();
        $input = [
            'PostRequestForm' => [
                'title' => 'test product title',
                'description' => 'test product text',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => 'USD',
                'price' => null,
                'contract_price' => 1,
                'amount' => 1,

                'locale' => 'en-GB',
            ]
        ];
        $model->myLoad($input);

        expect('Product tender is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-product-tender-contract', 'Заявка на товар - успешное создание по договорной цене', Test::STATUS_SUCCESS);
    }

    public function testCreateRightProductTenderPrice()
    {
        $this->_testService->logTestResult('create-right-product-tender-price', 'Заявка на товар - успешное создание по заданной цене', Test::STATUS_FAIL);

        $model = new PostRequestForm();
        $model->product = new Product();
        $input = [
            'PostRequestForm' => [
                'title' => 'test product title',
                'description' => 'test product text',
                'parent_category_id' => 2455,
                'category_id' => 2458,
                'subcategory_id' => null,

                'measure_id' => null,
                'currency_code' => 'USD',
                'price' => 60,
                'contract_price' => 0,
                'amount' => 1,

                'locale' => 'en-GB',
            ]
        ];
        $model->myLoad($input);

        expect('Product tender is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-product-tender-price', 'Заявка на товар - успешное создание по заданной цене', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}