<?php

namespace frontend\tests\unit\models;

use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\User as UserFixture;
use common\models\user\RegisterForm;
use common\models\Test;
use common\services\TestService;

/**
 * Class SignupFormTest
 * @package frontend\tests\unit\models
 */
class SignupFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function _before()
    {
        $this->tester->haveFixtures([
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }

    public function testValidation()
    {
        $this->_testService->logTestResult('sign-up-validation', 'Регистрация - валидация формы', Test::STATUS_FAIL);

        $model = new RegisterForm([
            'login' => 'some_email@example.com',
            'username' => 'some_username',
            'password' => 'sa',
        ]);
        expect('Password is short', $model->validate(['password']))->false();

        $model->password = '';
        expect('Password is empty', $model->validate(['password']))->false();

        $model->login = '';
        expect('Login is empty', $model->validate(['login']))->false();

        $this->_testService->logTestResult('sign-up-validation', 'Регистрация - валидация формы', Test::STATUS_SUCCESS);
    }

    public function testCorrectSignup()
    {
        $this->_testService->logTestResult('sign-up-correct', 'Регистрация - успешная регистрация', Test::STATUS_FAIL);

        $model = new RegisterForm([
            'login' => 'some_email@example.com',
            'username' => 'some_username',
            'password' => 'some_password',
        ]);
        expect('Register by email succeed', $model->register())->true();

        $model = new RegisterForm([
            'login' => '+380951234567',
            'username' => 'some_username_2',
            'password' => 'some_password',
        ]);
        expect('Register by phone succeed', $model->register())->true();

        $this->_testService->logTestResult('sign-up-correct', 'Регистрация - успешная регистрация', Test::STATUS_SUCCESS);
    }

    public function testIncorrectSignup()
    {
        $this->_testService->logTestResult('sign-up-incorrect', 'Регистрация - неправильно заполненная форма', Test::STATUS_FAIL);

        $model = new RegisterForm([
            'login' => '+380954150093',
            'username' => '',
            'password' => '',
        ]);

        expect('Register failed', $model->register())->false();

        $this->_testService->logTestResult('sign-up-incorrect', 'Регистрация - неправильно заполненная форма', Test::STATUS_SUCCESS);
    }
}
