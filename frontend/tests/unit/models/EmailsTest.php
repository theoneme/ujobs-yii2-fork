<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 22.05.2017
 * Time: 14:28
 */

namespace frontend\tests\unit\models;

use common\fixtures\Cart as CartFixture;
use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Freelancer as FreelancerFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobPackage as JobPackageFixture;
use common\fixtures\Order as OrderFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Payment as PaymentFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\EmailNotification;
use common\models\Event;
use common\models\Freelancer;
use common\models\Job;
use common\models\Job as JobModel;
use common\models\JobPackage as JobPacModel;
use common\models\Referral;
use common\models\user\LoginForm;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\store\models\Order;
use frontend\modules\account\models\JobSearch;
use Yii;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;


class EmailsTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    protected $client;

    public function testSendEmailFreelancers()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $freelancer = Freelancer::find()->where(['email' => 'test_email@gmail.com'])->one();
        $jobs = Job::find()->joinWith(['specialties'])->where([
            'type' => Job::TYPE_ADVANCED_TENDER,
            'status' => Job::STATUS_ACTIVE,
        ])->limit(4)->orderBy('id asc')->all();
        $view = 'freelancers';
        $this->tester->cleanMessages();

        expect($mailer->compose(['html' => $view], [
            'name' => $freelancer->name,
            'specialty' => 'SEO-специалист',
            'jobs' => $jobs
        ])
            ->setTo($freelancer->email)
            ->setFrom(Yii::$app->params['ujobsNoty'])
            ->setSubject('Вакансии на сайте ' . Yii::$app->name)
            ->send())->equals(true);

        expect($this->tester->getTo())->equals('test_email@gmail.com');
        expect($this->tester->getFrom())->equals('message@ujobs.me');
        expect($this->tester->getSubject())->equals('Вакансии на сайте ' . Yii::$app->name);
        expect($this->tester->getContent())->contains('Вас интересует работа по специальности');
    }

    public function testSendEmailAfterOrder()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $view = 'after-order';

        $this->tester->cleanMessages();

        $time = time();
        $orders = Order::find()
            ->joinWith(['customer'])
            ->joinWith(['customer.profile'])
            ->andWhere(['user.email' => 'test_email_1@gmail.com'])
            ->andWhere(['profile.email_notifications' => true])
            ->andWhere(['product_type' => Order::TYPE_JOB_PACKAGE])
            ->andWhere(['order.status' => Order::STATUS_COMPLETED])
            ->limit(10)
            ->groupBy('customer_id')->all();
        Yii::$app->db->createCommand()->batchInsert(
            'event',
            ['user_id', 'code', 'created_at'],
            array_map(function ($o) use ($time) {
                return [$o->customer_id, Event::AFTER_ORDER_MESSAGE, $time];
            }, $orders)
        )->execute();
        Event::deleteAll(['and',
            ['code' => Event::AFTER_ORDER_MESSAGE],
            ['<', 'created_at', $time - 60 * 60 * 24 * 7 * 2]
        ]);
        foreach ($orders as $order) {
            $jobs = Job::find()->where([
                'type' => Job::TYPE_JOB,
                'status' => Job::STATUS_ACTIVE,
                'category_id' => $order->product->job->category_id,
            ])->limit(4)->all();

            if (!empty($jobs)) {
                expect($mailer->compose(['html' => $view], [
                    'name' => $order->customer->profile->getSellerName(),
                    'jobName' => $order->product->getLabel(),
                    'jobs' => $jobs
                ])
                    ->setTo($order->customer->email)
                    ->setFrom(Yii::$app->params['ujobsNoty'])
                    ->setSubject('Интересные предложения на сайте ' . Yii::$app->name)
                    ->send())->equals(true);
            }

            expect($this->tester->getTo())->equals('test_email_1@gmail.com');
            expect($this->tester->getFrom())->equals('message@ujobs.me');
            expect($this->tester->getSubject())->equals('Интересные предложения на сайте ' . Yii::$app->name);
            expect($this->tester->getContent())->contains('Спасибо, что воспользовались нашим сервисом. Мы подобрали подобные услуги, которые могут вас заинтересовать:');
        }
    }

    public function testSendEmailCartReturn()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $view = 'cart-return';

        $this->tester->cleanMessages();

        $time = time();
        $users = User::find()
            ->joinWith(['profile'])
            ->andWhere(['user.email' => 'test_email_1@gmail.com'])
            ->andWhere(['profile.email_notifications' => true])
            ->andWhere(['exists', (new Query())
                ->select('sessionId')
                ->from('cart')
                ->where('cart.sessionId=user.id')
                ->andWhere(['between', 'updated_at', $time - 60 * 90, $time - 60 * 30])
            ])
            ->all();
        Yii::$app->db->createCommand()->batchInsert(
            'event',
            ['user_id', 'code', 'created_at'],
            array_map(function ($u) use ($time) {
                return [$u->id, Event::CART_RETURN_MESSAGE, $time];
            }, $users)
        )->execute();
        foreach ($users as $user) {
            $job = JobModel::findOne(['requirements' => "Заранее планируется расписание уборки test 5 лет опыта Пашка"]);
            $jobId = $job->id;
            $jobPac = JobPacModel::findOne(['job_id' => $jobId]);
            $jobPacId = $jobPac->id;
            $cart = serialize([
                $jobPacId => [
                    'item' => JobPacModel::findOne($jobPacId),
                    'quantity' => 1,
                    'options' => []
                ]
            ]);
            $cart = unserialize($cart);

            $jobs = implode(',', array_map(function ($item) {
                return '"' . $item['item']->getLabel() . '"';
            }, $cart));
            expect($mailer->compose(['html' => $view], ['user' => $user, 'jobs' => $jobs])
                ->setTo($user->email)
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject('Ваша корзина на сайте ' . Yii::$app->name)
                ->send())->equals(true);
        }
        expect($this->tester->getTo())->equals('test_email_1@gmail.com');
        expect($this->tester->getFrom())->equals('message@ujobs.me');
        expect($this->tester->getSubject())->equals('Ваша корзина на сайте ' . Yii::$app->name);
        expect($this->tester->getContent())->contains('В вашей корзине остались не оплаченные услуги. Вы хотели купить:');
    }

    public function testSendEmailNoInfo()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';

        $this->tester->cleanMessages();
        $profiles = User::find()->select('user.*')
            ->addSelect([
                    'jobsCount' => (new Query)
                        ->select("count('job.id')")
                        ->from('job')
                        ->where('job.user_id =  user.id')
                        ->andWhere([
                            'job.type' => Job::TYPE_JOB,
                        ])
                ]
            )
            ->joinWith(['profile', 'lastNotification'])
            ->where(['or',
                ['profile.bio' => ''],
                ['profile.gravatar_email' => ''],
                ['not exists', (new Query())
                    ->select('job.id')
                    ->from('job')
                    ->where('job.user_id = user.id')
                    ->andWhere(['job.type' => Job::TYPE_JOB])
                ],
                ['exists', (new Query())
                    ->select('j2.id')
                    ->from('job j2')
                    ->where('j2.user_id = user.id')
                    ->andWhere(['not exists', (new Query())
                        ->select('at3.id')
                        ->from('attachment at3')
                        ->where('at3.entity_id = j2.id')
                        ->andWhere(['at3.entity' => Job::TYPE_JOB])
                    ])
                    ->andWhere(['j2.type' => Job::TYPE_JOB])
                ],
            ])
            ->andWhere(['not exists', (new Query())
                ->select('id')
                ->from('email_notification en')
                ->where('to_id = user.id')
                ->andWhere(['>', 'en.created_at', time() - 60 * 60 * 24 * 5])
            ])
            ->andWhere(['in', 'profile.status', [Profile::STATUS_REQUIRES_MODIFICATION]])
            ->andWhere(['profile.email_notifications' => true])
            ->andWhere(['not', ['user.email' => '']])
            //->andWhere(['not', ['user.email' => 'test_email_1@gmail.com']])
            ->andWhere(['user.id' => 2245])
            ->orderBy('created_at asc')
            ->all();
        $hasPhoto = true;
        $hasBio = true;
        $hasBioAndPhoto = true;
        $hasJobsWithoutPhotos = true;
        $hasJobs = true;
        foreach ($profiles as $k => $profile) {
            $problems = '';

            if ($profile->profile->bio == '') {
                $hasBio = false;
            }

            if ($profile->profile->gravatar_email == '') {
                $hasPhoto = false;
            }

            if ($profile->profile->gravatar_email == '' && $profile->profile->bio == '') {
                $hasBioAndPhoto = false;
            }

            if ($profile->jobsCount == 0) {
                $hasJobs = false;
            }

            if ($jobsWithoutAttachments = Job::find()->where([
                'user_id' => $profile->id,
                'type' => Job::TYPE_JOB
            ])->andWhere(['not exists', (new Query())
                ->select('at3.id')
                ->from('attachment at3')
                ->where('at3.entity_id = job.id')
                ->andWhere(['at3.entity' => Job::TYPE_JOB])
            ])->all()
            ) {
                $hasJobsWithoutPhotos = false;
            }

            if ($hasBioAndPhoto == false) {

                $problems .= Yii::$app->view->render('@frontend/views/email/problem-items/common', [
                    'text' => Yii::t('notifications', 'It seems your photo and profile description are not set. Please fix this!'),
                ]);
            } elseif ($hasBio == false || $hasPhoto == false) {
                if ($hasPhoto == false) {
                    $problems .= Yii::$app->view->render('@frontend/views/email/problem-items/common', [
                        'text' => Yii::t('notifications', 'It seems your profile photo is not set. Please fix this!')
                    ]);
                }
                if ($hasBio == false) {
                    $problems .= Yii::$app->view->render('@frontend/views/email/problem-items/common', [
                        'text' => Yii::t('notifications', 'It seems your profile description is not set. Please fix this!')
                    ]);
                }
            }

            if ($hasJobs == false) {
                $problems .= Yii::$app->view->render('@frontend/views/email/problem-items/common', [
                    'text' => Yii::t('notifications', 'You still have not added jobs. You can do it now!'),

                ]);
            }

            if ($hasJobsWithoutPhotos == false) {
                $jobs = Job::find()->joinWith(['translation'])
                    ->where([
                        'user_id' => $profile->id,
                        'type' => Job::TYPE_JOB
                    ])
                    ->andWhere(['not exists', (new Query())
                        ->select('at3.id')
                        ->from('attachment at3')
                        ->where('at3.entity_id = job.id')
                        ->andWhere(['at3.entity' => Job::TYPE_JOB])
                    ])
                    ->all();

                $problems .= Yii::$app->view->render('@frontend/views/email/problem-items/no-attachments', [
                    'text' => Yii::t('notifications', 'These jobs are missing their images. Please, add them!'),
                    'jobs' => $jobs,
                ]);
            }

            $problems .= Yii::$app->view->render('@frontend/views/email/problem-items/link-button', [
                'link' => Html::a(Yii::t('notifications', 'Visit profile settings'), Url::to(['/account/job/list', 'status' => JobSearch::STATUS_ALL], true), [
                    'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                    'target' => '_blank'
                ]),
            ]);

            expect($mailer->compose(['html' => 'problems'], ['content' => $problems, 'user' => $profile])
                ->setTo($profile->email)
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'Recommendations about your profile', ['site' => Yii::$app->name]))
                ->send())->equals(true);

            expect($this->tester->getTo())->equals('uJobs.me@yandex.ru');
            expect($this->tester->getFrom())->equals('message@ujobs.me');
            expect($this->tester->getSubject())->equals(Yii::t('notifications', 'Recommendations about your profile', ['site' => Yii::$app->name]));
            expect($this->tester->getContent())->contains(Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']));
        }
    }

    public function testSendEmailStartSelling()
    {
        $this->tester->cleanMessages();

        $profile = Profile::find()->with('user')->where(['message2_flag' => false, 'public_email' => 'test_email_2@gmail.com'])->one();
        Profile::updateAll(['message2_flag' => true]);
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $jobs = Job::find()->joinWith(['translation'])->where(['job.type' => 'job', 'status' => Job::STATUS_ACTIVE])->limit(4)->orderBy('created_at desc')->all();

        /* @var $profile Profile */
        if (!empty($profile->user->email)) {
            expect($mailer->compose(['html' => 'start-selling', 'text' => 'text/' . 'start-selling'], ['user' => $profile->user, 'jobs' => $jobs])
                ->setTo($profile->user->email)
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'You are welcome on {site}', ['site' => Yii::$app->name]))
                ->send())->equals(true);
        }

        expect($this->tester->getTo())->equals('test_email_2@gmail.com');
        expect($this->tester->getFrom())->equals('message@ujobs.me');
        expect($this->tester->getSubject())->equals(Yii::t('notifications', 'You are welcome on {site}', ['site' => Yii::$app->name]));
        expect($this->tester->getContent())->contains(Yii::t('notifications', "You have just become part of the largest, most affordable and easiest to use marketplace for services and products!"));
    }

    public function testSendEmailReferral()
    {
        $this->tester->cleanMessages();

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
//        $modelLogin->login = 'devouryo@gmail.com';
//        $modelLogin->password = 'QF3nRuyF71UXoh6cKh0n';
        $modelLogin->login();

        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $email = trim('test_email_3@gmail.com');
        $referral = new Referral();
        $referral->email = $email;
        $referral->created_by = Yii::$app->user->identity->getId();
        $referral->save();
        expect($mailer->compose(['html' => 'referral'], ['user' => Yii::$app->user->identity, 'language' => 'ru-RU'])
            ->setTo($email)
            ->setFrom(Yii::$app->params['ujobsNoty'])
            ->setSubject(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]))
            ->send())->equals(true);
        expect($this->tester->getTo())->equals('test_email_3@gmail.com');
        expect($this->tester->getFrom())->equals('message@ujobs.me');
        expect($this->tester->getSubject())->equals(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]));
        expect($this->tester->getContent())->contains(Yii::t('notifications', 'uJobs is an online marketplace for services where you can find <br>everything you need, and in unrivaled quality.'));
    }

    public function testSendEmailCustom()
    {
        $this->tester->cleanMessages();

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
//        $modelLogin->login = 'devouryo@gmail.com';
//        $modelLogin->password = 'QF3nRuyF71UXoh6cKh0n';
        $modelLogin->login();

        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';

        $emailNotification = new EmailNotification();
        $emailNotification->to_id = Yii::$app->user->identity->getId();
        $emailNotification->type = EmailNotification::TYPE_PROFILE_PHOTO_MISSING;
        $emailNotification->save();

        /* $mailer->compose(['html' => 'referral'])
             ->setTo()
             ->setFrom(['support@ujobs.me' => 'Команда uJobs'])
             ->setSubject(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]))
             ->send();
         expect($this->tester->getTo())->equals('test_email_2@gmail.com');
         expect($this->tester->getFrom())->equals('support@ujobs.me');
         expect($this->tester->getSubject())->equals(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]));
         expect($this->tester->getContent())->contains(Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']));*/
    }

    protected function _before()
    {
        Yii::$app->language = 'ru-RU';
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'package' => [
                'class' => JobPackageFixture::class,
                'dataFile' => codecept_data_dir() . 'package.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_rating_customer.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product.php'
            ],
            'freelancer' => [
                'class' => FreelancerFixture::class,
                'dataFile' => codecept_data_dir() . 'freelancer.php'
            ],
            'cart' => [
                'class' => CartFixture::class,
                'dataFile' => codecept_data_dir() . 'cart.php'
            ],
        ]);

        Yii::$app->params = array_merge(
            require(__DIR__ . '/../../../../common/config/params.php'),
            require(__DIR__ . '/../../../../common/config/params-local.php'),
            require(__DIR__ . '/../../../config/params.php'),
            require(__DIR__ . '/../../../config/params-local.php')
        );
    }
}

