<?php

namespace frontend\tests\unit\models;

use Codeception\Test\Unit;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\models\Job;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use frontend\models\PostJobForm;
use Yii;

/**
 * Class CreateJobTest
 * @package frontend\tests\unit\models
 */
class CreateJobTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('create-job-validation', 'Услуга - проверка валидаторов', Test::STATUS_FAIL);

        $model = new PostJobForm();
        $model->job = new Job();
        $input = [
            'PostJobForm' => [
                'title' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'tags' => null,
                'execution_time' => null,
                'description' => null,
                'requirements' => null,
                'locale' => null,
                'price_per_unit' => null,
                'currency_code' => null,
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => null,
                    'description' => null,
                    'included' => null,
                    'excluded' => null,
                    'price' => null,
                    'type' => null,
                ]
            ],
            'JobQa' => [
                [
                    'question' => null,
                    'answer' => null,
                ]
            ],
            'JobExtras' => [
                [
                    'title' => null,
                    'price' => null,
                    'description' => null,
                ]
            ],
        ];

        $model->myLoad($input);

        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        expect('Locale', $model->validate(['locale']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Basic package description must not be empty', $model->packages['basic']->validate(['description']))->false();
        expect('Basic package price must not be empty', $model->packages['basic']->validate(['price']))->false();

        $input = array_merge($input, [
            'PostJobForm' => [
                'title' => str_repeat('a', 90),
                'tags' => str_repeat('tag,', 70),
                'requirements' => str_repeat('a', 700),
                'execution_time' => 1500,
                'locale' => 'ka-KA',
                'currency_code' => 'HRY',
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => str_repeat('a', 260),
                    'description' => str_repeat('a', 1050),
                ]
            ]
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Too many tags', $model->validate(['tags']))->false();
        expect('Requirements is too long', $model->validate(['requirements']))->false();
        expect('Execution time is too big', $model->validate(['execution_time']))->false();
        expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Requirements is too long', $model->validate(['requirements']))->false();
        expect('Wrong currency code', $model->validate(['currency_code']))->false();
        expect('Too long package title', $model->packages['basic']->validate(['title']))->false();
        expect('Too long package description', $model->packages['basic']->validate(['description']))->false();

        $this->_testService->logTestResult('create-job-validation', 'Услуга - проверка валидаторов', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create job with validation failing
     */
    public function testCreateWrongJob()
    {
        $this->_testService->logTestResult('create-wrong-job', 'Услуга - создание с ошибками', Test::STATUS_FAIL);

        $model = new PostJobForm();
        $model->job = new Job();
        $model->myLoad([
            'PostJobForm' => [
                'title' => null,
                'parent_category_id' => null,
                'category_id' => null,
                'tags' => null,
                'execution_time' => null,
                'description' => null,
                'requirements' => null,
                'locale' => null
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => null,
                    'description' => null,
                    'included' => null,
                    'excluded' => null,
                    'price' => null,
                    'type' => null,
                ]
            ],
            'JobQa' => [
                [
                    'question' => null,
                    'answer' => null,
                ]
            ],
            'JobExtras' => [
                [
                    'title' => null,
                    'price' => null,
                    'description' => null,
                ]
            ],
        ]);

        expect('Job not saving', $model->save())->false();

        $this->_testService->logTestResult('create-wrong-job', 'Услуга - создание с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create job without errors
     */
    public function testCreateRightJob()
    {
        $this->_testService->logTestResult('create-right-job', 'Услуга - успешное создание', Test::STATUS_FAIL);

        $model = new PostJobForm();
        $model->job = new Job();
        $model->myLoad([
            'PostJobForm' => [
                'title' => 'Уборка квартиры до 40 м2',
                'parent_category_id' => 79,
                'category_id' => 80,
                'tags' => 'tag1, tag2, tag3',
                'execution_time' => 1,
                'description' => 'детальное описание работы бла бла бла',
                'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка',
                'locale' => 'ru-RU'
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => 'Уборка квартиры до 40 м2',
                    'description' => 'Поддерживающая уборка квартиры до 40 м2.',
                    'included' => '',
                    'excluded' => '',
                    'price' => '1000000',
                    'type' => 'basic',
                ]
            ],
            'JobQa' => [
                [
                    'question' => 'Вы делаете влажную уборку?',
                    'answer' => 'Да',
                ]
            ],
            'JobExtras' => [
                [
                    'title' => 'Приготовить поесть',
                    'price' => 500,
                    'description' => '',
                ]
            ],
        ]);

        expect('job is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-job', 'Услуга - успешное создание', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}