<?php

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobExtra as JobExtraFixture;
use common\fixtures\Order as OrderFixture;
use common\fixtures\OrderExtra as OrderExtraFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Payment as PaymentFixture;
use common\fixtures\PaymentMethod as PaymentMethodFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\AdvancedDecisionForm;
use frontend\modules\account\models\history\DecisionForm;
use Yii;

/**
 * Class OrderRequirementsDecisionTest
 * @package frontend\tests\unit\models
 */
class OrderRequirementsDecision2Test extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'jobExtra' => [
                'class' => JobExtraFixture::class,
                'dataFile' => codecept_data_dir() . 'extra.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'payment_method' => [
                'class' => PaymentMethodFixture::class,
                'dataFile' => codecept_data_dir() . 'payment_method.php'
            ],
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_requirements.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product.php'
            ],
            'orderExtra' => [
                'class' => OrderExtraFixture::class,
                'dataFile' => codecept_data_dir() . 'order_extra.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }

    public function testOrderRequirementsAccept()
    {
        $this->_testService->logTestResult('order-requirements-accept', 'Заказ услуги - подтверждение требований', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);
        $time = time() + 3 * 86400;
        $date = date('d-m-Y', $time);

        if ($order !== null) {
            $postRequirementsExist = OrderHistory::find()->where(['type' => OrderHistory::TYPE_POST_REQUIREMENTS, 'order_id' => $order->id])->exists();
            if ($postRequirementsExist === true) {
                $model = new AdvancedDecisionForm([
                    'h' => SecurityHelper::encrypt(Order::STATUS_ACTIVE),
                    't' => SecurityHelper::encrypt(OrderHistory::TYPE_REQUIREMENTS_ACCEPT),
                    'placeholder' => Yii::t('order', 'When are you planning to finish this order?'),
                    'zxToken' => $formToken,
                    'content' => 'да, ок test',
                    'seller_id' => $order->seller_id,
                    'customer_id' => $order->customer_id,
                    'order_id' => $order->id,
                    'tbd_at' => $date,
                ]);

                expect('Order requirements accepted', $model->save())->true();
            }
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-requirements-accept', 'Заказ услуги - подтверждение требований', Test::STATUS_SUCCESS);
    }

    public function testOrderRequirementsDeny()
    {
        $this->_testService->logTestResult('order-requirements-deny', 'Заказ услуги - уточнение требований', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if ($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_MISSING_DETAILS),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_REQUIREMENTS_DENY),
                'placeholder' => Yii::t('account', 'What is missing in additional details?'),
                'zxToken' => $formToken,
                'content' => 'давайте уточним test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
            ]);

            expect('Order requirements denied', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-requirements-deny', 'Заказ услуги - уточнение требований', Test::STATUS_SUCCESS);
    }

    public function testOrderCancel()
    {
        $this->_testService->logTestResult('order-cancel', 'Заказ услуги - отмена заказа', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if ($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_CANCEL),
                'placeholder' => Yii::t('order', 'Enter the reason for canceling the order'),
                'zxToken' => $formToken,
                'content' => 'нет, извините test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
            ]);

            expect('Order cancelled', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-cancel', 'Заказ услуги - отмена заказа', Test::STATUS_SUCCESS);
    }
}