<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.05.2017
 * Time: 14:52
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\PriceDecisionForm;
use Yii;

/**
 * Class OrderTenderPriceDecisionTest
 * @package frontend\tests\unit\models
 */
class OrderTenderPriceDecisionTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testTenderPriceDecisionApprove()
    {
        $this->_testService->logTestResult('order-tender-price-approve', 'Заказ на заявку - подтверждение цены', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_APPROVED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_APPROVED_HEADER),
                'zxToken' => $formToken,
                'content' => 'ок за 1234321р. test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
            ]);

            expect('tender approve price', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-price-approve', 'Заказ на заявку - подтверждение цены', Test::STATUS_SUCCESS);
    }

    public function testTenderPriceDecisionDeny()
    {
        $this->_testService->logTestResult('order-tender-price-deny', 'Заказ на заявку - заказчик не принял цену', Test::STATUS_FAIL);
        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_CANCELLED),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WORKER_DENIED_HEADER),
                'zxToken' => $formToken,
                'content' => 'нет, дорого. test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
            ]);
            expect('tender deny price', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-price-deny', 'Заказ на заявку - заказчик не принял цену', Test::STATUS_SUCCESS);
    }

    public function testTenderNewPrice()
    {
        $this->_testService->logTestResult('order-tender-new-price', 'Заказ на заявку - новая цена', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new priceDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                'zxToken' => $formToken,
                'content' => 'нет, давайте за 999998. test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
                'price' => '999998'
            ]);
            expect('tender new price', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-new-price', 'Заказ на заявку - новая цена', Test::STATUS_SUCCESS);
    }

    public function testTenderHistory()
    {
        $this->_testService->logTestResult('order-tender-history', 'Заказ на заявку - переписка', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new DecisionForm([
                'h' => SecurityHelper::encrypt($order->status),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_CUSTOM_MESSAGE),
                'placeholder' => Yii::t('account', 'Type your message here...'),
                'zxToken' => $formToken,
                'content' => 'почему такая цена. test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
            ]);
            expect('tender deny price', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-history', 'Заказ на заявку - переписка', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender_contract.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_tender_price.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_2@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}