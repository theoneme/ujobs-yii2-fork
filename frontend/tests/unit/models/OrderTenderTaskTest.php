<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.05.2017
 * Time: 13:44
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\Order as OrderFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Payment as PaymentFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\AttachmentDecisionForm;
use frontend\modules\account\models\history\DecisionForm;
use Yii;

/**
 * Class OrderTenderTaskTest
 * @package frontend\tests\unit\models
 */
class OrderTenderTaskTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testTenderTask()
    {
        $this->_testService->logTestResult('order-tender-task', 'Заказ на заявку - отправка задания', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new AttachmentDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_REQUIRES_TASK_APPROVE),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_WEEKLY_TASK),
                'zxToken' => $formToken,
                'content' => 'вот ваше задание test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
            ]);

            expect('tender send task', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-task', 'Заказ на заявку - отправка задания', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_tender_pay.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}