<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.05.2017
 * Time: 14:52
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderNoPayment as OrderFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\PriceDecisionForm;
use Yii;

/**
 * Class OrderTenderPriceTest
 * @package frontend\tests\unit\models
 */
class OrderTenderPriceTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testTenderPrice()
    {
        $this->_testService->logTestResult('order-tender-price', 'Заказ на заявку - создание с ценой', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if($order !== null) {
            $model = new PriceDecisionForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_TENDER_RESPONSE_PRICE),
                'zxToken' => $formToken,
                'content' => 'возьмусь за работу за 1234321р. test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
                'price' => '1234321'
            ]);
            expect('tender price form', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-price', 'Заказ на заявку - создание с ценой', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender_contract.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_tender_create.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product_tender.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}