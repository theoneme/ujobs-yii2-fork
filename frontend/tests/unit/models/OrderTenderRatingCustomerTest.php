<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.05.2017
 * Time: 16:06
 */

namespace frontend\tests\unit\models;

use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\Order as OrderFixture;
use common\fixtures\OrderHistory as OrderHistoryFixture;
use common\fixtures\OrderProduct as OrderProductFixture;
use common\fixtures\Payment as PaymentFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\helpers\FormHelper;
use common\helpers\SecurityHelper;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\DecisionForm;
use frontend\modules\account\models\history\RatingForm;
use Yii;

/**
 * Class OrderTenderRatingCustomerTest
 * @package frontend\tests\unit\models
 */
class OrderTenderRatingCustomerTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testTenderRatingCustomer()
    {
        $this->_testService->logTestResult('order-tender-rating-customer', 'Заказ на заявку - рейтинг исполнителя', Test::STATUS_FAIL);

        $formToken = FormHelper::generateTokenInput();
        $order = Order::findOne(['email' => 'zxc-order-tester@test.com']);

        if ($order !== null) {
            $model = new RatingForm([
                'h' => SecurityHelper::encrypt(Order::STATUS_TENDER_AWAITING_REVIEW),
                't' => SecurityHelper::encrypt(OrderHistory::TYPE_REVIEW_SELLER),
                'scenario' => RatingForm::SCENARIO_SELLER,
                'zxToken' => $formToken,
                'type' => DecisionForm::DECISION_TENDER_HISTORY,
                'content' => 'рейтинг test',
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'rating' => '10',
                'communication' => '6',
                'service' => '8',
                'recommend' => '8',
            ]);
            expect('tender rating customer', $model->save())->true();
        }

        expect('order exists', $order !== null)->true();

        $this->_testService->logTestResult('order-tender-rating-customer', 'Заказ на заявку - рейтинг исполнителя', Test::STATUS_SUCCESS);
    }

    protected function _before()
    {
        $this->tester->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender.php'
            ],
            'orderHistory' => [
                'class' => OrderHistoryFixture::class,
                'dataFile' => codecept_data_dir() . 'order_history_tender_delivery_decision.php'
            ],
            'orderProduct' => [
                'class' => OrderProductFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product.php'
            ],
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_2@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}