<?php

namespace frontend\tests\unit\models;

use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobExtra as JobExtraFixture;
use common\fixtures\JobPackage as JobPackageFixture;
use common\fixtures\JobQA as JobQAFixture;
use common\fixtures\JobTag as JobTagFixture;
use common\fixtures\ModAttributeDescription as ModAttributeDescriptionFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\models\Job;
use common\models\Test;
use common\models\user\LoginForm;
use common\services\TestService;
use frontend\models\PostJobForm;
use Yii;

class UpdateJobTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function testUpdateJobRight()
    {
        $this->_testService->logTestResult('update-right-job', 'Услуга - успешное редактирование', Test::STATUS_FAIL);

        $job = Job::findOne(['requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка']);
        $model = Yii::createObject(PostJobForm::class);
        $model->loadJob($job->id);
        $model->myLoad([
            'PostJobForm' => [
                'title' => 'Уборка квартиры до 40 м2',
                'parent_category_id' => '79',
                'category_id' => '80',
                'tags' => 'tag1, tag2, tag3',
                'execution_time' => '1',
                'description' => 'детальное описание работы бла бла бла',
                'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка',
                'price_per_unit' => false,
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => 'Уборка квартиры до 40 м2',
                    'description' => 'Поддерживающая уборка квартиры до 40 м2.',
                    'included' => '',
                    'excluded' => '',
                    'price' => '1234587',
                    'type' => 'basic',
                ]
            ],
            'JobQa' => [
                [
                    'question' => 'Вы делаете влажную уборку?',
                    'answer' => 'Да',
                ]
            ],
            'JobExtras' => [
                [
                    'title' => 'Приготовить поесть',
                    'price' => '500',
                    'description' => '',
                ], [
                    'title' => 'Полить цветы',
                    'price' => '50',
                    'description' => '',
                ]
            ],

        ]);
        expect('job is updating', $model->save())->true();

        $this->_testService->logTestResult('update-right-job', 'Услуга - успешное редактирование', Test::STATUS_SUCCESS);
    }

    public function testUpdateJobWrong()
    {
        $this->_testService->logTestResult('update-wrong-job', 'Услуга - редактирование с ошибками', Test::STATUS_FAIL);

        $job = Job::findOne(['requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка']);
        $model = Yii::createObject(PostJobForm::class);
        $model->loadJob($job->id);
        $model->myLoad([
            'PostJobForm' => [
                'title' => '',
                'parent_category_id' => '79',
                'category_id' => '80',
                'tags' => 'tag1, tag2, tag3',
                'execution_time' => '1',
                'description' => '',
                'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка',
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => 'Уборка квартиры до 40 м2',
                    'description' => 'Поддерживающая уборка квартиры до 40 м2.',
                    'included' => '',
                    'excluded' => '',
                    'price' => '1234587',
                    'type' => 'basic',
                ]
            ],
            'JobQa' => [
                [
                    'question' => 'Вы делаете влажную уборку?',
                    'answer' => 'Да',
                ]
            ],
            'JobExtras' => [
                [
                    'title' => 'Приготовить поесть',
                    'price' => '500',
                    'description' => '',
                ], [
                    'title' => 'Полить цветы',
                    'price' => '50',
                    'description' => '',
                ]
            ],

        ]);
        expect('job is not updating', $model->save())->false();

        $this->_testService->logTestResult('update-wrong-job', 'Услуга - редактирование с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('update-job-validation', 'Услуга - проверка валидаторов при редактировании', Test::STATUS_FAIL);

        $job = Job::findOne(['requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка']);
        $model = new PostJobForm();
        $model->loadJob($job->id);
        $input = [
            'PostJobForm' => [
                'parent_category_id' => null,
                'category_id' => null,
                'title' => null,
                'description' => null,
                'locale' => null,
                'price_per_unit' => true,
                'currency_code' => null,
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => null,
                    'description' => null,
                    'included' => null,
                    'excluded' => null,
                    'price' => null,
                    'type' => null,
                ]
            ],
            'JobQa' => [
            ],
            'JobExtras' => [
            ],

        ];
        $model->myLoad($input);
        expect('Title must not be empty', $model->validate(['title']))->false();
        expect('Description must not be empty', $model->validate(['description']))->false();
        expect('Locale must not be empty', $model->validate(['locale']))->false();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();
        expect('Basic package description must not be empty', $model->packages['basic']->validate(['description']))->false();
        expect('Basic package price must not be empty', $model->packages['basic']->validate(['price']))->false();
        expect('Currency code must not be empty', $model->validate(['currency_code']))->false();

        $input = array_merge($input, [
            'PostJobForm' => [
                'title' => str_repeat('a', 90),
                'tags' => str_repeat('tag,', 70),
                'requirements' => str_repeat('a', 700),
                'execution_time' => 1500,
                'locale' => 'ka-KA',
                'currency_code' => 'HRY',
            ],
            'JobPackage' => [
                'basic' => [
                    'title' => str_repeat('a', 260),
                    'description' => str_repeat('a', 1050),
                ]
            ]
        ]);
        $model->myLoad($input);

        expect('Title is too long', $model->validate(['title']))->false();
        expect('Too many tags', $model->validate(['tags']))->false();
        expect('Requirements is too long', $model->validate(['requirements']))->false();
        expect('Execution time is too big', $model->validate(['execution_time']))->false();
        expect('Wrong locale', $model->validate(['locale']))->false();
        expect('Requirements is too long', $model->validate(['requirements']))->false();
        expect('Wrong currency code', $model->validate(['currency_code']))->false();
        expect('Too long package title', $model->packages['basic']->validate(['title']))->false();
        expect('Too long package description', $model->packages['basic']->validate(['description']))->false();

        $this->_testService->logTestResult('update-job-validation', 'Услуга - проверка валидаторов при редактировании', Test::STATUS_SUCCESS);
    }

    // tests

    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'extra' => [
                'class' => JobExtraFixture::class,
                'dataFile' => codecept_data_dir() . 'extra.php'
            ],
            'package' => [
                'class' => JobPackageFixture::class,
                'dataFile' => codecept_data_dir() . 'package.php'
            ],
            'qa' => [
                'class' => JobQAFixture::class,
                'dataFile' => codecept_data_dir() . 'qa.php'
            ],
            'tag' => [
                'class' => JobTagFixture::class,
                'dataFile' => codecept_data_dir() . 'tag.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);
        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }

    protected function _after()
    {
    }
}