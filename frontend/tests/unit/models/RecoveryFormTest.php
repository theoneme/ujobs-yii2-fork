<?php

namespace frontend\tests\unit\models;

use common\fixtures\Token as TokenFixture;
use common\fixtures\User as UserFixture;
use common\models\user\RecoveryForm;
use common\models\user\Token;
use common\models\user\User;
use common\models\Test;
use common\services\TestService;
use Yii;

/**
 * Class RecoveryFormTest
 * @package frontend\tests\unit\models
 */
class RecoveryFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TestService
     */
    private $_testService = null;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'token' => [
                'class' => TokenFixture::class,
                'dataFile' => codecept_data_dir() . 'token.php'
            ]
        ]);

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }

    public function testSendEmailSuccessfully()
    {
        $this->_testService->logTestResult('recovery-send-email', 'Восстановление пароля - успешная отправка писаьма', Test::STATUS_FAIL);

        $userFixture = $this->tester->grabFixture('user', 0);

        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'request',
        ]);
        $model->email = $userFixture['email'];

        $user = User::findOne(['username' => $userFixture['username']]);

        expect_that($model->sendRecoveryMessage());
        expect_that($user->token->code);

        $emailMessage = $this->tester->grabLastSentEmail();
        expect('valid email is sent', $emailMessage)->isInstanceOf('yii\mail\MessageInterface');
        expect($emailMessage->getTo())->hasKey($model->email);
        expect($emailMessage->getFrom())->hasKey(Yii::$app->params['supportEmail']);

        $this->_testService->logTestResult('recovery-send-email', 'Восстановление пароля - отправка письма', Test::STATUS_SUCCESS);
    }

    public function testResetWrongToken()
    {
        $this->_testService->logTestResult('recovery-wrong-token', 'Восстановление пароля - неверный токен', Test::STATUS_FAIL);

        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'reset',
        ]);
        $model->password = $model->confirm = 'new_password';

        $tokenModel = Token::findOne(['code' => '', 'type' => Token::TYPE_RECOVERY]);
        $token = $tokenModel ? $tokenModel : new Token();

        expect('model should not reset user password', $model->resetPassword($token))->false();

        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'reset',
        ]);
        $model->password = $model->confirm = 'new_password';

        $tokenModel = Token::findOne(['code' => 'elelelelelel', 'type' => Token::TYPE_RECOVERY]);
        $token = $tokenModel ? $tokenModel : new Token();

        expect('model should not reset user password', $model->resetPassword($token))->false();

        $this->_testService->logTestResult('recovery-wrong-token', 'Восстановление пароля - неверный токен', Test::STATUS_SUCCESS);
    }

    public function testResetCorrect()
    {
        $this->_testService->logTestResult('recovery-reset-correct', 'Восстановление пароля - успешный сброс пароля', Test::STATUS_FAIL);

        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'reset',
        ]);
        $model->password = $model->confirm = 'new_password';

        $tokenModel = Token::findOne(['code' => 'resetcode', 'type' => Token::TYPE_RECOVERY]);
        $token = $tokenModel ? $tokenModel : new Token();

        expect('model should reset user password', $model->resetPassword($token))->true();

        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'reset',
        ]);
        $model->password = $model->confirm = 'new_password';

        $tokenModel = Token::findOne(['code' => 'resetcode2', 'type' => Token::TYPE_RECOVERY]);
        $token = $tokenModel ? $tokenModel : new Token();

        expect('model should reset user password', $model->resetPassword($token))->true();

        $this->_testService->logTestResult('recovery-reset-correct', 'Восстановление пароля - успешный сброс пароля', Test::STATUS_SUCCESS);
    }
}
