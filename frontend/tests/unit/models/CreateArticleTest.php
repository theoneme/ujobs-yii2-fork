<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 04.07.2018
 * Time: 15:16
 */

namespace frontend\tests;

use Codeception\Test\Unit;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\models\Page;
use common\models\user\LoginForm;
use common\models\Test;
use common\services\TestService;
use frontend\models\PostArticleForm;
use Yii;

/**
 * Class CreateArticleTest
 * @package frontend\tests\unit\models
 */
class CreateArticleTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    /**
     * @var TestService
     */
    private $_testService = null;
    /**
     * Test validation
     */
    public function testValidation()
    {
        $this->_testService->logTestResult('create-article-validation', 'Статья - проверка валидаторов', Test::STATUS_FAIL);

        $model = new PostArticleForm();
        $model->page = new Page();
        $input = [
            'PostArticleForm' => [
                'parent_category_id' => null,
                'category_id' => null,
            ],
            'ContentTranslation' => [
                [
                    'title' => null,
                    'content' => null,
                    'locale' => 'en-GB',
                ]
            ],
        ];
        $model->myLoad($input);

        expect('Title must not be empty', $model->page->translations['en-GB']->validate(['title']))->false();
        expect('Content must not be empty', $model->page->translations['en-GB']->validate(['content']))->false();
//        expect('Locale', $model->page->translations['en-GB']->validate(['locale']))->true();
        expect('Parent category must not be empty', $model->validate(['parent_category_id']))->false();
        expect('Category must not be empty', $model->validate(['category_id']))->false();

        $input = array_merge($input, [
            'PostArticleForm' => [
                'tags' => str_repeat('tag,', 70),
            ],
            'ContentTranslation' => [
                [
                    'title' => str_repeat('a', 210),
                    'locale' => 'en-KA'
                ]
            ],
        ]);

        $model->myLoad($input);

        expect('Title is too long', $model->page->translations['en-GB']->validate(['title']))->false();
        expect('Too many tags', $model->validate(['tags']))->false();
//        expect('Wrong locale', $model->page->translations['en-GB']->validate(['locale']))->false();
        $this->_testService->logTestResult('create-article-validation', 'Статья - проверка валидаторов', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create article with validation failing
     */
    public function testCreateWrongArticle()
    {
        $this->_testService->logTestResult('create-wrong-article', 'Статья - создание с ошибками', Test::STATUS_FAIL);

        $model = new PostArticleForm();
        $model->page = new Page();
        $input = [
            'PostArticleForm' => [
                'parent_category_id' => null,
                'category_id' => null,
            ],
            'ContentTranslation' => [
                [
                    'title' => null,
                    'content' => null,
                    'locale' => null
                ]
            ],
        ];
        $model->myLoad($input);
        expect('Article not saving', $model->save())->false();


        $this->_testService->logTestResult('create-wrong-article', 'Статья - создание с ошибками', Test::STATUS_SUCCESS);
    }

    /**
     * Attempt to create article without errors
     */
    public function testCreateRightArticle()
    {

        $this->_testService->logTestResult('create-right-article', 'Статья - успешное создание', Test::STATUS_FAIL);

        $model = new PostArticleForm();
        $model->page = new Page();
        $input = [
            'PostArticleForm' => [
                'parent_category_id' => 79,
                'category_id' => 80,
            ],
            'ContentTranslation' => [
                [
                    'title' => 'test article title',
                    'content' => 'test article text',
                    'locale' => 'en-GB'
                ]
            ],
        ];
        $model->myLoad($input);
        expect('Article is saving', $model->save())->true();

        $this->_testService->logTestResult('create-right-article', 'Статья - успешное создание', Test::STATUS_SUCCESS);
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->tester->haveFixtures([
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $modelLogin = Yii::createObject(LoginForm::class);
        $modelLogin->login = 'test_email_1@gmail.com';
        $modelLogin->password = '222222';
        $modelLogin->login();

        $this->_testService = new TestService(Test::GROUP_UNIT);
    }
}