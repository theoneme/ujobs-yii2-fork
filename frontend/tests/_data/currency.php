<?php

return [
    [
        'title' => 'Cointy',
        'code' => 'CTY',
        'symbol_left' => null,
        'symbol_right' => 'CTY',
        'value' => 0,
        'status' => 0,
    ],
    [
        'title' => 'Ruble',
        'code' => 'RUB',
        'symbol_left' => null,
        'symbol_right' => 'р.',
        'value' => 0,
        'status' => 0,
    ],
    [
        'title' => 'Dollar',
        'code' => 'USD',
        'symbol_left' => '$',
        'symbol_right' => null,
        'value' => 0,
        'status' => 0,
    ],
];
