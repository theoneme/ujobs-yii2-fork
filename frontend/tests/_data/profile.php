<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.03.2017
 * Time: 22:22
 */

use common\models\user\Profile;

return [
    [
        'name' => 'Тест юзер 1',
        'public_email' => 'test_email_1@gmail.com',
        'bio' => 'Универсальный работник 1.',
        'status' => Profile::STATUS_ACTIVE,
        'status_text' => 'Универсальный текст 1',
        'country' => 'Россия',
        'email_notifications' => '1',
        'sms_notifications' => '1',
        'message2_flag' => true,
        'gravatar_email' => 'qwe.png'
    ],
    [
        'name' => 'Тест юзер 2',
        'public_email' => 'test_email_2@gmail.com',
        'bio' => 'Универсальный работник 2.',
        'status' => Profile::STATUS_ACTIVE,
        'status_text' => 'Универсальный текст 2',
        'country' => 'Россия',
        'email_notifications' => '1',
        'sms_notifications' => '1',
        'message2_flag' => false,
        'gravatar_email' => 'asd.png'
    ],
];