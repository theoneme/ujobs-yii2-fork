<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 08.08.2018
 * Time: 14:12
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content fixture',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_MESSAGE,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content fixture',
        'status' => 0,
        'type' => 0,
        'created_at' => $time,
        'updated_at' => $time,
    ],
];