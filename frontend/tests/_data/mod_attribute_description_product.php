<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.07.2018
 * Time: 14:10
 */


return [
    [
        'entity' => 'attribute-value',
        'title' => 'Тег товар1',
        'locale' => 'ru-RU',
    ],
    [
        'entity' => 'attribute-value',
        'title' => 'Тег товар2',
        'locale' => 'ru-RU',
    ],
    [
        'entity' => 'attribute-value',
        'title' => 'Белый фиксутра',
        'locale' => 'ru-RU',
    ],
    [
        'entity' => 'attribute-value',
        'title' => 'Коттон фикстура',
        'locale' => 'ru-RU',
    ],
    [
        'entity' => 'attribute-value',
        'title' => 'Новый фикстура',
        'locale' => 'ru-RU',
    ],
    [
        'entity' => 'attribute-value',
        'title' => 'Возможна доставка фикстура',
        'locale' => 'ru-RU',
    ],
    [
        'entity' => 'attribute-value',
        'title' => 'apple фикстура',
        'locale' => 'ru-RU',
    ],
];