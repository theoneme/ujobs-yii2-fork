<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 02.05.2017
 * Time: 14:59
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'content' => 'content1',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_ORDER_CREATED,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content2',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_ORDER_PAID,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content3',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_START_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content4',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_NEED_REQUIREMENTS,
        'created_at' => $time - 60 * 50,
        'updated_at' => $time - 60 * 50,
    ],
];