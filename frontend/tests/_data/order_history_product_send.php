<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 10.08.2018
 * Time: 13:16
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_ORDER_CREATED,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_ORDER_PAID,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_REQUIREMENTS_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_NEED_REQUIREMENTS,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'fixture text shipping',
        'status' => 0,
        'type' => OrderHistory::TYPE_PRODUCT_SET_SHIPPING,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'fixture ship',
        'status' => 0,
        'type' => OrderHistory::TYPE_PRODUCT_SEND_PRODUCT,
        'created_at' => $time,
        'updated_at' => $time,
    ],
];