<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.07.2018
 * Time: 14:57
 */

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => 'test title for fixture product content',
        'content' => $faker->sentences($nb = 3, $asText = true),
        'locale' => 'ru-RU',
        'entity' => 'product'
    ],
];