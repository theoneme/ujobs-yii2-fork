<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.05.2017
 * Time: 17:35
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [

    [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content1',
        'status' => Order::STATUS_TENDER_UNAPPROVED,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content2',
        'status' => Order::STATUS_TENDER_UNAPPROVED,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_MESSAGE,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'возьмусь за работу test',
        'status' => Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_PRICE,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ],
];