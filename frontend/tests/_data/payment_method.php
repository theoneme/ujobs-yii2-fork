<?php

return [
    [
        'id' => 3,
        'enabled' => true,
        'code' => 'robokassa',
    ],
    [
        'id' => 5,
        'enabled' => true,
        'code' => 'sberbank',
    ],
    [
        'id' => 6,
        'enabled' => true,
        'code' => 'balance',
    ],
];
