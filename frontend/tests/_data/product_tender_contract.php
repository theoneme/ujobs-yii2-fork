<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 08.08.2018
 * Time: 13:20
 */

$time = time();

return [
    [
        'created_at' => $time - 60 * 60,
        'updated_at' => $time - 60 * 60,
        'status' => 30,
        'price' => null,
        'currency_code' => 'USD',
        'contract_price' => 1,
        'parent_category_id' => 2455,
        'category_id' => 2458,
        'subcategory_id' => null,
        'alias' => 'fixture_alias',
        'locale' => 'en-GB',
        'shipping_info' => null,
        'length' => null,
        'width' => null,
        'height' => null,
        'type' => 'tender',
        'amount' => 1,
        'measure_id' => null,
    ],
];