<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 15.05.2017
 * Time: 14:57
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'content' => 'content1',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_ORDER_CREATED,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content2',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_ORDER_PAID,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content3',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_START_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content4',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_NEED_REQUIREMENTS,
        'created_at' => $time - 60 * 50,
        'updated_at' => $time - 60 * 50,
    ], [
        'content' => 'покормить попугая test',
        'status' => Order::STATUS_MISSING_DETAILS,
        'type' => OrderHistory::TYPE_POST_REQUIREMENTS,
        'created_at' => $time + 86400,
        'updated_at' => $time + 86400,
    ], [
        'content' => 'да, ок test',
        'status' => Order::STATUS_ACTIVE,
        'type' => OrderHistory::TYPE_REQUIREMENTS_ACCEPT,
        'created_at' => $time + 86400,
        'updated_at' => $time + 86400,
    ], [
        'content' => 'content5',
        'status' => Order::STATUS_ACTIVE,
        'type' => OrderHistory::TYPE_JOB_START,
        'created_at' => $time + 86400,
        'updated_at' => $time + 86400,
    ], [
        'content' => 'content6',
        'status' => Order::STATUS_DELIVERED,
        'type' => OrderHistory::TYPE_DELIVERY,
        'created_at' => $time + 3 * 86400,
        'updated_at' => $time + 3 * 86400,
    ], [
        'content' => 'Ваш заказ test',
        'status' => Order::STATUS_AWAITING_REVIEW,
        'type' => OrderHistory::TYPE_JOB_ACCEPT,
        'created_at' => $time + 3 * 86400,
        'updated_at' => $time + 3 * 86400,
    ],
];