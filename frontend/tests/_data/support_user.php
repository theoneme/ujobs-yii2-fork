<?php

$time = time();

return [
    [
        'id' => 301,
        'username' => 'ujobs-support',
        'email' => 'support@ujobs.me',
        'password_hash' => '$2y$10$Z1RLh08n9nlHTP7TEj4GtO3E8WL6FlNOcnR6HB6VP8IXd.kEWJkeS',
        'created_at' => $time,
        'updated_at' => $time,
        'confirmed_at' => $time,
        'unconfirmed_email' => null,
        'auth_key' => 'TnXTrtLdj-YJBlG2A6jFHJreKgbsLYCa',
        'phone' => '',
    ],
];
