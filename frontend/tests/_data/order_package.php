<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 02.05.2017
 * Time: 14:59
 */

use common\modules\store\models\Order;

$time = time();

return [
    [
        'payment_method_id' => '3',
        'email' => 'zxc-order-tester@test.com',
        'phone' => '0954150094',
        'total' => '999998',
        'created_at' => $time,
        'updated_at' => $time,
        'status' => Order::STATUS_MISSING_DETAILS,
//        'full_name' => 'test-name',
        'product_type' => Order::TYPE_JOB_PACKAGE,
        'is_price_approved' => '1'
    ],
];