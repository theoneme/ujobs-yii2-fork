<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 10.03.2017
 * Time: 16:45
 */

$time = time();

return [
    [
        'created_at' => $time,
        'updated_at' => $time,
        'status' => '30',
        'price' => '300',
        'currency_code' => 'RUB',
        'parent_category_id' => '79',
        'category_id' => '80',
        'execution_time' => '1',
        'type' => 'job',
        'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка',
        'price_per_unit' => true,
        'alias' => Yii::$app->utility->generateSlug('Тестовая услуга', 5, 155)
    ],
];
