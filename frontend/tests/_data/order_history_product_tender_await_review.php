<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 09.08.2018
 * Time: 15:07
 */


use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content fixture',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_MESSAGE,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content fixture',
        'status' => 0,
        'type' => 0,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'content fixture customer price',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_PRICE,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'ph',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_SELLER_APPROVED_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => '',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_PRODUCT_PAID,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'fixture order delivered',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_PRODUCT_REPORT,
        'created_at' => $time,
        'updated_at' => $time,
    ], [
        'content' => 'ph',
        'status' => 0,
        'type' => OrderHistory::TYPE_TENDER_REPORT_ACCEPT_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
    ]
];