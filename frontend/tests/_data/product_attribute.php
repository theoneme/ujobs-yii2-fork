<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.07.2018
 * Time: 16:20
 */

return [
    [
        'locale' => 'ru-RU',
        'entity_alias' => 'product_tag',
        'value_alias' => 'tag1-fixture'
    ], [
        'locale' => 'ru-RU',
        'entity_alias' => 'product_tag',
        'value_alias' => 'tag2-fixture'
    ], [
        'locale' => 'ru-RU',
        'entity_alias' => 'color',
        'value_alias' => 'beliy-fixture'
    ], [
        'locale' => 'ru-RU',
        'entity_alias' => 'material',
        'value_alias' => 'cotton-fixture'
    ], [
        'locale' => 'ru-RU',
        'entity_alias' => 'condition',
        'value_alias' => 'new-fixture'
    ], [
        'locale' => 'ru-RU',
        'entity_alias' => 'shipping_type',
        'value_alias' => 'vozmozhna-dostavka-fixture'
    ], [
        'locale' => 'ru-RU',
        'entity_alias' => 'brand_a',
        'value_alias' => 'apple-fixture'
    ],
];