<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.03.2017
 * Time: 14:55
 */

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => $faker->sentences($nb = 1, $asText = true),
        'description' => $faker->sentences($nb = 3, $asText = true),
        'price' => '1234587',
        'currency_code' => 'RUB',
        'included' => $faker->paragraphs($nb = 4, $asText = true),
        'excluded' => $faker->paragraphs($nb = 2, $asText = true),
        'type' => 'basic',
    ], [
        'title' => $faker->sentences($nb = 1, $asText = true),
        'description' => $faker->sentences($nb = 3, $asText = true),
        'price' => '1234587',
        'currency_code' => 'RUB',
        'included' => $faker->paragraphs($nb = 4, $asText = true),
        'excluded' => $faker->paragraphs($nb = 2, $asText = true),
        'type' => 'standart',
    ], [
        'title' => $faker->sentences($nb = 1, $asText = true),
        'description' => $faker->sentences($nb = 3, $asText = true),
        'price' => '1234587',
        'currency_code' => 'RUB',
        'included' => $faker->paragraphs($nb = 4, $asText = true),
        'excluded' => $faker->paragraphs($nb = 2, $asText = true),
        'type' => 'premium',
    ],
];
