<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.07.2018
 * Time: 16:19
 */

$time = time();

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => 'test title for fixture video',
        'description' => $faker->sentences($nb = 3, $asText = true),
        'parent_category_id' => 79,
        'category_id' => 80,
        'currency_code' => 'USD',
        'locale' => 'ru-RU',
        'price' => 500,
        'status' => '30',
        'created_at' => $time - 60 * 60,
        'updated_at' => $time - 60 * 60,
        'alias' => Yii::$app->utility->generateSlug('Тестовое видео', 5, 155)
    ],
];