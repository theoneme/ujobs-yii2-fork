<?php

return [
    [
        'id' => '36',
        'alias' => 'tag',
        'name' => 'Тег',
        'is_searchable' => true,
    ],
    [
        'id' => '38',
        'alias' => 'skill',
        'name' => 'Навык',
        'is_searchable' => true,
    ],
    [
        'id' => '40',
        'alias' => 'language',
        'name' => 'Язык',
        'is_searchable' => true,
    ],
    [
        'id' => '41',
        'alias' => 'specialty',
        'name' => 'Специальность',
        'is_searchable' => true,
    ],
    [
        'id' => '42',
        'alias' => 'city',
        'name' => 'Город',
        'is_searchable' => true,
    ],
    [
        'id' => '45',
        'alias' => 'material',
        'name' => 'Материал',
        'is_searchable' => true,
    ],
    [
        'id' => '46',
        'alias' => 'brand_a',
        'name' => 'Бренд',
        'is_searchable' => true,
    ],
    [
        'id' => '47',
        'alias' => 'shipping_type',
        'name' => 'Доставка',
        'is_searchable' => true,
    ],
    [
        'id' => '48',
        'alias' => 'product_tag',
        'name' => 'Тег',
        'is_searchable' => true,
    ],
    [
        'id' => '50',
        'alias' => 'condition',
        'name' => 'Состояние',
        'is_searchable' => true,
    ],
    [
        'id' => '53',
        'alias' => 'color',
        'name' => 'Цвет',
        'is_searchable' => true,
    ],
    [
        'id' => '1319',
        'alias' => 'portfolio_tag',
        'name' => 'Тег',
        'is_searchable' => true,
    ],
    [
        'id' => '3030',
        'alias' => 'page_tag',
        'name' => 'Тег',
        'is_searchable' => true,
    ],
];