<?php

return [
    [
        'category_id' => 79,
        'alias' => 'index',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'asd',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'contact',
        'publish_date' => 123,
        'template' => 'contact'
    ],
    [
        'category_id' => 141,
        'alias' => 'support',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'referral-program',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'become-freelancer-landing',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'outsource-mpp',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'start-selling-landing',
        'publish_date' => 123,
        'template' => 'page'
    ],
    [
        'category_id' => 141,
        'alias' => 'tariffs',
        'publish_date' => 123,
        'template' => 'tariff'
    ],
    [
        'category_id' => 141,
        'alias' => 'fair-play',
        'publish_date' => 123,
        'template' => 'fair-play'
    ],
];
