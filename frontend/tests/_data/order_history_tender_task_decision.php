<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.05.2017
 * Time: 15:18
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content1',
        'status' => Order::STATUS_TENDER_UNAPPROVED,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content2',
        'status' => Order::STATUS_TENDER_UNAPPROVED,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_MESSAGE,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'возьмусь за работу test',
        'status' => Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
        'type' => OrderHistory::TYPE_TENDER_RESPONSE_PRICE,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content3',
        'status' => Order::STATUS_TENDER_APPROVED,
        'type' => OrderHistory::TYPE_TENDER_WORKER_APPROVED_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content4',
        'status' => Order::STATUS_TENDER_PAID,
        'type' => OrderHistory::TYPE_TENDER_PAID,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'вот ваше задание test',
        'status' => Order::STATUS_TENDER_REQUIRES_TASK_APPROVE,
        'type' => OrderHistory::TYPE_TENDER_WEEKLY_TASK,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'ок, через три дня будет сделано test',
        'status' => Order::STATUS_TENDER_ACTIVE,
        'type' => OrderHistory::TYPE_TENDER_WEEKLY_TASK_ACCEPT,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '83',
    ],
];