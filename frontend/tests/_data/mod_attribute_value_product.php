<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.07.2018
 * Time: 14:09
 */

return [
    [
        'attribute_id' => '48',
        'alias' => 'tag1-fixture',
    ],
    [
        'attribute_id' => '48',
        'alias' => 'tag2-fixture',
    ],
    [
        'attribute_id' => '53',
        'alias' => 'beliy-fixture',
    ],
    [
        'attribute_id' => '45',
        'alias' => 'cotton-fixture',
    ],
    [
        'id' => 49183,
        'attribute_id' => '50',
        'alias' => 'new-fixture',
    ],
    [
        'attribute_id' => '47',
        'alias' => 'vozmozhna-dostavka-fixture',
    ],
    [
        'id' => 47936,
        'attribute_id' => '47',
        'alias' => 'samovyvoz',
    ],
    [
        'attribute_id' => '46',
        'alias' => 'apple-fixture',
    ],
];