<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 09.08.2018
 * Time: 13:31
 */

use common\modules\store\models\Order;

$time = time();

return [
    [
        'payment_method_id' => '6',
        'email' => 'zxc-order-tester@test.com',
        'phone' => '0954150094',
        'total' => 99911,
        'currency_code' => 'RUB',
        'created_at' => $time,
        'updated_at' => $time,
        'status' => Order::STATUS_PRODUCT_TENDER_DELIVERED,
//        'full_name' => 'Андрей Витальевич test',
        'product_type' => Order::TYPE_PRODUCT_TENDER,
        'is_price_approved' => '0'
    ],
];