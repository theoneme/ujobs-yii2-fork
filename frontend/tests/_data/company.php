<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.07.2018
 * Time: 14:07
 */

$time = time();

return [
    [
        'status' => '10',
        'status_text' => 'test status_text company for fixture',
        'created_at' => $time - 60 * 60,
        'updated_at' => $time - 60 * 60,
    ],
];