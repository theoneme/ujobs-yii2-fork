<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 14.03.2017
 * Time: 16:53
 */

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => 'test title for fixture',
        'content' => $faker->sentences($nb = 3, $asText = true),
        'locale' => 'ru-RU',
        'entity' => 'job'
    ],
];