<?php

$time = time();

return [
    [
        'username' => 'test_entity_0',
        'email' => 'test_email_1@gmail.com',
        'password_hash' => '$2y$10$Z1RLh08n9nlHTP7TEj4GtO3E8WL6FlNOcnR6HB6VP8IXd.kEWJkeS',
        'created_at' => $time,
        'updated_at' => $time,
        'confirmed_at' => $time,
        'auth_key' => 'TnXTrtLdj-YJBlG2A6jFHJreKgbsLYCa',
    ],
];
