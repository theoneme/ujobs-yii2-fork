<?php

return [
    [
        'tariff_id' => 1,
        'code' => 'time_since_tender_post_to_respond',
        'value' => 172800,
        'is_enabled' => true,
    ],
    [
        'tariff_id' => 1,
        'code' => 'new_conversations_per_day',
        'value' => 3,
        'is_enabled' => true,
    ],
    [
        'tariff_id' => 1,
        'code' => 'allowed_to_respond_on_advanced_tender',
        'value' => 0,
        'is_enabled' => true,
    ],
    [
        'tariff_id' => 1,
        'code' => 'allowed_tender_responses',
        'value' => 3,
        'is_enabled' => true,
    ],
];