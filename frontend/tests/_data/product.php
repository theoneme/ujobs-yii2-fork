<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.07.2018
 * Time: 14:51
 */

$time = time();

return [
    [
        'created_at' => $time - 60 * 60,
        'updated_at' => $time - 60 * 60,
        'status' => 30,
        'price' => 99911,
        'currency_code' => 'USD',
        'contract_price' => null,
        'parent_category_id' => 2455,
        'category_id' => 2458,
        'subcategory_id' => null,
        'alias' => 'fixture_alias',
        'locale' => 'en-GB',
        'shipping_info' => 'test shipping_info product for fixture',
        'length' => 20,
        'width' => 60,
        'height' => 30,
        'type' => 'product',
        'amount' => 1,
        'measure_id' => null,
    ],
];