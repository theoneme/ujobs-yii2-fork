<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 10.05.2017
 * Time: 17:10
 */

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;

$time = time();

return [
    [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content1',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_ORDER_CREATED,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content2',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_ORDER_PAID,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content3',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_START_HEADER,
        'created_at' => $time,
        'updated_at' => $time,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'content4',
        'status' => Order::STATUS_REQUIRES_PAYMENT,
        'type' => OrderHistory::TYPE_NEED_REQUIREMENTS,
        'created_at' => $time - 60 * 50,
        'updated_at' => $time - 60 * 50,
        'sender_id' => '596',
    ], [
        'seller_id' => '83',
        'customer_id' => '596',
        'content' => 'покормить попугая test',
        'status' => Order::STATUS_MISSING_DETAILS,
        'type' => OrderHistory::TYPE_POST_REQUIREMENTS,
        'created_at' => $time + 86400,
        'updated_at' => $time + 86400,
        'sender_id' => '596',
    ],
];