<?php

use common\models\user\Profile;

return [
    [
        'name' => 'Тест сапорт',
        'public_email' => 'support@ujobs.me',
        'bio' => 'Универсальный работник 1.',
        'status' => Profile::STATUS_ACTIVE,
        'status_text' => 'Универсальный текст 1',
        'country' => 'Россия',
        'email_notifications' => '1',
        'sms_notifications' => '1',
        'message2_flag' => true,
        'gravatar_email' => 'qwe.png'
    ],
];