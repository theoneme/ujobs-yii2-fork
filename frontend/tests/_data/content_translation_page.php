<?php

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => 'Тест пейж',
        'content' => $faker->sentences($nb = 3, $asText = true),
        'locale' => 'ru-RU',
        'entity' => 'page',
    ],
    [
        'title' => 'Test page',
        'content' => $faker->sentences($nb = 3, $asText = true),
        'locale' => 'en-GB',
        'entity' => 'page',
    ],
];