<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.05.2017
 * Time: 15:27
 */

use common\modules\store\models\Order;

$time = time();

return [
    [
        'payment_method_id' => '3',
        'email' => 'zxc-order-tester@test.com',
        'phone' => '0954150094',
        'total' => '999998',
        'created_at' => $time,
        'updated_at' => $time,
        'status' => Order::STATUS_TENDER_UNAPPROVED,
//        'full_name' => 'Андрей Витальевич test',
        'product_type' => Order::TYPE_SIMPLE_TENDER,
        'is_price_approved' => '1'
    ],
];