<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 19.07.2018
 * Time: 14:13
 */

$time = time();

return [
    [
        'created_at' => $time - 60 * 60,
        'updated_at' => $time - 60 * 60,
        'title' => 'test title portfolio for fixture',
        'description' => 'test description portfolio for fixture',
        'parent_category_id' => 2455,
        'category_id' => 2458,
        'subcategory_id' => null,
        'version' => 0,
        'status' => 30,
        'views_count' => 2,
        'likes_count' => 3,
        'locale' => 'en-GB',
    ],
];