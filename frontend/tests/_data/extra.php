<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 14.03.2017
 * Time: 16:18
 */

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => 'test title for extra fixture',
        'description' => $faker->sentences($nb = 3, $asText = true),
        'price' => '1234587',
        'currency_code' => 'RUB',
        'additional_days' => $faker->numberBetween($min = 1, $max = 10)
    ],
];