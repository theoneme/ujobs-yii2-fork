<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.07.2018
 * Time: 15:50
 */

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

return [
    [
        'title' => 'test company title',
        'content' => $faker->sentences($nb = 3, $asText = true),
        'locale' => 'ru-RU',
        'entity' => 'company'
    ],
];