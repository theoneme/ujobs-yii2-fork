<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 15.03.2017
 * Time: 16:36
 */

use common\models\Job;

$time = time();

return [
    [
        'created_at' => $time,
        'updated_at' => $time,
        'status' => '30',
        'price' => '999998',
        'currency_code' => 'RUB',
        'parent_category_id' => '79',
        'category_id' => '87',
        'type' => Job::TYPE_ADVANCED_TENDER,
        'period_type' => 'start',
        'date_start' => $time,
        'date_end' => '',
        'contract_price' => false,
        'percent_bonus' => '2%',
        'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка',
        'alias' => 'zxc-test-tender-1'
    ], [
        'created_at' => $time,
        'updated_at' => $time,
        'status' => '30',
        'price' => '0',
        'currency_code' => 'RUB',
        'parent_category_id' => '79',
        'category_id' => '87',
        'type' => Job::TYPE_TENDER,
        'period_type' => 'start',
        'date_start' => $time,
        'date_end' => '',
        'contract_price' => 'true',
        'requirements' => 'Заранее планируется расписание уборки test 5 лет опыта Пашка',
        'alias' => 'zxc-test-tender-2'
    ],
];