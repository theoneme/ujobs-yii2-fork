<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 02.05.2017
 * Time: 14:58
 */

$time = time();

return [
    [
        'created_at' => $time,
        'updated_at' => $time,
        'total' => '999998',
        'currency_code' => 'RUB',
        'status' => '10',
        'payment_method_id' => '3',
        'code' => null,
        'description' => 'zxc-test-payment',
        'type' => '1',
    ],
];