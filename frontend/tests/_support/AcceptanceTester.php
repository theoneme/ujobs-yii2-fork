<?php

namespace frontend\tests;

use Yii;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * @param $login
     * @param $password
     */
    public function loginAsUser($login, $password)
    {
        $I = $this;
        $I->maximizeWindow();
        $I->wait(1);
        $I->click(Yii::t('app', 'Sign in'));
        $I->wait(1);
        $I->fillField('login-form[login]', $login);
        $I->fillField('login-form[password]', $password);
        $I->dontSee('Необходимо заполнить «Логин».');
        $I->dontSee('Необходимо заполнить «Пароль».');
        $I->click('#login-form > button[type=submit]');
        $I->wait(2);
    }

    /**
     * @param $login
     * @param $username
     * @param $password
     */
    public function registerAsUser($login, $username, $password)
    {
        $I = $this;
        $I->maximizeWindow();
        $I->click(['link' => Yii::t('app', 'Post announcement for free')]);
        $I->wait(1);
        $I->fillField('register-form[login]', $login);
        //$I->click('#signup-form > button[type=submit]');
        //$I->wait(1);
        //$I->fillField('register-form[username]',$username);
        $I->fillField('register-form[password]', $password);
        $I->dontSee(Yii::t('app', 'Phone must contain from {0} numbers'));
        $I->dontSee(Yii::t('app', 'Password cannot be blank.'));
        $I->click('#signup-form > button[type=submit]');
        $I->wait(4);
    }
}
