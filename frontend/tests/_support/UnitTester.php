<?php

namespace frontend\tests;

use yii\helpers\Html;
use yii\mail\MessageInterface;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

    /* @var MessageInterface*/
    private $_lastMessage = null;

    /* @var MessageInterface[]*/
    private $_messages = null;


// Doesn't seem like mailer was actually sending anything to this 'mailtrap'. How was it working?

//    public function cleanMessages()
//    {
//        $this->client = new Client();
//        $inboxesId = $this->getInboxesId();
//        $this->client->createRequest()
//            ->setMethod('patch')
//            ->setUrl('https://mailtrap.io/api/v1/inboxes/' . $inboxesId . '/clean')
//            ->setHeaders(['Api-Token' => '49c56e93b12355dace3497f6e1b2d06d'])
//            ->setOptions([
//                'timeout' => 5,
//            ])
//            ->send();
//    }
//
//    public function getInboxesId()
//    {
//        $this->client = new Client();
//        $response = $this->client->createRequest()
//            ->setMethod('get')
//            ->setUrl('https://mailtrap.io/api/v1/inboxes/')
//            ->setHeaders(['Api-Token' => '49c56e93b12355dace3497f6e1b2d06d'])
//            ->setOptions([
//                'timeout' => 5,
//            ])
//            ->send();
//        $json_array = json_decode($response->getContent());
//        return $json_array[0]->{'id'};
//    }
//
//    public function getMessage()
//    {
//        $this->client = new Client();
//        $inboxesId = $this->getInboxesId();
//        $response = $this->client->createRequest()
//            ->setMethod('get')
//            ->setUrl('https://mailtrap.io/api/v1/inboxes/' . $inboxesId . '/messages')
//            ->setHeaders(['Api-Token' => '49c56e93b12355dace3497f6e1b2d06d'])
//            ->setOptions([
//                'timeout' => 5,
//            ])
//            ->send();
//        return json_decode($response->getContent(), true);
//    }

    public function cleanMessages()
    {
        $this->_messages = null;
    }

    public function getMessages()
    {
        if ($this->_messages === null) {
            $this->_messages = $this->grabSentEmails();
        }
        return $this->_messages;
    }

    /**
     * @return null|MessageInterface
     */
    public function getLastMessage()
    {
        if ($this->_lastMessage === null && $this->getMessages()) {
            $this->_lastMessage = $this->grabLastSentEmail();
        }
        return $this->_lastMessage;
    }

    public function getTo()
    {
        $result = null;
        $lastMessage = $this->getLastMessage();
        if ($lastMessage !== null) {
            $to = $lastMessage->getTo();
            $result = is_array($to) ? array_key_first($to) : $to;
        }
        return $result;
    }

    public function getFrom()
    {
        $result = null;
        $lastMessage = $this->getLastMessage();
        if ($lastMessage !== null) {
            $from = $lastMessage->getFrom();
            $result = is_array($from) ? array_key_first($from) : $from;
        }
        return $result;
    }

    public function getSubject()
    {
        $lastMessage = $this->getLastMessage();
        return $lastMessage ? $lastMessage->getSubject() : null;
    }

    public function getContent()
    {
        $lastMessage = $this->getLastMessage();
        return $lastMessage ? quoted_printable_decode($lastMessage->toString()) : null;
    }
}
