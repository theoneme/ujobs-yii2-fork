<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 20.03.2017
 * Time: 16:09
 */

namespace frontend\tests\functional;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobPackage as JobPackageFixture;
use common\fixtures\ContentTranslation as ContentTranslationJobFixture;
use common\fixtures\ModAttributeDescription as ModAttributeDescriptionFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Tariff as TariffFixture;
use common\fixtures\TariffRule as TariffRuleFixture;
use frontend\tests\FunctionalTester;
use common\models\Test;
use common\services\TestService;
use Yii;

class CartCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {

        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'package' => [
                'class' => JobPackageFixture::class,
                'dataFile' => codecept_data_dir() . 'package.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'tariff' => [
                'class' => TariffFixture::class,
                'dataFile' => codecept_data_dir() . 'tariff.php'
            ],
            'tariff_rule' => [
                'class' => TariffRuleFixture::class,
                'dataFile' => codecept_data_dir() . 'tariff_rule.php'
            ],
        ]);
        $I->amLoggedInAs($I->grabFixture('user', 1));

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkCart(FunctionalTester $I)
    {
        Yii::$app->language = 'ru-RU';

        $this->_testService->logTestResult('functional-cart', 'Доступ к корзине', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Services'));
        $I->click('a.bf-alias');
        $I->click('a.add-to-cart');
        $I->see(Yii::t('store', 'Checkout'));
        $I->click(Yii::t('store', 'Checkout'));
        $I->see(Yii::t('app', 'Payment methods'));

        $this->_testService->logTestResult('functional-cart', 'Доступ к корзине', Test::STATUS_SUCCESS);
    }
}