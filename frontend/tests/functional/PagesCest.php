<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.03.2017
 * Time: 18:03
 */

namespace frontend\tests\functional;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\ContentTranslation as ContentTranslationJobFixture;
use frontend\tests\FunctionalTester;
use common\models\Test;
use common\services\TestService;
use Yii;

class PagesCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkContactUnlogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-contact-unlogged', 'Доступ к странице контактов не зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Contact us'));
        $I->see(Yii::t('app', 'Submit'));

        $this->_testService->logTestResult('functional-contact-unlogged', 'Доступ к странице контактов не зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkContactLogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-contact-logged', 'Доступ к странице контактов зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->click(Yii::t('app', 'Contact us'));
        $I->see(Yii::t('app', 'Submit'));

        $this->_testService->logTestResult('functional-contact-logged', 'Доступ к странице контактов зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkStatFrilanseromUnlogged(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-stat-frilanserom-unlogged', 'Доступ к странице Стань Фрилансером не зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Become a freelancer'));
        $I->see(Yii::t('app', 'How it works'));
        $I->see(Yii::t('account', 'HISTORY OF OUR PROFESSIONALS'));

        $this->_testService->logTestResult('functional-stat-frilanserom-unlogged', 'Доступ к странице Стань Фрилансером не зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkStatFrilanseromLogged(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-stat-frilanserom-logged', 'Доступ к странице Стань Фрилансером зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->click(Yii::t('app', 'Become a freelancer'));
        $I->see(Yii::t('app', 'How it works'));
        $I->see(Yii::t('account', 'HISTORY OF OUR PROFESSIONALS'));

        $this->_testService->logTestResult('functional-stat-frilanserom-logged', 'Доступ к странице Стань Фрилансером зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkAutsorsingMenedgeraUnlogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-autsorsing-menedgera-unlogged', 'Доступ к странице Аутсорсинг Менеджера не зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Outsourcing of managers'));
        $I->see(Yii::t('app', 'WHAT OUR CLIENTS SAY'));
        $I->see(Yii::t('account', 'Q&A'));

        $this->_testService->logTestResult('functional-autsorsing-menedgera-unlogged', 'Доступ к странице Аутсорсинг Менеджера не зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkAutsorsingMenedgeraLogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-autsorsing-menedgera-logged', 'Доступ к странице Аутсорсинг Менеджера зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->click(Yii::t('app', 'Outsourcing of managers'));
        $I->see(Yii::t('app', 'WHAT OUR CLIENTS SAY'));
        $I->see(Yii::t('account', 'Q&A'));

        $this->_testService->logTestResult('functional-autsorsing-menedgera-logged', 'Доступ к странице Аутсорсинг Менеджера зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkNachatZarabativatUnlogged(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-nachat-zarabativat-unlogged', 'Доступ к странице Начать Зарабатывать не зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Increase freelancer`s salary'));
        $I->see(Yii::t('app', 'How it works'));
        $I->see(Yii::t('account', 'Q&A'));

        $this->_testService->logTestResult('functional-nachat-zarabativat-unlogged', 'Доступ к странице Начать Зарабатывать не зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkNachatZarabativatLogged(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-nachat-zarabativat-logged', 'Доступ к странице Начать Зарабатывать зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->click(Yii::t('app', 'Increase freelancer`s salary'));
        $I->see(Yii::t('app', 'How it works'));
        $I->see(Yii::t('account', 'Q&A'));

        $this->_testService->logTestResult('functional-nachat-zarabativat-logged', 'Доступ к странице Начать Зарабатывать зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkPageUnlogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-page-unlogged', 'Доступ к странице о Безопасной Сделке и подобным не зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Fair Play'));
        $I->see(Yii::t('app', 'Fair Play'));

        $this->_testService->logTestResult('functional-page-unlogged', 'Доступ к странице о Безопасной Сделке и подобным не зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkPageLogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-page-logged', 'Доступ к странице о Безопасной Сделке и подобным зарегистированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->click(Yii::t('app', 'Fair Play'));
        $I->see(Yii::t('app', 'Fair Play'));

        $this->_testService->logTestResult('functional-page-logged', 'Доступ к странице о Безопасной Сделке и подобным зарегистированных пользователей', Test::STATUS_SUCCESS);
    }
}

