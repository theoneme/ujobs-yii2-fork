<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.03.2017
 * Time: 16:19
 */

namespace frontend\tests\functional;

use common\fixtures\OrderProductPackage;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\ContentTranslation as ContentTranslationFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\JobPackage as JobPackageFixture;
use common\fixtures\ContentTranslation as ContentTranslationJobFixture;
use common\fixtures\Company as CompanyFixture;
use common\fixtures\ContentTranslationCompany as ContentTranslationCompanyFixture;
use common\fixtures\Order as OrderFixture;
use common\fixtures\OrderProductPackage as OrderProductPackageFixture;
use common\fixtures\Payment as PaymentFixture;
use frontend\tests\FunctionalTester;
use Yii;
use yii\helpers\Url;
use common\models\Test;
use common\services\TestService;

class AdminPageCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {

        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
        ]);

        $I->amLoggedInAs($I->grabFixture('user', 0));
//        $I->amOnRoute('site/index');
//        $I->amLoggedInAs($I->grabFixture('user', 0));

        // $devour = User::findByUsername('admin');
        //$I->amLoggedInAs('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    // First Column

    public function checkPostAnnouncement(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-post-announcement', 'Доступ к странице Разместить объявление', Test::STATUS_FAIL);

        $url = Url::to(['/entity/global-create']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('app', 'What announcement do you want to post?'), '.set-title');

        $this->_testService->logTestResult('functional-post-announcement', 'Доступ к странице Разместить объявление', Test::STATUS_SUCCESS);
    }

    public function checkCreateCompany(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-create-comp', 'Доступ к странице Создать компанию', Test::STATUS_FAIL);

        $url = Url::to(['/company/profile/form']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('model', 'Company Name'), '.set-title');

        $this->_testService->logTestResult('functional-create-comp', 'Доступ к странице Создать компанию', Test::STATUS_SUCCESS);
    }

    public function checkPostVideo(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-post-vid', 'Доступ к странице Опубликовать видео', Test::STATUS_FAIL);

        $url = Url::to(['/video/create']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('app', 'Select the language in which you want to post video '), '.set-title');

        $this->_testService->logTestResult('functional-post-vid', 'Доступ к странице Опубликовать видео', Test::STATUS_SUCCESS);
    }

    public function checkListOfMyServices(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-list-of-my-services', 'Доступ к странице Список моих услуг', Test::STATUS_FAIL);

        $url = Url::to(['/account/job/all']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('account', 'Manage Jobs'), 'h1');

        $this->_testService->logTestResult('functional-list-of-my-services', 'Доступ к странице Список моих услуг', Test::STATUS_SUCCESS);
    }

    public function checkOrdersOnMyServices(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'package' => [
                'class' => JobPackageFixture::class,
                'dataFile' => codecept_data_dir() . 'package.php'
            ],
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_package.php'
            ],
            'orderProduct' => [
                'class' => OrderProductPackageFixture::class,
                'dataFile' => codecept_data_dir() . 'order_product.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-order-on-my-services', 'Доступ к странице Заказы на мои услуги', Test::STATUS_FAIL);

        $url = Url::to(['/account/sell/all']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('account', 'Manage Sales'), 'h1');

        $this->_testService->logTestResult('functional-order-on-my-services', 'Доступ к странице Заказы на мои услуги', Test::STATUS_SUCCESS);
    }

    // Second Column

    public function checkPostRequest(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-post-request', 'Доступ к странице Опубликовать заявку', Test::STATUS_FAIL);

        $url = Url::to(['/entity/global-create?type=tender-service']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('app','What announcement do you want to post?'),'.set-title:first-child');

        $this->_testService->logTestResult('functional-post-request', 'Доступ к странице Опубликовать заявку', Test::STATUS_SUCCESS);
    }
    public function checkResponsesOnMyRequests(FunctionalTester $I)
    {
        $I->haveFixtures([
            'payment' => [
                'class' => PaymentFixture::class,
                'dataFile' => codecept_data_dir() . 'payment.php'
            ],
            'order' => [
                'class' => OrderFixture::class,
                'dataFile' => codecept_data_dir() . 'order_tender.php'
            ]
        ]);
        $I->amLoggedInAs($I->grabFixture('user', 1));
        $this->_testService->logTestResult('functional-responses-on-my-requests', 'Доступ к странице Отклики на мои заявки', Test::STATUS_FAIL);

        $url = Url::to(['/account/tender-response/responses-on-my-tenders']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('account', 'Responses on requests'), 'h1');

        $this->_testService->logTestResult('functional-responses-on-my-requests', 'Доступ к странице Отклики на мои заявки', Test::STATUS_SUCCESS);
    }

      // Third Column

    public function checkDashboard(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-dashboard', 'Доступ к Сводке', Test::STATUS_FAIL);

        $url = Url::to(['/account/service/dashboard']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->seeElement('.dashboards-tabs');

        $this->_testService->logTestResult('functional-dashboard', 'Доступ к Сводке', Test::STATUS_SUCCESS);
    }

    public function checkMessage(FunctionalTester $I)
    {
        $I->haveFixtures([
            'support_user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'support_user.php'
            ],
            'support_profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'support_profile.php'
            ],
        ]);

        $this->_testService->logTestResult('functional-message', 'Доступ к Сообщениям', Test::STATUS_FAIL);

        $I->amOnRoute('site/index');
        $url = Url::to(['/account/inbox']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('account', 'Inbox'), 'h1');
        $I->click(Yii::t('model', 'Read'), 'nav.profa-menu');
        $I->see(Yii::t('model', 'Read'), '.all-messages');
        $I->click(Yii::t('model', 'Unread'), 'nav.profa-menu');
        $I->see(Yii::t('model', 'Unread'), '.all-messages');
        $I->click(Yii::t('model', 'Order'), 'nav.profa-menu');
        $I->see(Yii::t('model', 'Order'), '.all-messages');
        $I->click(Yii::t('model', 'Starred'), 'nav.profa-menu');
        $I->see(Yii::t('model', 'Starred'), '.all-messages');
        $I->click(Yii::t('model', 'Deleted'), 'nav.profa-menu');
        $I->see(Yii::t('model', 'Deleted'), '.all-messages');
        $I->click(Yii::t('app', 'Contact Support'), 'nav.profa-menu');
        $I->seeElement('.message-block');

        $this->_testService->logTestResult('functional-message', 'Доступ к Сообщениям', Test::STATUS_SUCCESS);
    }

    public function checkNotifications(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-notifications', 'Доступ к Уведомлениям', Test::STATUS_FAIL);

        $url = Url::to(['/account/service/notifications']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->see(Yii::t('app','Notifications'),'.dashboards-tabs');

        $this->_testService->logTestResult('functional-notifications', 'Доступ к Уведомлениям', Test::STATUS_SUCCESS);
    }

    public function checkPayments(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-payments', 'Доступ к странице Баланса', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('account', 'My Balance'), '.nav-head-profile');
        $I->see(Yii::t('account', 'Manage Payments'), 'h1');

        $this->_testService->logTestResult('functional-payments', 'Доступ к странице Баланса', Test::STATUS_SUCCESS);
    }

    public function checkSettings(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-settings', 'Доступ к странице Настроек', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('account', 'Settings'), '.nav-head-profile');
        $I->see(Yii::t('account', 'Public Profile Settings'), 'h1');
        $I->click(Yii::t('account', 'Account Settings'));
        $I->see(Yii::t('account', 'How to contact you'), 'h1');
        $I->click(Yii::t('account', 'Payment Settings'));
        $I->see(Yii::t('account', 'Payment Information'), 'h1');
        $I->click(Yii::t('account', 'Social Network Settings'));
        $I->see(Yii::t('account', 'Social Network'), 'h1');
        $I->click(Yii::t('account', 'Account Actions'));
        $I->see(Yii::t('account', 'Account Actions'), 'h1');

        $this->_testService->logTestResult('functional-settings', 'Доступ к странице Настроек', Test::STATUS_SUCCESS);
    }

    public function checkReferralProgram(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-referral-program', 'Доступ к странице Реферальной программы', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'Invite Friends & Get Bonus'), '.nav-head-profile');
        $I->seeElement('.friends-bg');

        $this->_testService->logTestResult('functional-referral-program', 'Доступ к странице Реферальной программы', Test::STATUS_SUCCESS);
    }

    public function checkFavorites(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-favorites', 'Доступ к странице Избранного', Test::STATUS_FAIL);

        $url = Url::to(['/account/favourite/index']);
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click("a[href='{$url}']");
        $I->seeElement('.collection-header');

        $this->_testService->logTestResult('functional-favorites', 'Доступ к странице Избранного', Test::STATUS_SUCCESS);
    }

    // Forth Column

    public function checkComp(FunctionalTester $I)
    {
        $I->haveFixtures([
            'company' => [
                'class' => CompanyFixture::class,
                'dataFile' => codecept_data_dir() . 'company.php'
            ],
            'content_translation_company' => [
                'class' => ContentTranslationCompanyFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_company.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-comp', 'Доступ к странице компании', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click('test company title', 'a');
        $url = Url::to(['/account/favourite/index']);
        $I->dontSee("a[href='{$url}']",'.nav-head-profile');

        $this->_testService->logTestResult('functional-comp', 'Доступ к странице компании', Test::STATUS_SUCCESS);

        /*$I->seeElement('body');
        $I->click(Yii::t('account', 'Settings'), '.nav-head-profile');
        $I->see(Yii::t('account', 'Company Profile Settings'), 'h1');*/

    }
}