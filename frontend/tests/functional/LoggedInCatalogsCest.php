<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.03.2017
 * Time: 14:39
 */

namespace frontend\tests\functional;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Job as JobFixture;
use common\fixtures\ContentTranslation as ContentTranslationJobFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use common\fixtures\Product as ProductFixture;
use common\fixtures\ContentTranslationProduct as ContentTranslationProductFixture;
use common\fixtures\ModAttributeDescription as ModAttributeDescriptionFixture;
use common\fixtures\ModAttribute as ModAttributeFixture;
use common\fixtures\ModAttributeValue as ModAttributeValueFixture;
use common\fixtures\Currency as CurrencyFixture;
use common\fixtures\Tariff as TariffFixture;
use common\fixtures\TariffRule as TariffRuleFixture;
use common\models\Category;
use frontend\tests\FunctionalTester;
use common\models\Test;
use common\services\TestService;
use Yii;

class LoggedInCatalogsCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'attr' => [
                'class' => ModAttributeFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute.php'
            ],
            'attr_value' => [
                'class' => ModAttributeValueFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_value.php'
            ],
            'attr_descr' => [
                'class' => ModAttributeDescriptionFixture::class,
                'dataFile' => codecept_data_dir() . 'mod_attribute_description.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);
        $I->amOnRoute('site/index');
        $I->amLoggedInAs($I->grabFixture('user', 0));

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkServices(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        Yii::$app->language = 'ru-RU';
        $this->_testService->logTestResult('functional-service-catalog-logged', 'Доступ к каталогу услуг для зарегистрированных пользователей', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'uborka-i-pomoshch-po-khozyaystvu'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'podderzhivayushchaya-uborka'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('app', 'Services'));
        $I->click(Yii::t('app', 'Services'));
        $I->see(Yii::t('account', 'How the service works'), 'a');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$categoryTitle, 'h2');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$subCategoryTitle, 'h2');
        $I->click('a.bf-alias');
        $I->seeElement('.short-author');

        $this->_testService->logTestResult('functional-service-catalog-logged', 'Доступ к каталогу услуг для зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkProducts(FunctionalTester $I)
    {
        $I->haveFixtures([
            'product' => [
                'class' => ProductFixture::class,
                'dataFile' => codecept_data_dir() . 'product.php'
            ],
            'content_translation_product' => [
                'class' => ContentTranslationProductFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_product.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-product-catalog-logged', 'Доступ к каталогу товаро для зарегистрированных пользователейв', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'avto-i-moto'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'spetstekhnika'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('board', 'Products'));
        $I->click(Yii::t('app', 'Products'));
        $I->see(Yii::t('account', 'How the service works'), 'a');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$categoryTitle, 'h2');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$subCategoryTitle, 'h2');
        $I->click('a.bf-alias');
        $I->seeElement('.short-author');

        $this->_testService->logTestResult('functional-product-catalog-logged', 'Доступ к каталогу товаров для зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkPros(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'job.php'
            ],
            'content_translation_job' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
        ]);
        $this->_testService->logTestResult('functional-pro-catalog-logged', 'Доступ к каталогу Про для зарегистрированных пользователей', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'uborka-i-pomoshch-po-khozyaystvu'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'podderzhivayushchaya-uborka'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('app', 'Catalog of professionals'));
        $I->click(Yii::t('app', 'Catalog of professionals'));
        $I->see(Yii::t('account', 'How the service works'), 'a');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$categoryTitle, 'h2');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$subCategoryTitle, 'h2');
        $I->click('a.bf-alias');
        $I->seeElement('.worker-gigs');

        $this->_testService->logTestResult('functional-pro-catalog-logged', 'Доступ к каталогу Про для зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkArticles(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-article-catalog-logged', 'Доступ к каталогу статей для зарегистрированных пользователей', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'uborka-i-pomoshch-po-khozyaystvu'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'drugaya-pomosch'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('app', 'News feed'));
        $I->click(Yii::t('app', 'News feed'));
        $I->see(Yii::t('account', 'How the service works'), 'a');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$categoryTitle, 'h2');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$subCategoryTitle, 'h2');
//        $I->click('a.cmd-img');
//        $I->seeElement('h1.news-title');

        $this->_testService->logTestResult('functional-article-catalog-logged', 'Доступ к каталогу статей для зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkTenders(FunctionalTester $I)
    {
        $I->haveFixtures([
            'job' => [
                'class' => JobFixture::class,
                'dataFile' => codecept_data_dir() . 'tender.php'
            ],
            'content_translation' => [
                'class' => ContentTranslationJobFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation.php'
            ],
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php'
            ],
            'tariff' => [
                'class' => TariffFixture::class,
                'dataFile' => codecept_data_dir() . 'tariff.php'
            ],
            'tariff_rule' => [
                'class' => TariffRuleFixture::class,
                'dataFile' => codecept_data_dir() . 'tariff_rule.php'
            ],
        ]);
        Yii::$app->language = 'ru-RU';
        $this->_testService->logTestResult('functional-tender-catalog-logged', 'Доступ к каталогу заявок для зарегистрированных пользователей', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'uborka-i-pomoshch-po-khozyaystvu'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'drugaya-pomosch'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('app', 'Buyers requests'));
        $I->click(Yii::t('app', 'Buyers requests'));
        $I->see(Yii::t('account', 'How the service works'), 'a');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$categoryTitle, 'h2');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see(Yii::t('filter', 'All in').' '.$subCategoryTitle, 'h2');
        $I->click('a.bf-alias');
        $I->seeElement('.short-author');

        $this->_testService->logTestResult('functional-tender-catalog-logged', 'Доступ к каталогу заявок для зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }
}