<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 20.03.2017
 * Time: 16:50
 */

namespace frontend\tests\functional;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use common\fixtures\Video as VideoFixture;
use common\models\Category;
use frontend\tests\FunctionalTester;
use common\models\Test;
use common\services\TestService;
use Yii;

class VideoCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
            'video' => [
                'class' => VideoFixture::class,
                'dataFile' => codecept_data_dir() . 'video.php'
            ],
        ]);

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkVideoUnlogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-video-unlogged', 'Доступ к видео каталогу не зарегистированных пользователей', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'uborka-i-pomoshch-po-khozyaystvu'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'podderzhivayushchaya-uborka'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('app', 'Video Catalog'));
        $I->click(Yii::t('app', 'Video Catalog'));
        $I->seeElement('.banner-product h1');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see($categoryTitle, 'h1');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see($subCategoryTitle, 'h1');
        $I->click('a.bf-alias');
        $I->seeElement('.short-author');

        $this->_testService->logTestResult('functional-video-unlogged', 'Доступ к видео каталогу не зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkVideoLogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-video-logged', 'Доступ к видео каталогу зарегистированных пользователей', Test::STATUS_FAIL);

        $category = Category::find()->joinWith(['translations'])->where(['alias' => 'uborka-i-pomoshch-po-khozyaystvu'])->one();
        $categoryTitle = $category->translations[Yii::$app->language]->title;
        $subCategory = Category::find()->joinWith(['translations'])->where(['alias' => 'podderzhivayushchaya-uborka'])->one();
        $subCategoryTitle = $subCategory->translations[Yii::$app->language]->title;

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->seeLink(Yii::t('app', 'Video Catalog'));
        $I->click(Yii::t('app', 'Video Catalog'));
        $I->seeElement('.banner-product h1');
        $I->click($categoryTitle, '.aside-block .cats');
        $I->see($categoryTitle, 'h1');
        $I->click($subCategoryTitle, '.aside-block .cats');
        $I->see($subCategoryTitle, 'h1');
        $I->click('a.bf-alias');
        $I->seeElement('.short-author');

        $this->_testService->logTestResult('functional-video-logged', 'Доступ к видео каталогу зарегистированных пользователей', Test::STATUS_SUCCESS);
    }

}