<?php

namespace frontend\tests\functional;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use frontend\tests\FunctionalTester;
use common\models\Test;
use common\services\TestService;
use Yii;

class LoginCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);
        $I->amOnRoute('site/index');

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkValidLogin(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-login', 'Авторизация', Test::STATUS_FAIL);

        $I->submitForm('#login-form', $this->formParams('test_email_1@gmail.com', '222222', true));
        $I->see(Yii::t('app', 'You have successfully logged in'));
        $I->dontSeeLink(Yii::t('app', 'Login'));

        $this->_testService->logTestResult('functional-login', 'Авторизация', Test::STATUS_SUCCESS);
    }

    protected function formParams($login, $password, $rememberMe)
    {
        return [
            'login-form[login]' => $login,
            'login-form[password]' => $password,
            'login-form[rememberMe]' => $rememberMe,
        ];
    }
}
