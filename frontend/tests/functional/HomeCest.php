<?php

namespace frontend\tests\functional;

use frontend\tests\FunctionalTester;
use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use common\models\Test;
use common\services\TestService;
use Yii;

class HomeCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ],
        ]);

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkOpen(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-tariff', 'Доступ к странице тарифов', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->see(Yii::t('app', 'Why working with uJobs is profitable?'));
        $I->seeLink(Yii::t('app', 'Tariffs'));
        $I->click(Yii::t('app', 'Tariffs'));
        $I->see(Yii::t('app','Tariffs of uJobs'));

        $this->_testService->logTestResult('functional-tariff', 'Доступ к странице тарифов', Test::STATUS_SUCCESS);
    }
}