<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 17.03.2017
 * Time: 15:27
 */

namespace frontend\tests\functional;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use common\fixtures\Category as CategoryFixture;
use common\fixtures\ContentTranslationCategory as ContentTranslationCategoryFixture;
use common\fixtures\Page as PageFixture;
use common\fixtures\ContentTranslationPage as ContentTranslationPageFixture;
use frontend\tests\FunctionalTester;
use common\models\Test;
use common\services\TestService;
use Yii;

class NewsCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    function _before(FunctionalTester $I)
    {
        $I->haveFixtures([
            'page' => [
                'class' => PageFixture::class,
                'dataFile' => codecept_data_dir() . 'page.php'
            ],
            'content_translation_page' => [
                'class' => ContentTranslationPageFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_page.php'
            ],
            'category' => [
                'class' => CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'content_translation_category' => [
                'class' => ContentTranslationCategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'content_translation_category.php'
            ],
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ]);

        $this->_testService = new TestService(Test::GROUP_FUNCTIONAL);
    }

    public function checkNewsUnlogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-news-unlogged', 'Доступ к новостям для не зарегистрированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->click(Yii::t('app', 'News'), 'footer');
        $I->seeElement('.banner h1');
        $I->click('.category-main a.bf-alias');
        //$I->see(Yii::t('app','Read Next'));
        $I->seeElement('.more-news');

        $this->_testService->logTestResult('functional-news-unlogged', 'Доступ к новостям для не зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }

    public function checkNewsLogged(FunctionalTester $I)
    {
        $this->_testService->logTestResult('functional-news-logged', 'Доступ к новостям для зарегистрированных пользователей', Test::STATUS_FAIL);

        $I->amOnPage(\Yii::$app->homeUrl);
        $I->amLoggedInAs($I->grabFixture('user', 0));
        $I->click(Yii::t('app', 'News'), 'footer');
        $I->seeElement('.banner h1');
        $I->click('.category-main a.bf-alias');
        $I->seeElement('.more-news');

        $this->_testService->logTestResult('functional-news-logged', 'Доступ к новостям для зарегистрированных пользователей', Test::STATUS_SUCCESS);
    }
}