<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:37
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfileSpecialitiesCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfileSpecialities(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-profile-specialties', 'Редактирование профиля - специальности', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->registerAsUser('tester@mail.ru', 'tester', '111111');
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);

        /* Specialty test */

        $I->scrollTo(['css' => '#specialty-container'], 0, -500);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} specialties', ['count' => 10]));
        $I->wait(2);
        $I->scrollTo(['css' => '#specialty-container'], 0, -200);
        $I->click('#specialty-form_1 a.closeeditt');
        $I->click('#specialty-form_2 a.closeeditt');
        $I->click('#specialty-form_3 a.closeeditt');
        $I->click('#specialty-form_4 a.closeeditt');
        $I->click('#specialty-form_5 a.closeeditt');
        $I->click('#specialty-form_6 a.closeeditt');
        $I->click('#specialty-form_7 a.closeeditt');
        $I->click('#specialty-form_8 a.closeeditt');
        $I->click('#specialty-form_9 a.closeeditt');
        $I->wait(1);
        $I->dontSeeElement('#specialty-form_10');
        $I->dontSeeElement('#specialty-form_9');
        $I->dontSeeElement('#specialty-form_5');
        $I->dontSeeElement('#specialty-form_2');
        $I->dontSeeElement('#specialty-form_1');
        $I->scrollTo(['css' => '#specialty-container'], 0, -200);
        $I->fillField('UserSpecialtyForm[title]', '');
        $I->fillField('UserSpecialtyForm[salary]', '');
        $I->fillField('UserSpecialtyForm[title]', '');
        $I->wait(1);
        //$I->see('Необходимо заполнить «Attribute Id».');
        $I->seeElement('.field-specialty_input_0.has-error');
        $I->fillField('UserSpecialtyForm[title]', 'Программист');
        $I->click('#specialty-form_0 input[type="submit"]');
        $I->wait(1);
        //$I->dontSee('Необходимо заполнить «Attribute Id».');
        //$I->see('Необходимо заполнить «Зарплата».');
        $I->dontSeeElement('.field-specialty_input_0.has-error');
        $I->seeElement('.field-userspecialtyform-salary.has-error');
        $I->fillField('UserSpecialtyForm[salary]', '50000');
        $I->click('#specialty-form_0 input[type="submit"]');
        $I->wait(2);
        //$I->dontSee('Необходимо заполнить «Зарплата».');
        $I->dontSeeElement('.field-userspecialtyform-salary.has-error');
        $I->see('Программист - 50 000р.');
        $I->scrollTo(['css' => '#specialty-container'], 0, -500);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->fillField('UserSpecialtyForm[title]', 'Web-программист');
        $I->fillField('UserSpecialtyForm[salary]', '60000');
        $I->click('#specialty-form_1 input[type="submit"]');
        $I->wait(1);
        $I->see('Web-программист - 60 000р.');
        $I->click('#load-specialty');
        $I->wait(1);
        $I->fillField('UserSpecialtyForm[title]', 'Тестировщик');
        $I->fillField('UserSpecialtyForm[salary]', '40000');
        $I->click('#specialty-form_2 input[type="submit"]');
        $I->wait(1);
        $I->see('Тестировщик - 40 000р.');
        $I->moveMouseOver(['css' => '#specialty-listview .worker-info-item:nth-child(1)']);
        $I->wait(1);
        $I->click('#specialty-listview .worker-info-item:nth-child(1) a.specialty-delete');
        $I->wait(1);
        $I->click(['css' => '#cancelform']);
        $I->see('Программист - 50 000р.');
        $I->moveMouseOver(['css' => '#specialty-listview .worker-info-item:nth-child(1)']);
        $I->wait(1);
        $I->click('#specialty-listview .worker-info-item:nth-child(1) a.specialty-update');
        $I->wait(1);
        //$I->pressKey('#userspecialty-attribute_id-selectized', \Facebook\WebDriver\WebDriverKeys::BACKSPACE);
        $I->fillField('UserSpecialtyForm[title]', 'Верстальщик');
        $I->fillField('UserSpecialtyForm[salary]', '30000');
        $I->click('#specialty-container input[type="submit"]');
        $I->wait(1);
        $I->see('Верстальщик - 30 000р.');
        $I->moveMouseOver(['css' => '#specialty-listview .worker-info-item:nth-child(3)']);
        $I->click('#specialty-listview .worker-info-item:nth-child(3) a.specialty-update');
        $I->wait(1);
        $I->click('a.closeeditt');
        $I->wait(1);
        $I->see('Тестировщик - 40 000р.');
        $I->moveMouseOver(['css' => '#specialty-listview .worker-info-item:nth-child(1)']);
        $I->wait(1);
        $I->click('#specialty-listview .worker-info-item:nth-child(1) a.specialty-delete');
        $I->wait(1);
        $I->click(['css' => '#delete']);
        $I->wait(5);
        $I->dontSee('Верстальщик - 30 000р.');
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->dontSee(Yii::t('account', 'You can\'t set more than {count} specialties', ['count' => 10]));
        $I->click('#load-specialty');
        $I->wait(1);
        //$I->see(Yii::t('account', 'You can\'t set more than {count} specialties', ['count' => 10]));
        $I->wait(2);
        $I->fillField('UserSpecialtyForm[title]', 'Верстальщик');
        $I->fillField('#specialty-form_0  #userspecialty-salary', '30000');
        $I->click('#specialty-form_0 input[type="submit"]');
        //$I->fillField('.selectize-dropdown-content:nth-child(2) #userspecialty-attribute_id-selectized','Тест');
        //$I->wait(1);
        //$I->click('.selectize-dropdown-content:nth-child(2) #specialty-form_3 .selectize-dropdown-content div.option[data-value="4315"]');
        //$I->wait(1);
        //$I->fillField('.selectize-dropdown-content:nth-child(2) #specialty-form_3 UserSpecialtyForm[salary]','45000');
        //$I->click('.selectize-dropdown-content:nth-child(2) #specialty-form_3 input[type="submit"]');
        $I->wait(1);
        $I->see('Верстальщик - 30 000р.');
        //$I->see('Программист - 45 000р.');
        $I->scrollTo(['css' => '#specialty-container'], 0, -500);
        $I->click('#load-specialty');
        $I->wait(1);
        /*$I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);
        $I->click('#load-specialty');
        $I->wait(1);*/
        //$I->dontSee(Yii::t('account', 'You can\'t set more than {count} specialties', ['count' => 10]));
        $I->click('#load-specialty');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} specialties', ['count' => 10]));
        $model = user::findOne(['email' => 'tester@mail.ru']);
        $model->delete();

        $this->_testService->logTestResult('acceptance-profile-specialties', 'Редактирование профиля - специальности', Test::STATUS_SUCCESS);
    }
}