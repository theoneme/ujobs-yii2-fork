<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 23.03.2017
 * Time: 15:05
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class FillJobWithErrorCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkJobWithErrors(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-fill-job-with-error', 'Создание услуги с ошибками', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click('.prof-menu');
        $I->wait(1);
        $I->seeLink(Yii::t('account', 'Post announcement'));
        $I->click(['link' => Yii::t('account', 'Post announcement')]);
        $I->wait(1);
        $I->see(Yii::t('app', 'Select the language in which you want to create announcement'));
        $I->click('label[for="job"]');
        $I->click('.settings-content button[type="submit"]');
        $I->wait(2);
        // First Step
        $I->fillField('PostJobForm[title]', '');
        $I->fillField('.redactor-editor', '');
        $I->selectOption('PostJobForm[parent_category_id]', 'Помощь по хозяйству');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postjobform-category_id.has-error');
        $I->seeElement('.field-postjobform-title.has-error');
        $I->seeElement('.field-postjobform-description.has-error');
        $I->fillField('PostJobForm[title]', 'Выпечка тортов');
        $I->fillField('.redactor-editor', 'Приготовление и дизайн тортов любой сложности.');
        $I->selectOption('PostJobForm[category_id]', 'Приготовление еды');
        $I->click('#step1 button.next-step');
        $I->wait(2);
        // Second Step
        $I->click('.onoffswitch-label');
        $I->wait(1);
        $I->seeCheckboxIsChecked('#myonoffswitch');
        $I->fillField('JobPackage[basic][title]', '');
        $I->fillField('JobPackage[basic][description]', '');
        $I->fillField('JobPackage[basic][price]', '');
        $I->fillField('PostJobForm[execution_time]', '3');
        $I->click(Yii::t('app', 'Add Extra'));
        $I->wait(1);
        $I->click(Yii::t('app', 'Add Extra'));
        $I->wait(1);
        $I->click(Yii::t('app', 'Remove'), '.cgb-optbox:nth-child(1)');
        $I->wait(1);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->see(Yii::t('app', 'Your offers'));
        $I->fillField('JobPackage[basic][title]', 'Выпечка маленьких тортов');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->see(Yii::t('app', 'Your offers'));
        $I->fillField('JobPackage[basic][description]', 'Выпечка тортов до 1 кг');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->see(Yii::t('app', 'Your offers'));
        $I->fillField('JobPackage[basic][price]', '200');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->see(Yii::t('app', 'Your offers'));
        $I->click(Yii::t('app', 'Remove'), '.cgb-optbox:nth-child(2)');
        $I->wait(1);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->see(Yii::t('app', 'Your offers'));
        $I->fillField('JobPackage[standart][title]', 'Выпечка средних тортов');
        $I->fillField('JobPackage[standart][description]', 'Выпечка тортов до 2.5 кг');
        $I->fillField('JobPackage[standart][price]', '500');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->see(Yii::t('app', 'Your offers'));
        $I->fillField('JobPackage[premium][title]', 'Выпечка тортов на заказ');
        $I->fillField('JobPackage[premium][description]', 'Выпечка тортов на заказ');
        $I->fillField('JobPackage[premium][price]', '1500');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->fillField('PostJobForm[requirements]', 'Какие коржи, крем, личные пожелания.');
        $I->fillField('PostJobForm[address]', 'Донецк');
        $I->fillField('#tags-selectized', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки,');
        $I->seeInField('PostJobForm[tags]', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки');
        $I->click('#step3 button.next-step');
        $I->wait(2);
        $I->seeElement('.field-tags.has-error');
        $I->click('div[data-value="торт без выпечки"] a.remove');
        $I->click('#step3 button.next-step');
        $I->wait(2);
        // Forth Step
        $I->attachFile('#file-upload-input', '../images/cake1.png');
        $I->wait(2);
        $I->click('div[data-fileindex="0"] button.kv-file-remove');
        $I->wait(1);
        $I->click('#step5 button.next-step');
        $I->wait(2);
        // Fifth Step
        $I->click('#step6 button.prev-step');
        $I->wait(2);
        // Fourth Step
        $I->click('#step5 button.prev-step');
        $I->wait(1);
        // Third Step
        $I->click('#step3 button.prev-step');
        $I->wait(1);
        // Second Step
        $I->fillField('JobPackage[basic][included]', '2 коржа, крем');
        $I->fillField('JobPackage[basic][excluded]', 'Фигурки');
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->fillField('PostJobForm[title]', '');
        $I->selectOption('PostJobForm[parent_category_id]', '-- Выберите категорию');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postjobform-title.has-error');
        $I->seeElement('.field-postjobform-parent_category_id.has-error');
        $I->fillField('PostJobForm[title]', 'Выпечка тортов');
        $I->selectOption('PostJobForm[parent_category_id]', 'Помощь по хозяйству');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postjobform-category_id.has-error');
        $I->selectOption('PostJobForm[category_id]', 'Приготовление еды');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->click('.onoffswitch-label');
        $I->dontSeeCheckboxIsChecked('#myonoffswitch');
        /*$I->click('label[for=""]');
        $I->fillField('PostJobForm[units_amount]','');
        $I->selectOption('PostJobForm[measure_id]', array('value' => ''));
        $I->click('#step2 button.next-step');
        $I->seeElement('.field-postjobform-units_amount.has-error');
        $I->seeElement('.field-postjobform-measure_id.has-error');
        $I->fillField('PostJobForm[units_amount]','one');
        $I->selectOption('PostJobForm[measure_id]', array('value' => '1'));
        $I->click('#step2 button.next-step');
        $I->seeElement('.field-postjobform-units_amount.has-error');
        $I->fillField('PostJobForm[units_amount]','1');*/
        $I->click(Yii::t('app', 'Add Extra'));

        $I->wait(1);
        $I->fillField('JobExtra[2][title]', 'Свой дизайн');
        $I->fillField('JobExtra[2][price]', '500');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->fillField('#tags-selectized', '');
        //$I->click(['css' => '#step3 .load-faq']);

        $I->click(['link' => '+ '.Yii::t('app', 'Add FAQ')]);
        $I->wait(5);
        $I->fillField('JobQa[0][question]', 'Надо ли делать заказ заранее?');
        $I->fillField('JobQa[0][answer]', 'Да, очередь может быть');
        $I->click(Yii::t('app', 'Save'), '.faq-block:nth-child(1)');
        $I->wait(2);
        $I->click(Yii::t('app', 'Add FAQ'));
        $I->wait(1);
        $I->fillField('JobQa[1][question]', 'Торты свежие?');
        $I->fillField('JobQa[1][answer]', 'Да, делаются за день или в тот же день');
        $I->click(Yii::t('app', 'Save'), '.faq-block:nth-child(3)');
        $I->wait(2);
        $I->click('#step3 button.next-step');
        $I->wait(2);
        // Fourth Step
        $I->attachFile('#file-upload-input', '../images/cake1.png');
        $I->attachFile('#file-upload-input', '../images/cake2.png');
        $I->wait(2);
        $I->click('#step5 button.next-step');
        $I->wait(8);
        // Fifth Step
        $I->see(Yii::t('app', 'It\'s almost ready...'));
        $I->click(Yii::t('app', 'Publish job'));
        $I->wait(5);
        $I->see('Выпечка тортов');

        $this->_testService->logTestResult('acceptance-fill-job-with-error', 'Создание услуги с ошибками', Test::STATUS_SUCCESS);
    }
}