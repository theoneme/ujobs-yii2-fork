<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.04.2017
 * Time: 15:46
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class FillTenderCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkTender(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-fill-tender', 'Создание заявки', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click('.prof-menu');
        $I->wait(1);
        $I->seeLink(Yii::t('account', 'Post announcement'));
        $I->click(['link' => Yii::t('account', 'Post announcement')]);
        $I->wait(1);
        $I->see(Yii::t('app', 'Select the language in which you want to create announcement'));
        $I->click('label[for="tender-service"]');
        $I->click('.settings-content button[type="submit"]');
        $I->wait(2);
        // First Step
        $I->fillField('PostRequestForm[title]', '');
        $I->click('label[for="payment_1"]');
        $I->wait(1);
        //$I->seeOptionIsSelected('PostRequestForm[type]','advanced-tender');
        $I->selectOption('PostRequestForm[parent_category_id]', 'Помощь по хозяйству');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postrequestform-category_id.has-error');
        $I->seeElement('.field-postrequestform-title.has-error');
        $I->fillField('PostRequestForm[title]', 'Праздничный торт на заказ');
        $I->wait(1);
        $I->selectOption('PostRequestForm[category_id]', 'Приготовление еды');
        $I->wait(1);
        $I->fillField('#specialties-selectized', 'Пов');
        $I->wait(1);
        $I->click('.selectize-dropdown-content div.option[data-value="Повар"]');
        $I->wait(1);
        $I->seeInField('PostRequestForm[specialties]', 'Повар');
        $I->fillField('#specialties-selectized', 'Пов');
        $I->wait(1);
        $I->click('.selectize-dropdown-content div.option[data-value="Шеф-повар"]');
        $I->wait(1);
        $I->seeInField('PostRequestForm[specialties]', 'Повар,Шеф-повар');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->fillField('.redactor-editor', '');
        $I->selectOption('PostRequestForm[period_type]', 'Выполнить в период');
        $I->fillField('PostRequestForm[date_start]', '05-05-2017');
        $I->fillField('PostRequestForm[date_end]', '10-05-2017');
        $I->attachFile('#file-upload-input', '../images/cake1.png');
        $I->attachFile('#file-upload-input', '../images/cake2.png');
        $I->wait(2);
        $I->fillField('PostRequestForm[price]', '10000');
        $I->seeInField('#price-weekly', '2333');
        $I->fillField('PostRequestForm[percent_bonus]', '100.01');
        $I->click('label[for="contract-price-checkbox"]');
        $I->wait(1);
        //$I->seeCheckboxIsChecked('PostRequestForm[contract_price])');
        $I->seeInField('PostRequestForm[price]', '');
        $I->seeInField('#price-weekly', '0');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        //$I->see('Значение «Бонусный процент» не должно превышать 100.');
        $I->seeElement('.field-postrequestform-percent_bonus.has-error');
        $I->fillField('PostRequestForm[percent_bonus]', '99%');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        //$I->see('Значение «Бонусный процент» должно быть числом.');
        $I->seeElement('.field-postrequestform-percent_bonus.has-error');
        $I->fillField('PostRequestForm[percent_bonus]', '2.5');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        //$I->see('Необходимо заполнить «Описание».');
        $I->seeElement('.field-postrequestform-description.has-error');
        $I->fillField('.redactor-editor', 'Торт на заказ.');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('label[for="payment_0"]');
        $I->wait(1);
        //$I->seeOptionIsSelected('PostRequestForm[type]','tender');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->fillField('PostRequestForm[price]', '1000er');
        $I->wait(1);
        //$I->dontSeeCheckboxIsChecked('PostRequestForm[contract_price])');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        //$I->see('Значение «Цена» должно быть числом.');
        $I->fillField('PostRequestForm[price]', '10000');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->scrollTo(['css' => '#step3 button[type="submit"]'], 0, -200);
        $I->click('#step3 button.prev-step');
        $I->wait(1);
        // Second Step
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->scrollTo(['css' => '#step1 button.next-step'], 0, -200);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->scrollTo(['css' => 'label[for="contract-price-checkbox"]'], 0, -200);
        $I->click('label[for="contract-price-checkbox"]');
        $I->wait(1);
        //$I->seeCheckboxIsChecked('PostRequestForm[contract_price])');
        $I->seeInField('PostRequestForm[price]', '');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->click('button[type="submit"]');
        $I->wait(5);

        $this->_testService->logTestResult('acceptance-fill-tender', 'Создание заявки', Test::STATUS_SUCCESS);
    }
}