<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:38
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfilePortfolioCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfilePortfolio(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-portfolio', 'Создание портфолио', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);
        $I->click(['css' => 'a[href="/account/portfolio/create"]']);
        $I->wait(1);
        $I->see(Yii::t('app', 'Select the languages in which you want to create portfolio'));
        $I->click('.settings-content button[type="submit"]');
        $I->wait(2);
        // First Step
        $I->fillField('PostPortfolioForm[title]', '');
        $I->fillField('.redactor-editor', '');
        $I->selectOption('PostPortfolioForm[parent_category_id]', 'Программирование и IT');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postportfolioform-title.has-error');
        $I->seeElement('.field-postportfolioform-description.has-error');
        $I->seeElement('.field-postportfolioform-category_id.has-error');
        $I->fillField('PostPortfolioForm[title]', 'Сайт недвижимости');
        $I->wait(1);
        $I->selectOption('PostPortfolioForm[category_id]', 'Сайт под ключ');
        $I->wait(1);
        $I->fillField('.redactor-editor', 'Сайт с нуля под ключ');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->click(Yii::t('account','Add Extra Image'));
        $I->wait(1);
        $I->click(Yii::t('account','Add Extra Image'));
        $I->wait(1);
        $I->fillField('Attachment[0][description]', 'Изображение 1');
        $I->wait(1);
        $I->seeInField('Attachment[0][description]', 'Изображение 1');
        $I->scrollTo(['css' => '#step2 button.next-step'], 0, -200);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->see(Yii::t('account','Let`s publish your album.'));
        $I->click('#step3 button.prev-step');
        $I->wait(1);
        // Second Step
        $I->fillField('Attachment[1][description]', 'Изображение 2');
        $I->wait(1);
        $I->seeInField('Attachment[1][description]', 'Изображение 2');
        $I->click(Yii::t('account','Add Extra Image'));
        $I->wait(1);
        $I->click(Yii::t('app', 'Remove'), '.cgb-optbox:nth-child(2)');
        $I->wait(1);
        $I->dontSee('Изображение 2');
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->fillField('PostPortfolioForm[title]', '');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->fillField('PostPortfolioForm[title]', 'Сайт под ключ');
        $I->seeInField('PostPortfolioForm[title]', 'Сайт под ключ');
        $I->wait(1);
        $I->fillField('#tags-selectized', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки,');
        $I->seeInField('PostPortfolioForm[tags]', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки');
        $I->click('#step1 button.next-step');
        $I->wait(2);
        $I->seeElement('.field-tags.has-error');
        $I->click('div[data-value="торт без выпечки"] a.remove');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->click(Yii::t('account','Add Extra Image'));
        $I->wait(1);
        $I->fillField('Attachment[3][description]', 'Изображение 3');
        $I->wait(1);
        $I->seeInField('Attachment[3][description]', 'Изображение 3');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->click(Yii::t('account', 'Publish album'));
        $I->wait(5);

        $this->_testService->logTestResult('acceptance-portfolio', 'Создание портфолио', Test::STATUS_SUCCESS);
    }
}
