<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 28.04.2017
 * Time: 16:30
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class IndividualWorkerCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkIndividualWorker(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-individual-worker', 'Индвидуальный заказ - исполнитель', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->seeLink(Yii::t('app', 'Pros'));
        $I->click(Yii::t('app', 'Pros'));
        $I->wait(1);
        $I->see(Yii::t('seo','Catalog of professionals'), 'h1');
        $I->click('a.bf-alias');
        $I->wait(1);
        $I->seeElement('.worker-gigs');
        $I->click(Yii::t('account', 'Request service'));
        $I->wait(1);
        $I->see(Yii::t('account', 'Individual job'));
        $I->click('label[for="request_type_2"]');
        $I->wait(1);
        $I->fillField('Offer[price]', '');
        $I->fillField('Message[message]', '');
        $I->click(['css' => '#request-form button[type="submit"]']);
        $I->wait(1);
        $I->seeElement('.field-offer-price.has-error');
        $I->fillField('Offer[price]', '500.6');
        $I->click(['css' => '#request-form button[type="submit"]']);
        $I->wait(1);
        $I->seeElement('.field-message-message.has-error');
        $I->fillField('Message[message]', 'Какой-то текст2');
        $I->click(['css' => '#request-form button[type="submit"]']);
        $I->wait(1);
        $I->seeElement('.field-offer-price.has-error');
        $I->fillField('Offer[price]', '50');
        $I->click(['css' => '#request-form button[type="submit"]']);
        $I->wait(2);
        $I->see(Yii::t('account', 'Request successfully sent'));
        /*$I->wait(5);
        $I->click('a[href="/account/inbox"]');
        $I->wait(1);
        $I->see(Yii::t('account', 'Inbox'), 'h1');
        $I->click('Какой-то текст2');
        $I->wait(1);
        $I->see('Какой-то текст2');
        $I->see(Yii::t('account', 'Individual job offer description'));
        $I->see('Бюджет: 500 р.');
        $I->see(Yii::t('account', 'Your offer has been sent'));*/

        $this->_testService->logTestResult('acceptance-individual-worker', 'Индвидуальный заказ - исполнитель', Test::STATUS_SUCCESS);
    }
}