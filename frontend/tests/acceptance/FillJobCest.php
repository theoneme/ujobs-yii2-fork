<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.03.2017
 * Time: 14:58
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class FillJobCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkJobRight(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-fill-job', 'Создание услуги', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click('.prof-menu');
        $I->wait(1);
        $I->seeLink(Yii::t('account', 'Post announcement'));
        $I->click(['link' => Yii::t('account', 'Post announcement')]);
        $I->wait(1);
        // Languages
        $I->see(Yii::t('app', 'Select the language in which you want to create announcement'));
        $I->click('label[for="job"]');
        $I->click('.settings-content button[type="submit"]');
        $I->wait(2);
        // First Step
        $I->see(Yii::t('app', 'Russian'));
        $I->fillField('PostJobForm[title]', 'Выпечка тортов');
        $I->fillField('.redactor-editor', 'Приготовление и дизайн тортов любой сложности.');
        $I->selectOption('PostJobForm[parent_category_id]', 'Помощь по хозяйству');
        $I->wait(1);
        $I->selectOption('PostJobForm[category_id]', 'Приготовление еды');
        $I->click(['css' => '#step1 button.next-step']);
        $I->wait(1);
        // Second Step
        $I->click(['link' =>Yii::t('app', 'Try Now')]);
        $I->fillField('JobPackage[basic][title]', 'Выпечка маленьких тортов');
        $I->fillField('JobPackage[basic][description]', 'Выпечка тортов до 1 кг');
        $I->fillField('JobPackage[basic][included]', '2 коржа, крем');
        $I->fillField('JobPackage[basic][excluded]', 'Фигурки');
        $I->fillField('JobPackage[basic][price]', '200');
        $I->fillField('JobPackage[standart][title]', 'Выпечка средних тортов');
        $I->fillField('JobPackage[standart][description]', 'Выпечка тортов до 2.5 кг');
        $I->fillField('JobPackage[standart][included]', '3 коржа, крем, фрукты');
        $I->fillField('JobPackage[standart][excluded]', 'Фигурки');
        $I->fillField('JobPackage[standart][price]', '500');
        $I->fillField('JobPackage[premium][title]', 'Выпечка тортов на заказ');
        $I->fillField('JobPackage[premium][description]', 'Выпечка тортов на заказ');
        $I->fillField('JobPackage[premium][included]', 'Все пожелания');
        $I->fillField('JobPackage[premium][excluded]', 'Нет исключений');
        $I->fillField('JobPackage[premium][price]', '1500');
        $I->fillField('PostJobForm[execution_time]', '1');
        $I->click(Yii::t('app', 'Add Extra'));
        $I->wait(2);
        $I->fillField('JobExtra[0][title]', 'Фигурки');
        $I->fillField('JobExtra[0][price]', '200');
        $I->fillField('JobExtra[0][description]', 'Цена за одну фигурку');
        $I->wait(2);
        $I->click(Yii::t('app', 'Add Extra'));
        $I->wait(2);
        $I->fillField('JobExtra[1][title]', 'Свой дизайн');
        $I->fillField('JobExtra[1][price]', '500');
        $I->fillField('JobExtra[1][description]', 'Эксклюзивный дизайн');
        $I->click(Yii::t('app', 'Remove'), '.cgb-optbox:nth-child(1)');
        $I->wait(2);
        $I->click('#step2 button.next-step');
        $I->wait(2);
        // Third Step
        $I->fillField('PostJobForm[requirements]', 'Какие коржи, крем, личные пожелания.');
        $I->fillField('PostJobForm[address]', 'Донецк');
        $I->fillField('#tags-selectized', 'приготовление тортов,выпечка,выпечка тортов,');
        $I->seeInField('PostJobForm[tags]', 'приготовление тортов,выпечка,выпечка тортов');
        $I->click(Yii::t('app', 'Add FAQ'));
        $I->wait(1);
        $I->fillField('JobQa[0][question]', 'Надо ли делать заказ заранее?');
        $I->fillField('JobQa[0][answer]', 'Да, очередь может быть');
        $I->click(Yii::t('app', 'Save'), '.faq-block:nth-child(1)');
        $I->wait(2);
        $I->click(Yii::t('app', 'Add FAQ'));
        $I->wait(1);
        $I->fillField('JobQa[1][question]', 'Торты свежие?');
        $I->fillField('JobQa[1][answer]', 'Да, делаются за день или в тот же день');
        $I->click(Yii::t('app', 'Save'), '.faq-block:nth-child(3)');
        $I->wait(2);
        $I->click('.faq-block:nth-child(1)');
        $I->click(Yii::t('app', 'Delete'), '.faq-buttons');
        $I->click('#step3 button.next-step');
        $I->wait(2);
        // Fourth Step
        $I->attachFile('#file-upload-input', '../images/cake1.png');
        $I->attachFile('#file-upload-input', '../images/cake2.png');
        $I->wait(2);
        $I->click('div[data-fileindex="0"] button.kv-file-remove');
        $I->wait(1);
        $I->click('#step5 button.next-step');
        $I->wait(5);
        // Fifth Step
        $I->see(Yii::t('app', 'It\'s almost ready...'));
        $I->click(Yii::t('app', 'Publish job'));
        $I->wait(5);
        $I->see('Выпечка маленьких тортов');

        $this->_testService->logTestResult('acceptance-fill-job', 'Создание услуги', Test::STATUS_SUCCESS);
    }
}