<?php

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class HomeCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkHome(AcceptanceTester $I)
    {
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-home', 'Главная страница и страница тарифов', Test::STATUS_FAIL);

        Yii::$app->language = 'ru-RU';
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->seeLink(Yii::t('app', 'Tariffs'));
        $I->click(Yii::t('app', 'Tariffs'));
        $I->see(Yii::t('app', 'For freelancers and sellers'));

        $this->_testService->logTestResult('acceptance-home', 'Главная страница и страница тарифов', Test::STATUS_SUCCESS);
    }
}
