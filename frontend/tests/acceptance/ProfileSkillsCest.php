<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:36
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfileSkillsCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfileSkills(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-profile-skills', 'Редактирование профиля - навыки', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->registerAsUser('tester@mail.ru', 'tester', '111111');
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);


        /* Skill test */

        $I->scrollTo(['css' => '#skill-container'], 0, -500);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->see('Вы не можете указать более 10 навыков');
        $I->wait(2);
        $I->scrollTo(['css' => '#skill-container'], 0, -500);
        $I->click('#skill-form_1 a.closeeditt');
        $I->click('#skill-form_2 a.closeeditt');
        $I->click('#skill-form_3 a.closeeditt');
        $I->click('#skill-form_4 a.closeeditt');
        $I->click('#skill-form_5 a.closeeditt');
        $I->click('#skill-form_6 a.closeeditt');
        $I->click('#skill-form_7 a.closeeditt');
        $I->click('#skill-form_8 a.closeeditt');
        $I->click('#skill-form_9 a.closeeditt');
        $I->wait(1);
        $I->dontSeeElement('#skill-form_10');
        $I->dontSeeElement('#skill-form_9');
        $I->dontSeeElement('#skill-form_5');
        $I->dontSeeElement('#skill-form_2');
        $I->dontSeeElement('#skill-form_1');
        $I->scrollTo(['css' => '#skill-container'], 0, -500);
        $I->fillField('UserSkillForm[title]', '');
        $I->selectOption('UserSkillForm[level]', Yii::t('account', 'Beginner'));
        $I->wait(1);
        //$I->see('Необходимо заполнить «Название».');
        $I->seeElement('.field-skill_input_0.has-error');
        $I->selectOption('UserSkillForm[level]', '-- ' . Yii::t('account', 'Level of experience'));
        $I->fillField('UserSkillForm[title]', 'CSS');
        $I->click('#skill-form_0 input[type="submit"]');
        $I->wait(1);
        $I->dontSee('CSS - ' . Yii::t('account', 'Beginner'));
        //$I->dontSee('Необходимо заполнить «Название».');
        //$I->see('Необходимо заполнить «Уровень».');
        $I->dontSeeElement('.field-skill_input_0.has-error');
        $I->seeElement('.field-userskillform-level.has-error');
        $I->selectOption('UserSkillForm[level]', Yii::t('account', 'Expert'));
        $I->click('#skill-form_0 input[type="submit"]');
        $I->wait(2);
        //$I->dontSee('Необходимо заполнить «Уровень».');
        $I->dontSeeElement('.field-userskillform-level.has-error');
        //$I->see('CSS - '.Yii::t('account','Expert'));
        $I->see('CSS');
        $I->click('#load-skill');
        $I->wait(1);
        $I->fillField('UserSkillForm[title]', 'HTML');
        $I->selectOption('UserSkillForm[level]', Yii::t('account', 'Intermediate'));
        $I->click('#skill-form_1 input[type="submit"]');
        $I->wait(1);
        //$I->see('HTML - '.Yii::t('account','Intermediate'));
        $I->see('HTML');
        $I->click('#load-skill');
        $I->wait(1);
        $I->fillField('UserSkillForm[title]', 'jQuery');
        $I->selectOption('UserSkillForm[level]', Yii::t('account', 'Intermediate'));
        $I->click('#skill-form_2 input[type="submit"]');
        $I->wait(1);
        //$I->see('jQuery - '.Yii::t('account','Intermediate'));
        $I->see('jQuery');
        $I->moveMouseOver(['css' => '#skill-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#skill-listview .worker-info-item:nth-child(2) a.skill-delete');
        $I->wait(1);
        $I->click(['css' => '#cancelform']);
        //$I->see('HTML - '.Yii::t('account','Intermediate'));
        $I->see('HTML');
        $I->moveMouseOver(['css' => '#skill-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#skill-listview .worker-info-item:nth-child(2) a.skill-update');
        $I->wait(1);
        $I->fillField('UserSkillForm[title]', 'HTML');
        $I->selectOption('UserSkillForm[level]', Yii::t('account', 'Expert'));
        $I->click('#skill-container input[type="submit"]');
        $I->wait(1);
        //$I->see('HTML - '.Yii::t('account','Expert'));
        $I->see('HTML');
        $I->moveMouseOver(['css' => '#skill-listview .worker-info-item:nth-child(3)']);
        $I->click('#skill-listview .worker-info-item:nth-child(3) a.skill-update');
        $I->wait(1);
        $I->click('a.closeeditt');
        $I->wait(1);
        //$I->see('jQuery - '.Yii::t('account','Intermediate'));
        $I->see('jQuery');
        $I->moveMouseOver(['css' => '#skill-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#skill-listview .worker-info-item:nth-child(2) a.skill-delete');
        $I->wait(1);
        $I->click(['css' => '#delete']);
        $I->wait(5);
        $I->dontSee('HTML - ' . Yii::t('account', 'Expert'));
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        //$I->dontSee('Вы не можете указать более 10 навыков');
        $I->see(Yii::t('account', 'You can\'t set more than {count} skills', ['count' => 10]));
        $I->click('#load-skill');
        $I->wait(1);
        //$I->see('Вы не можете указать более 10 навыков');
        //$I->see(Yii::t('account', 'You can\'t set more than {count} skills', ['count' => 10]));
        $I->wait(2);
        $I->scrollTo(['css' => '#skill-container'], 0, -500);
        $I->fillField('#skill-form_0 input[name="UserSkillForm[title]"]', 'Yii2');
        $I->selectOption('#skill-form_0 select[name="UserSkillForm[level]"]', Yii::t('account', 'Beginner'));
        $I->click('#skill-form_0 input[type="submit"]');
        //$I->fillField('#skill-form_3 #userskill-title','HTML');
        //$I->selectOption('#skill-form_3 #userskill-level',Yii::t('account','Expert'));
        //$I->click('#skill-form_3 input[type="submit"]');
        $I->wait(1);
        //$I->see('Yii2 - '.Yii::t('account','Beginner'));
        $I->see('Yii2');
        //$I->see('HTML - '.Yii::t('account','Expert'));
        $I->scrollTo(['css' => '#skill-container'], 0, -500);
        $I->click('#load-skill');
        $I->wait(1);
        $I->click('#load-skill');
        $I->wait(1);
        /* $I->click('#load-skill');
         $I->wait(1);
         $I->click('#load-skill');
         $I->wait(1);
         $I->click('#load-skill');
         $I->wait(1);
         $I->click('#load-skill');
         $I->wait(1);*/
        //$I->dontSee('Вы не можете указать более 10 навыков');
        //$I->dontSee(Yii::t('account', 'You can\'t set more than {count} skills', ['count' => 10]));
        $I->click('#load-skill');
        $I->wait(1);
        $I->see('Вы не можете указать более 10 навыков');
        $I->see(Yii::t('account', 'You can\'t set more than {count} skills', ['count' => 10]));
        $model = user::findOne(['email' => 'tester@mail.ru']);
        $model->delete();

        $this->_testService->logTestResult('acceptance-profile-skills', 'Редактирование профиля - навыки', Test::STATUS_SUCCESS);
    }
}