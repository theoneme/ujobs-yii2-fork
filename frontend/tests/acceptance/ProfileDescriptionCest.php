<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:35
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfileDescriptionCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfileDescription(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-profile-description', 'Редактирование профиля - описание', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->registerAsUser('tester@mail.ru', 'tester', '111111');
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);

        /* Description test */

        $I->click(Yii::t('account', 'Edit Description'));
        $I->fillField('ContentTranslation[ru-RU][content]', '');
        $I->click('#description-form input[type="submit"]');
        $I->wait(1);
        $I->click(Yii::t('account', 'Edit Description'));
        $I->fillField('ContentTranslation[ru-RU][content]', 'Универсальный работник');
        $I->click('#description-form input[type="submit"]');
        $I->wait(2);
        $I->see('Универсальный работник', 'p.prof-descr');
        $I->click(Yii::t('account', 'Edit Description'));
        $I->fillField('ContentTranslation[ru-RU][content]', 'Делаю всё');
        $I->click(['css' => '#description-form a.closeedit']);
        $I->scrollTo(['css' => '#description-form'], 0, -200);
        $I->wait(1);
        $I->see('Универсальный работник', 'p.prof-descr');
        $I->click(Yii::t('account', 'Edit Description'));
        $I->fillField('ContentTranslation[ru-RU][content]', 'Делаю всё');
        $I->click('#description-form input[type="submit"]');
        $I->wait(2);
        $I->see('Делаю всё', 'p.prof-descr');
        $model = user::findOne(['email' => 'tester@mail.ru']);
        $model->delete();

        $this->_testService->logTestResult('acceptance-profile-description', 'Редактирование профиля - описание', Test::STATUS_SUCCESS);
    }
}
