<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 25.10.2018
 * Time: 17:14
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;
use yii\helpers\Url;

class DoVideoCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkVideo(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-video', 'Размещение видео', Test::STATUS_FAIL);

        $url = Url::to(['/video/create']);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click("a[href='{$url}']");
        $I->wait(2);
        $I->see(Yii::t('app', 'Select the language in which you want to post video'));
        $I->click('.settings-content button[type="submit"]');
        $I->wait(2);
        // First Step
        $I->fillField('PostVideoForm[title]', '');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postvideoform-title.has-error');
        $I->seeElement('.field-postvideoform-parent_category_id.has-error');
        $I->fillField('PostVideoForm[title]', 'Изучение английского');
        $I->selectOption('PostVideoForm[parent_category_id]', 'Обучение и курсы');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->dontSeeElement('.field-postvideoform-title.has-error');
        $I->dontSeeElement('.field-postvideoform-parent_category_id.has-error');
        $I->seeElement('.field-postvideoform-category_id.has-error');
        $I->selectOption('PostVideoForm[category_id]', 'Изучение языка');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        // Second Step
        $I->seeCheckboxIsChecked('PostVideoForm[free]');
        $I->scrollTo(['css' => '#step2 button.prev-step'], 0, -200);
        $I->wait(1);
        $I->click('label[for="free-price-checkbox"]');
        $I->fillField('PostVideoForm[price]', '500');
        $I->wait(1);
        $I->dontSeeCheckboxIsChecked('PostVideoForm[free]');
        $I->seeInField('PostVideoForm[price]', '500');
        $I->click('label[for="free-price-checkbox"]');
        $I->wait(1);
        $I->seeCheckboxIsChecked('PostVideoForm[free]');
        $I->dontSeeInField('PostVideoForm[price]', '500');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-video-input.has-error');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->click('label[for="free-price-checkbox"]');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-price.has-error');
        $I->click('label[for="free-price-checkbox"]');
        $I->wait(1);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->dontSeeElement('.field-price.has-error');
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->fillField('PostVideoForm[title]', '');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postvideoform-title.has-error');
        $I->wait(1);
        $I->fillField('PostVideoForm[title]', 'Изучение немецкого');
        $I->click('#step1 button.next-step');
        $I->wait(2);
        // Second Step
        $I->scrollTo(['css' => '#step2 button.prev-step'], 0, -200);
        $I->wait(1);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-video-input.has-error');


        $this->_testService->logTestResult('acceptance-video', 'Размещение видео', Test::STATUS_SUCCESS);
    }
}