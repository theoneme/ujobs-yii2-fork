<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 28.04.2017
 * Time: 13:42
 */

namespace frontend\tests\acceptance;

use common\fixtures\Profile as ProfileFixture;
use common\fixtures\User as UserFixture;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class DoTenderCest
{
    /**
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'profile' => [
                'class' => ProfileFixture::class,
                'dataFile' => codecept_data_dir() . 'profile.php'
            ]
        ];
    }

    /**
     * @var TestService
     */
    private $_testService = null;

    /**
     * @param AcceptanceTester $I
     */
    public function checkDoTender(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-do-tender', 'Заявка на работу', Test::STATUS_FAIL);

        $I->amOnPage('catalog-tender');
        $I->loginAsUser('test_email_1@gmail.com', '222222');

        $I->see(Yii::t('app', 'Request catalog'), 'h1');
        $I->wait(1);
        $I->click(['css' => 'a.bf-alias']);
        $I->seeElement('.short-author');

        $jobTitle = $I->grabTextFrom('.work-title');
        $I->click('.ap-buttons .btn-big');
        $I->wait(1);
        $I->seeElement('#PriceModal');
        $I->fillField('TenderResponseForm[price]', '');
        $I->fillField('TenderResponseForm[content]', '');
        $I->click(['css' => '#PriceModal button[type="submit"]']);
        $I->wait(1);
        $I->seeElement('.offer-price.has-error');
        $I->fillField('TenderResponseForm[price]', '500.6');
        $I->fillField('TenderResponseForm[content]', 'Какое-то крутое качество');
        $I->wait(1);
        $I->seeElement('.offer-price.has-error');
        $I->fillField('TenderResponseForm[price]', '199');
        $I->click(['css' => '#PriceModal button[type="submit"]']);
        $I->wait(1);
        $I->seeElement('.offer-price.has-error');
        $I->fillField('TenderResponseForm[price]', '2000');
        $I->click(['css' => '#PriceModal button[type="submit"]']);
        $I->wait(2);
        $I->see('Отклик отправлен');
        $I->wait(2);
        $I->click('#noti-id');
        $I->wait(1);
        $I->click('#notifications-container > a');
        $I->see($jobTitle);

        $this->_testService->logTestResult('acceptance-do-tender', 'Заявка на работу', Test::STATUS_SUCCESS);
    }
}