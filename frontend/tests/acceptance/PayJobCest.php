<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 27.04.2017
 * Time: 14:28
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;


class PayJobCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkPayJob(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-pay-job', 'Оплата', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->click(['link' => Yii::t('app', 'Services')]);
        $I->see(Yii::t('app', 'Catalog of services'), 'h1');
        $I->wait(1);
        $I->click('a.bf-alias');
        $I->wait(1);
        $I->seeElement('.short-author');
        $jobTitle = $I->grabTextFrom('.work-title');
        $I->click('.add-to-cart');
        $I->wait(1);
        $I->seeInSource($jobTitle);
        $jobPrice = $I->grabTextFrom('.cart-price');
        //$I->click('.cart-plus');
        //$I->see($jobPrice*2, '.price-total-sum');
        $I->see($jobPrice, '.price-total-sum');
        $I->click(['link' => Yii::t('store','Checkout')]);
        $I->wait(1);
        $I->see(Yii::t('store', 'Choose Payment Method'));
        $I->click(['link' => 'Оплата с баланса']);
        $I->wait(2);
        $I->see(Yii::t('app', 'Order is successfully paid'));

        $this->_testService->logTestResult('acceptance-pay-job', 'Оплата', Test::STATUS_SUCCESS);
    }
}