<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:37
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfileEducationsCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfileEducations(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-profile-education', 'Редактирование профиля - образование', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->registerAsUser('tester@mail.ru', 'tester', '111111');
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);

        /* Education test */

        $I->scrollTo(['css' => '#education-container'], 0, -500);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} educations', ['count' => 10]));
        $I->wait(2);
        $I->scrollTo(['css' => '#education-container'], 0, -200);
        $I->click('#education-form_1 a.closeeditt');
        $I->click('#education-form_2 a.closeeditt');
        $I->click('#education-form_3 a.closeeditt');
        $I->click('#education-form_4 a.closeeditt');
        $I->click('#education-form_5 a.closeeditt');
        $I->click('#education-form_6 a.closeeditt');
        $I->click('#education-form_7 a.closeeditt');
        $I->click('#education-form_8 a.closeeditt');
        $I->click('#education-form_9 a.closeeditt');
        $I->wait(1);
        $I->dontSeeElement('#education-form_10');
        $I->dontSeeElement('#education-form_9');
        $I->dontSeeElement('#education-form_5');
        $I->dontSeeElement('#education-form_2');
        $I->dontSeeElement('#education-form_1');
        $I->scrollTo(['css' => '#education-container'], 0, -200);
        $I->selectOption('UserEducation[country_title]', Yii::t('app', 'Ukraine'));
        $I->selectOption('UserEducation[country_title]', '-- ' . Yii::t('account', 'Country'));
        $I->fillField('UserEducation[title]', '');
        $I->fillField('UserEducation[degree]', '');
        $I->fillField('UserEducation[specialization]', '');
        $I->selectOption('UserEducation[year_start]', '2010');
        $I->selectOption('UserEducation[year_start]', '-- ' . Yii::t('account', 'Year Start'));
        $I->selectOption('UserEducation[year_end]', '2014');
        $I->selectOption('UserEducation[year_end]', '-- ' . Yii::t('account', 'Year End'));
        $I->wait(1);
        $I->seeElement('.field-usereducation-country_title.has-error');
        $I->seeElement('.field-usereducation-title.has-error');
        $I->seeElement('.field-usereducation-degree.has-error');
        $I->seeElement('.field-usereducation-specialization.has-error');
        $I->seeElement('.field-usereducation-year_start.has-error');
        //$I->see('Необходимо заполнить «Страна».');
        //$I->see('Необходимо заполнить «Название».');
        //$I->see('Необходимо заполнить «Степень».');
        //$I->see('Необходимо заполнить «Специализация».');
        //$I->see('Необходимо заполнить «Год начала».');
        $I->selectOption('UserEducation[country_title]', Yii::t('app', 'Ukraine'));
        $I->fillField('UserEducation[title]', 'ДонНТУ');
        $I->fillField('UserEducation[degree]', 'бакалавр');
        $I->fillField('UserEducation[specialization]', 'ИУС');
        $I->click('#education-form_0 input[type="submit"]');
        $I->wait(1);
        $I->dontSee('бакалавр - ИУС');
        $I->dontSee('ДонНТУ, Ukraine , 2010 - 2014');
        //$I->dontSee('Необходимо заполнить «Страна».');
        //$I->dontSee('Необходимо заполнить «Название».');
        //I->dontSee('Необходимо заполнить «Степень».');
        //$I->dontSee('Необходимо заполнить «Специализация».');
        //$I->see('Необходимо заполнить «Год начала».');
        $I->dontSeeElement('.field-usereducation-country_title.has-error');
        $I->dontSeeElement('.field-usereducation-title.has-error');
        $I->dontSeeElement('.field-usereducation-degree.has-error');
        $I->dontSeeElement('.field-usereducation-specialization.has-error');
        $I->seeElement('.field-usereducation-year_start.has-error');
        $I->selectOption('UserEducation[year_start]', '2010');
        $I->selectOption('UserEducation[year_end]', '2014');
        $I->click('#education-form_0 input[type="submit"]');
        $I->wait(2);
        //$I->dontSee('Необходимо заполнить «Год начала».');
        $I->dontSeeElement('.field-usereducation-year_start.has-error');
        $I->see('бакалавр - ИУС');
        $I->see('ДонНТУ, Ukraine , 2010 - 2014');
        $I->scrollTo(['css' => '#education-container'], 0, -500);
        $I->click('#load-education');
        $I->wait(1);
        $I->selectOption('UserEducation[country_title]', Yii::t('app', 'Russia'));
        $I->fillField('UserEducation[title]', 'ДПИ');
        $I->fillField('UserEducation[degree]', 'маг');
        $I->fillField('UserEducation[specialization]', 'иус');
        $I->selectOption('UserEducation[year_start]', '2013');
        $I->selectOption('UserEducation[year_end]', '2015');
        $I->click('#education-form_1 input[type="submit"]');
        $I->wait(1);
        $I->see('маг - иус');
        $I->see('ДПИ, Russia , 2013 - 2015');
        $I->click('#load-education');
        $I->wait(1);
        $I->selectOption('UserEducation[country_title]', Yii::t('app', 'Ukraine'));
        $I->fillField('UserEducation[title]', 'ЯППИ');
        $I->fillField('UserEducation[degree]', 'Intermediate');
        $I->fillField('UserEducation[specialization]', 'English');
        $I->selectOption('UserEducation[year_start]', '2012');
        $I->selectOption('UserEducation[year_end]', '2013');
        $I->click('#education-form_2 input[type="submit"]');
        $I->wait(1);
        $I->see('Intermediate - English');
        $I->see('ЯППИ, Ukraine , 2012 - 2013');
        $I->moveMouseOver(['css' => '#education-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#education-listview .worker-info-item:nth-child(2) a.edu-delete');
        $I->wait(1);
        $I->click(['css' => '#cancelform']);
        $I->see('маг - иус');
        $I->see('ДПИ, Russia , 2013 - 2015');
        $I->moveMouseOver(['css' => '#education-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#education-listview .worker-info-item:nth-child(2) a.edu-update');
        $I->wait(1);
        $I->selectOption('UserEducation[country_title]', Yii::t('app', 'Ukraine'));
        $I->fillField('UserEducation[title]', 'ДонНТУ');
        $I->fillField('UserEducation[degree]', 'магистр');
        $I->fillField('UserEducation[specialization]', 'ИУС');
        $I->selectOption('UserEducation[year_start]', '2014');
        $I->selectOption('UserEducation[year_end]', '2016');
        $I->click('#education-container input[type="submit"]');
        $I->wait(1);
        $I->see('магистр - ИУС');
        $I->see('ДонНТУ, Ukraine , 2014 - 2016');
        $I->moveMouseOver(['css' => '#education-listview .worker-info-item:nth-child(3)']);
        $I->click('#education-listview .worker-info-item:nth-child(3) a.edu-update');
        $I->wait(1);
        $I->click('a.closeeditt');
        $I->wait(1);
        $I->see('Intermediate - English');
        $I->see('ЯППИ, Ukraine , 2012 - 2013');
        $I->moveMouseOver(['css' => '#education-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#education-listview .worker-info-item:nth-child(2) a.edu-delete');
        $I->wait(1);
        $I->click(['css' => '#delete']);
        $I->wait(5);
        $I->dontSee('магистр - ИУС');
        $I->dontSee('ДонНТУ, Ukraine , 2014 - 2016');
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        //$I->see(Yii::t('account', 'You can\'t set more than {count} educations', ['count' => 10]));
        $I->click('#load-education');
        $I->wait(1);
        //$I->see(Yii::t('account', 'You can\'t set more than {count} educations', ['count' => 10]));
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(2);
        $I->scrollTo(['css' => '#education-container'], 0, -500);
        $I->selectOption('#education-form_0 #usereducation-country_title', Yii::t('app', 'Ukraine'));
        $I->fillField('#education-form_0 #usereducation-title', 'ДонНТУ');
        $I->fillField('#education-form_0 #usereducation-degree', 'магистр');
        $I->fillField('#education-form_0 #usereducation-specialization', 'ИУС');
        $I->selectOption('#education-form_0 #usereducation-year_start', '2014');
        $I->selectOption('#education-form_0 #usereducation-year_end', '2016');
        $I->scrollTo(['css' => '#education-container'], 0, -500);
        $I->click('#education-form_0 input[type="submit"]');
        //$I->selectOption('#education-form_1 #usereducation-country_title',Yii::t('app','Ukraine'));
        //$I->fillField('#education-form_1 #usereducation-title','Шервуд');
        //$I->fillField('#education-form_1 #usereducation-degree','Intermediate');
        //$I->fillField('#education-form_1 #usereducation-specialization','English');
        //$I->selectOption('#education-form_1 #usereducation-year_start','2013');
        //$I->selectOption('#education-form_1 #usereducation-year_end','2014');
        //$I->click('#education-form_1 input[type="submit"]');
        $I->wait(1);
        $I->scrollTo(['css' => '#education-container'], 0, -500);
        $I->see('магистр - ИУС');
        $I->see('ДонНТУ, Ukraine , 2014 - 2016');
        //$I->see('Intermediate - English');
        //$I->see('Шервуд, Ukraine , 2013 - 2014');
        $I->scrollTo(['css' => '#education-container'], 0, -500);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        /*$I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);
        $I->click('#load-education');
        $I->wait(1);*/
        //$I->dontSee('Вы не можете указать более 10 образований');
        $I->click('#load-education');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} educations', ['count' => 10]));
        $model = user::findOne(['email' => 'tester@mail.ru']);
        $model->delete();

        $this->_testService->logTestResult('acceptance-profile-education', 'Редактирование профиля - образование', Test::STATUS_SUCCESS);
    }
}