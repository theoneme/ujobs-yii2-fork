<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:47
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfileCertificatesCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfileCertificates(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-profile-certificates', 'Редактирование профиля - сертификаты', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->registerAsUser('tester@mail.ru', 'tester', '111111');
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);

        /* Certificate test */

        $I->scrollTo(['css' => '#certificate-container'], 0, -500);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} certificates', ['count' => 10]));
        $I->wait(2);
        $I->scrollTo(['css' => '#certificate-container'], 0, -200);
        $I->click('#certificate-form_1 a.closeeditt');
        $I->click('#certificate-form_2 a.closeeditt');
        $I->click('#certificate-form_3 a.closeeditt');
        $I->click('#certificate-form_4 a.closeeditt');
        $I->click('#certificate-form_5 a.closeeditt');
        $I->click('#certificate-form_6 a.closeeditt');
        $I->click('#certificate-form_7 a.closeeditt');
        $I->click('#certificate-form_8 a.closeeditt');
        $I->click('#certificate-form_9 a.closeeditt');
        $I->wait(1);
        $I->dontSeeElement('#certificate-form_10');
        $I->dontSeeElement('#certificate-form_9');
        $I->dontSeeElement('#certificate-form_5');
        $I->dontSeeElement('#certificate-form_2');
        $I->dontSeeElement('#certificate-form_1');
        $I->scrollTo(['css' => '#certificate-container'], 0, -200);
        $I->fillField('UserCertificate[title]', '');
        $I->fillField('UserCertificate[description]', '');
        $I->selectOption('#usercertificate-year', '2000');
        $I->wait(1);
        $I->seeElement('.field-usercertificate-title.has-error');
        $I->seeElement('.field-usercertificate-description.has-error');
        //$I->see('Необходимо заполнить «Название».');
        //$I->see('Необходимо заполнить «Описание».');
        $I->selectOption('UserCertificate[year]', '-- ' . Yii::t('account', 'Year'));
        $I->fillField('UserCertificate[title]', 'Сертификат1');
        $I->fillField('UserCertificate[description]', 'Adobe');
        $I->click('#certificate-form_0 input[type="submit"]');
        $I->wait(1);
        $I->dontSee('Сертификат1');
        $I->dontSee('Adobe - 2000');
        $I->dontSeeElement('.field-usercertificate-title.has-error');
        $I->dontSeeElement('.field-usercertificate-description.has-error');
        $I->seeElement('.field-usercertificate-year.has-error');
        //$I->dontSee('Необходимо заполнить «Название».');
        //$I->dontSee('Необходимо заполнить «Описание».');
        //$I->see('Необходимо заполнить «Год».');
        $I->selectOption('UserCertificate[year]', '2000');
        $I->click('#certificate-form_0 input[type="submit"]');
        $I->wait(2);
        $I->dontSee('Необходимо заполнить «Год».');
        $I->see('Сертификат1');
        $I->see('Adobe - 2000');
        $I->scrollTo(['css' => '#certificate-container'], 0, -500);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->fillField('UserCertificate[title]', 'Сертификат2');
        $I->fillField('UserCertificate[description]', 'CodeAcademy');
        $I->selectOption('UserCertificate[year]', '2002');
        $I->click('#certificate-form_1 input[type="submit"]');
        $I->wait(1);
        $I->see('Сертификат2');
        $I->see('CodeAcademy - 2002');
        $I->click('#load-certificate');
        $I->wait(1);
        $I->fillField('UserCertificate[title]', 'Сертификат3');
        $I->fillField('UserCertificate[description]', 'Microsoft');
        $I->selectOption('UserCertificate[year]', '2005');
        $I->click('#certificate-form_2 input[type="submit"]');
        $I->wait(1);
        $I->see('Сертификат3');
        $I->see('Microsoft - 2005');
        $I->moveMouseOver(['css' => '#certificate-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#certificate-listview .worker-info-item:nth-child(2) a.cert-delete');
        $I->wait(1);
        $I->click(['css' => '#cancelform']);
        $I->see('Сертификат2');
        $I->see('CodeAcademy - 2002');;
        $I->moveMouseOver(['css' => '#certificate-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#certificate-listview .worker-info-item:nth-child(2) a.cert-update');
        $I->wait(1);
        $I->fillField('UserCertificate[title]', 'Сертификат22222');
        $I->fillField('UserCertificate[description]', 'codeAcademy');
        $I->selectOption('UserCertificate[year]', '2001');
        $I->click('#certificate-container input[type="submit"]');
        $I->wait(1);
        $I->see('Сертификат22222');
        $I->see('codeAcademy - 2001');
        $I->moveMouseOver(['css' => '#certificate-listview .worker-info-item:nth-child(3)']);
        $I->click('#certificate-listview .worker-info-item:nth-child(3) a.cert-update');
        $I->wait(1);
        $I->click('a.closeeditt');
        $I->wait(1);
        $I->see('Сертификат3');
        $I->see('Microsoft - 2005');
        $I->moveMouseOver(['css' => '#certificate-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#certificate-listview .worker-info-item:nth-child(2) a.cert-delete');
        $I->wait(1);
        $I->click(['css' => '#delete']);
        $I->wait(5);
        $I->dontSee('Сертификат22222');
        $I->dontSee('codeAcademy - 2001');
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} certificates', ['count' => 10]));
        $I->click('#load-certificate');
        $I->wait(1);
        //$I->see('Вы не можете указать более 10 сертификатов');
        $I->wait(2);
        $I->scrollTo(['css' => '#certificate-container'], 0, -500);
        $I->fillField('#certificate-form_0 #usercertificate-title', 'Сертификат4');
        $I->fillField('#certificate-form_0 #usercertificate-description', 'Oracle');
        $I->selectOption('#certificate-form_0 #usercertificate-year', '1999');
        $I->click('#certificate-form_0 input[type="submit"]');
        //$I->fillField('#certificate-form_1 #usercertificate-title','Сертификат5');
        //$I->fillField('#certificate-form_1 #usercertificate-description','CodeAcademy');
        //$I->selectOption('#certificate-form_1 #usercertificate-year','2017');
        //$I->click('#certificate-form_1 input[type="submit"]');
        $I->wait(1);
        $I->see('Сертификат4');
        $I->see('Oracle - 1999');
        //$I->see('Сертификат5');
        //$I->see('CodeAcademy - 2017');
        $I->scrollTo(['css' => '#certificate-container'], 0, -500);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        /*$I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);
        $I->click('#load-certificate');
        $I->wait(1);*/
        //$I->dontSee('Вы не можете указать более 10 сертификатов');
        $I->click('#load-certificate');
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} certificates', ['count' => 10]));
        $model = user::findOne(['email' => 'tester@mail.ru']);
        $model->delete();

        $this->_testService->logTestResult('acceptance-profile-certificates', 'Редактирование профиля - сертификаты', Test::STATUS_SUCCESS);
    }
}