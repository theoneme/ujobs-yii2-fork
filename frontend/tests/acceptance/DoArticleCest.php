<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 23.10.2018
 * Time: 15:30
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class DoArticleCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkArticle(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-article', 'Создание статьи', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->seeLink(Yii::t('account', 'Post announcement'));
        $I->click(['link' => Yii::t('account', 'Post announcement')]);
        $I->wait(1);
        $I->see(Yii::t('app', 'Select the language in which you want to create announcement'));
        $I->click('label[for="article"]');
        $I->click('.settings-content button[type="submit"]');
        $I->wait(2);
        // First Step
        $I->fillField('ContentTranslation[ru-RU][title]', '');
        $I->fillField('.redactor-editor', '');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-contenttranslation-ru-ru-title.has-error');
        $I->seeElement('.field-contenttranslation-ru-ru-content.has-error');
        $I->fillField('ContentTranslation[ru-RU][title]', 'Статья');
        $I->wait(1);
        $I->fillField('.redactor-editor', 'Сайт с нуля под ключ');
        $I->wait(1);
        $I->seeInField('ContentTranslation[ru-RU][title]', 'Статья');
        $I->see('Сайт с нуля под ключ');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->selectOption('PostArticleForm[parent_category_id]', 'Программирование и IT');
        $I->wait(1);
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postarticleform-category_id.has-error');
        $I->selectOption('PostArticleForm[category_id]', 'Сайт под ключ');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->see(Yii::t('app', 'Publish article'));
        $I->click('#step3 button.prev-step');
        $I->wait(1);
        // Second Step
        $I->fillField('#tags-selectized', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки,');
        $I->wait(1);
        $I->seeInField('PostArticleForm[tags]', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки');
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->fillField('ContentTranslation[ru-RU][title]', '');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-contenttranslation-ru-ru-title.has-error');
        $I->fillField('ContentTranslation[ru-RU][title]', 'Сайт под ключ');
        $I->seeInField('ContentTranslation[ru-RU][title]', 'Сайт под ключ');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(2);
        // Second Step
        $I->click('#step2 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-tags.has-error');
        $I->click('div[data-value="торт без выпечки"] a.remove');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->click(Yii::t('app', 'Publish article'));
        $I->wait(5);

        $this->_testService->logTestResult('acceptance-article', 'Создание статьи', Test::STATUS_SUCCESS);
    }
}