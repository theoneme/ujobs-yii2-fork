<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 23.10.2018
 * Time: 16:44
 */

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;
use yii\helpers\Url;

class DoCompanyCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkCompany(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-company', 'Создание компании', Test::STATUS_FAIL);

        $url = Url::to(['/company/profile/form']);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->loginAsUser('devouryo@gmail.com', 'QF3nRuyF71UXoh6cKh0n');
        $I->wait(1);
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click("a[href='{$url}']");
        $I->wait(2);
        $I->see(Yii::t('model', 'Company Name'), '.set-title');
        // First Step
        $I->fillField('PostCompanyForm[title]', '');
        //$I->fillField('.redactor-editor', '');
        //PostCompanyForm[short_description]
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postcompanyform-title.has-error');
        $I->seeElement('.field-postcompanyform-description.has-error');
        $I->fillField('PostCompanyForm[title]', 'ЮДЖОБС');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->dontSeeElement('.field-postcompanyform-title.has-error');
        $I->seeElement('.field-postcompanyform-description.has-error');
        $I->fillField('PostCompanyForm[description]', 'Описание компании');
        $I->wait(1);
        $I->seeInField('PostCompanyForm[title]', 'ЮДЖОБС');
        $I->seeInField('PostCompanyForm[description]', 'Описание компании');
        $I->click('#step1 button.next-step');
        $I->wait(1);
        // Second Step
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->see(Yii::t('app', 'Publish company'));
        $I->click('#step3 button.prev-step');
        $I->wait(1);
        // Second Step
        $I->fillField('PostCompanyForm[address]', 'г. Неверленд, б. Шевченко, д. 55, кв. 55');
        $I->fillField('#activities-selectized', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки,');
        $I->wait(1);
        $I->seeInField('PostCompanyForm[activities]', 'приготовление тортов,выпечка,выпечка тортов,готовка,торты,торты на заказ,торты с кремом,торты большие,торты  маленькие,торты на меропритятие,торты с фруктами,бисквит,сметанный крем,молочный крем,сливочный крем,заварной крем,желейный торт,торты из печенья,торт без выпечки');
        $I->click('#step2 button.prev-step');
        $I->wait(1);
        // First Step
        $I->fillField('PostCompanyForm[title]', 'Очень длинный тайтл Очень длинный тайтл Очень длинный тайтл Очень длинный тайтл Очень длинный тайтл');
        $I->fillField('PostCompanyForm[short_description]', 'Очень длинное короткое описание Очень длинное короткое описание Очень длинное короткое описание
        Очень длинное короткое описание Очень длинное короткое описание Очень длинное короткое описание
        Очень длинное короткое описание Очень длинное короткое описание');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(1);
        $I->seeElement('.field-postcompanyform-title.has-error');
        $I->seeElement('.field-postcompanyform-short_description.has-error');
        $I->fillField('PostCompanyForm[title]', 'Очень длинный тайтл Очень длинный тайтл Очень длинный тайтл Очень длинный тайтл');
        $I->fillField('PostCompanyForm[short_description]', 'Очень длинное короткое описание Очень длинное короткое описание Очень длинное короткое описание
        Очень длинное короткое описание Очень длинное короткое описание Очень длинное короткое описание');
        $I->wait(1);
        $I->click('#step1 button.next-step');
        $I->wait(2);
        // Second Step
        $I->seeInField('PostCompanyForm[address]', 'г. Неверленд, б. Шевченко, д. 55, кв. 55');
        $I->click('#step2 button.next-step');
        $I->wait(1);
        // Third Step
        $I->click(Yii::t('app', 'Publish company'));
        $I->wait(5);

        $this->_testService->logTestResult('acceptance-company', 'Создание компании', Test::STATUS_SUCCESS);
    }
}