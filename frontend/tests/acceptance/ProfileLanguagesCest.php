<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 07.04.2017
 * Time: 11:36
 */

namespace frontend\tests\acceptance;

use common\models\user\User;
use frontend\tests\AcceptanceTester;
use Yii;
use common\models\Test;
use common\services\TestService;

class ProfileLanguagesCest
{
    /**
     * @var TestService
     */
    private $_testService = null;

    public function checkProfileLanguages(AcceptanceTester $I)
    {
        Yii::$app->language = 'ru-RU';
        $this->_testService = new TestService(Test::GROUP_ACCEPTANCE);

        $this->_testService->logTestResult('acceptance-profile-languages', 'Редактирование профиля - языки', Test::STATUS_FAIL);

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 3600;')->execute();
        $I->amOnPage(\Yii::$app->homeUrl);
        $I->registerAsUser('tester@mail.ru', 'tester', '111111');
        $I->click(['css' => '.prof-menu']);
        $I->wait(1);
        $I->click(['link' => Yii::t('account','View My Profile')]);

        /* Language test */

        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->see(Yii::t('account', 'You can\'t set more than {count} languages', ['count' => 10]));
        $I->wait(2);
        $I->scrollTo(['css' => '#language-container'], 0, -200);
        $I->click('#lang-form_3 a.closeeditt');
        $I->click('#lang-form_2 a.closeeditt');
        $I->click('#lang-form_1 a.closeeditt');
        $I->click('#lang-form_4 a.closeeditt');
        $I->click('#lang-form_5 a.closeeditt');
        $I->click('#lang-form_6 a.closeeditt');
        $I->click('#lang-form_7 a.closeeditt');
        $I->click('#lang-form_8 a.closeeditt');
        $I->click('#lang-form_9 a.closeeditt');
        $I->wait(1);
        $I->dontSeeElement('#lang-form_10');
        $I->dontSeeElement('#lang-form_9');
        $I->dontSeeElement('#lang-form_5');
        $I->dontSeeElement('#lang-form_2');
        $I->dontSeeElement('#lang-form_1');
        $I->scrollTo(['css' => '#language-container'], 0, -500);
        $I->fillField('UserLanguageForm[title]', '');
        $I->selectOption('UserLanguageForm[level]', Yii::t('account', 'Basic'));
        $I->wait(1);
        //$I->see('Необходимо заполнить «Язык».');
        $I->seeElement('.field-language_input_0.has-error');
        $I->selectOption('UserLanguageForm[level]', '-- ' . Yii::t('account', 'Proficiency'));
        $I->fillField('UserLanguageForm[title]', 'Английский');
        $I->click('#lang-form_0 input[type="submit"]');
        $I->wait(1);
        $I->dontSee('Английский - ' . Yii::t('account', 'Basic'));
        //$I->dontSee('Необходимо заполнить «Язык».');
        //$I->see('Необходимо заполнить «Уровень».');
        $I->dontSeeElement('.field-language_input_0.has-error');
        $I->seeElement('.field-userlanguageform-level.has-error');
        $I->selectOption('UserLanguageForm[level]', Yii::t('account', 'Conversational'));
        $I->click('#lang-form_0 input[type="submit"]');
        $I->wait(1);
        //$I->dontSee('Необходимо заполнить «Уровень».');
        $I->dontSeeElement('.field-userlanguageform-level.has-error');
        $I->see('Английский - ' . Yii::t('account', 'Conversational'));
        $I->scrollTo(['css' => '#language-container'], 0, -500);
        $I->click('#load-language');
        $I->wait(1);
        $I->fillField('UserLanguageForm[title]', 'русский');
        $I->selectOption('UserLanguageForm[level]', Yii::t('account', 'Fluent'));
        $I->click('#lang-form_1 input[type="submit"]');
        $I->wait(1);
        $I->see('русский - ' . Yii::t('account', 'Fluent'));
        $I->click('#load-language');
        $I->wait(1);
        $I->fillField('UserLanguageForm[title]', 'Украинский');
        $I->selectOption('UserLanguageForm[level]', Yii::t('account', 'Fluent'));
        $I->click('#lang-form_2 input[type="submit"]');
        $I->wait(1);
        $I->see('Украинский - ' . Yii::t('account', 'Fluent'));
        $I->moveMouseOver(['css' => '#language-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#language-listview .worker-info-item:nth-child(2) a.lang-delete');
        $I->wait(1);
        $I->click(['css' => '#cancelform']);
        $I->see('Русский - ' . Yii::t('account', 'Fluent'));
        $I->moveMouseOver(['css' => '#language-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#language-listview .worker-info-item:nth-child(2) a.lang-update');
        $I->wait(1);
        $I->fillField('UserLanguageForm[title]', 'Русский');
        $I->selectOption('UserLanguageForm[level]', Yii::t('account', 'Native'));
        $I->click('#language-container input[type="submit"]');
        $I->wait(1);
        $I->see('Русский - ' . Yii::t('account', 'Native'));
        $I->moveMouseOver(['css' => '#language-listview .worker-info-item:nth-child(3)']);
        $I->click('#language-listview .worker-info-item:nth-child(3) a.lang-update');
        $I->wait(1);
        $I->click('a.closeeditt');
        $I->wait(1);
        $I->see('Украинский - ' . Yii::t('account', 'Fluent'));
        $I->moveMouseOver(['css' => '#language-listview .worker-info-item:nth-child(2)']);
        $I->wait(1);
        $I->click('#language-listview .worker-info-item:nth-child(2) a.lang-delete');
        $I->wait(1);
        $I->click(['css' => '#delete']);
        $I->wait(5);
        $I->dontSee('Русский - ' . Yii::t('account', 'Native'));
        $I->click('#load-language');
        $I->wait(1);
        $I->click('#load-language');
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        $I->click(['css' => '#load-language']);
        $I->wait(1);
        //$I->dontSee('Вы не можете указать более 10 языков');
        $I->see(Yii::t('account', 'You can\'t set more than {count} languages', ['count' => 10]));
        $I->click('#load-language');
        $I->wait(1);
        //$I->see('Вы не можете указать более 10 языков');
        // становится 11 доступных языков из-за бага с удалением,
        // Андрей знает о чём я, но ни я ни вы не знаем как его исправить, забудем о нём.
        $I->wait(30);
        $I->scrollTo(['css' => '#language-container'], 0, -200);
        $I->fillField(['css' => '#lang-form_0 input[name="UserLanguageForm[title]"]'], 'Белорусский');
        $I->selectOption('#lang-form_0 select[name="UserLanguageForm[level]"]', Yii::t('account', 'Basic'));
        $I->click('#lang-form_0 input[type="submit"]');
        //$I->fillField('#lang-form_3 #userlanguage-language','Русский');
        //$I->selectOption('#lang-form_3 #userlanguage-level',Yii::t('account','Native'));
        //$I->click('#lang-form_3 input[type="submit"]');
        $I->wait(1);
        $I->see('Белорусский - ' . Yii::t('account', 'Basic'));
        //$I->see('Русский - '.Yii::t('account','Native'));
        //$I->scrollTo(['css' => '#language-container'], 0, -100);
        //$I->click(['css' => '#load-language']);
        //$I->wait(1);
        //$I->click(['css' => '#load-language']);
        //$I->wait(1);
        //$I->click(['css' => '#load-language']);
        //$I->wait(1);
        //$I->click(['css' => '#load-language']);
        //$I->wait(1);
        //$I->click(['css' => '#load-language']);
        //$I->wait(1);
        //$I->click(['css' => '#load-language']);
        //$I->wait(1);
        //$I->click('#load-language');
        //$I->wait(2);
        //$I->see('Вы не можете указать более 4 языков');
        $model = user::findOne(['email' => 'tester@mail.ru']);
        $model->delete();

        $this->_testService->logTestResult('acceptance-profile-languages', 'Редактирование профиля - языки', Test::STATUS_SUCCESS);
    }
}