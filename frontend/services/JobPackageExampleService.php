<?php

namespace frontend\services;

/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.04.2018
 * Time: 12:29
 */
class JobPackageExampleService
{
    /**
     * @var string
     */
    private $locale;

    /**
     * JobPackageExampleService constructor.
     * @param $locale
     */
    public function __construct($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $locale = $this->locale;
        $data = [];

        switch ($locale) {
            case 'en-GB':
                $data = [
                    [
                        'title' => 'Rough finishing',
                        'description' => 'Construction of houses on wireframe technology for up to 15 days. 3 bedrooms. House area - 98 m², including garage',
                        'included' => 'Package includes basic (rough) finish',
                        'excluded' => 'Decorative elements, textiles, laminate, flooring, furniture, household appliances',
                        'price' => 2775815,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Fine finishing',
                        'description' => 'Construction of houses on wireframe technology for up to 15 days. 3 bedrooms. House area - 98 m², including garage',
                        'included' => 'Fine finishing, laminate flooring, carpet in the bedrooms',
                        'excluded' => 'Furniture, textiles, decorative items, household appliances',
                        'price' => 2974085,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Turnkey construction',
                        'description' => 'Construction of houses on wireframe technology for up to 15 days. 3 bedrooms. House area - 98 m², including garage',
                        'included' => 'Finishing, furniture, household appliances, flooring',
                        'excluded' => 'Textiles, decorative elements (for example, curtains, paintings, vases)',
                        'price' => 3271495,
                        'currency_code' => 'RUB'
                    ],
                ];
                break;

            case 'es-ES':
                $data = [
                    [
                        'title' => 'Acabado áspero',
                        'description' => 'Construcción de casas en tecnología wireframe en 15 días. 3 habitaciones. Área de la casa - 98 m², incluido garaje',
                        'included' => 'El paquete incluye acabado básico (áspero)',
                        'excluded' => 'Elementos decorativos, textiles, laminado, pisos, muebles, electrodomésticos',
                        'price' => 2775815,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Acabado fino',
                        'description' => 'Construcción de casas en tecnología wireframe en 15 días. 3 habitaciones. Área de la casa - 98 m², incluido garaje',
                        'included' => 'Acabado fino, pisos laminados, alfombra en las habitaciones',
                        'excluded' => 'Muebles, textiles, artículos decorativos, electrodomésticos',
                        'price' => 2974085,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Construcción llave en mano',
                        'description' => 'Construcción de casas en tecnología wireframe en 15 días. 3 habitaciones. Área de la casa - 98 m², incluido garaje',
                        'included' => 'Acabado, muebles, electrodomésticos, suelos',
                        'excluded' => 'Textiles, elementos decorativos (por ejemplo, cortinas, pinturas, jarrones)',
                        'price' => 3271495,
                        'currency_code' => 'RUB'
                    ],
                ];
                break;

            case 'ru-RU':
                $data = [
                    [
                        'title' => 'Уборка квартиры до 40 м2',
                        'description' => 'Поддерживающая уборка квартиры до 50 м2.  ',
                        'included' => '- мытье полов
- мытье сантехники
- пылесос
- протирание пыли
- поливка цветов',
                        'excluded' => '- мытье окон (дополнительная опция)',
                        'price' => 1300,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Уборка квартиры до 100 м2',
                        'description' => 'Поддерживающая уборка квартиры до 100 м2',
                        'included' => '- мытье полов
- мытье сантехники
- пылесос
- протирание пыли
- поливка цветов',
                        'excluded' => '- мытье окон (дополнительная опция)',
                        'price' => 1800,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Уборка дома или квартиры более 100 м2',
                        'description' => 'Я осуществляю регулярную поддерживающую чистоту уборку квартиры или дома.  ',
                        'included' => '- мытье полов
- мытье сантехники
- пылесос
- протирание пыли
- поливка цветов',
                        'excluded' => '- мытье окон (дополнительная опция)',
                        'price' => 2700,
                        'currency_code' => 'RUB'
                    ],
                ];
                break;

            case 'uk-UA':
                $data = [
                    [
                        'title' => 'Прибирання квартири до 40 м²',
                        'description' => 'Сухе та вологе прибирання квартири площею до 40 м²',
                        'included' => '- миття пiдлоги
- миття сантехнiки
- чистка килимiв пилососом.',
                        'excluded' => 'Миття вiкон, прання (додаткова опцiя).',
                        'price' => 1300,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Прибирання квартири до 100 м²',
                        'description' => 'Сухе та вологе прибирання квартири площею до 100 м²',
                        'included' => '- миття пiдлоги
- миття сантехнiки
- чистка килимiв пилососом.',
                        'excluded' => 'Миття вiкон, прання (додаткова опцiя).',
                        'price' => 1800,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'Прибирання квартири, бiльшої за 100 м²',
                        'description' => 'Сухе та вологе прибирання квартири площею бiльше, нiж 100 м².',
                        'included' => '- миття пiдлоги
- миття сантехнiки
- чистка килимiв пилососом.',
                        'excluded' => 'Миття вiкон, прання (додаткова опцiя).',
                        'price' => 2700,
                        'currency_code' => 'RUB'
                    ],
                ];
                break;
            case 'ka-GE':
                $data = [
                    [
                        'title' => 'უხეში დასრულება',
                        'description' => 'სახლების მშენებლობას საკაბელო ტელევიზიაში 15 დღემდე. 3 საძინებელი. სახლის ფართი - 98 კვ / მ, ავტოფარეხის ჩათვლით',
                        'included' => 'პაკეტი მოიცავს ძირითად (უხეში) დასრულებას',
                        'excluded' => '- დეკორატიული ელემენტები
- ტექსტილი, ლამინატი
- იატაკი
- ავეჯი
- საყოფაცხოვრებო ნივთები',
                        'price' => 2775815,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'სახვითი დასასრული',
                        'description' => 'სახლების მშენებლობას საკაბელო ტელევიზიაში 15 დღემდე. 3 საძინებელი. სახლის ფართი - 98 კვ / მ, ავტოფარეხის ჩათვლით',
                        'included' => '- სახვითი დასრულება
- ლამინირებული იატაკი
- საძინებელი ხალიჩა',
                        'excluded' => '- ავეჯი
- ტექსტილი
- დეკორატიული ნივთები
- საყოფაცხოვრებო ნივთები',
                        'price' => 2974085,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => 'ანაზრაურების მშენებლობა',
                        'description' => 'სახლების მშენებლობას საკაბელო ტელევიზიაში 15 დღემდე. 3 საძინებელი. სახლის ფართი - 98 კვ / მ, ავტოფარეხის ჩათვლით',
                        'included' => '- დასრულება
- ავეჯი
- საყოფაცხოვრებო ნივთები
- იატაკი',
                        'excluded' => '- ტექსტილი
- დეკორატიული ელემენტები (მაგალითად, ფარდები, ნახატები, ვაზაები)',
                        'price' => 3271495,
                        'currency_code' => 'RUB'
                    ],
                ];
                break;

            case 'zh-CN':
                $data = [
                    [
                        'title' => '粗加工',
                        'description' => '在线框技术上建造房屋长达15天。 3间卧室。 房屋面积 - 98平方米，包括车库',
                        'included' => '套餐包括基本（粗糙）完成',
                        'excluded' => '- 装饰元素
- 纺织品，层压板
- 地板
- 家具
- 家用设备',
                        'price' => 2775815,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => '精加工',
                        'description' => '在线框技术上建造房屋长达15天。 3间卧室。 房屋面积 - 98平方米，包括车库',
                        'included' => '- 精加工
- 强化木地板
- 卧室内的地毯',
                        'excluded' => '- 家具
- 纺织品
- 装饰物品
- 家用设备',
                        'price' => 2974085,
                        'currency_code' => 'RUB'
                    ],
                    [
                        'title' => '交钥匙工程',
                        'description' => '在线框技术上建造房屋长达15天。 3间卧室。 房屋面积 - 98平方米，包括车库',
                        'included' => '- 整理
- 家具
- 家用设备
- 地板',
                        'excluded' => '- 纺织品
- 装饰元素（例如，窗帘，绘画，花瓶）',
                        'price' => 3271495,
                        'currency_code' => 'RUB'
                    ],
                ];
                break;
        }

        return $data;
    }
}