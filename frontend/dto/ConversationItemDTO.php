<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.12.2017
 * Time: 17:03
 */

namespace frontend\dto;

use common\models\Conversation;
use common\models\ConversationMember;
use Yii;
use yii\helpers\Html;

/**
 * Class HeaderMessageItemDTO
 * @package frontend\dto
 */
class ConversationItemDTO
{
    /**
     * @var Conversation
     */
    private $_conversation;

    private $fromId = null;

    /**
     * HeaderMessageItemDTO constructor.
     * @param Conversation $conversation
     */
    public function __construct(Conversation $conversation, int $fromId = null)
    {
        $this->_conversation = $conversation;
        $this->fromId = $fromId;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $conversation = $this->_conversation;
        $userId = $this->fromId ?? userId();
        $baseRoute = $userId === Yii::$app->params['supportId'] ? '/account/inbox/support-conversation' : '/account/inbox/conversation';

        $data = [
            'createdAt' => $conversation->lastMessage->created_at,
            'lastMessageId' => $conversation->lastMessage->id,
            'messageImage' => $conversation->lastMessage->user->getThumb('catalog'),
            'text' => $conversation->lastMessage->message,
            'starred' => $conversation->conversationMembers[$userId]->starred,
            'isDeleted' => $conversation->conversationMembers[$userId]->status === ConversationMember::STATUS_DELETED,
            'attachments' => array_map(function ($attachment) {
                $pathInfo = pathinfo($attachment->content);
                return [
                    'attachment' => $attachment->content,
                    'type' => in_array($pathInfo['extension'], ['jpg', 'jpeg', 'gif', 'png'])
                        ? 'image'
                        : 'link',
                    'filename' => $pathInfo['filename']
                ];
            }, $conversation->lastMessage->attachments),
            'conversationId' => $conversation->id,
            'route' => [$baseRoute, 'id' => $conversation->id],
            'type' => $conversation->type
        ];
        switch ($conversation->type) {
            case Conversation::TYPE_DEFAULT:
            case Conversation::TYPE_ORDER:
                $userMember = $conversation->conversationMembers[$userId];
                $targetMember = $conversation->getOtherMember($userId);
                if ($targetMember === null) {
                    return null;
                }
                $data['icon'] = $conversation->type === Conversation::TYPE_ORDER ? 'fa fa-money' : null;
                $data['image'] = $targetMember->user->getThumb('catalog');
                $data['name'] = $targetMember->user->getSellerName();
                $data['unread'] = hasAccess($conversation->lastMessage->user_id)
                    ? $targetMember->viewed_at > $conversation->lastMessage->created_at
                    : $userMember->viewed_at > $conversation->lastMessage->created_at;
                $data['isOnline'] = $targetMember->user->isOnline();
                break;
            case Conversation::TYPE_GROUP:
                $otherMembers = array_diff_key($conversation->conversationMembers, [$userId => 0]);
                $data['icon'] = 'fa fa-users';
                $data['image'] = $conversation->getThumb('catalog');
                $data['name'] = $conversation->getLabel();
                $data['unread'] = hasAccess($conversation->lastMessage->user_id)
                    ? array_reduce($otherMembers, function ($carry, $item) use ($conversation) {
                        return $carry || $item->viewed_at > $conversation->lastMessage->created_at;
                    }, false)
                    : $conversation->conversationMembers[$userId]->viewed_at > $conversation->lastMessage->created_at;
                $data['isOnline'] = false;
                break;
            case Conversation::TYPE_INFO:
                $data['icon'] = 'fa fa-info-circle';
                $data['image'] = $conversation->getThumb('catalog');
                $data['name'] = $conversation->getLabel();
                $data['unread'] = false;
                $data['isOnline'] = false;
                break;
        }

        return $data;
    }
}
