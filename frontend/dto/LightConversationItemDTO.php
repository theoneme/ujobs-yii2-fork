<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.04.2018
 * Time: 20:37
 */

namespace frontend\dto;

use common\models\Conversation;
use common\models\ConversationMember;
use Yii;

/**
 * Class LightConversationItemDTO
 * @package frontend\dto
 */
class LightConversationItemDTO
{
    /**
     * @var Conversation
     */
    private $_conversation;

    private $fromId = null;

    /**
     * HeaderMessageItemDTO constructor.
     * @param Conversation $conversation
     */
    public function __construct(Conversation $conversation, int $fromId = null)
    {
        $this->_conversation = $conversation;
        $this->fromId = $fromId;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $conversation = $this->_conversation;
        $userId = $this->fromId ?? userId();
//        $baseRoute = $userId === Yii::$app->params['supportId'] ? '/account/inbox/support-conversation' : '/account/inbox/conversation';

        $data = [
            'conversationId' => $conversation->id,
            'type' => $conversation->type,
            'isDeleted' => $conversation->conversationMembers[$userId]->status === ConversationMember::STATUS_DELETED,
            'route' => null
        ];

        switch ($conversation->type) {
            case Conversation::TYPE_DEFAULT:
            case Conversation::TYPE_ORDER:
                $targetMember = $conversation->getOtherMember($userId);
                if ($targetMember === null) {
                    return null;
                }
                $data['image'] = $targetMember->user->getThumb('catalog');
                $data['name'] = $targetMember->user->getSellerName();
                $data['isOnline'] = $targetMember->user->isOnline();
                $data['route'] = $targetMember->user->getSellerUrl();
                break;
            case Conversation::TYPE_GROUP:
            case Conversation::TYPE_INFO:
                $data['image'] = $conversation->getThumb('catalog');
                $data['name'] = $conversation->getLabel();
                $membersCount = ConversationMember::find()
                    ->where(['conversation_id' => $conversation->id, 'conversation_member.status' => ConversationMember::STATUS_ACTIVE])
                    ->count();
                $data['members'] = [Yii::t('messenger', '{count, plural, one{# member} other{# members}}', ['count' => $membersCount])];
                break;
        }

        return $data;
    }
}
