<?php

namespace frontend\components;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class ScrollPager
 * @package frontend\components
 */
class ScrollPager extends Widget
{
    /**
     * @var Pagination the pagination object that this pager is associated with.
     * You must set this property in order to make LinkPager work.
     */
    public $pagination;
    /**
     * @var array HTML attributes for the pager container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'scroll-pagination'];
    /**
     * @var string
     */
    public $containerSelector = '[data-scroll-container]';
    /**
     * @var string
     */
    public $itemSelector = '[data-scroll-item]';
    /**
     * @var string
     */
    public $loadUrl;

    /**
     * Initializes the pager.
     */
    public function init()
    {
        if ($this->pagination === null) {
            throw new InvalidConfigException('The "pagination" property must be set.');
        }
        if ($this->loadUrl === null) {
            throw new InvalidConfigException('The "loadUrl" property must be set.');
        }
    }

    /**
     * Executes the widget.
     */
    public function run()
    {
        echo $this->renderPager();
        $this->registerClientScript();
        $this->registerClientCss();
    }

    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPager()
    {
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        $this->options['class'] = ArrayHelper::getValue($this->options, 'class', 'scroll-pagination');
        if (strpos($this->options['class'], 'scroll-pagination') === false) {
            $this->options['class'] .= ' scroll-pagination';
        }
        return Html::tag($tag, '<div class="scroll-loading-bg"><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i></div>', $this->options);
    }

    /**
     * Register scroll script
     */
    protected function registerClientScript()
    {
        $pageSize = $this->pagination->getPageSize();
        $script = <<<JS
            (function() {
                let container = $('{$this->containerSelector}');
                container.data('offset', 0);
                let delay = (function() {
                    let timer = 0;
                    return function(callback, ms){
                        clearTimeout (timer);
                        timer = setTimeout(callback, ms);
                    };
                })();
                $(window).scroll(function() {
                    if (container.is(':visible') && !container.find('.scroll-pagination').is('.scroll-loading, .out-of-items')) {
                        let item = container.find('{$this->itemSelector}').last();
                        if(($(window).scrollTop() + $(window).height()) >= item.offset().top) {
                            container.find('.scroll-pagination').addClass('scroll-loading');
                            let offset = parseInt(container.data('offset')) + $pageSize;
                            delay(function(){
                                $.post('{$this->loadUrl}', 
                                    {limit: $pageSize, offset: offset},
                                    function(data) {
                                        if (data.length) {
                                            container.find('.scroll-pagination').before(data);
                                            container.data('offset', offset);
                                        }
                                        else {
                                            container.find('.scroll-pagination').addClass('out-of-items');
                                        }
                                        container.find('.scroll-pagination').removeClass('scroll-loading');
                                    }
                                );
                            }, 500);
                        }
                    }
                });
            }).call();
JS;

        $this->view->registerJs($script);
    }

    /**
     * Register scroll css
     */
    protected function registerClientCss()
    {
        $css = <<<CSS
            .scroll-pagination {
                display: none;
                width: 100%;
                height: 200px;
                position: relative;
            }
            .scroll-pagination.scroll-loading{
                display: block;
            }
            .scroll-loading-bg {
                position: absolute;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                opacity: 0.3;
                z-index: 4;
            }
            
            .scroll-loading-bg .fa-spinner {
                color: #acacac;
                position: absolute;
                left: 50%;
                top: 50%;
                margin: -40px 0 0 -40px;
                font-size: 80px;
                cursor: default;
            }
CSS;

        $this->view->registerCss($css);
    }
}
