<?php

namespace frontend\components\oauth;

use dektrium\user\clients\Facebook as BaseFacebook;

/**
 * Class Facebook
 * @package frontend\components\oauth
 */
class Facebook extends BaseFacebook
{
    /** @inheritdoc */
    public function getUsername()
    {
        return $this->getUserAttributes()['email'] ?? null;
    }
}
