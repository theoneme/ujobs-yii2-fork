<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.12.2016
 * Time: 19:46
 */

namespace frontend\components\oauth;

use dektrium\user\clients\Google as BaseGoogle;

/**
 * Class Google
 * @package frontend\components\oauth
 */
class Google extends BaseGoogle
{
    /** @inheritdoc */
    public function getUsername()
    {
        return isset($this->getUserAttributes()['emails'][0]['value']) ?? null;
    }
}
