<?php

namespace frontend\components;

use yii\base\BaseObject;
use yii\base\BootstrapInterface;

/**
 * Class LanguageSelector
 * @package frontend\components
 */
class LanguageSelector extends BaseObject implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $supportedLanguages = array_keys($app->params['languages']);
        $preferredLanguage = !empty($app->request->cookies['language']) && in_array($app->request->cookies['language'], $supportedLanguages)
            ? (string)$app->request->cookies['language']
//            : 'ru-RU';
            : $app->request->getPreferredLanguage($supportedLanguages);

        $app->language = $preferredLanguage;
    }
}