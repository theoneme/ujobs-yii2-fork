<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 15:18
 */

namespace frontend\components;

use yii\base\Widget;

/**
 * Class Breadcrumbs
 * @package frontend\components
 */
class Breadcrumbs extends Widget
{
    /**
     * @var string
     */
    public $template = 'breadcrumbs';
    /**
     * @var array
     */
    public $breadcrumbs = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->template, ['breadcrumbs' => $this->breadcrumbs]);
    }
}