<?php

namespace frontend\components;

use yii\base\Model;
use yii\base\Widget;

/**
 * Class GmapsInputWidget
 * @package frontend\components
 */
class GmapsInputWidget extends Widget
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var array
     */
    public $inputOptions = [];

    /**
     * @var bool
     */
    public $modal = true;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('gmaps-input-widget', ['model' => $this->model, 'inputOptions' => $this->inputOptions, 'modal' => $this->modal]);
    }
}