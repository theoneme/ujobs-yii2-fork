<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.12.2016
 * Time: 17:09
 */

namespace frontend\components\pro;

use common\models\Category;
use common\models\Job;
use common\models\user\Profile;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class RecommendedPros
 * @package frontend\components\pro
 */
class RecommendedPros extends Widget
{
    /**
     * @var Category
     */
    public $category;
    /**
     * @var array
     */
    public $similarCategories;
    /**
     * @var Job
     */
    public $exceptId;
    /**
     * @var integer
     */
    public $sliderCount = 4;

    /**
     * @var string
     */
    public $template = 'propositions';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->category !== null) {
            $categoryIds = null;
            if (!empty($this->similarCategories)) {
                $categoryIds = $this->similarCategories;
            } else if ($this->category->rgt - $this->category->lft === 1) {
                $categoryIds = $this->category->id;
            } else if ($this->category->lvl !== 0) {
                $categoryIds = $this->category->getChildrenIds();
            }

            $pros = Profile::find()
                ->joinWith(['translation', 'activeTariff', 'user'])
                ->where(['exists', Job::find()
                    ->select('job.id')
                    ->where('job.user_id = profile.user_id')
                    ->andWhere(['job.type' => 'job'])
                    ->andWhere(['job.status' => Job::STATUS_ACTIVE])
                    ->andFilterWhere(['job.category_id' => $categoryIds])
                ])
                ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
                ->andWhere(['not', ['gravatar_email' => '']])
                ->andWhere(['not', ['gravatar_email' => null]])
                ->andWhere(['not', ['bio' => '']])
                ->andWhere(['not', ['bio' => null]])
                ->andFilterWhere(['not', ['profile.id' => $this->exceptId]])
                ->orderBy('profile.user_id desc')
                ->groupBy('profile.user_id')
                ->limit(6);

            $dataProvider = new ActiveDataProvider([
                'query' => $pros,
                'pagination' => false
            ]);

            $template = (empty($this->similarCategories) || count($this->similarCategories) === 1)
                ? 'Recommended For You In Category {item}'
                : 'Recommended For You In Categories: {item}';

            $item = empty($this->similarCategories)
                ? Html::a($this->category->translation->title, Url::to(['/category/pros', 'category_1' => $this->category->translation->slug]), ['data-pjax' => 0])
                : implode(', ',
                    array_map(
                        function ($var) {
                            return Html::a($var['title'], Url::to(['/category/pros', 'category_1' => $var['slug']]), ['data-pjax' => 0]);
                        },
                        Category::find()->where(['category.id' => $this->similarCategories])->joinWith(['translation'])->select(['content_translation.title', 'content_translation.slug', 'category.id'])->limit(3)->asArray()->all()
                    )
                ) . (count($this->similarCategories) > 3 ? '...' : '');

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => Yii::t('app', $template, ['item' => $item]),
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}