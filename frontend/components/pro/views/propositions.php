<?php

use yii\widgets\ListView;

/** @var string $heading */
/** @var string $button */
/** @var integer $sliderCount */

$hash = uniqid();

?>
<?php if (!empty($button)) { ?>
    <div class="load-button-place text-center">
        <?= $button ?>
    </div>
<?php } ?>
    <div class="ws-block container-fluid">
        <div class="ws-title"><?= $heading ?></div>
        <div class="hidden-xs">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => function ($model) {
                    return $this->render('listview/proposition-item', ['model' => $model]);
                },
                'id' => "propositions-{$hash}",
                'layout' => '{items}',
                'options' => [
                    'tag' => 'div',
                    'class' => 'ws-content slickable-props'
                ],
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => 'slideitem-over'
                ],
            ]); ?>
        </div>
        <div class="visible-xs">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '@frontend/modules/filter/views/pro-list-mobile',
                'id' => "mobile-propositions-{$hash}",
                'itemOptions' => [
                    'class' => 'mob-work-item'
                ],
                'layout' => '{items}'
            ]) ?>
        </div>
    </div>

<?php $script = <<<JS
        $('.slickable-props').slick(new SlickConfig().getContentPropositionsConfig({$sliderCount}));
JS;
$this->registerJs($script);