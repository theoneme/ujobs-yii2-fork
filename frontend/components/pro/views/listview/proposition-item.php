<?php

use common\models\elastic\ProfileElastic;
use common\models\user\Profile;

/* @var Profile|ProfileElastic $model */

?>

<div class="bf-item">
    <?= $this->render('@frontend/modules/filter/views/pro-list', ['model' => $model]); ?>
</div>