<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.12.2016
 * Time: 17:09
 */

namespace frontend\components\pro;

use common\helpers\ElasticConditionsHelper;
use common\models\elastic\ProfileElastic;
use common\models\user\Profile;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\Html;

/**
 * Class SimilarPros
 * @package frontend\components\pro
 */
class SimilarPros extends Widget
{
    /**
     * @var string
     */
    public $request;
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var integer
     */
    public $sliderCount = 4;

    /**
     * @return string
     */
    public function run()
    {
        if ($this->request !== null) {
            /** @var ActiveQuery $pros */
            $pros = ProfileElastic::find()
                ->where(['status' => Profile::STATUS_ACTIVE])
                ->andWhere(['not', ['is_bio_empty' => true]])
                ->andWhere(['not', ['gravatar_email' => null]])
                ->addOrderBy('tariff.price desc')
                ->addOrderBy('created_at desc')
                ->limit(6);

            $pros->query([
                'bool' => [
                    'should' => ElasticConditionsHelper::getNestedSearchQuery('translations', ['translations.title'], $this->request),
                ]
            ]);

            $dataProvider = new ActiveDataProvider([
                'query' => $pros,
                'pagination' => false
            ]);

            $heading = Yii::t('app', 'Recommended on "{item}" request', [
                'item' => Html::a($this->request, ['/category/pro-catalog', 'request' => $this->request], ['data-pjax' => 0])
            ]);

            $button = Html::a(Yii::t('app', 'View more options on your request: "{item}"', ['item' => $this->request]),
                ['/category/pro-catalog', 'request' => $this->request],
                ['class' => 'load', 'data-pjax' => 0]
            );

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => $heading,
                'button' => $button,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}