<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.10.2017
 * Time: 10:00
 */

namespace frontend\components;

use Yii;
use yii\base\Widget;

/**
 * Class AdultPopup
 * @package common\modules\board\components
 */
class WelcomePopup extends Widget
{
    /**
     * @return null|string
     */
    public function run()
    {
        if (Yii::$app->session->get('showWelcome', false)) {
            Yii::$app->session->remove('showWelcome');
            return $this->render('welcome-popup');
        }
        return null;
    }
}