<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.12.2016
 * Time: 18:58
 */

namespace frontend\components;

use common\models\Notification;
use Yii;
use yii\base\Widget;

/**
 * Class Notifications
 * @package frontend\components
 */
class Notifications extends Widget
{
    /**
     * @var string
     */
    public $template = 'notifications';

    /**
     * @var null
     */
    public $unreadNotifications;

    /**
     * @return string
     */
    public function run()
    {
        if ($this->unreadNotifications === null) {
            $unreadNotifications = Notification::find()->where(['to_id' => Yii::$app->user->identity->getId(), 'is_read' => false, 'is_visible' => true])->select('id')->count();
        } else {
            $unreadNotifications = $this->unreadNotifications;
        }

        $notifications = [];

        return $this->render($this->template, [
            'notifications' => $notifications,
            'unreadNotifications' => $unreadNotifications
        ]);
    }
}