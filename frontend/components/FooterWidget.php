<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.06.2017
 * Time: 11:50
 */

namespace frontend\components;

use yii\base\Widget;
use Yii;

/**
 * Class FooterWidget
 * @package frontend\components
 */
class FooterWidget extends Widget
{
	/**
	 * @var string
	 */
	public $template = 'footer-widget';

	/**
	 * @return string
	 */
	public function run()
	{
	    $categories = [];
		$cacheLayer = Yii::$app->get('cacheLayer');
		if($cacheLayer !== null) {
            $categories = array_slice($cacheLayer->get('categories3_' . Yii::$app->language), 0, 9);
        }

		return $this->render($this->template, [
			'categories' => $categories
		]);
	}
}