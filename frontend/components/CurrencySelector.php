<?php

namespace frontend\components;

use common\models\Country;
use Exception;
use GeoIp2\Database\Reader;
use Yii;
use yii\base\BaseObject;
use yii\base\BootstrapInterface;
use yii\web\Application;

/**
 * Class CurrencySelector
 * @package frontend\components
 */
class CurrencySelector extends BaseObject implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $currency_code = strtoupper($app->request->get('app_currency_code'));
            if ($currency_code && isset($app->params['currencies'][$currency_code])) { //
                $app->params['app_currency_code'] = $currency_code;
                $app->session->set('app_currency_code', $currency_code);
                if (!Yii::$app->user->isGuest) {
                    $app->user->identity->updateAttributes(['site_currency' => $currency_code]);
                }
            } else if (!Yii::$app->user->isGuest && isset($app->params['currencies'][$app->user->identity->site_currency])) {
                $app->params['app_currency_code'] = $app->user->identity->site_currency;
            } else if (isset($app->params['currencies'][$app->session->get('app_currency_code')])) {
                $app->params['app_currency_code'] = $app->session['app_currency_code'];
            } else {
                $currency_code = 'RUB';
                try {
                    $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-Country.mmdb');

                    $ip = $app->request->getUserIP();
                    if ($ip !== '127.0.0.1') {
                        $country_code = $geoIpReader->country($ip)->country->isoCode;
                    }

                    if (!empty($country_code)) {
                        /* @var $country Country */
                        $country = Country::find()->where(['code' => $country_code])->one();
                        if ($country !== null && isset($app->params['currencies'][$country->currency_code])) {
                            $currency_code = $country->currency_code;
                        }
                    }
                } catch (Exception $e) {
                    Yii::warning("\n*******************\n" . $e->getMessage() . "\n*******************\n");
                }

                $app->params['app_currency_code'] = $currency_code;
                $app->session->set('app_currency_code', $currency_code);
            }
        }
    }
}