<?php

use common\models\Video;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Video */
/* @var $index integer */

?>

<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img($model->user->getThumb('catalog'),['alt' => $model->user->getSellerName(), 'title' => $model->user->getSellerName()]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'by') ?>
            <?= Html::a(Html::encode($model->user->getSellerName()), $model->user->getSellerUrl(), ['data-pjax' => 0]) ?>
        </div>
    </div>
</div>
<a class="bf-alias" data-pjax="0" href="<?= Url::to(['/video/view', 'alias' => $model->alias]) ?>">
    <div class="bf-img bf-video-img">
        <?= Html::img($model->getThumb(), [
            'alt' => $model->title,
            'title' => $model->title,
        ]) ?>
    </div>
    <div class="bf-descr">
        <?= Html::encode($model->title) ?>
    </div>
</a>
<div class="block-bottom">
    <div class="markers"></div>
    <?php echo Html::a($model->getPriceString(),
        ['/videos/view', 'alias' => $model->alias],
        ['class' => 'block-price', 'data-pjax' => 0]
    ) ?>
</div>