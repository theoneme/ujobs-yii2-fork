<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 15:01
 */

use common\models\Video;
use yii\helpers\Html;

/* @var $model Video */
/* @var $index integer */
/* @var $metaKeywords string */

?>

<a class="like" href="">
    <i class="fa fa-heart" aria-hidden="true"></i>
</a>
<div class="mob-work-table">
    <?= Html::a(
        Html::img($model->getThumb('catalog'), ['alt' => $model->title, 'title' => $model->title]),
        ['video/view', 'alias' => $model->alias],
        ['class' => 'mob-work-img', 'data-pjax' => 0]
    )?>

    <div class="mob-work-info">
        <?= Html::a(Html::encode($model->title),
            ['/video/view', 'alias' => $model->alias],
            ['class' => 'bf-descr', 'data-pjax' => 0]
        ) ?>
        <div class="markers"></div>

        <div class="mob-block-bottom">
            <div class="by"><?= Yii::t('app', 'by') ?>
                &nbsp;<?= Html::a(Html::encode($model->user->getSellerName()), $model->user->getSellerUrl()) ?>
            </div>
            <?= Html::a($model->getPriceString(),
                ['/video/view', 'alias' => $model->alias],
                ['class' => 'block-price', 'data-pjax' => 0]
            ) ?>
        </div>
    </div>
</div>