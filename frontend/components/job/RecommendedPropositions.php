<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.12.2016
 * Time: 17:09
 */

namespace frontend\components\job;

use common\models\Category;
use common\models\Job;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class RecommendedPropositions
 * @package frontend\components\job
 */
class RecommendedPropositions extends Widget
{
    /**
     * @var Category
     */
    public $category;

    /**
     * @var array
     */
    public $similarCategories;

    /**
     * @var Job
     */
    public $exceptId;

    /**
     * @var integer
     */
    public $sliderCount = 4;

    /**
     * @var string
     */
    public $template = 'propositions';

    /**
     * @var string
     */
    public $entity = 'job';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->category !== null) {
            $categoryIds = null;
            if (!empty($this->similarCategories)) {
                $categoryIds = $this->similarCategories;
            } else if ($this->category->rgt - $this->category->lft === 1) {
                $categoryIds = $this->category->id;
            } else if ($this->category->lvl !== 0) {
                $categoryIds = $this->category->getChildrenIds();
            }
            $items = Job::find()
                ->joinWith(['translation', 'packages', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->where(['job.type' => $this->entity, 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => Yii::$app->language])
                ->andFilterWhere(['category_id' => $categoryIds])
                ->andFilterWhere(['not', ['job.id' => $this->exceptId]])
                ->groupBy('job.id')
                ->orderBy('job.id desc')
                ->limit(6);

            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => false
            ]);

            $type = $this->entity === Job::TYPE_JOB ? Job::TYPE_JOB : Job::TYPE_TENDER;
            $template = (empty($this->similarCategories) || count($this->similarCategories) === 1)
                ? 'Recommended For You In Category {item}'
                : 'Recommended For You In Categories: {item}';
            $item = empty($this->similarCategories)
                ? Html::a($this->category->translation->title, Url::to(["/category/{$type}s", 'category_1' => $this->category->translation->slug]), ['data-pjax' => 0])
                : implode(', ',
                    array_map(
                        function ($var) use ($type) {
                            return Html::a($var['title'], Url::to(["/category/{$type}s", 'category_1' => $var['slug']]), ['data-pjax' => 0]);
                        },
                        Category::find()->where(['category.id' => $this->similarCategories])->joinWith(['translation'])->select(['category.id', 'content_translation.title', 'content_translation.slug'])->limit(3)->asArray()->all()
                    )
                ) . (count($this->similarCategories) > 3 ? '...' : '');

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => Yii::t('app', $template, ['item' => $item]),
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}