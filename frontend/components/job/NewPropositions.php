<?php

namespace frontend\components\job;

use common\models\Job;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class NewPropositions
 * @package frontend\components\job
 */
class NewPropositions extends Widget
{
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var string
     */
    public $entity = 'job';

    /**
     * @return string
     */
    public function run()
    {
        $items = Job::find()
            ->joinWith(['translation', 'packages', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
            ->where(['job.type' => $this->entity, 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => Yii::$app->language])
            ->groupBy('job.id')
            ->orderBy('job.id desc')
            ->limit(6);

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
            'pagination' => false
        ]);

        return $this->render($this->template, [
            'dataProvider' => $dataProvider,
            'heading' => Yii::t('app', 'New on site'),
            'sliderCount' => $this->sliderCount
        ]);
    }
}