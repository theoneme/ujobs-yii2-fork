<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.12.2016
 * Time: 11:59
 */

namespace frontend\components\job;

use common\models\Job;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class ViewedPropositions
 * @package frontend\components\job
 */
class ViewedPropositions extends Widget
{
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var string
     */
    public $entity = 'job';
    /**
     * @var Job
     */
    public $currentItem;
    /**
     * @var int
     */
    public $sliderCount = 4;

    /**
     * @return string
     */
    public function run()
    {
        $session = Yii::$app->session;
        $viewed = $session->get('viewed');
        $heading = null;

        if ($viewed !== null) {
            $items = Job::find()
                ->joinWith(['translation', 'packages', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->where(['job.type' => $this->entity, 'job.id' => $viewed, 'job.status' => Job::STATUS_ACTIVE])
                ->groupBy('job.id')
                ->limit(10);

            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => false
            ]);

            if($this->currentItem instanceof Job) {
                switch ($this->currentItem->type) {
                    case Job::TYPE_JOB:
                        $heading = Yii::t('app', 'Your Recently Viewed jobs');
                        break;
                    case Job::TYPE_TENDER:
                    case Job::TYPE_ADVANCED_TENDER:
                        $heading = Yii::t('app', 'Viewed requests');
                }
            }

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'currentItem' => $this->currentItem,
                'heading' => $heading,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}