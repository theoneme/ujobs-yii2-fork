<?php

namespace frontend\components\job;

use common\helpers\ElasticConditionsHelper;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\modules\store\models\Order;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class SimilarPropositions
 * @package frontend\components\job
 */
class SimilarPropositions extends Widget
{
    /**
     * @var string
     */
    public $request;

    /**
     * @var integer
     */
    public $sliderCount = 4;

    /**
     * @var string
     */
    public $template = 'propositions';

    /**
     * @var string
     */
    public $entity = 'job';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->request !== null) {
            /** @var ActiveQuery $jobs */
            $jobs = JobElastic::find()
                ->where([
                    'type' => $this->entity,
                    'status' => Job::STATUS_ACTIVE,
                    'locale' => Yii::$app->language
                ])
                ->addOrderBy('tariff.price desc')
                ->addOrderBy('created_at desc')
                ->limit(6);

            if ($this->entity !== Job::TYPE_JOB) {
                $jobs->query([
                    'bool' => [
                        'must' => [
                            [
                                'bool' => ElasticConditionsHelper::getNoOrdersQuery()
                            ]
                        ]
                    ]
                ]);
            }
            $jobs->query([
                'bool' => [
                    'must' => [
                        [
                            'bool' => [
                                'should' => array_merge(
                                    ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $this->request),
                                    ElasticConditionsHelper::getNestedSearchQuery('attrs', ['attrs.title'], $this->request)
                                )
                            ]
                        ],
                    ]
                ]
            ]);

            $dataProvider = new ActiveDataProvider([
                'query' => $jobs,
                'pagination' => false
            ]);

            $type = $this->entity === Job::TYPE_JOB ? Job::TYPE_JOB : Job::TYPE_TENDER;
            $heading = Yii::t('app', 'Recommended on "{item}" request', [
                'item' => Html::a($this->request, Url::to(["/category/{$type}-catalog", 'request' => $this->request]), ['data-pjax' => 0])
            ]);

            $button = Html::a(Yii::t('app', 'View more options on your request: "{item}"', ['item' => $this->request]),
                ["/category/{$type}-catalog", 'request' => $this->request],
                ['class' => 'load', 'data-pjax' => 0]
            );

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => $heading,
                'button' => $button,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}