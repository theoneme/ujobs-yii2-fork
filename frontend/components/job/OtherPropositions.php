<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.12.2016
 * Time: 17:09
 */

namespace frontend\components\job;

use common\models\Job;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class OtherPropositions
 * @package frontend\components\job
 */
class OtherPropositions extends Widget
{
    /**
     * @var integer
     */
    public $userId;
    /**
     * @var integer
     */
    public $currentItem;
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var string
     */
    public $entity = 'job';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->userId !== null && $this->currentItem !== null) {
            $items = Job::find()
                ->joinWith(['translation', 'packages', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->where(['job.user_id' => $this->userId, 'job.type' => $this->entity, 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => Yii::$app->language])
                ->andWhere(['not in', 'job.id', $this->currentItem])
                ->groupBy('job.id')
                ->limit(6);

            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => false
            ]);

            $heading = $this->entity === Job::TYPE_JOB
                ? Yii::t('app', 'Other Propositions By This User')
                : Yii::t('app', 'Other Requests From This User');

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => $heading,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}