<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.12.2016
 * Time: 17:16
 */

use common\models\elastic\JobElastic;
use common\models\Job;

/* @var Job|JobElastic $model */

?>

<div class="bf-item">
    <?= $this->render('@frontend/modules/filter/views/job-list', ['model' => $model]); ?>
</div>