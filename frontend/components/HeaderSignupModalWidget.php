<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\components;

use common\models\user\RegisterForm;
use yii\base\Widget;
use Yii;

/**
 * Class HeaderSignupModalWidget
 * @package frontend\components
 */
class HeaderSignupModalWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'header-signup-modal';

    /**
     * @return string
     */
    public function run()
    {
        $type = (int)Yii::$app->request->get('type');
        if($type !== RegisterForm::TYPE_REALTOR) {
            $type = null;
        }

        $registrationModel = Yii::createObject(RegisterForm::class);

        return $this->render($this->template, [
            'registrationModel' => $registrationModel,
            'type' => $type
        ]);
    }
}