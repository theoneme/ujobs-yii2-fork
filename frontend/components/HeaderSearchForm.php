<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 11:41
 */

namespace frontend\components;

use frontend\models\IndexSearchForm;
use yii\base\Widget;

/**
 * Class HeaderSearchForm
 * @package frontend\components
 */
class HeaderSearchForm extends Widget
{
    /**
     * @var string
     */
    public $template = 'header-search';

    /**
     * @return string
     */
    public function run()
    {
        $searchModel = new IndexSearchForm();

        return $this->render($this->template, ['searchModel' => $searchModel]);
    }
}