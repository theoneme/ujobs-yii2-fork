<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\components;

use yii\base\Widget;

/**
 * Class SocialShareWidget
 * @package frontend\components
 */
class SocialShareWidget extends Widget
{
    /**
     * @var array
     */
    public $mailItem;

    /**
     * Url to be shared
     * @var string
     */
    public $url;

    /**
     * Twitter message.
     * If not set - only link will be sent
     * @var string
     */
    public $twitterMessage;

    /**
     * @var boolean
     */
    public $showLink = false;

    /**
     * @return string
     */
    public function run()
    {
        $renderMailModal = false;
        if($this->mailItem !== null && array_key_exists('label', $this->mailItem)
            && array_key_exists('thumb', $this->mailItem)
            && !isGuest()) {
            $renderMailModal = true;
        }

        return $this->render('social-share-widget', [
            'url' => $this->url,
            'twitterMessage' => $this->twitterMessage,
            'renderMailModal' => $renderMailModal,
            'mailItem' => $this->mailItem,
            'showLink' => $this->showLink
        ]);
    }
}