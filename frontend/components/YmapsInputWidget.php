<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\components;

use yii\base\Model;
use yii\base\Widget;

/**
 * Class YmapsInputWidget
 * @package frontend\components
 */
class YmapsInputWidget extends Widget
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var array
     */
    public $inputOptions = [];

    /**
     * @var bool
     */
    public $modal = true;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ymaps-input-widget', ['model' => $this->model, 'inputOptions' => $this->inputOptions, 'modal' => $this->modal]);
    }
}