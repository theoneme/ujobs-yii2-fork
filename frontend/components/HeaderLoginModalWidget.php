<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\components;

use common\models\user\LoginForm;
use yii\base\Widget;
use Yii;

/**
 * Class HeaderLoginModalWidget
 * @package frontend\components
 */
class HeaderLoginModalWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'header-login-modal';

    /**
     * @return string
     */
    public function run()
    {
        $loginModel = Yii::createObject(LoginForm::class);

        return $this->render($this->template, ['loginModel' => $loginModel]);
    }
}