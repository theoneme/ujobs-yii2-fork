<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\components;

use dektrium\user\models\RecoveryForm;
use yii\base\Widget;
use Yii;

/**
 * Class HeaderRecoverModalWidget
 * @package frontend\components
 */
class HeaderRecoverModalWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'header-recover-modal';

    /**
     * @return string
     */
    public function run()
    {
        $model = Yii::createObject([
            'class'    => RecoveryForm::class,
            'scenario' => 'request',
        ]);

        return $this->render($this->template, ['model' => $model]);
    }
}