<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.10.2016
 * Time: 12:19
 */

namespace frontend\components;

use Yii;
use yii\web\View;

/**
 * Class OpenGraph
 * @package frontend\components
 */
class OpenGraph
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $site_name;
    /**
     * @var mixed|string
     */
    public $url;
    /**
     * @var string
     */
    public $description;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $locale;
    /**
     * @var null|string
     */
    public $image;
    /**
     * @var
     */
    public $app_id;

    /**
     * OpenGraph constructor.
     */
    public function __construct()
    {
        if (!Yii::$app->request->isAjax) {
            $this->title = Yii::$app->name;
            $this->site_name = Yii::$app->name;
            $this->url = Yii::$app->request->absoluteUrl;
            $this->description = null;
            $this->type = 'article';
            $this->locale = str_replace('-', '_', Yii::$app->language);
            $this->image = null;

            Yii::$app->view->on(View::EVENT_BEGIN_PAGE, function () {
                Yii::$app->controller->view->registerMetaTag(['property' => 'og:title', 'content' => $this->title], 'og:title');
                Yii::$app->controller->view->registerMetaTag(['property' => 'og:site_name', 'content' => $this->site_name], 'og:site_name');
                Yii::$app->controller->view->registerMetaTag(['property' => 'og:url', 'content' => $this->url], 'og:url');
                Yii::$app->controller->view->registerMetaTag(['property' => 'og:type', 'content' => $this->type], 'og:type');

                Yii::$app->controller->view->registerMetaTag(['property' => 'og:locale', 'content' => $this->locale], 'og:locale');
                Yii::$app->controller->view->registerMetaTag(['property' => 'fb:app_id', 'content' => Yii::$app->params['fb_appid']], 'fb:app_id');

                if ($this->description !== null) {
                    Yii::$app->controller->view->registerMetaTag(['property' => 'og:description', 'content' => $this->description], 'og:description');
                }

                if ($this->image !== null) {
                    $plain = str_replace('https://', 'http://', $this->image);

                    Yii::$app->controller->view->registerMetaTag(['property' => 'og:image', 'content' => $plain], 'og:image');
                    Yii::$app->controller->view->registerMetaTag(['property' => 'og:image:secure_url', 'content' => $this->image], 'og:image:secure_url');
                }

                Yii::$app->controller->view->registerLinkTag(['rel' => 'search', 'type' => 'application/opensearchdescription+xml', 'title' => 'Search uJobs', 'href' => '/search.xml']);
            });
        }
    }

    /**
     * @param array $metas
     */
    public function set($metas = [])
    {
        foreach ($metas as $property => $content) {
            if (property_exists($this, $property)) {
                $this->$property = $content;
            }
        }
    }
}