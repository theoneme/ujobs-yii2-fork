<?php

namespace frontend\components;

use yii\base\Model;
use yii\base\Widget;

/**
 * Class GmapsViewWidget
 * @package frontend\components
 */
class GmapsViewWidget extends Widget
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('gmaps-view-widget', ['model' => $this->model]);
    }
}