<?php

namespace frontend\components;

use Yii;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRule;

/**
 * Class LanguageUrlRule
 * @package frontend\components
 */
class LanguageUrlRule extends UrlRule
{
    /**
     * @var string
     */
    public $defaultLocale = 'ru';

    /**
     *
     */
    public function init()
    {
        if ($this->pattern !== null) {
//            if (Yii::$app->language !== 'ru-RU') {
                $this->pattern = '<app_language>' . (strpos($this->pattern, '/') === 0 ? '' : '/') . $this->pattern;
//            }
        }
        $this->defaults['app_language'] = $this->defaultLocale;
        parent::init();
    }

    /**
     * Add language to params if not specified and create a URL according to the given route and parameters.
     * @param UrlManager $manager the URL manager
     * @param string $route the route. It should not have slashes at the beginning or the end.
     * @param array $params the parameters
     * @return string|bool the created URL, or `false` if this rule cannot be used for creating this URL.
     */
    public function createUrl($manager, $route, $params)
    {
        if (empty($params['app_language'])) {
            $params['app_language'] = Yii::$app->params['supportedLocales'][Yii::$app->language];
        }
        return parent::createUrl($manager, $route, $params);
    }

    /**
     * Parses the given request and returns the corresponding route and parameters.
     * @param UrlManager $manager the URL manager
     * @param Request $request the request component
     * @return array|bool the parsing result. The route and the parameters are returned as an array.
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        $locales = implode('|',
            array_map(
                function($var) { return "({$var})"; },
                Yii::$app->params['supportedLocales']
            )
        );
        if (!preg_match("/^({$locales})(\/.*)?$/", $pathInfo)) {
            $pathInfo = Yii::$app->params['supportedLocales'][Yii::$app->language] . ($pathInfo !== '' ? '/' . $pathInfo : '');
            $request->setPathInfo($pathInfo);
        }
        return parent::parseRequest($manager, $request);
    }
}