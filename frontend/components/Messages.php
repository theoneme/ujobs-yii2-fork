<?php

namespace frontend\components;

use frontend\modules\account\models\MessageSearch;
use yii\base\Widget;
use Yii;

/**
 * Class Messages
 * @package frontend\components
 */
class Messages extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $unreadMessages = (new MessageSearch(['from_id' => Yii::$app->user->identity->getCurrentId()]))->getStatusCount(MessageSearch::STATUS_UNREAD);

        return $this->render('messages', ['unreadMessages' => $unreadMessages]);
    }
}