<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2017
 * Time: 14:50
 */

namespace frontend\components;

use yii\base\Widget;

/**
 * Class WizardSteps
 * @package common\modules\store\components
 */
class WizardSteps extends Widget
{
    public const STEP_STATUS_COMPLETED = 'completed';
    public const STEP_STATUS_ACTIVE = 'active';

    /**
     * @var string
     */
    public $template = 'wizard-steps';
    /**
     * @var array
     */
    public $steps = [];
    /**
     * @var int
     */
    public $step = 1;
    /**
     * @var bool
     */
    public $isMobile = false;

    /**
     * @return string
     */
    public function run()
    {
        $config = [];
        foreach ($this->steps as $key => $step) {
            $status = '';
            if ($key < $this->step) {
                $status = self::STEP_STATUS_COMPLETED;
            }

            if ($key === $this->step) {
                $status = self::STEP_STATUS_ACTIVE;
            }

            $config[$key] = [
                'status' => $status,
                'title' => $step
            ];
        }

        return $this->render($this->template, [
            'steps' => $config,
            'isMobile' => $this->isMobile
        ]);
    }
}