<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\components;

use common\models\Company;
use common\models\CompanyMember;
use common\models\Notification;
use common\models\user\User;
use common\modules\board\models\search\ProductSearch;
use frontend\modules\account\models\JobSearch;
use frontend\modules\account\models\MessageSearch;
use frontend\modules\account\models\OrderSearch;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\web\Cookie;

/**
 * Class HeaderWidget
 * @package frontend\components
 */
class HeaderWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'header-widget';

    /**
     * @return string
     */
    public function run()
    {
        $unreadNotificationsCount = null;
        $unreadMessagesCount = null;
        $menuItems = null;
        $userData = null;
        $askSubscribe = false;

        if (Yii::$app->controller->id !== 'service' && Yii::$app->controller->action->id !== 'account-settings' && !isGuest()) {
            $pushCookie = Yii::$app->request->cookies->getValue('pushCookie');

            if ($pushCookie === null || (time() - $pushCookie) > (60 * 60)) {
                $askSubscribe = true;
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'pushCookie',
                    'value' => time(),
                ]));
            }
        }

        if (!isGuest()) {
            /** @var User $user */
            $user = Yii::$app->user->identity;

            $messageSearchModel = new MessageSearch(['from_id' => Yii::$app->user->identity->getCurrentId()]);
            $unreadMessagesCount = $messageSearchModel->getStatusCount(MessageSearch::STATUS_UNREAD);

            $unreadNotificationsCount = Notification::find()
                ->where(['to_id' => Yii::$app->user->identity->getCurrentId(), 'is_read' => false, 'is_visible' => true])
                ->select('id')->count();

            $userCounts = Yii::$app->cacheLayer->get('user_cache');

            $menuItems = [
                'seller' => [
                    'announcement' => [
                        'title' => Yii::t('account', 'Post announcement'),
                        'link' => ['/entity/global-create'],
                        'icon' => 'fa-pencil'
                    ],
                    'company' => [
                        'title' => Yii::t('account', '{sub} Create company', ['sub' => Html::tag('span', 'NEW!', ['style' => 'color: #00b22d; white-space: normal;'])]),
                        'link' => ['/company/profile/form'],
//                        'count' => $userCounts['portfoliosCount'],
                        'icon' => 'fa-briefcase'
                    ],
                    'post_video' => [
                        'title' => Html::tag('span', 'NEW!', ['style' => 'color: #00b22d; white-space: normal;']) . ' ' . Yii::t('app', 'Post Video'),
                        'link' => ['/video/create'],
                        'icon' => 'fa-video-camera'
                    ],
                    'services' => [
                        'title' => Yii::t('account', 'List of my services'),
                        'link' => ['/account/job/list', 'status' => JobSearch::STATUS_ALL],
                        'count' => $userCounts['servicesCount'],
                        'icon' => 'fa-file-text-o'
                    ],
                    'products' => [
                        'title' => Yii::t('account', 'List of my products'),
                        'link' => ['/board/product/list', 'status' => ProductSearch::STATUS_ALL],
                        'count' => $userCounts['productsCount'],
                        'icon' => 'fa-cubes'
                    ],
                    'portfolio' => [
                        'title' => Yii::t('account', '{sub} My Portfolio', ['sub' => Html::tag('span', 'NEW!', ['style' => 'color: #00b22d; white-space: normal;'])]),
                        'link' => ['/account/portfolio/show', 'id' => userId()],
                        'count' => $userCounts['portfoliosCount'],
                        'icon' => 'fa-suitcase'
                    ],
                    'ordersOnMyJobsCount' => [
                        'title' => Yii::t('account', 'Orders on my services'),
                        'link' => ['/account/sell/list', 'status' => OrderSearch::STATUS_ALL],
                        'count' => $userCounts['ordersOnMyJobsCount'],
                        'icon' => 'fa-thumb-tack'
                    ],
                    'ordersOnMyProducts' => [
                        'title' => Yii::t('account', 'Orders on my products'),
                        'link' => ['/board/sell/list', 'status' => ProductSearch::STATUS_ALL],
                        'count' => $userCounts['ordersOnMyProducts'],
                        'icon' => 'fa-refresh'
                    ],
                    'tendersIWorkOn' => [
                        'title' => Yii::t('account', 'Requests I\'m working on'),
                        'link' => ['/account/worker-tender/list', 'status' => OrderSearch::STATUS_TENDER_ALL],
                        'count' => $userCounts['tendersIWorkOn'],
                        'icon' => 'fa-thumbs-up'
                    ],
                    'myResponses' => [
                        'title' => Yii::t('account', 'My responses to requests'),
                        'link' => ['/account/tender-response/list', 'status' => OrderSearch::STATUS_RESPONSE_MY],
                        'count' => $userCounts['myResponses'],
                        'icon' => 'fa-anchor'
                    ],
                    'myProductResponses' => [
                        'title' => Yii::t('account', 'My responses to product requests'),
                        'link' => ['/account/product-tender-response/list', 'status' => OrderSearch::STATUS_RESPONSE_MY],
                        'count' => $userCounts['myProductResponses'],
                        'icon' => 'fa-anchor'
                    ]
                ],
                'customer' => [
                    'announcement' => [
                        'title' => Yii::t('account', 'Post request'),
                        'link' => ['/entity/global-create?type=tender-service'],
                        'icon' => 'fa-edit'
                    ],
                    'productTendersCount' => [
                        'title' => Yii::t('account', 'List of my product requests'),
                        'link' => ['/board/tender/list', 'status' => JobSearch::STATUS_ALL],
                        'count' => $userCounts['productTendersCount'],
                        'icon' => 'fa-file-text'
                    ],
                    'jobTendersCount' => [
                        'title' => Yii::t('account', 'List of my service requests'),
                        'link' => ['/account/tender/list', 'status' => JobSearch::STATUS_ALL],
                        'count' => $userCounts['jobTendersCount'],
                        'icon' => 'fa-calendar-check-o'
                    ],
                    'jobOrdersCount' => [
                        'title' => Yii::t('account', 'Services in process'),
                        'link' => ['/account/order/list', 'status' => OrderSearch::STATUS_ALL],
                        'count' => $userCounts['jobOrdersCount'],
                        'icon' => 'fa-calendar-o'
                    ],
                    'productOrdersCount' => [
                        'title' => Yii::t('account', 'Products in process'),
                        'link' => ['/board/order/list', 'status' => OrderSearch::STATUS_ALL],
                        'count' => $userCounts['productOrdersCount'],
                        'icon' => 'fa-cart-arrow-down'
                    ],
                    'tendersCount' => [
                        'title' => Yii::t('account', 'Requests in process'),
                        'link' => ['/account/customer-tender/list', 'status' => OrderSearch::STATUS_TENDER_ALL],
                        'count' => $userCounts['tendersCount'],
                        'icon' => 'fa-hourglass-o'
                    ],
                    'responsesCount' => [
                        'title' => Yii::t('account', 'Responses on my requests'),
                        'link' => ['/account/tender-response/list', 'status' => OrderSearch::STATUS_RESPONSE_TO_ME],
                        'count' => $userCounts['responsesCount'],
                        'icon' => 'fa-reply-all'
                    ],
                    'responsesProductsCount' => [
                        'title' => Yii::t('account', 'Responses on my product requests'),
                        'link' => ['/account/product-tender-response/list', 'status' => OrderSearch::STATUS_RESPONSE_TO_ME],
                        'count' => $userCounts['responsesProductsCount'],
                        'icon' => 'fa-reply-all'
                    ],
                ],
                'settings' => [
                    'dashboard' => [
                        'title' => Yii::t('account', 'Dashboard'),
                        'link' => ['/account/service/dashboard'],
                        'icon' => 'fa-pie-chart'
                    ],
                    'messages' => [
                        'title' => Yii::t('account', 'Inbox') . '(' . Html::tag('span', $unreadMessagesCount) . ')',
                        'link' => ['/account/inbox'],
                        'icon' => 'fa-envelope'
                    ],
                    'notifications' => [
                        'title' => Yii::t('account', 'Notifications') . '(' . Html::tag('span', $unreadNotificationsCount) . ')',
                        'link' => ['/account/service/notifications'],
                        'icon' => 'fa-bell-o'
                    ],
                    'balance' => [
                        'title' => Yii::t('account', 'My Balance'),
                        'link' => ['/account/payment/index'],
                        'icon' => 'fa-money'
                    ],
                    'settings' => [
                        'title' => Yii::t('account', 'Settings'),
                        'link' => ['/account/service/public-profile-settings'],
                        'icon' => 'fa-cog'
                    ],
                    'referral' => [
                        'title' => Yii::t('app', 'Invite Friends & Get Bonus'),
                        'link' => ['/referral/index'],
                        'htmlOptions' => ['style' => 'color: #00b22d; white-space: normal;'],
                        'icon' => 'fa-group'
                    ],
                    'favorites' => [
                        'title' => Yii::t('account', 'My Favorites'),
                        'link' => ['/account/favourite/index'],
                        'icon' => 'fa-heart'
                    ],
                    'logout' => [
                        'title' => Yii::t('app', 'Logout'),
                        'link' => ['/user/security/logout'],
                        'htmlOptions' => ['data-method' => 'post'],
                        'icon' => 'fa-sign-out'
                    ],
                ],
            ];

            $hasCompanies = CompanyMember::find()->where(['user_id' => userId(), 'status' => CompanyMember::STATUS_ACTIVE])->exists();
            if ($hasCompanies === true) {
                /** @var Company $companies */
                $companies = Company::find()
                    ->joinWith(['companyMembers', 'translations'])
                    ->where(['company_member.user_id' => userId(), 'company_member.status' => CompanyMember::STATUS_ACTIVE])
                    ->all();

                $menuItems['additional']['type'] = 'companies';
                $menuItems['additional']['items'][] = [
                    'title' => $user->profile->getSellerName(),
                    'link' => ['/company/profile-ajax/switch'],
                    'htmlOptions' => [
                        'data-action' => 'identity-switcher',
                        'data-identity' => null,
                        'class' => $user->company_user_id === null ? 'active' : null
                    ],
                    'thumb' => $user->profile->getThumb('catalog'),
                    'icon' => 'fa-question-circle',
                ];
                foreach ($companies as $company) {
                    $menuItems['additional']['items'][] = [
                        'title' => $company->getSellerName(),
                        'link' => ['/company/profile-ajax/switch'],
                        'htmlOptions' => [
                            'data-action' => 'identity-switcher',
                            'data-identity' => $company->user_id,
                            'class' => $user->company_user_id === $company->user_id ? 'active' : null
                        ],
                        'thumb' => $company->getThumb('catalog'),
                        'icon' => 'fa-question-circle',
                    ];
                }
            } else {
                $menuItems['additional']['type'] = 'articles';
                $menuItems['additional']['items'] = [
                    'how-works' => [
                        'title' => Yii::t('account', 'How service works'),
                        'link' => ['/news/view', 'alias' => 'voprosy-i-otvety'],
                        'icon' => 'fa-question-circle'
                    ],
                    'request' => [
                        'title' => Yii::t('account', 'How to post request'),
                        'link' => ['/news/view', 'alias' => 'kak-sozdat-tender'],
                        'icon' => 'fa-question'
                    ],
                    'profile' => [
                        'title' => Yii::t('account', 'How to fill out a profile'),
                        'link' => ['/news/view', 'alias' => 'kak-gramotno-zapolnit-profil'],
                        'icon' => 'fa-id-badge'
                    ],
                    'payment' => [
                        'title' => Yii::t('account', 'How to set Payment settings'),
                        'link' => ['/news/view', 'alias' => 'nastroyka-platezhnoy-informatsii'],
                        'icon' => 'fa-credit-card-alt'
                    ],
                    'instruction' => [
                        'title' => Yii::t('account', 'The instruction on work with service'),
                        'link' => ['/news/view', 'alias' => 'instruktsia-po-rabote-s-ujobs'],
                        'icon' => 'fa-info'
                    ],
                ];
            }

            $isCompany = $user->company_user_id !== null;

            if($isCompany === true) {
                $company = Company::find()->where(['user_id' => $user->company_user_id])->one();

                $menuItems['seller']['portfolio'] = [
                    'title' => Yii::t('account', '{sub} My Portfolio', ['sub' => Html::tag('span', 'NEW!', ['style' => 'color: #00b22d; white-space: normal;'])]),
                    'link' => ['/company/portfolio/show', 'id' => $company->id],
                    'count' => $userCounts['portfoliosCount'],
                    'icon' => 'fa-suitcase'
                ];
                unset($menuItems['seller']['company']);

                $menuItems['settings'] = [
                    'settings' => [
                        'title' => Yii::t('account', 'Settings'),
                        'link' => ['/company/service/public-profile-settings'],
                        'icon' => 'fa-cog'
                    ],
                    'referral' => [
                        'title' => Yii::t('app', 'Invite Friends & Get Bonus'),
                        'link' => ['/referral/index'],
                        'htmlOptions' => ['style' => 'color: #00b22d; white-space: normal;'],
                        'icon' => 'fa-group'
                    ],
                    'logout' => [
                        'title' => Yii::t('app', 'Logout'),
                        'link' => ['/user/security/logout'],
                        'htmlOptions' => ['data-method' => 'post'],
                        'icon' => 'fa-sign-out'
                    ],
                ];
            }

            $userData = [
                'sellerName' => $user->company_user_id === null ? $user->profile->getSellerName() : $user->companyUser->company->getSellerName(),
                'thumb' => $user->company_user_id === null ? $user->profile->getThumb('catalog') : $user->companyUser->company->getThumb('catalog'),
                'sellerUrl' => $user->company_user_id === null ? ['/account/profile/show', 'id' => $user->id] : ['/company/profile/show', 'id' => $user->companyUser->company->id],
            ];
        }

        return $this->render($this->template, [
            'unreadMessagesCount' => $unreadMessagesCount,
            'unreadNotificationsCount' => $unreadNotificationsCount,
            'askSubscribe' => $askSubscribe,
            'menuItems' => $menuItems,
            'userData' => $userData
        ]);
    }
}