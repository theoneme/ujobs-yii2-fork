<?php

namespace frontend\components;

use common\models\Job;
use common\models\JobSearchStep;
use frontend\assets\JobSearchAsset;
use Yii;
use yii\base\Widget;

/**
 * Class JobSearchHelpWidget
 * @package frontend\components
 */
class JobSearchHelpWidget extends Widget
{
    /**
     * @var integer
     */
    public $step_id;
    /**
     * @var integer
     */
    public $job_id;
    /**
     * @var string
     */
    public $entity;
    /**
     * @var string
     */
    public $entity_content;
    /**
     * @var integer
     */
    public $lvl = 1;
    /**
     * @var integer
     */
    public $forceShow = false;

    /**
     * @return string
     */
    public function run()
    {
        return '';
        if (Yii::$app->request->get('page', 1) > 1) {
            JobSearchAsset::register($this->view);
            return '';
        }

        $job = Job::find()->joinWith(['packages', 'attachments', 'translation'])->where(['job.id' => $this->job_id])->one();
        $template = $job ? 'job-search-help-job' : 'job-search-help-step';
        $join = ['translation', 'options.attachment', 'tree.entities'];
        if ($job === null) {
            $join[] = 'options.translation';
        }
        $condition = ['or', ['job_search_step.tree_id' => 1, 'previous_step_id' => null]];
        if (!empty($this->step_id)) {
            $condition[] = ['job_search_step.id' => $this->step_id];
        } else if (!empty($this->entity) && !empty($this->entity_content)) {
            $condition[] = ['previous_step_id' => null, 'job_search_tree_to_entity.entity' => $this->entity, 'job_search_tree_to_entity.entity_content' => $this->entity_content];
        }

        $step = JobSearchStep::find()->joinWith($join)->where($condition)->orderBy('job_search_step.tree_id desc')->one();

        return ($job || $step) ? $this->render($template, ['job' => $job, 'step' => $step, 'lvl' => $this->lvl, 'forceShow' => $this->forceShow]) : '';
    }
}