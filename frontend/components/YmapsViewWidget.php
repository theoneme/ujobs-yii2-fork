<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\components;

use yii\base\Model;
use yii\base\Widget;

/**
 * Class YmapsViewWidget
 * @package frontend\components
 */
class YmapsViewWidget extends Widget
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ymaps-view-widget', ['model' => $this->model]);
    }
}