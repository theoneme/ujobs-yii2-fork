<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 11:41
 */

namespace frontend\components;

use yii\base\Widget;
use Yii;

/**
 * Class HeaderCategories
 * @package frontend\components
 */
class HeaderCategories extends Widget
{
    /**
     * @var string
     */
    public $template = 'new-header-categories';

    /**
     * @return string
     */
    public function run()
    {
        $categories = $productCategories = [];

        $cacheLayer = Yii::$app->get('cacheLayer');
        if($cacheLayer !== null) {
            $categories = $cacheLayer->get('categories3_' . Yii::$app->language);
            $productCategories = $cacheLayer->get('productCategories_' . Yii::$app->language);
        }

        return $this->render($this->template, ['categories' => $categories, 'productCategories' => $productCategories]);
    }
}