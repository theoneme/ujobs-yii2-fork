<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.04.2017
 * Time: 18:18
 */

namespace frontend\components;

use common\modules\store\models\Order;
use yii\base\Widget;
use yii\db\Query;

/**
 * Class HeaderStatsWidget
 * @package frontend\components
 */
class HeaderStatsWidget extends Widget
{
    /**
     * @var null
     */
    public $onlineVariable = 60 * 25;
    /**
     * @var string
     */
    public $template = 'header-stats-widget';

    /**
     * @return string
     */
    public function run()
    {
        $totalOnline = (new Query())->select(['user_id'])
            ->from('session')
            ->where(['>', 'last_write', time() - $this->onlineVariable])
            ->count('id');

        $now = new \DateTime('now');
        $month = $now->format('m');
        $year = $now->format('Y');
        $day = $now->format('d');
        $hour = $now->format('H');

        $ordersTotal = Order::find()
                ->where(['between', 'created_at', strtotime("{$day}-{$month}-{$year} 00:00:00"), strtotime("{$day}-{$month}-{$year} 23:59:59")])
                ->count() + (int)$hour * ($day % 3 + 1);

        $totalOnline = round($totalOnline * 1.45);

        return $this->render($this->template, [
            'totalOnline' => $totalOnline,
            'ordersTotal' => $ordersTotal
        ]);
    }
}