<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 17:32
 */

namespace frontend\components;

use common\models\Company;
use common\models\CompanyMember;
use common\models\Notification;
use common\models\user\User;
use frontend\modules\account\models\MessageSearch;
use yii\base\Widget;
use Yii;

/**
 * Class MobileMenu
 * @package frontend\components
 */
class MobileMenu extends Widget
{
    /**
     * @var string
     */
    public $template = 'mobile-menu';

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $userData = [
            'profiles' => [],
            'unreadNotificationsCount' => 0,
            'unreadMessagesCount' => 0
        ];
        $cacheLayer = Yii::$app->get('cacheLayer');

        $hasCompanies = CompanyMember::find()->where(['user_id' => userId(), 'status' => CompanyMember::STATUS_ACTIVE])->exists();
        if ($hasCompanies === true) {
            /** @var Company $companies */
            $companies = Company::find()
                ->joinWith(['companyMembers', 'translations'])
                ->where(['company_member.user_id' => userId(), 'company_member.status' => CompanyMember::STATUS_ACTIVE])
                ->all();

            $userData['profiles'][] = [
                'title' => Yii::$app->user->identity->profile->getSellerName(),
                'link' => ['/company/profile-ajax/switch'],
                'htmlOptions' => [
                    'data-action' => 'identity-switcher',
                    'data-identity' => null,
                    'class' => Yii::$app->user->identity->company_user_id === null ? 'mm-none active' : 'mm-none'
                ],
            ];
            foreach ($companies as $company) {
                $userData['profiles'][] = [
                    'title' => $company->getSellerName(),
                    'link' => ['/company/profile-ajax/switch'],
                    'htmlOptions' => [
                        'data-action' => 'identity-switcher',
                        'data-identity' => $company->user_id,
                        'class' => Yii::$app->user->identity->company_user_id === $company->user_id ? 'mm-none active' : 'mm-none'
                    ],
                ];
            }
        }

        if(!isGuest()) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $userData['sellerName'] = $user->company_user_id === null ? $user->profile->getSellerName() : $user->companyUser->company->getSellerName();
            $userData['thumb'] = $user->company_user_id === null ? $user->profile->getThumb('catalog') : $user->companyUser->company->getThumb('catalog');
            $userData['sellerUrl'] = $user->company_user_id === null ? ['/account/profile/show', 'id' => $user->id] : ['/company/profile/show', 'id' => $user->companyUser->company->id];

            $messageSearchModel = new MessageSearch(['from_id' => Yii::$app->user->identity->getCurrentId()]);
            $userData['unreadMessagesCount'] = $messageSearchModel->getStatusCount(MessageSearch::STATUS_UNREAD);
            $userData['unreadNotificationsCount'] = Notification::find()
                ->where(['to_id' => Yii::$app->user->identity->getCurrentId(), 'is_read' => false, 'is_visible' => true])
                ->select('id')->count();
        }

        return $this->render($this->template, [
            'categories' => $cacheLayer->get('categories3_' . Yii::$app->language),
            'productCategories' => $cacheLayer->get('productCategories_' . Yii::$app->language),
            'userData' => $userData,
        ]);
    }
}