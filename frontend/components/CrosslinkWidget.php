<?php

namespace frontend\components;

use common\models\JobAttribute;
use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\base\Widget;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * Class CrosslinkWidget
 * @package frontend\components
 */
class CrosslinkWidget extends Widget
{
    /**
     * @var array
     */
    private $_queryParams;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_queryParams = Yii::$app->request->queryParams;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function run()
    {
        $data = [
            [
                'title' => Yii::t('app', 'Tags in Services'),
                'items' => []
            ],
            [
                'title' => Yii::t('app', 'Tags in Products'),
                'items' => []
            ],
            [
                'title' => Yii::t('app', 'Specialties'),
                'items' => []
            ],
            [
                'title' => Yii::t('app', 'Skills'),
                'items' => []
            ]
        ];

        $attributeObjects = Attribute::find()
            ->with(['translations' => function ($query) {
                /** @var \yii\db\ActiveQuery $query */
                return $query->select('entity, entity_content, title, locale');
            }])
            ->select('mod_attribute.id, alias')->where(['mod_attribute.alias' => ['city', 'tag', 'product_tag', 'skill', 'specialty']])
            ->indexBy('alias')
            ->asArray()
            ->all();

        $city = null;
        if (array_key_exists('city', $this->_queryParams)) {
            $city = AttributeValue::find()
                ->with(['translations'])
                ->where(['alias' => $this->_queryParams['city'], 'mod_attribute_value.attribute_id' => $attributeObjects['city']['id']])
                ->groupBy('mod_attribute_value.id')
                ->indexBy('alias')
                ->one();
        }

        if (array_key_exists('tag', $attributeObjects)) {
            $values = AttributeValue::find()
                ->with(['translations'])
                ->where(['mod_attribute_value.attribute_id' => $attributeObjects['tag']['id'], 'status' => AttributeValue::STATUS_APPROVED])
                ->andWhere(['exists', JobAttribute::find()->where('job_attribute.value=mod_attribute_value.id and job_attribute.attribute_id = mod_attribute_value.attribute_id')])
                ->groupBy('mod_attribute_value.id')
                ->indexBy('alias')
                ->orderBy(new Expression('rand()'))
                ->limit(5)
                ->cache(720)
                ->all();

            foreach ($values as $value) {
                $data[0]['items'][] = [
                    'title' => Yii::t('seo', '{tag} {city}', ['tag' => $value->getTitle(), 'city' => $city !== null ? $city->getTitle() : null]),
                    'url' => Url::to(['/category/tag', 'reserved_job_tag' => $value['alias'], 'city' => $city !== null ? $city['alias'] : null])
                ];
            }
        }

        if (array_key_exists('product_tag', $attributeObjects)) {
            $values = AttributeValue::find()
                ->with(['translations'])
                ->where(['mod_attribute_value.attribute_id' => $attributeObjects['product_tag']['id'], 'status' => AttributeValue::STATUS_APPROVED])
                ->andWhere(['exists', ProductAttribute::find()->where('product_attribute.value=mod_attribute_value.id and product_attribute.attribute_id = mod_attribute_value.attribute_id')])
                ->groupBy('mod_attribute_value.id')
                ->indexBy('alias')
                ->orderBy(new Expression('rand()'))
                ->limit(5)
                ->cache(720)
                ->all();

            foreach ($values as $value) {
                $data[1]['items'][] = [
                    'title' => Yii::t('seo', '{tag} {city}', ['tag' => $value->getTitle(), 'city' => $city !== null ? $city->getTitle() : null]),
                    'url' => Url::to(['/board/category/tag', 'reserved_product_tag' => $value['alias'], 'city' => $city !== null ? $city['alias'] : null])
                ];
            }
        }

        if (array_key_exists('specialty', $attributeObjects)) {
            $values = AttributeValue::find()
                ->with(['translations'])
                ->where(['mod_attribute_value.attribute_id' => $attributeObjects['specialty']['id'], 'status' => AttributeValue::STATUS_APPROVED])
                ->andWhere(['exists', UserAttribute::find()->where('user_attribute.value=mod_attribute_value.id and user_attribute.attribute_id = mod_attribute_value.attribute_id')])
                ->groupBy('mod_attribute_value.id')
                ->indexBy('alias')
                ->orderBy(new Expression('rand()'))
                ->limit(5)
                ->cache(720)
                ->all();

            foreach ($values as $value) {
                $data[2]['items'][] = [
                    'title' => Yii::t('seo', '{tag} {city}', ['tag' => $value->getTitle(), 'city' => $city !== null ? $city->getTitle() : null]),
                    'url' => Url::to(['/category/specialty-pro', 'reserved_specialty' => $value['alias'], 'city' => $city !== null ? $city['alias'] : null])
                ];
            }
        }

        if (array_key_exists('skill', $attributeObjects)) {
            $values = AttributeValue::find()
                ->with(['translations'])
                ->where(['mod_attribute_value.attribute_id' => $attributeObjects['skill']['id'], 'status' => AttributeValue::STATUS_APPROVED])
                ->andWhere(['exists', UserAttribute::find()->where('user_attribute.value=mod_attribute_value.id and user_attribute.attribute_id = mod_attribute_value.attribute_id')])
                ->groupBy('mod_attribute_value.id')
                ->indexBy('alias')
                ->orderBy(new Expression('rand()'))
                ->limit(5)
                ->cache(720)
                ->all();

            foreach ($values as $value) {
                $data[3]['items'][] = [
                    'title' => Yii::t('seo', '{tag} {city}', ['tag' => $value->getTitle(), 'city' => $city !== null ? $city->getTitle() : null]),
                    'url' => Url::to(['/category/skill', 'reserved_skill' => $value['alias'], 'city' => $city !== null ? $city['alias'] : null])
                ];
            }
        }

        return $this->render('crosslink', [
            'data' => $data
        ]);
    }
}