<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\components;

use Yii;
use yii\base\Widget;

/**
 * Class AccountConfirmModalWidget
 * @package frontend\components
 */
class AccountConfirmModalWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'account-confirm-modal';

    /**
     * @return null|string
     */
    public function run()
    {
        if (Yii::$app->session->has('confirmUserId')) {
            return $this->render($this->template, ['confirmUserId' => Yii::$app->session->remove('confirmUserId')]);
        }

        return null;
    }
}