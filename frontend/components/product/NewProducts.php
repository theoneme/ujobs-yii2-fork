<?php

namespace frontend\components\product;

use common\modules\board\models\Product;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class NewProducts
 * @package frontend\components\product
 */
class NewProducts extends Widget
{
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var string
     */
    public $entity = 'product';

    /**
     * @return string
     */
    public function run()
    {
        $items = Product::find()
            ->joinWith(['translation', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
            ->where(['type' => $this->entity, 'product.status' => Product::STATUS_ACTIVE, 'product.locale' => Yii::$app->language])
            ->andWhere(['not', ['product.user_id' => 36461]])
            ->groupBy('product.id')
            ->orderBy('product.id desc')
            ->limit(6);

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
            'pagination' => false
        ]);

        return $this->render($this->template, [
            'dataProvider' => $dataProvider,
            'heading' => Yii::t('app', 'New on site'),
            'sliderCount' => $this->sliderCount
        ]);
    }
}