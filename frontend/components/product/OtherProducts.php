<?php

namespace frontend\components\product;

use common\modules\board\models\Product;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class OtherProducts
 * @package frontend\components\product
 */
class OtherProducts extends Widget
{
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var integer
     */
    public $userId;
    /**
     * @var integer
     */
    public $exceptId;
    /**
     * @var string
     */
    public $type = 'product';
    /**
     * @var string
     */
    public $template = 'propositions';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->userId !== null && $this->exceptId !== null) {
            $items = Product::find()
                ->joinWith(['translation', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->where(['product.user_id' => $this->userId, 'type' => $this->type, 'product.status' => Product::STATUS_ACTIVE, 'product.locale' => Yii::$app->language])
                ->andWhere(['not in', 'product.id', $this->exceptId])
                ->groupBy('product.id')
                ->limit(6);

            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => false
            ]);

            $heading = $this->type === Product::TYPE_PRODUCT
                ? Yii::t('app', 'Other Propositions By This User')
                : Yii::t('app', 'Other Requests From This User');

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => $heading,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}