<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.12.2016
 * Time: 11:59
 */

namespace frontend\components\product;

use common\modules\board\models\Product;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class ViewedPropositions
 * @package frontend\components\product
 */
class ViewedProducts extends Widget
{
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var string
     */
    public $type = 'product';

    /**
     * @return string
     */
    public function run()
    {
        $viewed = Yii::$app->session->get('viewed_products');
        if ($viewed !== null) {
            $items = Product::find()
                ->joinWith(['translation', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->where(['type' => $this->type, 'product.id' => $viewed, 'product.status' => Product::STATUS_ACTIVE])
                ->groupBy('product.id')
                ->limit(10);

            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => false
            ]);

            $heading = Yii::t('app', 'Recently viewed products');

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => $heading,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}