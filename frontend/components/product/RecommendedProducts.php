<?php

namespace frontend\components\product;

use common\models\Category;
use common\modules\board\models\Product;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class RecommendedProducts
 * @package frontend\components\product
 */
class RecommendedProducts extends Widget
{
    /**
     * @var Category
     */
    public $category;
    /**
     * @var array
     */
    public $similarCategories;
    /**
     * @var integer
     */
    public $exceptId;
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var string
     */
    public $template = 'propositions';
    /**
     * @var string
     */
    public $entity = 'product';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->category !== null) {
            $categoryIds = null;
            if (!empty($this->similarCategories)) {
                $categoryIds = $this->similarCategories;
            } else if ($this->category->rgt - $this->category->lft === 1) {
                $categoryIds = $this->category->id;
            } else if ($this->category->lvl !== 0) {
                $categoryIds = $this->category->getChildrenIds();
            }

            $items = Product::find()
                ->joinWith(['translation', 'attachments', 'user.activeTariff.tariff', 'user.profile.translation', 'user.company.translations'])
                ->where(['type' => $this->entity, 'product.status' => Product::STATUS_ACTIVE, 'product.locale' => Yii::$app->language])
                ->andFilterWhere(['category_id' => $categoryIds])
                ->andFilterWhere(['not', ['product.id' => $this->exceptId]])
                ->groupBy('product.id')
                ->orderBy('product.id desc')
                ->limit(6);

            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => false
            ]);

            $template = (empty($this->similarCategories) || count($this->similarCategories) === 1)
                ? 'Recommended For You In Category {item}'
                : 'Recommended For You In Categories: {item}';
            $item = empty($this->similarCategories)
                ? Html::a($this->category->translation->title, Url::to(['/board/category/products', 'category_1' => $this->category->translation->slug]), ['data-pjax' => 0])
                : implode(', ',
                    array_map(
                        function ($var) {
                            return Html::a($var['title'], Url::to(['/board/category/products', 'category_1' => $var['slug']]), ['data-pjax' => 0]);
                        },
                        Category::find()->where(['category.id' => $this->similarCategories])->joinWith(['translation'])->select(['category.id', 'content_translation.title', 'content_translation.slug'])->limit(3)->asArray()->all()
                    )
                ) . (count($this->similarCategories) > 3 ? '...' : '');

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => Yii::t('app', $template, ['item' => $item]),
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}