<?php

use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;

/* @var Product|ProductElastic $model */

?>

<div class="bf-item">
    <?= $this->render('@frontend/modules/filter/views/product-list', ['model' => $model]);?>
</div>