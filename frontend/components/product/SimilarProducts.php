<?php

namespace frontend\components\product;

use common\helpers\ElasticConditionsHelper;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\elasticsearch\ActiveQuery;
use yii\helpers\Html;

/**
 * Class SimilarProducts
 * @package frontend\components\product
 */
class SimilarProducts extends Widget
{
    /**
     * @var integer
     */
    public $sliderCount = 4;
    /**
     * @var string
     */
    public $request;
    /**
     * @var string
     */
    public $template = 'propositions';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->request !== null) {
            /** @var ActiveQuery $products */
            $products = ProductElastic::find()
                ->where(['status' => Product::STATUS_ACTIVE, 'locale' => Yii::$app->language, 'type' => Product::TYPE_PRODUCT])
                ->addOrderBy('tariff.price desc')
                ->addOrderBy('created_at desc')
                ->limit(6);

            $products->query([
                'bool' => [
                    'should' => ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $this->request),
                ]
            ]);

            $dataProvider = new ActiveDataProvider([
                'query' => $products,
                'pagination' => false
            ]);

            $heading = Yii::t('app', 'Recommended on "{item}" request', [
                'item' => Html::a($this->request, ['/board/category/products', 'request' => $this->request], ['data-pjax' => 0])
            ]);

            $button = Html::a(Yii::t('app', 'View more options on your request: "{item}"', ['item' => $this->request]),
                ['/board/category/products', 'request' => $this->request],
                ['class' => 'load', 'data-pjax' => 0]
            );

            return $this->render($this->template, [
                'dataProvider' => $dataProvider,
                'heading' => $heading,
                'button' => $button,
                'sliderCount' => $this->sliderCount
            ]);
        }

        return '';
    }
}