<?php

namespace frontend\components;

use common\models\Banner;
use Yii;
use yii\base\Widget;
use yii\db\Expression;

/**
 * Class BannerWidget
 * @package frontend\components
 */
class BannerWidget extends Widget
{
    /**
     * @var integer
     */
    public $type;

    /**
     * @var integer
     */
    public $limit = 1;

    /**
     * @return string
     */
    public function run()
    {
        $banners = Banner::find()->andFilterWhere(['type' => $this->type, 'locale' => Yii::$app->language])->orderBy(new Expression('rand()'))->limit($this->limit)->all();

        return $this->render('banners', ['banners' => $banners]);
    }
}