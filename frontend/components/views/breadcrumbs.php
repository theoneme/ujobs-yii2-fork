<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 15:19
 */

/* @var array $breadcrumbs */

?>

<div class="breadcrumbs-div">
    <div class="row">
        <div class="col-xs-12">
            <?= yii\widgets\Breadcrumbs::widget([
                'itemTemplate' => '<li>{link}</li>',
                'activeItemTemplate' => '{link}',
                'links' => $breadcrumbs,
                'options' =>
                    [
                        'class' => 'breadcrumbs'
                    ],
                'tag' => 'ul'
            ]); ?>
        </div>
    </div>
</div>