<?php
use common\modules\board\assets\AdultAsset;
use yii\helpers\Html;

AdultAsset::register($this);

?>

<div class="modal fade in" id="welcome-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true" style="padding-right: 17px;">
    <div class="modal-dialog age21">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="text-left">
                    <?= Html::img('/images/new/user.svg', ['style' => 'width: 80px;'])?>
                </div>
                <div class="title-age" style="margin-bottom: 20px;">
                    <span>
                        <?= Yii::t('app', 'Welcome to uJobs.') ?>
                    </span>
                </div>
                <p style="color: #777;">
                    <?= Yii::t('app', 'Attention. Your profile is not completely filled.') ?><br>
                    <?= Yii::t('app', 'This may restrict correspondence with other participants.') ?><br>
                    <?= Yii::t('app', 'Do you want to go to profile or return to site?') ?><br>
                </p>
                <div class="text-left">
                    <?= Html::a(Yii::t('app', 'Fill Profile'), ['/account/service/public-profile-settings'], ['class' => 'button white no-size'])?>
                    <?= Html::a(Yii::t('app', 'Go to site'), null, ['data-dismiss' => 'modal', 'aria-hidden' => true, 'style' => 'color: #00698c; font-size: 14px; line-height: 50px; font-weight: bold;'])?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("$('#welcome-modal').modal('show');");

