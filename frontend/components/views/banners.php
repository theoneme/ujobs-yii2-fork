<?php

use common\models\Banner;
use yii\helpers\Html;

/* @var Banner[] $banners */

?>

<?php foreach ($banners as $banner) {?>
    <div class="ajcbxkqjef">
        <?= Html::a(
            Html::img($banner->getThumb(), ['class' => 'qwdgfcash']),
            preg_match('/^http(s)?:\/\/.*/', $banner->url) ? $banner->url : [$banner->url],
            ['target' => '_blank', 'rel' => 'nofollow', 'data-pjax' => 0]
        ) ?>
    </div>
<?php }?>