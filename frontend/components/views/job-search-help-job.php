<?php

use common\components\CurrencyHelper;
use common\models\Job;
use common\models\JobSearchStep;
use common\modules\store\models\Currency;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $job Job
 * @var $step JobSearchStep
 * @var $this View
 * @var $lvl integer
 * @var $forceShow boolean
 * @var $currency Currency
 */

$utility = Yii::$app->utility;
$titles = ['basic' => 'Saving', 'standart' => 'Standard', 'premium' => 'Premium'];
$currency = Currency::getDb()->cache(function () {
    return Currency::find()->where(['code' => Yii::$app->params['app_currency_code']])->one();
});
?>

<div class="help-content <?= $forceShow ? 'show' : '' ?>">
    <div class="container-fluid">
        <div class="help-header text-center">
            <?= $step->getTitle() . ' ' . Html::a(Yii::t('app', 'Learn more'), null, ['data-toggle' => 'modal', 'data-target' => '#jobSearchMoreModal']) ?>
            <div id="help-close" class="help-close">&times;</div>
        </div>
        <div class="help-box">
            <div class="help-step-title text-center">
                <?= Yii::t('app', 'Step') . ' ' . $lvl . '. ' . $step->getSubtitle() ?>
            </div>
            <div class="help-step <?= $job->price_per_unit ? 'price-per-unit' : '' ?>">
                <?php
                foreach ($job->packages as $id => $package) {
                    $unFormattedPrice = CurrencyHelper::convert($job->currency_code, Yii::$app->params['app_currency_code'], $package->price);
                    $price = $package->getPrice();
                    if ($job->price_per_unit) {
                        $price .= Yii::t('app', ' for {measure}', ['measure' => "{$job->units_amount} {$job->measure->translation->title}"]);
                    }
                    ?>
                    <div class="help-radio budget" data-price="<?= $unFormattedPrice ?>"
                         data-units="<?= $job->units_amount ?>">
                        <input id="job-search-package-<?= $id ?>" class="job-search-help-package"
                               data-id="<?= $package->id ?>" name="help-budget" value="radio" type="radio">
                        <label for="job-search-package-<?= $id ?>">
                            <span class="help-radio-package text-center"><?= Yii::t('app', $titles[$package->type]) ?></span>
                            <span class="help-radio-include text-center"><?= $package->included ?></span>
                            <span class="help-radio-title text-center">
                                <?= $price ?>
                            </span>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if ($job->price_per_unit) { ?>
        <div class="container-fluid job-search-quantity-block" style="display: none;">
            <div class="help-header text-center">
                <?= $step->getMeasureTitle() ?? Yii::t('app', 'The price of this job is <span class="price"></span>') ?>
                <div id="help-close" class="help-close">&times;</div>
            </div>
            <div class="help-box">
                <div class="help-step-title text-center">
                    <?= Yii::t('app', 'Step') . ' ' . (count($job->packages) === 1 ? $lvl : $lvl + 1) . '. ' ?>
                    <?= $step->getMeasureSubtitle() ?? Yii::t('app', 'Specify amount of work you would like to order') ?>
                </div>
                <div class="help-step">
                    <?= Html::hiddenInput('package_id', null, ['id' => 'job-search-package-id']) ?>
                    <span class="help-step-calculator">
                        <?= Yii::t('app', '{input} {measure} x {sl}<span class="price-one"></span>{sr}/{amount}{measure} = {sl}<span class="price"></span>{sr}', [
                            'amount' => ($job->units_amount > 1) ? $job->units_amount : '',
                            'measure' => $job->measure->translation->title,
                            'sl' => $currency->symbol_left,
                            'sr' => $currency->symbol_right,
                            'input' => Html::input('number', 'quantity', 1, ['id' => 'job-search-quantity']),
                        ]) ?>
                    </span>
                    <?= Html::button(Yii::t('account', 'Add to Cart'), ['class' => 'add-to-cart btn-middle pull-right']) ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div class="modal fade" id="jobSearchMoreModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <?php foreach ($job->packages as $package) { ?>
                    <p>
                        <b><?= Yii::t('app', $titles[$package->type]) ?></b> - <?= $package->description ?>
                    </p>
                <?php } ?>
                <?= Html::a(Yii::t('app', 'Continue'), null, ['class' => 'btn-big width100', 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
    </div>
</div>
