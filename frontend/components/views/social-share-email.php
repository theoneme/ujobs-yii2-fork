<?php

use frontend\assets\CommonAsset;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * @var array $mailItem
 * @var \yii\web\View $this
 * @var $url string
 */

$this->registerCssFile('/css/new/form-share.css', ['depends' => CommonAsset::class])
?>

<?= Html::a('<i class="fa fa-envelope-open-o" aria-hidden="true"></i>',
    '#',
    ['class' => 'in hint--bottom email-share', 'data-hint' => Yii::t('app', 'Send by Email'), 'data-toggle' => 'modal', 'data-target' => '#share-on-email']
) ?>
<div class="modal fade" id="share-on-email" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog share-form">
        <div class="modal-content">
            <div class="modal-header text-left">
                <?= Yii::t('app', 'Share this link via email') ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>
            <div class="modal-body">
                <?php echo Html::beginForm(['/ajax/share-by-email'], 'post', ['id' => 'email-share-form']); ?>
                    <div class="share-container">
                        <div class="share-img">
                            <?= Html::img($mailItem['thumb']) ?>
                        </div>
                        <div class="share-info text-left">
                            <div class="share-item-title"><?= $mailItem['label'] ?></div>
                            <div class="share-by">
                                <?= Yii::t('app', 'by {who}', ['who' => $mailItem['name']])?>
                            </div>
                            <div class="share-link">
                                <span><?= Yii::t('app', 'Link') ?>: </span>
                                <?= $url ?>
                            </div>
                        </div>
                    </div>
                    <div class="share-email text-left">
                        <?= Html::textInput('email', null, [
                            'class' => 'share-list',
                            'placeholder' => Yii::t('app', 'Enter email of user you want to share this link with'),
                        ])?>
<!--                            <textarea class="share-list" placeholder="--><?//= Yii::t('app', 'Enter email of user you want to share this link with') ?><!--"></textarea>-->
                    </div>
                    <div class="clearfix" style="margin-top: 10px;">
                        <div class="share-bottom">
                            <div class="shares"></div>
                            <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'email-share-submit btn-middle fright']) ?>
                        </div>
                    </div>
                <?php echo Html::endForm();?>
            </div>
            <div class="spinner-bg">
                <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</div>

<?php
$mailData = Json::encode($mailItem);
$script = <<<JS
    var mailData = $mailData;

    $(document).on('click', '.email-share-submit', function() {
        let form = $('#email-share-form');
        mailData.link = '$url';
        mailData.email = form.find('input.share-list').val();
        $('.spinner-bg').show();
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: mailData,
            success: function(response) {
                if (response.success) {
                    $('#share-on-email').modal('hide');
                    alertCall('top', 'success', response.message);
                } else {
                    alertCall('top', 'error', response.message);
                }
                $('.spinner-bg').hide();
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
