<?php
/**
 * @var $this View
 * @var $model \yii\base\Model
 * @var $inputOptions array
 * @var $modal boolean
 */

use yii\helpers\Html;
use yii\web\View;

?>

<?= Html::activeHiddenInput($model, 'lat', ['id' => 'ymaps-input-lat']) ?>
<?= Html::activeHiddenInput($model, 'long', ['id' => 'ymaps-input-long']) ?>
<?= Html::activeTextInput($model,
    'address',
    array_merge([
        'id' => 'ymaps-input-address',
        'placeholder' => Yii::t('board', 'Enter address'),
        'readonly' => $modal ? false : true
    ], $inputOptions)
); ?>

<?php if ($modal) { ?>
    <div class="modal fade" id="ymaps-input-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('app', 'Select address on map') ?></div>
                </div>
                <div class="modal-body">
                    <b><?= Yii::t('app', 'To save the location, enter the address in the search field, click "Find" and select the most suitable from the drop-down list') ?></b>:<br><br>
                    <div id="ymaps_modal" style="width: 100%; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else { ?>
    <br>
    <b><?= Yii::t('app', 'To save the location, enter the address in the search field, click "Find" and select the most suitable from the drop-down list') ?></b>:
    <br>
    <div id="ymaps_modal" style="width: 100%; height: 400px;"></div>
<?php }

$showMap = $modal ? "$('#ymaps-input-modal').modal('show');" : '';
$hideMap = $modal ? "$('#ymaps-input-modal').modal('hide');" : '';

if (!empty($model->lat) && !empty($model->long)) {
    $center = "[{$model->lat}, {$model->long}]";
    $placemark = <<<JS
        let placemark = new ymaps.Placemark($center, {
            balloonContent: '$model->address'
        }, {
            preset: 'islands#icon'
        });
        myMapModal.geoObjects.add(placemark);
JS;
} else {
    $center = '[55.75396, 37.620393]'; // Москва
    $placemark = '';
}

$script = <<<JS
    if (typeof (window.ymaps) === 'undefined') {
        $.getScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU', function() {
            ymaps.ready(ymapsInit);
        });
    } else {
        ymapsInit();
    }
    
    function ymapsInit() {
        let myMapModal = new ymaps.Map('ymaps_modal', {
            center: $center,
            zoom: 12,
            controls: []
        });
        mySearchControlModal = new ymaps.control.SearchControl({
            options: {
                noPlacemark: true
            }
        });
        mySearchResultsModal = new ymaps.GeoObjectCollection(null, {
            hintContentLayout: ymaps.templateLayoutFactory.createClass('$[properties.name]')
        });
    
        {$placemark};
    
        myMapModal.controls.add(mySearchControlModal);
        myMapModal.geoObjects.add(mySearchResultsModal);
    
        // Выбранный результат помещаем в коллекцию.
        mySearchControlModal.events.add('resultselect', function (e) {
            let index = e.get('index');
            mySearchControlModal.getResult(index).then(function (res) {
                mySearchResultsModal.add(res);
                let coordinates = res.geometry.getCoordinates();
                $('#ymaps-input-lat').val(coordinates[0]);
                $('#ymaps-input-long').val(coordinates[1]);
                $('#ymaps-input-address').val(res.properties.get('text'));
                $hideMap
            });
        }).add('submit', function () {
            mySearchResultsModal.removeAll();
        })
    }
    $('#ymaps-input-address').on('focus', function(){
        $showMap;
    });
JS;

$this->registerJs($script);