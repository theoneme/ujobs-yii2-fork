<?php

use frontend\assets\CommonAsset;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * @var boolean $renderMailModal
 * @var array $mailItem
 * @var \yii\web\View $this
 * @var $url string
 * @var $twitterMessage string
 * @var $showLink boolean
 */

?>

<?= Html::a('<i class="fa fa-facebook" aria-hidden="true"></i>',
    'https://www.facebook.com/sharer/sharer.php?u=' . $url,
    ['class' => 'fb hint--bottom text-center social-share', 'data-hint' => Yii::t('app', 'Share on') . ' Facebook']
) ?>
<?= Html::a('<i class="fa fa-vk" aria-hidden="true"></i>',
    'http://vk.com/share.php?url=' . preg_replace('/\/\/ujobs.me/', '//ujobs.ru', $url),
    ['class' => 'in hint--bottom text-center social-share', 'data-hint' => Yii::t('app', 'Share on') . ' Vk']
) ?>
<?= Html::a('<i class="fa fa-twitter" aria-hidden="true"></i>',
    'https://twitter.com/intent/tweet?' . ($twitterMessage ? 'text=' . $twitterMessage . '&' : '') . 'url=' . $url,
    ['class' => 'tw hint--bottom text-center social-share', 'data-hint' => Yii::t('app', 'Share on') . ' Twitter']
) ?>
<?= Html::a('<i class="fa fa-google-plus" aria-hidden="true"></i>',
    'https://plus.google.com/share?url=' . $url,
    ['class' => 'g hint--bottom text-center social-share', 'data-hint' => Yii::t('app', 'Share on') . ' Google+']
) ?>
<?= Html::a('<i class="fa fa-linkedin" aria-hidden="true"></i>',
    'https://www.linkedin.com/shareArticle?mini=true&url=' . $url . '&source=' . Yii::$app->name,
    ['class' => 'in hint--bottom text-center social-share', 'data-hint' => Yii::t('app', 'Share on') . ' LinkedIn']
) ?>

<?php if ($renderMailModal === true) {
    echo $this->render('social-share-email', ['mailItem' => $mailItem, 'url' => $url]);
}?>
<?= Html::a('<i class="fa fa-link" aria-hidden="true"></i>',
    null,
    ['class' => 'link hint--bottom text-center clipboard', 'data-hint' => Yii::t('app', 'Copy to Clipboard')]
) ?>
<div class="referral-link-block text-center" <?= $showLink ? '' : 'style= "display:none;"'?>>
    <div class="your-link"><?= Yii::t('app', 'Your link') ?></div>
    <?= Html::textInput('link', $url, ['id' => 'clipboard-link']) ?>
</div>
<?php
$script = <<<JS
    $(document).on('click', 'a.social-share', function() {
        let ww = $(window).width();
        let wh = $(window).height();
        let params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no' + ',width=' + ww * 0.6 + ',height=' + wh * 0.7 + ',left=' + ww * 0.2 + ',top=' + wh * 0.15;
        window.open($(this).attr('href'), $(this).data('hint'), params);
        
        return false;
    });

    $(document).on('click', 'a.clipboard', function(){
        $('#clipboard-link').select();
        document.execCommand('copy');
    });
JS;

$this->registerJs($script);
