<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.04.2017
 * Time: 18:19
 */

/* @var integer $totalOnline
 * @var integer $ordersTotal */

?>

<div class="header-stats">
    <div class="users-total">
        <?= Yii::t('app', 'Users online')?>: <?= $totalOnline ?>
    </div>
    <div class="orders-total">
        <?= Yii::t('app', 'Orders today')?>: <?= $ordersTotal ?>
    </div>
</div>