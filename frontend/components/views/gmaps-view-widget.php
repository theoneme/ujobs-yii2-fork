<?php
/**
 * @var $model \yii\base\Model
 */

use yii\helpers\Json;

?>

<div id="gmaps_modal" style="width: 100%; height: 300px;"></div>

<?php $apiKey = Yii::$app->params['googleAPIKey'];
$address = addslashes($model->getAddress());

if (!empty($model->lat) && !empty($model->long)) {
    $center = ['lat' => (float)$model->lat, 'lng' => (float)$model->long];
    $placemark = <<<JS
        let marker = new google.maps.Marker({
            map: googleMapView,
            position: myLatLng,
            title: '$address'
        });
        let infowindow = new google.maps.InfoWindow({content: '$address'});
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
JS;
}
else {
    $center = ['lat' => 55.75396, 'lng' => 37.620393]; // Москва
    $placemark = '';
}
$center = Json::encode($center);

$script = <<<JS
    var googleMapView;
    if (typeof (googleMapView) === 'undefined') {
        $.getScript('https://maps.googleapis.com/maps/api/js?key={$apiKey}', function() {
            initMap();
        });
    } else {
        initMap();
    }
    
    function initMap() {
        let myLatLng = $center;
    
        googleMapView = new google.maps.Map($('#gmaps_modal').get(0), {
            center: myLatLng,
            zoom: 10
        });
        
        $placemark
    }
JS;

$this->registerJs($script);