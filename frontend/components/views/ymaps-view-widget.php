<?php

/**
 * @var $model \yii\base\Model
 */

?>

    <div id="ymaps_modal" style="width: 100%; height: 300px;"></div>

<?php $this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');

if (!empty($model->lat) && !empty($model->long)) {
    $center = "[{$model->lat}, {$model->long}]";
    $placemark = <<<JS
        let placemark = new ymaps.Placemark($center, {
            balloonContent: '$model->address'
        }, {
            preset: 'islands#icon'
        });
        myMap.geoObjects.add(placemark);
        placemark.balloon.open();
JS;
} else {
    $center = '[55.75396, 37.620393]'; // Москва
    $placemark = '';
}

$script = <<<JS
    ymaps.ready(function () {
        let myMap = new ymaps.Map('ymaps_modal', {
            center: $center,
            zoom: 12,
            controls: []
        });
        $placemark
    });
JS;

$this->registerJs($script);