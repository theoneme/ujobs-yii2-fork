<?php

use common\models\user\RegisterForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $registrationModel RegisterForm
 * @var $this \yii\web\View
 * @var $type integer|null
 */
?>


    <div class="modal fade" id="ModalSignup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="steps" data-step="1">
                        <div class="form-title"><?= Yii::t('app', 'Welcome to Ujobs') ?>!</div>
                        <div class="form-subtitle"><?= Yii::t('app', 'More than 3 million services. Secure Transactions.') ?></div>
                    </div>
                    <div class="steps" data-step="2" style="display: none;">
                        <div class="form-title"><?= Yii::t('app', 'Join Ujobs') ?></div>
                        <div class="form-subtitle"><?= Yii::t('app', 'Almost done - Less than 30 seconds!') ?></div>
                    </div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'signup-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/user/registration/register-ajax'],
                        'options' => [
                            'class' => 'formbot',
                            'data-step' => 1,
                            'data-final-step' => 1
                        ],
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'error-mes text-left'
                            ]
                        ]
                    ]); ?>
                    <div class="steps" data-step="1">
                        <?= $form->field($registrationModel, 'login', [
                            'inputOptions' => [
                                'placeholder' => Yii::t('app', 'Enter your email or phone')
                            ]
                        ]) ?>
                        <?= $form->field($registrationModel, 'password', [
                            'inputOptions' => [
                                'placeholder' => Yii::t('app', 'Choose a Password')
                            ]
                        ])->passwordInput() ?>
                        <?php
                        $invited_by = Yii::$app->response->cookies->getValue('invited_by');
                        if ($invited_by) {
                            echo Html::activeHiddenInput($registrationModel, 'invited_by', [
                                'value' => $invited_by
                            ]);
                        } ?>

                        <?php if ($type !== null) {
                            echo Html::activeHiddenInput($registrationModel, 'type', [
                                'value' => $type
                            ]);
                        } ?>
                    </div>
                    <?= Html::submitButton(Yii::t('app', 'Sign Up'), ['class' => 'btn-big width100']) ?>
                    <?php ActiveForm::end(); ?>
                    <div class="form-warning text-center">
                        <div class="fw-item">
                            <?= Yii::t('app', 'By joining I agree to {privacy} and {terms}, and receive emails from Ujobs.', [
                                'privacy' => Html::a(Yii::t('app', 'with privacy policy'),
                                    ['/ajax/load-page', 'alias' => 'privacy-policy'],
                                    ['class' => 'page-modal-toggle']
                                ),
                                'terms' => Html::a(Yii::t('app', 'with terms of service'),
                                    ['/ajax/load-page', 'alias' => 'terms-of-service'],
                                    ['class' => 'page-modal-toggle']
                                ),
                            ]) ?>
                        </div>
                    </div>
                    <div class="or-line text-center">
                        <div class="or-text"><?= Yii::t('app', 'or') ?></div>
                    </div>
                    <div class="social-login">
                        <?= Html::a('<i class="fa fa-facebook" aria-hidden="true"></i>', ['/user/security/auth', 'authclient' => 'facebook'], ['class' => 'fb text-center']) ?>
                        <?= Html::a('<i class="fa fa-google" aria-hidden="true"></i>', ['/user/security/auth', 'authclient' => 'google'], ['class' => 'goo text-center']) ?>
                    </div>
                    <div class="form-bottom text-center">
                        <?= Yii::t('app', 'Already a member?') ?>
                        <a data-dismiss="modal" data-toggle="modal"
                           data-target="#ModalLogin"><?= Yii::t('app', 'Sign In') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="page-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"></div>
                </div>
                <div class="modal-body">
                    <div class="page-content"></div>
                </div>
            </div>
        </div>
    </div>
<?php $this->registerJs("
	$('#signup-form').on('beforeValidateAttribute', function (event, attribute, messages) {
		var current_step = $(this).data('step');
		var input_step = $(attribute.container).closest('.steps').data('step');
		if (current_step != input_step) {
			return false;
		}
	});
	$('#signup-form').on('beforeSubmit', function (event) {
		var current_step = $(this).data('step');
		if (current_step != $(this).data('final-step')) {
			$('#ModalSignup div[data-step=\"' + current_step + '\"]').hide();
			current_step++;
			$(this).data('step', current_step);
			$('#ModalSignup div[data-step=\"' + current_step + '\"]').show();
			return false;
		}
	});
	$('#ModalSignup').on('hidden.bs.modal', function (e) {
		$('#signup-form')[0].reset();
		$('#signup-form').data('step', 1);
		$('#ModalSignup .steps').hide();
		$('#ModalSignup div[data-step=\"1\"]').show();
	});

	$(document).on('click', 'a.page-modal-toggle', function () {
	    $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            success: function (result) {
                if (result.error) {
                    alertCall('top', 'warning', result.error);
                }
                else {
                    $('#page-modal .form-title').html(result.title);
                    $('#page-modal .page-content').html(result.content);
                    $('#page-modal').modal('show');
                }
            }
        });
        return false;
    });
");