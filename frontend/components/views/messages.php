<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var integer $unreadMessages */

?>

<?php if ($unreadMessages > 0) { ?>
    <div class="header-cart-amount text-center" id="header-messages-count"><?= $unreadMessages ?></div>
<?php } ?>

<?= Html::a('<i class="fa fa-comment" aria-hidden="true"></i>', ['/account/inbox'], [
    'class' => 'icon-profnav text-center hint--bottom',
    'id' => 'header-messages-load',
    'data-hint' => Yii::t('account', 'Inbox')
]) ?>

    <div class="noti-block">
        <div class="noti-head clearfix">
            <div class="noti-title"><?= Yii::t('account', 'Inbox') ?></div>

            <div class="noti-options">
                <?= Html::a(Yii::t('messenger', 'Create Group Chat'), '#', [
                    'data-action' => 'create-group-chat',
                    'class' => 'link-dashb'
                ]) ?>
            </div>
        </div>
        <div class="noti-body" id="header-messages-container"></div>
        <?= Html::a(Yii::t('account', 'View All'), ['/account/inbox'], ['class' => 'noti-footer text-center']) ?>
    </div>

<?php $messagesRoute = Url::to(['/ajax/render-messages']);

$script = <<<JS
    let count = {$unreadMessages};

    $("#header-messages-load").on("click", function() {
        $('.cart-head').hide();
        $('.noti-block').hide();
        let self = $(this);
        if (!$('#header-messages-container').children().length) {
            $.get("{$messagesRoute}", {}, function(data) {
                $("#header-messages-container").html(data.html);
                
                if(data.subtract > 0) {
                    let newCount = count - data.subtract;
                    $('#header-messages-count').html(newCount);
                }
            }, "json");
        }
        self.siblings('.noti-block').show();
        return false;
    });
JS;

$this->registerJs($script);