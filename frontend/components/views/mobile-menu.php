<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 17:32
 */

use common\components\CurrencyHelper;
use common\models\user\User;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var array $categories
 * @var array $productCategories
 * @var array $userData
 */

?>

<div class="mobile-menu mLeft">
    <div class="mmhead">
        <?php if (!Yii::$app->user->isGuest) {
            /* @var $user User */
            $user = Yii::$app->user->identity;
            ?>
            <div class="flex">
                <a href="<?= Url::to($userData['sellerUrl']) ?>">
                    <div class="menu-avatar text-center">
                        <?php if ($user->company_user_id !== null || $user->profile->gravatar_email) {
                            echo Html::img($userData['thumb']);
                        } elseif ($userData['sellerName']) {
                            echo StringHelper::truncate(Html::encode($userData['sellerName']), 1, '');
                        } else {
                            echo StringHelper::truncate(Html::encode($user->username), 1, '');
                        } ?>
                    </div>
                </a>
                <div class="menu-infoprof">
                    <div class="menu-nick">
                        <div>
                            <a href="<?= Url::to($userData['sellerUrl']) ?>">
                                <?= $userData['sellerName'] ?>
                            </a>
                        </div>
                        <div class="menu-sum">
                            <a href="<?= Url::to(['/account/payment/index']) ?>">
                                <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], $user->getBalance()) ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <?= Html::a(Yii::t('app', 'Sign Up'), null, [
                'class' => 'btn-small',
                'data-toggle' => 'modal',
                'data-target' => '#ModalSignup'
            ]) ?>
        <?php } ?>
    </div>
    <div class="nav-links">
        <?= Html::a(Yii::t('app', 'Home'), '/', ['class' => 'mm-home']) ?>
        <?php /*= Html::a('Екатеринбург <i class="fa fa-angle-down" aria-hidden="true"></i>', '#', ['class' => 'mm-none'])*/ ?>
        <?= Html::a(Yii::t('app', 'Post announcement for free'), ['/entity/global-create'], ['class' => 'mm-fa mm-announcement']) ?>
        <?php if (!Yii::$app->user->isGuest) { ?>
            <?= Html::a(Yii::t('app', 'Messages') . "&nbsp;({$userData['unreadMessagesCount']})", ['/account/inbox'], ['class' => 'mm-fa mm-message']) ?>
            <?= Html::a(Yii::t('app', 'Notifications') . "&nbsp;({$userData['unreadNotificationsCount']})", ['/account/service/notifications'], ['class' => 'mm-fa mm-noti']) ?>
        <?php } ?>
        <?= Html::a(Yii::t('app', 'Products'), ['/board/category/products'], ['class' => 'mm-fa mm-product']) ?>
        <?= Html::a(Yii::t('app', 'Services'), ['/category/job-catalog'], ['class' => 'mm-fa mm-service']) ?>
        <?= Html::a(Yii::t('app', 'News feed'), ['/category/article-catalog'], ['class' => 'mm-fa mm-feed']) ?>
        <?= Html::a(Yii::t('app', 'Professionals'), ['/category/pro-catalog'], ['class' => 'mm-fa mm-pro']) ?>
        <?= Html::a(Yii::t('app', 'Requests'), ['/category/tender-catalog'], ['class' => 'mm-fa mm-tender']) ?>
        <?= Html::a(Yii::t('app', 'Tariffs'), ['/page/tariffs'], ['class' => 'mm-fa mm-tender']) ?>

        <?php if (!Yii::$app->user->isGuest) { ?>
            <?= Html::a(Yii::t('app', 'Dashboard'), ['/account/service/dashboard'], ['class' => 'mm-dashb']) ?>
            <?= Html::a(Yii::t('app', 'Favorites'), '#', ['class' => 'mm-fav']) ?>

            <?php if (!empty($userData['profiles'])) { ?>
                <div class="nav-subtitle"><?= Yii::t('account', 'My pages') ?></div>
                <?php foreach ($userData['profiles'] as $profile) { ?>
                    <?= Html::a($profile['title'], $profile['link'], $profile['htmlOptions']) ?>
                <?php } ?>
            <?php } ?>

            <div class="nav-subtitle"><?= Yii::t('app', 'Buying') ?></div>
            <?= Html::a(Yii::t('app', 'Post a Request'), Url::to(['/entity/global-create', 'type' => 'tender-service']), ['class' => 'mm-request']) ?>

            <div class="nav-subtitle"><?= Yii::t('app', 'Selling') ?></div>
            <?= Html::a(Yii::t('app', 'Post a Job'), Url::to(['/entity/global-create']), ['class' => 'mm-sales']) ?>

            <div class="nav-subtitle"><?= Yii::t('app', 'General') ?></div>
            <?= Html::a(Yii::t('app', 'Settings'), Url::to(['/account/service/account-settings']), ['class' => 'mm-set']) ?>
            <?= Html::a(Yii::t('app', 'Logout'), ['/user/security/logout'], ['class' => 'mm-logout', 'data-method' => 'post']) ?>
        <?php } ?>

        <div class="nav-subtitle"><?= Yii::t('app', 'Service categories') ?></div>
        <?php foreach ($categories as $category) { ?>
            <?= Html::a($category['translation']['title'], Url::to(['/category/jobs', 'category_1' => $category['translation']['slug']], true), ['class' => 'mm-none']) ?>
        <?php } ?>
        <div class="nav-subtitle"><?= Yii::t('app', 'Product categories') ?></div>
        <?php foreach ($productCategories as $category) { ?>
            <?= Html::a($category['translation']['title'], Url::to(['/board/category/products', 'category_1' => $category['translation']['slug']], true), ['class' => 'mm-none']) ?>
        <?php } ?>
    </div>
</div>
<div class="mobile-menu mRight">
    <div class="nav-links">
        <?= Html::a(Yii::t('app', 'Catalog of services'), ['/category/job-catalog'], [
            'class' => 'mm-fa mm-service'
        ]) ?>
        <?= Html::a(Yii::t('app', 'Catalog of products'), ['/board/category/products'], [
            'class' => 'mm-fa mm-product'
        ]) ?>
        <?= Html::a(Yii::t('app', 'Catalog of professionals'), ['/category/pro-catalog'], [
            'class' => 'mm-fa mm-pro'
        ]) ?>
        <?= Html::a(Yii::t('app', 'Request catalog'), ['/entity/catalog'], [
            'class' => 'mm-fa mm-tender'
        ]) ?>
        <?= Html::a(Yii::t('app', 'Portfolio Catalog'), ['/category/portfolio-catalog'], [
            'class' => 'mm-fa mm-portfolio'
        ]) ?>
        <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], [
            'class' => 'btn-big mm-fa mm-none text-center',
        ]) ?>

        <div class="select-wrap mm-lang">
            <select name="app_language" class="app_language">
                <?php foreach (Yii::$app->params['supportedLocales'] as $locale => $code) {
                    echo Html::tag('option', Yii::t('app', Yii::$app->params['languages'][$locale], [], $locale), [
                        'value' => Yii::$app->utility->localeCurrentUrl($code),
                        'selected' => $locale === Yii::$app->language
                    ]);
                } ?>
            </select>
        </div>
        <div class="select-wrap mm-cur">
            <select name="app_currency_code" class="app_currency_code">
                <?php foreach (Yii::$app->params['currencies'] as $k => $v) {
                    echo Html::tag('option', $v['sign'] . ' ' . $k, [
                        'value' => Url::current(['app_currency_code' => strtolower($k)]),
                        'selected' => $k === Yii::$app->params['app_currency_code']
                    ]);
                } ?>
            </select>
        </div>
    </div>
</div>
