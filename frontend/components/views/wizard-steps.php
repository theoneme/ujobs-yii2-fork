<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.03.2018
 * Time: 14:53
 */

/* @var $steps array */
/* @var $isMobile boolean */

?>

<div class="grey-steps <?= $isMobile === false ? 'hidden-xs' : 'hidden visible-xs' ?>">
    <div class="container-fluid">
        <ul class="gig-steps cart-steps">
            <?php foreach ($steps as $step) { ?>
                <li class="<?= $step['status'] ?>">
                    <a href="#"><?= $step['title'] ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
