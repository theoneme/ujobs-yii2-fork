<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 12:40
 */

use common\models\Notification;
use yii\helpers\Html;

/* @var Notification $model */

?>

<a href="<?= $model->link ?>" class="noti-alias">
    <div class="noti-avatar">
        <div class="text-center">
            <?= Html::img($model->getThumb('catalog')) ?>
        </div>
    </div>
    <div class="noti-info">
        <div class="noti-name">
            <p>
                <?= Html::encode($model->message ?? $model->getHeader()) ?>
            </p>
        </div>
        <div class="noti-extra">
            <?= Yii::$app->formatter->asDate($model->created_at) ?>
            <span class="noti-status hint--left" data-hint="Delivered">
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </span>
        </div>
    </div>
    <div class="noti-adds text-right">
        <span class="noti-unread hint--bottom" data-hint="Mark as Unread">
            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
        </span>
        <?php if (in_array($model->subject, [Notification::SUBJECT_ORDER_UPDATED, Notification::SUBJECT_YOU_HAVE_NEW_ORDER])) { ?>
            <div class="noti-preview">
                <?= Html::img($model->getThumb('catalog')) ?>
            </div>
        <?php } ?>
    </div>
</a>