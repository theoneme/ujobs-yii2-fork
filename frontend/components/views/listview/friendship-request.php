<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.04.2018
 * Time: 19:13
 */


use common\models\Notification;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var Notification $model
 * @var View $this
 */

?>

<div class="noti-alias">
    <div class="noti-avatar">
        <div class="text-center">
            <?= Html::img(Yii::$app->mediaLayer->getThumb($model->thumb)) ?>
        </div>
    </div>
    <div class="noti-info">
        <div class="noti-name">
            <p>
                <?= Html::a(Html::encode($model->message ?? $model->getHeader()), $model->link) ?>
            </p>
        </div>
        <div class="noti-extra">
            <?= Yii::$app->formatter->asDate($model->created_at) ?>
            <span data-role="action-buttons">
                <?php if ($model->customDataArray['action'] === null) { ?>
                    <?= Html::a(Yii::t('app', 'Accept'), ['/friend/accept-friendship'], [
                        'data-action' => 'friendship-invite-accept',
                        'data-entity-id' => $model->from_id,
                        'data-invite-id' => $model->id,
                        'class' => 'btn btn-success-inverse btn-xs'
                    ]) ?>
                    <?= Html::a(Yii::t('app', 'Decline'), ['/friend/decline-friendship'], [
                        'data-action' => 'friendship-invite-decline',
                        'data-entity-id' => $model->from_id,
                        'data-invite-id' => $model->id,
                        'class' => 'btn btn-danger-inverse btn-xs'
                    ]) ?>
                <?php } else if ($model->customDataArray['action'] === Notification::ACTION_ACCEPT) { ?>
                    <span><?= Yii::t('notifications', 'You have accepted friendship') ?></span>
                <?php } else if ($model->customDataArray['action'] === Notification::ACTION_DECLINE) { ?>
                    <span><?= Yii::t('notifications', 'You have declined friendship') ?></span>
                <?php } ?>
            </span>
        </div>
    </div>
    <div class="noti-adds text-right">
        <span class="noti-unread hint--bottom" data-hint="Mark as Unread">
            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
        </span>
    </div>
</div>

