<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.04.2018
 * Time: 16:44
 */

use common\models\Notification;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var Notification $model
 * @var View $this
 */

?>

<div class="noti-alias">
    <div class="noti-avatar">
        <div>
            <?= Html::img(Yii::$app->mediaLayer->getThumb($model->thumb)) ?>
        </div>
    </div>
    <div class="noti-info">
        <div class="noti-name">
            <p>
                <?= Html::a(Html::encode($model->message ?? $model->getHeader()), $model->link) ?>
            </p>
        </div>
        <div class="noti-extra">
            <?= Yii::$app->formatter->asDate($model->created_at) ?>
            <span data-role="action-buttons">
                <?php if ($model->customDataArray['action'] === null) { ?>
                    <?= Html::a(Yii::t('app', 'Accept'), ['/account/service-ajax/accept-company-invitation'], [
                        'data-action' => 'notification-accept-invite',
                        'data-entity-id' => $model->customDataArray['company_id'],
                        'data-invite-id' => $model->id,
                        'class' => 'btn btn-success-inverse btn-xs'
                    ]) ?>
                    <?= Html::a(Yii::t('app', 'Decline'), ['/account/service-ajax/decline-company-invitation'], [
                        'data-action' => 'notification-decline-invite',
                        'data-entity-id' => $model->customDataArray['company_id'],
                        'data-invite-id' => $model->id,
                        'class' => 'btn btn-danger-inverse btn-xs'
                    ]) ?>
                <?php } else if ($model->customDataArray['action'] === Notification::ACTION_ACCEPT) { ?>
                    <span><?= Yii::t('notifications', 'You have accepted invitation') ?></span>
                <?php } else if ($model->customDataArray['action'] === Notification::ACTION_DECLINE) { ?>
                    <span><?= Yii::t('notifications', 'You have declined invitation') ?></span>
                <?php } ?>
            </span>
        </div>
    </div>
    <div class="noti-adds text-right">
        <span class="noti-unread hint--bottom" data-hint="Mark as Unread">
            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
        </span>
    </div>
</div>

