<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.05.2017
 * Time: 15:24
 */

use yii\helpers\Html;
use common\models\Notification;

/* @var Notification $model */

?>

<a href="<?= $model->link ?>" class="noti-alias">
	<div class="noti-avatar">
		<div class="text-center">
			<?= Html::img('/images/new/icons/support-icon.png', ['alt' => 'support']) ?>
		</div>
	</div>
	<div class="noti-info">
		<div class="noti-name">
			<p>
				<?= Html::encode($model->message ?? $model->getHeader()) ?>
			</p>
		</div>
		<div class="noti-extra">
			<?= Yii::$app->formatter->asDate($model->created_at) ?>
		</div>
	</div>
	<div class="noti-adds text-right">
        <span class="noti-unread hint--bottom" data-hint="Mark as Unread">
            <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
        </span>
	</div>
</a>