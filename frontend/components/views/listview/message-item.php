<?php

use common\models\Conversation;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var array $data */

?>

<a href="<?= Url::to(['/account/inbox/conversation', 'id' => $data['conversationId']])?>"
   class="noti-alias"
    <?= $data['type'] !== Conversation::TYPE_INFO ? 'data-live="1"' : ''?>
   data-dialog-id="<?= $data['conversationId'] ?>"
>
    <div class="noti-avatar">
        <div class="text-center">
            <?= Html::img($data['image']) ?>
        </div>
    </div>
    <div class="noti-info">
        <div class="noti-name">
            <p>
                <?= ($data['icon'] ? Html::tag('i', '&nbsp;', ['class' => $data['icon']]) : null) . $data['name'] ?>
            </p>
            <div class="noti-subinfo <?= $data['unread'] ? 'unread' : '' ?>">
                <div class="noti-subinfo-avatar"><?= Html::img($data['messageImage']) ?></div>
                <div class="noti-subinfo-text">
                    <?php if (strlen($data['text']) > 1) { ?>
                        <?= \yii\helpers\StringHelper::truncate(Html::encode($data['text']), 100) ?>
                    <?php } else if (!empty($data['attachments'])) { ?>
                        <?= Yii::t('app', 'Attachment') ?>
                    <?php } else if (strpos($data['text'], '.') !== false) { ?>
                        <?= Yii::t('app', 'System message (individual job)') ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="noti-extra">
            <?= $data['createdAt'] ?>
        </div>
    </div>
</a>