<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.01.2019
 * Time: 13:43
 */

use yii\helpers\Html;

?>

<div class="container-fluid crosslink">
    <h2 class="text-center">
        <?= Yii::t('app', 'People are also looking for') ?>
    </h2>
    <div class="row">
        <?php foreach ($data as $column) { ?>
            <div class="col-md-3">
                <h3><?= $column['title'] ?></h3>
                <?php foreach ($column['items'] as $item) { ?>
                    <h4>
                        <?= Html::a($item['title'], $item['url'], ['data-pjax' => 0]) ?>
                    </h4>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
