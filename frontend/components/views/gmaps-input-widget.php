<?php
/**
 * @var $this View
 * @var $model \yii\base\Model
 * @var $inputOptions array
 * @var $modal boolean
 */

use yii\helpers\Html;
use yii\web\View;

?>

<?= Html::activeHiddenInput($model, 'lat', ['id' => 'gmaps-input-lat'])?>
<?= Html::activeHiddenInput($model, 'long', ['id' => 'gmaps-input-long'])?>
<?= Html::activeTextInput($model,
    'address',
    array_merge([
        'id' => 'gmaps-input-address',
        'placeholder' => Yii::t('board', 'Enter address'),
    ], $inputOptions)
);
$apiKey = Yii::$app->params['googleAPIKey'];
$language = Yii::$app->params['supportedLocales'][Yii::$app->language];
if ($language === 'ua') {
    $language = 'uk';
}
$script = <<<JS
    var googleAutocomplete;
    if (typeof(googleAutocomplete) === 'undefined') {
        $.getScript('https://maps.googleapis.com/maps/api/js?key={$apiKey}&libraries=places&language={$language}', function() {
            initAutocomplete();
        });
    } else {
        initAutocomplete();
    }

    function initAutocomplete() {
        googleAutocomplete = new google.maps.places.Autocomplete(
            $('#gmaps-input-address').get(0), {
                types: ['address']
            }
        );
        googleAutocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        let place = googleAutocomplete.getPlace();
        if (typeof place.address_components !== "undefined") {
            let fullAddress = [];
            for (let i = 0; i < place.address_components.length; i++) {
                let addressPart = place.address_components[i].long_name;
                if (addressPart.length) {
                    let addressType = place.address_components[i].types[0];
                    switch (addressType) {
                        case 'route':
                            fullAddress[0] = addressPart;
                            break;
                        case 'street_number':
                            fullAddress[1] = addressPart;
                            break;
                        case 'locality':
                            fullAddress[2] = addressPart;
                            break;
                        case 'administrative_area_level_1':
                            fullAddress[3] = addressPart;
                            break;
                        case 'country':
                            fullAddress[4] = addressPart;
                            break;
                    }
                }
            }
            fullAddress = fullAddress.filter(String);
            $('#gmaps-input-lat').val(place.geometry.location.lat());
            $('#gmaps-input-long').val(place.geometry.location.lng());
            $('#gmaps-input-address').val(fullAddress.join(', '));
        } else {
            $('#gmaps-input-lat').val('');
            $('#gmaps-input-long').val('');
            $('#gmaps-input-address').val('');
        }
    }
JS;

$this->registerJs($script);