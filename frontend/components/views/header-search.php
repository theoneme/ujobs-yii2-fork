<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.11.2016
 * Time: 14:44
 */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\IndexSearchForm;

/* @var IndexSearchForm $searchModel */

?>
<div class="input-group search">
    <?= Html::beginForm(['/search/search'], 'get', ['id' => 'header-form']) ?>
        <div class="form-group">
            <div class="input-group">
                <?= Html::textInput('request', '', [
                    'class' => 'form-control autocomplete',
                    'type' => 'search',
                    'data-f' => 'header-form',
                    'placeholder' => Yii::t('app', 'Search')
                ]) ?>
                <div class="input-group-addon">
                    <button type="submit" class="button-as-link"><i class="fa fa-1 fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    <?= Html::endForm(); ?>
</div>

<?php $catalogSearchUrl = Url::to(['/ajax/catalog-search', 'type' => 'job']);
$script = <<<JS
    let xhr;
    new autoComplete({
        selector: "#header-form .autocomplete",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$catalogSearchUrl}', { request: request }, function(data) { 
                let autoCSource = [];
                if (data.success === true) {
                    $.each(data.data, function(key, value) {
                        autoCSource.push(value.label);
                    });
                }
                response(autoCSource); 
            });     
        },
    });
JS;

$this->registerJs($script);