<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.12.2016
 * Time: 18:59
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var integer $unreadNotifications */

?>

<?php if ($unreadNotifications > 0) { ?>
    <div class="header-cart-amount text-center" id="header-noti-count"><?= $unreadNotifications ?></div>
<?php } ?>

<?= Html::a('<i class="fa fa-bullhorn" aria-hidden="true"></i>', '#', [
    'class' => 'icon-profnav text-center hint--bottom hid',
    'id' => 'noti-id',
    'data-hint' => Yii::t('labels', 'Notifications')
]) ?>

    <div class="noti-block">
        <div class="noti-head clearfix">
            <div class="noti-title"><?= Yii::t('account', 'Notifications') ?></div>
            <div class="noti-options">
                <a href="<?= Url::to(['/account/service/account-settings']) ?>" class="noti-opt-icons hint--bottom"
                   data-hint="<?= Yii::t('account', 'Notifications Settings') ?>">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                </a>
                <?= Html::a(Yii::t('account', 'View Dashboard'), ['/account/service/notifications'], ['class' => 'link-dashb']) ?>
            </div>
        </div>
        <div class="noti-body">
            <div id="notifications-container">

            </div>
        </div>
        <?= Html::a(Yii::t('account', 'View All'), ['/account/service/notifications'], ['class' => 'noti-footer text-center']) ?>
    </div>

<?php $notificationRoute = Url::to(['/ajax/render-notifications']);

$script = <<<JS
    let notifications = {$unreadNotifications};

    $("#noti-id").on("click", function() {
        $('.cart-head').hide();
        $('.noti-block').hide();
        let self = $(this);
        $.get("{$notificationRoute}", {}, function(data) {
            if(data.success === true) {
                $("#notifications-container").html(data.html);
                
                if(data.subtract > 0) {
                    let newNoti = notifications - data.subtract;
                    
                    $('#header-noti-count').html(newNoti);
                }
                self.siblings('.noti-block').show();
            }
        }, "json");
        
        return false;
    });
    
    $(document).on('click', '[data-action="notification-accept-invite"], [data-action="notification-decline-invite"], [data-action="friendship-invite-accept"], [data-action="friendship-invite-decline"]', function() {
        let entityId = $(this).data('entity-id'), 
            inviteId = $(this).data('invite-id'),
            url = $(this).attr('href'),
            self = $(this);
        
        $.post(url, {entityId: entityId, inviteId: inviteId}, function(response) {
             if(response.success === true) {
                 alertCall('top', 'success', response.message);
                 self.closest('[data-role="action-buttons"]').replaceWith('<span>' + response.message + '</span>');
             } else {
                 alertCall('top', 'error', response.message);
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);