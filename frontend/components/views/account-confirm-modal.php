<?php
use yii\helpers\Html;

/**
 * @var $confirmUserId integer
 * @var $this \yii\web\View
 */
?>

<div class="modal fade" id="ModalConfirm" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="form-title"><?= Yii::t('app', 'Enter the confirmation code')?></div>
			</div>
			<div class="modal-body">
				<?= Html::beginForm('/user/registration/confirm', 'get', ['class' => 'formbot']); ?>
					<?= Html::hiddenInput('id', $confirmUserId); ?>
					<?= Html::textInput('code', null, ['placeholder' => Yii::t('app', 'Confirmation code')]) ?>
					<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
				<?= Html::endForm(); ?>
			</div>
		</div>
	</div>
</div>
<?php $this->registerJs("
	$('#ModalConfirm').modal('show');
");