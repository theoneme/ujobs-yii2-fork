<?php

use common\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $categories Category[]
 * @var $productCategories Category[]
 */

?>

<nav class="top-menu hidden-xs">
    <div class="container-fluid">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-menu">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse" id="top-menu">
            <ul class="nav nav-tabs">
                <?php $iterator = 0;
                foreach ($categories as $key => $category) {
                    /** @var array $children */
                    $children = $category['children'];
                    if (!empty($children)) {
                        $childrenMiddleIndex = round(count($children) / 2);
                        ?>
                        <li class="dropdown">
                            <?= Html::a(
                                $category['translation']['title'],
                                Url::to(['/category/jobs', 'category_1' => $category['translation']['slug']]),
                                [
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown',
                                    'aria-haspopup' => 'true',
                                    'aria-expanded' => 'false'
                                ]
                            ); ?>
                            <i class="fa fa-2 fa-angle-down" aria-hidden="true"></i>
                            <?php if ($iterator++ > 5) continue; ?>
                            <div class="drop-menu">
                                <ul>
                                    <?php $k = 0;
                                    foreach ($children as $id => $child) { ?>
                                        <li>
                                            <?= Html::a(
                                                $child['translation']['title'],
                                                Url::to(['/category/jobs', 'category_1' => $child['translation']['slug']])
                                            ); ?>
                                        </li>
                                        <?= (($k !== 0) && (($k + 1) % $childrenMiddleIndex) === 0) ? '</ul><ul>' : ''; ?>
                                        <?php $k++;
                                    } ?>
                                </ul>
                            </div>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li>
                            <?= Html::a(
                                $category['translation']['title'],
                                Url::to(['/category/jobs', 'category_1' => $category['translation']['slug']])
                            ); ?>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li class="dropdown">
                    <?= Html::a(Yii::t('app', 'More') . ' ' . Yii::t('app', 'services'), ['/category/job-catalog'], [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'aria-haspopup' => 'true',
                        'aria-expanded' => 'false'
                    ]) ?>
                    <i class="fa  fa-2 fa-angle-down" aria-hidden="true"></i>

                    <div class="drop-menu drop100">
                        <ul></ul>
                        <ul></ul>
                    </div>
                </li>
            </ul>
            <ul class="nav nav-tabs">
                <?php $iterator = 0;
                foreach ($productCategories as $key => $category) {
                    $iterator++;
                    /** @var array $children */
                    $children = $category['children'];
                    if (!empty($children)) {
                        $childrenMiddleIndex = round(count($children) / 2);
                        ?>
                        <li class="dropdown">
                            <?= Html::a(
                                $category['translation']['title'],
                                Url::to(['/board/category/products', 'category_1' => $category['translation']['slug']]),
                                [
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown',
                                    'aria-haspopup' => 'true',
                                    'aria-expanded' => 'false'
                                ]
                            ); ?>
                            <i class="fa fa-2 fa-angle-down" aria-hidden="true"></i>
                            <?php if ($iterator > 9) continue; ?>
                            <div class="drop-menu">
                                <ul>
                                    <?php $k = 0;
                                    foreach ($children as $id => $child) { ?>
                                        <li>
                                            <?= Html::a(
                                                $child['translation']['title'],
                                                Url::to(['/board/category/products', 'category_1' => $child['translation']['slug']])
                                            ); ?>
                                        </li>
                                        <?= (($k !== 0) && (($k + 1) % $childrenMiddleIndex) === 0) ? '</ul><ul>' : ''; ?>
                                        <?php $k++;
                                    } ?>
                                </ul>
                            </div>
                        </li>
                    <?php } else { ?>
                        <li>
                            <?= Html::a(
                                $category['translation']['title'],
                                Url::to(['/board/category/products', 'category_1' => $category['translation']['slug']])
                            ); ?>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li class="dropdown">
                    <?= Html::a(Yii::t('app', 'More') . ' ' . Yii::t('app', 'products'), ['/board/category/products'], [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'aria-haspopup' => 'true',
                        'aria-expanded' => 'false'
                    ]) ?>
                    <i class="fa  fa-2 fa-angle-down" aria-hidden="true"></i>

                    <div class="drop-menu drop100">
                        <ul></ul>
                        <ul></ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>