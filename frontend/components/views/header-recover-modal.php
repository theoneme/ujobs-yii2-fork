<?php

use common\models\user\RecoveryForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $model RecoveryForm
 * @var $this \yii\web\View
 */
?>

<div class="modal fade" id="ModalRecover" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="form-title"><?= Yii::t('app', 'Reset Password')?></div>
					<div class="form-subtitle"><?= Yii::t('app', 'Enter the email you used in registration. A password reset link will be sent to your email.')?></div>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin([
					'id' => 'recovery-form',
					'enableAjaxValidation' => true,
					'enableClientValidation' => false,
					'action' => ['/user/recovery/request-ajax'],
					'options' => [
						'class' => 'formbot',
					],
					'fieldConfig' =>  [
						'template' => "{input}\n{error}",
						'errorOptions' => [
							'tag' => 'label',
							'class' => 'error-mes text-left'
						]
					]
				]); ?>
						<?= $form->field($model, 'email', [
							'inputOptions' => [
								'placeholder' => Yii::t('app', 'Enter email address')
							]
						]) ?>
					<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
				<?php ActiveForm::end(); ?>
				<div class="form-bottom text-center" style="margin-top: 20px;">
					<a data-dismiss="modal" data-toggle="modal" data-target="#ModalLogin"><?= Yii::t('app', 'Back to Sign In') ?></a>
				</div>
			</div>
		</div>
	</div>
</div>