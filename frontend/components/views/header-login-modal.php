<?php

use common\models\user\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $loginModel LoginForm
 */

?>

<div class="modal fade" id="ModalLogin" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="form-title"><?= Yii::t('app', 'Log in')?></div>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin([
					'id' => 'login-form',
					'enableAjaxValidation' => true,
					'enableClientValidation' => false,
					'action' => ['/user/registration/login-ajax'],
					'options' => [
						'class' => 'formbot'
					],
					'fieldConfig' =>  [
						'template' => "{input}\n{error}",
						'errorOptions' => [
							'tag' => 'label',
							'class' => 'error-mes text-left'
						]
					]
				]); ?>
					<?= $form->field($loginModel, 'login', [
						'inputOptions' => [
							'placeholder' => Yii::t('app', 'Email') . ' / ' . Yii::t('app', 'Phone')
						]
					]) ?>
					<?= $form->field($loginModel, 'password', [
						'inputOptions' => [
							'placeholder' => Yii::t('app', 'Password')
						]
					])->passwordInput() ?>
					<?= Html::submitButton(Yii::t('app', 'Log in'), ['class' => 'btn-big width100']) ?>
					<div class="add-form-op row">
						<div class="chover remember">
							<?= $form->field($loginModel, 'rememberMe', [
								'template' => '{input}',
								'options' => [
									'tag' => false
								]
							])->checkbox(['id' => 'rem-check-box'], false) ?>
							<label for="rem-check-box">
								<span class="ctext"><?= Yii::t('app', 'Remember me') ?></span>
							</label>
						</div>
						<?= Html::a(Yii::t('app', 'Forgot Password') . '?', null, [
							'class' => 'forgot',
							'data-dismiss' => 'modal',
							'data-toggle' => 'modal',
							'data-target' => '#ModalRecover'
						]) ?>
					</div>
				<?php ActiveForm::end(); ?>
				<div class="or-line text-center">
					<div class="or-text"><?= Yii::t('app', 'or')?></div>
				</div>
				<div class="social-login">
                    <?= Html::a('<i class="fa fa-facebook" aria-hidden="true"></i>', ['/user/security/auth', 'authclient' => 'facebook'], ['class' => 'fb text-center'])?>
                    <?= Html::a('<i class="fa fa-google" aria-hidden="true"></i>', ['/user/security/auth', 'authclient' => 'google'], ['class' => 'goo text-center'])?>
				</div>
				<div class="form-bottom text-center">
					<?= Yii::t('app', 'Not a member yet?') ?>
					<a data-dismiss="modal" data-toggle="modal" data-target="#ModalSignup">
						<?= Yii::t('app', 'Register now') ?>
					</a>
					— <?= Yii::t('app', 'it\'s fun and easy!')?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->registerJs("
	$('#ModalLogin').on('hidden.bs.modal', function (e) {
		$('#login-form')[0].reset();
	});
")?>