<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.06.2017
 * Time: 11:51
 */

use yii\helpers\Html;

/* @var $categories array */

?>

<div class="row">
    <div class="col-md-3 col-sm-3">
        <div class="footer-title"><?= Yii::t('app', 'Categories') ?></div>
        <?php foreach ($categories as $category) {
            echo Html::a(
                $category['translation']['title'],
                ['/category/jobs', 'category_1' => $category['translation']['slug']]
            );
        } ?>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="footer-title"><?= Yii::t('app', 'About us') ?></div>
        <?= Html::a(Yii::t('app', 'Contact us'), ['/site/contact']) ?>
        <?= Html::a(Yii::t('app', 'News'), ['/news/list']) ?>
        <?= Html::a(Yii::t('app', 'Become a freelancer'), ['/page/become-freelancer']) ?>
        <?= Html::a(Yii::t('app', 'Sell products'), ['/page/product-landing']) ?>
        <?= Html::a(Yii::t('app', 'Outsourcing of managers'), ['/page/outsource-mpp']) ?>
        <?= Html::a(Yii::t('app', 'Increase freelancer`s salary'), ['/page/start-selling']) ?>
        <?= Html::a(Yii::t('app', 'Tariffs'), ['/page/tariffs']) ?>
        <?= Html::a(Yii::t('app', 'Catalog of professionals'), ['/category/pro-catalog']) ?>
        <?= Html::a(Yii::t('app', 'Catalog of products'), ['/board/category/products']) ?>
        <?= Html::a(Yii::t('app', 'Video Catalog'), ['/category/video-catalog']) ?>
        <?= Html::a(Yii::t('app', 'Portfolio Catalog'), ['/category/portfolio-catalog']) ?>
        <?= Html::a(Yii::t('app', 'Article Catalog'), ['/category/article-catalog']) ?>
        <?= Html::a(Yii::t('app', 'Search by service tag'), ['/attribute-value/search', 'alias' => 'tag']) ?>
        <?= Html::a(Yii::t('app', 'Search by product tag'), ['/attribute-value/search', 'alias' => 'product_tag']) ?>
        <?= Html::a(Yii::t('app', 'Search by portfolio tag'), ['/attribute-value/search', 'alias' => 'portfolio_tag']) ?>
        <?= Html::a(Yii::t('app', 'Search by skill'), ['/attribute-value/search', 'alias' => 'skill']) ?>
        <?= Html::a(Yii::t('app', 'Search by specialty'), ['/attribute-value/search', 'alias' => 'specialty']) ?>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="footer-title"><?= Yii::t('app', 'Support') ?></div>
        <?= Html::a(Yii::t('app', 'Contact Support'), ['/account/inbox/start-conversation', 'user_id' => Yii::$app->params['supportId']]) ?>
        <?= Html::a(Yii::t('app', 'Terms of service'), ['/page/static', 'alias' => 'terms-of-service']) ?>
        <?= Html::a(Yii::t('app', 'Privacy policy'), ['/page/static', 'alias' => 'privacy-policy']) ?>
        <?= Html::a(Yii::t('app', 'Payment methods'), ['/page/static', 'alias' => 'pay']) ?>
        <?= Html::a(Yii::t('app', 'Payment process'), ['/page/static', 'alias' => 'payment-process']) ?>
        <?= Html::a(Yii::t('app', 'Payment system PayAnyWay'), ['/page/static', 'alias' => 'pay-any-way']) ?>
        <?= Html::a(Yii::t('app', 'Fair Play'), ['/page/static', 'alias' => 'fair-play']) ?>
        <?= Html::a(Yii::t('app', 'Referral Program'), ['/referral/index']) ?>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="footer-title"><?= Yii::t('app', 'Contact us') ?></div>
        <a rel="nofollow" target="_blank" class="footer-follow"
           href="https://www.facebook.com/Ujobsme-425244320979311/">
            <i class="fa fa-facebook" aria-hidden="true"></i>Facebook
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="https://vk.com/clubujobs">
            <i class="fa fa-vk" aria-hidden="true"></i>Vkontakte
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="https://twitter.com/UjobsFreelance">
            <i class="fa fa-twitter" aria-hidden="true"></i>Twitter
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow"
           href="https://plus.google.com/u/0/106033473256064939407">
            <i class="fa fa-google-plus" aria-hidden="true"></i>Google
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="#">
            <i class="fa fa-pinterest-p" aria-hidden="true"></i>Pinterest
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="https://www.instagram.com/ujobs.me777/">
            <i class="fa fa-instagram" aria-hidden="true"></i>Instagram
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="https://www.linkedin.com/in/ujobs-me-79ba39160/">
            <i class="fa fa-linkedin" aria-hidden="true"></i>LinkedIn
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow"
           href="https://zen.yandex.ru/media/id/5a6ee0d748c85e1395022a51">
            <i class="fa fa-link" aria-hidden="true"></i>YandexZen
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="https://user.qzone.qq.com/3474700193">
            <i class="fa fa-link" aria-hidden="true"></i>QZone
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="https://weibo.com/6475312275/profile">
            <i class="fa fa-weibo" aria-hidden="true"></i>Weibo
        </a>
        <a rel="nofollow" target="_blank" class="footer-follow" href="http://www.renren.com/963658216">
            <i class="fa fa-link" aria-hidden="true"></i>RenRen
        </a>
        <div class="payment-icons">
            <div><?= Yii::t('app', 'We accept:') ?></div>
            <?= Html::img(['/images/new/payment/mastercard.png'], ['alt' => 'mastercard', 'title' => 'mastercard']) ?>
            <?= Html::img(['/images/new/payment/visa.png'], ['alt' => 'visa', 'title' => 'visa']) ?>
            <?= Html::img(['/images/new/payment/american.png'], ['alt' => 'american', 'title' => 'american']) ?>
            <?= Html::img(['/images/new/payment/diners.png'], ['alt' => 'diners', 'title' => 'diners']) ?>
        </div>
    </div>
</div>
