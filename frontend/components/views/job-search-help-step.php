<?php

use common\models\Job;
use common\models\JobSearchOption;
use common\models\JobSearchStep;
use frontend\assets\JobSearchAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $job Job
 * @var $step JobSearchStep
 * @var $this View
 * @var $lvl integer
 * @var $forceShow boolean
 */

$utility = Yii::$app->utility;
$options = $step->options;
$priceRanges = [];
if ($step->show_prices) {
    $priceRanges = ArrayHelper::map($options, 'id', function ($var) {
        /** @var JobSearchOption $var */
        return $var->getPriceRange();
    });
    uasort($options, function ($a, $b) use ($priceRanges) {
        return $priceRanges[$a->id]['min'] - $priceRanges[$b->id]['min'];
    });
}

JobSearchAsset::register($this);

?>
    <div class="job-search-toggle-block <?= $forceShow ? 'hide' : '' ?>">
        <div class="help-header text-center">
            <?= Yii::t('app', 'We developed a new professional search functionality.') ?>
        </div>
        <div class="help-box">
            <div class="help-step-title text-center">
                <?= Yii::t('app', 'We can find the best professional for your task, just as easy as ordering a taxi.') ?>
            </div>
            <div class="help-step">
                <?= Html::a(Yii::t('app', 'Try it'), null, ['class' => 'job-search-toggle button green auto']) ?>
            </div>
        </div>
    </div>
    <div class="help-content <?= $forceShow ? 'show' : '' ?>">
        <div class="container-fluid">
            <div class="help-header text-center">
                <?= $step->getTitle() . ' ' . Html::a(Yii::t('app', 'Learn more'), null, ['data-toggle' => 'modal', 'data-target' => '#jobSearchMoreModal']) ?>
                <div id="help-close" class="help-close">&times;</div>
            </div>
            <div class="help-box">
                <div class="help-step-title text-center">
                    <?= Yii::t('app', 'Step') . ' ' . $lvl . '. ' . $step->getSubtitle() ?>
                </div>
                <div class="help-step">
                    <?php
                    foreach ($options as $id => $option) {
                        if ($option->next_step_id || $option->getJobId()) { ?>
                            <div class="help-radio step-option <?= $step->show_prices ? 'job-step-option' : '' ?>">
                                <input id="job-search-option-<?= $id ?>" name="help-style" value="<?= $id ?>"
                                       type="radio" data-next-step="<?= $option->next_step_id ?>"
                                       data-job="<?= $option->getJobId() ?>">
                                <label for="job-search-option-<?= $id ?>">
                                    <?= Html::img($option->getThumb('catalog'),['alt' => $option->getLabel(), 'title' => $option->getLabel()]) ?>
                                    <span class="help-radio-title text-center">
                                    <?= $option->getLabel() ?>
                                </span>
                                    <?php if ($step->show_prices) {
                                        $priceRange = $utility->formatRangeWithThousands(Yii::$app->params['app_currency_code'], $priceRanges[$option->id]['min'], $priceRanges[$option->id]['max']);
                                        echo '<span class="help-radio-title text-center price-range">' . $priceRange . '</span>';
                                    } ?>
                                </label>
                            </div>
                        <?php }
                    }
                    if ($lvl === 1) { ?>
                        <div class="help-radio uhome-link">
                            <input id="job-search-option-uh" name="help-style" value="uh" type="radio">
                            <label for="job-search-option-uh">
                                <?= Html::img(['/images/new/uhome-house.jpg'],['alt' => Yii::t('app', 'Construction'), 'title' => Yii::t('app', 'Construction')]) ?>
                                <span class="help-radio-title text-center">
                                <?= Yii::t('app', 'Construction') ?>
                            </span>
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="jobSearchMoreModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <?= $step->getDescription() ?>
                    <?= Html::a(Yii::t('app', 'Continue'), null, ['class' => 'btn-big width100', 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
    </div>
<?php
$cartUrl = Url::to(['/store/cart/index']);
$addToCartUrl = Url::to(['/store/cart/add']);
$nextStepUrl = Url::to(['/ajax/job-search-step']);
$script = <<<JS
    let lvl = $lvl;
    let price = 0;
    let units = 0;
    
    $(document).on('click', '#help-close', function() {
        $('.help-content').slideUp(300, function() {
            $('.help-content').removeClass('show');
        });
    });
    
    $(document).on('click', '.help-radio.uhome-link', function() {
        window.location.href = 'http://uhome.site/';
        return false;
    });
    
    $(document).on('click', '.help-radio.budget', function() {
        if ($(this).closest('.help-step').hasClass('price-per-unit')) {
            $(this).closest('.container-fluid').hide();
            price = parseInt($(this).data('price'));
            units = parseInt($(this).data('units'));
            $('.help-step-calculator span.price, .help-step-calculator span.price-one').html(price);
            $('.job-search-quantity-block .help-header span.price').html($(this).find('.help-radio-title').html());
            $('#job-search-package-id').val($(this).find('input[name="help-budget"]').data('id'));
            $('.job-search-quantity-block').show();
        } else {
            let id = $(this).find('input[name="help-budget"]').data('id');
            $.ajax({
                url: '$addToCartUrl',
                type: 'post',
                data: {
                    id: id
                },
                success: function(response) {
                    $('#cart-head-container').replaceWith(response);
                    window.location.href = '$cartUrl';
                },
            });
        }
        return false;
    });
    
    $(document).on('click', '.help-radio.step-option', function() {
        let nextStepId = $(this).find('input[name="help-style"]').data('next-step');
        let jobId = $(this).find('input[name="help-style"]').data('job');
        lvl++;
        $.ajax({
            url: '$nextStepUrl',
            type: 'post',
            data: {
                step_id: nextStepId,
                job_id: jobId,
                lvl: lvl
            },
            success: function(response) {
                $('#job-search-help').html(response);
                if ($('.help-radio.budget').length === 1 && $('.help-radio.budget').closest('.help-step').hasClass('price-per-unit')) {
                    $('.help-radio.budget').trigger('click');
                }
            },
        });
        
        return false;
    });

    $(document).on('click', '.add-to-cart', function() {
        let id = $('#job-search-package-id').val();
        let quantity = $('#job-search-quantity').val();
        $.ajax({
            url: '$addToCartUrl',
            type: 'post',
            data: {
                id: id,
                quantity: quantity
            },
            success: function(response) {
                $('#cart-head-container').replaceWith(response);
                window.location.href = '$cartUrl';
            },
        });
        
        return false;
    });

    $(document).on('click', '.job-search-toggle', function() {
        $(this).closest('.job-search-toggle-block').slideUp(400, function() {
            $('.help-content').slideDown(400);
        });
        
        return false;
    });
    
    $(document).on("keyup", "#job-search-quantity", function() {
        let amount = parseInt($(this).val());
        if (amount > 0) {
            $('.help-step-calculator span.units').html(amount * units);
            $('.help-step-calculator span.price').html(amount * price);
        }
    });
JS;
$this->registerJs($script);
