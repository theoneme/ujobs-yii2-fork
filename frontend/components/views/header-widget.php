<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:25
 */

use common\components\CurrencyHelper;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\store\components\PopupCart;
use common\widgets\HeaderAlert;
use frontend\components\Messages;
use frontend\components\Notifications;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var integer $unreadNotificationsCount
 * @var array $userData
 * @var array $menuItems
 * @var boolean $askSubscribe
 */
?>

<?= HeaderAlert::widget() ?>

    <div class="dark-head">
        <div class="container-fluid">
            <div class="logo-search">
                <div class="trigger-menu visible-xs">
                    <i></i>
                </div>
                <a class="logo text-center" href="/">
                    <?= Html::img('/images/new/ujobs.svg', ['alt' => 'logo', 'title' => 'logo']) ?>
                </a>
                <?php if (!Yii::$app->user->isGuest) { ?>
                    <div class="logon-category-menu visible-xs">
                        <i class="fa fa-th-large"></i>
                    </div>
                <?php } ?>
                <?php if (Yii::$app->user->isGuest) { ?>
                    <a data-toggle="modal" data-target="#ModalLogin" href=""
                       class="mob-cat-log text-center log-mobile visible-xs"></a>
                <?php } ?>
                <?= \frontend\components\HeaderSearchForm::widget() ?>
                <div class="lang-change">
                    <div class="globe">
                        <div class="locale">
                            <?= Yii::$app->params['supportedLocales'][Yii::$app->language] ?>&nbsp;/&nbsp;<?= Yii::$app->params['currencies'][Yii::$app->params['app_currency_code']]['sign'] ?>
                        </div>
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <div class="nav-head-profile" id="header-language">
                        <div class="container-fluid">
                            <a href="" class="close-nav-head-profile">
                                <i class="fa fa-times"></i>
                            </a>
                            <div class="row nph-padding">
                                <div class="col-md-9 col-sm-8">
                                    <div class="nhp-title"><?= Yii::t('labels', 'Language') ?></div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <?php
                                            $columnCount = ceil(count(Yii::$app->params['supportedLocales']) / 4);
                                            $iterator = 0;
                                            foreach (Yii::$app->params['supportedLocales'] as $locale => $code) {
                                                $language = Yii::t('app', Yii::$app->params['languages'][$locale], [], $locale);
                                                echo Html::a(
                                                    '<b>' . $language . '</b>&nbsp;' . mb_strtolower(Yii::t('app', 'Language', [], $locale)),
                                                    Yii::$app->utility->localeCurrentUrl($code), [
                                                    'class' => $locale === Yii::$app->language ? 'active' : ''
                                                ]);
                                                if (++$iterator >= $columnCount) {
                                                    echo '</div><div class="col-md-3 col-sm-6">';
                                                    $iterator = 0;
                                                }
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="nhp-title"><?= Yii::t('app', 'Currency') ?></div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <?php
                                            foreach (Yii::$app->params['currencies'] as $k => $v) {
                                                $currency = Yii::t('app', Yii::$app->params['currencies'][$k]['title']);
                                                echo Html::a(
                                                    '<b>' . $currency . '</b> - ' . $v['sign'],
                                                    Url::current(['app_currency_code' => strtolower($k)]),
                                                    ['class' => $k === Yii::$app->params['app_currency_code'] ? 'active' : '']
                                                );
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-right hidden-xs">
                <?= Html::a(Yii::t('app', 'Services'), null, [
                    'class' => 'nav-autor',
                    'href' => Url::to(['/category/job-catalog'])
                ]) ?>
                <?= Html::a(Yii::t('app', 'Buyers requests'), null, [
                    'class' => 'nav-autor',
                    'href' => Url::to(['/entity/catalog'])
                ]) ?>
                <?php if (Yii::$app->user->isGuest) { ?>
                    <?= Html::a(Yii::t('app', 'Sign in'), null, [
                        'class' => 'nav-autor modal-dismiss',
                        'data-toggle' => 'modal',
                        'data-target' => '#ModalLogin'
                    ]) ?>
                    <?= Html::a(Yii::t('app', 'Post offer for free'), null, [
                        'class' => 'nav-autor green-b modal-dismiss redirect-set',
                        'data-toggle' => 'modal',
                        'data-target' => '#ModalSignup',
                        'data-redirect' => Url::to(['/entity/global-create'])
                    ]) ?>
                <?php } else {
                    /* @var $user User */
                    $user = Yii::$app->user->identity; ?>
                    <div class="nav-right-item">
                        <?= Html::a('<i class="fa fa-tasks" aria-hidden="true"></i>', ['/account/service/dashboard'], [
                            'class' => 'icon-profnav text-center hint--bottom',
                            'data-hint' => Yii::t('account', 'Dashboard')
                        ]) ?>
                    </div>
                    <div class="nav-right-item">
                        <?= Notifications::widget(['unreadNotifications' => $unreadNotificationsCount]) ?>
                    </div>
                    <div class="nav-right-item">
                        <?= Messages::widget() ?>
                    </div>
                    <div class="nav-right-item">
                        <?= PopupCart::widget() ?>
                    </div>
                    <div class="nav-right-item">
                        <?= Html::a(CurrencyHelper::format(Yii::$app->params['app_currency_code'], $user->getBalance()), ['/account/payment/index'], [
                            'class' => 'header-balance text-right hint--bottom',
                            'data-hint' => Yii::t('account', 'Balance'),
                            'style' => 'width: auto;'
                        ]) ?>
                    </div>
                    <nav class="prof-menu">
                        <div class="prof-headm <?= $user->profile->status === Profile::STATUS_PAUSED ? 'vacation' : 'online' ?>">
                            <div class="head-avatar text-center">
                                <?php if ($user->company_user_id !== null || $user->profile->gravatar_email) {
                                    echo Html::img($userData['thumb'],['alt' => Html::encode($user->username),'title' => Html::encode($user->username)]);
                                } elseif ($userData['sellerName']) {
                                    echo StringHelper::truncate(Html::encode($userData['sellerName']), 1, '');
                                } else {
                                    echo StringHelper::truncate(Html::encode($user->username), 1, '');
                                } ?>
                            </div>
                            <div class="head-name"><?= Html::encode(StringHelper::truncateWords($userData['sellerName'], 1, '')) ?></div>
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </div>
                    </nav>
                <?php } ?>
            </div>
        </div>
    </div>
<?php if (!Yii::$app->user->isGuest) { ?>
    <div class="nav-head-profile" id="profile-drop">
        <div class="container-fluid">
            <a href="" class="close-nav-head-profile">
                <i class="fa fa-times"></i>
            </a>
            <div class="row nph-padding">
                <div class="col-md-6">
                    <?= Html::a(Yii::t('account', 'View My Profile'),
                        $userData['sellerUrl'],
                        ['class' => 'nhp-title', 'style' => 'color: #555;']
                    ) ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="nhp-subtitle"><?= Yii::t('account', 'I am seller') ?></div>
                            <ul>
                                <?php foreach ($menuItems['seller'] as $key => $menuItem) { ?>
                                    <?php if ($key === 'portfolio' || !array_key_exists('count', $menuItem) || $menuItem['count'] > 0) { ?>
                                        <li>
                                            <?= Html::tag('i', '', ['class' => "fa {$menuItem['icon']}"]) ?>
                                            <?= Html::a($menuItem['title'] . (array_key_exists('count', $menuItem) ? "({$menuItem['count']})" : ''), $menuItem['link']) ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="nhp-subtitle"><?= Yii::t('account', 'I am customer') ?></div>
                            <ul>
                                <?php foreach ($menuItems['customer'] as $menuItem) { ?>
                                    <?php if (!array_key_exists('count', $menuItem) || $menuItem['count'] > 0) { ?>
                                        <li>
                                            <?= Html::tag('i', '', ['class' => "fa {$menuItem['icon']}"]) ?>
                                            <?= Html::a($menuItem['title'] . (array_key_exists('count', $menuItem) ? "({$menuItem['count']})" : ''), $menuItem['link']) ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <?= Html::a(Html::encode($userData['sellerName']), $userData['sellerUrl'], ['class' => 'nhp-title']) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <?php foreach ($menuItems['settings'] as $menuItem) { ?>
                                    <li>
                                        <?= Html::tag('i', '', ['class' => "fa {$menuItem['icon']}"]) ?>
                                        <?= Html::a($menuItem['title'], $menuItem['link'], $menuItem['htmlOptions'] ?? []) ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <?php if ($menuItems['additional']['type'] === 'articles') { ?>
                                <ul>
                                    <?php foreach ($menuItems['additional']['items'] as $menuItem) { ?>
                                        <li>
                                            <?= Html::tag('i', '', ['class' => "fa {$menuItem['icon']}"]) ?>
                                            <?= Html::a($menuItem['title'], $menuItem['link'], $menuItem['htmlOptions'] ?? []) ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else if ($menuItems['additional']['type'] === 'companies') { ?>
                                <div class="nhp-subtitle"><?= Yii::t('account', 'My pages') ?>:</div>
                                <div class="switcher-companies-list">
                                    <?php foreach ($menuItems['additional']['items'] as $menuItem) { ?>
                                        <div class="switcher-companies-item flex flex-v-center">
                                            <div class="switcher-companies-item-img">
                                                <?= Html::a(Html::img($menuItem['thumb']), $menuItem['link'], $menuItem['htmlOptions'] ?? []) ?>
                                            </div>
                                            <div>
                                                <?= Html::a($menuItem['title'], $menuItem['link'], $menuItem['htmlOptions'] ?? []) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($askSubscribe === true) { ?>
        <div class="push-block">
            <div class="push-msg">
                <div class="push-img">
                    <?= Html::img(['/images/new/cat-img.jpg'], ['alt' => 'push-img','title' => 'push-img']) ?>
                </div>
                <div class="push-text">
                    <p>
                        <?= Yii::t('app', 'Get all info about orders via push-notifications') ?>
                    </p>
                </div>
                <div class="push-close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 246 20 20">
                        <path d="M18.947 262.44L12.51 256l6.437-6.438c.695-.695.695-1.814 0-2.51-.696-.695-1.814-.695-2.51 0L10 253.492l-6.438-6.44c-.695-.695-1.813-.695-2.51 0-.694.695-.694 1.815 0 2.51L7.493 256l-6.44 6.44c-.694.692-.694 1.812 0 2.507.697.695 1.815.695 2.51 0L10 258.51l6.438 6.438c.695.695 1.813.695 2.51 0 .69-.696.69-1.82 0-2.51z"></path>
                    </svg>
                </div>
            </div>
            <a class="push-btn js-push-button" href="#">
                <div class="push-bell">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 257.75 20 20">
                        <path d="M5.466 259.977l-1.296-1.295c-2.22 1.666-3.7 4.256-3.886 7.217h1.85c.186-2.5 1.388-4.628 3.332-5.923zm12.4 5.922h1.85c-.185-2.962-1.573-5.552-3.794-7.218l-1.295 1.295c1.85 1.295 3.053 3.424 3.238 5.922zm-1.85.462c0-2.868-1.944-5.182-4.628-5.83v-.647c0-.74-.647-1.388-1.388-1.388-.74 0-1.388.648-1.388 1.388v.648c-2.684.647-4.627 2.96-4.627 5.83v5.088l-1.85 1.852v.926h15.73v-.926l-1.85-1.85v-5.09zM10 277.004h.37c.648-.094 1.11-.556 1.296-1.11.093-.186.185-.464.185-.74h-3.7c0 1.016.832 1.85 1.85 1.85z"></path>
                    </svg>
                </div>
                <div class="push-subscribe">
                    <span><?= Yii::t('app', 'Subscribe') ?></span>
                    <?= Yii::t('app', 'on push-notifications') ?>
                </div>
            </a>
        </div>
    <?php } ?>
<?php } ?>

<?php if ($menuItems['additional']['type'] === 'companies') {
    $script = <<<JS
        $('[data-action="identity-switcher"]').on('click', function() {
            let href = $(this).attr('href'),
                id = $(this).data('identity');
            
            $.post(href, {identity: id}, function(response) {
                if(response.success === true) {
                    window.location.reload();
                }
            });
            
            return false;
        });
JS;
    $this->registerJs($script);
}