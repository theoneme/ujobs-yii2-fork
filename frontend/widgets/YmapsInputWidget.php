<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class YmapsInputWidget
 * @package frontend\components
 */
class YmapsInputWidget extends Widget
{
    public $model;

    /**
     * @var array
     */
    public $inputOptions = [];

    public function run()
    {
        return $this->render('ymaps-input-widget', ['model' => $this->model, 'inputOptions' => $this->inputOptions]);
    }
}