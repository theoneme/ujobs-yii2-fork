<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 17.10.2017
 * Time: 14:29
 */

/* @var integer $rating */
/* @var integer $size */

?>

<?php if ($rating > 0) { ?>
    <div class="rating-container">
        <div class="rating-stars">
            <?php for ($i = 1; $i <= 5; $i++) {
                if ($i <= $rating) {
                    $class = '';
                } else if ($i <= $rating + 0.5) {
                    $class = '-half-o';
                } else {
                    $class = '-o';
                } ?>

                <span class="star">
                    <i class="fa fa-star<?= $class ?>" <?= $size ? "style='font-size: {$size}px'" : '' ?>></i>
                </span>
            <?php } ?>
        </div>
    </div>
<?php } ?>


