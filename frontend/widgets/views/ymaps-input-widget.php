<?php
/**
 * @var $this View
 * @var $model \yii\base\Model
 * @var $inputOptions array
 * @var $modal boolean
 */

use yii\helpers\Html;
use yii\web\View;

?>

<?= Html::activeHiddenInput($model, 'lat', ['id' => 'ymaps-input-lat'])?>
<?= Html::activeHiddenInput($model, 'long', ['id' => 'ymaps-input-long'])?>
<?= Html::activeTextInput($model,
    'address',
    array_merge([
        'id' => 'ymaps-input-address',
        'placeholder' => Yii::t('board', 'Enter address'),
        'readonly' => false
    ], $inputOptions)
);

$script = <<<JS
    new autoComplete({
        selector: "#ymaps-input-address",
        minChars: 2,
        autoFocus: false,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.getJSON('https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + request, function(data) { 
                let autoCSource = [];
                for (let i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                    let parts = data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos.split(' '),
                        lat = parts[1],
                        long = parts[0];

                    autoCSource.push({
                        label: data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.text,
                        value: data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.text,
                        longlat: data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos,
                        lat: lat,
                        long: long
                    });
                }
                response(autoCSource); 
            });     
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-lat="'+item.lat+'" data-long="'+item.long+'" data-val="'+item.label+'">'+item.label.replace(re, "<b>$1</b>")+'</div>';
        },
        onSelect: function(e, term, item) {
            $("#ymaps-input-address").val(term);
            $('#ymaps-input-lat').val(item.getAttribute('data-lat'));
            $('#ymaps-input-long').val(item.getAttribute('data-long'));
        }
    });
JS;

$this->registerJs($script);