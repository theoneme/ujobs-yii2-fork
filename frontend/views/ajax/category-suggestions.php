<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.08.2018
 * Time: 18:21
 */

?>

<p class="category-suggestion"><?= Yii::t('app', 'Category suggestions') ?></p>

<?php foreach ($data as $item) { ?>
    <p>
        <a href='#' data-action='suggestion-select' data-p-id="<?= $item[0]['id'] ?>" data-c-id="<?= $item[1]['id']?>" data-s-id="<?= $item[2]['id'] ?? null?>">
            <?php foreach ($item as $key => $a) { ?>
                <span>
                    <?= $a['title'] ?>
                    <?php if ($key < count($item) - 1) { ?>
                        &nbsp;-&nbsp;
                    <?php } ?>
                </span>
            <?php } ?>
        </a>
    </p>
<?php } ?>
