<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 30.11.2016
 * Time: 12:10
 */

use common\models\Job;
use common\models\Message;
use common\models\Offer;
use common\models\user\Profile;
use frontend\components\job\OtherPropositions;
use frontend\components\job\RecommendedPropositions;
use frontend\components\job\ViewedPropositions;
use frontend\components\SocialShareWidget;
use frontend\models\TenderResponseForm;
use frontend\modules\account\components\MiniView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $model Job
 * @var $breadcrumbs array
 * @var $extraFields array
 * @var $message Message
 * @var $offer Offer
 * @var $accessInfo array
 * @var $contactAccessInfo array
 * @var $tenderResponseForm TenderResponseForm
 * @var $reviewDataProvider ActiveDataProvider
 */

\frontend\assets\ProductAsset::register($this);
$this->registerCssFile('/css/new/profile.css');
$shareUrl = Url::current([], true);

if (isGuest()) {
    $respondParams = [
        'class' => 'btn-big',
        'data-toggle' => 'modal',
        'data-id' => $model->id,
        'data-target' => '#ModalSignup'
    ];
} else {
    if ($accessInfo['allowed'] === true) {
        if ($contactAccessInfo['allowed'] === true) {
            $respondParams = [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-id' => $model->id,
                'data-target' => '#PriceModal'
            ];
        } else {
            $respondParams = [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-id' => $model->id,
                'data-target' => '#tariffContactModal'
            ];
        }
    } else {
        $respondParams = [
            'class' => 'btn-big',
            'data-toggle' => 'modal',
            'data-id' => $model->id,
            'data-target' => '#tariffModal'
        ];
    }
}

$price = $model->contract_price ? Yii::t('app', 'contract') : $model->getPrice();
if ($model->type === Job::TYPE_ADVANCED_TENDER && !$model->contract_price) {
    $price .= ' ' . Yii::t('app', 'per month');
}
?>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Product",
    "description": "<?= $model->translation->title ?>",
    "name": "<?= $this->title ?>",
    "image": "<?= $model->getThumb() ?>",
    "offers": {
        "@type": "Offer",
        "availability": "http://schema.org/InStock",
        "price": "<?= $model->price ?>",
        "priceCurrency": "<?= $model->currency_code ?>"
    }
}
</script>

    <div class="work-bg">
        <div class="nav-fixed">
            <div class="container-fluid">
                <div class="nav-work row">
                    <ul class="menu-work">
                        <li>
                            <a class="selected" href="#w-slides"><?= Yii::t('app', 'Overview') ?></a>
                        </li>
                        <li>
                            <a href="#w-info"><?= Yii::t('app', 'Description') ?></a>
                        </li>
                    </ul>
                    <!--<div class="work-like">

                    <div class="save-choose">
                        <?= Html::a('<i class="fa fa-heart" aria-hidden="true"></i>Gigs I Love', null, [
                        'class' => 'sc-item like-itembig'
                    ]) ?>
                        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i>Add Collection', null, [
                        'class' => 'sc-item',
                        'data-toggle' => 'modal',
                        'data-target' => '#favouriteSave'
                    ]) ?>
                    </div>
                    <?= Html::a('<i class="fa fa-heart-o" aria-hidden="true"></i>Favorite', null, [
                        'class' => 'wl-box hint--bottom biglike',
                        'data-hint' => 'Favorite'
                    ]) ?>
                    <div class="wl-count">1376</div>
                </div>-->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="work-content row">
                <div class="work-side col-md-8 col-sm-8 col-xs-12">
                    <div id="w-slides" class="work-section">
                        <div class="work-header">
                            <h1 class="work-title text-left">
                                <?= Html::encode($model->translation->title) ?>
                            </h1>

                            <div class="work-stars">

                            </div>
                            <?= Breadcrumbs::widget([
                                'itemTemplate' => "{link}/", // template for all links
                                'activeItemTemplate' => "&nbsp;{link}",
                                'links' => $breadcrumbs,
                                'options' => ['class' => 'breadcrumbs'],
                                'tag' => 'div'
                            ]); ?>
                        </div>
                        <div class="gallery-container" id="zoom">
                            <div class="zoom-desktop">
                                <div class="container">
                                    <div class="big-image-container">
                                        <a class="lens-image" data-lens-image="<?= $model->getThumb() ?>">
                                            <img src="<?= $model->getThumb('catalog') ?>" class="big-image lazy-load"
                                                 data-src="<?= $model->getThumb() ?>" alt="<?= Html::encode($model->translation->title) ?>" title="<?= Html::encode($model->translation->title) ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-prod-mob">
                                <div class="container">
                                    <div class="big-image-container slider-bigz">
                                        <?php
                                        if ($model->attachments) {
                                            foreach ($model->attachments as $attachment) { ?>
                                                <div data-lens-image="<?= $attachment->getThumb() ?>"
                                                     data-big-image="<?= $attachment->getThumb() ?>">
                                                    <?= Html::img($attachment->getThumb('catalog'), [
                                                        'alt' => $model->translation->title,
                                                        'title' => $model->translation->title,
                                                    ]) ?>
                                                </div>
                                            <?php }
                                        } else { ?>
                                            <div data-lens-image="<?= $model->getThumb() ?>"
                                                 data-big-image="<?= $model->getThumb() ?>">
                                                <?= Html::img($model->getThumb('catalog'), [
                                                    'alt' => $model->translation->title,
                                                    'title' => $model->translation->title,
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="thumbnails-container">
                                <?php
                                if ($model->attachments) {
                                    foreach ($model->attachments as $attachment) { ?>
                                        <a href="#" class="thumbnail-wrapper"
                                           data-lens-image="<?= $attachment->getThumb() ?>"
                                           data-big-image="<?= $attachment->getThumb() ?>">
                                            <?= Html::img($attachment->getThumb('catalog'), [
                                                'alt' => $model->translation->title,
                                                'title' => $model->translation->title,
                                            ]) ?>
                                        </a>
                                    <?php }
                                } else { ?>
                                    <a href="#" class="thumbnail-wrapper" data-lens-image="<?= $model->getThumb() ?>"
                                       data-big-image="<?= $model->getThumb() ?>">
                                        <?= Html::img($model->getThumb('catalog'), [
                                            'alt' => $model->translation->title,
                                            'title' => $model->translation->title,
                                        ]) ?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $contactAccessInfo,
                        'template' => 'mini_view_mobile',
                    ]) ?>

                    <div id="w-info" class="work-section">
                        <div class="head-work">
                            <?= Yii::t('app', 'About This Request') ?>
                        </div>
                        <div class="body-work mobile-description">
                            <?php
                            $innerLinkPattern = '/<a[^<>]*?href=\"(?=http(s)?:\/\/ujobs.me)([^"]*?)\"[^<>]*?>(.*?)<\/a>/';
                            $outerLinkPattern = '/<a[^<>]*?href=\"(?!http(s)?:\/\/ujobs.me)([^"]*?)\"[^<>]*?>(.*?)<\/a>/';
                            echo preg_replace(
                                $outerLinkPattern,
                                "\\2",
                                preg_replace($innerLinkPattern, "<a href='\\2' target='_blank'>\\2</a>", $model->translation->content)
                            )
                            ?>
                            <?php if($model->date_start) { ?>
                                <p><?= Yii::t('app', 'Service execution has to be started at {date}', ['date' => $model->date_start]) ?></p>
                            <?php } ?>
                            <?php if($model->date_end) { ?>
                                <p><?= Yii::t('app', 'Service has to be provided to {date}', ['date' => $model->date_end]) ?></p>
                            <?php } ?>
                            <div class="mobile-more-gradient">
                                <a class="mob-more-descr" href="#"><?= Yii::t('app', 'See more'); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="work-section visible-xs mobile-one-pack">
                        <div class="ap-head text-center">
                            <div class="ap-type">
                                <?= Yii::t('app', 'Price') ?>:
                                <span class="ap-typeprice">
                                        <?= $price ?>
                                    </span>
                            </div>
                        </div>
                        <?php if (!hasAccess($model->user_id)) { ?>
                            <div class="add-to-cart-mob pos-bottom visible-xs">
                                <?php if ($model->status === Job::STATUS_ACTIVE) { ?>
                                    <?= Html::a(Yii::t('app', 'To Respond'), null, $respondParams) ?>
                                <?php } else { ?>
                                    <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                        'class' => 'btn-big add-to-cart',
                                        'disabled' => 'disabled'
                                    ]) ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($reviewDataProvider->getCount() > 0) { ?>
                        <div id="w-reviews" class="work-section">
                            <div class="head-work clearfix">
                                <div class="rev-count">
                                    <div class="rc-text">
                                        <?= Yii::t('app', 'Reviews about this customer ({count})', ['count' => $reviewDataProvider->getCount()]) ?>
                                    </div>
                                    <div class="mobile-btn">
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="body-rate hidden-xs">
                                <?= ListView::widget([
                                    'dataProvider' => $reviewDataProvider,
                                    'itemView' => 'listview/review-item',
                                    'options' => [
                                        'class' => ''
                                    ],
                                    'itemOptions' => [
                                        'class' => 'comment'
                                    ],
                                    'emptyText' => Html::tag('p', Yii::t('app', 'No reviews yet'), ['class' => 'text-center']),
                                    'layout' => "{items}"
                                ]) ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div id="w-reviews" class="work-section clearfix">
                            <div>
                                <div class="head-work">
                                    <div class="rc-text">
                                        <?= Yii::t('app', 'Reviews') ?>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="body-rate hidden-xs">
                                    <br>
                                    <p class="text-center"><?= Yii::t('app', 'No reviews yet') ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="author-side col-md-4 col-sm-4 col-xs-12 hidden-xs">
                    <div class="aside-price">
                        <div class="ap-head text-center">
                            <div class="ap-type">
                                <?= Yii::t('app', 'Price') ?>:
                                <span class="ap-typeprice">
                                    <?= $price ?>
                                </span>
                            </div>
                        </div>
                        <div class="ap-body" style='display:block'>
                            <?php if ($model->type === Job::TYPE_TENDER) { ?>
                                <div class="ap-title"><?= Yii::t('app', 'Need a one-time service') ?></div>
                            <?php } else { ?>
                            <p><?= Yii::t('app', 'Employee is required on permanent basis. Payment is done weekly.');
                                if (!$model->contract_price) {
                                    echo '<br>' . Yii::t('app', '{amount} rubles per week.', ['amount' => number_format(($model->price / 30) * 7 * 0.8, 0, ',', ' ')]);
                                }
                                if ($model->percent_bonus) {
                                    echo '<br>' . Yii::t('app', '+{percent}% of sales', ['percent' => ($model->percent_bonus)]);
                                }
                                } ?>
                            </p>
                            <?php if($model->date_start) { ?>
                                <p><?= Yii::t('app', 'Service execution has to be started at {date}', ['date' => $model->date_start]) ?></p>
                            <?php } ?>
                            <?php if($model->date_end) { ?>
                                <p><?= Yii::t('app', 'Service has to be provided to {date}', ['date' => $model->date_end]) ?></p>
                            <?php } ?>
                            <div class="ap-buttons text-center">
                                <?php if (!hasAccess($model->user_id)) { ?>
                                    <?php if ($model->status === Job::STATUS_ACTIVE) { ?>
                                        <?= Html::a(Yii::t('app', 'To Respond'), null, $respondParams) ?>
                                    <?php } else { ?>
                                        <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                            'class' => 'btn-big add-to-cart',
                                            'disabled' => 'disabled'
                                        ]) ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::current(['invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                            'twitterMessage' => Yii::t('app', 'Check out this request on {site}:', ['site' => Yii::$app->name]),
                            'mailItem' => [
                                'label' => $model->getLabel(),
                                'thumb' => $model->getThumb('catalog'),
                                'name' => $model->getSellerName()
                            ]
                        ]) ?>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $contactAccessInfo,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
    <div class="work-sliders">
        <?= OtherPropositions::widget(['entity' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'userId' => $model->user->id, 'currentItem' => $model->id, 'sliderCount' => 6]) ?>
        <?= RecommendedPropositions::widget(['entity' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'category' => $model->category, 'exceptId' => $model->id, 'sliderCount' => 6]) ?>
        <?= ViewedPropositions::widget(['entity' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'currentItem' => $model]) ?>
    </div>

    <div class="modal fade" id="PriceModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('app', 'Send response') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'tender-price-form',
                        'action' => ['/tender/respond-advanced'],
                        'options' => [
                            'class' => 'row',
                            'data-pjax' => 0,
                            'style' => 'margin: 0;'
                        ]
                    ]); ?>
                    <?php if ($tenderResponseForm->scenario == TenderResponseForm::SCENARIO_CONTRACT_PRICE) { ?>
                        <?= $form->field($tenderResponseForm, 'price', [
                            'template' => "{input}\n{error}",
                            'options' => ['class' => 'form-group offer-price', 'enctype' => 'multipart/form-data']
                        ])->textInput([
                            'placeholder' => $tenderResponseForm->scenario == TenderResponseForm::SCENARIO_CONTRACT_PRICE ?
                                Yii::t('app', 'Offer your price on this request') :
                                Yii::t('app', 'You can offer your price on this request'),
                            'type' => 'number'
                        ]) ?>
                    <?php } ?>
                    <?= $form->field($tenderResponseForm, 'id')->hiddenInput(['value' => $model->id])->label(false) ?>
                    <div class="col-md-12 white-mes-bg">
                        <?= $form->field($tenderResponseForm, 'content', ['template' => "{input}\n{error}"])->textarea([
                            'class' => 'readsym',
                            'placeholder' => Yii::t('app', 'Tell a little about yourself. Why choose you?')
                        ]) ?>
                        <div class="white-mes-bottom text-left row">
                            <div class="counttext">
                                <span class="ctspan">0</span> / 500
                            </div>
                            <div class="fileup">
                                <label class="file-upload">
                                                    <span class="button text-center hint--top-right"
                                                          data-hint="<?= Yii::t('account', 'Attach files. {mb}MB max.', ['mb' => 30]) ?>">
                                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                    </span>
                                    <?= $form->field($tenderResponseForm, 'uploadedFiles[]')->fileInput([
                                        'id' => 'file-input',
                                        'multiple' => true
                                    ])->label(false) ?>
                                    <mark style="background-color: #fff"></mark>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="spinner-bg">
                <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            </div>
        </div>
    </div>


<?php if ($accessInfo['allowed'] === false && !isGuest()) { ?>
    <?= $this->render('@frontend/views/site/partial/response-tariff-modal', [
        'model' => $model,
        'accessInfo' => $accessInfo,
        'profile' => Yii::$app->user->identity->profile
    ]) ?>
<?php } ?>

<?php if ($contactAccessInfo['allowed'] === false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>
<?php $encodedName = Html::encode($model->getSellerName());
$translations = json_encode([
    'see_less' => Yii::t('app', 'See less'),
    'see_more' => Yii::t('app', 'See more')
]);

$script = <<<JS
    const translations = $translations;
     if($(window).width()>992){
        $('#zoom .big-image').simpleLens({
            loading_image: '/images/new/loading.gif',
            //open_lens_event: 'click'
        });
         $('#zoom .thumbnails-container img').simpleGallery({
            loading_image: '/images/new/loading.gif',
            show_event: 'click'
        });
    } else {
        var slickDir = false;
        if($('body').hasClass('rtl')){
            slickDir = true;
        }
        $(".slider-bigz").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            rtl: slickDir,
            asNavFor: ".slider-navz"
        });
        $(".slider-navz").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: ".slider-bigz",
            dots: false,
            centerMode: false,
            arrows: false,
            rtl: slickDir,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },{
		        breakpoint: 600,
		        settings: {
		            slidesToShow: 4,
		            slidesToScroll: 1
		        }
		    }, {
		        breakpoint: 480,
		        settings: {
		            slidesToShow: 3,
		            slidesToScroll: 1
		        }
		    }, {
		        breakpoint: 360,
		        settings: {
		            slidesToShow: 2,
		            slidesToScroll: 1
		        }
		     }
        ]
    });
    }
	$('body').on('click', '.mobile-btn', function(){
        $(this).closest('.work-section').toggleClass('mobile-open');
    }).on('click', '.mob-more-descr', function(e){
        e.preventDefault();
        $(this).closest('.mobile-description').css('max-height','99999px');
        $(this).closest('.mobile-more-gradient').hide();
    }).on('click', '.mobwork-info-head .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.work-section').find('.mob-more-info').toggle();
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { console.log(translations);
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    }).on('click', '.author-intro .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.author-intro').find('.short-descr-desktop').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { console.log(translations);
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
	$(window).scroll(function() {
        let btn = $('.add-to-cart-mob'),
            scrollpos = $('.mobile-one-pack');
        if($(window).scrollTop()+$(window).height()>=(scrollpos.offset().top+scrollpos.height()+200) && !btn.hasClass('pos-bottom')){
            btn.addClass('pos-bottom');
        } else {
            if($(window).scrollTop()+$(window).height()<(scrollpos.offset().top+scrollpos.height()+200) && btn.hasClass('pos-bottom')) {
                btn.removeClass('pos-bottom');
            }
        }
    });
	$(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
	if(typeof fbq !== 'undefined') {
        fbq('track', 'ViewContent', {
            content_ids: [{$model->id}],
            content_type: 'tender',
            value: {$model->price},
            currency: {$model->currency_code}
        });
    }
JS;

$this->registerJs($script, \yii\web\View::POS_LOAD);

if ($model->type === Job::TYPE_ADVANCED_TENDER || $model->type === Job::TYPE_TENDER) {
    $responseUrl = Url::to(['/tender/respond-advanced']);
    $isProfileActive = (int)($model->profile->status === Profile::STATUS_ACTIVE);
    $script = <<<JS
        $(document).on('submit', '#tender-price-form', function(e){
            $('#PriceModal .spinner-bg').show();
            
            let formData = new FormData($(this)[0]),
                self = $(this);
            $.ajax({
                url: '$responseUrl',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(response) {
                    if(response.success) {
                        $('#PriceModal').modal('hide');
//                        Live.addResponse({
//                            moderated: {$isProfileActive},
//                            id: {$model->id},
//                            price: '{$model->getPrice()}'
//                        });
                    }
                    alertCall('top', response.success ? 'success' : 'error', response.message);
    
                    $('#PriceModal .spinner-bg').hide();
                },
            });
            return false;
        });
        $(document).on('change', '#file-input', function(){
            var names = $.map($(this).prop('files'), function(val) { return val.name; });
            $(this).parent().siblings('mark').html('(' + names.length + ') ' + names.join(', '));
        });
JS;
    $this->registerJs($script);
}