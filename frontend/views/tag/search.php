<?php
use common\modules\attribute\models\AttributeValue;
use frontend\assets\SelectizeAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $tags array
 * @var $this View
 * @var $city AttributeValue
 */

SelectizeAsset::register($this);
$this->title = Yii::t('app', 'Search by service tag');
if ($city) {
    $this->title .= Yii::t('seo', ' in {city}', ['city' => $city->translation->title]);
}
?>
    <div class="container-fluid">
        <h1 class="text-center" style="color: #000"><?= $this->title ?></h1>
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <?= Html::textInput('tag', '', ['id' => 'tag-search-input', 'placeholder' => Yii::t('app', 'Enter a tag')]) ?>
            </div>
            <div class="col-md-4 selectize-input-container">
                <?= Html::dropDownList('city',
                    $city ? $city->alias : null,
                    $city ? [$city->alias => $city->translation->title] : [],
                    ['id' => 'city-search-input', 'placeholder' => Yii::t('app', 'Select a city')]
                ) ?>
            </div>
        </div>
        <div class="tags-container text-center">
            <div class="tags-list">
                <?php foreach ($tags as $tag) {
                    $url = ["/category/tag", 'reserved_job_tag' => $tag['alias']];
                    if ($city) {
                        $url['city'] = $city->alias;
                    }
                    echo Html::a("<span class='tag-name'>" . Yii::$app->utility->extractTranslation($tag['translations']) . "</span><span class='grey'>({$tag["relatedCount"]})</span>",
                        $url,
                        ['class' => 'tag-but']
                    );
                } ?>
            </div>
            <div class="spinner-bg2">
                <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            </div>
        </div>
    </div>

<?php
$cityListUrl = Url::to(['/ajax/city-list']);
$currentCityUrl = Url::to(['/tag/search', 'city' => $city ? $city->alias : null]);
$currentQueryUrl = Url::to(['/tag/search', 'query' => $query]);
$script = <<<JS
    delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).on('keyup', '#tag-search-input', function() {
        $('.spinner-bg2').show();
        $('.tags-list').hide();
        let value = $(this).val().toLowerCase();
        delay(function(){
            $('a.tag-but').hide().each(function() {
                if ($(this).children('.tag-name').html().toLowerCase().indexOf(value) >= 0) {
                    $(this).show();
                }
            });
        $('.spinner-bg2').hide();
        $('.tags-list').show();
        }, value.length ? 300 : 0);
    });
    
    $("#city-search-input").selectize({
        valueField: "alias",
        labelField: "title",
        searchField: ["title"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.post('$cityListUrl', {query: encodeURIComponent(query)}, function(data) {
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });
    $(document).on('change', '#city-search-input', function() {
        if ($(this).val().trim().length) {
            window.location.href = '$currentUrl?city=' + $(this).val();
        }
    });
JS;
$this->registerJs($script);
?>