<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 11.09.2017
 * Time: 17:34
 */
/* @var $this \yii\web\View */

/* @var $content string */

use frontend\components\AccountConfirmModalWidget;
use frontend\components\HeaderCategories;
use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderRecoverModalWidget;
use frontend\components\HeaderSignupModalWidget;
use frontend\components\MobileMenu;
use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\CommonAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
        <link rel="shortcut icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
    <?php $this->beginBody() ?>
    <?= MobileMenu::widget() ?>
    <div class="mainblock error-blue">
        <header>
            <?= $this->render('_header') ?>
            <?= HeaderCategories::widget() ?>
        </header>
        <div class="all-content">
            <?= $content ?>
            <div class="mfooter"></div>
        </div>
    </div>

    <div class="container-fluid">
        <div id="footer" class="box-404">
            <div class="worker"></div>
            <div class="tools"></div>
        </div>
    </div>
    <?= HeaderLoginModalWidget::widget() ?>
    <?= HeaderSignupModalWidget::widget() ?>
    <?= AccountConfirmModalWidget::widget() ?>
    <?= HeaderRecoverModalWidget::widget() ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
<?php

