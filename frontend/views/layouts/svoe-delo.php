<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 12.10.2017
 * Time: 13:09
 */


/* @var $this \yii\web\View */
/* @var $content string */

use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderSignupModalWidget;
use frontend\components\MobileMenu;
use yii\helpers\Html;

\frontend\assets\NewLandingAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/images/new/.png" type="image/x-icon"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
    <?php $this->beginBody() ?>
    <div class="main-content">
        <header class="text-left">
            <a class="logo delo" href="https://ujobs.me/account/profile/show/23537">
                Своё Дело
            </a>
        </header>
        <?= $content ?>
        <div class="mfooter"></div>
        <footer class="text-center">
            <div class="inline-center">
                <a href="https://ujobs.me" class="logo-footer delo">
                    Своё Дело
                </a>
            </div>
            <div class="inline-center">
                <a class="social" target="_blank" href="https://vk.com/svoedelo.ujobs">
                    <?= Html::img('/images/new/landing/vk.png', ['alt' => 'vk']) ?>
                </a>
                <a class="social" target="_blank" href="https://www.facebook.com/groups/485470681809873/">
                    <?= Html::img('/images/new/landing/facebook.png', ['alt' => 'facebook']) ?>
                </a>
                <a class="social" target="_blank" href="https://twitter.com/svoedelo1">
                    <?= Html::img('/images/new/landing/twitter.png', ['alt' => 'twitter']) ?>
                </a>
                <a class="social" target="_blank" href="https://www.instagram.com/svoedelo5684/">
                    <?= Html::img('/images/new/landing/instagram.png', ['alt' => 'instagram']) ?>
                </a>
                <a class="social" target="_blank" href="https://plus.google.com/b/101497946722663626416/communities/114597002738095712465?hl=ru">
                    <?= Html::img('/images/new/landing/googleplus.png', ['alt' => 'google plus']) ?>
                </a>
            </div>
        </footer>
    </div>

    <?= HeaderLoginModalWidget::widget() ?>
    <?= HeaderSignupModalWidget::widget() ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>