<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 20.03.2018
 * Time: 14:42
 */


/* @var $this \yii\web\View */
/* @var $content string */

use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderSignupModalWidget;
use yii\helpers\Html;

\frontend\assets\QwickAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/images/new/.png" type="image/x-icon"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
    <?php $this->beginBody() ?>
    <div class="main-content">
        <header>
            <a class="logo qwick" href="https://ujobs.me">
                QWICK
            </a>
            <div class="powered">Power by UJOBS</div>
        </header>
        <?= $content ?>
        <div class="mfooter"></div>
        <footer>
            <div class="inline-center text-center">
                <a href="https://ujobs.me" class="logo-footer qwick">
                    QWICK
                </a>
                <div class="powered">Power by UJOBS</div>
            </div>
            <div class="inline-center text-center">
                <a class="social" target="_blank" href="https://vk.com/">
                    <?= Html::img('/images/new/landing/vk.png', ['alt' => 'vk']) ?>
                </a>
                <a class="social" target="_blank" href="https://www.facebook.com/groups/">
                    <?= Html::img('/images/new/landing/facebook.png', ['alt' => 'facebook']) ?>
                </a>
                <a class="social" target="_blank" href="https://twitter.com/">
                    <?= Html::img('/images/new/landing/twitter.png', ['alt' => 'twitter']) ?>
                </a>
                <a class="social" target="_blank" href="https://www.instagram.com/">
                    <?= Html::img('/images/new/landing/instagram.png', ['alt' => 'instagram']) ?>
                </a>
                <a class="social" target="_blank" href="https://plus.google.com/">
                    <?= Html::img('/images/new/landing/googleplus.png', ['alt' => 'google plus']) ?>
                </a>
            </div>
        </footer>
    </div>

    <?= HeaderLoginModalWidget::widget() ?>
    <?= HeaderSignupModalWidget::widget() ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>