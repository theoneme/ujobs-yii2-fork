<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderRecoverModalWidget;
use frontend\components\HeaderSignupModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\CommonAsset::register($this);
\frontend\assets\AccountAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>
<?= \frontend\components\MobileMenu::widget() ?>
<div class="mainblock">
    <header>
        <?= $this->render('_header') ?>
    </header>
    <div class="all-content">
        <?= $content ?>
        <p class="pre-footer-google text-center">
            <!-- Adaptive test -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-7280089675102373"
                 data-ad-slot="7820957577"
                 data-ad-format="auto"
                 data-full-width-responsive="true"></ins>
        </p>
        <div class="mfooter"></div>
    </div>
</div>

<?= $this->render('_footer') ?>
<? //= NotyAlert::widget() ?>
<?= HeaderLoginModalWidget::widget() ?>
<?= HeaderSignupModalWidget::widget() ?>
<?= HeaderRecoverModalWidget::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
