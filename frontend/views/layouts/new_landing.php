<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.04.2017
 * Time: 16:59
 */

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderSignupModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\NewLandingAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
        <link rel="shortcut icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
    <?php $this->beginBody() ?>
    <div class="main-content">
        <header class="text-left">
            <a class="logo text-center" href="https://ujobs.me">
                <?= Html::img('/images/new/landing/logo.jpg') ?>
            </a>
        </header>
        <?= $content ?>
        <div class="mfooter"></div>
        <footer class="text-center">
            <div class="inline-center">
                <a href="https://ujobs.me" class="logo-footer">
                    <?= Html::img('/images/new/landing/logo.jpg') ?>
                </a>
            </div>
            <div class="inline-center">
                <h2 class="text-center">
                    <a class="https://ujobs.me">
                        Биржа услуг uJobs<br/>
                        www.ujobs.me
                    </a>
                </h2>
            </div>
            <div class="inline-center">
                <a class="social" href="https://www.facebook.com/Ujobsme-425244320979311/">
                    <?= Html::img('/images/new/landing/facebook.png', ['alt' => 'facebook']) ?>
                </a>
                <a class="social" href="https://twitter.com/UjobsFreelance">
                    <?= Html::img('/images/new/landing/twitter.png', ['alt' => 'twitter']) ?>
                </a>
                <a class="social" href="https://www.instagram.com/ujobs.me777/">
                    <?= Html::img('/images/new/landing/instagram.png', ['alt' => 'instagram']) ?>
                </a>
                <a class="social" href="https://plus.google.com/u/0/106033473256064939407">
                    <?= Html::img('/images/new/landing/googleplus.png', ['alt' => 'google plus']) ?>
                </a>
            </div>
        </footer>
    </div>

    <?= HeaderLoginModalWidget::widget() ?>
    <?= HeaderSignupModalWidget::widget() ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>