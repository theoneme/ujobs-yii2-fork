<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 17:59
 */

use common\modules\livechat\components\LivechatWidget;
use frontend\modules\messenger\components\MessengerWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this;
 */

?>

<footer class="">
    <div class="container-fluid">
        <?= \frontend\components\FooterWidget::widget() ?>
        <div class="footer-bottom">
            <div class="fcheck">
                <a href="" class="check-but">
                    <span class="current"> <?= Yii::t('app', Yii::$app->params['languages'][Yii::$app->language]) ?></span>
                    |
                    <span class="current">
                        <?= Yii::$app->params['currencies'][Yii::$app->params['app_currency_code']]['sign'] . ' ' . Yii::$app->params['app_currency_code'] ?>
                    </span>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </a>
                <div class="lang-cur">
                    <div class="lc-header">
                        <?= Yii::t('app', 'Regional Settings') ?>
                        <i class="fa fa-globe fa-lg"></i>
                    </div>
                    <div class="lc-inner">
                        <select name="app_language" class="app_language">
                            <?php foreach (Yii::$app->params['supportedLocales'] as $locale => $code) {
                                echo Html::tag('option', Yii::t('app', Yii::$app->params['languages'][$locale], [], $locale), [
                                    'value' => Yii::$app->utility->localeCurrentUrl($code),
                                    'selected' => $locale === Yii::$app->language
                                ]);
                            } ?>
                        </select>
                        <select name="app_currency_code" class="app_currency_code">
                            <?php foreach (Yii::$app->params['currencies'] as $k => $v) {
                                echo Html::tag('option', $v['sign'] . ' ' . $k, [
                                    'value' => Url::current(['app_currency_code' => strtolower($k)]),
                                    'selected' => $k === Yii::$app->params['app_currency_code']
                                ]);
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <!--            --><?php //= Html::a(Html::img('/images/new/payanyway-banner.gif'), 'http://www.payanyway.ru', ['target' => '_blank'])?>
            <div class="copyright text-center"> &#169; 2015 - <?= date('Y') ?> uJobs.me</div>
        </div>
    </div>
</footer>

<?= LivechatWidget::widget() ?>

<div class="modal fade modalSubscribe" id="modalSubscribe" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-news">
                    <div class="ujobs-ava">
                        <img src="/images/new/logo.png" alt="ujobs">
                    </div>
                    <div class="news-info">
                        <div class="title text-left">
                            <?= Yii::t('app', 'Get all fresh news from {site}', ['site' => 'uJobs.me']) ?>
                        </div>
                        <p>
                            <?= Yii::t('app', 'Press to enable notifications') ?>
                        </p>
                        <div class="right-side">
                            <a class="button white middle" href="#"><?= Yii::t('app', 'No, thanks') ?></a>
                            <a class="button green middle" href="#" data-toggle="modal" data-target="#ModalLogin"
                               data-redirect="/tender/create"><?= Yii::t('app', 'Allow') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!isGuest()) {
    echo MessengerWidget::widget();
} ?>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "https://ujobs.me",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://ujobs.me/search/search?request={search_term_string}",
        "query-input": "required name=search_term_string"
    }
}
</script>

<?php $toggleJobUrl = Url::to(['/account/favourite/toggle']);
$toggleProductUrl = Url::to(['/account/favourite/toggle-product']);
$redirectSetUrl = Url::to(['/redirect/set']);
$pushSubscribeUrl = Url::to(['/push/ajax/subscribe'], true);

$watchMore = Yii::t('app', 'Show more');
$otherCategories = Yii::t('app', 'Other categories');
$minimize = Yii::t('app', 'Minimize');
$pushUnsubscribe = Yii::t('account', 'Unsubscribe');
$pushSubscribe = Yii::t('account', 'Subscribe');
$this->registerJsFile('//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js', [
    'async' => true
]);
$script = <<<JS
    $(document).on('change', 'select[name=app_currency_code], select[name=app_language]', function() {
        window.location.href = $(this).val();
    });

    $(document).on('click', '.toggle-favourite', function() {
        let self = $(this),
            type = $(this).data('type'),
            url = type === 'product' ? '$toggleProductUrl' : '$toggleJobUrl',
            data = {};

        data[(type === 'product' ? 'product_id' : 'job_id')] = $(this).data('id');
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function(response) {
                self.closest('.bf-item').find('.markers .feat').toggleClass('active');
                self.toggleClass('active');
                if (self.hasClass('active')) {
                    self.next('.wl-count').html(parseInt(self.next('.wl-count').html()) + 1);
                    self.data('hint', self.data('hint-remove'));
                } else {
                    self.next('.wl-count').html(parseInt(self.next('.wl-count').html()) - 1);
                    self.data('hint', self.data('hint-add'));
                }
            },
        });
        return false;
    });

    $(document).on('click', '.redirect-set', function() {
        $.post('$redirectSetUrl', {
            url: $(this).data('redirect')
        });
    });

    let oldMoreText = '';
    $(document).on('click', '.more-less', function() {
        if ($(this).siblings(".ml").hasClass('hide-filter')) {
            oldMoreText = $(this).html();
            $(this).html($(this).data('less') ? $(this).data('less') : '{$minimize}');
            $(this).siblings(".ml").removeClass('hide-filter').addClass('show-filter');
        } else {
            if ($(this).hasClass('more-cats')) {
                let oldText = oldMoreText ? oldMoreText : '{$otherCategories}';
                $(this).html(oldText);
            } else {
                let oldText = oldMoreText ? oldMoreText : '{$watchMore}';
                $(this).html(oldText);
            }
            $(this).siblings(".ml").removeClass('show-filter').addClass('hide-filter');
        }

        return false;
    });

    function Push() {
        const SERVER_API_SUBSCRIBERS = '{$pushSubscribeUrl}';
        
        /**
         * @type {Push}
         */
        const self = this;
        /**
        * @type {boolean}
        */
        this.isPushEnabled = false;
        
        this.attachEvent = function() {
            window.addEventListener('load', function() {
                let pushButton = document.querySelector('.js-push-button');
                if (pushButton) {
                    pushButton.addEventListener('click', function() {
                        if (self.isPushEnabled) {
                            self.unsubscribe();
                        } else {
                            self.subscribe();
                        }
            
                        $('.push-block').removeClass('fadeIn');
                    });
                }
            
                if ('serviceWorker' in navigator) {
                    navigator.serviceWorker.register('/worker.js')
                        .then(self.initializeState);
                }
            });
        };

        this.initializeState = function() {
            if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
                console.warn('Push notifications not supported by browser');
                return;
            }

            if (Notification.permission === 'denied') {
                console.warn('Permission for notifications denied');
                return;
            }

            if (!('PushManager' in window)) {
                console.warn('Push notifications not supported by browser');
                return;
            }

            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                serviceWorkerRegistration.pushManager.getSubscription()
                    .then(function(subscription) {
                        let pushButton = document.querySelector('.js-push-button');
                        if (pushButton) {
                            pushButton.disabled = false;

                            if (!subscription) {
                                setTimeout(function() {
                                    $('.push-block').addClass('fadeIn');
                                }, 2000);

                                $(document).on('click', '.push-block .push-close', function() {
                                    $('.push-block').removeClass('fadeIn');
                                });
                                return;
                            }

                            self.sendSubscriptionToServer(subscription);

                            pushButton.textContent = '{$pushUnsubscribe}';
                            self.isPushEnabled = true;
                        }
                    })
                    .catch(function(err) {
                        console.warn('Error retrieving info about subscriber', err);
                    });
            });
        };

        this.subscribe = function() {
            let pushButton = document.querySelector('.js-push-button');
            pushButton.disabled = true;

            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                serviceWorkerRegistration.pushManager.subscribe({
                        userVisibleOnly: true,
                        applicationServerKey: self.urlBase64ToUint8Array('BMuUgP6qOgp0onw4MyAN2vvESGyNC-WPtbfzUgRJ-xMon7ouL3NkL8Mzfwe4oafPxOcOqQLlk6OFcyNbaiZIk2E')
                    })
                    .then(function(subscription) {
                        self.isPushEnabled = true;
                        pushButton.textContent = '{$pushUnsubscribe}';
                        pushButton.disabled = false;

                        return self.sendSubscriptionToServer(subscription);
                    })
                    .catch(function(err) {
                        if (Notification.permission === 'denied') {
                            console.warn('User has denied notifications');
                            pushButton.disabled = true;
                        } else {
                            console.error('Cannot subscribe, error: ', err);
                            pushButton.disabled = false;
                            pushButton.textContent = '{$pushSubscribe}';
                        }
                    });
            });
        };

        this.urlBase64ToUint8Array = function(base64String) {
            const padding = '='.repeat((4 - base64String.length % 4) % 4);
            const base64 = (base64String + padding)
                .replace(/\-/g, '+')
                .replace(/_/g, '/');

            const rawData = window.atob(base64);
            const outputArray = new Uint8Array(rawData.length);

            for (let i = 0; i < rawData.length; ++i) {
                outputArray[i] = rawData.charCodeAt(i);
            }
            
            return outputArray;
        };

        this.unsubscribe = function() {
            let pushButton = document.querySelector('.js-push-button');
            pushButton.disabled = true;

            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                serviceWorkerRegistration.pushManager.getSubscription().then(
                    function(subscription) {
                        if (!subscription) {
                            self.isPushEnabled = false;
                            pushButton.disabled = false;
                            pushButton.textContent = '{$pushSubscribe}';
                            return;
                        }

                        let endpoint = subscription.endpoint;

                        subscription.unsubscribe().then(function(successful) {
                            pushButton.disabled = false;
                            pushButton.textContent = '{$pushSubscribe}';
                            self.isPushEnabled = false;
                        }).catch(function(err) {
                            console.log('Error on unsubscribe: ', err);
                            pushButton.disabled = false;
                            pushButton.textContent = '{$pushSubscribe}';
                        });
                    }).catch(function(err) {
                    console.error('Error on retrieving client data: ', err);
                });
            });
        };

        this.sendSubscriptionToServer = function(subscription) {
            let req = new XMLHttpRequest();

            req.open('POST', SERVER_API_SUBSCRIBERS, true);
            req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            req.setRequestHeader("Content-type", "application/json");
            req.onreadystatechange = function(e) {
                if (req.readyState === 4) {
                    if (req.status !== 200) {
                        console.error("[SW] Error:" + e.target.status);
                    }
                }
            };
            req.onerror = function(e) {
                console.error("[SW] Error:" + e.target.status);
            };

            let key = subscription.getKey('p256dh');
            let token = subscription.getKey('auth');

            req.send(JSON.stringify({
                'endpoint': self.getEndpoint(subscription),
                'key': key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
                'token': token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null
            }));

            return true;
        };

        this.getEndpoint = function(pushSubscription) {
            let endpoint = pushSubscription.endpoint;
            let subscriptionId = pushSubscription.subscriptionId;

            if (subscriptionId && endpoint.indexOf(subscriptionId) === -1) {
                endpoint += '/' + subscriptionId;
            }

            return endpoint;
        };
    }

    let PushObject = new Push();
    PushObject.attachEvent();
    
    [].forEach.call(document.querySelectorAll('.adsbygoogle'), function(){
        (adsbygoogle = window.adsbygoogle || []).push({});
    });
JS;
$this->registerJs($script); ?>
<?php $this->registerJs('
    function initMetrics() {
        (function(d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter32446180 = new Ya.Metrika({
                        id: 32446180,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor:true
                    });
                } catch (e) {}
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function() {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "/js/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
        if ($("#yandex_rtb_R-A-243363-1").length){
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-243363-1",
                        renderTo: "yandex_rtb_R-A-243363-1",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        }
        if ($("#yandex_rtb_R-A-243385-1").length) {
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: "R-A-243385-1",
                        renderTo: "yandex_rtb_R-A-243385-1",
                        async: true
                    });
                });
                t = d.getElementsByTagName("script")[0];
                s = d.createElement("script");
                s.type = "text/javascript";
                s.src = "//an.yandex.ru/system/context.js";
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, "yandexContextAsyncCallbacks");
        }
    }
    function initFB() {
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,\'script\',\'https://connect.facebook.net/en_US/fbevents.js\');
		fbq(\'init\', \'169448780209946\');
		fbq(\'track\', \'PageView\');
    }
    setTimeout(initMetrics, 200);
    setTimeout(initFB, 3500);
');
?>


