<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.11.2016
 * Time: 17:59
 */

?>

<?= \frontend\components\HeaderWidget::widget() ?>

<?php $this->registerJs("
    $(document).on('click', '.modal-dismiss', function () {
        var target = $(this).data('target');
        if (target) {
            $('.modal:not(' + target + ')').modal('hide');
        }
        else {
            $('.modal').modal('hide');
        }
    });
");