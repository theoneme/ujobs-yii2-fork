<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\components\HeaderRecoverModalWidget;
use frontend\components\MobileMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\HeaderLoginModalWidget;
use frontend\components\HeaderSignupModalWidget;
use frontend\components\HeaderCategories;

\frontend\assets\CommonAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>
<?= MobileMenu::widget() ?>
<div class="mainblock" style="background-color: #F7F7F7;">
    <header>
        <?= $this->render('_header') ?>
        <?= HeaderCategories::widget() ?>
    </header>
    <div class="all-content">
        <?= $content ?>
        <div class="mfooter"></div>
    </div>
</div>

<?= $this->render('_footer') ?>
<?= HeaderLoginModalWidget::widget() ?>
<?= HeaderSignupModalWidget::widget() ?>
<?= HeaderRecoverModalWidget::widget() ?>
<?//= NotyAlert::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
