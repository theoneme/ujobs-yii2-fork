<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.07.2017
 * Time: 20:43
 */

use yii\helpers\Html;

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'We provide a free bulletin board{attributes}{city}. On this board there are products from individuals and organizations. You can post your advertisement about selling or buying products for free.', $params)?>
</p>

<p>
    <?= Yii::t('seo', 'In this section of site you can buy products from categories {randomChildrenCategories} from people and companies with prices starting at {minPrice}', $params)?>
</p>

<p>
    <?= Yii::t('seo', 'Most of the products are being sold by secure transaction.') ?> <?= Html::a(Yii::t('app', 'Read more'), ['/page/static', 'alias' => 'fair-play']) ?>, <?= Yii::t('seo', 'With it it`s guaranteed that you will receive bought product or we will refund your money. If you want to choose products that are available through secure transaction you can use appropriate filter.') ?>
</p>

<p>
    <?= Yii::t('seo', 'Also you can buy products from other cities, if shipping is available for them.') ?>
</p>

<p>
    <?= Yii::t('seo', 'If you have not found product, that you need, you can post request. So sellers from the entire world will see your request and you will get huge amount of offers about selling this product.') ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
