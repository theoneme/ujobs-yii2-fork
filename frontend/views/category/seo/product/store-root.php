<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.10.2017
 * Time: 11:21
 */

use yii\helpers\Html;

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'We provide store page {store}{attributes}{city}.', $params)?>
</p>

<p>
    <?= Yii::t('seo', 'In this section of site you can buy products from categories {randomChildrenCategories} in store {store} with prices starting at {minPrice}', $params)?>
</p>

<p>
    <?= Yii::t('seo', 'Most of the products are being sold by secure transaction.') ?> <?= Html::a(Yii::t('app', 'Read more'), ['/page/static', 'alias' => 'fair-play']) ?>, <?= Yii::t('seo', 'With it it`s guaranteed that you will receive bought product or we will refund your money.') ?>
</p>

<p>
    <?= Yii::t('seo', 'If you have not found product, that you need, you can post request. So sellers from the entire world will see your request and you will get huge amount of offers about selling this product.') ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
