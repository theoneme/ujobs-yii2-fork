<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.07.2017
 * Time: 22:01
 */
use yii\helpers\Html;

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'In section {category}{attributes} are {count} products that you can buy from individuals and companies{city}. There are new and used products with prices starting at {minPrice}', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'You can choose offer that you are interested in by price and quality in your city, or in other city with shipping.') ?>
</p>

<p>
    <?= Yii::t('seo', 'We offer you to use secure transaction for money safety, so seller receives money right after you receive product and confirm that it corresponds to the specifications.') ?> <?= Html::a(Yii::t('app', 'Read more'), ['/page/static', 'alias' => 'fair-play']) ?>
</p>


<p>
    <?= Yii::t('seo', 'Also you can post request for product you are want to buy, so thousands of sellers from the entire world will see your request and will make offers to you. So you can choose the best offer.') ?> <?= Html::a(Yii::t('app', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-product']) ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
