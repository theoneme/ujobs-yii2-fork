<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.07.2017
 * Time: 22:01
 */
use yii\helpers\Html;

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'In category {category}{attributes}{city} are {count} products that you can buy from individuals and companies. There are new and used products with prices starting at {minPrice}.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'For safety of your money we implemented secure transaction, with it it`s guaranteed that you will receive bought product or we will refund your money. If you want to choose products that are available through secure transaction you can use appropriate filter.') ?>
</p>

<p>
    <?= Yii::t('seo', 'You can choose products, that are present not only in your city, but in other cities if shipping is available for product. So you can Buy {category} from other cities.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'If you have not found required product in {category}{attributes}{city}, you can post request, so sellers from the entire world can see what product for what price you want to buy. Posting request is absolutely for free.', $params) ?> <?= Html::a(Yii::t('app', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-product']) ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
