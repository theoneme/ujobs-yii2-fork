<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.08.2017
 * Time: 19:04
 */

?>

<p>
    <?= Yii::t('seo', 'On this page you can see products with filter {title}{city}, which you can buy from individuals and companies.', $params)?>
</p>

<p>
    <?= Yii::t('seo', 'For protection of your interests, we implemented secure transaction, so seller will receive money just after customer receives product.', $params)?>
</p>

<p>
    <?= Yii::t('seo', 'Also some products can be sold with shipping. Agree shipping details with seller before ordering.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'If you have not found product, that you need, you can post request and specify what product are you ready to buy and for what price. So thousands of sellers from around the world may see your request for buying {title}{attributes}{city} and make offers.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
