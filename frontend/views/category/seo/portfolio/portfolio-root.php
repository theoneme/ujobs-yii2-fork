<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.09.2017
 * Time: 14:52
 */

use yii\helpers\Html;

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'Here are photos and examples of works from our users. We hope that this will help you choose the best specialists for your tasks. You can also rate albums and leave comments.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'You can post your portfolio and tell us about the work that you have done.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'Real work examples have always been the best confirmation of a specialist\'s qualifications.') ?>
</p>

<p>
    <?= Yii::t('seo', 'You can post photos of any of your works for free.') ?>
</p>

<p>
    <?= Yii::t('seo', 'Portfolio can be created under the link {link}. Don`t forget to share your portfolio with friends. The more likes your albums have - the higher the positions they occupy in the catalog.', ['link' => Html::a(Yii::t('seo', 'Create portfolio for free'), ['/account/portfolio/create'])]) ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
