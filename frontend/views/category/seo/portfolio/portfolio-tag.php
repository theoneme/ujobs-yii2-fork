<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.09.2017
 * Time: 15:58
 */

use yii\helpers\Html;

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'Users of our portal posted their albums in {title} and awaiting for you to rate them.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'We hope, that this section will help you to choose the best specialist in {title} for your tasks.', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'Also you may post your own albums and tell about works that you have done. Real work examples have always been the best confirmation of a specialist\'s qualifications.') ?>
</p>

<p>
    <?= Yii::t('seo', 'You can post photos of any of your works for free.') ?>
</p>

<p>
    <?= Yii::t('seo', 'Portfolio can be created under the link {link}. Don`t forget to share your portfolio with friends. The more likes your albums have - the higher the positions they occupy in the catalog.', ['link' => Html::a(Yii::t('seo', 'Create portfolio for free'), ['/account/portfolio/create'])]) ?>
</p>

<p>
    <?= Yii::t('seo', 'Enjoy the shopping') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely, uJobs Team') ?>
</p>
