<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.05.2017
 * Time: 17:12
 */

/* @var array $params */

?>
<p>
    <?= Yii::t('seo', 'Ordering "{keyword}" is more advantageous through the marketplace than directly from the performer, for several reasons.', $params) ?>
</p>
<p>
    <?= Yii::t('seo', 'The main ones are:') ?>
</p>
<ol>
    <li>
        <?= Yii::t('seo', 'You pay only for the result, your money is protected when ordering the service. You do not risk losing money if the professional does not perform the job, or does it poorly;') ?>

    </li>
    <li>
        <?= Yii::t('seo', 'A large number of performers in one place allows you to find the right one for your job.') ?>
    </li>
</ol>
<p>
    <?= Yii::t('seo', 'Service ordering process is as follows:') ?>
</p>
<ol>
    <li>
        <?= Yii::t('seo', 'You find a suitable offer "{keyword}" by price, rating, terms of service;', $params) ?>
    </li>
    <li>
        <?= Yii::t('seo', 'Add the cost of "{keyword}" in the form of a deposit to your account;', $params) ?>
    </li>
    <li>
        <?= Yii::t('seo', 'Give the Professional all the details for the task;') ?>
    </li>
    <li>
        <?= Yii::t('seo', 'The Professional proceeds to the task. At the same time, he is obliged to make "{keyword}" in the specified period;', $params) ?>
    </li>
    <li>
        <?= Yii::t('seo', 'Upon completion of the assignment, the Professional notifies you of the implementation and provides the necessary confirmation;') ?>
    </li>
    <li>
        <?= Yii::t('seo', 'You check the work. And if everything suits you, take it. Otherwise - send it for revision.') ?>
    </li>
    <li>
        <?= Yii::t('seo', 'After the approval of the work by the client, the professional will receive the payment.') ?>
    </li>
</ol>
<p>
    <?= Yii::t('seo', 'We created this service to help customers find the best performers') ?>
</p>