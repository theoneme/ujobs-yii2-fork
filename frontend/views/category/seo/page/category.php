<?php

/* @var array $params */

?>

<p>
    <?= Yii::t('seo', 'On our portal you can not only read but also publish news and articles about «{category}»', $params) ?>
</p>

<p>
    <?= Yii::t('seo', 'Publishing articles and news will allow you to increase your reputation in the eyes of customers and tell about the features of your business.') ?>
</p>

<p>
    <?= Yii::t('seo', 'And interesting articles and news from other participants will help you to understand the features of the services and products that they offer and choose the most worthy.') ?>
</p>

<p>
    <?= Yii::t('seo', 'To ensure that your articles bring the maximum benefit to you, based on our experience, we recommend you:') ?>
</p>

<ol>
    <li>
        <?= Yii::t('seo', 'Try to write in the first person, telling in detail what you have news and information about «{category}»', $params) ?>
    </li>
    <li>
        <?= Yii::t('seo', 'The volume of articles that will allow you to be indexed by search engines should be at least 3 000 characters (one A4 page)') ?>
    </li>
    <li>
        <?= Yii::t('seo', 'Place as many articles and news as possible. Do this regularly') ?>
    </li>
    <li>
        <?= Yii::t('seo', 'Try to tell in an unobtrusive manner why you can achieve a better result by «{category}». Direct advertising rarely gives results.', $params) ?>
    </li>
    <li>
        <?= Yii::t('seo', 'The specific examples (Life case) about «{category}», where you tell what special features you had while working with this topic, what difficulties you faced and how you solved them, work well.', $params) ?>
    </li>
    <li>
        <?= Yii::t('seo', 'Publish high-quality pictures. Photo or infographics can quickly explain what you want to report.') ?>
    </li>
</ol>

<p>
    <?= Yii::t('seo', 'Good luck in reading and publishing news and articles on our portal.') ?>
</p>

<p>
    <?= Yii::t('seo', 'And remember that for the publication of articles our ALGORITHM raises your ratings and puts you higher in the issuance on the portal when searching.') ?>
</p>

<p>
    <?= Yii::t('seo', 'Sincerely,</br>Support service ujobs.me') ?>
</p>