<?php

use common\models\Category;
use frontend\assets\ArticleCatalogAsset;
use frontend\modules\elasticfilter\components\BaseFilter;
use frontend\modules\elasticfilter\components\FilterPage;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $category Category */
/* @var $entity string */
/* @var $seo array */
/* @var $filter FilterPage */
/* @var $filterView string */

ArticleCatalogAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => true, 'id' => 'catalog-pjax']); ?>
    <div class="catalog-breadcrumbs-container">
        <?= Breadcrumbs::widget([
            'itemTemplate' => "{link}/", // template for all links
            'activeItemTemplate' => "&nbsp;{link}",
            'links' => $filter->breadcrumbs,
            'options' => ['class' => 'breadcrumbs hidden-xs container-fluid'],
            'tag' => 'div'
        ]); ?>
    </div>
    <div class="how-works text-center">
        <a class="close-how" href=""></a>

        <h2 class="text-center"><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
            <div class="three-steps text-center">
                <div class="three-steps-item">
                    <div class="circle-works">
                        <div class="inner-circle"></div>
                        <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/articles/screen1.png"), [
                            'alt' => Yii::t('app', 'Register'),'title' => Yii::t('app', 'Register')
                        ]) ?>
                    </div>
                    <div class="circle-title"><?= Yii::t('app', 'Register') ?></div>
                    <p class="circle-descr text-center">
                        <?= $seo['upperBlockColumn1'] ?>
                    </p>
                </div>
                <div class="three-steps-item">
                    <div class="circle-works">
                        <div class="inner-circle"></div>
                        <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/articles/screen2.png"), [
                            'alt' => Yii::t('seo', 'Create article'),'title' => Yii::t('seo', 'Create article')
                        ]) ?>
                    </div>
                    <div class="circle-title"><?= Yii::t('seo', 'Create article') ?></div>
                    <p class="circle-descr text-center">
                        <?= $seo['upperBlockColumn2'] ?>
                    </p>
                </div>
                <div class="three-steps-item">
                    <div class="circle-works">
                        <div class="inner-circle"></div>
                        <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/articles/screen3.png"), [
                            'alt' => Yii::t('app', 'Share with friends'),'title' => Yii::t('app', 'Share with friends')
                        ]) ?>
                    </div>
                    <div class="circle-title"><?= Yii::t('app', 'Share with friends') ?></div>
                    <p class="circle-descr text-center">
                        <?= $seo['upperBlockColumn3'] ?>
                    </p>
                </div>
            </div>
    </div>
    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1 class="text-center"><?= Html::encode($seo['h1']) ?></h1>
            <div class="banner-prod-descr text-center">
                <?= Html::encode($seo['subhead']) ?>
            </div>
            <a class="product-how-works hidden-xs" href="#" id="how-work">
                <?= Yii::t('app', 'How the service works'); ?>
            </a>
        </div>
    </div>

    <div class="container-fluid category-top">
        <div class="block-mob mar-fixedmob visible-xs">
            <?php if (!isPagespeed()) {
                $filter->template = "{$filterView}-mobile";
                $filter->filterType = BaseFilter::FILTER_TYPE_MOBILE;
                echo $filter->run();
            } ?>
        </div>

        <div class="category-main row hidden-xs">
            <?php if (!isPagespeed()) {
                $filter->template = "{$filterView}";
                $filter->filterType = BaseFilter::FILTER_TYPE_DEFAULT;
                echo $filter->run();
            } ?>
        </div>
    </div>

<?php if ($seo['textHeading'] !== null) { ?>
    <div class="text-categories">
        <div class="container-fluid">
            <h2 class="text-left">
                <?= $seo['textHeading'] ?>
            </h2>
            <h3>
                <?= $seo['textSubheading'] ?>
            </h3>
            <div class="categories-descr">
                <?= nl2br($seo['text']) ?>
            </div>
        </div>
    </div>
<?php } ?>

<?= \frontend\components\CrosslinkWidget::widget() ?>

<?php Pjax::end(); ?>

<?php
$howItWorks = Yii::t('app', 'How the service works');
$minimizeText = Yii::t('app', 'Minimize');

$script = <<<JS
	var body = $('body');

	body.on('click', '#how-work', function(e) {
        if($('.how-works').hasClass('open-how')) {
            $(this).html('$minimizeText');
        } else {
            $(this).html('$howItWorks');
        }
    }).on('click', '.categories-read-more', function(e) {
        e.preventDefault();
        $('.cd-short').hide();
        $('.cd-all').show();
    });

	var text = $('.cd-all').html();
	var size = 900;
    if (text !== undefined && text.length > size) {
	    text = text.slice(0, 900);
		var lastSpace = text.lastIndexOf(" ");
		if( lastSpace > 0) {
		    text = text.substr(0, lastSpace);
		}
		$('.cd-short').html(text + '...<br/> <a class=\"categories-read-more\" href=\"\">Read more</a>');
	}
	
    $(document).on('pjax:complete', function(e) {
		var page = getParameterByName('page');
		if(page !== null) {
			$('html, body').stop().animate({
	            scrollTop: $('#jobs-pjax').offset().top
	        }, 500);
		}
	});
    
    $("aside,.job-section").stick_in_parent({offset_top: 100,inner_scrolling: true});
JS;

$this->registerJs($script);