<?php

use common\models\Category;
use common\models\Job;
use frontend\assets\CatalogAsset;
use frontend\components\JobSearchHelpWidget;
use frontend\modules\elasticfilter\components\BaseFilter;
use frontend\modules\elasticfilter\components\FilterJob;
use frontend\modules\elasticfilter\components\FilterPro;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $category Category */
/* @var $children Category[] */
/* @var $entity string */
/* @var $seo array */
/* @var $filterView string */
/* @var $filter FilterJob|FilterPro */

CatalogAsset::register($this);

?>

<?php Pjax::begin(['enablePushState' => true, 'id' => 'catalog-pjax']); ?>
    <div class="catalog-breadcrumbs-container">
        <?= Breadcrumbs::widget([
            'itemTemplate' => "{link}/", // template for all links
            'activeItemTemplate' => "&nbsp;{link}",
            'links' => $filter->breadcrumbs,
            'options' => ['class' => 'breadcrumbs hidden-xs container-fluid'],
            'tag' => 'div'
        ]); ?>
    </div>
    <div class="how-works text-center">
        <a class="close-how" href=""></a>

        <h2 class="text-center"><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
        <div class="three-steps text-center">
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/{$entity}s/screen1.png", [
                        'alt' => Yii::t('app', 'Make choice'),'title' => Yii::t('app', 'Make choice')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Make choice') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn1'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/{$entity}s/screen2.png", [
                        'alt' => Yii::t('app', 'Make order'),'title' => Yii::t('app', 'Make order')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Make order') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn2'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/{$entity}s/screen3.png", [
                        'alt' => Yii::t('app', 'Done'),'title' => Yii::t('app', 'Done')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Done') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn3'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1 class="text-center"><?= Html::encode($seo['h1']) ?></h1>
            <div class="banner-prod-descr text-center">
                <?= Html::encode($seo['subhead']) ?>
            </div>
            <a class="product-how-works hidden-xs" href="#" id="how-work">
                <?= Yii::t('app', 'How the service works'); ?>
            </a>
        </div>
    </div>

<?php if ($entity === Job::TYPE_JOB) { ?>
    <div id="job-search-help">
        <?= JobSearchHelpWidget::widget([
            'entity' => 'category',
            'entity_content' => $category->id,
            'step_id' => Yii::$app->request->post('jobSearchHelpStepId'), 'job_id' => Yii::$app->request->post('jobSearchHelpJobId'),
            'lvl' => Yii::$app->request->post('jobSearchHelpLvl', 1)
        ]) ?>
    </div>
<?php } ?>

    <div class="container-fluid category-top">
        <div class="block-mob mar-fixedmob visible-xs">
            <?php if (!isPagespeed()) {
                $filter->template = "{$filterView}-mobile";
                $filter->filterType = BaseFilter::FILTER_TYPE_MOBILE;
                echo $filter->run();
            } ?>
        </div>

        <div class="category-main row hidden-xs">
            <?php $filter->template = $filterView;
            $filter->filterType = BaseFilter::FILTER_TYPE_DEFAULT;
            echo $filter->run(); ?>
            <?php if ($entity !== Job::TYPE_TENDER) { ?>
                <div class="mark-container">
                    <div class="mark row">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                        <div class="mark-text">
                            <?= Yii::t('app', 'Didn\'t find what you were looking for or don\'t have time to search?') ?>
                            <div class="mark-small">
                                <?= Yii::t('app', 'Submit Request and hundreds of Workers will be able to send you their proposals. You will have plenty to choose from.') ?>
                            </div>
                        </div>
                        <?php
                        if (isGuest()) {
                            echo Html::a(Yii::t('app', 'Submit request'), null, [
                                'class' => 'btn-big bright redirect-set',
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalSignup',
                                'data-redirect' => Url::to(['/entity/global-create', 'type' => 'tender-service'])
                            ]);
                        } else {
                            echo Html::a(Yii::t('app', 'Submit request'), ['/entity/global-create', 'type' => 'tender-service'], [
                                'class' => 'btn-big bright',
                                'data-pjax' => 0
                            ]);
                        } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

<?= \frontend\components\CrosslinkWidget::widget() ?>

<?php Pjax::end(); ?>

<?php $script = <<<JS
    $(document).on('pjax:complete', function(e) {
		var page = getParameterByName('page');
		if(page !== null) {
			$('html, body').stop().animate({
	            scrollTop: $('#jobs-pjax').offset().top
	        }, 500);
		}
	})
JS;

$this->registerJs($script);
