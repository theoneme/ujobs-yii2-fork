<?php

use common\models\Category;
use frontend\assets\AccountAsset;
use frontend\assets\CatalogAsset;
use frontend\components\BannerWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $category Category */
/* @var $children Category[] */
/* @var $sortData array */
/* @var $dataProvider ActiveDataProvider */
/* @var $seo array */
/* @var $request string */
/* @var $breadcrumbs array */

CatalogAsset::register($this);
AccountAsset::register($this);

?>
<?php Pjax::begin(['clientOptions' => ['maxCacheLength' => 0], 'id' => 'catalog-pjax']); ?>
    <div class="catalog-breadcrumbs-container">
        <?= Breadcrumbs::widget([
            'itemTemplate' => "{link}/", // template for all links
            'activeItemTemplate' => "&nbsp;{link}",
            'links' => $breadcrumbs,
            'options' => ['class' => 'breadcrumbs hidden-xs container-fluid'],
            'tag' => 'div'
        ]); ?>
    </div>
    <div class="how-works text-center">
        <a class="close-how" href=""></a>

        <h2 class="text-center"><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
        <div class="three-steps text-center">
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/videos/screen1.png"), [
                        'alt' => Yii::t('app', 'Make choice'),'title' => Yii::t('app', 'Make choice')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Select a topic') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn1'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/videos/screen2.png"), [
                        'alt' => Yii::t('app', 'Make order'),'title' => Yii::t('app', 'Make order')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Select a video') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn2'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/videos/screen3.png"), [
                        'alt' => Yii::t('app', 'Done'),'title' => Yii::t('app', 'Done')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Done') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn3'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1 class="text-center"><?= Html::encode($seo['h1']) ?></h1>
            <div class="banner-prod-descr text-center">
                <?= Html::encode($seo['subhead']) ?>
            </div>
            <a class="product-how-works hidden-xs" href="#" id="how-work">
                <?= Yii::t('app', 'How the service works'); ?>
            </a>
        </div>
    </div>

    <div class="container-fluid">
        <div class="visible-xs">
            <div class="subcats-mobile-block">
                <div>
                    <div class="text-chose-subc text-left visible-xs "><?= Yii::t('app', 'Choose Subcategory') ?></div>
                    <div class="ml hide-filter">
                        <?php foreach ($children as $key => $child) { ?>
                            <?= Html::a($child->translations[Yii::$app->language]->title . ($child->relatedCount ? " ({$child->relatedCount})" : ''), ['/category/videos', 'alias' => $child->translations[Yii::$app->language]->slug]); ?>
                        <?php } ?>
                    </div>
                    <?php if (count($children) > 4) {
                        echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($children) - 4)]), null, [
                            'class' => 'more-less more-cats',
                            'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($children) - 4)]),
                            'data-less' => Yii::t('app', 'See less'),
                        ]) ?>
                    <?php } ?>
                </div>
            </div>
            <?php echo ListView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'jobs-mobile',
                'itemView' => '@frontend/components/catalog/views/video-list-mobile',
                'layout' => "{pager}<div class='clearfix'>{items}</div>{pager}",
                'options' => [
                    'class' => ''
                ],
                'itemOptions' => [
                    'class' => 'mob-work-item'
                ],
                'pager' => [
                    'nextPageLabel' => '→',
                    'prevPageLabel' => '←',
                    'maxButtonCount' => 5,
                ],
            ]); ?>
        </div>

        <div class="category-main row hidden-xs">
            <div class="filter-top clearfix">
                <?= Html::beginForm('', 'post', [
                    'id' => 'top_filter_form'
                ]) ?>
                <div id="catalog-search">
                    <?= Html::textInput('request', $request, [
                        'class' => 'form-control autocomplete',
                        'placeholder' => Yii::t('app', 'Search'),
                    ]) ?>
                    <button type="submit" class="btn-middle">
                        <?= Yii::t('app', 'Search') ?>
                    </button>
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
                <div id="catalog-entity-create">
                    <?= Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'btn-middle btn-hov-dark', 'data-pjax' => 0]) ?>
                </div>
                <div id="catalog-sort" class="text-right">
                    <?= Html::hiddenInput('sort_by', isset(Yii::$app->request->queryParams['sort_by']) ? $sortData['value'] : '', ['id' => 'sort-by']) ?>
                    <div class="ftr-text"><?= Yii::t('app', 'Sort By') ?>:</div>
                    <div class="ftr-choose" id="sort-selector">
                        <div class="ftr-current">
                            <a href="javascript:void(0)"><?= $sortData['title'] ?></a>
                        </div>
                        <ul data-target="sort-by no-markers" data-selector="sort-selector" class="ftr-list no-markers">
                            <li><a data-pjax="0" data-entity="created_at;desc"
                                   href="#"><?= Yii::t('filter', 'Publish date') ?></a></li>
                            <li><a data-pjax="0" data-entity="price;asc"
                                   href="#"><?= Yii::t('filter', 'Price ascending') ?></a></li>
                            <li><a data-pjax="0" data-entity="price;desc"
                                   href="#"><?= Yii::t('filter', 'Price descending') ?></a></li>
                        </ul>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
            <aside>
                <div class="aside-body">
                    <div class="aside-block">
                        <ul class="cats no-markers">
                            <li>
                                <h2 class="text-left"><?= Yii::t('filter', 'All in') . ' ' . Html::encode($seo['h1']) ?></h2>
                                <div class="ml <?= (count($children) > 4) ? "hide-filter hf-categories" : "" ?>">
                                    <ul>
                                        <?php foreach ($children as $key => $child) { ?>
                                            <li class="<?= isset($child['active']) ? "active" : "" ?>">
                                                <?= Html::a($child->translations[Yii::$app->language]->title . ($child->relatedCount ? " ({$child->relatedCount})" : ''), ['/category/videos', 'alias' => $child->translations[Yii::$app->language]->slug]); ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php if (count($children) > 4) {
                                    echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($children) - 4)]), null, [
                                        'class' => 'more-less more-cats',
                                        'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($children) - 4)]),
                                        'data-less' => Yii::t('app', 'See less'),
                                    ]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <?= BannerWidget::widget() ?>
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:240px;height:400px"
                         data-ad-client="ca-pub-7280089675102373"
                         data-ad-slot="7816114377">
                    </ins>
                </div>
            </aside>
            <div class="cat-list">
                <?php echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '@frontend/components/catalog/views/video-list',
                    'layout' => "{items}{pager}",
                    'options' => [
                        'tag' => 'div',
                        'class' => 'block-flex'
                    ],
                    'itemOptions' => [
                        'tag' => 'div',
                        'class' => 'bf-item bf-item-video'
                    ],
                    'emptyText' =>
                        '<p class="catalog-empty-text">' .
                        Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => $category->translation->title]) .
                        '</p>'
                ]); ?>
            </div>
        </div>
    </div>
<?php $canonical = Url::canonical();
$script = <<<JS
    function applyFilter(url) {
        let baseUrl = "$canonical";
        if(url.length > 0) {
            if(baseUrl.indexOf('?') > -1) {
                baseUrl += '&' + url;
            } else {
                baseUrl += '?' + url;
            }         
        } 

        $.pjax.reload({container: '#catalog-pjax', url: baseUrl});
    }

    function processFilterForms(object) {
        let inputs = $('#top_filter_form input:not([name=_csrf-frontend])'),
            dropdowns = $('#filter_form select');
        
        $.each(dropdowns, function(index, value) {
            if($(this).find('option:selected') && $(this).find('option:selected').val()) {
                inputs.push($(this)[0]);
            }
        }).promise().done(function() {
            let url = inputs.filter(function(index, element) {
				return $(element).val() !== "";
			}).serialize();
            
            applyFilter(url);
        });
    }

    $('#top_filter_form').submit(function() {
        processFilterForms($(this));
        return false;
    });
    
    $('#sort-selector li a, #sort-dir-selector li a').click(function(e) {
        e.preventDefault();
        let target = $(this).parent().parent().attr('data-target'),
            selector = $(this).parent().parent().attr('data-selector'),
            entity = $(this).attr('data-entity');

        $('#' + target).val(entity);
        processFilterForms($(this));
    });
JS;

$this->registerJs($script); ?>
<?php Pjax::end(); ?>

<?php $howItWorks = Yii::t('app', 'How the service works');
$minimizeText = Yii::t('app', 'Minimize');
$script = <<<JS
    $(document).on('click', '.ftr-current a', function(e) {
        e.preventDefault();
        if ($(this).parents('.ftr-choose').hasClass('open')) {
            $(this).parents('.ftr-choose').removeClass('open');
        } else {
            $(this).parents('.ftr-choose').addClass('open');
        }
    });

    $(document).on('mouseleave', '.ftr-list', function() {
        $(this).parents('.ftr-choose').removeClass('open');
    });
    
    $(document).on('click', '#how-work', function(e) {
        if($('.how-works').hasClass('open-how')) {
            $(this).html('$minimizeText');
        } else {
            $(this).html('$howItWorks');
        }
    }).on('click', '.categories-read-more', function(e) {
        e.preventDefault();
        $('.cd-short').hide();
        $('.cd-all').show();
    });
JS;

$this->registerJs($script);