<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.09.2017
 * Time: 16:35
 */

use common\models\Page;
use frontend\assets\CatalogAsset;
use frontend\assets\PortfolioAsset;
use frontend\modules\elasticfilter\components\BaseFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/** @var $page Page */
/** @var $seo array */
/** @var $filterView string */

CatalogAsset::register($this);
PortfolioAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => true, 'id' => 'catalog-pjax']); ?>
    <div class="catalog-breadcrumbs-container">
        <?= Breadcrumbs::widget([
            'itemTemplate' => "{link}/", // template for all links
            'activeItemTemplate' => "&nbsp;{link}",
            'links' => $filter->breadcrumbs,
            'options' => ['class' => 'breadcrumbs hidden-xs container-fluid'],
            'tag' => 'div'
        ]); ?>
    </div>
    <div class="how-works text-center">
        <a class="close-how" href=""></a>

        <h2 class="text-center"><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
        <div class="three-steps text-center">
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/portfolios/screen1.png"), [
                        'alt' => Yii::t('app', 'Register'),'title' => Yii::t('app', 'Register')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Register') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn1'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/portfolios/screen2.png"), [
                        'alt' => Yii::t('app', 'Create album'),'title' => Yii::t('app', 'Create album')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Create album') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn2'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/portfolios/screen3.png"), [
                        'alt' => Yii::t('app', 'Share with friends'),'title' => Yii::t('app', 'Share with friends')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('app', 'Share with friends') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn3'] ?>
                </p>
            </div>
        </div>
    </div>

    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1 class="text-center"><?= $seo['h1'] ?></h1>
            <div class="banner-prod-descr text-center">
                <?= $seo['subhead'] ?>
            </div>
            <a class="product-how-works hidden-xs" href="#" id="how-work">
                <?= Yii::t('app', 'How to create a portfolio'); ?>
            </a>
        </div>
    </div>

    <div class="container-fluid">
        <?php if (!isPagespeed()) { ?>
            <div class="block-mob mar-fixedmob visible-xs">
                <?php
                $filter->template = "{$filterView}-mobile";
                $filter->filterType = BaseFilter::FILTER_TYPE_MOBILE;
                echo $filter->run();
                ?>
            </div>
        <?php } ?>

        <div class="category-main row hidden-xs">
            <?php
            $filter->template = $filterView;
            $filter->filterType = BaseFilter::FILTER_TYPE_DEFAULT;
            echo $filter->run();
            ?>
            <div class="mark-container">
                <div class="mark row">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                    <div class="mark-text">
                        <?= Yii::t('app', 'Didn\'t find what you were looking for or don\'t have time to search?') ?>
                        <div class="mark-small">
                            <?= Yii::t('app', 'Submit Request and hundreds of Workers will be able to send you their proposals. You will have plenty to choose from.') ?>
                        </div>
                    </div>
                    <?php
                    if (isGuest()) {
                        echo Html::a(Yii::t('app', 'Submit request'), null, [
                            'class' => 'btn-big bright redirect-set',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalSignup',
                            'data-redirect' => Url::to(['/entity/global-create', 'type' => 'tender-product'])
                        ]);
                    } else {
                        echo Html::a(Yii::t('app', 'Submit request'), ['/entity/global-create', 'type' => 'tender-product'], [
                            'class' => 'btn-big bright',
                            'data-pjax' => 0
                        ]);
                    } ?>
                </div>
            </div>
        </div>
    </div>

<?php if (!empty($seo['textHeading'])) { ?>
    <div class="text-categories">
        <div class="container-fluid">
            <h2 class="text-left">
                <?= $seo['textHeading'] ?>
            </h2>
            <h3>
                <?= $seo['textSubheading'] ?>
            </h3>
            <div class="categories-descr">
                <?= nl2br($seo['text']) ?>
            </div>
        </div>
    </div>
<?php } ?>

<?= \frontend\components\CrosslinkWidget::widget() ?>

    <div class="album-modal">

    </div>
<?php Pjax::end(); ?>

<?php
$howItWorks = Yii::t('app', 'How the service works');
$minimizeText = Yii::t('app', 'Minimize');

$script = <<<JS
    function masonry(container, selector) {
        let msnry;
        
        imagesLoaded(container, function() {
            msnry = new Masonry(container, {
                itemSelector: selector,
                columnWidth: selector
            });
        });
    }

	$(document).on('pjax:complete', function() {
		$('html, body').stop().animate({
            scrollTop: $('#catalog-pjax').offset().top - 50
        }, 500);
		
		masonry(document.querySelector('#portfolio-grid'), '.pg-item');
	});
    
    masonry(document.querySelector('#portfolio-grid'), '.pg-item');

    $('body').on('click', '#how-work', function(e) {
        if($('.how-works').hasClass('open-how')) {
            $(this).html('$minimizeText');
        } else {
            $(this).html('$howItWorks');
        }
    });
JS;

$this->registerJs($script);