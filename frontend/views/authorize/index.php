<?php

use common\components\CurrencyHelper;
use common\models\Payment;
use common\modules\store\assets\StoreAsset;
use frontend\assets\CatalogAsset;
use frontend\assets\FairPlayAsset;
use frontend\models\AuthorizeForm;
use yii\helpers\Html;
use yii\web\View;

FairPlayAsset::register($this);
StoreAsset::register($this);
CatalogAsset::register($this);

/* @var AuthorizeForm $model */
/* @var Payment $payment */
/* @var View $this */
/* @var string $formTemplate */

$this->title = Yii::t('order', 'Pay with {what}', ['what' => 'Authorize.net']);
?>

<div class="container-fluid">
    <div class="cart-box row">
        <div class="cart-left col-md-8 col-sm-8 col-xs-12">
            <div class="cart-header-block"><?= Yii::t('order', 'Pay with {what}', ['what' => 'Authorize.net']) ?></div>
            <div class="payment-options cart-tutorial">
                <?= $this->render($formTemplate, ['model' => $model])?>
            </div>
        </div>
        <div class="cart-right col-md-4 col-sm-4 col-xs-12">
            <div class="total-box">
                <div class="price-line">
                    <div class="price-total-title text-left">
                        <?= Yii::t('model', 'Summary') ?>
                    </div>
                    <div class="price-total-sum text-right">
                        <?= CurrencyHelper::convertAndFormat($payment->currency_code, Yii::$app->params['app_currency_code'], $payment->total)?>
                    </div>
                </div>
                <div class="price-line total-all">
                    <div class="price-total-title text-left">
                        <?= Yii::t('model', 'Total') ?>
                    </div>
                    <div class="price-total-sum text-right">
                        <?= CurrencyHelper::convertAndFormat($payment->currency_code, Yii::$app->params['app_currency_code'], $payment->total)?>
                    </div>
                </div>
            </div>
            <div class="cart-fair-play">
                <?= Html::a(Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
                    ['/page/static', 'alias' => 'fair-play'],
                    [
                        'data-pjax' => 0,
                        'class' => 'image-container'
                    ]
                ) ?>
                <div class="post-mes" style="margin-top: 20px;">
                    <?= Html::a(Yii::t('app', 'We guarantee the execution of each order or we will refund your money back'),
                        ['/page/static', 'alias' => 'fair-play'],
                        [
                            'data-pjax' => 0,
                            'class' => 'message-container'
                        ]
                    ) ?>
                </div>
            </div>
        </div>
    </div>
</div>