<?php

use frontend\assets\CardPaymentAsset;
use frontend\assets\JqueryMaskAsset;
use frontend\models\AuthorizeForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

JqueryMaskAsset::register($this);
CardPaymentAsset::register($this);

/* @var AuthorizeForm $model */
/* @var View $this */

$years = range(date('Y'), date('Y') + 50);
$years = array_combine($years, $years);
$months = range(1, 12);
$months = array_combine($months, $months);
?>

<?php $form = ActiveForm::begin([
    'id' => 'authorize-form',
    'action' => Url::to(['/payment/authorize-process']),
    'options' => ['class' => 'card-payment-form-container text-left']
]); ?>
    <div class="form-group field-authorizeform-cardnumber required">
        <?= Html::label(Yii::t('model', 'Card Number'))?>
        <div class="payment-method-icons">
            <?= Html::img('/images/new/payment/visa.png')?>
            <?= Html::img('/images/new/payment/mastercard.png')?>
            <?= Html::img('/images/new/payment/american.png')?>
            <?= Html::img('/images/new/payment/diners.png')?>
        </div>
        <?= Html::textInput('card_number', $model->cardCode, ['class' => 'form-control', 'id' => 'card-number']) ?>
        <?= $form->field($model, 'cardNumber', ['options' => ['tag' => false]])->hiddenInput(['class' => 'card-number-input'])->label(false) ?>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="form-group card-payment-date-block field-authorizeform-year field-authorizeform-month required">
                <?= Html::label(Yii::t('model', 'Expiration Date'))?>
                <?= $form->field($model, 'year', ['template' => '{input}', 'options' => ['tag' => false]])->dropDownList($years, ['class' => 'form-control card-payment-year']) ?>
                <?= $form->field($model, 'month', ['template' => '{input}', 'options' => ['tag' => false]])->dropDownList($months, ['class' => 'form-control card-payment-month']) ?>
            </div>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'cardCode')->textInput(['id' => 'cvv'])->error(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'firstName')->textInput() ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'lastName')->textInput() ?>
        </div>
    </div>
    <?= $form->field($model, 'address')->textInput() ?>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'country')->textInput() ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'state')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'city')->textInput() ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'zip')->textInput() ?>
        </div>
    </div>
    <?= $form->field($model, 'paymentId')->hiddenInput(['class' => 'shortcut-payment-id'])->label(false) ?>
    <?= Html::hiddenInput('scenario', AuthorizeForm::SCENARIO_ADVANCED) ?>
    <div class="text-right">
        <?= Html::submitButton(Yii::t('app', 'Pay'), ['class' => 'btn-mid card-payment-submit shortcut-submit']) ?>
    </div>
<?php ActiveForm::end() ?>

<?php
$script = <<<JS
    $('#card-number').mask("0000-0000-0000-0000", {
        placeholder: "1234-5678-9012-3456", 
        onChange: function(val){
            $('.card-number-input').val(val.replace(/[^0-9]/g, '')).trigger('change');
        }
    });
    $('#cvv').mask("0009");
JS;
$this->registerJs($script);
