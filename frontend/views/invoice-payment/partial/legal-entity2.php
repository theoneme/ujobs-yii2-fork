<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.03.2017
 * Time: 18:25
 */

?>

<div class="container-doc">
    <header>
        <div class="logo-doc"></div>
        <div class="head-doc">
            <h1 class="title-doc text-center">OOO uJobs</h1>
            <p>127000, г. Москва, Малый пер., д.1, стр. 1, оф. 10</p>
            <p>ИНН/КПП 7700000001 / 7700000001</p>
            <p>ОГРН 11046000001</p>
            <p>(555) 555-0000</p>
            <p>(555) 555-0000</p>
        </div>
    </header>
    <div class="lines"></div>
    <table class="rekviziti">
        <caption>Общество с огрниченной ответственностью "Компания"</caption>
        <tbody>
        <tr>
            <td>ООО "Компания"</td>
            <td>ООО "Покупатель"</td>
        </tr>
        <tr>
            <td>127000, г. Москва, Малый пер., д.1, стр. 1, оф. 10</td>
            <td>127000, г. Москва, Малый пер., д.2, стр. 2, оф. 20</td>
        </tr>
        <tr>
            <td>ИНН/КПП 7700000001 / 7700000001</td>
            <td>ИНН/КПП 7700000002 / 7700000002</td>
        </tr>
        <tr>
            <td>ОГРН 1107746000001</td>
            <td>ОГРН 1107746000002</td>
        </tr>
        <tr>
            <td>Расчётный счёт 4070000000000000001</td>
            <td>Расчётный счёт 4070000000000000002</td>
        </tr>
        <tr>
            <td>ЗАО "БАНК" г. Москва</td>
            <td>ЗАО "БАНК" г. Москва</td>
        </tr>
        <tr>
            <td>Кор. счёт 3010000000000000001</td>
            <td>Кор. счёт 3010000000000000002</td>
        </tr>
        <tr>
            <td>БИК 044000000</td>
            <td>БИК 044000001</td>
        </tr>
        <tr>
            <td>Телефон (555) 555-0000</td>
            <td>Телефон (555) 555-1111</td>
        </tr>
        <tr>
            <td>Факс (555) 555-0000</td>
            <td>Факс (555) 555-1111</td>
        </tr>
        </tbody>
    </table>
    <div class="number-schet">Счёт № 1-0405-1 от 28 марта 2017 г.</div>
    <div class="name-schet">обслуживание вычислительной техники</div>
    <table class="buy">
        <thead>
        <tr>
            <th>№</th>
            <th>Товар / Услуги</th>
            <th>Цена, руб.</th>
            <th>Кол-во</th>
            <th>Ед. изм.</th>
            <th>НДС</th>
            <th>Сумма, руб.</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="align-center">1</td>
            <td>Обслживание вычислительной техники</td>
            <td class="align-right">2'000.00</td>
            <td class="align-center">1</td>
            <td class="align-center">шт</td>
            <td class="align-center">18%</td>
            <td class="align-right">2'000.00</td>
        </tr>
        <tr>
            <td class="align-center">2</td>
            <td>Ремонт сетевого принтера</td>
            <td class="align-right">1'200.00</td>
            <td class="align-center">1</td>
            <td class="align-center">шт</td>
            <td class="align-center">18%</td>
            <td class="align-right">1'200.00</td>
        </tr>
        <tr>
            <td class="align-center">3</td>
            <td>Ремонт системного блока</td>
            <td class="align-right">2'600.00</td>
            <td class="align-center">1</td>
            <td class="align-center">шт</td>
            <td class="align-center">18%</td>
            <td class="align-right">2'600.00</td>
        </tr>
        <tr>
            <td class="align-center">4</td>
            <td>Установка и программирование АТС</td>
            <td class="align-right">8'100.00</td>
            <td class="align-center">1</td>
            <td class="align-center">шт</td>
            <td class="align-center">18%</td>
            <td class="align-right">8'100.00</td>
        </tr>
        <tr>
            <td colspan="6">
                <p class="align-right">Итого, в т. ч. НДС (18%)=2120.33 руб.:</p>
                <p class="align-right">Тринадцать тысяч девятьсот рублей, 00 копеек</p>
            </td>
            <td class="align-right"><strong>13'900.00</strong></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="7">
                <p>Оплата по счёту № 1-0405-1 от 28 марта 2017 г. за обслуживание вычислительной техники в т. ч. НДС (18%)=2120.33 руб.</p>
                <p class="align-right"><em>Назначение платежа</em></p>
            </td>
        </tr>
        </tfoot>
    </table>
    <footer>
        <div class="footer-line">
            <div class="who">Главный директор</div>
            <div class="who-name">Кашин С.</div>
            <div class="who-subscribe">
                <div class="subscribe"></div>
            </div>
        </div>
        <div class="pechat">

        </div>
    </footer>
</div>