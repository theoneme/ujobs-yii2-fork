<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 29.03.2017
 * Time: 15:21
 */

use common\components\CurrencyHelper;
use common\models\Payment;
use frontend\models\InvoiceForm;
use yii\helpers\Html;

/* @var Payment $payment */
/* @var InvoiceForm $model */

$convertedFormattedTotal = CurrencyHelper::convertAndFormat($payment->currency_code, 'RUB', $payment->total, false, 2);
$convertedTotal = CurrencyHelper::convertAndFormat($payment->currency_code, 'RUB', $payment->total, false, 2);

?>
<div class="pdf-container">
    <link href="/css/new/pdf-style.css" rel="stylesheet">
    <p class="align-center" style="font-size: 12px;">
        Внимание! Оплата данного счёта означает согласие с условиями предоставления услуг.
    </p>
    <p class="align-center" style="font-size: 12px;">
        Уведомление об оплате обязательно отправить на Email support@ujobs.me, в противном случае не гарантируется
        предоставление услуг.
    </p>
    <p class="align-center" style="font-size: 12px;">
        Услуги предоставляются по факту прихода денег на р/с Поставщика, информацию о Вашем счете Вы можете посмотреть
        по ссылке https://ujobs.me/account/payment/index.
    </p>
    <table width="800" class="rekviziti">
        <tr>
            <td width="500" style="border-bottom: none;" colspan="2">
                Уральский банк ПАО Сбербанк
            </td>
            <td width="100">
                БИК
            </td>
            <td width="200" style="border-bottom: none;">
                046577674
            </td>
        </tr>
        <tr>
            <td style="border-top: none;border-bottom: none;" colspan="2"></td>
            <td style="border-bottom: none;">
                Сч.
            </td>
            <td style="border-top: none;border-bottom: none;">
                30101810500000000674
            </td>
        </tr>
        <tr>
            <td style="border-top: none;" colspan="2">Банк получателя</td>
            <td style="border-top: none;"></td>
            <td style="border-top: none;"></td>
        </tr>
        <tr>
            <td>ИНН 6658496220</td>
            <td>КПП 665801001</td>
            <td style="border-bottom: none;">Сч.</td>
            <td style="border-bottom: none;">40702810816540034374</td>
        </tr>
        <tr>
            <td style="border-top: none;border-bottom: none;" colspan="2">ООО “ЮДЖОБС”</td>
            <td style="border-top: none;border-bottom: none;"></td>
            <td style="border-top: none;border-bottom: none;"></td>
        </tr>
        <tr>
            <td style="border-top: none;border-bottom: none;" colspan="2"></td>
            <td style="border-top: none;border-bottom: none;"></td>
            <td style="border-top: none;border-bottom: none;"></td>
        </tr>
        <tr>
            <td style="border-top: none;" colspan="2">Получатель</td>
            <td style="border-top: none;"></td>
            <td style="border-top: none;"></td>
        </tr>
    </table>
    <p class="number-schet">Счёт на оплату № <?= $model->orderId ?>
        от <?= Yii::$app->formatter->asDate(time(), 'd MMMM yyyy') ?></p>
    <table class="legal-contacts">
        <tr>
            <td>Поставщик:</td>
            <td>
                <strong>
                    ООО “ЮДЖОБС”, ИНН 6658496220, КПП 665801001, 620109, г. Екатеринбург, ул. Ключевская 15, Телефон +7
                    (912) 251-89-48
                </strong>
            </td>
        </tr>
        <tr>
            <td>Покупатель:</td>
            <td>
                <strong>
                    <?= $model->companyName ?>, ИНН <?= $model->inn ?>, КПП <?= $model->ogrn ?>, <?= $model->address ?>,
                    Телефон <?= $model->phone ?>
                </strong>
            </td>
        </tr>
    </table>
    <table class="buy" width="800">
        <thead>
        <tr>
            <th>№</th>
            <th>Товары (работы, услуги)</th>
            <th>Кол-во</th>
            <th>Ед.</th>
            <th>Цена</th>
            <th>Сумма</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($payment->type !== Payment::TYPE_ACCOUNT_REPLENISHMENT) { ?>
            <?php $i = 1;
            foreach ($payment->orders as $key => $order) { ?>
                <tr>
                    <td class="align-center"><?= ($i) ?></td>
                    <td><?= $order->product ? Html::encode($order->product->getLabel()) : '<span style="color: red">' . Yii::t('app', 'Service not found') . '</span>' ?></td>
                    <td class="align-center"><?= $order->orderProduct->quantity ?></td>
                    <td class="align-center">шт</td>
                    <td class="align-right"><?= CurrencyHelper::convert($order->orderProduct->currency_code, 'RUB', $order->orderProduct->price * $order->orderProduct->quantity) . '.00' ?></td>
                    <td class="align-right"><?= CurrencyHelper::convert($order->orderProduct->currency_code, 'RUB', $order->orderProduct->price * $order->orderProduct->quantity) . '.00' ?></td>
                </tr>
                <?php $i++ ?>
                <?php if (!empty($order->orderProduct->extra)) { ?>
                    <?php foreach ($order->orderProduct->extra as $extra) { ?>
                        <tr>
                            <td class="align-center"><?= $i ?></td>
                            <td><?= Html::encode($extra->title) ?></td>
                            <td class="align-center"><?= $extra->quantity ?></td>
                            <td class="align-center">шт</td>
                            <td class="align-right"><?= CurrencyHelper::convert($extra->currency_code, 'RUB', $extra->price * $extra->quantity) . '.00' ?></td>
                            <td class="align-right"><?= CurrencyHelper::convert($extra->currency_code, 'RUB', $extra->price * $extra->quantity) . '.00' ?></td>
                        </tr>
                        <?php $i++;
                    } ?>
                <?php } ?>
            <?php } ?>
        <?php } else {
            $i = 2; ?>
            <tr>
                <td class="align-center">1</td>
                <td>Пополнение счета на uJobs</td>
                <td class="align-center">1</td>
                <td class="align-center">шт</td>
                <td class="align-right"><?= $convertedTotal?></td>
                <td class="align-right"><?= $convertedTotal?></td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="6" style="border: none;"></td>
        </tr>
        <tr>
            <td colspan="5" style="border: none;" class="align-right"><strong>Итого:</strong></td>
            <td style="border: none;" class="align-right"><strong><?= $convertedTotal ?></strong></td>
        </tr>
        <tr>
            <td colspan="5" style="border: none;" class="align-right"><strong>В том числе НДС:</strong></td>
            <td style="border: none;" class="align-right"><strong>0.00</strong></td>
        </tr>
        <tr>
            <td colspan="5" style="border: none;" class="align-right"><strong>Всего к оплате:</strong></td>
            <td style="border: none;" class="align-right"><strong><?= $convertedTotal ?></strong></td>
        </tr>
    </table>
    <p style="font-size: 12px;line-height: 1.5em;">
        Всего наименований <?= ($i - 1) ?>, на сумму <?= $convertedFormattedTotal ?>
    </p>
    <p style="font-size: 12px;line-height: 1.5em;">
        <strong><?= Yii::$app->utility->num2str(CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total, false, 2)) ?></strong>
    </p>
    <hr>
    <table>
        <tr>
            <td width="120" style="padding: 0 0 10px 0; vertical-align: bottom;"><strong>Руководитель</strong></td>
            <td width="580">
                <table class="subscribe" width="580">
                    <tr>
                        <td width="150" style="border-bottom: 1px solid #333;position: relative;">
                            <img src="/images/new/subscribe.png" alt="subscribe"
                                 style="text-align:center;margin:-60px 0 -40px 60px;width: 60px; display: block;">
                        </td>
                        <td width="100" class="align-right"
                            style="border-bottom: 1px solid #333;position: relative; margin:-60px 0 0 0;vertical-align: bottom;white-space: nowrap;">
                            Кашин С. Г.
                        </td>
                        <td width="200" style="">
                            <img src="/images/new/pechat.png" alt="pechat"
                                 style="margin: -60px 0 -40px 0; width: 130px; display: block;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
