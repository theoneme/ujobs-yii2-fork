<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.03.2017
 * Time: 18:26
 */

use common\components\CurrencyHelper;
use common\models\Payment;
use frontend\models\InvoiceForm;
use yii\helpers\Html;

/* @var Payment $payment */
/* @var InvoiceForm $model */

$convertedTotal = CurrencyHelper::convertAndFormat($payment->currency_code, 'RUB', $payment->total);
?>

<link href="/css/new/pdf-style.css" rel="stylesheet">
<table class="phys" width="900" style="font-family: Calibri,sans-serif; overflow: wrap">
<tr>
    <td class="column-type"  width="200">
        <table width="200">
            <tr>
                <td style="font-size: 18px" align="center"><strong>Извещение</strong></td>
            </tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr>
                <td style="font-size: 18px" align="center"><strong>Кассир</strong></td>
            </tr>
        </table>
    </td>
    <td width="700" style="padding: 10px;">
        <table  width="700" class="kvit-row">
            <tr><td class="form-name" width="700">Форма № ПД-4</td></tr>
            <tr><td class="field-underline" width="700" style="border-bottom:1px solid #333;text-align:center;font-size: 13px;">ООО “ЮДЖОБС”, счет #<?=$model->orderId?></td></tr>
            <tr>
                <td class="kvit-description align-center" style="font-size: 11px">(название получателя платежа)</td>
            </tr>
        </table>
        <table class="kvit-row">
            <tr>
                <td width="200">
                    <table class="cells">
                        <tr>
                            <td style="font-size: 13px">6</td><td style="font-size: 13px">6</td><td style="font-size: 13px">5</td><td style="font-size: 13px">8</td><td style="font-size: 13px">4</td><td style="font-size: 13px">9</td><td style="font-size: 13px">6</td><td style="font-size: 13px">2</td><td style="font-size: 13px">2</td><td style="font-size: 13px">0</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="kvit-description" style="font-size: 11px">(ИНН получателя платежа)</td>
                        </tr>
                    </table>
                </td>
                <td width="200">
                    <table class="cells">
                        <tr>
                            <?php foreach(str_split($model->ogrn) as $digit) { ?>
                                <td style="font-size: 13px"><?= $digit?></td>
                            <?php } ?>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="kvit-description" style="font-size: 11px; text-align: center;">(КПП)</td>
                        </tr>
                    </table>
                </td>
                <td class="align-right" width="300">
                    <table class="cells">
                        <tr>
                            <td style="font-size: 13px">4</td><td style="font-size: 13px">0</td><td style="font-size: 13px">7</td><td style="font-size: 13px">0</td><td style="font-size: 13px">2</td><td style="font-size: 13px">8</td><td style="font-size: 13px">1</td><td style="font-size: 13px">0</td><td style="font-size: 13px">8</td><td style="font-size: 13px">1</td>
                            <td style="font-size: 13px">6</td><td style="font-size: 13px">5</td><td style="font-size: 13px">4</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">3</td><td style="font-size: 13px">4</td><td style="font-size: 13px">3</td><td style="font-size: 13px">7</td><td style="font-size: 13px">4</td>
                        </tr>
                    </table>
                    <table width="300">
                        <tr>
                            <td class="kvit-description align-right" style="font-size: 11px">(номер счёта получателя платежа)</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td align="top" width="450">
                    <table width="450">
                        <tr>
                            <td style="font-size: 13px">В</td>
                            <td width="425" class="field-underline" style="border-bottom:1px solid #333;text-align:center;font-size: 13px"> Уральский банк ПАО Сбербанк</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="kvit-description align-center" style="font-size: 11px">
                                (наименование банка получателя платежа)
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="top" width="250">
                    <table width="250">
                        <tr>
                            <td style="padding-left: 40px;">БИК</td>
                            <td class="align-right">
                                <table class="cells">
                                    <tr>
                                        <td style="font-size: 13px">0</td><td style="font-size: 13px">4</td><td style="font-size: 13px">6</td><td style="font-size: 13px">5</td><td style="font-size: 13px">7</td><td style="font-size: 13px">7</td><td style="font-size: 13px">6</td><td style="font-size: 13px">7</td><td style="font-size: 13px">4</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td width="300" style="font-size: 13px">Номер кор./сч. банка получателя платежа</td>
                <td width="400" class="align-right">
                    <table class="cells">
                        <tr style="font-size: 13px">
                            <td style="font-size: 13px">3</td><td style="font-size: 13px">0</td><td style="font-size: 13px">1</td><td style="font-size: 13px">0</td><td style="font-size: 13px">1</td><td style="font-size: 13px">8</td><td style="font-size: 13px">1</td><td style="font-size: 13px">0</td><td style="font-size: 13px">5</td><td style="font-size: 13px">0</td>
                            <td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">6</td><td style="font-size: 13px">7</td><td style="font-size: 13px">4</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td width="150" style="font-size: 13px">Ф.И.О. плательщика</td>
                <td width="550" class="field-underline" style="border-bottom:1px solid #333;font-size: 13px;"><?= "{$model->lastName} {$model->firstName}"?>
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td width="150" style="font-size: 13px">Адрес плательщика</td>
                <td width="550" class="field-underline" style="border-bottom:1px solid #333;font-size:13px;"><?= $model->address?>
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td width="300" style="font-size: 13px">Сумма платежа <?= $convertedTotal ?> 00 коп.</td>
                <td width="400" style="font-size: 13px" class="align-right">
                    Сумма платы за услуги <?= $convertedTotal ?> 00 коп.
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td width="300" style="font-size: 13px">Итого <?= $convertedTotal ?> 00 коп.</td>
                <td width="400" class="align-right" style="font-size: 13px">
                    « <?= $dateParts[0] ?> » <?=$dateParts[1]?> <?=$dateParts[2]?> г.
                </td>
            </tr>
        </table>

        <table class="kvit-row">
            <tr>
                <td width="600" class="kvit-description">
                    С условиями приёма указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы
                    за услуги банка ознакомелен и согласен.
                </td>
            </tr>
            <tr>
                <td width="600" style="padding: 0 0 5px 0; text-align: right; font-size: 13px; float: right;">
                    <strong>Подпись плательщика ____________</strong>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td class="column-type"  width="200">
        <table width="200">
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr>
                <td style="font-size: 18px" align="center"><strong>Квитанция</strong></td>
            </tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr><td style="font-size: 13px"></td></tr>
            <tr>
                <td style="font-size: 18px" align="center"><strong>Кассир</strong></td>
            </tr>
        </table>
    </td>
    <td style="padding: 10px;">
        <table class="kvit-row">
            <tr><td class="field-underline" width="700" style="border-bottom:1px solid #333;text-align:center;font-size: 13px;">ООО “ЮДЖОБС”, счет #<?=$model->orderId?></td></tr>
            <tr>
                <td class="kvit-description align-center" style="font-size: 11px">(название получателя платежа)</td>
            </tr>
        </table>
    <table class="kvit-row">
        <tr>
            <td width="200">
                <table class="cells">
                    <tr>
                        <td style="font-size: 13px">6</td><td style="font-size: 13px">6</td><td style="font-size: 13px">5</td><td style="font-size: 13px">8</td><td style="font-size: 13px">4</td><td style="font-size: 13px">9</td><td style="font-size: 13px">6</td><td style="font-size: 13px">2</td><td style="font-size: 13px">2</td><td style="font-size: 13px">0</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="kvit-description" style="font-size: 11px">(ИНН получателя платежа)</td>
                    </tr>
                </table>
            </td>
            <td width="200">
                <table class="cells">
                    <tr>
                        <?php foreach(str_split($model->ogrn) as $digit) { ?>
                            <td style="font-size: 13px"><?= $digit?></td>
                        <?php } ?>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="kvit-description" style="font-size: 11px; text-align: center">(КПП)</td>
                    </tr>
                </table>
            </td>
            <td class="align-right" width="300">
                <table class="cells">
                    <tr>
                        <td style="font-size: 13px">4</td><td style="font-size: 13px">0</td><td style="font-size: 13px">7</td><td style="font-size: 13px">0</td><td style="font-size: 13px">2</td><td style="font-size: 13px">8</td><td style="font-size: 13px">1</td><td style="font-size: 13px">0</td><td style="font-size: 13px">8</td><td style="font-size: 13px">1</td>
                        <td style="font-size: 13px">6</td><td style="font-size: 13px">5</td><td style="font-size: 13px">4</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">3</td><td style="font-size: 13px">4</td><td style="font-size: 13px">3</td><td style="font-size: 13px">7</td><td style="font-size: 13px">4</td>
                    </tr>
                </table>
                <table width="300">
                    <tr>
                        <td class="kvit-description align-right" style="font-size: 11px">(номер счёта получателя платежа)</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td align="top" width="450">
                <table width="450">
                    <tr>
                        <td style="font-size: 13px">В</td>
                        <td width="425" class="field-underline" style="border-bottom:1px solid #333;text-align:center;font-size: 13px"> Уральский банк ПАО Сбербанк</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="kvit-description align-center" style="font-size: 11px">
                            (наименование банка получателя платежа)
                        </td>
                    </tr>
                </table>
            </td>
            <td align="top" width="250">
                <table width="250">
                    <tr>
                        <td style="padding-left: 40px;">БИК</td>
                        <td class="align-right">
                            <table class="cells">
                                <tr>
                                    <td style="font-size: 13px">0</td><td style="font-size: 13px">4</td><td style="font-size: 13px">6</td><td style="font-size: 13px">5</td><td style="font-size: 13px">7</td><td style="font-size: 13px">7</td><td style="font-size: 13px">6</td><td style="font-size: 13px">7</td><td style="font-size: 13px">4</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td width="300" style="font-size: 13px">Номер кор./сч. банка получателя платежа</td>
            <td width="400" class="align-right">
                <table class="cells">
                    <tr style="font-size: 13px">
                        <td style="font-size: 13px">3</td><td style="font-size: 13px">0</td><td style="font-size: 13px">1</td><td style="font-size: 13px">0</td><td style="font-size: 13px">1</td><td style="font-size: 13px">8</td><td style="font-size: 13px">1</td><td style="font-size: 13px">0</td><td style="font-size: 13px">5</td><td style="font-size: 13px">0</td>
                        <td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">0</td><td style="font-size: 13px">6</td><td style="font-size: 13px">7</td><td style="font-size: 13px">4</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td width="150" style="font-size: 13px">Ф.И.О. плательщика</td>
            <td width="550" class="field-underline" style="border-bottom:1px solid #333;font-size: 13px;"><?= "{$model->lastName} {$model->firstName}"?>
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td width="150" style="font-size: 13px">Адрес плательщика</td>
            <td width="550" class="field-underline" style="border-bottom:1px solid #333;font-size:13px;"><?= $model->address?>
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td width="300" style="font-size: 13px">Сумма платежа <?= $convertedTotal ?> 00 коп.</td>
            <td width="400" style="font-size: 13px" class="align-right">
                Сумма платы за услуги <?= $convertedTotal ?> 00 коп.
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td width="300" style="font-size: 13px">Итого <?= $convertedTotal ?> 00 коп.</td>
            <td width="400" class="align-right" style="font-size: 13px">
                « <?= $dateParts[0] ?> » <?=$dateParts[1]?> <?=$dateParts[2]?> г.
            </td>
        </tr>
    </table>

    <table class="kvit-row">
        <tr>
            <td width="600" class="kvit-description">
                С условиями приёма указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы
                за услуги банка ознакомелен и согласен.
            </td>
        </tr>
        <tr>
            <td width="600" style="padding: 0 0 5px 0; text-align: right; font-size: 13px; float: right;">
                <strong>Подпись плательщика ____________</strong>
            </td>
        </tr>
    </table>
    </td>
</tr>
</table>