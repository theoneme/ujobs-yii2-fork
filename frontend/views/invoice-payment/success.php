<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.03.2017
 * Time: 18:29
 */

use common\components\CurrencyHelper;
use common\models\Invoice;
use common\modules\store\assets\StoreAsset;
use common\modules\store\components\Cart;
use frontend\assets\CatalogAsset;
use frontend\assets\FairPlayAsset;
use frontend\models\EmailInvoiceForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


FairPlayAsset::register($this);
StoreAsset::register($this);
CatalogAsset::register($this);

/* @var Invoice $invoice */
/* @var EmailInvoiceForm $emailInvoiceForm */

$this->title = Yii::t('order', 'Invoice #{number} is created', ['number' => $invoice->id]);

?>

    <div class="container-fluid">
        <div class="cart-box row">
            <div class="cart-left col-md-8 col-sm-8 col-xs-12">
                <div class="cart-header-block"><?= Yii::t('order', 'Invoice #{number}', ['number' => $invoice->id]) ?></div>
                <div class="payment-options cart-tutorial">
                    <p><?= Yii::t('order', 'Your invoice is created. You can download it, or send to your email') ?></p>

                    <div class="clearfix">
                        <p>
                            <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i> ' . Yii::t('order', 'Invoice #{number}', ['number' => $invoice->id]),
                                Url::to($invoice->invoice_file, true), [
                                    'download' => true,
                                ]) ?>
                        </p>
                        <div>
                            <h2 class="text-center"><?= Yii::t('order', 'Send invoice to this Email') ?></h2>
                            <?php $form = ActiveForm::begin([
                                'type' => ActiveForm::TYPE_INLINE,
                                'id' => 'email-invoice-form',
                                'action' => Url::to(['/invoice-payment/send-invoice'])
                            ]); ?>
                            <?= $form->field($emailInvoiceForm, 'email') ?>
                            <?= $form->field($emailInvoiceForm, 'invoiceId')->hiddenInput(['value' => $invoice->id])->label(false); ?>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('order', 'Send invoice to this Email'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <p>&nbsp;</p>
            </div>
            <div class="cart-right col-md-4 col-sm-4 col-xs-12">
                <div class="total-box">
                    <div class="price-line">
                        <div class="price-total-title text-left">
                            <?= Yii::t('model', 'Summary') ?>
                        </div>
                        <div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT)) ?>
                        </div>
                    </div>
                    <div class="price-line total-all">
                        <div class="price-total-title text-left">
                            <?= Yii::t('model', 'Total') ?>
                        </div>
                        <div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT)) ?>
                        </div>
                    </div>
                </div>
                <div class="cart-fair-play">
                    <?= Html::a(Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
                        ['/page/static', 'alias' => 'fair-play'],
                        [
                            'data-pjax' => 0,
                            'class' => 'image-container'
                        ]
                    ) ?>
                    <div class="post-mes" style="margin-top: 20px;">
                        <?= Html::a(Yii::t('app', 'We guarantee the execution of each order or we will refund your money back'),
                            ['/page/static', 'alias' => 'fair-play'],
                            [
                                'data-pjax' => 0,
                                'class' => 'message-container'
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->registerJs('
    $(document).on("submit", "#email-invoice-form", function(e) {
        e.preventDefault();
        var action = $(this).attr("action");
        var data = $(this).serialize();

        $.ajax({
            url: action,
            data: data,
            type: "post",
            success: function(response) {
                if (response.success == true) {
                    $("#email-invoice-form").hide();
                    alertCall("top", "success", response.message);
                } else {
                    alertCall("top", "warning", response.message);
                }
            }
        });
    });
');