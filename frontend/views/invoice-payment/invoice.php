<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.03.2017
 * Time: 17:39
 */
use borales\extensions\phoneInput\PhoneInput;
use common\components\CurrencyHelper;
use common\models\Payment;
use common\modules\store\assets\StoreAsset;
use common\modules\store\components\Cart;
use common\modules\store\Module;
use frontend\assets\CatalogAsset;
use frontend\assets\FairPlayAsset;
use frontend\models\InvoiceForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

FairPlayAsset::register($this);
StoreAsset::register($this);
CatalogAsset::register($this);

/* @var InvoiceForm $individualInvoiceForm */
/* @var InvoiceForm $legalEntityInvoiceForm */
/* @var Payment $payment */

$this->title = Yii::t('order', 'Create invoice');

?>

	<div class="container-fluid">
		<div class="cart-box row">
			<div class="cart-left col-md-8 col-sm-8 col-xs-12">

				<div class="cart-header-block"><?= Yii::t('order', 'Create invoice') ?></div>
				<div class="payment-options cart-tutorial">
					<?= Html::radioList('decision', InvoiceForm::SCENARIO_LEGAL_ENTITY, [
						InvoiceForm::SCENARIO_LEGAL_ENTITY => Yii::t('order', 'Legal entity'),
						InvoiceForm::SCENARIO_INDIVIDUAL => Yii::t('order', 'Individual'),
					], [
						'class' => 'checkbox-enchance text-left',
						'id' => 'invoice-decision',
						'item' => function ($index, $label, $name, $checked, $value) {
							$chk = $checked ? 'checked' : '';
							$output = "<div class='radio'><input {$chk} value=\"{$value}\" id=\"payment_{$index}\" type='radio' name=\"{$name}\"/><label for=\"payment_{$index}\">{$label}</label></div>";
							return $output;
						}
					]); ?>

					<div id="individual" class="invoice-decision hidden">
						<h2 class="text-center"><?= Yii::t('order', 'Individual') ?></h2>
						<?php $form = ActiveForm::begin([
							'options' => [
								'class' => 'formpost'
							],
							'id' => 'individual-form',
							'action' => Url::to(['/invoice-payment/process'])
						]); ?>
						<div class="row">
							<div class="col-xs-4">
								<?= $form->field($individualInvoiceForm, 'lastName')->textInput() ?>
							</div>
							<div class="col-xs-4">
								<?= $form->field($individualInvoiceForm, 'firstName')->textInput() ?>
							</div>
							<div class="col-xs-4">
								<?= $form->field($individualInvoiceForm, 'middleName')->textInput() ?>
							</div>
						</div>
						<?= $form->field($individualInvoiceForm, 'ogrn')->textInput() ?>
						<?= $form->field($individualInvoiceForm, 'phone')->widget(PhoneInput::class, [
							'jsOptions' => [
								'preferredCountries' => ['ru', 'ua'],
							],
							'defaultOptions' => ['id' => 'phone2'],
							'options' => ['id' => 'phone2']
						]); ?>
						<?= $form->field($individualInvoiceForm, 'address')->textInput() ?>
						<?= $form->field($individualInvoiceForm, 'decision')->hiddenInput(['value' => InvoiceForm::SCENARIO_INDIVIDUAL])->label(false) ?>
						<?= $form->field($individualInvoiceForm, 'orderId')->hiddenInput()->label(false) ?>
						<div class="text-right">
							<?= Html::submitButton(Yii::t('app', 'Create Invoice'), ['class' => 'btn-mid']) ?>
						</div>
						<p>&nbsp;</p>
						<?php ActiveForm::end() ?>
					</div>

					<div id="legal-entity" class="invoice-decision">
						<h2 class="text-center"><?= Yii::t('order', 'Legal entity') ?></h2>
						<?php $form = ActiveForm::begin([
							'options' => [
								'class' => 'formpost'
							],
							'id' => 'legal-entity-form',
							'action' => Url::to(['/invoice-payment/process'])
						]); ?>

						<?= $form->field($legalEntityInvoiceForm, 'companyName')->textInput() ?>
						<div class="row">
							<div class="col-xs-6">
								<?= $form->field($legalEntityInvoiceForm, 'inn')->textInput() ?>
							</div>
							<div class="col-xs-6">
								<?= $form->field($legalEntityInvoiceForm, 'ogrn')->textInput() ?>
							</div>
						</div>
						<?= $form->field($legalEntityInvoiceForm, 'phone')->widget(PhoneInput::class, [
							'jsOptions' => [
								'preferredCountries' => ['ru', 'ua'],
							],
						]); ?>
						<?= $form->field($legalEntityInvoiceForm, 'address')->textInput() ?>
						<?= $form->field($legalEntityInvoiceForm, 'decision')->hiddenInput(['value' => InvoiceForm::SCENARIO_LEGAL_ENTITY])->label(false) ?>
						<?= $form->field($legalEntityInvoiceForm, 'orderId')->hiddenInput()->label(false) ?>
						<div class="create-gig-block text-right">
							<?= Html::submitButton(Yii::t('order', 'Create Invoice'), ['class' => 'btn-mid']) ?>
						</div>
						<p>&nbsp;</p>
						<?php ActiveForm::end() ?>
					</div>
				</div>
			</div>
			<div class="cart-right col-md-4 col-sm-4 col-xs-12">
				<div class="total-box">
					<div class="price-line">
						<div class="price-total-title text-left">
							<?= Yii::t('model', 'Summary') ?>
						</div>
						<div class="price-total-sum text-right">
                            <?= CurrencyHelper::convertAndFormat($payment->currency_code, Yii::$app->params['app_currency_code'], $payment->total)?>
						</div>
					</div>
					<div class="price-line total-all">
						<div class="price-total-title text-left">
							<?= Yii::t('model', 'Total') ?>
						</div>
						<div class="price-total-sum text-right">
                            <?= CurrencyHelper::convertAndFormat($payment->currency_code, Yii::$app->params['app_currency_code'], $payment->total)?>
						</div>
					</div>
				</div>
				<div class="cart-fair-play">
					<?= Html::a(Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
						['/page/static', 'alias' => 'fair-play'],
						[
							'data-pjax' => 0,
							'class' => 'image-container'
						]
					) ?>
					<div class="post-mes" style="margin-top: 20px;">
						<?= Html::a(Yii::t('app', 'We guarantee the execution of each order or we will refund your money back'),
							['/page/static', 'alias' => 'fair-play'],
							[
								'data-pjax' => 0,
								'class' => 'message-container'
							]
						) ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->registerJs('
    var decisionLegal = ' . InvoiceForm::SCENARIO_LEGAL_ENTITY . ';
    var decisionIndividual = ' . InvoiceForm::SCENARIO_INDIVIDUAL . ';

    $("#invoice-decision").on("change", function() {
        var value = $("#invoice-decision input:checked").val();

        if(value == decisionLegal) {
            $(".invoice-decision").addClass("hidden");
            $("#legal-entity").removeClass("hidden");
        } else {
            $(".invoice-decision").addClass("hidden");
            $("#individual").removeClass("hidden");
        }
    });
');