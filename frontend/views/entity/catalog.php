<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.08.2017
 * Time: 13:55
 */

use common\models\Category;
use common\modules\board\assets\BoardAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/* @var $this View */
/* @var $seo array */
/* @var $rootJobCategories Category[] */
/* @var $rootProductCategories Category[] */
/* @var $latestServicesTendersProvider ActiveDataProvider */
/* @var $latestProductsTendersProvider ActiveDataProvider */

BoardAsset::register($this);

?>

    <div class="how-works text-center hidden-xs">
        <a class="close-how" href=""></a>

        <h2 class="text-center"><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
        <div class="three-steps text-center">
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/tenders/screen1.png"), [
                        'alt' => Yii::t('board', 'Make choice'),'title' => Yii::t('board', 'Make choice')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Make choice') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn1'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/tenders/screen2.png"), [
                        'alt' => Yii::t('board', 'Pay deposit'),'title' => Yii::t('board', 'Pay deposit')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Pay deposit') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn2'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws("/images/new/catalog/" . Yii::$app->language . "/tenders/screen3.png"), [
                        'alt' => Yii::t('board', 'Get your product'),'title' => Yii::t('board', 'Get your product')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Get your product') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn3'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1 class="text-center"><?= $seo['h1'] ?></h1>
            <div class="banner-prod-descr text-center">
                <?= $seo['subhead'] ?>
            </div>
            <a class="product-how-works hidden-xs" href="#"><?= Yii::t('board', 'How the service works'); ?></a>
        </div>
    </div>

    <div class="container-fluid">
        <div class="visible-xs">
            <div class="subcats-mobile-block">
                <div>
                    <div class="text-chose-subc text-left visible-xs "><?= Yii::t('app', 'Job categories') ?></div>
                    <div class="ml hide-filter">
                        <?php foreach ($rootJobCategories as $key => $category) { ?>
                            <?= Html::a($category->translations[Yii::$app->language]->title . ($category->relatedCount ? " ({$category->relatedCount})" : ''), ['/category/tenders', 'category_1' => $category->translations[Yii::$app->language]->slug]); ?>
                        <?php } ?>
                    </div>
                    <?php if (count($rootJobCategories) > 4) {
                        echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootJobCategories) - 4)]), null, [
                            'class' => 'more-less more-cats',
                            'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootJobCategories) - 4)]),
                            'data-less' => Yii::t('app', 'See less'),
                        ]) ?>
                    <?php } ?>
                </div>
                <div>
                    <div class="text-chose-subc text-left visible-xs "><?= Yii::t('app', 'Product categories') ?></div>
                    <div class="ml hide-filter">
                        <?php foreach ($rootProductCategories as $key => $category) { ?>
                            <?= Html::a($category->translations[Yii::$app->language]->title . ($category->relatedCount ? " ({$category->relatedCount})" : ''), ['/category/tenders', 'category_1' => $category->translations[Yii::$app->language]->slug]); ?>
                        <?php } ?>
                    </div>
                    <?php if (count($rootProductCategories) > 4) {
                        echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootProductCategories) - 4)]), null, [
                            'class' => 'more-less more-cats',
                            'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootProductCategories) - 4)]),
                            'data-less' => Yii::t('app', 'See less'),
                        ]) ?>
                    <?php } ?>
                </div>
            </div>
            <br>
            <div class="ws-title"><?= Yii::t('app', 'Latest service requests') ?></div>
            <?php echo ListView::widget([
                'dataProvider' => $latestServicesTendersProvider,
                'id' => 'jobs-mobile',
                'itemView' => '@frontend/modules/filter/views/job-list-mobile',
                'layout' => "<div class='clearfix'>{items}</div>{pager}",
                'options' => [
                    'class' => ''
                ],
                'itemOptions' => [
                    'class' => 'mob-work-item'
                ],
                'pager' => [
                    'nextPageLabel' => '→',
                    'prevPageLabel' => '←',
                    'maxButtonCount' => 5,
                ],
                'emptyText' =>
                    '<p class="catalog-empty-text">' .
                    Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => Yii::t('app', 'Catalog')]) .
                    '</p>',
            ]); ?>

            <div class="load-button-place text-center">
                <?= Html::a(Yii::t('app', 'Show more requests'), ['/category/tender-catalog'], ['class' => 'load', 'id' => 'more-service-tenders']) ?>
            </div>

            <br>

            <div class="ws-title"><?= Yii::t('app', 'Latest product requests') ?></div>

            <?php echo ListView::widget([
                'dataProvider' => $latestProductsTendersProvider,
                'id' => 'jobs-mobile',
                'itemView' => '@frontend/modules/filter/views/product-list-mobile',
                'layout' => "<div class='clearfix'>{items}</div>{pager}",
                'options' => [
                    'class' => ''
                ],
                'itemOptions' => [
                    'class' => 'mob-work-item'
                ],
                'pager' => [
                    'nextPageLabel' => '→',
                    'prevPageLabel' => '←',
                    'maxButtonCount' => 5,
                ],
                'emptyText' =>
                    '<p class="catalog-empty-text">' .
                    Yii::t('app', 'Nothing was found for "{request}", try to rephrase the request, check the keyboard layout and start the search again.', ['request' => Yii::t('app', 'Catalog')]) .
                    '</p>',
            ]); ?>

            <div class="load-button-place text-center">
                <?= Html::a(Yii::t('app', 'Show more requests'), ['/board/category/ptenders'], ['class' => 'load', 'id' => 'more-product-tenders']) ?>
            </div>
            <br>
        </div>
        <div class="category-main row hidden-xs">
            <aside>
                <div class="aside-body">
                    <div class="aside-block">
                        <ul class="cats no-markers">
                            <li>
                                <h2 class="text-left"><?= Yii::t('app', 'Job categories') ?></h2>
                                <div class="ml <?= (count($rootJobCategories) > 6) ? "hide-filter hf-categories" : "" ?>">
                                    <ul>
                                        <?php foreach ($rootJobCategories as $category) { ?>
                                            <li class="">
                                                <?= Html::a($category->translations[Yii::$app->language]->title . ($category->relatedCount > 0 ? " ($category->relatedCount)" : ''), ['/category/tenders', 'category_1' => $category->translations[Yii::$app->language]->slug]); ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php if (count($rootJobCategories) > 6) {
                                    echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootJobCategories) - 6)]), null, [
                                        'class' => 'more-less more-cats',
                                        'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootJobCategories) - 6)]),
                                        'data-less' => Yii::t('app', 'See less'),
                                    ]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <div class="aside-block">
                        <ul class="cats no-markers">
                            <li>
                                <h2 class="text-left"><?= Yii::t('app', 'Product categories') ?></h2>
                                <div class="ml <?= (count($rootProductCategories) > 6) ? "hide-filter hf-categories" : "" ?>">
                                    <ul>
                                        <?php foreach ($rootProductCategories as $category) { ?>
                                            <li class="">
                                                <?= Html::a($category->translations[Yii::$app->language]->title . ($category->relatedCount > 0 ? " ($category->relatedCount)" : ''), ['/board/category/ptenders', 'category_1' => $category->translations[Yii::$app->language]->slug]); ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php if (count($rootProductCategories) > 6) {
                                    echo Html::a(Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootProductCategories) - 6)]), null, [
                                        'class' => 'more-less more-cats',
                                        'data-more' => Yii::t('app', 'Show {count, plural, one{# more category} other{# more categories}}', ['count' => (count($rootProductCategories) - 6)]),
                                        'data-less' => Yii::t('app', 'See less'),
                                    ]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <div class="prof-aside-block aside-post text-center">
                        <?= Html::a(Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
                            ['/page/static', 'alias' => 'fair-play'],
                            ['data-pjax' => 0]
                        ) ?>
                        <div class="post-mes" style="margin-top: 20px;">
                            <?= Html::a(Yii::t('board', 'We guarantee the execution of each order or we will refund your money back'),
                                ['/page/static', 'alias' => 'fair-play'],
                                ['data-pjax' => 0, 'style' => 'color: #000;']
                            ) ?>
                        </div>
                    </div>
                </div>
            </aside>
            <div class="cat-list">
                <br>
                <div class="ws-title"><?= Yii::t('app', 'Latest service requests') ?></div>

                <?php echo ListView::widget([
                    'dataProvider' => $latestServicesTendersProvider,
                    'itemView' => '@frontend/modules/filter/views/job-list',
                    'layout' => "{items}{pager}",
                    'options' => [
                        'tag' => 'div',
                        'class' => 'block-flex'
                    ],
                    'itemOptions' => [
                        'tag' => 'div',
                        'class' => 'bf-item bf-item-video'
                    ],
                ]); ?>

                <div class="load-button-place text-center">
                    <?= Html::a(Yii::t('app', 'Show more requests'), ['/category/tender-catalog'], ['class' => 'load', 'id' => 'more-service-tenders']) ?>
                </div>
                <br>
                <br>

                <div class="ws-title"><?= Yii::t('app', 'Latest product requests') ?></div>

                <?php echo ListView::widget([
                    'dataProvider' => $latestProductsTendersProvider,
                    'itemView' => '@frontend/modules/filter/views/product-list',
                    'layout' => "{items}{pager}",
                    'options' => [
                        'tag' => 'div',
                        'class' => 'block-flex'
                    ],
                    'itemOptions' => [
                        'tag' => 'div',
                        'class' => 'bf-item bf-item-video'
                    ],
                ]); ?>

                <div class="load-button-place text-center">
                    <?= Html::a(Yii::t('app', 'Show more requests'), ['/board/category/ptenders'], ['class' => 'load', 'id' => 'more-product-tenders']) ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->registerJs("
    $('body').on('click', '.product-how-works', function(e) {
        $('.how-works').slideToggle();
    });
");