<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.08.2017
 * Time: 11:26
 */

use frontend\assets\AccountAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $locales array
 * @var $selected string
 */

AccountAsset::register($this);
$this->title = Yii::t('app', 'Create Announcement');

?>
    <div class="profile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?= Html::beginForm(['/job/create'], 'post', ['id' => 'global-locale-form']) ?>
                        <div class="tab-content nostyle-tab-content">
                            <div class="settings-content step tab-pane fade in active">
                                <div class="formgig-block" style="padding-top: 20px;">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12 leftset">
                                            <div class="form-group">
                                                <div class="set-title">
                                                    <label>
                                                        <?= Yii::t('app', 'What announcement do you want to post?') ?>
                                                    </label>
                                                </div>
                                                <?= Yii::t('app', 'You can post any number of different announcements.')?>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 rightset big-checkbox-container">
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'job', [
                                                    'value' => 'job',
                                                    'id' => 'job',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/job/create'])
                                                ]) .
                                                Html::label(Yii::t('app', 'Post a service ready on command, I am a service provider.'), 'job')
                                                ?>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'product', [
                                                    'value' => 'product',
                                                    'id' => 'product',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/board/product/create'])
                                                ]) .
                                                Html::label(Yii::t('app', 'Post announcement of product (s) I have for sale.'), 'product')
                                                ?>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'tender-service', [
                                                    'value' => 'tender-service',
                                                    'id' => 'tender-service',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/tender/create'])
                                                ]) .
                                                Html::label(Yii::t('app', 'Service (s) I like to order.'), 'tender-service')
                                                ?>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'tender-product', [
                                                    'value' => 'tender-product',
                                                    'id' => 'tender-product',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/board/tender/create'])
                                                ]) .
                                                Html::label(Yii::t('app', 'Buyer ready to purchase.'), 'tender-product')
                                                ?>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'portfolio', [
                                                    'value' => 'portfolio',
                                                    'id' => 'portfolio',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/account/portfolio/create'])
                                                ]) .
                                                Html::label(Yii::t('app', 'Create a portfolio. Post examples of your work.'), 'portfolio')
                                                ?>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'article', [
                                                    'value' => 'article',
                                                    'id' => 'article',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/article/form'])
                                                ]) .
                                                Html::label(Yii::t('app', 'Post an article. Publish the news or tell about your business.'), 'article')
                                                ?>
                                            </div>
                                            <div class="chover">
                                                <?= Html::radio('entity', $selected === 'video', [
                                                    'value' => 'video',
                                                    'id' => 'video',
                                                    'class' => 'radio-checkbox',
                                                    'data-action' => Url::to(['/video/create'])
                                                ]) .
                                                Html::label(Yii::t('app', 'I want to post a teaching video course.'), 'video')
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('app', 'Announcement type') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('app', 'In our exchange of services and products, you can post announcements about buying and selling products and services, as well as you can publish your articles or opinions and your portfolio. Besides find new clients and buyers, you can also publish the jobs, services or products that you wish to order.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="formgig-block" style="padding-top: 20px;">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12 leftset">
                                            <div class="form-group">
                                                <div class="set-title">
                                                    <label>
                                                        <?= Yii::t('app', 'Select the language in which you want to create announcement') ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6 rightset big-checkbox-container">
                                            <?php
                                            $i = 1;
                                            foreach ($locales as $locale => $title) {
                                                echo '<div class="chover">' .
                                                    Html::radio('locales[]', $locale == Yii::$app->language, ['value' => $locale, 'id' => $locale, 'class' => 'radio-checkbox']) .
                                                    Html::label(Yii::t('app', $title, [], $locale), $locale) .
                                                    '</div>';
                                                if ($i == round(count($locales) / 2)) {
                                                    echo '</div><div class="col-md-4 col-sm-4 col-xs-6 rightset big-checkbox-container">';
                                                }
                                                $i++;
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('app', 'You can select multiple languages') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('app', 'If you select more languages, more clients  in different countries will be able to see your offer and thus increase your profits. And if you want to order services or buy products you will have more options to choose from.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="formgig-block">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>

<?php
$script = <<<JS
    $('input[name="entity"]').change(function() {
        $('#global-locale-form').attr('action', $(this).data('action'));
    });
    $('input[name="entity"]:checked').trigger('change');
    $('#global-locale-form').on('submit', function() {
        if (['job', 'product', 'portfolio', 'article', 'video'].indexOf($('input[name="entity"]:checked').val()) === -1) {
            window.location.href = $(this).attr('action');
            return false;
        }
    });
JS;

$this->registerJs($script);
