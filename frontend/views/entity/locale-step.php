<?php

use frontend\assets\AccountAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $locales array
 */
AccountAsset::register($this);
$this->title = Yii::t('app', 'Create Job');
?>
<div class="profile">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?= Html::beginForm(['/job/create'], 'post', ['id' => 'job-locale-form']) ?>
                <div class="tab-content nostyle-tab-content">
                    <div class="settings-content step tab-pane fade in active">
                        <div class="formgig-block" style="padding-top: 20px;">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12 leftset">
                                    <div class="form-group">
                                        <div class="set-title">
                                            <label>
                                                <?= Yii::t('app', 'Please, select what do you want to post') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6 rightset big-checkbox-container">
                                    <div class="chover">
                                        <?= Html::radio('entity', true, ['value' => 'job', 'id' => 'job']) .
                                        Html::label(Yii::t('app', 'Service, that you are ready to do'), 'job')
                                        ?>
                                        <?= Html::radio('entity', false, ['value' => 'product', 'id' => 'product']) .
                                        Html::label(Yii::t('app', 'Announcement about selling product'), 'product')
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('app', 'Announcement type') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'Specify type on your announcement, is it announcement about selling product or announcement about selling service') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block" style="padding-top: 20px;">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12 leftset">
                                    <div class="form-group">
                                        <div class="set-title">
                                            <label>
                                                <?= Yii::t('app', 'Select the languages in which you want to create service or product announcements') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6 rightset">
                                    <?php
                                    $i = 1;
                                    foreach ($locales as $locale => $title) {
                                        echo '<div class="chover">' .
                                            Html::radio('locales[]', $locale == Yii::$app->language, ['value' => $locale, 'id' => $locale, 'class' => 'radio-checkbox']) .
                                            Html::label(Yii::t('app', $title), $locale) .
                                            '</div>';
                                        if ($i == round(count($locales) / 2)) {
                                            echo '</div><div class="col-md-4 col-sm-4 col-xs-6 rightset">';
                                        }
                                        $i++;
                                    } ?>
                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('app', 'You can select multiple languages') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'If you select more languages, more clients  in different countries will be able to see your offer and thus increase your profits. And if you want to order services or buy products you will have more options to choose from.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>

<?
$createJobRoute = Url::to(['/job/create']);
$createProductRoute = Url::to(['/board/product/create']);

$script = <<<JS
    $('input[name="entity"]').change(function() {
        let value = $(this).val(), 
            form = $('#job-locale-form');
        switch(value) {
            case 'product':
                form.attr('action', '$createProductRoute');
                break;
            case 'job':
                form.attr('action', '$createJobRoute');
                break;
        }
    });
JS;

$this->registerJs($script);
