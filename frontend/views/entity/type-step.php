<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.08.2017
 * Time: 13:19
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $locales array
 */
\frontend\assets\AccountAsset::register($this);
$this->title = Yii::t('app', 'Create Request');
?>
    <div class="profile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?= Html::beginForm(['/tender/create'], 'post', ['id' => 'job-locale-form']) ?>
                    <div class="tab-content nostyle-tab-content">
                        <div class="settings-content step tab-pane fade in active">
                            <div class="formgig-block" style="padding-top: 20px;">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12 leftset">
                                        <div class="form-group">
                                            <div class="set-title">
                                                <label>
                                                    <?= Yii::t('app', 'Please, select what do you want to buy') ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-8 rightset big-checkbox-container">
                                        <div class="chover">
                                            <?= Html::radio('entity', true, ['value' => 'service', 'id' => 'job']) .
                                            Html::label(Yii::t('app', 'Service (s) I like to order.'), 'job')
                                            ?>
                                            <?= Html::radio('entity', false, ['value' => 'product', 'id' => 'product']) .
                                            Html::label(Yii::t('app', 'Buyer ready to purchase.'), 'product')
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Announcement type') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Specify type on your announcement, is it announcement about buying product or service') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>

<?
$createJobRoute = Url::to(['/tender/create']);
$createProductRoute = Url::to(['/board/tender/create']);

$script = <<<JS
    $('input[name="entity"]').change(function() {
        let value = $(this).val(), 
            form = $('#job-locale-form');
        switch(value) {
            case 'product':
                form.attr('action', '$createProductRoute');
                break;
            case 'service':
                form.attr('action', '$createJobRoute');
                break;
        }
    });

    $('#job-locale-form').on('submit', function() {
        let address = $(this).attr('action');
        window.location.href = address;
        
        return false;
    });
JS;

$this->registerJs($script);
