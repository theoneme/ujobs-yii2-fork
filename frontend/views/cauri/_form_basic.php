<?php

use common\helpers\CauriHelper;
use frontend\assets\CardPaymentAsset;
use frontend\assets\JqueryMaskAsset;
use frontend\models\CauriForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

JqueryMaskAsset::register($this);
CardPaymentAsset::register($this);

/* @var CauriForm $model */
/* @var View $this */

$years = range(date('Y'), date('Y') + 50);
$years = array_combine($years, $years);
$months = range(1, 12);
$months = array_combine($months, $months);
?>

<?php $form = ActiveForm::begin([
    'id' => 'cauri-form',
    'action' => Url::to(['/payment/cauri-process']),
    'options' => ['class' => 'card-payment-form-container text-left card-payment-standalone'],
]); ?>
    <div class="form-group field-cauriform-cardnumber required">
        <?= Html::label(Yii::t('model', 'Card Number'))?>
        <div class="payment-method-icons">
            <?= Html::img('/images/new/payment/visa.png')?>
            <?= Html::img('/images/new/payment/mastercard.png')?>
            <?= Html::img('/images/new/payment/american.png')?>
            <?= Html::img('/images/new/payment/diners.png')?>
        </div>
        <?= Html::textInput('card_number', $model->cardCode, ['class' => 'form-control', 'id' => 'card-number']) ?>
        <?= $form->field($model, 'cardNumber', ['options' => ['tag' => false]])->hiddenInput(['class' => 'card-number-input'])->label(false) ?>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="form-group card-payment-date-block field-cauriform-year field-cauriform-month required" style="letter-spacing: -0.1em;">
                <?= Html::label(Yii::t('model', 'Expiration Date'))?>
                <?= $form->field($model, 'year', ['template' => '{input}', 'options' => ['tag' => false]])->dropDownList($years, ['class' => 'form-control card-payment-year']) ?>
                <?= $form->field($model, 'month', ['template' => '{input}', 'options' => ['tag' => false]])->dropDownList($months, ['class' => 'form-control card-payment-month']) ?>
            </div>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'cardCode')->textInput(['id' => 'cvv'])->error(false) ?>
        </div>
    </div>
    <?= $form->field($model, 'paymentId')->hiddenInput(['class' => 'shortcut-payment-id'])->label(false) ?>
    <?= $form->field($model, 'userId')->hiddenInput(['id' => 'user-id'])->label(false) ?>
    <div class="text-right">
        <?= Html::submitButton(Yii::t('app', 'Pay'), ['class' => 'btn-mid card-payment-submit shortcut-submit']) ?>
    </div>
<?php ActiveForm::end() ?>
<div class="spinner-bg">
    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
</div>
<div id="temp-form-container"></div>
<?php
$getTokenUrl = 'https://api.cauri.uk/rest-v1/card/getToken';
$tokenError = Yii::t('app', 'Error while getting card token');
$processError = Yii::t('app', 'Error occurred during payment process');
$publicKey = CauriHelper::$publicKey;
$script = <<<JS
    $('#card-number').mask("0000-0000-0000-0000999", {
        placeholder: "1234-5678-9012-3456", 
        onChange: function(val){
            $('.card-number-input').val(val.replace(/[^0-9]/g, '')).trigger('change');
        }
    });
    $('#cvv').mask("0009");
    
    $(document).on("beforeSubmit", "#cauri-form", function(){
        let form = $(this);
        form.hide();
        $('.spinner-bg').show();
        $.get('$getTokenUrl',
            {
                number: $('#card-number').val().replace(/[^0-9]/g, ''),
                expiration_month: $('.card-payment-month').val(),
                expiration_year: $('.card-payment-year').val(),
                security_code: $('#cvv').val(),
                project: '$publicKey'
            },
            function(data) {
                if (data.id) {
                    let formData = form.serializeArray();
                    formData.push({name: "CauriForm[token]", value: data.id});
                    $.post(form.attr('action'),
                        formData,
                        function(data) {
                            if (data.success === true) {
                                if (data.acs === true) {
                                    $("#temp-form-container").append(data.acs_form);
                                    $('#temp-form-container form').submit();
                                }
                            } else {
                                alertCall('top', 'error', '$processError');
                            }
                        },
                        "json"
                    );
                } else {
                    alertCall('top', 'error', '$tokenError');
                }
            },
            "json"
        ).fail(function() {
            alertCall('top', 'error', '$tokenError');
            $('.spinner-bg').hide();
            form.show();
        });
        return false;
    });
JS;
$this->registerJs($script);
