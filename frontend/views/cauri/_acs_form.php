<?php

use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var string $url */
/* @var array $params */

?>

<?php
echo Html::beginForm($url);
    foreach ($params as $key => $value) {
        echo Html::hiddenInput($key, $value);
    }
echo Html::endForm();
?>
