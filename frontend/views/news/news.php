<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 10:59
 */

use frontend\assets\NewsAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\models\PostCommentForm;
use yii\data\ActiveDataProvider;
use common\models\Page;
use yii\widgets\ListView;

/* @var PostCommentForm $postCommentForm
 * @var ActiveDataProvider $otherNewsDataProvider
 * @var ActiveDataProvider $relatedNewsDataProvider
 * @var ActiveDataProvider $commentsDataProvider
 * @var Page $model
 */

NewsAsset::register($this);

$translation = $model->translations[Yii::$app->language] ?? reset($model->translations);
?>

<div class="container-fluid">
    <div class="news-content row">
        <h1 class="col-md-8 col-sm-8 col-xs-12 news-title text-left">
            <?= Html::encode($translation->title) ?>
        </h1>

        <div class="news-text-side col-md-8 col-sm-8 col-xs-12">
            <div class="clearfix">
                <a class="alias-news-comment" href="#news-comments">
                    <?= Yii::t('app', '{count} comments', ['count' => count($model->comments)]) ?>
                </a>
            </div>
            <div class="news-img text-center">
                <?= Html::img($model->getThumb(),['alt' => Html::encode($translation->title), 'title' =>  Html::encode($translation->title)]) ?>
            </div>
            <div class="share-date clearfix">
                <div class="share col-md-6 col-sm-6 col-xs-12">
                    <div class="pluso" data-background="transparent"
                         data-options="medium,square,line,horizontal,counter,theme=04"
                         data-services="facebook,twitter,print,email"></div>
                </div>
                <div class="date-post col-md-6 col-sm-6 col-xs-12">
                    <div class="ujobs-circle-cell">
                        <div class="ujobs-circle text-center">
                            uJobs
                        </div>
                    </div>
                    <div class="date-cell">
                        <div class="who-post"><?= Yii::t('app', 'uJobs Team') ?></div>
                        <div class="date-news"><?= Yii::$app->formatter->asDate($model->publish_date) ?></div>
                    </div>
                </div>
            </div>
            <div class="news-box">
                <?= $translation->content ?>
            </div>

            <?php if ($otherNewsDataProvider->getCount() > 0) { ?>
                <div class="more-news row">
                    <div class=" more-news-title text-center">
                        <div><?= Yii::t('app', 'Read Next') ?></div>
                    </div>
                    <?= \yii\widgets\ListView::widget([
                        'dataProvider' => $otherNewsDataProvider,
                        'itemView' => 'listview/other-news',
                        'options' => [
                            'class' => ''
                        ],
                        'itemOptions' => [
                            'class' => 'col-md-6 col-sm-6 col-xs-12 news-list-over'
                        ],
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>

            <div class="share col-md-12 col-sm-12 col-xs-12">
                <div class="pluso" data-background="transparent"
                     data-options="medium,square,line,horizontal,counter,theme=04"
                     data-services="facebook,twitter,print,email"></div>
            </div>
            <div class="comment-news-box" id="news-comments">
                <div class="news-comtitle"><?= Yii::t('app', 'Comments') ?></div>
                <?= \yii\widgets\ListView::widget([
                    'dataProvider' => $commentsDataProvider,
                    'itemView' => 'listview/comment-item',
                    'options' => [
                        'class' => ''
                    ],
                    'itemOptions' => [
                        'class' => 'comment-news'
                    ],
                    'emptyText' => Yii::t('app', 'No comments yet'),
                    'layout' => "{items}"
                ]) ?>

                <?php if (!Yii::$app->user->isGuest) { ?>
                    <div class="news-comtitle"><?= Yii::t('app', 'Submit a Comment') ?></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'comment-form',
                        'action' => Url::to(['/comment/create'])
                    ]); ?>
                    <?= $form->field($postCommentForm, 'entityId')->hiddenInput(['value' => $model->id])->label(false) ?>
                    <?= $form->field($postCommentForm, 'comment')->textarea() ?>
                    <?= $form->field($postCommentForm, 'name', [
                        'template' => '<div class="row">
                                    <div class="col-md-6">
                                        {label}{input}{error}
                                    </div>
                                </div>'
                    ])->textInput() ?>
                    <?= $form->field($postCommentForm, 'email', [
                        'template' => '<div class="row">
                                        <div class="col-md-6">
                                            {label}{input}{error}
                                        </div>
                                    </div>'
                    ])->textInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Post Comment'), ['class' => 'btn-middle bright']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                <?php } ?>
            </div>
        </div>
        <aside class="news-aside col-md-4 col-sm-4 col-xs-12">
            <!--<div class="news-aside-block">
                <div class="news-aside-title">SUBSCRIBE FOR UPDATES</div>
                <form class="subscribe-form">
                    <div class="form-group">
                        <label for="subscribe">Subscribe here to get news, stories and tips directly to your inbox.</label>
                        <input type="email" class="form-control" placeholder="Email Address" id="subscribe">
                    </div>
                    <div class="form-group row">
                        <input class="btn-middle bright" type="submit" value="SUBSCRIBE">
                    </div>
                </form>
            </div>-->
            <div class="news-aside-block">
                <div class="news-aside-follow">
                    <div class="news-aside-soc-text"><?= Yii::t('app', 'Follow uJobs') ?></div>

                    <a rel="nofollow" target="_blank" href="https://www.facebook.com/Ujobsme-425244320979311/"
                       class="news-aside-socials">
                        <div class="text-center"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                    </a>
                    <a rel="nofollow" target="_blank" href="https://twitter.com/UjobsFreelance"
                       class="news-aside-socials">
                        <div class="text-center"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                    </a>
                    <a rel="nofollow" target="_blank" href="" class="news-aside-socials">
                        <div class="text-center"><i class="fa fa-rss" aria-hidden="true"></i></div>
                    </a>
                </div>
            </div>
            <?php if ($relatedNewsDataProvider->getCount() > 0) { ?>
                <div class="news-aside-block">
                    <div class="news-aside-title"><?= Yii::t('app', 'Related News') ?></div>
                    <?= ListView::widget([
                        'dataProvider' => $relatedNewsDataProvider,
                        'itemView' => 'listview/related-item',
                        'options' => [
                            'class' => ''
                        ],
                        'itemOptions' => [
                            'class' => 'related-post row'
                        ],
                        'emptyText' => Yii::t('app', 'No related yet'),
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php } ?>
        </aside>
    </div>
</div>