<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 16:41
 */

use common\models\elastic\PageElastic;
use common\models\Page;
use yii\helpers\Html;

/* @var $model Page|PageElastic */

?>

<div class="mob-news-header">
    <div class="mob-news-u-img">
        <img src="/images/new/og_default.png" alt="<?=Yii::t('app', 'uJobs Team')?>" title="<?=Yii::t('app', 'uJobs Team')?>">
    </div>
    <div class="mob-news-author">
        <?=Yii::t('app', 'uJobs Team')?>
    </div>
</div>
<?= Html::a(
    Html::img($model->getThumb('catalog'), ['alt' => Html::encode($model->getLabel()), 'title' => Html::encode($model->getLabel())]),
    ["/news/view", 'alias' => $model->alias],
    ['class' => 'mob-news-img']
)?>
<div class="mob-news-info">
    <?= Html::a(
        Html::encode($model->getLabel()),
        ["/news/view", 'alias' => $model->alias],
        ['class' => 'mob-news-title']
    )?>
    <div class="mob-news-text hide">
        text texttexttext text text text text text text text text text text text text
        text text text text text text text text text text text text
    </div>
</div>
<div class="mob-news-bottom">
    <div class="mob-news-comments">
        <?=Yii::t('app', '{count} comments', ['count' => count($model->comments)])?>
    </div>
    <div class="mob-news-date">
        <?=Yii::t('app', 'uJobs Team')?> <?= Yii::$app->formatter->asDate($model->publish_date) ?>
    </div>
</div>