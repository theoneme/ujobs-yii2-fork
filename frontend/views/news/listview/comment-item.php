<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 12:14
 */

use common\models\Comment;
use yii\helpers\Html;

/* @var Comment $model */

?>

<div class="cn-date"><?= Yii::$app->formatter->asDate($model->created_at) ?></div>
<div class="row cn-author-row">
    <div class="cn-author"><?= Html::encode($model->name) ?></div>
</div>
<div class="nc-text">
    <p>
        <?= nl2br(Html::encode($model->comment)) ?>
    </p>
</div>