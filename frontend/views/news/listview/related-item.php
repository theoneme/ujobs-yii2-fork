<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 13:00
 */

use common\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model Page
 */
?>

<div class="related-post-img">
    <?=Html::a(Html::img(Url::to($model->getThumb('catalog')), [ 'alt' => Html::encode($model->translation->title), 'title' => Html::encode($model->translation->title)]), Url::to([
        '/news/view', 'alias' => $model->alias
    ]))?>
</div>
<?=Html::a(Html::encode($model->translation->title), Url::to([
    '/news/view', 'alias' => $model->alias
]), ['class' => 'related-alias'])?>
<p>
    <?=\yii\helpers\BaseStringHelper::truncate(strip_tags($model->translation->content), 75)?>
</p>