<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 12:09
 */

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Page;

/* @var Page $model */

?>

<a href="<?=Url::to(['/news/view', 'alias' => $model->alias])?>" class="news-list-alias">
    <div class="news-list-img">
        <?=Html::img(Url::to($model->getThumb()), ['alt' => $model->translation->title, 'title' => $model->translation->title])?>
        <div class="news-list-descr">
            <?=$model->translation->title?>
        </div>
    </div>
</a>