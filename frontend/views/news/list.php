<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 10:59
 */

use yii\helpers\Html;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\NewsAsset::register($this);

?>

<div class="banner text-center" style="background:url(<?= $category->image ?>) no-repeat center/ cover">
    <h1 class="cat-title text-center"><?= Html::encode($category->translation->title) ?></h1>

    <div class="banner-descr"><?= Html::encode($category->translation->content_prev) ?></div>
</div>

<div class="container-fluid">
    <div class="category-main row hidden-xs">
        <p>&nbsp;</p>
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'listview/news-item',
            'options' => [
                'class' => 'block-flex',
                'style' => 'float: none; width: 100%'
            ],
            'itemOptions' => [
                'class' => 'bf-item',
                'style' => 'width: 18%'
            ],
            'emptyText' => Yii::t('app', 'No records found'),
            'layout' => "{items}"
        ]) ?>
    </div>

    <div class="category-main row visible-xs no-margin">
        <p>&nbsp;</p>
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'listview/news-item-mobile',
            'itemOptions' => [
                'class' => 'mob-news',
            ],
            'emptyText' => Yii::t('app', 'No records found'),
            'layout' => "{items}"
        ]) ?>
    </div>
</div>