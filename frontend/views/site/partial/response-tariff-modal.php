<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.11.2017
 * Time: 16:32
 */

use common\models\user\Profile;
use common\services\UserAccessService;
use yii\helpers\Html;

/** @var array $accessInfo */
/** @var Profile $profile */

?>

<div class="modal fade" id="tariffModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="form-title">
                    <?= $accessInfo['reason'] === UserAccessService::REASON_PROFILE_NOT_MODERATED ?
                        Yii::t('app', 'You cannot send response on this request') :
                        Yii::t('app', 'You cannot send more responses') ?>
                </div>
            </div>
            <div class="modal-body">
                <?php if($accessInfo['reason'] === UserAccessService::REASON_PROFILE_NOT_MODERATED) { ?>
                    <p style="font-size: 18px; text-align: center; line-height: 28px; margin-top: 15px;">
                        <?= Yii::t('app', 'Only users with filled and moderated profiles can send responses on requests. Please, fill your profile first and wait for it to be checked.') ?>
                    </p>
                    <ul class="text-left" style="font-size:14px;">
                        <li>
                            <?= Yii::t('app', 'Profile status: {status}', ['status' => $profile->getStatusLabel()]) ?>
                        </li>
                        <li>
                            <?= Yii::t('model', 'Name') ?>: <?= $profile->name ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>" ?>
                        </li>
                        <li>
                            <?= Yii::t('model', 'Something about you') ?>: <?= $profile->bio ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>" ?>
                        </li>
                        <li>
                            <?= Yii::t('model', 'Skills') ?>: <?= !empty($profile->skills) ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>" ?>
                        </li>
                        <li>
                            <?= Yii::t('model', 'Photo') ?>: <?= !empty($profile->gravatar_email) ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>" ?>
                        </li>
                    </ul>
                    <p style="font-size: 18px; text-align: center; line-height: 28px; margin-top: 15px;">
                        <?= Yii::t('app', 'If you want to increase chances of being chosen as worker, you can:') ?>
                    </p>
                    <ul class="text-left" style="font-size:14px;">
                        <li>
                            <?= Yii::t('app', 'Post job in this category {link}', ['link' => Html::a(Yii::t('app', 'Post a Job'), ['/job/create'])]) ?>
                        </li>
                        <li>
                            <?= Yii::t('app', 'Create portfolio with your projects {link}', ['link' => Html::a(Yii::t('app', 'Post a Portfolio'), ['/account/portfolio/create'])]) ?>
                        </li>
                        <li>
                            <?= Yii::t('app', 'Fill profile and set your skills {link}', ['link' => Html::a(Yii::t('app', 'Fill Profile'), ['/account/service/public-profile-settings'])]) ?>
                        </li>
                    </ul>
                <?php } ?>
                <?php if($accessInfo['reason'] === UserAccessService::REASON_TARIFF_RESPONSES_EXPIRED) { ?>
                    <p style="font-size: 18px; text-align: center; line-height: 28px; margin-top: 15px;">
                        <?= Yii::t('app', 'You cannot send more responses per day on with your tariff. <br> If you want to send more responses, you should choose other tariff') ?>
                    </p>
                    <?= Html::a(Yii::t('app', 'Choose tariff'), ['/page/static', 'alias' => 'tariffs'], ['class' => 'btn-big width100']) ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
