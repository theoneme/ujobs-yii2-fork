<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.11.2017
 * Time: 16:39
 */
use yii\helpers\Html;

?>

<div class="modal fade" id="tariffContactModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="form-title"><?= Yii::t('app', 'You are on free tariff') ?></div>
            </div>
            <div class="modal-body">
                <p style="font-size: 18px; text-align: center; line-height: 28px; margin-top: 15px;">
                    <?= Yii::t('app', 'You can contact {count, plural, one{# additional member} other {# additional members}} per day. If you want to contact more users per day, you should choose other tariff', ['count' => 3]) ?>
                </p>
                <?= Html::a(Yii::t('app', 'Change tariff'), ['/page/static', 'alias' => 'tariffs'], ['class' => 'btn-big width100']) ?>
            </div>
        </div>
    </div>
</div>
