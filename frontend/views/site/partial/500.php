<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 12.09.2017
 * Time: 13:56
 */

?>
<div class="container-fluid box-404">
    <div class="brick"></div>
    <div class="number text-right">
        <div class="five text-right">
            <div class="nail"></div>
            5
        </div>
        <div class="zero">
            <div class="nail"></div>
        </div>
        <div class="zero">
            <div class="nail"></div>
        </div>
    </div>
    <div class="info text-left">
        <h1 class="text-center"><?= Yii::t('app', 'Something is wrong'); ?></h1>
        <p><?= Yii::t('app', 'Internal Server Error.'); ?></p>
        <a href="/" class="button green big text-center"><?= Yii::t('app', 'Go Home') ?></a>
    </div>
</div>