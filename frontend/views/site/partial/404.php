<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 12.09.2017
 * Time: 13:56
 */

?>

<div class="container-fluid box-404">
    <div class="brick"></div>
    <div class="number text-right">
        <div class="four"></div>
        <div class="zero">
            <div class="nail"></div>
        </div>
        <div class="four"></div>
    </div>
    <div class="info text-left text-xl">
        <h1 class="text-center"><?= Yii::t('app', 'Something is wrong'); ?></h1>
        <p><?= Yii::t('app', 'The page you are looking for was moved, removed, renamed or might never existed.'); ?></p>
        <a href="/" class="button green big text-center"><?= Yii::t('app', 'Go Home') ?></a>
    </div>
</div>
