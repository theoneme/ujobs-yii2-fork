<?php
Yii::t('order', 'To simplify the customer search, you can create a job and more customers will see your offer');
Yii::t('order', 'To simplify the worker selection, you can create a request and more workers will see your request');
Yii::t('order', 'You accepted');
Yii::t('order', 'You declined');
Yii::t('order', 'You received offer');
Yii::t('order', 'You received request');
Yii::t('order', 'Waiting for customer`s response');
Yii::t('order', 'Waiting for worker`s response');
Yii::t('order', 'Waiting for customer to make deposit for work');
Yii::t('order', 'To continue conversation about this order click the button below');
Yii::t('order', 'Customer accepted individual job offer');
Yii::t('order', 'Customer declined individual job offer');
Yii::t('order', 'Customer withdrew individual job request');
Yii::t('order', 'Customer paid for individual job');
Yii::t('order', 'Customer made deposit');
Yii::t('order', 'You made deposit');
Yii::t('order', 'You need to accept or decline');
Yii::t('order', 'Worker canceled individual job offer');
Yii::t('order', 'Customer canceled individual job request');

Yii::t('order', 'Your Offer has been sent');
Yii::t('order', 'Your Request has been sent');
Yii::t('order', 'Worker accepted');
Yii::t('order', 'Worker declined');
Yii::t('order', 'Customer declined');
Yii::t('order', 'Customer accepted');
Yii::t('order', 'To get started, you need to make a deposit of {price}, which will be on your account until you accept the work. After that, the money will be transferred to the worker');

Yii::t('app', 'English');
Yii::t('app', 'Russian');
Yii::t('app', 'Spanish');
Yii::t('app', 'Ukrainian');
Yii::t('app', 'Georgian');
Yii::t('app', 'Chinese');
Yii::t('app', 'Finnish');
Yii::t('app', 'Swedish');
Yii::t('app', 'Polish');
Yii::t('app', 'Greek');
Yii::t('app', 'Belarusian');
Yii::t('app', 'Turkish');
Yii::t('app', 'Japanese');
Yii::t('app', 'Korean');
Yii::t('app', 'Vietnamese');
Yii::t('app', 'Thai');
Yii::t('app', 'Arabic');
Yii::t('app', 'Hindi');
Yii::t('app', 'Hebrew');

Yii::t('app', 'Dollar');
Yii::t('app', 'Hryvnia');
Yii::t('app', 'Ruble');
Yii::t('app', 'Lari');
Yii::t('app', 'Euro');
Yii::t('app', 'Yuan');

Yii::t('account', 'in English');
Yii::t('account', 'in Russian');
Yii::t('account', 'in Spanish');
Yii::t('account', 'in Ukrainian');
Yii::t('account', 'in Georgian');
Yii::t('account', 'in Chinese');

Yii::t('account', 'Add information about yourself {language}');
Yii::t('account', 'Change information about yourself {language}');

Yii::t('app', 'Recommended For You In Category {item}');
Yii::t('app', 'Recommended For You In Categories: {item}');

Yii::t('app', 'Saving');
Yii::t('app', 'Standard');
Yii::t('app', 'Premium');

Yii::t('app', 'Get Paid');
Yii::t('app', 'Get paid');
Yii::t('app', 'Choose the task');
Yii::t('app', 'Respond to request');

Yii::t('app', 'Our services');
Yii::t('app', 'Product for sale');
Yii::t('app', 'Our tenders');
Yii::t('app', 'Our product tenders');

Yii::t('notifications', '{sender} sent you individual job offer');
Yii::t('notifications', '{sender} sent you individual job request');
Yii::t('notifications', '{sender} left you inbox message');
Yii::t('notifications', '{sender} left you inbox message');
