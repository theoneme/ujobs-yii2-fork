<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 11.09.2017
 * Time: 17:34
 */

use frontend\assets\CommonAsset;
use frontend\assets\ErrorAsset;
use frontend\components\HeaderCategories;
use frontend\components\MobileMenu;
use yii\base\Action;
use yii\base\Controller;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var \yii\base\Exception $exception
 * @var $content string
 */

CommonAsset::register($this);
ErrorAsset::register($this);
Yii::$app->controller = new Controller('site', Yii::$app);
Yii::$app->controller->action = new Action('index', Yii::$app->controller);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/new/favicon.png'], true) ?>" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= MobileMenu::widget() ?>
<div class="mainblock error-blue">
    <header>
        <?= $this->render('@frontend/views/layouts/_header') ?>
        <?= HeaderCategories::widget() ?>
    </header>
    <div class="all-content">
        <?php switch ($exception->statusCode) {
            case 404:
                echo $this->render('partial/404', [
                    'name' => $exception->getMessage(),
                    'exception' => $exception
                ]);
                break;
            default:
                echo $this->render('partial/500', [
                    'name' => $exception->getMessage(),
                    'exception' => $exception
                ]);
                break;
        } ?>
        <div class="mfooter"></div>
    </div>
</div>

<div class="container-fluid">
    <div id="footer" class="box-404 text-center">
        <div class="worker"></div>
        <div class="tools"></div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

