<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.12.2017
 * Time: 16:13
 */

use common\helpers\SecurityHelper;

?>

<?php $userId = SecurityHelper::encrypt(Yii::$app->user->identity->getId());
$name = 'devvy';
$dialogId = SecurityHelper::encrypt(15273);
$chatUrl = Yii::$app->params['chatUrl'];

$script = <<<JS
    let userId = '{$userId}',
        dialogId = '{$dialogId}',
        chatSocket = io.connect('$chatUrl');

    chatSocket.emit('joinServer', {
        user_id: userId,
        name: '$name'
    });
    
    chatSocket.on('joined', function() {
        chatSocket.emit('joinDialog', {
            room_id: '$dialogId' 
        });
    });
    
    function send() {
        chatSocket.emit('sendMessage', {
            room_id: '$dialogId',
            message: 'hello',
        });
    }
    setTimeout(send, 1000);
JS;

$this->registerJs($script);

