<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 13:33
 */

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Page;

/* @var Page $model */

?>

<div class="nslide">
    <?=Html::a(Html::encode($model->translation->title), Url::to([
        '/news/view',
        'alias' => $model->alias
    ]), [
        'class' => 'nslide-tit'
    ])?>
    <p>
        <?=strip_tags(\yii\helpers\BaseStringHelper::truncate($model->translation->content, 250))?>
    </p>
    <?=Html::a(Yii::t('app', 'Read the full story'), Url::to([
        '/news/view',
        'alias' => $model->alias
    ]), [
        'class' => 'nmore'
    ])?>
    <?=Html::a('', Url::to([
        '/news/view',
        'alias' => $model->alias
    ]), [
        'class' => 'nslide-img',
        'style' => "background-image:url(" . $model->getThumb('news') . ")"
    ])?>
</div>