ИЗ components/views/header-widget

<div class="hide">
    <div class="push-block">
        <div class="push-msg">
            <div class="push-img">
                <img src="/images/new/cat-img.jpg" alt="push-img">
            </div>
            <div class="push-text">
                <p>
                    Хакеры смогли обойти двухфакторную
                    авторизацию с помощью уговоров
                </p>
            </div>
            <div class="push-close">
                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 246 20 20">
                    <path d="M18.947 262.44L12.51 256l6.437-6.438c.695-.695.695-1.814 0-2.51-.696-.695-1.814-.695-2.51 0L10 253.492l-6.438-6.44c-.695-.695-1.813-.695-2.51 0-.694.695-.694 1.815 0 2.51L7.493 256l-6.44 6.44c-.694.692-.694 1.812 0 2.507.697.695 1.815.695 2.51 0L10 258.51l6.438 6.438c.695.695 1.813.695 2.51 0 .69-.696.69-1.82 0-2.51z"></path>
                </svg>
            </div>
        </div>
        <div class="push-btn">
            <div class="push-bell">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 257.75 20 20">
                    <path d="M5.466 259.977l-1.296-1.295c-2.22 1.666-3.7 4.256-3.886 7.217h1.85c.186-2.5 1.388-4.628 3.332-5.923zm12.4 5.922h1.85c-.185-2.962-1.573-5.552-3.794-7.218l-1.295 1.295c1.85 1.295 3.053 3.424 3.238 5.922zm-1.85.462c0-2.868-1.944-5.182-4.628-5.83v-.647c0-.74-.647-1.388-1.388-1.388-.74 0-1.388.648-1.388 1.388v.648c-2.684.647-4.627 2.96-4.627 5.83v5.088l-1.85 1.852v.926h15.73v-.926l-1.85-1.85v-5.09zM10 277.004h.37c.648-.094 1.11-.556 1.296-1.11.093-.186.185-.464.185-.74h-3.7c0 1.016.832 1.85 1.85 1.85z"></path>
                </svg>
            </div>
            <div class="push-subscribe">
                <span>Подписаться</span>
                на push-уведомления
            </div>
        </div>
    </div>
</div>

<!--<div class="head-banner hide">-->
<!--    <div class="container-fluid">-->
<!--        <div class="hb-body text-center">-->
<!--            <div class="hb-img text-center">-->
<!--                <img src="/images/new/head-banner.png" alt="banner">-->
<!--            </div>-->
<!--            <div class="hb-info">-->
<!--                <div class="title">-->
<!--                    Кэшбэк 5% на всё самое-самое-->
<!--                </div>-->
<!--                <p>-->
<!--                    Возвращаем деньги за поездки, билеты в кино, еду и литры бензина: просто включите Apple Pay или бесконтактные платежи для Android через приложение Яндекс.Денег. До 31 августа-->
<!--                </p>-->
<!--                <a class="button green big" href="">Подробности</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <a class="close-banner" href="#"></a>-->
<!--</div>-->