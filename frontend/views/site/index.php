<?php
use common\models\Attachment;
use common\models\Job;
use frontend\components\JobSearchHelpWidget;
use frontend\models\IndexSearchForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $jobsDataProvider ActiveDataProvider */
/* @var $tendersDataProvider ActiveDataProvider */
/* @var $usersDataProvider ActiveDataProvider */
/* @var $searchModel IndexSearchForm */
/* @var $newsDataProvider ActiveDataProvider */
/* @var $showLoginModal bool */
/* @var $showSignupModal bool */
/* @var $indexCache array */
/* @var $job Job */

\frontend\assets\IndexAsset::register($this);

$stepsImageLanguage = is_dir(Yii::getAlias('@frontend') . '/web/images/new/steps/' . Yii::$app->language) ? Yii::$app->language : 'en-GB';
$videoLanguage = is_dir(Yii::getAlias('@frontend') . '/web/images/new/review/' . Yii::$app->language) ? Yii::$app->language : 'en-GB';
?>
	<div class="slider">
		<div id="mainslider">
			<div class="main-slide" style="background-image:url(<?= Url::to('/images/new/slide-3.jpg') ?>);">
				<div class="container-fluid">
					<div class="main-slide-info">
						<div class="main-slide-info-title">
							<?= Yii::t('app', 'Business card design') ?>
						</div>
						<div class="main-slide-info-price text-center">
							<div class="center-price">
                                <?= Yii::$app->utility->formatBannerPrice(1000) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!------------------NEW SLIDER FORM------------------>
		<div class="slider-form text-left">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<h1 class="text-left">
							<?= Yii::t('app', 'Marketplace powered by Blockchain') ?><br>

							<div class="slider-midtext text-left"><?= Yii::t('app', 'Buy or sell services <br> and products directly') ?></div>
						</h1>
						<?= Html::beginForm(['/search/search'], 'get', ['id' => 'banner-form', 'class' => 'search-container-form']) ?>
						<div class="search-box">
							<div class="form-group">
								<div class="input-group">
									<?= Html::textInput('request', '', [
										'class' => 'form-control autocomplete',
										'id' => 'banner-form-search',
										'type' => 'search',
										'data-f' => 'banner-form',
										'placeholder' => Yii::t('app', 'What service or product are you looking for?')
									]) ?>
									<span class="input-group-btn">
                                            <input type="submit" class="button green middle show-button-text" value="<?=Yii::t('app', 'Search')?>">
                                            <input type="submit" class="button green middle hide-button-text" value="<?=Yii::t('app', 'Search')?>">
                                        </span>
								</div>
							</div>
						</div>
						<?= Html::endForm(); ?>
						<div class="slider-smalltext">
                            <?= Yii::t('app', 'Protecting money transactions with Smart Contract technology') ?>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<!----------------END NEW SLIDER FORM---------------->
	</div>
	<div class="block-white">
		<div class="container-fluid">
			<div class="row display-flex">
				<div class="col-md-6">
					<div class="switch-title">
						<div data-id="role1" class="open">
							<div class="ujobs-presents">
								<p class="big-font">
                                    <?= Yii::t('app', 'Marketplace u<span class="text-green">J</span>obs offers'); ?>
								</p>

								<p class="small-font">
                                    <?//= Html::a(Yii::t('app', 'secure transaction'), ['/page/static', 'alias' => 'fair-play']); ?>
									<?= Yii::t('app', 'Secure transaction with Smart Contract'); ?>
								</p>
							</div>
						</div>
						<div data-id="role2">
							<div class="ujobs-presents">
								<p class="big-font">
                                    <?= Yii::t('app', 'Marketplace u<span class="text-green">J</span>obs offers'); ?>
								</p>

								<p class="small-font">
                                    <?= Yii::t('app', 'Secure transaction with Smart Contract'); ?>
								</p>
							</div>
						</div>
					</div>
					<ul class="role-switch">
						<li class="active text-center">
							<a href="#role1" data-toggle="tab"> <?= Yii::t('app', 'I’m client'); ?> </a>
						</li>
						<li class="text-center">
							<a href="#role2" data-toggle="tab"> <?= Yii::t('app', 'I’m supplier'); ?> </a>
						</li>
					</ul>
					<div class="tab-content tab-roles">
						<div class="role-content tab-pane fade in active" id="role1">
							<div class="role-info">
								<div class="role-info-item">
<!--									<div class="role-info-img text-right">-->
<!--                                        --><?//= Html::img(Yii::$app->mediaLayer->getThumb('/images/new/tab-icon1.png'), ['alt' => Yii::t('app', 'Secure buying of products and services'),'title' => Yii::t('app', 'Secure buying of products and services')]) ?>
<!--                                    </div>-->
									<div class="role-info-text">
										<div class="role-info-title"><?= Yii::t('app', 'Secure buying of products and services'); ?></div>
										<p><?= Yii::t('app', 'You can buy services and products through our secure site.'); ?></p>
									</div>
								</div>
								<div class="role-info-item">
<!--									<div class="role-info-img text-right">-->
<!--										--><?//= Html::img(Yii::$app->mediaLayer->getThumb('/images/new/tab-icon2.png'), ['alt' =>  Yii::t('app', 'Post the request on required service or product'),'title' => Yii::t('app', 'Post the request on required service or product')]) ?>
<!--									</div>-->
									<div class="role-info-text">
										<div class="role-info-title"><?= Yii::t('app', 'Post the request on required service or product'); ?></div>
										<p><?= Yii::t('app', 'If you did not find the desired product or service - Post the request and you will receive a huge amount of offers you can choose from. You will be guaranteed to find something  within the most attractive.'); ?></p>
									</div>
								</div>
							</div>
							<div class="role-buttons">
								<?php if (!Yii::$app->user->isGuest) {
									echo Html::a(Yii::t('app', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'button green auto']);
								} else {
									echo Html::a(Yii::t('app', 'Post a Request'), ['/entity/global-create', 'type' => 'tender-service'], [
										'class' => 'button green auto',
										'data-toggle' => 'modal',
										'data-target' => '#ModalLogin',
										'data-redirect' => Url::to(['/entity/global-create', 'type' => 'tender-service'])
									]);
								} ?>
                                <div class="clear showsmall"></div>
								<?= Html::a(Yii::t('app', 'Services'), ['/category/job-catalog'], ['class' => 'button white auto']) ?>
                                <?= Html::a(Yii::t('app', 'Products'), ['/board/category/products'], ['class' => 'button white auto']) ?>
							</div>
						</div>
						<div class="role-content tab-pane fade in" id="role2">
							<div class="role-info">
                                <div class="role-info-item">
<!--                                    <div class="role-info-img text-right">-->
<!--                                        --><?//= Html::img(Yii::$app->mediaLayer->getThumb('/images/new/tab-icon4.png'), ['alt' => Yii::t('app', 'Post offers of services and products'),'title' => Yii::t('app', 'Post offers of services and products')]) ?>
<!--                                    </div>-->
                                    <div class="role-info-text">
                                        <div class="role-info-title"><?= Yii::t('app', 'Post offers of services and products'); ?></div>
                                        <p><?= Yii::t('app', 'You can publish offers of your products and services, and attract new buyers'); ?></p>
                                    </div>
                                </div>
								<div class="role-info-item">
<!--									<div class="role-info-img text-right">-->
<!--										--><?//= Html::img(Yii::$app->mediaLayer->getThumb('/images/new/tab-icon3.png'), ['alt' => Yii::t('app', 'Offer your products and services to buyers'),'title' => Yii::t('app', 'Offer your products and services to buyers')]) ?>
<!--									</div>-->
									<div class="role-info-text">
										<div class="role-info-title"><?= Yii::t('app', 'Offer your products and services to buyers'); ?></div>
										<p><?= Yii::t('app', 'You can view requests on buying products and services, and make your offers.'); ?></p>
									</div>
								</div>
							</div>
							<div class="role-buttons">
								<?php if (!Yii::$app->user->isGuest) {
									echo Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], ['class' => 'button green auto']);
								} else {
									echo Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'], [
										'class' => 'button green auto',
										'data-toggle' => 'modal',
										'data-target' => '#ModalLogin',
										'data-redirect' => Url::to(['/entity/global-create'])
									]);
								} ?>
								<?= Html::a(Yii::t('app', 'Check requests'), ['/category/tender-catalog'], ['class' => 'button white auto']) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 how-work-slider-center hidden-xs hidden-sm">
					<div class="ujobs-anim-tab">
						<div class="ujobs-anim-item open" data-id="role1">
							<div class="ujobs-anim-tab1">
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step1.jpg'), ['alt' => 'step1', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step2.jpg'), ['alt' => 'step2', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step3.jpg'), ['alt' => 'step3', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step4.jpg'), ['alt' => 'step4', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step5.jpg'), ['alt' => 'step5', 'style' => 'width: 100%']) ?>
								</div>
							</div>
						</div>
						<div class="ujobs-anim-item open" data-id="role2">
							<div class="ujobs-anim-tab2">
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step1_isp.jpg'), ['alt' => 'step1', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step2_isp.jpg'), ['alt' => 'step2', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step3_isp.jpg'), ['alt' => 'step3', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step4_isp.jpg'), ['alt' => 'step4', 'style' => 'width: 100%']) ?>
								</div>
								<div>
									<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/steps/' . $stepsImageLanguage . '/Step5_isp.jpg'), ['alt' => 'step5', 'style' => 'width: 100%']) ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div id="job-search-help">
        <?= JobSearchHelpWidget::widget(['step_id' => Yii::$app->request->post('jobSearchHelpStepId'), 'job_id' => Yii::$app->request->post('jobSearchHelpJobId'), 'lvl' => Yii::$app->request->post('jobSearchHelpLvl', 1)])?>
    </div>
	<div class="block-light-grey">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="text-center">
						<?= Yii::t('app', 'Why working with uJobs is profitable?'); ?>
						<span>
                                <?= Yii::t('app', 'You pay only for the result. We guarantee a refund if the work is not done via fair play.'); ?>
                            </span>
					</h2>
				</div>
			</div>

			<div class="review-slider">
				<div class="rev-slide">
					<div class="rev-slide-info">
						<div class="info-center">
							<div class="rev-slide-title"><?= Yii::t('app', 'I pay for the result'); ?></div>
							<blockquote><?= Yii::t('app', 'I like that I pay for the visible result'); ?></blockquote>
							<cite><?= Yii::t('app', 'Katya Pavlova'); ?>,
								<span><?= Yii::t('app', 'Head of production of the publishing house "DV-PRESS"'); ?></span></cite>
						</div>
					</div>
					<div class="rev-slide-video">
						<a class="href-video" href="" data-toggle="modal" data-target="#video0">
							<?= Html::img(Yii::$app->mediaLayer->tryLoadFromAws('/images/new/review/poster0.jpg'), ['alt' => Yii::t('app', 'Katya Pavlova'), 'title' => Yii::t('app', 'Katya Pavlova')]) ?>
							<div class="play text-center">
								<i class="fa fa-play"></i>
							</div>
							<div class="slide-info-mobile">
								<div class="rev-slide-title"><?= Yii::t('app', 'I pay for the result'); ?></div>
								<blockquote><?= Yii::t('app', 'I like that I pay for the visible result'); ?></blockquote>
								<cite><?= Yii::t('app', 'Katya Pavlova'); ?>,
									<span><?= Yii::t('app', 'Head of production of the publishing house "DV-PRESS"'); ?></span></cite>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block-light-grey visible-xs">
		<div class="container-fluid">
			<div class="index-benefit-block">
				<h2 class="text-left">
					<?= Yii::t('app', 'Our benefits') ?>
					<span><?= Yii::t('app', 'Why is it profitable to order with us') ?></span>
				</h2>

				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12 text-center about">
						<a href="">
							<div class="about-img"
								 style="background: url(/images/new/icons/b1.png); background-size: cover;"></div>
							<div class="three-title"><?= Yii::t('app', 'Fair play') ?></div>
							<p class="text-center">
								<?= Yii::t('app', 'We will refund the money if the job is not done or if you`re not satisfied by the results. The worker will receive the money only when the customer accepts the finished job.') ?>
							</p>
						</a>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 text-center about">
						<a href="">
							<div class="about-img"
								 style="background: url(/images/new/icons/b2.png); background-size: cover;"></div>
							<div class="three-title"><?= Yii::t('app', 'Saving money') ?></div>
							<p class="text-center">
								<?= Yii::t('app', 'With ordering directly from the worker, you save up to 70% of service`s cost compared to service price from company.') ?>
							</p>
						</a>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 text-center about">
						<a href="">
							<div class="about-img"
								 style="background: url(/images/new/icons/b3.png); background-size: cover;"></div>
							<div class="three-title"><?= Yii::t('app', 'Saving time') ?></div>
							<p class="text-center">
								<?= Yii::t('app', 'It`s easier to choose among the workers, when they are all in one place. It`s easier to compare their offers and choose the most profitable proposition for you.') ?>
							</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block-white hidden-xs">
		<div class="container-fluid">
			<h2 class="text-center">
				<?= Yii::t('app', 'Our benefits') ?>
				<span><?= Yii::t('app', 'Why is it profitable to order with us') ?></span>
			</h2>

			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-6 text-center about">
					<a href="">
						<div class="about-img"
							 style="background: url(/images/new/icons/b1.png); background-size: cover;"></div>
						<div class="three-title"><?= Yii::t('app', 'Fair play') ?></div>
						<p class="text-center">
							<?= Yii::t('app', 'We will refund the money if the job is not done or if you`re not satisfied by the results. The worker will receive the money only when the customer accepts the finished job.') ?>
						</p>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-6 text-center about">
					<a href="">
						<div class="about-img"
							 style="background: url(/images/new/icons/b2.png); background-size: cover;"></div>
						<div class="three-title"><?= Yii::t('app', 'Saving money') ?></div>
						<p class="text-center">
							<?= Yii::t('app', 'With ordering directly from the worker, you save up to 70% of service`s cost compared to service price from company.') ?>
						</p>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-6 text-center about">
					<a href="">
						<div class="about-img"
							 style="background: url(/images/new/icons/b3.png); background-size: cover;"></div>
						<div class="three-title"><?= Yii::t('app', 'Saving time') ?></div>
						<p class="text-center">
							<?= Yii::t('app', 'It`s easier to choose among the workers, when they are all in one place. It`s easier to compare their offers and choose the most profitable proposition for you.') ?>
						</p>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php if (!isPagespeed()) { ?>
	<!--<div class="block-light-grey visible-xs">
		<div class="container-fluid">
			<div class="mob-head">
				<h2 class="text-left"><?= Yii::t('app', 'Services from professionals') ?>
					<span><?= Yii::t('app', 'Choose from thousands of options the services that you need') ?></span></h2>
			</div>
			<?= ListView::widget([
				'dataProvider' => $jobsDataProvider,
				'itemView' => '@frontend/modules/filter/views/job-list-mobile',
				'options' => [
					'class' => ''
				],
				'itemOptions' => [
					'class' => 'mob-work-item'
				],
				'layout' => "{items}"
			])?>
			<div class="load-button-place text-center">
				<a href="<?= Url::to(['/category/job-catalog']) ?>"
				   class="load"><?= Yii::t('app', 'Go to catalog') ?></a>
			</div>
		</div>
	</div>

	<div class="block-light-grey visible-xs">
		<div class="container-fluid">
			<div class="mob-head">
				<h2 class="text-left"><?= Yii::t('app', 'Do you need money?') ?>
					<span><?= Yii::t('app', 'Please choose an order that you can complete') ?></span></h2>
			</div>
			<?= ListView::widget([
				'dataProvider' => $tendersDataProvider,
				'itemView' => '@frontend/modules/filter/views/job-list-mobile',
				'options' => [
					'class' => ''
				],
				'itemOptions' => [
					'class' => 'mob-work-item'
				],
				'layout' => "{items}"
			]); ?>
			<div class="load-button-place text-center">
				<a href="<?= Url::to(['/category/tender-catalog']) ?>"
				   class="load"><?= Yii::t('app', 'Go to catalog') ?></a>
			</div>
		</div>
	</div>-->
    <!--new slider-->
    <div class="block-light-grey visible-xs">
        <div class="container-fluid">
            <div class="mob-head">
                <h2 class="text-left"><?= Yii::t('app', 'Services from professionals') ?>
                    <span><?= Yii::t('app', 'Choose from thousands of options the services that you need') ?></span></h2>
            </div>
            <?= ListView::widget([
                'dataProvider' => $jobsDataProvider,
                'itemView' => '@frontend/modules/filter/views/job-list-mobile-slider2',
                'options' => [
                    'class' => '',
                    'id' => 'mob-work-sl-pro',
                ],
                'itemOptions' => [
                    'class' => 'mob-work-sl-v2'
                ],
                'layout' => "{items}"
            ])?>
            <div class="load-button-place text-center">
                <a href="<?= Url::to(['/category/job-catalog']) ?>"
                   class="load"><?= Yii::t('app', 'Go to catalog') ?></a>
            </div>
        </div>
    </div>

    <div class="block-light-grey visible-xs">
        <div class="container-fluid">
            <div class="mob-head">
                <h2 class="text-left"><?= Yii::t('app', 'Do you need money?') ?>
                    <span><?= Yii::t('app', 'Please choose an order that you can complete') ?></span></h2>
            </div>
            <?= ListView::widget([
                'dataProvider' => $tendersDataProvider,
                'itemView' => '@frontend/modules/filter/views/job-list-mobile-slider2',
                'options' => [
                    'class' => '',
                    'id' => 'mob-work-sl-job',
                ],
                'itemOptions' => [
                    'class' => 'mob-work-sl-v2'
                ],
                'layout' => "{items}"
            ]); ?>
            <div class="load-button-place text-center">
                <a href="<?= Url::to(['/category/tender-catalog']) ?>"
                   class="load"><?= Yii::t('app', 'Go to catalog') ?></a>
            </div>
        </div>
    </div>
    <!--/////////-->
	<div class="block-light-grey response-block bottom-bordered-block hidden-xs">
		<div class="container-fluid">
			<h2 class="text-center"><?= Yii::t('app', 'Services from professionals') ?>
				<span><?= Yii::t('app', 'Choose from thousands of options the services that you need') ?></span></h2>
			<?= ListView::widget([
				'dataProvider' => $jobsDataProvider,
				'itemView' => '@frontend/modules/filter/views/job-list',
				'options' => [
					'class' => 'block-flex'
				],
				'itemOptions' => [
					'class' => 'bf-item'
				],
				'layout' => "{items}"
			]) ?>
			<div class="load-button-place text-center">
				<a href="<?= Url::to(['/category/job-catalog']) ?>"
				   class="load"><?= Yii::t('app', 'Go to catalog') ?></a>
			</div>
		</div>
	</div>

	<div class="block-light-grey response-block bottom-bordered-block hidden-xs">
		<div class="container-fluid">
			<h2 class="text-center"><?= Yii::t('app', 'Do you need money?') ?>
				<span><?= Yii::t('app', 'Please choose an order that you can complete') ?></span></h2>
			<?= ListView::widget([
				'dataProvider' => $tendersDataProvider,
				'itemView' => '@frontend/modules/filter/views/job-list',
				'options' => [
					'class' => 'block-flex'
				],
				'itemOptions' => [
					'class' => 'bf-item'
				],
				'layout' => "{items}"
			]) ?>
			<div class="load-button-place text-center">
				<a href="<?= Url::to(['/category/tender-catalog']) ?>"
				   class="load"><?= Yii::t('app', 'Go to catalog') ?></a>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="block-white">
		<div class="container-fluid">
			<h2 class="news-heading text-center">
				<?= Yii::t('app', 'Benefits for freelancers') ?>
				<span><?= Yii::t('app', 'Why working with us is profitable and safe?') ?></span>
			</h2>

			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12 text-center about">
					<a href="">
						<div class="about-img"
							 style="background: url(/images/new/icons/1.png); background-size: cover;"></div>
						<div class="three-title"><?= Yii::t('app', 'Do what you like') ?></div>
						<p class="text-center">
							<?= Yii::t('app', 'You can make business you like and still earn good money.') ?>
						</p>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 text-center about">
					<a href="">
						<div class="about-img"
							 style="background: url(/images/new/icons/2.png); background-size: cover;"></div>
						<div class="three-title"><?= Yii::t('app', 'Start earning') ?></div>
						<p class="text-center">
							<?= Yii::t('app', 'Register on our service, post a service and get orders.') ?>
						</p>
					</a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 text-center about">
					<a href="">
						<div class="about-img"
							 style="background: url(/images/new/icons/3.png); background-size: cover;"></div>
						<div class="three-title"><?= Yii::t('app', 'Guarantee of earning') ?></div>
						<p class="text-center">
							<?= Yii::t('app', 'We guarantee that you will get paid once you finish the job.') ?>
						</p>
					</a>
				</div>
			</div>
			<?php if (isGuest()) { ?>
				<div class="load-button-place text-center" style="margin: 0 0 25px;">
					<?= Html::a(Yii::t('app', 'Register as freelancer'), null, [
						'class' => 'load',
						'data-toggle' => 'modal',
						'data-target' => '#ModalSignup',
					]) ?>
				</div>
			<?php } ?>
            <div class="mark row">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                <div class="mark-text">
                    <?= Yii::t('app', 'Didn\'t find what you were looking for or don\'t have time to search?') ?>
                    <div class="mark-small">
                        <?= Yii::t('app', 'Submit Request and hundreds of Workers will be able to send you their proposals. You will have plenty to choose from.') ?>
                    </div>
                </div>
                <?php
                if (isGuest()) {
                    echo Html::a(Yii::t('app', 'Submit request'), null, [
                        'class' => 'btn-big bright redirect-set',
                        'data-toggle' => 'modal',
                        'data-target' => '#ModalSignup',
                        'data-redirect' => Url::to(['/entity/global-create', 'type' => 'tender-service'])
                    ]);
                } else {
                    echo Html::a(Yii::t('app', 'Submit request'), ['/entity/global-create', 'type' => 'tender-service'], [
                        'class' => 'btn-big bright',
                        'data-pjax' => 0
                    ]);
                }
                ?>
            </div>
		</div>
	</div>

	<div class="text-categories">
		<div class="container-fluid">
			<h2 class="text-left">
				<?= Yii::t('app', 'Private announcements uJobs.'); ?>
			</h2>
			<h3>
				<?= Yii::t('app', 'We help to sell your products and services through secure transaction.') ?>
			</h3>
			<div class="categories-descr">
				<p>
					<strong><?= Yii::t('app', 'For clients of products and services'); ?></strong><br>
					<?= Yii::t('app', 'We implemented secure transaction for protection of your money, when you buy products or order services from unknown people. If you want to buy product or service through secure transaction you may visit {services} or {products}', ['services' => Html::a(Yii::t('app', 'Catalog of services'), ['/category/job-catalog']), 'products' => Html::a(Yii::t('app', 'Catalog of products'), ['/board/category/products'])]) ?><br>
					<?= Yii::t('app', 'If you do not find the required service of product, you can post the request on what you need and specify terms and prices. This request will be noticed by thousands of sellers on our marketplace, so they will be able to make offers to you.'); ?>
                    <?php if (isGuest()) { ?>
                        <?= Html::a(Yii::t('app', 'Post request'), null, [
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalSignup',
                        ]) ?>
                    <?php } else { ?>
                        <?= Html::a(Yii::t('app', 'Post request'), ['/entity/global-create', 'type' => 'tender-service']) ?>
                    <?php } ?>
				</p>
				<p>
					<strong><?= Yii::t('app', 'For providers of products and services'); ?></strong><br>
					<?= Yii::t('app', 'Our marketplace allows to post announcements about selling products and services for free.') ?><br>
					<?= Yii::t('app', 'For providers of products and servicesWe care about your comfort and offer 2 options of working: you may sell through secure transaction with 10% commission, or set your contacts work directly with potential client.', ['link' => Html::a(Yii::t('app', 'Post announcement'), ['/entity/global-create'])]) ?>
					<br>
					<?= Yii::t('app', 'Also you may view requests from clients and offer your products and services.') ?> <?= Html::a(Yii::t('app', 'Request catalog'), ['/entity/catalog']) ?>
				</p>
			</div>
		</div>
	</div>

    <?= \frontend\components\CrosslinkWidget::widget() ?>

	<div class="modal fade video-modal" id="video0" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header header-col text-left">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="modal-body"></div>
				</div>
			</div>
		</div>
	</div>

<?
$searchUrl = Url::to(['/ajax/catalog-search', 'type' => 'job']);
if ($jobsDataProvider->totalCount > 0 || $tendersDataProvider->totalCount > 0) {
    $slickScript = <<<JS
    var slickDir = false;
    if($('body').hasClass('rtl')){
        slickDir = true;
    }
    $("#mob-work-sl-pro").slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: false,
        speed: 300,
        slidesToShow: 1,
		centerMode: false,
        variableWidth: true,
        rtl: slickDir
	});
    $("#mob-work-sl-job").slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: false,
        speed: 300,
        slidesToShow: 1,
		centerMode: false,
        variableWidth: true,
        rtl: slickDir
	});
    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
JS;
    $this->registerJs($slickScript);
}

$script = <<<JS
    var timeSlider1 = setInterval(function() {slider('.ujobs-anim-tab1');}, 2000);
    var timeSlider2 = setInterval(function() {slider('.ujobs-anim-tab2');}, 2000);
    $('#video0').on('show.bs.modal', function () {
        $(this).find('.modal-body').html('<video width=\'100%\' controls><source src=\'../images/new/review/$videoLanguage/video0.mp4\' type=\'video/mp4\'></video>');
    });
    $('.video-modal').on('shown.bs.modal', function () {
        $(this).find('video').get(0).play();
    }).on('hidden.bs.modal', function () {
        $(this).find('video').get(0).pause();
    });

    var data_id = $('.role-switch li.active a').attr('href').replace('#','');
	$('.switch-title > div').hide().removeClass('open');
	$('.switch-title > div[data-id='+data_id+']').show().addClass('open');
	$('.ujobs-anim-tab > div').hide().removeClass('open');
	$('.ujobs-anim-tab > div[data-id='+data_id+']').show().addClass('open');

	$('.role-switch a[data-toggle=\'tab\']').on('shown.bs.tab', function () {
		var data_id = $(this).attr('href').replace('#','');
		$('.switch-title > div').hide().removeClass('open');
		$('.switch-title > div[data-id='+data_id+']').css('display','block');
		$('.ujobs-anim-tab > div').hide().removeClass('open');
		$('.ujobs-anim-tab > div[data-id='+data_id+']').css('display','block');
		clearTimeout(timeSlider1);
		clearTimeout(timeSlider2);
		timeSlider1 = setInterval(function() {slider('.ujobs-anim-tab1');}, 2000);
		timeSlider2 = setInterval(function() {slider('.ujobs-anim-tab2');}, 2000);
		setTimeout(function() {
			$('.switch-title > div[data-id='+data_id+']').addClass('open');
			$('.ujobs-anim-tab > div[data-id='+data_id+']').addClass('open');},100);

	});
	
    new autoComplete({
        selector: "#banner-form .autocomplete",
        minChars: 2,
        autoFocus: false,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$searchUrl}', { request: request }, function(data) { 
                let autoCSource = [];
                if (data.success === true) {
                    $.each(data.data, function(key, value) {
                        autoCSource.push(value.label);
                    });
                }
                response(autoCSource); 
            });     
        },
    });
JS;

$this->registerJs($script);

if (isGuest() && $showLoginModal) {
	$this->registerJs("
        $('#ModalLogin').modal('show');
    ");
}
else if (isGuest() && $showSignupModal) {
    $this->registerJs("
        $('#ModalSignup').modal('show');
    ");
}