<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 19.04.2017
 * Time: 17:38
 */

use frontend\assets\CatalogAsset;
use frontend\assets\JobSearchAsset;
use frontend\modules\elasticfilter\components\FilterJob;
use frontend\modules\elasticfilter\components\FilterPro;
use frontend\modules\filter\components\BaseFilter;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\Category;
use yii\widgets\Pjax;

/* @var $category Category */
/* @var $children Category[] */
/* @var $seo array */
/* @var $filter FilterJob|FilterPro */
/* @var $filterView string */

CatalogAsset::register($this);
JobSearchAsset::register($this);

?>

<?php Pjax::begin(['enablePushState' => true, 'id' => 'catalog-pjax']); ?>
    <div class="new-banner text-center hidden-xs">
        <div class="how-works text-center">
            <a class="close-how" href=""></a>

            <h2 class="text-center"><?= $seo['upperBlockHeading'] ?></h2>

            <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
            <div class="three-steps text-center">
                <div class="three-steps-item">
                    <div class="circle-works">
                        <div class="inner-circle"></div>
                        <?= Html::img('/images/new/screen1.png', ['alt' => Yii::t('app', 'Make choice'),'title' => Yii::t('app', 'Make choice')]) ?>
                    </div>
                    <div
                            class="circle-title"><?= Yii::t('app', 'Make choice') ?></div>
                    <p class="circle-descr text-center">
                        <?= $seo['upperBlockColumn1'] ?>
                    </p>
                </div>
                <div class="three-steps-item">
                    <div class="circle-works">
                        <div class="inner-circle"></div>
                        <?= Html::img('/images/new/screen2.png', ['alt' => Yii::t('app', 'Make order'),'title' => Yii::t('app', 'Make order')]) ?>
                    </div>
                    <div
                            class="circle-title"><?= Yii::t('app', 'Make order') ?></div>
                    <p class="circle-descr text-center">
                        <?= $seo['upperBlockColumn2'] ?>
                    </p>
                </div>
                <div class="three-steps-item">
                    <div class="circle-works">
                        <div class="inner-circle"></div>
                        <?= Html::img('/images/new/screen3.png', ['alt' => Yii::t('app', 'Done'),'title' => Yii::t('app', 'Done')]) ?>
                    </div>
                    <div class="circle-title"><?= Yii::t('app', 'Done') ?></div>
                    <p class="circle-descr text-center">
                        <?= $seo['upperBlockColumn3'] ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="banner-product text-center">
            <div class="container-fluid">
                <h1 class="text-center"><?= $seo['h1'] ?></h1>
                <div class="banner-prod-descr text-center">
                    <?= $seo['subhead'] ?>
                </div>
                <a class="product-how-works hidden-xs" id="how-work" href="#"><?= Yii::t('board', 'How uJobs work'); ?></a>
            </div>
        </div>
    </div>
    <div class="container-fluid category-top">
        <div class="block-mob mar-fixedmob visible-xs">
            <?php if (!isPagespeed()) {
                $filter->template = "{$filterView}-mobile";
                $filter->filterType = BaseFilter::FILTER_TYPE_MOBILE;
                echo $filter->run();
            } ?>
        </div>

        <div class="category-main row hidden-xs">
            <?php $filter->template = $filterView;
            $filter->filterType = BaseFilter::FILTER_TYPE_DEFAULT;
            echo $filter->run(); ?>
        </div>
    </div>
<?php Pjax::end(); ?>

<?php $howItWorks = Yii::t('app', 'How the service works');
$minimizeText = Yii::t('app', 'Minimize');
$script = <<<JS
    $('body').on('click', '#how-work', function(e) {
        if($('.how-works').hasClass('open-how')) {
            $(this).html('$minimizeText');
        } else {
            $(this).html('$howItWorks');
        }
    });
JS;

$this->registerJs($script);