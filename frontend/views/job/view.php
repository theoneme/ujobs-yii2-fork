<?php

use common\components\CurrencyHelper;
use common\models\Job;
use common\modules\store\models\Currency;
use frontend\assets\ProductAsset;
use frontend\components\GmapsViewWidget;
use frontend\components\job\OtherPropositions;
use frontend\components\job\RecommendedPropositions;
use frontend\components\job\ViewedPropositions;
use frontend\components\SocialShareWidget;
use frontend\modules\account\components\MiniView;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;

/**
 * @var Job $model
 * @var View $this
 * @var array $accessInfo
 * @var array $breadcrumbs
 * @var ActiveDataProvider $reviewDataProvider
 * @var float $recommendAverage
 * @var float $reviewsAverage
 * @var float $serviceAverage
 * @var int $reviewsCount
 * @var float $communicationAverage
 * @var Currency $currency
 */

ProductAsset::register($this);
$currency = Currency::getDb()->cache(function ($db) {
    return Currency::find()->where(['code' => Yii::$app->params['app_currency_code']])->one();
});
?>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Product",
    <?php if (count($model->reviews) > 0) { ?>
    "aggregateRating": {
        "@type": "aggregateRating",
        "ratingValue": "<?= $model->rating / 2 ?>",
        "reviewCount": "<?= count($model->reviews) > 0 ? count($model->reviews) : 1 ?>"
    },
    <?php } ?>
    "description": "<?= $model->translation->title ?>",
    "name": "<?= $this->title ?>",
    "image": "<?= $model->getThumb() ?>",
    "offers": {
        "@type": "Offer",
        "availability": "http://schema.org/InStock",
        "price": "<?= $model->price ?>",
        "priceCurrency": "<?= $model->currency_code ?>"
    }
}
</script>

    <div class="work-bg">
        <div class="nav-fixed">
            <div class="container-fluid">
                <div class="nav-work row">
                    <ul class="menu-work">
                        <li>
                            <a class="selected" href="#w-slides"><?= Yii::t('app', 'Overview') ?></a>
                        </li>
                        <li>
                            <a href="#w-packs"><?= Yii::t('app', 'Compare Packages') ?></a>
                        </li>
                        <li>
                            <a href="#w-info"><?= Yii::t('app', 'Description') ?></a>
                        </li>
                        <li>
                            <a href="#w-reviews"><?= Yii::t('app', 'Reviews') ?></a>
                        </li>

                    </ul>
                    <div class="work-like">
                        <?php
                        if (!Yii::$app->user->isGuest) {
                            echo Html::a('<i class="fa fa-heart-o" aria-hidden="true"></i>' . Yii::t('app', 'Favorites'), null, [
                                'class' => 'wl-box hint--bottom biglike toggle-favourite ' . ($model->isFavourite() ? 'active' : ''),
                                'data-hint' => Yii::t('app', 'Favourite'),
                                'data-hint-add' => Yii::t('app', 'Favourite'),
                                'data-hint-remove' => Yii::t('app', 'Favourite'),
                                'data-id' => $model->id,
                                'data-type' => 'job'
                            ]);
                        } ?>
                        <div class="wl-count text-center"><?= count($model->favourites) ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="work-content row">
                <div class="work-side col-md-8 col-sm-8 col-xs-12">
                    <div id="w-slides" class="work-section">
                        <div class="work-header">
                            <h1 class="work-title text-left">
                                <?= Html::encode($model->translation->title) ?>
                            </h1>
                            <div class="work-stars hidden-xs">
                                <div class="work-rate">
                                    <!--                                --><?php //= StarRating::widget([
                                    //                                    'name' => 'rating_1',
                                    //                                    'value' => $reviewsAverage,
                                    //                                    'pluginOptions' => [
                                    //                                        'theme' => 'krajee-uni',
                                    //                                        'filledStar' => '&#x2605;',
                                    //                                        'emptyStar' => '&#x2606;',
                                    //                                        'size' => 'xxxxs',
                                    //                                        'min' => 0,
                                    //                                        'max' => 10,
                                    //                                        'step' => 1,
                                    //                                        'displayOnly' => true,
                                    //                                    ],
                                    //                                ]); ?>
                                    <!--	                            <a href="#w-reviews">(-->
                                    <?php //=$reviewsCount?><!--)</a>-->
                                    <?= $this->render('@frontend/widgets/views/rating', [
                                        'rating' => $model->rating / 2,
                                        'size' => 14
                                    ]) ?>
                                </div>
                                <!--<div class="queue">20 Orders in Queue</div>-->
                            </div>
                            <?= Breadcrumbs::widget([
                                'itemTemplate' => "{link}/", // template for all links
                                'activeItemTemplate' => "&nbsp;{link}",
                                'links' => $breadcrumbs,
                                'options' => ['class' => 'breadcrumbs hidden-xs'],
                                'tag' => 'div'
                            ]); ?>
                        </div>
                        <div class="gallery-container" id="zoom">
                            <div class="zoom-desktop">
                                <div class="container">
                                    <div class="big-image-container">
                                        <a class="lens-image" data-lens-image="<?= $model->getThumb() ?>">
                                            <img src="<?= $model->getThumb('catalog') ?>" class="big-image lazy-load"
                                                 data-src="<?= $model->getThumb() ?>" alt="<?=$model->category->translation->title?>" title="<?=$model->category->translation->title?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-prod-mob">
                                <div class="container">
                                    <div class="big-image-container slider-bigz">
                                        <?php
                                        if ($model->attachments) {
                                            foreach ($model->attachments as $attachment) { ?>
                                                <div data-lens-image="<?= $attachment->getThumb() ?>"
                                                     data-big-image="<?= $attachment->getThumb() ?>">
                                                    <?= Html::img($attachment->getThumb('catalog'), [
                                                        'alt' => $model->category->translation->title,
                                                        'title' => $model->category->translation->title,
                                                    ]) ?>
                                                </div>
                                            <?php }
                                        } else { ?>
                                            <div data-lens-image="<?= $model->getThumb() ?>"
                                                 data-big-image="<?= $model->getThumb() ?>">
                                                <?= Html::img($model->getThumb('catalog'), [
                                                    'alt' => $model->category->translation->title,
                                                    'title' => $model->category->translation->title,
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="thumbnails-container slider-navz">
                                <?php
                                if ($model->attachments) {
                                    foreach ($model->attachments as $attachment) { ?>
                                        <a href="#" class="thumbnail-wrapper"
                                           data-lens-image="<?= $attachment->getThumb() ?>"
                                           data-big-image="<?= $attachment->getThumb() ?>">
                                            <?= Html::img($attachment->getThumb('catalog'), [
                                                'alt' => $model->category->translation->title,
                                                'title' => $model->category->translation->title,
                                            ]) ?>
                                        </a>
                                    <?php }
                                } else { ?>
                                    <a href="#" class="thumbnail-wrapper" data-lens-image="<?= $model->getThumb() ?>"
                                       data-big-image="<?= $model->getThumb() ?>">
                                        <?= Html::img($model->getThumb('catalog'), [
                                            'alt' => $model->category->translation->title,
                                            'title' => $model->category->translation->title,
                                        ]) ?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $accessInfo,
                        'template' => 'mini_view_mobile',
                    ]) ?>

                    <?php if (!empty($model->packages)) { ?>
                        <div id="w-packs" class="work-section">
                            <div class="head-work">
                                <?= Yii::t('app', 'Compare Packages') ?>
                            </div>
                            <div class="body-work no-padding">
                                <div class="table-packs no-margin">
                                    <div class="row-packs">
                                        <div class="cell-packs text-left">
                                        </div>
                                        <?php foreach ($model->packages as $package) { ?>
                                            <div class="cell-packs">
                                                <div class="ap-head text-left">
                                                    <div class="ap-type">
                                                <span class="ap-typeprice">
                                                    <?= $package->getPrice() ?></span>
                                                        <br/>
                                                        <span class="ap-typetitle">
                                                    <?= Html::encode($package->title) ?>
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if (!$model->isPackageColumnEmpty('description')) { ?>
                                        <div class="row-packs">
                                            <div class="cell-packs text-left">
                                                <?= Yii::t('app', 'Description') ?>
                                            </div>
                                            <?php foreach ($model->packages as $package) { ?>
                                                <div class="cell-packs">
                                                    <div class="pack-tit text-left">
                                                        <?= nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $package->description))) ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (!$model->isPackageColumnEmpty('included')) { ?>
                                        <div class="row-packs">
                                            <div class="cell-packs text-left">
                                                <?= Yii::t('app', 'What Is Included?') ?>
                                            </div>
                                            <?php foreach ($model->packages as $package) { ?>
                                                <div class="cell-packs">
                                                    <?= $package->included ? nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $package->included))) : '-' ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (!$model->isPackageColumnEmpty('excluded')) { ?>
                                        <div class="row-packs">
                                            <div class="cell-packs text-left">
                                                <?= Yii::t('app', 'What is not included?') ?>
                                            </div>
                                            <?php foreach ($model->packages as $package) { ?>
                                                <div class="cell-packs">
                                                    <?= $package->excluded ? nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $package->excluded))) : '-' ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (!hasAccess($model->user_id)) { ?>
                                        <div class="row-packs">
                                            <div class="cell-packs text-left">
                                            </div>
                                            <?php foreach ($model->packages as $package) { ?>
                                                <div class="cell-packs">
                                                    <?php if ($model->status === Job::STATUS_ACTIVE) { ?>
                                                        <?= Html::a(
                                                            Yii::t('app', 'Order for {price}', [
                                                                'price' =>
                                                                    CurrencyHelper::convertAndFormat(
                                                                        $package->currency_code,
                                                                        Yii::$app->params['app_currency_code'],
                                                                        $package->price,
                                                                        true,
                                                                        0,
                                                                        '{sl}<span class="job-package-price" data-type="' . $package->type . '">{amount}</span>{sr}'
                                                                    )
                                                            ]),
                                                            ['/store/cart/index'],
                                                            ['class' => 'btn-middle add-to-cart', 'data-id' => $package->id]
                                                        ) ?>
                                                    <?php } else { ?>
                                                        <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                                            'class' => 'btn-middle add-to-cart',
                                                            'disabled' => 'disabled'
                                                        ]) ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div id="w-info" class="work-section">
                        <div class="head-work">
                            <?= Yii::t('app', 'About This Work') ?>
                        </div>
                        <div class="body-work mobile-description">
                            <?php
                            $innerLinkPattern = '/<a[^<>]*?href=\"(?=http(s)?:\/\/ujobs.me)([^"]*?)\"[^<>]*?>(.*?)<\/a>/';
                            $outerLinkPattern = '/<a[^<>]*?href=\"(?!http(s)?:\/\/ujobs.me)([^"]*?)\"[^<>]*?>(.*?)<\/a>/';
                            echo preg_replace(
                                $outerLinkPattern,
                                '\\2',
                                preg_replace($innerLinkPattern, "<a href='\\2' target='_blank'>\\2</a>", $model->translation->content)
                            )
                            ?>
                            <div class="mobile-more-gradient">
                                <a class="mob-more-descr" href="#"><?= Yii::t('app', 'See more'); ?></a>
                            </div>
                        </div>
                    </div>

                    <div class="work-flex-container">
                        <?php if (!empty($model->lat) && !empty($model->long)) { ?>
                            <div id="w-map" class="work-section map-flex-column">
                                <div class="head-work">
                                    <?= Yii::t('app', 'Address') ?>
                                </div>
                                <div class="body-work">
                                    <div class="ap-title"><?= Yii::t('app', 'Address') ?>
                                        : <?= $model->getAddress() ?></div>
                                    <br>
                                    <?= GmapsViewWidget::widget(['model' => $model]); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (count($model->indexedAttributes) > 0) { ?>
                            <div id="w-extra" class="work-section">
                                <div class="head-work">
                                    <?= Yii::t('board', 'Extra information') ?>
                                </div>
                                <div class="body-work">
                                    <?php foreach ($model->formatAttributes() as $key => $attribute) { ?>
                                        <div class="ap-title">
                                            <?= $key ?>:
                                            <span><?= is_array($attribute) ? implode(', ', $attribute) : $attribute ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!empty($model->packages) && count($model->packages) > 1) { ?>
                            <div class="work-section visible-xs mobile-packs">
                                <ul class="packs-switch no-markers">
                                    <?php foreach ($model->packages as $key => $package) { ?>
                                        <li class="<?= $key === 0 ? 'active' : '' ?>">
                                            <a class="text-center" href="#pack<?= $key ?>" data-toggle="tab"><?= $package->getPrice() ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content tab-packs">
                                    <?php foreach ($model->packages as $key => $package) { ?>
                                        <div class="tab-pack-content tab-pane fade in <?= $key === 0 ? 'active' : '' ?>"
                                             id="pack<?= $key ?>">
                                            <div class="mob-pack-title"><?= $package->title ?: ucfirst($package->type) ?></div>
                                            <div class="mob-pack-descr">
                                                <p>
                                                    <?= nl2br(Html::encode($package->description)) ?>
                                                </p>
                                            </div>
                                            <?php if (!empty($package->included)) { ?>
                                                <div class="mob-pack-title"><?= Yii::t('app', 'What Is Included?') ?></div>
                                                <div class="mob-pack-descr">
                                                    <p>
                                                        <?= nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $package->included))) ?>
                                                    </p>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($package->excluded)) { ?>
                                                <div class="mob-pack-title"><?= Yii::t('app', 'What is not included?') ?></div>
                                                <div class="mob-pack-descr">
                                                    <p>
                                                        <?= nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $package->excluded))) ?>
                                                    </p>
                                                </div>
                                            <?php } ?>
                                            <div class="mob-pack-delivery text-left">
                                                <?= Yii::t('app', 'Execution time') ?>
                                                <div class="mob-day-delivery">
                                                    <?= Yii::t('app', '{count, plural, one{# day} other{# days}}', ['count' => $model->execution_time]) ?>
                                                </div>
                                            </div>

                                            <?php if (!hasAccess($model->user_id)) { ?>
                                                <div class="add-to-cart-mob pos-bottom visible-xs">
                                                    <?php if ($model->status === Job::STATUS_ACTIVE) { ?>
                                                        <?= Html::a(
                                                            Yii::t('app', 'Order for {price}', [
                                                                'price' =>
                                                                    CurrencyHelper::convertAndFormat(
                                                                        $package->currency_code,
                                                                        Yii::$app->params['app_currency_code'],
                                                                        $package->price,
                                                                        true,
                                                                        0,
                                                                        '{sl}<span class="job-package-price" data-type="' . $package->type . '">{amount}</span>{sr}'
                                                                    )
                                                            ]),
                                                            ['/store/cart/index'],
                                                            ['class' => 'btn-big add-to-cart', 'data-id' => $package->id]
                                                        ) ?>
                                                    <?php } else { ?>
                                                        <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                                            'class' => 'btn-big add-to-cart',
                                                            'disabled' => 'disabled'
                                                        ]) ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } else if (count($model->packages) === 1) { ?>
                            <div class="work-section visible-xs mobile-one-pack">
                                <div class="mob-pack-title"><?= $model->packages[0]->title ?: ucfirst($model->packages[0]->type) ?></div>
                                <div class="mob-pack-descr">
                                    <p>
                                        <?= nl2br(Html::encode($model->packages[0]->description)) ?>
                                    </p>
                                </div>
                                <?php if (!empty($model->packages[0]->included)) { ?>
                                    <div class="mob-pack-title"><?= Yii::t('app', 'What Is Included?') ?></div>
                                    <div class="mob-pack-descr">
                                        <p>
                                            <?= nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $model->packages[0]->included))) ?>
                                        </p>
                                    </div>
                                <?php } ?>
                                <?php if (!empty($model->packages[0]->excluded)) { ?>
                                    <div class="mob-pack-title"><?= Yii::t('app', 'What is not included?') ?></div>
                                    <div class="mob-pack-descr">
                                        <p>
                                            <?= nl2br(Html::encode(preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $model->packages[0]->excluded))) ?>
                                        </p>
                                    </div>
                                <?php } ?>
                                <div class="mob-pack-delivery text-left">
                                    <?= Yii::t('app', 'Execution time') ?>
                                    <div class="mob-day-delivery"><?= Yii::t('app', '{count, plural, one{# day} other{# days}}', ['count' => $model->execution_time]) ?></div>
                                </div>
                                <?php if (!hasAccess($model->user_id)) { ?>
                                    <div class="add-to-cart-mob pos-bottom visible-xs">
                                        <?php if ($model->status === Job::STATUS_ACTIVE) { ?>
                                            <?= Html::a(
                                                Yii::t('app', 'Order for {price}', [
                                                    'price' =>
                                                        CurrencyHelper::convertAndFormat(
                                                            $model->packages[0]->currency_code,
                                                            Yii::$app->params['app_currency_code'],
                                                            $model->packages[0]->price,
                                                            true,
                                                            0,
                                                            '{sl}<span class="job-package-price" data-type="' . $model->packages[0]->type . '">{amount}</span>{sr}'
                                                        )
                                                ]),
                                                ['/store/cart/index'],
                                                ['class' => 'btn-big add-to-cart', 'data-id' => $model->packages[0]->id]
                                            ) ?>
                                        <?php } else { ?>
                                            <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                                'class' => 'btn-big add-to-cart',
                                                'disabled' => 'disabled'
                                            ]) ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if ($model->extras != null) { ?>
                            <?= Html::beginForm('', 'post', ['id' => 'optionsForm']) ?>
                            <?php foreach ($model->extras as $extra) { ?>
                                <?= Html::hiddenInput("extra[{$extra->id}][enabled]", 0, ['id' => "e_a_{$extra->id}_e"]) ?>
                                <?= Html::hiddenInput("extra[{$extra->id}][quantity]", 1, ['id' => "e_a_{$extra->id}_q"]) ?>
                            <?php } ?>
                            <?= Html::endForm() ?>

                            <div id="w-order" class="work-section hidden-xs">
                                <div class="head-work">
                                    <?= Yii::t('app', 'Additional options') ?>
                                </div>
                                <div id="w-options" class="work-additional">
                                    <?php foreach ($model->extras as $extra) { ?>
                                        <div class="wa-item">
                                            <input data-target="e_a_<?= $extra->id ?>_e" data-type="checkbox"
                                                   class="extra-trig form-control" id="extra-<?= $extra->id ?>"
                                                   type="checkbox"/>
                                            <label class="work-opt clearfix" for="extra-<?= $extra->id ?>">
                                                <span class="ctext"><?= $extra->title ?>&nbsp;
                                                    <span class="hint--top hint-work" data-hint="<?= $extra->description ?>">
                                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    </span>
                                                </span>
                                            </label>
                                            <div class="select120" style="width: 140px;">
                                                <?= Html::dropDownList('quantity', '', $extra->getQuantityList(), [
                                                    'data-type' => 'select',
                                                    'data-target' => "e_a_{$extra->id}_q",
                                                    'class' => 'extra-trig form-control'
                                                ]) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!-- опции -->
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!empty($model->faq)) { ?>
                            <div id="w-faq" class="work-section ">
                                <div class="head-work">
                                    <?= Yii::t('app', 'Frequently Asked Questions') ?>
                                    <div class="mobile-btn">
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                </div>
                                <div class="body-work hidden-xs">
                                    <div class="faq">
                                        <?php foreach ($model->faq as $item) { ?>
                                            <div class="faq-item">
                                                <div class="faq-head">
                                                    <?= nl2br(Html::encode($item->question)) ?>
                                                </div>
                                                <div class="faq-body">
                                                    <p>
                                                        <?= nl2br(Html::encode($item->answer)) ?>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($reviewDataProvider->getCount() > 0) { ?>
                            <div id="w-reviews" class="work-section">
                                <div class="head-work clearfix">
                                    <div class="rev-count">
                                        <div class="rc-text">
                                            <!--                                    --><?php //= Yii::t('app', '{count, plural, =0{reviews} =1{review} other{reviews}}', ['count' => $reviewsCount]) ?>
                                            <?= Yii::t('app', 'Rating') ?>
                                        </div>
                                        <div class="rev-stars">
                                            <?= $this->render('@frontend/widgets/views/rating', [
                                                'rating' => $model->rating / 2,
                                                'size' => 18
                                            ]) ?>
                                        </div>
                                        <!--                                <div class="rev-rate">-->
                                        <?php //= round($reviewsAverage / 2, 1, PHP_ROUND_HALF_UP) ?><!--</div>-->
                                        <div class="rev-rate"><?= round($model->rating / 2, 1, PHP_ROUND_HALF_UP) ?></div>
                                        <div class="mobile-btn">
                                            <i class="fa fa-chevron-down"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="body-rate hidden-xs">
                                    <div class="rate-cols">
                                        <div class="rate-col text-center">
                                            <?= Yii::t('app', 'Contact seller') ?>
                                            <div class="rc-stars">
                                                <?= $this->render('@frontend/widgets/views/rating', [
                                                    'rating' => $communicationAverage / 2,
                                                    'size' => 16
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="rate-col text-center">
                                            <?= Yii::t('app', 'The service matches the description') ?>
                                            <div class="rc-stars">
                                                <?= $this->render('@frontend/widgets/views/rating', [
                                                    'rating' => $serviceAverage / 2,
                                                    'size' => 16
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="rate-col text-center">
                                            <?= Yii::t('app', 'Would Recommend') ?>
                                            <div class="rc-stars">
                                                <?= $this->render('@frontend/widgets/views/rating', [
                                                    'rating' => $recommendAverage / 2,
                                                    'size' => 16
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?= ListView::widget([
                                        'dataProvider' => $reviewDataProvider,
                                        'itemView' => 'listview/review-item',
                                        'options' => [
                                            'class' => ''
                                        ],
                                        'itemOptions' => [
                                            'class' => 'comment'
                                        ],
                                        'emptyText' => Html::tag('p', Yii::t('app', 'No reviews yet'), ['class' => 'text-center']),
                                        'layout' => "{items}"
                                    ]) ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div id="w-reviews" class="work-section clearfix">
                                <div class="col-md-12">
                                    <div class="head-work row">
                                        <div class="rev-count">
                                            <div class="rc-text">
                                                <!--                                    --><?php //= Yii::t('app', '{count, plural, =0{reviews} =1{review} other{reviews}}', ['count' => $reviewsCount]) ?>
                                                <?= Yii::t('app', 'Reviews') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="body-rate hidden-xs">
                                        <br>
                                        <p class="text-center"><?= Yii::t('app', 'No reviews yet') ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (!empty($model->tags)) { ?>
                            <div class="tags hidden-xs">
                                <?php foreach ($model->tags as $tag) { ?>
                                    <?php if (isset($tag->attrValue->translations[Yii::$app->language])) { ?>
                                        <?= Html::a('<h2>' . Html::encode($tag->attrValue->translations[Yii::$app->language]->title) . '</h2>', [
                                            '/category/tag', 'reserved_job_tag' => $tag->value_alias
                                        ]); ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="author-side col-md-4 col-sm-4 col-xs-12 hidden-xs">
                    <?php if (!empty($model->packages)) { ?>
                        <?php foreach ($model->packages as $key => $package) { ?>
                            <div class="aside-price">
                                <div class="ap-head text-left">
                                    <div class="ap-type">
                                    <span class="ap-typeprice">
                                        <?= $package->getPrice() ?>
                                    </span>
                                        <?= $package->title ? $package->title : ucfirst($package->type) ?>
                                    </div>
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </div>
                                <div class="ap-body" <?php if ($key == 0) echo "style='display:block'"; ?>>
                                    <div class="ap-info">
                                        <div class="ap-info-item">
                                            <i class="fa fa-clock-o"
                                               aria-hidden="true"></i> <?= Yii::t('app', 'Execution time') ?>
                                            &nbsp;<?= Yii::t('app', '{count, plural, one{# day} other{# days}}', ['count' => $model->execution_time]) ?>
                                        </div>
                                    </div>
                                    <div class="ap-title"><?= Html::encode($package->title) ?></div>
                                    <p>
                                        <?= nl2br(Html::encode($package->description)) ?>
                                    </p>
                                    <?php if ($package->included) { ?>
                                        <div class="ap-title"><?= Yii::t('app', 'Included') ?>:</div>
                                        <p>
                                            <?= nl2br(Html::encode($package->included)) ?>
                                        </p>
                                    <?php } ?>
                                    <?php if ($package->excluded) { ?>
                                        <div class="ap-title"><?= Yii::t('app', 'Excluded') ?>:</div>
                                        <p>
                                            <?= nl2br(Html::encode($package->excluded)) ?>
                                        </p>
                                    <?php } ?>
                                    <div class="ap-buttons text-center">
                                        <?php if ($model->status === Job::STATUS_ACTIVE) { ?>
                                            <?php if (!hasAccess($model->user_id)) { ?>
                                                <?= Html::a(
                                                    Yii::t('app', 'Order for {price}', [
                                                        'price' =>
                                                            CurrencyHelper::convertAndFormat(
                                                                $package->currency_code,
                                                                Yii::$app->params['app_currency_code'],
                                                                $package->price,
                                                                true,
                                                                0,
                                                                '{sl}<span class="job-package-price" data-type="' . $package->type . '">{amount}</span>{sr}'
                                                            )
                                                    ]),
                                                    ['/store/cart/index'],
                                                    ['class' => 'btn-big add-to-cart', 'data-id' => $package->id]
                                                ) ?>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?= Html::a(Yii::t('app', 'This service is currently unavailable'), '#', [
                                                'class' => 'btn-big add-to-cart',
                                                'disabled' => 'disabled'
                                            ]) ?>
                                        <?php } ?>
                                        <?php if ($model->getActualPackagesCount() > 1) { ?>
                                            <a class="read-more"
                                               href="#w-packs"><?= Yii::t('app', 'Compare Packages') ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="portfolio-card">
                        <div class="pcard-head">
                            <div class="pcard-title"><?= Yii::t('app', 'My portfolio') ?></div>
                            <a href="<?= Url::to(['/account/portfolio/show', 'id' => $model->user_id]) ?>"
                               class="link-blue text-right"><?= Yii::t('app', 'View my portfolio') ?></a>
                        </div>
                        <?php if (count($model->user->userPortfolios) > 0) { ?>
                            <a href="<?= Url::to(['/account/portfolio/show', 'id' => $model->user_id]) ?>"
                               class="portf-alias">
                                <ul data-portfolio="<?= Yii::t('app', 'View my portfolio') ?>">
                                    <?php foreach ($model->user->userPortfolios as $k => $portfolio) { ?>
                                        <li>
                                            <?= Html::img($portfolio->getThumb('catalog'),['alt' => Yii::t('app','Portfolio'), 'title' => Yii::t('app','Portfolio')]) ?>
                                        </li>
                                        <?php if ($k === 2) break;
                                    } ?>
                                </ul>
                            </a>
                        <?php } else { ?>
                            <p>&nbsp;</p>
                            <div><?= Yii::t('account', 'Portfolio is empty') ?></div>
                        <?php } ?>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $accessInfo,
                    ]) ?>

                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::current(['invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                            'twitterMessage' => Yii::t('app', 'Check out this service on {site}:', ['site' => Yii::$app->name]),
                            'mailItem' => [
                                'label' => $model->getLabel(),
                                'thumb' => $model->getThumb('catalog'),
                                'name' => $model->getSellerName()
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="work-sliders">
        <?= OtherPropositions::widget(['userId' => $model->user->id, 'currentItem' => $model->id, 'sliderCount' => 6]) ?>
        <?= RecommendedPropositions::widget(['category' => $model->category, 'exceptId' => $model->id, 'sliderCount' => 6]) ?>
        <?= ViewedPropositions::widget(['currentItem' => $model]) ?>
    </div>

    <?= \frontend\components\CrosslinkWidget::widget() ?>

<?php if ($accessInfo['allowed'] === false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>

<?php $encodedName = Html::encode($model->getSellerName());
$escapedLabel = addslashes($model->getLabel());
$translations = json_encode([
    'see_less' => Yii::t('app', 'See less'),
    'see_more' => Yii::t('app', 'See more')
]);
$addToCartUrl = Url::to(['/store/cart/add']);
$currentUrl = Url::current([], true);
$packagesData = Json::encode(ArrayHelper::map($model->packages, 'type', function($var){
    return CurrencyHelper::convert($var->currency_code, Yii::$app->params['app_currency_code'], $var->price, true);
}));
$optionsData = Json::encode(ArrayHelper::map($model->extras, 'id', function($var){
    return CurrencyHelper::convert($var->currency_code, Yii::$app->params['app_currency_code'], $var->price, true);
}));
$script = <<<JS
	const translations = $translations;
	const packagesData = $packagesData;
	const optionsData = $optionsData;
    var slickDir = false;
	if($('body').hasClass('rtl')){
        slickDir = true;
    }
    if($(window).width() > 992){
        $('#zoom .big-image').simpleLens({
            loading_image: '/images/new/loading.gif',
            //open_lens_event: 'click'
        });
         $('#zoom .thumbnails-container img').simpleGallery({
            loading_image: '/images/new/loading.gif',
            show_event: 'click'
        });
    } else {
        $(".slider-bigz").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            rtl: slickDir,
            asNavFor: ".slider-navz"
        });
        $(".slider-navz").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: ".slider-bigz",
            dots: false,
            rtl: slickDir,
            centerMode: false,
            arrows: false,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },{
		        breakpoint: 600,
		        settings: {
		            slidesToShow: 4,
		            slidesToScroll: 1
		        }
		    }, {
		        breakpoint: 480,
		        settings: {
		            slidesToShow: 3,
		            slidesToScroll: 1
		        }
		    }, {
		        breakpoint: 360,
		        settings: {
		            slidesToShow: 2,
		            slidesToScroll: 1
		        }
		     }
        ]
    });
    }
    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
    /* $('.slider-nav .slide-w').css('opacity','.3');
	$('.slider-nav .slide-w.slick-active').first().css('opacity','1');
	$('.slider-nav').on('afterChange', function(){
		$('.slider-nav .slide-w').css('opacity','.3');
		$('.slider-nav .slide-w.slick-active').first().css('opacity','1');
	});
	$('body').on('click','.slider-big', function(){
		$('.slider-nav .slide-w').css('opacity','.3');
		$('.slider-nav .slide-w').eq($('.slider-big .slick-active').index()).css('opacity','1');
	});
	$('body').on('click','.slider-nav .slide-w',function () {
		$('.slider-nav .slide-w').css('opacity','.3');
		$('.slider-nav .slide-w.slick-active').first().css('opacity','1');
	});*/
	$('body').on('click', '.mobile-btn', function(){
        $(this).closest('.work-section').toggleClass('mobile-open');
    }).on('click', '.mob-more-descr', function(e){
        e.preventDefault();
        $(this).closest('.mobile-description').css('max-height','99999px');
        $(this).closest('.mobile-more-gradient').hide();
    }).on('click', '.work-section .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.work-section').find('.mob-more-info').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { 
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    }).on('click', '.author-intro .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.author-intro').find('.short-descr-desktop').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { 
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
	$(document).on('click', '.add-to-cart', function(){
        var id = $(this).attr('data-id'),
			self = $(this),
			options = $('#optionsForm').serialize();
        $.ajax({
            url: '$addToCartUrl',
            type: 'post',
            data: {id: id, options: options},
            success: function(response) {
                $('#cart-head-container').replaceWith(response);
                if (typeof Live !== 'undefined') {
                    Live.addToCart({
                        id: {$model->id},
                        thumb: '{$model->getThumb()}',
                        label: '{$escapedLabel}',
                        url: '{$currentUrl}',
                        price: '{$model->getPrice()}'
                    }); 
                }
                
                if(typeof fbq !== 'undefined') {
					fbq('track', 'AddToCart', {
						content_type: 'job',
						content_ids: [{$model->id}],
						value: {$model->price},
						currency: '{$model->currency_code}'
					});
					fbq('trackCustom', 'AddToCartCustom', {
						content_type: 'job',
						content_ids: [{$model->id}],
						value: {$model->price},
						currency: '{$model->currency_code}',
						'who': '{$encodedName}',
						'user_id': {$model->user_id},
						'email': '{$model->user->email}'
					});
                }

				window.location.href = self.attr('href');
            },
        });
        return false;
    });
	
    $(window).scroll(function() {
        var btn = $('.add-to-cart-mob');
        var scrollpos = $('.mobile-one-pack,.mobile-packs');
        if($(window).scrollTop()+$(window).height()>=(scrollpos.offset().top+scrollpos.height()+200) && !btn.hasClass('pos-bottom')){
            btn.addClass('pos-bottom');
        } else {
            if($(window).scrollTop()+$(window).height()<(scrollpos.offset().top+scrollpos.height()+200) && btn.hasClass('pos-bottom')) {
                btn.removeClass('pos-bottom');
            }
        }
    });
    
    $(document).on("change", ".extra-trig", function() {
	    var type = $(this).data("type"),
	    	target = $(this).data("target");

        if(type === "checkbox") {
            var isChecked = $(this).is(":checked");

            $("#" + target).val(isChecked ? 1 : 0);
        } else if (type === "select") {
            var value = $(this).find("option:selected").val();
            $("#" + target).val(value);
        }
        updateButtonPrices();
	});
    
    function updateButtonPrices(){
        let extraPrice = 0;
        $.each($('#optionsForm').serializeObject().extra, function(k, v){
            if (parseInt(v.enabled) === 1) {
                extraPrice += optionsData[k] * parseInt(v.quantity);
            }
        });
        $('.job-package-price').each(function(){
            $(this).html(formatter.formatPrice(packagesData[$(this).data('type')] + extraPrice));
        });
    }
   /* if(typeof fbq != 'undefined') {
		fbq('track', 'ViewContent', {
			content_ids: [{$model->id}],
			content_type: 'job',
			value: {$model->price},
			currency: {$model->currency_code}
		});
    }*/
JS;

$this->registerJs($script, \yii\web\View::POS_LOAD);