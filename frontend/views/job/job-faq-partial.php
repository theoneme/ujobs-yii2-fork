<?php

use yii\helpers\Html;
use common\models\JobQa;

/* @var JobQa $model */
/* @var integer $iterator */

?>

<div class="faq-block">
    <div id="faq-edit-<?= $iterator ?>" style="display: none">
        <div class="faqedit-block">
            <div class="feb-head"></div>
            <div class="feb-body"></div>
        </div>
    </div>
    <div id="faq-add-<?= $iterator ?>">
        <div id="container-question-<?= $iterator ?>">
            <?= Html::activeTextInput($model, "[$iterator]question", [
                'placeholder' => Yii::t('app', 'Add a question, for example: Do you translate into English?')
            ]) ?>
            <div class="help-block"></div>
        </div>

        <div id="container-answer-<?= $iterator ?>">
            <?= Html::activeTextarea($model, "[$iterator]answer", [
                'placeholder' => Yii::t('app', 'Add an answer, for example: Yes, I also do translation from English to Spanish'),
                'class' => 'readsym'
            ]) ?>
            <div class="counttext"><span class="ctspan">0</span> / 800 <?= Yii::t('app', 'Max') ?></div>
            <div class="help-block"></div>
        </div>

        <div class="row faq-buttons text-right">
            <?= Html::a(Yii::t('account', 'Cancel'), '#', ['class' => 'grey-but text-center grey-small closeeditt']) ?>
            <?= Html::button(Yii::t('account', 'Save'), ['class' => 'btn-small process-faq', 'data-key' => $iterator]) ?>
        </div>
    </div>
    <?php $this->registerJs('
        $("#job-form").yiiActiveForm("add", {
            id: "jobqa-' . $iterator . '-question",
            name: "JobQa[' . $iterator . '][question]",
            input: "#jobqa-' . $iterator . '-question",
            container: "#container-question-' . $iterator . '",
            error: ".help-block",
            enableAjaxValidation: true,
            validateOnChange: false,
            validateOnBlur: false
        });
        $("#job-form").yiiActiveForm("add", {
            id: "jobqa-' . $iterator . '-answer",
            name: "JobQa[' . $iterator . '][answer]",
            input: "#jobqa-' . $iterator . '-answer",
            container: "#container-answer-' . $iterator . '",
            error: ".help-block",
            enableAjaxValidation: true,
            validateOnChange: false,
            validateOnBlur: false
        });

        $(".process-faq").on("click", function() {
            var $faqBlock = $(this).closest(".faq-block"),
                key = $(this).data("key");

            $faqEditBlock = $faqBlock.find("#faq-edit-"+key);
            $faqAddBlock = $faqBlock.find("#faq-add-"+key);
            var faqBlockQuestion = $faqBlock.find("#container-question-" + key + " input").val();
            var faqBlockAnswer = $faqBlock.find("#container-answer-" + key + " textarea").val();

            if (faqBlockQuestion.length && faqBlockAnswer.length) {
                $faqEditBlock.find(".feb-head").html(faqBlockQuestion + "<i class=\"fa fa-chevron-down\"></i>");
                $faqAddBlock.appendTo($faqEditBlock.find(".feb-body"));

                $faqEditBlock.find(".faqedit-block.feb-open").removeClass("feb-open");
                $faqEditBlock.find(".closeeditt").html("' . Yii::t('app', 'Remove') . '");
                $faqEditBlock.show();
                $(this).closest(".create-gig-block").find(".load-faq").show();
            }
            return false;
        });

        $(".readsym").bind("keyup", function() {
            $(this).parent().children(".counttext").children(".ctspan").html($(this).val().length);
        });
    ');
    ?>
</div>
