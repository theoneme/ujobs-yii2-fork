<?php

use common\components\CurrencyHelper;
use common\helpers\FileInputHelper;
use common\helpers\ImperaviHelper;
use common\models\Attachment;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\components\WizardSteps;
use frontend\models\PostJobForm;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostJobForm
 * @var $jobPackageExamples array
 * @var $action string
 * @var $locales array
 * @var $currencySymbols array
 * @var $currencies array
 * @var $measures array
 */
CropperAsset::register($this);
SelectizeAsset::register($this);
$this->title = Yii::t('app', 'Create Job');

?>

<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('app', 'Description'),
    2 => Yii::t('app', 'Price and details'),
    3 => Yii::t('app', 'Additional information & FAQ'),
    4 => Yii::t('app', 'Gallery'),
    5 => Yii::t('app', 'Publish'),
], 'step' => 1]); ?>

    <div class="profile">
        <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true"
             style="padding-right: 17px;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div class="mobile-note-title"></div>
                    </div>
                    <div class="modal-body">
                        <div class="mobile-note-text"></div>
                        <div class="mobile-note-btn text-center">
                            <a class="button big green" href="#"><?= Yii::t('app', 'Continue') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a>
                        <?= Yii::t('app', Yii::$app->params['languages'][$model->locale]) ?>
                    </a>
                </li>
            </ul>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-step' => 1,
                            'data-final-step' => 5
                        ],
                        'id' => 'job-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]); ?>
                    <?= Html::hiddenInput('id', $model->job->id, ['id' => 'job-id']) ?>
                    <div class="tab-content nostyle-tab-content">
                        <div class="settings-content step tab-pane fade in active" id="step1" data-step="1">
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, 'locale')->hiddenInput()->label(false)->error(false); ?>
                                    <?= $form->field($model, "title", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                        <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('app', 'Max') . '</div>
                                    </div>'
                                    ])->textarea([
                                        'class' => 'readsym bigsymb',
                                        'placeholder' => Yii::t('app', 'Place a representative title to offer your services'),
                                        'data-action' => 'category-suggest'
                                    ])
                                    ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Name your work') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Choose wisely a concise and specific name for your service of no more than 80 characters.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "description", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->widget(Widget::class, ArrayHelper::merge(ImperaviHelper::getDefaultConfig(), [
                                        'settings' => [
                                            'buttonsHide' => [
                                                'deleted',
                                                'horizontalrule',
                                                'link',
                                            ]
                                        ]
                                ])) ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Description') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Try to describe your service with as many details as possible, if you provide many details more possibilities you will have to be hired.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label class="control-label" for="postjobform-category_id">
                                                    <?= Yii::t('app', 'Category') ?>
                                                </label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                                        'prompt' => ' -- ' . Yii::t('app', 'Select A Category'),
                                                        'class' => 'form-control'
                                                    ])->label(false); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postjobform-parent_category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helper'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ]
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="suggestions-block" class="col-md-12">

                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('app', 'How is your work classified?') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Please select the most relevant category and subcategory for your work.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('app', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane step fade in form-gig" id="step2" data-step="2">
                            <div class="gig-head row">
                                <div class="profileh"><?= Yii::t('app', 'Your offers') ?></div>
                                <div class="vac-mode packs3">
                                    <div class="vac-text"><?= Yii::t('app', '{count} Packages', ['count' => 3]) ?></div>
                                    <?= $form->field($model, 'three_packages', [
                                        'template' => '{input}{label}',
                                        'options' => ['class' => 'onoffswitch']
                                    ])
                                        ->checkbox(['class' => 'onoffswitch-checkbox', 'id' => 'myonoffswitch'], false)
                                        ->label('<span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span>', ['class' => 'onoffswitch-label'])
                                    ?>
                                </div>
                                <h3 class="no-padding clearfix"><?= Yii::t('app', 'Details and cost of your services') ?></h3>
                            </div>
                            <p>&nbsp;</p>
                            <div class="row formgig-block show-notes">
                                <?= $form->field($model, 'execution_time', [
                                    'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>'
                                ])->textInput([
                                    'type' => 'number',
                                    'class' => 'form-control',
                                    'placeholder' => Yii::t('app', 'Set execution time for this service in days')
                                ]) ?>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Execution time (days)') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Set approximated time (in days) that you need to provide your service. Cannot be more than 200.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <?= $form->field($model, "currency_code", [
                                    'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>'
                                ])->dropDownList($currencies, [
                                    'class' => 'form-control'
                                ]) ?>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Currency and price') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Set price that you want for your service. Also set currency for your price.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes price-per-unit-container">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label for="price-per-unit" class="control-label">
                                                <?= Yii::t('app', 'Price type') ?>
                                            </label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="chover">
                                            <?= Html::activeRadio($model, 'price_per_unit', ['value' => 0, 'id' => 'ppu-false', 'class' => 'ppu-radio radio-checkbox', 'label' => false]) ?>
                                            <?= Html::label(Yii::t('app', 'Set price for whole job'), 'ppu-false') ?>
                                        </div>
                                        <div class="chover">
                                            <?= Html::activeRadio($model, 'price_per_unit', ['value' => 1, 'id' => 'ppu-true', 'class' => 'ppu-radio radio-checkbox', 'label' => false]) ?>
                                            <?= Html::label(Yii::t('app', 'Set price per unit'), 'ppu-true') ?>
                                        </div>
                                    </div>
                                    <div class="measure-container">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label for="price-per-unit" class="control-label">
                                                    <?= Yii::t('app', 'Price for:') ?>
                                                </label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?= $form->field($model, "units_amount")->textInput([
                                                        'placeholder' => Yii::t('app', 'Amount in units of measurement specified below')
                                                    ])->label(false) ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?= $form->field($model, 'measure_id')->dropDownList($measures[$model->category_id] ?? [], [
                                                        'prompt' => Yii::t('app', 'Select the unit of measurement'),
                                                        'class' => 'form-control measure-select'
                                                    ])->label(false); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Price type') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Specify what your clients will pay. Whether it is for whole job or for a certain unit of work.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="cgb-head"><?= Yii::t('app', 'Packages') ?></div>
                                <div class="table-packs col-md-8 col-sm-12 col-xs-12">
                                    <div class="row-packs">
                                        <div class="cell-packs text-left noborder"></div>
                                        <div class="cell-packs">
                                            <?= Yii::t('app', 'First Package') ?>
                                        </div>
                                        <div class="cell-packs">
                                            <?= Yii::t('app', 'Second package') ?>
                                        </div>
                                        <div class="cell-packs">
                                            <?= Yii::t('app', 'Third package') ?>
                                        </div>
                                    </div>
                                    <div class="row-packs">
                                        <div class="cell-packs text-left"><strong><?= Yii::t('app', 'Package Name') ?></strong>
                                        </div>
                                        <?php $intKey = 0;
                                        foreach ($model->packages as $key => $package) { ?>
                                            <div class="cell-packs">
                                                <?= $form->field($package, "[$key]title", [
                                                    'template' => '{label}{input}'
                                                ])->textarea([
                                                    'class' => 'packstext form-control gigp-title pack-name',
                                                    'placeholder' => Yii::t('app', 'For example') . ":\n" . $jobPackageExamples[$intKey]['title']
                                                ])->label(false);
                                                $intKey++;
                                                ?>
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                        <?php } ?>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('app', 'Title') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <ul>
                                                    <li><?= Yii::t('app', 'Give your package a name that describes what it includes.') ?></li>
                                                    <li>
                                                        <strong><?= Yii::t('app', 'For example:') ?></strong> <?= Yii::t('app', '2D or 3D cover, print version') ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-packs">
                                        <div class="cell-packs text-left">
                                            <strong><?= Yii::t('app', 'Describe your bid details') ?></strong>
                                        </div>
                                        <?php $intKey = 0;
                                        foreach ($model->packages as $key => $package) { ?>
                                            <div class="cell-packs">
                                                <?= $form->field($package, "[$key]description", [
                                                    'template' => '{label}{input}'
                                                ])->textarea([
                                                    'class' => 'packstext form-control gigp-title pack-descr',
                                                    'placeholder' => Yii::t('app', 'For example') . ":\n" . $jobPackageExamples[$intKey]['description']
                                                ])->label(false);
                                                $intKey++;
                                                ?>
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                        <?php } ?>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('app', 'Description') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <ul>
                                                    <li><?= Yii::t('app', 'Summarize what this package offers buyers.') ?></li>
                                                    <li><?= Yii::t('app', 'You can use maximum 1000 chars.') ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-packs">
                                        <div class="cell-packs text-left">
                                            <strong><?= Yii::t('app', 'What Is Included?') ?></strong>
                                        </div>
                                        <?php $intKey = 0;
                                        foreach ($model->packages as $key => $package) { ?>
                                            <div class="cell-packs">
                                                <?= $form->field($package, "[$key]included", [
                                                    'template' => '{label}{input}'
                                                ])->textarea([
                                                    'class' => 'packstext form-control gigp-title pack-included',
                                                    'placeholder' => Yii::t('app', 'For example') . ":\n" . $jobPackageExamples[$intKey]['included']
                                                ])->label(false);
                                                $intKey++;
                                                ?>
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                        <?php } ?>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('app', 'What Is Included?') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <ul>
                                                    <li><?= Yii::t('app', 'Describe what is included in the package') ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-packs">
                                        <div class="cell-packs text-left">
                                            <strong><?= Yii::t('app', 'What is not included?') ?></strong>
                                        </div>
                                        <?php $intKey = 0;
                                        foreach ($model->packages as $key => $package) { ?>
                                            <div class="cell-packs">
                                                <?= $form->field($package, "[$key]excluded", [
                                                    'template' => '{label}{input}'
                                                ])->textarea([
                                                    'class' => 'packstext form-control gigp-title pack-excluded',
                                                    'placeholder' => Yii::t('app', 'For example') . ":\n" . $jobPackageExamples[$intKey]['excluded']
                                                ])->label(false);
                                                $intKey++;
                                                ?>
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                        <?php } ?>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('app', 'What is not included?') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <p><?= Yii::t('app', 'Describe what is excluded from the package') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-packs">
                                        <div class="cell-packs text-left">
                                            <strong><?= Yii::t('app', 'Price') ?> (<span class="currency-place"></span>)</strong>
                                        </div>
                                        <?php $intKey = 0;
                                        foreach ($model->packages as $key => $package) { ?>
                                            <div class="cell-packs">
                                                <?= $form->field($package, "[$key]price", [
                                                    'template' => '{label}{input}'
                                                ])->textInput([
                                                    'type' => 'number',
                                                    'class' => 'packstext pack-price',
                                                    'placeholder' => Yii::t('app', 'For example') . ":\n "
                                                        . CurrencyHelper::convert($jobPackageExamples[$intKey]['currency_code'], Yii::$app->params['app_currency_code'], $jobPackageExamples[$intKey]['price'], true)
                                                ])->label(false);
                                                $intKey++;
                                                ?>
                                            </div>
                                        <?php } ?>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('app', 'Price') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <ul>
                                                    <li><?= Yii::t('app', 'Earn up to 64% more per order, specifying 3 packages for the service.') ?></li>
                                                    <li><?= Yii::t('app', 'Price your packages from lowest to highest: Standard is lowest, Premium is mid-level, and Pro is highest.') ?>
                                                    </li>
                                                    <li><?= Yii::t('app', 'You can always change your package price in the future.') ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="unlock-packs text-center">
                                        <?= Yii::t('app', 'Unlock your potential <br/> revenue with all 3 Packages') ?>
                                        <br/><br/>
                                        <a class="blue-but" href=""><?= Yii::t('app', 'Try Now') ?></a>
                                    </div>
                                </div>
                            </div>

                            <p>&nbsp;</p>

                            <div class="formgig-block">
                                <div class="cgb-head">
                                    <?= Yii::t('app', 'Additional options') ?>
                                </div>
                                <div class="cgb-body">
                                    <div class="additional-gig-item">
                                        <div class="additional-gig-head" style="display: block;">
                                            <i class="fa fa-plus"></i>
                                            <a class="load-job-extra" href=""><?= Yii::t('app', 'Add Extra') ?></a>
                                        </div>
                                        <?php foreach ($model->extras as $key => $value) { ?>
                                            <div class="cgb-optbox">
                                                <div class="row line-add-extra">
                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                        <?= Yii::t('app', 'Title') ?>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                                        <?= $form->field($value, "[{$key}]title")->textInput([
                                                            'placeholder' => Yii::t('app', 'Title your extra service')
                                                        ])->label(false) ?>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                        <?= Yii::t('app', 'Price') ?>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-12 extra-recommend">
                                                        <?= $form->field($value, "[{$key}]price")->textInput([
                                                            'placeholder' => Yii::t('app', '500')
                                                        ])->label(false) ?>
                                                    </div>
                                                </div>
                                                <div class="row line-add-extra">
                                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                                        <?= Yii::t('app', 'Description') ?>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-12">
                                                        <?= $form->field($value, "[{$key}]description")->textInput([
                                                            'placeholder' => Yii::t('app', 'Describe your offering')
                                                        ])->label(false) ?>
                                                    </div>
                                                </div>
                                                <div class="delete-extra">
                                                    <i class="fa fa-times"></i>
                                                    <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue']) ?>
                                                </div>
                                                <hr>
                                            </div>
                                        <?php } ?>
                                        <div class="extra-container"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade step in" id="step3" data-step="3">
                            <!--                        <div class="gig-head row">-->
                            <!--                            <div class="profileh">-->
                            <? //= Yii::t('app', 'Additional Information') ?><!--</div>-->
                            <!--                        </div>-->
                            <p>&nbsp;</p>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "requirements", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                        <div class="counttext"><span class="ctspan">0</span> / 600 ' . Yii::t('app', 'Max') . '</div>
                                    </div>
                                '
                                    ])->textarea([
                                        'class' => 'readsym',
                                        'placeholder' => Yii::t('app', 'What do you need from customers to do this job?')
                                    ]) ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Set requirements') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'What additional information/materials/things do you need from potential customers?') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="additional-info"></div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, 'tags', [
                                        'template' => '
                                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                                <div class="set-title">
                                                    {label}<div class="mobile-question" data-toggle="modal"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                                {input}{error}
                                                <div class="counttext"> ' . Yii::t('app', 'We recommend using at least 3 tags') . '</div>
                                            </div>
                                        '
                                    ])->textInput([
                                        'id' => 'tags',
                                        'class' => 'readsym'
                                    ]) ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Tags, tags, tags!') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'These are the words by which customers can find your offer.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="attributes-info"></div>
                            <div class="formgig-block show-notes">
                                <div class="gig-head row">
                                    <div class="profileh"><?= Yii::t('app', 'Frequently Asked Questions') ?></div>
                                </div>
                                <div class="cgb-head"><?= Yii::t('app', 'Add Questions & Answers for Your Buyers.') ?>
                                    <div class="mobile-question" data-toggle="modal"></div>
                                </div>
                                <a class="load-faq addfaq" href="#">+ <?= Yii::t('app', 'Add FAQ') ?></a>
                                <?php foreach ($model->faq as $key => $value) { ?>
                                    <div class="faqedit-block">
                                        <div class="feb-head">
                                            <?= $value->question ?>
                                            <i class="fa fa-chevron-down"></i>
                                        </div>
                                        <div class="feb-body">
                                            <?= $form->field($value, "[{$key}]question")->textInput(['placeholder' => Yii::t('app', 'Add a question, for example: Do you translate into English?')])->label(false) ?>
                                            <?= $form->field($value, "[{$key}]answer")->textarea(['placeholder' => Yii::t('app', 'Add an answer, for example: Yes, I also do translation from English to Spanish')])->label(false) ?>
                                            <div class="counttext"><span class="ctspan">0</span> /
                                                300 <?= Yii::t('app', 'Max') ?></div>
                                            <div class="row faq-buttons text-right">
                                                <a class="grey-but text-center grey-small closeedit"><?= Yii::t('app', 'Cancel') ?></a>
                                                <a class="btn-small process-loaded-faq"
                                                   data-key="<?= $key ?>"><?= Yii::t('app', 'Update') ?></a>
                                                <a class="del-faq">
                                                    <i class="fa fa-trash"></i>
                                                    <?= Yii::t('app', 'Delete') ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="faq-container"></div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'What are FAQs?') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'Here you can add answers to the most commonly asked questions. Your FAQs will be displayed in your Job page.') ?>
                                        </p>
                                    </div>
                                    <div class="example">
                                        <p>
                                            <span><?= Yii::t('app', 'For example') ?>: </span>
                                        </p>

                                        <p>
                                            <strong> <?= Yii::t('app', 'Question') ?>: </strong>
                                            <?= Yii::t('app', 'Can construction take more than 15 days?') ?>
                                        </p>

                                        <p>
                                            <strong> <?= Yii::t('app', 'Answer') ?>: </strong>
                                            <?= Yii::t('app', "Yes, everything depends on the weather conditions, but the average delivery is 15 days.") ?>
                                            <br/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block row">
                                <div class="col-md-12">
                                    <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                    <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade in file-input-step" id="step5" data-step="4">
                            <!--                        <div class="gig-head row">-->
                            <!--                            <div class="profileh">-->
                            <? //= Yii::t('app', 'Gallery') ?><!--</div>-->
                            <!--                        </div>-->
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label"
                                                   for="postproductform-uploaded_images"><?= Yii::t('app', 'Photos') ?></label>
                                            <!--<?= Yii::t('app', '(Here you can upload photos)') ?>-->
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="cgp-upload-files">
                                            <?= FileInput::widget(
                                                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                    'id' => 'file-upload-input',
                                                    'name' => 'uploaded_images[]',
                                                    'options' => ['multiple' => true],
                                                    'pluginOptions' => [
                                                        'dropZoneTitle' => Yii::t('app', 'Drag & drop photos here &hellip;'),
                                                        'overwriteInitial' => false,
                                                        'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                                            return $var->getThumb();
                                                        }, $model->attachments) : [],
                                                        'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                                                    ]
                                                ])
                                            ) ?>
                                            <div class="images-container">
                                                <?php foreach ($model->attachments as $key => $image) {
                                                    /* @var $image Attachment */
                                                    echo $form->field($image, "[{$key}]content")->hiddenInput([
                                                        'value' => $image->content,
                                                        'data-key' => 'image_init_' . $image->id
                                                    ])->label(false);
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('app', 'Photos') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('app', 'It’s photo time! You can upload as many images as you want.') ?>
                                            </p>
                                            <ul>
                                                <li><?= Yii::t('app', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                                <li><?= Yii::t('app', 'Dimensions: more - better') ?></li>
                                                <li><?= Yii::t('app', 'Max File size: {count} MB', ['count' => 5]) ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block row">
                                <div class="col-md-12">
                                    <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                    <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade in" id="step6" data-step="5">
                            <div class="publish-title text-center">
                                <?= Yii::t('app', 'It\'s almost ready...') ?>
                            </div>
                            <div class="publish-text text-center">
                                <?= Yii::t('app', "We are going to publish your bid <br/> and attract the attention of potential clients.") ?>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= !empty($locales) ? Html::button(Yii::t('app', 'Next language'), ['class' => 'btn-mid fright next-language']) : '' ?>
                                <?= Html::submitButton(Yii::t('app', 'Publish job'), ['class' => 'btn-mid fright']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <?php
        if (!empty($locales)) {
            echo Html::beginForm(['/job/create'], 'post', ['id' => 'job-locale-form']);
            foreach ($locales as $locale) {
                echo Html::hiddenInput('locales[]', $locale);
            }
            echo Html::endForm();
        }
        ?>

        <?= $this->render('@frontend/views/common/crop-bg.php')?>

    </div>


<?php $userId = Yii::$app->user->identity->getId();
$faqCount = count($model->faq);
$extraCount = count($model->extras);
$attachmentCount = count($model->attachments);
$ajaxCreateRoute = Url::toRoute('/job/ajax-create');
$categorySuggestRoute = Url::toRoute('/ajax/category-suggest');
$draftUrl = Url::toRoute('/job/draft');
$renderAttributesRoute = Url::toRoute('/job/render-attributes');
$renderAdditionalRoute = Url::toRoute('/job/render-additional');
$renderFaqRoute = Url::toRoute('/job/render-faq');
$renderExtraRoute = Url::toRoute('/job/render-extra');
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'tag']);
$addTagMessage = Yii::t('app', 'Add tag');
$currencySymbolsJson = Json::encode($currencySymbols);
$measures = json_encode($measures);

$placeholderPacks = json_encode([
    'PackageName' => Yii::t('app', 'Package Name'),
    'DescriptionName' => Yii::t('app', 'Describe your bid details'),
    'Exclude' => Yii::t('app', 'What Is Included?'),
    'Include' => Yii::t('app', 'What is not included?'),
    'Price' => Yii::t('app', 'Price'),
]);

$scriptPlaceholder = <<<JS
    var placeholders = $placeholderPacks;
    function placeholdersMobile(){
        if ($(window).width() <= 768) {
            $('.pack-name').attr('placeholder',placeholders['PackageName']);
            $('.pack-descr').attr('placeholder',placeholders['DescriptionName']);
            $('.pack-excluded').attr('placeholder',placeholders['Exclude']);
            $('.pack-included').attr('placeholder',placeholders['Include']);
            $('.pack-price').attr('placeholder',placeholders['Price']);
        }
    }
    placeholdersMobile();
    $(window).resize(function () {
        placeholdersMobile();
    });
JS;

$this->registerJs($scriptPlaceholder);

?>

<?php if (isset($action) && $action === 'create') {
    $script = <<<JS
        function makeDraft() {
            let currentStep = $("#job-form").data('step');
            if(currentStep > 1) {
                isDraftProcessing = true;
                let form = $("#job-form").serialize();
                return $.ajax({
                    url: '$draftUrl',
                    type: "post",
                    data: form,
                    success: function(response) {
                        if(response.success === true) {
                            $("#job-id").val(response.job_id);
                            let atts = response.attachments;
                            if(Object.keys(atts).length) {
                                let fileInput = $(".file-upload-input");
                                let inputId = fileInput.data('krajee-fileinput');
                                let opts = window[inputId];
                                opts = atts ? $.extend(true, {}, opts, atts) : opts;
                                fileInput.fileinput('destroy');
                                fileInput.fileinput(opts);

                                $(".images-container").html("");
                                $.each(atts.initialPreview, function(key, value) {
                                    let kostyl = value.toString(),
                                        kostylParts = kostyl.split("?");
                                    $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + key+ "\' type=\'hidden\' value=\'" + kostylParts[0].toString() + "\'>");
                                    attachments++;
                                });
                            }
                        }
                        isDraftProcessing = false;
                    }
                });
            }
        }
JS;
    $this->registerJs($script);
} else {
    $script = <<<JS
        function makeDraft() {
            isDraftProcessing = false;
            return false;
        }
JS;
    $this->registerJs($script);
}

$script = <<<JS
    $(document).on('click', '.mobile-question', function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click', '.mobile-note-btn a', function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });

    let uid = $userId,
        faqs = $faqCount,
        extras = $extraCount,
        attachments = $attachmentCount,
        isDraftProcessing = false,
        measures = $measures,
        currencySymbols = $currencySymbolsJson;

    function loadAdditional(categoryId, jobId) {
        $.get('$renderAdditionalRoute', {
            category_id: categoryId,
            entity: jobId,
        }, function(data) {
            $("#additional-info").html(data);
        }, "html");
    }

    function loadAttributes(categoryId, jobId) {
        $.get('$renderAttributesRoute', {
            category_id: categoryId,
            entity: jobId
        }, function(data) {
            $("#attributes-info").html(data);
        }, "html");
    }

    function loadMeasures(categoryId) {
        $('.measure-select option:not([value=""])').remove();
        if (measures[categoryId] !== undefined) {
            $('.price-per-unit-container').show();
            $.each(measures[categoryId], function(index, value) {
                $('.measure-select').append('<option value="' + index + '">' + value + '</option>');
            });
        } else {
            $('.price-per-unit-container').hide();
            $('#ppu-false').prop('checked', true);
            $('.ppu-radio').trigger('change');
        }
    }

    $(document).on("click", ".next-language", function() {
        $.post('$ajaxCreateRoute',
            $("#job-form").serialize(),
            function(data) {
                if (data.success === true) {
                    $("#job-locale-form").submit();
                } else {
                    // alertCall('top', 'error', data.message);
                }
            },
            "json"
        );
        
        return false;
    });

    $(document).on("change", "#postjobform-currency_code", function(e) {
        $('.currency-place').html(currencySymbols[$(this).val()]);
    });

    $('#postjobform-currency_code').trigger('change');

    $(document).on("change", "#postjobform-category_id", function(e) {
        let jobId = $('#job-id').val();
        let categoryId = $(this).find('option:selected').val();
        if(categoryId) {
            loadAttributes(categoryId, jobId);
            loadAdditional(categoryId, jobId);
            loadMeasures(categoryId); 
        }
    });
    
    let suggestTimer = null;
    $(document).on("keyup", "[data-action=category-suggest]", function() {
        let self = $(this);
        clearTimeout(suggestTimer);
        suggestTimer = setTimeout(function() {
            let val = self.val();
            $.get('{$categorySuggestRoute}',
                {entity: 'job', request: val},
                function(data) {
                    if (data.success === true) {
                        $('#suggestions-block').html(data.html);
                    } else {
                        $('#suggestions-block').html('');
                    }
                },
                "json"
            );
        }, 700);
    });
    
    $(document).on('click', '[data-action=suggestion-select]', function() {
        let parentCategoryInput = $('#postjobform-parent_category_id');
        let categoryInput = $('#postjobform-category_id');
        let subcategoryInput = $('#postjobform-subcategory_id');
        let pId = $(this).data('p-id');
        let cId = $(this).data('c-id');
        let sId = $(this).data('s-id');
        
        categoryInput.attr('data-c-id', cId);
        subcategoryInput.attr('data-s-id', sId);
        parentCategoryInput.val(pId).trigger('change');
        
        return false;
    });
    
    $('#postjobform-category_id').on('depdrop:afterChange', function(event, id, value) {
        let cId = $(this).attr('data-c-id');
        if(cId) {
            $('#postjobform-category_id').val(cId).trigger('change');
        }
    });
    
    $('#postjobform-subcategory_id').on('depdrop:afterChange', function(event, id, value) {
        let sId = $(this).attr('data-s-id');
        if(sId) {
            $('#postjobform-subcategory_id').val(sId).trigger('change');
        }
    });

    let depdropVal = $('#depdrop-helper').val();
    if (depdropVal) {
        let jobId = $('#job-id').val();
        loadAdditional(depdropVal, jobId);
        loadAttributes(depdropVal, jobId);
        // loadMeasures(depdropVal);
    }

    $(document).on("click", ".closeeditt", function() {
        $(this).closest(".faq-block").remove();
        $(this).closest(".formgig-block").find(".load-faq").show();

        return false;
    });

    $(document).on('click', '.feb-head', function() {
        let faqparent = $(this).parents('.faqedit-block');
        if (faqparent.hasClass('feb-open')) {
            faqparent.removeClass('feb-open');
            faqparent.find('.fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
        } else {
            $('.faqedit-block').removeClass('feb-open');
            $('.faqedit-block .fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
            faqparent.addClass('feb-open');
            faqparent.find('.fa-chevron-down').addClass('fa-chevron-up').removeClass('fa-chevron-down');
        }
    });

    $(document).on('click', '.addfaq', function() {
        $('.faqadd-block').show();

        return false;
    });

    $(document).on('click', '.faqedit-block .closeedit', function() {
        $(this).parents('.faqedit-block').removeClass('feb-open');
        $('.faqedit-block .fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');

        return false;
    });

    $(document).on('click', '.faqedit-block .del-faq', function() {
        $(this).parents('.faqedit-block').remove();

        return false;
    });

    $(document).on("click", ".load-faq", function(e) {
        e.preventDefault();
        let container = $(this).closest(".formgig-block").find(".faq-container");
        $.post('$renderFaqRoute', {
            iterator: faqs
        }, function(data) {
            if (data.success === true) {
                faqs++;
                container.append(data.html);
            } else {
                alertCall('top', 'error', data.message);
            }
        }, "json");
    });

    $(document).on("click", ".load-job-extra", function(e) {
        e.preventDefault();
        let container = $(this).closest(".formgig-block").find(".extra-container");
        $.post('$renderExtraRoute', {
            iterator: extras
        }, function(data) {
            if (data.success === true) {
                extras++;
                container.append(data.html);

                $('#postjobform-currency_code').trigger('change');
            } else {
                alertCall('top', 'error', data.message);
            }
        }, "json");
    });

    $(".process-loaded-faq").on("click", function() {
        let faqBlock = $(this).closest(".faqedit-block"),
            key = $(this).data("key"),
            faqBlockQuestion = faqBlock.find(".field-jobqa-" + key + "-question input").val(),
            faqBlockAnswer = faqBlock.find(".field-jobqa-" + key + "-answer textarea").val();

        if (faqBlockQuestion.length && faqBlockAnswer.length) {
            let title = faqBlockQuestion.concat("<i class='fa fa-chevron-down'></i>");

            faqBlock.find('.feb-head').html(title);
            faqBlock.find('.feb-head').trigger('click');
        }
        return false;
    });

    $("#job-form").on("beforeValidateAttribute", function(event, attribute, messages) {
        let current_step = $(this).data("step"),
            input_step = $(attribute.container).closest(".step").data("step");
        if (current_step !== input_step) {
            return false;
        }
    }).on("afterValidate", function(event, messages, errorAttributes) {
        if (errorAttributes.length) {
            let current_step = $(this).data("step");
            $.each(errorAttributes, function(k, v) {
                $(v.container).closest(".faqedit-block").addClass("feb-open");
            });
            $("html, body").animate({
                scrollTop: $(this).find("div[data-step=\'" + current_step + "\'] .has-error").first().offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function(event) {
        let current_step = $(this).data("step"),
            self = $(this);
        if ($(this).find("div[data-step=\'" + current_step + "\']").hasClass("file-input-step")) {
            let returnValue = true;
            $(".file-upload-input").each(function(index) {
                if ($(this).fileinput("getFilesCount") > 0) {
                    $(this).fileinput("upload");
                    returnValue = false;
                    return false;
                }
            });
            if (!returnValue) {
                return false;
            }
        }
        if (current_step !== $(this).data("final-step")) {
            if (!isDraftProcessing) {
                $.when(makeDraft()).done(function() {
                    self.find("div[data-step=\'" + current_step + "\']").removeClass("active");
                    $(".gig-steps li").removeClass("active");
                    current_step++;
                    self.data("step", current_step);
                    self.find("div[data-step=\'" + current_step + "\']").addClass("active");
                    $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                });
            }
            return false;
        } else {
            if (typeof Live !== 'undefined') {
                Live.postJob({
                    id: $("#job-id").val(),
                    label: $('#postjobform-title').val(),
                    url: '',
                    price: $('#jobpackage-basic-price').val() + " " + $('#postjobform-currency_code option:selected').val()
                });
            }
        }
    });

    $(".prev-step").on("click", function() {
        let jobForm = $("#job-form"),
            current_step = jobForm.data("step");

        if (current_step > 1) {
            jobForm.find("div[data-step=\'" + current_step + "\']").removeClass("active");
            $(".gig-steps li").removeClass("active");
            current_step--;
            jobForm.data("step", current_step);
            jobForm.find("div[data-step=\'" + current_step + "\']").addClass("active");
            $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        }
        return false;
    });

    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
        attachments++;
    }).on("filedeleted", function(event, key) {
        $(".images-container").find("input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $("#job-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });

    $(document).on("click", ".link-blue", function() {
        $(this).closest(".cgb-optbox").remove();
    });

    $("#tags").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class=\"create\">$addTagMessage&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$tagListUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    });

    $(document).on("change", ".ppu-radio", function(e) {
        if (parseInt($(".ppu-radio:checked").val())) {
            $('.measure-container').show();
        } else {
            $('.measure-container').hide();
        }
    });
    $('.ppu-radio').trigger('change');
JS;

$this->registerJs($script);