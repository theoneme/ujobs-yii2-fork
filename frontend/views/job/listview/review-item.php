<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 16:12
 */

use common\models\Review;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var Review $model
 */

?>

<div class="this-comment">
    <div class="photo-com">
        <div class="text-center">
            <?php
            if ($model->createdBy->profile->gravatar_email) {
                echo Html::img($model->createdBy->profile->getThumb('catalog'),['alt' => $model->createdBy->getSellerName(), 'title' => $model->createdBy->getSellerName()]);
            } else {
                echo StringHelper::truncate(Html::encode($model->createdBy->getSellerName()), 1, '');
            }
            ?>
        </div>
    </div>
    <div class="info-com text-left">
        <div class="who-com">
            <?= Html::a(Html::encode($model->createdBy->getSellerName()), Url::to([
                '/account/profile/show',
                'id' => $model->created_by
            ])) ?>
            <div class="com-stars">
                <?= $this->render('@frontend/widgets/views/rating', [
                    'rating' => $model->rating / 2,
                    'size' => 14
                ]) ?>
            </div>
        </div>
        <div class="com-text">
            <p><?= Html::encode($model->text) ?></p>
        </div>
        <div class="com-time"><?= Yii::$app->formatter->asDate($model->created_at) ?></div>
    </div>
</div>