<?php

use frontend\components\GmapsInputWidget;
use frontend\models\PostJobForm;

/**
 * @var $model PostJobForm
 * @var $jobTypeOptions array
 * @var $placeOptions array
 * @var $timeOptions array
 */

?>
<?php if($showAddressField === true) { ?>
    <div class="row formgig-block show-notes">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label"
                           for="postproductform-currency_code"><?= Yii::t('app', 'Address') ?></label>
                    <div class="mobile-question" data-toggle="modal"></div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <?= GmapsInputWidget::widget(['model' => $model, 'inputOptions' => [
                    'placeholder' => Yii::t('app', 'Moscow, Novy Arbat 12')]
                ]); ?>
            </div>
            <div class="notes col-md-6 col-md-offset-12">
                <div class="notes-head">
                    <div class="lamp text-center">
                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                    </div>
                    <div class="notes-title"><?= Yii::t('app', 'Your address') ?></div>
                </div>
                <div class="notes-descr">
                    <p>
                        <?= Yii::t('app', 'Enter your address It is not necessary to put the number of your apartment, or house number, if you do not want it.You can write the city and select the country.') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>