<?php

use frontend\models\PostJobForm;
use yii\helpers\Html;

/**
 * @var $model PostJobForm
 * @var $jobTypeOptions array
 * @var $placeOptions array
 * @var $timeOptions array
 */

?>
    <!--<div class="formgig-block">
        <div class="set-item">
            <div class="cgb-head"><?= Yii::t('app', 'Online/Offline job') ?></div>
            <?= Html::activeCheckboxList($model, "jobTypeOptions", $jobTypeOptions, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $output = "<input {$chk} value=\"{$value}\" id=\"type_{$index}\" type='checkbox' name=\"{$name}\"/><label for=\"type_{$index}\"><span class=\"ctext\">{$label}</span></label><br/><br/>";
                    return $output;
                }
            ]) ?>
        </div>
        <div class="notes col-md-6 col-md-offset-12">
            <div class="notes-head">
                <div class="lamp text-center">
                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                </div>
                <div class="notes-title"><?= Yii::t('app', 'Select How You Can Perform This Job') ?></div>
            </div>
            <div class="notes-descr">
                <p>
                    <?= Yii::t('app', 'Some jobs can be performed only offline, some online. Also some jobs can be performed and online and offline.') ?>
                </p>
            </div>
        </div>
    </div>-->
    <div class="formgig-block">
        <div class="set-item">
            <div class="cgb-head"><?= Yii::t('app', 'Place where work can be done') ?></div>
            <?= Html::activeCheckboxList($model, "placeOptions", $placeOptions, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $output = "
                        <input {$chk} value=\"{$value}\" id=\"place_{$index}\" type='checkbox' name=\"{$name}\"/>
                        <label for=\"place_{$index}\">
                            <span class=\"ctext\">{$label}</span>
                        </label>
                        <br/><br/>
                    ";
                    return $output;
                }
            ]) ?>

            <div id="myAddress" class="hidden">
<!--                <div class="row">-->
<!--                    <div class="col-xs-6">-->
<!--                        <div id="container-postcode">-->
<!--                            --><?//= Html::activeTextInput($model, 'address[postcode]', [
//                                'placeholder' => Yii::t('app', 'Enter {entity}', ['entity' => Yii::t('model', 'Postcode')])
//                            ]) ?>
<!--                            <div class="help-block"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-xs-6">-->
<!--                        <div id="container-region">-->
<!--                            --><?//= Html::activeTextInput($model, 'address[region]', [
//                                'placeholder' => Yii::t('app', 'Enter {entity}', ['entity' => Yii::t('model', 'Region')])
//                            ]) ?>
<!--                            <div class="help-block"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="row">
                    <div class="col-xs-6">
                        <div id="container-city">
                            <?= Html::activeTextInput($model, 'address[city]', [
                                'placeholder' => Yii::t('app', 'Enter {entity}', ['entity' => Yii::t('model', 'City')])
                            ]) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="container-street">
                            <?= Html::activeTextInput($model, 'address[street]', [
                                'placeholder' => Yii::t('app', 'Enter Street')
                            ]) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div id="container-house">
                            <?= Html::activeTextInput($model, 'address[house]', [
                                'placeholder' => Yii::t('app', 'Enter {entity}', ['entity' => Yii::t('model', 'House')])
                            ]) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div id="container-housing">
                            <?= Html::activeTextInput($model, 'address[housing]', [
                                'placeholder' => Yii::t('app', 'Enter {entity}', ['entity' => Yii::t('model', 'Housing')])
                            ]) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div id="container-room">
                            <?= Html::activeTextInput($model, 'address[room]', [
                                'placeholder' => Yii::t('app', 'Enter Apartment')
                            ]) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="notes col-md-6 col-md-offset-12">
            <div class="notes-head">
                <div class="lamp text-center">
                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                </div>
                <div class="notes-title"><?= Yii::t('app', 'Tell where you can perform this job.') ?></div>
            </div>
            <div class="notes-descr">
                <p>
                    <?= Yii::t('app', 'Some services can be done at client`s home or at the address of a professional. Services can also be provided remotely.') ?>
                </p>
            </div>
        </div>
    </div>
    <div class="formgig-block">
        <div class="set-item">
            <div class="cgb-head"><?= Yii::t('app', 'Time when work can be done') ?></div>
            <?= Html::activeCheckboxList($model, "timeOptions", $timeOptions, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $output = "<input {$chk} value=\"{$value}\" id=\"time_{$index}\" type='checkbox' name=\"{$name}\"/><label for=\"time_{$index}\"><span class=\"ctext\">{$label}</span></label><br/><br/>";
                    return $output;
                }
            ]) ?>
        </div>
        <div class="notes col-md-6 col-md-offset-12">
            <div class="notes-head">
                <div class="lamp text-center">
                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                </div>
                <div class="notes-title"><?= Yii::t('app', 'Tell when you can perform this job.') ?></div>
            </div>
            <div class="notes-descr">
                <p>
                    <?= Yii::t('app', 'Some services can be done urgently or at the time specified by the customer.') ?>
                </p>
            </div>
        </div>
    </div>

<?php $this->registerJs("
    $('#postjobform-placeoptions input').on('change', function() {
        if ($(this).val() == 'office') {
            if($(this).is(':checked')) {
                $('#myAddress').removeClass('hidden');
            } else {
                $('#myAddress').addClass('hidden');
            }
        }
    });
"); ?>