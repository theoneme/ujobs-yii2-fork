<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.01.2017
 * Time: 13:05
 */

use yii\helpers\Html;
use common\models\JobExtra;

/* @var JobExtra $model */
/* @var integer $iterator */
?>

    <div class="cgb-optbox">
        <div class="row line-add-extra">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <?= Yii::t('app', 'Title') ?>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12">
                <div id="container-title-<?= $iterator ?>">
                    <?= Html::activeTextInput($model, "[{$iterator}]title", [
                        'placeholder' => Yii::t('app', 'Title your extra service'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <?= Yii::t('app', 'Price') ?> (<span class="currency-place"></span>)
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 extra-recommend">
                <div id="container-price-<?= $iterator ?>">
                    <?= Html::activeTextInput($model, "[{$iterator}]price", [
                        'placeholder' => Yii::t('app', '500'),
                        'class' => 'form-control',
                        'type' => 'number'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="row line-add-extra">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <?= Yii::t('app', 'Description') ?>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <div id="container-description-<?= $iterator ?>">
                    <?= Html::activeTextInput($model, "[{$iterator}]description", [
                        'placeholder' => Yii::t('app', 'Describe your offering') . " " . Yii::t('app', '(This field is optional)'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
        </div>

        <div class="delete-extra">
            <i class="fa fa-times"></i>

            <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue']) ?>
        </div>
        <hr>
    </div>

<?php $this->registerJs('

    $("#job-form").yiiActiveForm("add", {
        id: "jobextra-' . $iterator . '-title",
        name: "jobExtra[' . $iterator . '][title]",
        input: "#jobextra-' . $iterator . '-title",
        container: "#container-title-' . $iterator . '",
        error: ".help-block",
        enableAjaxValidation: true,
        validateOnChange: false,
        validateOnBlur: false
    });
    $("#job-form").yiiActiveForm("add", {
        id: "jobextra-' . $iterator . '-description",
        name: "jobExtra[' . $iterator . '][description]",
        input: "#jobextra-' . $iterator . '-description",
        container: "#container-description-' . $iterator . '",
        error: ".help-block",
        enableAjaxValidation: true,
        validateOnChange: false,
        validateOnBlur: false
    });

    $("#job-form").yiiActiveForm("add", {
        id: "jobextra-' . $iterator . '-price",
        name: "jobExtra[' . $iterator . '][price]",
        input: "#jobextra-' . $iterator . '-price",
        container: "#container-price-' . $iterator . '",
        error: ".help-block",
        enableAjaxValidation: true,
        validateOnChange: false,
        validateOnBlur: false
    });

    $(".readsym").bind("keyup", function() {
        $(this).parent().children(".counttext").children(".ctspan").html($(this).val().length);
    });
');