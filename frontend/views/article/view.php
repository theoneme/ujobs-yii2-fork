<?php

use common\models\Subscription;
use frontend\assets\NewsAsset;
use frontend\components\SocialShareWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use frontend\models\PostCommentForm;
use yii\data\ActiveDataProvider;
use common\models\Page;
use yii\widgets\ListView;

/* @var PostCommentForm $postCommentForm
 * @var ActiveDataProvider $otherNewsDataProvider
 * @var ActiveDataProvider $relatedDataProvider
 * @var ActiveDataProvider $jobsDataProvider
 * @var ActiveDataProvider $tendersDataProvider
 * @var Page $model
 * @var View $this
 */

NewsAsset::register($this);

$hasLike = $model->hasLike();
$translation = $model->translations[Yii::$app->language] ?? array_values($model->translations)[0];
?>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Article",
    "author": "<?= $model->getSellerName() ?>",
    "name": "<?= $this->title ?>"
}
</script>

<div class="container-fluid">
    <div class="news-content row">
        <div class="news-text-side col-md-8 col-sm-8 col-xs-12">
            <h1 class="news-title text-left">
                <?= Html::encode($translation->title) ?>
            </h1>
            <div class="clearfix">
                <a class="alias-news-comment" href="#news-comments">
                    <?= Yii::t('app', '{count} comments', ['count' => count($model->comments)]) ?>
                </a>
                <div class="album-opt like-over pull-right" style="margin-top: 8px;">
                    <i class="fa fa-thumbs-up"></i>
                    <span><?= count($model->likes) ?></span>
                    <?php if(count($model->likes) > 0) { ?>
                        <div class="people-likes">
                            <div class="people-likes-title"><?= Yii::t('account', '{count, plural, one{# user likes} other{# users like}} this', ['count' => count($model->likes)]) ?></div>
                            <?php foreach ($model->likes as $userId => $like) {
                                echo Html::a(
                                    Html::img($model->getLikeUserThumb($userId, 'catalog'), ['alt' => $model->getLikeSellerName($userId), 'title' => $model->getLikeSellerName($userId)]),
                                    ['/account/profile/show', 'id' => $userId]
                                );
                            } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="album-opt pull-right" style="margin-top: 8px;">
                    <i class="fa fa-eye"></i>
                    <span><?= $model->views ?></span>
                </div>
            </div>
            <div class="news-img text-center">
                <?= Html::img($model->getThumb(),['alt' => Html::encode($translation->title), 'title' => Html::encode($translation->title)]) ?>
            </div>
            <div class="share-date clearfix">
                <div class="share col-md-6 col-sm-6 col-xs-12">
                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::current(['invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                            'mailItem' => [
                                'label' => $model->getLabel(),
                                'thumb' => $model->getThumb('catalog'),
                                'name' => $model->getSellerName()
                            ]
                        ])?>
                    </div>
                </div>
                <div class="date-post col-md-6 col-sm-6 col-xs-12">
                    <div class="ujobs-circle-cell">
                        <?= Html::img($model->getSellerThumb(), ['class' => 'article-avatar', 'alt' => $model->getSellerName(),'title' => $model->getSellerName()])?>
                    </div>
                    <div class="date-cell">
                        <div class="who-post">
                            <?= Html::a($model->getSellerName(), $model->getSellerUrl())?>
                        </div>
                        <div class="date-news"><?= Yii::$app->formatter->asDate($model->publish_date) ?></div>
                    </div>
                </div>
            </div>
            <div class="news-box">
                <?= $translation->content ?>
            </div>

            <div class="comment-news-box" id="news-comments">
                <div class="news-comtitle">
                    <div class="row">
                        <div class="col-md-4" style="line-height: 50px;">
                            <?= Yii::t('app', 'Comments') ?>
                        </div>
                        <div class="col-md-8 text-right">
                            <?php if ($hasLike) {
                                echo Html::a(
                                    '<i class="fa fa-thumbs-up"></i>&nbsp;' . Yii::$app->formatter->asDate($model->likes[userId()]->created_at),
                                    null,
                                    ['class' => 'btn-big', 'disabled' => true]
                                );
                            } else {
                                if (!Yii::$app->user->isGuest) {
                                    if (!hasAccess($model->user_id)) {
                                        echo Html::a(
                                            '<i class="fa fa-thumbs-up"></i>&nbsp;' . Yii::t('account', 'Like'),
                                            null,
                                            ['class' => 'button white big post-like text-center', 'data-id' => $model->id, 'data-entity' => 'page']
                                        );
                                    }
                                } else {
                                    echo Html::a(
                                        '<i class="fa fa-thumbs-up"></i>&nbsp;' . Yii::t('account', 'Like'),
                                        null,
                                        ['class' => 'button white big text-center', 'data-toggle' => 'modal', 'data-target' => '#ModalLogin']
                                    );
                                }
                            } ?>
                            <?php
                                if (!Yii::$app->user->isGuest) {
                                    if (!hasAccess($model->user_id)) {
                                        if (Yii::$app->user->identity->isSubscribedTo(Subscription::ENTITY_PROFILE, $model->user_id)) {
                                            echo Html::a(
                                                '<i class="fa fa-rss"></i>&nbsp;' . Yii::t('app', 'Unsubscribe'),
                                                null,
                                                ['class' => 'button orange big text-center subscribe-button active', 'data-id' => $model->user_id, 'data-entity' => Subscription::ENTITY_PROFILE]
                                            );
                                        } else {
                                            echo Html::a(
                                                '<i class="fa fa-rss"></i>&nbsp;' . Yii::t('app', 'Subscribe'),
                                                null,
                                                ['class' => 'button orange big text-center subscribe-button', 'data-id' => $model->user_id, 'data-entity' => Subscription::ENTITY_PROFILE]
                                            );
                                        }
                                    }
                                } else {
                                    echo Html::a(
                                        '<i class="fa fa-rss"></i>&nbsp;' . Yii::t('app', 'Subscribe'),
                                        null,
                                        ['class' => 'button orange big text-center', 'data-toggle' => 'modal', 'data-target' => '#ModalLogin']
                                    );
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div>
                    <?php
                    if (count($model->comments)) {
                        foreach ($model->comments as $comment) {
                            echo $this->render('listview/comment-item', ['model' => $comment]);
                        }
                    }
                    else {
                        echo Yii::t('app', 'No comments yet');
                    } ?>
                </div>

                <?php if (!Yii::$app->user->isGuest) { ?>
                    <div class="news-comtitle"><?= Yii::t('app', 'Submit a Comment') ?></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'comment-form',
                        'action' => Url::to(['/comment/create'])
                    ]); ?>
                        <?= $form->field($postCommentForm, 'entityId')->hiddenInput(['value' => $model->id])->label(false) ?>
                        <?= $form->field($postCommentForm, 'comment')->textarea() ?>
                        <?= $form->field($postCommentForm, 'name', [
                            'template' => '<div class="row">
                                        <div class="col-md-6">
                                            {label}{input}{error}
                                        </div>
                                    </div>'
                        ])->textInput() ?>
                        <?= $form->field($postCommentForm, 'email', [
                            'template' => '<div class="row">
                                            <div class="col-md-6">
                                                {label}{input}{error}
                                            </div>
                                        </div>'
                        ])->textInput() ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Post Comment'), ['class' => 'btn-middle bright']) ?>
                        </div>
                    <?php ActiveForm::end();
                } ?>
            </div>
        </div>
        <aside class="news-aside col-md-4 col-sm-4 col-xs-12">
            <?php if ($relatedDataProvider->getCount() > 0) { ?>
                <div class="news-aside-block">
                    <div class="news-aside-title"><?= Yii::t('app', 'Related Articles') ?></div>
                    <?= ListView::widget([
                        'dataProvider' => $relatedDataProvider,
                        'itemView' => 'listview/related-article',
                        'options' => [
                            'class' => ''
                        ],
                        'itemOptions' => [
                            'class' => 'related-post row'
                        ],
                        'emptyText' => Yii::t('app', 'No related yet'),
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php }
            if ($jobsDataProvider->getCount() > 0) { ?>
                <div class="news-aside-block">
                    <div class="news-aside-title"><?= Yii::t('app', 'Related Jobs') ?></div>
                    <?= ListView::widget([
                        'dataProvider' => $jobsDataProvider,
                        'itemView' => 'listview/related-job',
                        'options' => [
                            'class' => ''
                        ],
                        'itemOptions' => [
                            'class' => 'related-post row'
                        ],
                        'emptyText' => Yii::t('app', 'No related yet'),
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php }
            if ($tendersDataProvider->getCount() > 0) { ?>
                <div class="news-aside-block">
                    <div class="news-aside-title"><?= Yii::t('app', 'Related Requests') ?></div>
                    <?= ListView::widget([
                        'dataProvider' => $tendersDataProvider,
                        'itemView' => 'listview/related-job',
                        'options' => [
                            'class' => ''
                        ],
                        'itemOptions' => [
                            'class' => 'related-post row'
                        ],
                        'emptyText' => Yii::t('app', 'No related yet'),
                        'layout' => "{items}"
                    ]) ?>
                </div>
            <?php }?>
        </aside>
    </div>
</div>
<?php
$postLikeUrl = Url::to(['/like/like-ajax']);
$subscribeUrl = Url::to(['/subscription/toggle']);
$thankYouText = Yii::t('account', 'Thank You!');
$subscribeText = Yii::t('app', 'Subscribe');
$unsubscribeText = Yii::t('app', 'Unsubscribe');

$script = <<<JS
    let sent = 0;

    $('.post-like').on('click', function() {
        if(sent === 0) {
            let entity = $(this).data('entity'),
                id = $(this).data('id');

            $.post("{$postLikeUrl}", {
                entity: entity,
                entity_id: id
            }, function(response) {                 
                sent = 1;
                if (response.success === true) {
                    $('a.post-like').html("{$thankYouText}");
                }
            });
        }

        return false;
    });
    
    $('.subscribe-button').on('click', function() {
        if(sent === 0) {
            let entity = $(this).data('entity'),
                id = $(this).data('id'),
                self = $(this);

            $.post("{$subscribeUrl}", {
                entity: entity,
                entity_id: id
            }, function(response) {
                if (response.success === true) {
                    self.toggleClass('active');
                    self.html('<i class="fa fa-rss"></i>&nbsp;' + (self.hasClass('active') ? '{$unsubscribeText}' : '{$subscribeText}'));
                }
            });
        }

        return false;
    });
JS;
$this->registerJs($script);
?>