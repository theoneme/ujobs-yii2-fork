<?php

use common\helpers\FileInputHelper;
use common\models\ContentTranslation;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\components\WizardSteps;
use frontend\models\PostArticleForm;
use kartik\file\FileInput;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $rootCategoriesList array
 * @var $model PostArticleForm
 * @var $locales array
 */
CropperAsset::register($this);
SelectizeAsset::register($this);
$this->title = Yii::t('app', 'Create Article');
?>

<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('app', 'Overview'),
    2 => Yii::t('app', 'Description'),
    3 => Yii::t('app', 'Publish')
], 'step' => 1]); ?>

<div class="profile">
    <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="mobile-note-title"></div>
                </div>
                <div class="modal-body">
                    <div class="mobile-note-text"></div>
                    <div class="mobile-note-btn text-center">
                        <a class="button big green" href="#"><?=Yii::t('app','Continue')?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'data-step' => 1,
                        'data-final-step' => 3
                    ],
                    'id' => 'page-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnChange' => false,
                    'validateOnBlur' => false,
                ]); ?>
                <?= Html::hiddenInput('id', $model->page->id, ['id' => 'page-id']) ?>
                <div class="tab-content nostyle-tab-content">
                    <div class="settings-content step tab-pane fade in active" id="step1" data-step="1">
                        <?php
                        $active = reset($locales);
                        echo Tabs::widget([
                            'items' =>
                                array_map(function ($locale, $language) use ($form, $model, $active) {
                                    return [
                                        'label' => $language,
                                        'content' => $this->render('_translation', [
                                            'model' => $model->translations[$locale] ?? new ContentTranslation(),
                                            'form' => $form,
                                            'locale' => $locale,
                                        ]),
                                        'active' => $locale === $active
                                    ];
                                }, $locales, array_intersect_key(Yii::$app->params['languages'], array_flip($locales)))
                        ]);
                        ?>
                        <div class="create-gig-block clearfix">
                            <?= Html::a(Yii::t('app', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray']) ?>
                            <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                        </div>
                    </div>
                    <div class="settings-content tab-pane step fade in form-gig file-input-step" id="step2" data-step="2">
                        <div class="formgig-block">
                            <div class="row set-item">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label" for="postarticleform-category_id"><?= Yii::t('app', 'Category') ?></label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                                    'prompt' => ' -- ' . Yii::t('app', 'Select A Category'),
                                                    'class' => 'form-control'
                                                ])->label(false); ?>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                                    'pluginOptions' => [
                                                        'initialize' => true,
                                                        'depends' => ['postarticleform-parent_category_id'],
                                                        'placeholder' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                        'url' => Url::to(['/ajax/subcat']),
                                                        'params' => ['depdrop-helper'],
                                                        'skipDep' => true,
                                                        'emptyMsg' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                    ],
                                                    'options' => [
                                                        'class' => 'form-control',
                                                    ]
                                                ])->label(false); ?>
                                                <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="suggestions-block" class="col-md-12">

                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                    <div class="notes-title"><?= Yii::t('app', 'Where will your article be?') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'Please select a category and subcategory most relevant to your article.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row set-item">
                                <?= $form->field($model, 'tags', [
                                    'template' => '
								    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
								        <div class="set-title">
								            {label}<div class="mobile-question" data-toggle="modal"></div>
								        </div>
								    </div>
								    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
								        {input}{error}
								        <div class="counttext"> ' . Yii::t('app', 'We recommend using at least 3 tags') . '</div>
								    </div>
								'
                                ])->textInput([
                                    'id' => 'tags',
                                    'class' => 'readsym'
                                ]) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('app', 'Tags, tags, tags!') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'These are the words by which customers can find your offer.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row formgig-block show-notes">
                            <div class="form-group">
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title">
                                        <label class="control-label" for="postarticleform-uploaded_images"><?= Yii::t('app', 'Photos') ?></label>
                                        <!--<?= Yii::t('app', '(Here you can upload photos)') ?>-->
                                        <div class="mobile-question" data-toggle="modal"></div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                    <div class="cgp-upload-files">
                                        <?= FileInput::widget(
                                            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                'id' => 'file-upload-input',
                                                'pluginOptions' => [
                                                    'overwriteInitial' => true,
                                                    'initialPreview' => !empty($model->image) ? $model->getThumb() : [],
                                                    'initialPreviewConfig' => !empty($model->image) ? [[
                                                        'caption' => basename($model->image),
                                                        'url' => Url::toRoute('/image/delete'),
                                                        'key' => 'image_init_' . $model->id
                                                    ]] : [],
                                                ]
                                            ])
                                        )?>
                                        <div class="images-container">
                                            <?= $form->field($model, "image")->hiddenInput([
                                                'value' => $model->image,
                                                'data-key' => 'image_init_' . $model->id
                                            ])->label(false);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Photos') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'It’s photo time! You can upload as many images as you want.') ?>
                                        </p>
                                        <ul>
                                            <li><?= Yii::t('app', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                            <li><?= Yii::t('app', 'Dimensions: more - better') ?></li>
                                            <li><?= Yii::t('app', 'Max File size: {count} MB', ['count' => 5]) ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="create-gig-block">
                            <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                            <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                        </div>
                    </div>
                    <div class="settings-content tab-pane fade in" id="step3" data-step="3">
                        <div class="publish-title text-center">
                            <?= Yii::t('app', 'It\'s almost ready...') ?>
                        </div>
                        <div class="publish-text text-center">
                            <?= Yii::t('app', "Let`s publish your article and <br/> attract the attention of buyers.") ?>
                        </div>
                        <div class="create-gig-block row">
                            <div class="col-md-12">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Publish article'), ['class' => 'btn-mid fright']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?= $this->render('@frontend/views/common/crop-bg.php')?>

</div>

<?php
$userId = Yii::$app->user->identity->getId();
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'page_tag']);
$categorySuggestRoute = Url::toRoute('/ajax/category-suggest');
$addTagMessage = Yii::t('app', 'Add tag');

$script = <<<JS
    $('body').on('click','.mobile-question',function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click','.mobile-note-btn a',function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });

	let uid = $userId;
	
	$("#page-form").on("beforeValidateAttribute", function (event, attribute, messages) {
		let current_step = $(this).data("step"),
		    input_step = $(attribute.container).closest(".step").data("step");
		if (current_step !== input_step) {
			return false;
		}
	}).on("afterValidate", function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
			let current_step = $(this).data("step");
            let firstError = $(this).find("div[data-step='" + current_step + "'] .has-error").first();
            let firstErrorTab = firstError.closest('.tab-pane');
            if (firstErrorTab.length) {
                $('a[href="#' + firstErrorTab.attr('id') + '"]').tab('show');
            }
            $("html, body").animate({
                scrollTop: firstError.offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function (event) {
		let current_step = $(this).data("step"),
		    self = $(this);
		if ($(this).find("div[data-step='" + current_step + "']").hasClass("file-input-step")) {
		    let returnValue = true;
		    $(".file-upload-input").each(function(index){
                if ($(this).fileinput("getFilesCount") > 0) {
                    $(this).fileinput("upload");
                    returnValue = false;
                    return false;
                }
		    });
		    if (!returnValue) {
		        return false;
		    }
        }
		if (current_step !== $(this).data("final-step")) {
            self.find("div[data-step='" + current_step + "']").removeClass("active");
            $(".gig-steps li").removeClass("active");
            current_step++;
            self.data("step", current_step);
            self.find("div[data-step='" + current_step + "']").addClass("active");
            $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
			return false;
		}
	});

    $(".prev-step").on("click", function() {
    	let pageForm = $("#page-form"),
		    current_step = pageForm.data("step");
    	
		if (current_step > 1) {
			pageForm.find("div[data-step='" + current_step + "']").removeClass("active");
			$(".gig-steps li").removeClass("active");
			current_step--;
			pageForm.data("step", current_step);
			pageForm.find("div[data-step='" + current_step + "']").addClass("active");
			$(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
			$("html, body").animate({
				scrollTop: 0
			}, "slow");
		}
		return false;
    });
    
    let suggestTimer = null;
    $(document).on("keyup", "[data-action=category-suggest]", function() {
        let self = $(this);
        clearTimeout(suggestTimer);
        suggestTimer = setTimeout(function() {
            let val = self.val();
            $.get('{$categorySuggestRoute}',
                {entity: 'job', request: val},
                function(data) {
                    if (data.success === true) {
                        $('#suggestions-block').html(data.html);
                    } else {
                        $('#suggestions-block').html('');
                    }
                },
                "json"
            );
        }, 700);
    });
    
    $(document).on('click', '[data-action=suggestion-select]', function() {
        let parentCategoryInput = $('#postarticleform-parent_category_id');
        let categoryInput = $('#postarticleform-category_id');
        let subcategoryInput = $('#postarticleform-subcategory_id');
        let pId = $(this).data('p-id');
        let cId = $(this).data('c-id');
        let sId = $(this).data('s-id');
        
        categoryInput.attr('data-c-id', cId);
        subcategoryInput.attr('data-s-id', sId);
        parentCategoryInput.val(pId).trigger('change');
        
        return false;
    });
    
    $('#postarticleform-category_id').on('depdrop:afterChange', function(event, id, value) {
        let cId = $(this).attr('data-c-id');
        if(cId) {
            $('#postarticleform-category_id').val(cId).trigger('change');
        }
    });
    
    $('#postarticleform-subcategory_id').on('depdrop:afterChange', function(event, id, value) {
        let sId = $(this).attr('data-s-id');
        if(sId) {
            $('#postarticleform-subcategory_id').val(sId).trigger('change');
        }
    });

    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $('.images-container');
        imagesContainer.find("input").remove();
        imagesContainer.append("<input class='image-source' name='PostArticleForm[image]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    }).on("filedeleted", function(event, key) {
        $(".images-container").find("input[data-key='" + key + "']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $("#page-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });

    $(document).on("click", ".link-blue", function() {
        $(this).closest(".cgb-optbox").remove();
    });

    $("#tags").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class='create'>$addTagMessage&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$tagListUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                    callback();
                }
            );
        }
    });
JS;

$this->registerJs($script);