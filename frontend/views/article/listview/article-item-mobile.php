<?php

use common\models\Page;
use yii\helpers\Html;

/**
 * @var $model Page
 */
?>

<div class="mob-work-table">
    <?= Html::a(
        Html::img($model->getThumb('catalog'), ['alt' => Html::encode($model->getLabel()),'title' => Html::encode($model->getLabel())]),
        ["/news/view", 'alias' => $model->alias],
        ['class' => 'mob-work-img']
    )?>
    <div class="mob-work-info">
        <?= Html::a(
            Html::encode($model->getLabel()),
            ["/news/view", 'alias' => $model->alias],
            ['class' => 'bf-descr']
        )?>
        <div class="rate">
            <?= Yii::t('app', '{count} comments', ['count' => count($model->comments)]) ?>
        </div>

        <div class="mob-block-bottom row">
            <div class="col-md-12">
                <?= Yii::t('app', 'uJobs Team') ?>
                <?= Yii::$app->formatter->asDate($model->publish_date) ?>
            </div>
        </div>
    </div>
</div>