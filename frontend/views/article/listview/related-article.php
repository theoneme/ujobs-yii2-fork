<?php

use common\models\Page;
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;

/**
 * @var $model Page
 */
?>

<div class="related-post-img">
    <?= Html::a(Html::img($model->getThumb(), ['alt' => Html::encode($model->getLabel()),'title' => Html::encode($model->getLabel())]), $model->getUrl())?>
</div>
<?= Html::a(Html::encode($model->getLabel()), $model->getUrl(), ['class' => 'related-alias'])?>
<p>
    <?= BaseStringHelper::truncate(strip_tags($model->getContent()), 75)?>
</p>