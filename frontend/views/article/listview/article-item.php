<?php

use common\models\elastic\PageElastic;
use common\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model Page|PageElastic
 */
?>

<div class="block-top">
    <div class="block-top-ava">
        <?= Html::img($model->getSellerThumb('catalog'),['alt' => Yii::t('app', 'uJobs Team'), 'title' => Yii::t('app', 'uJobs Team')]) ?>
    </div>
    <div class="bt-name-rate">
        <div class="by">
            <?= Yii::t('app', 'uJobs Team') ?>
        </div>
        <div class="rate">
            <?= Yii::t('app', '{count} comments', ['count' => count($model->comments)]) ?>
        </div>
    </div>
</div>
<a class="bf-alias" href="<?= Url::to(["/news/view", 'alias' => $model->alias]) ?>">
    <div class="bf-img">
        <?= Html::img($model->getThumb('catalog'), ['alt' => Html::encode($model->getLabel()), 'title' => Html::encode($model->getLabel()), 'style' => 'height: 100%; width: auto;'])?>
    </div>
    <div class="bf-descr">
        <?= Html::encode($model->getLabel()) ?>
    </div>
</a>
<div class="block-bottom">
    <div class="markers">
        <div class="stick text-center feat"><?= Yii::$app->formatter->asDate($model->publish_date) ?></div>
    </div>
    <?= Yii::$app->formatter->asDate($model->publish_date) ?>
</div>