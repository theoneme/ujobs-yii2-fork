<?php

use common\models\Comment;
use yii\helpers\Html;

/* @var Comment $model */

?>

<div class="comment-news">
    <div class="cn-date"><?= Yii::$app->formatter->asDate($model->created_at) ?></div>
    <div class="row cn-author-row">
        <div class="cn-author"><?= Html::encode($model->name) ?></div>
    </div>
    <div class="nc-text">
        <p>
            <?= nl2br(Html::encode($model->comment)) ?>
        </p>
    </div>
</div>