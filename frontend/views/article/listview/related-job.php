<?php

use common\models\Job;
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model Job
 */
$entity = $model->type === Job::TYPE_JOB ? Job::TYPE_JOB : Job::TYPE_TENDER;
$price = $model->contract_price ? '<span>' . Yii::t('app', 'contract') . '</span>' : $model->getPrice();
if ($model->type == Job::TYPE_ADVANCED_TENDER && !$model->contract_price) {
    $price .= ' ' . Yii::t('app', 'per month');
}
?>

<div class="related-post-img">
    <?= Html::a(Html::img($model->getThumb(), ['alt' => Html::encode($model->getLabel()),'title' => Html::encode($model->getLabel())]), ["/{$entity}/view", 'alias' => $model->alias])?>
</div>
<?= Html::a(Html::encode($model->getLabel()), ["/{$entity}/view", 'alias' => $model->alias], ['class' => 'related-alias'])?>
<p>
    <?= Yii::t('app', 'Price') . ': ' . $price?>
</p>