<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Page;

/* @var Page $model */

?>

<a href="<?=Url::to(['/news/view', 'alias' => $model->alias])?>" class="news-list-alias">
    <div class="news-list-img">
        <?= Html::img($model->getThumb(), ['alt' => $model->getLabel(),'title' => $model->getLabel()])?>
        <div class="news-list-descr">
            <?= $model->getLabel()?>
        </div>
    </div>
</a>