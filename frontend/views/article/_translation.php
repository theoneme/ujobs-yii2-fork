<?php

use common\helpers\ImperaviHelper;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $locale string */
/* @var $form ActiveForm */
/* @var $group $string */

?>

<?= $form->field($model, "[$locale]id")->hiddenInput()->label(false); ?>
<?= $form->field($model, "[$locale]locale")->hiddenInput(['value' => $locale])->label(false); ?>
<div class="formgig-block">
    <div class="row set-item">
        <?= $form->field($model, "[$locale]title", [
            'template' => '
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        {label}
                        <div class="mobile-question" data-toggle="modal"></div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    {input}
                    {error}
                    <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('app', 'Max') . '</div>
                </div>
            '
        ])->textarea([
            'class' => 'readsym bigsymb',
            'placeholder' => Yii::t('app', 'Name your article'),
            'data-action' => 'category-suggest'
        ])
        ?>
    </div>
    <div class="notes col-md-6 col-md-offset-12">
        <div class="notes-head">
            <div class="lamp text-center">
                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
            </div>
            <div class="notes-title"><?= Yii::t('app', 'Name your article') ?></div>
        </div>
        <div class="notes-descr">
            <p>
                <?= Yii::t('app', 'This is your article`s title. Choose wisely, you can only use 80 characters.') ?>
            </p>
        </div>
    </div>
</div>
<div class="formgig-block">
    <div class="row set-item">
        <?= $form->field($model, "[$locale]content", [
            'template' => '
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    {label}<div class="mobile-question" data-toggle="modal"></div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                {input}
                {error}
            </div>
        '])->widget(Widget::class, ImperaviHelper::getDefaultConfig());
        ?>
    </div>
    <div class="notes col-md-6 col-md-offset-12">
        <div class="notes-head">
            <div class="lamp text-center">
                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
            </div>
            <div class="notes-title"><?= Yii::t('app', 'Text of your article') ?></div>
        </div>
        <div class="notes-descr">
            <p>
                <?//= Yii::t('app', 'Try to describe your service with as many details as possible, if you provide many details more possibilities you will have to be hired.') ?>
            </p>
        </div>
    </div>
</div>