<?php

use common\models\user\RecoveryForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var ActiveForm $form
 * @var RecoveryForm $model
 */

$this->title = Yii::t('app', 'Reset Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reset-block" style="margin-top: 30px;">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    <div class="reset-form">
        <?php $form = ActiveForm::begin([
            'id' => 'password-recovery-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'New Password')])->label(false) ?>

        <?= $form->field($model, 'confirm')->passwordInput(['placeholder' => Yii::t('app', 'Confirm Password')])->label(false) ?>

        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-8 col-xs-7" style="font-weight: bold">
                <?= Yii::t('app', '6 characters or more. We recommend using uppercase and lowercase letters and numbers.') ?>
            </div>
            <div class="col-sm-4 col-xs-5 text-right">
                <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn-middle']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>