<?php
use common\helpers\YandexHelper;
use common\modules\store\models\Order;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var Order $order */
?>

<?php $form = ActiveForm::begin([
    'action' => 'https://money.yandex.ru/quickpay/confirm.xml'
]); ?>

<?= Html::hiddenInput('receiver', YandexHelper::$wallet) ?>
<?= Html::hiddenInput('formcomment', Yii::t('order', 'Payment for service on {0} site', Yii::$app->name)) ?>
<?= Html::hiddenInput('short-dest', $order->total) ?>
<?= Html::hiddenInput('targets', 'Test') ?>
<?= Html::hiddenInput('quickpay-form', 'shop') ?>
<?= Html::hiddenInput('paymentType', 'PC') ?>
<?= Html::hiddenInput('label', $order->id) ?>
<?= Html::hiddenInput('sum', $order->total) ?>
<?php ActiveForm::end(); ?>