<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.03.2017
 * Time: 16:58
 */

use common\components\CurrencyHelper;
use common\models\Payment;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var Payment $payment */
?>

<?php $form = ActiveForm::begin([
    'action' => \yii\helpers\Url::to(['/invoice-payment/invoice']),
    'id' => 'invoice-form',
    'method' => 'post'
]); ?>

<?= Html::hiddenInput('order_id', $payment->id) ?>
<?= Html::hiddenInput('description', $payment->description) ?>
<?= Html::hiddenInput('total', CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total)) ?>
<?php ActiveForm::end(); ?>