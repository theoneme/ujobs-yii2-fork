<?php
use common\helpers\PaypalHelper;
use common\models\Payment;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var Payment $payment */
?>

<?php $form = ActiveForm::begin([
//  'action' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
  'action' => 'https://www.paypal.com/cgi-bin/webscr',
  'method' => 'post'
]); ?>

  <?= Html::hiddenInput('cmd', '_xclick') ?>
  <?= Html::hiddenInput('charset', 'UTF-8') ?>
  <?= Html::hiddenInput('item_name', $payment->description) ?>
  <?= Html::hiddenInput('item_number', $payment->id) ?>
  <?= Html::hiddenInput('amount', $payment->total) ?>
  <?= Html::hiddenInput('currency_code', $payment->currency_code) ?>
  <?= Html::hiddenInput('business', PaypalHelper::$login) ?>
  <?= Html::hiddenInput('return', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
  <?= Html::hiddenInput('notify_url', Yii::$app->urlManager->createAbsoluteUrl(['/payment/paypal-callback'])) ?>
  <div class="form-group">
    <?= Html::submitButton('Оплатить', ['class' => 'btn btn-blue']) ?>
  </div>
<?php ActiveForm::end(); ?>