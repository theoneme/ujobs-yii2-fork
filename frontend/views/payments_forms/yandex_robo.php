<?php
use common\components\CurrencyHelper;
use common\helpers\RobokassaHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use common\models\Payment;

/** @var Payment $payment */
$sign = md5(RobokassaHelper::$client_id . ":" . CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total) . ":" . $payment->id . ":" . RobokassaHelper::$password1);
?>

<?php $form = ActiveForm::begin([
    'action' => 'https://auth.robokassa.ru/Merchant/Index.aspx',
    'method' => 'post'
]); ?>

<?= Html::hiddenInput('MrchLogin', RobokassaHelper::$client_id) ?>
<?= Html::hiddenInput('OutSum', CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total)) ?>
<?= Html::hiddenInput('InvId', $payment->id) ?>
<?= Html::hiddenInput('Desc', $payment->description) ?>
<?= Html::hiddenInput('SignatureValue', $sign) ?>
<?= Html::hiddenInput('IncCurrLabel', RobokassaHelper::$yandexLabel) ?>
<?//= Html::hiddenInput('IsTest', 1) ?>

<?= Html::hiddenInput('successUrl', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
<?= Html::hiddenInput('failUrl', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
<?php ActiveForm::end(); ?>