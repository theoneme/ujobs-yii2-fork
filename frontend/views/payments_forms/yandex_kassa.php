<?php
use common\components\CurrencyHelper;
use common\helpers\YandexHelper;
use common\models\Payment;
use common\models\user\User;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * @var Payment $payment
 * @var User $user
 */
$user = Yii::$app->user->identity;
?>

<?php $form = ActiveForm::begin([
  'action' => 'https://money.yandex.ru/eshop.xml'
]); ?>
    <?= Html::hiddenInput('shopId', YandexHelper::$shopId) ?>
    <?= Html::hiddenInput('scid', YandexHelper::$scid) ?>
    <?= Html::hiddenInput('sum', CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total)) ?>
    <?= Html::hiddenInput('customerNumber', $user->id) ?>
    <?= Html::hiddenInput('orderNumber', $payment->id) ?>
    <?= !empty($user->phone) ? Html::hiddenInput('cps_phone', $user->phone) : '' ?>
    <?= !empty($user->email) ? Html::hiddenInput('cps_email', $user->email) : '' ?>
    <?= Html::hiddenInput('shopSuccessURL', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
    <?= Html::hiddenInput('shopFailURL', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
<?php ActiveForm::end(); ?>
