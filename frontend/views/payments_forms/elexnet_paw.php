<?php
use common\components\CurrencyHelper;
use common\helpers\PayAnyWayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use common\models\Payment;

/** @var Payment $payment */
$total = number_format(CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total), 2, '.', '');
$sign = md5(PayAnyWayHelper::$shopId . $payment->id . $total .
    'RUB' . $payment->user_id . PayAnyWayHelper::$testMode . PayAnyWayHelper::$integrityCode);
?>

<?php $form = ActiveForm::begin([
    'action' => 'https://www.payanyway.ru/assistant.htm',
    'method' => 'post'
]); ?>

<?= Html::hiddenInput('MNT_ID', PayAnyWayHelper::$shopId) ?>
<?= Html::hiddenInput('MNT_AMOUNT', $total) ?>
<?= Html::hiddenInput('MNT_TRANSACTION_ID', $payment->id) ?>
<?= Html::hiddenInput('MNT_CURRENCY_CODE', 'RUB') ?>
<?= Html::hiddenInput('MNT_DESCRIPTION', $payment->description) ?>
<?= Html::hiddenInput('MNT_SUBSCRIBER_ID', $payment->user_id) ?>
<?= Html::hiddenInput('MNT_SIGNATURE', $sign) ?>
<?= Html::hiddenInput('MNT_TEST_MODE', PayAnyWayHelper::$testMode) ?>
<?= Html::hiddenInput('paymentSystem.unitId', PayAnyWayHelper::$elexnetId) ?>

<?= Html::hiddenInput('MNT_SUCCESS_URL', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
<?= Html::hiddenInput('MNT_INPROGRESS_URL', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
<?= Html::hiddenInput('MNT_FAIL_URL', Yii::$app->urlManager->createAbsoluteUrl(['/payment/return', 'payment_id' => $payment->id])) ?>
<?= Html::hiddenInput('MNT_RETURN_URL', Yii::$app->urlManager->createAbsoluteUrl(['/shop/checkout/cart'])) ?>
<?php ActiveForm::end(); ?>