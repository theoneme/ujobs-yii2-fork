<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 12:49
 */

use common\components\CurrencyHelper;
use common\models\Payment;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var Payment $payment */
?>

<?php $form = ActiveForm::begin([
    'action' => \yii\helpers\Url::to(['/sberbank-payment/sberbank'])
]); ?>

<?= Html::hiddenInput('order_id', $payment->id) ?>
<?= Html::hiddenInput('description', $payment->description) ?>
<?= Html::hiddenInput('total', CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total)) ?>
<?php ActiveForm::end(); ?>