<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use common\models\Payment;

/** @var Payment $payment */
?>

<?php $form = ActiveForm::begin(['action' => ['/payment/balance'], 'method' => 'post']); ?>
    <?= Html::hiddenInput('payment_id', $payment->id) ?>
<?php ActiveForm::end(); ?>