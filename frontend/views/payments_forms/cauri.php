<?php

use common\models\Payment;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var Payment $payment */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/payment/cauri-form']),
    'method' => 'post'
]); ?>

<?= Html::hiddenInput('paymentId', $payment->id) ?>
<?php ActiveForm::end(); ?>