<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.09.2016
 * Time: 18:46
 */

use common\models\Page;
use yii\helpers\Html;

/**
 * @var $page Page
 * @var $breadcrumbs array
 */

?>

<div class="container">
    <?= yii\widgets\Breadcrumbs::widget([
        'itemTemplate' => "{link}<span></span>", // template for all links
        'activeItemTemplate' => "{link}",
        'links' => $breadcrumbs,
        'options' =>
            [
                'class' => 'breadcrumbs'
            ],
        'tag' => 'div'
    ]); ?>

    <div class="title">
        <h1 class="text-center"><?= Html::encode($page->getLabel()) ?></h1>
    </div>
    <div class="text">
        <?= Html::encode($page->getContent()) ?>
    </div>
</div>