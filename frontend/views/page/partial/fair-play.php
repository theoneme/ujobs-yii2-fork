<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.02.2017
 * Time: 15:16
 */

use yii\helpers\Html;
use common\models\Page;



/* @var Page $page */

?>

<div class="news-content">
    <h1 class="profileh text-left">
        <?= Html::encode($page->getLabel()) ?>
    </h1>
    <?= $page->getContent() ?>
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 fair-play text-center">
            <div class="fp-img">
                <?= Html::img('/images/new/icons/safe1.svg', ['alt' => Html::encode($page->getLabel()),'title' => Html::encode($page->getLabel())]) ?>
            </div>
            <div class="fp-text">
                <?= Yii::t('app', 'fair-play-text-1') ?>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 fair-play text-center">
            <div class="fp-img">
                <?= Html::img('/images/new/icons/safe2.svg', ['alt' => Html::encode($page->getLabel()),'title' => Html::encode($page->getLabel())]) ?>
            </div>
            <div class="fp-text">
                <?= Yii::t('app', 'fair-play-text-2') ?>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 fair-play text-center">
            <div class="fp-img">
                <?= Html::img('/images/new/icons/safe3.svg', ['alt' => Html::encode($page->getLabel()),'title' => Html::encode($page->getLabel())]) ?>
            </div>
            <div class="fp-text">
                <?= Yii::t('app', 'fair-play-text-3') ?>
            </div>
        </div>
    </div>
</div>