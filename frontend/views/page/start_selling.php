<?php

use common\models\Page;
use common\models\user\Profile;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use common\models\Job;


/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $page Page
 * @var $cheapest Job
 * @var $mostExpensive Job
 * @var $profilesDataProvider ActiveDataProvider
 * @var $buyersDataProvider ActiveDataProvider
 */

frontend\assets\WorkerAsset::register($this);
$this->registerCssFile('/css/new/category.css');

// Ееее, быдлокод
if (!empty($page->banner)) {
    $this->registerCss("
        .start-selling .banner {
            background:url('" . Yii::$app->mediaLayer->getThumb($page->banner) . "') no-repeat center/ cover !important;
        }
    ");
}
if (!empty($page->banner_small)) {
    $this->registerCss("
        @media screen and (max-width: 768px) {
            .start-selling .banner {
                background:url('" . Yii::$app->mediaLayer->getThumb($page->banner_small) . "') no-repeat center/ cover !important;
            }
        }
    ");
}

$translation = $page->translations[Yii::$app->language] ?? array_values($page->translations)[0];
?>

<div class="start-selling">
    <div class="row">
        <div class="banner bsmall" style="">
            <div class="dark-overlay"></div>
            <div class="content-on-overlay container">

                <div class="btext-left text-left">
                    <?= yii\widgets\Breadcrumbs::widget([
                        'itemTemplate' => "{link}<span></span>", // template for all links
                        'activeItemTemplate' => "{link}",
                        'links' => [
                            ['label' => Yii::t('account', 'Start Earning')]
                        ],
                        'options' =>['class' => 'breadcrumbs text-left'],
                        'tag' => 'div'
                    ]); ?>
                </div>

                <div class="btext-left text-left">
                    <h1 class="cat-title text-left">
                        <?= $translation->title ?>
                    </h1>

                    <div class="banner-descr text-left">
                        <?= $translation->content_prev ?>
                    </div>
                    <br>
                    <?php if (Yii::$app->user->isGuest) {
                        echo Html::a(Yii::t('account', 'Start Registration'), null,[
                            'class' => 'btn-big',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalSignup'
                        ]);
                    }
                    else {
                        echo Html::a(Yii::t('account', 'Start Registration'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id],[
                            'class' => 'btn-big'
                        ]);
                    }?>
                    <a class="how-to-work" href="https://ujobs.me/news/instruktsia-po-rabote-s-ujobs">
                        <?= Yii::t('app', 'Guide for working on site') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="video-bottom row">
        <div class="container-fluid">
            <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                <?= Yii::t('account', 'Order every')?><br/>
                <span><?= Yii::t('account', '{0} sec', 5)?></span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                <?= Yii::t('account', 'Projects Completed')?><br/>
                <span>200 000</span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                <?= Yii::t('account', 'Price Range')?><br/>
                <?= $cheapest->getPrice() . ' - ' . $mostExpensive->getPrice()?>
            </div>
        </div>
    </div>

    <div id="benefits2-block" class="container page-content">
        <div class="row">
            <div class="page-content-title col-md-6 col-md-offset-3 text-center">
                <?= Yii::t('app', 'How it works')?>
            </div>
        </div>

        <div class="summary page-content-text text-center">
            <p class="text-center">
                <?= Yii::t('app', 'This site unites Professionals and Customers. You need to register and publish the work, and we will advertise it and find new Clients.') ?>
            </p>
        </div>
    </div>

    <div class="container-fluid three-blocks-container">
        <div class="three-blocks row">
            <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                <a>
                    <div class="about-img" style="background: url(/images/new/icons/man.png); background-size: cover;"></div>
                    <div class="three-title"><?= Yii::t('app', 'Sign up')?></div>
                    <p class="text-center">
                        <?= Yii::t('app', 'Sign up with us on the site and tell us about yourself, add photos, in general, as always.')?>
                    </p>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                <a>
                    <div class="about-img" style="background: url(/images/new/icons/clipboard.png); background-size: cover;"></div>
                    <div class="three-title"><?= Yii::t('app', 'Place a service')?></div>
                    <p class="text-center">
                        <?= Yii::t('app', 'Describe in the section "Create a Service" what you know how to do, and set the price. Publish.')?>
                    </p>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                <a>
                    <div class="about-img" style="background: url(/images/new/icons/money-bag.png); background-size: cover;"></div>
                    <div class="three-title"><?= Yii::t('app', 'Receive orders')?></div>
                    <p class="text-center">
                        <?= Yii::t('app', 'We carry out advertising services and constantly bring you new customers. So you earn your money.')?>
                    </p>
                </a>
            </div>
        </div>
    </div>

    <div class="bs-block white-block hidden-xs">
        <h2 class="text-center"><?= Yii::t('account', 'JOIN OUR GROWING FREELANCE COMMUNITY')?></h2>
        <?php if(!isPagespeed()) { ?>
            <?= ListView::widget([
                'dataProvider' => $profilesDataProvider,
                'itemView' => '@frontend/modules/account/views/profile/listview/ss_profile',
                'options' => [
                    'class' => 'container-fluid'
                ],
                'itemOptions' => [
                    'class' => ''
                ],
                'viewParams' => [
                    'count' => $profilesDataProvider->getTotalCount(),
                    'keywords' => $translation->seo_keywords
                ],
                'layout' => "{items}"
            ]) ?>
        <?php } ?>
    </div>

    <div class="mobile-people-over visible-xs">
        <?php if(!isPagespeed()) { ?>
            <?= ListView::widget([
                'dataProvider' => $profilesDataProvider,
                'itemView' => '@frontend/modules/account/views/profile/listview/ss_profile_mobile',
                'options' => [
                    'class' => '',
                    'id' => 'mobile-people'
                ],
                'itemOptions' => [
                    'class' => 'top-mobile-seller'
                ],
                'viewParams' => [
                    'keywords' => $translation->seo_keywords
                ],
                'layout' => "{items}"
            ]) ?>
        <?php } ?>
    </div>

    <div class="ss-grey-block">
        <div class="container-fluid">
            <h2 class="text-center"><?= Yii::t('account', 'HOW IT WORKS')?></h2>
            <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
                <div class="how-work-title text-center">
                    1. <?= Yii::t('account', 'Publish job')?>
                </div>
                <p class="text-center">
                    <?= Yii::t('account', 'Sign up for free, set up your profile and offer your services to our global audience.')?>
                </p>
            </div>
            <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
                <div class="how-work-title text-center">
                    2. <?= Yii::t('account', 'Deliver Great Work')?>
                </div>
                <p class="text-center">
                    <?= Yii::t('account', 'Get notified when you get an order and use our system to discuss details with customers.')?>
                </p>
            </div>
            <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
                <div class="how-work-title text-center">
                    3. <?= Yii::t('account', 'Get Paid')?>
                </div>
                <p class="text-center">
                    <?= Yii::t('account', 'You will receive payment on time, we guarantee it. The reward will be credited to your balance after completing the order.')?>
                </p>
            </div>
        </div>
    </div>

    <div class="white-block">
        <div class="container-fluid">
            <h2 class="text-center"><?= Yii::t('account', 'HISTORY OF OUR PROFESSIONALS')?></h2>
            <?php if(!isPagespeed()) { ?>
                <?= ListView::widget([
                    'dataProvider' => $buyersDataProvider,
                    'itemView' => '@frontend/modules/account/views/profile/listview/ss_buyer',
                    'options' => [
                        'class' => 'row'
                    ],
                    'itemOptions' => [
                        'class' => 'col-md-6 col-sm-6 col-xs-12 buyer-stories'
                    ],
                    'viewParams' => [
                        'keywords' => $translation->seo_keywords
                    ],
                    'layout' => "{items}"
                ]) ?>
            <?php } ?>
        </div>
    </div>

    <div class="ss-grey-block">
        <div class="container-fluid">
            <h2 class="text-center"><?= Yii::t('account', 'Q&A')?></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 qa">
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('account', 'What services can I offer?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('account', 'Be creative! Think about what you can do best. Offer any services if they are legitimate and comply with our rules. The site presents more than 100 categories for placement of works. Study them: maybe this will give you an idea.')?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('account', 'How much can I earn?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('account', 'It totally depends on you. You can work without time limits. Many Performers stay on Ujobs during the working day, some combine their permanent job with freelancing to get extra money. Average tariffs range from 500 - 3000 rubles per hour, the price depends on the client and your qualification.')?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('account', 'How much does it cost? Wich is the commission?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('account', 'You can join Ujobs absolutely free of charge. No subscription or other costs. You get {percent1}% of each transaction. Our commission is {percent2}% of the amount of payment made, and it should be included in the cost that you set for your work.', ['percent1' => 80, 'percent2' => 20])?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 qa">
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('account', 'How much time will I have to invest?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('account', 'It’s very flexible. You need to put in some time and effort in the beginning to learn the marketplace and then you can decide for yourself what amount of work you want to do.')?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('account', 'How should I offer my service?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('account', 'In the standard services section, you can assign any price from {minPrice} to {maxPrice} and offer three service options with different cost. When setting tariffs, be as realistic as possible, and think about whether customers are ready to buy services for your price.')?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('account', 'How do I get paid?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('account', 'Once you complete a buyer’s order, the money is transferred to your account. You don`t need to chase clients for payments and wait 60 or 90 days for a check.')?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="white-block">
        <div class="container-fluid">
            <div class="get-started text-center">
                <p class="text-center"><?= Yii::t('account', 'Sign up and create your first job')?></p>
                <?php if (Yii::$app->user->isGuest) {
                    echo Html::a(Yii::t('account', 'Get Started'), null,[
                        'class' => 'btn-big',
                        'data-toggle' => 'modal',
                        'data-target' => '#ModalSignup'
                    ]);
                }
                else {
                    echo Html::a(Yii::t('account', 'Get Started'), ['/account/profile/show', 'id' => Yii::$app->user->identity->id],[
                        'class' => 'btn-big'
                    ]);
                }?>
            </div>
        </div>
    </div>

</div>