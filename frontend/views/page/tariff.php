<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 27.02.2017
 * Time: 13:45
 */

use common\components\CurrencyHelper;
use common\models\Page;
use yii\helpers\Html;

\frontend\assets\TariffAsset::register($this);

/* @var Page $page */

?>

<div class="container-fluid">
    <div class="tariff-content">
        <h1 class="profileh text-center"><?= Yii::t('app', 'Tariffs of uJobs')?></h1>
        <p class="text-center"><?= Yii::t('app', 'Help you achieve better results in work')?></p>
        <div class="tariff-buttons text-center">
            <a class="btn-white text-center" href="#performer"><?= Yii::t('app','Customers'); ?></a>
            <a class="btn-white text-center" href="#customer"><?= Yii::t('app','For freelancers and sellers'); ?></a>
        </div>
        <div class="profileh text-center"><?= Yii::t('app', 'Premium tariffs for Freelancers and Sellers')?></div>
        <p class="text-center"><?= Yii::t('app', 'Let you be the first to learn about applications, pay a reduced commission and receive the best bids')?></p>
        <div class="tariff-block" id="customer">
            <div class="tariff-box tariff-white">
                <div class="tariff-head">
                    <div class="tariff-head-center text-center">
                        <?= Yii::t('app','Description')?>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <?= Yii::t('app','Posting of announcements in the catalog')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app','Write to Customer or receive messages from Customer')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app','Reply to the request')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app','Show in the catalog')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app','Ability to respond to new requests from customers')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app','Percentage of the system commission from earned money')?>
                    </div>
                    <div class="tariff-item"><?= Yii::t('app','Tariff')?></div>
                    <div class="center-button text-center"></div>
                </div>
            </div>

            <div class="tariff-box tariff-red">
                <div class="tariff-head">
                    <div class="tariff-title text-center"><?= Yii::t('app','Student')?></div>
                    <div class="tariff-price text-center">
                        <span><?= Yii::t('app','Free')?></span>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Posting of announcements in the catalog')?> —
                        </div>
                        <?= Yii::t('app','1 announcement')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Write to Customer or receive messages from Customer')?> —
                        </div>
                        <?= Yii::t('app','One new customer per day')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Reply to the request')?> —
                        </div>
                        <?= Yii::t('app','There is no possibility')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Show in the catalog')?> —
                        </div>
                        <?= Yii::t('app','After Masters')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Ability to respond to new requests from customers')?> —
                        </div>
                        <?= Yii::t('app','{hours} hours after publication', ['hours' => 48])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Percentage of the system commission from earned money')?> —
                        </div>
                        20%
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Tariff')?> —
                        </div>
                        <?= Yii::t('app','Free')?>
                    </div>
                    <div class="center-button text-center">
                        <!-- <?= Html::a(Yii::t('app', 'Choose tariff'), ['/store/cart/tariff', 'id' => 1], ['class' => 'btn-big']) ?> -->
                    </div>
                </div>
            </div>

            <div class="tariff-box tariff-blue">
                <div class="tariff-head">
                    <div class="tariff-title text-center"><?= Yii::t('app','Master')?></div>
                    <div class="tariff-price text-center">
                        <span><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'],15, true)?></span>
                        <?= Yii::t('app','per day')?>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Posting of announcements in the catalog')?> —
                        </div>
                        <?= Yii::t('app','Up to {count} announcements', ['count' => 3])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Write to Customer or receive messages from Customer')?> —
                        </div>
                        <?= Yii::t('app','Up to {count} new customers per day', ['count' => 3])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Reply to the request')?> —
                        </div>
                        <?= Yii::t('app','Up to {count} replies', ['count' => 3])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Show in the catalog')?> —
                        </div>
                        <?= Yii::t('app','After Pros')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Ability to respond to new requests from customers')?> —
                        </div>
                        <?= Yii::t('app','{hours} hours after publication', ['hours' => 24])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Percentage of the system commission from earned money')?>  —
                        </div>
                        20%
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Tariff')?> —
                        </div>
                        <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'],15, true) . ' ' .  Yii::t('app','per day')?>
                    </div>
                    <div class="center-button text-center">
                        <?= Html::a(Yii::t('app', 'Choose tariff'), ['/store/cart/tariff', 'id' => 2], ['class' => 'btn-big']) ?>
                    </div>
                </div>
            </div>

            <div class="tariff-box tariff-grey">
                <div class="tariff-head">
                    <div class="tariff-title text-center"><?= Yii::t('app', 'Pro')?></div>
                    <div class="tariff-price text-center">
                        <span><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'],30, true)?></span>
                        <?= Yii::t('app','per day')?>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Posting of announcements in the catalog')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Write to Customer or receive messages from Customer')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Reply to the request')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Show in the catalog')?> —
                        </div>
                        <?= Yii::t('app','On the first positions')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Ability to respond to new requests from customers')?> —
                        </div>
                        <?= Yii::t('app', 'Right away')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Percentage of the system commission from earned money')?> —
                        </div>
                        20%
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Tariff')?> —
                        </div>
                        <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'],30, true) . ' ' .  Yii::t('app','per day')?>
                    </div>
                    <div class="center-button text-center">
                        <?= Html::a(Yii::t('app', 'Choose tariff'), ['/store/cart/tariff', 'id' => 3], ['class' => 'btn-big']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="profileh text-center"><?= Yii::t('app', 'Premium tariffs for Customers')?></div>
        <p class="text-center"><?= Yii::t('app', 'Will allow you to post, without limitation, requests for work (purchase of goods), contact freelancers and sellers, and receive offers without restrictions')?></p>
        <div class="tariff-block" id="performer">
            <div class="tariff-box tariff-white">
                <div class="tariff-head">
                    <div class="tariff-head-center text-center">
                        <?= Yii::t('app', 'Description')?>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <?= Yii::t('app', 'Posting a request')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app', 'Write to Professional or Seller on site')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app', 'Receive messages from Members')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app', 'Cashback from order')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app', 'Purchase of services and products from other members')?>
                    </div>
                    <div class="tariff-item">
                        <?= Yii::t('app', 'Order an individual job or product')?>
                    </div>
                    <div class="tariff-item"><?= Yii::t('app', 'Tariff')?></div>
                    <div class="center-button text-center"></div>
                </div>
            </div>

            <div class="tariff-box tariff-red">
                <div class="tariff-head">
                    <div class="tariff-title text-center"><?= Yii::t('app', 'Beginner')?></div>
                    <div class="tariff-price text-center">
                        <span><?= Yii::t('app', 'Free')?></span>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Posting a request')?> —
                        </div>
                        <?= Yii::t('app', '{count, plural, one{# request} other{# requests}}', ['count' => 1])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Write to Professional or Seller on site')?> —
                        </div>
                        <?= Yii::t('app', 'Two new professionals (sellers) per day')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Receive messages from Members')?> —
                        </div>
                        <?= Yii::t('app', '{count} messages per day from new members', ['count' => 2])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Cashback from order')?> —
                        </div>
                        <?= Yii::t('app', 'No')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Purchase of services and products from other members')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Order an individual job or product')?> —
                        </div>
                        <?= Yii::t('app', '{count, plural, one{# request} other{# requests}}', ['count' => 1]) . ' ' . Yii::t('app','per day')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Tariff')?> —
                        </div>
                        <?= Yii::t('app', 'Free')?>
                    </div>
                    <div class="center-button text-center">
                        <!-- <?= Html::a(Yii::t('app', 'Choose tariff'), ['/store/cart/tariff', 'id' => 4], ['class' => 'btn-big']) ?> -->
                    </div>
                </div>
            </div>

            <div class="tariff-box tariff-grey">
                <div class="tariff-head">
                    <div class="tariff-title text-center"><?= Yii::t('app', 'Business')?></div>
                    <div class="tariff-price text-center">
                        <span><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'],90, true)?></span>
                        <?= Yii::t('app','per day')?>
                    </div>
                </div>
                <div class="tariff-body">
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Posting a request')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Write to Professional or Seller on site')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Receive messages from Members')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Cashback from order')?> —
                        </div>
                        <?= Yii::t('app', '{percent}% we pay on card', ['percent' => 5])?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Purchase of services and products from other members')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app', 'Order an individual job or product')?> —
                        </div>
                        <?= Yii::t('app','Unlimited')?>
                    </div>
                    <div class="tariff-item">
                        <div class="tariff-mobile-text">
                            <?= Yii::t('app','Tariff')?>
                        </div>
                        <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'],90, true) . ' ' .  Yii::t('app','per day')?>
                    </div>
                    <div class="center-button text-center">
                        <?= Html::a(Yii::t('app', 'Choose tariff'), ['/store/cart/tariff', 'id' => 5], ['class' => 'btn-big']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
    $('body').on('click', '.tariff-buttons a', function() {
        elementClick = $(this).attr('href');
        destination = $(elementClick).offset().top - 245;
        $('body,html').animate({
            scrollTop: destination
        }, 500);
        return false;
    });
");