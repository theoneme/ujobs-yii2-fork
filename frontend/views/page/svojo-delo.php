<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 12.10.2017
 * Time: 12:52
 */

use common\components\CurrencyHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="block-title no-padding">
    <div class="main-center text-center width650">
        <h1 class="text-center"> <?= Yii::t('app', 'We will create your own business for you');?></h1>
        <h3 class="text-center"><?= Yii::t('app', 'We will help you quickly create your business and start earning from {value} a month.', [
                            'value' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 300000)
                    ]);?></h3>
            <?= Html::a( Yii::t('app', 'Choose business'), 'https://ujobs.me/account/profile/show/23537', [
                'class' => 'button',
            ]) ?>
    </div>
</div>
<div class="examples">
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block1.jpg', ['alt' => Yii::t('app', 'Web-studio turn-key')]) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                <?= Yii::t('app', 'Web-studio turn-key');?>
            </h2>
            <p class="text-left">
                <?= Yii::t('app', 'You will receive a finished Web-studio.');?>
            </p>
            <p class="text-left">
                <span>
                    <?= Yii::t('app', 'Price: from {value}', [
                            'value' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 9900)
                    ])?></span>
            </p>
            <?= Html::a(Yii::t('app','Learn more'), 'https://ujobs.me/sozdadim-dlya-vas-veb-studiyu-59de88b4dabe6-job', [
                'class' => 'button text-center',
            ]) ?>
        </div>
    </div>
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block2.jpg', ['alt' => Yii::t('app', 'Create an Advertising Studio')]) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                <?= Yii::t('app', 'Create an Advertising Studio');?>
            </h2>
            <p class="text-left">
                <?= Yii::t('app', 'Advertising business is a constantly growing industry.');?>
            </p>
            <p class="text-left">
                <span>  <?= Yii::t('app', 'Price: from {value}', [
                        'value' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 9900)
                    ])?></span>
            </p>
            <?= Html::a(Yii::t('app','Learn more'), 'https://ujobs.me/otkroem-vam-svoyu-reklamnuyu-studiyu-59de903ac9212-job', [
                'class' => 'button text-center',
            ]) ?>
        </div>
    </div>
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block3.jpg', ['alt' => Yii::t('app', 'Open the design studio')]) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                <?= Yii::t('app', 'Open the design studio');?>
            </h2>
            <p class="text-left">
                <?= Yii::t('app', 'Creating a design, you not only make money, but you make the world brighter.');?>
            </p>
            <p class="text-left">
                <span>  <?= Yii::t('app', 'Price: from {value}', [
                        'value' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 9900)
                    ])?></span>
            </p>
            <?= Html::a(Yii::t('app','Learn more'), 'https://ujobs.me/otkroem-vam-svoyu-studiyu-dizayna-59de932cb57eb-job', [
                'class' => 'button text-center',
            ]) ?>
        </div>
    </div>
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block4.jpg', ['alt' =>  Yii::t('app', 'Your own Digital Agency')]) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                <?= Yii::t('app', 'Your own Digital Agency');?>
            </h2>
            <p class="text-left">
                <?= Yii::t('app', 'Create and sell all kinds of electronic services. This is actual.');?>
            </p>
            <p class="text-left">
                <span>  <?= Yii::t('app', 'Price: from {value}', [
                        'value' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 14900)
                    ])?></span>
            </p>
            <?= Html::a(Yii::t('app','Learn more'), 'https://ujobs.me/otkroem-vam-digital-agentstvo-59df48af41e6c-job', [
                'class' => 'button text-center',
            ]) ?>
        </div>
    </div>
</div>

<div class="process-steps">
    <h3 class="text-center">
        <?= Yii::t('app', 'Advantages of your business in comparison with franchising');?>
    </h3>
    <div class="steps-content">
        <div class="step-item">
            <h2 class="text-center">
                <?= Yii::t('app', 'Your brand');?>
            </h2>
            <p class="text-center">
                <?= Yii::t('app', 'When you develop own company, you create your brand, but do not promote the brand of franchising.');?>
            </p>
        </div>
        <div class="step-item">
            <h2 class="text-center">
                <?= Yii::t('app', 'No royalty');?>
            </h2>
            <p class="text-center">
                <?= Yii::t('app', 'You do not need to pay monthly payments and interest on sales (royalties), as in franchising.');?>
            </p>
        </div>
        <div class="step-item">
            <h2 class="text-center">
                <?= Yii::t('app', 'Turn-key');?>
            </h2>
            <p class="text-center">
                <?= Yii::t('app', 'You get a turn-key company with us . You just have to start selling and earning.');?>
            </p>
        </div>
    </div>
</div>

<div class="block-info text-center">
    <h2 class="text-center">
        <?= Yii::t('app', 'Open your business');?>
    </h2>
    <h3 class="text-center">
        <?= Yii::t('app', 'When you created your business, you will get more freedom, you will learn how to manage your time and your incomes. You are creating your future when you are working for yourself.');?>
    </h3>
        <?= Html::a(Yii::t('app', 'Business catalog'), '#', [
            'class' => 'button',
        ]) ?>
</div>

<div class="process-steps">
    <h3 class="text-center">
        <?= Yii::t('app', 'How to start your business');?>
    </h3>
    <div class="steps-content">
        <div class="step-item">
            <h2 class="text-center">
                <?= Yii::t('app', 'Choose direction');?>
            </h2>
            <p class="text-center">
                <?= Yii::t('app', 'Choose the direction that is most interesting to you and in which you will be interested to develop. Make payment.');?>
            </p>
        </div>
        <div class="step-item">
            <h2 class="text-center">
                <?= Yii::t('app', 'Get the company');?>
            </h2>
            <p class="text-center">
                <?= Yii::t('app', 'We will provide you with everything you need to start a business: a company, a website, a price list, presentations and courses on how to sell and customize advertising.');?>
            </p>
        </div>
        <div class="step-item">
            <h2 class="text-center">
                <?= Yii::t('app', 'Start earning');?>
            </h2>
            <p class="text-center">
                <?= Yii::t('app', 'You need to start selling in your region with the gained knowledge, and we will teach you how to carry out assignments and earn up to 70%.');?>
            </p>
        </div>
    </div>
</div>

<div class="block-image">
    <div class="parallax-image" data-parallax="scroll" data-image-src="/images/new/landing/big-bg.jpg">
        <div class="dark-bg">
        </div>
    </div>
    <div class="center-text text-center">
        <h2 class="text-center"><?= Yii::t('app', 'Do you want to be financially independent?');?></h2>
        <h3 class="text-center"><?= Yii::t('app', 'Start your business today. It\'s simple! We will help to make everything turn-key.');?></h3>
            <?= Html::a(Yii::t('app', 'Choose your business'), 'https://ujobs.me/account/profile/show/23537 ', [
                'class' => 'button',
            ]) ?>
    </div>
</div>