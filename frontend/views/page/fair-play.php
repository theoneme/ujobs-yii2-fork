<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.02.2017
 * Time: 17:41
 */

use yii\helpers\Html;
use common\models\Page;
\frontend\assets\NewsAsset::register($this);
\frontend\assets\FairPlayAsset::register($this);

/* @var Page $page */

?>

<div class="container-fluid">
    <div class="news-content">
        <h1 class="profileh text-left">
            <?= Html::encode($page->getLabel()) ?>
        </h1>
        <?= $page->getContent() ?>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 fair-play text-center">
                <div class="fp-img">
                    <?=Html::img('/images/new/icons/safe1.svg', ['alt' => Html::encode($page->getLabel()),'title' => Html::encode($page->getLabel())])?>
                </div>
                <div class="fp-text">
                    <?=Yii::t('app', 'Buyer chooses a product or service and deposits 100 per cent of the cost on the site.')?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 fair-play text-center">
                <div class="fp-img">
                    <?=Html::img('/images/new/icons/safe2.svg', ['alt' => Html::encode($page->getLabel()),'title' => Html::encode($page->getLabel())])?>
                </div>
                <div class="fp-text">
                    <?=Yii::t('app', 'Seller receives confirmation of the deposit and get down to business. He is ships the goods or provides a service.')?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 fair-play text-center">
                <div class="fp-img">
                    <?=Html::img('/images/new/icons/safe3.svg', ['alt' => Html::encode($page->getLabel()),'title' => Html::encode($page->getLabel())])?>
                </div>
                <div class="fp-text">
                    <?=Yii::t('app', 'After confirmation of delivery of the products or service from the Buyer, the money will be credited to the Seller\'s account.')?>
                </div>
            </div>
        </div>
    </div>
</div>