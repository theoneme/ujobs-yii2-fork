<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 20.11.2017
 * Time: 14:32
 */

\frontend\assets\WizardAsset::register($this);

?>

<div class="container-fluid">
    <div class="block-repair-menu">
        <div class="repair-menu-mobile-title text-center">
            <div id="repair-current-step">
                Параметры квартиры
            </div>
            <div class="close-repair-menu">&times;</div>
        </div>
        <ul class="repair-menu">
            <li class="active">
                <a href="#step1" data-toggle="tab">
                    Параметры квартиры
                </a>
            </li>
            <li>
                <a href="#step2" data-toggle="tab">
                    Стиль квартиры
                </a>
            </li>
            <li>
                <a href="#step3" data-toggle="tab">
                    Кухня
                </a>
                <ul>
                    <li class="active" data-id="opt11">
                        <a href="#">кухня опция 1</a>
                    </li>
                    <li data-id="opt12">
                        <a href="#">кухня опция 2</a>
                    </li>
                    <li data-id="opt13">
                        <a href="#">кухня опция 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#step4" data-toggle="tab">
                    Ванная
                </a>
            </li>
            <li>
                <a href="#step5" data-toggle="tab">
                    Мебель
                </a>
                <ul>
                    <li class="active" data-id="opt21">
                        <a href="#">мебель опция 1</a>
                    </li>
                    <li data-id="opt22">
                        <a href="#">мебель опция 2</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#step6" data-toggle="tab">
                    Бытовая техника
                </a>
            </li>
            <li>
                <a href="#step7" data-toggle="tab">
                    Домашняя утварь
                </a>
            </li>
            <li>
                <a href="#step8" data-toggle="tab">
                    Дополнительные опции
                </a>
            </li>
            <li>
                <a href="#step9" data-toggle="tab">
                    Итого
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-repair-steps">
        <div class="repair-content tab-pane fade in active" id="step1">
            <div class="repair-head-info">
                <div class="title-repair">
                    Базовый
                </div>
                <div class="cost-repair">
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                             Базовая
                        </div>
                        <div class="cost-repair-price">
                            10 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Выбранные опции
                        </div>
                        <div class="cost-repair-price">
                            90 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Итого
                        </div>
                        <div class="cost-repair-price">
                            100 000 Р
                        </div>
                    </div>
                </div>
            </div>
            <div class="repair-slider">
                <div class="repair-big-slider">
                    <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom2.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom3.jpg" alt="photo"></div>
                </div>
                <div class="clearfix">
                    <div class="repair-small-slider">
                        <div class="repair-slide"><img src="/images/new/dom1.jpg" alt="photo"></div>
                        <div class="repair-slide"><img src="/images/new/dom2.jpg" alt="photo"></div>
                        <div class="repair-slide"><img src="/images/new/dom3.jpg" alt="photo"></div>
                    </div>
                    <a class="repair-next-step btn-big" href="">Дальше</a>
                </div>
            </div>
            <div class="repair-packs">
                <div class="repair-pack-box">
                    <div class="repair-pack-item">
                        <div class="repair-pack-img">
                            <img src="/images/new/dom1.jpg" alt="photo">
                        </div>
                        <div class="chover check-pack-repair">
                            <input id="pack1" class="radio-checkbox" name="group" value="" checked type="radio">
                            <label for="pack1">
                                <div>
                                    <span class="repair-pack-title">Базовый</span>
                                    <span class="repair-pack-price">10 000 Р</span>
                                </div>
                            </label>
                        </div>
                        <a class="pack-detail text-right" href="" data-id="details1"><span>Скрыть</span> Детали</a>
                    </div>
                    <div class="repair-pack-item">
                        <div class="repair-pack-img">
                            <img src="/images/new/dom2.jpg" alt="photo">
                        </div>
                        <div class="chover check-pack-repair">
                            <input id="pack2" class="radio-checkbox" name="group" value="" type="radio">
                            <label for="pack2">
                                <div>
                                    <span class="repair-pack-title">Стандартный</span>
                                    <span class="repair-pack-price">50 000 Р</span>
                                </div>
                            </label>
                        </div>
                        <a class="pack-detail text-right" href=""><span>Скрыть</span> Детали</a>
                    </div>
                    <div class="repair-pack-item">
                        <div class="repair-pack-img">
                            <img src="/images/new/dom3.jpg" alt="photo">
                        </div>
                        <div class="chover check-pack-repair">
                            <input id="pack3" class="radio-checkbox" name="group" value="" type="radio">
                            <label for="pack3">
                                <div>
                                    <span class="repair-pack-title">Эксклюзив</span>
                                    <span class="repair-pack-price">100 000 Р</span>
                                </div>
                            </label>
                        </div>
                        <a class="pack-detail text-right" href=""><span>Скрыть</span> Детали</a>
                </div>
                </div>
                <div class="repair-pack-details">
                    <div class="detail" id="details1">
                        <div class="detail-close">&times;</div>
                        <div class="detail-pack-title">
                            Пакет Базовый
                            <div class="detail-pack-price">
                                10 000 Р
                            </div>
                        </div>
                        <div class="repair-pack-bigimg">
                            <img src="/images/new/dom1.jpg" alt="photo">
                        </div>
                        <div class="repair-pack-block">
                            <div class="repair-pack-block-title">
                                кухня
                            </div>
                            <div class="repair-pack-opts">
                                <div class="chover">
                                    <input id="pack-opt11" name="group1" value="" checked type="radio">
                                    <label for="pack-opt11">
                                        <span class="repair-opt-img">
                                            <img src="/images/new/dom1.jpg" alt="">
                                        </span>
                                        <span class="repair-opt-title">кухня опция 1
                                            <span class="opt-price-mob">0 Р</span>
                                        </span>
                                        <span class="repair-opt-price text-right">0 Р</span>
                                    </label>
                                </div>
                                <div class="chover">
                                    <input id="pack-opt12" name="group1" value="" type="radio">
                                    <label for="pack-opt12">
                                        <span class="repair-opt-img">
                                            <img src="/images/new/dom1.jpg" alt="">
                                        </span>
                                        <span class="repair-opt-title">кухня опция 2
                                            <span class="opt-price-mob">0 Р</span>
                                        </span>
                                        <span class="repair-opt-price text-right">0 Р</span>
                                    </label>
                                </div>
                                <div class="chover">
                                    <input id="pack-opt13" name="group1" value="" type="radio">
                                    <label for="pack-opt13">
                                        <span class="repair-opt-img">
                                            <img src="/images/new/dom1.jpg" alt="">
                                        </span>
                                        <span class="repair-opt-title">кухня опция 3
                                            <span class="opt-price-mob">0 Р</span>
                                        </span>
                                        <span class="repair-opt-price text-right">0 Р</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="repair-pack-block">
                            <div class="repair-pack-block-title">
                                мебель
                            </div>
                            <div class="repair-pack-opts">
                                <div class="chover">
                                    <input id="pack-opt21" name="group2" value="" checked type="checkbox">
                                    <label for="pack-opt21">
                                        <span class="repair-opt-img">
                                            <img src="/images/new/dom2.jpg" alt="">
                                        </span>
                                        <span class="repair-opt-title">мебель опция 1
                                            <span class="opt-price-mob">0 Р</span>
                                        </span>
                                        <span class="repair-opt-price text-right">0 Р</span>
                                    </label>
                                </div>
                                <div class="chover">
                                    <input id="pack-opt22" name="group2" value="" type="checkbox">
                                    <label for="pack-opt22">
                                        <span class="repair-opt-img">
                                            <img src="/images/new/dom3.jpg" alt="">
                                        </span>
                                        <span class="repair-opt-title">мебель опция 2
                                            <span class="opt-price-mob">0 Р</span>
                                        </span>
                                        <span class="repair-opt-price text-right">0 Р</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">

                    </div>
                    <div class="detail">

                    </div>
                </div>
            </div>
        </div>
        <div class="repair-content tab-pane fade in" id="step2">

        </div>
        <div class="repair-content withOption tab-pane fade in" id="step3">



            <div class="repair-head-info">
                <div class="title-repair">
                    Базовый
                </div>
                <div class="cost-repair">
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Базовая
                        </div>
                        <div class="cost-repair-price">
                            10 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Выбранные опции
                        </div>
                        <div class="cost-repair-price">
                            90 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Итого
                        </div>
                        <div class="cost-repair-price">
                            100 000 Р
                        </div>
                    </div>
                </div>
            </div>
            <div class="repair-slider">
                <div class="repair-big-slider">
                    <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom2.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom3.jpg" alt="photo"></div>
                </div>
                <div class="clearfix">
                    <div class="repair-small-slider">
                        <div class="repair-slide"><img src="/images/new/dom1.jpg" alt="photo"></div>
                        <div class="repair-slide"><img src="/images/new/dom2.jpg" alt="photo"></div>
                        <div class="repair-slide"><img src="/images/new/dom3.jpg" alt="photo"></div>
                    </div>
                    <a class="repair-next-step btn-big" href="">Дальше</a>
                </div>
            </div>
            <div class="repair-content-option open" id="opt11">
                <div class="content-option-title">кухня</div>
                <div class="repair-option-box">
                    <div class="chover check-none">
                        <input id="check11" name="group11" value=""  type="checkbox">
                        <label for="check11" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom1.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 1</div>
                            <div class="roi-price">5 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check12" name="group11" value=""  type="checkbox">
                        <label for="check12" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom2.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 2</div>
                            <div class="roi-price">7 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check13" name="group11" value="" checked type="checkbox">
                        <label for="check13" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 3</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                </div>
                <div class="opt-for-mobile">
                    <div class="ofm-title"></div>
                    <div class="ofm-price text-right"></div>
                </div>
            </div>
            <div class="repair-content-option" id="opt12">
                    <div class="content-option-title">кухня</div>
                    <div class="repair-option-box">
                            <div class="chover check-none">
                                <input id="check21" name="group12" value=""  type="checkbox">
                                <label for="check21" class="repair-option-item">
                                    <div class="roi-img">
                                        <img src="/images/new/dom1.jpg" alt="photo">
                                    </div>
                                    <div class="roi-title">кухня опция 21</div>
                                    <div class="roi-price">5 000 Р</div>
                                </label>
                            </div>
                            <div class="chover check-none">
                                <input id="check22" name="group12" value=""  type="checkbox">
                                <label for="check22" class="repair-option-item">
                                    <div class="roi-img">
                                        <img src="/images/new/dom1.jpg" alt="photo">
                                    </div>
                                    <div class="roi-title">кухня опция 22</div>
                                    <div class="roi-price">9 000 Р</div>
                                </label>
                            </div>
                    </div>
                <div class="opt-for-mobile">
                    <div class="ofm-title"></div>
                    <div class="ofm-price text-right"></div>
                </div>
            </div>

            <div class="repair-content-option" id="opt13">
                <div class="content-option-title">кухня</div>
                <div class="repair-option-box">
                    <div class="chover check-none">
                        <input id="check31" name="group13" value=""  type="checkbox">
                        <label for="check31" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom1.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 31</div>
                            <div class="roi-price">5 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check32" name="group13" value=""  type="checkbox">
                        <label for="check32" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom2.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 32</div>
                            <div class="roi-price">7 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check33" name="group13" value=""  type="checkbox">
                        <label for="check33" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 33</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check34" name="group13" value=""  type="checkbox">
                        <label for="check34" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">кухня опция 34</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                </div>
                <div class="opt-for-mobile">
                    <div class="ofm-title"></div>
                    <div class="ofm-price text-right"></div>
                </div>
            </div>
        </div>

        <div class="repair-content tab-pane fade in" id="step4">

        </div>
        <div class="repair-content withOption tab-pane fade in" id="step5">

            <div class="repair-head-info">
                <div class="title-repair">
                    Базовый
                </div>
                <div class="cost-repair">
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Базовая
                        </div>
                        <div class="cost-repair-price">
                            10 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Выбранные опции
                        </div>
                        <div class="cost-repair-price">
                            90 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Итого
                        </div>
                        <div class="cost-repair-price">
                            100 000 Р
                        </div>
                    </div>
                </div>
            </div>
            <div class="repair-slider">
                <div class="repair-big-slider">
                    <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom2.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom3.jpg" alt="photo"></div>
                </div>
                <div class="clearfix">

                </div>
            </div>
            <div class="repair-content-option open" id="opt21">
                <div class="content-option-title">мебель</div>
                <div class="repair-option-box">
                    <div class="chover check-none">
                        <input id="check41" name="group21" value=""  type="checkbox">
                        <label for="check41" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom1.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 1</div>
                            <div class="roi-price">5 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check42" name="group21" value=""  type="checkbox">
                        <label for="check42" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom2.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 2</div>
                            <div class="roi-price">7 000 Р</div>
                        </label>
                    </div>
                </div>
                <div class="opt-for-mobile">
                    <div class="ofm-title"></div>
                    <div class="ofm-price text-right"></div>
                </div>
            </div>
            <div class="repair-content-option" id="opt22">
                <div class="content-option-title">мебель</div>
                <div class="repair-option-box">
                    <div class="chover check-none">
                        <input id="check51" name="group22" value=""  type="checkbox">
                        <label for="check51" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom1.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 21</div>
                            <div class="roi-price">5 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check52" name="group22" value=""  type="checkbox">
                        <label for="check52" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom2.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 22</div>
                            <div class="roi-price">7 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check53" name="group22" value=""  type="checkbox">
                        <label for="check53" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 3</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check54" name="group22" value=""  type="checkbox">
                        <label for="check54" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 3</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check55" name="group22" value=""  type="checkbox">
                        <label for="check55" class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 3</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                    <div class="chover check-none">
                        <input id="check56" name="group22" value=""  type="checkbox">
                        <label class="repair-option-item">
                            <div class="roi-img">
                                <img src="/images/new/dom3.jpg" alt="photo">
                            </div>
                            <div class="roi-title">мебель опция 3</div>
                            <div class="roi-price">9 000 Р</div>
                        </label>
                    </div>
                </div>
                <div class="opt-for-mobile">
                    <div class="ofm-title"></div>
                    <div class="ofm-price text-right"></div>
                </div>
            </div>
        </div>

        <div class="repair-content tab-pane fade in" id="step6">

        </div>

        <div class="repair-content tab-pane fade in" id="step7">

        </div>

        <div class="repair-content tab-pane fade in" id="step8">

        </div>

        <div class="repair-content tab-pane fade in" id="step9">
            <div class="repair-head-info">
                <div class="title-repair">
                    Базовый
                </div>
                <div class="cost-repair">
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Базовая
                        </div>
                        <div class="cost-repair-price">
                            10 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Выбранные опции
                        </div>
                        <div class="cost-repair-price">
                            90 000 Р
                        </div>
                    </div>
                    <div class="cost-repair-item">
                        <div class="cost-repair-title">
                            Итого
                        </div>
                        <div class="cost-repair-price">
                            100 000 Р
                        </div>
                    </div>
                </div>
            </div>
            <div class="repair-slider">
                <div class="repair-big-slider">
                    <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom2.jpg" alt="photo"></div>
                    <div><img src="/images/new/dom3.jpg" alt="photo"></div>
                </div>
                <div class="clearfix">
                    <div class="repair-small-slider">
                        <div class="repair-slide"><img src="/images/new/dom1.jpg" alt="photo"></div>
                        <div class="repair-slide"><img src="/images/new/dom2.jpg" alt="photo"></div>
                        <div class="repair-slide"><img src="/images/new/dom3.jpg" alt="photo"></div>
                    </div>
                </div>
            </div>
            <div class="repair-sum-big">
                <div class="repair-sum-block">
                    <div class="repair-sum-title">
                        Параметры квартиры
                        <div class="repair-sum-arrow">
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="repair-sum-over-block">
                        <div class="repair-sum-table-over">
                            <div class="rst-price-text">Цена</div>
                            <div class="repair-sum-table">
                                <div class="rst-row">
                                    <div class="rst-img">
                                        <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                    </div>
                                    <div class="rst-title">
                                        something
                                    </div>
                                    <div class="rst-price">Стандартная опция</div>
                                </div>
                                <div class="rst-row">
                                    <div class="rst-img">
                                        <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                    </div>
                                    <div class="rst-title">
                                        test text
                                    </div>
                                    <div class="rst-price">10 000 Р</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="repair-sum-block">
                    <div class="repair-sum-title">
                        Кухня
                        <div class="repair-sum-arrow">
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="repair-sum-over-block">
                        <div class="repair-sum-opt-block">
                            <div class="repair-sum-opt-title">
                                кухня опция 1
                                <a class="repair-change text-left" href="#" data-step="step3" data-id="opt11">Изменить опцию</a>
                            </div>
                            <div class="repair-sum-table-over">
                                <div class="rst-price-text">Цена</div>
                                <div class="repair-sum-table">
                                    <div class="rst-row">
                                        <div class="rst-img">
                                            <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                        </div>
                                        <div class="rst-title">
                                            something
                                        </div>
                                        <div class="rst-price">Стандартная опция</div>
                                    </div>
                                    <div class="rst-row">
                                        <div class="rst-img">
                                            <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                        </div>
                                        <div class="rst-title">
                                            test text
                                        </div>
                                        <div class="rst-price">10 000 Р</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="repair-sum-opt-block">
                            <div class="repair-sum-opt-title">
                                кухня опция 3
                                <a class="repair-change text-left" href="#" data-step="step3" data-id="opt13">Изменить опцию</a>
                            </div>
                            <div class="repair-sum-table-over">
                                <div class="rst-price-text">Цена</div>
                                <div class="repair-sum-table">
                                    <div class="rst-row">
                                        <div class="rst-img">
                                            <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                        </div>
                                        <div class="rst-title">
                                            other option
                                        </div>
                                        <div class="rst-price">5 000 Р</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="repair-sum-block">
                    <div class="repair-sum-title">
                        Мебель
                        <div class="repair-sum-arrow">
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="repair-sum-over-block">
                        <div class="repair-sum-opt-block">
                            <div class="repair-sum-opt-title">
                                мебель опция 1
                                <a class="repair-change text-left" href="#" data-step="step5" data-id="opt21">Изменить опцию</a>
                            </div>
                            <div class="repair-sum-table-over">
                                <div class="rst-price-text">Цена</div>
                                <div class="repair-sum-table">
                                    <div class="rst-row">
                                        <div class="rst-img">
                                            <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                        </div>
                                        <div class="rst-title">
                                            someth
                                        </div>
                                        <div class="rst-price">Стандартная опция</div>
                                    </div>
                                    <div class="rst-row">
                                        <div class="rst-img">
                                            <div><img src="/images/new/dom1.jpg" alt="photo"></div>
                                        </div>
                                        <div class="rst-title">
                                            text
                                        </div>
                                        <div class="rst-price">Стандартная опция</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right repair-btn">
                <a class="btn-big" href="">Оформить заказ</a>
            </div>
            <div class="repair-sum-small">
                <div class="rss-row rss-grey">
                    <div class="rss-bigtitle">Итоговая цена</div>
                    <div class="rss-smalltitle">Цена</div>
                </div>
                <div class="rss-row rss-inner">
                    <div>Стоимость пакета</div>
                    <div>100 000 Р</div>
                </div>
                <div class="rss-row rss-grey">
                    <div>Стоимость выбранных опций</div>
                    <div>15 000 Р</div>
                </div>
                <div class="rss-row">
                    <div>Итоговая цена</div>
                    <div>115 000 Р</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs('
var slickDir = false;
if($("body").hasClass("rtl")){
    slickDir = true;
}
    function slickinit(){
        $(".repair-big-slider").slick({
            autoplay: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            rtl: slickDir,
            responsive: [{
                breakpoint: 768,
                settings: {
                    dots: false,
                    arrows: true
                }
                },
            ]
        });
        $(".repair-small-slider").slick({
            autoplay: false,
            slidesToShow: 10,
            slidesToScroll: 1,
            asNavFor: ".repair-big-slider",
            dots: false,
            arrows: false,
            rtl: slickDir,
            focusOnSelect: true,
        });
        $(".repair-small-slider .slick-slide").removeClass("slick-active");
        $(".repair-small-slider .slick-slide:first-child").addClass("slick-active");
    }
    function slickReinit(){
        $(".repair-big-slider").slick("unslick");
        $(".repair-small-slider").slick("unslick");
        slickinit();
    }
    function slickOption(){
            $(".repair-option-box").slick("unslick");
            $(".repair-option-box").slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: false, 
                infinite: false,
                arrows: true,
                rtl: slickDir,
                dots: false,
                responsive: [{
                    breakpoint: 600,
                    settings: {         
                        slidesToShow: 5,
                    }
                    },{
                    breakpoint: 480,
                    settings: {         
                        slidesToShow: 3,
                    }
                    },{
                    breakpoint: 320,
                    settings: {         
                        slidesToShow: 2,
                    }
                    },
                ]
            });
    }
    //говно-скрипты 
    $(document).ready(function(){
    slickinit();
    if($(window).width()<=768){
        $(".block-repair-menu li ul li").removeClass("active");
        $(".repair-option-box").slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: false, 
            infinite: false,
            arrows: true,
            dots: false,
            rtl: slickDir,
        });
    }
      $("body").on("click", ".repair-small-slider .slick-slide", function(e){
      alert();
      });
        $("body").on("click", ".pack-detail", function(e){
            e.preventDefault();
            var dId = $(this).attr("data-id");
            if($(window).width()>768){
                $(".pack-detail span").hide();
                $(".detail").slideUp();
                if($("#"+dId).css("display")!="block"){
                    $("#"+dId).slideToggle();
                    $(this).find("span").show();
                } 
            } else {
                $("#"+dId).addClass("open");
            }
        });
         $("body").on("click", ".detail-close", function(){
             if($(window).width()>768){
                $(this).closest(".detail").slideUp();
                $(".pack-detail span").hide();
             } else {
                $(this).closest(".detail").removeClass("open");
             }
        });
        
        /*$("body").on("change",".check-pack-repair input[type=\'radio\']", function(){
            $(".detail").slideUp();
            $(".pack-detail span").hide();
        });*/
         $("body").on("click", ".repair-menu li", function(e){
            $(".repair-menu li ul").hide();
            $(this).find("ul").show();
             if($(window).width()<=768){
                $(".block-repair-menu").removeClass("open");
                setTimeout(slickOption, 200);
            }
            setTimeout(slickReinit, 200);
            
         });
         
         $("body").on("click", ".repair-menu ul li", function(){
            $(this).closest("ul").find("li").removeClass("active");
            $(this).addClass("active");
            var dId = $(this).attr("data-id"); 
            $("#"+dId).closest(".repair-content").find(".repair-content-option").removeClass("open");
            $("#"+dId).addClass("open");
            if($(window).width()<=768){
                $(this).closest("ul").closest("li").children("a").tab("show");
                setTimeout(slickOption, 200);
            }
         });
         $("body").on("click", ".repair-menu-mobile-title", function(e){
            if(!$(this).closest(".block-repair-menu").hasClass("open")){
                $(this).closest(".block-repair-menu").addClass("open");
            }
         });
         $("body").on("click", ".close-repair-menu", function(){
            setTimeout(function(){$(".block-repair-menu").removeClass("open");}, 100);
         });
          $("body").on("click", ".repair-sum-arrow", function(){
             $(this).closest(".repair-sum-block").find(".repair-sum-over-block").toggle();
             $(this).find(".fa").toggleClass("fa-chevron-down fa-chevron-up");
         });
          $("body").on("click", ".repair-change", function(e){
            e.preventDefault();
            $(".repair-menu li ul li").removeClass("active");
            var dId = $(this).attr("data-id"); 
            var step = $(this).attr("data-step");
            $(".repair-menu li ul").find("[data-id=\'"+dId+"\']").addClass("active");
            $("#"+dId).closest(".repair-content").find(".repair-content-option").removeClass("open");
            $("#"+dId).addClass("open");
            $(".repair-menu").find("a[href=\'#"+step+"\']").closest("li").find("ul").show();
            $(".repair-menu").find("a[href=\'#"+step+"\']").tab("show");
            setTimeout(slickReinit, 200);
         });
         /*var tit = $(".ofm-title").closest(".repair-content-option").find("input[type=\'checkbox\']:checked").closest(".check-none").find(".roi-title").html;
         $(".ofm-title").html(tit);
         var price = $(".ofm-title").closest(".repair-content-option").find("input[type=\'checkbox\']:checked").closest(".check-none").find(".roi-price").html;
         $(".ofm-price").html(price);*/
          $("body").on("change", ".repair-option-box input[type=\'checkbox\']", function(){
             var tit = $(this).closest(".check-none").find(".roi-title").html();
             $(this).closest(".repair-content-option").find(".ofm-title").html(tit);
             var price =$(this).closest(".check-none").find(".roi-price").html();
             $(this).closest(".repair-content-option").find(".ofm-price").html(price);
          });
          });
         $(document).ready(function () {
            if($("body").hasClass("rtl")) {
                $(".slick-slider").parent().attr("dir", "rtl");
            }
        });
');?>
