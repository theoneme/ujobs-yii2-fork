<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 04.08.2017
 * Time: 12:10
 */

use common\models\Job;
use common\models\Page;
use common\models\user\Profile;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;


/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var $this yii\web\View
 * @var $page Page
 * @var $cheapest Job
 * @var $mostExpensive Job
 * @var $profilesDataProvider ActiveDataProvider
 * @var $buyersDataProvider ActiveDataProvider
 */

frontend\assets\WorkerAsset::register($this);
$this->registerCssFile('/css/new/category.css');
$this->registerCssFile('/css/new/start-selling.css');

// Ееее, быдлокод
if (!empty($page->banner)) {
    $this->registerCss("
        .start-selling .banner {
            background:url('" . Yii::$app->mediaLayer->getThumb($page->banner) . "') no-repeat center/ cover !important;
        }
    ");
}
if (!empty($page->banner_small)) {
    $this->registerCss("
        @media screen and (max-width: 768px) {
            .start-selling .banner {
                background:url('" . Yii::$app->mediaLayer->getThumb($page->banner_small) . "') no-repeat center/ cover !important;
            }
        }
    ");
}
?>

    <div class="start-selling">
        <div class="row">
            <div class="banner-board bsmall" style="">
                <div class="dark-overlay"></div>
                <div class="container-fluid">
                    <div class="btext-left">
                        <?= yii\widgets\Breadcrumbs::widget([
                            'itemTemplate' => "{link}<span></span>", // template for all links
                            'activeItemTemplate' => "{link}",
                            'links' => [
                                ['label' => Yii::t('account', 'Start Earning')]
                            ],
                            'options' => ['class' => 'breadcrumbs text-left'],
                            'tag' => 'div'
                        ]); ?>
                    </div>

                    <div class="btext-board text-left">
                        <h1 class="title-board text-left">
                            <?php //= $page->translation->title ?>
                            <?= Yii::t('app', 'Sell goods and services. <br/>Post your announcements!<br/>Find new customers.') ?>
                        </h1>
                        <!-- <div class="board-text-green">
                            <?= Yii::t('account', 'Sell unnecessary goods!') ?>
                        </div>
                        <div class="banner-price hidden-xs"><?= Yii::t('account', 'For 12 700 RUB') ?></div>
                        -->
                        <div class="banner-descr-board">
                            <?php //= $page->translation->content_prev ?>
                            <?= Yii::t('app', 'Announcement placement is free.') ?>
                        </div>
                        <?php if (Yii::$app->user->isGuest) {
                            echo Html::a(Yii::t('account', 'Post announcement'), null, [
                                'class' => 'button big green',
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalSignup'
                            ]);
                        } else {
                            echo Html::a(Yii::t('account', 'Post announcement'), ['/entity/global-create', 'type' => 'product'], [
                                'class' => 'button big green text-center'
                            ]);
                        }
                        echo Html::a(Yii::t('app', 'Product catalog'), ['/board/category/products'], [
                            'class' => 'button big white text-center']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="video-bottom vb-board">
            <div class="container-fluid">
                <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                    <?= Yii::t('account', 'Goods are buying') ?><br/>
                    <span><?= Yii::t('account', '{0} sec', 5) ?></span>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                    <?= Yii::t('account', 'Sold goods') ?><br/>
                    <span>97 000</span>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                    <?= Yii::t('account', 'Sell <br> <span>worldwide</span>') ?>
                </div>
            </div>
        </div>

        <div id="benefits2-block" class="container page-content">
            <div class="row">
                <div class="page-content-title col-md-6 col-md-offset-3 text-center">
                    <?= Yii::t('app', 'How it works') ?>
                </div>
            </div>

            <div class="summary page-content-text text-center">
                <p class="text-center">
                    <?= Yii::t('app', 'This site unites Professionals and Customers. You need to register and publish the work, and we will advertise it and find new Clients.') ?>
                </p>
            </div>
        </div>

        <div class="container-fluid three-blocks-container">
            <div class="three-blocks row">
                <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                    <a>
                        <div class="about-img"
                             style="background: url(/images/new/icons/man.png); background-size: cover;"></div>
                        <div class="three-title"><?= Yii::t('app', 'Sign up') ?></div>
                        <p class="text-center">
                            <?= Yii::t('app', 'Sign up with us on the site and tell us about yourself, add photos, in general, as always.') ?>
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                    <a>
                        <div class="about-img"
                             style="background: url(/images/new/icons/hanger.svg); background-size: cover;"></div>
                        <div class="three-title"><?= Yii::t('app', 'You post announcement') ?></div>
                        <p class="text-center">
                            <?= Yii::t('app', 'Detailed description and quality photos will make the announcement more attractive.') ?>
                        </p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                    <a>
                        <div class="about-img"
                             style="background: url(/images/new/icons/money-bag.png); background-size: cover;"></div>
                        <div class="three-title"><?= Yii::t('app', 'Receive orders') ?></div>
                        <p class="text-center">
                            <?= Yii::t('app', 'We do advertising and bring you new clients and buyer. That\'s how you earn.') ?>
                        </p>
                    </a>
                </div>
            </div>
        </div>

        <div class="block-light-grey">
            <div class="container-fluid">
                <h2 class="text-center">
                    <?= Yii::t('app', 'Popular Categories') ?>
                </h2>
                <div class="categories-main">
                    <?php foreach ($categories as $category) { ?>
                        <div class="cat-img-block">
                            <div class="cat-alias"
                                 href="<?= Url::to(['/board/category/products', 'category_1' => $category->translations[Yii::$app->language]->slug]) ?>">
                                <div class="cat-name hidden-xs"><?= $category->translations[Yii::$app->language]->title ?></div>
                                <div class="cat-img">
                                    <?= Html::img($category->getThumb(), ['alt' => $category->translations[Yii::$app->language]->title,'title' => $category->translations[Yii::$app->language]->title]) ?>
                                    <a href="<?= Url::to(['/board/category/products', 'category_1' => $category->translations[Yii::$app->language]->slug]) ?>"
                                       class="center-btn button green big text-center"><?= Yii::t('app', 'View') ?></a>
                                </div>
                                <div class="cat-name-mobile text-center visible-xs"><?= $category->translations[Yii::$app->language]->title ?></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="ss-grey-block">
            <div class="container-fluid">
                <h2 class="text-center"><?= Yii::t('account', 'Q&A') ?></h2>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 qa">
                        <div class="faq-item">
                            <div class="faq-head">
                                <?= Yii::t('account', 'What products can I sell?') ?>
                            </div>
                            <div class="faq-body">
                                <p>
                                    <?= Yii::t('account', 'You can sell any legal goods, not prohibited for sale in the territory of the Russian Federation and other countries.') ?>
                                </p>
                            </div>
                        </div>
                        <div class="faq-item">
                            <div class="faq-head">
                                <?= Yii::t('account', 'How much does it cost to place an announcement?') ?>
                            </div>
                            <div class="faq-body">
                                <p>
                                    <?= Yii::t('account', 'Posting announcement on out site is for free.') ?> <br>
                                    <?= Yii::t('account', 'We have two additional paid services') ?>: <br>
                                    <?= Yii::t('account', 'Premium placement of your announcement at the top of the list. See <a href="https://ujobs.me/tariffs">tariffs</a>.') ?>
                                    <br>
                                    <?= Yii::t('account', 'All purchases on site are guarded by secure transaction. The cost of service is 10% of product`s price. Learn more about a <a href="https://ujobs.me/fair-play">secure transaction</a>.') ?>
                                </p>
                            </div>
                        </div>
                        <div class="faq-item">
                            <div class="faq-head">
                                <?= Yii::t('account', 'What is other ways to earn money on your service?') ?>
                            </div>
                            <div class="faq-body">
                                <ol>
                                    <li>
                                        <?= Yii::t('account', 'You can invite friends and earn 5% of all their purchases on our service. More <a href="https://ujobs.me/referral_program">Referral Program</a>.') ?>
                                    </li>
                                    <li>
                                        <?= Yii::t('account', 'You can sell the services that you are willing to do.') ?>
                                    </li>
                                    <li>
                                        <?= Yii::t('account', 'Post requests and offer your services. <a href="https://ujobs.me/entity/catalog">Request catalog</a>.') ?>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 qa">
                        <div class="faq-item">
                            <div class="faq-head">
                                <?= Yii::t('account', 'Delivery of goods to the buyer, what are the options?') ?>
                            </div>
                            <div class="faq-body">
                                <p>
                                    <?= Yii::t('account', 'We offer two options:') ?>
                                </p>
                                <ol>
                                    <li>
                                        <?= Yii::t('account', 'You can work with our transport partner the company SDEC, and then you only need to carry your goods to the nearest office and indicate where you are sending.') ?>
                                    </li>
                                    <li>
                                        <?= Yii::t('account', 'You can decide where and how the goods will be sent in the correspondence.') ?>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="faq-item">
                            <div class="faq-head">
                                <?= Yii::t('account', 'How do I get paid?') ?>
                            </div>
                            <div class="faq-body">

                                <p>
                                    <?= Yii::t('account', 'You have two options:') ?>
                                </p>
                                <ol>
                                    <li>
                                        <?= Yii::t('account', 'You can work directly with the Buyer and agree on payment terms.') ?>
                                    </li>
                                    <li>
                                        <?= Yii::t('account', 'Work via fair play and then our service guarantees that you will receive payment for your goods 5 days after the receipt of the goods by the Buyer.') ?>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="faq-item">
                            <div class="faq-head">
                                <?= Yii::t('account', 'Who pays for shipping?') ?>
                            </div>
                            <div class="faq-body">
                                <p>
                                    <?= Yii::t('account', 'You can specify when placing the goods, who pays for delivery or decide it with the Buyer.') ?>
                                    <br>
                                    <?= Yii::t('account', 'We recommend choosing the option: Payment for delivery by the Buyer on the spot and at the time of receipt of the goods.') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="white-block">
            <div class="container-fluid">
                <div class="get-started text-center">
                    <p class="text-center"><?= Yii::t('account', 'Post announcement for free') ?></p>
                    <?php if (Yii::$app->user->isGuest) {
                        echo Html::a(Yii::t('app', 'Sign Up'), null, [
                            'class' => 'btn-big',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalSignup'
                        ]);
                    } else {
                        echo Html::a(Yii::t('account', 'Get Started'), ['/entity/global-create', 'type' => 'product'], [
                            'class' => 'btn-big'
                        ]);
                    } ?>
                </div>
            </div>
        </div>

    </div>
<?php $this->registerJs("
    function bigToSmall(){
        var btn = $('.categories-main a.center-btn');
        if($(window).width()<1200){
            btn.removeClass('big').addClass('small');
        } else {
            if(!btn.hasClass('big')){
            btn.removeClass('small').addClass('big');
            }
        }
    }
    $(document).ready(function(){
        bigToSmall();
    });
    $(window).resize(function(){
        bigToSmall();
    });
");