<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.04.2017
 * Time: 16:57
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="block-title">
    <div class="main-center">
        <h1 class="text-center">Получи одну из услуг бесплатно</h1>
        <h3> Специальное предложение только для новых зарегистрированных участников биржи ujobs.me</h3>
        <?php if (Yii::$app->user->isGuest) { ?>
            <?= Html::a('Зарегистрироваться', null, [
                'class' => 'button modal-dismiss',
                'data-toggle' => 'modal',
                'data-target' => '#ModalSignup',
            ]) ?>
        <?php } else { ?>
            <?= Html::a('Перейти на главную', 'https://ujobs.me/', [
                'class' => 'button',
            ]) ?>
        <?php } ?>
    </div>
</div>
<div class="examples">
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block1.jpg', ['alt' => 'facebook']) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                Landing page/Лендинг страница
            </h2>
            <p class="text-left">
                Лендинги, или страницы захвата, помогают получать больше заявок на ваши услуги или товары. Сэкономь 3000 рублей – получи бесплатно!
            </p>
        </div>
    </div>
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block2.jpg', ['alt' => 'SEO аудит сайта']) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                SEO аудит сайта
            </h2>
            <p class="text-left">
                Проведение анализа и составление рекомендации по повышению позиций сайта в поисковой выдаче. Сэкономь 4 000 рублей - получи бесплатно!
            </p>
        </div>
    </div>
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block3.jpg', ['alt' => 'Разработка логотипа']) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                Разработка логотипа
            </h2>
            <p class="text-left">
                Это важная часть бренда. Благодаря качественному логотипу растет узнаваемость и лояльность к бренду. Сэкономь 3 000 рублей - получи бесплатно!
            </p>
        </div>
    </div>
    <div class="example-item">
        <div class="example-img">
            <?= Html::img('/images/new/landing/block4.jpg', ['alt' => 'Ведение соц. сетей неделю']) ?>
        </div>
        <div class="example-info">
            <h2 class="text-left">
                Ведение соц. сетей неделю
            </h2>
            <p class="text-left">
                Профессиональное продвижение в социальных сетях может увеличить продажи в 5 раз. Сэкономь 3 000 рублей - получи бесплатно!
            </p>
        </div>
    </div>
</div>

<div class="block-info text-center">
    <h2 class="text-center">
        Условия проведения акции:
    </h2>
    <h3>
        Вам необходимо зарегистрироваться на сайте и пополнить ваш счет на 5 000 рублей. Эти деньги, вы сможете использовать на оплату любых услуг с биржи. А выбранную услугу по акции вы получите бесплатно, в качестве благодарности за начала использования нашего сервиса.
    </h3>
    <?php if (Yii::$app->user->isGuest) { ?>
        <?= Html::a('Зарегистрироваться', null, [
            'class' => 'button modal-dismiss',
            'data-toggle' => 'modal',
            'data-target' => '#ModalSignup',
        ]) ?>
    <?php } else { ?>
        <?= Html::a('Перейти на главную', 'https://ujobs.me/', [
            'class' => 'button',
        ]) ?>
    <?php } ?>
</div>

<div class="process-steps">
    <h3>
        КАК РАБОТАЕТ ПРЕДЛОЖЕНИЕ:
    </h3>
    <div class="steps-content">
        <div class="step-item">
            <div class="number-steps">
                01
            </div>
            <h2 class="text-center">
                Регистрируетесь
            </h2>
            <p class="text-center">
                Регистрируетесь на сайте. И пишете нам в службу поддержки, какую услугу вы хотите получить бесплатно.
            </p>
        </div>
        <div class="step-item">
            <div class="number-steps">
                02
            </div>
            <h2 class="text-center">
                Вносите депозит
            </h2>
            <p class="text-center">
                Пополняете ваш счет на 5 000 рублей. Эти деньги вы можете потратить на покупку любых других услуг на бирже или забрать обратно, если ничего не выберете.
            </p>
        </div>
        <div class="step-item">
            <div class="number-steps">
                03
            </div>
            <h2 class="text-center">
                Получаете услугу
            </h2>
            <p class="text-center">
                В течении недели мы предоставим выбранную услугу абсолютно бесплатно в качестве благодарности за регистрацию на нашем сайте.
            </p>
        </div>
    </div>
</div>

<div class="block-image">
    <div class="parallax-image" data-parallax="scroll" data-image-src="/images/new/landing/big-bg.jpg">
        <div class="dark-bg">
        </div>
    </div>
    <div class="center-text text-center">
        <h2 class="text-center">Хотите получить услугу бесплатно?</h2>
        <h3>Регистрируйтесь сейчас</h3>
        <?php if (Yii::$app->user->isGuest) { ?>
            <?= Html::a('Зарегистрироваться', null, [
                'class' => 'button modal-dismiss',
                'data-toggle' => 'modal',
                'data-target' => '#ModalSignup',
            ]) ?>
        <?php } else { ?>
            <?= Html::a('Перейти на главную', 'https://ujobs.me/', [
                'class' => 'button',
            ]) ?>
        <?php } ?>
    </div>
</div>