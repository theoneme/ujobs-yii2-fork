<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 19.01.2017
 * Time: 18:33
 */

use frontend\models\ContactForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\models\Page;

\frontend\assets\NewsAsset::register($this);
\frontend\assets\AccountAsset::register($this);

/* @var Page $page */
/* @var ContactForm $model */

?>

<div class="container-fluid2">
    <div class="news-content">
        <h1 class="profileh text-left">
            <?= Html::encode($page->getLabel()) ?>
        </h1>
        <p>
            <?= Yii::t('app', 'If you have questions or offers about working with service, you can contact us to email: {email}, or send us message with contact form and with pleasure we will be analyzing your questions or suggestions:', ['email' => Html::a('support@ujobs.me', 'mailto:support@ujobs.me')]) ?>
        </p>
        <div class="row">
            <?php $form = ActiveForm::begin([
                'id' => 'contact-form',
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'col-md-7'
                ]
            ]); ?>

            <?= $form->field($model, 'name')->textInput() ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= Html::activeHiddenInput($model, 'firstname') ?>

            <?= $form->field($model, 'verifyCode')->widget(Captcha::class, [
                'template' => '
                    <div class="row">
                        <div class="col-lg-3">{image}</div><div class="col-lg-9">{input}</div>
                    </div>',
            ]) ?>

            <div class="form-group contact-submit-block">
                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-middle', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <div class="col-md-5">
                <div class="contact-block row">
                    <?= $page->getContent() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div itemscope itemtype="http://schema.org/Organization" style="display:none">
    <span itemprop="name">ujobs.me</span>
    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress">г. Екатеринбург, ул. Ключевская 15</span>
        <span itemprop="postalCode">620109</span>
        <span itemprop="addressLocality">Екатеринбург</span>
    </div>
    <span itemprop="email">support@ujobs.me</span>
</div>