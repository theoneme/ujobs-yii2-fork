<?php

use common\models\Page;
use common\models\user\Profile;
use yii\bootstrap\Html;


/**
 * @var \yii\web\View $this
 * @var $this yii\web\View
 * @var $page Page
 */

frontend\assets\WorkerAsset::register($this);
$this->registerCssFile('/css/new/category.css');
$this->registerCssFile('/css/new/index.css');

// Ееее, быдлокод
if (!empty($page->banner)) {
    $this->registerCss("
        .start-selling .banner {
            background:url('" . Yii::$app->mediaLayer->getThumb($page->banner) . "') no-repeat center/ cover !important;
        }
    ");
}
if (!empty($page->banner_small)) {
    $this->registerCss("
        @media screen and (max-width: 768px) {
            .start-selling .banner {
                background:url('" . Yii::$app->mediaLayer->getThumb($page->banner_small) . "') no-repeat center/ cover !important;
            }
        }
    ");
}
$translation = $page->translations[Yii::$app->language] ?? array_values($page->translations)[0];
?>

<div class="start-selling">
    <div class="row">
        <div class="banner bsmall" style="">
            <div class="dark-overlay"></div>
            <div class="content-on-overlay container">

                <div class="btext-left text-left">
                    <?= yii\widgets\Breadcrumbs::widget([
                        'itemTemplate' => "{link}<span></span>", // template for all links
                        'activeItemTemplate' => "{link}",
                        'links' => [
                            ['label' => Yii::t('app', 'Outsourcing')]
                        ],
                        'options' =>['class' => 'breadcrumbs text-left'],
                        'tag' => 'div'
                    ]); ?>
                </div>

                <div class="btext-left text-left">
                    <h1 class="cat-title text-left">
                        <?= $translation->title ?>
                    </h1>

                    <div class="banner-descr text-left">
                        <?= $translation->content_prev ?>
                    </div>
                    <br>
                    <?php if (Yii::$app->user->isGuest) {
                        echo Html::a(Yii::t('app', 'Place an order for free'), null,[
                            'class' => 'btn-big',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalSignup'
                        ]);
                    }
                    else {
                        echo Html::a(Yii::t('app', 'Place an order for free'), ['/entity/global-create', 'type' => 'tender-service'], ['class' => 'btn-big']);
                    }?>
                    <a class="read-more-mpp">
                        <?= Yii::t('app', 'Read more')?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="video-bottom row">
        <div class="container-fluid">
            <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                <?= Yii::t('app', 'Represented employees')?><br/>
                <span>2400</span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                <?= Yii::t('app', 'Money saved')?><br/>
                <span>20 450 000 руб</span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 video-bottom-item text-center">
                <?= Yii::t('app', 'Savings taxes')?><br/>
                <span>15 679 000 руб</span>
            </div>
        </div>
    </div>

    <div id="benefits2-block" class="container page-content auto">
        <div class="row">
            <div class="page-content-title col-md-6 col-md-offset-3 text-center">
                <?= Yii::t('app', 'How it works')?>
            </div>
        </div>
        <div class="summary page-content-text text-center">
            <p class="text-center">
                <?= Yii::t('app', 'You place a request, and our system looks for the most suitable employees according to your requirements') ?>
            </p>
        </div>
    </div>
    <div class="container-fluid three-blocks-container">
        <div class="three-blocks row">
            <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                <a>
                    <div class="about-img" style="background: url(/images/new/icons/clipboard.png); background-size: cover;"></div>
                    <div class="three-title"><?= Yii::t('app', '1. REGISTER AND PLACE AN APPLICATION.')?></div>
                    <p class="text-center">
                        <?= Yii::t('app', 'You place an application, and our system looks for the most suitable employees according to your requirements')?>
                    </p>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                <a>
                    <div class="about-img" style="background: url(/images/new/icons/man.png); background-size: cover;"></div>
                    <div class="three-title"><?= Yii::t('app', '2. WE SELECT WORKERS.')?></div>
                    <p class="text-center">
                        <?= Yii::t('app', 'Our service selects the specialists you need and sends it to the agreement. After approval, the performers begin work.')?>
                    </p>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center about">
                <a>
                    <div class="about-img" style="background: url(/images/new/icons/wallet.png); background-size: cover;"></div>
                    <div class="three-title"><?= Yii::t('app', '3. YOU TAKE THE WEEKLY ACTIVITY.')?></div>
                    <p class="text-center">
                        <?= Yii::t('app', 'Every week you set tasks and accept them. We transfer money from your account only after the work is approved.')?>
                    </p>
                </a>
            </div>
        </div>
    </div>

    <div class="block-content">
        <h2 class="clearfix text-center">
            <div><?= Yii::t('app', 'WHAT OUR CLIENTS SAY')?></div>
            <span class="col-md-6 col-md-offset-3">
                <?= Yii::t('app', 'For us, it is very important to evaluate our service from our customers, and these are some of the reviews')?>
            </span>
        </h2>
        <div class="review-slider">
            <div class="rev-slide">
                <div class="rev-slide-info">
                    <div class="info-center">
                        <div class="rev-slide-title"><?= Yii::t('app','I pay for the result'); ?></div>
                        <blockquote><?= Yii::t('app','I like that I pay for the visible result'); ?></blockquote>
                        <cite><?= Yii::t('app','Katya Pavlova'); ?>, <?= Yii::t('app','Head of production of the publishing house "DV-PRESS"'); ?></cite>
                    </div>
                </div>
                <div class="rev-slide-video">
                    <a class="href-video" href="" data-video="/images/new/review/<?= Yii::$app->language?>/video0.mp4" data-toggle="modal" data-target="#video0">
                        <?= Html::img('/images/new/review/poster0.jpg', ['alt' => 'Katya Pavlova', 'title' => 'Katya Pavlova']) ?>
                        <div class="play text-center">
                            <i class="fa fa-play"></i>
                        </div>
                        <div class="slide-info-mobile">
                            <div class="rev-slide-title"><?= Yii::t('app','I pay for the result'); ?></div>
                            <blockquote><?= Yii::t('app','I like that I pay for the visible result'); ?></blockquote>
                            <cite><?= Yii::t('app','Katya Pavlova'); ?>, <?= Yii::t('app','Head of production of the publishing house "DV-PRESS"'); ?></cite>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="ss-grey-block">
        <div class="container-fluid">
            <h2 class="text-center" style="padding-bottom: 40px">
                <div><?= Yii::t('app', 'WHY IS IT BENEFICIAL TO WORK WITH UJOBS')?></div>
                <span class="col-md-6 col-md-offset-3">
                    <?= Yii::t('app', 'You not only quickly find the right employee, but also get financial benefits.')?>
                </span>
            </h2>
            <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
                <div class="how-work-title text-center tax-icon">
                    <?= Yii::t('app', '1. Savings on payroll taxes')?>
                </div>
                <p class="text-center">
                    <?= Yii::t('app', 'We provide you with services such as outsourcing of personnel with invoices. You do not need to pay taxes on wages.')?>
                </p>
            </div>
            <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
                <div class="how-work-title text-center cash-icon">
                    <?= Yii::t('app', '2. Savings on the employee\'s salary')?>
                </div>
                <p class="text-center">
                    <?= Yii::t('app', 'According to statistics, remote executives cost the employer 30% less than regular employees.')?>
                </p>
            </div>
            <div class="how-works-item col-md-4 col-sm-4 col-xs-12">
                <div class="how-work-title text-center desk-icon">
                    <?= Yii::t('app', '3. Saving on the organization of the workplace')?>
                </div>
                <p class="text-center">
                    <?= Yii::t('app', 'You do not need to pay office rent and bear the costs of organizing a workplace.')?>
                </p>
            </div>
        </div>
    </div>

    <div class="ss-grey-block">
        <div class="container-fluid">
            <h2 class="text-center"><?= Yii::t('account', 'Q&A')?></h2>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 qa">
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app', 'How much does it cost to use the service?')?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app', 'Our service for the selection and provision of one employee is free for the Employer.')?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app', 'How much does the employee cost? Can I pay for the result?');?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app', 'We recommend that you set a salary based on two factors: a minimum salary that guarantees the employee financial security, and a percentage of sales or a bonus on the implementation of the plan.');?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app','How best to apply?');?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app','To find a good specialist, you need to describe your requirements in detail. Specify what tasks will be assigned to the employee, and how you will assess the quality of the work. If you encourage your employees through bonuses, indicate the estimated amount of compensation.');?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app','Do you present documents with VAT?');?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app','No, we are working on a simplified taxation scheme and, unfortunately, do not display VAT in the documents. With us, you do not save on VAT, but save on income taxes, as well as on payroll taxes that far exceed the amount of VAT.');?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 qa">
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app','What is the process of labor remuneration?');?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app','First, our performers confirm their agreement to work on your assignment. After approval of the candidacy, you must replenish your account for the amount of a monthly salary. From this amount, after receiving a weekly report on the performance of work, we will list the salary of a specialist. A week before the end of the month you will receive a new invoice to pay for the services of the employee.');?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app','Can I transfer you money by bank transfer?');?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app','Yes, we can bill invoices. You need to go to the link https://ujobs.me/account/payment/index in your account and click the button "Request invoice for payment". Specify in the application your requisites, e-mail, the name of the company (or to whom we must invoice). The invoice will be sent on the same day.');?>
                            </p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-head">
                            <?= Yii::t('app','What documents do you provide?');?>
                        </div>
                        <div class="faq-body">
                            <p>
                                <?= Yii::t('app','We provide all necessary documents in accordance with Russian law. Invoices and certificates of work performed. You can request documents in your account via the link.');?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="white-block">
        <div class="container-fluid">
            <div class="get-started text-center">
                <p class="text-center"><?= Yii::t('app','Register, place an request for free and find the employee that you need.'); ?></p>
                <?php if (Yii::$app->user->isGuest) {
                    echo Html::a(Yii::t('app', 'Place an order for free'), null,[
                        'class' => 'btn-big',
                        'data-toggle' => 'modal',
                        'data-target' => '#ModalSignup'
                    ]);
                }
                else {
                    echo Html::a(Yii::t('app', 'Place an order for free'), ['/entity/global-create', 'type' => 'tender-service'],['class' => 'btn-big']);
                }?>
            </div>
        </div>
    </div>

    <div class="modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header header-col text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->registerJs("
    $(document).on('click', '.href-video', function () {
        var src = $(this).data('video');
        $('#videoModal .modal-body').html('<video width=\"100%\" controls><source src=\"' + src +  '\"></video>');
        $('#videoModal').modal('show');
        return false;
    })
    $('#videoModal').on('hidden.bs.modal', function (e) {
		$('#videoModal .modal-body').html('');
	});

	$('.video-modal').on('shown.bs.modal', function () {
        $(this).find('video').get(0).play();
    })
    $(document).on('click', '.read-more-mpp', function () {
        $('body,html').animate({
            scrollTop: $('#benefits2-block').offset().top - 90
        }, 800);
        return false;
    })
");