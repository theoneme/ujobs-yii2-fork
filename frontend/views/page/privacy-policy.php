<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 19.01.2017
 * Time: 16:44
 */

use yii\helpers\Html;
use common\models\Page;
\frontend\assets\NewsAsset::register($this);

/* @var Page $page */

?>

<div class="container-fluid">
    <div class="news-content">
        <h1 class="profileh text-left">
            <?= Html::encode($page->getLabel()) ?>
        </h1>
        <?= $page->getContent() ?>
    </div>
</div>