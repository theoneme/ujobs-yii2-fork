<?php

use common\helpers\FileInputHelper;
use common\helpers\ImperaviHelper;
use frontend\assets\CropperAsset;
use frontend\components\WizardSteps;
use frontend\models\PostVideoForm;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $rootCategoriesList array
 * @var $model PostVideoForm
 * @var $locale string
 * @var $currencies array
 */
CropperAsset::register($this);
$this->title = Yii::t('app', 'Post Video');
?>

<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('app', 'Overview'),
    2 => Yii::t('app', 'Description'),
    3 => Yii::t('app', 'Publish')
], 'step' => 1]); ?>

<div class="profile">
    <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="mobile-note-title"></div>
                </div>
                <div class="modal-body">
                    <div class="mobile-note-text"></div>
                    <div class="mobile-note-btn text-center">
                        <a class="button big green" href="#"><?=Yii::t('app','Continue')?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li class="active">
                <a>
                    <?= Yii::t('app', Yii::$app->params['languages'][$model->locale]) ?>
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'data-step' => 1,
                        'data-final-step' => 3
                    ],
                    'id' => 'video-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnChange' => false,
                    'validateOnBlur' => false,
                ]); ?>
                <?= Html::hiddenInput('id', $model->videoObject->id, ['id' => 'video-id']) ?>
                <div class="tab-content nostyle-tab-content">
                    <div class="settings-content step tab-pane fade in active" id="step1" data-step="1">
                        <div class="formgig-block">
                            <div class="row set-item">
                                <?= $form->field($model, "title", [
                                    'template' => '
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                {label}
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            {input}
                                            {error}
                                            <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('app', 'Max') . '</div>
                                        </div>
                                    '
                                ])->textarea([
                                    'class' => 'readsym bigsymb',
                                    'placeholder' => Yii::t('app', 'Name your video'),
                                    'data-action' => 'category-suggest'
                                ])
                                ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('app', 'Name your video') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'This is your video`s title.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row set-item">
                                <?= $form->field($model, "description", [
                                    'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}<div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->widget(Widget::class, ArrayHelper::merge(ImperaviHelper::getDefaultConfig(), [
                                    'settings' => [
                                        'buttonsHide' => [
                                            'deleted',
                                            'horizontalrule',
                                            'link',
                                        ]
                                    ]
                                ])) ?>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('app', 'Description of your video') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?//= Yii::t('app', 'Try to describe your service with as much details as possible. The more detailed description you provide, the more chances to sell service you have.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row set-item">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label" for="postvideoform-category_id"><?= Yii::t('app', 'Category') ?></label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                                    'prompt' => ' -- ' . Yii::t('app', 'Select A Category'),
                                                    'class' => 'form-control'
                                                ])->label(false); ?>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                <?= $form->field($model, 'category_id')->widget(DepDrop::classname(), [
                                                    'pluginOptions' => [
                                                        'initialize' => true,
                                                        'depends' => ['postvideoform-parent_category_id'],
                                                        'placeholder' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                        'url' => Url::to(['/ajax/subcat']),
                                                        'params' => ['depdrop-helper'],
                                                        'skipDep' => true,
                                                        'emptyMsg' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                    ],
                                                    'options' => [
                                                        'class' => 'form-control',
                                                    ]
                                                ])->label(false); ?>
                                                <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="suggestions-block" class="col-md-12">

                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                    <div class="notes-title"><?= Yii::t('app', 'Where will your video be?') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'Please select a category and subcategory most relevant to your video.') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="create-gig-block clearfix">
                            <?= Html::a(Yii::t('app', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray']) ?>
                            <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                        </div>
                    </div>
                    <div class="settings-content tab-pane step fade in form-gig file-input-step" id="step2" data-step="2">
                        <div class="row formgig-block show-notes">
                            <div class="form-group">
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title">
                                        <label class="control-label" for="postvideoform-uploaded_images"><?= Yii::t('app', 'Preview') ?></label>
                                        <div class="mobile-question" data-toggle="modal"></div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset file-input-container">
                                    <div class="cgp-upload-files">
                                        <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                            'id' => 'file-upload-input',
                                            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
                                            'pluginOptions' => [
                                                'uploadUrl' => Url::toRoute('/image/upload-wide'),
                                                'overwriteInitial' => true,
                                                'initialPreview' => !empty($model->image) ? $model->getThumb() : [],
                                                'initialPreviewConfig' => !empty($model->image) ? [[
                                                    'caption' => basename($model->image),
                                                    'url' => Url::toRoute('/image/delete'),
                                                    'key' => $model->id
                                                ]] : [],
                                                'otherActionButtons' =>
                                                    '<button type="button" class="btn btn-sm btn-default  crop-wide-button" title="' . Yii::t('app', 'Crop image') . '">
                                                        <i class="fa fa-crop" aria-hidden="true"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-default crop-wide-button" title="' . Yii::t('app', 'Rotate image') . '">
                                                        <i class="fa fa-rotate-left" aria-hidden="true"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-default crop-wide-button" title="' . Yii::t('app', 'Rotate image') . '">
                                                        <i class="fa fa-rotate-right" aria-hidden="true"></i>
                                                    </button>',
                                            ]
                                        ])) ?>
                                        <div class="files-container">
                                            <?= $form->field($model, 'image')->hiddenInput([
                                                'id' => 'video-image-input',
                                                'value' => $model->image,
                                                'data-key' => $model->id
                                            ])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Photos') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('app', 'It’s preview for your video') ?>
                                        </p>
                                        <ul>
                                            <li><?= Yii::t('app', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                            <li><?= Yii::t('app', 'Dimensions: the more - the better') ?></li>
                                            <li><?= Yii::t('app', 'Max File size: {count} MB', ['count' => 5]) ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row formgig-block show-notes">
                            <div class="form-group">
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title">
                                        <label class="control-label" for="postvideoform-uploaded_images"><?= Yii::t('app', 'Video') ?></label>
                                        <div class="mobile-question" data-toggle="modal"></div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset file-input-container">
                                    <div class="cgp-upload-files">
                                        <?= FileInput::widget([
                                            'name' => 'uploaded_files[]',
                                            'id' => 'file-upload-input-video',
                                            'options' => ['accept' => 'video/*', 'class' => 'file-upload-input'],
                                            'pluginOptions' => [
                                                'required' => true,
                                                'uploadUrl' => Url::to(['/file/upload']),
                                                'allowedFileExtensions' => ['mp4', 'ogg', 'webm'],
                                                'showPreview' => true,
                                                'dropZoneEnabled' => true,
                                                'initialPreviewAsData' => true,
                                                'showCancel' => false,
                                                'showRemove' => false,
                                                'showUpload' => false,
                                                'fileActionSettings' => [
                                                    'showZoom' => false,
                                                    'showDrag' => false,
                                                    'showUpload' => false
                                                ],
                                                'previewThumbTags' => [
                                                    '{actions}' => '{actions}',
                                                ],
                                                'initialPreview' => !empty($model->video) ? $model->video : [],
                                                'initialPreviewConfig' => !empty($model->video) ? [[
                                                    'type' => 'video',
                                                    'filetype' => 'video/*',
                                                    'key' => 'video-' . $model->id,
                                                    'caption' => basename($model->video),
                                                    'url' => Url::toRoute('/file/delete')
                                                ]] : [],
                                                'uploadExtraData' => new JsExpression("
                                                    function (previewId, index) {
                                                        if (previewId !== undefined) {
                                                            var obj = $('#' + previewId).data();
                                                            return obj;
                                                        }
                                                    }
                                                "),
                                                'previewTemplates' => [
                                                    'video' => '
                                                        <div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}" title="{caption}">
                                                            <div class="kv-file-content">
                                                                <video class="kv-preview-data file-preview-video" controls {style}>
                                                                    <source src="{data}">
                                                                    <div class="file-preview-other">
                                                                        <span class="file-other-icon"><i class="glyphicon glyphicon-file"></i></span>
                                                                    </div>                                
                                                                </video>
                                                            </div>
                                                            {footer}
                                                        </div>
                                                    '
                                                ]
                                            ]
                                        ]) ?>
                                        <div class="files-container">
                                            <?= $form->field($model, 'video')->hiddenInput([
                                                'id' => 'video-input',
                                                'value' => $model->video,
                                                'data-key' => 'video-' . $model->id
                                            ])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Video') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <ul>
                                            <li><?= Yii::t('app', 'Allowed formats are MP4, WEBM and OGG') ?></li>
                                            <li><?= Yii::t('app', 'Video resolution: the more - the better') ?></li>
                                            <li><?= Yii::t('app', 'Max File size: {count} MB', ['count' => 10000]) ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row set-item">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label"><?= Yii::t('app', 'Currency and price')?></label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <?= $form->field($model, "currency_code")->dropDownList($currencies, [
                                                    'class' => 'form-control'
                                                ])->label(false); ?>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <?= $form->field($model, 'price')->textInput(['placeholder' => '9 999', 'id' => 'price', 'type' => 'number'])->label(false); ?>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <?= $form->field($model, 'free', ['template' => '{input}{label}'])
                                                    ->checkbox(['id' => 'free-price-checkbox'], false)
                                                    ->label(Yii::t('app', 'Free'))
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('app', 'Set the price') ?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('app', 'Price of video watching including the commission of our service ({percent}%)', ['percent' => 20]) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="create-gig-block">
                            <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                            <?= Html::submitButton(Yii::t('app', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                        </div>
                    </div>
                    <div class="settings-content tab-pane fade in" id="step3" data-step="3">
                        <div class="publish-title text-center">
                            <?= Yii::t('app', 'It\'s almost ready...') ?>
                        </div>
                        <div class="publish-text text-center">
                            <?= Yii::t('app', "Let`s publish your video and <br/> attract the attention of buyers.") ?>
                        </div>
                        <div class="create-gig-block row">
                            <div class="col-md-12">
                                <?= Html::button(Yii::t('app', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Publish video'), ['class' => 'btn-mid fright']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?= $this->render('@frontend/views/common/crop-bg.php')?>

</div>

<?php
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'page_tag']);
$categorySuggestRoute = Url::toRoute('/ajax/category-suggest');
$addTagMessage = Yii::t('app', 'Add tag');
$videoRequiredError = Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => $model->getAttributeLabel('video')]);
$script = <<<JS
    $('body').on('click','.mobile-question',function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click','.mobile-note-btn a',function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });
	
	$("#video-form").on("beforeValidateAttribute", function (event, attribute, messages) {
		let current_step = $(this).data("step"),
		    input_step = $(attribute.container).closest(".step").data("step");
		if (current_step !== input_step) {
			return false;
		}
	}).on("afterValidate", function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
			let current_step = $(this).data("step");
            let firstError = $(this).find("div[data-step='" + current_step + "'] .has-error").first();
            $("html, body").animate({
                scrollTop: firstError.offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function (event) {
		let current_step = $(this).data("step"),
		    self = $(this);
		if ($(this).find("div[data-step='" + current_step + "']").hasClass("file-input-step")) {
		    let videoFileInput = $('#file-upload-input-video');
		    let videoFilesContainer = videoFileInput.closest('.file-input-container').find('.files-container');
		    if (!videoFilesContainer.find('input').val().length && !videoFileInput[0].files.length) {
                videoFilesContainer.find('.help-block').html('$videoRequiredError');
                videoFilesContainer.find('.form-group').addClass('has-error').removeClass('has-success');
                return false;
		    }
		    let returnValue = true;
		    $(".file-upload-input").each(function(index){
                if ($(this).fileinput("getFilesCount") > 0) {
                    $(this).fileinput("upload");
                    returnValue = false;
                    return false;
                }
		    });
		    if (!returnValue) {
		        return false;
		    }
        }
		if (current_step !== $(this).data("final-step")) {
            self.find("div[data-step='" + current_step + "']").removeClass("active");
            $(".gig-steps li").removeClass("active");
            current_step++;
            self.data("step", current_step);
            self.find("div[data-step='" + current_step + "']").addClass("active");
            $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
			return false;
		}
	});

    $(".prev-step").on("click", function() {
    	let pageForm = $("#video-form"),
		    current_step = pageForm.data("step");
    	
		if (current_step > 1) {
			pageForm.find("div[data-step='" + current_step + "']").removeClass("active");
			$(".gig-steps li").removeClass("active");
			current_step--;
			pageForm.data("step", current_step);
			pageForm.find("div[data-step='" + current_step + "']").addClass("active");
			$(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
			$("html, body").animate({
				scrollTop: 0
			}, "slow");
		}
		return false;
    });
    
    let suggestTimer = null;
    $(document).on("keyup", "[data-action=category-suggest]", function() {
        let self = $(this);
        clearTimeout(suggestTimer);
        suggestTimer = setTimeout(function() {
            let val = self.val();
            $.get('{$categorySuggestRoute}',
                {entity: 'job', request: val},
                function(data) {
                    if (data.success === true) {
                        $('#suggestions-block').html(data.html);
                    } else {
                        $('#suggestions-block').html('');
                    }
                },
                "json"
            );
        }, 700);
    });
    
    $(document).on('click', '[data-action=suggestion-select]', function() {
        let parentCategoryInput = $('#postvideoform-parent_category_id');
        let categoryInput = $('#postvideoform-category_id');
        let subcategoryInput = $('#postvideoform-subcategory_id');
        let pId = $(this).data('p-id');
        let cId = $(this).data('c-id');
        let sId = $(this).data('s-id');
        
        categoryInput.attr('data-c-id', cId);
        subcategoryInput.attr('data-s-id', sId);
        parentCategoryInput.val(pId).trigger('change');
        
        return false;
    });
    
    $('#postvideoform-category_id').on('depdrop:afterChange', function(event, id, value) {
        let cId = $(this).attr('data-c-id');
        if(cId) {
            $('#postvideoform-category_id').val(cId).trigger('change');
        }
    });
    
    $('#postvideoform-subcategory_id').on('depdrop:afterChange', function(event, id, value) {
        let sId = $(this).attr('data-s-id');
        if(sId) {
            $('#postvideoform-subcategory_id').val(sId).trigger('change');
        }
    });

    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(this).closest('.file-input-container').find('.files-container input').val(response.uploadedPath).data('key', response.imageKey);
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').find('.files-container input').val('');
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $("#video-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });
    
    $(document).on("change", "#free-price-checkbox", function() {
        if ($(this).prop("checked")) {
            $("#price").parent().removeClass("has-error");
            $("#price").siblings(".help-block").html("");
            $("#price").val("");
        } else {
            $("#price").prop("disabled", false);
        }
    });

    $(document).on("keyup", "#price", function() {
        let value = $(this).val();
        value = parseInt(value.replace(/\s/g, ""));
        value = value > 0 ? value : 0;
        $("#free-price-checkbox").prop("checked", value === 0);
    });
JS;

$this->registerJs($script);