<?php

use common\models\Video;
use frontend\assets\ProductAsset;
use frontend\components\SocialShareWidget;
use frontend\modules\account\components\MiniView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use \yii\widgets\Breadcrumbs;

/**
 * @var $this View
 * @var $model Video
 * @var $breadcrumbs array
 * @var $extraFields array
 * @var $accessInfo array
 */

ProductAsset::register($this);

?>
<div class="work-bg">
	<div class="nav-fixed">
		<div class="container-fluid">
			<div class="nav-work row">
				<ul class="menu-work">
					<li>
						<a class="selected" href="#w-slides"><?= Yii::t('app', 'Overview')?></a>
					</li>
					<li>
						<a href="#w-info"><?= Yii::t('app', 'Description')?></a>
					</li>
				</ul>
				<div class="work-like">
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="work-content row">
			<div class="work-side col-md-8 col-sm-6 col-xs-12">
				<div id="w-slides" class="work-section">
					<div class="work-header">
						<h1 class="work-title text-left">
							<?= Html::encode($model->title) ?>
						</h1>
<!--						<div class="work-stars">-->
<!--							<div class="work-rate">-->
<!--                                --><?//= StarRating::widget([
//                                    'name' => 'rating_1',
//                                    'value' => 10,
//                                    'pluginOptions' => [
//                                        'theme' => 'krajee-uni',
//                                        'filledStar' => '&#x2605;',
//                                        'emptyStar' => '&#x2606;',
//                                        'size' => 'xxxxs',
//                                        'min' => 0,
//                                        'max' => 10,
//                                        'step' => 1,
//                                        'displayOnly' => true,
//                                    ],
//                                ]); ?>
<!--	                        </div>-->
<!--	                    </div>-->
						<?= Breadcrumbs::widget([
							'itemTemplate' => "{link}/", // template for all links
							'activeItemTemplate' => "&nbsp;{link}",
							'links' => $breadcrumbs,
							'options' =>['class' => 'breadcrumbs'],
							'tag' => 'div'
						]); ?>
					</div>
					<a class="video-view-image" data-video="<?= $model->price ? '' : $model->video ?>">
                        <div class="play text-center">
                            <i class="fa fa-play"></i>
                        </div>
                        <div class="big-image-container" style="overflow: hidden">
                            <?= Html::img($model->getThumb(),['alt' => Html::encode($model->title) , 'title' => Html::encode($model->title)])?>
                        </div>
					</a>
				</div>
				<div id="w-info" class="work-section">
					<div class="head-work">
						<?= Yii::t('app', 'About This Video') ?>
					</div>
					<div class="body-work">
                        <?=$model->description?>
					</div>
					<div id="map"></div>
				</div>
			</div>
			<div class="author-side col-md-4 col-sm-6 col-xs-12">
                <div class="aside-price">
                    <div class="ap-head text-left">
                        <div style="text-align: center">
                            <?= Yii::t('app', 'Price')?>: <span class="ap-typeprice"><?= $model->getPriceString() ?>
                        </div>
                    </div>
                    <div class="ap-body" style='display:block'>
                        <div class="ap-buttons text-center">
                            <?= !empty($model->price) ? Html::a(Yii::t('app', 'Order for {price}', ['price' => $model->getPriceString()]), null, ['class' => 'btn-big']) : ''?>
                        </div>
                    </div>
                </div>

                <?= MiniView::widget([
                    'user' => $model->user,
                    'accessInfo' => $accessInfo,
                ]) ?>

                <div class="work-share text-center">
                    <?= SocialShareWidget::widget([
                        'url' => Url::current(['invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                        'twitterMessage' => Yii::t('app', 'Check out this video on {site}:', ['site' => Yii::$app->name]),
                        'mailItem' => [
                            'label' => $model->getLabel(),
                            'thumb' => $model->getThumb('catalog'),
                            'name' => $model->getSellerName()
                        ]
                    ]) ?>
                </div>
			</div>
		</div>
	</div>
</div>

<?php if ($accessInfo['allowed'] == false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>

<div class="modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header header-col text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$translations = json_encode([
    'see_less' => Yii::t('app', 'See less'),
    'see_more' => Yii::t('app', 'See more')
]);
$addToCartUrl = Url::to(['/store/cart/add']);
$videoScript = <<<JS
    $(document).on('click', '.video-view-image', function () {
        let src = $(this).data('video');
        $('#videoModal .modal-body').html('<video width=\"100%\" controls><source src=\"' + src +  '\"></video>');
        $('#videoModal').modal('show');
        return false;
    });
    $('#videoModal').on('hidden.bs.modal', function (e) {
		$('#videoModal .modal-body').html('');
	});
JS;

$script = <<<JS
	const translations = $translations;
    $(document).on('click', '.add-to-cart', function(){
        let id = $(this).attr('data-id');
		let self = $(this);
		let options = $('#optionsForm').serialize();
        $.ajax({
            url: '$addToCartUrl',
            type: 'post',
            data: {id: id, options: options},
            success: function(response) {
				window.location.href = self.attr('href');
            },
        });
        return false;
    });
    
    $('body').on('click', '.work-section .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.work-section').find('.mob-more-info').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { 
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    }).on('click', '.author-intro .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.author-intro').find('.short-descr-desktop').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { 
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
JS;

$this->registerJs($script);
if (!$model->price) {
    $this->registerJs($videoScript);
}