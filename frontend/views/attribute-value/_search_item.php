<?php

use common\modules\attribute\models\AttributeValue;
use yii\helpers\Html;

/**
 * @var AttributeValue $model
 * @var string $baseUrl
 * @var string $attributeParam
 * @var string|null $city
 */

?>
<?= Html::a(
    "<span class='tag-name'>" . $model->getTitle() . "</span><span class='grey'>({$model->relatedCount})</span>",
    [$baseUrl, $attributeParam => $model->alias, 'city' => $city],
    ['class' => 'tag-but']
);