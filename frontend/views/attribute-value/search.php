<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use frontend\assets\SelectizeAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var Attribute $attribute
 * @var ActiveDataProvider $dataProvider
 * @var View $this
 * @var AttributeValue $city
 * @var string $baseUrl
 * @var string $attributeParam
 * @var string $query
 */

SelectizeAsset::register($this);
$this->title = Yii::t('app', 'Search by «{attribute}» attribute', [
    'attribute' => $attribute->alias === 'tag' ? Yii::t('app', 'Tags') : $attribute->getTitle()
]);
if ($city) {
    $this->title .= Yii::t('seo', ' in {city}', ['city' => $city->getTitle()]);
}
?>
    <div class="container-fluid">
        <h1 class="text-center" style="color: #000; margin-bottom: 15px;"><?= $this->title ?></h1>
        <?= Html::beginForm(Url::to(['/attribute-value/search', 'alias' => $attribute->alias]), 'get')?>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <?= Html::textInput('query', $query, ['placeholder' => Yii::t('app', 'Enter your request')]) ?>
                </div>
                <div class="col-md-4 selectize-input-container">
                    <?= Html::dropDownList('city',
                        $city ? $city->alias : null,
                        $city ? [$city->alias => $city->getTitle()] : [],
                        ['id' => 'city-search-input', 'placeholder' => Yii::t('app', 'Select a city')]
                    ) ?>
                </div>
                <div class="col-md-2">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn-mid'])?>
                </div>
            </div>
        <?= Html::endForm()?>
        <div class="tags-container text-center">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_search_item',
                'viewParams' => [
                    'city' => $city ? $city->alias : null,
                    'baseUrl' => $baseUrl,
                    'attributeParam' => $attributeParam,
                ],
                'layout' => "{items}<div class='clearfix'></div>{pager}",
                'options' => [
                    'tag' => 'div',
                    'class' => 'tags-list'
                ],
                'itemOptions' => [
                    'tag' => false,
                ],
                'emptyText' =>
                    '<p>' . Yii::t('app', 'Attribute values not found') . '</p>'
            ]); ?>
        </div>
    </div>

<?php
$cityListUrl = Url::to(['/ajax/city-list']);
$script = <<<JS
    $("#city-search-input").selectize({
        valueField: "alias",
        labelField: "title",
        searchField: ["title"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.post('$cityListUrl', {query: encodeURIComponent(query)}, function(data) {
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });
JS;
$this->registerJs($script);
?>