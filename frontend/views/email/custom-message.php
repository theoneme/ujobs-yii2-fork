<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.01.2017
 * Time: 18:28
 */

use common\models\user\User;
use yii\helpers\Html;

/**
 * @var User $user
 * @var mixed $content
 * @var mixed $link
 * @var mixed $linkLabel
 */

$locale = 'ru-RU';
if (isset($user)) {
    $name = $user->getSellerName();
    $locale = $user->site_language;
}

?>
<table style="padding:30px 10px;background:#eee;width:100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
                <tbody>
                <tr>
                    <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                        <a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
                           style="text-decoration: none;display: block;">
                            <?= Html::img('https://ujobs.me/images/new/email/logo.png', ['alt' => 'logo']) ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                        <table align="center" style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="color:#666;text-align:center;">
                                    <?php if (isset($user)) { ?>
                                        <table style="margin:auto; width: 100%;" align="center">
                                            <tbody>
                                            <tr>
                                                <td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
                                                    <?= Yii::t('notifications', 'Hi, {username}!', ['username' => $name, 'site' => Yii::$app->name], $locale) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px"></td>
                                                </td></tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                    <?php echo $this->render('problem-items/common', [
                                        'text' => $content,
                                        'link' => $link,
                                        'linkLabel' => $linkLabel ?? Yii::t('notifications', 'Go to view', [], $locale),
                                        'user' => $user
                                    ]) ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:#aaa;padding:15px;font-size:11px;line-height:15px;text-align:left">
                                    <div style="color:#aaa;font-family:arial">
                                        <?= Yii::t('notifications', 'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).', ['email' => 'message@ujobs.me'], $locale) ?>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="max-width:650px" align="center">
                <tbody>
                <tr>
                    <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px">
                        © <?= Yii::$app->name ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>