<?php

use common\models\user\User;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var User $user
 * @var string $jobs
 */

$name = $user->getSellerName();

?>
<table style="padding:30px 10px;background:#eee;width:100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
                <tbody>
                <tr>
                    <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                        <a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
                           style="text-decoration: none;display: block;">
                            <?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                        <table align="center" style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="border-bottom:1px solid #dfdfd0;color:#666;text-align:center;padding-bottom:30px">
                                    <table style="margin:auto; width: 100%;" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
                                                <?= Yii::t('notifications', 'Hi, {username}!', ['username' => $name], $user->site_language) ?>
                                            </td>
                                            <!-- <td rowspan="3">
                                                <?= Html::img(Url::to('/images/new/icons/support-icon.png', true), ['alt' => 'uJobs', 'width' => '64']) ?>
                                            </td>-->
                                        </tr>
                                        <tr>
                                            <td style="height: 10px"></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#666;font-size:16px;font-weight:bold;text-align:left">
                                                <?= Yii::t('notifications', 'You still have services in cart that are awaiting for payment. You wanted to buy: {items}', ['items' => $jobs], $user->site_language) ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="margin:auto; width: 100%;" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="height: 10px"></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#666;font-size:18px;text-align:left">
                                                <?= Yii::t('notifications', 'Press button to return to cart', [], $user->site_language) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="margin:auto" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                                <a target="_blank" href="<?= Url::to(['/store/cart/index'], true) ?>"
                                                   style="padding:10px 60px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold">
                                                    <?= Yii::t('notifications', 'Go to cart', [], $user->site_language) ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px;">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="margin:auto; width: 100%;" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="color:#666;font-size:18px;text-align:left">
                                                <?= Yii::t('notifications', 'If you have any troubles with payment, you can {contact_our_support}.', [
                                                    'contact_our_support' => Html::a(
                                                        Yii::t('notifications', 'contact our support', [], $user->site_language),
                                                        Url::to(['/account/inbox/start-conversation', 'user_id' => Yii::$app->params['supportId'], 'email-action' => 1], true)
                                                    )
                                                ], $user->site_language) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:#aaa;padding:15px;font-size:11px;line-height:15px;text-align:left">
                                    <div style="color:#aaa;font-family:arial">
                                        <?= Yii::t('notifications', 'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).', ['email' => 'message@ujobs.me'], $user->site_language) ?>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="max-width:650px" align="center">
                <tbody>
                <tr>
                    <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px">
                        © <?= Yii::$app->name ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>