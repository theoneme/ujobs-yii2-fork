<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\User;

/**
 * @var User $user
 * @var string $language
 * @var integer $client_id
 */
?>

<table style="padding:30px 10px;background:#eee;width:100%;font-family:arial" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="margin: 0 auto;max-width:650px;min-width:320px" cellspacing="0" align="center">
                <tbody>
                <tr>
                    <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                        <a href="https://ujobs.me" target="_blank" data-saferedirecturl="" style="text-decoration: none;display: block;">
                            <?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                        <table align="center">
                            <tbody>
                            <tr>
                                <td style="color:#666;text-align:left">
                                    <table style="margin:auto" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="text-align:center;padding-bottom:5px">
                                                <?php if (!empty($user->profile->gravatar_email) || ($user->is_company && !empty($user->company->logo))) {
                                                    echo Html::img($user->getThumb('catalog'), [
                                                        'style' => 'width: 60px; height: 60px; -webkit-border-radius: 30px; -moz-border-radius: 30px;border-radius: 30px;'
                                                    ]);
                                                } else {
                                                    echo Html::img(Url::to('/images/new/email/message.png', true));
                                                }  ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#005f84;font-size:16px;font-weight:bold;text-align:left;font-family:arial">
                                                <p style="color:#005f84;font-size:16px;font-weight:bold;margin: 5px 0;">
                                                    <?= Yii::t('notifications', 'Greetings!') ?>
                                                </p>
                                                <p style="color:#005f84;font-size:16px;font-weight:bold;margin: 5px 0;">
                                                    <?= Yii::t('notifications', '{user} sent you invitation to join uJobs.', ['user' => $user->getSellerName()], $language) ?>
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table style="margin:10px auto 0;width:100%" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="color:#666;font-size:16px;text-align:left;font-family:arial">
                                                <div style="padding-bottom:15px;font-family:arial;line-height:20px;text-align:left">
                                                    <p style="margin: 5px 0">
                                                        <?= Yii::t('notifications', 'On {site} you can find a lot of services for construction or buy a ready property', ['site' => Yii::$app->name], $language) ?>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="margin:auto" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                                <?= Html::a(
                                                    Yii::t('notifications', 'Join Now', [], $language),
                                                    Url::to(['/site/index', 'client_id' => $client_id, 'email-action' => 1], true),
                                                    [
                                                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                                                        'target' => '_blank'
                                                    ]
                                                ) ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="max-width:650px" align="center">
                <tbody>
                <tr>
                    <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px;font-family:arial">
                        © <?= Yii::$app->name ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>