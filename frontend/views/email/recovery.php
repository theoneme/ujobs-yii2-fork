<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.12.2016
 * Time: 11:45
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var common\models\user\User $user
 * @var common\models\user\Token $token
 */
if (!empty($user->profile->name)) {
    $name = $user->profile->name;
}
elseif (!empty($user->username)) {
    $name = $user->username;
}
else {
    $name = $user->email;
}
?>

<table style="padding:30px 10px;background:#eee;width:100%;font-family:arial" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
                <tbody>
                <tr>
                    <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                        <a href="https://ujobs.me" target="_blank" data-saferedirecturl="" style="text-decoration: none;display: block;">
	                        <?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                        <table align="center">
                            <tbody>
                            <tr>
                                <td style="color:#666;text-align:center">
                                    <table style="margin:auto" align="center">
                                        <tbody>

                                        <tr>
                                            <td style="color:#666;font-size:20px;font-weight:bold;text-align:center;font-family:arial"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:#666;padding:15px;font-size:14px;line-height:18px;text-align:left">
                                    <div style="margin-bottom:25px">
                                        <p><?= Yii::t('notifications', 'Hi, {username}', ['username' => $name], $user->site_language) ?>,</p>

                                        <p><?= Yii::t('notifications', 'We got a request to reset your uJobs password.', [], $user->site_language) ?></p>

                                        <p>
                                            <?= Yii::t('notifications', 'To start the process, please click the following link:<br>{link}',['link' => ''], $user->site_language)?>
                                        </p>
                                        <table style="margin:auto" align="center">
                                            <tbody>
                                            <tr>
                                                <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                                    <?= Html::a(Yii::t('notifications', 'Change Password', [], $user->site_language), $token->url, [
                                                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                                                        'target' => '_blank'
                                                    ]) ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <p><?= Yii::t('notifications', "If the link above does not work, copy and paste the URL in a new browser tab. For security reasons, the link is valid for 24 hours.", [], $user->site_language) ?></p>
                                    </div>
                                    <div style="padding-top:10px;text-align:center;font-family:arial">
                                        <?= Yii::t('notifications', 'The uJobs Team', [], $user->site_language) ?>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="max-width:650px" align="center">
                <tbody>
                <tr>
                    <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px;font-family:arial">
                        © <?= Yii::$app->name ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>