<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.01.2017
 * Time: 11:36
 */

/* @var mixed $link */
/* @var mixed $text */
/* @var string $linkLabel */

?>

<table style="margin:auto; width: 100%;" align="center">
    <tbody>
    <tr>
        <td style="height: 10px"></td>
    </tr>
    <tr>
        <td style="color:#666;font-size:18px;text-align:left">
            <?= $text ?>
        </td>
    </tr>
    <tr>
        <td style="height: 20px"></td>
    </tr>
    </tbody>
</table>

<?php if (isset($link)) { ?>
    <table style="margin:auto" align="center">
        <tbody>
        <tr>
            <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
	            <a href="<?php echo $link?>" style = "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold" target = '_blank'>
					<?php echo $linkLabel?>
				</a>
            </td>
        </tr>
        <tr>
            <td style="height: 10px;">
            </td>
        </tr>
        </tbody>
    </table>
<?php } ?>
