<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.01.2017
 * Time: 18:28
 */

use common\models\user\User;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $name
 * @var mixed $content
 * @var mixed $link
 * @var mixed $linkLabel
 * @var string $locale
 */

?>
<table style="padding:30px 10px;background:#eee;width:100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
                <tbody>
                <tr>
                    <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                        <a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
                           style="text-decoration: none;display: block;">
                            <?= Html::img('https://ujobs.me/images/new/email/logo.png', ['alt' => 'logo']) ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                        <table align="center" style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="color:#666">
                                    <table style="margin:auto; text-align:center;" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="text-align:center;padding-bottom:5px">
                                                <?= Html::img(Url::to('/images/new/email/message.png', true), [
                                                    'alt' => Yii::t('notifications', 'New Message', [], $locale)
                                                ]) ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="color:#005f84;font-size:16px;font-weight:bold;text-align:center;font-family:arial">
                                                <?= Yii::t('notifications', 'Auto-reply', [], $locale) ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <p style="font-size:16px;margin-bottom:0; text-align: left;"><?= Yii::t('notifications', 'Dear {username}', ['username' => $name], $locale) ?>!</p>

                                    <p style="font-size:16px;margin-top:5px; text-align: left;"><?= Yii::t('notifications', 'Thanks for your message. We will contact you as soon as possible!') ?></p>

                                    <p style="font-size:16px;margin-top:5px; text-align: left;"><?= Yii::t('notifications', 'If you wish to post announcement on our site for free, please press the following button.', [], $locale) ?></p>

                                    <table style="margin:auto" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                                <a href="<?php echo $link?>" style = "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold" target = '_blank'>
                                                    <?php echo $linkLabel?>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px;">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="max-width:650px" align="center">
                <tbody>
                <tr>
                    <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px">
                        © <?= Yii::$app->name ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

