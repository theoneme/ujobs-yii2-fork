<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.12.2016
 * Time: 19:16
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\User;
use common\models\Message;

/* @var Message $message */
/* @var User $user */
/* @var User $user2 */
/* @var string $heading */

$name = $user->getSellerName();
$name2 = $user2->getSellerName();

?>

<table style="padding:30px 10px;background:#eee;width:100%;font-family:arial" cellspacing="0" cellpadding="0">
	<tbody>
	<tr>
		<td>
			<table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
				<tbody>
				<tr>
					<td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
						<a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
						   style="text-decoration: none;display: block;">
							<?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
						</a>
					</td>
				</tr>
				<tr>
					<td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
						<table align="center">
							<tbody>
							<tr>
								<td style="color:#666;text-align:left">
									<table style="margin:auto" align="center">
										<tbody>
										<tr>
											<td style="text-align:center;padding-bottom:5px">
												<?= Html::img(Url::to('/images/new/email/message.png', true), [
													'alt' => Yii::t('notifications', 'New Message', [], $user->site_language)
												]) ?>
											</td>
										</tr>

										<tr>
											<td style="color:#005f84;font-size:16px;font-weight:bold;text-align:center;font-family:arial">
												<?= Yii::t('notifications', 'New Message', [], $user->site_language) ?>
											</td>
										</tr>
										</tbody>
									</table>

									<p style="font-size:16px;margin-bottom:0"><?= Yii::t('notifications', 'Dear {username}', ['username' => $name], $user->site_language) ?>,</p>

									<p style="font-size:16px;margin-top:5px"><?= $heading ?>:</p>

									<table style="margin:auto;width:100%" align="center">
										<tbody>
										<tr>
											<td style="color:#666;font-size:16px;padding-bottom:30px;text-align:left;font-family:arial">
												<div
													style="font-style:italic;padding-bottom:15px;font-family:arial;line-height:20px;text-align:left">
													"<?= Html::encode($content) ?>"
												</div>
											</td>
										</tr>
										</tbody>
									</table>

									<table style="margin:auto" align="center">
										<tbody>
										<tr>
											<td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                                <a href="<?php echo $link?>" style = "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold" target = '_blank'>
                                                    <?php echo Yii::t('notifications', 'View and respond', [], $user->site_language) ?>
                                                </a>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="max-width:650px" align="center">
				<tbody>
				<tr>
					<td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px;font-family:arial">
						© <?= Yii::$app->name ?>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>