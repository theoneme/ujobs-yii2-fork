<?php
use common\models\Job;
use common\models\user\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $jobs Job[]*/
/* @var $name string*/
/* @var $jobName string*/
/* @var $user User */

?>

<div style="font-family:Helvetica Light,Helvetica,Arial,sans-serif;margin:0;padding:0;width:100%;" bgcolor="#eeeeee">
<div style="background:#eee;width:100%">
    <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee">
        <tbody>
        <tr>
            <td bgcolor="#eeeeee" align="center">
                <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#1f1f1f" align="center">
                    <tbody>
                    <tr>
                        <td style="padding:10px 20px;width:100%" width="100%" bgcolor="#eee">
                            <a href="https://ujobs.me" target="_blank" data-saferedirecturl="" style="text-decoration: none;display: block;">
	                            <?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
    <td style="padding:0 15px" bgcolor="#eeeeee" align="center">
        <table style="border-collapse:collapse;max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
            <tbody>
            <tr>
                <td>
                    <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td>
                                <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:20px;font-weight:bold;padding:20px 5% 0" align="center"><?= Yii::t('notifications', 'Hi, {username}!', ['username' => $name], $user->site_language) ?></td>
                                    </tr>
                                    <tr>
                                        <td width="100%" valign="top" height="15" bgcolor="#ffffff" align="center"></td>
                                    </tr>
                                    <tr>
                                        <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:16px;padding:0 5%" class="m_1050845079695132951padding" align="center">
                                            <?= Yii::t('notifications', 'You did order {title} recently.', ['title' => $jobName], $user->site_language) ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" valign="top" height="15" bgcolor="#ffffff" align="center"></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 30px" width="100%" valign="top" height="1" bgcolor="#ffffff" align="center">
                                            <table style="border-collapse:collapse" width="30%" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                <tr>
                                                    <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" valign="top" height="15" bgcolor="#ffffff" align="center"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td style="padding:0 15px" bgcolor="#eeeeee" align="center">
        <table style="border-collapse:collapse;max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
            <tbody>
            <tr>
                <td>
                    <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:16px;line-height:28px;padding:0 10%" class="" valign="top" align="center">
                                <?= Yii::t('notifications', 'Thanks for using our service! We offer you similar services you may be interested in:', [], $user->site_language) ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" valign="top" height="20" bgcolor="#ffffff" align="center"></td>
            </tr>
            <tr>
                <td width="100%" valign="top" height="100%" align="center">
                    <table style="border-collapse:collapse;max-width:540px" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                            <td style="font-size:0" valign="top" align="center">
                                <?php foreach($jobs as $key => $job) { ?>
                                <div style="display:inline-block;max-width:50%;min-width:240px;vertical-align:top;width:100%">
                                    <table style="border-collapse:collapse;max-width:240px" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                        <tbody>
                                        <tr>
                                            <td valign="top" align="center">
                                                <table style="border-collapse:collapse;margin-bottom:20px" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border:1px solid #e0e0e0;border-radius:2px;padding:0 0 10px">
                                                            <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td valign="middle" bgcolor="#ffffff" align="center">
                                                                        <?= Html::a(Html::img(Url::to($job->getThumb('catalog'), true), [
                                                                            'style' => "color:#666666;display:block;font-family:Helvetica,arial,sans-serif;font-size:13px;height:200px;width:243px",
                                                                            'alt' => $job->translation->title,
                                                                            'width' => '243',
                                                                            'border' => 0,
                                                                            'height' => '200'
                                                                        ]), Url::to(['/job/view', 'alias' => $job->alias, 'email-action' => 1], true), [
                                                                            'style' => "color:#00698c;font-weight:bold;text-decoration:none",
                                                                            'target' => '_blank'
                                                                        ]); ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color:#333333;font-family:Arial,sans-serif;font-size:16px;height:64px;padding:5px 8px 0" bgcolor="#ffffff" align="center"><?=Html::encode($job->translation->title)?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color:#666666;font-family:Arial,sans-serif;font-size:14px;line-height:20px;padding:5px 8px 0" bgcolor="#ffffff" align="center">
                                                                        <?= Html::a(Yii::t('notifications', 'Order for {price}', ['price' => $job->packages[0]->getPrice()], $user->site_language), Url::to(['/job/view', 'alias' => $job->alias, 'email-action' => 1], true), [
                                                                            'style' => "background:#00b422;border-radius:2px;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:bold;line-height:20px;margin-top:10px;padding:7px 6px;text-align:center;text-decoration:none",
                                                                            'target' => '_blank'
                                                                        ]); ?>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php if($key%2 != 0 && $key != 0) { ?>
                            </td></tr><tr><td width="100%" valign="top" height="10" bgcolor="#ffffff" align="center"></td></tr><tr><td style="font-size:0" valign="top" align="center">
                                <?php } ?>
                                <?php } ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" valign="top" height="10" bgcolor="#ffffff" align="center"></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td style="padding:20px 0" bgcolor="#eeeeee" align="center">
        <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
            <tr>
                <td style="color:#818181;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:12px;line-height:1.5;padding-top:5px" align="center">
                    <table style="border-collapse:collapse;text-align:center;width:100%">
                        <tbody>
                        <tr>
                            <td style="color:#535353;font-size:10px;line-height:16px;padding-bottom:20px" align="center">
                                <span style="font-size:12px">
                                    <span style="font-family:arial,helvetica neue,helvetica,sans-serif">
                                        <!--<a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Privacy Policy</a> | <a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Support</a>&nbsp;| <a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Invite a Friend</a><br>-->
                                        <?=Yii::t('notifications', 'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}', ['site' => Yii::$app->name], $user->site_language)?><br>
                                        <br><br>© <?=Yii::$app->name?>
                                    </span>
                                </span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
</div>