<?php
use common\models\Job;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $jobs Job[] */

if (!empty($user->profile->name)) {
    $name = $user->profile->name;
} elseif (!empty($user->username)) {
    $name = $user->username;
} else {
    $name = $user->email;
}
?>

<div style="font-family:Helvetica Light,Helvetica,Arial,sans-serif;margin:0;padding:0;width:100%;" bgcolor="#eeeeee">
    <div style="background:#1f1f1f;width:100%">
        <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0"
               bgcolor="#eeeeee">
            <tbody>
            <tr>
                <td bgcolor="#eeeeee" align="center">
                    <table style="border-collapse:collapse;max-width:600px"
                           class="m_1050845079695132951responsive-table" width="100%" cellspacing="0" cellpadding="0"
                           border="0" bgcolor="#1f1f1f" align="center">
                        <tbody>
                        <tr>
                            <td style="padding:10px 20px;width:100%" width="100%" bgcolor="#1f1f1f">
                                <a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
                                   style="text-decoration: none;display: block;">
                                    <?= Html::img(Url::to('/images/new/email/logo.svg', true), ['alt' => 'logo', 'style' => 'width:110px']) ?>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr>
            <td style="padding:0 15px" class="m_1050845079695132951section-padding" bgcolor="#eeeeee" align="center">
                <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table"
                       width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                    <tbody>
                    <tr>
                        <td>
                            <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0"
                                   border="0">
                                <tbody>
                                <tr>
                                    <td align="center">
                                        <?= Html::a(Html::img(Url::to('/images/new/email/banner.jpg', true), [
                                            'style' => "color:#666666;display:block",
                                            'alt' => Yii::t('notifications', 'Welcome to {site}', ['site' => Yii::$app->name]),
                                            'width' => '100%'
                                        ]), Url::to(['/site/index', 'email-action' => 1], true)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:18px;font-weight:bold;padding:20px 5% 0"
                                                    align="center"><?= Yii::t('notifications', 'Hi, {username}', ['username' => $name], $user->site_language) ?>
                                                    ,
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:20px 5% 0"
                                                    align="center"><?= Yii::t('notifications', "You have just become part of the largest, most affordable and easiest to use marketplace for services and products!", [], $user->site_language) ?></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 30px" width="100%" valign="top" height="1"
                                                    bgcolor="#ffffff" align="center">
                                                    <table style="border-collapse:collapse" width="30%" cellspacing="0"
                                                           cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:21px;font-weight:bold;padding:0 5%"
                                                    class="m_1050845079695132951padding"
                                                    align="center"><?= Yii::t('notifications', "It is never been so easy to get it done", [], $user->site_language) ?></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 30px" width="100%" valign="top" height="1"
                                                    bgcolor="#ffffff" align="center">
                                                    <table style="border-collapse:collapse" width="30%" cellspacing="0"
                                                           cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:32px;padding:0 5%"
                                                    valign="top" bgcolor="#ffffff" align="left">
                                                    <?= Html::img(Url::to('/images/new/email/money.jpg', true), [
                                                        'style' => "color:#666666;margin-right:15px;",
                                                        'valign' => 'bottom',
                                                        'border' => 0,
                                                    ]) ?>
                                                    <?= Yii::t('notifications', 'Additional income') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:0 5% 20px"
                                                    bgcolor="#ffffff" align="left">
                                                    <?= Yii::t('notifications', 'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 30px" width="580" valign="top" height="1" bgcolor="#ffffff"
                                        align="center">
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                <td>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:32px;padding:20px 5% 0"
                                                    valign="top" bgcolor="#ffffff" align="left">
                                                    <?= Html::img(Url::to('/images/new/email/find.jpg', true), [
                                                        'style' => "color:#666666;margin-right:15px;",
                                                        'alt' => Yii::t('notifications', 'Welcome to {site}', ['site' => Yii::$app->name], $user->site_language),
                                                        'valign' => 'bottom',
                                                        'border' => 0,
                                                    ]) ?>
                                                    <?= Yii::t('notifications', 'Looking for service or order', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:0 5% 20px"
                                                    bgcolor="#ffffff" align="left">
                                                    <?= Yii::t('notifications', 'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 30px" width="580" valign="top" height="1" bgcolor="#ffffff"
                                        align="center">
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                <td>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:32px;padding:20px 5% 0"
                                                    valign="top" bgcolor="#ffffff" align="left">
                                                    <?= Html::img(Url::to('/images/new/email/lock.jpg', true), [
                                                        'style' => "color:#666666;margin-right:15px;",
                                                        'valign' => 'bottom',
                                                        'border' => 0,
                                                    ]) ?>
                                                    <?= Yii::t('notifications', 'Deal safety!', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:0 5%"
                                                    bgcolor="#ffffff" align="left">
                                                    <?= Yii::t('notifications', 'It is really that simple.', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="top" height="15" bgcolor="#eeeeee" align="center"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:arial,sans-serif;font-size:28px;font-weight:bold;line-height:32px;padding:30px 30px 0"
                                                    valign="top" bgcolor="#ffffff" align="center">
                                                    <?= Yii::t('notifications', 'Ready to get started?', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 30px" width="100%" valign="top" height="1"
                                                    bgcolor="#ffffff" align="center">
                                                    <table style="border-collapse:collapse" width="250" height="46"
                                                           cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-radius:2px" height="46" bgcolor="#00b422"
                                                                align="center">
                                                                <?= Html::a(Yii::t('notifications', 'Start Earning', [], $user->site_language), Url::to(['/account/profile/start-selling', 'email-action' => 1], true), [
                                                                    'style' => "color:#ffffff;display:inline-block;font-family:'Helvetica Neue',arial;font-size:17px;font-weight:bold;line-height:46px;max-width:280px;text-align:center;text-decoration:none",
                                                                    'target' => '_blank'
                                                                ]); ?>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#35343a;font-family:Arial,sans-serif;font-size:15px;font-weight:normal;line-height:25px;padding:0 30px"
                                                    bgcolor="#ffffff" align="center">
                                                    <?= Yii::t('notifications', 'Do you need to get a job done?', [], $user->site_language) ?><br>
                                                    <?= Html::a(Yii::t('notifications', 'Find service that you need!'), Url::to(['/site/index', 'email-action' => 1], true), [
                                                        'style' => "color:#00698c;font-weight:bold;text-decoration:none",
                                                        'target' => '_blank'
                                                    ]); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="top" height="15" bgcolor="#eeeeee" align="center"></td>
                                </tr>
                                <tr>
                                    <td style="font-size:0" class="m_1050845079695132951padding" valign="top"
                                        align="center">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top" height="20" bgcolor="#ffffff" align="center"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:0 15px" class="m_1050845079695132951section-padding" bgcolor="#eeeeee" align="center">
                <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table"
                       width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                    <tbody>
                    <tr>
                        <td>
                            <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0"
                                   border="0">
                                <tbody>
                                <tr>
                                    <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:28px;padding:0 10%"
                                        class="" valign="top"
                                        align="center"><?= Yii::t('notifications', 'Check the latest offers', [], $user->site_language) ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top" height="20" bgcolor="#ffffff" align="center"></td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top" height="100%" align="center">
                            <table style="border-collapse:collapse;max-width:540px" width="100%" cellspacing="0"
                                   cellpadding="0" border="0" align="center">
                                <tbody>
                                <tr>
                                    <td style="font-size:0" class="m_1050845079695132951padding" valign="top"
                                        align="center">
                                        <?php foreach ($jobs as $key => $job) { ?>
                                        <div style="display:inline-block;max-width:50%;min-width:240px;vertical-align:top;width:100%"
                                             class="m_1050845079695132951wrapper">
                                            <table style="border-collapse:collapse;max-width:240px"
                                                   class="m_1050845079695132951wrapper" width="100%" cellspacing="0"
                                                   cellpadding="0" border="0" align="left">
                                                <tbody>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <table style="border-collapse:collapse;margin-bottom:20px"
                                                               width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="border:1px solid #e0e0e0;border-radius:2px;padding:0 0 10px">
                                                                    <table style="border-collapse:collapse" width="100%"
                                                                           cellspacing="0" cellpadding="0" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td valign="middle" bgcolor="#ffffff"
                                                                                align="center">
                                                                                <?= Html::a(Html::img(Url::to($job->getThumb('catalog'), true), [
                                                                                    'style' => "color:#666666;display:block;font-family:Helvetica,arial,sans-serif;font-size:13px;height:200px;width:243px",
                                                                                    'alt' => $job->translation->title,
                                                                                    'width' => '243',
                                                                                    'border' => 0,
                                                                                    'height' => '200'
                                                                                ]), Url::to(['/job/view', 'alias' => $job->alias, 'email-action' => 1], true), [
                                                                                    'style' => "color:#00698c;font-weight:bold;text-decoration:none",
                                                                                    'target' => '_blank'
                                                                                ]); ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="color:#333333;font-family:Arial,sans-serif;font-size:16px;height:64px;padding:5px 8px 0"
                                                                                bgcolor="#ffffff"
                                                                                align="center"><?= Html::encode($job->translation->title) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="color:#666666;font-family:Arial,sans-serif;font-size:12px;line-height:20px;padding:5px 8px 0"
                                                                                bgcolor="#ffffff"
                                                                                align="center"><?= Html::encode($job->user->username) ?></td>
                                                                        </tr>
                                                                        <!--<tr>
                                                                            <td style="color:#666666;font-family:Arial,sans-serif;font-size:14px;line-height:20px;padding:5px 8px 0" bgcolor="#ffffff" align="center">
                                                                                <img src="https://ci4.googleusercontent.com/proxy/u--KQJjRokggXnkxEA3EpW8R3z4G06Ca3T6vBL3v_xjwY_sUBDbjxxYXV1TNVbpEzFBpXVcEWgHLE_4nJxbmC_Q6zjhNgV01nqajdmLepAt2n25snUzgOaRGVWnre0OpOdnIBktgfhi7aGGcp83Bw7O5dfQ8BFyshgJJKlQ=s0-d-e1-ft#https://gallery.mailchimp.com/e271c966703e72a2f760a2499/images/4c297244-a206-4acb-a470-413f73f05ddc.png" alt="rating" valign="middle" class="CToWUd"><span style="color:#a1a1a1;font-size:12px">&nbsp;&nbsp; (865)</span>
                                                                            </td>
                                                                        </tr>-->
                                                                        <tr>
                                                                            <td style="color:#666666;font-family:Arial,sans-serif;font-size:14px;line-height:20px;padding:5px 8px 0"
                                                                                bgcolor="#ffffff" align="center">
                                                                                <?= Html::a(Yii::t('app', 'Order for {price}', ['price' => $job->packages[0]->getPrice()], $user->site_language), Url::to(['/job/view', 'alias' => $job->alias, 'email-action' => 1], true), [
                                                                                    'style' => "background:#00b422;border-radius:2px;color:#fff;display:block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:bold;line-height:20px;margin-top:10px;padding:7px 6px;text-align:center;text-decoration:none",
                                                                                    'target' => '_blank'
                                                                                ]); ?>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if ($key % 2 != 0 && $key != 0) { ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="top" height="10" bgcolor="#ffffff" align="center"></td>
                                </tr>
                                <tr>
                                    <td style="font-size:0" class="m_1050845079695132951padding" valign="top"
                                        align="center">
                                        <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top" height="10" bgcolor="#ffffff" align="center"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:20px 0" bgcolor="#eeeeee" align="center">
                <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table"
                       width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td style="color:#818181;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:12px;line-height:1.5;padding-top:5px"
                            align="center">
                            <table style="border-collapse:collapse;text-align:center;width:100%">
                                <tbody>
                                <tr>
                                    <td style="color:#535353;font-size:10px;line-height:16px;padding-bottom:20px"
                                        align="center">
                                <span style="font-size:12px">
                                    <span style="font-family:arial,helvetica neue,helvetica,sans-serif">
                                        <!--<a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Privacy Policy</a> | <a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Support</a>&nbsp;| <a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Invite a Friend</a><br>-->
                                        <?= Yii::t('notifications', 'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}', ['site' => Yii::$app->name], $user->site_language) ?>
                                        <br>
                                        <br><br>© <?= Yii::$app->name ?>
                                    </span>
                                </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>