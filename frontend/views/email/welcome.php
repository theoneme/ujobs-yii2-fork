<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.12.2016
 * Time: 11:45
 */

use dektrium\user\Module;
use common\models\user\Token;
use common\models\user\User;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var User $user
 * @var Token $token
 * @var boolean $showPassword
 * @var Module $module
 */

$name = $user->getSellerName();

?>
<table style="padding:30px 10px;background:#eee;width:100%" cellspacing="0" cellpadding="0">
	<tbody>
	<tr>
		<td>
			<table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
				<tbody>
				<tr>
					<td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
						<a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
						   style="text-decoration: none;display: block;">
							<?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
						</a>
					</td>
				</tr>
				<tr>
					<td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
						<table align="center" style="width: 100%">
							<tbody>
							<tr>
								<td style="border-bottom:1px solid #dfdfd0;color:#666;text-align:center;padding-bottom:30px">
									<table style="margin:auto; width: 100%;" align="center">
										<tbody>
										<tr>
											<td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
												<?= Yii::t('notifications', 'Hi, {username}, <br> Welcome to {site}!', ['username' => $name, 'site' => Yii::$app->name], $user->site_language) ?>
											</td>
										</tr>
										<tr>
											<td style="height: 10px"></td>
										</tr>
										<tr>
											<td style="color:#666;font-size:18px;text-align:left">
												<?= Yii::t('notifications', 'Thanks for registration on our portal!', [], $user->site_language) ?>
											</td>
										</tr>
										<tr>
											<td style="height: 10px"></td>
										</tr>
										</tbody>
									</table>
									<?php if ($showPassword || $module->enableGeneratingPassword) { ?>
										<table style="margin:auto; width: 100%;" align="center">
											<tbody>
											<tr>
												<td style="color:#666;font-size:20px;text-align:left">
													<?= Yii::t('user', 'We have generated a password for you', [], $user->site_language) ?>:
													<strong><?= $user->password ?></strong>
												</td>
											</tr>
											<tr>
												<td style="color:#666;font-size:18px;text-align:left">
													<?= Yii::t('notifications', 'Which you can change in your account settings', [], $user->site_language) ?>
												</td>
											</tr>
											</tbody>
										</table>
									<?php } ?>

									<table style="margin:auto; width: 100%;" align="center">
										<tbody>
										<tr>
											<td style="height: 10px"></td>
										</tr>
										<tr>
											<td style="color:#666;font-size:18px;text-align:left">
												<?= Yii::t('notifications', '<strong> uJobs.me </strong> is a freelance market, where you can place your services for free, get new customers or hire a freelancer for the job you need to do.', [], $user->site_language) ?>
											</td>
										</tr>
										<tr>
											<td style="height: 20px"></td>
										</tr>
										</tbody>
									</table>

									<?php if ($token !== null) { ?>
										<table style="margin:auto" align="center">
											<tbody>
											<tr>
												<td style="color:#666;font-size:16px;padding-bottom:30px;text-align:center;font-family:arial">
													<?= Yii::t('notifications', 'Click on the link below to confirm your email and get started.', [], $user->site_language) ?>
												</td>
											</tr>
											</tbody>
										</table>

										<table style="margin:auto" align="center">
											<tbody>
											<tr>
												<td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
													<?= Html::a(Yii::t('notifications', 'Activate Your Account', [], $user->site_language), $token->url, [
														'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
														'target' => '_blank'
													]); ?>
												</td>
											</tr>
											</tbody>
										</table>
									<?php } else { ?>
										<table style="margin:auto" align="center">
											<tbody>
											<tr>
												<td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
													<?= Html::a(Yii::t('notifications', 'Visit Website', [], $user->site_language), Yii::$app->urlManager->createAbsoluteUrl('/'), [
														'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
														'target' => '_blank'
													]); ?>
												</td>
											</tr>
											</tbody>
										</table>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td style="color:#aaa;padding:15px;font-size:11px;line-height:15px;text-align:left">
									<?php if ($token !== null) { ?>
										<div style="color:#aaa;padding-bottom:15px;font-family:arial">
											<?= Yii::t('notifications', 'If the button above does not work, copy and paste the following URL into your browser\'s address bar.', [], $user->site_language) ?>
											:
											<?= Html::a(Yii::t('notifications', 'Activate Your Account', [], $user->site_language), $token->url, [
												'style' => "color:#00b22c",
												'target' => '_blank'
											]); ?>
										</div>
									<?php } ?>
									<?= Yii::t('notifications', 'Or you can unsubscribe from email messages by this link {link}).', ['link' => Html::a(Yii::t('account', 'Unsubscribe', [], $user->site_language), Url::to(['/site/unsubscribe', 'id' => $user->id, 'created_at' => $user->created_at, 'updated_at' => $user->updated_at], true))], $user->site_language) ?>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="max-width:650px" align="center">
				<tbody>
				<tr>
					<td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px">
						© <?= Yii::$app->name ?>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>