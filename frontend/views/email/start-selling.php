<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.12.2016
 * Time: 17:00
 */

use yii\helpers\Html;
use yii\helpers\Url;

if (!empty($user->profile->name)) {
    $name = $user->profile->name;
} elseif (!empty($user->username)) {
    $name = $user->username;
} else {
    $name = $user->email;
}
?>

<div style="font-family:Helvetica Light,Helvetica,Arial,sans-serif;margin:0;padding:0;width:100%;" bgcolor="#eeeeee">
    <div style="background:#1f1f1f;width:100%">
        <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0"
               bgcolor="#eeeeee">
            <tbody>
            <tr>
                <td bgcolor="#eeeeee" align="center">
                    <table style="border-collapse:collapse;max-width:600px"
                           class="m_1050845079695132951responsive-table" width="100%" cellspacing="0" cellpadding="0"
                           border="0" bgcolor="#1f1f1f" align="center">
                        <tbody>
                        <tr>
                            <td style="padding:10px 20px;width:100%" width="100%" bgcolor="#1f1f1f">
                                <a href="https://ujobs.me" target="_blank" data-saferedirecturl=""
                                   style="text-decoration: none;display: block;">
                                    <?= Html::img(Url::to('/images/new/email/logo.svg', true), ['alt' => 'logo', 'style' => 'width:110px']) ?>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr>
            <td style="padding:0 15px" class="m_1050845079695132951section-padding" bgcolor="#eeeeee" align="center">
                <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table"
                       width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                    <tbody>
                    <tr>
                        <td>
                            <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0"
                                   border="0">
                                <tbody>
                                <tr>
                                    <td align="center">
                                        <?= Html::a(Html::img(Url::to('/images/new/email/banner.jpg', true), [
                                            'style' => "color:#666666;display:block",
                                            'width' => '100%'
                                        ]), Url::to(['/site/index', 'email-action' => 1], true)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:18px;font-weight:bold;padding:20px 5% 0"
                                                    align="center"><?= Yii::t('notifications', 'Hi, {username}', ['username' => $name], $user->site_language) ?>
                                                    ,
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:20px 5% 0"
                                                    align="center"><?= Yii::t('notifications', "You have just become part of the largest, most affordable and easiest to use marketplace for services and products!", [], $user->site_language) ?></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 30px" width="100%" valign="top" height="1"
                                                    bgcolor="#ffffff" align="center">
                                                    <table style="border-collapse:collapse" width="30%" cellspacing="0"
                                                           cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:21px;font-weight:bold;padding:0 5%"
                                                    align="center"><?= Yii::t('notifications', "It is never been so easy to get it done", [], $user->site_language) ?></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 30px" width="100%" valign="top" height="1"
                                                    bgcolor="#ffffff" align="center">
                                                    <table style="border-collapse:collapse" width="30%" cellspacing="0"
                                                           cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:32px;padding:0 5%"
                                                    valign="top" bgcolor="#ffffff" align="left">
                                                    <?= Html::img(Url::to('/images/new/email/money.jpg', true), [
                                                        'style' => "color:#666666;margin-right:15px;",
                                                        'valign' => 'bottom',
                                                        'border' => 0,
                                                    ]) ?>
                                                    <?= Yii::t('notifications', 'Additional income', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:0 5% 20px"
                                                    bgcolor="#ffffff" align="left">
                                                    <?= Yii::t('notifications', 'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 30px" width="580" valign="top" height="1" bgcolor="#ffffff"
                                        align="center">
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                <td>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:32px;padding:20px 5% 0"
                                                    valign="top" bgcolor="#ffffff" align="left">
                                                    <?= Html::img(Url::to('/images/new/email/find.jpg', true), [
                                                        'style' => "color:#666666;margin-right:15px;",
                                                        'valign' => 'bottom',
                                                        'border' => 0,
                                                    ]) ?>
                                                    <?= Yii::t('notifications', 'Looking for service or order', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:0 5% 20px"
                                                    bgcolor="#ffffff" align="left">
                                                    <?= Yii::t('notifications', 'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 30px" width="580" valign="top" height="1" bgcolor="#ffffff"
                                        align="center">
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td style="border-top-color:#eeeeee;border-top-style:solid;border-top-width:1px;padding:0 30px"></td>
                                                <td>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:18px;font-weight:bold;line-height:32px;padding:20px 5% 0"
                                                    valign="top" bgcolor="#ffffff" align="left">
                                                    <?= Html::img(Url::to('/images/new/email/lock.jpg', true), [
                                                        'style' => "color:#666666;margin-right:15px;",
                                                        'valign' => 'bottom',
                                                        'border' => 0,
                                                    ]) ?>
                                                    <?= Yii::t('notifications', 'Deal safety!', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="15" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:15px;font-weight:normal;line-height:22px;padding:0 5%"
                                                    bgcolor="#ffffff" align="left">
                                                    <?= Yii::t('notifications', 'It is really that simple.', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="top" height="15" bgcolor="#eeeeee" align="center"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="border-collapse:collapse" width="100%" cellspacing="0"
                                               cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="color:#000000;font-family:arial,sans-serif;font-size:28px;font-weight:bold;line-height:32px;padding:30px 30px 0"
                                                    valign="top" bgcolor="#ffffff" align="center">
                                                    <?= Yii::t('notifications', 'Ready to get started?', [], $user->site_language) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 30px" width="100%" valign="top" height="1"
                                                    bgcolor="#ffffff" align="center">
                                                    <table style="border-collapse:collapse" width="250" height="46"
                                                           cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-radius:2px" height="46" bgcolor="#00b422"
                                                                align="center">
                                                                <?= Html::a(Yii::t('notifications', 'Start Earning', [], $user->site_language), Url::to(['/account/profile/start-selling', 'email-action' => 1], true), [
                                                                    'style' => "color:#ffffff;display:inline-block;font-family:'Helvetica Neue',arial;font-size:17px;font-weight:bold;line-height:46px;max-width:280px;text-align:center;text-decoration:none",
                                                                    'target' => '_blank'
                                                                ]); ?>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            <tr>
                                                <td style="color:#35343a;font-family:Arial,sans-serif;font-size:15px;font-weight:normal;line-height:25px;padding:0 30px"
                                                    bgcolor="#ffffff" align="center">
                                                    <?= Yii::t('notifications', 'Do you need to get a job done?', [], $user->site_language) ?><br>
                                                    <?= Html::a(Yii::t('notifications', 'Find service that you need!', [], $user->site_language), Url::to(['/site/index', 'email-action' => 1], true), [
                                                        'style' => "color:#00698c;font-weight:bold;text-decoration:none",
                                                        'target' => '_blank'
                                                    ]); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" valign="top" height="20" bgcolor="#ffffff"
                                                    align="center"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="top" height="15" bgcolor="#eeeeee" align="center"></td>
                                </tr>
                                <tr>
                                    <td style="font-size:0" class="m_1050845079695132951padding" valign="top"
                                        align="center">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:20px 0" bgcolor="#eeeeee" align="center">
                <table style="border-collapse:collapse;max-width:600px" class="m_1050845079695132951responsive-table"
                       width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td style="color:#818181;font-family:'Helvetica Light','Helvetica',Arial,sans-serif;font-size:12px;line-height:1.5;padding-top:5px"
                            align="center">
                            <table style="border-collapse:collapse;text-align:center;width:100%">
                                <tbody>
                                <tr>
                                    <td style="color:#535353;font-size:10px;line-height:16px;padding-bottom:20px"
                                        align="center">
                                <span style="font-size:12px">
                                    <span style="font-family:arial,helvetica neue,helvetica,sans-serif">
                                        <!--<a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Privacy Policy</a> | <a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Support</a>&nbsp;| <a href="" style="color:#00698c;text-decoration:none" target="_blank" data-saferedirecturl="">Invite a Friend</a><br>-->
                                        <?= Yii::t('notifications', 'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}', ['site' => Yii::$app->name], $user->site_language) ?>
                                        <br>
                                        <br><br>© <?= Yii::$app->name ?>
                                    </span>
                                </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>