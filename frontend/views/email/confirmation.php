<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.12.2016
 * Time: 11:45
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var common\models\user\User $user
 * @var common\models\user\Token $token
 */

$name = $user->getSellerName();

?>
<tr>
    <td>
        <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
            <tbody>
            <tr>
                <td style="text-align:left;font-family:arial;padding-bottom:10px;font-size: 36px;color: #666;font-weight: bold;">
                    <a href="https://ujobs.me" target="_blank" data-saferedirecturl="" style="text-decoration: none;display: block;">
	                    <?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                    <table align="center">
                        <tbody>
                        <tr>
                            <td style="border-bottom:1px solid #dfdfd0;color:#666;text-align:center;padding-bottom:30px">
                                <table style="margin:auto" align="center">
                                    <tbody>
                                    <tr>
                                        <td style="color:#666;font-size:20px;font-weight:bold;text-align:center;font-family:arial">
                                            <?= Yii::t('notifications', 'Hi, {username}, Welcome to {site}!', ['username' => $name, 'site' => Yii::$app->name], $user->site_language) ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table style="margin:auto" align="center">
                                    <tbody>
                                    <tr>
                                        <td style="color:#666;font-size:16px;padding-bottom:30px;text-align:center;font-family:arial">
                                            <?= Yii::t('notifications', 'Click on the link below to confirm your email and get started.', [], $user->site_language) ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table style="margin:auto" align="center">
                                    <tbody>
                                    <tr>
                                        <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                            <?= Html::a(Yii::t('notifications', 'Activate Your Account', [], $user->site_language), $token->url, [
                                                'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                                                'target' => '_blank'
                                            ]); ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#aaa;padding:15px;font-size:11px;line-height:15px;text-align:left">
                                <div style="color:#aaa;padding-bottom:15px;font-family:arial">
                                    <?= Yii::t('notifications', 'If the button above does not work, copy and paste the following URL into your browser\'s address bar.', [], $user->site_language) ?>
                                    :
                                    <?= Html::a(Yii::t('notifications', 'Activate Your Account', [], $user->site_language), $token->url, [
                                        'style' => "color:#00b22c",
                                        'target' => '_blank'
                                    ]); ?>
                                </div>
                                <div style="color:#aaa;font-family:arial">
                                    <?= Yii::t('notifications', 'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).', ['email' => 'message@ujobs.me'], $user->site_language) ?>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
