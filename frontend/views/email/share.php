<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\User;

/**
 * @var string $sender
 * @var string $senderThumb
 * @var string $name
 * @var string $label
 * @var string $thumb
 * @var string $link
 */
?>

<table style="padding:30px 10px;background:#eee;width:100%;font-family:arial" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
                <tbody>
                <tr>
                    <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                        <a href="https://ujobs.me" target="_blank" data-saferedirecturl="" style="text-decoration: none;display: block;">
                            <?= Html::img(Url::to('/images/new/email/logo.png', true), ['alt' => 'logo']) ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                        <table align="center">
                            <tbody>
                            <tr>
                                <td style="color:#666;text-align:left">
                                    <table style="margin:auto" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="text-align:center;padding-bottom:5px">
                                                <?php if ($senderThumb) {
                                                    echo Html::img($senderThumb, [
                                                        'style' => 'width: 60px; height: 60px; -webkit-border-radius: 30px; -moz-border-radius: 30px;border-radius: 30px;'
                                                    ]);
                                                } else {
                                                    echo Html::img(Url::to('/images/new/email/share.png', true));
                                                }  ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#005f84;font-size:16px;font-weight:bold;text-align:center;font-family:arial">
                                                <p style="margin: 0">
                                                    <?= Yii::t('notifications', 'User {user} shared with you a link on {site} site', ['user' => $sender, 'site' => Yii::$app->name]) ?>
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="width: 100%;margin: 10px 0;" align="center">
                                        <tbody>
                                        <tr>
                                            <td align="left" style="padding: 10px; width: 100px; height: 100px;">
                                                <?= Html::img($thumb, ['style' => 'width: 90px; height: 90px;']); ?>
                                            </td>
                                            <td align="left" style="padding: 5px;font-family: arial; font-size: 18px;">
                                                <p style="line-height:22px;margin:2px 0; font-size: 18px;">
                                                    <?= $label ?>
                                                </p>
                                                <p style="line-height:22px;margin:2px 0; font-size: 18px;">
                                                    <?= Yii::t('app', 'by {who}', ['who' => $name])?>
                                                </p>
                                                <p style="line-height:22px;margin:2px 0; font-size: 18px;">
                                                    <span style="font-weight: 700"><?= Yii::t('app', 'Link') ?>: </span>
                                                    <?= $link ?>
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="margin:auto" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="background-color:#00b22d;border:1px solid #028a25;border-radius:3px;text-align:center">
                                                <?= Html::a(
                                                    Yii::t('notifications', 'Visit Website'),
                                                    $link,
                                                    [
                                                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                                                        'target' => '_blank'
                                                    ]
                                                ) ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="max-width:650px" align="center">
                <tbody>
                <tr>
                    <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px;font-family:arial">
                        © <?= Yii::$app->name ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>