<div class="crop-bg">
    <div class="crop-container"></div>
    <div class="crop-actions">
        <i class="fa fa-times fa-lg crop-close" aria-hidden="true"></i>
        <button class="btn btn-primary crop-rotate-left">
            <i class="fa fa-rotate-left fa-lg" aria-hidden="true"></i>
        </button>
        <button class="btn btn-primary crop-rotate-right">
            <i class="fa fa-rotate-right fa-lg" aria-hidden="true"></i>
        </button>
        <button class="btn btn-primary crop-confirm"><?= Yii::t('labels', 'Save') ?></button>
    </div>
</div>