<?php

use common\models\Page;
use yii\bootstrap\Html;

/**
 * @var $this \yii\web\View
 * @var $page Page
 */

$this->registerCssFile('/css/new/friends.css');
?>

<div class="friends-bg">
    <div class="container-fluid2 clearfix">
        <div class="friend-block text-friend">
            <h1 class="friend-title text-center">
                <?= Yii::t('app', 'Earn a reward for you and your friends')?>
            </h1>
            <div class="to-do-list">
                <div class="to-do-item">
                    <div class="to-do-img">
                        <img src="/images/new/check-box.svg" alt="<?= Yii::t('app', 'Join uJobs today in one click')?>" title="<?= Yii::t('app', 'Join uJobs today in one click')?>">
                    </div>
                    <div class="to-do-text text-left">
                        <?= Yii::t('app', 'Join uJobs today in one click')?>.
                        <?= Yii::t('app', 'And receive 1 Cointy to your account')?>
                    </div>
                </div>
                <div class="to-do-item">
                    <div class="to-do-img">
                        <img src="/images/new/envelope.svg" alt="<?= Yii::t('app', 'Invite your friends to join uJobs')?>" title="<?= Yii::t('app', 'Invite your friends to join uJobs')?>">
                    </div>
                    <div class="to-do-text text-left">
                        <?= Yii::t('app', 'Invite your friends to join uJobs')?>.
                        <?= Yii::t('app', 'And get 1 Cointy for each registered')?>
                    </div>
                </div>
                <div class="to-do-item">
                    <div class="to-do-img">
                        <img src="/images/new/gift.svg" alt="<?= Yii::t('app', 'Earn {0} from every purchase that friend makes on uJobs', ['5%'])?>" title="<?= Yii::t('app', 'Earn {0} from every purchase that friend makes on uJobs', ['5%'])?>">
                    </div>
                    <div class="to-do-text text-left">
                        <?= Yii::t('app', 'Earn {0} from every purchase that friend makes on uJobs', ['5%'])?>
                    </div>
                </div>
            </div>
            <?= Html::a(Yii::t('app', 'Join Now'), null, [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-target' => '#ModalSignup'
            ]) ?>
        </div>
    </div>
</div>