<?php

use common\models\Referral;

/* @var $model Referral*/

?>
<div class="cell-friends"><?= $model->email?></div>
<div class="cell-friends"><?= $model->getStatusLabel()?></div>
<div class="cell-friends"><?= Yii::$app->formatter->asDatetime($model->updated_at, 'dd.MM.y HH:mm')?></div>
<div class="cell-friends"><?= $model->getTotal()?></div>