<?php

use common\components\CurrencyHelper;
use common\models\Page;
use common\models\Referral;
use frontend\components\SocialShareWidget;
use frontend\models\ReferralForm;
use frontend\models\search\ReferralSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $page Page
 * @var $referralForm ReferralForm
 * @var ReferralSearch $searchModel
 */

$this->registerCssFile('/css/new/friends.css');
?>

    <div class="friends-bg">
        <div class="container-fluid2 clearfix">
            <div class="friend-block2 text-center">
                <div class="friend-title text-center">
                    <?= Yii::t('app', 'Invite Friends <br/>& Get up to') . ' ' . CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 30000) ?>
                </div>
                <div class="friend-descr">
                    <?= Yii::t('app', 'Earn {0} from every deal</br> that friend makes on uJobs', ['5%']) . '<br/>' . Yii::t('app', '+ 1 Cointy for each registered.') ?>
                </div>
                <!--                <a class="gmail-btn" href="">-->
                <? //= Yii::t('app', 'Invite Your Gmail Contacts')?><!--</a>-->
                <!--                <div class="friend-or text-center">--><? //= Yii::t('app', 'Or')?><!--</div>-->
                <div class="friend-email row">
                    <?php $form = ActiveForm::begin([
                        'action' => ['/referral/send'],
                        'options' => ['enctype' => 'multipart/form-data'],
                        'id' => 'job-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                    ]); ?>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <?= $form->field($referralForm, 'emails_string', [
                            'template' => "
                                    {input}\n
                                    <div class='email-hint'>" . Yii::t('app', 'Separate multiple emails with commas.') . "</div>
                                    \n{error}
                                "
                        ])->textInput([
                            'placeholder' => Yii::t('app', 'Add Email')
                        ])->label(false) ?>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 but-email-friend">
                        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn-middle']) ?>
                        <div class="email-hint">
                            <?= Html::a(Yii::t('app', 'Preview Email'), null, [
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalPreview'
                            ]) ?>
                            <i class="fa fa-caret-right"></i>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="friend-social">
                    <div class="col-md-12"><?= Yii::t('app', 'Share with more friends and earn more money!') ?></div>
                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::to(['/site/index', 'invited_by' => Yii::$app->user->identity->getCurrentId()], true),
                            'twitterMessage' => Yii::t('app', 'I\'m tired of keeping uJobs a secret. Thousands of services at an unbeatable value! Discover uJobs:'),
                            'showLink' => true
                        ]) ?>
                    </div>
                </div>
                <a class="ref-alias text-center" href=""><?= Yii::t('app', 'Referral Program Terms and Conditions.') ?></a>
            </div>
        </div>
    </div>
    <div class="referral-program">
        <div class="container-fluid2">
            <h2 class="text-center"><?= $page->getLabel() ?></h2>
            <?= $page->getContent() ?>
            <a class="grey-but text-center grey-small close-ref"><?= Yii::t('app', 'CLOSE') ?></a>
        </div>
    </div>

    <div class="referral-list">
        <div class="container-fluid2">
            <h1 class="ref-h1 text-center">
                <?= Yii::t('app', 'Track Your Referrals') ?>
            </h1>
            <p class="text-center">
                <?= Yii::t('app', 'Earn up to {0} in shopping credit.', [CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], 30000)]) ?>
                <br/>
                <?= Yii::t('app', 'Earn {0} from every deal that friend makes on uJobs', ['5%']) . ' ' . Yii::t('app', '+ 1 Cointy for each registered.') ?>
            </p>
            <?php Pjax::begin(); ?>
            <?php $form = ActiveForm::begin([
                'id' => 'referral-filter-form',
                'action' => ['/referral/index'],
                'method' => 'get',
                'options' => [
                    'data-pjax' => true
                ]
            ]); ?>
            <div class="table-friends">
                <div class='row-friends'>
                    <div class='cell-friends'><?= Yii::t('app', 'Email') ?></div>
                    <div class='cell-friends'><?= Yii::t('app', 'Status') ?></div>
                    <div class='cell-friends'><?= Yii::t('app', 'Updated') ?></div>
                    <div class='cell-friends'><?= Yii::t('model', 'Total bonus') ?></div>
                </div>
                <div class='row-friends'>
                    <div class='cell-friends'></div>
                    <div class='cell-friends'><?= Html::activeDropDownList($searchModel, 'status', Referral::getStatusLabels(), [
                            'class' => 'form-control',
                            'name' => 'status',
                            'prompt' => '-- ' . Yii::t('app', 'Choose invitation`s status')
                        ]) ?></div>
                    <div class='cell-friends'></div>
                    <div class='cell-friends'></div>
                </div>

                <?php if ($dataProvider->getTotalCount() > 0) { ?>
                    <?php foreach ($dataProvider->models as $model) { ?>
                        <div class="row-friends">
                            <div class="cell-friends"><?= $model->email ?></div>
                            <div class="cell-friends"><?= $model->getStatusLabel() ?></div>
                            <div class="cell-friends"><?= Yii::$app->formatter->asDatetime($model->updated_at, 'dd.MM.y HH:mm') ?></div>
                            <div class="cell-friends"><?= $model->getTotal() ?></div>
                        </div>
                    <?php } ?>
                    <?= LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                    ]) ?>
                <?php } else { ?>
                    <p><?= Yii::t('app', 'No records found') ?></p>
                <?php } ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>

    <div class="modal fade" id="ModalPreview" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header header-col text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <?= Yii::t('app', 'Preview Email') ?>
                </div>
                <div class="modal-body" style="text-align: center;overflow:auto;">
                    <?= $this->render('@frontend/views/email/referral', [
                        'user' => Yii::$app->user->identity->company_user_id ? Yii::$app->user->identity->companyUser : Yii::$app->user->identity,
                        'language' => Yii::$app->user->identity->site_language
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    $(document).on('change', '#referralsearch-status', function() {
        $('#referral-filter-form').submit();
    });
JS;

$this->registerJs($script);