<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 16:11
 */

namespace frontend\controllers;

use common\components\CurrencyHelper;
use common\helpers\SberbankHelper;
use common\models\Payment;
use frontend\modules\account\models\OrderSearch;
use Voronkovich\SberbankAcquiring\OrderStatus;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;

/**
 * Class SberbankPaymentController
 * @package frontend\controllers
 */
class SberbankPaymentController extends Controller
{
    public $client;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        if ($this->client == null) {
            $this->client = new Client([
                'userName' => SberbankHelper::$login,
                'password' => SberbankHelper::$password,
                'language' => 'ru',
                'currency' => Currency::RUB,
                'apiUri' => SberbankHelper::$url,
                'httpMethod' => 'POST',
            ]);
        }

        return parent::beforeAction($action);
    }

    /**
     *
     */
    public function actionSberbank()
    {
        /** @var Client $client */
        $client = $this->client;

        $input = Yii::$app->request->post();
        $orderId = $input['order_id'];
        $orderAmount = $input['total'] * 100;
        $returnUrl = Yii::$app->urlManager->createAbsoluteUrl(['/sberbank-payment/sberbank-callback', 'payment_id' => $orderId]);

        $params['currency'] = Currency::RUB;
        $params['failUrl'] = Yii::$app->urlManager->createAbsoluteUrl(['/sberbank-payment/sberbank-callback', 'payment_id' => $orderId]);
        $params['description'] = $input['description'];

        $result = $client->registerOrder($orderId, $orderAmount, $returnUrl, $params);

        $paymentFormUrl = $result['formUrl'];

        header('Location: ' . $paymentFormUrl);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSberbankCallback()
    {
        $input = Yii::$app->request->get();
        $payment_id = $input['payment_id'];

        $returnUrl = Url::to(['/account/order/list', 'status' => OrderSearch::STATUS_MISSING_DETAILS]);

        /** @var $payment Payment */
        $payment = Payment::findOne((int)$payment_id);
        if ($payment === null || $payment->status !== Payment::STATUS_PENDING) {
            throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
        }

        /** @var Client $client */
        $client = $this->client;
        $status = $client->getOrderStatusExtended($input['orderId']);

        if ($status['amount'] / 100 != CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total)) {
            $payment->markRefused();
            Yii::$app->session->setFlash('error', 'Ты че сука падла ты че');
            return $this->redirect($returnUrl);
        }

        switch ($status['orderStatus']) {
            case OrderStatus::DEPOSITED:
                Yii::$app->session->set('paymentId', $payment->id);
                Yii::$app->session->set('livePaymentId', $payment->id);

                $payment->markComplete();
                $returnUrl = $payment->getReturnUrl();

                Yii::$app->cart->clear();
                $message = Yii::t('app', 'Order is successfully paid');

                break;
            case OrderStatus::DECLINED:
                $payment->markRefused();
                $message = Yii::t('app', 'Order payment is failed');
                break;
            default:
                $message = Yii::t('app', 'Payment is in processing');
                break;
        }

        $payment->updateAttributes(['code' => $input['orderId']]);
        Yii::$app->session->setFlash('success', $message);
        return $this->redirect($returnUrl);
    }
}