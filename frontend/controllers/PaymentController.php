<?php

namespace frontend\controllers;

use common\components\CurrencyHelper;
use common\helpers\CauriHelper;
use common\helpers\PayAnyWayHelper;
use common\helpers\PaypalHelper;
use common\helpers\RobokassaHelper;
use common\models\Payment;
use common\models\UserWallet;
use common\modules\store\components\Cart;
use common\services\CauriPaymentService;
use frontend\models\AuthorizeForm;
use frontend\models\CauriForm;
use frontend\modules\account\models\OrderSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PaymentController.
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @param null $payment_id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionReturn($payment_id = null)
    {
        /* @var $cart Cart */
        $cart = Yii::$app->cart;

        $returnUrl = Url::to(['/account/order/list', 'status' => OrderSearch::STATUS_MISSING_DETAILS]);

        if ($payment_id === null) {
            $payment_id = Yii::$app->request->post('InvId');
            if ($payment_id === null) {
                throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
            }
        }

        /** @var $payment Payment */
        $payment = Payment::findOne((int)$payment_id);
        if ($payment === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
        }
        if ($payment->status == Payment::STATUS_PAID) {
            Yii::$app->session->set('paymentId', (int)$payment_id);
            Yii::$app->session->set('livePaymentId', (int)$payment_id);
            $message = 'Услуга успешно оплачена.';
            $cart->clear();

            $returnUrl = $payment->getReturnUrl();

        } elseif ($payment->status == Payment::STATUS_REFUSED) {
            $message = Yii::t('app', 'Payment cancelled');
        } else {
            $message = Yii::t('app', 'Payment is in processing');
        }
        Yii::$app->session->setFlash('success', $message);

        return $this->redirect($returnUrl);
    }

    /**
     *
     */
    public function actionRobokassaCallback()
    {
        $post = Yii::$app->request->post();
        if (!isset($post['InvId'])) {
            Yii::error('Не указан номер платежа');
        } else {
            /** @var $payment Payment */
            $payment = Payment::findOne((int)$post['InvId']);
            if ($payment === null) {
                Yii::error('Платёж с данным номером не найден');
            } else {
                $md5 = strtolower(md5($post['OutSum'] . ':' . $post['InvId'] . ':' . RobokassaHelper::$password2));
                if ($md5 == strtolower($post['SignatureValue'])) {
                    if ($post['OutSum'] == CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total)) {
                        echo 'OK' . $post['InvId'];
                        Yii::$app->session->set('paymentId', $payment->id);
                        Yii::$app->session->set('livePaymentId', $payment->id);

                        $payment->markComplete();

                        Yii::$app->cart->clear();
                    } else {
                        $payment->markRefused();
                        Yii::error('Неверные параметры платежа');
                    }
                } else {
                    $payment->markRefused();
                    Yii::error('Хеш не совпадает');
                }
            }
        }
    }

    /**
     * @return string
     */
    public function actionPayanywayCallback()
    {
        $post = Yii::$app->request->post();
        if (!isset($post['MNT_TRANSACTION_ID'])) {
            Yii::error('Не указан номер платежа');
        } else {
            /** @var $payment Payment */
            $payment = Payment::findOne((int)$post['MNT_TRANSACTION_ID']);
            if ($payment === null) {
                Yii::error('Платёж с данным номером не найден');
            } else {
                if ($payment->status == Payment::STATUS_PAID) {
                    return 'SUCCESS';
                }
                $md5 = md5(PayAnyWayHelper::$shopId . $post['MNT_TRANSACTION_ID'] . $post['MNT_OPERATION_ID'] .
                    $post['MNT_AMOUNT'] . $post['MNT_CURRENCY_CODE'] . $post['MNT_SUBSCRIBER_ID'] . $post['MNT_TEST_MODE']
                    . PayAnyWayHelper::$integrityCode);
                if ($md5 == strtolower($post['MNT_SIGNATURE'])) {
                    if ($post['MNT_AMOUNT'] == number_format(CurrencyHelper::convert($payment->currency_code, 'RUB', $payment->total), 2, '.', '')) {
                        $payment->markComplete();

                        Yii::$app->cart->clear();
                        $payment->save();
                        return 'SUCCESS';
                    }

                    $payment->markRefused();
                    Yii::error('Неверные параметры платежа');
                } else {
                    $payment->markRefused();
                    Yii::error('Хеш не совпадает');
                }
            }
        }

        return 'FAIL';
    }

    /**
     * @return string|Response
     */
    public function actionBalance()
    {
        $post = Yii::$app->request->post();
        if (!isset($post['payment_id'])) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Payment id is not set'));
        } else {
            /** @var $payment Payment */
            $payment = Payment::findOne((int)$post['payment_id']);
            if ($payment === null) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Payment with such id is not found'));
            } else {
                $wallet = UserWallet::findOrCreate(['user_id' => Yii::$app->user->identity->getCurrentId(), 'currency_code' => $payment->currency_code]);
                if ($wallet->balance >= $payment->total) {
                    Yii::$app->session->set('paymentId', $payment->id);
                    Yii::$app->session->set('livePaymentId', $payment->id);

                    $payment->markComplete();
                    $returnUrl = $payment->getReturnUrl();

                    Yii::$app->cart->clear();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Order is successfully paid'));

                    return $this->redirect($returnUrl);
                }

                Yii::$app->session->setFlash('error', Yii::t('app', 'There is not enough money on your balance. Please replenish your balance, or transfer money from other your wallet'));

                return $this->redirect(['/account/payment/index']);
            }
        }

        return 'FAIL';
    }

    /**
     *
     */
    public function actionPaypalCallback()
    {
        $raw_post_data = file_get_contents('php://input');
//    $raw_post_data = 'mc_gross=19.95&protection_eligibility=Eligible&address_status=confirmed&payer_id=LPLWNMTBWMFAY&tax=0.00&address_street=1+Main+St&payment_date=20%3A12%3A59+Jan+13%2C+2009+PST&payment_status=Completed&charset=windows-1252&address_zip=95131&first_name=Test&mc_fee=0.88&address_country_code=US&address_name=Test+User&notify_version=2.6&custom=&payer_status=verified&address_country=United+States&address_city=San+Jose&quantity=1&verify_sign=AtkOfCXbDm2hu0ZELryHFjY-Vb7PAUvS6nMXgysbElEn9v-1XcmSoGtf&payer_email=gpmac_1231902590_per%40paypal.com&txn_id=61E67681CH3238416&payment_type=instant&last_name=User&address_state=CA&receiver_email=gpmac_1231902686_biz%40paypal.com&payment_fee=0.88&receiver_id=S8XGHLYDW9T3S&txn_type=express_checkout&item_name=&mc_currency=USD&item_number=&residence_country=US&test_ipn=1&handling_amount=0.00&transaction_subject=&payment_gross=19.95&shipping=0.00';
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = [];
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }

        if (!isset($myPost['item_number'])) {
            Yii::error('Не указан номер платежа');
//            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
        /** @var $payment Payment */
        $payment = Payment::findOne((int)$myPost['item_number']);
        if ($payment === null) {
            Yii::error('Платёж с данным номером не найден');
//            throw new BadRequestHttpException('Что-то пошло не так. Сообщите нам. Спасибо за сотрудничество.');
        }

        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        foreach ($myPost as $key => $value) {
            $value = urlencode($value);
            $req .= "&$key=$value";
        }

//    $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        $paypal_url = 'https://www.paypal.com/cgi-bin/webscr';

        $client = new Client();
        $response = $client->post($paypal_url, [
            'query' => $req
        ])->send();

        if ($response->getStatusCode() == 200) {
            $res = $response->getData();

            if (strcmp($res, 'VERIFIED') == 0) {
                // check whether the payment_status is Completed
                if ($myPost['payment_status'] === 'Completed'
                    && $myPost['receiver_email'] === PaypalHelper::$login
                    && $myPost['mc_gross'] == $payment->total
                    && $myPost['mc_currency'] == $payment->currency_code
                ) {
                    $payment->markComplete();

                    Yii::$app->cart->clear();
                } else {
                    $payment->markRefused();
                    Yii::error('Неверные параметры платежа');
                }

                // assign posted variables to local variables
                //$item_name = $_POST['item_name'];
                //$item_number = $_POST['item_number'];
                //$payment_status = $_POST['payment_status'];
                //$payment_amount = $_POST['mc_gross'];
                //$payment_currency = $_POST['mc_currency'];
                //$txn_id = $_POST['txn_id'];
                //$receiver_email = $_POST['receiver_email'];
                //$payer_email = $_POST['payer_email'];

            } else if (strcmp($res, 'INVALID') == 0) {
                // Business logic here which deals with invalid IPN messages
                Yii::error('Верификация IPN не пройдена');
                $payment->markRefused();
            }
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAuthorizeForm()
    {
        $paymentId = Yii::$app->request->post('paymentId');
        if ($paymentId != null) {
            $payment = Payment::findOne((int)$paymentId);
            if ($payment !== null) {
                $form = new AuthorizeForm(['paymentId' => $payment->id]);
                $total = CurrencyHelper::convert($payment->currency_code, 'USD', $payment->total);
                $form->scenario = $total <= 20 ? AuthorizeForm::SCENARIO_BASIC : AuthorizeForm::SCENARIO_ADVANCED;

                return $this->render('@frontend/views/authorize/index', ['model' => $form, 'payment' => $payment, 'formTemplate' => $form->getTemplate()]);
            }

            Yii::$app->session->setFlash('error', Yii::t('app', 'Payment with such id is not found'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Payment id is not set'));
        }

        return $this->redirect(['/store/cart/index']);
    }

    /**
     * @return string|Response
     */
    public function actionAuthorizeProcess()
    {
        $form = new AuthorizeForm();
        $form->scenario = Yii::$app->request->post('scenario', AuthorizeForm::SCENARIO_BASIC);
        $returnUrl = Url::to(['/site/index']);
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            $client = new Client([
                'responseConfig' => ['format' => Client::FORMAT_JSON],
                'requestConfig' => ['format' => Client::FORMAT_JSON]
            ]);
            $url = 'https://apitest.authorize.net/xml/v1/request.api';
//                $url = 'https://api.authorize.net/xml/v1/request.api';
            try {
                $response = $client->post($url, $form->getRequest())->send();
                $isOk = $response->isOk;
                $responseData = Json::decode(str_replace("\xEF\xBB\xBF", '', $response->content));
            } catch (\Exception $e) {
                $isOk = false;
                Yii::warning($e->getMessage());
            }
            $success = false;
            $payment = $form->payment;
            if ($isOk) {
                if (isset($responseData['transactionResponse']['transId'])) {
                    $payment->updateAttributes(['code' => $responseData['transactionResponse']['transId']]);
                }
                if (isset($responseData['messages']['resultCode']) && $responseData['messages']['resultCode'] === 'Ok') {
                    if (
                        isset($responseData['transactionResponse']['responseCode']) && $responseData['transactionResponse']['responseCode'] === '1' &&
                        !empty($responseData['transactionResponse']['messages']) && $responseData['transactionResponse']['messages'][0]['code'] === '1'
                    ) {
                        if (empty($responseData['transactionResponse']['errors'])) {
                            if (
                                $responseData['transactionResponse']['avsResultCode'] === 'Y' ||
                                $responseData['transactionResponse']['cvvResultCode'] === 'M' ||
                                $responseData['transactionResponse']['cavvResultCode'] === '2'
                            ) {
                                if ((int)$responseData['refId'] === $payment->id) {
                                    $success = true;
                                    Yii::$app->session->set('paymentId', $payment->id);
                                    Yii::$app->session->set('livePaymentId', $payment->id);

                                    $payment->markComplete();
                                    $returnUrl = $payment->getReturnUrl();

                                    Yii::$app->cart->clear();
                                    Yii::$app->session->setFlash('success', Yii::t('app', 'Order is successfully paid'));
                                }
                            }
                        }
                    }
                }
                if ($success === false) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'The payment was rejected by the payment system'));
                    Yii::error('Неверные параметры платежа');
                    Yii::error(VarDumper::export($responseData));
//                    d($responseData);
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error occurred during payment process'));
                Yii::error('Ошибка при отправке запроса');
//                d($responseData);
            }
            if ($success === false) {
                $payment->markRefused();
            }
        }

        return $this->redirect($returnUrl);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCauriForm()
    {
        $paymentId = Yii::$app->request->post('paymentId');
        if ($paymentId != null) {
            $payment = Payment::findOne((int)$paymentId);
            if ($payment !== null) {
                $cauriPaymentService = new CauriPaymentService();
                $cauriUserId = $cauriPaymentService->resolve(Yii::$app->user->identity);
                if ($cauriUserId !== false) {
                    $form = new CauriForm(['paymentId' => $payment->id, 'userId' => $cauriUserId]);
                    return $this->render('@frontend/views/cauri/index', ['model' => $form, 'payment' => $payment, 'formTemplate' => '_form_basic']);
                }

                Yii::$app->session->setFlash('error', Yii::t('app', 'Cauri cannot resolve user'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Payment with such id is not found'));
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Payment id is not set'));
        }

        return $this->redirect(['/store/cart/index']);
    }

    /**
     * @return string|Response|array
     */
    public function actionCauriProcess()
    {
        $form = new CauriForm();

        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            $cauriPaymentService = new CauriPaymentService();
            $responseData = $cauriPaymentService->process($form);
            ////////////////////////////////////////
            $payment = $form->payment;
            if ($responseData) {
                if (isset($responseData['id'])) {
                    $payment->updateAttributes(['code' => $responseData['id']]);
                }
                if (isset($responseData['success']) && $responseData['success'] === true) {
                    if (!empty($responseData['acs'])) {
                        if (!empty($responseData['acs']['url']) && !empty($responseData['acs']['parameters'])) {
                            $params = $responseData['acs']['parameters'];
//                            $params['TermUrl'] = Url::to(['/payment/cauri-authenticate']);
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'success' => true,
                                'acs' => true,
                                'acs_form' => $this->renderPartial('@frontend/views/cauri/_acs_form', [
                                    'url' => $responseData['acs']['url'],
                                    'params' => $params
                                ])
                            ];
                        }
                    } else {
                        Yii::$app->session->set('paymentId', $payment->id);
                        Yii::$app->session->set('livePaymentId', $payment->id);

                        $payment->markComplete();
                        Yii::$app->cart->clear();

                        Yii::$app->session->setFlash('success', Yii::t('app', 'Order is successfully paid'));

                        return $this->redirect($payment->getReturnUrl());
                    }
                }
                Yii::$app->session->setFlash('error', Yii::t('app', 'The payment was rejected by the payment system'));
                Yii::error('Платёж отклонён платёжной системой');
                Yii::error(VarDumper::export($responseData));
//                    d($responseData);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error occurred during payment process'));
                Yii::error('Ошибка при отправке запроса');
//                d($responseData);
            }
            $payment->markRefused();
        }

        return $this->redirect(['/site/index']);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCauriAuthenticate()
    {
        $rares = Yii::$app->request->post('PaRes');
        $md = Yii::$app->request->post('MD');
        if ($rares && $md) {
            $cauriPaymentService = new CauriPaymentService();
            $responseData = $cauriPaymentService->authenticate($rares, $md);
            if (!empty($responseData['id'])) {
                /* @var Payment $payment */
                $payment = Payment::find()->joinWith(['paymentMethod'])->where(['payment_method.code' => 'cauri', 'payment.code' => $responseData['id']])->one();
                if ($payment !== null) {
                    if ($payment->status === Payment::STATUS_PENDING) {
                        if (isset($responseData['success']) && $responseData['success'] === true) {
                            Yii::$app->session->set('paymentId', $payment->id);
                            Yii::$app->session->set('livePaymentId', $payment->id);

                            $payment->markComplete();
                            Yii::$app->cart->clear();

                            Yii::$app->session->setFlash('success', Yii::t('app', 'Order is successfully paid'));
                            return $this->redirect($payment->getReturnUrl());
                        }

                        $payment->markRefused();
                        Yii::$app->session->setFlash('error', Yii::t('app', 'The payment was rejected by the payment system'));
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'The payment has already been processed'));
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Payment with such id is not found'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Payment id is not set'));
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Payment system did not return required data'));
        }

        return $this->redirect(['/site/index']);
    }

    /**
     * @throws BadRequestHttpException|Response
     */
    public function actionCauriCallback()
    {
        $post = Yii::$app->request->post();
        Yii::error($post);
        if (isset($post['id'], $post['user'], $post['price'], $post['earned'], $post['currency'], $post['status'], $post['sandbox'], $post['project'], $post['signature'])) {
            if ($post['sandbox'] === '0') {
                $cauriPaymentService = new CauriPaymentService();
                $signature = ArrayHelper::remove($post, 'signature');
                if ($post['project'] === CauriHelper::$publicKey && $cauriPaymentService->generateSignature($post) === $signature) {
                    /* @var Payment $payment */
                    $payment = Payment::find()->joinWith(['paymentMethod'])->where(['payment_method.code' => 'cauri', 'payment.code' => $post['id']])->one();
                    if ($payment !== null && $payment->user_id == $post['user'] && (empty($post['order_id']) || $payment->id == $post['order_id'])) {
                        if ($payment->status === Payment::STATUS_PENDING) {
                            if ($post['status'] === 'completed') {
                                $price = Yii::$app->formatter->asDecimal(CurrencyHelper::convert($payment->currency_code, $post['currency'], $payment->total), 2);
                                if ($price == $post['price']) {
                                    $payment->markComplete();
                                } else {
                                    Yii::error(Yii::t('app', 'Invalid payment parameters'));
                                    $payment->markRefused();
                                }
                            } else {
                                Yii::error(Yii::t('app', 'The payment was rejected by the payment system'));
                                $payment->markRefused();
                            }
                        } else {
                            Yii::error(Yii::t('app', 'The payment has already been processed'));
                        }
                    } else {
                        Yii::error(Yii::t('app', 'Payment with such id is not found'));
                    }
                } else {
                    Yii::error(Yii::t('app', 'Invalid payment signature'));
                }
            } else {
                Yii::error(Yii::t('app', 'Payment system is in test mode. Service is not provided'));
            }

            return true;
        }

        Yii::error(Yii::t('app', 'Payment system did not return required data'));
        throw new BadRequestHttpException('');
    }
}