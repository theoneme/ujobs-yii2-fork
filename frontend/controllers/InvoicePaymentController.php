<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.03.2017
 * Time: 16:38
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Invoice;
use frontend\models\EmailInvoiceForm;
use frontend\models\InvoiceForm;
use common\models\Payment;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class InvoicePaymentController
 * @package frontend\controllers
 */
class InvoicePaymentController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true, 'actions' => [
                            'invoice',
                            'process',
                            'success',
                            'send-invoice'
                        ], 'roles' => ['@']
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['send-invoice'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionInvoice()
    {
        $paymentId = Yii::$app->request->post('order_id');
        if ($paymentId !== null) {
            $payment = Payment::findOne((int)$paymentId);
            if($payment !== null) {
                $individualInvoiceForm = new InvoiceForm(['orderId' => $payment->id]);
                $individualInvoiceForm->scenario = InvoiceForm::SCENARIO_INDIVIDUAL;

                $legalEntityInvoiceForm = new InvoiceForm(['orderId' => $payment->id]);
                $legalEntityInvoiceForm->scenario = InvoiceForm::SCENARIO_LEGAL_ENTITY;

                return $this->render('invoice', [
                    'individualInvoiceForm' => $individualInvoiceForm,
                    'legalEntityInvoiceForm' => $legalEntityInvoiceForm,
                    'payment' => $payment
                ]);
            }
        }

        return $this->redirect(['/store/cart/index']);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionProcess()
    {
        $model = new InvoiceForm();

        if ($input = Yii::$app->request->post()) {
            $model->scenario = $input['InvoiceForm']['decision'];
            $model->load($input);

            if ($model->validate()) {
                $payment = Payment::findOne($model->orderId);

                $date = Yii::$app->formatter->asDate(time(), 'd MMMM yyyy');
                $dateParts = explode(' ', $date);

                $htmlInvoice = $this->renderPartial("partial/{$model->getTemplate()}", [
                    'model' => $model,
                    'payment' => $payment,
                    'dateParts' => $dateParts
                ]);

                $model->htmlInvoice = $htmlInvoice;
                if ($model->save()) {
                    return $this->redirect(['/invoice-payment/success', 'id' => $model->invoice->id]);
                }
            }
        }
        return $this->redirect(['/store/cart/index']);
    }

    /**
     * @param integer $id
     * @return string|ForbiddenHttpException
     */
    public function actionSuccess($id = null)
    {
        $invoice = Invoice::findOne(['id' => $id, 'user_id' => userId()]);
        $emailInvoiceForm = new EmailInvoiceForm();

        if ($invoice != null) {
            return $this->render('success', [
                'invoice' => $invoice,
                'emailInvoiceForm' => $emailInvoiceForm
            ]);
        }

        return new ForbiddenHttpException(Yii::t('app', 'Invoice not found'));
    }

    /**
     * @return array
     */
    public function actionSendInvoice()
    {
        $input = Yii::$app->request->post();
        $output = [
            'success' => false,
            'message' => Yii::t('app', 'An error is encountered')
        ];

        $emailInvoiceForm = new EmailInvoiceForm();

        if($input) {
            $emailInvoiceForm->load($input);

            $result = $emailInvoiceForm->sendMail();
            $output['success'] = $result;
            $output['message'] = $result ? Yii::t('app', 'Invoice sent on Email') : $output['message'];
        }

        return $output;
    }
}