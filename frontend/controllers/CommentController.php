<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 12:41
 */

namespace frontend\controllers;

use frontend\models\PostCommentForm;
use Yii;
use common\controllers\FrontEndController;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class CommentController
 * @package frontend\controllers
 */
class CommentController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['create', 'create-ajax'], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['create-ajax'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     */
    public function actionCreate()
    {
        $postCommentForm = new PostCommentForm();
        $input = Yii::$app->request->post();

        $postCommentForm->load($input);
        $postCommentForm->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     *
     */
    public function actionCreateAjax()
    {
        $postCommentForm = new PostCommentForm();
        $input = Yii::$app->request->post();

        $postCommentForm->load($input);
        if($postCommentForm->save()) {
            return ['success' => true];
        }

        return ['success' => false];
    }
}