<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:23
 */

namespace frontend\controllers;

use common\models\Category;
use common\models\elastic\JobElastic;
use common\models\elastic\PageElastic;
use common\models\elastic\ProfileElastic;
use common\models\elastic\UserPortfolioElastic;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Page;
use common\models\SeoAdvanced;
use common\models\Subscription;
use common\models\user\Profile;
use common\models\UserAttribute;
use common\models\UserPortfolio;
use common\models\UserPortfolioAttribute;
use common\models\Video;
use frontend\modules\elasticfilter\components\FilterJob;
use frontend\modules\elasticfilter\components\FilterPortfolio;
use frontend\modules\elasticfilter\components\FilterPro;
use frontend\modules\elasticfilter\models\BaseFilter;
use frontend\modules\elasticfilter\models\FilterJobSpecialty;
use frontend\modules\elasticfilter\models\FilterJobTag;
use frontend\modules\elasticfilter\models\FilterPage;
use frontend\modules\elasticfilter\models\FilterPortfolioTag;
use frontend\modules\elasticfilter\models\FilterProSkill;
use frontend\modules\elasticfilter\models\FilterProSpecialty;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class CategoryController extends BaseCategoryController
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, [
                'jobs', 'job-catalog', 'pros', 'pro-catalog', 'skill', 'specialty-pro', 'tag'
            ], true) && preg_match('/^https:\/\/ujobs\.ru.*$/u', Yii::$app->request->referrer)
        ) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @param null $category_1
     * @return mixed
     */
    public function actionJobs($category_1 = null)
    {
        return $this->prepareCatalogPage($category_1, self::ENTITY_JOB);
    }

    /**
     * @param null $category_1
     * @return mixed
     */
    public function actionTenders($category_1 = null)
    {
        return $this->prepareCatalogPage($category_1, self::ENTITY_TENDER);
    }

    /**
     * @param null $category_1
     * @return mixed
     */
    public function actionPros($category_1 = null)
    {
        return $this->prepareUserCatalogPage($category_1, self::ENTITY_PRO);
    }

    /**
     * @param null $category_1
     * @return string|\yii\web\Response
     */
    public function actionPortfolios($category_1 = null)
    {
        return $this->prepareCatalogPage($category_1, self::ENTITY_PORTFOLIO);
    }

    /**
     * @param null $category_1
     * @return string|\yii\web\Response
     */
    public function actionArticles($category_1 = null)
    {
        return $this->prepareCatalogPage($category_1, self::ENTITY_ARTICLE);
    }

    /**
     * /catalog-job
     * @return mixed
     */
    public function actionJobCatalog()
    {
        return $this->prepareRootCatalogPage(self::ENTITY_JOB);
    }

    /**
     * /catalog-tender
     * @return string
     */
    public function actionTenderCatalog()
    {
        return $this->prepareRootCatalogPage(self::ENTITY_TENDER);
    }

    /**
     * /catalog-pro
     * @return string
     */
    public function actionProCatalog()
    {
        return $this->prepareRootCatalogPage(self::ENTITY_PRO);
    }

    /**
     * @return mixed
     */
    public function actionPortfolioCatalog()
    {
        return $this->prepareRootCatalogPage(self::ENTITY_PORTFOLIO);
    }

    /**
     * @return mixed
     */
    public function actionArticleCatalog()
    {
        return $this->prepareRootCatalogPage(self::ENTITY_ARTICLE);
    }

    /**
     * @param $category_id
     * @param bool $mobile
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionArticleMore($category_id, $mobile = false)
    {
        $template = $mobile ? 'page-list-mobile' : 'page-list';
        $itemSelector = $mobile ? 'data-scroll-item-mobile' : 'data-scroll-item';
        $container = $mobile ? 'mob-work-item' : 'cat-middle-block';
        $allChildrenCategoriesIds = [];

        $category = Category::findOne($category_id);
        if($category !== null) {
            $allChildrenCategoriesIds = $category->isRoot() ? null : array_merge($category->children()->select('id')->column(), [$category->id]);
        }

        $subscriptions = [];
        if (!isGuest()) {
            $subscriptions = ArrayHelper::getColumn(
                array_filter(Yii::$app->user->identity->subscriptions, function ($var) {
                    return $var->entity === Subscription::ENTITY_PROFILE;
                }),
                'entity_id'
            );
        }
        $items = PageElastic::find()
            ->where(['type' => Page::TYPE_ARTICLE])
            ->andFilterWhere(['category_id' => $allChildrenCategoriesIds])
            ->andWhere(['or',
                ['status' => Page::STATUS_ACTIVE],
                ['and',
                    ['status' => Page::STATUS_ACTIVE_FOR_SUBSCRIBERS],
                    ['user_id' => $subscriptions]
                ]
            ])
            ->limit(Yii::$app->request->post('limit', 5))
            ->offset(Yii::$app->request->post('offset', 0));

        $filter = new FilterPage([
            'query' => $items,
            'lightQuery' => clone $items,
            'request' => ArrayHelper::getValue(Yii::$app->request->queryParams, 'request'),
            'type' => Page::TYPE_ARTICLE,
            'category' => $category,
        ]);

        $filter->applyFiltersToCatalog();
        $articles = $filter->query->all();
        return array_reduce($articles, function ($carry, $var) use ($template, $itemSelector, $container) {
            return $carry . '<div class="' . $container . '" ' . $itemSelector . '>' . $this->renderPartial('@frontend/modules/filter/views/' . $template, ['model' => $var]) . '</div>';
        }, '');
    }

    /**
     * /catalog-video
     * @return string
     */
    public function actionVideoCatalog()
    {
        return $this->actionVideos();
    }

    /**
     * @param null $reserved_job_tag
     * @return string|\yii\web\Response
     */
    public function actionTag($reserved_job_tag = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $category = $cacheLayer->getCategoryFromCache('root');

        /** @var $tagAttribute JobAttribute */
        $tagAttribute = JobAttribute::find()->select('id, job_id, value, value_alias, attribute_id')->where(['entity_alias' => 'tag', 'value_alias' => $reserved_job_tag])->one();
        if ($tagAttribute !== null) {
            $query = JobElastic::find()->where([
                'type' => Job::TYPE_JOB,
                'status' => Job::STATUS_ACTIVE,
            ]);

            $seoAdvanced = $tagAttribute->getMeta( Yii::$app->request->queryParams);
            $this->registerMetaTags($category, $seoAdvanced->templates);
            $seo = $this->buildSeoArray($category, $seoAdvanced->templates);

            $filter = new FilterJob([
                'query' => $query,
                'category' => $category,
                'filterClass' => FilterJobTag::class,
                'request' => $tagAttribute->value_alias,
                'requestType' => BaseFilter::MODE_TYPE_RESERVED
            ]);

            return $this->render('tag', [
                'tagAttribute' => $tagAttribute,
                'category' => $category,
                'seo' => $seo,
                'request' => $tagAttribute->value_alias,
                'filter' => $filter,
                'entity' => self::ENTITY_JOB
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested tag is not found'));
        return $this->redirect(['/']);
    }

    /**
     * @param null $reserved_job_tag
     * @return \yii\web\Response
     */
    public function actionOldTag($reserved_job_tag = null)
    {
        return $this->redirect(['/category/tag', 'reserved_job_tag' => $reserved_job_tag, 'city' => Yii::$app->request->get('city')]);
    }

    /**
     * @param null $reserved_portfolio_tag
     * @return string|\yii\web\Response
     */
    public function actionPortfolioTag($reserved_portfolio_tag = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $category = $cacheLayer->getCategoryFromCache('root');

        /* @var $tagAttribute JobAttribute */
        $tagAttribute = UserPortfolioAttribute::find()->where(['entity_alias' => 'portfolio_tag', 'value_alias' => $reserved_portfolio_tag])->one();
        if ($tagAttribute !== null) {
            $query = UserPortfolioElastic::find()->where([
                'version' => UserPortfolio::VERSION_NEW,
                'status' => UserPortfolio::STATUS_ACTIVE,
            ]);

            $seoAdvanced = $tagAttribute->getMeta( Yii::$app->request->queryParams);
            $this->registerMetaTags($category, $seoAdvanced->templates);
            $seo = $this->buildSeoArray($category, $seoAdvanced->templates);

            $filter = new FilterPortfolio([
                'query' => $query,
                'category' => $category,
                'filterClass' => FilterPortfolioTag::class,
                'request' => $tagAttribute->value_alias,
                'requestType' => BaseFilter::MODE_TYPE_RESERVED
            ]);

            return $this->render('portfolio-tag', [
                'tagAttribute' => $tagAttribute,
                'category' => $category,
                'seo' => $seo,
                'filterView' => 'filter-portfolio',
                'filter' => $filter
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested tag is not found'));
        return $this->redirect(['/']);
    }

    /**
     * @param null $reserved_skill
     * @return string|\yii\web\Response
     */
    public function actionSkill($reserved_skill = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $category = $cacheLayer->getCategoryFromCache('root');
        $seo = [];

        /* @var $userSkill UserAttribute */
        $userSkill = UserAttribute::find()->select('id, user_id, value, value_alias, attribute_id')->where(['entity_alias' => 'skill', 'value_alias' => $reserved_skill])->one();
        if ($userSkill !== null) {
            $query = ProfileElastic::find()
                ->where(['status' => Profile::STATUS_ACTIVE])
                ->andWhere(['not', ['is_bio_empty' => true]])
                ->andWhere(['not', ['gravatar_email' => null]]);

            $seoAdvanced = $userSkill->getMeta(Yii::$app->request->queryParams);
            if($seoAdvanced instanceof SeoAdvanced) {
                $this->registerMetaTags($category, $seoAdvanced->templates);
                $seo = $this->buildSeoArray($category, $seoAdvanced->templates);
            }

            $filter = new FilterPro([
                'query' => $query,
                'category' => $category,
                'filterClass' => FilterProSkill::class,
                'request' => $userSkill->value_alias,
                'requestType' => BaseFilter::MODE_TYPE_RESERVED
            ]);

            return $this->render('skill', [
                'category' => $category,
                'entity' => self::ENTITY_PRO,
                'seo' => $seo,
                'filter' => $filter,
                'request' => $userSkill->value_alias
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested skill is not found'));

        return $this->redirect(['/']);
    }

    /**
     * @param null $reserved_specialty
     * @return string|\yii\web\Response
     */
    public function actionSpecialtyTender($reserved_specialty = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $category = $cacheLayer->getCategoryFromCache('root');

        /* @var $specialtyAttribute JobAttribute */
        $specialtyAttribute = JobAttribute::find()->where(['attribute_id' => 41, 'value_alias' => $reserved_specialty])->one();
        if ($specialtyAttribute !== null) {
            $query = JobElastic::find()
                ->where([
                    'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER],
                    'status' => Job::STATUS_ACTIVE,
                ]);

            $seoAdvanced = $specialtyAttribute->getMeta( Yii::$app->request->queryParams);
            $this->registerMetaTags($category, $seoAdvanced->templates);
            $seo = $this->buildSeoArray($category, $seoAdvanced->templates);

            $filter = new FilterJob([
                'query' => $query,
                'category' => $category,
                'type' => self::ENTITY_TENDER,
                'filterClass' => FilterJobSpecialty::class,
                'request' => $specialtyAttribute->value_alias,
                'requestType' => BaseFilter::MODE_TYPE_RESERVED
            ]);

            return $this->render('specialty-tender', [
                'specialtyAttribute' => $specialtyAttribute,
                'category' => $category,
                'entity' => self::ENTITY_TENDER,
                'seo' => $seo,
                'request' => $specialtyAttribute->value_alias,
                'filter' => $filter
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested tag is not found'));
        return $this->redirect(['/']);
    }

    /**
     * @param null $reserved_specialty
     * @return string|\yii\web\Response
     */
    public function actionSpecialtyPro($reserved_specialty = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $category = $cacheLayer->getCategoryFromCache('root');
        $seo = [];

        /* @var $userSpecialty UserAttribute */
        $userSpecialty = UserAttribute::find()->where(['entity_alias' => 'specialty', 'value_alias' => $reserved_specialty])->one();
        if ($userSpecialty !== null) {
            $query = ProfileElastic::find()
                ->where(['status' => Profile::STATUS_ACTIVE])
                ->andWhere(['not', ['is_bio_empty' => true]])
                ->andWhere(['not', ['gravatar_email' => null]]);

            $seoAdvanced = $userSpecialty->getMeta(Yii::$app->request->queryParams);
            if($seoAdvanced instanceof SeoAdvanced) {
                $this->registerMetaTags($category, $seoAdvanced->templates);
                $seo = $this->buildSeoArray($category, $seoAdvanced->templates);
            }

            $filter = new FilterPro([
                'query' => $query,
                'category' => $category,
                'filterClass' => FilterProSpecialty::class,
                'request' => $userSpecialty->value_alias,
                'requestType' => BaseFilter::MODE_TYPE_RESERVED
            ]);

            return $this->render('specialty-pro', [
                'category' => $category,
                'entity' => self::ENTITY_PRO,
                'seo' => $seo,
                'filter' => $filter,
                'request' => $userSpecialty->value_alias
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested specialty is not found'));
        return $this->redirect(['/']);
    }

    /**
     * @param null|string $alias
     * @return string|\yii\web\Response
     */
    public function actionVideos($alias = 'root')
    {
        $category = $this->defineCategory($alias, 'video');
        if($category !== null) {
//            $seo = $this->buildSeoArray($category, $category->meta->templates);

            $categoryIds = $category->alias === 'root' ? [] : array_merge($category->children()->select('id')->column(), [$category->id]);

            $items = Video::find()
                ->select('video.*')
                ->joinWith(['category', 'user.profile.translation', 'user.company.translations'])
                ->filterWhere(['video.category_id' => $categoryIds])
                ->andWhere(['video.status' => Video::STATUS_ACTIVE])
                ->orderBy([new Expression('FIELD (video.locale, ' . Yii::$app->language . ') DESC')])
                ->groupBy('video.id');
            $sortByParam = Yii::$app->request->get('sort_by', 'created_at;desc');
            if (!in_array($sortByParam, ['price;asc', 'price;desc', 'created_at;desc'])) {
                $sortByParam = 'created_at;desc';
            }
            $sortData = $this->getSortData($sortByParam);
            $sortByParam = explode(';', $sortByParam);
            $items->orderBy($sortByParam[0] . ' ' . $sortByParam[1]);

            $request = Yii::$app->request->get('request');
            if($request) {
                $items->andFilterWhere(['like', 'title', $request]);
            }

            if (($category->rgt - $category->lft) > 1) {
                $children = Category::find()
                    ->select('category.*')
                    ->addSelect(['relatedCount' => Video::find()
                        ->select('count(video.id)')
                        ->where('video.parent_category_id=category.id')
                        ->andWhere(['video.status' => Video::STATUS_ACTIVE])
                    ])
                    ->joinWith(['translations'])
                    ->where(['category.id' => $category->getChildrenIds()])
                    ->orderBy('relatedCount desc')
                    ->all();
            } else {
                /* @var $parent Category */
                $parent = $category->parents(1)->one();
                $children = Category::find()
                    ->select('category.*')
                    ->addSelect(['relatedCount' => Video::find()
                        ->select('count(video.id)')
                        ->where('video.category_id=category.id')
                        ->andWhere(['video.status' => Video::STATUS_ACTIVE])
                    ])
                    ->joinWith(['translation'])
                    ->where(['category.id' => $parent->getChildrenIds()])
                    ->orderBy('relatedCount desc')
                    ->all();
            }
            $dataProvider = new ActiveDataProvider([
                'query' => $items,
                'pagination' => ['pageSize' => 20]
            ]);
            $seo = $this->getCategorySeo($category, 'video', [], $request);

            $result = $this->render('video', [
                'category' => $category,
                'dataProvider' => $dataProvider,
                'children' => $children,
                'sortData' => $sortData,
                'seo' => $seo,
                'request' => $request,
                'breadcrumbs' => Yii::$app->utility->buildPageBreadcrumbsNoLevel($category, $category->translation->title, '/category/videos')
            ]);
        } else {
            $category_2 = $this->defineCategoryByAnyLocale($alias);
            if ($category_2 !== null) {
                return $this->redirect(['/category/videos', 'alias' => $category_2]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested category is not found'));
            return $this->redirect(['/category/video-catalog']);
        }

        return $result;
    }

    /**
     * @param $sortByParam
     * @return array
     */
    private function getSortData($sortByParam)
    {
        $sortTitles = [
            'created_at;desc' => Yii::t('filter', 'Publish date'),
            'price;asc' => Yii::t('filter', 'Price ascending'),
            'price;desc' => Yii::t('filter', 'Price descending'),
        ];
        $sort = [
            'value' => $sortByParam,
            'title' => $sortTitles[$sortByParam]
        ];
        return $sort;
    }
}