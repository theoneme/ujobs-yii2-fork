<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.11.2016
 * Time: 18:44
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\helpers\ElasticConditionsHelper;
use common\models\Category;
use common\models\elastic\JobElastic;
use common\models\elastic\PageElastic;
use common\models\elastic\ProfileElastic;
use common\models\elastic\UserPortfolioElastic;
use common\models\Job;
use common\models\Page;
use common\models\Subscription;
use common\models\user\Profile;
use common\models\UserPortfolio;
use common\modules\store\models\Order;
use frontend\modules\elasticfilter\components\FilterJob;
use frontend\modules\elasticfilter\components\FilterPage;
use frontend\modules\elasticfilter\components\FilterPortfolio;
use frontend\modules\elasticfilter\components\FilterPro;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BaseCategoryController
 * @package frontend\controllers
 */
class BaseCategoryController extends FrontEndController
{
    const ENTITY_JOB = 'job';
    const ENTITY_TENDER = 'tender';
    const ENTITY_PRO = 'pro';
    const ENTITY_PORTFOLIO = 'portfolio';
    const ENTITY_ARTICLE = 'article';

    /**
     * @param string $type
     * @return mixed
     */
    protected function prepareRootCatalogPage($type = null)
    {
        $category = $this->defineCategory('root');

        $view = 'root';

        switch ($type) {
            case self::ENTITY_PRO:
                $items = ProfileElastic::find()
                    ->where(['status' => Profile::STATUS_ACTIVE])
                    ->andWhere(['not', ['is_bio_empty' => true]])
                    ->andWhere(['not', ['gravatar_email' => null]]);

                $filterClass = FilterPro::class;
                $filterView = 'filter-pro';

                break;
            case self::ENTITY_PORTFOLIO:
                $items = UserPortfolioElastic::find()
                    ->where(['status' => UserPortfolio::STATUS_ACTIVE])
                    ->andWhere(['not', ['image' => null]])
                    ->andWhere(['not', ['pictures' => 0]]);
//                    ->andWhere(['not', ['pictures' => 1]]);

                $filterClass = FilterPortfolio::class;
                $filterView = 'filter-portfolio';
                $view = 'root-portfolio';

                break;
            case self::ENTITY_ARTICLE:
                $subscriptions = [];
                if  (!isGuest()) {
                    $subscriptions = ArrayHelper::getColumn(
                        array_filter(Yii::$app->user->identity->subscriptions, function($var) { return $var->entity === Subscription::ENTITY_PROFILE;}),
                        'entity_id'
                    );
                }
                $items = PageElastic::find()
                    ->where(['type' => Page::TYPE_ARTICLE])
                    ->andWhere(['or',
                        ['status' => Page::STATUS_ACTIVE],
                        ['and',
                            ['status' => Page::STATUS_ACTIVE_FOR_SUBSCRIBERS],
                            ['user_id' => $subscriptions]
                        ]
                    ]);

                $filterClass = FilterPage::class;
                $filterView = 'filter-page';
                $view = 'page';

                break;
            default:
                $condition = [
                    'bool' => [
                        'must_not' => [
                            ['term' => ['profile.status' => Profile::STATUS_PAUSED]],
                            ['term' => ['profile.status' => Profile::STATUS_DISABLED]],
                        ]
                    ]
                ];
                if (in_array($type, [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER])) {
                    $condition['bool']['must'][] = [
                        'bool' => ElasticConditionsHelper::getNoOrdersQuery()
                    ];
                }
                $items = JobElastic::find()
                    ->query($condition)
                    ->where([
                        'type' => $type === Job::TYPE_TENDER ? [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER] : $type,
                        'status' => Job::STATUS_ACTIVE,
                    ]);
                $filterClass = FilterJob::class;
                $filterView = 'filter';
        }
        $filter = new $filterClass([
            'query' => $items,
            'type' => $type,
            'category' => $category,
        ]);
        $search = Yii::$app->request->get('request');

        $seo = $this->getCategorySeo($category, $type, $filter->attributeFilters, $search);

        return $this->render($view, [
            'category' => $category,
            'filter' => $filter,
            'seo' => $seo,
            'entity' => $type,
            'filterView' => $filterView,
        ]);
    }

    /**
     * @param string $category_1
     * @param string $type
     * @return string|\yii\web\Response
     */
    protected function prepareCatalogPage($category_1 = null, $type = null)
    {
        /** @var Category $category */
        $category = $this->defineCategory($category_1);

        if ($category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();
            $allChildrenCategoriesIds[] = $category->id;

            $view = 'jobs';
            $filterClass = FilterJob::class;
            $filterView = 'filter';

            switch ($type) {
                case self::ENTITY_TENDER:
                    $items = JobElastic::find()
                        ->where([
                            'category_id' => $allChildrenCategoriesIds,
                            'type' => $type === Job::TYPE_TENDER ? [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER] : $type,
                            'status' => Job::STATUS_ACTIVE,
                        ])->query([
                            'bool' => ElasticConditionsHelper::getNoOrdersQuery()
                        ]);

                    break;
                case self::ENTITY_PORTFOLIO:
                    $items = UserPortfolioElastic::find()
                        ->filter(['range' => ['pictures' => ['gt' => 0]]])
                        ->where(['status' => UserPortfolio::STATUS_ACTIVE, 'category_id' => $allChildrenCategoriesIds])
                        ->andWhere(['not', ['image' => null]]);

                    $filterClass = FilterPortfolio::class;
                    $filterView = 'filter-portfolio';
                    $view = 'root-portfolio';

                    break;
                case self::ENTITY_ARTICLE:
                    $subscriptions = [];
                    if  (!isGuest()) {
                        $subscriptions = ArrayHelper::getColumn(
                            array_filter(Yii::$app->user->identity->subscriptions, function($var) { return $var->entity === Subscription::ENTITY_PROFILE;}),
                            'entity_id'
                        );
                    }
                    $items = PageElastic::find()
                        ->where(['type' => Page::TYPE_ARTICLE, 'category_id' => $allChildrenCategoriesIds])
                        ->andWhere(['or',
                            ['status' => Page::STATUS_ACTIVE],
                            ['and',
                                ['status' => Page::STATUS_ACTIVE_FOR_SUBSCRIBERS],
                                ['user_id' => $subscriptions]
                            ]
                        ]);

                    $filterClass = FilterPage::class;
                    $filterView = 'filter-page';
                    $view = 'page';

                    break;

                case self::ENTITY_JOB:
                default:
                    $condition = [
                        'bool' => [
                            'must_not' => [
                                ['term' => ['profile.status' => Profile::STATUS_PAUSED]],
                                ['term' => ['profile.status' => Profile::STATUS_DISABLED]],
                            ]
                        ]
                    ];
                    $items = JobElastic::find()
                        ->query($condition)
                        ->where([
                            'category_id' => $allChildrenCategoriesIds,
                            'status' => Job::STATUS_ACTIVE,
                            'type' => $type
                        ]);

                    break;
            }

            $search = Yii::$app->request->get('request');

            $filter = new $filterClass([
                'query' => $items,
                'type' => $type,
                'category' => $category,
            ]);

            $seo = $this->getCategorySeo($category, $type, $filter->attributeFilters, $search);

            $result = $this->render($view, [
                'category' => $category,
                'filter' => $filter,
                'seo' => $seo,
                'entity' => $type,
                'filterView' => $filterView,
                'search' => $search,
            ]);
        } else {
            $category_2 = $this->defineCategoryByAnyLocale($category_1);
            if ($category_2 !== null) {
                return $this->redirect(['/category/' . $type . 's', 'category_1' => $category_2]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested category is not found'));
            return $this->redirect(['/category/' . $type . '-catalog']);
        }

        return $result;
    }

    /**
     * @param string $category_1
     * @param string $type
     * @return string|\yii\web\Response
     */
    protected function prepareUserCatalogPage($category_1 = null, $type = null)
    {
        $category = $this->defineCategory($category_1);

        if ($category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();
            $allChildrenCategoriesIds[] = $category->id;

            $pros = ProfileElastic::find()
                ->query([
                    'bool' => [
                        'must' => [
                            ElasticConditionsHelper::getCategoriesCondition($allChildrenCategoriesIds),
                        ]
                    ]
                ])
                ->where(['status' => Profile::STATUS_ACTIVE])
                ->andWhere(['not', ['is_bio_empty' => true]])
                ->andWhere(['not', ['gravatar_email' => null]]);

            $filter = new FilterPro([
                'query' => $pros,
                'type' => $type,
                'category' => $category,
            ]);
            $search = Yii::$app->request->get('request');

            $seo = $this->getCategorySeo($category, $type, $filter->attributeFilters, $search);

            $result = $this->render('jobs', [
                'category' => $category,
                'filter'=> $filter,
                'seo' => $seo,
                'entity' => $type,
            ]);
        } else {
            $category_2 = $this->defineCategoryByAnyLocale($category_1);
            if ($category_2 !== null) {
                return $this->redirect(['/category/' . $type . 's', 'category_1' => $category_2]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested category is not found'));
            return $this->redirect(['/category/' . $type . '-catalog']);
        }

        return $result;
    }


    /**
     * @param null $category_1
     * @return Category
     */
    protected function defineCategory($category_1 = null)
    {
        /** @var $category Category */
        $category = Category::find()
            ->select('category.id, type, alias, lvl, lft, rgt, root, template, image, attribute_set_id')
            ->joinWith(['translation'])
            ->where(['content_translation.slug' => $category_1])
            ->one();

        return $category;
    }

    /**
     * @param null $category_1
     * @return string
     */
    protected function defineCategoryByAnyLocale($category_1 = null)
    {
        $category = Category::find()
            ->joinWith(['translations'])
            ->where(['content_translation.slug' => $category_1, 'type' => 'job_category'])
            ->one();

        return $category->translations[Yii::$app->language]->slug ?? null;
    }

    /**
     * @param Category $category
     * @param string $type
     * @param array $filters
     * @param string $request
     * @return array
     */
    protected function getCategorySeo($category, $type = Job::TYPE_JOB, $filters = [], $request = null){
        $seoAdvanced = $category->getMeta($type, 'category', $filters, $request);
        $templates = [];
        if($seoAdvanced !== null) {
            $seoAdvanced->keyword = $category->translation->title;
            $templates = $seoAdvanced->templates;
        }
        $this->registerMetaTags($category, $templates);
        $seo = $this->buildSeoArray($category, $templates);
        return $seo;
    }
}