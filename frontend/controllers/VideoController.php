<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 13:31
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\Video;
use common\models\user\User;
use common\modules\store\models\Currency;
use frontend\models\PostVideoForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class VideoController
 * @package frontend\controllers
 */
class VideoController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['view'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost() && Video::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        /** @var Video $model */
        $model = Video::find()
            ->joinWith(['user', 'profile'])
            ->where(['alias' => $alias])
            ->one();

        if ($model === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested video is not found'));
            return $this->redirect(['/site/index']);
        }

        $this->registerMetaTags($model);

//        $parentCategories = $model->category->getParents();
        $breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($model->category, $model->title, '/category/videos');
//        $breadcrumbs = [];

        /* @var User $user */
        $user = Yii::$app->user->identity;
        $targetUser = $model->user;

        $accessInfo = $user ? $user->isAllowedToCreateConversation($targetUser) : ['allowed' => false];

        return $this->render('view', [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'accessInfo' => $accessInfo,
        ]);
    }


    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'nomenu_layout';

        $input = Yii::$app->request->post();
        $availableLocales = Yii::$app->params['languages'];
        if (!$input) { // Нулевой шаг - выбор локалей
            return $this->render('locale-step', ['locales' => $availableLocales]);
        }

        $model = new PostVideoForm();
        $model->videoObject = new Video();
        $locales = !empty($input['locales']) ? $input['locales'] : [];
        $locales = array_filter($locales, function ($locale) use ($availableLocales) {
            return isset($availableLocales[$locale]);
        });
        if ($locales) { // Если в посте есть локали - попадаем на первый шаг
            $model->locale = array_shift($locales);
            $rootCategoriesList = ArrayHelper::map(
                Category::find()
                    ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                    ->andWhere(['lvl' => 1])
                    ->joinWith(['translation'])
                    ->andWhere(['type' => 'job_category'])
                    ->all(),
                'id',
                'translation.title'
            );
            return $this->render('_form', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'action' => 'create',
                'currencies' => Currency::getFormattedCurrencies()
            ]);
        }

// Иначе попадаем на валиацию\сабмит
        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $model->validateAll();
        }

        if (!empty($input['id'])) {
            $model->loadVideo($input['id']);
        }
        $model->myLoad($input);

        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Video successfully posted.'));

            return $this->redirect(['/video/view', 'alias' => $model->videoObject->alias]);
        }

        Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred during Video creation.'));

        return $this->render('locale-step', ['locales' => $availableLocales]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'nomenu_layout';

        $model = new PostVideoForm();
        $model->loadVideo($id);
        $input = Yii::$app->request->post();

        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $model->validateAll();
        }

        if (!empty($input)) {
            $model->myLoad($input);

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Video successfully updated.'));

                return $this->redirect(['/video/view', 'alias' => $model->videoObject->alias]);
            }
        }
        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])
                ->all(),
            'id',
            'translation.title'
        );

        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'currencies' => Currency::getFormattedCurrencies()
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Video::STATUS_DELETED]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Video
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}