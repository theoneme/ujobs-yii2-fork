<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use Yii;
use yii\console\Application;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * Class FileController
 * @package frontend\controllers
 */
class FileController extends FrontEndController
{
    /**
     * @return string
     */
    public function actionUpload()
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $response = [];
        $files = UploadedFile::getInstancesByName('uploaded_files');
        $post = Yii::$app->request->post();
        $path = '/uploads/temp/' . $year . '/' . $month . '/' . $day . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
            if (Yii::$app instanceof Application) {
                chown($dir, 'www-data');
                chgrp($dir, 'www-data');
            }
        }
        foreach ($files as $file) {
            try {
                $sourceName = $file->name ?? 'unknown';
                $hash = hash('crc32b', $file->name . time());
                $name = $path . $hash . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@webroot') . $name);

                $response = [
                    'initialPreview' => [$name],
                    'initialPreviewConfig' => [
                        [
                            'type' => 'video',
                            'filetype' => 'video/*',
                            'caption' => basename($name),
                            'url' => Url::toRoute('/file/delete'),
                            'key' => 'file_new_' . $post['fileindex'],
                        ]
                    ],
                    'initialPreviewThumbTags' => [
                        '{actions}' => '{actions}'
                    ],
                    'uploadedPath' => $name,
                    'imageKey' => 'file_new_' . $post['fileindex'],
                ];
            } catch (\Exception $e) {
                Yii::error(VarDumper::export($e));
                $response = [
                    'error' => "File {$sourceName} is corrupted"
                ];
            }
        }

        return Json::encode($response);
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
        // TODO Придумать нормальную логику удаления
        return true;
    }
}