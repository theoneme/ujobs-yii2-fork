<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Page;
use frontend\models\ReferralForm;
use frontend\models\search\ReferralSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ReferralController
 * @package frontend\controllers
 */
class ReferralController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['send'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'referral-program'])->one();
        $this->registerMetaTags($page);

        if (Yii::$app->user->isGuest) {
            return $this->render('guest', ['page' => $page]);
        }

        $searchModel = new ReferralSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'page' => $page,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'referralForm' => new ReferralForm()
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSend()
    {
        $model = new ReferralForm();
        if (Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->submit();
        }

        return $this->redirect(['/referral/index']);
    }
}