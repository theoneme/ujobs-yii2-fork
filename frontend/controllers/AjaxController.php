<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 15:45
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\helpers\ElasticConditionsHelper;
use common\models\Category;
use common\models\elastic\JobElastic;
use common\models\elastic\ProfileElastic;
use common\models\Event;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Notification;
use common\models\Page;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use common\services\RememberViewService;
use frontend\components\JobSearchHelpWidget;
use frontend\modules\account\models\ConversationSearch;
use Yii;
use yii\db\Expression;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\validators\EmailValidator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers
 */
class AjaxController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => [
                    'attribute-list',
                    'render-notifications',
                    'render-messages',
                    'remove-viewed',
                    'tag-search',
                    'catalog-search',
                    'city-list',
                    'subcat',
                    'sellers-list',
                    'category-list',
                    'house-list',
                    'load-page',
                    'share-by-email',
                    'category-suggest',
                ],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionSubcat()
    {
        $output = [];
        $selected = '';

        if ($parents = Yii::$app->request->post('depdrop_parents')) {
            if ($parents !== null) {
                $parentCategory = $parents[0];
                $parentCategoryObject = Category::findOne((int)$parentCategory);
                if ($parentCategoryObject) {
                    /* @var Category[] $children */
                    $children = $parentCategoryObject->children(1)->orderBy('sort_order')->all();

                    foreach ($children as $child) {
                        $output[] = [
                            'id' => $child->id,
                            'name' => $child->translation->title,
                            'lft' => $child->lft,
                            'rgt' => $child->rgt
                        ];
                    }
                }

                if ($params = Yii::$app->request->post('depdrop_params')) {
                    $selected = $params[0];
                }

                return ['output' => $output, 'selected' => $selected];
            }
        }

        return ['output' => $output, 'selected' => ''];
    }

    /**
     * @return array
     */
    public function actionTagSearch()
    {
        $output = [
            'success' => false
        ];
        $request = Yii::$app->request->post('request');

        if ($request) {
            $tags = JobAttribute::find()
                ->joinWith(['job'])
                ->where([
                    'job.type' => 'job',
                    'job.status' => Job::STATUS_ACTIVE,
                    'job_attribute.entity_alias' => 'tag'
                ])
                ->andWhere(['like', 'job_attribute.value', $request])
                ->groupBy('job_attribute.value_alias')
                ->all();
            /* @var $tags JobAttribute[] */
            if ($tags) {
                $output['success'] = true;

                foreach ($tags as $tag) {
                    $output['data'][] = [
                        'label' => $tag->value,
                        'link' => Url::to(['/category/tag', 'reserved_job_tag' => $tag->value_alias])
                    ];
                }
            }

            $categories = Category::find()
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->joinWith(['translation'])
                ->where(['type' => 'job_category'])
                ->andWhere(['like', 'content_translation.title', $request])
                ->all();

            if (!empty($categories)) {
                $output['success'] = true;

                foreach ($categories as $category) {
                    $validPageUrl = Url::to(['/category/jobs', 'category_1' => $category->alias]);
                    $output['data'][] = [
                        'label' => $category->translation->title,
                        'link' => $validPageUrl
                    ];
                }
            }
        }
        return $output;
    }

    /**
     * @param string $type
     * @return array
     */
    public function actionCatalogSearch($type = 'job')
    {
        $output = ['success' => false];
        $request = Yii::$app->request->post('request');
        $request = str_replace('-', ' ', $request);

        if ($request) {
            if ($type === 'pro') {
                $profiles = ProfileElastic::find()
                    ->where(['status' => Profile::STATUS_ACTIVE])
                    ->query([
                        'bool' => [
                            'should' => array_merge(
                                ElasticConditionsHelper::getNestedSearchQuery('translations', ['translations.title'], $request),
                                ElasticConditionsHelper::getNestedSearchQuery('attrs', ['attrs.title'], $request)
                            )
                        ]
                    ])->all();

                /* @var $profiles ProfileElastic[] */
                if ($profiles) {
                    $output['success'] = true;
                    foreach ($profiles as $profile) {
                        $output['data'][] = [
                            'label' => $profile->name,
                            'link' => Url::to(["/category/{$type}-catalog", 'request' => $profile->name])
                        ];
                    }
                }
            } else {
                $jobs = JobElastic::find()->where([
                    'type' => $type === Job::TYPE_TENDER ? [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER] : $type,
                    'status' => Job::STATUS_ACTIVE
                ])->query([
                    'bool' => [
                        'should' => array_merge(
                            ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $request),
                            ElasticConditionsHelper::getNestedSearchQuery('attrs', ['attrs.title'], $request)
                        )
                    ]
                ])->all();

                /* @var $jobs JobElastic[] */
                if ($jobs) {
                    $output['success'] = true;
                    foreach ($jobs as $job) {
                        $output['data'][] = [
                            'label' => $job->getLabel(),
                            'link' => Url::to(["/category/{$type}-catalog", 'request' => $job->getLabel()])
                        ];
                    }
                }
            }
        }
        if (array_key_exists('data', $output)) {
            $output['data'] = array_unique($output['data'], SORT_REGULAR);
        }

        return $output;
    }

    /**
     * @return array
     */
    public function actionRemoveViewed()
    {
        $model = Job::findOne((int)Yii::$app->request->post('id'));
        if ($model !== null) {
            $rememberViewService = new RememberViewService($model, 'viewed', Yii::$app->session);
            return ['success' => $rememberViewService->removeFromSession()];
        }

        return ['success' => false];
    }

    /**
     * Lists all tags for ajax queries.
     * @param $alias
     * @return mixed
     */
    public function actionAttributeList($alias)
    {
        $query = Yii::$app->request->post('query');
        $attributes = AttributeValue::find()
            ->joinWith(['relatedAttribute', 'translation'])
            ->select(['mod_attribute_value.id', 'mod_attribute_value.attribute_id', 'mod_attribute_value.alias', 'mod_attribute_description.title as value'])
            ->where(['mod_attribute.alias' => $alias, 'mod_attribute_value.status' => AttributeValue::STATUS_APPROVED])
            ->andWhere(['like', 'mod_attribute_description.title', urldecode($query)])
            ->groupBy('mod_attribute_value.id')
            ->asArray()
            ->all();

        return $attributes;
    }

    /**
     * Renders user`s notifications
     * @return array
     */
    public function actionRenderNotifications()
    {
        $output = [
            'success' => false,
            'html' => null,
            'message' => Yii::t('account', 'Notification list is empty')
        ];

        $limit = 5;
        $subtract = 0;

        if (!isGuest()) {
            $notifications = Notification::find()->joinWith(['from'])->where(['to_id' => Yii::$app->user->identity->getCurrentId(), 'is_visible' => true])->limit($limit)->orderBy('created_at desc')->all();
            if (count($notifications) > 0) {
                $output['success'] = true;
                foreach ($notifications as $notification) {
                    /* @var Notification $notification */
                    if ($notification->is_read === 0) {
                        $notification->updateAttributes(['is_read' => 1]);
                        $subtract++;
                    }

                    $output['html'] .= $this->renderPartial('@frontend/components/views/listview/' . $notification->subjectsToViews[$notification->subject], [
                        'model' => $notification
                    ]);
                }
            }
        }
        $output['subtract'] = $subtract;

        return $output;
    }

    /**
     * Renders user`s notifications
     * @return array
     */
    public function actionRenderMessages()
    {
        $output = [
            'success' => false,
            'html' => '<div class="header-no-messages text-center">' . Yii::t('account', 'No new messages') . '</div>'
        ];

        if (!isGuest()) {
            $conversationSearch = new ConversationSearch(['from_id' => Yii::$app->user->identity->getCurrentId(), 'limit' => 5]);
            $data = $conversationSearch->search(Yii::$app->request->queryParams);
            $conversations = $data['items'];

            if (count($conversations) > 0) {
                $output['success'] = true;
                $output['html'] = array_reduce($conversations, function ($carry, $item) {
                    return $carry . $this->renderPartial('@frontend/components/views/listview/message-item', ['data' => $item]);
                }, '');
            }
        }

        return $output;
    }

    /**
     * Lists cities for ajax queries.
     * @return mixed
     */
    public function actionCityList()
    {
        $query = Yii::$app->request->post('query');
        $cities = AttributeValue::find()
            ->select(['mod_attribute_value.id', 'mod_attribute_value.attribute_id', 'mod_attribute_value.alias', 'mod_attribute_description.title'])
            ->joinWith(['translation', 'relatedAttribute'])
            ->where(['mod_attribute.alias' => 'city'])
            ->andWhere(['like', 'mod_attribute_description.title', urldecode($query)])
            ->asArray()
            ->all();

        return $cities;
    }


    /**
     * @return string
     */
    public function actionJobSearchStep()
    {
        return JobSearchHelpWidget::widget([
            'step_id' => Yii::$app->request->post('step_id'),
            'job_id' => Yii::$app->request->post('job_id'),
            'lvl' => Yii::$app->request->post('lvl', 1),
            'forceShow' => true
        ]);
    }

    /**
     * @return array
     */
    public function actionSellersList()
    {
        $query = urldecode(Yii::$app->request->post('query'));
        $profiles = Profile::find()
            ->joinWith(['user', 'translations'])
            ->andWhere(['or',
                ['user.id' => $query],
                ['like', 'user.email', $query],
                ['like', 'content_translation.title', $query],
//                ['like', 'user.phone', $query]
            ])
            ->groupBy('profile.user_id')
            ->limit(10)
            ->all();

        $result = array_map(function ($value) {
            return [
                'id' => $value->user_id,
                'value' => "{$value->getSellerName()} (ID: {$value->user_id})",
                'img' => $value->getThumb('catalog'),
                'phone' => $value->user->phone,
                'email' => $value->user->email
            ];
        }, $profiles);

        return $result;
    }


    /**
     * @return array
     */
    public function actionCategoryList()
    {
        $query = urldecode(Yii::$app->request->post('query'));
        return Category::find()
            ->select(['category.id', 'content_translation.title as label'])
            ->joinWith(['translations'])
            ->andWhere(['like', 'content_translation.title', $query])
            ->andWhere(['>=', 'category.lvl', 2])
            ->groupBy('category.id')
            ->limit(10)
            ->asArray()
            ->all();
    }

    /**
     * @param $alias
     * @return mixed
     */
    public function actionLoadPage($alias)
    {
        /* @var Page $page */
        $page = Page::find()->joinWith(['translations'])->where(['alias' => $alias])->one();
        if ($page !== null) {
            $response = [
                'title' => $page->getLabel(),
                'content' => $page->getContent()
            ];
        } else {
            $response = ['error' => Yii::t('app', 'Requested page is not found')];
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function actionShareByEmail()
    {
        $response = [
            'success' => false,
            'message' => Yii::t('app', 'Error occurred during sending email')
        ];
        $post = Yii::$app->request->post();
        if (Event::find()->where(['user_id' => userId(), 'code' => Event::EMAIL_SHARE])->andWhere(['>', 'created_at', time() - 60 * 60])->exists()) {
            $response['message'] = Yii::t('app', 'You can only share links once per hour');
        } else {
            if (isset($post['email'], $post['label'], $post['name'], $post['thumb'], $post['link'])) {
                if (!preg_match('/^http(s)?:\/\/ujobs.me.*/ui', $post['link'])) {
                    $response['message'] = Yii::t('app', 'You can only share links to pages on our site');
                } else {
                    $emailValidator = new EmailValidator();
                    if ($emailValidator->validate($post['email']) !== true) {
                        $response['message'] = Yii::t('yii', '{attribute} is not a valid email address.', ['attribute' => $post['email']]);
                    } else {
                        $view = 'share';
                        $subject = Yii::t('app', 'User shared with you a link on {site} site', ['site' => Yii::$app->name]);
                        /* @var $user User */
                        $user = Yii::$app->user->identity;
                        $params = [
                            'name' => $post['name'],
                            'label' => $post['label'],
                            'thumb' => $post['thumb'],
                            'link' => $post['link'],
                            'sender' => $user->getSellerName(),
                            'senderThumb' => (!empty($user->profile->gravatar_email) || ($user->is_company && !empty($user->company->logo))) ? $user->getThumb('catalog') : null,
                        ];
                        $mailer = Yii::$app->mailer;
                        $mailer->viewPath = '@frontend/views/email';
                        if ($mailer->compose(['html' => $view], $params)->setTo($post['email'])->setFrom(Yii::$app->params['ujobsNoty'])->setSubject($subject)->send()) {
                            Event::addEvent(Event::EMAIL_SHARE);
                            $response = [
                                'success' => true,
                                'message' => Yii::t('app', 'Email successfully sent')
                            ];
                        }
                    }
                }
            }
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function actionHouseList()
    {
        $query = urldecode(Yii::$app->request->post('query'));
        $categories = Category::find()
            ->joinWith(['translations'])
            ->where(['content_translation.slug' => ['stroitelstvo', 'nedvizhimost']])
            ->select('category.id')
            ->column();
        $jobs = Job::find()
            ->joinWith(['translations', 'attachments'])
            ->where(['like', 'content_translation.title', $query])
            ->andWhere(['type' => Job::TYPE_JOB])
            ->andWhere(['or',
                ['category_id' => $categories],
                ['parent_category_id' => $categories],
            ])
            ->groupBy('job.id')
            ->limit(10)
            ->all();
        $products = Product::find()
            ->joinWith(['translations', 'attachments'])
            ->where(['like', 'content_translation.title', $query])
            ->andWhere(['type' => Product::TYPE_PRODUCT])
            ->andWhere(['content_translation.slug' => $categories])
            ->andWhere(['or',
                ['category_id' => $categories],
                ['subcategory_id' => $categories],
                ['parent_category_id' => $categories],
            ])
            ->groupBy('product.id')
            ->limit(10)
            ->all();

        $result = array_map(function ($var) {
            /* @var Job|Product $var */
            return [
                'value' => $var->type . '-' . $var->id,
                'label' => $var->getLabel(),
                'image' => $var->getThumb('catalog')
            ];
        }, array_merge($jobs, $products));

        return $result;
    }

    /**
     * @param string $entity
     * @param $request
     * @return array
     */
    public function actionCategorySuggest($entity = 'job', $request)
    {
        $output = [
            'success' => false,
        ];

        if (!in_array($entity, ['job', 'product'])) {
            return $output;
        }

        switch ($entity) {
            case 'job':
                $aggs = JobElastic::find();
                break;
            case 'product':
                $aggs = ProductElastic::find();
                break;
        }

        $aggs = $aggs->where([
            'type' => Job::TYPE_JOB,
            'status' => Job::STATUS_ACTIVE,
        ])
            ->query(['bool' => [
                'should' => ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $request)
            ]])
            ->aggregations([
                'category' => [
                    'terms' => [
                        'field' => 'category_id',
                        'size' => 6,
                        'order' => [
                            'score' => 'desc'
                        ]
                    ],
                    'aggs' => [
                        'score' => [
                            'max' => [
                                'script' => [
                                    'inline' => '_score'
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->search(null, ['search_type' => 'count']);
        $categoryIds = array_map(function ($value) {
            return $value['key'];
        }, $aggs['aggregations']['category']['buckets'] ?? []);

        if (empty($categoryIds)) {
            return $output;
        }

        $orderCondition = 'FIELD(`id`,' . implode(',', $categoryIds) . ')';
        $categories = Category::find()
            ->where(['id' => $categoryIds])
            ->orderBy(new Expression($orderCondition))
            ->all();

        $output['success'] = true;
        $data = [];
        foreach ($categories as $category) {
            $subData = [];
            /** @var Category $baseCategory */
            $baseCategory = Category::find()->where(['id' => $category->id])->andWhere(['>', 'lvl', 1])->one();
            if ($baseCategory instanceof Category) {
                $parents = $baseCategory->parents()->andWhere(['not', ['lvl' => 0]])->all();
                foreach ($parents as $parent) {
                    $subData[] = [
                        'title' => $parent->translation->title,
                        'id' => $parent->id
                    ];
                }
                $subData[] = [
                    'title' => $baseCategory->translation->title,
                    'id' => $baseCategory->id
                ];
                $data[] = $subData;
            }
        }

        $output['html'] = $this->render('category-suggestions', [
            'data' => $data
        ]);

        return $output;
    }
}
