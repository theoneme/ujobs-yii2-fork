<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Notification;
use common\models\UserFriend;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Class FriendController
 * @package frontend\controllers
 */
class FriendController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['toggle'],
                    'update' => ['accept-friendship'],
                    'delete' => ['decline-friendship'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['toggle', 'accept-friendship', 'decline-friendship'], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['toggle', 'accept-friendship', 'decline-friendship'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['toggle', 'accept-friendship', 'decline-friendship']
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionToggle()
    {
        $input = Yii::$app->request->post();
        $response = ['success' => false];

        if (!empty($input)) {
            $entityId = (int)$input['entity_id'];

            $friendshipExists = Yii::$app->user->identity->hasFriendship($entityId);
            if ($friendshipExists === false) {
                $friendship = new UserFriend([
//                    'user_id' => userId(),
                    'target_user_id' => $entityId
                ]);
                try {
                    if ($friendship->save() === true) {
                        return ['success' => true, 'message' => Yii::t('app', 'Friendship request has been sent')];
                    }
                } catch (Exception $e) {
                    return ['success' => false, 'message' => Yii::t('app', 'Friendship request has already been sent')];
                }

            } else {
                $friendship = UserFriend::find()
                    ->where(['or', ['user_id' => userId(), 'target_user_id' => $entityId], ['user_id' => $entityId, 'target_user_id' => userId()]])
                    ->one();

                if ($friendship !== null) {
                    if ($friendship->delete() !== false) {
                        $inviteNotification = Notification::find()
                            ->where(['to_id' => $entityId, 'subject' => Notification::SUBJECT_FRIENDSHIP_REQUEST])
                            ->orderBy('id desc')
                            ->one();

                        if($inviteNotification !== null) {
                            $inviteNotification->delete();
                        }

                        return ['success' => true];
                    }
                }
            }
        }

        return $response;
    }


    /**
     * @return array
     */
    public function actionAcceptFriendship()
    {
        $userId = Yii::$app->request->post('entityId');
        $inviteId = Yii::$app->request->post('inviteId');

        $result = ['success' => false, 'message' => 'An unexpected error has occured'];

        if ($userId !== null) {
            /** @var Notification $inviteNotification */
            $inviteNotification = Notification::find()
                ->where(['to_id' => userId(), 'id' => $inviteId, 'subject' => Notification::SUBJECT_FRIENDSHIP_REQUEST])
                ->one();

            if ($inviteNotification !== null) {
                $updatedRows = UserFriend::updateAll([
                    'status' => UserFriend::STATUS_APPROVED
                ], ['or', [
                    'user_id' => userId(),
                    'target_user_id' => (int)$userId,
                    'status' => UserFriend::STATUS_PENDING
                ], [
                    'target_user_id' => userId(),
                    'user_id' => (int)$userId,
                    'status' => UserFriend::STATUS_PENDING
                ]]);

                if ($updatedRows > 0) {
                    $updatedCustomData = array_merge($inviteNotification->customDataArray, [
                        'action' => Notification::ACTION_ACCEPT
                    ]);
                    $inviteNotification->updateAttributes(['custom_data' => json_encode($updatedCustomData)]);

                    return [
                        'success' => true,
                        'message' => Yii::t('notifications', 'You have accepted friendship')
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function actionDeclineFriendship()
    {
        $userId = Yii::$app->request->post('entityId');
        $inviteId = Yii::$app->request->post('inviteId');

        $result = ['success' => false, 'message' => 'An unexpected error has occured'];

        if ($userId !== null) {
            /** @var Notification $inviteNotification */
            $inviteNotification = Notification::find()
                ->where(['to_id' => userId(), 'id' => $inviteId, 'subject' => Notification::SUBJECT_FRIENDSHIP_REQUEST])
                ->one();

            if ($inviteNotification !== null) {
                $deletedRows = UserFriend::deleteAll(['or', [
                    'user_id' => userId(),
                    'target_user_id' => (int)$userId,
                    'status' => UserFriend::STATUS_PENDING
                ], [
                    'target_user_id' => userId(),
                    'user_id' => (int)$userId,
                    'status' => UserFriend::STATUS_PENDING
                ]]);

                if ($deletedRows > 0) {
                    $updatedCustomData = array_merge($inviteNotification->customDataArray, [
                        'action' => Notification::ACTION_DECLINE
                    ]);
                    $inviteNotification->updateAttributes(['custom_data' => json_encode($updatedCustomData)]);

                    return [
                        'success' => true,
                        'message' => Yii::t('notifications', 'You have declined friendship')
                    ];
                }
            }
        }

        return $result;
    }
}