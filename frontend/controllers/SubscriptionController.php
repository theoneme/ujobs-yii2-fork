<?php

namespace frontend\controllers;

use common\models\Subscription;
use Yii;
use common\controllers\FrontEndController;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class LikeController
 * @package frontend\controllers
 */
class SubscriptionController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['toggle'], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['toggle'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionToggle()
    {
        $input = Yii::$app->request->post();
        $response = ['success' => false];

        if(!empty($input)) {
            $subscription = Subscription::find()->where(['entity' => $input['entity'], 'entity_id' => $input['entity_id'], 'user_id' => Yii::$app->user->identity->id])->one();
            if($subscription !== null) {
                $subscription->delete();
                $response['success'] = true;
            } else {
                if((new Subscription(['entity' => $input['entity'], 'entity_id' => $input['entity_id']]))->save()) {
                    $response['success'] = true;
                }
            }
        }

        return $response;
    }
}