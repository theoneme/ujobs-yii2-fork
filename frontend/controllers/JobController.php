<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 13:31
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\DynamicForm;
use common\models\Job;
use common\models\JobAttribute;
use common\models\JobExtra;
use common\models\JobQa;
use common\models\Review;
use common\models\user\User;
use common\modules\store\models\Currency;
use common\services\CounterService;
use common\services\RememberViewService;
use frontend\models\PostJobForm;
use frontend\services\JobPackageExampleService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class JobController
 * @package frontend\controllers
 */
class JobController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['render-faq', 'render-additional', 'render-extra', 'render-attributes', 'draft'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['view'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['create', 'ajax-create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                    [
                        'actions' => ['update', 'delete', 'pause', 'resume'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost() && Job::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-extra', 'render-faq', 'draft', 'ajax-create'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        /* @var Job $model */
        $model = Job::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username, is_company');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio, profile.country');
            }, 'extra', 'attachments', 'packages', 'translation', 'profile.translation'])
            ->where(['alias' => $alias])
            ->andWhere(['or',
                ['not', ['job.status' => Job::STATUS_DELETED]],
                ['job.user_id' => userId()]
            ])
            ->one();

        if ($model === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested job is not found'));
            return $this->redirect(['/category/job-catalog']);
        }

        $this->registerMetaTags($model);

        $counterServiceClick = new CounterService($model, 'click_count');
        $counterServiceClick->process();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'viewed_clicks', Yii::$app->session);
        $counterServiceView->process();

        $rememberViewService = new RememberViewService($model, 'viewed', Yii::$app->session);
        $rememberViewService->saveInSession();

        /*$validPageUrl = Yii::$app->url->buildPageFullUrl($model, false, '/job/view');

        if ($validPageUrl != Url::current()) {
            return $this->redirect($validPageUrl, 301);
        }*/

        $breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($model->category, $model->getLabel(), '/category/jobs');

        $reviewCondition = ['created_for' => $model->user_id, 'job_id' => $model->id];
        if (hasAccess($model->user_id)) {
            $reviewCondition['is_fake'] = false;
        }

        /* @var User $user */
        $user = Yii::$app->user->identity;
        $targetUser = $model->user;

        $accessInfo = $user ? $user->isAllowedToCreateConversation($targetUser) : ['allowed' => false];

        $reviewQuery = Review::find()->where($reviewCondition);
        $reviewsCount = Review::find()->where($reviewCondition)->count();
        $reviewsAverage = Review::find()->where($reviewCondition)->average('rating');
        $communicationAverage = Review::find()->where($reviewCondition)->average('communication');
        $serviceAverage = Review::find()->where($reviewCondition)->average('service');
        $recommendAverage = Review::find()->where($reviewCondition)->average('recommend');
        $reviewDataProvider = new ActiveDataProvider(['query' => $reviewQuery, 'pagination' => false]);

        return $this->render('view', [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'reviewDataProvider' => $reviewDataProvider,
            'reviewsCount' => $reviewsCount,
            'reviewsAverage' => $reviewsAverage,
            'communicationAverage' => $communicationAverage,
            'serviceAverage' => $serviceAverage,
            'recommendAverage' => $recommendAverage,
            'accessInfo' => $accessInfo,
        ]);
    }

    /**
     * @param $category_id
     * @param $entity
     * @return null|string
     */
    public function actionRenderAttributes($category_id, $entity)
    {
        /* @var Category $category */
        $category = Category::findOne($category_id);
        if ($category === null || !$category->isLeaf()) {
            return null;
        }

        $values = JobAttribute::find()
            ->where(['job_id' => $entity])
            ->joinWith(['attr', 'attrValueDescriptions', 'attrValue'])
            ->groupBy('job_attribute.value')
            ->all();

        $values = DynamicForm::formatValuesArray($values);
        $attributes = DynamicForm::getAttributesListByCategory($category);
        $model = DynamicForm::initModelFromGroup($attributes, $values);

        $showAdditionalAtStart = array_intersect(array_column($values, 'attribute_id'), array_column($attributes, 'id'));

        $temp = ArrayHelper::map($attributes, 'id', function ($model) {
            return $model;
        }, 'is_primary');
        $attributes = $temp[1] ?? [];
        $additionalAttributes = $temp[0] ?? [];

        return $this->renderAjax('@common/modules/attribute/views/partial/attributes', [
            'model' => $model,
            'attributes' => $attributes,
            'additionalAttributes' => $additionalAttributes,
            'showAdditionalAtStart' => $showAdditionalAtStart,
        ]);
    }

    /**
     * @param $category_id
     * @param $entity
     * @return string
     */
    public function actionRenderAdditional($category_id, $entity)
    {
        $model = new PostJobForm();
        $showAddressField = false;

        /** @var Category $category */
        $category = Category::findOne($category_id);
        /** @var Job $job */
        $job = Job::findOne($entity);

        if ($category !== null) {
            if ($category->job_offline || $category->job_office_available) {
                $showAddressField = true;
            }

            if ($job !== null) {
                $model->attributes = $job->attributes;
            }
        }
        return $this->renderAjax('additional-info', [
            'model' => $model,
            'showAddressField' => $showAddressField
        ]);
    }

    /**
     * Creates a new Job model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'nomenu_layout';

        $input = Yii::$app->request->post();
        $availableLocales = Yii::$app->params['languages'];
        if (!$input) { // Нулевой шаг - выбор локалей
            return $this->render('locale-step', ['locales' => $availableLocales]);
        }

        $model = new PostJobForm();
        $model->job = new Job();
        $model->scenario = PostJobForm::SCENARIO_DEFAULT;
        $model->currency_code = Yii::$app->params['app_currency_code'];
        $locales = !empty($input['locales']) ? $input['locales'] : [];
        $locales = array_filter($locales, function ($locale) use ($availableLocales) {
            return isset($availableLocales[$locale]);
        });
        if ($locales) { // Если в посте есть локали - попадаем на первый шаг
            $model->locale = array_shift($locales);
            $jobPackageExampleService = new JobPackageExampleService(Yii::$app->language);
            $jobPackageExamples = $jobPackageExampleService->getData();
            $currencies = Currency::getFormattedCurrencies();
            $currencySymbols = Currency::getCurrencySymbols();

            $rootCategoriesList = ArrayHelper::map(
                Category::find()
                    ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                    ->andWhere(['lvl' => 1])
                    ->joinWith(['translation'])
                    ->andWhere(['type' => 'job_category'])
                    ->all(),
                'id',
                'translation.title'
            );

            return $this->render('_form', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'action' => 'create',
                'jobPackageExamples' => $jobPackageExamples,
                'locales' => $locales,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
            ]);
        }

// Иначе попадаем на валиацию\сабмит
        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $model->validateAll();
        }

        if (!empty($input['id'])) { // Если работа уже есть в базе благодаря драфту (примерно всегда)
            $model->loadJob($input['id']);
            $model->job->status = Job::STATUS_REQUIRES_MODERATION;
        }
        $model->myLoad($input);

        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Job has been created. Thank you for posting an announcement!'));

            return $this->redirect(['/job/view', 'alias' => $model->job->alias]);
        }

        Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred during Job creation.'));

        return $this->render('locale-step', ['locales' => $availableLocales]);
    }

    /**
     * Creates a new Job model on ajax request.
     * @return mixed
     */
    public function actionAjaxCreate()
    {
        $input = Yii::$app->request->post();
        $response = ['success' => false];
        if ($input) {
            $model = new PostJobForm();
            $model->job = new Job();
            $model->scenario = PostJobForm::SCENARIO_DEFAULT;
            if (!empty($input['id'])) { // Если работа уже есть в базе благодаря драфту (примерно всегда)
                $model->loadJob($input['id']);
                $model->job->status = Job::STATUS_REQUIRES_MODERATION;
            }
            $model->myLoad($input);

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Job has been created. Thank you for posting an announcement!'));
                $response['success'] = true;
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred during Job creation.'));
            }
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionDraft()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [
            'success' => false
        ];
        $attachments = [];

        $model = new PostJobForm();
        $model->scenario = PostJobForm::SCENARIO_DRAFT;
        if ($input = Yii::$app->request->post()) {
            if (!empty($input['id'])) {
                $model->loadJob($input['id']);
            } else {
                $model->job = new Job();
            }

            $model->myLoad($input);
            if ($model->save()) {
                if (!empty($model->job->attachments)) {
                    foreach ($model->job->attachments as $key => $attachment) {
                        $attachments['initialPreview'][$key] = $attachment->content . '?' . time();
                        $attachments['initialPreviewConfig'][] = [
                            'caption' => basename($attachment->content),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => $key,
                        ];
                    }
                }
                $output = [
                    'success' => true,
                    'job_id' => $model->job->id,
                    'attachments' => $attachments
                ];
            }
        }

        return $output;
    }

    /**
     * Updates an existing Job model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'nomenu_layout';

        $model = new PostJobForm();
        $model->loadJob($id);
        $input = Yii::$app->request->post();
        if ($input) {
            if (Yii::$app->request->isAjax) {
                $model->myLoad($input);
                Yii::$app->response->format = Response::FORMAT_JSON;

                return $model->validateAll();
            }

            $model->myLoad($input);
            $model->job->status = Job::STATUS_REQUIRES_MODERATION;

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Job has been updated.'));

                return $this->redirect(['/job/view', 'alias' => $model->job->alias]);
            }
        }
        $jobPackageExampleService = new JobPackageExampleService(Yii::$app->language);
        $jobPackageExamples = $jobPackageExampleService->getData();

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])
                ->all(),
            'id',
            'translation.title'
        );
        $currencies = Currency::getFormattedCurrencies();
        $currencySymbols = Currency::getCurrencySymbols();

        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'action' => 'update',
            'jobPackageExamples' => $jobPackageExamples,
            'currencies' => $currencies,
            'currencySymbols' => $currencySymbols,
            'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionPause($id)
    {
        $job = $this->findModel($id);
        if ($job->status === Job::STATUS_ACTIVE) {
            $job->updateAttributes(['status' => Job::STATUS_PAUSED]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionResume($id)
    {
        $job = $this->findModel($id);
        if ($job->status === Job::STATUS_PAUSED) {
            $job->updateAttributes(['status' => Job::STATUS_ACTIVE]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Job model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Job::STATUS_DELETED]);
        $model->updateElastic();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return array
     */
    public function actionRenderFaq()
    {
        return [
            'html' => $this->renderAjax('job-faq-partial', [
                'model' => new JobQa(),
                'iterator' => Yii::$app->request->post('iterator', 0)
            ]),
            'success' => true
        ];
    }

    /**
     * @return array
     */
    public function actionRenderExtra()
    {
        return [
            'html' => $this->renderAjax('job-extra-partial', [
                'model' => new JobExtra(),
                'iterator' => Yii::$app->request->post('iterator', 0)
            ]),
            'success' => true
        ];
    }

    /**
     * Finds the Job model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Job the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}