<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\Job;
use common\models\Page;
use common\services\CounterService;
use frontend\models\PostArticleForm;
use frontend\models\PostCommentForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ArticleController
 * @package frontend\controllers
 */
class ArticleController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['view'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['form'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param string $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        /* @var Page $page */
        $page = Page::find()
            ->joinWith(['translations', 'comments', 'profile'])
            ->where(['type' => Page::TYPE_ARTICLE])
            ->andWhere(['or',
                ['alias' => $alias],
                ['content_translation.slug' => $alias],
            ])
            ->andWhere(['or',
                ['not', ['page.status' => Page::STATUS_DELETED]],
                ['page.user_id' => userId()]
            ])
            ->one();

        if ($page !== null) {
            $this->registerMetaTags($page);

            $relatedDataProvider = new ActiveDataProvider([
                'query' => Page::find()
                    ->joinWith(['translation'])
                    ->where(['not', ['alias' => $alias]])
                    ->andWhere([
                        'type' => Page::TYPE_ARTICLE,
                        'page.status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS],
                        'content_translation.locale' => Yii::$app->language
                    ])
                    ->andWhere(['not', ['page.id' => $page->id]])
                    ->orderBy([
                        new Expression('FIELD (page.user_id, ' . $page->user_id . ') DESC'),
                        new Expression('FIELD (page.category_id, ' . $page->category_id . ') DESC'),
                        'publish_date' => SORT_DESC
                    ])
                    ->groupBy('page.id')
                    ->limit(5),
                'pagination' => false
            ]);
            $jobsDataProvider = new ActiveDataProvider([
                'query' => Job::find()
                    ->joinWith(['translation', 'attachments'])
                    ->andWhere(['type' => Job::TYPE_JOB, 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => Yii::$app->language])
                    ->orderBy([
                        new Expression('FIELD (job.user_id, ' . $page->user_id . ') DESC'),
                        new Expression('FIELD (job.category_id, ' . $page->category_id . ') DESC'),
                        'created_at' => SORT_DESC
                    ])
                    ->groupBy('job.id')
                    ->limit(5),
                'pagination' => false
            ]);
            $tendersDataProvider = new ActiveDataProvider([
                'query' => Job::find()
                    ->joinWith(['translation', 'attachments'])
                    ->andWhere(['type' => Job::TYPE_TENDER, 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => Yii::$app->language])
                    ->orderBy([
                        new Expression('FIELD (job.user_id, ' . $page->user_id . ') DESC'),
                        new Expression('FIELD (job.category_id, ' . $page->category_id . ') DESC'),
                        'created_at' => SORT_DESC
                    ])
                    ->groupBy('job.id')
                    ->limit(5),
                'pagination' => false
            ]);
            (new CounterService($page, 'views', 1, 'article_views', Yii::$app->session))->process();

            return $this->render('view', [
                'model' => $page,
                'relatedDataProvider' => $relatedDataProvider,
                'jobsDataProvider' => $jobsDataProvider,
                'tendersDataProvider' => $tendersDataProvider,
                'postCommentForm' => new PostCommentForm()
            ]);
        }

        throw new NotFoundHttpException();
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $id
     * @return mixed
     */
    public function actionForm($id = null)
    {
        $this->layout = 'nomenu_layout';

        $input = Yii::$app->request->post();
        $availableLocales = Yii::$app->params['languages'];
        $isUpdate = ($id !== null && Page::find()->where(['id' => $id, 'user_id' => userId()])->exists());
        if (!$input && !$isUpdate) { // Нулевой шаг - выбор локалей
            return $this->render('locale-step', ['locales' => $availableLocales]);
        }

        $model = new PostArticleForm();
        if ($isUpdate) {
            $model->loadPage($id);
        } else {
            $model->page = new Page();
        }

        $locales = $isUpdate ? array_map(function ($var) {
            return $var->locale;
        }, $model->page->translations) : Yii::$app->request->post('locales', []);
        $locales = array_intersect($locales, array_keys($availableLocales));
        if ($locales) { // Если в посте есть локали - попадаем на первый шаг
            $rootCategoriesList = ArrayHelper::map(
                Category::find()
                    ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                    ->andWhere(['lvl' => 1])
                    ->joinWith(['translation'])
                    ->andWhere(['type' => 'job_category'])
                    ->all(),
                'id',
                'translation.title'
            );

            return $this->render('_form', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'locales' => $locales,
            ]);
        }

// Иначе попадаем на валиацию\сабмит
        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $model->validateAll();
        }

        $model->myLoad($input);

        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Article has been created!'));
            return $this->redirect($model->page->getUrl());
        }

        Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error occurred during Article creation.'));

        return $this->render('locale-step', ['locales' => $availableLocales]);
    }
}