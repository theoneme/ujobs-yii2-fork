<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use frontend\models\search\AttributeValueSearch;
use yii\web\NotFoundHttpException;

/**
 * Class AttributeSearchController
 * @package frontend\controllers
 */
class AttributeValueController extends FrontEndController
{
    /**
     * @param $alias
     * @param null $city
     * @param null $query
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSearch($alias, $city = null, $query = null)
    {
        if (in_array($alias, AttributeValueSearch::getAllowedAttributes())) {
            $attribute = Attribute::find()->joinWith(['translation'])->where(['mod_attribute.alias' => $alias])->one();
            if ($attribute !== null){
                $searchModel = new AttributeValueSearch();
                $dataProvider = $searchModel->search([
                    'attributeAlias' => $alias,
                    'valueTitle' => $query,
                ]);

                $cityModel = AttributeValue::find()->joinWith(['translations', 'relatedAttribute'])->where([
                    'mod_attribute.alias' => 'city',
                    'mod_attribute_value.alias' => $city
                ])->one();

                return $this->render('search', [
                    'attribute' => $attribute,
                    'baseUrl' => $searchModel->getBaseUrl(),
                    'attributeParam' => $searchModel->getAttributeParam(),
                    'dataProvider' => $dataProvider,
                    'city' => $cityModel,
                    'query' => $query,
                ]);
            }
        }
        throw new NotFoundHttpException();
    }
}
