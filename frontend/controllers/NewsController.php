<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.12.2016
 * Time: 10:51
 */

namespace frontend\controllers;

use common\models\Category;
use common\models\Comment;
use frontend\models\PostCommentForm;
use common\models\Page;
use yii\data\ActiveDataProvider;
use common\controllers\FrontEndController;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * Class NewsController
 * @package frontend\controllers
 */
class NewsController extends FrontEndController
{
    /**
     * @return string
     */
    public function actionList()
    {
        $category = Category::find()->joinWith(['translation'])->where(['category.id' => 141])->one();
        $this->registerMetaTags($category);

        $newsQuery = Page::find()->joinWith(['translations', 'comments', 'user'])->where(['category_id' => 141])->orderBy('publish_date desc');
        $dataProvider = new ActiveDataProvider([
            'query' => $newsQuery,
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'category' => $category
        ]);
    }

    /**
     * @param string $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        /* @var Page $page */
        $page = Page::find()
            ->joinWith(['translations', 'comments'])
            ->andWhere(['or',
                ['alias' => $alias],
                ['content_translation.slug' => $alias],
            ])
            ->one();

        if ($page !== null) {
            $this->registerMetaTags($page);

            $otherNewsQuery = Page::find()->joinWith(['translation', 'comments'])->where(['not', ['alias' => $alias]])->andWhere(['category_id' => 141])->orderBy(new Expression('rand()'))->limit(4);
            $relatedNewsQuery = Page::find()->joinWith(['translation', 'comments'])->where(['not', ['alias' => $alias]])->andWhere(['category_id' => 141])->orderBy('publish_date desc');
            $commentsQuery = Comment::find()->where(['entity' => 'comment', 'entity_id' => $page->id])->orderBy('created_at desc');

            $otherNewsDataProvider = new ActiveDataProvider(['query' => $otherNewsQuery, 'pagination' => false]);
            $relatedNewsDataProvider = new ActiveDataProvider(['query' => $relatedNewsQuery, 'pagination' => false]);
            $commentsDataProvider = new ActiveDataProvider(['query' => $commentsQuery]);

            $postCommentForm = new PostCommentForm();

            return $this->render('news', [
                'model' => $page,
                'otherNewsDataProvider' => $otherNewsDataProvider,
                'relatedNewsDataProvider' => $relatedNewsDataProvider,
                'commentsDataProvider' => $commentsDataProvider,
                'postCommentForm' => $postCommentForm
            ]);
        }

        throw new NotFoundHttpException();
    }
}