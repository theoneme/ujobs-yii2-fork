<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 03.08.2017
 * Time: 15:54
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * Class EntityController
 * @package frontend\controllers
 */
class EntityController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['create', 'tender-create', 'global-create'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['catalog'], 'roles' => ['@', '?']]
                ],
            ],
        ];
    }

    /**
     * Creates a new Job model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->render('locale-step', ['locales' => Yii::$app->params['languages']]);
    }

    /**
     * @return string
     */
    public function actionTenderCreate()
    {
        return $this->render('type-step');
    }

    /**
     * @param string $type
     * @return string
     */
    public function actionGlobalCreate($type = 'job')
    {
        return $this->render('global-step', [
            'selected' => $type,
            'locales' => Yii::$app->params['languages']
        ]);
    }

    /**
     * @return string
     */
    public function actionCatalog()
    {
        $rootJobCategories = Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Job::find()
                ->select('count(job.id)')
                ->where('job.parent_category_id=category.id')
                ->andWhere(['job.type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'job.status' => Job::STATUS_ACTIVE])
            ])
            ->joinWith(['translations'])
            ->where(['lvl' => 1, 'type' => 'job_category'])
            ->orderBy('relatedCount desc')
            ->all();
        $rootProductCategories = Category::find()
            ->select('category.*')
            ->addSelect(['relatedCount' => Product::find()
                ->select('count(product.id)')
                ->where('product.parent_category_id=category.id')
                ->andWhere(['product.type' => Product::TYPE_TENDER, 'product.status' => Product::STATUS_ACTIVE])
            ])
            ->joinWith(['translations'])
            ->where(['lvl' => 1, 'type' => 'product_category'])
            ->orderBy('relatedCount desc')
            ->all();

        $count = Job::find()->where(['type' => Job::TYPE_TENDER, 'status' => Job::STATUS_ACTIVE])->count();
        $this->view->title = Yii::t('seo', '{count, plural, one{# request} other{# requests}} | Send an application, complete the assignment and earn money | Site of professionals uJobs.me', ['count' => $count]);
        $seo = [
            'h1' => Yii::t('seo', 'Request catalog{request}{tattributes}', ['request' => '', 'tattributes' => '']),
            'subhead' => Yii::t('seo', 'Choose a task and earn money'),
            'title' => Yii::t('seo', '{count, plural, one{# request} other{# requests}} | Send an application, complete the assignment and earn money | Site of professionals uJobs.me', ['count' => $count]),
            'upperBlockHeading' => Yii::t('seo', 'Are you looking for a way to make money?'),
            'upperBlockSubheading' => Yii::t('seo', 'We created a section of requests for services and products.'),
            'upperBlockColumn1' => Yii::t('seo', 'Choose which request suits you. Offer your work or product, agree on the price.'),
            'upperBlockColumn2' => Yii::t('seo', 'Agree with the buyer how he will pay, we advise you to look at secure transaction.'),
            'upperBlockColumn3' => Yii::t('seo', 'If you choose to work through secure transaction, then we 100% guarantee that you will receive payment.'),
        ];

        $latestProductsTenders = ProductElastic::find()
            ->where(['type' => Product::TYPE_TENDER, 'status' => Product::STATUS_ACTIVE])
            ->orderBy(['lang.locale' => [
                'order' => 'asc',
                'nested_path' => 'lang',
                'nested_filter' => [
                    ['term' => ['lang.locale' => strtolower(Yii::$app->params['languages'][Yii::$app->language])]],
                ]
            ]])
            ->addOrderBy('created_at desc')
            ->limit(10);
        $latestServicesTenders = JobElastic::find()
            ->where(['type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])
            ->orderBy(['lang.locale' => [
                'order' => 'asc',
                'nested_path' => 'lang',
                'nested_filter' => [
                    ['term' => ['lang.locale' => strtolower(Yii::$app->params['languages'][Yii::$app->language])]],
                ]
            ]])
            ->addOrderBy('created_at desc')
            ->limit(10);

        $latestProductsTendersProvider = new ActiveDataProvider([
            'query' => $latestProductsTenders,
            'pagination' => false
        ]);
        $latestServicesTendersProvider = new ActiveDataProvider([
            'query' => $latestServicesTenders,
            'pagination' => false
        ]);

        return $this->render('catalog', [
            'rootJobCategories' => $rootJobCategories,
            'rootProductCategories' => $rootProductCategories,
            'seo' => $seo,
            'latestProductsTendersProvider' => $latestProductsTendersProvider,
            'latestServicesTendersProvider' => $latestServicesTendersProvider
        ]);
    }
}