<?php

namespace frontend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Class RedirectController
 * @package frontend\controllers
 */
class RedirectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'set' => ['POST'],
                    'clear' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function actionSet()
    {
        $url = Yii::$app->request->post('url');
        if ($url !== null) {
            Url::remember($url, 'returnUrl3');
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function actionClear()
    {
        Yii::$app->session->remove('returnUrl3');

        return true;
    }
}
