<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.08.2016
 * Time: 17:25
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\Event;
use common\models\Job;
use common\models\Message;
use common\models\Offer;
use common\models\Review;
use common\models\user\User;
use common\modules\store\models\Currency;
use common\modules\store\models\Order;
use common\services\CounterService;
use common\services\RememberViewService;
use frontend\models\PostRequestForm;
use frontend\models\TenderResponseForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class TenderController
 * @package frontend\controllers
 */
class TenderController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['respond', 'respond-advanced'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['view'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                    [
                        'actions' => ['update', 'delete', 'pause', 'resume'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->isAllowedToPost() && Job::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $alias
     * @return string
     */
    public function actionView($alias)
    {
        /* @var Job $model */
        $model = Job::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username, user.is_company');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio, profile.country');
            }, 'extra', 'attachments', 'translation', 'profile.translation'])
            ->where(['alias' => $alias])
            ->andWhere(['or',
                ['not', ['job.status' => Job::STATUS_DELETED]],
                ['job.user_id' => userId()]
            ])
            ->one();

        if ($model === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested job is not found'));
            return $this->redirect(['/category/job-catalog']);
        }

        $counterServiceClick = new CounterService($model, 'click_count');
        $counterServiceClick->process();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'viewed_clicks', Yii::$app->session);
        $counterServiceView->process();

        $rememberViewService = new RememberViewService($model, 'viewed', Yii::$app->session);
        $rememberViewService->saveInSession();

        $this->registerMetaTags($model);

        /* @var User $user */
        $targetUser = $model->user;
        $user = Yii::$app->user->identity;
        if ($user) {
            $accessInfo = $user->isAllowedToRespondOnTender($model);
            $contactAccessInfo = $user->isAllowedToCreateConversation($targetUser);
        } else {
            $accessInfo = ['allowed' => false];
            $contactAccessInfo = ['allowed' => false];
        }

        $breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($model->category, $model->getLabel(), '/category/tenders');

        $tenderResponseForm = new TenderResponseForm();
        $tenderResponseForm->scenario = $model->contract_price ? TenderResponseForm::SCENARIO_CONTRACT_PRICE : TenderResponseForm::SCENARIO_NOT_CONTRACT_PRICE;

        $reviewCondition = ['created_for' => $model->user_id, 'job_id' => $model->id];
        if (hasAccess($model->user_id)) {
            $reviewCondition['is_fake'] = false;
        }

        $reviewQuery = Review::find()
            ->where(['created_for' => $model->user_id, 'type' => Review::TYPE_CUSTOMER_REVIEW]);
        $reviewDataProvider = new ActiveDataProvider(['query' => $reviewQuery]);

        return $this->render('view', [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'message' => new Message(),
            'offer' => new Offer(),
            'accessInfo' => $accessInfo,
            'contactAccessInfo' => $contactAccessInfo,
            'tenderResponseForm' => $tenderResponseForm,
            'reviewDataProvider' => $reviewDataProvider
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'nomenu_layout';

        $model = new PostRequestForm(['type' => Job::TYPE_TENDER]);
        $model->job = new Job();

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            if (Yii::$app->request->isAjax) {
                $model->myLoad($input);
                Yii::$app->response->format = Response::FORMAT_JSON;

                return $model->validateAll();
            }

            if (!empty($input['id'])) {
                $model->loadJob($input['id']);
                $model->myLoad($input);
                $model->save();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Request successfully created. Thank you for posting the request!'));

                return $this->redirect(['/tender/view', 'alias' => $model->job->alias]);
            }

            $model->myLoad($input);

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Request successfully created. Thank you for posting the request!'));

                return $this->redirect(['/tender/view', 'alias' => $model->job->alias]);
            }
        }

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])
                ->all(),
            'id',
            'translation.title'
        );

        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'action' => 'create',
            'currencies' => Currency::getFormattedCurrencies()
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $this->layout = 'nomenu_layout';

        $model = new PostRequestForm();
        $model->loadJob($id);
        $input = Yii::$app->request->post();

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])
                ->all(),
            'id',
            'translation.title'
        );

        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if (!empty($input)) {
            $model->myLoad($input);

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Request successfully updated.'));

                return $this->redirect(['/tender/view', 'alias' => $model->job->alias]);
            }
        }

        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'currencies' => Currency::getFormattedCurrencies()
        ]);
    }

    /**
     * @return array
     */
    public function actionRespond()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [
            'success' => false,
            'message' => Yii::t('app', 'An error is encountered')
        ];

        if (isGuest()) {
            return $output;
        }

        $id = Yii::$app->request->post('id');
        $model = Job::findOne(['id' => (int)$id, 'type' => [Job::TYPE_ADVANCED_TENDER, Job::TYPE_TENDER]]);

        if ($model) {
            $type = $model->type == Job::TYPE_ADVANCED_TENDER ? Order::TYPE_TENDER : Order::TYPE_SIMPLE_TENDER;
            $similarOrder = Order::find()
                ->where(['order.product_type' => $type, 'seller_id' => Yii::$app->user->identity->getCurrentId(), 'customer_id' => $model->user_id])
                ->andWhere(['>', 'created_at', time() - 60 * 60 * 24 * 3])
                ->andWhere(['exists', (new Query())
                    ->select('op.product_id')
                    ->from('order_product op')
                    ->where('op.order_id = order.id')
                    ->andWhere(['op.product_id' => $model->id])
                ])
                ->one();

            if ($similarOrder !== null) {
                $output['message'] = Yii::t('app', 'You have already sent response on this request');

                return $output;
            }

            if (hasAccess($model->user_id)) {
                $output['message'] = Yii::t('app', 'You cant send response on your request');

                return $output;
            }

            $order = new Order([
                'total' => $model->price,
                'product_type' => $type,
                'cartItems' => [$model],
                'seller_id' => Yii::$app->user->identity->getCurrentId(),
                'customer_id' => $model->user_id,
                'isBlameable' => false,
                'status' => Order::STATUS_TENDER_UNAPPROVED
            ]);

            if ($order->save()) {
                Event::addEvent(Event::RESPOND_ON_TENDER);
                $output = [
                    'success' => true,
                    'message' => 'Отклик отправлен',
                    'order_id' => $order->id
                ];
            }
        }

        return $output;
    }

    /**
     * @return array
     */
    public function actionRespondAdvanced()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [
            'success' => false,
            'message' => Yii::t('app', 'An error is encountered')
        ];

        if (isGuest()) {
            return $output;
        }

        $tenderResponseForm = new TenderResponseForm();
        $input = Yii::$app->request->post();
        $tenderResponseForm->load($input);

        $model = Job::findOne(['id' => $tenderResponseForm->id, 'type' => [Job::TYPE_ADVANCED_TENDER, Job::TYPE_TENDER]]);

        if ($model && $tenderResponseForm->validate()) {
            $attachments = $tenderResponseForm->save();
            $type = $model->type === Job::TYPE_ADVANCED_TENDER ? Order::TYPE_TENDER : Order::TYPE_SIMPLE_TENDER;

            /*if (!$model->contract_price) {
                return $output;
            }*/

            $price = $tenderResponseForm->price;
            $comment = $tenderResponseForm->content;

            $similarOrder = Order::find()
                ->where(['order.product_type' => $type, 'seller_id' => Yii::$app->user->identity->getCurrentId(), 'customer_id' => $model->user_id])
                ->andWhere(['>', 'created_at', time() - 60 * 60 * 24 * 3])
                ->andWhere(['exists', (new Query())
                    ->select('op.product_id')
                    ->from('order_product op')
                    ->where('op.order_id = order.id')
                    ->andWhere(['op.product_id' => $model->id])
                ])
                ->one();

            if ($similarOrder !== null) {
                $output['message'] = Yii::t('app', 'You have already sent response on this request');

                return $output;
            }

            if (hasAccess($model->user_id)) {
                $output['message'] = Yii::t('app', 'You cant send response on your request');

                return $output;
            }

            $order = new Order([
                'total' => $model->contract_price ? $price : $model->price,
                'customComment' => $comment,
                'attachments' => $attachments,
                'is_price_approved' => $model->contract_price ? false : true,
                'product_type' => $type,
                'cartItems' => [$model],
                'seller_id' => Yii::$app->user->identity->getCurrentId(),
                'customer_id' => $model->user_id,
                'isBlameable' => false,
                'status' => $model->contract_price ? Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION : Order::STATUS_TENDER_UNAPPROVED
            ]);
            if ($model->contract_price) {
                $order->currency_code = Yii::$app->params['app_currency_code'];
            } else {
                $order->currency_code = $model->currency_code;
            }

            if ($order->save()) {
                Event::addEvent(Event::RESPOND_ON_TENDER);
                $output = [
                    'success' => true,
                    'message' => Yii::t('app', 'Response is sent'),
                    'order_id' => $order->id
                ];
            }
        }

        return $output;
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionPause($id)
    {
        $job = $this->findModel($id);
        if ($job->status === Job::STATUS_ACTIVE) {
            $job->updateAttributes(['status' => Job::STATUS_PAUSED]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionResume($id)
    {
        $job = $this->findModel($id);
        if ($job->status === Job::STATUS_PAUSED) {
            $job->updateAttributes(['status' => Job::STATUS_ACTIVE]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Job::STATUS_DELETED]);
        $model->updateElastic();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Job
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}