<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 14:52
 */

namespace frontend\controllers\user;

use dektrium\user\controllers\SettingsController as BaseSettingsController;

/**
 * Class SettingsController
 * @package frontend\controllers\user
 */
class SettingsController extends BaseSettingsController
{
    /**
     * @return string|\yii\web\Response
     */
    public function actionProfile()
    {
        return $this->redirect(['/account/service/public-profile-settings']);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAccount()
    {
        return $this->redirect(['/account/service/account-settings']);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionNetworks()
    {
        return $this->redirect(['/account/service/account-settings']);
    }
}