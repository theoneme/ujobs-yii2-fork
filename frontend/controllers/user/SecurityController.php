<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.11.2016
 * Time: 19:12
 */

namespace frontend\controllers\user;

use dektrium\user\controllers\SecurityController as BaseSecurityController;
use common\models\user\Account;
use common\models\user\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\Url;

class SecurityController extends BaseSecurityController
{
    public function beforeAction($action)
    {
        if ($action->id == 'login') {
            Url::remember(Yii::$app->user->returnUrl, 'returnUrl2');
            Yii::$app->session->set('showLoginModal', true);
        }

        return parent::beforeAction($action);
    }

    public function actionLogin()
    {
        Yii::$app->session->setFlash('warning', Yii::t('app', 'This page is only available to authorized users'));
        return $this->redirect(['/']);
    }

    /**
     * Tries to authenticate user via social network. If user has already used
     * this network's account, he will be logged in. Otherwise, it will try
     * to create new user account.
     *
     * @param ClientInterface $client
     */
    public function authenticate(ClientInterface $client)
    {
        $account = $this->finder->findAccount()->byClient($client)->one();

        if (!$this->module->enableRegistration && ($account === null || $account->user === null)) {
            \Yii::$app->session->setFlash('warning', \Yii::t('user', 'Registration on this website is disabled'));
            $this->action->successUrl = Url::to(['/']);
            return;
        }

        if ($account === null) {
            /** @var Account $account */
            $accountObj = \Yii::createObject(Account::class);
            $account = $accountObj::create($client);
        }

        $event = $this->getAuthEvent($account, $client);

        $this->trigger(self::EVENT_BEFORE_AUTHENTICATE, $event);

        if ($account->user instanceof User) {
            if ($account->user->isBlocked) {
                \Yii::$app->session->setFlash('warning', \Yii::t('user', 'Your account has been blocked.'));
                $this->action->successUrl = Url::to(['/']);
            } else {
                \Yii::$app->user->login($account->user, $this->module->rememberFor);
                $returnUrl = Url::previous('returnUrl3');
                if (!$returnUrl) {
                    $returnUrl = ['/account/service/public-profile-settings'];
                }
                $this->action->successUrl = Url::to($returnUrl);
            }
        } else {
            $this->action->successUrl = $account->getConnectUrl();
        }

        $this->trigger(self::EVENT_AFTER_AUTHENTICATE, $event);
    }
}