<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 16:11
 */

namespace frontend\controllers\user;

use common\components\Sms;
use common\models\user\LoginForm;
use common\models\user\RegisterForm;
use common\models\user\Token;
use common\models\user\User;
use dektrium\user\controllers\RegistrationController as BaseRegistrationController;
use dektrium\user\events\ConnectEvent;
use dektrium\user\Mailer;
use dektrium\user\models\Account;
use dektrium\user\models\RegistrationForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class RegistrationController
 * @package frontend\controllers\user
 */
class RegistrationController extends BaseRegistrationController
{
    const REGISTRATION_EVENT = 'registration';
    const LOGIN_EVENT = 'login';

    /**
     * Event is triggered before logging user in.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_BEFORE_LOGIN = 'beforeLogin';

    /**
     * Event is triggered after logging user in.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_AFTER_LOGIN = 'afterLogin';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->on(self::EVENT_AFTER_CONNECT, function ($e) {
            /**
             * @var $e ConnectEvent
             * @var $user User
             * @var $account Account
             */
            $user = $e->getUser();
            $account = $e->getAccount();
            $user->profile->setAvatarFromSocialNetwork($account->provider);
        });

        parent::init();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true,
                        'actions' => ['register', 'validate-ajax', 'connect'],
                        'roles' => ['?']
                    ],
                    ['allow' => true, 'actions' => ['send-confirmation'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['confirm', 'resend', 'register-ajax', 'login-ajax'], 'roles' => ['?', '@']],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id === 'login-ajax') {
            Url::remember(Yii::$app->request->referrer, 'returnUrl');
        }

        return parent::beforeAction($action);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionValidateAjax()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }
        $mode = Yii::$app->request->post('mode');

        if ($mode == self::REGISTRATION_EVENT) {
            /** @var RegistrationForm $model */
            $model = Yii::createObject(RegistrationForm::class);

            $this->performAjaxValidation($model);

        } elseif ($mode == self::LOGIN_EVENT) {
            /** @var LoginForm $model */
            $model = Yii::createObject(LoginForm::class);

            $this->performAjaxValidation($model);
        }
    }

    /**
     * Displays the registration page.
     * After successful registration if enableConfirmation is enabled shows info message otherwise redirects to home page.
     *
     * @return array|Response
     * @throws \yii\web\HttpException
     */
    public function actionRegisterAjax()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }
        /** @var RegisterForm|LoginForm $model */
        $input = Yii::$app->request->post();
        $user = User::findUserByPhoneOrEmail(trim($input['register-form']['login']));
        $isLogin = $user !== null && !empty($input['register-form']['password']) && Yii::$app->security->validatePassword($input['register-form']['password'], $user->password_hash);
        $model = $isLogin ? Yii::createObject(LoginForm::class) : new RegisterForm();
        $event = $this->getFormEvent($model);

        $this->trigger($isLogin ? self::EVENT_BEFORE_LOGIN : self::EVENT_BEFORE_REGISTER, $event);

        if (Yii::$app->request->isAjax) {
            $model->load($input, 'register-form');
            Yii::$app->response->format = Response::FORMAT_JSON;
            $errors = ActiveForm::validate($model);
            foreach ($errors as $key => $error) {
                if (mb_strpos($key, 'login-form') !== false) {
                    $errors[str_replace('login-form', 'register-form', $key)] = $error;
                    unset($errors[$key]);
                }
            }

            return $errors;
        }

        if ($model->load($input, 'register-form')) {
            if ($isLogin) {
                $returnUrl = Url::to(['/site/index']);
                if ($model->login()) {
                    $this->trigger(self::EVENT_AFTER_LOGIN, $event);
                    Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully logged in. Have a good job!'));

                    $returnUrl = Url::previous('returnUrl3');
                    if (!$returnUrl) {
                        $returnUrl = Url::previous('returnUrl2');
                    }
                    if (!$returnUrl) {
                        $returnUrl = Url::previous('returnUrl');
                    }
                    if (!$returnUrl) {
                        $returnUrl = ['/account/service/public-profile-settings'];
                    }
                }

                return $this->redirect($returnUrl);
            }

            if ($model->register()) {
                $this->trigger(self::EVENT_AFTER_REGISTER, $event);
                /* @var $loginModel LoginForm */
                $loginModel = Yii::createObject(LoginForm::class);
                $loginModel->login = $model->login;
                $loginModel->password = $model->password;
                $loginModel->rememberMe = true;
                $loginModel->login();
                $returnUrl = Url::previous('returnUrl3');
                if (!$returnUrl) {
                    $returnUrl = ['/account/service/public-profile-settings'];
                } else {
                    Yii::$app->session->set('showWelcome', true);
                }

                return $this->redirect($returnUrl);
            }

            Yii::$app->session->setFlash('warning', 'Validation failed');
        } else {
            Yii::$app->session->setFlash('warning', 'Model load failed');
        }

        return $this->redirect(['/']);
    }

    /**
     * @return array|Response
     */
    public function actionLoginAjax()
    {
        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::class);
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        $input = Yii::$app->getRequest()->post();
        $model->login = $input['login-form']['login'];
        $model->password = $input['login-form']['password'];
        $model->rememberMe = $input['login-form']['rememberMe'];

        if (Yii::$app->request->isAjax) {
            $model->load($_POST);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $returnUrl = Url::to(['/site/index']);
        if ($model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully logged in. Have a good job!'));

            $returnUrl = Url::previous('returnUrl3');
            if (!$returnUrl) {
                $returnUrl = Url::previous('returnUrl2');
            }
            if (!$returnUrl) {
                $returnUrl = Url::previous('returnUrl');
            }
            if (!$returnUrl) {
                $returnUrl = ['/account/service/public-profile-settings'];
            }
        }

        return $this->redirect($returnUrl);
    }

    /**
     * Displays page where user can create new account that will be connected to social account.
     *
     * @param string $code
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConnect($code)
    {
        $account = $this->finder->findAccount()->byCode($code)->one();

        if ($account === null || $account->getIsConnected()) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = Yii::createObject([
            'class' => User::class,
            'scenario' => 'connect',
            'username' => $account->username,
            'email' => $account->email,
        ]);

        $event = $this->getConnectEvent($account, $user);

        $this->trigger(self::EVENT_BEFORE_CONNECT, $event);

        if ($user->load(Yii::$app->request->post()) && $user->create()) {
            $account->connect($user);
            $this->trigger(self::EVENT_AFTER_CONNECT, $event);
            Yii::$app->user->login($user, $this->module->rememberFor);
            $returnUrl = Url::previous('returnUrl3');
            if (!$returnUrl) {
                $returnUrl = ['/account/service/public-profile-settings'];
            }
            return $this->redirect($returnUrl);
        }

        return $this->render('connect', [
            'model' => $user,
            'account' => $account,
        ]);
    }

    /**
     * @param int $id
     * @param string $code
     * @return Response
     */
    public function actionConfirm($id, $code)
    {
        $user = $this->finder->findUserById($id);

//        if ($user === null || $this->module->enableConfirmation == false) {
        if ($user === null) {
            //throw new NotFoundHttpException();
            Yii::$app->session->setFlash('warning', Yii::t('app', 'This account has already been verified or the verification code has already expired.'));
            return $this->redirect(['/site/index']);
        }

        $event = $this->getUserEvent($user);

        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);

        $user->attemptConfirmation($code);

        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);

        return $this->redirect(['/account/service/public-profile-settings']);
    }

    /**
     * @param $target
     * @return array
     */
    public function actionSendConfirmation($target)
    {
        /** @var Token $token */
        /* @var $mailer Sms|Mailer */
        /* @var $user User */
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [
            'success' => false,
            'message' => Yii::t('account', 'Error occurred during sending confirmation code. Please try again later.'),
            'messageType' => 'error'
        ];
        $user = Yii::$app->user->identity;
        $oldToken = Token::find()->where(['user_id' => $user->id, 'type' => Token::TYPE_CONFIRMATION])->andWhere(['>=', 'created_at', time() - 60 * 30])->one();
        if ($oldToken === null) {
            $token = new Token([
                'type' => Token::TYPE_CONFIRMATION,
                'target' => $target,
                'user_id' => $user->id
            ]);
            if ($token->save()) {
                /* @var $mailer Sms|Mailer */
                $mailer = Yii::$container->get($target == Token::TARGET_PHONE ? Sms::class : Mailer::class);
                if ($mailer->sendConfirmationMessage($user, $token)) {
                    $response = [
                        'success' => true,
                        'message' => ($target == Token::TARGET_PHONE) ? Yii::t('app', 'Sms with confirmation code has been sent to your phone') : Yii::t('app', 'Message with further instructions has been sent to your email'),
                        'messageType' => 'success'
                    ];
                } else {
                    $token->delete();
                }
            }
        } else {
            $response = [
                'success' => true,
                'message' => Yii::t('account', 'Confirmation code has already been sent. Please wait until it is delivered or try again {time}.', [
                    'time' => Yii::$app->formatter->asRelativeTime($oldToken->created_at + 60 * 30)
                ]),
                'messageType' => 'warning'
            ];
        }

        return $response;
    }
}