<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.12.2016
 * Time: 15:31
 */

namespace frontend\controllers\user;

use common\models\user\RecoveryForm;
use common\models\user\Token;
use dektrium\user\controllers\RecoveryController as BaseRecoveryController;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class RecoveryController
 * @package frontend\controllers\user
 */
class RecoveryController extends BaseRecoveryController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true,
                        'actions' => ['request-ajax', 'request', 'reset'],
                        'roles' => ['?']
                    ],
                ],
                'denyCallback' => function () {
                    Yii::$app->session->setFlash('warning', Yii::t('app', 'It seems you are already logged in. No need to restore password'));
                    if (!$returnUrl = Url::previous('returnUrl')) {
                        $returnUrl = ['/'];
                    }
                    return $this->redirect($returnUrl);
                },
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id === 'request-ajax') {
            Url::remember(Yii::$app->request->referrer, 'returnUrl');
        }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequestAjax()
    {
        if (!$this->module->enablePasswordRecovery) {
            throw new NotFoundHttpException();
        }

        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'request',
        ]);
        $event = $this->getFormEvent($model);
        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_REQUEST, $event);

        if ($model->load(Yii::$app->request->post()) && $model->sendRecoveryMessage()) {
            $this->trigger(self::EVENT_AFTER_REQUEST, $event);
        }

        if (!$returnUrl = Url::previous('returnUrl')) {
            $returnUrl = ['/'];
        }

        return $this->redirect($returnUrl);
    }

    /**
     * Shows page where user can request password recovery.
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequest()
    {
        if (!$this->module->enablePasswordRecovery) {
            throw new NotFoundHttpException();
        }

        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'request',
        ]);
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_REQUEST, $event);

        if ($model->load(Yii::$app->request->post()) && $model->sendRecoveryMessage()) {
            $this->trigger(self::EVENT_AFTER_REQUEST, $event);
            return $this->redirect(['/site/index']);
        }

        return $this->render('request', [
            'model' => $model,
        ]);
    }

    /**
     * Displays page where user can reset password.
     *
     * @param int $id
     * @param string $code
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReset($id, $code)
    {
        $this->layout = '@frontend/views/layouts/reset_layout';
        if (!$this->module->enablePasswordRecovery) {
            throw new NotFoundHttpException();
        }

        /** @var Token $token */
        $token = $this->finder->findToken(['user_id' => $id, 'code' => $code, 'type' => Token::TYPE_RECOVERY])->one();
        $event = $this->getResetPasswordEvent($token);

        $this->trigger(self::EVENT_BEFORE_TOKEN_VALIDATE, $event);

        if ($token === null || $token->isExpired || $token->user === null) {
            $this->trigger(self::EVENT_AFTER_TOKEN_VALIDATE, $event);
            Yii::$app->session->setFlash('error', Yii::t('user', 'Recovery link is invalid or expired. Please try requesting a new one.'));
            return $this->redirect(['/site/index']);
        }

        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'reset',
        ]);
        $event->setForm($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_RESET, $event);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->resetPassword($token)) {
            $this->trigger(self::EVENT_AFTER_RESET, $event);
            return $this->redirect(['/site/index']);
        }

        return $this->render('reset', [
            'model' => $model,
        ]);
    }
}
