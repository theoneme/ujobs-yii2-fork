<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.08.2016
 * Time: 13:46
 */

namespace frontend\controllers\user;

use dektrium\user\controllers\ProfileController as BaseProfileController;

/**
 * Class ProfileController
 * @package frontend\controllers\user
 */
class ProfileController extends BaseProfileController
{
    /**
     * @param int $id
     * @return \yii\web\Response
     */
    public function actionShow($id)
    {
        return $this->redirect(['/account/profile/show', 'id' => $id]);
    }
}