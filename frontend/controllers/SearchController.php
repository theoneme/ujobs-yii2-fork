<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 19.04.2017
 * Time: 16:14
 */

namespace frontend\controllers;

use common\components\CurrencyHelper;
use common\controllers\FrontEndController;
use common\helpers\ElasticConditionsHelper;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use frontend\modules\elasticfilter\components\SearchFilter;
use frontend\modules\elasticfilter\components\SearchProduct;
use frontend\modules\elasticfilter\models\FilterSearchJob;
use frontend\modules\elasticfilter\models\FilterSearchProduct;
use frontend\modules\elasticfilter\widgets\EntitySwitcherWidget;
use Yii;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * Class SearchController
 * @package frontend\controllers
 */
class SearchController extends FrontEndController
{
    /**
     * @param null $entity
     * @return string|\yii\web\Response
     */
    public function actionSearch($entity = null)
    {
        $request = Yii::$app->request->get('request');

        if (!empty($request)) {
            $request = HtmlPurifier::process($request);

            $jobsCount = JobElastic::find()
                ->where([
                    'type' => Job::TYPE_JOB,
                    'status' => Job::STATUS_ACTIVE,
                ])
                ->query([
                    'bool' => [
                        'must' => [
                            'bool' => [
                                'should' => ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $request)
                            ]
                        ]
                    ]
                ])
                ->count();

            $productsCount = ProductElastic::find()
                ->where([
                    'type' => Product::TYPE_PRODUCT,
                    'status' => Product::STATUS_ACTIVE,
                ])
                ->query([
                    'bool' => [
                        'must' => [
                            'bool' => [
                                'should' => ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $request)
                            ]
                        ]
                    ]
                ])
                ->count();

            if ($entity === null) {
                $entity = $productsCount > $jobsCount ? 'product' : 'job';
            }

            switch ($entity) {
                case 'product':
                    $items = ProductElastic::find()
                        ->where([
                            'type' => Product::TYPE_PRODUCT,
                            'status' => Product::STATUS_ACTIVE
                        ]);
                    $filterClass = SearchProduct::class;
                    $modelClass = FilterSearchProduct::class;
                    $filterView = 'filter-product';
                    $entity = Product::TYPE_PRODUCT;

                    $seo = [
//                        'h1' => Yii::t('seo', 'Search by request {request}', ['request' => $request]),
                        'h1' => $request,
                        'subhead' => '',
                        'upperBlockHeading' => Yii::t('seo', 'Looking for products related to {request}?', ['request' => $request]),
                        'upperBlockSubheading' => Yii::t('seo', 'Search results by request {request}', ['request' => $request]),
                        'upperBlockColumn1' => Yii::t('seo', 'On our site you can find product {request}', ['request' => $request]),
                        'upperBlockColumn2' => Yii::t('seo', 'Make an order in few simple steps'),
                        'upperBlockColumn3' => Yii::t('seo', 'Seller will get money just after you receive product {request}', ['request' => $request])
                    ];

                    break;
                case 'job':
                default:
                    $items = JobElastic::find()
                        ->where([
                            'type' => Job::TYPE_JOB,
                            'status' => Job::STATUS_ACTIVE,
                        ]);
                    $filterClass = SearchFilter::class;
                    $modelClass = FilterSearchJob::class;
                    $entity = Job::TYPE_JOB;
                    $filterView = 'filter';

                    $seo = [
                        'h1' => Yii::t('seo', 'Search by request {request}', ['request' => $request]),
                        'subhead' => '',
                        'upperBlockHeading' => Yii::t('seo', 'Looking for services related to {request}?', ['request' => $request]),
                        'upperBlockSubheading' => Yii::t('seo', 'Search results by request {request}', ['request' => $request]),
                        'upperBlockColumn1' => Yii::t('seo', 'On our site you can choose pro by {request}', ['request' => $request]),
                        'upperBlockColumn2' => Yii::t('seo', 'Make an order in few simple steps, provide required details, and chosen professional will get to work'),
                        'upperBlockColumn3' => Yii::t('seo', 'Worker will get money just after order {request} is completed and accepted by customer', ['request' => $request])
                    ];

                    break;
            }

            $filter = new $filterClass([
                'query' => $items,
                'type' => $entity,
                'filterClass' => $modelClass,
                'extra' => EntitySwitcherWidget::widget(['jobsCount' => $jobsCount, 'productsCount' => $productsCount, 'entity' => $entity, 'request' => $request])
            ]);
            if (count($filter->dataProvider->models)) {
                $minPrice = CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], min(array_column($filter->dataProvider->models, 'default_price')));
            }
            $seoParams = [
                'request' => Yii::$app->utility->upperFirstLetter($request),
                'count' => max($productsCount, $jobsCount),
                'minPrice' => isset($minPrice) ? " | " . Yii::t('seo', 'Price from {minPrice}', ['minPrice' => $minPrice]) : ''
            ];

            $title = Yii::t('seo', '{request} | {count} Offers{minPrice}', $seoParams);
            $description = Yii::t('seo', 'We are offering {count} options for your request: «{request}». Choose verified professionals on uJobs.me.', $seoParams);
            $keywords = $request;

            Yii::$app->opengraph->description = $description;
            $this->view->title = Yii::$app->opengraph->title = $title;
            $this->view->registerMetaTag(['name' => 'description', 'content' => Html::encode($description)], 'description');
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => Html::encode($keywords)], 'keywords');

            return $this->render('search', [
                'seo' => $seo,
                'filter' => $filter,
                'filterView' => $filterView,
                'request' => $request
            ]);
        }

        return $this->redirect(['/category/job-catalog']);
    }
}