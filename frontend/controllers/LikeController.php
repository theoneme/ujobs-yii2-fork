<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.09.2017
 * Time: 10:17
 */

namespace frontend\controllers;

use common\models\Like;
use Yii;
use common\controllers\FrontEndController;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class LikeController
 * @package frontend\controllers
 */
class LikeController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['like-ajax'], 'roles' => ['@']],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['like-ajax'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionLikeAjax()
    {
        $input = Yii::$app->request->post();

        if(!empty($input)) {
            $like = Like::find()->where(['entity' => $input['entity'], 'entity_id' => $input['entity_id'], 'user_id' => Yii::$app->user->identity->getId()])->one();
            if($like !== null) {
                $like->delete();

                return [
                    'success' => true,
                    'action' => 'delete'
                ];
            }

            $like = new Like([
                'entity' => $input['entity'],
                'entity_id' => $input['entity_id'],
            ]);
            if($like->save()) {
                return [
                    'success' => true,
                    'action' => 'create'
                ];
            }
        }

        return [
            'success' => false
        ];
    }
}