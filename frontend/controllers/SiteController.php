<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\elastic\JobElastic;
use common\models\elastic\ProfileElastic;
use common\models\Job;
use common\models\Page;
use common\models\user\Profile;
use common\models\user\User;
use frontend\models\ContactForm;
use frontend\models\IndexSearchForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;


/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id === 'index' && preg_match('/^https:\/\/ujobs\.ru.*$/u', Yii::$app->request->referrer)) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $page = Page::find()
            ->select('page.id, page.alias, page.publish_date, page.category_id, page.image')
            ->joinWith(['translation' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('content_translation.id, content_translation.entity, content_translation.entity_id, content_translation.title');
            }], false)
            ->where(['page.alias' => 'index'])
            ->one();

        $this->registerMetaTags($page);

        $indexCache = Yii::$app->cacheLayer->get('index_cache');

        $jobs = JobElastic::find()
            ->where(['type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE, 'id' => $indexCache[Yii::$app->language]['jobs']])
            ->limit(5);

        $tenders = JobElastic::find()
            ->where(['type' => [Job::TYPE_ADVANCED_TENDER, Job::TYPE_TENDER], 'status' => Job::STATUS_ACTIVE, 'id' => $indexCache[Yii::$app->language]['tenders']])
            ->limit(5);

        $users = ProfileElastic::find()
            ->andWhere(['user_id' => $indexCache[Yii::$app->language]['users']])
            ->limit(5);

        $news = Page::find()->select('page.id, page.alias, page.publish_date, page.category_id, page.image')->joinWith(['translation' => function ($q) {
            /* @var ActiveQuery $q */
            $q->select('content_translation.id, content_translation.entity, content_translation.entity_id, content_translation.title, content_translation.content');
        }])->where(['category_id' => 141])->limit(4)->orderBy('publish_date desc');

        $jobsDataProvider = new ActiveDataProvider(['query' => $jobs, 'pagination' => false]);
        $tendersDataProvider = new ActiveDataProvider(['query' => $tenders, 'pagination' => false]);
        $usersDataProvider = new ActiveDataProvider(['query' => $users, 'pagination' => false]);
        $newsDataProvider = new ActiveDataProvider(['query' => $news, 'pagination' => false]);

        $searchModel = new IndexSearchForm();
        $showLoginModal = Yii::$app->session->has('showLoginModal') || Yii::$app->request->post('showLoginModal');
        $showSignupModal = Yii::$app->request->post('showSignupModal') || Yii::$app->request->get('showSignupModal') || Yii::$app->request->cookies->has('client_id');
        Yii::$app->session->remove('showLoginModal');
        if (Yii::$app->response->cookies->has('invited_by')) {
            Yii::$app->opengraph->title = Yii::t('app', 'Become a member of the affiliate program and get 5% of each transaction');
            Yii::$app->opengraph->description = Yii::t('app', 'You can participate in the affiliate program uJobs.me, and earn 5% income from all the operations that your invitees will carry during the year.');
        }

        return $this->render('index', [
            'jobsDataProvider' => $jobsDataProvider,
            'tendersDataProvider' => $tendersDataProvider,
            'usersDataProvider' => $usersDataProvider,
            'newsDataProvider' => $newsDataProvider,
            'searchModel' => $searchModel,
            'showLoginModal' => $showLoginModal,
            'showSignupModal' => $showSignupModal,
            'indexCache' => $indexCache
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $page = Page::find()->joinWith(['translation'])->where(['page.alias' => 'contact'])->one();
        $this->registerMetaTags($page);

        $searchModel = new IndexSearchForm();
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!isGuest()) {
                $model->user = Yii::$app->user->identity;
            }

            $model->submit();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for contacting us, we will answer you as soon as possible.'));

            return $this->refresh();
        }

        return $this->render('@frontend/views/page/' . $page->template, [
            'model' => $model,
            'searchModel' => $searchModel,
            'page' => $page
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionSupport()
    {
        $page = Page::find()->joinWith(['translation'])->where(['page.alias' => 'support'])->one();
        $this->registerMetaTags($page);

        $searchModel = new IndexSearchForm();
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->submit();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for contacting us, we will answer you as soon as possible.'));
            return $this->refresh();
        }

        return $this->render($page->template, [
            'model' => $model,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionUnsubscribe()
    {
        $input = Yii::$app->request->get();

        /* @var User $user */
        $user = User::find()->where(['created_at' => $input['created_at'], 'id' => $input['id'], 'updated_at' => $input['updated_at']])->one();
        if ($user) {
            /* @var Profile $profile */
            $profile = $user->profile;

            $profile->updateAttributes(['email_notifications' => false]);
            Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully unsubscribed from email notifications'));
        }

        return $this->redirect(['/site/index']);
    }
}
