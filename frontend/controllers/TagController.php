<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Job;
use common\models\JobAttribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\db\Query;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class TagController
 * @package frontend\controllers
 */
class TagController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['list', 'product'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Lists all tags for ajax queries.
     * @return mixed
     */
    public function actionList()
    {
        $query = Yii::$app->request->post('query');
        $tags = JobAttribute::find()
            ->select(['id', 'value', 'count' => 'count(id)'])
            ->where(['entity_alias' => 'tag'])
            ->andWhere(['like', 'value', urldecode($query)])
            ->groupBy('value_alias')
            ->having(['>=', 'count', 3])
            ->asArray()
            ->all();
        return $tags;
    }

    /**
     * Lists all tags for ajax queries.
     * @return mixed
     */
    public function actionProduct()
    {
        $query = Yii::$app->request->post('query');
        $tags = ProductAttribute::find()
            ->select(['id', 'value', 'count' => 'count(id)'])
            ->where(['entity_alias' => 'product_tag'])
            ->andWhere(['like', 'value', urldecode($query)])
            ->groupBy('value_alias')
            ->having(['>=', 'count', 3])
            ->asArray()
            ->all();

        return $tags;
    }
}
