<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\Job;
use common\models\Page;
use common\models\user\Profile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends FrontEndController
{
    /**
     * @param null $alias
     * @return string|\yii\web\Response
     */
    public function actionStatic($alias = null)
    {
        $parentCategories = [];

        /* @var Page $page */
        $page = Page::find()->joinWith(['translations', 'category'])->where(['page.alias' => $alias])->one();

        if ($page) {
            if ($page->category) {
                $parentCategories = $page->category->getParents();
            }

            $breadcrumbs = Yii::$app->utility->buildPageBreadcrumbs($parentCategories, $page);
            $this->registerMetaTags($page);

            return $this->render($page->template, [
                'page' => $page,
                'breadcrumbs' => $breadcrumbs
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));
        return $this->redirect(['/']);
    }

    /**
     * @return string|Response
     */
    public function actionTariffs()
    {
        /* @var Page $page */
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'tariffs'])->one();

        if ($page) {
            $this->registerMetaTags($page);

            return $this->render('tariff', ['page' => $page]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));
        return $this->redirect(['/']);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionStartSelling()
    {
        $this->layout = '@frontend/modules/account/views/layouts/start_selling';
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'start-selling-landing'])->one();
        $this->registerMetaTags($page);

        $profiles = Profile::find()
            ->joinWith(['user', 'jobs', 'translation'])
            ->where(['exists', Job::find()
                ->select('job.id')
                ->where('job.user_id = user.id')
                ->andWhere(['job.type' => 'job'])
                ->andWhere(['job.status' => Job::STATUS_ACTIVE])
            ])
            ->andWhere(['not', ['gravatar_email' => '']])
            ->groupBy('user.id')
            ->orderBy(new Expression('rand()'))
            ->limit(7);
        $buyers = Profile::find()
            ->joinWith(['user', 'translation'])
            ->where(['or',
                ['profile.status' => Profile::STATUS_ACTIVE],
                ['exists', Job::find()
                    ->select('job.id')
                    ->where('job.user_id = user.id')
                    ->andWhere(['job.type' => 'job'])
                    ->andWhere(['job.status' => Job::STATUS_ACTIVE])
                ]
            ])
            ->andWhere(['and',
                ['not', ['pct.title' => null]],
                ['not', ['pct.title' => '']],
                ['not', ['pct.content' => null]],
                ['not', ['pct.content' => '']],
                ['profile.status' => Profile::STATUS_ACTIVE]
            ])
            ->andWhere(['not', ['gravatar_email' => '']])
            ->groupBy('user.id')
            ->orderBy(new Expression('rand()'))
            ->limit(4);
        $profilesDataProvider = new ActiveDataProvider(['query' => $profiles, 'pagination' => false]);
        $buyersDataProvider = new ActiveDataProvider(['query' => $buyers, 'pagination' => false]);

        $cheapest = Job::find()->where(['type' => 'job', 'status' => Job::STATUS_ACTIVE, 'currency_code' => 'RUB'])->andWhere(['>', 'price', 250])->orderBy('price asc')->one();
        $mostExpensive = Job::find()->where(['type' => 'job', 'status' => Job::STATUS_ACTIVE, 'currency_code' => 'RUB'])->andWhere(['<', 'price', 60000])->orderBy('price desc')->one();
        return $this->render('start_selling', [
            'page' => $page,
            'profilesDataProvider' => $profilesDataProvider,
            'buyersDataProvider' => $buyersDataProvider,

            'cheapest' => $cheapest,
            'mostExpensive' => $mostExpensive
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionBecomeFreelancer()
    {
        $this->layout = '@frontend/modules/account/views/layouts/start_selling';
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'become-freelancer-landing'])->one();
        $this->registerMetaTags($page);

        $profiles = Profile::find()
            ->joinWith(['user', 'jobs', 'translation'])
            ->where(['exists', Job::find()
                ->select('job.id')
                ->where('job.user_id = user.id')
                ->andWhere(['job.type' => 'job'])
                ->andWhere(['job.status' => Job::STATUS_ACTIVE])
            ])
            ->andWhere(['not', ['gravatar_email' => '']])
            ->groupBy('user.id')
            ->orderBy(new Expression('rand()'))
            ->limit(7);
        $buyers = Profile::find()
            ->joinWith(['user', 'translation'])
            ->where(['or',
                ['profile.status' => Profile::STATUS_ACTIVE],
                ['exists', Job::find()
                    ->select('job.id')
                    ->where('job.user_id = user.id')
                    ->andWhere(['job.type' => 'job'])
                    ->andWhere(['job.status' => Job::STATUS_ACTIVE])
                ]
            ])
            ->andWhere(['and',
                ['not', ['pct.title' => null]],
                ['not', ['pct.title' => '']],
                ['not', ['pct.content' => null]],
                ['not', ['pct.content' => '']],
                ['profile.status' => Profile::STATUS_ACTIVE]
            ])
            ->andWhere(['not', ['gravatar_email' => '']])
            ->groupBy('user.id')
            ->orderBy(new Expression('rand()'))
            ->limit(4);
        $profilesDataProvider = new ActiveDataProvider(['query' => $profiles, 'pagination' => false]);
        $buyersDataProvider = new ActiveDataProvider(['query' => $buyers, 'pagination' => false]);

        $cheapest = Job::find()->where(['type' => 'job', 'status' => Job::STATUS_ACTIVE, 'currency_code' => 'RUB'])->andWhere(['>', 'price', 250])->orderBy('price asc')->one();
        $mostExpensive = Job::find()->where(['type' => 'job', 'status' => Job::STATUS_ACTIVE, 'currency_code' => 'RUB'])->andWhere(['<', 'price', 60000])->orderBy('price desc')->one();
        return $this->render('become_freelancer', [
            'page' => $page,
            'profilesDataProvider' => $profilesDataProvider,
            'buyersDataProvider' => $buyersDataProvider,

            'cheapest' => $cheapest,
            'mostExpensive' => $mostExpensive
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionOutsourceMpp()
    {
        $this->layout = '@frontend/modules/account/views/layouts/start_selling';
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'outsource-mpp'])->one();
        Url::remember(Url::to(['/entity/global-create', 'type' => 'tender-service']), 'returnUrl3');
        $this->registerMetaTags($page);

        return $this->render('outsource_mpp', [
            'page' => $page
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionFreeService()
    {
        $this->layout = '@frontend/views/layouts/new_landing';
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'poluchi-odnu-iz-uslug-besplatno'])->one();
        Url::remember(Url::to(['/account/service/public-profile-settings']), 'returnUrl3');
        $this->registerMetaTags($page);

        return $this->render('new-land', [
            'page' => $page
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSvoeDelo()
    {
        $this->layout = '@frontend/views/layouts/svoe-delo';
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'svoe-delo'])->one();
        Url::remember(Url::to(['/account/service/public-profile-settings']), 'returnUrl3');
        $this->registerMetaTags($page);

        return $this->render('svojo-delo', [
            'page' => $page
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionOutsourceMarketing()
    {
        $this->layout = '@frontend/modules/account/views/layouts/start_selling';
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'smm-marketing-outsource'])->one();
        Url::remember(Url::to(['/entity/global-create', 'type' => 'tender-service']), 'returnUrl3');
        $this->registerMetaTags($page);

        return $this->render('outsource_mpp', [
            'page' => $page
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionProductLanding()
    {
        $page = Page::find()->joinWith(['translations'])->where(['page.alias' => 'product-landing'])->one();
        Url::remember(Url::to(['/entity/global-create', 'type' => 'product']), 'returnUrl3');
        $this->registerMetaTags($page);

        $categories = Category::find()->joinWith(['translations'])->where(['type' => 'product_category', 'lvl' => 1, 'banner_size' => 2])->limit(8)->groupBy('category.id')->all();

        return $this->render('product-landing', [
            'page' => $page,
            'categories' => $categories
        ]);
    }
}
