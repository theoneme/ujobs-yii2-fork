<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'store', 'livechat', 'board', 'attribute'],
    'name' => 'uJobs',
    'modules' => [
        'treemanager' =>  [
            'class' => 'kartik\tree\Module',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'Account' => 'common\models\user\Account',
                'User' => 'common\models\user\User',
                'Profile' => 'common\models\user\Profile',
                'UserSearch' => 'backend\models\search\UserSearch'
            ],
            'controllerMap' => [
                'admin' => 'backend\controllers\user\AdminController',
                'security' => 'backend\controllers\user\SecurityController',
            ],
            'mailer' => [
                'sender' => ['support@ujobs.me' => 'Команда uJobs'],
                'viewPath' => '@frontend/views/email'
            ],
            'admins' => ['devour', 'ujobs-support', 'warden'],
            'enableConfirmation' => false,
            'enableRegistration' => false,
            'rememberFor' => 252000,
        ],
        'attribute' => [
            'class' => 'common\modules\attribute\Module',
        ],
        'formbuilder' => [
            'class' => 'common\modules\form\Module',
        ],
        'filter' => [
            'class' => 'common\modules\filter\Module',
        ],
        'store' => [
            'class' => 'common\modules\store\Module',
        ],
	    'livechat' => [
		    'class' => 'common\modules\livechat\Module'
	    ],
        'board' => [
            'class' => 'common\modules\board\Module'
        ],
        'wizard' => [
            'class' => 'common\modules\wizard\Module'
        ]
    ],
    'components' => [
        'user' => [
            'identityCookie' => [
                'name' => '_backendUser',
                'path' => '/'
            ],
            'authTimeout' => 60 * 60 * 24 * 10,
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'utility' => [
            'class' => 'common\components\Utility',
        ],
        'cart' => [
            'class' => 'common\modules\store\components\Cart'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'js/jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => [
                'httponly' => true,
                'lifetime' => 3600 * 8,
            ],
            'timeout' => 3600 * 24 * 30,
            'useCookies' => true,
            'name' => 'backendSession'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/views/user'
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'labels*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'labels' => 'labels.php',
                    ],
                ],
                'order*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'order' => 'order.php',
                    ],
                ],
                'filter*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'fileMap' => [
                        'formbuilder' => 'filter.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'account*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'account' => 'account.php',
                    ],
                ],
                'notifications*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'notifications' => 'notifications.php',
                    ],
                ],
            ],
        ],
//        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'suffix' => '',
//            'rules' => [
//                '<controller:\w+>/<id:\d+>' => '<controller>/view',
//                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                //'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>'
//            ],
//        ],
        'urlManagerFrontEnd' => [
            'ruleConfig' => [
                'class' => 'frontend\components\LanguageUrlRule'
            ],
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'baseUrl' => 'https://ujobs.me',
            'rules' => [
                'conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/start-conversation',
                'conversation/<id:\d+>' => 'account/inbox/conversation',

                'support-conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/start-conversation',
                'support-conversation/<id:\d+>' => 'account/inbox/support-conversation',

                '<specialty:[\w-+]+>-specialty-tenders' => 'category/specialty-tender',
                '<specialty:[\w-+]+>-specialty-pros' => 'category/specialty-pro',

                '<alias:[\w-]+>-<action:(tender|job|video)>' => '<action>/view',
                '<category_1:[\w-]+>-<action:(category|jobs|tenders|pros)>' => 'category/<action>',

                '<tag:[\w-+]+>-usluga' => 'category/tag',
                '<skill:[\w-+]+>-skill' => 'category/skill',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
	    'prettyUrlManager' => [
            'ruleConfig' => [
                'class' => 'frontend\components\LanguageUrlRule'
            ],
		    'class' => 'yii\web\urlManager',
		    'enablePrettyUrl' => true,
		    'showScriptName' => false,
		    'suffix' => '',
		    'baseUrl' => '',
		    'rules' => [
			    'conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/conversation',
			    'conversation/<user_id:\d+>' => 'account/inbox/conversation',

			    'support-conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/support-conversation',
			    'support-conversation/<user_id:\d+>' => 'account/inbox/support-conversation',

			    '<specialty:[\w-+]+>-specialty-tenders' => 'category/specialty-tender',
			    '<specialty:[\w-+]+>-specialty-pros' => 'category/specialty-pro',

			    '<tag:[\w-+]+>-usluga' => 'category/tag',
			    '<skill:[\w-+]+>-skill' => 'category/skill',

			    '<alias:[\w-]+>-<action:(tender|job|video)>' => '<action>/view',

			    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			    '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
			    '<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
		    ],
	    ],
    ],
    'params' => $params,
];
