<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.02.2018
 * Time: 13:33
 */

namespace backend\dto;

use common\controllers\BackEndController;
use common\models\JobPackage;
use common\models\user\Profile;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class MonitorDTO
 * @package backend\dto
 */
class MonitorDTO
{
    /**
     * @var array
     */
    private $_status;

    /**
     * MonitorDTO constructor.
     * @param array $status
     */
    public function __construct(array $status)
    {
        $this->_status = $status;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if(!empty($this->_status)) {
            return [
                'state' => $this->_status['Slave_IO_State'],
                'master_host' => $this->_status['Master_Host'],
                'slave_io_running' => $this->_status['Slave_IO_Running'],
                'slave_sql_running' => $this->_status['Slave_SQL_Running'],
                'relay_log_file' => $this->_status['Relay_Log_File'],
                'relay_log_pos' => $this->_status['Relay_Log_Pos'],
            ];
        }

        return [];
    }
}
