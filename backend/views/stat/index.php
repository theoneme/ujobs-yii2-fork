<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.01.2017
 * Time: 15:34
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

/* @var ActiveDataProvider $provider */
/* @var integer $month */
/* @var integer $year */
echo $month;
$this->title = Yii::t('app', 'Stats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <div>
                <?php \yii\widgets\Pjax::begin(['id' => 'stat-pjax']); ?>
                <?= Html::beginForm(['/stat/index'], 'get', ['id' => 'stat-form', 'data-pjax' => '']) ?>
                <div>
                    <div style="float:right" class="col-md-3">
                        <label>Год</label>
                        <?= Html::dropDownList('year', Yii::$app->request->get('year', $year), [
                            array_combine(range(date('Y'), 2016), range(date('Y'), 2016))
                        ], [
                            'class' => 'form-control stat-trigger'
                        ]) ?>
                    </div>
                    <div style="float:right" class="col-md-3">
                        <label>Месяц</label>
                        <?= Html::dropDownList('month', Yii::$app->request->get('month', 3), [
                            array_combine(range(1, 12), range(1, 12))
                        ], [
                            'class' => 'form-control stat-trigger'
                        ]) ?>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $provider,
                        'id' => 'users',
                        'columns' => [
                            [
                                'header' => 'День',
                                'value' => function ($model) use ($year, $month){
                                    return Yii::$app->formatter->asDate("{$year}-{$month}-{$model['id']}", 'long');;
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Итого (Юзеры)',
                                'value' => function ($model) {
                                    return $model['totalUsers'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Новые (Юзеры)',
                                'value' => function ($model) {
                                    return $model['currentDayUsers'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => '% прироста (Юзеры)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::tag('strong', $model['usersPercent'] . ' %', ['class' => $model['usersPercent'] > 0 ? 'text-success' : 'text-danger']);
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'В каталоге (Юзеры)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model['activeUsers'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Новые в  каталоге (Юзеры)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model['activeCurrentDayUsers'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => '% прироста  в каталоге (Юзеры)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::tag('strong', $model['activeUsersPercent'] . ' %', ['class' => $model['activeUsersPercent'] > 0 ? 'text-success' : 'text-danger']);
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Итого (Работы)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model['totalJobs'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Новые (Работы)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model['currentDayJobs'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => '% прироста (Работы)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::tag('strong', $model['jobsPercent'] . ' %', ['class' => $model['jobsPercent'] > 0 ? 'text-success' : 'text-danger']);
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Итого (Товары)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model['totalProducts'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Новые (Товары)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model['currentDayProducts'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => '% прироста (Товары)',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::tag('strong', $model['productsPercent'] . ' %', ['class' => $model['productsPercent'] > 0 ? 'text-success' : 'text-danger']);
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                        ],
                    ]); ?>
                    <?= Html::endForm() ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

<?php $this->registerJs('
    $(document).on("change", ".stat-trigger", function() {
        $("#stat-form").submit();
    });
');?>