<?php

use common\models\Invoice;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="invoice-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'payment_id',
                    [
                        'header' => 'Пользователь',
                        'attribute' => 'username',
                        'value' => function ($model) {
                            return Html::a($model->user->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->user->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->created_at) . " (" . Yii::$app->formatter->format($model->created_at, 'relativeTime') . ")";
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->updated_at) . " (" . Yii::$app->formatter->format($model->updated_at, 'relativeTime') . ")";
                        }
                    ],
                    [
                        'attribute' => 'type',
                        'filter' => [
                            Invoice::TYPE_LEGAL_ENTITY => Yii::t('account', 'Legal entity'),
                            Invoice::TYPE_INDIVIDUAL => Yii::t('account', 'Individual'),
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Invoice */
                            return $model->getTypeLabel(true);
                        },
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => [
                            Invoice::STATUS_WAITING => Yii::t('account', 'Waiting'),
                            Invoice::STATUS_DECLINED => Yii::t('account', 'Declined'),
                            Invoice::STATUS_APPROVED => Yii::t('account', 'Approved'),
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Invoice */
                            return $model->getStatusLabel(true);
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
