<?php

use common\models\Invoice;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="alert alert-info">
        <?= $model->user->getSellerName() . '(' . $model->getTypeLabel(false) . ')' ?>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'cost')->textInput(['disabled' => 'disabled']) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'currency_code')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
        </div>
    </div>

    <div class="clearfix">
        <?= $form->field($model, 'status')->dropDownList([
            Invoice::STATUS_WAITING => 'Ожидает',
            Invoice::STATUS_DECLINED => 'Отклонен',
            Invoice::STATUS_APPROVED => 'Одобрен',
        ]) ?>
    </div>

    <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i> ' . basename($model->invoice_file),
        Yii::$app->urlManagerFrontEnd->createUrl($model->invoice_file, true), [
            //'download' => true,
            'data-pjax' => 0
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
