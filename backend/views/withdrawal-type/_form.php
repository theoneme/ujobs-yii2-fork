<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var $type integer
 * @var $label string
 * @var $countries array
 */

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'withdrawal-type-form',
        'data-pjax' => false,
        'class' => 'ajax-submit',
    ],
]); ?>
    <?= Html::hiddenInput("type", $type) ?>
    <h2><?= $label?></h2>
    <div class="form-group selectize-input-container" style="margin-bottom: 50px;">
        <?= Html::label('Показывать в странах:', null, ['style' => 'margin: 0;'])?>
        <h6>(Если нужно показывать во всех странах - оставьте поле пустым)</h6>
        <?= Html::textarea("countries", null, ['id' => "countries-input"]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$countryListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'country']);
$options = json_encode(array_map(
    function($value, $title) {return ['id' => $value, 'value' => $title];},
    array_keys($countries), $countries
));
$values = json_encode(array_keys($countries));
$script = <<<JS
    $.pjax.defaults.timeout = 6000;
    
    $('#countries-input').selectize({
        options: $options,
        items: $values,
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        maxItems: null,
        plugins: ["remove_button"],
        persist: false,
        'load': function(query, callback) {
            if (!query.length)
                return callback();
            $.post('$countryListUrl', {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script);