<?php

use backend\assets\SelectizeAsset;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

SelectizeAsset::register($this);
$this->title = 'Типы вывода денег';

?>
<div class="job-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(['id' => 'grid-container']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'label',
                            'header' => 'Тип вывода денег'
                        ],
                        [
                            'attribute' => 'countries',
                            'header' => 'Показывать в странах'
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => '',
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('app', 'Update {0}', 'Wallet type'),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]);
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                switch ($action) {
                                    case 'update':
                                        $url = Url::to(["/withdrawal-type/update", 'type' => $model['type']]);
                                        break;
                                }
                                return $url;
                            }
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>