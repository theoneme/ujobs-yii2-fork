<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.12.2016
 * Time: 16:34
 */

use yii\helpers\Html;
use common\models\JobQa;

/* @var JobQa $model */
/* @var integer $iterator */

?>

    <div id="faq-block-<?= $iterator ?>">
        <div id="faq-edit-<?= $iterator ?>" style="display: none">
            <div class="faqedit-block">
                <div class="feb-head">
                </div>
                <div class="feb-body">
                </div>
            </div>
        </div>
        <div id="faq-add-<?= $iterator ?>">
            <div id="container-question-<?= $iterator ?>">
                <?= Html::activeTextInput($model, "[{$iterator}]question", [
                    'placeholder' => Yii::t('app', 'Add a Question: i.e. Do you translate to English as well?'),
                    'class' => 'form-control'
                ]) ?>
                <div class="help-block"></div>
            </div>

            <div id="container-answer-<?= $iterator ?>">
                <?= Html::activeTextarea($model, "[{$iterator}]answer", [
                    'placeholder' => Yii::t('app', 'Add an Answer: i.e. Yes, I also translate from English to Hebrew.'),
                    'class' => 'readsym form-control'
                ]) ?>
                <div class="row"><div class="counttext"><span class="ctspan">0</span> / 800 <?= Yii::t('app', 'Max') ?></div></div>
                <div class="help-block"></div>
            </div>

            <div class="row faq-buttons text-right">
                <?= Html::a(Yii::t('app', 'Cancel'), '#', ['class' => 'btn btn-sm btn-warning closeeditt', 'data-new' => 1, 'data-target' => "faq-block-{$iterator}", 'data-key' => $iterator]) ?>
                <?= Html::button(Yii::t('app', 'Save'), ['data-target' => "faq-block-{$iterator}", 'class' => 'btn btn-success btn-sm process-faq', 'data-key' => $iterator]) ?>
            </div>
        </div>
    </div>

<?php $this->registerJs('
    $("#job-form").yiiActiveForm("add", {
        id: "jobqa-' . $iterator . '-question",
        name: "JobQa[' . $iterator . '][question]",
        input: "#jobqa-' . $iterator . '-question",
        container: "#container-question-' . $iterator . '",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred, $form) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Question")]) . '",
            });
            yii.validation.string(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Question")]) . '",
                "max": 300,
                "tooLong": "' . Yii::t("yii", "{attribute} must be no greater than {max}.", ["attribute" => Yii::t("model", "Question"), "max" => 300]) . '",
            });
        }
    });
    $("#job-form").yiiActiveForm("add", {
        id: "jobqa-' . $iterator . '-answer",
        name: "JobQa[' . $iterator . '][answer]",
        input: "#jobqa-' . $iterator . '-answer",
        container: "#container-answer-' . $iterator . '",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred, $form) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Answer")]) . '"
            });
            yii.validation.string(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Answer")]) . '",
                "max": 800,
                "tooLong": "' . Yii::t("yii", "{attribute} must be no greater than {max}.", ["attribute" => Yii::t("model", "Answer"), "max" => 800]) . '",
            });
        }
    });

    $(".process-faq").on("click", function() {
        var target = $(this).data("target"),
            isValid = true,
            $faqBlock = $("#"+target),
            formId = "job-form",
            key = $(this).data("key");

        var inputsOnCurrentStep = $faqBlock.find("input:visible, textarea:visible");
        $.each(inputsOnCurrentStep, function() {
            $("#" + formId).yiiActiveForm("validateAttribute", $(this).attr("id"), true, 0);
        }).promise().done(function() {
            var errors = $faqBlock.find(".has-error");
            if(errors.length > 0) {
                isValid = false;
            }
        });

        if(isValid) {
            $faqEditBlock = $faqBlock.find("#faq-edit-"+key);
            $faqAddBlock = $faqBlock.find("#faq-add-"+key);
            var faqBlockQuestion = $faqBlock.find("#container-question-" + key + " input").val();
            var faqBlockAnswer = $faqBlock.find("#container-answer-" + key + " input").val();

            $faqEditBlock.find(".feb-head").html(faqBlockQuestion + "<i class=\"fa fa-chevron-down\"></i>");
            $faqAddBlock.appendTo($faqEditBlock.find(".feb-body"));

            $faqEditBlock.find(".feb-head:visible").trigger("click");
            $faqEditBlock.show();
        }
    });

    $(".readsym").bind("keyup", function() {
        $(this).parent().children(".counttext").children(".ctspan").html($(this).val().length);
    });
');