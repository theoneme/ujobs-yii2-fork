<?php

use backend\models\PostJobForm;
use common\helpers\FileInputHelper;
use common\helpers\ImperaviHelper;
use common\models\Attachment;
use common\models\Job;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $rootCategoriesList array
 * @var $model PostJobForm
 * @var $measures array
 * @var $currencies array
 * @var $isAjax boolean
 */

?>
<div class="page-form">
    <?php Pjax::begin(['id' => 'new_job', 'enablePushState' => !$isAjax]) ?>
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
            'class' => ($isAjax ? 'ajax-submit' : ''),
            'data-pj-container' => 'jobs-pjax'
        ],
        'id' => 'job-form',
    ]); ?>
    <?= Html::hiddenInput('id', $model->job->id, ['id' => 'job-id']) ?>
    <div class="box box-primary container">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Overview') ?></h3>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="formgig-block">
                    <div class="row set-item form-group" style="padding-top: 10px;">
                        <?= $form->field($model, 'title', [
                            'template' => '
                            <div class="col-md-2 col-sm-3 col-xs-3 leftset">
                                <div class="set-title">
                                    {label}
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-3 rightset">
                                {input}
                                {error}
                            </div>
                        ',
                            'options' => ['tag' => false]
                        ])->textInput([
                            'class' => 'readsym form-control',
                            'placeholder' => Yii::t('app', 'I will do something I am really good at')
                        ]) ?>
                        <?= $form->field($model, 'locale', [
                            'template' => '
                            <div class="col-md-2 col-sm-3 col-xs-3 leftset">
                                <div class="set-title">
                                    {label}
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-3 rightset">
                                {input}
                                {error}
                            </div>
                        ',
                            'options' => ['tag' => false]
                        ])->dropDownList(Yii::$app->params['languages'])->label('Язык работы') ?>
                    </div>
                </div>
                <div class="formgig-block">
                    <div class="row set-item">
                        <div class="form-group">
                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">
                                    <label class="control-label"
                                           for="postjobform-category_id"><?= Yii::t('app', 'Category') ?></label>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                        <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                            'prompt' => ' -- ' . Yii::t('app', 'Select A Category'),
                                            'class' => 'form-control'
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                        <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                            'pluginOptions' => [
                                                'initialize' => true,
                                                'depends' => ['postjobform-parent_category_id'],
                                                'placeholder' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                                'url' => Url::to(['/category/subcat']),
                                                'params' => ['depdrop-helper']
                                            ],
                                            'options' => [
                                                'class' => 'form-control',
                                            ]
                                        ])->label(false); ?>

                                        <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="formgig-block">
                    <div class="row set-item">
                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                            <div class="set-title">
                                Теги
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                            <?= Html::activeTextInput($model, 'tags', ['class' => 'readsym', 'id' => 'tags']) ?>
                            <div class="counttext">
                                <?= Yii::t('app', 'min. three tags separated with commas') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="attributes-info"></div>
            </div>
        </div>
    </div>
    <div class="box box-primary container form-gig">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Scope & Pricing') ?></h3>
        </div>
        <h3><?= Yii::t('app', 'Packages') ?></h3>

        <div class="cgb-head">
            <?= Yii::t('app', 'Currency for your prices') ?>
        </div>
        <?= $form->field($model, "currency_code")->dropDownList($currencies, [
            'class' => 'form-control'
        ])->label(false); ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="formgig-block">
                    <div class="row set-item">
                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                            <div class="set-title">
                                <label for="price-per-unit" class="control-label">
                                    Тип цены
                                </label>
                                <div class="mobile-question" data-toggle="modal"></div>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                            <div class="chover">
                                <?= Html::activeRadio($model, 'price_per_unit', ['value' => 0, 'id' => 'ppu-false', 'class' => 'ppu-radio radio-checkbox', 'label' => false]) ?>
                                <?= Html::label('Указать цену за работу целиком', 'ppu-false') ?>
                            </div>
                            <div class="chover">
                                <?= Html::activeRadio($model, 'price_per_unit', ['value' => 1, 'id' => 'ppu-true', 'class' => 'ppu-radio radio-checkbox', 'label' => false]) ?>
                                <?= Html::label(Yii::t('app', 'Указать цену за единицу'), 'ppu-true') ?>
                            </div>
                        </div>
                        <div class="measure-container">
                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">
                                    <label for="price-per-unit" class="control-label">
                                        Цена за:
                                    </label>
                                    <div class="mobile-question" data-toggle="modal"></div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                <div class="col-md-6">
                                    <?= $form->field($model, "units_amount")->textInput(['placeholder' => Yii::t('app', 'Quantity')])->label(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'measure_id')->dropDownList($measures[$model->category_id] ?? [], [
                                        'prompt' => Yii::t('model', 'Measure unit'),
                                        'class' => 'form-control measure-select'
                                    ])->label(false); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="create-gig-block row">
            <div class="table-packs col-md-8 col-sm-12 col-xs-12">
                <div class="row-packs">
                    <div class="cell-packs text-left noborder"></div>
                    <div class="cell-packs">
                        <?= Yii::t('app', 'Basic') ?>
                    </div>
                    <div class="cell-packs">
                        <?= Yii::t('app', 'Standart') ?>
                    </div>
                    <div class="cell-packs">
                        <?= Yii::t('app', 'Premium') ?>
                    </div>
                </div>

                <div class="row-packs">
                    <div class="cell-packs text-left noborder"></div>
                    <?php foreach ($model->packages as $key => $package) { ?>
                        <div class="cell-packs">
                            <?= $form->field($package, "[$key]title", [
                                'template' => '{label}{input}'
                            ])->textarea([
                                'class' => 'packstext form-control gigp-title',
                                'placeholder' => Yii::t('app', 'Package Name')
                            ])->label(false); ?>
                            <i class="fa fa-pencil"></i>
                        </div>
                    <?php } ?>
                </div>

                <div class="row-packs">
                    <div class="cell-packs text-left noborder"></div>
                    <?php foreach ($model->packages as $key => $package) { ?>
                        <div class="cell-packs">
                            <?= $form->field($package, "[$key]description", [
                                'template' => '{label}{input}'
                            ])->textarea([
                                'class' => 'packstext form-control gigp-title',
                                'placeholder' => Yii::t('app', 'Describe your bid details')
                            ])->label(false); ?>
                            <i class="fa fa-pencil"></i>
                        </div>
                    <?php } ?>
                </div>

                <div class="row-packs">
                    <div class="cell-packs text-left noborder"></div>
                    <?php foreach ($model->packages as $key => $package) { ?>
                        <div class="cell-packs">
                            <?= $form->field($package, "[$key]included", [
                                'template' => '{label}{input}'
                            ])->textarea([
                                'class' => 'packstext form-control gigp-title',
                                'placeholder' => Yii::t('app', 'What Is Included?')
                            ])->label(false); ?>
                            <i class="fa fa-pencil"></i>
                        </div>
                    <?php } ?>
                </div>

                <div class="row-packs">
                    <div class="cell-packs text-left noborder"></div>
                    <?php foreach ($model->packages as $key => $package) { ?>
                        <div class="cell-packs">
                            <?= $form->field($package, "[$key]excluded", [
                                'template' => '{label}{input}'
                            ])->textarea([
                                'class' => 'packstext form-control gigp-title',
                                'placeholder' => Yii::t('app', 'What is not included?')
                            ])->label(false); ?>
                            <i class="fa fa-pencil"></i>
                        </div>
                    <?php } ?>
                </div>
                <div class="row-packs">
                    <div class="cell-packs text-left">
                        <?= Yii::t('app', 'Price') ?> (RUB)
                    </div>

                    <?php foreach ($model->packages as $key => $package) { ?>
                        <div class="cell-packs">
                            <?= $form->field($package, "[$key]price", [
                                'template' => '{label}{input}

                                    '
                            ])->textInput([
                                'type' => 'number',
                                'class' => 'packstext',
                                'placeholder' => 999
                            ])->label(false); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="create-gig-block">
            <div class="cgb-head">
                <?= Yii::t('app', 'Execution time (days)') ?>
            </div>
            <?= $form->field($model, 'execution_time')->textInput([
                'type' => 'number',
                'class' => 'form-control',
                'placeholder' => Yii::t('app', 'Set execution time for this service in days')
            ]) ?>
        </div>
        <div class="create-gig-block">
            <div class="cgb-head">
                <?= Yii::t('app', 'Extras') ?>
            </div>
            <div class="cgb-body">
                <div class="additional-gig-item">
                    <div class="additional-gig-head" style="display: block;">
                        <i class="fa fa-plus"></i> <a id="load-extra" href=""><?= Yii::t('app', 'Add Extra') ?></a>
                    </div>

                    <?php foreach ($model->extras as $key => $value) { ?>
                        <div class="cgb-optbox" id="extra-block-<?= $key ?>">
                            <div class="row line-add-extra">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <?= Yii::t('app', 'Title') ?>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <?= $form->field($value, "[{$key}]title")->textInput([
                                        'placeholder' => Yii::t('app', 'Title your extra service')
                                    ])->label(false) ?>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <?= Yii::t('app', 'For an extra') ?>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 extra-recommend">
                                    <?= $form->field($value, "[{$key}]price")->textInput([
                                        'placeholder' => Yii::t('app', '500')
                                    ])->label(false) ?>
                                </div>
                            </div>
                            <div class="row line-add-extra">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <?= Yii::t('app', 'Description') ?>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <?= $form->field($value, "[{$key}]description")->textInput([
                                        'placeholder' => Yii::t('app', 'Describe your offering')
                                    ])->label(false) ?>
                                </div>
                            </div>
                            <div class="delete-extra">
                                <i class="fa fa-times"></i>

                                <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue', 'data-target' => "extra-block-{$key}", 'data-key' => $key]) ?>
                            </div>
                            <hr>
                        </div>
                    <?php } ?>
                    <div id="extra-container"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-primary container form-gig">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Requirements') ?></h3>
        </div>
        <div class="cgb-head"><?= Yii::t('app', 'Set requirements for this job') ?></div>
        <div class="formgig-block">
            <div class="row set-item">
                <?= $form->field($model, 'requirements_type', [
                    'template' => '
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title">
                                        {label}
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                    {input}
                                </div>
                            '
                ])->radioList([
                    Job::REQUIREMENTS_TYPE_DEFAULT => Yii::t('app', 'Default'),
                    Job::REQUIREMENTS_TYPE_SIMPLE => Yii::t('app', 'Questions'),
                    Job::REQUIREMENTS_TYPE_ADVANCED => Yii::t('app', 'Questions with steps (wizard)')
                ])->label(Yii::t('model', 'Requirements type'))
                ?>
            </div>
        </div>
        <div id="requirements_type_0" class="hidden requirements-type-block">
            <div class="formgig-block">
                <?= $form->field($model, 'requirements', [
                    'template' => '
                                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                    <div class="set-title">
                                        {label}
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                    {input}
                                </div>
                            '
                ])->textInput([
                    'class' => 'readsym form-control'
                ])->label(Yii::t('model', 'Additional info'))
                ?>
            </div>
        </div>
        <div id="requirements_type_1" class="hidden requirements-type-block">
            <div class="box box-danger wizard-options">
                <div class="box-header with-border">
                    <h3 class="box-title">Требования</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="elements-container"
                         data-index="<?= count($model->simple_requirements) > 0 ? count($model->simple_requirements) - 1 : 0 ?>"
                         id="type-1-block" style="margin-bottom:15px;">
                        <?php foreach ($model->simple_requirements as $key => $requirement) {
                            echo $this->renderAjax('list-items/job-requirement', [
                                'model' => $requirement,
                                'form' => $form,
                                'index' => "[simple][{$key}]"
                            ]);
                        } ?>
                    </div>
                    <?= Html::a('<i class="fa fa-plus"></i> Добавить требование', '#', [
                        'data-action' => "add-requirement",
                        'data-container' => "type-1-block"
                    ]) ?>
                </div>
            </div>
        </div>
        <div id="requirements_type_2" class="hidden requirements-type-block">
            <div class="box box-danger wizard-options">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('app', 'Steps') ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="elements-container"
                         data-index="<?= count($model->advanced_requirements) > 0 ? count($model->advanced_requirements) - 1 : 0 ?>">
                        <?php foreach ($model->advanced_requirements as $key => $step) {
                            echo $this->renderAjax('list-items/job-requirement-step', [
                                'iterator' => $key,
                                'model' => $step,
                                'form' => $form,
                                'index' => "[advanced][{$key}]"
                            ]);
                        } ?>
                    </div>

                    <?= Html::a('<i class="fa fa-plus"></i> Добавить шаг', '#', [
                        'data-action' => 'add-step'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Description') ?></h3>
        </div>
        <div class="create-gig-block row show-notes">
            <div class="col-md-12">
                <div class="cgb-head"><?= Yii::t('app', 'Briefly Describe Your Gig') ?></div>
                <?= $form->field($model, "description")->widget(Widget::class, [
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'plugins' => [
                            'fullscreen',
                            'fontsize',
                            'fontcolor'
                        ]
                    ]
                ])->label(false);
                ?>
            </div>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Frequently Asked Questions') ?></h3>
        </div>
        <div class="create-gig-block row show-notes">
            <div class="col-md-12">
                <div class="cgb-head"><?= Yii::t('app', 'Add Questions & Answers for Your Buyers.') ?></div>
                <a id="load-faq" class="addfaq" href="#" data-pjax="0">+ <?= Yii::t('app', 'Add FAQ') ?></a>

                <?php foreach ($model->faq as $key => $value) { ?>
                    <div id="faq-block-<?= $key ?>">
                        <div id="faq-edit-<?= $key ?>">
                            <div class="faqedit-block">
                                <div class="feb-head">
                                    <?= $value->question ?>
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                                <div class="feb-body">
                                    <div id="container-question-<?= $key ?>">
                                        <?= Html::activeTextInput($value, "[{$key}]question", [
                                            'placeholder' => Yii::t('app', 'Add a Question: i.e. Do you translate to English as well?'),
                                            'class' => 'form-control'
                                        ]) ?>
                                    </div>

                                    <div id="container-answer-<?= $key ?>">
                                        <?= Html::activeTextarea($value, "[{$key}]answer", [
                                            'placeholder' => Yii::t('app', 'Add an Answer: i.e. Yes, I also translate from English to Hebrew.'),
                                            'class' => 'readsym form-control'
                                        ]) ?>
                                        <div class="row">
                                            <div class="counttext"><span class="ctspan">0</span> /
                                                300 <?= Yii::t('app', 'Max') ?></div>
                                        </div>
                                    </div>
                                    <div class="row faq-buttons text-right">
                                        <?= Html::a(Yii::t('app', 'Cancel'), '#', ['class' => 'btn btn-sm btn-warning closeeditt', 'data-new' => 0, 'data-target' => "faq-block-{$key}", 'data-key' => $key]) ?>
                                        <?= Html::button(Yii::t('app', 'Save'), ['data-target' => "faq-block-{$key}", 'class' => 'btn btn-success btn-sm process-faq', 'data-key' => $key]) ?>
                                        <?= Html::a('<i class="fa fa-trash"></i>&nbsp;' . Yii::t('app', 'Remove'), '#', ['class' => 'del-faq', 'data-target' => "faq-block-{$key}", 'data-key' => $key]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div id="faq-container"></div>
            </div>
        </div>
        <div id="additional-info"></div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Gig Gallery') ?></h3>
        </div>
        <div class="create-gig-portfolio show-notes">
            <div class="cgp-head row">
                <div class="cgp-title">
                    <?= Yii::t('app', 'Portfolio Photos') ?>
                    <span
                            class="cgp-descr"><?= Yii::t('app', 'Upload photos that describe or relate to your Gig.') ?></span>
                </div>
            </div>
            <div class="cgp-upload-files">
                <?= FileInput::widget(
                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'id' => 'file-upload-input',
                        'options' => ['accept' => 'image/*', 'multiple' => true, 'class' => 'file-upload-input'],
                        'pluginOptions' => [
                            'overwriteInitial' => false,
                            'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                return $var->getThumb();
                            }, $model->attachments) : [],
                            'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                            'previewThumbTags' => [
                                '{actions}' => '{actions}',
                            ],
                        ]
                    ])
                ) ?>
                <div class="images-container">
                    <?php foreach ($model->attachments as $key => $image) {
                        /* @var $image Attachment */
                        echo $form->field($image, "[{$key}]content")->hiddenInput([
                            'value' => $image->content,
                            'data-key' => 'image_init_' . $image->id
                        ])->label(false);
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="formpost-block row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->radioList(Job::getStatusLabels()) ?>
        </div>
    </div>
    <div class="container box box-success" style="padding:5px;">
        <?= Html::submitButton(Yii::t('app', 'Save'), [
            'id' => 'submit-button',
            'class' => 'btn btn-success pull-right'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php $renderFaqRoute = Url::toRoute('/job/render-faq');
    $renderAttributesRoute = Url::toRoute('/job/render-attributes');
    $renderExtraRoute = Url::toRoute('/job/render-extra');
    $renderAdditionalRoute = Url::toRoute('/job/render-additional');
    $faqs = count($model->faq);
    $extras = count($model->extras);
    $attachments = count($model->attachments);
    $requirements = count($model->simple_requirements);
    $steps = count($model->advanced_requirements);
    $measures = json_encode($measures);
    $tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'tag']);

    $script = <<<JS
        let faqs = $faqs,
		    extras = $extras,
		    attachments = $attachments,
    	    measures = $measures,
		    requirements = $requirements,
		    steps = $steps,
		    form = $("#job-form");

		form.yiiActiveForm("remove", "jobpackage-standart-description");
		form.yiiActiveForm("remove", "jobpackage-premium-description");
		form.yiiActiveForm("remove", "jobpackage-basic-description");
		form.yiiActiveForm("remove", "jobpackage-standart-title");
		form.yiiActiveForm("remove", "jobpackage-premium-title");
		form.yiiActiveForm("remove", "jobpackage-basic-title");
		form.yiiActiveForm("remove", "jobpackage-standart-price");
		form.yiiActiveForm("remove", "jobpackage-premium-price");
		form.yiiActiveForm("remove", "jobpackage-basic-price");

		function loadAdditional(categoryId, jobId) {
		    $.get('$renderAdditionalRoute', {
		        category_id: categoryId,
		        entity: jobId,
		    }, function(data) {
		        $("#additional-info").html(data);
		    }, "html");
		}

		function loadAttributes(categoryId, jobId) {
		    $.get('$renderAttributesRoute', {
		        category_id: categoryId,
		        entity: jobId
		    }, function(data) {
		        $("#attributes-info").html(data);
		    }, "html");
		}

		function loadMeasures(categoryId) {
		    $('.measure-select option:not([value=""])').remove();
		    if (measures[categoryId] !== undefined) {
		        $('.price-per-unit-container').show();
		        $.each(measures[categoryId], function(index, value) {
		            $('.measure-select').append('<option value="' + index + '">' + value + '</option>');
		        });
		    } else {
		        $('.price-per-unit-container').hide();
		        $('#ppu-false').prop('checked', true);
		        $('.ppu-radio').trigger('change');
		    }
		}

		$("#postjobform-category_id").on("change", function(e) {
		    let jobId = $('#job-id').val();
		    let categoryId = $(this).find('option:selected').val();
		    loadAttributes(categoryId, jobId);
		    loadAdditional(categoryId, jobId);
		    loadMeasures(categoryId);
		});

		let depdropVal = $('#depdrop-helper').val();
		if (depdropVal) {
		    let jobId = $('#job-id').val();
		    loadAdditional(depdropVal, jobId);
		    loadAttributes(depdropVal, jobId);
		    // loadMeasures(depdropVal);
		}

		$(".process-faq").on("click", function() {
		    let target = $(this).data("target"),
		        isValid = true,
		        faqBlock = $("#" + target),
		        formId = "job-form",
		        key = $(this).data("key"),
		        inputsOnCurrentStep = faqBlock.find("input:visible, textarea:visible");

		    $.each(inputsOnCurrentStep, function() {
		        $("#" + formId).yiiActiveForm("validateAttribute", $(this).attr("id"), true, 0);
		    }).promise().done(function() {
		        let errors = faqBlock.find(".has-error");
		        if (errors.length > 0) {
		            isValid = false;
		        }
		    });

		    if (isValid) {
		        faqEditBlock = faqBlock.find("#faq-edit-" + key);
		        faqAddBlock = faqBlock.find("#faq-add-" + key);
		        let faqBlockQuestion = faqBlock.find("#container-question-" + key + " input").val(),
		            faqBlockAnswer = faqBlock.find("#container-answer-" + key + " input").val();

		        faqEditBlock.find(".feb-head").html(faqBlockQuestion + "<i class=\"fa fa-chevron-down\"></i>");
		        faqAddBlock.appendTo(faqEditBlock.find(".feb-body"));

		        faqEditBlock.find(".feb-head:visible").trigger("click");
		        faqEditBlock.show();
		    }
		});

		$(".closeeditt").on("click", function(e) {
		    e.preventDefault();
		    let target = $(this).data("target"),
		        faqBlock = $("#" + target),
		        key = $(this).data("key"),
		        isNew = $(this).data("new");

		    if (isNew === true) {
		        faqBlock.remove();
		    } else {
		        let faqEditBlock = faqBlock.find("#faq-edit-" + key);

		        faqEditBlock.find(".feb-head:visible").trigger("click");
		    }
		});

		$(".del-faq").on("click", function(e) {
		    e.preventDefault();
		    let target = $(this).data("target"),
		        faqBlock = $("#" + target);

		    faqBlock.remove();
		});

		$("#load-faq").on("click", function(e) {
		    e.preventDefault();
		    $.get("{$renderFaqRoute}", {
		        iterator: faqs
		    }, function(data) {
		        if (data.success === true) {
		            faqs++;
		            $("#faq-container").append(data.html);
		        } else {
		            alertCall('error', data.message);
		        }
		    }, "json");
		});

		$("#load-extra").on("click", function(e) {
		    e.preventDefault();
		    $.get("{$renderExtraRoute}", {
		        iterator: extras
		    }, function(data) {
		        if (data.success === true) {
		            extras++;
		            $("#extra-container").append(data.html);
		        } else {
		            alertCall('error', data.message);
		        }
		    }, "json");
		});

		let hasFileUploadError = false;
		$(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
		    let response = data.response;
		    $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
		    attachments++;
		}).on("filedeleted", function(event, key) {
		    $(".images-container input[data-key=\'" + key + "\']").remove();
		}).on("filebatchuploadcomplete", function() {
		    if (hasFileUploadError === false) {
		        $(this).closest("form").submit();
		    } else {
		        hasFileUploadError = false;
		    }
		}).on("fileuploaderror", function(event, data, msg) {
		    hasFileUploadError = true;
		    $('#' + data.id).find('.kv-file-remove').click();
		});

		$("#submit-button").on("click", function(e) {
		    if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
		        $("#file-upload-input").fileinput("upload");
		        return false;
		    } else {
		        return true;
		    }
		});

		$(".link-blue").on("click", function() {
		    let target = $(this).data("target");

		    $("#" + target).remove();
		});
		$('.ppu-radio').on("change", function(e) {
		    if (parseInt($(".ppu-radio:checked").val())) {
		        $('.measure-container').show();
		    } else {
		        $('.measure-container').hide();
		    }
		}).trigger('change');

		$("#tags").selectize({
		    valueField: "value",
		    labelField: "value",
		    searchField: ["value"],
		    plugins: ["remove_button"],
		    persist: false,
		    create: true,
		    maxOptions: 10,
		    render: {
		        option_create: function(data, escape) {
		            return "<div class='create'>Добавить тег&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
		        }
		    },
		    "load": function(query, callback) {
		        if (!query.length)
		            return callback();
		        $.post("$tagListUrl", {
		                query: encodeURIComponent(query)
		            },
		            function(data) {
		                callback(data);
		            }).fail(function() {
		            callback();
		        });
		    }
		});

		//**************Requirements**************/

		$('.field-postjobform-requirements_type input').on('change', function() {
		    let type = this.value;

		    $('.requirements-type-block').addClass('hidden');
		    $('#requirements_type_' + type).removeClass('hidden');
		});
		$(".field-postjobform-requirements_type input:checked").trigger('change');
JS;

    $this->registerJs($script); ?>
    <?php Pjax::end() ?>
</div>
