<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.01.2018
 * Time: 17:14
 */

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model \common\models\JobRequirementStep */
/* @var $form ActiveForm */
/* @var $index integer */
/* @var boolean $createForm */

?>

<?php if ($createForm) {
    $form = new ActiveForm(['id' => 'job-form']);
    ob_end_clean();
} ?>
    <div class="element-form">
        <?= $form->field($model, "[{$iterator}]title")->textInput([
            'class' => 'form-control title-source',
        ]) ?>

        <div class="form-buttons clearfix form-group">
            <?= Html::button('Сохранить', ['class' => 'pull-right btn btn-success submit-button']) ?>
            <?= Html::button('Отменить', ['class' => 'pull-right btn btn-warning cancel-button']) ?>
        </div>
    </div>
    <div class="element-children" <?= $createForm ? 'style="display: none;"' : ''; ?>>
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Requirements') ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="elements-container" id="type-2-block" style="margin-bottom:15px;"
                     data-index="<?= count($model->jobRequirements) > 0 ? count($model->jobRequirements) - 1 : 0 ?>">
                    <?php foreach ($model->jobRequirements as $key => $jobRequirement) {
                        echo $this->renderAjax('list-items/job-requirement', ['model' => $jobRequirement, 'form' => $form, 'index' => ($index . "[" . $key . "]")]);
                    } ?>
                </div>
                <?= Html::a('<i class="fa fa-plus"></i> Добавить требование', '#', [
                    'class' => 'add-element',
                    'data-action' => "add-step-requirement",
                    'data-container' => "type-2-block"
                ]) ?>
            </div>
        </div>
    </div>
<?php
if ($createForm) {
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    let attributes = $attributes;
    $.each(attributes, function() {
        $("#job-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);

//    $form->registerClientScript();
} ?>