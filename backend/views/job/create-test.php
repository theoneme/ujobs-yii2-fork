<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Job */

$this->title = Yii::t('app', 'Create Job for user {user}', ['user' => $user->profile->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-test', [
        'model' => $model,
        'rootCategoriesList' => $rootCategoriesList
    ]) ?>

</div>
