<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.01.2018
 * Time: 18:08
 */

use common\modules\wizard\models\WizardOption;
use yii\helpers\Html;

/* @var $model \common\models\JobRequirementStep */
/* @var $index string */

?>
<div class="element-item" data-id="<?= $model->id?>">
    <div class="element-preview">
        <div class="element-name">
            <?= $model->title ?>
        </div>
        <div class="element-actions pull-right">
            <?= Html::a('<i class="fa fa-pencil"></i>', null, ['class' => 'edit-element'])?>
            <?= Html::a('<i class="fa fa-times"></i>', null, ['class' => 'remove-element'])?>
        </div>
    </div>
    <?= $this->renderAjax('../job-requirement-step-partial', ['iterator' => $iterator, 'model' => $model, 'form' => $form, 'index' => $index,  'createForm' => false]);?>
</div>