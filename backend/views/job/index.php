<?php

use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use common\models\Job;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\models\Message;
use common\models\user\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

\backend\assets\AutocompleteAsset::register($this);
MyCropAsset::register($this);
SelectizeAsset::register($this);

$type = $_GET['JobSearch']['type'];
$this->title = $type == 'tender' ? Yii::t('app', 'Requests') : Yii::t('app', 'Jobs');

$this->params['breadcrumbs'][] = $this->title;
$sup_id = Yii::$app->params['supportId'];
$sentMessages = Message::find()
    ->joinWith(['conversationMembers'])
    ->select(['COUNT(message.id)', 'conversation_member.user_id as uid'])
    ->where(['message.user_id' => $sup_id])
    ->andWhere('conversation_member.user_id != message.user_id')
    ->indexBy('uid')
    ->groupBy('uid')
    ->asArray()
    ->column();
$unreadMessages = Message::find()
    ->joinWith(['conversationMembers'])
    ->select(['COUNT(message.id)', 'message.user_id as uid'])
    ->where('message.created_at > conversation_member.viewed_at')
    ->andWhere('conversation_member.user_id != message.user_id')
    ->andWhere(['conversation_member.user_id' => $sup_id])
    ->indexBy('uid')
    ->groupBy('uid')
    ->asArray()
    ->column();
?>
    <div class="job-index">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?php \yii\widgets\Pjax::begin(['id' => 'grid-container']); ?>
                <?= Html::beginForm(['job/moderate-selected'], 'post', ['id' => 'trig']); ?>
                <?= Html::submitButton(Yii::t('app', 'Moderate Selected'), ['class' => 'btn btn-success', 'id' => 'delete-selected']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                        ],
                        [
                            'header' => 'Продавец',
                            'attribute' => 'username',
                            'value' => function ($model) {
                                /* @var $model Job*/
                                return Html::a(Html::img($model->getSellerThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->getSellerName(),
                                    ['/user/admin/update-ajax', 'id' => $model->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                );
                            },
                            'format' => 'html',
                        ],
                        'created_at:datetime',
	                    [
		                    'attribute' => 'title',
		                    'value' => function($model) {
                                /* @var $model Job*/
			                    return StringHelper::truncate($model->getLabel(), 45);
		                    }
	                    ],
	                    [
		                    'attribute' => 'locale',
		                    'header' => 'Язык',
		                    'filter' => Yii::$app->params['languages']
	                    ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /* @var $model Job*/
                                return $model->getStatusLabel(true);
                            },
                            'filter' => Job::getStatusLabels()
                        ],
                        [
                            'header' => 'Письма',
                            'value' => function ($model) use ($sentMessages, $unreadMessages) {
                                /* @var $model Job*/
                                $sent = isset($sentMessages[$model->user->id]) ? $sentMessages[$model->user->id] : 0;
                                $unread = isset($unreadMessages[$model->user->id]) ? $unreadMessages[$model->user->id] : 0;
                                return  $sent . ' отправлено<br>' .
                                    Html::a($unread . ' не прочитано',
                                        Yii::$app->urlManagerFrontEnd->createUrl(['/account/inbox/start-support-conversation', 'user_id' => $model->user_id]),
                                        ['target' => '_blank', 'data-pjax' => 0]
                                    );
                            },
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                            'options' => ['style' => 'width: 150px']
                        ],
                        [
                            'label' => Yii::t('app', 'Photo'),
                            'format' => 'image',
                            'value' => function ($data) {
                                return $data->getThumb();
                            },
                            'contentOptions' => ['class' => 'grid-image-container', 'style' => 'max-width: 40px;']
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Actions',
                            'template' => Yii::$app->user->identity->isAdmin() ? '{update} {delete}' : '{update}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]);
                                }
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                $type = $model->type == Job::TYPE_ADVANCED_TENDER ? Job::TYPE_TENDER : $model->type;
                                switch ($action) {
                                    case 'update':
                                        $url = Url::toRoute(["/{$type}/{$action}", 'id' => $model->id, 'user_id' => $model->user_id]);
                                        break;
                                    default:
                                        $url = Url::toRoute(["/{$type}/{$action}", 'id' => $model->id]);
                                        break;
                                }
                                return $url;
                            }
                        ],
                    ],
                ]); ?>
                <?= Html::endForm(); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>

<?php
$requirementsUrl = Url::to(['/job/render-requirement']);
$advancedRequirementsUrl = Url::to(['/job/render-requirement-step']);
$script = <<<JS
        //**************Requirements**************/
        function renderElementPreview(title, prependTo) {
            let titleBlock = title !== undefined ? '<div class="element-name">' + title + '</div>' : '';
            prependTo.prepend(
                '<div class="element-preview">' + 
                    titleBlock + 
                    '<div class="element-actions pull-right">' + 
                        '<a class="edit-element"><i class="fa fa-pencil"></i></a>' + 
                        '<a class="remove-element"><i class="fa fa-times"></i></a>' + 
                    '</div>' + 
                '</div>'
            );
        }
        
        $('.field-postjobform-requirements_type input').on('change', function() {
		    let type = this.value; 
		    
		    $('.requirements-type-block').addClass('hidden');
		    $('#requirements_type_' + type).removeClass('hidden');
		});
        $(".field-postjobform-requirements_type input:checked").trigger('change');
        
        $(document).on('click', "a[data-action=add-requirement]", function() {
            let self = $(this);
             let id = self.siblings('.elements-container').data('index') + 1;
             let container = $(this).data('container');
             let index = "[simple][" + id + "]";             
             
             $.get('$requirementsUrl', {iterator: id, index: index}, function(response) {
                 self.siblings('.elements-container')
                    .append('<div class="element-item" data-id="' + id + '">' + response.html + '</div>')
                    .data('index', id)
                    .find('.element-item:last > .element-form')
                    .slideDown(400);
                 
                // requirements++;
             });
        });
        
        $(document).on('click', "a[data-action=add-step-requirement]", function() {
            let self = $(this);
             let id = self.siblings('.elements-container').data('index') + 1;
             let parentId = self.closest('.elements-container').data('index');
             let container = $(this).data('container');
             let index = "[advanced][" + parentId + "][" + id + "]";             
             
             $.get('$requirementsUrl', {iterator: id, index: index}, function(response) {
                 self.siblings('.elements-container')
                    .append('<div class="element-item" data-id="' + id + '">' + response.html + '</div>')
                    .data('index', id)
                    .find('.element-item:last > .element-form')
                    .slideDown(400);
                 
                // requirements++;
             });
        });
        
        $(document).on('click', "a[data-action=add-step]", function() {
            let self = $(this);   
            let id = self.siblings('.elements-container').data('index') + 1;
            let container = $(this).data('container');
            let index = "[advanced][" + id + "][]";                      
           
            $.get('$advancedRequirementsUrl', {iterator: id, index: index}, function(response) {
                 self.siblings('.elements-container')
                    .append('<div class="element-item" data-id="' + id + '">' + response.html + '</div>')
                    .data('index', id)
                    .find('.element-item:last > .element-form')
                    .slideDown(400);
                 
                // steps++;
            });
        });

        
        $(document).on("click", ".submit-button", function() {
            let formBlock = $(this).closest('.element-form');
            $(this).closest('.element-form').slideUp(400, function() {
                let itemBlock = formBlock.closest('.element-item');
                let title = formBlock.find('.title-source').val();
                if (!formBlock.siblings('.element-preview').length) {
                    renderElementPreview(title, itemBlock);
                } else {
                    itemBlock.find('.element-name:first').html(title);
                }
                formBlock.siblings('.element-preview, .element-children').slideDown(400);
                formBlock.closest('.elements-container').siblings('.add-element').show();
            });
            
            return false;
        });
        
        $(document).on("click", ".remove-element", function() {
            $(this).closest('.element-item').remove();
            return false;
        });
        
        $(document).on("click", ".cancel-button", function() {
            let elementBlock = $(this).closest('.element-item');
            elementBlock.children('.element-form').slideUp(400, function() {
                if (elementBlock.children('.element-preview').length) {
                    elementBlock.children('.element-preview').slideDown(400);
                }
                else {
                    elementBlock.closest('.elements-container').siblings('.add-element').show();
                }
            });
            return false;
        });
        
        $(document).on("click", ".edit-element", function() {
            let elementBlock = $(this).closest('.element-item');
            elementBlock.children('.element-preview').slideUp(400, function() {
                elementBlock.children('.element-form').slideDown(400);
            });
            return false;
        });
JS;

$this->registerJs($script);
