<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.01.2017
 * Time: 13:05
 */

use yii\helpers\Html;
use common\models\JobExtra;

/* @var JobExtra $model */
/* @var integer $iterator */

?>

    <div class="cgb-optbox" id="extra-block-<?= $iterator ?>">
        <div class="row line-add-extra">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <?= Yii::t('app', 'Title') ?>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-5">
                <div id="container-title-<?= $iterator ?>">
                    <?= Html::activeTextInput($model, "[{$iterator}]title", [
                        'placeholder' => Yii::t('app', 'Title your extra service'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <?= Yii::t('app', 'For an extra') ?>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3 extra-recommend">
                <div id="container-price-<?= $iterator ?>">
                    <?= Html::activeTextInput($model, "[{$iterator}]price", [
                        'placeholder' => Yii::t('app', '500 р.'),
                        'class' => 'form-control',
                        'type' => 'number'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="row line-add-extra">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <?= Yii::t('app', 'Description') ?>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-10">
                <div id="container-description-<?= $iterator ?>">
                    <?= Html::activeTextInput($model, "[{$iterator}]description", [
                        'placeholder' => Yii::t('app', 'Describe your offering') . " " . Yii::t('app', '(This field if optional)'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
        </div>

        <div class="delete-extra">
            <i class="fa fa-times"></i>

            <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue', 'data-target' => "extra-block-{$iterator}", 'data-key' => $iterator]) ?>
        </div>
        <hr>
    </div>

<?php $this->registerJs('
    $(".link-blue").on("click", function() {
        var target = $(this).data("target");

        $("#" + target).remove();
    });

    $("#job-form").yiiActiveForm("add", {
        id: "jobextra-' . $iterator . '-title",
        name: "jobExtra[' . $iterator . '][title]",
        input: "#jobextra-' . $iterator . '-title",
        container: "#container-title-' . $iterator . '",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred, $form) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Title")]) . '",
            });
            yii.validation.string(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Title")]) . '",
                "max": 200,
                "tooLong": "' . Yii::t("yii", "{attribute} must be no greater than {max}.", ["attribute" => Yii::t("model", "Title"), "max" => 200]) . '",
            });
        }
    });
    $("#job-form").yiiActiveForm("add", {
        id: "jobextra-' . $iterator . '-description",
        name: "jobExtra[' . $iterator . '][description]",
        input: "#jobextra-' . $iterator . '-description",
        container: "#container-description-' . $iterator . '",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred, $form) {
            yii.validation.string(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be a string.", ["attribute" => Yii::t("model", "Description")]) . '",
                "max": 800,
                "tooLong": "' . Yii::t("yii", "{attribute} must be no greater than {max}.", ["attribute" => Yii::t("model", "Description"), "max" => 800]) . '",
            });
        }
    });

    $("#job-form").yiiActiveForm("add", {
        id: "jobextra-' . $iterator . '-price",
        name: "jobExtra[' . $iterator . '][price]",
        input: "#jobextra-' . $iterator . '-price",
        container: "#container-price-' . $iterator . '",
        error: ".help-block",
        validate:  function (attribute, value, messages, deferred, $form) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Price")]) . '"
            });
        }
    });

    $(".readsym").bind("keyup", function() {
        $(this).parent().children(".counttext").children(".ctspan").html($(this).val().length);
    });
');