<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2017
 * Time: 14:22
 */

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var Attribute $attribute */
/* @var array $values */
/* @var DynamicForm $model */

?>
<div class="selectize-input-container">
    <?= Html::activeTextInput($model, "attribute_{$attribute->id}", [
        'placeholder' => $attribute->translation->title,
        'data-at' => $attribute->id,
        'class' => 'form-control'
    ]) ?>
</div>

<?
$url = Url::to(['/ajax/attribute-list', 'alias' => $attribute->alias]);

$script = <<<JS
    $("#dynamicform-attribute_{$attribute->id}").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{$url}",
                dataType: "json",
                type : 'post',
                data: {
                    query: request.term
                },
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.value,
                            value: item.value
                        }
                    }));
                }
            });
        },
        minLength: 2
    });
JS;

$this->registerJs($script);
