<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2017
 * Time: 15:05
 */

use common\modules\attribute\models\Attribute;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var Attribute $attribute */
/* @var array $values */

?>

<?= Html::activeTextInput($model, "attribute_{$attribute->id}", [
    'class' => 'form-control',
    'type' => 'number',
    'prompt' => '-- ' . Yii::t('app', 'Select value'),
    'placeholder' => $attribute->translation->title
]) ?>
