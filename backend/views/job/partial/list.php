<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.09.2016
 * Time: 16:33
 */

use common\modules\attribute\models\Attribute;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var Attribute $attribute */
/* @var array $values */

?>

<?= Html::activeDropDownList($model, "attribute_{$attribute->id}", ArrayHelper::map($values, 'id', 'translation.title'), [
    'class' => 'form-control',
    'prompt' => '-- ' . Yii::t('app', 'Select value')
]) ?>
