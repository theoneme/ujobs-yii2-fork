<?php

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;

/* @var Attribute $attribute */
/* @var DynamicForm $model */

$checked = (array) $model->{"attribute_{$attribute->id}"};

?>

<?php foreach ($attribute->values as $value) { ?>
    <div class="chover mini">
        <input id="dynamicform-<?= $attribute->alias ?>-<?= $value->id ?>" type="radio"
               name="DynamicForm[attribute_<?= $attribute->id?>][]" value="<?= $value->id ?>" <?= in_array($value->id, $checked) ? 'checked' : '' ?> class="radio-checkbox"/>
        <label for="dynamicform-<?= $attribute->alias ?>-<?= $value->id ?>">
            <span class="ctext"><?= $value->translation->title ?></span>
        </label>
    </div>
<?php } ?>