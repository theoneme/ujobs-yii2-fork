<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.01.2018
 * Time: 13:30
 */

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model \common\models\JobRequirement */
/* @var $form ActiveForm */
/* @var $index integer */
/* @var boolean $createForm */

?>

<?php if ($createForm) {
    $form = new ActiveForm(['id' => 'job-form']);
    ob_end_clean();
} ?>
    <div class="element-form">
        <?= $form->field($model, "{$index}title")->textarea([
            'class' => 'form-control title-source',
            'placeholder' => 'For example: specifications, dimensions, brand guidelines, or background materials.'
        ]) ?>

        <?= $form->field($model, "{$index}is_required")->checkbox() ?>
        <div class="form-buttons clearfix form-group">
            <?= Html::button('Сохранить', ['class' => 'pull-right btn btn-success submit-button']) ?>
            <?= Html::button('Отменить', ['class' => 'pull-right btn btn-warning cancel-button']) ?>
        </div>
    </div>
<?php
if ($createForm) {
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    let attributes = $attributes;
    $.each(attributes, function() {
        $("#job-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);

//    $form->registerClientScript();
} ?>