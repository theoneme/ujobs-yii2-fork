<?php

use frontend\components\GmapsInputWidget;
use frontend\models\PostJobForm;

/**
 * @var $model PostJobForm
 * @var $jobTypeOptions array
 * @var $placeOptions array
 * @var $timeOptions array
 * @var $showAddressField bool
 */

?>
<?php if ($showAddressField === true) { ?>
    <div class="formgig-block clearfix">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label"
                           for="postproductform-currency_code"><?= Yii::t('app', 'Address') ?></label>
                    <div class="mobile-question" data-toggle="modal"></div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <?= GmapsInputWidget::widget(['model' => $model, 'inputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Moscow, Novy Arbat 12')]
                ]); ?>
            </div>
        </div>
    </div>
<?php } ?>