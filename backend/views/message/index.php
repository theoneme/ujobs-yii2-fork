<?php

use common\models\Message;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

<?php Pjax::begin(); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => 'conversation_message',
        'options' => [
            'tag' => 'div',
            'id' => 'conversation-container'
        ],
        'itemOptions' => [
            'tag' => false
        ],
        'showOnEmpty' => true,
        'layout' => "{items}"
    ]) ?>

<?php Pjax::end(); ?></div>
