<?php
use common\models\Message;
use common\models\Offer;
use yii\web\View;

/**
 * @var View $this
 * @var Message $model
 * @var integer $from_id
 */
$offer = $model->conversation->firstMessage->offer;
$fromMe = ($offer->type == Offer::TYPE_OFFER && hasAccess($offer->from_id)) || ($offer->type == Offer::TYPE_REQUEST && hasAccess($offer->to_id));
$messageParams = [];
if ($model->type == Message::TYPE_OFFER_ACCEPTED) {
    $messageParams['price'] = $offer->getPrice();
}

?>
<div class="conversation-message offer-status" id="message-<?= $model->id?>">
    <div class="message-body">
        <div class="osh-icon <?= $model->getTypeIcon()?>"></div>
        <div class="osh-title text-center">
            <?= Yii::t('order', $model->getTypeMessage($fromMe, $offer->type))?>
        </div>
        <p class="osh-descr text-center">
            <?= Yii::t('order', $model->getTypeSubMessage($fromMe, $offer->type), $messageParams)?>
        </p>
    </div>
    <div class="message-time text-right">
        <?= $model->created_at?>
    </div>
</div>