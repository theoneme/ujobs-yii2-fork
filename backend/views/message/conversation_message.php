<?php

use backend\assets\ProfileAsset;
use common\models\Message;
use yii\web\View;

/**
 * @var View $this
 * @var Message $model
 * @var integer $from_id
 */

ProfileAsset::register($this);
echo $this->render('type/' . $model->getTypeView(), [
    'model' => $model,
//    'from_id' => $from_id
]);