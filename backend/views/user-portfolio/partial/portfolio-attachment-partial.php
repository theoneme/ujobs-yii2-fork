<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.01.2017
 * Time: 13:05
 */

use common\helpers\FileInputHelper;
use common\models\attachment;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var attachment $model */
/* @var integer $iterator */
?>

    <div class="cgb-optbox">
        <div class="row line-add-extra">
            <div class="col-xs-12">
                <div id="container-description-<?= $iterator ?>">
                    <?= Html::activeTextarea($model, "[{$iterator}]description", [
                        'placeholder' => Yii::t('account', 'Description of your extra image'),
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="help-block"></div>
            </div>
            <div class="col-xs-12 extra-recommend">
                <div id="container-content-<?= $iterator ?>" data-chetotam="<?= $iterator ?>" class="chetotam">
                    <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'id' => "file-upload-input-extra-{$iterator}",
                        'options' => ['class' => 'file-upload-input-extra'],
                        'pluginOptions' => [
                            'overwriteInitial' => true,
                            'initialPreview' => $model->content ? $model->getThumb() : [],
                            'initialPreviewConfig' => !empty($model->content) ? [[
                                'caption' => basename($model->content),
                                'url' => Url::toRoute('/image/delete'),
                                'key' => $model->user_id
                            ]] : [],
                        ]
                    ])) ?>
                </div>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="delete-extra">
            <i class="fa fa-times"></i>
            <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue', 'id' => "link-blue-{$iterator}"]) ?>
        </div>
        <hr>
    </div>
<?php $script = <<<JS
    $("#portfolio-form").yiiActiveForm("add", {
        id: "attachment-{$iterator}-description",
        name: "attachment[{$iterator}][description]",
        input: "#attachment-{$iterator}-description",
        container: "#container-description-{$iterator}",
        error: ".help-block",
        enableAjaxValidation: false,
        validateOnChange: false,
        validateOnBlur: false
    });
    let input = $("#file-upload-input-extra-{$iterator}");

    let hasFileUploadError = false;
    input.on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(".images-container").append("<input name='Attachment[{$iterator}][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    }).on("filedeleted", function(event, key) {
        $(".images-container").find("input[data-key='" + key + "']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $("#portfolio-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    $("#link-blue-{$iterator}").on("click", function() {
        let id = $(this).data('id');
        $(this).closest(".cgb-optbox").remove();
        $(".images-container").find("input[data-key=image_init_" + id + "]").remove();
    });
    
JS;

$this->registerJs($script);