<?php

use common\models\UserPortfolio;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserPortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Portfolios');
$this->params['breadcrumbs'][] = $this->title;

\backend\assets\SelectizeAsset::register($this);

?>
<div class="user-withdrawal-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model UserPortfolio*/
                            return \yii\helpers\StringHelper::truncate($model->title, 35);
                        },
                    ],
                    [
                        'header' => 'Пользователь',
                        'attribute' => 'profile.name',
                        'value' => function ($model) {
                            /* @var $model \common\models\UserPortfolio */
                            return Html::a(Html::img($model->getSellerThumb('catalog'), ['style' => 'width: 50px']) . "&nbsp;" . $model->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->user_id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model UserPortfolio*/
                            return $model->getStatusLabel(true);
                        },
                        'filter' => UserPortfolio::getStatusLabels()
                    ],
                    [
                        'label' => Yii::t('app', 'Photo'),
                        'format' => 'image',
                        'value' => function ($data) {
                            return $data->getThumb('catalog');
                        },
                        'contentOptions' => ['class' => 'grid-image-container', 'style' => 'max-width: 40px;']
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => Yii::$app->user->identity->isAdmin() ? '{update} {delete}' : '{update}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'update':
                                    $url = Url::toRoute(["/user-portfolio/update", 'id' => $model->id, 'user_id' => $model->user_id]);
                                    break;
                                default:
                                    $url = Url::toRoute(["/user-portfolio/delete", 'id' => $model->id]);
                                    break;
                            }
                            return $url;
                        }
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>