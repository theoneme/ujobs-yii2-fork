<?php

use backend\models\PostPortfolioForm;
use common\helpers\FileInputHelper;
use common\models\Attachment;
use common\models\UserPortfolio;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostPortfolioForm
 * @var $exampleModel common\models\Job
 * @var $action string
 * @var $locales array
 * @var $currencySymbols array
 * @var $currencies array
 */

\backend\assets\MyCropAsset::register($this);

?>
    <div class="user-withdrawal-index">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <?= Html::a(Yii::t('app', 'Link on site'), Yii::$app->urlManagerFrontEnd->createUrl(['/account/portfolio/view', 'id' => $model->portfolio->id], true), ['target' => '_blank']) ?>
                (<?= Yii::$app->urlManagerFrontEnd->createUrl(['/account/portfolio/view', 'id' => $model->portfolio->id]) ?>)
            </div>

            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'data-pjax' => false,
                        'class' => ($isAjax ? 'ajax-submit' : ''),
                        'data-pj-container' => 'grid-container'
                    ],

                    'id' => 'portfolio-form',
                ]); ?>
                <?= Html::hiddenInput('id', $model->portfolio->id, ['id' => 'portfolio-id']) ?>
                <div class="formgig-block">
                    <div class="row set-item">
                        <?= $form->field($model, 'locale', [
                            'template' => '
                            <div class="col-md-2 col-sm-3 col-xs-3 leftset">
                                <div class="set-title">
                                    {label}
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-3 rightset">
                                {input}
                                {error}
                            </div>
                        ',
                            'options' => ['tag' => false]
                        ])->dropDownList(Yii::$app->params['languages'])->label('Язык портфолио') ?>
                    </div>
                    <div class="row set-item">
                        <?= $form->field($model, "title", [
                            'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                        <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('account', 'Max') . '</div>
                                    </div>
                                '
                        ])->textarea([
                            'class' => 'readsym bigsymb',
                            'placeholder' => Yii::t('account', 'Name your portfolio')
                        ])
                        ?>
                    </div>
                </div>
                <div class="formgig-block">
                    <div class="row set-item">
                        <?= $form->field($model, "description", [
                            'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->widget(Widget::class, [
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 200,
                                'plugins' => [
                                    'fullscreen',
                                    'fontsize',
                                    'fontcolor'
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <div class="formgig-block">
                    <div class="row set-item">
                        <div class="form-group">
                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">
                                    <label class="control-label"
                                           for="postproductform-category_id"><?= Yii::t('account', 'Category') ?></label>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                        <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                            'prompt' => ' -- ' . Yii::t('account', 'Select An Upper Category'),
                                            'class' => 'form-control'
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                        <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                            'pluginOptions' => [
                                                'initialize' => true,
                                                'depends' => ['postportfolioform-parent_category_id'],
                                                'placeholder' => ' -- ' . Yii::t('account', 'Select A Category'),
                                                'url' => Url::to(['/category/subcat']),
                                                'params' => ['depdrop-helper']
                                            ],
                                            'options' => [
                                                'class' => 'form-control',
                                            ]
                                        ])->label(false); ?>
                                        <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-md-offset-6 col-xs-12 select-margin hidden 3rd-level">
                                        <?= $form->field($model, 'subcategory_id')->widget(DepDrop::class, [
                                            'pluginOptions' => [
                                                'initialize' => true,
                                                'depends' => ['postportfolioform-category_id'],
                                                'placeholder' => ' -- ' . Yii::t('account', 'Select A Subcategory'),
                                                'url' => Url::to(['/category/subcat']),
                                                'params' => ['depdrop-helperx']
                                            ],
                                            'pluginEvents' => [
                                                "depdrop.change" => "function(event, id, value, count) { 
                                                                let container = $(this).closest('.3rd-level');
                                                                if(count > 0) {
                                                                    container.removeClass('hidden');
                                                                } else {
                                                                    container.addClass('hidden');
                                                                }
                                                            }",
                                                "depdrop:change" => "function(event, id, value, count) { 
                                                                let container = $(this).closest('.3rd-level');
                                                                if(count > 0) {
                                                                    container.removeClass('hidden');
                                                                } else {
                                                                    container.addClass('hidden');
                                                                }
                                                            }",
                                            ],
                                            'options' => [
                                                'class' => 'form-control',
                                            ]
                                        ])->label(false); ?>
                                        <?= Html::hiddenInput('depdrop-helperx', $model->subcategory_id, ['id' => 'depdrop-helperx']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="formgig-block">
                    <div class="row set-item">
                        <?= $form->field($model, 'tags', [
                            'template' => '
								    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
								        <div class="set-title">
								            {label}
								        </div>
								    </div>
								    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
								        {input}{error}
								        <div class="counttext"> ' . Yii::t('account', 'min. three tags separated with commas') . '</div>
								    </div>
								'
                        ])->textInput([
                            'id' => 'tags',
                            'class' => 'readsym'
                        ]) ?>
                    </div>
                </div>
                <div class="row formgig-block show-notes">
                    <div class="form-group">
                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                            <div class="set-title">
                                <label class="control-label"
                                       for="postproductform-uploaded_images"><?= Yii::t('account', 'Photos') ?></label>
                                <?= Yii::t('account', '(Here you can upload photos)') ?>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                            <div class="cgp-upload-files">
                                <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                    'id' => 'file-upload-input',
                                    'pluginOptions' => [
                                        'overwriteInitial' => true,
                                        'initialPreview' => $model->image ? $model->portfolio->getThumb() : [],
                                        'initialPreviewConfig' => !empty($model->image) ? [[
                                            'caption' => basename($model->image),
                                            'url' => Url::toRoute('/image/delete'),
                                            'key' => $model->portfolio->user_id
                                        ]] : [],
                                    ]
                                ])) ?>
                                <?= $form->field($model, 'image')->hiddenInput([
                                    'id' => 'image-input',
                                    'value' => $model->image,
                                    'data-key' => $model->portfolio->user_id
                                ])->label(false); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row formgig-block show-notes">
                    <div class="additional-gig-item">
                        <?php foreach ($model->attachments as $key => $value) { ?>
                            <?php /** @var Attachment $value */ ?>
                            <div class="cgb-optbox">
                                <div class="row line-add-extra">
                                    <div class="col-xs-12">
                                        <?= $form->field($value, "[{$key}]description")->textarea([
                                            'placeholder' => Yii::t('account', 'Description for extra image')
                                        ])->label(false) ?>
                                    </div>
                                    <div class="col-xs-12 extra-recommend chetotam" data-chetotam="<?= $key ?>">
                                        <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                            'id' => "file-upload-input-{$key}",
                                            'options' => ['class' => 'file-upload-input-extra'],
                                            'pluginOptions' => [
                                                'overwriteInitial' => true,
                                                'initialPreview' => $value->content ? $value->getThumb() : [],
                                                'initialPreviewConfig' => !empty($value->content) ? [[
                                                    'caption' => basename($value->content),
                                                    'url' => Url::toRoute('/image/delete'),
                                                    'key' => "image_init_{$value->id}"
                                                ]] : [],
                                            ]
                                        ])) ?>
                                    </div>
                                </div>
                                <div class="delete-extra">
                                    <i class="fa fa-times"></i>
                                    <?= Html::a(Yii::t('account', 'Delete'), '#', ['class' => 'link-blue', 'data-id' => $value->id]) ?>
                                </div>
                                <hr>
                            </div>
                        <?php } ?>
                        <div class="extra-container"></div>
                        <div class="images-container">
                            <?php foreach ($model->attachments as $key => $image) {
                                /* @var $image Attachment */
                                echo $form->field($image, "[{$key}]content")->hiddenInput([
                                    'value' => $image->content,
                                    'data-key' => 'image_init_' . $image->id
                                ])->label(false);
                            } ?>
                        </div>
                        <div class="additional-gig-head" style="display: block;">
                            <i class="fa fa-plus"></i>
                            <a class="load-portfolio-extra" href=""><?= Yii::t('account', 'Add Extra Image') ?></a>
                        </div>
                    </div>
                </div>
                <div class="formpost-block row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'status')->radioList(UserPortfolio::getStatusLabels()) ?>
                    </div>
                </div>
                <div class="container box box-success" style="padding:5px;">
                    <?= Html::submitButton(Yii::t('app', 'Save'), [
                        'id' => 'submit-button',
                        'class' => 'btn btn-success pull-right'
                    ]) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?
$userId = Yii::$app->user->identity->getId();
$attachmentCount = count($model->attachments);
$renderExtraRoute = Url::to(['/user-portfolio/render-extra']);;
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'portfolio_tag']);

$script = <<<JS
	let uid = $userId,
    attachments = $attachmentCount,
    iterator = $attachmentCount;

$(".load-portfolio-extra").on("click", function(e) {
    e.preventDefault();
    let container = $(this).closest(".formgig-block").find(".extra-container");

    $.post('$renderExtraRoute', {
        iterator: iterator
    }, function(data) {
        if (data.success === true) {
            container.append(data.html);
        } else {
            alertCall('error', data.message);
        }
    }, "json");
    iterator++;
});

$(".link-blue").on("click", function() {
    let id = $(this).data('id');
    $(this).closest(".cgb-optbox").remove();
    $(".images-container").find("input[data-key=image_init_" + id + "]").remove();
});

$('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
    let response = data.response;
    $('#image-input').val(response.uploadedPath).data('key', response.imageKey); 
    $("#portfolio-form").submit(); 
}).on('filedeleted', function(event, key) {
    $('#image-input').val(null);
});

$(".file-upload-input-extra").on("fileuploaded", function(event, data, previewId, index) {
    let response = data.response;
    let id = $(this).closest('.chetotam').data('chetotam');
    $('[name="Attachment[' + id + '][content]"]').remove();
    $(".images-container").append("<input name='Attachment[" + id + "][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
}).on("filedeleted", function(event, key) {
    $(".images-container").find("input[data-key='" + key + "']").remove();
}).on("filebatchuploadcomplete", function(event, files, extra) {
    $("#portfolio-form").submit();
});

$('#portfolio-form').on('beforeSubmit', function() { 
    let returnValue = true;
    $(".file-upload-input, .file-upload-input-extra").each(function(index) {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            returnValue = false;
            return returnValue;
        }
    });
    
    if (!returnValue) {
        return false;
    }
});

$("#tags").selectize({
    valueField: "value",
    labelField: "value",
    searchField: ["value"],
    plugins: ["remove_button"],
    persist: false,
    create: true,
    maxOptions: 10,
    render: {
        option_create: function(data, escape) {
            return "<div class=\"create\">Add&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
        }
    },
    "load": function(query, callback) {
        if (!query.length)
            return callback();
        $.post("$tagListUrl", {
                query: encodeURIComponent(query)
            },
            function(data) {
                callback(data);
            }).fail(function() {
            callback();
        });
    }
})

JS;

$this->registerJs($script);