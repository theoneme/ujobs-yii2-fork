<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.05.2018
 * Time: 15:49
 */

use backend\models\PostPortfolioForm;
use common\models\UserPortfolio;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostPortfolioForm
 * @var $action string
 * @var $locales array
 * @var $currencySymbols array
 * @var $currencies array
 * @var $isAjax boolean
 */

?>
    <div class="user-withdrawal-index">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>

            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'data-pjax' => false,
                        'class' => ($isAjax ? 'ajax-submit' : ''),
                        'data-pj-container' => 'grid-container'
                    ],

                    'id' => 'portfolio-form',
                ]); ?>
                <?= Html::hiddenInput('id', $model->portfolio->id, ['id' => 'portfolio-id']) ?>
                <div class="formgig-block">
                    <div class="row set-item">
                        <?= $form->field($model, 'locale', [
                            'template' => '
                            <div class="col-md-2 col-sm-3 col-xs-3 leftset">
                                <div class="set-title">
                                    {label}
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-3 rightset">
                                {input}
                                {error}
                            </div>
                        ',
                            'options' => ['tag' => false]
                        ])->dropDownList(Yii::$app->params['languages'])->label('Язык портфолио') ?>
                    </div>
                    <div class="row set-item">
                        <?= $form->field($model, "title", [
                            'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                        <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('account', 'Max') . '</div>
                                    </div>
                                '
                        ])->textarea([
                            'class' => 'readsym bigsymb',
                            'placeholder' => Yii::t('account', 'Name your portfolio')
                        ])
                        ?>
                    </div>
                </div>
                <div class="formgig-block">
                    <div class="row set-item">
                        <?= $form->field($model, "description", [
                            'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->widget(Widget::class, [
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 200,
                                'plugins' => [
                                    'fullscreen',
                                    'fontsize',
                                    'fontcolor'
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <div class="formpost-block row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'status')->radioList(UserPortfolio::getStatusLabels()) ?>
                    </div>
                </div>
                <div class="container box box-success" style="padding:5px;">
                    <?= Html::submitButton(Yii::t('app', 'Save'), [
                        'id' => 'submit-button',
                        'class' => 'btn btn-success pull-right'
                    ]) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php $userId = Yii::$app->user->identity->getId();
$attachmentCount = count($model->attachments);
$renderExtraRoute = Url::to(['/user-portfolio/render-extra']);;
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'portfolio_tag']);

$script = <<<JS
	let uid = $userId,
    attachments = $attachmentCount,
    iterator = $attachmentCount;

$(".load-portfolio-extra").on("click", function(e) {
    e.preventDefault();
    let container = $(this).closest(".formgig-block").find(".extra-container");

    $.post('$renderExtraRoute', {
        iterator: iterator
    }, function(data) {
        if (data.success === true) {
            container.append(data.html);
        } else {
            alertCall('error', data.message);
        }
    }, "json");
    iterator++;
});

$(".link-blue").on("click", function() {
    let id = $(this).data('id');
    $(this).closest(".cgb-optbox").remove();
    $(".images-container").find("input[data-key=image_init_" + id + "]").remove();
});

$('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
    let response = data.response;
    $('#image-input').val(response.uploadedPath).data('key', response.imageKey); 
    $("#portfolio-form").submit(); 
}).on('filedeleted', function(event, key) {
    $('#image-input').val(null);
});

$(".file-upload-input-extra").on("fileuploaded", function(event, data, previewId, index) {
    let response = data.response;
    let id = $(this).closest('.chetotam').data('chetotam');
    $('[name="Attachment[' + id + '][content]"]').remove();
    $(".images-container").append("<input name='Attachment[" + id + "][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
}).on("filedeleted", function(event, key) {
    $(".images-container").find("input[data-key='" + key + "']").remove();
}).on("filebatchuploadcomplete", function(event, files, extra) {
    $("#portfolio-form").submit();
});

$('#portfolio-form').on('beforeSubmit', function() { 
    let returnValue = true;
    $(".file-upload-input, .file-upload-input-extra").each(function(index) {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            returnValue = false;
            return returnValue;
        }
    });
    
    if (!returnValue) {
        return false;
    }
});

$("#tags").selectize({
    valueField: "value",
    labelField: "value",
    searchField: ["value"],
    plugins: ["remove_button"],
    persist: false,
    create: true,
    maxOptions: 10,
    render: {
        option_create: function(data, escape) {
            return "<div class=\"create\">Add&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
        }
    },
    "load": function(query, callback) {
        if (!query.length)
            return callback();
        $.post("$tagListUrl", {
                query: encodeURIComponent(query)
            },
            function(data) {
                callback(data);
            }).fail(function() {
            callback();
        });
    }
})

JS;

$this->registerJs($script);