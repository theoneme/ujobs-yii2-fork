<?php

use common\models\Conversation;
use common\models\Offer;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ConversationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Conversations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'id' => 'user-grid',
                'columns' => [
                    [
                        'header' => 'Исполнитель',
                        'attribute' => 'from',
                        'value' => function ($model) {
                            return isset($model->firstMessage->offer->from->profile) ? Html::a(Html::img($model->firstMessage->offer->from->profile->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->firstMessage->offer->from->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->firstMessage->offer->from_id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->firstMessage->offer->from))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            ) : '';
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Заказчик',
                        'attribute' => 'to',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return isset($model->firstMessage->offer->from->profile) ? Html::a(Html::img($model->firstMessage->offer->to->profile->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->firstMessage->offer->to->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->firstMessage->offer->to_id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->firstMessage->offer->to))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            ) : '';
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Сообщение',
                        'attribute' => 'message',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return $model->firstMessage->message;
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Бюджет',
                        'attribute' => 'price',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return $model->firstMessage->offer ? $model->firstMessage->offer->getPrice() : '';
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'status',
                        'header' => 'Статус',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return $model->firstMessage->offer ? $model->firstMessage->offer->getStatusLabel(true) : '';
                        },
                        'format' => 'raw',
                        'filter' => [
                            Offer::STATUS_SENT => 'Отправлен',
                            Offer::STATUS_DECLINED => 'Отклонен',
                            Offer::STATUS_CANCELED => 'Отозван',
                            Offer::STATUS_ACCEPTED => 'Принят',
                        ]
                    ],
                    [
                        'header' => 'Дата',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return $model->firstMessage->created_at;
                        },
                    ],
                    [
                        'header' => 'Переписка',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return Html::a("Переписка (" . count($model->messages) . ")",
                                ['/message/index', 'conversation_id' => $model->id],
                                [
                                    'title' => Yii::t('app', 'Individual order `{0}`', [$model->firstMessage->offer ? $model->firstMessage->offer->subject : '']),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>