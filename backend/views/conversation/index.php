<?php

use common\models\Conversation;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ConversationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Conversations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'id' => 'user-grid',
                'columns' => [
                    [
                        'header' => 'Отправитель',
                        'attribute' => 'from',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            if ($model->lastMessage->user->is_company) {
                                return $model->lastMessage->user->company
                                    ? Html::img($model->lastMessage->user->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->lastMessage->user->getSellerName()
                                    : '';
                            } else {
                                return $model->lastMessage->user->profile ? Html::a(Html::img($model->lastMessage->user->profile->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->lastMessage->user->getSellerName(),
                                    ['/user/admin/update-ajax', 'id' => $model->lastMessage->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->lastMessage->user))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                ) : '';
                            }
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Получатель',
                        'attribute' => 'to',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            if ($model->lastMessage->anotherMember->user->is_company) {
                                return $model->lastMessage->anotherMember->user->company
                                    ? Html::img($model->lastMessage->anotherMember->user->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->lastMessage->anotherMember->user->getSellerName()
                                    : '';
                            } else {
                                return $model->lastMessage->anotherMember->user->profile ? Html::a(Html::img($model->lastMessage->anotherMember->user->profile->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->lastMessage->anotherMember->user->getSellerName(),
                                    ['/user/admin/update-ajax', 'id' => $model->lastMessage->anotherMember->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->lastMessage->anotherMember->user))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                ) : '';
                            }
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Сообщение',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return $model->lastMessage->message;
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Последнее сообщение',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return $model->lastMessage->created_at;
                        },
                    ],
                    [
                        'header' => 'Переписка',
                        'value' => function ($model) {
                            /* @var $model Conversation */
                            return Html::a("Переписка (" . count($model->messages) . ")",
                                ['/message/index', 'conversation_id' => $model->id],
                                [
                                    'title' => Yii::t('app', 'View {0}', 'Messages'),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>