<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.12.2016
 * Time: 17:26
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Job */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Job',
    ]) . $model->job->translation->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job->id, 'url' => ['view', 'id' => $model->job->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="job-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-test', [
        'model' => $model,
        'rootCategoriesList' => $rootCategoriesList
    ]) ?>

</div>