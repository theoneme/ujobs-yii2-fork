<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.12.2016
 * Time: 16:30
 */

use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use backend\models\PostRequestForm;
use common\helpers\FileInputHelper;
use common\models\Attachment;
use common\models\Job;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $rootCategoriesList array
 * @var $model PostRequestForm
 * @var $isAjax boolean
 */
MyCropAsset::register($this);
SelectizeAsset::register($this);
?>
<div class="page-form">
    <?php \yii\widgets\Pjax::begin([
        'id' => 'new_tender',
        'enablePushState' => !$isAjax
    ]) ?>
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
            'class' => ($isAjax ? 'ajax-submit' : '') . " formpost",
            'data-pj-container' => 'tenders-pjax'
        ],
        'id' => 'job-form',
    ]); ?>
    <div class="box box-primary container">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Yii::t('app', 'Overview') ?></h3>
        </div>
        <div class="clearfix">
            <div class="formgig-block">
                <div class="set-item" style="margin-left: 20px">
                    <?= $form->field($model, 'type')->radioList([
                        Job::TYPE_TENDER => Yii::t('app', 'One Time Service. Pay upon completion.'),
                        Job::TYPE_ADVANCED_TENDER => Yii::t('app', 'Long Term service. Weekly, Bi-weekly, Monthly or contract.')
                    ], [
                        'class' => 'checkbox-enchance text-left',
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $chk = $checked ? 'checked' : '';
                            $output = "<div class='radio'><input {$chk} value=\"{$value}\" id=\"payment_{$index}\" type='radio' name=\"{$name}\"/><label for=\"payment_{$index}\">{$label}</label></div>";
                            return $output;
                        }
                    ])->label(false)->error(false) ?>
                </div>
            </div>
            <div class="formgig-block">
                <div class="row set-item">
                    <?= $form->field($model, 'specialties', [
                        'template' => '
                            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                <div class="set-title">
                                {label}
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                {input}{error}
                                <div class="counttext"> ' . Yii::t('app', 'Minimum One specialty, if more separate with commas') . '</div>
                            </div>
                        '
                    ])->textInput([
                        'id' => 'specialties',
                        'class' => 'readsym'
                    ]) ?>
                </div>
                <div class="form-box">
                    <?= $form->field($model, 'locale')->dropDownList(Yii::$app->params['languages'])->label('Язык заявки') ?>
                </div>
            </div>
            <div class="formpost-block">
                <div class="form-group">
                    <label>
                        <?= Yii::t('app', "Describe the service you're looking for - please be as detailed as possible") ?>
                        :
                    </label>
                    <div class="form-box">
                        <?= $form->field($model, 'title', [
                        ])->textInput([
                            'class' => 'readsym form-control',
                            'placeholder' => Yii::t('app', 'I want to order')
                        ])->label(false) ?>

                        <?= $form->field($model, "description")->widget(Widget::class, [
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 200,
                                'plugins' => [
                                    'fullscreen',
                                    'fontsize',
                                    'fontcolor'
                                ]
                            ]
                        ])->label(false);
                        ?>
                    </div>
                    <div class="form-box">
                        <div class="cgp-upload-files">
                            <?= FileInput::widget(
                                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                    'id' => 'file-upload-input',
                                    'options' => ['accept' => 'image/*', 'multiple' => true, 'class' => 'file-upload-input'],
                                    'pluginOptions' => [
                                        'overwriteInitial' => false,
                                        'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                            return $var->getThumb();
                                        }, $model->attachments) : [],
                                        'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                                        'previewThumbTags' => [
                                            '{actions}' => '{actions}',
                                        ],
                                    ]
                                ])
                            ) ?>
                            <div class="images-container">
                                <?php foreach ($model->attachments as $key => $image) {
                                    /* @var $image Attachment */
                                    echo $form->field($image, "[{$key}]content")->hiddenInput([
                                        'value' => $image->content,
                                        'data-key' => 'image_init_' . $image->id
                                    ])->label(false);
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="formpost-block">
                <div class="form-group">
                    <label>
                        <?= Yii::t('app', 'Choose a category') ?>: </label>

                    <div class="form-box">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                    'prompt' => ' -- ' . Yii::t('app', 'Select A Category'),
                                    'class' => 'form-control'
                                ])->label(false); ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                    'pluginOptions' => [
                                        'initialize' => true,
                                        'depends' => ['postrequestform-parent_category_id'],
                                        'placeholder' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                                        'url' => Url::to(['/category/subcat']),
                                        'params' => ['depdrop-helper']
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                    ]
                                ])->label(false); ?>

                                <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="formpost-block">
                <div class="form-group">
                    <label> <?= Yii::t('app', 'Once you place your order, when would you like your service delivered?') ?> </label>

                    <div class="form-box">
                        <div class="row">
                            <div class="col-sm-4">
                                <?= $form->field($model, 'period_type')->dropDownList([
                                    'start' => Yii::t('app', 'Start from'),
                                    'end' => Yii::t('app', 'End on'),
                                    'range' => Yii::t('app', 'Do in the period')
                                ])->label(false) ?>
                            </div>
                            <div class="col-sm-8">
                                <?= $form->field($model, 'date_start')->widget(DatePicker::class, [
                                    'options' => ['placeholder' => Yii::t('app', 'Set start date'), 'class' => 'col-sm-4'],
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ]
                                ])->label(false); ?>

                                <?= $form->field($model, 'date_end')->widget(DatePicker::class, [
                                    'options' => ['placeholder' => Yii::t('app', 'Set end date'), 'class' => 'col-sm-4'],
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ]
                                ])->label(false); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="formpost-block">
                <div class="form-group">
                    <label> <?= Yii::t('app', 'What is your budget for this service?') ?> </label>

                    <div class="form-box">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?= $form->field($model, 'price')->textInput(['placeholder' => '9 999', 'id' => 'price-budget'])->label(false); ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?= $form->field($model, 'contract_price', ['template' => '{input}{label}'])
                                    ->checkbox(['id' => 'contract-price-checkbox'], false)
                                    ->label(Yii::t('app', 'Contract price'))
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="formpost-block clearfix">
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->radioList(Job::getStatusLabels(), [
                        'class' => 'blocked'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="container box box-success" style="padding:5px;">
            <?= Html::submitButton(Yii::t('app', 'Publish Request'), [
                'id' => 'submit-button',
                'class' => 'btn btn-success'
            ]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php $attachments = count($model->attachments);
$specialtyListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$script = <<<JS
	let attachments = {$attachments};

    $("#contract-price-checkbox").on("change", function() {
        if ($(this).prop("checked")) {
            $("#price-budget").parent().removeClass("has-error");
            $("#price-budget").siblings(".help-block").html("");
            $("#price-budget").val("");
            $("#price-weekly").val(0);
        } else {
            $("#price-budget").prop("disabled", false);
        }
    });
    $("#price-budget").on("keyup", function() {
        let value = $(this).val();
        value = parseInt(value.replace(/\s/g, ""));
        value = value > 0 ? value : 0;
        if (value === 0) {
            $("#contract-price-checkbox").prop("checked", true);
        } else {
            $("#contract-price-checkbox").prop("checked", false);
        }
    });
    $("#contract-price-checkbox").trigger("change");

    let hasFileUploadError = false;
    $("#file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
        attachments++;
    }).on("filedeleted", function(event, key) {
        $(".images-container input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) { 
            $(this).closest("form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) { 
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });

    $("#postrequestform-periodtype").on("change", function() {
        if ($(this).val() === "start") {
            $(".field-postrequestform-datestart").show();
            $(".field-postrequestform-dateend").hide();
        } else if ($(this).val() === "end") {
            $(".field-postrequestform-datestart").hide();
            $(".field-postrequestform-dateend").show();
        } else {
            $(".field-postrequestform-datestart").show();
            $(".field-postrequestform-dateend").show();
        }
    });

    $("#postrequestform-periodtype").trigger("change");
    $("#submit-button").on("click", function(e) {
        if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
            $("#file-upload-input").fileinput("upload");
            return false;
        } else {
            return true;
        }
    });

    $("#specialties").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("{$specialtyListUrl}", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script); ?>
<?php Pjax::end() ?>
