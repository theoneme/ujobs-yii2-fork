<?php

use common\components\CurrencyHelper;
use common\modules\store\models\PaymentMethod;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Payment;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('model', 'Payments');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="payment-history-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(['id' => 'payment-pjax']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'id' => 'payment-grid',
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                            'options' => ['style' => 'width: 72px']
                        ],
                        [
                            'attribute' => 'created_at',
                            'contentOptions' => ['style' => 'width: 200px'],
                            'options' => ['style' => 'width: 200px'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->created_at);
                            }
                        ],
                        [
                            'header' => 'Покупатель',
                            'format' => 'raw',
                            'attribute' => 'customer',
                            'contentOptions' => ['style' => 'width: 100px; white-space: normal; font-size: 12px;'],
                            'options' => ['style' => 'width: 100px'],
                            'value' => function ($model) {
                                /* @var $model Payment */
                                return Html::a($model->user->getSellerName(),
                                        ['/user/admin/update-ajax', 'id' => $model->user_id],
                                        [
                                            'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->user))->getShortName()),
                                            'data-pjax' => 0,
                                            'class' => 'modal-edit'
                                        ]
                                    ) .
                                    '<br><br>' .
                                    Html::a('Письма',
                                        Yii::$app->urlManagerFrontEnd->createUrl(['/account/inbox/start-support-conversation', 'user_id' => $model->user_id]),
                                        ['style' => 'font-weight:bold;', 'target' => '_blank']
                                    );
                            }
                        ],
                        [
                            'header' => 'Продавец',
                            'format' => 'raw',
                            'attribute' => 'seller',
                            'contentOptions' => ['style' => 'width: 92px; white-space: normal; font-size: 12px;'],
                            'options' => ['style' => 'width: 92px'],
                            'value' => function ($model) {
                                /* @var $model Payment */
                                if ($model->order) {
                                    return Html::a($model->order->seller->getSellerName(),
                                            ['/user/admin/update-ajax', 'id' => $model->order->seller->id],
                                            [
                                                'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->order->seller))->getShortName()),
                                                'data-pjax' => 0,
                                                'class' => 'modal-edit'
                                            ]
                                        ) .
                                        '<br><br>' .
                                        Html::a('Письма',
                                            Yii::$app->urlManagerFrontEnd->createUrl(['/account/inbox/start-support-conversation', 'user_id' => $model->order->seller_id]),
                                            ['style' => 'font-weight:bold;', 'target' => '_blank']
                                        );
                                } else {
                                    return 'uJobs';
                                }
                            }
                        ],
                        [
                            'header' => 'Сумма',
                            'attribute' => 'total',
                            'value' => function ($model) {
                                return CurrencyHelper::convertAndFormat($model->currency_code, Yii::$app->params['app_currency_code'], $model->total);
                            },
                            'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                            'options' => ['style' => 'width: 72px']
                        ],
                        [
                            'header' => 'Способ оплаты',
                            'attribute' => 'payment_method_id',
                            'value' => function ($model) {
                                return $model->paymentMethod->name;
                            },
                            'contentOptions' => ['style' => 'width: 300px'],
                            'options' => ['style' => 'width: 300px'],
                            'filter' => PaymentMethod::find()->select('title')->joinWith('translation')->where(['enabled' => true])->indexBy('id')->asArray()->column(),
                        ],
                        [
                            'header' => 'Статус',
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model) {
                                /* @var Payment $model */
                                return $model->getStatusLabel(true);
                            },
                            'filter' => Payment::getStatusLabels(),
                            'contentOptions' => ['style' => 'width: 82px; white-space: normal; font-size: 12px;'],
                            'options' => ['style' => 'width: 82px']
                        ],
                        [
                            'header' => 'За что оплата',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $output = null;
                                switch ($model->type) {
                                    case Payment::TYPE_ACCOUNT_REPLENISHMENT:
                                        $output = $this->render('partial/replenish-wrap', [
                                            'model' => $model
                                        ]);
                                        break;
                                    case Payment::TYPE_JOB_PAYMENT:
                                        $output = $this->render('partial/order-wrap', [
                                            'model' => $model
                                        ]);
                                        break;
                                    case Payment::TYPE_ACCESS_PAYMENT:
                                        $output = $this->render('partial/tariff-wrap', [
                                            'model' => $model
                                        ]);
                                        break;
                                    case Payment::TYPE_TENDER_PAYMENT:
                                        $output = $this->render('partial/tender-wrap', [
                                            'model' => $model
                                        ]);
                                        break;
                                    case Payment::TYPE_PRODUCT_PAYMENT:
                                        $output = $this->render('partial/product-wrap', [
                                            'model' => $model
                                        ]);
                                }
                                return $output;
                            }
                        ]
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>