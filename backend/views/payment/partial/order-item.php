<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.01.2017
 * Time: 11:21
 */

use common\components\CurrencyHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<tr>
    <td>
        <?= Html::a("Оплата заказа (ID " . $order->id. ")", Url::to(['/order/view', 'id' => $order->id]), [
            'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($order))->getShortName()),
            'data-pjax' => 0,
            'class' => 'modal-edit'
        ]);?>
    </td>
    <td>
        <?= CurrencyHelper::convertAndFormat($order->currency_code, 'RUB', $order->total)?>
    </td>
    <td>
        <?= $order->getStatusLabel(true) ?>
    </td>
</tr>