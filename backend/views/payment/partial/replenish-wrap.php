<?php
use common\components\CurrencyHelper;
use common\models\Payment;

/* @var $model Payment*/

?>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>
            Услуга
        </th>
        <th>
            Сумма
        </th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Пополнение счёта
            </td>
            <td>
                <?= CurrencyHelper::convertAndFormat($model->currency_code, 'RUB', $model->total)?>
            </td>
        </tr>
    </tbody>
</table>