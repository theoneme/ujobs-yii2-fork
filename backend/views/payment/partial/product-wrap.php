<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.09.2017
 * Time: 14:16
 */

use common\models\Payment;

/* @var Payment $model */

?>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>
            Товар + ID заказа
        </th>
        <th>
            Сумма
        </th>
        <th>
            Статус заказа
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($model->orders as $order) { ?>
        <?= $this->render('order-item', [
            'order' => $order
        ]);?>
    <?php } ?>
    </tbody>
</table>