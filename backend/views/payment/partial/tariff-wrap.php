<?php
use common\models\Payment;

/* @var $model Payment*/

?>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>
            Услуга
        </th>
        <th>
            Длительность
        </th>
        <th>
            Дата окончания
        </th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Оплата тарифа "<?= $model->tariff->tariff->title ?>"
            </td>
            <td>
                <?= $model->tariff->duration / (60 * 60 * 24) . ' дней';?>
            </td>
            <td>
                <?= Yii::$app->formatter->asDatetime($model->tariff->expires_at, 'php:d-m-Y') ?>
            </td>
        </tr>
    </tbody>
</table>