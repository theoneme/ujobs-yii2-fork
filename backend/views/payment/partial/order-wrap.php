<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.01.2017
 * Time: 11:23
 */

use common\models\Payment;

/* @var Payment $model */

?>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>
            Услуга + ID заказа
        </th>
        <th>
            Сумма
        </th>
        <th>
            Статус заказа
        </th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($model->orders as $order) { ?>
            <?= $this->render('order-item', [
                'order' => $order
            ]);?>
        <?php } ?>
    </tbody>
</table>