<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.03.2017
 * Time: 11:39
 */

use common\models\Payment;

/* @var $model Payment */

?>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>
            Заявка (временный работник)
        </th>
        <th>
            Цена
        </th>
        <th>
            Статус
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model->orders as $order) { ?>
        <?= $this->render('order-item', [
            'order' => $order
        ]); ?>
    <?php } ?>
    </tbody>
</table>