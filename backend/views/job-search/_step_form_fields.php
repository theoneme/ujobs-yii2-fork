<?php

use common\models\ContentTranslation;
use common\models\JobSearchStep;
use yii\bootstrap\Tabs;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model JobSearchStep
 */

?>
<?= $form->field($model, 'tree_id')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'previous_step_id')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'show_prices')->checkbox([], false)->label('Выводить цены?') ?>
<?= Tabs::widget([
    'items' =>
        array_map(function ($locale, $language) use ($form, $model) {
            return [
                'label' => $language,
                'content' => $this->renderAjax('_content_translation', [
                    'model' => $model->translations[$locale] ?? new ContentTranslation(),
                    'form' => $form,
                    'locale' => $locale,
                    'group' => 'JobSearchStep'
                ]),
                'active' => $locale === 'ru-RU'
            ];
        }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
]);
?>
