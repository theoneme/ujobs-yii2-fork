<?php

use vova07\imperavi\Widget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $locale string */
/* @var $form ActiveForm */
/* @var $group $string */

?>
<div class="asdf">
    <?= $form->field($model, "[$group][$locale]id")->hiddenInput()->label(false); ?>
	<?= $form->field($model, "[$group][$locale]title") ?>
	<?= $group === 'JobSearchStep' || $group === 'JobSearchStepMeasure' ? $form->field($model, "[$group][$locale]content_prev") : ''?>
	<?= $group === 'JobSearchStep' ? $form->field($model, "[$group][$locale]content")->widget(Widget::class, [
		'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'fullscreen',
                'fontsize',
                'fontcolor'
            ],
            'buttonsHide' => ['formatting', 'link']
		]
	]) : '';
	?>
    <?= $form->field($model, "[$group][$locale]locale")->hiddenInput(['value' => $locale])->label(false); ?>
</div>