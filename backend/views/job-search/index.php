<?php

use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use common\models\JobSearchTree;
use kartik\tree\TreeViewAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $models JobSearchTree[] */

$this->title = 'Деревья поиска работы';
$this->params['breadcrumbs'][] = $this->title;
TreeViewAsset::register($this);
SelectizeAsset::register($this);
MyCropAsset::register($this);
?>
    <div class="category-index">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-control kv-tree-wrapper">
                            <div class="kv-header-container" style="border: 1px solid #d2d6de">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="kv-heading-container">Деревья</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="kv-search-sm kv-search-container">
                                            <span class="close kv-search-clear"
                                                  title="Очистить результаты поиска">×</span>
                                            <input class="form-control input-sm kv-search-input" name="kv-tree-search"
                                                   placeholder="Поиск..." type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="w0-tree" class="kv-tree-container" style="height:600px; border: 1px solid #d2d6de">
                                <ul class="kv-tree">
                                    <?php Pjax::begin(['id' => 'tree-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
                                    <?php foreach ($models as $tree) {
                                        echo $this->render('list-items/tree', ['model' => $tree]);
                                    } ?>
                                    <?php Pjax::end(); ?>
                                </ul>
                            </div>
                            <div class="kv-footer-container">
                                <div id="w0-toolbar" class="kv-toolbar-container" role="toolbar">
                                    <div class="btn-group-sm btn-group" role="group">
                                        <button data-url="<?= Url::to(['/job-search/tree']) ?>" type="button"
                                                class="btn btn-default kv-toolbar-btn kv-create-root hint--top" title=""
                                                data-hint="Добавить новое дерево">
                                            <span class="kv-icon-10 fa fa-tree"></span>
                                        </button>
                                        <button type="button" class="btn btn-default kv-toolbar-btn kv-create hint--top"
                                                title="" data-hint="Добавить новый шаг">
                                            <span class="kv-icon-10 fa fa-plus"></span>
                                        </button>
                                        <button type="button" class="btn btn-default kv-toolbar-btn kv-remove hint--top"
                                                title="" data-hint="Удалить">
                                            <span class="kv-icon-10 fa fa-trash"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div id="w0-detail" class="kv-detail-container">
                            <div class="kv-node-message">Выберите элемент дерева для редактирования</div>
                            <div class="edit-form-container">
                                <?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $script = <<<JS
    $(document).on('click', '.kv-node-expand, .kv-node-collapse', function(){
        $(this).closest('.kv-parent').toggleClass('kv-collapsed');
        return false;
    });
    $(document).on('click', '.kv-node-detail', function() {
        $('.new-tree-node').remove();
        $('.kv-node-detail').removeClass('kv-focussed');
        $(this).addClass('kv-focussed');
        $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
 	    $.pjax.reload({container: "#edit-pjax-container", url: $(this).closest('li').data('url'), replace: false});
    });
    $(document).on('click', '.kv-create-root', function() {
        $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
        renderNewNode('Новое дерево', 'tree', $('#tree-pjax-container'));
 	    $.pjax.reload({container: "#edit-pjax-container", url: $(this).data('url'), replace: false});
    });
    $(document).on('click', '.kv-create', function() {
        var li = $('.kv-focussed').closest('li');
        if (li.length) {
            if (!li.hasClass('final-step')) {
                $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
                renderNewNode('Новый шаг', 'file', li.children('ul'));
                $.pjax.reload({container: "#edit-pjax-container", url: li.data('child-url'), replace: false});  
            }
            else {
                alertCall('warning', 'Данный шаг является финальным, к нему нельзя добавить следующий');
            }
        }
        else {
            alertCall('warning', 'Выберите элемент дерева к которому хотите добавить дочерний элемент');
        }
    });
    $(document).on('click', '.kv-remove', function() {
        var li = $('.kv-focussed').closest('li');
        if (li.length) {
            if (confirm('Вместе с элементом дерева будут удалены все дочерние элементы. Вы уверены что хотите удалить"' + $.trim($('.kv-focussed .kv-node-label').text()) + '"?')){
                $.post(li.data('delete-url'), {},
                    function(data) {
                        $('.kv-detail-container').removeClass('kv-editing kv-loading');
                        $.pjax.reload({container: "#tree-pjax-container"});
                    }
                );
            }
        }
        else {
            alertCall('warning', 'Выберите элемент дерева который хотите удалить');
        }
    });
    
    $('#edit-pjax-container').on('pjax:success', function(event, data, status, xhr, options) {
        $('.kv-detail-container').removeClass('kv-loading').addClass('kv-editing');
    }).on('pjax:error', function (event) {
		event.preventDefault();
	});
    
    $(document).on('beforeSubmit', '#edit-pjax-container form', function () {
        var self = $(this);
        var formData = new FormData($(this)[0]);
        
        $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
        
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    $('.kv-detail-container').removeClass('kv-editing kv-loading');
                    $('#tree-pjax-container').one('pjax:success', function(event, data, status, xhr, options) {
                        let treeItem = $('#' + result.id);
                        treeItem.parents('.kv-parent').removeClass('kv-collapsed');
                        treeItem.find('.kv-node-detail').first().addClass('kv-focussed');
                        $('#tree-pjax-container').animate({
                            scrollTop: treeItem.position().top - $('#tree-pjax-container').position().top
                        }, 800);
                    });
                    $.pjax.reload({container: "#tree-pjax-container"});
                    alertCall('success', 'Элемент дерева успешно сохранен');
                }
                else {
                    alertCall('error', 'Ошибка при сохранении');
                }
            }
        });
        return false;
    });
    
    $(document).on("afterValidate", '#edit-pjax-container form', function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
            let firstError = $(this).find(".has-error").first();
            let firstErrorTab = firstError.closest('.tab-pane');
            if (firstErrorTab.length) {
                $('a[href="#' + firstErrorTab.attr('id') + '"]').tab('show');
            }
            $("html, body").animate({scrollTop: firstError.offset().top}, 800);
        }
    });
    
    function renderNewNode(text, icon, prependTo) {
        prependTo.prepend(
            '<li class="new-tree-node">' +
                '<div class="kv-tree-list" tabindex="-1">' +
                    '<div class="kv-node-detail" tabindex="-1">' +
                        '<span class="text-info kv-node-icon kv-icon-child">' +
                            '<span class="fa fa-' + icon +'"></span>' +
                        '</span>' +
                        '<span class="kv-node-label">' +
                            text +
                        '</span>' +
                    '</div>' +
                '</div>' +
           '</li>'
        );
        $('.kv-node-detail').removeClass('kv-focussed');
        $('.new-tree-node .kv-node-detail').addClass('kv-focussed');
        $('.new-tree-node').parents('.kv-parent').removeClass('kv-collapsed');
    }
JS;
$this->registerJs($script);
