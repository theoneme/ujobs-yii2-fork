<?php

use common\models\JobSearchStep;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model JobSearchStep */
/* @var $form ActiveForm */
/* @var $create bool */

?>
<?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['/job-search/step', 'id' => $model->id],
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
        ],
        'id' => 'step-form',
    ]); ?>
    <div class="kv-detail-heading">
        <div class="kv-detail-crumbs">
            <span class="kv-crumb-active"><?= $create ? 'Добавление' : 'Редактирование'?> шага</span>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary" title="" data-original-title="Сохранить">
                    <i class="glyphicon glyphicon-floppy-disk"></i>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="category-form">
        <div class="box box-danger">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $form->field($model, 'id')->textInput(['readonly' => true])->label('ID шага') ?>
                    <?= $this->renderAjax('_step_form_fields', ['model' => $model, 'form' => $form])?>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= Html::submitButton('Сохранить', ['class' => 'pull-right btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

