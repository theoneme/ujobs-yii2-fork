<?php

use common\models\JobSearchTree;
use yii\bootstrap\Tabs;
use common\models\ContentTranslation;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model JobSearchTree */
/* @var $form ActiveForm */
/* @var $create bool */

?>
<?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['/job-search/tree', 'id' => $model->id],
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
        ],
        'id' => 'tree-form',
    ]); ?>
    <div class="kv-detail-heading">
        <div class="kv-detail-crumbs">
            <span class="kv-crumb-active"><?= $create ? 'Добавление' : 'Редактирование'?> дерева</span>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary" title="" data-original-title="Сохранить">
                    <i class="glyphicon glyphicon-floppy-disk"></i>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="category-form">
        <div class="box ">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $form->field($model, 'name') ?>
                </div>
            </div>
        </div>
        <div class="box box-danger">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $form->field($model, 'entityData')->textarea([
                        'id' => 'tree-to-entity',
                        'class' => 'readsym'
                    ])->label('Показывать в категориях, тегах:') ?>
                </div>
            </div>
        </div>
<!--        <div class="box box-danger">-->
<!--            <div class="box-body">-->
<!--                <div class="col-xs-12">-->
<!--                    --><?//= Tabs::widget([
//                        'items' =>
//                            array_map(function ($locale, $language) use ($form, $model) {
//                                return [
//                                    'label' => $language,
//                                    'content' => $this->renderAjax('_content_translation', [
//                                        'model' => $model->translations[$locale] ?? new ContentTranslation(),
//                                        'form' => $form,
//                                        'locale' => $locale,
//                                        'group' => 'JobSearchTree'
//                                    ]),
//                                    'active' => $locale === 'ru-RU'
//                                ];
//                            }, ['ru-RU'], ['Russian'])
//                    ]);
//                    ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="box">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= Html::submitButton('Сохранить', ['class' => 'pull-right btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <?php
    $options = json_encode(array_map(
        function($var) {return ['value' => $var->entity . '-' . $var->entity_content, 'label' => $var->getEntityLabel()];},
        $model->entities
    ));
    $values = json_encode(array_map(
        function($var) {return $var->entity . '-' . $var->entity_content;},
        $model->entities
    ));
    $loadUrl = Url::to(['/job-search/entity-list']);
    $script = <<<JS
        $("#tree-to-entity").selectize({
            options: $options,
            items: $values,
            create: false,
            maxItems: null,
            valueField: "value",
            labelField: "label",
            searchField: ["label"],
            plugins: ["remove_button"],
            persist: false,
            maxOptions: 10,
            "load": function(query, callback) {
                if (!query.length)
                    return callback();
                $.post("$loadUrl", 
                    {query: encodeURIComponent(query)},
                    function(data) {
                        callback(data);
                    }
                ).fail(function() {
                    callback();
                });
            }
        });
JS;
    $this->registerJs($script);
Pjax::end(); ?>

