<?php
use common\models\JobSearchOption;
use yii\helpers\Url;

/* @var $model JobSearchOption */
$editUrl = Url::to(['/job-search/option', 'id' => $model->id]);
$childUrl = Url::to(['/job-search/option', 'step_id' => $model->next_step_id]);
$deleteUrl = Url::to(['/job-search/delete-option', 'id' => $model->id]);
?>
<li id="option-<?= $model->id?>"
    class="<?= $model->type === JobSearchOption::TYPE_NEXT_STEP && $model->next_step_id && count($model->nextStep->options) ? 'kv-parent kv-collapsed' : '' ?> <?= $model->type !== JobSearchOption::TYPE_NEXT_STEP ? 'final-step' : ''?>"
    data-url="<?= $editUrl?>"
    data-child-url="<?= $childUrl?>"
    data-delete-url="<?= $deleteUrl?>"
>
    <div class="kv-tree-list" tabindex="-1">
        <div class="kv-node-indicators">
            <span class="text-muted kv-node-toggle">
                <span class="kv-node-expand">
                    <span class="fa fa-plus-square-o"></span>
                </span>
                <span class="kv-node-collapse">
                    <span class="fa fa-minus-square-o"></span>
                </span>
            </span>
        </div>
        <div class="kv-node-detail" tabindex="-1">
            <span class="text-warning kv-node-icon kv-icon-parent">
                <span class="fa fa-folder kv-node-closed"></span>
                <span class="fa fa-folder-open kv-node-opened"></span>
            </span>
            <span class="text-info kv-node-icon kv-icon-child">
                <span class="fa fa-file"></span>
            </span>
            <span class="kv-node-label">
                <?= $model->getLabel() ?>
            </span>
        </div>
    </div>
    <ul>
        <?php if ($model->type === JobSearchOption::TYPE_NEXT_STEP && $model->next_step_id && count($model->nextStep->options)) { ?>
            <?php foreach ($model->nextStep->options as $option) {
                echo $this->render('/job-search/list-items/option', ['model' => $option]);
            } ?>
        <?php } ?>
    </ul>
</li>