<?php
use common\models\JobSearchTree;
use yii\helpers\Url;

/* @var $model JobSearchTree */
$editUrl = Url::to(['/job-search/tree', 'id' => $model->id]);
$childUrl = Url::to(['/job-search/step', 'tree_id' => $model->id]);
$deleteUrl = Url::to(['/job-search/delete-tree', 'id' => $model->id]);
?>
<li id="tree-<?= $model->id?>" class="<?= count($model->jobSearchRootSteps) ? 'kv-parent kv-collapsed' : '' ?>" data-url="<?= $editUrl?>" data-child-url="<?= $childUrl?>" data-delete-url="<?= $deleteUrl?>">
    <div class="kv-tree-list" tabindex="-1">
        <div class="kv-node-indicators">
            <span class="text-muted kv-node-toggle">
                <span class="kv-node-expand">
                    <span class="fa fa-plus-square-o"></span>
                </span>
                <span class="kv-node-collapse">
                    <span class="fa fa-minus-square-o"></span>
                </span>
            </span>
        </div>
        <div class="kv-node-detail" tabindex="-1">
            <span class="text-warning kv-node-icon kv-icon-parent">
                <span class="fa fa-folder kv-node-closed"></span>
                <span class="fa fa-folder-open kv-node-opened"></span>
            </span>
            <span class="text-info kv-node-icon kv-icon-child">
                <span class="fa fa-tree"></span>
            </span>
            <span class="kv-node-label">
                <?= $model->name ?>
            </span>
        </div>
    </div>
<!--    --><?php //if (count($model->jobSearchRootSteps)) { ?>
        <ul>
            <?php foreach ($model->jobSearchRootSteps as $step) {
                echo $this->render('/job-search/list-items/step', ['model' => $step]);
            } ?>
        </ul>
<!--    --><?php //} ?>
</li>