<?php

use common\helpers\FileInputHelper;
use common\models\Attachment;
use common\models\ContentTranslation;
use common\models\JobSearchOption;
use common\models\JobSearchStep;
use kartik\file\FileInput;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model JobSearchOption */
/* @var $form ActiveForm */
/* @var $create bool */

?>
<?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['/job-search/option', 'id' => $model->id],
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
        ],
        'id' => 'option-form',
        'validateOnChange' => false,
        'validateOnBlur' => false,
    ]); ?>
    <div class="kv-detail-heading">
        <div class="kv-detail-crumbs">
            <span class="kv-crumb-active"><?= $create ? 'Добавление' : 'Редактирование'?> опции</span>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary" title="" data-original-title="Сохранить">
                    <i class="glyphicon glyphicon-floppy-disk"></i>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="category-form">
        <div class="box">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $model->type === JobSearchOption::TYPE_NEXT_STEP ? $form->field($model, 'id')->textInput(['readonly' => true])->label('ID шага') : '' ?>
                    <?= $form->field($model, 'step_id')->hiddenInput()->label(false) ?>
                    <label class="control-label">Изображение для данной опции. Можно оставить пустым - тогда будет использовано изображение из связанной работы.</label>
                    <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'id' => 'file-upload-input',
                        'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
                        'pluginOptions' => [
                            'overwriteInitial' => true,
                            'initialPreview' => $model->attachment !== null ? $model->attachment->getThumb() : [],
                            'initialPreviewConfig' => $model->attachment !== null ? [[
                                'caption' => basename($model->attachment->content),
                                'url' => Url::toRoute('/image/delete'),
                                'key' => 'image_init_' . $model->attachment->id
                            ]] : [],
                        ]
                    ])) ?>
                    <div class="images-container">
                        <?php foreach ($model->attachments as $key => $image) {
                            /* @var $image Attachment */
                            echo $form->field($image, "[{$key}]content")->hiddenInput([
                                'value' => $image->content,
                                'data-key' => 'image_init_' . $image->id
                            ])->label(false);
                        } ?>
                    </div>
                    <br><br>
                    <label class="control-label">Название опции</label>
                    <?= Tabs::widget([
                        'items' =>
                            array_map(function ($locale, $language) use ($form, $model) {
                                return [
                                    'label' => $language,
                                    'content' => $this->renderAjax('_content_translation', [
                                        'model' => $model->translations[$locale] ?? new ContentTranslation(),
                                        'form' => $form,
                                        'locale' => $locale,
                                        'group' => 'JobSearchOption'
                                    ]),
                                    'active' => $locale === 'ru-RU'
                                ];
                            }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="box box-danger">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $form->field($model, 'type')->radioList(JobSearchOption::getTypeLabels(), ['id' => 'option-type-radio'])->label('Выберите куда будет вести данная опция:')?>
                    <br><br>
                    <div class="option-type" data-type="<?= JobSearchOption::TYPE_JOB?>">
                        <label class="control-label">Выберите работу (для каждого языка может быть разная):</label>
                        <?= Tabs::widget([
                            'items' =>
                                array_map(function ($locale, $language) use ($form, $model) {
                                    return [
                                        'label' => $language,
                                        'content' => $form->field($model, "custom_data[$locale]")->textInput(['class' => 'option-job-autocomplete'])->label(false),
                                        'active' => $locale === 'ru-RU'
                                    ];
                                }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
                        ]);
                        ?>
                    </div>
                    <div class="option-type" data-type="<?= JobSearchOption::TYPE_NEXT_STEP?>">
                        <label class="control-label">Введите название и описание следующего шага:</label>
                        <?= $this->renderAjax('_step_form_fields', [
                            'model' => $model->nextStep ?? new JobSearchStep(['tree_id' => $model->step->tree_id, 'previous_step_id' => $model->step_id]),
                            'form' => $form
                        ])?>
                    </div>
                    <div class="option-type measure-block" data-type="<?= JobSearchOption::TYPE_JOB?>">
                        <label class="control-label">Тексты для шага выбора количества работы (Если у работы указана цена за единицу):</label>
                        <?= Tabs::widget([
                            'items' =>
                                array_map(function ($locale, $language) use ($form, $model) {
                                    return [
                                        'label' => $language,
                                        'content' => $this->renderAjax('_content_translation', [
                                            'model' => $model->nextStep->measureTranslations[$locale] ?? new ContentTranslation(),
                                            'form' => $form,
                                            'locale' => $locale,
                                            'group' => 'JobSearchStepMeasure'
                                        ]),
                                        'active' => $locale === 'ru-RU'
                                    ];
                                }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
                        ]);
                        ?>
                    </div>
                    <div class="option-type" data-type="<?= JobSearchOption::TYPE_ANOTHER_TREE_STEP?>">
                        <?= $form->field($model, 'next_step_id')->label('Введите ID шага')?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <div class="col-xs-12">
                    <?= Html::submitButton('Сохранить', ['class' => 'pull-right btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php
    $jobListUrl = Url::to(['/job-search/job-list']);
    $typeJob = JobSearchOption::TYPE_JOB;
    $typeNextStep = JobSearchOption::TYPE_NEXT_STEP;
    $options = json_encode(array_map(
        function($var) {return ['value' => $var->id, 'label' => "ID: " . $var->id . "  .Название работы: " . $var->translation->title];},
        $model->jobs
    ));
    $script = <<<JS
        $("#option-form").on("beforeValidateAttribute", function (event, attribute, messages) {
            if ($(attribute.container).closest('.measure-block').length) {
                return false;
            }
            if ($(attribute.container).closest('#option-type-radio').length) {
                return false;
            }
            let selectedType = parseInt($("#option-type-radio input:checked").val());
            let inputType = parseInt($(attribute.container).closest(".option-type").data("type"));
            if ($(attribute.container).closest(".option-type").length && (selectedType !== inputType && !(selectedType === $typeJob && inputType === $typeNextStep))) {
                return false;
            }
        }).on("beforeSubmit", function (event) {
            if ($('.file-upload-input').fileinput('getFilesCount') > 0) {
                $('.file-upload-input').fileinput('upload');
                return false;
            }
        });

        $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
            let response = data.response;
            $(".images-container input").remove();
            $(".images-container").append("<input name='Attachment[][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
        }).on("filedeleted", function(event, key) {
            $(".images-container").find("input[data-key='" + key + "']").remove();
        }).on("filebatchuploadcomplete", function(event, files, extra) {
            $("#option-form").submit();
        });
        
        
        $(".option-job-autocomplete").selectize({
            options: $options,
            valueField: "value",
            labelField: "label",
            searchField: ["label"],
            persist: false,
            create: false,
            maxItems: 1,
            "load": function(query, callback) {
                if (!query.length)
                    return callback();
                $.post("$jobListUrl", {
                        query: encodeURIComponent(query)
                    },
                    function(data) {
                        callback(data);
                    }).fail(function() {
                    callback();
                });
            },
            'onInitialize': function() {
                $('.selectize-control').removeClass('form-control');
            }
        });
        
        $("#option-type-radio").on("change", function () {
            let type = $("#option-type-radio input:checked").val();
            $('.option-type').removeClass('active');
            $('.option-type[data-type="' + type + '"]').addClass('active');
            $('.option-type input, .option-type textarea').prop('disabled', true);
            $('.option-type[data-type="' + type + '"] input, .option-type[data-type="' + type + '"] textarea').prop('disabled', false);
            if (type === '$typeJob') {
                $('.option-type[data-type="$typeNextStep"]').addClass('active');
                $('.option-type[data-type="$typeNextStep"] input, .option-type[data-type="$typeNextStep"] textarea').prop('disabled', false);
            }
        }).trigger('change');
JS;
    $this->registerJs($script);

?>
<?php Pjax::end(); ?>

