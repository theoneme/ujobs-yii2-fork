<?php

use backend\assets\SelectizeAsset;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use common\models\search\JobSearch;
use common\models\Job;

/* @var $this yii\web\View */
/* @var $latestUsersDataProvider ActiveDataProvider */
/* @var $latestJobsDataProvider ActiveDataProvider */
/* @var $latestTendersDataProvider ActiveDataProvider */
/* @var $notModeratedJobsDataProvider ActiveDataProvider */
/* @var $notModeratedTendersDataProvider ActiveDataProvider */
/* @var $totalUsers integer */
/* @var $usersWithJobs integer */
/* @var $totalJobs integer */
/* @var $workerUsers integer */

/* @var $jobSearchModel JobSearch */
/* @var $tenderSearchModel JobSearch */

SelectizeAsset::register($this);

$this->title = Yii::t('app', 'Dashboard');
?>
    <section class="content-header">
        <h1>
            <?= $this->title ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Всего пользователей</span>
                        <span class="info-box-number"><?= $totalUsers ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Из них опубликовано в каталоге</span>
                        <span class="info-box-number"><?= $workerUsers ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Из них разместили работы</span>
                        <span class="info-box-number"><?= $usersWithJobs ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Всего работ</span>
                        <span class="info-box-number"><?= $totalJobs ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= Yii::t('app', 'Not Moderated Jobs') ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php \yii\widgets\Pjax::begin(['id' => 'jobs-pjax', 'enablePushState' => false]); ?>
                        <?= Html::beginForm(['job/moderate-selected'], 'post', ['id' => 'job-moderate', 'class' => 'moderate-form', 'data-pj-container' => 'jobs-pjax']); ?>
                        <?= Html::submitButton(Yii::t('app', 'Moderate Selected'), ['class' => 'btn btn-success', 'id' => 'delete-selected']); ?>
                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => $notModeratedJobsDataProvider,
                            'filterModel' => $jobSearchModel,
                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                ],
                                [
                                    'attribute' => 'profile.name',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        /* @var $model Job */
                                        return $model->user->getSellerName();
                                    },
                                ],
                                'created_at:datetime',
                                'translation.title',
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        /* @var $model Job */
                                        return $model->getStatusLabel(true);
                                    },
                                    'filter' => Job::getStatusLabels()
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header' => 'Actions',
                                    'template' => '{update} {delete}',
                                    'buttons' => [
                                        'update' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                                'data-pjax' => 0,
                                                'class' => 'modal-edit'
                                            ]);
                                        }
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        switch ($action) {
                                            case 'update':
                                                $url = Url::toRoute(["/{$model->type}/{$action}", 'id' => $model->id, 'user_id' => $model->user_id]);
                                                break;
                                            default:
                                                $url = Url::toRoute(["/{$model->type}/{$action}", 'id' => $model->id]);
                                                break;
                                        }
                                        return $url;
                                    }
                                ],
                            ],
                        ]); ?>
                        <?= Html::endForm(); ?>
                        <?php \yii\widgets\Pjax::end(); ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?= Url::to(['/job/index', 'JobSearch[type]' => 'job']) ?>"
                           class="uppercase"><?= Yii::t('app', 'View All Jobs') ?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= Yii::t('app', 'Not Moderated Requests') ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php \yii\widgets\Pjax::begin(['id' => 'tenders-pjax', 'enablePushState' => false]); ?>
                        <?= Html::beginForm(['tender/moderate-selected'], 'post', ['id' => 'tender-moderate', 'class' => 'moderate-form', 'data-pj-container' => 'tenders-pjax']); ?>
                        <?= Html::submitButton(Yii::t('app', 'Moderate Selected'), ['class' => 'btn btn-success', 'id' => 'delete-selected']); ?>
                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => $notModeratedTendersDataProvider,
                            'filterModel' => $tenderSearchModel,
                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                ],
                                [
                                    'attribute' => 'profile.name',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        /* @var $model Job */
                                        return $model->user->getSellerName();
                                    },
                                ],
                                'created_at:datetime',
                                'translation.title',
                                [
                                    'attribute' => 'is_moderated',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        /* @var $model Job */
                                        return $model->getStatusLabel(true);
                                    },
                                    'filter' => Job::getStatusLabels()
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'header' => 'Actions',
                                    'template' => '{update} {delete}',
                                    'buttons' => [
                                        'update' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                                'data-pjax' => 0,
                                                'class' => 'modal-edit'
                                            ]);
                                        }
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        $type = $model->type == Job::TYPE_ADVANCED_TENDER ? Job::TYPE_TENDER : $model->type;
                                        switch ($action) {
                                            case 'update':
                                                $url = Url::toRoute(["/{$type}/{$action}", 'id' => $model->id, 'user_id' => $model->user_id]);
                                                break;
                                            default:
                                                $url = Url::toRoute(["/{$type}/{$action}", 'id' => $model->id]);
                                                break;
                                        }
                                        return $url;
                                    }
                                ],
                            ],
                        ]); ?>
                        <?= Html::endForm(); ?>
                        <?php \yii\widgets\Pjax::end(); ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?= Url::to(['/job/index', 'JobSearch[type]' => 'tender']) ?>"
                           class="uppercase"><?= Yii::t('app', 'View All Requests') ?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Left col -->
            <div class="col-md-6">
                <!-- MAP & BOX PANE -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- USERS LIST -->
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= Yii::t('app', 'Latest Members') ?></h3>

                                <div class="box-tools pull-right">
                                    <span class="label label-danger"><?= Yii::t('app', '{count} New Members', ['count' => 8]) ?></span>
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <?php echo ListView::widget([
                                    'dataProvider' => $latestUsersDataProvider,
                                    'itemView' => function ($model, $key, $index, $widget) {
                                        return $this->render('listview/user', ['model' => $model]);
                                    },
                                    'layout' => "{items}",
                                    'options' => [
                                        'tag' => 'ul',
                                        'class' => 'users-list clearfix'
                                    ],
                                    'itemOptions' => [
                                        'tag' => 'li',
                                    ],
                                ]); ?>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <a href="<?= Url::to(['/user/admin/index']) ?>"
                                   class="uppercase"><?= Yii::t('app', 'View All Users') ?></a>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!--/.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Orders</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Item</th>
                                    <th>Status</th>
                                    <th>Popularity</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR9842</a>
                                    </td>
                                    <td>Call of Duty IV</td>
                                    <td><span class="label label-success">Shipped</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#00a65a" data-height="20">
                                            90,80,90,-70,61,-83,63
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR1848</a>
                                    </td>
                                    <td>Samsung Smart TV</td>
                                    <td><span class="label label-warning">Pending</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f39c12" data-height="20">
                                            90,80,-90,70,61,-83,68
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR7429</a>
                                    </td>
                                    <td>iPhone 6 Plus</td>
                                    <td><span class="label label-danger">Delivered</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f56954" data-height="20">
                                            90,-80,90,70,-61,83,63
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR7429</a>
                                    </td>
                                    <td>Samsung Smart TV</td>
                                    <td><span class="label label-info">Processing</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#00c0ef" data-height="20">
                                            90,80,-90,70,-61,83,63
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR1848</a>
                                    </td>
                                    <td>Samsung Smart TV</td>
                                    <td><span class="label label-warning">Pending</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f39c12" data-height="20">
                                            90,80,-90,70,61,-83,68
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR7429</a>
                                    </td>
                                    <td>iPhone 6 Plus</td>
                                    <td><span class="label label-danger">Delivered</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f56954" data-height="20">
                                            90,-80,90,70,-61,83,63
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="pages/examples/invoice.html">OR9842</a>
                                    </td>
                                    <td>Call of Duty IV</td>
                                    <td><span class="label label-success">Shipped</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#00a65a" data-height="20">
                                            90,80,90,-70,61,-83,63
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                        <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All
                            Orders</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= Yii::t('app', 'Recently Added Jobs') ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo ListView::widget([
                            'dataProvider' => $latestJobsDataProvider,
                            'itemView' => function ($model, $key, $index, $widget) {
                                return $this->render('listview/job', ['model' => $model]);
                            },
                            'layout' => "{items}",
                            'options' => [
                                'tag' => 'ul',
                                'class' => 'products-list product-list-in-box'
                            ],
                            'itemOptions' => [
                                'tag' => 'li',
                                'class' => 'item'
                            ],
                        ]); ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?= Url::to(['/job/index', 'JobSearch[type]' => 'job']) ?>"
                           class="uppercase"><?= Yii::t('app', 'View All Jobs') ?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->

                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= Yii::t('app', 'Recently Added Requests') ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo ListView::widget([
                            'dataProvider' => $latestTendersDataProvider,
                            'itemView' => function ($model, $key, $index, $widget) {
                                return $this->render('listview/job', ['model' => $model]);
                            },
                            'layout' => "{items}",
                            'options' => [
                                'tag' => 'ul',
                                'class' => 'products-list product-list-in-box'
                            ],
                            'itemOptions' => [
                                'tag' => 'li',
                                'class' => 'item'
                            ],
                        ]); ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?= Url::to(['/tender/index', 'JobSearch[type]' => 'tender']) ?>"
                           class="uppercase"><?= Yii::t('app', 'View All Requests') ?></a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->

<?php

$this->registerJs('
	$(document).on("click", "#delete-selected", function(e) {
	    e.preventDefault();
	    var pjaxContainer = $(this).data("pj-container");
	    var isConfirmed = confirm("Вы подтверждаете модерацию?");
	    if (isConfirmed) {
	        var form = $(this);
	        $.ajax({
	            url: form.attr("action"),
	            type: "post",
	            data: form.serialize(),
	            success: function(response) {
					if(pjaxContainer != undefined && $("#" + pjaxContainer).length) {
                        $.pjax.reload({container: "#" + pjaxContainer});
                    }
	            }
	        });
	    }
	    return false;
	});
');