<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.12.2016
 * Time: 18:28
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Job;

/* @var Job $model */

$title = $model->getLabel();
$content = $model->getDescription();
$price = $model->getPrice();
?>

<div class="product-img">
    <?= Html::img($model->getThumb('catalog')) ?>
</div>
<div class="product-info">
    <?= Html::a(
        $title . '<span class="label label-warning pull-right">' . $price . '</span>',
        ["/{$model->type}/update", 'id' => $model->id],
        ['class' => 'product-title']
    ) ?>
    <span class="product-description">
         <?= strip_tags($content) ?>
    </span>
</div>