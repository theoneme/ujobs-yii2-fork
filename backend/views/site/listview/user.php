<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.12.2016
 * Time: 17:42
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\user\User;

/* @var User $model */

?>

<?= Html::img($model->profile->getThumb()) ?>
<?= Html::a(Html::encode($model->profile->name ? $model->profile->name : $model->username), Url::to(['/user/admin/update', 'id' => $model->id])) ?>
<span class="users-list-date"><?= Yii::$app->formatter->asDate($model->created_at) ?></span>