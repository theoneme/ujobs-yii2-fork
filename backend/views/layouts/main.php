<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;


/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {
    backend\assets\AppAsset::register($this);
    dmstr\web\AdminLteAsset::register($this);
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title>Ujobs - <?= Html::encode($this->title) ?></title>
		<link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <?php $this->head() ?>
    </head>
    <body class="skin-purple sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <?= $this->render('header.php') ?>
        <?= $this->render('left.php')?>
        <?= $this->render('content.php', ['content' => $content]) ?>
    </div>
    <?php
        Modal::begin([
            'headerOptions' => ['id' => 'modalHeader'],
            'id' => 'edit-modal',
            'size' => 'modal-xxl',
            //keeps from closing modal with esc key or by clicking out of the modal.
            // user must click cancel or X to close
            //'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
        ]);
            echo "<div class='modalContent'></div>";
        Modal::end();
    ?>
    <div class="crop-bg" style="display: none;">
        <div class="crop-container"></div>
        <div class="crop-actions">
            <i class="fa fa-times fa-lg crop-close" aria-hidden="true"></i>
            <button class="btn btn-primary crop-rotate-left">
                <i class="fa fa-rotate-left fa-lg" aria-hidden="true"></i>
            </button>
            <button class="btn btn-primary crop-rotate-right">
                <i class="fa fa-rotate-right fa-lg" aria-hidden="true"></i>
            </button>
            <button class="btn btn-primary crop-confirm">Сохранить</button>
        </div>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
