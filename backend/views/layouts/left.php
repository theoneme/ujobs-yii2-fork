<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\controllers\BackEndController;

/* @var BackEndController $controller */
$controller = Yii::$app->controller;

?>

<aside class="main-sidebar">
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<?php echo Html::img('@web/images/avat.png', ['class' => 'img-circle']) ?>
			</div>
			<div class="pull-left info">
				<?php if (!Yii::$app->user->isGuest) { ?>
					<p><?= Yii::$app->user->identity->username ?></p>
					<a href="#"><i class="circle text-success"></i> Online</a>
				<?php } ?>
			</div>
		</div>

		<?= dmstr\widgets\Menu::widget(
			[
				'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
				'items' => [
					[
						'label' => Yii::t('app', 'Menu Yii2'),
						'options' => ['class' => 'header']
					],
					[
						'label' => Yii::t('app', 'Dashboard'),
						'icon' => 'dashboard',
						'url' => Url::toRoute('/site/index'),
						'active' => $controller->id === 'site',
						'visible' => $controller->isAllowed('site', 'index'),
					],
					[
						'label' => Yii::t('app', 'System'),
						'url' => '#',
						'icon' => 'list-alt',
						'active' => in_array($controller->id, ['gii', 'debug', 'setting', 'test']),
						'items' => [
							[
								'label' => 'Gii',
								'icon' => 'file-code-o', 'url' => ['/gii'],
								'visible' => $controller->isAllowed('gii', 'index'),
							],
							[
								'label' => Yii::t('app', 'Debug'),
								'icon' => 'dashboard', 'url' => ['/debug'],
								'visible' => $controller->isAllowed('debug', 'index'),
							],
							[
                                'label' => Yii::t('app', 'Settings'),
                                'icon' => 'motorcycle',
                                'url' => Url::toRoute('/setting/index'),
                                'active' => $controller->id === 'setting',
                                'visible' => $controller->isAllowed('setting', 'index'),
                            ],
                            [
                                'label' => Yii::t('app', 'Replication status'),
                                'icon' => 'motorcycle',
                                'url' => Url::toRoute('/monitor/index'),
                                'active' => $controller->id === 'monitor',
                                'visible' => $controller->isAllowed('monitor', 'index'),
                            ],
                            [
                                'label' => Yii::t('app', 'Tests'),
                                'icon' => 'folder',
                                'url' => Url::toRoute('/test/index'),
                                'active' => $controller->id === 'test',
                                'visible' => $controller->isAllowed('test', 'index')
                            ],
							[
								'label' => Yii::t('app', 'Clear cache'),
								'icon' => 'folder',
								'url' => Url::toRoute('/setting/cache'),
								'visible' => $controller->isAllowed('setting', 'cache')
							],
						],
					],
					[
						'label' => Yii::t('app', 'Stats'),
						'url' => '#',
						'icon' => 'list-alt',
						'active' => in_array($controller->id, ['stat', 'money-stat']),
						'items' => [
							[
								'label' => Yii::t('app', 'Statistics'),
								'icon' => 'bar-chart',
								'url' => Url::toRoute('/stat/index'),
								'active' => $controller->id === 'stat',
								'visible' => $controller->isAllowed('stat', 'index'),
							],
							[
								'label' => Yii::t('app', 'Finance Statistics'),
								'icon' => 'bar-chart',
								'url' => Url::toRoute('/money-stat/index'),
								'active' => $controller->id === 'money-stat',
								'visible' => $controller->isAllowed('money-stat', 'index'),
							],
						],
					],
					[
						'label' => Yii::t('app', 'Content'),
						'url' => '#',
						'icon' => 'list-alt',
						'active' => in_array($controller->id, ['page', 'category', 'seo_advanced']),
						'items' => [
							[
								'label' => Yii::t('app', 'Categories'),
								'icon' => 'folder',
								'url' => Url::toRoute('/category/index'),
								'active' => $controller->id === 'category',
								'visible' => $controller->isAllowed('category', 'index'),
							],
							[
								'label' => Yii::t('app', 'Pages'),
								'icon' => 'folder',
								'url' => Url::toRoute('/page/index'),
								'active' => $controller->id === 'page' && Url::current() === Url::toRoute(['/page/index']),
								'visible' => $controller->isAllowed('page', 'index'),
							],
						],
					],
					[
						'label' => 'Login',
						'url' => ['site/login'],
						'visible' => Yii::$app->user->isGuest
					],
					[
						'label' => Yii::t('app', 'Modules'),
						'url' => '#',
						'icon' => 'list-alt',
						'active' => in_array($controller->id, ['attribute', 'attribute-group', 'attribute-value']),
//						'visible' => $controller->isAllowed('attribute-group', 'index') && $controller->isAllowed('admin-filter', 'index'),
						'items' => [
							[
								'label' => Yii::t('app', 'Attributes'),
								'icon' => 'list-alt',
								'url' => Url::toRoute('/attribute/attribute/index'),
								'active' => $controller->id === 'attribute' && $controller->action->id === 'index',
                                'visible' => $controller->isAllowed('attribute', 'index'),
							],
                            [
                                'label' => Yii::t('app', 'New Attribute Values'),
                                'icon' => 'list-alt',
                                'url' => Url::toRoute('/attribute/attribute/requires-moderation'),
                                'active' => $controller->id === 'attribute' && $controller->action->id === 'requires-moderation',
                                'visible' => $controller->isAllowed('attribute', 'requires-moderation'),
                            ],
							[
								'label' => Yii::t('app', 'Attribute Groups'),
								'icon' => 'list-alt',
								'url' => Url::toRoute('/attribute/attribute-group/index'),
								'active' => $controller->id === 'attribute-group',
                                'visible' => $controller->isAllowed('attribute-group'),
							],
							[
								'label' => Yii::t('app', 'Filter'),
								'icon' => 'filter',
								'url' => Url::toRoute('/filter/admin-filter/index'),
								'active' => $controller->id === 'admin-filter',
                                'visible' => $controller->isAllowed('admin-filter'),
							],
						],
					],
					[
						'label' => Yii::t('app', 'Jobs, requests, videos'),
						'url' => '#',
						'icon' => 'list-alt',
						'active' => in_array($controller->id, ['job', 'admin', 'video', 'user-portfolio']) && $controller->route !== 'user/admin/index' && $controller->route !== 'wizard/admin/index',
						'visible' => $controller->isAllowed('job', 'index') && $controller->isAllowed('job', 'drafts'),
						'items' => [
							[
								'label' => Yii::t('app', 'Jobs'),
								'icon' => 'folder',
								'url' => Url::toRoute(['/job/index', 'JobSearch[type]' => 'job']),
								'active' => $controller->id === 'job' && Url::current() === Url::toRoute(['/job/index', 'JobSearch[type]' => 'job']),
							],
                            [
                                'label' => 'Автопереведённые работы',
                                'icon' => 'folder',
                                'url' => Url::toRoute(['/job/auto', 'JobSearch[type]' => 'job']),
                                'active' => $controller->id === 'job' && Url::current() === Url::toRoute(['/job/auto', 'JobSearch[type]' => 'job']),
                            ],
                            [
                                'label' => 'Автопереведённые заявки',
                                'icon' => 'folder',
                                'url' => Url::toRoute(['/job/auto', 'JobSearch[type]' => 'tender']),
                                'active' => $controller->id === 'job' && Url::current() === Url::toRoute(['/job/auto', 'JobSearch[type]' => 'tender']),
                            ],
							[
								'label' => Yii::t('app', 'Requests'),
								'icon' => 'folder',
								'url' => Url::toRoute(['/job/index', 'JobSearch[type]' => 'tender']),
								'active' => $controller->id === 'job' && Url::current() === Url::toRoute(['/job/index', 'JobSearch[type]' => 'tender']),
							],
							[
								'label' => Yii::t('app', 'Drafts'),
								'icon' => 'folder',
								'url' => Url::toRoute(['/job/drafts', 'JobSearch[type]' => 'job']),
								'active' => $controller->id === 'job' && Url::current() === Url::toRoute(['/job/drafts', 'JobSearch[type]' => 'job']),
							],
							[
								'label' => Yii::t('app', 'Videos'),
								'icon' => 'video-camera',
								'url' => Url::toRoute(['/video/index']),
								'active' => $controller->id === 'video' && Url::current() === Url::toRoute(['/video/index']),
								'visible' => $controller->isAllowed('video', 'index')
							],
                            [
                                'label' => Yii::t('app', 'Products'),
                                'icon' => 'video-camera',
                                'url' => Url::toRoute(['/board/admin/index']),
                                'active' => $controller->id === 'admin' && Url::current() === Url::toRoute(['/board/admin/index']),
                                'visible' => $controller->isAllowed('admin', 'index')
                            ],
                            [
                                'label' => Yii::t('app', 'Products (Stores)'),
                                'icon' => 'video-camera',
                                'url' => Url::toRoute(['/board/admin/store']),
                                'active' => $controller->id === 'admin' && Url::current() === Url::toRoute(['/board/admin/store']),
                                'visible' => $controller->isAllowed('admin', 'store')
                            ],
                            [
                                'label' => Yii::t('app', 'Product requests'),
                                'icon' => 'video-camera',
                                'url' => Url::toRoute(['/board/admin/index-tender']),
                                'active' => $controller->id === 'admin' && Url::current() === Url::toRoute(['/board/admin/index-tender']),
                                'visible' => $controller->isAllowed('admin', 'index-tender')
                            ],
                            [
                                'label' => Yii::t('app', 'Portfolios'),
                                'icon' => 'video-camera',
                                'url' => Url::toRoute(['/user-portfolio/index']),
                                'active' => $controller->id === 'user-portfolio' && Url::current() === Url::toRoute(['/user-portfolio/index']),
                                'visible' => $controller->isAllowed('user-portfolio', 'index')
                            ],
                            [
                                'label' => Yii::t('app', 'Old Portfolios'),
                                'icon' => 'video-camera',
                                'url' => Url::toRoute(['/user-portfolio/old']),
                                'active' => $controller->id === 'user-portfolio' && Url::current() === Url::toRoute(['/user-portfolio/old']),
                                'visible' => $controller->isAllowed('user-portfolio', 'old')
                            ],
						],
					],
					[
						'label' => Yii::t('app', 'Orders'),
						'url' => '#',
						'icon' => 'list-alt',
						'active' => in_array($controller->id, ['order', 'conversation', 'payment', 'invoice']),
						'items' => [
							[
								'label' => Yii::t('app', 'Orders'),
								'icon' => 'money',
								'url' => Url::toRoute(['/order/index']),
								'active' => $controller->id === 'order' && $controller->action->id === 'index',
								'visible' => $controller->isAllowed('order', 'index')
							],
							[
								'label' => Yii::t('app', 'Request orders'),
								'icon' => 'money',
								'url' => Url::toRoute(['/order/tender']),
								'active' => $controller->id === 'order' && $controller->action->id === 'tender',
								'visible' => $controller->isAllowed('order', 'tender')
							],
                            [
                                'label' => Yii::t('app', 'Product orders'),
                                'icon' => 'money',
                                'url' => Url::toRoute(['/order/product']),
                                'active' => $controller->id === 'order' && $controller->action->id === 'product',
                                'visible' => $controller->isAllowed('order', 'product')
                            ],
                            [
                                'label' => Yii::t('app', 'Product tender orders'),
                                'icon' => 'money',
                                'url' => Url::toRoute(['/order/product-tender']),
                                'active' => $controller->id === 'order' && $controller->action->id === 'product-tender',
                                'visible' => $controller->isAllowed('order', 'product-tender')
                            ],
							[
								'label' => Yii::t('app', 'Individual orders'),
								'icon' => 'money',
								'url' => Url::toRoute(['/conversation/orders']),
								'active' => $controller->id === 'conversation' && Url::current() === Url::toRoute(['/conversation/orders']),
								'visible' => $controller->isAllowed('conversation', 'orders')
							],
							[
								'label' => Yii::t('app', 'Payments'),
								'icon' => 'money',
								'url' => Url::toRoute(['/payment/index']),
								'active' => $controller->id === 'payment',
								'visible' => $controller->isAllowed('payment', 'index')
							],
							[
								'label' => Yii::t('app', 'Invoices'),
								'icon' => 'money',
								'url' => Url::toRoute(['/invoice/index']),
								'active' => $controller->id === 'invoice',
								'visible' => $controller->isAllowed('invoice', 'index')
							],
						],
					],
					[
						'label' => Yii::t('app', 'Users'),
						'icon' => 'user',
						'url' => Url::toRoute('/user/admin/index'),
						'active' => $controller->id === 'admin' && $controller->route === 'user/admin/index',
						'visible' => $controller->isAllowed('admin', 'index')
					],
					[
						'label' => Yii::t('app', 'Referrals'),
						'icon' => 'user',
						'url' => Url::toRoute(['/referral/index']),
						'active' => $controller->id === 'referral',
						'visible' => $controller->isAllowed('referral', 'index')
					],
					[
						'label' => Yii::t('app', 'User conversations'),
						'icon' => 'money',
						'url' => Url::toRoute(['/conversation/index']),
						'active' => $controller->id === 'conversation' && Url::current() === Url::toRoute(['/conversation/index']),
						'visible' => $controller->isAllowed('conversation', 'index')
					],
					[
						'label' => Yii::t('app', 'Consultations (Jivosite-2)'),
						'icon' => 'money',
						'url' => Yii::$app->params['crmUrl'],
						'visible' => $controller->isAllowed('livechat', 'index'),
                        'template'=> '<a href="{url}" target="_blank">{icon}{label}</a>',
					],
					[
						'label' => Yii::t('app', 'Withdrawal requests'),
						'icon' => 'money',
						'url' => Url::toRoute(['/user-withdrawal/index']),
						'active' => $controller->id === 'user-withdrawal',
						'visible' => $controller->isAllowed('user-withdrawal', 'index')
					],
					[
						'label' => Yii::t('app', 'User tariffs'),
						'icon' => 'money',
						'url' => Url::toRoute(['/user-tariff/index']),
						'active' => $controller->id === 'user-tariff' && $controller->action->id === 'index',
						'visible' => $controller->isAllowed('user-tariff', 'index')
					],
                    [
                        'label' => 'Баннеры',
                        'icon' => 'money',
                        'url' => Url::toRoute(['/banner/index']),
                        'active' => $controller->id === 'banner' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('banner', 'index')
                    ],
                    [
                        'label' => 'Деревья поиска работы',
                        'icon' => 'tree',
                        'url' => Url::toRoute(['/job-search/index']),
                        'active' => $controller->id === 'job-search' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('job-search', 'index')
                    ],
                    [
                        'label' => 'Деревья ремонта',
                        'icon' => 'tree',
                        'url' => Url::toRoute(['/wizard/admin/index']),
                        'active' => $controller->route === 'wizard/admin/index',
                        'visible' => $controller->isAllowed('admin', 'index')
                    ],
                    [
                        'label' => 'Статьи',
                        'icon' => 'list-alt',
                        'url' => Url::toRoute(['/page/articles']),
                        'active' => $controller->id === 'page' && Url::current() === Url::toRoute(['/page/articles']),
                        'visible' => $controller->isAllowed('page', 'articles')
                    ],
                    [
                        'label' => 'Типы вывода денег',
                        'icon' => 'money',
                        'url' => Url::toRoute(['/withdrawal-type/index']),
                        'active' => $controller->id === 'withdrawal-type' && Url::current() === Url::toRoute(['/withdrawal-type/index']),
                        'visible' => $controller->isAllowed('withdrawal-type', 'index')
                    ],
                    [
                        'label' => 'Способы оплаты',
                        'icon' => 'money',
                        'url' => Url::toRoute(['/payment-method/index']),
                        'active' => $controller->id === 'payment-method' && Url::current() === Url::toRoute(['/payment-method/index']),
                        'visible' => $controller->isAllowed('payment-method', 'index')
                    ],
                    [
                        'label' => 'Клиенты риелторов',
                        'icon' => 'user',
                        'url' => Url::toRoute('/property-client/index'),
                        'active' => $controller->id === 'property-client' && Url::current() === Url::toRoute(['/property-client/index']),
                        'visible' => $controller->isAllowed('property-client', 'index')
                    ],
					[
						'label' => Yii::t('app', 'Logout'),
						'url' => ['/user/security/logout'],
						'icon' => 'user',
						'template' => '<a href="{url}" data-method="post">{icon} {label}</a>',
						'visible' => !Yii::$app->user->isGuest
					],
				],
			]
		) ?>
	</section>
</aside>
