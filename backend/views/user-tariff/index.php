<?php

use common\components\CurrencyHelper;
use common\models\UserTariff;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserTariffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User tariffs');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="setting-index">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title"><?= Html::encode($this->title) ?></h3>
		</div>
		<div class="box-body">
			<?php \yii\widgets\Pjax::begin(); ?>
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					[
						'header' => 'Пользователь',
						'attribute' => 'username',
						'value' => function ($model) {
							/* @var $model UserTariff */
							return Html::a(Html::img($model->user->getThumb('catalog'), ['style' => 'width: 50px']) . "&nbsp;" . $model->user->getSellerName(),
								['/user/admin/update-ajax', 'id' => $model->user_id],
								[
									'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
									'data-pjax' => 0,
									'class' => 'modal-edit'
								]
							);
						},
						'format' => 'html',
					],
					[
						'header' => 'Тариф',
						'value' => function ($model) {
							/* @var $model UserTariff */
							return $model->tariff->title;
						},
					],
					[
						'header' => 'Дата покупки',
						'attribute' => 'created_at',
						'value' => function ($model) {
							if (extension_loaded('intl')) {
								return Yii::t('user', '{0, date, dd.MM.YYYY HH:mm}', [$model->created_at]);
							} else {
								return date('Y-m-d G:i:s', $model->created_at);
							}
						},
					],
					[
						'header' => 'Дата начала',
						'attribute' => 'starts_at',
						'value' => function ($model) {
							if (extension_loaded('intl')) {
								return Yii::t('user', '{0, date, dd.MM.YYYY HH:mm}', [$model->starts_at]);
							} else {
								return date('Y-m-d G:i:s', $model->starts_at);
							}
						},
					],
					[
						'header' => 'Дата окончания',
						'attribute' => 'expires_at',
						'value' => function ($model) {
							if (extension_loaded('intl')) {
								return Yii::t('user', '{0, date, dd.MM.YYYY HH:mm}', [$model->expires_at]);
							} else {
								return date('Y-m-d G:i:s', $model->expires_at);
							}
						},
					],
					[
						'header' => 'Сумма оплаты',
						'value' => function ($model) {
							if($model->payment !== null) {
								return CurrencyHelper::convertAndFormat($model->currency_code, 'RUB', $model->payment->total);
							}

							return 0;
						},
					],
				],
			]); ?>
			<?php \yii\widgets\Pjax::end(); ?>
		</div>
	</div>
</div>
