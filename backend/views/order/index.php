<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 17:09
 */

use common\models\Job;
use GeoIp2\Database\Reader;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\modules\store\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\JobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';

$this->params['breadcrumbs'][] = $this->title;
\backend\assets\SelectizeAsset::register($this);
?>
<div class="job-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'id' => 'order-grid',
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width: 80px'],
                        'options' => ['style' => 'width: 80px']
                    ],
                    [
                        'header' => 'Продавец',
                        'attribute' => 'seller',
                        'value' => function ($model) {
                            return Html::a($model->seller->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->seller->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'header' => 'Покупатель',
                        'attribute' => 'customer',
                        'value' => function ($model) {
                            return Html::a($model->customer->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->customer->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'header' => 'Страна',
                        'value' => function($model) {
                            $region = 'unknown';
                            $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-Country.mmdb');
                            try {
                                $ip = $model->customer->registration_ip;
                                if ($ip != '127.0.0.1') {
                                    $region = $geoIpReader->country($ip)->country->names['ru'];

                                }
                            } catch (Exception $e) {

                            }

                            return $region;
                        },
                    ],
                    [
                        'header' => 'Метод',
                        'value' => function ($model) {
                            return $model->paymentMethod->name;
                        },
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->created_at) . " (" . Yii::$app->formatter->format($model->created_at, 'relativeTime') . ")";
                        },
                        'filter' => false
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->updated_at) . " (" . Yii::$app->formatter->format($model->updated_at, 'relativeTime') . ")";
                        },
                        'filter' => false
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => [
                            Order::STATUS_CANCELLED => Yii::t('account', 'Cancelled'),
                            Order::STATUS_REQUIRES_PAYMENT => Yii::t('account', 'Requires Payment'),
                            Order::STATUS_MISSING_DETAILS => Yii::t('account', 'Missing Details'),
                            Order::STATUS_ACTIVE => Yii::t('account', 'Active'),
                            Order::STATUS_DELIVERED => Yii::t('account', 'Delivered'),
                            Order::STATUS_AWAITING_REVIEW => Yii::t('account', 'Awaiting review'),
                            Order::STATUS_COMPLETED => Yii::t('account', 'Completed')
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Order */
                            return $model->getStatusLabel(true);
                        },
                    ],
                    [
                        'header' => 'Заказанные услуги',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Order */
                            if ($model->product === null) {
                                return '<span style="color: red">Услуга не найдена</span>';
                            }
                            $type = 'job';
                            $id = null;
                            switch($model->product_type) {
                                case Order::TYPE_TENDER:
                                case Order::TYPE_SIMPLE_TENDER:
                                    $type = 'tender';
                                    $id = $model->product->id;
                                    break;
                                case Order::TYPE_JOB_PACKAGE:
                                    $id = $model->product->job_id;
                                    $type = 'job';
                                    break;
                            }
                            return ($model->product_type == Order::TYPE_JOB_PACKAGE || $model->product_type == Order::TYPE_TENDER || $model->product_type == Order::TYPE_SIMPLE_TENDER) ?
                                Html::a(StringHelper::truncate($model->product->getLabel(), 25),
                                    ["/{$type}/update", 'id' => $id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                )
                                : StringHelper::truncate($model->product->getLabel(), 25);
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => '{view} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                        /*'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'view':
                                    $url = Url::toRoute(["/{$model->type}/{$action}", 'id' => $model->id, 'user_id' => $model->user_id]);
                                    break;
                                default:
                                    $url = Url::toRoute(["/{$model->type}/{$action}", 'id' => $model->id]);
                                    break;
                            }
                            return $url;
                        }*/
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>