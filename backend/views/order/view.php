<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 17:57
 */

use backend\assets\OrderDetailsAsset;
use common\components\CurrencyHelper;
use common\modules\store\models\backend\ChangeSellerForm;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\store\models\Order;
use yii\widgets\ActiveForm;

/* @var Order $order */
/* @var ChangeSellerForm $changeSellerForm */
/* @var boolean $isAjax */

$this->title = $order->product ? $order->product->getLabel() : 'Услуга не найдена';

OrderDetailsAsset::register($this);

?>

<?php if ($order->status > Order::STATUS_REQUIRES_PAYMENT && $order->status < Order::STATUS_COMPLETED) { ?>
    <div class="row">
        <div class="col-md-4 text-center">
            <?= Html::beginForm(['/order/cancel'], 'post', ['id' => 'cancel-form']) ?>
            <?= Html::hiddenInput('order_id', $order->id) ?>
            <?= Html::submitButton('Отменить заказ <br> (деньги заказчику)', ['class' => 'btn btn-danger', 'data-confirm' => 'Вы уверены что хотите пометить данный заказ как отмененный?']) ?>
            <?= Html::endForm() ?>
        </div>
        <div class="col-md-4 text-center">
            <?= Html::beginForm(['/order/justice'], 'post', ['id' => 'justice-form']) ?>
            <?= Html::hiddenInput('order_id', $order->id) ?>
            <?= Html::submitButton('Завершить заказ <br> (обоим по половине)', ['class' => 'btn btn-warning', 'disabled' => 'disabled']) ?>
            <?= Html::endForm() ?>
        </div>
        <div class="col-md-4 text-center">
            <?= Html::beginForm(['/order/complete'], 'post', ['id' => 'complete-form']) ?>
            <?= Html::hiddenInput('order_id', $order->id) ?>
            <?= Html::submitButton('Завершить заказ <br> (деньги исполнителю)', ['class' => 'btn btn-success', 'data-confirm' => 'Вы уверены что хотите пометить данный заказ как завершенный?']) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
    <br>

    <?php if ($changeSellerForm !== null) { ?>
        <div class="box box-primary">
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'attribute-value-merge',
                    ],
                    'action' => ['/order/merge']
                ]); ?>
                <?= $form->field($changeSellerForm, 'merge')->checkbox(['id' => 'merge-toggle-checkbox']); ?>
                <div id="mergePart" class="hidden">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="selectize-input-container">
                                <?= $form->field($changeSellerForm, 'new_seller_id')->textarea(['id' => 'seller-autocomplete']); ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="selectize-input-container">
                                <?= $form->field($changeSellerForm, 'new_job_id')->textarea(['id' => 'job-autocomplete']); ?>
                            </div>
                        </div>
                    </div>
                    <?= $form->field($changeSellerForm, "order_id")->hiddenInput()->label(false); ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('attribute', 'Update'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>
    <div class="green-status" style="margin:0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if ($order->isPaid()) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Order <br> is paid') ?></div>
                        </div>
                    <?php } else { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-close"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Order <br> is not paid') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->areRequirementSubmitted() === true) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Details <br> Specified') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->isDelivered() === true) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Delivery <br> Submitted') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->isCompleted() === true) { ?>
                        <div class="order-status">
                            <i class="fa fa-check-circle-o"></i>
                            <?= Yii::t('labels', 'Order Completed') ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p>&nbsp;</p>
                <!--<a href="" class="view-invoice text-right hint--top-right" data-hint="Orders purchased using an existing balance do not receive an invoice.">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    View Invoice
                </a>-->
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="order-history">
                    <div class="order-details">
                        <div class="order-descr-table">
                            <div class="order-img-cell">
                                <?= $order->product
                                    ? Html::a(Html::img($order->product->getThumb()), $order->product->getUrl(), [
                                        'class' => 'order-img'
                                    ])
                                    : ''
                                ?>
                            </div>
                            <div class="order-info">
                                <?= $order->product
                                    ? Html::a(nl2br(Html::encode(StringHelper::truncate($order->product->getLabel(), 100))), $order->product->getUrl(), [
                                        'class' => 'order-link'
                                    ])
                                    : '<span style="color: red">Услуга не найдена</span>'
                                ?>
                                <div class="order-opts">
                                    <div class="order-opts-item">
                                        <span><?= Yii::t('account', 'Seller') ?>:</span>
                                        <?= Html::a($order->seller->getSellerName(),
                                            $order->seller->getSellerUrl()
                                        ) ?>
                                    </div>
                                    <div class="order-opts-item">
                                        <span><?= Yii::t('account', 'Order') ?>:</span> #<?= $order->id ?>
                                    </div>
                                    <div class="order-opts-item">
                                        <?= Yii::$app->formatter->asDate($order->created_at) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="order-price text-right"></div>
                        </div>
                        <div class="order-job-table">
                            <div class="order-job-row">
                                <div class="order-job-cell">
                                    <?= Yii::t('account', 'Service') ?>
                                </div>
                                <div class="order-job-cell">
                                    <?= Yii::t('account', 'Quantity') ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= Yii::t('account', 'Completion Date') ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= Yii::t('account', 'Total') ?>
                                </div>
                            </div>

                            <div class="order-job-row">
                                <div class="order-job-cell">
                                    <?= $order->product ? nl2br(Html::encode($order->product->getLabel())) : '<span style="color: red">Услуга не найдена</span>' ?>
                                </div>
                                <div class="order-job-cell">
                                    <?= $order->orderProduct->quantity ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= Yii::$app->formatter->asDate($order->tbd_at) ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= CurrencyHelper::convertAndFormat($order->currency_code, 'RUB', $order->orderProduct->price * $order->orderProduct->quantity) ?>
                                </div>
                            </div>
                            <?php if (!empty($order->orderProduct->extra)) { ?>
                                <?php foreach ($order->orderProduct->extra as $extra) { ?>
                                    <div class="order-job-row">
                                        <div class="order-job-cell" style="font-weight: 100">
                                            - <?= Html::encode($extra->title) ?>
                                        </div>
                                        <div class="order-job-cell">
                                            <?= $extra->quantity ?>
                                        </div>
                                        <div class="order-job-cell text-right">

                                        </div>
                                        <div class="order-job-cell text-right">
                                            <?= CurrencyHelper::convertAndFormat($order->currency_code, 'RUB', $extra->price * $extra->quantity) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="order-total text-right"><?= Yii::t('account', 'Total') ?> <?= $order->getTotal() ?>
                            </div>
                        </div>
                    </div>
                    <div class="order-steps">
                        <?php foreach ($order->history as $history) { ?>
                            <?php if ($history->statusesToViews[$history->type] !== null) { ?>
                                <?= $this->render("@frontend/modules/account/views/order/history-items/" . $history->statusesToViews[$history->type], [
                                    'model' => $order,
                                    'history' => $history
                                ]) ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <?= $historyForm ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->registerJs('
    $("#cancel-form, #justice-form, #complete-form").on("submit", function() {
        var $form = $(this),
            action = $(this).attr("action"),
            data = $(this).serialize();

        $.ajax({
            url: action,
            type: "post",
            data: data,
            success: function(response) {
                $form.closest(".modal").modal("hide");
                $.pjax.reload({container: "#grid-container"});
            },
        });

        return false;
    });
');

$packagesUrl = Url::to(['/ajax/packages-list']);
$sellersUrl = Url::to(['/ajax/sellers-list']);

$script = <<<JS
    $('#merge-toggle-checkbox').on('change', function() { 
        $('#mergePart').toggleClass('hidden'); 
    });

    $('#attribute-value-merge').on('submit', function() {
        let action = $(this).attr('action');
        $.post(action, $(this).serialize(), function(response) {
            if(response.success === true) {
                $('#modalHeader .close').trigger('click');
                $.pjax.reload({container: "#grid-container"});
            }
        });
        
        return false;
    });

    $("#seller-autocomplete").selectize({
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: false,
        maxItems: 1,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$sellersUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
        'onInitialize': function() {
            $('.selectize-control').removeClass('form-control');
        }
    });
    
    $("#job-autocomplete").selectize({
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: false,
        maxItems: 1,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$packagesUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
        'onInitialize': function() {
            $('.selectize-control').removeClass('form-control');
        }
    });
JS;

$this->registerJs($script);