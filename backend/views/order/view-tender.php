<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 15.03.2017
 * Time: 20:53
 */

\backend\assets\OrderDetailsAsset::register($this);

use common\components\CurrencyHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\store\models\Order;

/* @var Order $order */

$this->title = $order->product ? $order->product->getLabel() : 'Услуга не найдена';

?>

<?php if($order->status >= Order::STATUS_TENDER_PAID && $order->status < Order::STATUS_TENDER_DELIVERED) { ?>
    <div class="row">
        <div class="col-md-4 text-center">
            <?= Html::beginForm(['/order/cancel'], 'post', ['id' => 'cancel-form']) ?>
            <?= Html::hiddenInput('order_id', $order->id) ?>
            <?= Html::submitButton('Отменить заказ <br> (деньги заказчику)', ['class' => 'btn btn-danger', 'data-confirm' => 'Вы уверены что хотите пометить данный заказ как отмененный?']) ?>
            <?= Html::endForm() ?>
        </div>
        <div class="col-md-4 text-center">
            <?= Html::beginForm(['/order/justice'], 'post', ['id' => 'justice-form']) ?>
            <?= Html::hiddenInput('order_id', $order->id) ?>
            <?= Html::submitButton('Завершить заказ <br> (обоим по половине)', ['class' => 'btn btn-warning', 'disabled' => 'disabled']) ?>
            <?= Html::endForm() ?>
        </div>
        <div class="col-md-4 text-center">
            <?= Html::beginForm(['/order/complete'], 'post', ['id' => 'complete-form']) ?>
            <?= Html::hiddenInput('order_id', $order->id) ?>
            <?= Html::submitButton('Завершить заказ <br> (деньги исполнителю)', ['class' => 'btn btn-success', 'disabled' => 'disabled', 'data-confirm' => 'Вы уверены что хотите пометить данный заказ как завершенный?']) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
    <br>
<?php } ?>
    <div class="green-status" style="margin:0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if ($order->isProFound() === true) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Freelancer <br> is found') ?></div>
                        </div>
                    <?php } else { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-close"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Freelancer <br> is not found') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->isPriceApproved() === true) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Price <br> approved') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->isTenderPaid() === true) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Request <br> paid') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->isTenderDelivered() === true) { ?>
                        <div class="order-finished-step">
                            <i class="fa fa-check-circle-o"></i>
                            <div class="finished-steps-title"><?= Yii::t('labels', 'Service <br> delivered') ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($order->isTenderCompleted() === true) { ?>
                        <div class="order-status">
                            <i class="fa fa-check-circle-o"></i>
                            <?= Yii::t('labels', 'Order Completed') ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p>&nbsp;</p>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="order-history">
                    <div class="order-details">
                        <div class="order-descr-table">
                            <div class="order-img-cell">
                                <?= $order->product
                                    ? Html::a(Html::img($order->product->getThumb()), $order->product->getUrl(), [
                                        'class' => 'order-img'
                                    ])
                                    : ''
                                ?>
                            </div>
                            <div class="order-info">
                                <?= $order->product
                                    ? Html::a(Html::encode($order->product->getLabel()),$order->product->getUrl(), [
                                        'class' => 'order-link'
                                    ])
                                    : '<span style="color: red">Услуга не найдена</span>'
                                ?>
                                <div class="order-opts">
                                    <div class="order-opts-item">
                                        <span><?= Yii::t('account', 'Customer') ?>:</span>
                                        <?= Html::a($order->customer->getSellerName(),
                                            $order->customer->getSellerUrl()
                                        ) ?>
                                    </div>
                                    <div class="order-opts-item">
                                        <span><?= Yii::t('account', 'Order') ?>:</span> #<?= $order->id ?>
                                    </div>
                                    <div class="order-opts-item">
                                        <?= Yii::$app->formatter->asDate($order->created_at) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="order-price text-right"></div>
                        </div>
                        <div class="order-job-table">
                            <div class="order-job-row">
                                <div class="order-job-cell">
                                    <?= Yii::t('account', 'Service') ?>
                                </div>
                                <div class="order-job-cell">
                                    <?= Yii::t('account', 'Quantity') ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= Yii::t('account', 'Completion Date') ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= Yii::t('account', 'Total') ?>
                                </div>
                            </div>

                            <div class="order-job-row">
                                <div class="order-job-cell">
                                    <?= $order->product ? Html::encode($order->product->getLabel()) : '<span style="color: red">Услуга не найдена</span>' ?>
                                </div>
                                <div class="order-job-cell">
                                    <?= $order->orderProduct->quantity ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= Yii::$app->formatter->asDate($order->tbd_at) ?>
                                </div>
                                <div class="order-job-cell text-right">
                                    <?= !$order->product->contract_price
                                        ? CurrencyHelper::convertAndFormat($order->currency_code, 'RUB', $order->orderProduct->price * $order->orderProduct->quantity)
                                        : 'Договорная'
                                    ?>
                                </div>
                            </div>
                            <?php if(!empty($order->orderProduct->extra)) { ?>
                                <?php foreach($order->orderProduct->extra as $extra) { ?>
                                    <div class="order-job-row">
                                        <div class="order-job-cell" style="font-weight: 100">
                                            - <?= Html::encode($extra->title) ?>
                                        </div>
                                        <div class="order-job-cell">
                                            <?= $extra->quantity ?>
                                        </div>
                                        <div class="order-job-cell text-right">

                                        </div>
                                        <div class="order-job-cell text-right">
                                            <?= CurrencyHelper::convertAndFormat($order->currency_code, 'RUB', $extra->price * $extra->quantity) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="order-total text-right"><?= Yii::t('account', 'Total') ?> <?= $order->getTotal() ?>
                            </div>
                        </div>
                    </div>
                    <div class="order-steps">
                        <?php foreach ($order->history as $history) { ?>
	                        <?php if ($history->statusesToViews[$history->type] !== null) { ?>
		                        <?= $this->render("@frontend/modules/account/views/order/history-items/" . $history->statusesToViews[$history->type], [
			                        'model' => $order,
			                        'history' => $history
		                        ]) ?>
	                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?= $historyForm ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->registerJs('
    $("#cancel-form, #justice-form, #complete-form").on("submit", function() {
        var $form = $(this),
            action = $(this).attr("action"),
            data = $(this).serialize();

        $.ajax({
            url: action,
            type: "post",
            data: data,
            success: function(response) {
                $form.closest(".modal").modal("hide");
                $.pjax.reload({container: "#grid-container"});
            },
        });

        return false;
    });
');