<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.08.2017
 * Time: 17:10
 */

use common\components\CurrencyHelper;
use common\modules\store\assets\CheckoutAsset;
use frontend\assets\OrderDetailsAsset;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\store\models\Order;
use yii\widgets\Pjax;

\backend\assets\OrderDetailsAsset::register($this);

/* @var Order $order */
/* @var mixed $historyForm */

$this->title = $order->product ? $order->product->getLabel() : Yii::t('app', 'Product not found');


?>

<div class="green-status" style="margin:0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <?php if($order->status >= Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION) { ?>
                    <div class="order-finished-step">
                        <i class="fa fa-check-circle-o"></i>
                        <div class="finished-steps-title"><?= Yii::t('labels', 'Order <br> is paid') ?></div>
                    </div>
                <?php } else { ?>
                    <div class="order-finished-step"> <?= Yii::t('labels', 'Order <br> is not paid') ?>
                        <i class="fa fa-close"></i>
                        <div class="finished-steps-title"></div>
                    </div>
                <?php } ?>
                <?php if ($order->status >= Order::STATUS_PRODUCT_SENT) { ?>
                    <div class="order-finished-step">
                        <i class="fa fa-check-circle-o"></i>
                        <div class="finished-steps-title"><?= Yii::t('labels', 'Details <br> Specified') ?></div>
                    </div>
                <?php } ?>
                <?php if ($order->status >= Order::STATUS_PRODUCT_AWAITING_REVIEW) { ?>
                    <div class="order-finished-step">
                        <i class="fa fa-check-circle-o"></i>
                        <div class="finished-steps-title"><?= Yii::t('labels', 'Delivery <br> Submitted') ?></div>
                    </div>
                <?php } ?>
                <?php if ($order->status >= Order::STATUS_PRODUCT_COMPLETED) { ?>
                    <div class="order-status">
                        <i class="fa fa-check-circle-o"></i>
                        <?= Yii::t('labels', 'Order Completed') ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <p>&nbsp;</p>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="order-history">
                <div class="order-details">
                    <div class="order-descr-table">
                        <div class="order-img-cell">
                            <?= $order->product
                                ? Html::a(Html::img($order->product->getThumb()), $order->product->getUrl(), [
                                    'class' => 'order-img'
                                ])
                                : '' ?>
                        </div>
                        <div class="order-info">
                            <?= $order->product
                                ? Html::a(nl2br(Html::encode(StringHelper::truncate($order->product->getLabel(), 130))), $order->product->getUrl(), [
                                    'class' => 'order-link'
                                ])
                                : '<span style="color: red">' . Yii::t('app', 'Product not found') . '</span>'
                            ?>
                            <div class="order-descr">
                                <?= $order->product->getDescription() ?>
                            </div>
                            <div class="order-opts">
                                <div class="order-opts-item">
                                    <span><?= Yii::t('account', 'Seller') ?>:</span>
                                    <?= Html::a($order->seller->getSellerName(),
                                        $order->seller->getSellerUrl()
                                    ) ?>
                                </div>
                                <div class="order-opts-item">
                                    <span><?= Yii::t('account', 'Order') ?>:</span> #<?= $order->id ?>
                                </div>
                                <div class="order-opts-item">
                                    <?= Yii::$app->formatter->asDate($order->created_at) ?>
                                </div>
                            </div>
                        </div>
                        <div class="order-price text-right"></div>
                    </div>
                    <div class="order-job-table">
                        <div class="order-job-row">
                            <div class="order-job-cell">
                                <?= Yii::t('account', 'Product') ?>
                            </div>
                            <div class="order-job-cell">
                                <?= Yii::t('account', 'Quantity') ?>
                            </div>
                            <div class="order-job-cell text-right">
                                <?= Yii::t('account', 'Total') ?>
                            </div>
                        </div>

                        <div class="order-job-row">
                            <div class="order-job-cell">
                                <?= $order->product ? nl2br(Html::encode($order->product->getLabel())) : '<span style="color: red">' . Yii::t('app', 'Product not found') . '</span>' ?>
                            </div>
                            <div class="order-job-cell">
                                <?= $order->orderProduct->quantity ?>
                            </div>
                            <div class="order-job-cell text-right">
                                <?= CurrencyHelper::convertAndFormat($order->product->currency_code, Yii::$app->params['app_currency_code'], $order->orderProduct->price * $order->orderProduct->quantity, true) ?>
                            </div>
                        </div>
                        <?php if (!empty($order->orderProduct->extra)) { ?>
                            <?php foreach ($order->orderProduct->extra as $extra) { ?>
                                <div class="order-job-row">
                                    <div class="order-job-cell" style="font-weight: 100">
                                        - <?= Html::encode($extra->title) ?>
                                    </div>
                                    <div class="order-job-cell">
                                        <?= $extra->quantity ?>
                                    </div>
                                    <div class="order-job-cell text-right">
                                        <?= CurrencyHelper::convertAndFormat($extra->currency_code, Yii::$app->params['app_currency_code'], $extra->price * $extra->quantity, true) ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="order-total text-right"><?= Yii::t('account', 'Total') ?> <?= $order->getTotal() ?></div>
                    </div>
                </div>
                <div class="order-steps">
                    <?php foreach ($order->history as $history) { ?>
                        <?php if ($history->statusesToViews[$history->type] !== null) { ?>
                            <?= $this->render("@frontend/modules/account/views/order/history-items/" . $history->statusesToViews[$history->type], [
                                'model' => $order,
                                'history' => $history
                            ]) ?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?= $historyForm ?>
            </div>
        </div>
    </div>
</div>