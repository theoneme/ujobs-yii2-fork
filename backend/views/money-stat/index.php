<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.01.2017
 * Time: 15:34
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/* @var ActiveDataProvider $provider */
/* @var integer $month */
/* @var integer $year */

$this->title = Yii::t('app', 'Stats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <div>
                <?php \yii\widgets\Pjax::begin(['id' => 'stat-pjax']); ?>
                <?= Html::beginForm(['/money-stat/index'], 'get', ['id' => 'stat-form', 'data-pjax' => '']) ?>
                <div>
                    <div style="float:right" class="col-md-3">
                        <label>Год</label>
                        <?= Html::dropDownList('year', Yii::$app->request->get('year', null), [
                            array_combine(range(date('Y'), 2016), range(date('Y'), 2016))
                        ], [
                            'class' => 'form-control stat-trigger'
                        ]) ?>
                    </div>
                    <div style="float:right" class="col-md-3">
                        <label>Месяц</label>
                        <?= Html::dropDownList('month', Yii::$app->request->get('month', 3), [
                            array_combine(range(1, 12), range(1, 12))
                        ], [
                            'class' => 'form-control stat-trigger'
                        ]) ?>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $provider,
                        'id' => 'users',
                        'columns' => [
                            [
                                'header' => 'День',
                                'value' => function ($model) use ($year, $month){
                                    if(is_int($model['id'])) {
                                        return Yii::$app->formatter->asDate("{$year}-{$month}-{$model['id']}", 'long');
                                    } else {
                                        return $model['id'];
                                    }
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Оплаченные услуги',
                                'value' => function ($model) {
                                    return $model['paidServices'] ? $model['paidServices'] : 0;
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Сумма (работы)',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDecimal($model['paymentsServices'] ? $model['paymentsServices'] : 0) . ' р.';
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
	                        [
		                        'header' => 'Депозиты по заявкам',
		                        'value' => function ($model) {
			                        return $model['paidTenders'] ? $model['paidTenders'] : 0;
		                        },
		                        'headerOptions' => ['style' => 'font-size: 12px;'],
		                        'contentOptions' => ['style' => 'font-size: 15px;']
	                        ],
	                        [
		                        'header' => 'Сумма (заявки)',
		                        'value' => function ($model) {
			                        return Yii::$app->formatter->asDecimal($model['paymentsTenders'] ? $model['paymentsTenders'] : 0) . ' р.';
		                        },
		                        'headerOptions' => ['style' => 'font-size: 12px;'],
		                        'contentOptions' => ['style' => 'font-size: 15px;']
	                        ],
	                        [
		                        'header' => 'Пополнения счета',
		                        'value' => function ($model) {
			                        return Yii::$app->formatter->asDecimal($model['paymentsReplenishments'] ? $model['paymentsReplenishments'] : 0) . ' р.';
		                        },
		                        'headerOptions' => ['style' => 'font-size: 12px;'],
		                        'contentOptions' => ['style' => 'font-size: 15px;']
	                        ],
	                        [
		                        'header' => 'Сумма депозитов',
		                        'value' => function ($model) {
			                        return Yii::$app->formatter->asDecimal($model['depositsSum'] ? $model['depositsSum'] : 0) . ' р.';
		                        },
		                        'headerOptions' => ['style' => 'font-size: 12px;'],
		                        'contentOptions' => ['style' => 'font-size: 15px;']
	                        ],
	                        [
		                        'header' => 'Комиссия',
		                        'value' => function ($model) {
			                        return Yii::$app->formatter->asDecimal($model['comission'] ? $model['comission'] : 0) . ' р.';
		                        },
		                        'headerOptions' => ['style' => 'font-size: 12px;'],
		                        'contentOptions' => ['style' => 'font-size: 15px;']
	                        ],
                            [
                                'header' => 'Оплачено тарифов',
                                'value' => function ($model) {
                                    return $model['paidTariffs'] ? $model['paidTariffs'] : 0;
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Сумма (тарифы)',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDecimal($model['paymentsTariffs'] ? $model['paymentsTariffs'] : 0) . ' р.';
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Выплачено',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDecimal($model['withdrawals'] ? $model['withdrawals'] : 0)  . ' р.';
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
//                            [
//                                'header' => 'Кол-во активных заказов',
//                                'value' => function ($model) {
//                                    return $model['activeCount'] ? $model['activeCount'] : 0;
//                                },
//                                'headerOptions' => ['style' => 'font-size: 12px;'],
//                                'contentOptions' => ['style' => 'font-size: 15px;']
//                            ],
//                            [
//                                'header' => 'Сумма активных заказов',
//                                'value' => function ($model) {
//                                    return Yii::$app->formatter->asDecimal($model['activeSum'] ? $model['activeSum'] : 0) . ' р.';
//                                },
//                                'headerOptions' => ['style' => 'font-size: 12px;'],
//                                'contentOptions' => ['style' => 'font-size: 15px;']
//                            ],
                            [
                                'header' => 'Реклама',
                                'value' => function ($model) use ($month, $year) {
                                    return is_int($model['id']) ? Html::a(Yii::$app->formatter->asDecimal($model['advert']) . " р.",
                                        ['/money-stat/rekl', 'year' => $year, 'month' => $month, 'day' => $model['id']],
                                        [
                                            'title' => 'Реклама',
                                            'data-pjax' => 0,
                                            'class' => 'modal-edit',
                                        ]
                                    ) : Yii::$app->formatter->asDecimal($model['advert']) . " р.";
                                },
                                'format' => 'html',
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                        ],
                    ]); ?>
                    <?= Html::endForm() ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

<?php $this->registerJs('
    $(document).on("change", ".stat-trigger", function() {
        $("#stat-form").submit();
    });
');?>