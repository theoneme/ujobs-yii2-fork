<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.03.2017
 * Time: 18:57
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="setting-form">

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'options'=>[
                    'enctype'=>'multipart/form-data', // important
                    'class' => 'ajax-submit',
                    'id' => 'page-update',
                    'data-pj-container' => 'stat-pjax'
                ]
            ]); ?>

            <?= $form->field($model, 'advertisement')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'year')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'day')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'month')->hiddenInput(['maxlength' => true])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box-body -->
    </div>
</div>
