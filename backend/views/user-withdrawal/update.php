<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 19:26
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserWithdrawal */

$this->title = Yii::t('model', 'Update {modelClass}: ', [
        'modelClass' => 'User Withdrawal',
    ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('model', 'User Withdrawals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('model', 'Update');
?>
<div class="user-withdrawal-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>