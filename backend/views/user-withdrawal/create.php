<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 19:25
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserWithdrawal */

$this->title = Yii::t('model', 'Create User Withdrawal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('model', 'User Withdrawals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-withdrawal-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>