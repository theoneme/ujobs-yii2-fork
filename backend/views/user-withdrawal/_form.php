<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 19:25
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\UserWithdrawal;

/* @var $this yii\web\View */
/* @var $model common\models\UserWithdrawal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-withdrawal-form">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="alert alert-info">
                <?= $model->user->profile->name ? $model->user->profile->name : $model->user->username ?>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'amount')->textInput() ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'currency_code')->textInput(['maxlength' => true, 'disabled']) ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'target_account')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="clearfix">
                <?= $form->field($model, 'status')->dropDownList([
                    UserWithdrawal::STATUS_WAITING => 'Ожидает',
                    UserWithdrawal::STATUS_APPROVED => 'Одобрен',
                    UserWithdrawal::STATUS_DECLINED => 'Отклонен',
                    UserWithdrawal::STATUS_COMPLETED => 'Выполнен'
                ]) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('model', 'Create') : Yii::t('model', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>