<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 19:26
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserWithdrawal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('model', 'User Withdrawals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-withdrawal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('model', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('model', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('model', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'amount',
            'currency_code',
            'target_method',
            'target_account',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>