<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 18:54
 */

use common\components\CurrencyHelper;
use kartik\rating\StarRating;
use common\models\PaymentHistory;
use yii\web\View;

/* @var PaymentHistory $model
 * @var View $this
 */

?>

<td class="<?= $model['class']?>">
    <?= $model['orderId'] ?>
</td>
<td class="<?= $model['class']?>">
    <?= $model['date'] ?>
</td>
<td class="<?= $model['class']?>">
    <?= $model['job'] ?>
</td>
<td class="<?= $model['class']?>">
    <?php if($model['rating']) { ?>
        <?php if(is_int($model['rating'])) { ?>
            <?= StarRating::widget([
                'name' => 'rating_1',
                'value' => $model['rating'] ,
                'pluginOptions' => [
                    'theme' => 'krajee-uni',
                    'filledStar' => '&#x2605;',
                    'emptyStar' => '&#x2606;',
                    'size' => 'xxs',
                    'min' => 0,
                    'max' => 10,
                    'step' => 1,
                    'displayOnly' => true,
                ],
            ]); ?>
        <?php } else { ?>
            <?= $model['rating'] ?>
        <?php } ?>
    <?php } ?>
</td>
<td class="<?= $model['class']?>">
    <?= CurrencyHelper::convertAndFormat('RUB', 'RUB', $model['amount']) ?>
</td>
<td class="<?= $model['class']?>">
    <?= CurrencyHelper::convertAndFormat('RUB', 'RUB', $model['withdrawAmount']) ?>
</td>
<td class="<?= $model['class']?>">
    <?= CurrencyHelper::convertAndFormat('RUB', 'RUB', $model['balance']) ?>
</td>