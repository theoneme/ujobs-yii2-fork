<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 01.03.2017
 * Time: 17:34
 */
use yii\widgets\ListView;

?>

<div class="all-messages allgigs-tabs">
    <div class="over-table-viewgigs">

        <table class="table-viewgigs">
            <tr>
                <td>
                    ID заказа
                </td>
                <td>
                    Дата
                </td>
                <td>
                    Название <br> работы
                </td>
                <td>
                    Покупатель <br> принял
                </td>
                <td>
                    Сумма начисленная <br> (минус комиссия)
                </td>
                <td>
                    Сумма <br> выплаченная
                </td>
                <td>
                    Баланс
                </td>
            </tr>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'listview/payment-item',
                'id' => 'payments',
                'options' => [
                    'tag' => false,
                    'id' => 'messages-list'
                ],
                'itemOptions' => [
                    'tag' => 'tr',
                    'class' => 'messages-row',
                ],
                'emptyTextOptions' => [
                    'tag' => 'tr',
                    'class' => 'messages-row'
                ],
                'emptyText' => "<td colspan='3'>" . Yii::t('yii', 'No results found.') . "</td>",
                'layout' => "{items}"
            ]) ?>
        </table>
    </div>
</div>
