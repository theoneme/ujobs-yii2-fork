<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 16:27
 */

use common\components\CurrencyHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\UserWithdrawal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserWithdrawalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('model', 'User Withdrawals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-withdrawal-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <?php Pjax::begin(['id' => 'withdrawals-pjax']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'id' => 'payments-grid',
                'columns' => [
                    [
                        'header' => 'Пользователь',
                        'attribute' => 'username',
                        'value' => function ($model) {
                            return Html::a($model->user->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->user->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html'
                    ],
                    [
                        'attribute' => 'amount',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model UserWithdrawal */
                            return CurrencyHelper::format($model->currency_code, $model->amount);
                        },
                    ],
                    'target_account',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model UserWithdrawal */
                            return $model->getStatusLabel(true);
                        },
                    ],
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => "Финансовая история пользователя {$model->user->getSellerName()}",
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'view':
                                    $url = Url::toRoute(["/user-withdrawal/view-history", 'userId' => $model->user_id]);
                                    break;
                                default:
                                    $url = Url::toRoute(["/user-withdrawal/{$action}", 'id' => $model->id]);
                                    break;
                            }
                            return $url;
                        }
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>