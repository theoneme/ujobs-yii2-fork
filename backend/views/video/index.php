<?php

use common\models\Video;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Videos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add Video'), ['/video/create'], [
            'class' => 'btn btn-success modal-edit',
            'title' => Yii::t('app', 'Update {0}', 'Video'),
        ]) ?>
    </p>
<?php Pjax::begin(['id' => 'grid-container']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'header' => 'Пользователь',
                'attribute' => 'username',
                'value' => function ($model) {
                    /* @var $model Video*/
                    return !$model->user->is_company ? Html::a(Html::img($model->user->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->user->getSellerName(),
                        ['/user/admin/update-ajax', 'id' => $model->user->id],
                        [
                            'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                            'data-pjax' => 0,
                            'class' => 'modal-edit'
                        ]
                    ) : Html::img($model->user->getThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->user->getSellerName();
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm');
                },
                'format' => 'raw',
                'contentOptions' => ['class' => 'grid-image-container', 'style' => 'max-width: 150px;'],
                'options' => ['style' => 'width: 150px']
            ],
            [
                'attribute' => 'title',
                'value' => function($model) {
                    return StringHelper::truncate($model->title, 45);
                },
                'contentOptions' => ['style' => 'width: 400px; white-space: normal;'],
            ],
            [
                'attribute' => 'price',
                'value' => function ($model) {
                    /* @var $model Video*/
                    return $model->getPriceString();
                },
            ],
            [
                'attribute' => 'locale',
                'header' => 'Язык',
                'filter' => Yii::$app->params['languages']
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model Video*/
                    return $model->getStatusLabel(true);
                },
                'filter' => Video::getStatusLabels()
            ],
            [
                'header' => 'Превью',
                'value' => function ($model) {
                    /* @var $model Video*/
                    return Html::img($model->getThumb(), ['style' => 'width: 50px']);
                },
                'format' => 'html',
                'contentOptions' => ['class' => 'grid-image-container', 'style' => 'max-width: 50px;'],
                'options' => ['style' => 'width: 50px']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => Yii::$app->user->identity->isAdmin() ? '{view}{update}{delete}' : '{view}{update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                            'data-pjax' => 0,
                            'class' => 'modal-edit'
                        ]);
                    },
                    'view' => function ($url, $model) {
                        /* @var $model Video*/
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                            'data-video' => $model->video,
                            'title' => 'Просмотреть видео',
                            'data-pjax' => 0,
                            'class' => 'view-video'
                        ]);
                    }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'update':
                            $url = Url::toRoute(["/video/{$action}", 'id' => $model->id]);
                            break;
                        default:
                            $url = Url::toRoute(["/video/{$action}", 'id' => $model->id]);
                            break;
                    }
                    return $url;
                }
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<div class="modal fade video-modal" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header header-col text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
    $(document).on('click', '.view-video', function () {
        var src = $(this).data('video');
        $('#videoModal .modal-body').html('<video width=\"100%\" controls><source src=\"' + src +  '\"></video>');
        $('#videoModal').modal('show');
        return false;
    })
    $('#videoModal').on('hidden.bs.modal', function (e) {
		$('#videoModal .modal-body').html('');
	});
");