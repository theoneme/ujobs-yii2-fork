<?php

use backend\assets\MyCropAsset;
use common\helpers\FileInputHelper;
use common\models\Category;
use common\models\Video;
use common\modules\store\models\Currency;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Video */
/* @var $form yii\widgets\ActiveForm */

MyCropAsset::register($this);
$rootCategoriesList = ArrayHelper::map(
    Category::find()
        ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
        ->andWhere(['lvl' => 1])
        ->joinWith(['translation'])
        ->andWhere(['type' => 'job_category'])
        ->all(),
    'id',
    'translation.title'
);
?>

<div class="video-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'video-form',
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
            'class' => 'ajax-submit'
        ]
    ]); ?>
    <?= $form->field($model, 'locale')->dropDownList(Yii::$app->params['languages'])->label('Язык видео') ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, "description")->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'fullscreen',
                'fontsize',
                'fontcolor'
            ],
            'buttonsHide' => ['formatting']
        ]
    ]);
    ?>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
            <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                'prompt' => ' -- ' . Yii::t('app', 'Select A Category'),
                'class' => 'form-control'
            ])->label(false); ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
            <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                'pluginOptions' => [
                    'initialize' => true,
                    'depends' => ['postvideoform-parent_category_id'],
                    'placeholder' => ' -- ' . Yii::t('app', 'Select A Subcategory'),
                    'url' => Url::to(['/category/subcat']),
                    'params' => ['depdrop-helper']
                ],
                'options' => [
                    'class' => 'form-control',
                ]
            ])->label(false); ?>

            <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, "currency_code")->dropDownList(Currency::getFormattedCurrencies(), [
                'class' => 'form-control'
            ])->label('Валюта'); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'price')->textInput(['placeholder' => '9 999', 'id' => 'price', 'type' => 'number']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'free', ['template' => '{label}{input}'])
                ->checkbox(['id' => 'free-price-checkbox', 'style' => 'display: block;'], false)
                ->label('Бесплатно')
            ?>
        </div>
    </div>

    <div class="file-input-container">
        <label class="control-label" for="video-image">Изображение-превью</label>
        <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
            'id' => 'file-upload-input',
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'uploadUrl' => Url::toRoute('/image/upload-wide'),
                'overwriteInitial' => true,
                'initialPreview' => !empty($model->image) ? $model->getThumb() : [],
                'initialPreviewConfig' => !empty($model->image) ? [[
                    'caption' => basename($model->image),
                    'url' => Url::toRoute('/image/delete'),
                    'key' => $model->id
                ]] : [],
                'otherActionButtons' =>
                    '<button type="button" class="btn btn-sm btn-default crop-wide-button" title="' . Yii::t('app', 'Crop image') . '">
                        <i class="fa fa-crop" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-wide-button" title="' . Yii::t('app', 'Rotate image') . '">
                        <i class="fa fa-rotate-left" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-wide-button" title="' . Yii::t('app', 'Rotate image') . '">
                        <i class="fa fa-rotate-right" aria-hidden="true"></i>
                    </button>',
            ]
        ])) ?>
        <div class="files-container">
            <?= $form->field($model, 'image')->hiddenInput([
                'id' => 'video-image-input',
                'value' => $model->image,
                'data-key' => $model->id
            ])->label(false); ?>
        </div>
    </div>

    <div class="file-input-container">
        <label class="control-label" for="video"><?= Yii::t('app', 'Video')?></label>
        <?= FileInput::widget([
            'name' => 'uploaded_files[]',
            'id' => 'file-upload-input-video',
            'options' => ['accept' => 'video/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/file/upload']),
                'allowedFileExtensions' => ['mp4', 'ogg', 'webm'],
                'showPreview' => true,
                'dropZoneEnabled' => true,
                'initialPreviewAsData' => true,
                'showCancel' => false,
                'showRemove' => false,
                'showUpload' => false,
                'fileActionSettings' => [
                    'showZoom' => false,
                    'showDrag' => false,
                    'showUpload' => false
                ],
                'previewThumbTags' => [
                    '{actions}' => '{actions}',
                ],
                'initialPreview' => !empty($model->video) ? $model->video : [],
                'initialPreviewConfig' => !empty($model->video) ? [[
                    'type' => 'video',
                    'filetype' => 'video/*',
                    'key' => 'video-' . $model->id,
                    'caption' => basename($model->video),
                    'url' => Url::toRoute('/file/delete')
                ]] : [],
                'uploadExtraData' => new JsExpression("
                    function (previewId, index) {
                        if (previewId !== undefined) {
                            var obj = $('#' + previewId).data();
                            return obj;
                        }
                    }
                "),
                'previewTemplates' => [
                    'video' => '
                        <div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}" title="{caption}">
                            <div class="kv-file-content">
                                <video class="kv-preview-data file-preview-video" controls {style}>
                                    <source src="{data}">
                                    <div class="file-preview-other">
                                        <span class="file-other-icon"><i class="glyphicon glyphicon-file"></i></span>
                                    </div>                                
                                </video>
                            </div>
                            {footer}
                        </div>
                    '
                ]
            ]
        ]) ?>
        <div class="files-container">
            <?= $form->field($model, 'video')->hiddenInput([
                'id' => 'video-input',
                'value' => $model->video,
                'data-key' => 'video-' . $model->id
            ])->label(false); ?>
        </div>
    </div>
    <div class="formpost-block row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->radioList(Video::getStatusLabels()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$videoRequiredError = Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => $model->getAttributeLabel('video')]);
$script = <<<JS
    $('.file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
        let response = data.response;
        $(this).closest('.file-input-container').find('.files-container input').val(response.uploadedPath).data('key', response.imageKey);
    }).on('filedeleted', function(event, key) {
        $(this).closest('.file-input-container').find('.files-container input').val('');
    }).on('filebatchuploadcomplete', function(event, files, extra) {
        $('#submit-button').trigger('click');
    });

    $('#submit-button').on('click', function(e) {
        let videoFileInput = $('#file-upload-input-video');
        let videoFilesContainer = videoFileInput.closest('.file-input-container').find('.files-container');
        if (!videoFilesContainer.find('input').val().length && !videoFileInput[0].files.length) {
            videoFilesContainer.find('.help-block').html('$videoRequiredError');
            videoFilesContainer.find('.form-group').addClass('has-error').removeClass('has-success');
            return false;
        }
        let returnValue = true;
        $('.file-upload-input').each(function(index){
            if ($(this).fileinput('getFilesCount') > 0) {
                $(this).fileinput('upload');
                returnValue = false;
                return false;
            }
        });
        if (!returnValue) {
            return false;
        }
    });
    $("#video-form").on("afterValidate", function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
            let firstError = $(this).find(".has-error").first();
            $(this).closest('.modal').animate({
                scrollTop: firstError.offset().top
            }, 500);
        }
    });
    
    $(document).on("change", "#free-price-checkbox", function() {
        if ($(this).prop("checked")) {
            $("#price").parent().removeClass("has-error");
            $("#price").siblings(".help-block").html("");
            $("#price").val("");
        } else {
            $("#price").prop("disabled", false);
        }
    });

    $(document).on("keyup", "#price", function() {
        let value = $(this).val();
        value = parseInt(value.replace(/\s/g, ""));
        value = value > 0 ? value : 0;
        $("#free-price-checkbox").prop("checked", value === 0);
    });
JS;

$this->registerJs($script);