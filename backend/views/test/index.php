<?php

use common\models\search\TestSearch;
use common\models\Test;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * @var TestSearch $unitSearch
 * @var TestSearch $acceptanceSearch
 * @var TestSearch $functionalSearch
 * @var ActiveDataProvider $unitProvider
 * @var ActiveDataProvider $functionalProvider
 * @var ActiveDataProvider $acceptanceProvider
 */

$this->title = Yii::t('model', 'Tests');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="test-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Юнит тесты</h3>
        </div>
        <div class="box-body">
            <?php \yii\widgets\Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $unitProvider,
                'filterModel' => $unitSearch,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'format' => 'html',
                        'header' => 'Статус',
                        'value' => function ($value) {
                            $html = null;

                            switch ($value->status) {
                                case Test::STATUS_SUCCESS:
                                    $html = "<a class='btn btn-xs btn-success'>Успех</a>";
                                    break;
                                case Test::STATUS_FAIL:
                                    $html = "<a class='btn btn-xs btn-danger'>Неудача</a>";
                                    break;
                            }

                            return $html;
                        },
                    ],

//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Функциональные тесты</h3>
        </div>
        <div class="box-body">
            <?php \yii\widgets\Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $functionalProvider,
                'filterModel' => $functionalSearch,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'format' => 'html',
                        'header' => 'Статус',
                        'value' => function ($value) {
                            $html = null;

                            switch ($value->status) {
                                case Test::STATUS_SUCCESS:
                                    $html = "<a class='btn btn-xs btn-success'>Успех</a>";
                                    break;
                                case Test::STATUS_FAIL:
                                    $html = "<a class='btn btn-xs btn-danger'>Неудача</a>";
                                    break;
                            }

                            return $html;
                        },
                    ],

//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Приемочные тесты</h3>
        </div>
        <div class="box-body">
            <?php \yii\widgets\Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $acceptanceProvider,
                'filterModel' => $acceptanceSearch,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'format' => 'html',
                        'header' => 'Статус',
                        'value' => function ($value) {
                            $html = null;

                            switch ($value->status) {
                                case Test::STATUS_SUCCESS:
                                    $html = "<a class='btn btn-xs btn-success'>Успех</a>";
                                    break;
                                case Test::STATUS_FAIL:
                                    $html = "<a class='btn btn-xs btn-danger'>Неудача</a>";
                                    break;
                            }

                            return $html;
                        },
                    ],

//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>

