<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.01.2017
 * Time: 17:52
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\PaymentHistory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PaymentHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Histories';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="payment-history-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>    <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'created_at:datetime',
                    [
                        'header' => 'Сумма',
                        'attribute' => 'amount',
                        'value' => function($model) {
                            return $model->getAmount();
                        }
                    ],
                    [
                        'header' => 'Способ оплаты',
                        'value' => function($model) {
                            return $model->order->paymentMethod->name;
                        }
                    ],
                    [
                        'header' => 'Пользователь',
                        'attribute' => 'user.profile.name',
                        'value' => function ($model) {
                            return Html::a(!empty($model->user->profile->name) ? $model->user->profile->name : $model->user->username,
                                ['/user/admin/update-ajax', 'id' => $model->user->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],

                    [
                        'header' => 'Тип операции',
                        'attribute' => 'type',
                        'format' => 'html',
                        'value' => function($model) {
                            switch($model->type) {
                                case PaymentHistory::TYPE_OPERATION_PURCHASE:
                                    return Html::a("Оплата заказа (ID " . $model->order_id . ")", Url::to(['/order/view', 'id' => $model->order_id]), [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]);
                                    break;
                                case PaymentHistory::TYPE_OPERATION_REVENUE:
                                    return Html::a("Оплата за выполненный заказ (ID " . $model->order_id . ")", Url::to(['/order/view', 'id' => $model->order_id]), [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]);
                                    break;
                                case PaymentHistory::TYPE_OPERATION_REFUND:
                                    return Html::a( Yii::t('model', 'Refund for canceled order') . " (ID " . $model->order_id . ")", Url::to(['/order/view', 'id' => $model->order_id]), [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]);
                                    break;
                                default:
                                    return '';
                            }
                        }
                    ],
                    // 'balance_change',
                    // 'amount',
                    // 'currency_code',
                    // 'created_at',
                    // 'updated_at',

                    /*['class' => 'yii\grid\ActionColumn'],*/
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>