<?php

use backend\assets\MyCropAsset;
use common\models\Banner;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
MyCropAsset::register($this);
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Banner'), ['/banner/create'],
            [
                'title' => Yii::t('app', 'Create {0}', 'Banner'),
                'data-pjax' => 0,
                'class' => 'btn btn-success modal-edit'
            ]
        ) ?>
    </p>
<?php Pjax::begin(['id' => 'grid-container']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'header' => 'Изображение',
                'attribute' => 'image',
                'value' => function ($model) {
                    /* @var $model Banner*/
                    return Html::img($model->getThumb(), ['style' => 'width: 100px']);
                },
                'format' => 'html',
            ],
            [
                'header' => 'Ссылка',
                'attribute' => 'url',
                'value' => function ($model) {
                    /* @var $model Banner*/
                    return Html::a($model->url, preg_match('/^http(s)?:\/\/.*/', $model->url) ? $model->url : Yii::$app->urlManagerFrontEnd->createUrl($model->url, true), ['target' => '_blank', 'data-pjax' => 0]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'locale',
                'header' => 'Язык',
                'filter' => Yii::$app->params['languages']
            ],
//            'type',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => Yii::$app->user->identity->isAdmin() ? '{update} {delete}' : '{update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                            'data-pjax' => 0,
                            'class' => 'modal-edit'
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
