<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */
/* @var $form yii\widgets\ActiveForm */
/* @var $isAjax bool */
?>

<div class="banner-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
            'class' => ($isAjax ? 'ajax-submit' : ''),
        ],
        'id' => 'banner-form',
    ]); ?>
        <?= $form->field($model, 'locale')->dropDownList(Yii::$app->params['languages'])->label('Язык') ?>
        <?= $form->field($model, 'uploadedImage')->widget(FileInput::class, [
            'options' => ['accept' => 'image/*'],
            'pluginOptions'=> [
                'allowedFileExtensions' => ['jpg','gif','png','jpeg'],
                'initialPreview' => [!empty($model->image) ? Html::img($model->getThumb(), ['class'=>'file-preview-image', 'alt'=>'', 'title'=>'']) : null],
            ]
        ])?>
        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    <!--    --><?//= $form->field($model, 'type')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            ) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
