<?php

use common\models\PropertyClient;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PropertyClient */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('model', 'Property Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-client-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'realtor',
                'header' => 'Риелтор',
                'value' => function ($model) {
                    /** @var PropertyClient $model */
                    return Html::a($model->createdBy->getSellerName(),
                        ['/user/admin/update-ajax', 'id' => $model->created_by],
                        [
                            'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                            'data-pjax' => 0,
                            'class' => 'modal-edit'
                        ]
                    );
                },
                'format' => 'html',
            ],
            [
                'header' => 'Клиент',
                'attribute' => 'client',
                'value' => function ($model) {
                    /** @var PropertyClient $model */
                    if($model->user_id !== null) {
                        return Html::a($model->user->getSellerName(),
                            ['/user/admin/update-ajax', 'id' => $model->user_id],
                            [
                                'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                'data-pjax' => 0,
                                'class' => 'modal-edit'
                            ]
                        );
                    }

                    return $model->getClientName();
                },
                'format' => 'html',
            ],
            'email:email',
            'first_name',
            'last_name',
            'phone',
            'mortgage',
            'info:ntext',
            [
                'format' => 'html',
                'header' => 'Статус',
                'attribute' => 'status',
                'value' => function ($value) {
                    $html = null;

                    switch ($value->status) {
                        case PropertyClient::STATUS_SENT:
                            $html = "<a class='btn btn-xs btn-primary'>Отправлено</a>";
                            break;
                        case PropertyClient::STATUS_REGISTERED:
                            $html = "<a class='btn btn-xs btn-success'>Зарегистрировался</a>";
                            break;
                    }

                    return $html;
                },
                'filter' => PropertyClient::getStatusLabels(),
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
