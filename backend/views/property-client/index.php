<?php

use common\models\PropertyClient;
use common\models\user\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PropertyClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('model', 'Property Clients');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'realtor',
                        'header' => 'Риелтор',
                        'value' => function ($model) {
                            /** @var PropertyClient $model */
                            return Html::a($model->createdBy->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->created_by],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Клиент',
                        'attribute' => 'client',
                        'value' => function ($model) {
                            /** @var PropertyClient $model */
                            if($model->user_id !== null) {
                                return Html::a($model->user->getSellerName(),
                                    ['/user/admin/update-ajax', 'id' => $model->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                );
                            }

                            return $model->getClientName();
                        },
                        'format' => 'html',
                    ],
                    [
                        'format' => 'html',
                        'header' => 'Статус',
                        'attribute' => 'status',
                        'value' => function ($value) {
                            $html = null;

                            switch ($value->status) {
                                case PropertyClient::STATUS_SENT:
                                    $html = "<a class='btn btn-xs btn-primary'>Отправлено</a>";
                                    break;
                                case PropertyClient::STATUS_REGISTERED:
                                    $html = "<a class='btn btn-xs btn-success'>Зарегистрировался</a>";
                                    break;
                            }

                            return $html;
                        },
                        'filter' => PropertyClient::getStatusLabels(),
                    ],
                    'created_at:datetime',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'View {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>