<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $form ActiveForm */
/* @var $locale string */
/* @var $type string */
?>
<div class="asdf">
    <?= $form->field($model, "[$locale]id")->hiddenInput()->label(false) ?>
    <?= $form->field($model, "[$locale]locale")->hiddenInput(["value" => $locale])->label(false); ?>
    <?= $form->field($model, "[$locale]title") ?>
    <?= $form->field($model, "[$locale]content_prev") ?>
</div>