<?php

use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use common\helpers\FileInputHelper;
use common\models\ContentTranslation;
use common\modules\store\models\PaymentMethod;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Tabs;

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model PaymentMethod */
/* @var $form ActiveForm */
/* @var $isAjax bool*/

MyCropAsset::register($this);
SelectizeAsset::register($this);
?>

<div class="page-form">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'options'=>[
                    'class' => ($isAjax ? 'ajax-submit' : ''),
                    'id' => 'payment-method-form'
				]
            ]); ?>

            <div class="translations-block">
                <?= Tabs::widget([
                    'items' => array_map(function ($locale, $language) use ($form, $model) {
                        return [
                            'label' => $language,
                            'content' => $this->render('_translation', [
                                'model' => $model->translations[$locale] ?? new ContentTranslation(),
                                'form' => $form,
                                'locale' => $locale,
                            ]),
                            'active' => $locale === 'ru-RU'
                        ];
                    }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
                ]);
                ?>
            </div>
            <?= $form->field($model, 'sort')?>
            <?= $form->field($model, 'enabled')->checkbox()?>
            <?= $form->field($model, 'additional')->checkbox()?>

            <div class="form-group selectize-input-container" style="margin-bottom: 50px;">
                <?= Html::label('Показывать в странах:', null, ['style' => 'margin: 0;'])?>
                <h6>(Если нужно показывать во всех странах - оставьте поле пустым)</h6>
                <?= Html::textarea("countries", null, ['id' => "countries-input"]) ?>
            </div>
            <div class="file-input-container">
                <?= FileInput::widget(
                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'id' => 'file-upload-input',
                        'pluginOptions' => [
                            'overwriteInitial' => true,
                            'minImageHeight' => null,
                            'minImageWidth' => null,
                            'initialPreview' => !empty($model->logo) ? [$model->logo . '?' .  time()] : [],
                            'initialPreviewConfig' => !empty($model->logo) ? [[
                                'caption' => basename($model->logo),
                                'url' => Url::toRoute('/image/delete'),
                                'key' => 'image_init_' . $model->id
                            ]] : [],
                        ]
                    ])
                )?>
                <div class="images-container">
                    <?= $form->field($model, 'logo')->hiddenInput([
                        'value' => $model->logo,
                        'data-key' => 'image_init_' . $model->id
                    ])->label(false);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                        'id' => 'submit-button',
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                    ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box-body -->

    </div>
</div>

<?php
$countryListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'country']);
$options = json_encode(array_map(
    function($value, $title) {return ['id' => $value, 'value' => $title];},
    array_keys($countries), $countries
));
$values = json_encode(array_keys($countries));
$script = <<<JS
    $.pjax.defaults.timeout = 6000;

    $("#payment-method-form").on("beforeValidateAttribute", function (event, attribute, messages) {
        if ($(attribute.container).closest('.translations-block').length) {
            return false;
        }
    });
    
    $("#submit-button").on("click", function(e) {
        let returnValue = true;
        $(".file-upload-input").each(function(index){
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                returnValue = false;
                return false;
            }
        });
        if (!returnValue) {
            return false;
        }
    });

    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        imagesContainer.find("input").remove();
        imagesContainer.append("<input name='PaymentMethod[logo]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key='" + key + "']").val('');
    }).on("filebatchuploadcomplete", function(event, files, extra) {
        $("#submit-button").trigger('click');
    });
        
    $('#countries-input').selectize({
        options: $options,
        items: $values,
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        maxItems: null,
        plugins: ["remove_button"],
        persist: false,
        'load': function(query, callback) {
            if (!query.length)
                return callback();
            $.post('$countryListUrl', {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }
            ).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script);