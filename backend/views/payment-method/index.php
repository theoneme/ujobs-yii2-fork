<?php

use backend\assets\SelectizeAsset;
use backend\models\PaymentMethodSearch;
use common\modules\store\models\PaymentMethod;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel PaymentMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

SelectizeAsset::register($this);

$this->title = 'Способы оплаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'header' => 'Название',
                        'value' => function ($model) {
                            /* @var $model PaymentMethod*/
                            return $model->name;
                        },
                        'format' => 'html',
                    ],
                    [
                        'header' => 'Код',
                        'value' => function ($model) {
                            /* @var $model PaymentMethod*/
                            return $model->code;
                        },
                    ],
                    [
                        'header' => 'Порядок сортировки',
                        'attribute' => 'sort',
                    ],
                    [
                        'attribute' => 'enabled',
                        'value' => function ($model) {
                            /* @var $model PaymentMethod*/
                            return $model->enabled ? '<div class="btn btn-success btn-xs">Да</div>' : '<div class="btn btn-danger btn-xs">Нет</div>';
                        },
                        'format' => 'html',
                        'filter' => [0 => 'Нет', 1 => 'Да']
                    ],
                    [
                        'attribute' => 'additional',
                        'value' => function ($model) {
                            /* @var $model PaymentMethod*/
                            return $model->additional ? '<div class="btn btn-success btn-xs">Да</div>' : '<div class="btn btn-danger btn-xs">Нет</div>';
                        },
                        'format' => 'html',
                        'filter' => [0 => 'Нет', 1 => 'Да']
                    ],
                    [
                        'attribute' => 'countries',
                        'header' => 'Показывать в странах',
                        'value' => function ($model) {
                            /* @var $model PaymentMethod*/
                            return implode(', ', ArrayHelper::getColumn($model->countries, 'translation.title'));
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => '',
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
