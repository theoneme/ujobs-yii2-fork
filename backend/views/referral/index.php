<?php

use common\models\Referral;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReferralSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Referrals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'from',
                        'label' => 'Пригласивший',
                        'value' => function ($model) {
                            /* @var $model Referral*/
                            return Html::a($model->createdBy->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->created_by],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->createdBy))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'attribute' => 'to',
                        'label' => 'Приглашённый',
                        'value' => function ($model) {
                            /* @var $model Referral*/
                            if (!empty($model->referral_id)) {
                                return Html::a($model->referral->getSellerName(),
                                    ['/user/admin/update-ajax', 'id' => $model->referral_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model->referral))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                );
                            }
                            else {
                                return null;
                            }
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    'email',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Referral */
                            return $model->getStatusLabel();
                        },
                    ],
                    'created_at:datetime',
                    'total_bonus',
                    // 'created_at',
                    // 'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => '{delete}',
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
