<?php

use common\models\UserEducation;
use yii\web\View;

/**
 * @var integer $index
 * @var UserEducation $model
 */

?>

<div class="row">
<!--    <div class="col-md-1 del-education">-->
<!--        <i class="fa fa-times-circle" aria-hidden="true"></i>-->
<!--    </div>-->
    <div class="col-md-5">
        <div id="container-title-<?= $index ?>">
<!--            --><?//= Html::activeTextInput($model, "[{$index}]title", ['class' => 'form-control']) ?>
            <?= $model->title?>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-md-5">
        <div id="container-degree-<?= $index ?>">
            <?= $model->degree?>
<!--            --><?//= Html::activeDropDownList($model, "[{$index}]degree", [
//                'Unspecified' => Yii::t('app', 'Unspecified'),
//                'Basic' => Yii::t('app', 'Basic'),
//                'Conversational' => Yii::t('app', 'Conversational'),
//                'Fluent' => Yii::t('app', 'Fluent'),
//                'Native' => Yii::t('app', 'Native')
//            ]
//            ) ?>
            <div class="help-block"></div>
        </div>
    </div>
</div>


<?php $this->registerJs('
    $("#public-profile-form").yiiActiveForm("add", {
        id: "usereducation-' . $index . '-title",
        name: "UserEducation[' . $index . '][title]",
        input: "#usereducation-' . $index . '-title",
        container: "#container-title-' . $index . '",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
        setTimeout(function(){console.log(yii.validation)}, 2033);
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Title")]) . '",
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "usereducation-' . $index . '-degree",
        name: "UserEducation[' . $index . '][degree]",
        input: "#usereducation-' . $index . '-degree",
        container: "#container-degree-' . $index . '",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Degree")]) . '"
            });
        }
    });
', View::POS_LOAD); ?>

