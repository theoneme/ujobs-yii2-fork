<?php

use backend\assets\AutocompleteAsset;
use backend\assets\SelectizeAsset;
use backend\models\search\UserSearch;
use common\components\CurrencyHelper;
use common\models\user\Profile;
use common\models\user\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 * @var array $sentMessages
 * @var array $unreadMessages
 * @var array $languages
 */

$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;

SelectizeAsset::register($this);
AutocompleteAsset::register($this);
?>

<?= $this->render('/_alert', [
    'module' => Yii::$app->getModule('user'),
]) ?>

<?= $this->render('/admin/_menu') ?>

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <?php Pjax::begin(['id' => 'user-pjax', 'timeout' => 6000]) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'id' => 'user-grid',
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                        'options' => ['style' => 'width: 72px']
                    ],
                    [
                        'header' => 'Дата',
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            if (extension_loaded('intl')) {
                                return Yii::t('user', '{0, date, dd.MM.YYYY HH:mm}', [$model->created_at]);
                            } else {
                                return date('Y-m-d G:i:s', $model->created_at);
                            }
                        },
                        'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                        'options' => ['style' => 'width: 72px']
                        //                    'filter' => DatePicker::widget([
                        //                        'model' => $searchModel,
                        //                        'attribute' => 'created_at',
                        //                        'dateFormat' => 'php:Y-m-d',
                        //                        'options' => [
                        //                            'class' => 'form-control',
                        //                        ],
                        //                    ]),
                    ],
                    [
                        'header' => 'Фото',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a(Html::img($model->profile->getThumb(), ['style' => 'width: 60px']),
                                ['/user/admin/update-ajax', 'id' => $model->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'contentOptions' => ['style' => 'width: 65px'],
                        'options' => ['style' => 'width: 65px']
                    ],
                    [
                        'attribute' => 'username',
                        'header' => 'Имя',
                        'value' => function ($model) {
                            /** @var User $model */
                            return Html::a($model->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'attribute' => 'email',
                        'contentOptions' => ['style' => 'width: 65px; white-space: normal;'],
                        'options' => ['style' => 'width: 65px']
                    ],
                    [
                        'attribute' => 'phone',
                        'contentOptions' => ['style' => 'width: 80px; white-space: normal;'],
                        'options' => ['style' => 'width: 80px']
                    ],
                    [
                        'header' => 'Соц',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model User */
                            $icons = [];
                            foreach ($model->socials as $social) {
                                $icons[] = Html::a('
                                    <span class="soc-square">
                                        <i class="fa fa-' . $social->provider . '" aria-hidden="true"></i>
                                    </span>',
                                    $social->getProfileLink(),
                                    ['target' => '_blank', 'data-pjax' => 0]
                                );
                            }
                            return implode('&nbsp;', $icons);
                        },
                        'contentOptions' => ['style' => 'width: 60px; white-space: normal'],
                        'options' => ['style' => 'width: 60px']
                    ],
                    [
                        'header' => 'О себе',
                        'value' => function ($model) {
                            /* @var $model User */
                            return StringHelper::truncate($model->profile->getDescription(), 70);
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 120px; white-space: normal; font-size: 12px;'],
                        'options' => ['style' => 'width: 120px']
                    ],
                    [
                        'attribute' => 'skill',
                        'header' => 'Навыки',
                        'value' => function ($model) {
                            /* @var $model User */
                            $skills = [];
                            foreach ($model->profile->skills as $index => $skill) {
                                if ($index == 3) {
                                    break;
                                }
                                $skills[] = $skill->title . ' ' . ($skill->customDataArray['level'] ?? '');
                            }
                            return implode('<br>', $skills);
                        },
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 120px; white-space: normal; text-align: center; font-size: 12px;'],
                        'options' => ['style' => 'width: 120px']
                    ],
                    /*[
                        'attribute' => 'registration_ip',
                        'value' => function ($model) {
                            return $model->registration_ip == null
                                ? '<span class="not-set">' . Yii::t('user', '(not set)') . '</span>'
                                : $model->registration_ip;
                        },
                        'format' => 'html',
                    ],*/
                    [
                        'header' => Yii::t('app', 'Jobs'),
                        'format' => 'html',
                        'value' => function ($model) {
                            $links = [];
                            foreach (array_slice($model->jobs, 0, 10) as $job) {
                                $links[] = Html::a('<small>' . StringHelper::truncate($job->translation ? $job->translation->title : '', 30) . '</small>',
                                    ["/{$job->type}/update", 'id' => $job->id, 'user_id' => $job->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($job))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                );
                            }
                            $linkString = implode('<br>', $links);
                            return $linkString;
                        },
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'header' => Yii::t('app', 'Requests'),
                        'format' => 'html',
                        'value' => function ($model) {
                            $links = [];
                            foreach (array_slice($model->tenders, 0, 10) as $job) {
                                $links[] = Html::a(
                                    '<small>' . StringHelper::truncate($job->translation ? $job->translation->title : '', 30) . '</small>',
                                    ["/{$job->type}/update", 'id' => $job->id, 'user_id' => $job->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($job))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                );
                            }
                            $linkString = implode('<br>', $links);
                            return $linkString;
                        },
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'header' => '$',
                        'value' => function ($model) {
                            /* @var $model User */
                            return Html::a(CurrencyHelper::format(Yii::$app->params['app_currency_code'], $model->getBalance()),
                                ['/user/admin/payment-history', 'userId' => $model->id],
                                [
                                    'title' => Yii::t('app', 'Balance'),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 50px; white-space: normal; text-align: center;'],
                        'options' => ['style' => 'width: 50px'],
                        'headerOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'header' => 'Бонусы',
                        'value' => function ($model) {
                            /* @var $model User */
                            return CurrencyHelper::format('CTY', isset($model->wallets['CTY']) ? $model->wallets['CTY']->balance : 0);
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 75px; white-space: normal; text-align: center;'],
                        'options' => ['style' => 'width: 75px']
                    ],
                    [
                        'header' => 'Письма',
                        'value' => function ($model) use ($sentMessages, $unreadMessages) {
                            /* @var $model User */
                            $sent = isset($sentMessages[$model->id]) ? $sentMessages[$model->id] : 0;
                            $unread = isset($unreadMessages[$model->id]) ? $unreadMessages[$model->id] : 0;
                            return $sent . ' отправлено<br>' .
                                Html::a($unread . ' не прочитано',
                                    Yii::$app->urlManagerFrontEnd->createUrl(['/account/inbox/start-support-conversation', 'user_id' => $model->id]),
                                    ['target' => '_blank', 'data-pjax' => 0]
                                );
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'attribute' => 'status',
                        'header' => 'Статус',
                        'value' => function ($model) {
                            /* @var $model User */
                            return $model->profile->getStatusLabel(true);
//                            return ($model->profile->status == Profile::STATUS_ACTIVE || Job::find()->select('id')->where(['type' => 'job', 'job.status' => Job::STATUS_ACTIVE, 'user_id' => $model->id])->exists()) ? 'Опубликован' : 'Не опубликован';
                        },
                        'filter' => Profile::getStatusLabels(),
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'attribute' => 'language',
                        'header' => 'Языки',
                        'value' => function ($model) {
                            /* @var $model User */
                            return implode('<br>', array_map(function($var){return $var->title;}, $model->profile->languages));
                        },
                        'filter' => $languages,
                        'format' => 'html',
                        'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                        'options' => ['style' => 'width: 100px']
                    ],
                    [
                        'header' => Yii::t('user', 'Confirmation'),
                        'value' => function ($model) {
                            if ($model->isConfirmed) {
                                return '<div class="text-center"><span class="text-success">' . Yii::t('user', 'Confirmed') . '</span></div>';
                            } else {
                                return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                                    'class' => 'btn btn-xs btn-success btn-block',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                                ]);
                            }
                        },
                        'format' => 'raw',
                        'visible' => Yii::$app->getModule('user')->enableConfirmation,
                    ],
//                    [
//                        'header' => 'Комментарий',
//                        'attribute' => 'admin_comment',
//                        'value' => function ($model) {
//                            return Html::a(!empty($model->admin_comment) ? $model->admin_comment : '<span class="not-set">(не задано)</span>',
//                                ['/user/admin/update-comment', 'id' => $model->id],
//                                [
//                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
//                                    'data-pjax' => 0,
//                                    'class' => 'modal-edit admin-comment',
//                                ]
//                            );
//                        },
//                        'format' => 'html',
//                        'contentOptions' => [
//                            'style' => 'width: 150px; white-space: normal; text-align: center; font-size: 12px; position: relative;'
//                        ],
//                        'options' => ['style' => 'width: 150px']
//                    ],
                    /*[
                        'header' => Yii::t('user', 'Block status'),
                        'value' => function ($model) {
                            if ($model->isBlocked) {
                                return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                                    'class' => 'btn btn-xs btn-success btn-block',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                                ]);
                            } else {
                                return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                                    'class' => 'btn btn-xs btn-danger btn-block',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                                ]);
                            }
                        },
                        'format' => 'raw',
                    ],*/
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'contentOptions' => ['style' => 'width: 60px'],
                        'options' => ['style' => 'width: 60px'],
                        'visible' => Yii::$app->user->identity->isAdmin()
                    ],
                ],
            ]); ?>
            <?php Pjax::end() ?>
        </div>
        <!-- /.box-body -->
    </div>

<?php $script = <<<JS
    var googleAutocomplete;
JS;
$this->registerJs($script);

