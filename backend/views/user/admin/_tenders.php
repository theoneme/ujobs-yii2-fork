<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\models\Profile $profile
 */

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add Request'), ['/tender/create', 'user_id' => $user->id, 'type' => 'tender'], [
            'class' => 'btn btn-success modal-edit',
            'title' => Yii::t('app', 'Update {0}', 'Request')
        ]) ?>
    </p>

<?php Pjax::begin(['id' => 'grid-container']); ?>    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        'created_at:datetime',
        'translation.title',
        'status',

        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                        'data-pjax' => 0,
                        'class' => 'modal-edit'
                    ]);
                }
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                switch ($action) {
                    case 'update':
                        $url = Url::toRoute(["/{$model->type}/{$action}", 'id' => $model->id, 'user_id' => Yii::$app->request->get('user_id')]);
                        break;
                    default:
                        $url = Url::toRoute(["/{$model->type}/{$action}", 'id' => $model->id]);
                        break;
                }
                return $url;
            }
        ],
    ],
]); ?>
<?php Pjax::end(); ?>

<?php $this->endContent() ?>