<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.05.2017
 * Time: 13:45
 */

use common\models\PaymentHistory;
use yii\web\View;

/* @var PaymentHistory $model
 * @var View $this
 */

?>


<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Date')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->created_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'For')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->description ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('account', 'Amount')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getColoredAmount($model->currency_code) ?>
    </div>
</div>
