<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 18:54
 */

use common\models\PaymentHistory;
use yii\web\View;

/** @var PaymentHistory $model
 * @var View $this
 * @var string $walletLetter
 * @var string $currency_code
 */

?>

<td width="50px" class="hidden-xs">
    <?= Yii::$app->formatter->asDate($model->created_at) ?>
</td>
<td>
    <div class="sum">
        <span class="doing"><?= $model->description ?? $model->getHeader() ?></span>
    </div>
</td>
<td>
    <div class="time"><?= $model->getColoredAmount($model->currency_code) ?></div>
</td>