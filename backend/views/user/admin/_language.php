<?php

use common\models\forms\UserLanguageForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var integer $index
 * @var UserLanguageForm $model
 */

?>

<div class="row lang-com-item">
    <div class="col-md-1 del-lang">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
    </div>
    <div class="col-md-5">
        <div id="container-language-<?= $index ?>">
            <?= Html::activeHiddenInput($model, "[{$index}]id", ['class' => 'form-control']) ?>
            <?= Html::activeTextInput($model, "[{$index}]title", ['id' => "userlanguageform-{$index}-title", 'class' => 'form-control']) ?>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-md-5">
        <div id="container-level-<?= $index ?>">
            <?= Html::activeDropDownList($model, "[{$index}]level", [
                'Unspecified' => Yii::t('labels', 'Unspecified'),
                'Basic' => Yii::t('labels', 'Basic'),
                'Conversational' => Yii::t('labels', 'Conversational'),
                'Fluent' => Yii::t('labels', 'Fluent'),
                'Native' => Yii::t('labels', 'Native')
            ], ['class' => 'form-control']
            ) ?>
            <div class="help-block"></div>
        </div>
    </div>
</div>

<?php $languageUrl = Url::to(['/ajax/attribute-list', 'alias' => 'language']);
$languageMessage = Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Language")]);
$levelMessage = Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Level")]);

$script = <<<JS
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userlanguageform-{$index}-title",
        name: "UserLanguageForm[{$index}][title]",
        input: "#userlanguageform-{$index}-title",
        container: "#container-language-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
        setTimeout(function(){console.log(yii.validation)}, 2033);
            yii.validation.required(value, messages, {
                message: "{$languageMessage}",
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userlanguageform-{$index}-level",
        name: "UserLanguageForm[{$index}][level]",
        input: "#userlanguageform-{$index}-level",
        container: "#container-level-{$index}",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: "{$levelMessage}"
            });
        }
    });
    
    new autoComplete({
        selector: "#input_language_{$index}",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post('{$languageUrl}', { query: request }, function(data) { 
                response($.map(data, function(value, key) {
                    return value.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script, View::POS_LOAD);

