<?php

use common\models\user\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


/**
 * @var yii\web\View $this
 * @var User $user
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/user/admin/update-comment', 'id' => $user->id]),
//    'enableAjaxValidation' => true,
//    'enableClientValidation' => false,
    'options' => [
        'id' => 'user-comment-form',
        'data-pjax' => false,
        'class' => 'ajax-submit',
        'data-pj-container' => 'user-pjax'
    ],
]); ?>

    <?= $form->field($user, 'admin_comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>