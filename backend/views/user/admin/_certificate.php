<?php

use common\models\UserCertificate;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var integer $index
 * @var UserCertificate $model
 */

?>

<div class="row">
<!--    <div class="col-md-1 del-certificate">-->
<!--        <i class="fa fa-times-circle" aria-hidden="true"></i>-->
<!--    </div>-->
    <div class="col-md-5">
        <div id="container-title-<?= $index ?>">
            <?= $model->title?>
<!--            --><?//= Html::activeTextInput($model, "[{$index}]title", ['class' => 'form-control']) ?>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-md-5">
        <div id="container-year-<?= $index ?>">
            <?= $model->year?>
<!--            --><?//= Html::activeDropDownList($model, "[{$index}]year", [
//                'Unspecified' => Yii::t('app', 'Unspecified'),
//                'Basic' => Yii::t('app', 'Basic'),
//                'Conversational' => Yii::t('app', 'Conversational'),
//                'Fluent' => Yii::t('app', 'Fluent'),
//                'Native' => Yii::t('app', 'Native')
//            ]) ?>
            <div class="help-block"></div>
        </div>
    </div>
</div>


<?php $this->registerJs('
    $("#public-profile-form").yiiActiveForm("add", {
        id: "usercertificate-' . $index . '-title",
        name: "UserCertificate[' . $index . '][title]",
        input: "#usercertificate-' . $index . '-title",
        container: "#container-title-' . $index . '",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
        setTimeout(function(){console.log(yii.validation)}, 2033);
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Title")]) . '",
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "usercertificate-' . $index . '-year",
        name: "UserCertificate[' . $index . '][year]",
        input: "#usercertificate-' . $index . '-year",
        container: "#container-year-' . $index . '",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Year")]) . '"
            });
        }
    });
', View::POS_LOAD); ?>

