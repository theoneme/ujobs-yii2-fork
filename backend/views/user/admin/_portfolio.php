<?php

use common\models\UserPortfolio;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var integer $index
 * @var UserPortfolio $model
 */

?>

<div class="row portfolio-com-item">
    <div class="col-md-1 del-portfolio">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
    </div>
    <div class="col-md-5">
        <div id="container-title-<?= $index ?>">
            <?= Html::activeHiddenInput($model, "[{$index}]id", ['class' => 'form-control']) ?>
            <?= Html::activeTextInput($model, "[{$index}]title", ['class' => 'form-control']) ?>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-md-5">
        <div id="container-description-<?= $index ?>">
            <?= Html::activeTextInput($model, "[{$index}]description", ['class' => 'form-control']) ?>
            <div class="help-block"></div>
        </div>
    </div>
</div>


<?php $this->registerJs('
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userportfolio-' . $index . '-title",
        name: "UserPortfolio[' . $index . '][title]",
        input: "#userportfolio-' . $index . '-title",
        container: "#container-title-' . $index . '",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
        setTimeout(function(){console.log(yii.validation)}, 2033);
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Title")]) . '",
            });
        }
    });
    $("#public-profile-form").yiiActiveForm("add", {
        id: "userportfolio-' . $index . '-description",
        name: "UserPortfolio[' . $index . '][description]",
        input: "#userportfolio-' . $index . '-description",
        container: "#container-description-' . $index . '",
        error: ".help-block",
        validate:  function (attribute, value, messages) {
            yii.validation.required(value, messages, {
                message: "' . Yii::t("yii", "{attribute} cannot be blank.", ["attribute" => Yii::t("model", "Description")]) . '"
            });
        }
    });
', View::POS_LOAD); ?>

