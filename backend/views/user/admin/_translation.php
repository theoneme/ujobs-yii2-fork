<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $locale string */
/* @var $form ActiveForm */

?>
<div class="asdf container-fluid">
    <?= Html::activeHiddenInput($model, "[$locale]id"); ?>
    <?= $form->field($model, "[$locale]title", [
        'wrapperOptions' => ['tag' => false],
        'labelOptions' => ['class' => 'control-label']
    ])->label('↓↓↓↓↓ ИМЯ ПОЛЬЗОВАТЕЛЯ НА ДАННОМ ЯЗЫКЕ (ЕГО НАДО ЗАПОЛНИТЬ ЧТОБЫ ПЕРЕВОД СОХРАНИЛСЯ) ↓↓↓↓↓') ?>
    <?= $form->field($model, "[$locale]content", [
        'wrapperOptions' => ['tag' => false],
        'labelOptions' => ['class' => 'control-label']
    ])->textarea(['rows' => 6]) ?>
    <?= Html::activeHiddenInput($model, "[$locale]locale", ['value' => $locale]); ?>
</div>