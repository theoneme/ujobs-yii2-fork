<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.01.2017
 * Time: 18:53
 */

use common\components\CurrencyHelper;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\web\View;
use yii\data\ActiveDataProvider;
use frontend\models\WithdrawalRequestForm;

$this->title = Yii::t('account', 'Manage Payments');

/* @var mixed $tabHeader
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var integer $posSum
 * @var integer $negSum
 * @var integer $balance
 * @var WithdrawalRequestForm $withdrawalRequestForm
 */

?>

<div class="inbox-background">
    <div class="profile">
        <div class="container-fluid">
            <?php Pjax::begin([
                'id' => 'payments-pjax',
                'scrollTo' => false,
				'enablePushState' => false
            ]); ?>

            <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                <a href="#" class="stat-alias">
                    <div class="sellstat-title text-center"><?= Yii::t('app','Income');?></div>
                    <?= CurrencyHelper::format('RUB', $posSum) ?>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                <a href="#" class="stat-alias">
                    <div class="sellstat-title text-center"><?= Yii::t('app','Expenses');?></div>
                    <?= CurrencyHelper::format('RUB', $negSum) ?>
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                <a href="#" class="stat-alias">
                    <div class="sellstat-title text-center"><?= Yii::t('app','Current balance');?></div>
                    <?= $balance ?>
                </a>
            </div>
            <div class="inbox row margin-30">
                <div class="col-md-12">
                    <div class="all-messages allgigs-tabs hidden-xs">
                        <div class="over-table-viewgigs">
                            <div class="head-viewgigs row">
                                <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                    <a class="grey-but text-center del-gigs delete-messages" data-pjax="0">
                                        <?= mb_strtoupper(Yii::t('account', 'Delete')) ?>
                                    </a>
                                </div>
                            </div>
                            <table class="table-viewgigs">
                                <tr>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Date')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'For')) ?>
                                    </td>
                                    <td>
                                        <?= mb_strtoupper(Yii::t('account', 'Amount')) ?>
                                    </td>
                                </tr>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => 'listview/payment-item',
                                    'options' => [
                                        'tag' => false,
                                        'id' => 'messages-list'
                                    ],
                                    'itemOptions' => [
                                        'tag' => 'tr',
                                        'class' => 'messages-row'
                                    ],
                                    'emptyTextOptions' => [
                                        'tag' => 'tr',
                                        'class' => 'messages-row'
                                    ],
                                    'emptyText' => "<td colspan='3'>" . Yii::t('yii', 'No results found.') . "</td>",
                                    'layout' => "{items}"
                                ]) ?>
                            </table>
	                        <?= LinkPager::widget([
		                        'pagination' => $dataProvider->pagination,
	                        ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $(document).on("submit", "#withdrawal-form", function(e) {
        e.preventDefault();
        var action = $(this).attr("action");
        var data = $(this).serialize();

        $.ajax({
            url: action,
            data: data,
            type: "post",
            success: function(response) {
                if (response.success == true) {
                    $.pjax.reload({
                        container: "#payments-pjax"
                    });
                    alertCall("success", response.message);
                } else {
                    alertCall("warning", response.message);
                }
            }
        });
    });

    $(document).on("submit", "#replenish-form", function(e) {
        var amount = $("#amount-input").val();
        if (!amount.match(/^[0-9]+$/)){
            $("#amount-input").siblings(".help-block").html("' . Yii::t('app', 'Amount must be integer number') . '");
            $(this).addClass("has-error");
            return false;
        }
    });

    $(document).on("change", "#withdrawalrequestform-targetmethod", function() {
        var $selectedOption = $(this).find("option:selected");
        var value = $selectedOption.text();
        $("#withdrawalrequestform-targetaccount").val(value);
    });
');