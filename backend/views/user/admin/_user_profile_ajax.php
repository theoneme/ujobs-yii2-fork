<?php

use backend\assets\FormStylerAsset;
use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use common\helpers\FileInputHelper;
use common\models\ContentTranslation;
use common\models\user\Profile;
use common\models\user\User;
use common\models\WithdrawalWallet;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var User $user
 * @var Profile $profile
 * @var array $withdrawalTypesData
 * @var ActiveDataProvider $languageDataProvider
 */

MyCropAsset::register($this);
FormStylerAsset::register($this);
SelectizeAsset::register($this);
?>

    <p>
        Ссылка на профиль: <?= Html::a('Ссылка на профиль: ' . Yii::$app->urlManagerFrontEnd->createUrl([
                '/account/profile/show', 'id' => $user->id
            ]), Yii::$app->urlManagerFrontEnd->createUrl([
            '/account/profile/show', 'id' => $user->id
        ]), ['target' => '_blank'])?>
    </p>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/user/admin/update-ajax', 'id' => $user->id]),
    'layout' => 'horizontal',
//    'enableAjaxValidation' => true,
//    'enableClientValidation' => false,
    'options' => [
        'id' => 'public-profile-form',
        'enctype' => 'multipart/form-data',
        'data-pjax' => false,
        'class' => 'ajax-submit',
        'data-pj-container' => 'user-pjax'
    ],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

    <div class="row">
        <div class="col-md-10">
            <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
        </div>
        <div class="col-md-2">
            <button type="button" id="random-email" class="btn btn-primary btn-xs">Random</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($user, 'phone')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($profile, 'skype')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($user, 'password')->passwordInput() ?>
        </div>
    </div>

    <!--    --><?//= $form->field($profile, 'name') ?>
<?= $form->field($profile, 'public_email')->label(Yii::t('app', 'Public Email')) ?>
<?= $form->field($profile, 'status_text')->label(Yii::t('app', 'Status text')) ?>

    <div class="col-md-9 col-md-offset-3">
        <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
            'id' => 'file-upload-input',
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'uploadUrl' => Url::toRoute('/image/upload-profile'),
                'overwriteInitial' => true,
                'initialPreview' => !empty($profile->gravatar_email) ? $profile->getThumb() : [],
                'initialPreviewConfig' => !empty($profile->gravatar_email) ? [[
                    'caption' => basename($profile->gravatar_email),
                    'url' => Url::toRoute('/image/delete'),
                    'key' => $profile->user_id
                ]] : [],
                'otherActionButtons' =>
                    '<button type="button" class="btn btn-sm btn-default crop-button" data-ratio="1" title="' . Yii::t('app', 'Crop image') . '">
                        <i class="fa fa-crop" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-button" data-ratio="1" title="' . Yii::t('app', 'Rotate image') . '">
                        <i class="fa fa-rotate-left" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-button" data-ratio="1" title="' . Yii::t('app', 'Rotate image') . '">
                        <i class="fa fa-rotate-right" aria-hidden="true"></i>
                    </button>',
            ]
        ])) ?>
        <?= $form->field($profile, 'gravatar_email')->hiddenInput([
            'id' => 'gravatar-input',
            'value' => $profile->gravatar_email,
            'data-key' => $profile->user_id
        ])->label(false); ?>
    </div>

<?= $form->field($profile, 'status')->radioList(Profile::getStatusLabels()) ?>
    <!--    --><?//= $form->field($profile, 'bio')->textarea(['rows' => 12]) ?>
    <div class="translations-block col-md-9 col-md-offset-3">
        <?php
        $locales = count($profile->translations) ? array_map(function($var){ return $var->locale; }, $profile->translations) : ['ru-RU'];
        echo Tabs::widget([
            'items' => array_map(function ($locale, $language) use ($form, $profile, $locales) {
                $translation = $profile->translations[$locale] ?? new ContentTranslation();
                if (!count($profile->translations) && $locale === 'ru-RU') {
                    $translation->title = $profile->name;
                    $translation->content = $profile->bio;
                }
                return [
                    'label' => $language,
                    'content' => $this->render('_translation', [
                        'model' => $translation,
                        'form' => $form,
                        'locale' => $locale,
                    ]),
                    'active' => $locale === reset($locales),
                    'linkOptions' => ['class' => $translation->isNewRecord ? 'grey' : 'green']
                ];
            }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
        ]);
        ?>
    </div>

    <div class="form-group">
        <label style="text-align: right" class="col-sm-3">
            <?=Yii::t('app', 'User Languages')?>
        </label>
        <div class="col-sm-9">
            <?php
            foreach ($languages as $k => $v) {
                echo $this->render('_language', [
                    'index' => $k,
                    'model' => $v,
                ]);
            } ?>
            <div id="language-container"></div>
            <a href="#" class="add-language" id="load-language">
                <i class="fa fa-plus"></i> <?=Yii::t('app', 'Add Language')?>
            </a>
        </div>
    </div>

    <div class="form-group">
        <label style="text-align: right" class="col-sm-3">
            <?=Yii::t('app', 'User Skills')?>
        </label>
        <div class="col-sm-9">
            <?php
            foreach ($skills as $k => $v) {
                echo $this->render('_skill', [
                    'index' => $k,
                    'model' => $v,
                ]);
            } ?>
            <div id="skill-container"></div>
            <a href="#" class="add-skill" id="load-skill">
                <i class="fa fa-plus"></i> <?=Yii::t('app', 'Add Skill')?>
            </a>
        </div>
    </div>

    <div class="form-group">
        <label style="text-align: right" class="col-sm-3">
            <?=Yii::t('app', 'User Specialties')?>
        </label>
        <div class="col-sm-9">
            <?php
            foreach ($specialties as $k => $v) {
                echo $this->render('_specialty', [
                    'index' => $k,
                    'model' => $v,
                ]);
            } ?>
            <div id="specialty-container"></div>
            <a href="#" class="add-specialty" id="load-specialty">
                <i class="fa fa-plus"></i> Добавить специальность
            </a>
        </div>
    </div>

    <div class="form-group">
        <label style="text-align: right" class="col-sm-3">
            <?=Yii::t('app', 'User Education')?>
        </label>
        <div class="col-sm-9">
            <?php
            foreach ($profile->educations as $k => $v) {
                echo $this->render('_education', [
                    'index' => $k,
                    'model' => $v,
                ]);
            } ?>
            <div id="education-container"></div>
            <!--            <a href="#" class="add-education" id="load-education">-->
            <!--                <i class="fa fa-plus"></i> --><?//=Yii::t('app', 'Add Education')?>
            <!--            </a>-->
        </div>
    </div>

    <div class="form-group">
        <label style="text-align: right" class="col-sm-3">
            <?=Yii::t('app', 'User Certificates')?>
        </label>
        <div class="col-sm-9">
            <?php
            foreach ($profile->certificates as $k => $v) {
                echo $this->render('_certificate', [
                    'index' => $k,
                    'model' => $v,
                ]);
            } ?>
            <div id="certificate-container"></div>
            <!--            <a href="#" class="add-certificate" id="load-certificate">-->
            <!--                <i class="fa fa-plus"></i> --><?//=Yii::t('app', 'Add Certificate')?>
            <!--            </a>-->
        </div>
    </div>

    <div class="form-group">
        <label style="text-align: right" class="col-sm-3">
            <?= Yii::t('app', 'Social Networks')?>
        </label>
        <div class="col-sm-9">
            <?php
            $icons= [];
            foreach($user->accounts as $account) {
                $icons[] = Html::a('
                    <span class="soc-square">
                        <i class="fa fa-' . $account->provider . '" aria-hidden="true"></i>
                    </span>',
                    $account->getProfileLink(),
                    ['target' => '_blank', 'data-pjax' => 0]
                );
            }
            echo implode('&nbsp;', $icons);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <label style="text-align: right" class="col-sm-3 control-label">
            <?= Yii::t('app', 'Address')?>
        </label>
        <div class="col-sm-9">
            <div id="address-string">
                <?php $locationString = $profile->getFrom() ?>
                <span>
                    <?= (!empty($locationString)
                        ? $locationString
                        : Yii::t('account', 'Enter city')) . '&nbsp;' . Html::a('<i class="fa fa-pencil"></i>', null, ['id' => 'edit-city'])
                    ?>
                </span>
            </div>
            <div id="address-form" class="selectize-input-container city-selectize" style="display: none">
                <?= Html::activeHiddenInput($profile, 'lat', ['id' => 'profile-lat']) ?>
                <?= Html::activeHiddenInput($profile, 'long', ['id' => 'profile-long']) ?>
                <?= Html::activeHiddenInput($profile, '[locationData]country', ['id' => 'profile-location-country', 'value' => false]) ?>
                <?= Html::activeHiddenInput($profile, '[locationData]city', ['id' => 'profile-location-city']) ?>
                <?= Html::textInput('location_input', null,
                    [
                        'id' => 'profile-location-input',
                        'placeholder' => Yii::t('app', 'Enter your city and choose it from dropdown'),
                        'class' => 'form-control'
                    ]
                ); ?>
            </div>
        </div>
    </div>

    <div class="withdrawal-wallets">
        <!--        --><?//= Html::label('Кошельки для вывода средств', null, ['class' => 'col-sm-offset-3 col-sm-9'])?>
        <?php foreach ($withdrawalTypesData['preferredTypes'] as $type => $label) {
            $wallet = $profile->withdrawalWallets[$type] ?? new WithdrawalWallet(['type' => $type]);
            echo "<h3 class='col-sm-offset-1'>$label</h3>";
            echo Html::activeHiddenInput($wallet, "[$type]id");
            echo Html::activeHiddenInput($wallet, "[$type]type");
            echo $form->field($wallet, "[$type]number");
            foreach ($wallet->getCustomDataFields() as $field) {
                echo $form->field($wallet, "[$type][customDataArray]$field")
                    ->textInput(['value' => $wallet->customDataArray[$field] ?? ''])
                    ->label($wallet->getAttributeLabel($field));
            }
        } ?>
        <div class="more-container">
            <a class="show-more col-sm-offset-3 col-sm-9">Показать другие</a>
            <div class="more">
                <?php foreach ($withdrawalTypesData['otherTypes'] as $type => $label) {
                    $wallet = $profile->withdrawalWallets[$type] ?? new WithdrawalWallet(['type' => $type]);
                    echo "<h3 class='col-sm-offset-1'>$label</h3>";
                    echo Html::activeHiddenInput($wallet, "[$type]id");
                    echo Html::activeHiddenInput($wallet, "[$type]type");
                    echo $form->field($wallet, "[$type]number");
                    foreach ($wallet->getCustomDataFields() as $field) {
                        echo $form->field($wallet, "[$type][customDataArray]$field")
                            ->textInput(['value' => $wallet->customDataArray[$field] ?? ''])
                            ->label($wallet->getAttributeLabel($field));
                    }
                } ?>
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-9">
            <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php $this->registerJsFile('@web/js/jquery.geocomplete.min.js', ['depends' => 'yii\web\JqueryAsset']); ?>

<?php
$languageUrl = Url::toRoute('/user/admin/render-public-language');
$skillUrl = Url::toRoute('/user/admin/render-public-skill');
$specialtyUrl = Url::toRoute('/user/admin/render-public-specialty');
$portfolioUrl = Url::toRoute('/user/admin/render-public-portfolio');
$emailUrl = Url::toRoute('/user/admin/random-email');
$specialtyListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'specialty']);
$languagesCount = count($profile->languages);
$portfolioCount = count($profile->portfolio);
$specialtiesCount = count($profile->specialties);
$skillsCount = count($profile->skills);

$apiKey = Yii::$app->params['googleAPIKey'];
$language = Yii::$app->params['supportedLocales'][Yii::$app->language];
if ($language === 'ua') {
    $language = 'uk';
}
$script = <<<JS
    $.pjax.defaults.timeout = 6000;

    var languages = {$languagesCount};
    var skills = {$skillsCount};
    var specialties = {$specialtiesCount};
    var portfolio = {$portfolioCount};
    
    $(document).off('click', '#load-language');
    $(document).on('click', '#load-language', function(e) {
        e.preventDefault();
        $.get('$languageUrl',
        {iterator: languages},
        function(data) {
            if(data.success === true) {
                languages++;
                $('#language-container').append(data.html);
            } else {
                alertCall('error', data.message);
            }
        }, 'json');
    });

    $(document).off('click', '#load-skill');
    $(document).on('click', '#load-skill', function(e) {
        e.preventDefault();
        $.get('$skillUrl',
        {iterator: skills},
        function(data) {
            if(data.success === true) {
                skills++;
                $('#skill-container').append(data.html);
            } else {
                alertCall('error', data.message);
            }
        }, 'json');
    });

    $(document).off('click', '#load-specialty');
    $(document).on('click', '#load-specialty', function(e) {
        e.preventDefault();
        $.get('$specialtyUrl',
        {iterator: specialties},
        function(data) {
            if(data.success === true) {
                specialties++;
                $('#specialty-container').append(data.html);
            } else {
                alertCall('error', data.message);
            }
        }, 'json');
    });

    $(document).off('click', '#load-portfolio');
    $(document).on('click', '#load-portfolio', function(e) {
        e.preventDefault();
        $.get('$portfolioUrl',
        {iterator: portfolio},
        function(data) {
            if(data.success === true) {
                portfolio++;
                $('#portfolio-container').append(data.html);
            } else {
                alertCall('error', data.message);
            }
        }, 'json');
    });

    $(document).off('click', '.del-lang');
    $(document).on('click', '.del-lang', function() {
        $(this).parent().remove();
    });

    $(document).off('click', '.del-skill');
    $(document).on('click', '.del-skill', function() {
        $(this).parent().remove();
    });

    $(document).off('click', '.del-specialty');
    $(document).on('click', '.del-specialty', function() {
        $(this).parent().remove();
    });

    $(document).off('click', '.del-portfolio');
    $(document).on('click', '.del-portfolio', function() {
        $(this).parent().remove();
    });
    
    if (typeof (googleAutocomplete) === 'undefined') {
        $.getScript('https://maps.googleapis.com/maps/api/js?key={$apiKey}&libraries=places&language={$language}', function() {
            initAutocomplete();
        });
    } else {
        initAutocomplete();
    }  
    function initAutocomplete() {
        googleAutocomplete = new google.maps.places.Autocomplete(
            $("#profile-location-input").get(0),
            {types: ['(cities)']}
        );
        googleAutocomplete.addListener('place_changed', function() {
            let place = googleAutocomplete.getPlace();
            let country = '';
            let city = '';
            if (typeof place.address_components !== "undefined") {
                for (let i = 0; i < place.address_components.length; i++) {
                    let addressType = place.address_components[i].types[0];
                    switch (addressType) {
                        case 'locality': 
                            city = place.address_components[i].long_name;
                            break;
                        case 'country': 
                            country = place.address_components[i].long_name;
                            break;
                    }
                }
            }
            $('#profile-lat').val(place.geometry.location.lat());
            $('#profile-long').val(place.geometry.location.lng());
            $('#profile-location-city').val(city);
            $('#profile-location-country').val(country);
        });
    }
    
    $('#edit-city').on('click', function() {
        $('#address-form').toggle();
        return false;
    });

    let hasFileUploadError = false;
    $('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
        let response = data.response;
        $('#gravatar-input').val(response.uploadedPath).data('key', response.imageKey);
    }).on('filedeleted', function(event, key) {
        $('#gravatar-input').val(null);
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $('#submit-button').trigger('click');
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });

    $('#submit-button').on('click', function(e) {
        if ($('#file-upload-input').fileinput('getFilesCount') > 0) {
            $('#file-upload-input').fileinput('upload');
            return false;
        }
        return true;
    });

    $("#public-profile-form").on("beforeValidateAttribute", function (event, attribute, messages) {
        if (attribute.id === 'user-phone') {
            $('#' + attribute.id).val($('#' + attribute.id).val().replace(/[^0-9]/g, ''));
        }
        if ($(attribute.container).closest('.translations-block').length || $(attribute.container).closest('.withdrawal-wallets').length) {
            return false;
        }
    });
    $('#random-email').click(function(){
        $.get('$emailUrl', {}, function(data){
            $('#user-email').val(data.email);
        }, 'json');
    });
    $('.specialty-input-container select:not(.selectized)').selectize({
        valueField: 'id',
        labelField: 'value',
        searchField: ['value'],
        'load': function(query, callback) {
            if (!query.length)
                return callback();
            $.post('$specialtyListUrl', {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script);