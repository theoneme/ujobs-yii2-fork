<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\User $user
 */
?>
    <div class="row">
        <div class="col-md-10">
            <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
        </div>
        <div class="col-md-2">
            <button type="button" id="random-email" class="btn btn-primary btn-xs">Random</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($user, 'password')->passwordInput() ?>
        </div>
    </div>
<?
$this->registerJs("
		$('#random-email').click(function(){
			$.get('" . yii\helpers\Url::toRoute('/user/admin/random-email') . "', {}, function(data){
				$('#user-email').val(data.email);
			}, 'json');
		});
		",
    yii\web\View::POS_END,
    'random-email');