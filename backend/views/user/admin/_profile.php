<?php

use common\helpers\FileInputHelper;
use common\models\user\Profile;
use common\models\user\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\helpers\Url;

\backend\assets\MyCropAsset::register($this);

/**
 * @var yii\web\View $this
 * @var User $user
 * @var Profile $profile
 */

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<?= $form->field($profile, 'name') ?>
<?= $form->field($profile, 'public_email') ?>

    <div class="col-md-9 col-md-offset-3">
        <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
            'id' => 'file-upload-input-extra-' . $widgetId,
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'overwriteInitial' => true,
                'initialPreview' => !empty($profile->gravatar_email) ? $profile->getThumb() : [],
                'initialPreviewConfig' => !empty($profile->gravatar_email) ? [[
                    'caption' => basename($profile->getThumb()),
                    'url' => Url::toRoute('/image/delete'),
                    'key' => $profile->user_id
                ]] : [],
            ]
        ])) ?>
        <?= $form->field($profile, 'gravatar_email')->hiddenInput([
            'id' => 'gravatar-input',
            'value' => $profile->getThumb(),
            'data-key' => $profile->user_id
        ])->label(false); ?>
    </div>
<?= $form->field($profile, 'status')->radioList(Profile::getStatusLabels()) ?>

<?= $form->field($profile, 'bio')->textarea(['rows' => 10]) ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-9">
            <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>

<?php $this->registerJs("
    $('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        $('#gravatar-input').val(response.uploadedPath).data('key', response.imageKey);
    });

    $('#file-upload-input').on('filedeleted', function(event, key) {
        $('#gravatar-input').val(null);
    });
    $('#submit-button').on('click', function(e) {
        if ($('#file-upload-input').fileinput('getFilesCount') > 0) {
            $('#file-upload-input').fileinput('upload');
            return false;
        }
        else {
            return true;
        }
    });
    $('#file-upload-input').on('filebatchuploadcomplete', function(event, files, extra) {
        $(this).closest('form').submit();
    });
    ",
    yii\web\View::POS_READY,
    'autocomplete');