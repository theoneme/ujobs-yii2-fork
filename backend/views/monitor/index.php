<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.02.2018
 * Time: 17:52
 */

use yii\helpers\Html;
use yii\helpers\Url;

/** @var array $servers */

$this->title = Yii::t('app', 'Monitor');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Server name</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($servers as $key => $server) { ?>
                    <tr>
                        <th scope="row"><?= $key ?></th>
                        <td data-label="replica" data-server="<?= $key?>"></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $jsonServers = json_encode($servers);
$url = Url::to(['/monitor/check-state']);
$script = <<<JS
    function checkStatus(server) {
        $.ajax({
            url: '$url',
            type: 'get',
            data: {server: server},
    
            success: function (result) {
                if (result.success === true) {
                    let html = '';
                    $.each(result.data, function(key, value) {
                        let tempValue = '';
                        if(value === 'Yes') {
                            tempValue = '<div class="btn btn-success btn-xs">Yes</div>';
                        } else if(value === 'No') {
                            tempValue = '<div class="btn btn-danger btn-xs">No</div>';
                        } else {
                            tempValue = value;
                        }
                        
                        html += key + ': ' + tempValue + '<br>';
                    });

                    $('td[data-label=replica][data-server=' + server + ']').html(html);
                }
            }
        });
    }
    
    $.each($jsonServers, function(key) {
        checkStatus(key);
    });
JS;
$this->registerJs($script);

