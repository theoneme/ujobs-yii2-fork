<?php

use yii\helpers\Html;
use kartik\tree\TreeView;
use common\models\Category;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление иерархией категорий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?= TreeView::widget([
                'query' => Category::find()->addOrderBy('root, lft'),
                'headingOptions' => ['label' => Yii::t('app', 'Categories')],
                'fontAwesome' => true,
                'isAdmin' => true,
                'displayValue' => 1,
                'softDelete' => false,
                'cacheSettings' => [
                    'enableCache' => false
                ],
                'nodeAddlViews' => [
                    2 => '@backend/views/category/_form_add'
                ],
                'nodeFormOptions' => [
                    'enctype' => 'multipart/form-data'
                ],
            ]);
            ?>
        </div>
    </div>
</div>

