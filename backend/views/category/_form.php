<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
			 <?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'per_page')->textInput() ?>

				<?= $form->field($model, 'sort_field')->textInput() ?>

				<?= $form->field($model, 'sort_order')->textInput() ?>

				<?= $form->field($model, 'layout')->textInput() ?>

				<?= $form->field($model, 'template')->textInput(['maxlength' => true]) ?>
				
				<?= $form->field($model, 'alias')->textInput() ?>

				<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>

				<?php ActiveForm::end(); ?>
        </div>
        <!-- /.box-body -->

    </div>

   

</div>
