<?php

use common\modules\attribute\models\AttributeGroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\bootstrap\Tabs;
use common\models\Language;
use common\models\ContentTranslation;

/* @var $this yii\web\View */
/* @var $node common\models\Category */
/* @var $form yii\widgets\ActiveForm */

if ($node->translations) {
    $node->translationsArr = $node->translations;
} else {
    foreach (Yii::$app->params['languages'] as $locale => $language) {
        $content = new ContentTranslation;
        $content->locale = $locale;
        $node->translationsArr[] = $content;
    }
}
?>

    <div class="category-form">
        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>

        <div class="box box-danger">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="col-xs-12">
                    <?php
                    $tabs = [];
                    foreach ($node->translationsArr as $key => $item) {
                        if (isset(Yii::$app->params['languages'][$item->locale])) {
                            $tabItem = [
                                'label' => Yii::$app->params['languages'][$item->locale],
                                'content' => $this->render('_pagetab', ['model' => $item, 'form' => $form, 'locale' => $item->locale])
                            ];
                            if (Yii::$app->language == $item->locale) {
                                $tabItem['active'] = true;
                            }
                            $tabs[] = $tabItem;
                        }
                    }
                    ?>

                    <?= Tabs::widget([
                        'items' => $tabs
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <div class="box box-danger">
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                <!--<div class="col-xs-6">
                    <?= $form->field($node, 'per_page')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($node, 'sort_field')->dropDownList(
                    ['title' => Yii::t('app', 'Title'), 'publish_date' => Yii::t('app', 'Publish date'), 'all_views' => Yii::t('app', 'All views'), 'lm_views' => Yii::t('app', 'Last month views'), 'm_views' => Yii::t('app', 'This month views')],
                    ['options' =>
                        [
                            'title' => ['selected' => true]
                        ]
                    ]
                );
                ?>
                </div>-->
                <div class="col-xs-6">
                    <?= $form->field($node, 'sort_order')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($node, 'alias')->textInput() ?>
                    <p class="text-green mt-15"><?= Yii::t('app', 'Category URL address. Will be generated automatically if left empty') ?></p>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($node, 'type')->dropDownList(
                        [
                            'category' => Yii::t('app', 'Category'),
                            'job_category' => Yii::t('app', 'Job category'),
                            'service_category' => Yii::t('app', 'Catalog'),
                            'product_category' => Yii::t('app', 'Product category')
                        ],
                        [
                            'options' => [
                                'category' => ['selected' => true]
                            ]
                        ]
                    );
                    ?>
                </div>
                <div class="col-xs-6">
                    <div class="form-group field-category-type">
                        <?= $form->field($node, 'attribute_set_id')->dropDownList(ArrayHelper::map(AttributeGroup::find()->all(), 'id', 'title'), ['prompt' => '-- Выберите группу атрибутов']) ?>
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <?= $form->field($node, 'layout')->textInput() ?>
                    <p class="text-green mt-15"><?= Yii::t('app', 'Main layout of html page. Defaults to main') ?></p>
                </div>
                <div class="col-xs-12">
                    <?= $form->field($node, 'template')->textInput(['maxlength' => true]) ?>
                    <p class="text-green mt-15"><?= Yii::t('app', 'Category content template. Defaults to category') ?></p>
                </div>
                <div class="col-xs-12">
                    <?= $form->field($node, 'pages_template')->textInput(['maxlength' => true]) ?>
                    <p class="text-green mt-15"><?= Yii::t('app', 'Inner category pages template. Defaults to page') ?></p>
                </div>
                <div class="col-xs-12">
                    <?= $form->field($node, 'image')->widget(FileInput::class, [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => [
                                'jpg', 'gif', 'png', 'jpeg'
                            ],
                            'initialPreview' => [
                                !empty($node->image) ? Html::img($node->image, ['class' => 'file-preview-image', 'alt' => '', 'title' => '']) : null,
                            ],
                        ]
                    ]); ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

<?
$this->registerJs("
    $(function () {
        var switcherEl = $('input[type=\'checkbox\']').switcher({language: 'ru'});
    });
    $('#menu-entity').trigger('change');
    ",
    yii\web\View::POS_END,
    'switcher');