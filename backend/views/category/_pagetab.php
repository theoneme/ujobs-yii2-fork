<?php

use vova07\imperavi\Widget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $locale string */
/* @var $form ActiveForm */

?>
<div class="asdf">
	<?= $form->field($model, "[$locale]title") ?>
	<?= $form->field($model, "[$locale]content_prev") ?>
	<?= $form->field($model, "[$locale]content")->widget(Widget::class, [
		'settings' => [
            'lang' => 'ru',
			'minHeight' => 200,
			'plugins' => [
				'fullscreen',
				'fontsize',
				'fontcolor'
			],
			'buttonsHide' => ['link', 'html', 'formatting']
		]
	]);
	?>
	<?= $form->field($model, "[$locale]seo_title") ?>
	<?= $form->field($model, "[$locale]seo_description") ?>
	<?= $form->field($model, "[$locale]seo_keywords") ?>
    <?= $form->field($model, "[$locale]slug") ?>
	<?= $form->field($model, "[$locale]locale", ["options" => ["value" => $locale]])->hiddenInput()->label(false); ?>
	<?= $form->field($model, "[$locale]entity_id")->hiddenInput()->label(false); ?>
</div><!-- asdf -->