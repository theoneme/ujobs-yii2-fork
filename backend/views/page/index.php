<?php

use common\models\Page;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Category;
use kartik\tree\TreeViewInput;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $currentCategory integer */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('app', 'Create Page'), ['create'], [
					'class' => 'btn btn-success modal-edit',
					'title' => Yii::t('app', 'Update {0}', 'Job')
				]) ?>
				<?= Html::a(Yii::t('app', 'Delete selected'), '#', ['class' => 'btn btn-danger', 'id' => 'd-multiple', 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete these items?'),]) ?>
            </p>
			
			<div class="col-md-3">
				<hr>
				<?= TreeViewInput::widget([
					'query' => Category::find()->addOrderBy('root, lft')->where(['type' => $searchModel->type === Page::TYPE_DEFAULT ? 'category' : 'job_category']),
					'headingOptions'=>['label'=>Yii::t('app', 'Categories')],
					'name' => 'kv-product', // input name
					'value' => $currentCategory,     // values selected (comma separated for multiple select)
					'asDropdown' => false,   // will render the tree input widget as a dropdown.
					'multiple' => false,     // set to false if you do not need multiple selection
					'fontAwesome' => true,  // render font awesome icons
					'rootOptions' => [
						'label'=>'<i class="fa fa-tree"></i>',  // custom root label
						'class'=>'text-success'
					], 
					'options'=>['id' => 'treaa'],
				]);?>
			</div>
			
			<div class="col-md-9">
				<?php \yii\widgets\Pjax::begin(['id' => 'grid-container', 'timeout' => 5000]); ?>
				
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
                    'id' => 'user-grid',
					'columns' => [
						[
                            'class' => 'yii\grid\CheckboxColumn',
                            'contentOptions' => ['style' => 'width: 30px'],
                            'options' => ['style' => 'width: 30px']
                        ],
						[
							'attribute' => 'title',
                            'value' => function ($model) {
                                /* @var $model Page*/
                                $translation = $model->translations[Yii::$app->language] ?? array_values($model->translations)[0] ?? null;
                                return $translation->title ?? null;
                            },
						],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /* @var $model Page*/
                                return $model->getStatusLabel(true);
                            },
                            'filter' => Page::getStatusLabels()
                        ],
                        'publish_date:datetime',
                        [
                            'header' => 'Автор',
                            'attribute' => 'username',
                            'value' => function ($model) {
                                /* @var $model Page*/
                                return $model->profile ? Html::a(Html::img($model->getSellerThumb('catalog'), ['style' => 'width: 25px']) . "&nbsp;" . $model->getSellerName(),
                                    ['/user/admin/update-ajax', 'id' => $model->user_id],
                                    [
                                        'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]
                                ) : '';
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'alias',
                            'value' => 'alias',
                            'contentOptions' => ['style' => 'width: 150px'],
                            'options' => ['style' => 'width: 150px']
                        ],
						['class' => 'yii\grid\ActionColumn',
                            'template' => Yii::$app->user->identity->isAdmin() ? '{update} {delete}' : '{update}',
							'buttons' => [
								'update' => function ($url, $model) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
										'class' => 'modal-edit'
									]);
								},
//								'delete' => function ($url, $model, $key) {
//									return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
//										'title' => Yii::t('yii', 'Delete'),
//										'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//										'data-pjax' => 1
//									]);
//								},
							],
                            'contentOptions' => ['style' => 'width: 50px'],
                            'options' => ['style' => 'width: 50px']
						],
					],
				]); ?>

				<?php \yii\widgets\Pjax::end(); ?>
			</div>

        </div>
        <!-- /.box-body -->

    </div>
</div>

<?
$urlAppendSign = (Yii::$app->urlManager->enablePrettyUrl === true) ? "?" : "&";
$currentUrl = Url::current();
$this->registerJs("
	$(document).on('change', '#treaa', function() {
		url = '" . $currentUrl . "';
		if ($(this).val()) {
			url = url + '" . $urlAppendSign . "PageSearch[category_id]=' + $(this).val();
		}
		$.pjax.reload({container: '#grid-container',url: url, timeout: 2000});
	});

	$(document).ready(function(){
		$('#d-multiple').click(function(){
			var ids = $('#w0').yiiGridView('getSelectedRows');
			  $.ajax({
				type: 'POST',
				url : '" . Url::to('/backend/page/delete-multiple', true) . "',
				data : {pk: ids},
				success : function() {
					$.each(ids, function(index, value){
						var el = $('#w0').find('[data-key=' + value + ']');
						console.log(el);
						el.remove();
					})
				}
			});

		});
	});
");
