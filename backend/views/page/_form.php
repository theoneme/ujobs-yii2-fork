<?php

use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use common\models\ContentTranslation;
use common\models\Page;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use common\models\Category;
use kartik\tree\TreeViewInput;

use kartik\form\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Page */
/* @var $form yii\widgets\ActiveForm */
/* @var $isAjax bool*/
MyCropAsset::register($this);
SelectizeAsset::register($this);
?>

<div class="page-form">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'options'=>[
					'enctype'=>'multipart/form-data', // important
                    'class' => ($isAjax ? 'ajax-submit' : ''),
                    'id' => 'page-update'
				]
            ]); ?>

			<div class="col-xs-12">
				<label class="control-label" for="page-category-id"><?=Yii::t('app', 'Select category')?></label>
				<?= TreeViewInput::widget([
						// single query fetch to render the tree
						// use the Product model you have in the previous step
						'query' => Category::find()->addOrderBy('root, lft')->where(['type' => 'category']),
						'headingOptions'=>['label'=>Yii::t('app','Categories')],
						'name' => 'Page[category_id]', // input name
						'value' => $model->category_id,     // values selected (comma separated for multiple select)
						'asDropdown' => true,   // will render the tree input widget as a dropdown.
						'multiple' => false,     // set to false if you do not need multiple selection
						'fontAwesome' => true,  // render font awesome icons
						'rootOptions' => [
							'label'=>'<i class="fa fa-tree"></i>',  // custom root label
							'class'=>'text-success'
						],
						'options'=>['id' => 'page-category'],
					]);
				?>
				<p class="text-green"><?=Yii::t('app', 'Leave empty if page should not be in any category')?></p>
			</div>
            <div class="col-xs-12">
                <?= Html::label('Теги', 'tags', ['class' => 'control-label'])?>
                <?= Html::activeTextInput($model, 'tagsString', ['id' => 'tags', 'class' => 'readsym']) ?>
            </div>

            <?= Tabs::widget([
                'items' => array_map(function ($locale, $language) use ($form, $model) {
                    return [
                        'label' => $language,
                        'content' => $this->render('_pagetab', [
                            'model' => $model->translations[$locale] ?? new ContentTranslation(),
                            'form' => $form,
                            'locale' => $locale,
                            'type' => Page::TYPE_DEFAULT
                        ]),
                        'active' => $locale === 'ru-RU'
                    ];
                }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
            ]);
            ?>

			<div class="col-xs-6">
				<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
				<p class="text-green mt-15"><?=Yii::t('app', 'Category URL address. Will be generated automatically if left empty')?></p>
			</div>

			<div class="col-xs-12">
				<?= $form->field($model, 'layout')->textInput() ?>
				<p class="text-green mt-15"><?=Yii::t('app', 'Main layout of html page. Defaults to main')?></p>
			</div>
			<div class="col-xs-12">
				<?= $form->field($model, 'template')->textInput(['maxlength' => true]) ?>
				<p class="text-green mt-15"><?=Yii::t('app', 'Page content template. Defaults to page')?></p>
			</div>

            <div class="row">
                <div class="col-xs-4">
                    <?= $form->field($model, 'all_views')->textInput(['type' => 'number', 'min' => 0]) ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'lm_views')->textInput(['type' => 'number', 'min' => 0]) ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'm_views')->textInput(['type' => 'number', 'min' => 0]) ?>
                </div>
            </div>

            <?= Tabs::widget([
                'items' => [
                    [
                        'label' => Yii::t('model', 'Image'),
                        'content' => $this->render('_file_input_block', ['form' => $form, 'model' => $model, 'imageField' => 'image'])
                    ],
                    [
                        'label' => Yii::t('model', 'Banner'),
                        'content' => $this->render('_file_input_block', ['form' => $form, 'model' => $model, 'imageField' => 'banner'])
                    ],
                    [
                        'label' => Yii::t('model', 'Banner mobile'),
                        'content' => $this->render('_file_input_block', ['form' => $form, 'model' => $model, 'imageField' => 'banner_small'])
                    ],
                ]
            ]);
            ?>
            <div class="clear"></div>
            <div class="row">
                <div class="col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                        'id' => 'submit-button',
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                    ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box-body -->

    </div>
</div>

<?php
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'page_tag']);
$script = <<<JS
    $("#submit-button").on("click", function(e) {
        let returnValue = true;
        $(".file-upload-input").each(function(index){
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                returnValue = false;
                return false;
            }
        });
        if (!returnValue) {
            return false;
        }
    });

    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        let field = imagesContainer.attr('data-field');
        imagesContainer.find("input").remove();
        imagesContainer.append("<input class='image-source' name='Page[" + field + "]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key='" + key + "']").val('');
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) { 
            $("#submit-button").trigger('click');
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) { 
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    $("#tags").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class='create'>Добавить тег&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$tagListUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                    callback();
                }
            );
        }
    });
JS;

$this->registerJs($script);