<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $formView string */
/* @var $locales array */
/* @var $isAjax boolean */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Page',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
#$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="page-update">

    <?= $this->render($formView, [
        'model' => $model,
		'locales' => $locales,
        'isAjax' => $isAjax,
    ]) ?>

</div>
