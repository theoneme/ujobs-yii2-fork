<?php

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $model ActiveRecord
 * @var $form ActiveForm
 * @var $imageField string
 */

$preview = null;
if(!empty($model->{$imageField})) {
    if(method_exists($model, 'getThumb')) {
        $preview = $model->getThumb();
    } else {
        $preview = $model->{$imageField};
    }
} else {
    $preview = [];
}

?>
<div class='row'>
    <div class='col-xs-12 file-input-container'>
        <?= FileInput::widget(
            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                'id' => 'file-upload-input-' . $imageField,
                'pluginOptions' => [
                    'overwriteInitial' => true,
                    'initialPreview' => $preview,
                    'initialPreviewConfig' => !empty($model->{$imageField}) ? [[
                        'caption' => basename($model->{$imageField}),
                        'url' => Url::toRoute('/image/delete'),
                        'key' => 'image_init_' . $model->id
                    ]] : [],
                ]
            ])
        )?>
        <div class="images-container" data-field="<?= $imageField?>">
            <?= $form->field($model, $imageField)->hiddenInput([
                'value' => $model->{$imageField},
                'data-key' => 'image_init_' . $model->id
            ])->label(false);
            ?>
        </div>
    </div>
</div>