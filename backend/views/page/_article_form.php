<?php

use backend\assets\MyCropAsset;
use backend\assets\SelectizeAsset;
use common\models\ContentTranslation;
use common\models\Page;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use common\models\Category;
use kartik\tree\TreeViewInput;

use kartik\form\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Page */
/* @var $form yii\widgets\ActiveForm */
/* @var $isAjax bool*/
MyCropAsset::register($this);
SelectizeAsset::register($this);
?>

<div class="page-form">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'options'=>[
					'enctype'=>'multipart/form-data', // important
                    'class' => ($isAjax ? 'ajax-submit' : ''),
                    'id' => 'page-form'
				]
            ]); ?>

			<div class="col-xs-12">
				<label class="control-label" for="page-category-id"><?=Yii::t('app', 'Select category')?></label>
				<?= TreeViewInput::widget([
						// single query fetch to render the tree
						// use the Product model you have in the previous step
						'query' => Category::find()->addOrderBy('root, lft')->where(['type' => 'job_category']),
						'headingOptions'=>['label'=>Yii::t('app','Categories')],
						'name' => 'Page[category_id]', // input name
						'value' => $model->category_id,     // values selected (comma separated for multiple select)
						'asDropdown' => true,   // will render the tree input widget as a dropdown.
						'multiple' => false,     // set to false if you do not need multiple selection
						'fontAwesome' => true,  // render font awesome icons
						'rootOptions' => [
							'label'=>'<i class="fa fa-tree"></i>',  // custom root label
							'class'=>'text-success'
						],
						'options'=>['id' => 'page-category'],
					]);
				?>
				<p class="text-green"><?=Yii::t('app', 'Leave empty if page should not be in any category')?></p>
			</div>

            <div class="col-xs-12">
                <?= Html::label('Теги', 'tags', ['class' => 'control-label'])?>
                <?= Html::activeTextInput($model, 'tagsString', ['id' => 'tags', 'class' => 'readsym']) ?>
            </div>

            <div class="translations-block">
                <?php
                $locales = array_map(function($var){ return $var->locale; }, $model->translations);
                echo Tabs::widget([
                    'items' => array_map(function ($locale, $language) use ($form, $model, $locales) {
                        return [
                            'label' => $language,
                            'content' => $this->render('_pagetab', [
                                'model' => $model->translations[$locale] ?? new ContentTranslation(),
                                'form' => $form,
                                'locale' => $locale,
                                'type' => Page::TYPE_ARTICLE
                            ]),
                            'active' => $locale === reset($locales)
                        ];
                    }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
                ]);
                ?>
            </div>

            <div class="col-xs-12">
                <?= Html::label('Изображение', null, ['class' => 'control-label'])?>
                <?= $this->render('_file_input_block', ['form' => $form, 'model' => $model, 'imageField' => 'image'])?>
            </div>
            <div class="col-xs-12">
                <?= $form->field($model, 'status')->radioList(Page::getStatusLabels()) ?>
            </div>
            <div class="clear"></div>
            <div class="row">
                <div class="col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                        'id' => 'submit-button',
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                    ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box-body -->

    </div>
</div>

<?php
$tagListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'page_tag']);
$script = <<<JS
    $("#submit-button").on("click", function(e) {
        let returnValue = true;
        $(".file-upload-input").each(function(index){
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                returnValue = false;
                return false;
            }
        });
        if (!returnValue) {
            return false;
        }
    });
    $("#page-form").on("beforeValidateAttribute", function (event, attribute, messages) {
        if ($(attribute.container).closest('.translations-block').length) {
            return false;
        }
    });
        
    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        let field = imagesContainer.attr('data-field');
        imagesContainer.find("input").remove();
        imagesContainer.append("<input class='image-source' name='Page[" + field + "]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key='" + key + "']").val('');
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) { 
            $("#submit-button").trigger('click');
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) { 
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    $("#tags").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class='create'>Добавить тег&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$tagListUrl", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                    callback();
                }
            );
        }
    });
JS;

$this->registerJs($script);