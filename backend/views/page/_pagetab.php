<?php

use common\models\Page;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $form ActiveForm */
/* @var $locale string*/
/* @var $type string*/
?>
<div class="asdf">
    <?= $form->field($model, "[$locale]id")->hiddenInput()->label(false) ?>
    <?= $form->field($model, "[$locale]title") ?>
    <?= $type === Page::TYPE_DEFAULT ? $form->field($model, "[$locale]content_prev")->textarea(['rows' => 6]) : ''?>
    <?= $form->field($model, "[$locale]content")->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['/page/file-upload']),
            'imageManagerJson' => Url::to(['/page/images-get']),
            'buttonSource' => true,
            'plugins' => [
                'fullscreen',
                'fontsize',
                'fontcolor',
                'imagemanager',
            ]
        ]
    ])->label(false);
    ?>
    <?= $form->field($model, "[$locale]locale")->hiddenInput(["value" => $locale])->label(false);?>
    <?= $form->field($model, "[$locale]entity_id")->hiddenInput()->label(false);?>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, "[$locale]seo_description")->textArea() ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, "[$locale]seo_title")->textArea() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, "[$locale]seo_keywords") ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, "[$locale]seo_robots") ?>
        </div>
    </div>
</div>