<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Category;
use kartik\tree\TreeViewInput;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Skill pages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <p>
				<?= Html::a(Yii::t('app', 'Delete selected'), '#', ['class' => 'btn btn-danger', 'id' => 'd-multiple', 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete these items?'),]) ?>
            </p>
			<div class="col-md-12">
				<?php \yii\widgets\Pjax::begin(['id' => 'grid-container', 'timeout' => 5000]); ?>
				
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
                    'id' => 'user-grid',
					'columns' => [
						[
                            'class' => 'yii\grid\CheckboxColumn',
                            'contentOptions' => ['style' => 'width: 30px'],
                            'options' => ['style' => 'width: 30px']
                        ],
						[
							'attribute' => 'title',
							'value' => 'translation.title'
						],
                        [
                            'attribute' => 'relation_alias',
                            'contentOptions' => ['style' => 'width: 150px'],
                            'options' => ['style' => 'width: 150px']
                        ],
						['class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
							'buttons' => [
								'update' => function ($url, $model) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
										'class' => 'modal-edit'
									]);
								},
								'delete' => function ($url, $model, $key) {
									return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
										'title' => Yii::t('yii', 'Delete'),
										'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
										'data-pjax' => 1
									]);
								},
							],
							'urlCreator' => function ($action, $model, $key, $index) {
								switch ($action) {
									case 'update':
										$url = Url::toRoute(["/page/{$action}", 'id' => $model->id, 'form' => '_skill_form']);
										break;
									default:
										$url = Url::toRoute(["/page/{$action}", 'id' => $model->id]);
										break;
								}
								return $url;
							},
                            'contentOptions' => ['style' => 'width: 50px'],
                            'options' => ['style' => 'width: 50px']
						],
					],
				]); ?>

				<?php \yii\widgets\Pjax::end(); ?>
			</div>

        </div>
        <!-- /.box-body -->

    </div>
</div>

<?
$this->registerJs("
	$(document).ready(function(){
		$('#d-multiple').click(function(){
			var ids = $('#w0').yiiGridView('getSelectedRows');
			  $.ajax({
				type: 'POST',
				url : '" . Url::to('/backend/page/delete-multiple', true) . "',
				data : {pk: ids},
				success : function() {
					$.each(ids, function(index, value){
						var el = $('#w0').find('[data-key=' + value + ']');
						console.log(el);
						el.remove();
					})
				}
			});

		});
	});
");
