<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Asset for job and tender editing
 */
class JobAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/select2-krajee.css',
        'css/select2.css',
    ];
    public $js = [
        'js/job.js',
    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];
}
