<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class FormStylerAsset
 * @package backend\assets
 */
class FormStylerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery.formstyler.css',
    ];
    public $js = [
        'js/jquery.formstyler.js',
    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];
}