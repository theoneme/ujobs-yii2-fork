<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class MyCropAsset
 * @package backend\assets
 */
class MyCropAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/mycrop.css'
    ];
    public $js = [
        'js/mycrop.js'
    ];
    public $depends = [
        'backend\assets\CropperAsset',
    ];
}