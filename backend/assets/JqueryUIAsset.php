<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 18:04
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class AutocompleteAsset
 * @package frontend\assets
 */
class JqueryUIAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui.min.css',
    ];
    public $js = [
        'js/jquery-ui.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}