<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/switcher.min.css',
        //'css/jquery-ui.min.css',
        'css/profile.css',
        //'css/main.css'
    ];
    public $js = [
        'js/switcher.js',
        'js/script.js',
        'js/jquery.mask.js',
        //'js/jquery-ui.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'dmstr\web\AdminLteAsset'
    ];
}
