<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 11:54
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class AutocompleteAsset
 * @package backend\assets
 */
class AutocompleteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/auto-complete.css'
    ];
    public $js = [
        'js/auto-complete.min.js'
    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];
}