<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.02.2017
 * Time: 12:53
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Asset for job and tender editing
 */
class ProfileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/profile.css',
    ];
    public $js = [

    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];
}
