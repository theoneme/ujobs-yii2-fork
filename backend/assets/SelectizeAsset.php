<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class CropperAsset
 * @package backend\assets
 */
class SelectizeAsset extends AssetBundle
{
    public $sourcePath = '@bower/selectize/dist';
    public $css = [
        'css/selectize.legacy.css',
    ];
    public $js = [
        'js/standalone/selectize.min.js'
    ];
    public $depends = [
        'backend\assets\AppAsset',
        'yii\web\JqueryAsset',
    ];
}