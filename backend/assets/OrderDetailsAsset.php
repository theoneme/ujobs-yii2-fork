<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 18:00
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Asset for job and tender editing
 */
class OrderDetailsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/order-details.css',
    ];
    public $js = [

    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];
}
