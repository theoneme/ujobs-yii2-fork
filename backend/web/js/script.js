$(document).on('click', 'a.modal-edit', function () {
    $('#edit-modal')
        .find('.modalContent')
        .load($(this).attr('href'))
        .closest('#edit-modal')
        .modal('show')
        .find('.modal-header')
        .html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3>' + $(this).attr("title") + '</h3>');
    return false;
});

$(document).on('beforeSubmit', 'form.ajax-submit', function () {
    var self = $(this);
    var formData = new FormData($(this)[0]);

    var pjaxContainer = $(this).data('pj-container');
    $.ajax({
        type: "POST",
        url: $(this).attr('action'),
        data: formData,
        processData: false,
        contentType: false,
        success: function (result) {
            if (result) {
                if ($('#grid-container').length) {
                    $.pjax.reload({container: '#grid-container'});
                }

                if(pjaxContainer != undefined && $('#' + pjaxContainer).length) {
                    $.pjax.reload({container: '#' + pjaxContainer});
                }
                self.closest('.modal').modal('hide');
            }
        }
    });
    return false;
});

$('body').on('click', '.feb-head', function () {
    var faqparent = $(this).parents('.faqedit-block');
    if (faqparent.hasClass('feb-open')) {
        faqparent.removeClass('feb-open');
        faqparent.find('.fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
    } else {
        $('.faqedit-block').removeClass('feb-open');
        $('.faqedit-block .fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
        faqparent.addClass('feb-open');
        faqparent.find('.fa-chevron-down').addClass('fa-chevron-up').removeClass('fa-chevron-down');
    }
});


$('body').on('click', '.faqedit-block .closeedit', function (e) {
    e.preventDefault();
    $(this).parents('.faqedit-block').removeClass('feb-open');
    $('.faqedit-block .fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
});

$(document).on('click', '.more-container a.show-more', function () {
    var self = $(this);
    $(this).slideUp(200, function() {
        self.siblings('.more').slideDown();
    });
    return false;
});

var alertTypes = {
    'success': 'success',
    'info': 'info',
    'warning': 'warning',
    'error': 'danger',
    'danger': 'danger'
};

var alertIcons = {
    'success': 'check',
    'info': 'info',
    'warning': 'warning',
    'error': 'ban',
    'danger': 'ban'
};
function alertCall(type, text) {
    if ($('section.content > .alert').length) {
        $('section.content > .alert').slideUp(400, function() {
            $('section.content > .alert').remove();
            alertCall(type, text);
        });
    }
    else {
        $('section.content').prepend(
            '<div class="alert-' + alertTypes[type] + ' alert fade in">' +
            '   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '   <i class="icon fa fa-' +  alertIcons[type] + '"></i>' + text +
            '</div>'
        );
        $('section.content > .alert').slideDown();
    }
}