$(document).on('change', '#is-shipment-radio', function() {
    if($('div[data-name=addr] input:checked').val() == 1) {
        $('#myAddress').hide().addClass('hidden');
        $('#newAddress').show().removeClass('hidden');
    } else {
        $('#myAddress').show().removeClass('hidden');
        $('#newAddress').hide().addClass('hidden');
    }
});

$(document).on('change', '#myaddressid, #proaddressid', function() {
    $('#employment-address_id').val($(this).val());
});

$(document).on('change', '#job-period_type', function() {
    if($(this).val() == 'start') {
        $('.field-job-date_start').show();
        $('.field-job-date_end').hide();
    } else if($(this).val() == 'end') {
        $('.field-job-date_start').hide();
        $('.field-job-date_end').show();
    } else {
        $('.field-job-date_start').show();
        $('.field-job-date_end').show();
    }
});