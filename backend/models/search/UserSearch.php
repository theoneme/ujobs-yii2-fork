<?php

namespace backend\models\search;

use common\models\Job;
use common\models\UserAttribute;
use dektrium\user\models\UserSearch as BaseUserSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UserSearch extends BaseUserSearch
{
    /** @var int */
    public $admin_comment;

    /** @var int */
    public $status;

    /** @var string */
    public $photo;

    /** @var string */
    public $bio;

    /** @var string */
    public $phone;

    /** @var string */
    public $language;

    /** @var string */
    public $skill;

    /** @inheritdoc */
    public function rules()
    {
        return [
            'fieldsSafe' => [['username', 'phone', 'email', 'registration_ip', 'created_at', 'admin_comment', 'status', 'photo', 'bio', 'id', 'language', 'skill'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Username'),
            'email' => Yii::t('user', 'Email'),
            'created_at' => Yii::t('user', 'Registration time'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
        ];
    }

    /**
     * @param $params
     * @return bool|ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->finder->getUserQuery()
            ->select(['user.id', 'user.username', 'user.phone', 'user.email', 'user.created_at'])
            ->joinWith([
                'jobs' => function($var){
                    /* @var $var ActiveQuery*/
                    return $var->andOnCondition([
                        'not',
                        ['job.status' => [Job::STATUS_DRAFT, Job::STATUS_DELETED]]
                    ]);
                },
                'tenders' => function($var){
                    /* @var $var ActiveQuery*/
                    return $var->andOnCondition([
                        'not',
                        ['tenders.status' => [Job::STATUS_DRAFT, Job::STATUS_DELETED]]
                    ]);
                },
                'socials',
                'profile.translations',
                'company.translations' => function($var){
                    /* @var $var ActiveQuery*/
                    $var->from('content_translation c_ct')->onCondition(['c_ct.entity' => 'company']);
                },
//                'profile.skills',
//                'wallets'
            ])
            ->andWhere(['is_company' => false])
            ->groupBy('user.id');


        if (!empty($this->created_at)) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'user.created_at', $date, $date + 3600 * 24]);
        }

        /*$status = Yii::$app->user->identity->getAccess();
        if($status == User::ROLE_MODERATOR) {
            $query->andWhere(['not', ['profile.status' => Profile::STATUS_ACTIVE]]);
        }*/

        $clonedQuery = clone ($query);
        $clonedQuery->joinWith = [];
        $clonedQuery->orderBy = null;
        if (!empty($this->id)) {
            $clonedQuery->andWhere(['user.id' => $this->id]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC,]],
            'totalCount' => $clonedQuery->count('c.id')
        ]);

        //Это пример на потом для сортировки опубликован\неопубликован
        $dataProvider->sort->attributes['status'] = [
            'asc' => ['profile.status' => SORT_ASC],
            'desc' => ['profile.status' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['photo'] = [
            'asc' => ['profile.gravatar_email' => SORT_ASC],
            'desc' => ['profile.gravatar_email' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bio'] = [
            'asc' => ['profile.bio' => SORT_ASC],
            'desc' => ['profile.bio' => SORT_DESC],
        ];

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->language)) {
            $query->andWhere(['exists', UserAttribute::find()
                ->where('user_attribute.user_id = user.id')
                ->andWhere(['user_attribute.value_alias' => $this->language, 'user_attribute.entity_alias' => 'language'])
            ]);
        }
        if (!empty($this->skill)) {
            $query->andWhere(['exists', UserAttribute::find()
                ->joinWith(['attrValueDescriptions'])
                ->where('user_attribute.user_id = user.id')
                ->andWhere(['user_attribute.entity_alias' => 'skill'])
                ->andWhere(['like', 'mad_2.title', $this->skill])
            ]);
        }

        $query->andFilterWhere(['or',
            ['like', 'user.username', $this->username],
            ['like', 'profile.name', $this->username],
            ['like', 'content_translation.title', $this->username],
            ['like', 'c_ct.title', $this->username],
        ])
            ->andFilterWhere(['user.id' => $this->id])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'user.admin_comment', $this->admin_comment])
            ->andFilterWhere(['profile.status' => $this->status])
            ->andFilterWhere(['user.registration_ip' => $this->registration_ip])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}