<?php

namespace backend\models;

use common\models\search\UserFinder;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Referral;

/**
 * ReferralSearch represents the model behind the search form about `common\models\Referral`.
 */
class ReferralSearch extends Referral
{
    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'total_bonus', 'created_at', 'updated_at'], 'integer'],
            [['email', 'from', 'to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Referral::find();

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $fromIds = null;
        $toIds = null;
        if ($this->from) {
            $query->andFilterWhere(['created_by' => UserFinder::getIdsByName($this->from)]);
        }
        if ($this->to) {
            $query->andFilterWhere(['referral_id' => UserFinder::getIdsByName($this->to)]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'total_bonus' => $this->total_bonus,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
