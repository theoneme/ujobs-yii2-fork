<?php

namespace backend\models;

use common\modules\store\models\PaymentMethod;
use yii\data\ActiveDataProvider;

/**
 * PaymentMethodSearch represents the model behind the search form about `PaymentMethod`.
 */
class PaymentMethodSearch extends PaymentMethod
{

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentMethod::find()
            ->joinWith(['translations', 'countries.translation'])
            ->orderBy(['enabled' => SORT_DESC, 'sort' => SORT_ASC])
            ->groupBy('payment_method.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sort' => $this->sort,
            'enabled' => $this->enabled,
            'additional' => $this->additional,
            'code' => $this->code,
        ]);

        return $dataProvider;
    }
}
