<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.05.2017
 * Time: 10:18
 */

namespace backend\models;

use common\models\Attachment;
use common\models\ContentTranslation;
use common\models\Job;
use common\models\JobAttribute;
use common\models\JobExtra;
use common\models\JobPackage;
use common\models\JobQa;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PostJobForm
 * @package frontend\models
 *
 * @property Job $job
 * @property JobPackage[] $packages
 * @property JobQa[] $faq
 * @property JobExtra[] $extras
 */
class PostRequestForm extends Model
{
    /**
     * @var string
     */
    public $locale;
	/**
	 * @var integer
	 */
	public $parent_category_id;
	/**
	 * @var integer
	 */
	public $category_id;
	/**
	 * @var string
	 */
	public $specialties;
	/**
	 * @var string
	 */
	public $title = null;
	/**
	 * @var string
	 */
	public $description = null;
	/**
	 * @var integer
	 */
	public $price;
	/**
	 * @var boolean
	 */
	public $contract_price;
	/**
	 * @var integer
	 */
	public $percent_bonus;
	/**
	 * @var string
	 */
	public $period_type;
	/**
	 * @var integer
	 */
	public $date_start;
	/**
	 * @var integer
	 */
	public $date_end;
	/**
	 * @var string
	 */
	public $type;
	/**
	 * @var integer
	 */
	public $status;
	/**
	 * @var Job
	 */
	private $_job;
	/**
	 * @var array
	 */
	private $_attachments = null;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			'categoryRequired' => [['category_id', 'parent_category_id'], 'required'],
			'categoryInteger' => [['category_id', 'parent_category_id'], 'integer'],
			'specialtiesLength' => ['specialties', 'string', 'max' => 160],
			['title', 'string', 'max' => 80],
			['title', 'required'],
			['description', 'string', 'max' => 65535],
			'descriptionRequired' => ['description', 'required'],
			'percentBonus' => ['percent_bonus', 'number', 'min' => 0, 'max' => 100],
			'contractPrice' => ['contract_price', 'boolean'],
			'priceInteger' => ['price', 'number', 'min' => 50, 'when' => function ($model) {
				return !$model->contract_price;
			}],
			'periodType' => ['period_type', 'string', 'max' => 15],
			'type' => ['type', 'in', 'range' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER]],
			'dates' => [['date_start', 'date_end'], 'date', 'format' => 'php:d-m-Y'],
			'statusInteger' => ['status', 'integer'],
            ['locale', 'string'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])]
		];
	}

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->locale === null) {
            $this->locale = 'ru-RU';
        }
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'title' => Yii::t('model', 'Title'),
			'category_id' => Yii::t('model', 'Category'),
			'parent_category_id' => Yii::t('model', 'Category'),
			'type' => Yii::t('model', 'Type'),
			'specialties' => Yii::t('model', 'Specialties'),
			'description' => Yii::t('model', 'Description'),
			'price' => Yii::t('model', 'Price'),
			'percent_bonus' => Yii::t('model', 'Percent bonus'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		$this->price = str_replace(' ', '', $this->price);
		if (empty($this->price)) {
			$this->contract_price = true;
		}
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 * @throws \yii\db\Exception
	 */
	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		$transaction = Yii::$app->db->beginTransaction();
		$this->job->attributes = $this->attributes;
		$this->job->user_id = Yii::$app->request->get('user_id');

		if($this->job->save()) {
			$transaction->commit();

            if ($this->job->getOldAttribute('status') !== Job::STATUS_ACTIVE && $this->status == Job::STATUS_ACTIVE) {
                $this->job->sendModerationNotification();
            }

			$this->job->updateElastic();
			$this->job->profile->updateElastic();

			return true;
		}
		
		$transaction->rollBack();
		return false;
	}

	/**
	 * @return array|Attachment[]
	 */
	public function getAttachments()
	{
		if ($this->_attachments === null) {
			if ($this->job->isNewRecord) {
				$this->_attachments = [];
			} else {
				$this->_attachments = $this->job->attachments;
			}
		}
		return $this->_attachments;
	}

	/**
	 * @return Job
	 */
	public function getJob()
	{
		return $this->_job;
	}

	/**
	 * @param Job $job
	 */
	public function setJob(Job $job)
	{
		$this->_job = $job;
	}

	/**
	 * @param $input
	 */
	public function myLoad($input)
	{
		$this->load($input);

		if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
			foreach ($input['Attachment'] as $key => $item) {
				$attachmentObject = new Attachment();
				$attachmentObject->setAttributes($item);
				$attachmentObject->entity = Job::TYPE_JOB;

				$this->job->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
			}
		}

		$translationObject = new ContentTranslation([
			'title' => $this->title,
			'content' => $this->description,
			'locale' => Yii::$app->language,
			'entity' => Job::TYPE_JOB,
		]);
		$this->job->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;

        $specialtyAttribute = Attribute::findOne(['alias' => 'specialty']);
        $this->bindJobAttributes(["attribute_{$specialtyAttribute->id}" => !empty($this->specialties) ? explode(',', $this->specialties) : null]);
	}

	/**
	 * @param $id
	 */
	public function loadJob($id)
	{
		$this->job = Job::findOne((int)$id);
		$this->attributes = $this->job->attributes;
		$this->title = $this->job->translation->title;
		$this->description = $this->job->translation->content;

        $this->specialties = implode(', ', array_map(function($value) {
            /* @var $value JobAttribute*/
            return $value->getTitle();
        }, $this->job->specialties));
	}

	/**
	 * @return array
	 */
	public function validateAll()
	{
		return ArrayHelper::merge(
			ActiveForm::validate($this),
			$this->job->validateWithRelations()
		);
	}

    /**
     * @param array $attributes
     */
    private function bindJobAttributes(array $attributes)
    {
        foreach ($attributes as $key => $attribute) {
            if (!$attribute) continue;

            $parts = explode('_', $key);

            if (is_array($attribute)) {
                foreach ($attribute as $pa) {
                    $jobAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(JobAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                    $this->job->bind('extra', $jobAttributeObject->id)->attributes = $jobAttributeObject->attributes;
                }
            } else {
                $valueParts = explode(',', $attribute);

                if (count($valueParts) > 1) {
                    foreach ($valueParts as $pa) {
                        $jobAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(JobAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                        $this->job->bind('extra', $jobAttributeObject->id)->attributes = $jobAttributeObject->attributes;
                    }
                } else {
                    $jobAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(JobAttribute::class, $parts[1], $attribute, ['locale' => $this->locale]);
                    $this->job->bind('extra', $jobAttributeObject->id)->attributes = $jobAttributeObject->attributes;
                }
            }
        }
    }
}