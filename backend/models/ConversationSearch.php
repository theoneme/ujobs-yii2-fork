<?php

namespace backend\models;

use common\models\Conversation;
use common\models\ConversationMember;
use common\models\Message;
use common\models\Offer;
use common\models\search\UserFinder;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * ConversationSearch represents the model behind the search form about `common\models\Conversation`.
 */
class ConversationSearch extends Conversation
{

    public $status;
    public $message;
    public $price;

    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'created_at', 'updated_at', 'price'], 'integer'],
            [['message', 'from', 'to'], 'string'],
            ['status', 'in', 'range' => [Offer::STATUS_SENT, Offer::STATUS_ACCEPTED, Offer::STATUS_DECLINED, Offer::STATUS_CANCELED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = Conversation::find()
            ->select('conversation.id, conversation.type, conversation.title, conversation.avatar')
            ->addSelect(['hue' => (new Query)
                ->select("max(created_at)")
                ->from('message')
                ->where('conversation.id =  message.conversation_id')
            ])
            ->joinWith(['messages'])
            ->andWhere(['exists', Message::find()
                ->from(['mes' => 'message'])
                ->andWhere("conversation.id = mes.conversation_id")
            ])
            ->orderBy('hue desc')
            ->groupBy(['conversation.id']);

        $fromIds = null;
        $toIds = null;
        if ($this->from) {
            $fromIds = UserFinder::getIdsByName($this->from);
        }
        if ($this->to) {
            $toIds = UserFinder::getIdsByName($this->to);
        }
        if ($this->type === self::TYPE_DEFAULT) {
            $query->joinWith(['lastMessage', 'lastMessage.user', 'lastMessage.user.profile']);
            $query->andWhere(['not', ['exists', ConversationMember::find()
                ->from(['cm' => 'conversation_member'])
                ->andWhere("conversation.id = cm.conversation_id")
                ->andWhere(["cm.user_id" => Yii::$app->params['supportId']])
            ]]);
            if ($this->from) {
                $query->andWhere(['exists', ConversationMember::find()
                    ->from(['cm2' => 'conversation_member'])
                    ->andWhere("conversation.id = cm2.conversation_id")
                    ->andWhere(["cm2.user_id" => $fromIds])
                ]);
            }
            if ($this->to) {
                $query->andWhere(['exists', ConversationMember::find()
                    ->from(['cm3' => 'conversation_member'])
                    ->andWhere("conversation.id = cm3.conversation_id")
                    ->andWhere(["cm3.user_id" => $toIds])
                ]);
            }
        } else {
            $query->joinWith(['firstMessage', 'firstMessage.user', 'firstMessage.user.profile', 'firstMessage.offer']);
            $query->andFilterWhere(['offer.status' => $this->status]);
            $query->andFilterWhere(['offer.price' => $this->price]);
            $query->andFilterWhere(['offer.from_id' => $fromIds]);
            $query->andFilterWhere(['offer.to_id' => $toIds]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'conversation.type' => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['like', 'message.message', $this->message]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
