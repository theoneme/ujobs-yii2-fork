<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserTariff;
use yii\db\ActiveQuery;

/**
 * UserTariffSearch represents the model behind the search form about `common\models\UserTariff`.
 */
class UserTariffSearch extends UserTariff
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'duration', 'cost', 'payment_id', 'status', 'tariff_id'], 'integer'],
            ['username', 'string'],
            [['currency_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserTariff::find()
            ->joinWith(['user.profile.translations', 'user.company.translations' => function($var){
                /* @var $var ActiveQuery*/
                $var->from('content_translation c_ct')->onCondition(['c_ct.entity' => 'company']);
            }, 'tariff'])
            ->orderBy(['user_tariff.created_at' => SORT_DESC])
            ->groupBy('user_tariff.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cost' => $this->cost,
            'user_tariff.status' => [UserTariff::STATUS_ACTIVE, UserTariff::STATUS_EXPIRED],
        ]);

        $query->andFilterWhere(['or',
            ['like', 'user.username', $this->username],
            ['like', 'profile.name', $this->username],
            ['like', 'content_translation.title', $this->username],
            ['like', 'c_ct.title', $this->username],
        ])->andFilterWhere(['like', 'currency_code', $this->currency_code]);

        return $dataProvider;
    }
}
