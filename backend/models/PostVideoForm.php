<?php

namespace backend\models;

use common\models\Video;
use Yii;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/**
 * Class PostVideoForm
 * @package backend\models
 *
 * @property Video $videoObject
 */
class PostVideoForm extends Video
{
    /**
     * @var boolean
     */
    public $free = true;
    /**
     * @var Video
     */
    private $_video;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title', 'category_id', 'parent_category_id'], 'required'],
            [['free'], 'boolean'],
            [['price'], 'required',
                'when' => function ($model) { return ((int)$model->free) === 0;},
                'whenClient' => new JsExpression("function (attribute, value) {return !$('#free-price-checkbox').is(':checked');}")
            ],
        ]);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!empty($this->validateAll())) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->videoObject->attributes = $this->attributes;
        if (!$this->videoObject->alias) {
            $this->videoObject->alias = Yii::$app->utility->generateSlug($this->title, 1, 100);
        }

        $isSaved = $this->videoObject->save();
        $transaction->commit();
        return $isSaved;
    }

    /**
     * @return Video
     */
    public function getVideoObject()
    {
        return $this->_video;
    }

    /**
     * @param Video $video
     */
    public function setVideoObject(Video $video)
    {
        $this->_video = $video;
    }

    /**
     * @param $input
     * @return bool
     */
    public function myLoad($input)
    {
        return $this->load($input);
    }

    /**
     * @param $id
     */
    public function loadVideo($id)
    {
        $this->videoObject = Video::findOne((int)$id);
        $this->attributes = $this->videoObject->attributes;
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ActiveForm::validate($this);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if ($this->free) {
            $this->price = null;
        }
        return parent::beforeValidate();
    }
}