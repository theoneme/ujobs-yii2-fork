<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SeoAdvanced;

/**
 * SeoAdvancedSearch represents the model behind the search form about `common\models\SeoAdvanced`.
 */
class SeoAdvancedSearch extends SeoAdvanced
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['entity', 'entity_content', 'type', 'locale', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeoAdvanced::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['entity' => $this->entity])
            ->andFilterWhere(['entity_content' => $this->entity_content])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'locale', $this->locale])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
