<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Video;
use yii\db\ActiveQuery;

/**
 * VideoSearch represents the model behind the search form about `common\models\Video`.
 */
class VideoSearch extends Video
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'price', 'category_id'], 'integer'],
            [['title', 'description', 'image', 'video', 'username', 'status', 'locale'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Video::find()->joinWith(['user', 'category', 'profile.translations', 'user.company.translations' => function($var){
            /* @var $var ActiveQuery*/
            $var->from('content_translation c_ct')->onCondition(['c_ct.entity' => 'company']);
        }])->groupBy('video.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'price' => $this->price,
            'video.status' => $this->status,
            'video.locale' => $this->locale,
        ]);

        $query->andFilterWhere(['or',
                ['like', 'user.username', $this->username],
                ['like', 'profile.name', $this->username],
                ['like', 'content_translation.title', $this->username],
                ['like', 'c_ct.title', $this->username],
            ])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
