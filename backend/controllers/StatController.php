<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.01.2017
 * Time: 15:25
 */

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\Job;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\board\models\Product;
use Yii;
use yii\data\ArrayDataProvider;


/**
 * Class StatController
 * @package backend\controllers
 */
class StatController extends BackEndController
{
    public function actionIndex()
    {
        $data = [];
        $now = new \DateTime('now');
        $month = Yii::$app->request->get('month') ? Yii::$app->request->get('month') : $now->format('m');
        $year = Yii::$app->request->get('year') ? Yii::$app->request->get('year') : $now->format('Y');
        $day = ((Yii::$app->request->get('month') == $now->format('m') && Yii::$app->request->get('year') == $now->format('Y')) || Yii::$app->request->get('month') == null) ? $now->format('d') : cal_days_in_month(CAL_GREGORIAN, $month, $year);

        for ($i = 1; $i < $day + 1; $i++) {
            $data[] = [
                'id' => $i,
                'totalUsers' => User::find()
                    ->where(['<', 'created_at', strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->count('id'),
                'currentDayUsers' => User::find()
                    ->where(['between', 'created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->count('id'),
                'usersPercent' => 0,
                'activeUsers' => User::find()
                    ->joinWith(['profile'])
                    ->where(['<', 'created_at', strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
                    ->count('id'),
                'activeCurrentDayUsers' => User::find()
                    ->joinWith(['profile'])
                    ->where(['between', 'created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
                    ->count('id'),
                'activeUsersPercent' => 0,
                'totalJobs' => Job::find()
                    ->where(['<', 'created_at', strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['status' => Job::STATUS_ACTIVE])
                    ->count('id'),
                'currentDayJobs' => Job::find()
                    ->where(['between', 'created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['status' => Job::STATUS_ACTIVE])
                    ->count('id'),
                'jobsPercent' => 0,
                'totalProducts' => Product::find()
                    ->where(['<', 'created_at', strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['status' => Product::STATUS_ACTIVE])
                    ->count('id'),
                'currentDayProducts' => Product::find()
                    ->where(['between', 'created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['status' => Product::STATUS_ACTIVE])
                    ->count('id'),
                'productsPercent' => 0
            ];
        }

        foreach ($data as $key => $item) {
            if ($key == 0) continue;

            $currentDayUsers = $item['currentDayUsers'];
            $prevDayUsers = $data[$key - 1]['currentDayUsers'];

            $activeCurrentDayUsers = $item['activeCurrentDayUsers'];
            $activePrevDayUsers = $data[$key - 1]['activeCurrentDayUsers'];

            $currentDayJobs = $item['currentDayJobs'];
            $prevDayJobs = $data[$key - 1]['currentDayJobs'];

            $currentDayProducts = $item['currentDayProducts'];
            $prevDayProducts = $data[$key - 1]['currentDayProducts'];

            if ($prevDayUsers != 0) {
                if ($currentDayUsers < $prevDayUsers) {
                    $usersPercent = (($currentDayUsers - $prevDayUsers) / $prevDayUsers) * 100;
                } else {
                    $usersPercent = (($currentDayUsers - $prevDayUsers) / $prevDayUsers) * 100;//$currentDayUsers / $prevDayUsers * 100;
                }
            } else {
                $usersPercent = ($currentDayUsers - $prevDayUsers) * 100;
            }

            $data[$key]['usersPercent'] = round($usersPercent, 2, PHP_ROUND_HALF_UP);

            if ($activePrevDayUsers != 0) {
                if ($activeCurrentDayUsers < $activePrevDayUsers) {
                    $activeUsersPercent = (($activeCurrentDayUsers - $activePrevDayUsers) / $activePrevDayUsers) * 100;
                } else {
                    $activeUsersPercent = (($activeCurrentDayUsers - $activePrevDayUsers) / $activePrevDayUsers) * 100; //$activeCurrentDayUsers / $activePrevDayUsers * 100;
                }
            } else {
                $activeUsersPercent = ($activeCurrentDayUsers - $activePrevDayUsers) * 100;
            }

            $data[$key]['activeUsersPercent'] = round($activeUsersPercent, 2, PHP_ROUND_HALF_UP);

            if ($prevDayJobs != 0) {
                if ($currentDayJobs < $prevDayJobs) {
                    $jobsPercent = (($currentDayJobs - $prevDayJobs) / $prevDayJobs) * 100;
                } else {
                    $jobsPercent = (($currentDayJobs - $prevDayJobs) / $prevDayJobs) * 100;//$currentDayJobs / $prevDayJobs * 100;
                }
            } else {
                $jobsPercent = ($currentDayJobs - $prevDayJobs) * 100;
            }

            $data[$key]['jobsPercent'] = round($jobsPercent, 2, PHP_ROUND_HALF_UP);

            if ($prevDayProducts != 0) {
                if ($currentDayProducts < $prevDayProducts) {
                    $productsPercent = (($currentDayProducts - $prevDayProducts) / $prevDayProducts) * 100;
                } else {
                    $productsPercent = (($currentDayProducts - $prevDayProducts) / $prevDayProducts) * 100;//$currentDayJobs / $prevDayJobs * 100;
                }
            } else {
                $productsPercent = ($currentDayProducts - $prevDayProducts) * 100;
            }

            $data[$key]['productsPercent'] = round($productsPercent, 2, PHP_ROUND_HALF_UP);
        }

        $data = array_reverse($data);

        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 35,
            ],
        ]);
        //echo '<pre>'; print_r($provider); die();
        // print_r($provider);

        return $this->render('index', [
            'provider' => $provider,
            'month' => $month,
            'year' => $year
        ]);
    }
}
