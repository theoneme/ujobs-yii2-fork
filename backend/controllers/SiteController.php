<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\Job;
use common\models\search\JobSearch;
use common\models\user\Profile;
use common\models\user\User;
use Yii;
use yii\data\ActiveDataProvider;


/**
 * Site controller
 */
class SiteController extends BackEndController
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $latestUsers = User::find()->joinWith(['profile'])->orderBy('created_at desc');
        $latestUsersDataProvider = new ActiveDataProvider([
            'query' => $latestUsers,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        $latestJobs = Job::find()->joinWith(['translation'])->where(['type' => 'job'])->andWhere(['>', 'status', Job::STATUS_REQUIRES_MODERATION])->orderBy('created_at desc');
        $latestJobsDataProvider = new ActiveDataProvider([
            'query' => $latestJobs,
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        $latestTenders = Job::find()->joinWith(['translation'])->where(['type' => 'tender'])->orderBy('created_at desc');
        $latestTendersDataProvider = new ActiveDataProvider([
            'query' => $latestTenders,
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        /*$notModeratedJobs = Job::find()->joinWith(['translation'])->where(['type' => 'job', 'is_moderated' => false])->orderBy('created_at asc');
        $notModeratedJobsDataProvider = new ActiveDataProvider([
            'query' => $notModeratedJobs,
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);*/

        $jobSearchModel = new JobSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['JobSearch']['status'] = Job::STATUS_REQUIRES_MODERATION;
        $queryParams['JobSearch']['type'] = 'job';
        $notModeratedJobsDataProvider = $jobSearchModel->search($queryParams, 6);

        $tenderSearchModel = new JobSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['JobSearch']['status'] = Job::STATUS_REQUIRES_MODERATION;
        $queryParams['JobSearch']['type'] = 'tender';
        $notModeratedTendersDataProvider = $tenderSearchModel->search($queryParams, 6);

        $totalUsers = User::find()->select('id')->count();
        $workerUsers = Profile::find()->where(['status' => Profile::STATUS_ACTIVE])->count();
        $usersWithJobs = Job::find()->select('user_id')->groupBy('user_id')->count();
        $totalJobs = Job::find()->select('id')->count();

        return $this->render('index', [
            'latestUsersDataProvider' => $latestUsersDataProvider,
            'latestJobsDataProvider' => $latestJobsDataProvider,
            'latestTendersDataProvider' => $latestTendersDataProvider,

            'notModeratedJobsDataProvider' => $notModeratedJobsDataProvider,
            'jobSearchModel' => $jobSearchModel,
            'notModeratedTendersDataProvider' => $notModeratedTendersDataProvider,
            'tenderSearchModel' => $tenderSearchModel,

            'totalUsers' => $totalUsers,
            'workerUsers' => $workerUsers,
            'usersWithJobs' => $usersWithJobs,
            'totalJobs' => $totalJobs
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
