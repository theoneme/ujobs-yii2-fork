<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.08.2016
 * Time: 16:29
 */

namespace backend\controllers;

use backend\models\PostRequestForm;
use common\controllers\BackEndController;
use common\models\Category;
use common\models\ContentTranslation;
use common\models\Job;
use common\models\search\JobSearch;
use common\models\user\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * JobController implements the CRUD actions for Job model.
 */
class TenderController extends BackEndController
{
    const CONTENT_ATTRIBUTE_ID = 8;
    const TITLE_ATTRIBUTE_ID = 6;
    const CONTENT_PREV_ATTRIBUTE_ID = 7;

    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Job model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Job model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Job the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreate()
    {
        $model = new PostRequestForm();
        $model->job = new Job();
        $isSaved = false;

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])
                ->all(),
            'id',
            'translation.title'
        );

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);

            if (!isset($input['ajax'])) {
                $isSaved = $model->save();
            }

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/user/admin/tenders', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles = [
                'yii\bootstrap\BootstrapPluginAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\web\JqueryAsset' => false,
            ];
            $this->layout = false;

            return $this->renderAjax('_form-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
            ]);
        } else {
            return $this->render('create-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = new PostRequestForm();
        $model->loadJob($id);
        $isSaved = false;

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])
                ->all(),
            'id',
            'translation.title'
        );

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);

            if (!isset($input['ajax'])) {
                $isSaved = $model->save();
            }

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/user/admin/tenders', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles = [
                'yii\bootstrap\BootstrapPluginAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\web\JqueryAsset' => false,
            ];
            $this->layout = false;

            return $this->renderAjax('_form-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
            ]);
        } else {
            return $this->render('update-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList
            ]);
        }
    }

    /**
     * Deletes an existing Job model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['index']);
        }
    }

    public function actionModerateSelected()
    {
        $keys = Yii::$app->request->post('selection');
        $res = Job::updateAll(['is_moderated' => 1], ['id' => $keys, 'type' => 'tender']);

        return $res;
    }
}