<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\Category;
use common\models\Page;
use common\models\PageAttribute;
use common\models\search\PageSearch;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use backend\actions\GetImagesAction;
use backend\actions\UploadFileAction;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BackEndController
{
    public function actions()
    {
        return [
            'file-upload' => [
                'class' => UploadFileAction::class,
                'path' => '@frontend/web/uploads/impera', // Or absolute path to directory where files are stored.
                'url' => 'https://ujobs.me/uploads/impera/',
                'uploadOnlyImage' => true, // For not image-only uploading.
            ],
            'images-get' => [
                'class' => GetImagesAction::class,
                'url' => 'https://ujobs.me/uploads/impera/', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/impera', // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']], // These options are by default.
            ]
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $params = Yii::$app->request->queryParams;
        $params['PageSearch']['type'] = Page::TYPE_DEFAULT;
        $dataProvider = $searchModel->search($params);

        $categories = Category::find()->orderBy('name asc')->all();
        $currentCategory = !empty($_GET['PageSearch']) ? (int)$_GET['PageSearch']['category_id'] : null;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'currentCategory' => $currentCategory
        ]);
    }


    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionArticles()
    {
        $searchModel = new PageSearch();
        $params = Yii::$app->request->queryParams;
        $params['PageSearch']['type'] = Page::TYPE_ARTICLE;
        $dataProvider = $searchModel->search($params);

        $categories = Category::find()->where(['type' => 'job_category'])->orderBy('name asc')->all();
        $currentCategory = $_GET['PageSearch']['category_id'] ?? null;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'currentCategory' => $currentCategory
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::find()->with(['translations'])->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        $input = Yii::$app->request->post();
        $locales = Yii::$app->params['languages'];

        if (!empty($input)) {
            $model->load($input);

            foreach ($input['ContentTranslation'] as $key => $value) {
                $value['entity'] = 'page';
                $model->bind('translations', (int)$value['id'] ?? null, $key)->attributes = $value;
            }
            $tagAttribute = Attribute::findOne(['alias' => 'page_tag']);
            $tags = !empty($model->tagsString) ? explode(',', $model->tagsString) : [];
            foreach ($tags as $tag) {
                $pageAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(PageAttribute::class, $tagAttribute->id, $tag, ['locale' => Yii::$app->language]);
                $model->bind('extra', $pageAttributeObject->id)->attributes = $pageAttributeObject->attributes;
            }
            if (empty($model->validateWithRelations())) {
                $isSaved = $model->save();
                $model->updateElastic();
                if (Yii::$app->request->isAjax) {
                    return $isSaved;
                }
                $this->redirect(['index']);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax("_form", [
                'model' => $model,
                'locales' => $locales,
                'isAjax' => true,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'locales' => $locales,
                'isAjax' => false,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->detachBehavior('blameable');
        $input = Yii::$app->request->post();
        $locales = Yii::$app->params['languages'];

        if (!empty($input)) {
            $model->load($input);

            foreach ($input['ContentTranslation'] as $key => $value) {
                if (!empty($value['title'])) {
                    $value['entity'] = 'page';
                    $model->bind('translations', $value['id'] ? (int)$value['id'] : null, $key)->attributes = $value;
                }
            }
            $tagAttribute = Attribute::findOne(['alias' => 'page_tag']);
            $tags = !empty($model->tagsString) ? explode(',', $model->tagsString) : [];
            foreach ($tags as $tag) {
                $pageAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(PageAttribute::class, $tagAttribute->id, $tag, ['locale' => Yii::$app->language]);
                $model->bind('extra', $pageAttributeObject->id)->attributes = $pageAttributeObject->attributes;
            }
            if (empty($model->validateWithRelations())) {
                $isSaved = $model->save();
                $model->updateElastic();
                if (Yii::$app->request->isAjax) {
                    return $isSaved;
                }
                $this->redirect([$model->type === Page::TYPE_DEFAULT ? 'index': 'articles']);
            }
        }

        $model->tagsString = implode(',',
            array_map(function($value) {
                /* @var $value PageAttribute*/
                return $value->getTitle();
            }, $model->tags)
        );
        $formView = $model->type === Page::TYPE_DEFAULT ? '_form' : '_article_form';
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($formView, [
                'model' => $model,
                'locales' => $locales,
                'isAjax' => true,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'locales' => $locales,
                'isAjax' => false,
                'formView' => $formView
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteMultiple()
    {
        $pk = Yii::$app->request->post('pk');

        if (!$pk) {
            return;
        }

        return Page::deleteAll(['id' => $pk]);
    }
}
