<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\JobPackage;
use common\models\user\Profile;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package backend\controllers
 */
class AjaxController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['attribute-list', 'sellers-list', 'packages-list'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @param $alias
     * @param null $id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAttributeList($alias = null, $id = null)
    {
        if ($alias === null && $id === null) {
            return [];
        }

        $query = Yii::$app->request->post('query', null);
        $attributes = AttributeValue::find()
            ->joinWith(['relatedAttribute', 'translation'])
            ->select(['mod_attribute_value.id', 'mod_attribute_value.alias', 'mod_attribute_description.title as value', 'mod_attribute_value.attribute_id'])
            ->andWhere(['like', 'mod_attribute_description.title', urldecode($query)])
            ->asArray();

        if ($alias !== null) {
            $attributes->andWhere(['mod_attribute.alias' => $alias]);
        } else if ($id !== null) {
            $attributes->andWhere(['mod_attribute.id' => $id]);
        }

        return $attributes->all();
    }

    /**
     * @return array
     */
    public function actionSellersList()
    {
        $query = Yii::$app->request->post('query', null);
        $attributes = Profile::find()
            ->joinWith(['user'])
            ->andWhere(['or',
                ['user.id' => urldecode($query)],
                ['like', 'user.email', urldecode($query)],
                ['like', 'user.phone', urldecode($query)]
            ])
            ->groupBy('profile.user_id')
            ->limit(100)
            ->all();

        $result = array_map(function ($value) {
            return ['id' => $value->user_id, 'value' => "{$value->getSellerName()} (ID: {$value->user_id})"];
        }, $attributes);

        return $result;
    }

    /**
     * @return array
     */
    public function actionPackagesList()
    {
        $query = Yii::$app->request->post('query', null);
        $attributes = JobPackage::find()
            ->joinWith(['job'])
            ->andWhere(['like', 'job_package.title', urldecode($query)])
            ->limit(15)
            ->groupBy('job.id')
            ->all();

        $result = array_map(function ($value) {
            return ['id' => $value->id, 'value' => "{$value->title} - {$value->job->translation->title} (Package ID: {$value->id}, Job ID: {$value->job->id}, User: {$value->job->user->getSellerName()} ID: {$value->job->user_id})"];
        }, $attributes);

        return $result;
    }
}
