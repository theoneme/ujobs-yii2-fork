<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 16:26
 */

namespace backend\controllers;

use common\components\CurrencyHelper;
use common\controllers\BackEndController;
use common\models\PaymentHistory;
use common\models\Review;
use common\models\search\UserWithdrawalSearch;
use common\models\UserWithdrawal;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;

/**
 * UserWithdrawalController implements the CRUD actions for UserWithdrawal model.
 */
class UserWithdrawalController extends BackEndController
{
    /**
     * Lists all UserWithdrawal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserWithdrawalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $userId
     * @return string
     */
    public function actionViewHistory($userId)
    {
        $data = [];
        $payments = PaymentHistory::find()
            ->where(['user_id' => $userId])
            ->orderBy(['id' => SORT_ASC])
            ->andWhere(['not', ['currency_code' => 'CTY']])
            ->all();

        foreach ($payments as $key => $payment) {
            /* @var PaymentHistory $payment */
            $relatedOrder = $payment->order;
            $rating = 0;
            $orderId = null;
            $date = $payment->created_at;
            if ($relatedOrder) {
                $relatedOrderReview = Review::find()->where(['order_id' => $relatedOrder->id, 'created_for' => $relatedOrder->seller_id])->one();
                if ($relatedOrderReview) {
                    $rating = $relatedOrderReview->rating;
                } else {
                    $rating = Yii::$app->formatter->asDate($relatedOrder->updated_at);
                }

                $orderId = $relatedOrder->id;
                //$date = $relatedOrder->created_at;
            }

            $amount = $payment->balance_change === PaymentHistory::TYPE_BALANCE_CHANGE_POS
                ? CurrencyHelper::convert($payment->currency_code, Yii::$app->params['app_currency_code'], $payment->amount)
                : 0;
            $withdrawAmount = $payment->balance_change === PaymentHistory::TYPE_BALANCE_CHANGE_NEG
                ? CurrencyHelper::convert($payment->currency_code, Yii::$app->params['app_currency_code'], $payment->amount)
                : 0;
            $posBalance = $negBalance = 0;

            $posBalanceItems = PaymentHistory::find()
                ->where(['balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS])
                ->andWhere(['user_id' => $userId])
                ->andWhere(['<=', 'id', $payment->id])
                ->andWhere(['not', ['currency_code' => 'CTY']])
                ->orderBy(['id' => SORT_ASC])
//                ->limit($key + 1)
                ->all();
            $negBalanceItems = PaymentHistory::find()
                ->where(['balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_NEG])
                ->andWhere(['user_id' => $userId])
                ->andWhere(['<=', 'id', $payment->id])
                ->andWhere(['not', ['currency_code' => 'CTY']])
                ->orderBy(['id' => SORT_ASC])
//                ->limit($key + 1)
                ->all();

            if ($posBalanceItems !== null) {
                foreach ($posBalanceItems as $item) {
                    /* @var PaymentHistory $item */
                    $posBalance += CurrencyHelper::convert($item->currency_code, Yii::$app->params['app_currency_code'], $item->amount);
                }
            }
            if ($negBalanceItems !== null) {
                foreach ($negBalanceItems as $item) {
                    /* @var PaymentHistory $item */
                    $negBalance += CurrencyHelper::convert($item->currency_code, Yii::$app->params['app_currency_code'], $item->amount);
                }
            }

            $balance = $posBalance - $negBalance;
            $type = null;

            switch ($payment->type) {
                case PaymentHistory::TYPE_OPERATION_PURCHASE:
                    $type = ($relatedOrder && $relatedOrder->product) ? 'Купил | ' . StringHelper::truncate($relatedOrder->product->getLabel(), 32) : 'Купил услугу';
                    break;
                case PaymentHistory::TYPE_OPERATION_REVENUE:
                    $type = ($relatedOrder && $relatedOrder->product) ? 'Продал | ' . StringHelper::truncate($relatedOrder->product->getLabel(), 32) : 'Продал услугу';
                    break;
                default:
                    $type = $payment->description ?? $payment->getHeader();
                    break;
            }

            $class = null;
            if ($payment->balance_change === PaymentHistory::TYPE_BALANCE_CHANGE_POS) {
                $class = 'green-cell';
            } else if ($payment->balance_change === PaymentHistory::TYPE_BALANCE_CHANGE_NEG) {
                $class = 'red-cell';
            }

            $data[] = [
                'orderId' => $orderId,
                'date' => Yii::$app->formatter->asDate($date),
                'job' => $type,
                'rating' => $rating,
                'amount' => $amount,
                'withdrawAmount' => $withdrawAmount,
                'balance' => $balance,
                'balance_change' => $payment->balance_change,
                'currency_code' => $payment->currency_code,
                'class' => $class
            ];
        }
        $data = array_reverse($data);
        $paymentsDataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('history', [
                'dataProvider' => $paymentsDataProvider
            ]);
        }

        return $this->render('history', [
            'dataProvider' => $paymentsDataProvider
        ]);
    }

    /**
     * Displays a single UserWithdrawal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the UserWithdrawal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserWithdrawal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserWithdrawal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new UserWithdrawal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserWithdrawal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserWithdrawal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserWithdrawal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}