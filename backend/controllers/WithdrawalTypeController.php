<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\WithdrawalTypeCountry;
use common\models\WithdrawalWallet;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class WithdrawalTypeController
 * @package backend\controllers
 */
class WithdrawalTypeController extends BackEndController
{
    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionIndex()
    {
        $types = WithdrawalWallet::getTypeLabels();
        $dataProvider = new ArrayDataProvider([
            'allModels' => array_map(function($type, $label) {
                $countries = WithdrawalTypeCountry::find()->joinWith(['country.translations'])->where(['type' => $type, 'locale' => 'ru-RU'])->select('mod_attribute_description.title')->column();
                return [
                    'type' => $type,
                    'label' => $label,
                    'countries' => implode(', ', $countries)
                ];
            }, array_keys($types), $types),
            'pagination' => [
                'pageSize' => 35,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $type
     * @return bool|string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($type)
    {
        $types = WithdrawalWallet::getTypeLabels();
        if (!array_key_exists($type, $types)) {
            throw new NotFoundHttpException('Type not found');
        }
        if ($input = Yii::$app->request->post()) {
            $countries = Yii::$app->request->post('countries', '');
            $countries = !empty($countries) ? explode(',', $countries) : [];
            WithdrawalTypeCountry::deleteAll(['and',
                ['type' => $type],
                ['not', ['country_id' => $countries]]
            ]);
            foreach ($countries as $country_id) {
                WithdrawalTypeCountry::findOrCreate(['type' => $type, 'country_id' => $country_id]);
            }

            if (Yii::$app->request->isAjax) {
                return true;
            }
        }
        $countries = WithdrawalTypeCountry::find()
            ->select('mod_attribute_description.title')
            ->joinWith(['country.translations'])
            ->where(['type' => $type])
            ->andWhere(['locale' => 'ru-RU'])
            ->indexBy('country_id')
            ->column();
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'type' => $type,
                'label' => $types[$type],
                'countries' => $countries,
                'isAjax' => true,
            ]);
        } else {
            return $this->render('_form', [
                'type' => $type,
                'label' => $types[$type],
                'countries' => $countries,
            ]);
        }
    }
}
