<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\Category;
use common\models\Job;
use common\models\JobSearchOption;
use common\models\JobSearchStep;
use common\models\JobSearchTree;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\db\Expression;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * JobSearchController implements the CRUD actions for JobSearchTree model.
 */
class JobSearchController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete-tree' => ['POST'],
                    'delete-step' => ['POST'],
                    'delete-option' => ['POST'],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['entity-list', 'job-list'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * Lists all JobSearchTree models.
     * @return mixed
     */
    public function actionIndex()
    {
        $trees = JobSearchTree::find()->joinWith(['jobSearchSteps.options'])->all();

        return $this->render('index', ['models' => $trees]);
    }

    /**
     * Creates or updates JobSearchTree model.
     * @param integer $id
     * @return mixed
     */
    public function actionTree($id = null)
    {
        $model = JobSearchTree::findOne($id) ?? new JobSearchTree();

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
//            $model->translationsData = Yii::$app->request->post('ContentTranslation', [])['JobSearchTree'];
            if (empty($model->validateWithRelations())) {
                return ['success' => $model->save(false), 'id' => 'tree-' . $model->id];
            }
            else {
                return ['success' => false];
            }
        }
        return $this->renderAjax('_tree_form', ['model' => $model, 'create' => $model->isNewRecord]);
    }

    /**
     * Creates or updates JobSearchStep model.
     * @param $tree_id null|integer
     * @param $id null|integer
     * @return mixed
     */
    public function actionStep($id = null, $tree_id = null)
    {
        $model = JobSearchStep::findOne($id) ?? new JobSearchStep(['tree_id' => $tree_id]);

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->translationsData = Yii::$app->request->post('ContentTranslation', [])['JobSearchStep'];
            if (empty($model->validateWithRelations())) {
                return ['success' => $model->save(false), 'id' => 'step-' . $model->id];
            }
            else {
                return ['success' => false];
            }
        }
        return $this->renderAjax('_step_form', ['model' => $model, 'create' => $model->isNewRecord]);
    }

    /**
     * Creates or updates JobSearchStep model.
     * @param $step_id null|integer
     * @param $id null|integer
     * @return mixed
     */
    public function actionOption($id = null, $step_id = null)
    {
        $model = JobSearchOption::findOne($id) ?? new JobSearchOption(['step_id' => $step_id]);

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->translationsData = Yii::$app->request->post('ContentTranslation', [])['JobSearchOption'];
            $model->attachmentsData = Yii::$app->request->post('Attachment', []);
            if (in_array((int)$model->type, [JobSearchOption::TYPE_NEXT_STEP, JobSearchOption::TYPE_JOB])) {
                $nextStep = JobSearchStep::findOne($model->next_step_id) ?? new JobSearchStep();
                if ($nextStep->load(Yii::$app->request->post())) {
                    $nextStep->translationsData = Yii::$app->request->post('ContentTranslation', [])['JobSearchStep'];
                    $nextStep->measureTranslationsData = Yii::$app->request->post('ContentTranslation', [])['JobSearchStepMeasure'];
                    if (empty($nextStep->validateWithRelations())) {
                        $nextStep->save(false);
                        $model->next_step_id = $nextStep->id;
                    }
                    else {
                        return ['success' => false];
                    }
                }
            }
            if (empty($model->validateWithRelations())) {
                return ['success' => $model->save(false), 'id' => 'option-' . $model->id];
            }
            else {
                return ['success' => false];
            }
        }
        return $this->renderAjax('_option_form', ['model' => $model, 'create' => $model->isNewRecord]);
    }

    /**
     * @return array
     */
    public function actionEntityList()
    {
        $query = urldecode(Yii::$app->request->post('query'));
        if (!$query) {
            return [];
        }
        $categories = Category::find()
            ->select([
                'category.id',
                'value' => new Expression('CONCAT("category-", category.id)'),
                'label' => new Expression('CONCAT("(Категория) ", content_translation.title)')
            ])
            ->joinWith(['translations'])
            ->where(['like', 'content_translation.title', $query])
            ->andWhere(['category.type' => 'job_category'])
            ->groupBy('category.id')
            ->limit(10)
            ->asArray()
            ->all();
        $tags = AttributeValue::find()
            ->select([
                'mod_attribute_value.id',
                'mod_attribute_value.attribute_id',
                'value' => new Expression('CONCAT("tag-", mod_attribute_value.alias)'),
                'label' => new Expression('CONCAT("(Тег) ", mod_attribute_description.title)')
            ])
            ->joinWith(['translations', 'relatedAttribute'])
            ->where(['mod_attribute.alias' => 'tag'])
            ->andWhere(['like', 'mod_attribute_description.title', $query])
            ->groupBy('mod_attribute_value.id')
            ->limit(10)
            ->asArray()
            ->all();
        return array_merge($categories, $tags);
    }


    /**
     * @return array
     */
    public function actionJobList()
    {
        $query = urldecode(Yii::$app->request->post('query', null));
        $jobs = Job::find()
            ->select([
                'value' => 'job.id',
                'label' => new Expression('CONCAT("ID: ", job.id, "  .Название работы: ", content_translation.title)')
            ])
            ->joinWith(['translations'])
            ->where(['or',
                ['like', 'content_translation.title', $query],
                ['job.id' => $query]
            ])
            ->groupBy('job.id')
            ->limit(20)
            ->asArray()
            ->all();
        return $jobs;
    }

    /**
     * Deletes an existing JobSearchTree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     */
    public function actionDeleteTree($id)
    {
        JobSearchTree::findOne($id)->delete();
    }

    /**
     * Deletes an existing JobSearchStep model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     */
    public function actionDeleteStep($id)
    {
        JobSearchStep::findOne($id)->delete();
    }

    /**
     * Deletes an existing JobSearchOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     */
    public function actionDeleteOption($id)
    {
        JobSearchOption::findOne($id)->delete();
    }
}
