<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace backend\controllers\user;

use backend\models\search\UserSearch;
use common\behaviors\LinkableBehavior;
use common\components\CurrencyHelper;
use common\models\ContentTranslation;
use common\models\forms\UserLanguageForm;
use common\models\forms\UserSkillForm;
use common\models\forms\UserSpecialtyForm;
use common\models\Job;
use common\models\Message;
use common\models\PaymentHistory;
use common\models\search\JobSearch;
use common\models\user\User;
use common\models\UserAttribute;
use common\models\UserPortfolio;
use common\models\WithdrawalWallet;
use common\modules\attribute\models\AttributeValue;
use common\services\AttributeToFormService;
use dektrium\user\controllers\AdminController as BaseAdminController;
use dektrium\user\Module;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;

/**
 * AdminController allows you to administrate users.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class AdminController extends BaseAdminController
{
    /*
     * @var array
     */
    private $allowedModeratorsControllers = [
        'admin' => [
            'update' => true,
            'index' => true,
            'update-ajax' => true,
            'render-public-skill' => true,
            'render-public-language' => true,
            'render-public-portfolio' => true,
            'render-public-specialty' => true,
            'emails' => true,
            'live' => true,
            'dialog' => true,
            'view' => true,
            'message' => true,
            'send-mail-ajax' => true,
            'adverts' => true,
            'history' => true,
            'index-tender' => true,
            'update-tender' => true,
            'render-attributes' => true,
            'item' => true,
            'option' => true,
            'detail-category' => true,
            'detail' => true,
            'extra' => true,
            'delete-item' => true,
        ],
        'image' => [
            'upload' => true,
            'upload-profile' => true,
            'upload-wide' => true,
            'delete' => true
        ],
        'job' => [
            'update' => true,
            'index' => [
                'status' => [
                    Job::STATUS_REQUIRES_MODERATION,
                    Job::STATUS_DRAFT,
                    Job::STATUS_REQUIRES_MODIFICATION
                ]
            ],
            'auto' => [
                'status' => [
                    Job::STATUS_REQUIRES_MODERATION,
                    Job::STATUS_DRAFT,
                    Job::STATUS_REQUIRES_MODIFICATION
                ]
            ],
            'drafts' => [
                'status' => [
                    Job::STATUS_REQUIRES_MODERATION,
                    Job::STATUS_DRAFT,
                    Job::STATUS_REQUIRES_MODIFICATION
                ]
            ],
            'render-faq' => true,
            'render-extra' => true,
        ],
        'tender' => [
            'update' => true,
            'update-ajax' => true
        ],
        'ajax' => [
            'attribute-list' => true,
        ],
        'category' => [
            'subcat' => true,
        ],
        'site' => [
            'index' => true
        ],
        'order' => [
            'index' => true,
            'view' => true,
            'tender' => true
        ],
        'payment' => [
            'index' => true,
        ],
        'stat' => [
            'index' => true
        ],
        'video' => [
            'index' => true,
            'create' => true,
            'update' => true
        ],
        'seo-advanced' => [
            'index' => true,
            'create' => true,
            'update' => true
        ],
        'live-dialog-member' => [
            'index' => true,
            'dialog' => true
        ],
        'livechat' => [
            'index' => true,
            'emails' => true
        ],
        'banner' => [
            'index' => true,
            'create' => true,
            'update' => true
        ],
        'job-search' => [
            'index' => true,
            'tree' => true,
            'step' => true,
            'option' => true,
            'entity-list' => true,
            'job-list' => true,
            'delete-tree' => true,
            'delete-step' => true,
            'delete-option' => true,
        ],
        'user-portfolio' => [
            'index' => true,
            'update' => true,
            'render-extra' => true,
            'old' => true,
        ],
        'page' => [
            'index' => true,
            'create' => true,
            'update' => true,
            'articles' => true,
            'file-upload' => true,
            'images-get' => true,
        ],
        'conversation' => [
            'index' => true,
            'orders' => true,
            'view' => true,
            'create' => true,
            'update' => true,
            'delete' => true,
        ],
        'message' => [
            'index' => true,
            'view' => true,
            'create' => true,
            'update' => true,
            'delete' => true,
        ],
        'withdrawal-type' => [
            'index' => true,
            'update' => true,
        ],
        'payment-method' => [
            'index' => true,
            'update' => true,
        ],
        'user-withdrawal' => [
            'index' => true,
            'update' => true,
            'view-history' => true
        ]
    ];

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'confirm' => ['post'],
                    'block' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAllowed();
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param null $controllerToCheck
     * @param null $actionToCheck
     * @return bool
     */
    public function isAllowed($controllerToCheck = null, $actionToCheck = null)
    {
        $isGranted = false;
        /* @var User $identity */
        $identity = Yii::$app->user->identity;

        switch ($identity->getAccess()) {
            case User::ROLE_ADMIN:
                $isGranted = true;
                break;
            case User::ROLE_MODERATOR:
                $controller = $controllerToCheck ? $controllerToCheck : Yii::$app->controller->id;
                $action = $actionToCheck ? $actionToCheck : Yii::$app->controller->action->id;
                if (isset($this->allowedModeratorsControllers[$controller][$action])) {
                    $isGranted = true;
                }
                break;
            default:
                break;
        }
        return $isGranted;
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        $searchModel = Yii::createObject(UserSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $sup_id = Yii::$app->params['supportId'];
        $userIds = ArrayHelper::getColumn($dataProvider->getModels(), 'id');
        $sentMessages = Message::find()
            ->joinWith(['conversationMembers'])
            ->select(['COUNT(message.id)', 'conversation_member.user_id as uid'])
            ->where(['message.user_id' => $sup_id])
            ->andWhere(['conversation_member.user_id' => $userIds])
            ->andWhere('conversation_member.user_id != message.user_id')
            ->indexBy('uid')
            ->groupBy('uid')
            ->asArray()
            ->column();
        $unreadMessages = Message::find()
            ->joinWith(['conversationMembers'])
            ->select(['COUNT(message.id)', 'message.user_id as uid'])
            ->where('message.created_at > conversation_member.viewed_at')
            ->where(['message.user_id' => $userIds])
            ->andWhere('conversation_member.user_id != message.user_id')
            ->andWhere(['conversation_member.user_id' => $sup_id])
            ->indexBy('uid')
            ->groupBy('uid')
            ->asArray()
            ->column();
        $languages = AttributeValue::find()
            ->joinWith(['relatedAttribute', 'translation'])
            ->select(['mod_attribute_description.title as value', 'mod_attribute_value.alias', 'mod_attribute_value.attribute_id', 'mod_attribute_value.id'])
            ->where(['mod_attribute.alias' => 'language', 'mod_attribute_description.locale' => 'ru-RU'])
            ->indexBy('alias')
            ->asArray()
            ->column();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'sentMessages' => $sentMessages,
            'unreadMessages' => $unreadMessages,
            'languages' => $languages,
        ]);
    }

    /**
     * @return array
     */
    public function actionRandomEmail()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $faker = \Faker\Factory::create();

        $output = [
            'email' => $faker->freeEmail
        ];

        return $output;
    }

    /**
     * @param null $user_id
     * @return string
     */
    public function actionJobs($user_id = null)
    {
        $user = $this->findModel($user_id);

        $searchModel = new JobSearch(['user_id' => $user_id, 'type' => Job::TYPE_JOB]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_jobs', [
            'user' => $user,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param null $user_id
     * @return string
     */
    public function actionTenders($user_id = null)
    {
        $user = $this->findModel($user_id);

        $searchModel = new JobSearch(['user_id' => $user_id, 'type' => Job::TYPE_TENDER]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_tenders', [
            'user' => $user,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Updates an existing User model.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdateAjax($id)
    {
        /* @var User $user */
        $user = $this->findModel($id);
        $profile = $user->profile;

        $input = Yii::$app->request->post();

        if ($user->load($input) && $profile->load($input)) {
            $profile->userSkills = $input['UserSkillForm'] ?? [];
            $profile->userLanguages = $input['UserLanguageForm'] ?? [];
//            $profile->userPortfolio = $input['UserPortfolio'] ? $input['UserPortfolio'] : [];
            $profile->userSpecialties = $input['UserSpecialtyForm'] ?? [];
            $profile->attachBehavior('linkableTemp', [
                'class' => LinkableBehavior::class,
                'relations' => ['withdrawalWallets'],
            ]);
            foreach ($input['WithdrawalWallet'] as $item) {
                if (!empty($item['number'])) {
                    $profile->bind('withdrawalWallets', $item['id'])->attributes = $item;
                }
            }
            $user->phone = preg_replace("/[^0-9]/", "", $user->phone);
            if (!empty($user->phone)) {
                $user->phone = "+" . $user->phone;
            }
            $isSaved = $user->save() && $profile->save();
            if ($isSaved) {
                if (!empty($input['User']['password'])) {
                    $user->resetPassword($input['User']['password']);
                }
                foreach ($input['ContentTranslation'] as $locale => $attributes) {
                    $translation = $profile->translations[$locale] ?? new ContentTranslation(['entity' => 'profile', 'entity_id' => $profile->user_id, 'locale' => $locale]);
                    $translation->load($attributes, '');
                    $translation->save();
                }
            }

            return $isSaved;
        }

        $languageQuery = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescription'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'language'])->all();
        $languages = AttributeToFormService::process($languageQuery, UserLanguageForm::class);

        $skillQuery = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescription'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'skill'])->all();
        $skills = AttributeToFormService::process($skillQuery, UserSkillForm::class);

        $specialtyQuery = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescription'])->where(['user_id' => $profile->user_id, 'entity_alias' => 'specialty'])->all();
        $specialties = AttributeToFormService::process($specialtyQuery, UserSpecialtyForm::class);

        $this->layout = false;
        return $this->renderAjax('_user_profile_ajax', [
            'user' => $user,
            'profile' => $profile,
            'languages' => $languages,
            'skills' => $skills,
            'specialties' => $specialties,
            'withdrawalTypesData' => WithdrawalWallet::getTypesByCountry($profile->user_id),
        ]);
    }

    /**
     * @param null $userId
     * @return string
     */
    public function actionPaymentHistory($userId = null)
    {
        $paymentsQuery = PaymentHistory::find()->where(['user_id' => $userId])->orderBy('created_at desc');
        $paymentsDataProvider = new ActiveDataProvider([
            'query' => $paymentsQuery,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $posSum = $negSum = 0;

        $pos = PaymentHistory::find()->where(['balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS, 'user_id' => $userId])->all();
        $neg = PaymentHistory::find()->where(['balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_NEG])->andWhere(['user_id' => $userId])->all();

        /* @var PaymentHistory $item */
        foreach ($pos as $item) {
            $posSum += CurrencyHelper::convert($item->currency_code, 'RUB', $item->amount);
        }
        foreach ($neg as $item) {
            $negSum += CurrencyHelper::convert($item->currency_code, 'RUB', $item->amount);
        }

        $user = User::findOne($userId);

        $this->layout = false;
        return $this->renderAjax('history', [
            'dataProvider' => $paymentsDataProvider,
            'posSum' => $posSum,
            'negSum' => $negSum,
            'balance' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], $user->getBalance()),
        ]);
    }

    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicLanguage($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($iterator < UserLanguageForm::MAX_LANGUAGES_AVAILABLE) {
            $model = new UserLanguageForm();

            $output = [
                'html' => $this->renderAjax('_language', [
                    'model' => $model,
                    'index' => (int)$iterator,
                    'isNew' => true
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('app', "You can't set more than {count} languages", ['count' => UserLanguageForm::MAX_LANGUAGES_AVAILABLE])
        ];
    }

    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicSkill($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($iterator < 10) {
            $model = new UserSkillForm();

            $output = [
                'html' => $this->renderAjax('_skill', [
                    'model' => $model,
                    'index' => (int)$iterator,
                    'isNew' => true
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('app', "You can't set more than {count} skills", ['count' => 10])
        ];
    }


    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicSpecialty($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($iterator < 10) {
            $model = new UserSpecialtyForm();

            $output = [
                'html' => $this->renderAjax('_specialty', [
                    'model' => $model,
                    'index' => (int)$iterator,
                    'isNew' => true
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('app', "You can't set more than {count} specialties", ['count' => 10])
        ];
    }

    /**
     * @param integer $iterator
     * @return array
     */
    public function actionRenderPublicPortfolio($iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($iterator < 10) {
            $model = new UserPortfolio();

            $output = [
                'html' => $this->renderAjax('_portfolio', [
                    'model' => $model,
                    'index' => (int)$iterator,
                    'isNew' => true
                ]),
                'success' => true
            ];

            return $output;
        }

        return [
            'success' => false,
            'message' => Yii::t('app', "You can't set more than {count} portfolio", ['count' => 10])
        ];
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdateComment($id)
    {
        $user = $this->findModel($id);

        if ($user->load(Yii::$app->request->post())) {
            return $user->save();
        }

        $this->layout = false;
        return $this->renderAjax('_user_comment', [
            'user' => $user
        ]);
    }
}
