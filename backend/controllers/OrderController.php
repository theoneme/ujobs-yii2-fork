<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 17:04
 */

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\Notification;
use common\modules\store\models\backend\ChangeSellerForm;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use common\modules\store\models\search\OrderSearch;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class OrderController
 * @package backend\controllers
 */
class OrderController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['merge'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch(['product_type' => [Order::TYPE_JOB, Order::TYPE_JOB_PACKAGE, Order::TYPE_OFFER, Order::TYPE_DOCUMENT]]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionTender()
    {
        $searchModel = new OrderSearch(['product_type' => [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER]]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-tender', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionProduct()
    {
        $searchModel = new OrderSearch(['product_type' => [Order::TYPE_PRODUCT]]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionProductTender()
    {
        $searchModel = new OrderSearch(['product_type' => [Order::TYPE_PRODUCT_TENDER]]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /* @var Order $order */
        $order = Order::find()->joinWith(['history'])->where(['order.id' => $id])->one();
        $template = 'view';
        if ($order != null) {
            switch ($order->product_type) {
                case Order::TYPE_SIMPLE_TENDER:
                case Order::TYPE_TENDER:
                    $template = 'view-tender';
                    break;
                case Order::TYPE_OFFER:
                case Order::TYPE_JOB:
                case Order::TYPE_JOB_PACKAGE:
                case Order::TYPE_DOCUMENT:
                    $template = 'view';
                    break;
                case Order::TYPE_PRODUCT:
                    $template = 'view-product';
                    break;
            }

            $changeSellerForm = new ChangeSellerForm(['order_id' => $order->id]);

            if (Yii::$app->request->isAjax) {
                Yii::$app->assetManager->bundles = [
                    'yii\bootstrap\BootstrapPluginAsset' => false,
                    'yii\bootstrap\BootstrapAsset' => false,
                    'yii\web\JqueryAsset' => false,
                ];
                $this->layout = false;

                return $this->renderAjax($template, [
                    'order' => $order,
                    'historyForm' => $this->renderHistoryForm($order),
                    'changeSellerForm' => $changeSellerForm,
                ]);
            } else {
                return $this->render($template, [
                    'order' => $order,
                    'historyForm' => $this->renderHistoryForm($order),
                    'changeSellerForm' => $changeSellerForm,
                ]);
            }
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @param Order $order
     * @return null
     */
    protected function renderHistoryForm(Order $order)
    {
        $output = null;

        return $output;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *
     */
    public function actionCancel()
    {
        $orderId = (int)Yii::$app->request->post('order_id', null);

        $order = Order::findOne($orderId);
        $order->status = Order::STATUS_CANCELLED;
        if ($order->save()) {
            $orderHistory = new OrderHistory();
            $orderHistory->type = OrderHistory::TYPE_MANUAL_CANCEL;
            $orderHistory->seller_id = $order->seller_id;
            $orderHistory->customer_id = $order->customer_id;
            $orderHistory->order_id = $order->id;
            $orderHistory->sender_id = $order->seller_id;
            $orderHistory->save();

            $notification = new Notification();
            $notification->to_id = $order->customer_id;
            $notification->subject = Notification::SUBJECT_ORDER_UPDATED;
            $notification->template = Notification::TEMPLATE_ORDER_CANCELLED;
            $notification->custom_data = json_encode([
                'number' => $order->id
            ]);
            $notification->thumb = $order->product ? $order->product->getThumb('catalog') : null;
            $notification->linkRoute = ['/account/order/history', 'id' => $order->id];
            $notification->sender = Notification::SENDER_SYSTEM;
            $notification->withEmail = true;
            $notification->save();
        }
    }

    /**
     *
     */
    public function actionJustice()
    {
        $orderId = (int)Yii::$app->request->post('order_id', null);

        $order = Order::findOne($orderId);
        $order->status = Order::STATUS_CANCELLED;
        if ($order->save()) {
            $orderHistory = new OrderHistory();
            $orderHistory->type = OrderHistory::TYPE_MANUAL_CANCEL;
            $orderHistory->seller_id = $order->seller_id;
            $orderHistory->customer_id = $order->customer_id;
            $orderHistory->order_id = $order->id;
            $orderHistory->sender_id = $order->seller_id;
            $orderHistory->save();
        }
    }

    /**
     *
     */
    public function actionComplete()
    {
        $orderId = (int)Yii::$app->request->post('order_id', null);

        $order = Order::findOne($orderId);
        $order->status = Order::STATUS_COMPLETED;
        if ($order->save()) {
            $orderHistory = new OrderHistory();
            $orderHistory->type = OrderHistory::TYPE_MANUAL_COMPLETE;
            $orderHistory->seller_id = $order->seller_id;
            $orderHistory->customer_id = $order->customer_id;
            $orderHistory->order_id = $order->id;
            $orderHistory->sender_id = $order->seller_id;
            $orderHistory->save();

            $notification = new Notification();
            $notification->to_id = $order->seller_id;
            $notification->subject = Notification::SUBJECT_ORDER_UPDATED;
            $notification->template = Notification::TEMPLATE_ORDER_AUTO_ACCEPTED;
            $notification->custom_data = json_encode([
                'number' => $order->id
            ]);
            $notification->thumb = $order->product ? $order->product->getThumb('catalog') : null;
            $notification->linkRoute = ['/account/order/history', 'id' => $order->id];
            $notification->sender = Notification::SENDER_SYSTEM;
            $notification->withEmail = true;
            $notification->save();
        }
    }

    /**
     * @return array
     */
    public function actionMerge()
    {
        $model = new ChangeSellerForm();

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $model->load($input);

            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    return ['success' => true];
                }
            }
        }

        return ['success' => false];
    }
}