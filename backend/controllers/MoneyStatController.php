<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.01.2017
 * Time: 15:25
 */

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\MoneyStat;
use common\models\Payment;
use common\models\UserWithdrawal;
use common\modules\store\models\Order;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class MoneyStatController
 * @package backend\controllers
 */
class MoneyStatController extends BackEndController
{
    public function actionIndex()
    {
        $data = [];
        $now = new \DateTime('now');
        $month = Yii::$app->request->get('month') ? Yii::$app->request->get('month') : $now->format('m');
        $year = Yii::$app->request->get('year') ? Yii::$app->request->get('year') : $now->format('Y');
        $day = ((Yii::$app->request->get('month') == $now->format('m') && Yii::$app->request->get('year') == $now->format('Y')) || Yii::$app->request->get('month') == null) ? $now->format('d') : cal_days_in_month(CAL_GREGORIAN, $month, $year);;

        $paidServices = $paymentServices = $paidTariffs = $paymentTariffs = $withdrawals = $comission = $activeCount = $activeSum = $advertSum = $paidTenders = $paymentsTenders = $paymentsReplenishments = $depositsSum = 0;

        for ($i = 1; $i < $day + 1; $i++) {
            $item = [
                'id' => $i,
                'paidServices' => Payment::find()
                    ->joinWith(['orders'])
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                    ->andWhere(['type' => Payment::TYPE_JOB_PAYMENT])
                    ->andWhere(['payment.status' => Payment::STATUS_PAID])
                    ->count('payment.id'),
                'paymentsServices' => Payment::find()
                    ->joinWith(['orders'])
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                    ->andWhere(['type' => Payment::TYPE_JOB_PAYMENT])
                    ->andWhere(['payment.status' => Payment::STATUS_PAID])
                    ->sum('payment.total'),
                'paidTenders' => Payment::find()
                    ->joinWith(['orders'])
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                    ->andWhere(['type' => Payment::TYPE_TENDER_PAYMENT])
                    ->andWhere(['payment.status' => Payment::STATUS_PAID])
                    ->count('payment.id'),
                'paymentsTenders' => Payment::find()
                    ->joinWith(['orders'])
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                    ->andWhere(['type' => Payment::TYPE_TENDER_PAYMENT])
                    ->andWhere(['payment.status' => Payment::STATUS_PAID])
                    ->sum('payment.total'),
                'paidTariffs' => Payment::find()
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['type' => Payment::TYPE_ACCESS_PAYMENT])
                    ->andWhere(['status' => Payment::STATUS_PAID])
                    ->count('id'),
                'paymentsTariffs' => Payment::find()
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['type' => Payment::TYPE_ACCESS_PAYMENT])
                    ->andWhere(['status' => Payment::STATUS_PAID])
                    ->sum('total'),
                'paymentsReplenishments' => Payment::find()
                    ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['type' => Payment::TYPE_ACCOUNT_REPLENISHMENT])
                    ->andWhere(['status' => Payment::STATUS_PAID])
                    ->sum('total'),
                'depositsSum' => (int)Payment::find()
                        ->joinWith(['orders'])
                        ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                        ->andWhere(['type' => Payment::TYPE_ACCOUNT_REPLENISHMENT])
                        ->andWhere(['payment.status' => Payment::STATUS_PAID])
                        ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                        ->sum('payment.total') + (int)Payment::find()
                        ->joinWith(['orders'])
                        ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                        ->andWhere(['type' => Payment::TYPE_JOB_PAYMENT])
                        ->andWhere(['payment.status' => Payment::STATUS_PAID])
                        ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                        ->sum('payment.total') + (int)Payment::find()
                        ->joinWith(['orders'])
                        ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                        ->andWhere(['type' => Payment::TYPE_TENDER_PAYMENT])
                        ->andWhere(['payment.status' => Payment::STATUS_PAID])
                        ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                        ->sum('payment.total'),
                'withdrawals' => UserWithdrawal::find()
                    ->where(['between', 'created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                    ->andWhere(['status' => UserWithdrawal::STATUS_COMPLETED])
                    ->sum('amount'),
                'comission' => Payment::find()
                        ->joinWith(['orders'])
                        ->where(['between', 'payment.created_at', strtotime("{$i}-{$month}-{$year} 00:00:00"), strtotime("{$i}-{$month}-{$year} 23:59:59")])
                        ->andWhere(['not', ['order.status' => Order::STATUS_CANCELLED]])
                        ->andWhere(['not', ['type' => Payment::TYPE_ACCESS_PAYMENT]])
                        ->andWhere(['payment.status' => Payment::STATUS_PAID])
                        ->sum('payment.total') * 0.2,
            ];

            $advert = MoneyStat::find()->where(['year' => (int)$year, 'month' => (int)$month, 'day' => (int)$i])->one();
            $item['advert'] = $advert ? $advert->advertisement : 0;

            $data[] = $item;
            $activeSum += $item['activeSum'];
            $activeCount += $item['activeCount'];
            $comission += $item['comission'];
            $withdrawals += $item['withdrawals'];
            $paymentTariffs += $item['paymentsTariffs'];
            $paidTariffs += $item['paidTariffs'];
            $paymentServices += $item['paymentsServices'];
            $paidServices += $item['paidServices'];
            $paidTenders += $item['paidTenders'];
            $paymentsTenders += $item['paymentsTenders'];
            $depositsSum += $item['depositsSum'];
            $paymentsReplenishments += $item['paymentsReplenishments'];

            $advertSum += $item['advert'];
        }

        $data[] = [
            'id' => 'Итого',
            'paidServices' => $paidServices,
            'paymentsServices' => $paymentServices,
            'paidTariffs' => $paidTariffs,
            'paymentsTariffs' => $paymentTariffs,
            'withdrawals' => $withdrawals,
            'comission' => $comission,
            'activeCount' => $activeCount,
            'activeSum' => $activeSum,
            'advert' => $advertSum,
            'paidTenders' => $paidTenders,
            'paymentsTenders' => $paymentsTenders,
            'depositsSum' => $depositsSum,
            'paymentsReplenishments' => $paymentsReplenishments
        ];

        $data = array_reverse($data);

        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 35,
            ],
        ]);

        return $this->render('index', [
            'provider' => $provider,
            'month' => $month,
            'year' => $year
        ]);
    }

    public function actionRekl($year = null, $month = null, $day = null)
    {
        $model = MoneyStat::find()->where(['year' => $year, 'month' => $month, 'day' => $day])->one();

        if (!$model) {
            $model = new MoneyStat();
            $model->year = $year;
            $model->month = $month;
            $model->day = $day;
        }

        $input = Yii::$app->request->post();
        if ($input) {
            $model->load($input);

            $isValid = $model->save();
            if ($isValid) {
                $isSaved = $model->save();
                if (Yii::$app->request->isAjax) {
                    return $isSaved;
                }
                $this->redirect(array(
                    'index',
                ));
            }
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles = [
                'yii\bootstrap\BootstrapPluginAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\web\JqueryAsset' => false,
            ];
            $this->layout = false;

            return $this->renderAjax('update-advert', [
                'model' => $model
            ]);
        } else {
            return $this->render('update-advert', [
                'model' => $model
            ]);
        }
    }
}
