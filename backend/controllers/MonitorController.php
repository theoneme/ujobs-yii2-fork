<?php

namespace backend\controllers;

use backend\dto\MonitorDTO;
use common\controllers\BackEndController;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;

/**
 * Class MonitorController
 * @package backend\controllers
 */
class MonitorController extends BackEndController
{
    private $config = [
        'amsterdam' => [
            'ip' => '188.166.119.199'
        ],
        'singapore' => [
            'ip' => '128.199.65.62'
        ],
        'new_york' => [
            'ip' => '198.199.71.71'
        ],
        'bangalore' => [
            'ip' => '139.59.83.248'
        ]
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['check-state'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'servers' => $this->config
        ]);
    }

    /**
     * @param $server
     * @return array
     * @throws MethodNotAllowedHttpException
     */
    public function actionCheckState($server)
    {
        if (Yii::$app->request->isAjax === true) {
            if (array_key_exists($server, $this->config)) {
                $db = new yii\db\Connection([
                    'dsn' => "mysql:host={$this->config[$server]['ip']};dbname=ujobs",
                    'username' => Yii::$app->params['replicaLogin'],
                    'password' => Yii::$app->params['replicaPassword'],
                    'charset' => 'utf8',
                ]);
                $db->open();

                $command = $db->createCommand('SHOW SLAVE STATUS');
                $result = $command->queryOne();
                $db->close();

                $dto = new MonitorDTO($result);
                $data = $dto->getData();
                if (!empty($data)) {
                    return ['success' => true, 'data' => $data];
                }

                return ['success' => false, 'message' => "Unable to check state of host {$server}"];
            }
        }

        throw new MethodNotAllowedHttpException('no');
    }
}
