<?php

namespace backend\controllers;

use backend\models\PostPortfolioForm;
use common\controllers\BackEndController;
use common\models\Attachment;
use common\models\Category;
use common\models\search\UserPortfolioSearch;
use common\models\UserPortfolio;
use Yii;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserPortfolioController
 * @package backend\controllers
 */
class UserPortfolioController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-extra'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * Lists all UserPortfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserPortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all UserPortfolio models.
     * @return mixed
     */
    public function actionOld()
    {
        $searchModel = new UserPortfolioSearch(['version' => UserPortfolio::VERSION_OLD]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing UserPortfolio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new PostPortfolioForm();
        $model->loadPortfolio($id);

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => ['job_category', 'product_category']])
                ->all(),
            'id',
            'translation.title',
            function ($value) {
                return $value->type === 'product_category' ? Yii::t('account', 'Product categories') : Yii::t('account', 'Job categories');
            }
        );

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);
            $isSaved = $model->save();

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/user-portfolio/index', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($model->portfolio->version === UserPortfolio::VERSION_NEW ? '_form' : 'mini_form', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
            ]);
        } else {
            return $this->render($model->portfolio->version === UserPortfolio::VERSION_NEW ? '_form' : 'mini_form', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
            ]);
        }
    }

    /**
     * Deletes an existing UserPortfolio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserPortfolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserPortfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPortfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return array
     */
    public function actionRenderExtra()
    {
        Yii::$app->assetManager->bundles = [
            'yii\bootstrap\BootstrapPluginAsset' => false,
            'yii\bootstrap\BootstrapAsset' => false,
            'yii\web\JqueryAsset' => false,
        ];
        return [
            'html' => $this->renderAjax('partial/portfolio-attachment-partial', [
                'model' => new Attachment(),
                'iterator' => Yii::$app->request->post('iterator', 0)
            ]),
            'success' => true
        ];
    }
}
