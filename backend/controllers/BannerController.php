<?php

namespace backend\controllers;

use backend\models\BannerSearch;
use common\controllers\BackEndController;
use common\models\Banner;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();
        $isAjax = Yii::$app->request->isAjax;

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = $model->save();
            return $isAjax ? $isSaved : $this->redirect(Yii::$app->request->referrer);
        }
        if ($isAjax) {
            $this->disableAjaxAssets();
            return $this->renderAjax('_form', ['model' => $model, 'isAjax' => $isAjax]);
        } else {
            return $this->render('create', ['model' => $model, 'isAjax' => $isAjax]);
        }
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $isAjax = Yii::$app->request->isAjax;

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = $model->save();
            return $isAjax ? $isSaved : $this->redirect(Yii::$app->request->referrer);
        }
        if ($isAjax) {
            $this->disableAjaxAssets();
            return $this->renderAjax('_form', ['model' => $model, 'isAjax' => $isAjax]);
        } else {
            return $this->render('update', ['model' => $model, 'isAjax' => $isAjax]);
        }
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
