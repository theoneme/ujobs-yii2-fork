<?php

namespace backend\controllers;

use backend\models\PostJobForm;
use common\controllers\BackEndController;
use common\models\Category;
use common\models\DynamicForm;
use common\models\Job;
use common\models\JobAttribute;
use common\models\JobExtra;
use common\models\JobQa;
use common\models\JobRequirement;
use common\models\JobRequirementStep;
use common\models\search\JobSearch;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeToGroup;
use common\modules\store\models\Currency;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class JobController
 * @package backend\controllers
 */
class JobController extends BackEndController
{
    const CONTENT_ATTRIBUTE_ID = 8;
    const TITLE_ATTRIBUTE_ID = 6;
    const CONTENT_PREV_ATTRIBUTE_ID = 7;

    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobSearch(['auto_translated' => false]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionAuto()
    {
        $searchModel = new JobSearch(['auto_translated' => true]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionDrafts()
    {
        $searchModel = new JobSearch(['status' => Job::STATUS_DRAFT]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Job model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Job model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Job the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Job::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new PostJobForm();
        $model->job = new Job();

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])->all(),
            'id',
            'translation.title'
        );
        $currencies = Currency::getFormattedCurrencies();
        $currencySymbols = Currency::getCurrencySymbols();

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);

            $isSaved = $model->save();

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/user/admin/jobs', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
            ]);
        } else {
            return $this->render('create-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        $model = new PostJobForm();
        $model->loadJob($id);

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'job_category'])->all(),
            'id',
            'translation.title'
        );
        $currencies = Currency::getFormattedCurrencies();
        $currencySymbols = Currency::getCurrencySymbols();

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);

            $isSaved = $model->save();

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/user/admin/jobs', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
            ]);
        } else {
            return $this->render('update-test', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols
            ]);
        }
    }

    /**
     * Deletes an existing Job model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Job::STATUS_DELETED]);
        $model->updateElastic();

        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderFaq($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $action = 'create';

        $model = new JobQa();
        if ($id != null) {
            $model = JobQa::find()->where(['id' => $id])->one();
            $action = "update?id={$model->id}";
        }

        $output = [
            'html' => $this->renderAjax('job-faq-partial', [
                'model' => $model,
                'key' => (int)$id,
                'iterator' => (int)$iterator,
                'action' => $action
            ]),
            'success' => true
        ];

        return $output;
    }

    /**
     * @param null $id
     * @param null $iterator
     * @param null $index
     * @return array
     */
    public function actionRenderRequirement($id = null, $iterator = null, $index = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $action = 'create';

        $model = new JobRequirement();
        if ($id != null) {
            $model = JobRequirement::find()->where(['id' => $id])->one();
            $action = "update?id={$model->id}";
        }

        return [
            'html' => $this->renderAjax('job-requirement-partial', [
                'model' => $model,
                'key' => (int)$id,
                'iterator' => (int)$iterator,
                'action' => $action,
                'index' => $index,
                'createForm' => true
            ]),

            'success' => true
        ];
    }

    /**
     * @param null $id
     * @param null $iterator
     * @param null $index
     * @return array
     */
    public function actionRenderRequirementStep($id = null, $iterator = null, $index = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $action = 'create';

        $model = new JobRequirementStep();
        if ($id != null) {
            $model = JobRequirementStep::find()->where(['id' => $id])->one();
            $action = "update?id={$model->id}";
        }

        return [
            'html' => $this->renderAjax('job-requirement-step-partial', [
                'model' => $model,
                'key' => (int)$id,
                'iterator' => (int)$iterator,
                'action' => $action,
                'index' => $index,
                'createForm' => true
            ]),

            'success' => true
        ];
    }

    public function actionModerateSelected()
    {
        $keys = Yii::$app->request->post('selection');
        $res = Job::updateAll(['status' => Job::STATUS_ACTIVE], ['id' => $keys, 'type' => 'job']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param integer $id
     * @param integer $iterator
     * @return array
     */
    public function actionRenderExtra($id = null, $iterator = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $action = 'create';

        $model = new JobExtra();

        $output = [
            'html' => $this->renderAjax('job-extra-partial', [
                'model' => $model,
                'key' => (int)$id,
                'iterator' => (int)$iterator,
                'action' => $action
            ]),
            'success' => true
        ];

        return $output;
    }

    /**
     * @param $category_id
     * @param $entity
     * @return string
     */
    public function actionRenderAdditional($category_id, $entity)
    {
        $model = new PostJobForm();
        $showAddressField = false;

        /* @var Category $category */
        $category = Category::findOne($category_id);
        if ($category->job_offline || $category->job_office_available) {
            $showAddressField = true;
        }

        $job = Job::findOne($entity);
        $model->attributes = $job->attributes;

        return $this->renderAjax('additional-info', [
            'model' => $model,
            'showAddressField' => $showAddressField
        ]);
    }

    /**
     * @param $category_id
     * @param $entity
     * @return null|string
     */
    public function actionRenderAttributes($category_id, $entity)
    {

        /* @var Category $category */
        $category = Category::findOne($category_id);
        if($category === null  || !$category->isLeaf()) {
            return null;
        }

        $values = JobAttribute::find()->where(['job_id' => $entity])->joinWith(['attr', 'attrValueDescription', 'attrValue'])->all();

        $values = DynamicForm::formatValuesArray($values);
        $attributes = DynamicForm::getAttributesListByCategory($category);
        $model = DynamicForm::initModelFromGroup($attributes, $values);

        return $this->renderAjax('partial/attributes', [
            'model' => $model,
            'attributes' => $attributes
        ]);
    }
}
