<?php

namespace backend\controllers;

use backend\models\PaymentMethodSearch;
use common\controllers\BackEndController;
use common\modules\store\models\PaymentMethod;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class PaymentMethodController
 * @package backend\controllers
 */
class PaymentMethodController extends BackEndController
{
    /**
     * Lists all Job models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentMethodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return bool|string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var $model PaymentMethod*/
        $model = PaymentMethod::find()->where(['payment_method.id' => $id])->joinWith(['translations', 'countries.translation'])->one();
        if ($model === null) {
            throw new NotFoundHttpException('Method not found');
        }
        if ($input = Yii::$app->request->post()) {
            $model->load($input);
            $countries = !empty($input['countries']) ? explode(',', $input['countries']) : [];
            foreach ($countries as $country_id) {
                $model->bind('methodCountries')->attributes = ['country_id' => $country_id];
            }
            foreach ($input['ContentTranslation'] as $key => $value) {
                if (!empty($value['title'])) {
                    $value['entity'] = 'payment_method';
                    $model->bind('translations', (int)$value['id'] ?? null)->attributes = $value;
                }
            }
            $isSaved = $model->save();

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
        }
        $countries = ArrayHelper::map($model->countries, 'id', function($var){return $var->getTitle();});
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'countries' => $countries,
                'isAjax' => true,
            ]);
        } else {
            return $this->render('_form', [
                'model' => $model,
                'countries' => $countries,
            ]);
        }
    }
}
