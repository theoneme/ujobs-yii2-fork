<?php

namespace backend\actions;

use Yii;
use yii\base\DynamicModel;
use yii\helpers\Inflector;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use vova07\imperavi\actions\UploadFileAction as BaseAction;

/**
 * Class UploadFileAction
 * @package backend\actions
 */
class UploadFileAction extends BaseAction
{
    /**
     * @var string Model validator name.
     */
    private $_validator = 'image';

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $file = UploadedFile::getInstanceByName($this->uploadParam);
            $model = new DynamicModel(['file' => $file]);
            $model->addRule('file', $this->_validator, $this->validatorOptions)->validate();

            if ($model->hasErrors()) {
                $result = [
                    'error' => $model->getFirstError('file'),
                ];
            } else {
                if ($this->unique === true && $model->file->extension) {
                    $model->file->name = uniqid() . '.' . $model->file->extension;
                } elseif ($this->translit === true && $model->file->extension) {
                    $model->file->name = Inflector::slug($model->file->baseName) . '.' . $model->file->extension;
                }

                if (file_exists($this->path . $model->file->name) && $this->replace === false) {
                    return [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_FILE_ALREADY_EXIST'),
                    ];
                }

                if ($model->file->saveAs($this->path . $model->file->name)) {
                    Yii::$app->mediaLayer->saveToAws('/uploads/impera/' . $model->file->name, false, false);

                    $result = ['id' => $model->file->name, 'filelink' => Yii::$app->mediaLayer->tryLoadFromAws('/uploads/impera/' .  $model->file->name)];

                    if ($this->uploadOnlyImage !== true) {
                        $result['filename'] = $model->file->name;
                    }
                } else {
                    $result = [
                        'error' => Yii::t('vova07/imperavi', 'ERROR_CAN_NOT_UPLOAD_FILE'),
                    ];
                }
            }

            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }
}
