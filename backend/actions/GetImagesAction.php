<?php

namespace backend\actions;

use Yii;
use yii\helpers\FileHelper;
use yii\web\Response;
use vova07\imperavi\actions\GetImagesAction as BaseAction;

/**
 * Class GetImagesAction
 * @package backend\actions
 */
class GetImagesAction extends BaseAction
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $files = [];

        foreach (FileHelper::findFiles($this->path, $this->options) as $path) {
            $file = basename($path);
//            $url = $this->url . urlencode($file);
            $url = Yii::$app->mediaLayer->tryLoadFromAws('/uploads/impera/' . $file);

            $files[] = [
                'id' => $file,
                'title' => $file,
                'thumb' => $url,
                'image' => $url,
            ];
        }

        return $files;
    }
}
