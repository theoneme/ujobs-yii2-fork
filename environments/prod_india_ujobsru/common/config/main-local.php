<?php
return [
	'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ujobs',
            'username' => 'ujobs',
            'password' => 'd8a9hd(*A(*DH98hAyd90apdokadY231239',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],
		'mailer' => [
            'class' => 'common\components\mailer\Mailer',
			'viewPath' => '@common/mail',
			'useFileTransport' => true,
		],
	],
];