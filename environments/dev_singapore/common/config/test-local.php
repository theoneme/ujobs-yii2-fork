<?php
return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/main.php'),
    require(__DIR__ . '/main-local.php'),
    require(__DIR__ . '/test.php'),
    [
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=127.0.0.1;dbname=ujobs-test',
                'username' => 'root',
                'password' => '23a72b1049bce31f24cd18cf0abdb8f2738047c890874a28',
                'charset' => 'utf8',
            ]
        ],
    ]
);
