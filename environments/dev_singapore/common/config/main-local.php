<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ujobs',
            'username' => 'root',
            'password' => '23a72b1049bce31f24cd18cf0abdb8f2738047c890874a28',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'common\components\mailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];
