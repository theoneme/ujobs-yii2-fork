<?php
return [
    'liveUrl' => 'http://127.0.0.1:3000',
    'chatUrl' => 'http://127.0.0.1:3005',
    'crmUrl' => 'http://ujobs-fork.chat',

    'syncTestResults' => true,
    'syncTestUrl' => 'https://ujobs.me/apientry/tests',

    'googleAPIKey' => 'AIzaSyAJsTRwvjFBZaFNZyrW1Ab9KyW493a7Y_w'
];
