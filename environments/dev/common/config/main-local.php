<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ujobs',
            'username' => 'root',
            'password' => '3c8c911007f47b2bbf64a12abf885365',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'common\components\mailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];
