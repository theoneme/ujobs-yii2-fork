<?php
return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/main.php'),
    require(__DIR__ . '/main-local.php'),
    require(__DIR__ . '/test.php'),
    [
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=localhost;dbname=ujobs-test',
                'username' => 'root',
                'password' => '3c8c911007f47b2bbf64a12abf885365',
                'charset' => 'utf8',
            ]
        ],
    ]
);
