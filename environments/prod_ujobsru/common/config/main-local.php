<?php
return [
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=127.0.0.1;dbname=ujobs',
			'username' => 'root',
			'password' => '9ahCqwFwHbdWWXRUIH!@H#!@)#I_)JDSIUASDIUAS*',
			'charset' => 'utf8',
			'enableSchemaCache' => true,
			'schemaCacheDuration' => 3600,
			'schemaCache' => 'cache',
		],
		'mailer' => [
            'class' => 'common\components\mailer\Mailer',
			'viewPath' => '@common/mail',
			'useFileTransport' => true,
		],
	],
];