<?php
/**
 * The manifest of files that are local to specific environment.
 * This file returns a list of environments that the application
 * may be installed under. The returned data must be in the following
 * format:
 *
 * ```php
 * return [
 *     'environment name' => [
 *         'path' => 'directory storing the local files',
 *         'skipFiles'  => [
 *             // list of files that should only copied once and skipped if they already exist
 *         ],
 *         'setWritable' => [
 *             // list of directories that should be set writable
 *         ],
 *         'setExecutable' => [
 *             // list of files that should be set executable
 *         ],
 *         'setCookieValidationKey' => [
 *             // list of config files that need to be inserted with automatically generated cookie validation keys
 *         ],
 *         'createSymlink' => [
 *             // list of symlinks to be created. Keys are symlinks, and values are the targets.
 *         ],
 *     ],
 * ];
 * ```
 */
return [
    'Development local' => [
        'path' => 'dev',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
            'tests/codeception/bin/yii',
        ],
        'setCookieValidationKey' => [
            'backend/config/main-local.php',
            'api/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Development Amsterdam' => [
        'path' => 'dev_amsterdam',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
            'tests/codeception/bin/yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Development Bangalore' => [
        'path' => 'dev_bangalore',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
            'tests/codeception/bin/yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Development New York' => [
        'path' => 'dev_new_york',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
            'tests/codeception/bin/yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Development Singapore' => [
        'path' => 'dev_singapore',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
            'tests/codeception/bin/yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production NetAngels' => [
        'path' => 'prod',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production Amsterdam' => [
        'path' => 'prod_amsterdam',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production Bangalore' => [
        'path' => 'prod_bangalore',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production New York' => [
        'path' => 'prod_new_york',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production Singapore' => [
        'path' => 'prod_singapore',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production uJobs.ru' => [
        'path' => 'prod_ujobsru',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production India uJobs.ru' => [
        'path' => 'prod_india_ujobsru',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
    'Production Netangels' => [
        'path' => 'prod_netangels',
        'setWritable' => [
            'api/runtime',
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'livechat/web/assets',
            'livechat/runtime'
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'api/config/main-local.php',
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'livechat/config/main-local.php',
        ],
    ],
];
