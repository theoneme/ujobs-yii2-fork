<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '',
            'baseUrl' => '/apientry',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
    ],
];

return $config;