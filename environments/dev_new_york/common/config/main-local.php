<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ujobs',
            'username' => 'root',
            'password' => '45940efabb8249e53184810db311d1ad5dcdac73636eb7a1',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'common\components\mailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];
