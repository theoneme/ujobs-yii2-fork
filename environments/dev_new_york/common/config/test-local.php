<?php
return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/main.php'),
    require(__DIR__ . '/main-local.php'),
    require(__DIR__ . '/test.php'),
    [
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=127.0.0.1;dbname=ujobs-test',
                'username' => 'root',
                'password' => '45940efabb8249e53184810db311d1ad5dcdac73636eb7a1',
                'charset' => 'utf8',
            ]
        ],
    ]
);
