<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.04.2017
 * Time: 13:45
 */

namespace console\controllers;

use common\components\elasticsearch\Command;
use common\models\Category;
use common\models\elastic\CategoryElastic;
use common\models\elastic\JobElastic;
use common\models\elastic\PageElastic;
use common\models\elastic\ProfileElastic;
use common\models\elastic\UserPortfolioElastic;
use common\models\Job;
use common\models\Page;
use common\models\user\Profile;
use common\models\UserPortfolio;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use yii\console\Controller;
use yii\db\ActiveQuery;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\Console;

class ElasticController extends Controller
{
    /**
     * @var Command
     */
    private $_command = null;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($this->_command === null) {
            $connection = ActiveRecord::getDb();
            $this->_command = $connection->createCommand();
        }

        return parent::beforeAction($action);
    }

    /**
     *
     */
    public function actionInitIndex()
    {
        $this->_command->deleteIndex('ujobs');
        $this->_command->createIndex('ujobs', [
            'settings' => [
                'index' => [
                    'refresh_interval' => '30s',
                    'index.translog.flush_threshold_ops' => 1000
                ],
                "persistent" => [
                    "indices.store.throttle.max_bytes_per_sec" => "100mb"
                ],
                "transient" => [
                    "indices.store.throttle.type" => "none"
                ],
                "analysis" => [
                    "filter" => [
                        "ru_stop" => [
                            "type" => "stop",
                            "stopwords" => "_russian_"
                        ],
                        "ru_stemmer" => [
                            "type" => "stemmer",
                            "language" => "russian"
                        ],
                        'snowball' => [
                            'type' => 'snowball',
                            'language' => 'russian'
                        ],
                        'worddelimiter' => [
                            'type' => 'word_delimiter',
                            'split_on_numerics' => false,
                            'preserve_original' => true
                        ],
                        'app_ngram' => [
                            'type' => 'nGram',
                            'min_gram' => 4,
                            'max_gram' => 20,
                            'token_chars' => ['letter', 'digit']
                        ],
                        'edgeNGram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 4,
                            'max_gram' => 20,
                            'token_chars' => ['letter', 'digit']
                        ]
                    ],
                    "analyzer" => [
//                        'app_analyzer' => [
//                            'type' => 'custom',
//                            'tokenizer' => 'nGram',
//                            'filter' => ['app_ngram', 'asciifolding', 'lowercase', 'snowball', 'worddelimiter', 'ru_stop', 'ru_stemmer']
//                        ],
//                        'app_search_analyzer' => [
//                            'type' => 'custom',
//                            'tokenizer' => 'standard',
//                            'filter' => ['app_ngram', 'asciifolding', 'lowercase', 'snowball', 'worddelimiter', 'ru_stop', 'ru_stemmer']
//                        ]
                        "edgeNGram" => [
                            "char_filter" => [
                                "html_strip"
                            ],
                            'type' => 'custom',
                            'tokenizer' => 'whitespace',
                            'filter' => ['edgeNGram', 'lowercase', 'asciifolding', 'ru_stop', 'ru_stemmer', 'snowball']
//                            'filter' => ['edgeNGram', 'asciifolding', 'lowercase', 'snowball', 'ru_stop', 'ru_stemmer']
                        ],
                        "default" => [
                            "char_filter" => [
                                "html_strip"
                            ],
                            'type' => 'custom',
                            'tokenizer' => 'whitespace',
                            'filter' => ['lowercase', 'asciifolding', 'ru_stop', 'ru_stemmer', 'snowball']
//                            'filter' => ['edgeNGram', 'asciifolding', 'lowercase', 'snowball', 'ru_stop', 'ru_stemmer']
                        ]
                    ],
                    "tokenizer" => [
                        'nGram' => [
                            'type' => 'nGram',
                            'min_gram' => 1,
                            'max_gram' => 20,
                            'token_chars' => ['letter', 'digit']
                        ],
                        'edgeNGram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 3,
                            'max_gram' => 20,
                            'token_chars' => ['letter', 'digit']
                        ]
                    ],
                ]
            ],
            'mappings' => array_merge(
                JobElastic::mapping(),
                ProfileElastic::mapping(),
                ProductElastic::mapping(),
                UserPortfolioElastic::mapping(),
                PageElastic::mapping(),
                CategoryElastic::mapping()
            ),
        ]);
//        ProfileElastic::updateMapping();      // Это вариант
//        UserAttributeElastic::updateMapping();// который по идее
//        JobElastic::updateMapping();          // обновляет
//        JobAttributeElastic::updateMapping(); // по отдельности
    }

    /**
     *
     */
    public function actionReIndexAll()
    {
        $this->actionJobIndex();
        $this->actionProductIndex();
        $this->actionProfileIndex();
        $this->actionPortfolioIndex();
        $this->actionPageIndex();
        $this->actionCategoryIndex();
    }

    /**
     * @param array $data
     * @param mixed $item
     * @return array
     */
    private function prepareBulkArray(array $data, $item)
    {
        if ($item != null) {
            $data['body'][] = [
                'index' => [
                    '_id' => $item->getPrimaryKey()
                ]
            ];
            $data['body'][] = $item;
        }

        return $data;
    }

    /**
     * @throws \yii\elasticsearch\Exception
     */
    public function actionCategoryIndex()
    {
        /** @var ActiveQuery $categories */
        $categories = Category::find()->joinWith(['translations'])->orderBy(['category.id' => SORT_ASC])->groupBy('category.id');
        $total = $categories->count();
        echo "Total categories: " . $total . PHP_EOL;
        Console::startProgress(0, $total);
        CategoryElastic::deleteAll();
        CategoryElastic::updateMapping();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $categories->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            foreach ($batch as $k => $category) {
                /* @var Category $category */
                $item = $category->updateElastic(false);
                $data = $this->prepareBulkArray($data, $item);

                $i++;
                Console::updateProgress($i, $total);
            }

            $this->_command->bulk('ujobs', 'category', $data, ['op_type' => 'create']);
            $iterator++;
        }

        Console::endProgress("end" . PHP_EOL);
    }

    /**
     * @throws \yii\elasticsearch\Exception
     */
    public function actionJobIndex()
    {
        $jobs = Job::find()->joinWith(['translations', 'category', 'packages', 'user', 'profile', 'extras', 'extra'])
            ->where(['job.status' => Job::STATUS_ACTIVE])
            ->orderBy(['job.id' => SORT_ASC])
            ->groupBy('job.id');
        $total = $jobs->count();
        echo "Total jobs: " . $total . PHP_EOL;
        Console::startProgress(0, $total);
        JobElastic::deleteAll();
        JobElastic::updateMapping();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $jobs->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            foreach ($batch as $k => $job) {
                /* @var Job $job */
                $item = $job->updateElastic(false);
                $data = $this->prepareBulkArray($data, $item);

                $i++;
                Console::updateProgress($i, $total);
            }

            $this->_command->bulk('ujobs', 'job', $data, ['op_type' => 'create']);
            $iterator++;
        }

        Console::endProgress("end" . PHP_EOL);
    }

    /**
     * @throws \yii\elasticsearch\Exception
     */
    public function actionProductIndex()
    {
        $products = Product::find()->joinWith(['translations', 'user', 'profile'])
            ->where(['product.status' => Product::STATUS_ACTIVE])
            ->orderBy(['product.id' => SORT_ASC])
            ->groupBy('product.id');
        $total = $products->count();
        echo "Total products: " . $total . PHP_EOL;
        Console::startProgress(0, $total);
        ProductElastic::deleteAll();
        ProductElastic::updateMapping();
        //JobElastic::deleteAll();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $products->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            foreach ($batch as $k => $product) {
                /* @var Product $product */
                $item = $product->updateElastic(false);
                $data = $this->prepareBulkArray($data, $item);

                $i++;
                Console::updateProgress($i, $total);
            }

            $this->_command->bulk('ujobs', 'product', $data, ['op_type' => 'create']);
            $iterator++;
        }

        Console::endProgress("end" . PHP_EOL);
    }

    /**
     * @throws \yii\elasticsearch\Exception
     */
    public function actionProfileIndex()
    {
        $users = Profile::find()->orderBy(['user_id' => SORT_ASC])->where(['status' => Profile::STATUS_ACTIVE])->groupBy('profile.user_id');
        $total = $users->count();

        echo "Total users: " . $total . PHP_EOL;
        Console::startProgress(0, $total);
        ProfileElastic::updateMapping();
        //ProfileElastic::deleteAll();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $users->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            foreach ($batch as $k => $profile) {
                /* @var Profile $profile */
                $item = $profile->updateElastic(false);
                $data = $this->prepareBulkArray($data, $item);

                $i++;
                Console::updateProgress($i, $total);
            }

            $this->_command->bulk('ujobs', 'profile', $data, ['op_type' => 'create']);
            $iterator++;
        }

        Console::endProgress("end" . PHP_EOL);
    }

    /**
     *
     */
    public function actionPortfolioIndex()
    {
        $portfolios = UserPortfolio::find()->where(['user_portfolio.status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW])
            ->groupBy('user_portfolio.id')
            ->orderBy('user_portfolio.id');
        $total = $portfolios->count();

        echo "Total portfolios: " . $total . PHP_EOL;
        Console::startProgress(0, $total);
        UserPortfolioElastic::deleteAll();
        UserPortfolioElastic::updateMapping();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $portfolios->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            foreach ($batch as $k => $portfolio) {
                /* @var UserPortfolio $portfolio */
                $item = $portfolio->updateElastic(false);
                $data = $this->prepareBulkArray($data, $item);

                $i++;
                Console::updateProgress($i, $total);
            }

            $this->_command->bulk('ujobs', 'user_portfolio', $data, ['op_type' => 'create']);
            $iterator++;
        }

        Console::endProgress("end" . PHP_EOL);
    }

    /**
     *
     */
    public function actionPageIndex()
    {
        $pages = Page::find()->where(['status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS], 'type' => Page::TYPE_ARTICLE])
            ->groupBy('id')
            ->orderBy('id');
        $total = $pages->count();

        echo "Total pages: " . $total . PHP_EOL;
        Console::startProgress(0, $total);
        PageElastic::deleteAll();
        PageElastic::updateMapping();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $pages->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            foreach ($batch as $k => $page) {
                /* @var Page $page */
                $item = $page->updateElastic(false);
                $data = $this->prepareBulkArray($data, $item);

                $i++;
                Console::updateProgress($i, $total);
            }

            $this->_command->bulk('ujobs', 'page', $data, ['op_type' => 'create']);
            $iterator++;
        }

        Console::endProgress("end" . PHP_EOL);
    }
}