<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 08.11.2016
 * Time: 18:41
 */

namespace console\controllers;

use common\components\CurrencyHelper;
use common\models\Category;
use common\models\Job;
use common\models\JobAttribute;
use common\models\Page;
use common\models\PageAttribute;
use common\models\SeoAdvanced;
use common\models\user\Profile;
use common\models\UserPortfolio;
use common\models\UserPortfolioAttribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\Product;
use common\modules\board\models\ProductAttribute;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;

class SeoGeneratorController extends Controller
{
    public function actionGlobalRun()
    {
        $this->actionGenerateProRootCategoryMeta();
        $this->actionGenerateProCategoryMeta();
        $this->actionGenerateProSubcategoryMeta();
        $this->actionGenerateProOtherSubcategoryMeta();
//        $this->actionGenerateProMeta();

        $this->actionGenerateTenderRootCategoryMeta();
        $this->actionGenerateTenderCategoryMeta();
        $this->actionGenerateTenderSubcategoryMeta();
//        $this->actionGenerateTenderMeta();

        $this->actionGenerateJobRootCategoryMeta();
        $this->actionGenerateJobCategoryMeta();
        $this->actionGenerateJobSubcategoryMeta();
        $this->actionGenerateJobOtherSubcategoryMeta();
//        $this->actionGenerateJobMeta();

        $this->actionGenerateProductRootCategoryMeta();
        $this->actionGenerateProductCategoryMeta();
        $this->actionGenerateProductSubcategoryMeta();
        $this->actionGenerateProductTagMeta();

        $this->actionGeneratePortfolioRootCategoryMeta();
        $this->actionGeneratePortfolioCategoryMeta();
        $this->actionGeneratePortfolioSubcategoryMeta();
        $this->actionGeneratePortfolioTagMeta();

        $this->actionGenerateVideoRootCategoryMeta();
        $this->actionGenerateVideoCategoryMeta();

        $this->actionGenerateSkillMeta();
        $this->actionGenerateTagMeta();
        $this->actionGenerateSpecialtyMeta();
        $this->actionGenerateSpecialtyProMeta();

        $this->actionGenerateStoreRootCategoryMeta();
        $this->actionGenerateStoreCategoryMeta();

        $this->actionGeneratePageRootCategoryMeta();
        $this->actionGeneratePageCategoryMeta();
        $this->actionGeneratePageTagMeta();
    }

    public function actionGenerateProRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countPros = Profile::find()
                ->joinWith(['jobs'])
                ->where([
                    'profile.status' => Profile::STATUS_ACTIVE,
                    'job.type' => Job::TYPE_JOB,
                    'job.status' => Job::STATUS_ACTIVE
                ])
                ->andWhere(['not', ['profile.bio' => '']])
                ->andWhere(['not', ['profile.bio' => null]])
                ->andWhere(['not', ['profile.gravatar_email' => '']])
                ->andWhere(['not', ['profile.gravatar_email' => null]])
                ->groupBy('profile.user_id')
                ->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'pro'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRO_ROOT_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPros,
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 1])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();

            $countPros = Profile::find()
                ->joinWith(['jobs'])
                ->where([
                    'job.category_id' => $allChildrenCategoriesIds,
                    'profile.status' => Profile::STATUS_ACTIVE,
                    'job.type' => Job::TYPE_JOB,
                    'job.status' => Job::STATUS_ACTIVE
                ])
                ->andWhere(['not', ['profile.bio' => '']])
                ->andWhere(['not', ['profile.bio' => null]])
                ->andWhere(['not', ['profile.gravatar_email' => '']])
                ->andWhere(['not', ['profile.gravatar_email' => null]])
                ->groupBy('profile.user_id')
                ->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'pro'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRO_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPros
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 2])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countPros = Profile::find()
                ->joinWith(['jobs'])
                ->where([
                    'profile.status' => Profile::STATUS_ACTIVE,
                    'job.category_id' => $category->id,
                    'job.type' => Job::TYPE_JOB,
                    'job.status' => Job::STATUS_ACTIVE
                ])
                ->andWhere(['not', ['profile.bio' => '']])
                ->andWhere(['not', ['profile.bio' => null]])
                ->andWhere(['not', ['profile.gravatar_email' => '']])
                ->andWhere(['not', ['profile.gravatar_email' => null]])
                ->groupBy('profile.user_id')
                ->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'pro'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRO_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPros
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProOtherSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 2, 'id' => [87, 94, 102, 107, 113, 118, 125, 133, 140, 159,]])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countPros = Profile::find()
                ->joinWith(['jobs'])
                ->where([
                    'profile.status' => Profile::STATUS_ACTIVE,
                    'job.category_id' => $category->id,
                    'job.type' => Job::TYPE_JOB,
                    'job.status' => Job::STATUS_ACTIVE
                ])
                ->andWhere(['not', ['profile.bio' => '']])
                ->andWhere(['not', ['profile.bio' => null]])
                ->andWhere(['not', ['profile.gravatar_email' => '']])
                ->andWhere(['not', ['profile.gravatar_email' => null]])
                ->groupBy('profile.user_id')
                ->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'pro'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRO_OTHER_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPros
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProMeta()
    {
        $profiles = Profile::find()->all();
        Console::startProgress(1, count($profiles));
        foreach ($profiles as $key => $profile) {
            /* @var $profile Profile */
            $name = $profile->getSellerName();
            $jobsCount = Job::find()->where(['status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB, 'user_id' => $profile->user_id])->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'profile', 'entity_content' => $profile->user_id], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRO;
            $advancedSeo->custom_data = json_encode([
                'title' => $name,
                'count' => $jobsCount,
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($profiles));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateTenderRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countJobs = Job::find()->where(['type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])->count();
            $countPros = Profile::find()->joinWith(['jobs'])->where(['profile.status' => Profile::STATUS_ACTIVE, 'job.type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'job.status' => Job::STATUS_ACTIVE])->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_TENDER], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_TENDER_ROOT_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'countPro' => $countPros,
                'minPrice' => 200
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateTenderCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 1])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();

            $countJobs = Job::find()->where(['category_id' => $allChildrenCategoriesIds, 'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])->count();
            $countPros = Profile::find()->joinWith(['jobs'])->where(['job.category_id' => $allChildrenCategoriesIds, 'profile.status' => Profile::STATUS_ACTIVE, 'job.type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'job.status' => Job::STATUS_ACTIVE])->count();
            $cheapestJob = Job::find()
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $allChildrenCategoriesIds, 'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }
            /* @var $advancedSeo SeoAdvanced */
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_TENDER], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_TENDER_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'countPro' => $countPros,
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateTenderSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 2])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {

            $countJobs = Job::find()->where(['category_id' => $category->id, 'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])->count();
            $countPros = Profile::find()->joinWith(['jobs'])->where(['profile.status' => Profile::STATUS_ACTIVE, 'job.category_id' => $category->id, 'job.type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'job.status' => Job::STATUS_ACTIVE])->count();
            $cheapestJob = Job::find()
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $category->id, 'type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_TENDER], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_TENDER_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'minPrice' => $minPrice,
                'countPro' => $countPros
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateJobRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countJobs = Job::find()->where(['type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])->count();
            $countPros = Profile::find()->joinWith(['jobs'])->where(['profile.status' => Profile::STATUS_ACTIVE, 'job.type' => Job::TYPE_JOB])->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_JOB], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_JOB_ROOT_CATEGORY;
            $minPrice = 200;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'countPro' => $countPros,
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateJobCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 1])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();

            $countJobs = Job::find()->where(['category_id' => $allChildrenCategoriesIds, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])->count();
            $countPros = Profile::find()->joinWith(['jobs'])->where(['job.category_id' => $allChildrenCategoriesIds, 'profile.status' => Profile::STATUS_ACTIVE, 'job.type' => Job::TYPE_JOB, 'job.status' => Job::STATUS_ACTIVE])->count();
            $cheapestJob = Job::find()
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $allChildrenCategoriesIds, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_JOB], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_JOB_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'countPro' => $countPros,
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateJobSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 2])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {

            $countJobs = Job::find()->where(['category_id' => $category->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])->count();

            $countPros = Profile::find()->joinWith(['jobs'])->where(['profile.status' => Profile::STATUS_ACTIVE, 'job.category_id' => $category->id, 'job.type' => Job::TYPE_JOB, 'job.status' => Job::STATUS_ACTIVE])->count();
            $cheapestJob = Job::find()
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $category->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_JOB], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_JOB_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'minPrice' => $minPrice,
                'countPro' => $countPros,
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateJobOtherSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => 2, 'id' => [87, 94, 102, 107, 113, 118, 125, 133, 140, 159,]])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countJobs = Job::find()->where(['category_id' => $category->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])->count();
            $countPros = Profile::find()->joinWith(['jobs'])->where(['profile.status' => Profile::STATUS_ACTIVE, 'job.category_id' => $category->id, 'job.type' => Job::TYPE_JOB, 'job.status' => Job::STATUS_ACTIVE])->count();
            $cheapestJob = Job::find()
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $category->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => Job::TYPE_JOB], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_JOB_OTHER_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countJobs,
                'minPrice' => $minPrice,
                'countPro' => $countPros
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProductRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'product_root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countProducts = Product::find()->where(['status' => Product::STATUS_ACTIVE])->count();
            $countSellers = Profile::find()->joinWith(['products'])->where(['profile.status' => Profile::STATUS_ACTIVE, 'product.status' => Product::STATUS_ACTIVE])->count();

            $allChildrenCategoriesIds = $category->children()->select('id')->column();
            $cheapestProduct = Product::find()
                ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $allChildrenCategoriesIds, 'status' => Product::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE product.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestProduct !== null && CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price);
            }

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'product'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRODUCT_ROOT_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countProducts,
                'countSellers' => $countSellers,
                'randomChildrenCategories' => '',
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProductCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'product_category', 'lvl' => 1])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();

            $countProducts = Product::find()->where(['category_id' => $allChildrenCategoriesIds, 'status' => Product::STATUS_ACTIVE])->count();
            $cheapestProduct = Product::find()
                ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $allChildrenCategoriesIds, 'status' => Product::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE product.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestProduct !== null && CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price);
            }

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'product'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRODUCT_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countProducts,
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProductSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'product_category'])->andWhere(['>=', 'lvl', 2])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countProducts = Product::find()->where(['category_id' => $category->id, 'status' => Product::STATUS_ACTIVE])->count();
            $cheapestProduct = Product::find()
                ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = 'RUB'")
                ->where(['category_id' => $category->id, 'status' => Product::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE product.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestProduct !== null && CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'product'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRODUCT_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countProducts,
                'minPrice' => $minPrice,
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateProductTagMeta()
    {
        $tags = ProductAttribute::find()
            ->select('product_attribute.*')
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                    ->select("count('p2.id')")
                    ->from('product p2')
                    ->leftJoin('product_attribute pa2', 'p2.id = pa2.product_id')
                    ->where('pa2.value_alias = product_attribute.value_alias')
                    ->andWhere(['p2.status' => Product::STATUS_ACTIVE])
                    ->andWhere([
                        'p2.type' => 'product',
                        'p2.status' => Product::STATUS_ACTIVE,
                        'pa2.entity_alias' => 'product_tag'
                    ])
                    ->groupBy('pa2.value_alias')
                ]
            )
            ->where(['entity_alias' => 'product_tag'])
            ->groupBy('product_attribute.value_alias')
            ->all();

        Console::startProgress(1, count($tags));
        foreach ($tags as $key => $tag) {
            $count = $tag->relatedCount ? $tag->relatedCount : 0;

            $cheapestJob = Product::find()
                ->joinWith(['extra'])
                ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = 'RUB'")
                ->where(['product_attribute.value_alias' => $tag->value_alias, 'product_attribute.entity_alias' => 'product_tag', 'type' => Product::TYPE_PRODUCT, 'status' => Product::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE product.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'product-tag', 'entity_content' => $tag->value_alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PRODUCT_TAG;
            $advancedSeo->custom_data = json_encode([
                'count' => $count,
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($tags));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePortfolioRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countPortfolios = UserPortfolio::find()->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->count();
            $countPro = UserPortfolio::find()->select('user_id')->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->groupBy('user_id')->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'portfolio'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PORTFOLIO_ROOT_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPortfolios,
                'countPro' => $countPro
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePortfolioCategoryMeta()
    {
        $categories = Category::find()->where(['type' => ['product_category', 'job_category'], 'lvl' => 1])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();

            $countPortfolios = UserPortfolio::find()->where(['or', ['category_id' => $allChildrenCategoriesIds], ['subcategory_id' => $allChildrenCategoriesIds]])->andWhere(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->count();
            $countPro = UserPortfolio::find()->select('user_id')->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->andWhere(['or', ['category_id' => $allChildrenCategoriesIds], ['subcategory_id' => $allChildrenCategoriesIds]])->groupBy('user_id')->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'portfolio'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PORTFOLIO_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPortfolios,
                'countPro' => $countPro
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePortfolioSubcategoryMeta()
    {
        $categories = Category::find()->where(['type' => ['job_category', 'product_category'], 'lvl' => [2, 3]])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countPortfolios = UserPortfolio::find()->where(['or', ['category_id' => $category->id], ['subcategory_id' => $category->id]])->andWhere(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->count();
            $countPro = UserPortfolio::find()->select('user_id')->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->andWhere(['or', ['category_id' => $category->id], ['subcategory_id' => $category->id]])->groupBy('user_id')->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'portfolio'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PORTFOLIO_SUBCATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPortfolios,
                'countPro' => $countPro,
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePortfolioTagMeta()
    {
        $tags = UserPortfolioAttribute::find()
            ->select('user_portfolio_attribute.*')
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                    ->select("count('up2.id')")
                    ->from('user_portfolio up2')
                    ->leftJoin('user_portfolio_attribute upa2', 'up2.id = upa2.user_portfolio_id')
                    ->where('upa2.value_alias = user_portfolio_attribute.value_alias')
                    ->andWhere(['up2.status' => UserPortfolio::STATUS_ACTIVE])
                    ->andWhere([
                        'up2.version' => UserPortfolio::VERSION_NEW,
                        'up2.status' => UserPortfolio::STATUS_ACTIVE,
                        'upa2.entity_alias' => 'portfolio_tag'
                    ])
                    ->groupBy('upa2.value_alias')
                ]
            )
            ->where(['entity_alias' => 'portfolio_tag'])
            ->groupBy('user_portfolio_attribute.value_alias')
            ->all();

        Console::startProgress(1, count($tags));
        foreach ($tags as $key => $tag) {
            $count = $tag->relatedCount ? $tag->relatedCount : 0;
            $countPro = UserPortfolio::find()->select('user_id')->where(['version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->groupBy('user_id')->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'portfolio_tag', 'entity_content' => $tag->value_alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PORTFOLIO_TAG;
            $advancedSeo->custom_data = json_encode([
                'count' => $count,
                'countPro' => $countPro
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($tags));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateVideoRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'video'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_VIDEO_ROOT_CATEGORY;
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateVideoCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category', 'lvl' => [1, 2]])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'video'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_VIDEO_CATEGORY;
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateSkillMeta()
    {
        $skills = AttributeValue::find()
            ->select('mod_attribute_value.*')
            ->joinWith(['translation'])
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                    ->select("count('u2.id')")
                    ->from('user u2')
                    ->leftJoin('user_attribute ua2', 'u2.id = ua2.user_id')
                    ->where('ua2.value_alias = mod_attribute_value.alias')
                    ->andWhere([
                        'ua2.entity_alias' => 'skill'
                    ])
                    ->groupBy('ua2.value_alias')
                ]
            )
            ->andWhere(['attribute_id' => 38])
            ->groupBy('mod_attribute_value.id')
            ->all();

        Console::startProgress(1, count($skills));
        foreach ($skills as $key => $skill) {
            $count = $skill->relatedCount ? $skill->relatedCount : 0;

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'skill', 'entity_content' => $skill->alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_SKILL;
            $advancedSeo->custom_data = json_encode(['count' => $count]);
            $advancedSeo->save();

            Console::updateProgress($key, count($skills));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateTagMeta()
    {
        $tags = JobAttribute::find()
            ->select('job_attribute.*')
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                    ->select("count('j2.id')")
                    ->from('job j2')
                    ->leftJoin('job_attribute ja2', 'j2.id = ja2.job_id')
                    ->where('ja2.value_alias = job_attribute.value_alias')
                    ->andWhere(['j2.status' => Job::STATUS_ACTIVE])
                    ->andWhere([
                        'j2.type' => 'job',
                        'j2.status' => Job::STATUS_ACTIVE,
                        'ja2.entity_alias' => 'tag'
                    ])
                    ->groupBy('ja2.value_alias')
                ]
            )
            ->where(['entity_alias' => 'tag'])
            ->groupBy('job_attribute.value_alias')
            ->all();

        Console::startProgress(1, count($tags));
        foreach ($tags as $key => $tag) {
            $count = $tag->relatedCount ? $tag->relatedCount : 0;

            $cheapestJob = Job::find()
                ->joinWith(['extra'])
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where(['job_attribute.value_alias' => $tag->value_alias, 'job_attribute.entity_alias' => 'tag', 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) ASC'))
                ->one();
            $minPrice = 200;
            if ($cheapestJob !== null && CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price) > 200) {
                $minPrice = CurrencyHelper::convert($cheapestJob->currency_code, 'RUB', $cheapestJob->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'tag', 'entity_content' => $tag->value_alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_TAG;
            $advancedSeo->custom_data = json_encode([
                'count' => $count,
                'minPrice' => $minPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($tags));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateSpecialtyMeta()
    {
        $specialties = AttributeValue::find()
            ->select('mod_attribute_value.*')
            ->joinWith(['translation'])
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                    ->select("count('j2.id')")
                    ->from('job j2')
                    ->leftJoin('job_attribute ja2', 'j2.id = ja2.job_id')
                    ->where('ja2.value_alias = mod_attribute_value.alias')
                    ->andWhere(['j2.status' => Job::STATUS_ACTIVE])
                    ->andWhere([
                        'j2.type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER],
                        'j2.status' => Job::STATUS_ACTIVE,
                        'ja2.attribute_id' => 41
                    ])
                    ->groupBy('ja2.value_alias')
                ]
            )
            ->andWhere(['attribute_id' => 41])
            ->groupBy('mod_attribute_value.id')
            ->all();

        Console::startProgress(1, count($specialties));
        foreach ($specialties as $key => $specialty) {
            if(strlen($specialty->alias) >= 30) continue;
            $count = $specialty->relatedCount ? $specialty->relatedCount : 0;

            $mostExpensiveJob = Job::find()
                ->joinWith(['extra'])
                ->leftJoin('currency_exchange', "code_from = job.currency_code AND code_to = 'RUB'")
                ->where([
                    'job_attribute.value_alias' => $specialty->alias,
                    'job_attribute.attribute_id' => 41,
                    'type' => [Job::TYPE_ADVANCED_TENDER, Job::TYPE_TENDER],
                    'status' => Job::STATUS_ACTIVE
                ])
                ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE job.price * ratio END) DESC'))
                ->one();
            $maxPrice = 10000;
            if ($mostExpensiveJob !== null && CurrencyHelper::convert($mostExpensiveJob->currency_code, 'RUB', $mostExpensiveJob->price) > 10000) {
                $maxPrice = CurrencyHelper::convert($mostExpensiveJob->currency_code, 'RUB', $mostExpensiveJob->price);
            }
            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'specialty', 'entity_content' => $specialty->alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_SPECIALTY;
            $advancedSeo->custom_data = json_encode([
                'count' => $count,
                'maxPrice' => $maxPrice
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($specialties));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateStoreRootCategoryMeta()
    {
        $category = Category::find()->where(['alias' => 'product_root'])->one();
        $stores = Profile::find()->where(['is_store' => true])->all();

        if ($stores !== null) {
            Console::startProgress(0, count($stores));
            $allChildrenCategoriesIds = $category->children()->select('id')->column();

            foreach ($stores as $key => $store) {
                $countProducts = Product::find()->where(['status' => Product::STATUS_ACTIVE, 'user_id' => $store->user_id])->count();

                $cheapestProduct = Product::find()
                    ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = 'RUB'")
                    ->where(['category_id' => $allChildrenCategoriesIds, 'status' => Product::STATUS_ACTIVE, 'user_id' => $store->user_id])
                    ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE product.price * ratio END) ASC'))
                    ->one();
                $minPrice = 200;
                if ($cheapestProduct !== null && CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price) > 200) {
                    $minPrice = CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price);
                }

                $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'store', 'entity_content' => $category->id, 'type' => (string)$store->user_id], false);
                $advancedSeo->template_category = SeoAdvanced::TEMPLATE_STORE_ROOT_CATEGORY;
                $advancedSeo->custom_data = json_encode([
                    'count' => $countProducts,
                    'randomChildrenCategories' => '',
                    'minPrice' => $minPrice,
                    'store' => $store->getStoreName()
                ]);
                $advancedSeo->save();

                Console::updateProgress($key, count($stores));
            }
        }

        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateStoreCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'product_category', 'lvl' => [1, 2, 3]])->all();
        $stores = Profile::find()->where(['is_store' => true])->all();

        if ($stores !== null) {
            Console::startProgress(0, count($stores));

            foreach ($stores as $key => $store) {
                foreach ($categories as $category) {
                    $countProducts = Product::find()->where(['category_id' => $category->id, 'status' => Product::STATUS_ACTIVE, 'user_id' => $store->user_id])->count();

                    $cheapestProduct = Product::find()
                        ->leftJoin('currency_exchange', "code_from = product.currency_code AND code_to = 'RUB'")
                        ->where(['category_id' => $category->id, 'status' => Product::STATUS_ACTIVE, 'user_id' => $store->user_id])
                        ->orderBy(new Expression('(CASE WHEN (ratio IS NULL) THEN price ELSE product.price * ratio END) ASC'))
                        ->one();
                    $minPrice = 200;
                    if ($cheapestProduct !== null && CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price) > 200) {
                        $minPrice = CurrencyHelper::convert($cheapestProduct->currency_code, 'RUB', $cheapestProduct->price);
                    }

                    $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'store', 'entity_content' => $category->id, 'type' => (string)$store->user_id], false);
                    $advancedSeo->template_category = SeoAdvanced::TEMPLATE_STORE_CATEGORY;
                    $advancedSeo->custom_data = json_encode([
                        'count' => $countProducts,
                        'randomChildrenCategories' => '',
                        'minPrice' => $minPrice,
                        'store' => $store->getStoreName()
                    ]);
                    $advancedSeo->save();

                    Console::updateProgress($key, count($stores));
                }
            }
        }

        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePageRootCategoryMeta()
    {
        $categories = Category::find()->where(['alias' => 'root'])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $countPages = Page::find()->where(['type' => Page::TYPE_ARTICLE, 'status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]])->count();
            $countPro = Page::find()->select('user_id')->where(['type' => Page::TYPE_ARTICLE, 'status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]])->groupBy('user_id')->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'article'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PAGE_ROOT_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPages,
                'countPro' => $countPro,
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePageCategoryMeta()
    {
        $categories = Category::find()->where(['type' => 'job_category'])->andWhere(['>', 'lvl', 0])->all();

        Console::startProgress(1, count($categories));
        foreach ($categories as $key => $category) {
            $categoryIds = array_merge($category->children()->select('id')->column(), [$category->id]);

            $countPortfolios = Page::find()->where(['category_id' => $categoryIds])->andWhere(['type' => Page::TYPE_ARTICLE, 'status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]])->count();
            $countPro = Page::find()->select('user_id')->where(['category_id' => $categoryIds])->andWhere(['type' => Page::TYPE_ARTICLE, 'status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]])->groupBy('user_id')->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'category', 'entity_content' => $category->id, 'type' => 'article'], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PAGE_CATEGORY;
            $advancedSeo->custom_data = json_encode([
                'count' => $countPortfolios,
                'countPro' => $countPro
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($categories));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGeneratePageTagMeta()
    {
        $tags = PageAttribute::find()
            ->select('page_attribute.*')
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                    ->select("count('p2.id')")
                    ->from('page p2')
                    ->leftJoin('page_attribute pa2', 'p2.id = pa2.page_id')
                    ->where('pa2.value_alias = page_attribute.value_alias')
                    ->andWhere(['p2.type' => Page::TYPE_ARTICLE, 'p2.status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]])
                    ->andWhere(['pa2.entity_alias' => 'page_tag'])
                    ->groupBy('pa2.value_alias')
                ]
            )
            ->where(['entity_alias' => 'page_tag'])
            ->groupBy('page_attribute.value_alias')
            ->all();

        Console::startProgress(1, count($tags));
        foreach ($tags as $key => $tag) {
            $count = $tag->relatedCount ? $tag->relatedCount : 0;
            $countPro = Page::find()
                ->select('user_id')
                ->where(['type' => Page::TYPE_ARTICLE, 'status' => [Page::STATUS_ACTIVE, Page::STATUS_ACTIVE_FOR_SUBSCRIBERS]])
                ->andWhere(['exists',
                    PageAttribute::find()
                        ->where('page_attribute.page_id = page.id')
                        ->andWhere(['page_attribute.entity_alias' => 'page_tag'])
                        ->andWhere(['page_attribute.value_alias' => $tag->value_alias])
                ])
                ->groupBy('user_id')
                ->count();

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'page_tag', 'entity_content' => $tag->value_alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_PAGE_TAG;
            $advancedSeo->custom_data = json_encode([
                'count' => $count,
                'countPro' => $countPro
            ]);
            $advancedSeo->save();

            Console::updateProgress($key, count($tags));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionAlmostGlobalRun()
    {
        $this->actionGenerateProRootCategoryMeta();
        $this->actionGenerateProCategoryMeta();
        $this->actionGenerateProSubcategoryMeta();
        $this->actionGenerateProOtherSubcategoryMeta();

        $this->actionGenerateTenderRootCategoryMeta();
        $this->actionGenerateTenderCategoryMeta();
        $this->actionGenerateTenderSubcategoryMeta();

        $this->actionGenerateJobRootCategoryMeta();
        $this->actionGenerateJobCategoryMeta();
        $this->actionGenerateJobSubcategoryMeta();
        $this->actionGenerateJobOtherSubcategoryMeta();

        $this->actionGenerateProductRootCategoryMeta();
        $this->actionGenerateProductCategoryMeta();
        $this->actionGenerateProductSubcategoryMeta();
    }

    public function actionGenerateCategoryMeta()
    {
        $categories = Category::find()->joinWith(['translation'])->where(['type' => 'job_category'])->all();

        foreach ($categories as $category) {
            $count = Job::find()->where(['category_id' => $category->id])->count();

            $title = "{$category->translation->title} |  {$count} Вам нужны услуги, вы легко найдете профессионала с uJobs.me";
            $description = "Большой выбор исполнителей и заказов в категории {$category->translation->title} вы можете найти на сайте и заказать.";
            $keywords = "{$category->translation->title}, заказать {$category->translation->title}, найти {$category->translation->title}, нужно {$category->translation->title}";

            $category->translation->seo_title = $title;
            $category->translation->seo_description = $description;
            $category->translation->seo_keywords = $keywords;
            $category->translation->save();
        }
    }

    public function actionGenerateJobMeta()
    {
        $jobs = Job::find()->joinWith(['translation'])->where(['type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])->all();
        Console::startProgress(1, count($jobs));
        foreach ($jobs as $key => $job) {
            /* @var $job Job */
            $translation = $job->translation;
            $username = $job->user->getSellerName();
            $title = "Заказать «{$translation->title}» по цене {$job->price} р. | Сайт профессионалов uJobs.me";
            $description = "Закажите услугу «{$translation->title}» по цене {$job->price} р. на сайте профессионалов uJobs.me. Исполнитель {$username} ждет ваших заявок!";
            $keywords = "{$translation->title}, услуга {$translation->title}, заказать {$translation->title}";

            $translation->seo_title = $title;
            $translation->seo_description = $description;
            $translation->seo_keywords = $keywords;
            $translation->save();

            Console::updateProgress($key, count($jobs));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateTenderMeta()
    {
        $jobs = Job::find()->joinWith(['translation'])->where(['type' => [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER], 'status' => Job::STATUS_ACTIVE])->all();
        Console::startProgress(1, count($jobs));
        foreach ($jobs as $key => $job) {
            $translation = $job->translation;
            $title = "Заказ «{$translation->title}» по цене {$job->price} р. | Сайт профессионалов uJobs.me";
            $description = "Заказ услуги «{$translation->title}» по цене {$job->price} р. на сайте профессионалов uJobs.me. Выполни задание и заработай!";
            $keywords = "{$translation->title}, заказ {$translation->title}";

            $translation->seo_title = $title;
            $translation->seo_description = $description;
            $translation->seo_keywords = $keywords;
            $translation->save();

            Console::updateProgress($key, count($jobs));
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionGenerateSpecialtyProMeta()
    {
        $specialties = AttributeValue::find()
            ->select('mod_attribute_value.*')
            ->joinWith(['translation'])
            ->addSelect(['relatedCount' => (new \yii\db\Query)
                ->select("count('p2.user_id')")
                ->from('profile p2')
                ->leftJoin('user_attribute ua2', 'p2.user_id = ua2.user_id')
                ->where('ua2.value_alias = mod_attribute_value.alias')
                ->andWhere(['p2.status' => Profile::STATUS_ACTIVE])
                ->andWhere(['not', ['p2.gravatar_email' => null]])
                ->andWhere(['not', ['p2.gravatar_email' => '']])
                ->groupBy('ua2.value_alias')
            ])
            ->andWhere(['attribute_id' => 41])
            ->groupBy('mod_attribute_value.id')
            ->all();

        Console::startProgress(1, count($specialties));
        foreach ($specialties as $key => $specialty) {
            if(strlen($specialty->alias) > 30) continue;
            $count = $specialty->relatedCount ? $specialty->relatedCount : 0;

            $advancedSeo = SeoAdvanced::findOrCreate(['entity' => 'specialty-pro', 'entity_content' => $specialty->alias], false);
            $advancedSeo->template_category = SeoAdvanced::TEMPLATE_SPECIALTY_PRO;
            $advancedSeo->custom_data = json_encode(['count' => $count]);
            $advancedSeo->save();

            Console::updateProgress($key, count($specialties));
        }
        Console::endProgress("end" . PHP_EOL);
    }
}