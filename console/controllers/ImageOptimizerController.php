<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.11.2016
 * Time: 18:51
 */

namespace console\controllers;

use Aws\S3\Exception\S3Exception;
use common\models\Attachment;
use common\models\Job;
use common\models\user\Profile;
use common\models\user\User;
use Yii;
use yii\console\Controller;
use yii\helpers\BaseStringHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\jui\Dialog;

/**
 * Class ImageOptimizerController
 * @package console\controllers
 */
class ImageOptimizerController extends Controller
{
    /**
     * @param int $limit
     */
    public function actionConvertAttachments($limit = 1)
    {
        $s3 = Yii::$app->get('s3');

        /** @var Attachment[] $pngAttachments */
        $pngAttachments = Attachment::find()->where(['entity' => 'job'])->andFilterWhere(['like', 'content', '.png'])->groupBy('attachment.id')->limit($limit)->all();
        $countAttachments = count($pngAttachments);
        $webFolder = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web';

        echo "Total PNGs to convert: {$countAttachments}";
        Console::startProgress(0, $countAttachments);

        foreach ($pngAttachments as $key => $pngAttachment) {
            $pathInfo = pathinfo($pngAttachment->content);
            if (!is_dir("{$webFolder}{$pathInfo['dirname']}")) {
                mkdir("{$webFolder}{$pathInfo['dirname']}", 0777, true);
            }
            $path = "{$webFolder}{$pngAttachment->content}";
            try {
                if ($s3->commands()->exist(ltrim($pngAttachment->content, '/'))->execute()) {
                    $s3->commands()->get(ltrim($pngAttachment->content, '/'))->saveAs($path)->execute();
                    $newPath = str_replace('.png', '.jpg', $path);
                    Image::getImagine()
                        ->open($path)
                        ->save($newPath, ['quality' => 87]);

                    $newFile = str_replace('.png', '.jpg', $pngAttachment->content);
                    Yii::$app->mediaLayer->saveToAws($newFile);
                    unlink($path);
                    Yii::$app->mediaLayer->removeMedia($pngAttachment->content);

                    $pngAttachment->content = $newFile;
                    $pngAttachment->save();
                } else {
                    $pngAttachment->delete();
                }
            } catch (S3Exception $e) {
                $pngAttachment->delete();
            }

            Console::updateProgress($key + 1, $countAttachments);
        }

        Console::endProgress("end" . PHP_EOL);
    }

    /**
     * @param int $limit
     */
    public function actionConvertAvatars($limit = 1)
    {
        $s3 = Yii::$app->get('s3');

        /** @var Profile[] $pngAttachments */
        $pngProfiles = Profile::find()->andFilterWhere(['like', 'gravatar_email', '.png'])->andFilterWhere(['not like', 'gravatar_email', 'http'])->groupBy('profile.user_id')->limit($limit)->all();
        $countPngProfiles = count($pngProfiles);
        $webFolder = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web';

        echo "Total PNGs to convert: {$countPngProfiles}";
        Console::startProgress(0, $countPngProfiles);

        foreach ($pngProfiles as $key => $pngProfile) {
            $pathInfo = pathinfo($pngProfile->gravatar_email);
            if (!is_dir("{$webFolder}{$pathInfo['dirname']}")) {
                mkdir("{$webFolder}{$pathInfo['dirname']}", 0777, true);
            }
            $path = "{$webFolder}{$pngProfile->gravatar_email}";
            try {
                if ($s3->commands()->exist(ltrim($pngProfile->gravatar_email, '/'))->execute()) {
                    $s3->commands()->get(ltrim($pngProfile->gravatar_email, '/'))->saveAs($path)->execute();
                    $newPath = str_replace('.png', '.jpg', $path);

                    Image::getImagine()
                        ->open($path)
                        ->save($newPath, ['quality' => 87]);

                    $newFile = str_replace('.png', '.jpg', $pngProfile->gravatar_email);
                    Yii::$app->mediaLayer->saveToAws($newFile);

                    unlink($path);

                    Yii::$app->mediaLayer->removeMedia($pngProfile->gravatar_email);

                    $pngProfile->updateAttributes(['gravatar_email' => $newFile]);
                } else {

                }
            } catch (S3Exception $e) {

            }

            Console::updateProgress($key + 1, $countPngProfiles);
        }

        Console::endProgress("end" . PHP_EOL);
    }
}