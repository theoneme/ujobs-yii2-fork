<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2017
 * Time: 14:03
 */

namespace console\controllers;

use yii\base\InvalidParamException;
use yii\console\Controller;

/**
 * Class YandexImportController
 * @package console\controllers
 */
class YandexImportController extends Controller
{
    /**
     * @param null $vendor
     * @return mixed
     */
    public function actionLoad($vendor)
    {
        $classPath = "console\\services\\{$vendor}";
        if (!class_exists($classPath)) {
            throw new InvalidParamException('Pezdes2!');
        }

        /* @var mixed $service */
        $service = new $classPath([], []);
        return $service->load();
    }

    /**
     * @param null $vendor
     * @return mixed
     */
    public function actionClear($vendor = null)
    {
        if ($vendor === null) {
            throw new InvalidParamException('Pezdes!');
        }

        $classPath = "console\\services\\{$vendor}";
        if (!class_exists($classPath)) {
            throw new InvalidParamException('Pezdes2!');
        }

        /* @var mixed $service */
        $service = new $classPath([], []);
        return $service->remove();
    }

    /**
     * @return bool|false|string
     */
    private function fileStat()
    {
        $curl = curl_init('https://kupimebel34.ru/image/cache/data/1111/68-600x600.jpg');
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FILETIME, true);

        $result = curl_exec($curl);

        if ($result === false) {
            die (curl_error($curl));
        }

        $timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
        if ($timestamp != -1) { //otherwise unknown
            return date("Y-m-d H:i:s", $timestamp); //etc
        }

        return false;
    }
}