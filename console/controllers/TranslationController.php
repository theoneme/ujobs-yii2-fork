<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.05.2017
 * Time: 17:43
 */

namespace console\controllers;

use common\models\Category;
use common\models\ContentTranslation;
use common\models\Measure;
use common\models\MeasureTranslation;
use common\models\Page;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\services\YandexTranslatorService;
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Writer_Excel2007;
use Yii;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;
use yii\helpers\VarDumper;

/**
 * Class TranslationController
 * @package console\controllers
 */
class TranslationController extends Controller
{
    public function actionTranslationsToFile($filename = 'export.xlsx')
    {
        $supportedLocales = array_diff_key(Yii::$app->params['supportedLocales'], ['en-GB' => 0, 'zh-CN' => 0, 'ka-GE' => 0, 'uk-UA' => 0]);

        $files = [
            'app' => '@frontend',
            'account' => '@frontend',
            'document' => '@frontend',
            'email' => '@frontend',
            'filter' => '@frontend',
            'seo' => '@frontend',
            'model' => '@common',
            'labels' => '@common',
            'order' => '@common',
            'notifications' => '@common',
            'livechat' => '@common/modules/livechat',
            'board' => '@common/modules/board',
            'store' => '@common/modules/store',
            'wizard' => '@common/modules/wizard',
            'messenger' => '@frontend/modules/messenger'
        ];

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Devour");
        $objPHPExcel->getProperties()->setTitle("uJobs Translations");

        $sheetIndex = 1;

        foreach ($supportedLocales as $languageKey => $language) {
            $rowIndex = 1;

            if ($sheetIndex > 1) {
                $activeSheet = $objPHPExcel->createSheet($sheetIndex);
            } else {
                $activeSheet = $objPHPExcel->getActiveSheet();
            }
            $activeSheet->setTitle($language);
            $activeSheet->SetCellValue("A{$rowIndex}", 'Исходник');
            $activeSheet->SetCellValue("B{$rowIndex}", 'Перевод');
            $activeSheet->getColumnDimension('A')->setWidth(50);
            $activeSheet->getColumnDimension('B')->setWidth(50);
            $activeSheet->getStyle("A{$rowIndex}")->applyFromArray([
                'font' => [
                    'bold' => true
                ], 'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => 'aaffff']
                ]]);
            $activeSheet->getStyle("B{$rowIndex}")->applyFromArray([
                'font' => [
                    'bold' => true
                ], 'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => 'aaffff']
                ]]);
            $rowIndex++;
            foreach ($files as $file => $path) {
                $translations = include(Yii::getAlias($path) . "/messages/{$languageKey}/{$file}.php");

                $activeSheet->SetCellValue("A{$rowIndex}", str_repeat('-', 10));
                $activeSheet->SetCellValue("B{$rowIndex}", str_repeat('-', 10));

                $rowIndex++;
                $activeSheet->SetCellValue("A{$rowIndex}", $file);
                $activeSheet->getStyle("A{$rowIndex}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ], 'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'bbbb00']
                    ]]);
                $activeSheet->SetCellValue("B{$rowIndex}", $file);
                $activeSheet->getStyle("B{$rowIndex}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ], 'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'bbbb00']
                    ]]);
                $rowIndex++;
                $activeSheet->SetCellValue("A{$rowIndex}", str_repeat('-', 10));
                $activeSheet->SetCellValue("B{$rowIndex}", str_repeat('-', 10));
                $rowIndex++;

                foreach ($translations as $itemKey => $translation) {
                    $activeSheet->getStyle("A{$rowIndex}")->getAlignment()->setWrapText(true);
                    $activeSheet->getStyle("B{$rowIndex}")->getAlignment()->setWrapText(true);
                    $activeSheet->SetCellValue("A{$rowIndex}", $itemKey);
                    $activeSheet->SetCellValue("B{$rowIndex}", $translation);

                    $rowIndex++;
                }
            }

            $sheetIndex++;
        }

        $rowIndex = 1;

        $activeSheet = $objPHPExcel->createSheet($sheetIndex);
        $activeSheet->setTitle('Категории');
        $activeSheet->SetCellValue("A{$rowIndex}", 'ID');
        $activeSheet->SetCellValue("B{$rowIndex}", 'Уровень');
        $activeSheet->getColumnDimension('A')->setWidth(5);
        $activeSheet->getColumnDimension('B')->setWidth(5);
        $activeSheet->getStyle("A{$rowIndex}")->applyFromArray([
            'font' => [
                'bold' => true
            ], 'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'aaffff']
            ]]);
        $activeSheet->getStyle("B{$rowIndex}")->applyFromArray([
            'font' => [
                'bold' => true
            ], 'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'aaffff']
            ]]);

        $columnName = 'C';
        foreach (Yii::$app->params['languages'] as $key => $language) {
            $activeSheet->SetCellValue("{$columnName}{$rowIndex}", $language);
            $activeSheet->getColumnDimension($columnName)->setWidth(45);
            $activeSheet->getStyle("{$columnName}{$rowIndex}")->applyFromArray([
                'font' => [
                    'bold' => true
                ], 'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => 'aaffff']
                ]]);

            $columnName++;
        }

        $rowIndex++;

        $categoryTypes = [
            'job_category',
            'product_category'
        ];

        foreach($categoryTypes as $categoryType) {
            $categories = Category::find()->joinWith(['translations'])->where(['type' => $categoryType, 'lvl' => 1])->orderBy('lft asc')->all();
            /* @var Category[] $categories */
            foreach ($categories as $category) {
                $activeSheet->SetCellValue("A{$rowIndex}", $category->id);
                $activeSheet->SetCellValue("B{$rowIndex}", $category->lvl);

                $columnName = 'C';
                foreach (Yii::$app->params['languages'] as $key => $language) {
                    $activeSheet->SetCellValue("{$columnName}{$rowIndex}", $category->translations[$key]->title);
                    $columnName++;
                }

                $rowIndex++;

                /* @var Category[] $children */
                foreach ($children = $category->children()->joinWith(['translations'])->orderBy('lft asc')->all() as $child) {
                    $activeSheet->SetCellValue("A{$rowIndex}", $child->id);
                    $activeSheet->SetCellValue("B{$rowIndex}", $child->lvl);

                    $columnName = 'C';
                    foreach (Yii::$app->params['languages'] as $key => $language) {
                        $activeSheet->SetCellValue("{$columnName}{$rowIndex}", $child->translations[$key]->title);
                        $columnName++;
                    }

                    $rowIndex++;
                }

                $activeSheet->SetCellValue("A{$rowIndex}", str_repeat('-', 10));
                $activeSheet->SetCellValue("B{$rowIndex}", str_repeat('-', 10));

                $rowIndex++;
            }
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(Yii::getAlias('@console') . '/export/' . $filename);
    }

    /**
     * @param $alias
     * @param int $limit
     * @param null $localeParam
     * @return bool
     * @throws Exception
     */
    public function actionAttributeValues($limit = 20, $alias = null, $localeParam = null)
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];
        $supportedLocales['uk-UA'] = 'uk';
        if ($localeParam !== null && (!array_key_exists($localeParam, $supportedLocales))) {
            throw new Exception('Данная локаль недоступна');
        }
        $locales = $localeParam === null ? $supportedLocales : [$localeParam => $supportedLocales[$localeParam]];
        /* @var AttributeValue[] $attributes */
        $translator = new YandexTranslatorService();
        $query = AttributeValue::find()
            ->joinWith(['relatedAttribute', 'translations'])
            ->andFilterWhere(['mod_attribute.alias' => $alias])
            ->andWhere(['not', ['mod_attribute.type' => 'numberbox']])
            ->andWhere(['status' => AttributeValue::STATUS_APPROVED])
            ->groupBy('mod_attribute_value.id')
            ->limit($limit);
        if ($localeParam === null) {
            $query->select(['mod_attribute_value.*', 'translationsCount' => 'count(distinct mod_attribute_description.locale)'])
                ->having(['and',
                    ['<', 'translationsCount', count($supportedLocales)],
                    ['>', 'translationsCount', 0],
                ]);
        }
        else {
            $query->andWhere(['not exists', AttributeDescription::find()->from(['mad2' => 'mod_attribute_description'])
                ->andWhere('mad2.entity_content = mod_attribute_value.id')
                ->andWhere(['mad2.entity' => 'attribute-value'])
                ->andWhere(['mad2.locale' => $localeParam])
            ]);
        }
        $attributes = $query->all();

        $count = count($attributes);
        Console::output("{$count} values require translations for attribute {$alias}");
        if ($count === 0) {
            return $count;
        }
        Console::startProgress(0, $count);

        foreach ($attributes as $key => $attribute) {
            $baseLocale = array_key_exists('ru-RU', $attribute->translations) ? 'ru-RU' : array_pop(array_keys($attribute->translations));
            $missingLocales = array_diff_key($locales, $attribute->translations);
            foreach ($missingLocales as $locale => $code) {
                $title = !is_numeric($attribute->translations[$baseLocale]->title)
                    ? $translator->translate($attribute->translations[$baseLocale]->title, $supportedLocales[$baseLocale], $code)
                    : $attribute->translations[$baseLocale]->title;
                (new AttributeDescription([
                    'entity' => 'attribute-value',
                    'entity_content' => $attribute->id,
                    'title' => !empty($title) ? $title : 'PLACEHOLDER',
                    'locale' => $locale
                ]))->save();
            }
            Console::updateProgress($key + 1, $count);
        }

        Console::endProgress();
        return $count;
    }

    /**
     *
     */
    public function actionAttributeValuesAll()
    {
        $limit = 1000;
        $locales = array_diff_key(Yii::$app->params['supportedLocales'], ['ru-RU' => 0]);
        /* @var $attributes Attribute[] */
        $attributes = Attribute::find()
            ->andWhere(['exists', AttributeValue::find()
                ->joinWith(['translations'])
                ->where('mod_attribute_value.attribute_id = mod_attribute.id')
                ->andWhere(['mod_attribute_description.locale' => 'ru-RU'])
                ->andWhere(['not exists', AttributeDescription::find()->from(['mad2' => 'mod_attribute_description'])
                    ->andWhere('mad2.entity_content = mod_attribute_value.id')
                    ->andWhere(['mad2.entity' => 'attribute-value'])
                    ->andWhere(['mad2.locale' => array_keys($locales)])
                ])
                ->andWhere(['status' => AttributeValue::STATUS_APPROVED])
            ])
            ->all();
        foreach ($attributes as $attribute) {
            Yii::error("\n***************** Translating attribute: " . $attribute->alias . " *****************\n");
            $limit -= $this->actionAttributeValues($limit > 100 ? 100 : $limit, $attribute->alias);
            if ($limit <= 0) {
                break;
            }
        }
    }

    /**
     * @param int $limit
     * @param null $localeParam
     * @return bool
     * @throws Exception
     */
    public function actionAttributes($limit = 100, $localeParam = null)
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];
        $supportedLocales['uk-UA'] = 'uk';
        if ($localeParam !== null && (!array_key_exists($localeParam, $supportedLocales))) {
            throw new Exception('Данная локаль недоступна');
        }
        $locales = $localeParam === null ? $supportedLocales : [$localeParam => $supportedLocales[$localeParam]];
        /* @var AttributeValue[] $attributes */
        $translator = new YandexTranslatorService();
        $query = Attribute::find()
            ->joinWith(['translations'])
//            ->andWhere(['mod_attribute_description.locale' => 'ru-RU'])
            ->groupBy('mod_attribute.id')
            ->limit($limit);
        if ($localeParam === null) {
            $query->select(['mod_attribute.*', 'translationsCount' => 'count(distinct mod_attribute_description.locale)'])
                ->having(['and',
                    ['<', 'translationsCount', count($supportedLocales)],
                    ['>', 'translationsCount', 0],
                ]);
        }
        else {
            $query->andWhere(['not exists', AttributeDescription::find()->from(['mad2' => 'mod_attribute_description'])
                ->andWhere('mad2.entity_content = mod_attribute.id')
                ->andWhere(['mad2.entity' => 'attribute'])
                ->andWhere(['mad2.locale' => $localeParam])
            ]);
        }
        $attributes = $query->all();

        $count = count($attributes);
        Console::output("{$count} attributes require translations");
        if ($count === 0) {
            return false;
        }
        Console::startProgress(0, $count);

        foreach ($attributes as $key => $attribute) {
            $baseLocale = array_key_exists('ru-RU', $attribute->translations) ? 'ru-RU' : array_pop(array_keys($attribute->translations));
            $missingLocales = array_diff_key($locales, $attribute->translations);
            foreach ($missingLocales as $locale => $code) {
                $title = $translator->translate($attribute->translations[$baseLocale]->title, $supportedLocales[$baseLocale], $code);
                (new AttributeDescription([
                    'entity' => 'attribute',
                    'entity_content' => $attribute->id,
                    'title' => !empty($title) ? $title : 'PLACEHOLDER',
                    'locale' => $locale
                ]))->save();
            }
            Console::updateProgress($key + 1, $count);
        }

        Console::endProgress();
        return true;
    }

    /**
     * @return int
     */
    public function actionClearAttributeTranslations()
    {
        AttributeDescription::deleteAll(['and', ['not', ['locale' => 'ru-RU']], ['entity' => 'attribute-value']]);
        return AttributeDescription::deleteAll(['and', ['not', ['locale' => 'ru-RU']], ['entity' => 'attribute']]);
    }

    /**
     * @param $locale
     * @throws Exception
     */
    public function actionFiles($locale)
    {
        if (!array_key_exists($locale, Yii::$app->params['supportedLocales']) || $locale === 'ru-RU') {
            throw new Exception('Данная локаль недоступна');
        }
        $translator = new YandexTranslatorService();
        $files = [
            'app' => '@frontend',
            'account' => '@frontend',
            'document' => '@frontend',
            'email' => '@frontend',
            'filter' => '@frontend',
            'seo' => '@frontend',
            'model' => '@common',
            'labels' => '@common',
            'order' => '@common',
            'notifications' => '@common',
            'livechat' => '@common/modules/livechat',
            'board' => '@common/modules/board',
            'store' => '@common/modules/store',
            'wizard' => '@common/modules/wizard',
        ];
        $code = Yii::$app->params['supportedLocales'][$locale];
        if ($code === 'ua') {
            $code = 'uk';
        }
        foreach ($files as $file => $path) {
            $result = [];
            $translations = include(Yii::getAlias($path) . "/messages/ru-RU/{$file}.php");
            foreach ($translations as $english => $russian) {
                if (!empty($russian)) {
                    $result[$english] = $translator->translateWithVariables($russian, 'ru', $code);
                }
            }
            if (!file_exists(Yii::getAlias($path) . "/messages/{$locale}/")) {
                mkdir(Yii::getAlias($path) . "/messages/{$locale}/", 0777, true);
            }
            file_put_contents(Yii::getAlias($path) . "/messages/{$locale}/" . $file . '.php', "<?php \n return " . VarDumper::export($result) . ";");
        }
        var_dump("success");
    }

    /**
     * @param $locale
     * @throws Exception
     */
    public function actionCategories($locale)
    {
        if (!array_key_exists($locale, Yii::$app->params['supportedLocales']) || $locale === 'ru-RU') {
            throw new Exception('Данная локаль недоступна');
        }

        $translator = new YandexTranslatorService();
//        ContentTranslation::deleteAll(['entity' => 'category', 'locale' => $locale]);
        /* @var $translations ContentTranslation[] */
        $translations = ContentTranslation::find()
            ->where(['entity' => 'category', 'locale' => 'ru-RU'])
            ->andWhere(['not exists', ContentTranslation::find()->from(['ct2' => 'content_translation'])
                ->andWhere('ct2.entity_id = content_translation.entity_id')
                ->andWhere(['ct2.entity' => 'category'])
                ->andWhere(['ct2.locale' => $locale])
            ]);
        $code = Yii::$app->params['supportedLocales'][$locale];
        if ($code === 'ua') {
            $code = 'uk';
        }
        foreach ($translations->batch(50) as $batch) {
            $insert = [];
            foreach ($batch as $translation) {
                $insert[] = [
                    'category',
                    $translation->entity_id,
                    $locale,
                    $translation->slug,
                    $translator->translate($translation->title, 'ru', $code),
                ];
            }
            Yii::error("*****************************\n" . Yii::$app->db->createCommand()->batchInsert('content_translation', ['entity', 'entity_id', 'locale', 'slug', 'title'], $insert)->getSql() . "\n*****************************\n");
            Yii::$app->db->createCommand()->batchInsert('content_translation', ['entity', 'entity_id', 'locale', 'slug', 'title'], $insert)->execute();
        }
        var_dump("success");
    }

    /**
     * @param $locale
     * @throws Exception
     */
    public function actionCategoriesUpdate($locale)
    {
        if (!array_key_exists($locale, Yii::$app->params['supportedLocales']) || $locale === 'ru-RU') {
            throw new Exception('Данная локаль недоступна');
        }

        $translator = new YandexTranslatorService();
        /* @var $translations ContentTranslation[] */
        $translations = ContentTranslation::find()
            ->where(['entity' => 'category', 'locale' => 'ru-RU'])
            ->andWhere(['not', ['or', ['content_prev' => null], ['content_prev' => '']]])
            ->andWhere(['exists', ContentTranslation::find()->from(['ct2' => 'content_translation'])
                ->andWhere('ct2.entity_id = content_translation.entity_id')
                ->andWhere(['ct2.entity' => 'category'])
                ->andWhere(['ct2.locale' => $locale])
            ]);
        $code = Yii::$app->params['supportedLocales'][$locale];
        if ($code === 'ua') {
            $code = 'uk';
        }
        foreach ($translations->batch(50) as $batch) {
            foreach ($batch as $translation) {
                $otherTranslation = ContentTranslation::find()
                    ->andWhere(['entity_id' => $translation->entity_id])
                    ->andWhere(['entity' => 'category'])
                    ->andWhere(['locale' => $locale])->one();
                if ($otherTranslation !== null) {
                    $otherTranslation->updateAttributes(['content_prev' => $translator->translate($translation->content_prev, 'ru', $code)]);
                }
            }
        }
        var_dump("success");
    }

    /**
     *
     */
    public function actionPage($alias)
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];
        $supportedLocales['uk-UA'] = 'uk';
        /** @var Page $page */
        $page = Page::find()->joinWith(['translations'])
            ->select(['page.*', 'translationsCount' => 'count(distinct content_translation.locale)'])
            ->where(['alias' => $alias])
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])
            ->groupBy(['page.id'])
            ->one();
        if ($page === null) {
            throw new Exception("Page not found or already translated");
        }
        $translations = $page->translations;
        $locales = array_diff_key($supportedLocales, $translations);
        $baseLocale = array_key_exists('ru-RU', $translations) ? 'ru-RU' : array_pop(array_keys($translations));
        $baseTranslation = $translations[$baseLocale];
        $translator = new YandexTranslatorService();
        Console::output("Translating page '{$baseTranslation->title}'");
        $toTranslate = [
            'title',
            'content_prev',
            'content',
            'seo_title',
            'seo_description',
            'seo_keywords',
        ];
        foreach ($locales as $locale => $code) {
            Console::output("...to locale '{$locale}'");
            $newTranslation = new ContentTranslation([
                'entity' => $baseTranslation->entity,
                'entity_id' => $baseTranslation->entity_id,
                'locale' => $locale
            ]);
            foreach ($toTranslate as $field) {
                Console::output("......field '{$field}'");
                $translated = $translator->translate($baseTranslation->{$field}, $supportedLocales[$baseLocale], $code);
                if ($translated) {
                    $newTranslation->{$field} = $translated;
                    Console::output(".........success");
                } else {
                    Console::output(".........fail");
                }
            }
            if ($newTranslation->save()) {
                Console::output("............saved");
            } else {
                Console::output("............not saved");
            }
        }
    }

    /**
     * @return bool
     */
    public function actionMeasures()
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];
        $supportedLocales['uk-UA'] = 'uk';
        $locales = $supportedLocales;
        /* @var Measure[] $measures */
        $translator = new YandexTranslatorService();
        $measures = Measure::find()
            ->select(['measure.*', 'translationsCount' => 'count(distinct measure_translation.locale)'])
            ->joinWith(['translations'])
            ->groupBy('measure.id')
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])->all();

        $count = count($measures);
        Console::output("{$count} measures require translations");
        if ($count === 0) {
            return false;
        }
        Console::startProgress(0, $count);

        foreach ($measures as $key => $measure) {
            $baseLocale = array_key_exists('ru-RU', $measure->translations) ? 'ru-RU' : array_pop(array_keys($measure->translations));
            $missingLocales = array_diff_key($locales, $measure->translations);
            foreach ($missingLocales as $locale => $code) {
                $title = $translator->translate($measure->translations[$baseLocale]->title, $supportedLocales[$baseLocale], $code);
                (new MeasureTranslation([
                    'measure_id' => $measure->id,
                    'title' => $title,
                    'locale' => $locale
                ]))->save();
            }
            Console::updateProgress($key + 1, $count);
        }

        Console::endProgress();
        return true;
    }

    /**
     * @param null $localeParam
     * @throws Exception
     */
    public function actionMissing($localeParam = null)
    {
        $supportedLocales = array_diff_key(Yii::$app->params['supportedLocales'], ['ru-RU' => 0, 'en-GB' => 0]);
        if ($localeParam !== null && (!array_key_exists($localeParam, $supportedLocales))) {
            throw new Exception('Данная локаль недоступна');
        }
        $locales = $localeParam === null ? $supportedLocales : [$localeParam => $supportedLocales[$localeParam]];
        if (isset($locales['uk-UA'])) {
            $locales['uk-UA'] = 'uk';
        }
        $translator = new YandexTranslatorService();
        $files = [
            'app' => '@frontend',
            'account' => '@frontend',
            'document' => '@frontend',
            'email' => '@frontend',
            'filter' => '@frontend',
            'seo' => '@frontend',
            'model' => '@common',
            'labels' => '@common',
            'order' => '@common',
            'notifications' => '@common',
            'livechat' => '@common/modules/livechat',
            'board' => '@common/modules/board',
            'store' => '@common/modules/store',
            'wizard' => '@common/modules/wizard',
            'messenger' => '@frontend/modules/messenger'
        ];
        foreach ($files as $file => $path) {
            $translations = include(Yii::getAlias($path) . "/messages/ru-RU/{$file}.php");
            foreach ($locales as $locale => $code) {
                $dir = Yii::getAlias($path) . "/messages/{$locale}";
                Yii::$app->utility->makeDir($dir);
                $localeTranslations = file_exists("$dir/{$file}.php") ? include("$dir/{$file}.php") : [];
                $toTranslate = array_diff_key(array_filter($translations), array_filter($localeTranslations));

                if (count($toTranslate)) {
                    Console::output("Translating file ../" . $locale . '/' . $file);
                    Console::startProgress(0, count($toTranslate));
                    $i = 1;
                    foreach ($toTranslate as $english => $russian) {
                        if (!empty($russian)) {
                            $localeTranslations[$english] = $translator->translateWithVariables($russian, 'ru', $code);
                        }
                        Console::updateProgress($i, count($toTranslate));
                        $i++;
                    }
                    file_put_contents(Yii::getAlias($path) . "/messages/{$locale}/" . $file . '.php', "<?php \n return " . VarDumper::export($localeTranslations) . ";");
                    Console::endProgress();
                }
            }
        }
        var_dump("success");
    }
}