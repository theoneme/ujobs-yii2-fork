<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.01.2017
 * Time: 12:05
 */

namespace console\controllers;

use common\models\EmailNotification;
use common\models\Event;
use common\models\Job;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\store\models\Order;
use frontend\modules\account\models\JobSearch;
use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\Console;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class EmailController
 * @package console\controllers
 */
class EmailController extends Controller
{
    public function actionNotifyUsers()
    {
        return false;
        $this->noPhotosOrBio();
    }

    protected function noPhotosOrBio()
    {
        $limit = 50;
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';

        $profiles = User::find()
            ->select('user.*')
            ->addSelect([
                    'jobsCount' => (new Query)
                        ->select("count('job.id')")
                        ->from('job')
                        ->where('job.user_id =  user.id')
                        ->andWhere([
                            'job.type' => Job::TYPE_JOB,
                        ])
                ]
            )
            ->addSelect([
                    'skillsCount' => (new Query)
                        ->select("count('user_skill.id')")
                        ->from('user_skill')
                        ->where('user_skill.user_id =  user.id')
                ]
            )
            ->joinWith(['profile', 'lastNotification'])
            ->where(['or',
                ['profile.bio' => ''],
                ['profile.gravatar_email' => ''],
                ['not exists', (new Query())
                    ->select('job.id')
                    ->from('job')
                    ->where('job.user_id = user.id')
                    ->andWhere(['job.type' => Job::TYPE_JOB])
                ],
                ['exists', (new Query())
                    ->select('j2.id')
                    ->from('job j2')
                    ->where('j2.user_id = user.id')
                    ->andWhere(['not exists', (new Query())
                        ->select('at3.id')
                        ->from('attachment at3')
                        ->where('at3.entity_id = j2.id')
                        ->andWhere(['at3.entity' => Job::TYPE_JOB])
                    ])
                    ->andWhere(['j2.type' => Job::TYPE_JOB])
                ],
                ['not exists', (new Query())
                    ->select('user_skill.id')
                    ->from('user_skill')
                    ->where('user_skill.user_id = user.id')
                ],
            ])
            ->andWhere(['not exists', (new Query())
                ->select('id')
                ->from('email_notification en')
                ->where('to_id = user.id')
                ->andWhere(['>', 'en.created_at', time() - 60 * 60 * 24 * 5])
            ])
            ->andWhere(['in', 'profile.status', [Profile::STATUS_REQUIRES_MODIFICATION]])
            ->andWhere(['profile.email_notifications' => true])
            ->andWhere(['not', ['user.email' => '']])
            ->andWhere(['user.id' => 2245])
            ->orderBy('created_at asc')
            ->all();

        $totalProfiles = count($profiles);
        echo "Total notifications to be sent: " . $totalProfiles . PHP_EOL;
        Console::startProgress(0, $limit);

        $time = time();
        $hasPhoto = true;
        $hasBio = true;
        $hasBioAndPhoto = true;
        $hasJobsWithoutPhotos = true;
        $hasJobs = true;
        $hasSkills = true;

        foreach ($profiles as $k => $profile) {
            $problems = '';
            if (!filter_var($profile->email, FILTER_VALIDATE_EMAIL)) {
                Console::updateProgress($k, $limit);
                continue;
            }
            //echo $profile->id . PHP_EOL;
            //if ($profile->id != 83) break;
            /*if ($profile->lastNotification) {
                if ($time - $profile->lastNotification->created_at < (60 * 60 * 24 * 4)) {
                    continue;
                }
            }*/
            if ($profile->profile->bio == '') {
                $hasBio = false;
            }

            if ($profile->profile->gravatar_email == '') {
                $hasPhoto = false;
            }

            if ($profile->profile->gravatar_email == '' && $profile->profile->bio == '') {
                $hasBioAndPhoto = false;
            }

            if ($profile->jobsCount == 0) {
                $hasJobs = false;
            }

            if ($profile->skillsCount == 0) {
                $hasSkills = false;
            }

            if ($jobsWithoutAttachments = Job::find()->where([
                'user_id' => $profile->id,
                'type' => Job::TYPE_JOB
            ])->andWhere(['not exists', (new Query())
                ->select('at3.id')
                ->from('attachment at3')
                ->where('at3.entity_id = job.id')
                ->andWhere(['at3.entity' => Job::TYPE_JOB])
            ])->all()
            ) {
                $hasJobsWithoutPhotos = false;
            }

            if ($hasBioAndPhoto == false) {
                $problems .= $this->renderPartial('@frontend/views/email/problem-items/common', [
                    'text' => Yii::t('notifications', 'It seems your photo and profile description are not set. Please fix this!'),
                ]);

                if ($hasSkills == false) {
                    $problems .= $this->renderPartial('@frontend/views/email/problem-items/common', [
                        'text' => Yii::t('notifications', 'You have not set your skills for your profile. You can do it now!'),
                    ]);
                }

                /*$problems .= $this->renderPartial('@frontend/views/email/problem-items/link-button', [
                    'link' => Html::a(Yii::t('notifications', 'Visit profile settings'), Url::to(['/account/job/all'], true), [
                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                        'target' => '_blank'
                    ]),
                ]);*/

                //$problems .= $this->renderPartial('@frontend/views/email/problem-items/separator');
            } elseif ($hasBio == false || $hasPhoto == false || $hasSkills == false) {
                if ($hasPhoto == false) {
                    $problems .= $this->renderPartial('@frontend/views/email/problem-items/common', [
                        'text' => Yii::t('notifications', 'It seems your profile photo is not set. Please fix this!')
                    ]);
                }
                if ($hasBio == false) {
                    $problems .= $this->renderPartial('@frontend/views/email/problem-items/common', [
                        'text' => Yii::t('notifications', 'It seems your profile description is not set. Please fix this!')
                    ]);
                }
                if ($hasSkills == false) {
                    $problems .= $this->renderPartial('@frontend/views/email/problem-items/common', [
                        'text' => Yii::t('notifications', 'You have not set your skills for your profile. You can do it now!'),
                    ]);
                }
                /*$problems .= $this->renderPartial('@frontend/views/email/problem-items/link-button', [
                    'link' => Html::a(Yii::t('notifications', 'Visit profile settings'), Url::to(['/account/service/public-profile-settings'], true), [
                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                        'target' => '_blank'
                    ]),
                ]);*/
                //$problems .= $this->renderPartial('@frontend/views/email/problem-items/separator');
            }

            if ($hasJobs == false) {
                $problems .= $this->renderPartial('@frontend/views/email/problem-items/common', [
                    'text' => Yii::t('notifications', 'You still have not added jobs. You can do it now!'),
                    /*'link' => Html::a(Yii::t('notifications', 'Post a job'), Url::to(['/job/create'], true), [
                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                        'target' => '_blank'
                    ]),*/
                ]);
                //$problems .= $this->renderPartial('@frontend/views/email/problem-items/separator');
            }

            if ($hasJobsWithoutPhotos == false) {
                $jobs = Job::find()->joinWith(['translation'])
                    ->where([
                        'user_id' => $profile->id,
                        'type' => Job::TYPE_JOB
                    ])
                    ->andWhere(['not exists', (new Query())
                        ->select('at3.id')
                        ->from('attachment at3')
                        ->where('at3.entity_id = job.id')
                        ->andWhere(['at3.entity' => Job::TYPE_JOB])
                    ])
                    ->all();

                $problems .= $this->renderPartial('@frontend/views/email/problem-items/no-attachments', [
                    'text' => Yii::t('notifications', 'These jobs are missing their images. Please, add them!'),
                    'jobs' => $jobs,
                ]);

                /*$problems .= $this->renderPartial('@frontend/views/email/problem-items/link-button', [
                    'link' => Html::a(Yii::t('notifications', 'Visit my jobs list'), Url::to(['/account/job/all'], true), [
                        'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                        'target' => '_blank'
                    ]),
                ]);*/
            }

            $problems .= $this->renderPartial('@frontend/views/email/problem-items/link-button', [
                'link' => Html::a(Yii::t('notifications', 'Visit profile settings'), Url::to(['/account/job/list', 'status' => JobSearch::STATUS_ALL], true), [
                    'style' => "padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold",
                    'target' => '_blank'
                ]),
            ]);

            $result = $mailer->compose(['html' => 'problems'], ['content' => $problems, 'user' => $profile])
                ->setTo($profile->email)
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'Recommendations about your profile', ['site' => Yii::$app->name]))
                ->send();

            if ($result) {
                $emailNotification = new EmailNotification();
                $emailNotification->to_id = $profile->id;
                $emailNotification->save();
            }

            Console::updateProgress($k, $limit);
            if ($k >= $limit) {
                break;
            }
        }
        Console::endProgress("end" . PHP_EOL);
    }

    public function actionAfterOrder()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $view = 'after-order';
        $time = time();
        $orders = Order::find()
            ->joinWith(['customer'])
            ->joinWith(['customer.profile'])
            ->andWhere(['not', ['user.email' => '']])
            ->andWhere(['profile.email_notifications' => true])
            ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
            ->andWhere(['product_type' => Order::TYPE_JOB_PACKAGE])
            ->andWhere(['order.status' => Order::STATUS_COMPLETED])
            ->andWhere(['between', 'order.updated_at', $time - 60 * 60 * 24 * 14, $time - 60 * 60 * 24 * 7])
            ->andWhere(['not exists',
                Event::find()->where('event.user_id=user.id')
                    ->andWhere(['between', 'created_at', $time - 60 * 60 * 24 * 7, $time])
                    ->andWhere(['code' => Event::AFTER_ORDER_MESSAGE])
            ])
            ->limit(10)
            ->groupBy('customer_id')->all();
        Yii::$app->db->createCommand()->batchInsert(
            'event',
            ['user_id', 'code', 'created_at'],
            array_map(function ($o) use ($time) {
                return [$o->customer_id, Event::AFTER_ORDER_MESSAGE, $time];
            }, $orders)
        )->execute();
        Event::deleteAll(['and',
            ['code' => Event::AFTER_ORDER_MESSAGE],
            ['<', 'created_at', $time - 60 * 60 * 24 * 7 * 2]
        ]);

        foreach ($orders as $order) {
            /* @var $order Order */
            $jobs = Job::find()->where([
                'type' => Job::TYPE_JOB,
                'status' => Job::STATUS_ACTIVE,
                'category_id' => $order->product->job->category_id,
            ])->limit(4)->all();

            if (!empty($jobs)) {
                $mailer->compose(['html' => $view], [
                    'name' => $order->customer->getSellerName(),
                    'jobName' => $order->product->getLabel(),
                    'jobs' => $jobs,
                    'user' => $order->customer
                ])
                    ->setTo($order->customer->email)
                    ->setFrom(Yii::$app->params['ujobsNoty'])
                    ->setSubject(Yii::t('notifications', 'Interesting propositions on {site}', ['site' => Yii::$app->name]))
                    ->send();
            }
        }
    }

    public function actionCartReturn()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $view = 'cart-return';
        $time = time();
        $users = User::find()
            ->joinWith(['profile'])
            ->andWhere(['not', ['user.email' => '']])
            ->andWhere(['profile.email_notifications' => true])
//            ->andWhere(['profile.status' => Profile::STATUS_ACTIVE])
            ->andWhere(['exists', (new Query())
                ->select('sessionId')
                ->from('cart')
                ->where('cart.sessionId=user.id')
                ->andWhere(['between', 'updated_at', $time - 60 * 90, $time - 60 * 30])
            ])
            ->andWhere(['not exists',
                Event::find()->where('event.user_id=user.id')
                    ->andWhere(['between', 'created_at', $time - 60 * 60, $time])
                    ->andWhere(['code' => Event::CART_RETURN_MESSAGE])
            ])
            ->all();
        Yii::$app->db->createCommand()->batchInsert(
            'event',
            ['user_id', 'code', 'created_at'],
            array_map(function ($u) use ($time) {
                return [$u->id, Event::CART_RETURN_MESSAGE, $time];
            }, $users)
        )->execute();
        Event::deleteAll(['and',
            ['code' => Event::CART_RETURN_MESSAGE],
            ['<', 'created_at', $time - 60 * 60 * 24]
        ]);
        foreach ($users as $user) {
            $cart = (new Query())->select('cartData')
                ->from('cart')
                ->where(['sessionId' => $user->id])
                ->createCommand()
                ->queryScalar();
            $cart = unserialize($cart);
            $jobs = implode(',', array_map(function ($item) {
                return '"' . $item['item']->getLabel() . '"';
            }, $cart));
            $mailer->compose(['html' => $view], ['user' => $user, 'jobs' => $jobs])
                ->setTo($user->email)
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'Your cart on site {site}', ['site' => Yii::$app->name]))
                ->send();
        }
    }
}