<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.12.2016
 * Time: 12:16
 */

namespace console\controllers;

use common\models\Category;
use common\models\ContentTranslation;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class ExcelToCategoryController
 * @package console\controllers
 */
class ExcelToCategoryController extends Controller
{
    /**
     *
     */
    public function actionUpdateTranslations()
    {
        $categories = Category::find()->where(['type' => 'job_category'])->joinWith(['translations'])->all();
        foreach ($categories as $category) {
            $translation = $category->translations['ru-RU'];
            $translation->slug = $category->alias;
            $translation->save();
        }

        $firstDataRowIndex = 2;
        $lastDataRowIndex = 140;

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/category_trans.xlsx');
        $objPHPExcel->setActiveSheetIndex(4);

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();

        $cats = [];

        foreach ($rowIterator as $key => $row) {
            if ($key < $firstDataRowIndex) {
                continue;
            }

            if ($key > $lastDataRowIndex) {
                break;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            if (1 == $row->getRowIndex()) {
                continue;
            }

            foreach ($cellIterator as $cell) {
                if ($cell->getColumn() == 'A') {
                    $cats[$key]['id'] = $cell->getCalculatedValue();
                }

//				if ($cell->getColumn() == 'C') {
//					$cats[$key]['t']['ru-RU'] = $cell->getCalculatedValue();
//				}

                if ($cell->getColumn() == 'D') {
                    $cats[$key]['t']['en-GB'] = $cell->getCalculatedValue();
                }

                if ($cell->getColumn() == 'E') {
                    $cats[$key]['t']['es-ES'] = $cell->getCalculatedValue();
                }

                if ($cell->getColumn() == 'F') {
                    $cats[$key]['t']['uk-UA'] = $cell->getCalculatedValue();
                }
            }
        }

        foreach ($cats as $item) {
            if ($key = intval($item['id'])) {
                $t = ContentTranslation::find()->where(['entity' => 'category', 'entity_id' => $key])->andWhere(['not', ['locale' => 'ru-RU']])->all();
                if ($t !== null) {
                    foreach ($t as $i) {
                        $i->delete();
                    }
                }

                foreach ($item['t'] as $translationKey => $translation) {
                    $contentTranslation = new ContentTranslation([
                        'entity' => 'category',
                        'entity_id' => $key,
                        'title' => $translation,
                        'locale' => $translationKey,
                        'slug' => Yii::$app->utility->transliterate($translation)
                    ]);
                    $contentTranslation->save();
                }
            }
        }
    }
}