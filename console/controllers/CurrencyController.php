<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers;

use common\helpers\CurrencyLayerHelper;
use common\modules\store\models\Currency;
use common\modules\store\models\CurrencyExchange;
use Exception;
use Yii;
use yii\console\Controller;
use yii\httpclient\Client;

/**
 * Class CurrencyController
 * @package console\controllers
 */
class CurrencyController extends Controller
{
//    public function actionUpdateCurrencies()
//    {
//        $xml = new \DOMDocument();
//        $url = 'http://www.cbr.ru/scripts/XML_daily.asp';
//
//        if (@$xml->load($url)) {
//            $listOfAvailableCurrencies = Currency::find()->where(['not', ['code' => 'RUB']])->select('code')->column();
//
//            $root = $xml->documentElement;
//            $items = $root->getElementsByTagName('Valute');
//            /* @var \DOMDocument $item */
//            $currencies = ['RUB' => 1];
//            foreach ($items as $item) {
//                $code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;
//                if (in_array($code, $listOfAvailableCurrencies)) {
//
//                    $curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
//                    $nominal = $item->getElementsByTagName('Nominal')->item(0)->nodeValue;
//                    $proportion = floatval(bcdiv(str_replace(',', '.', $curs), $nominal, 4));
//
//                    $currencies[$code] = $proportion;
//                }
//            }
//            $ex_ratio = [];
//            foreach ($currencies as $from => $fratio) {
//                foreach ($currencies as $to => $tratio) {
//                    if ($from === $to) {
//                        continue;
//                    }
//                    $ex_ratio[] = [
//                        'from' => $from,
//                        'to' => $to,
//                        'ratio' => round($fratio / $tratio, 4)
//                    ];
//                }
//            }
//
//            foreach ($ex_ratio as $item) {
//                $exchange = CurrencyExchange::find()->where(['code_from' => $item['from'], 'code_to' => $item['to']])->one();
//                if ($exchange === null) {
//                    $exchange = new CurrencyExchange(['code_from' => $item['from'], 'code_to' => $item['to']]);
//                }
//                $exchange->ratio = $item['ratio'];
//                $exchange->save();
//            }
//            return true;
//        }
//
//        return false;
//    }

//    public function actionUpdateCurrencies()
//    {
//        $currencies = Currency::find()->select('code')->where(['not', ['code' => 'CTY']])->column();
//        $currencyCombinations = [];
//        foreach ($currencies as $from) {
//            foreach ($currencies as $to) {
//                if ($from === $to) {
//                    continue;
//                }
//                $currencyCombinations[] = '"' . $from . $to . '"';
//            }
//        }
//        $query = 'select * from yahoo.finance.xchange where pair in (' . implode(',', $currencyCombinations) . ')';
//
//        $client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
//        $isOk = false;
//        try {
//            $response = $client->get('https://query.yahooapis.com/v1/public/yql', [
//                'q' => $query,
//                'env' => 'store://datatables.org/alltableswithkeys',
//                'format' => 'json'
//            ])->send();
//            $responseData = $response->getData();
//            $isOk = $response->isOk;
//        } catch (Exception $e) {
//            Yii::error($e->getMessage());
//            return false;
//        }
//        if ($isOk && !empty($responseData['query']['results']['rate'])) {
//            foreach ($responseData['query']['results']['rate'] as $result) {
//                list($from, $to) = explode('/', $result['Name']);
//
//                $exchange = CurrencyExchange::findOrCreate(['code_from' => $from, 'code_to' => $to], false);
//                $exchange->ratio = floatval($result['Rate']);
//                $exchange->save();
//            }
//            return true;
//        }
//        Yii::error("\n*************************\n Ошибка при обновлении курсов валют \n*************************\n");
//        return false;
//    }

    public function actionUpdateCurrencies()
    {
        $currencies = Currency::find()->select('code')->where(['not', ['code' => 'CTY']])->column();
        $client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
        $isOk = false;
        try {
            $response = $client->get('http://www.apilayer.net/api/live', [
                'access_key' => CurrencyLayerHelper::$apiKey,
                'currencies' => implode(',', $currencies),
                'format' => 1
            ])->send();
            $responseData = $response->getData();
            $isOk = $response->isOk;
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }
        if ($isOk && $responseData['success'] === true && !empty($responseData['quotes'])) {
            $items = $responseData['quotes'];
            $items["USDUSD"] = 1;
            foreach ($currencies as $from) {
                foreach ($currencies as $to) {
                    if ($from === $to) {
                        continue;
                    }
                    $ratio = round($items['USD' . $to] / $items['USD' . $from], 4);
                    $exchange = CurrencyExchange::find()->where(['code_from' => $from, 'code_to' => $to])->one();
                    if ($exchange === null) {
                        $exchange = new CurrencyExchange(['code_from' => $from, 'code_to' => $to]);
                    }
                    $exchange->ratio = $ratio;
                    $exchange->save();
                }
            }

            return true;
        }
        Yii::error("\n*************************\n Ошибка при обновлении курсов валют \n*************************\n");
        return false;
    }
}