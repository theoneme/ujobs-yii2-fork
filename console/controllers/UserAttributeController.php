<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 08.02.2017
 * Time: 18:03
 */

namespace console\controllers;

use common\models\JobAttribute;
use common\models\UserAttribute;
use common\models\UserLanguage;
use common\models\UserPortfolioAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\attribute\models\backend\MergeAttributeValueForm;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class UserAttributeController
 * @package console\controllers
 */
class UserAttributeController extends Controller
{
    /**
     * @param $alias
     * @param $threshold
     */
    public function actionClearTrash($alias, $threshold)
    {
        $attribute = Attribute::find()->where(['alias' => $alias])->one();

        if ($attribute !== null) {
            $valuesToDelete = AttributeValue::find()
                ->select('id, alias, attribute_id')
                ->addSelect(['productAttrs' => ProductAttribute::find()
                    ->select('count(product_attribute.id)')
                    ->where('product_attribute.value = mod_attribute_value.id'),
                    'jobAttrs' => JobAttribute::find()
                        ->select('count(job_attribute.id)')
                        ->where('job_attribute.value = mod_attribute_value.id'),
                    'userAttrs' => UserAttribute::find()
                        ->select('count(user_attribute.id)')
                        ->where('user_attribute.value = mod_attribute_value.id'),
                    'userPortfolioAttrs' => UserPortfolioAttribute::find()
                        ->select('count(user_portfolio_attribute.id)')
                        ->where('user_portfolio_attribute.value = mod_attribute_value.id'),
                ])
                ->where(['attribute_id' => $attribute->id])
                ->groupBy('alias')
                ->having("(productAttrs + jobAttrs + userAttrs + userPortfolioAttrs) < {$threshold}");
            $countQuery = clone ($valuesToDelete);
            $count = $countQuery->count();
            if ($count === 0) return;

            Console::output("Start clearing {$count} value for attribute {$alias}");
            Console::startProgress(0, $count);

            $iterator = 0;
            foreach ($valuesToDelete->batch(150) as $batch) {
                foreach ($batch as $attributeValue) {
                    /* @var AttributeValue $attributeValue */
                    $object = AttributeValue::findOne(['alias' => $attributeValue->alias, 'attribute_id' => $attributeValue->attribute_id]);
                    $iterator++;
                    $object->delete();
                    Console::updateProgress($iterator, $count);
                }
            }

            $values = AttributeValue::find()->where(['attribute_id' => $attribute->id]);
            $countQuery = clone($values);
            $count = $countQuery->count();
            if ($count === 0) return;

            Console::output("Start merging {$count} values for attribute {$alias}");
            Console::startProgress(0, $count);

            $iterator = 0;
            $control = [];
            foreach ($values->batch(150) as $batch) {
                foreach ($batch as $value) {
                    $iterator++;
                    if (array_key_exists($value->alias, $control)) {
                        $mergeModel = new MergeAttributeValueForm([
                            'attribute_id' => $value->attribute_id,
                            'merge' => true,
                            'attribute_value_id' => $value->id,
                            'target_attribute_value_id' => $control[$value->alias]
                        ]);
                        $mergeModel->save();
                    } else {
                        $control[$value->alias] = $value->id;
                    }
                    Console::updateProgress($iterator, $count);
                }
            }
        }
    }
}