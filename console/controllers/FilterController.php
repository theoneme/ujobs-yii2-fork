<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.11.2016
 * Time: 16:52
 */

namespace console\controllers;

use common\models\Category;
use common\models\JobAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeGroup;
use common\modules\attribute\models\AttributeToGroup;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class FilterController
 * @package console\controllers
 */
class FilterController extends Controller
{
    /**
     * @return bool
     */
    public function actionRenameAttributeGroups()
    {
        /** @var Category[] $categories */
        $categories = Category::find()
            ->where(['not', ['attribute_set_id' => null]])
            ->all();

        $total = count($categories);
        Console::startProgress(0, $total);

        if (count($categories) > 0) {
            foreach ($categories as $key => $category) {
                $attributeGroup = AttributeGroup::find()
                    ->joinWith(['attrs'])
                    ->where(['mod_attribute_group.id' => $category->attribute_set_id])
                    ->one();

                if ($attributeGroup !== null) {
                    $parents = $category->parents()->orderBy('lvl asc')->select('name')->andWhere(['>', 'lvl', 0])->column();
                    $parentsStr = implode(' - ', $parents) . " - {$category->name}";

                    $attributeGroup->updateAttributes(['title' => $parentsStr]);
                }

                Console::updateProgress($key + 1, $total);
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function actionMergeAttributes($level)
    {
        $exceptions = [
            'product_tag',
            'color',
            'material',
            'brand_a',
            'condition'
        ];

        /** @var Category[] $categories */
        $categories = Category::find()
            ->where('rgt-lft > 1')
            ->andWhere(['lvl' => $level])
//            ->andWhere(['not', ['attribute_set_id' => null]])
            ->all();

        $total = count($categories);
//        Console::startProgress(0, $total);

        if (count($categories) > 0) {
            foreach ($categories as $key => $category) {
                $children = $category->children(1)->all();
                $parents = $category->parents()->select('name')->column();
                $metAttrs = [];

                foreach ($children as $childKey => $child) {
                    $attributeGroup = AttributeGroup::find()
                        ->joinWith(['attrs'])
                        ->where(['mod_attribute_group.id' => $child->attribute_set_id])
                        ->one();

                    if ($attributeGroup !== null) {
                        $attrs = $attributeGroup->attrs;

                        $tempAttrs = [];
                        foreach ($attrs as $attr) {
                            $translations = $attr->translations;

                            if ($childKey === 0) {
                                if (!empty($translations) && array_key_exists('ru-RU', $translations) && !in_array($attr->alias, $exceptions)) {
                                    $metAttrs[] = $translations['ru-RU']->title;
                                }
                            } else {
                                if (!empty($translations) && array_key_exists('ru-RU', $translations) && !in_array($attr->alias, $exceptions)) {
                                    $tempAttrs[] = $translations['ru-RU']->title;
                                }
                            }
                        }

                        if ($childKey !== 0) {
                            $metAttrs = array_intersect($metAttrs, $tempAttrs);
                        }
                    }
                }
                if (!empty($metAttrs)) {
                    Console::output(implode(' - ', $parents) . " - {$category->name}, attrs: " . implode('; ', $metAttrs));

                    $setsId = $category->children(1)->select('attribute_set_id')->column();
                    $data = [];

                    foreach ($metAttrs as $metAttr) {
                        $values = [];
                        $sources = Attribute::find()
                            ->joinWith(['translations', 'attributeToGroups', 'values'])
                            ->where(['mod_attribute_description.title' => $metAttr, 'mod_attribute_to_group.attribute_group_id' => $setsId])
                            ->all();

                        foreach ($sources as $source) {
                            foreach ($source->values as $value) {
                                if (array_key_exists('ru-RU', $value->translations)) {
                                    $values[] = $value->translations['ru-RU']->title;
                                }
                            }
                        }
                        $values = array_unique($values);

                        if (!empty($values)) {
                            $attribute = new Attribute([
                                'is_system' => false,
                                'is_searchable' => true,
                                'name' => $metAttr,
                                'type' => 'checkboxlist',
                                'alias' => "{$category->id}_" . Yii::$app->utility->transliterate($metAttr)
                            ]);
                            $attribute->translationsArr['ru-RU']->title = $metAttr;
                            if ($attribute->save() === true) {
                                $data[$attribute->id]['title'] = $metAttr;
                                foreach ($values as $value) {
                                    $attrValue = new AttributeValue([
                                        'attribute_id' => $attribute->id,
                                        'alias' => Yii::$app->utility->transliterate($value),
                                        'status' => AttributeValue::STATUS_APPROVED,
                                        'translationsArr' => [
                                            [
                                                'title' => $value,
                                                'locale' => 'ru-RU'
                                            ],
                                        ]
                                    ]);

                                    if ($attrValue->save() === true) {
                                        $data[$attribute->id]['values'][$attrValue->id] = $value;
                                    } else {
                                        print_r($attrValue);
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($data)) {
                        $group = new AttributeGroup([
                            'translationsArr' => [
                                new AttributeDescription([
                                    'locale' => 'ru-RU',
                                    'title' => implode(' - ', $parents) . " - {$category->name}",
                                ])
                            ],
                            'title' => implode(' - ', $parents) . " - {$category->name}",
                            'is_generated' => true,
                            'sort_order' => 0
                        ]);

                        if ($group->save() === true) {
                            foreach ($data as $dkey => $item) {
                                $attrToGroup = new AttributeToGroup([
                                    'attribute_group_id' => $group->id,
                                    'attribute_id' => $dkey
                                ]);

                                $attrToGroup->save();
                            }

                            $category->updateAttributes(['attribute_set_id' => $group->id]);
                        }

                        $sources = Attribute::find()
                            ->joinWith(['translations', 'attributeToGroups', 'values'])
                            ->where(['mod_attribute_description.title' => $metAttrs, 'mod_attribute_to_group.attribute_group_id' => $setsId])
                            ->all();
                        $attrTitles = array_map(function ($val) {
                            return $val['title'];
                        }, $data);
                        foreach ($sources as $source) {
                            $attr = $source->translations['ru-RU']->title;
                            $newAttrId = array_search($attr, $attrTitles);
                            $a = Attribute::findOne($newAttrId);
                            if ($a !== null) {
                                AttributeToGroup::updateAll([
                                    'attribute_id' => $newAttrId
                                ], [
                                    'attribute_id' => $source->id
                                ]);

                                foreach ($source->values as $value) {
                                    if (array_key_exists('ru-RU', $value->translations)) {
                                        $productAttributes = ProductAttribute::find()
                                            ->where(['value' => $value->id])
                                            ->select('id')
                                            ->column();

                                        $val = $value->translations['ru-RU']->title;
                                        $newValId = array_search($val, $data[$newAttrId]['values']);

                                        if (!empty($productAttributes) && $newValId) {
                                            ProductAttribute::updateAll([
                                                'attribute_id' => $newAttrId,
                                                'entity_alias' => $a->alias,
                                                'value' => $newValId
                                            ], [
                                                'value' => $value->id
                                            ]);

                                            ProductAttribute::deleteAll(['value' => $value->id]);
                                        }
                                    }
                                }

                                $source->delete();
                            }
                        }
                    }
                }
            }

            return true;
        }

        return false;
    }

    public function actionNewDefaultAttributeGroups()
    {
        $attributeIds = [
            53,
            46,
            50,
            45,
        ];

        /** @var Category[] $targetCategories */
        $targetCategories = Category::find()
            ->where(['lvl' => [1, 2]])
            ->andWhere(['attribute_set_id' => null])
            ->andWhere(['type' => 'product_category'])
            ->all();

        foreach($targetCategories as $category) {
            if(!$category->attribute_set_id) {
                $parents = $category->parents()->select('name')->column();

                $group = new AttributeGroup([
                    'translationsArr' => [
                        new AttributeDescription([
                            'locale' => 'ru-RU',
                            'title' => implode(' - ', $parents) . " - {$category->name}",
                        ])
                    ],
                    'title' => implode(' - ', $parents) . " - {$category->name}",
                    'is_generated' => true,
                    'sort_order' => 0
                ]);

                if ($group->save() === true) {
                    foreach ($attributeIds as $item) {
                        $attrToGroup = new AttributeToGroup([
                            'attribute_group_id' => $group->id,
                            'attribute_id' => $item
                        ]);

                        $attrToGroup->save();
                    }

                    $category->updateAttributes(['attribute_set_id' => $group->id]);
                    Console::output(implode(' - ', $parents) . " - {$category->name}");
                }
            }
        }
    }
}