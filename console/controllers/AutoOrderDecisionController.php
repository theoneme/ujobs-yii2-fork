<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.03.2017
 * Time: 11:58
 */

namespace console\controllers;

use common\models\Notification;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\Url;

/**
 * Class AutoDeliveryController
 * @package console\controllers
 */
class AutoOrderDecisionController extends Controller
{
    /**
     *
     */
    public function actionAutoAcceptAdvanced()
    {
        $orders = Order::find()->where(['status' => [Order::STATUS_DELIVERED, Order::STATUS_AWAITING_CUSTOMER_REVIEW, Order::STATUS_AWAITING_SELLER_REVIEW, Order::STATUS_AWAITING_REVIEW]])->andWhere(['<', 'updated_at', time() - (3 * 60 * 60 * 24)])->all();

        Console::startProgress(0, count($orders));
        foreach ($orders as $key => $order) {
            Console::updateProgress($key, count($orders));

            $history = new OrderHistory(['isBlameable' => false]);
            $history->type = OrderHistory::TYPE_MANUAL_COMPLETE;
            $history->seller_id = $order->seller_id;
            $history->customer_id = $order->customer_id;
            $history->order_id = $order->id;
            if ($history->save()) {
                $order->status = Order::STATUS_COMPLETED;
                $order->save();

                $notification = new Notification();
                $notification->to_id = $order->seller_id;
                $notification->subject = Notification::SUBJECT_ORDER_UPDATED;
                $notification->template = Notification::TEMPLATE_ORDER_UPDATED;
                $notification->custom_data = [
                    'number' => $order->id
                ];
                $notification->thumb = $order->product ? $order->product->getThumb('catalog') : null;
                $notification->linkRoute = ['/account/order/history', 'id' => $order->id];
                $notification->sender = Notification::SENDER_SYSTEM;
                $notification->withEmail = false;
                $notification->save();
            }
        }

        Console::endProgress("end" . PHP_EOL);

        return true;
    }

    /**
     *
     */
    public function actionCancelOrders()
    {
        /* @var Order[] $orders */
        $orders = Order::find()
            ->where(['or',
                ['status' => Order::STATUS_MISSING_DETAILS],
                ['between', 'status', Order::STATUS_TENDER_PAID, Order::STATUS_TENDER_REQUIRES_TASK_APPROVE],
            ])->andWhere(['not',
                ['exists',
                    OrderHistory::find()
                        ->where('order_history.order_id = order.id')
                        ->andWhere(['>=', 'order_history.created_at', time() - 60 * 60 * 24 * 3])
                ]
            ])->all();
        foreach ($orders as $order) {
            $order->status = Order::STATUS_CANCELLED;
            if ($order->save()) {
                $orderHistory = new OrderHistory(['isBlameable' => false]);
                $orderHistory->type = OrderHistory::TYPE_MANUAL_CANCEL;
                $orderHistory->seller_id = $order->seller_id;
                $orderHistory->customer_id = $order->customer_id;
                $orderHistory->order_id = $order->id;
                $orderHistory->sender_id = $order->seller_id;
                $orderHistory->save();

                $notification = new Notification();
                $notification->to_id = $order->customer_id;
                $notification->subject = Notification::SUBJECT_ORDER_UPDATED;
                $notification->template = Notification::TEMPLATE_ORDER_CANCELLED;
                $notification->custom_data = ['number' => $order->id];
                $notification->params = ['number' => $order->id];
                $notification->thumb = $order->product ? $order->product->getThumb() : null;
                $notification->linkRoute = ['/account/order/history', 'id' => $order->id];
                $notification->sender = Notification::SENDER_SYSTEM;
                $notification->withEmail = true;
                $notification->save();
            }
        }
    }
}