<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.11.2016
 * Time: 16:13
 */

namespace console\controllers;

use common\models\Attachment;
use common\models\user\Profile;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * Class S3Controller
 * @package console\controllers
 */
class S3Controller extends Controller
{
    /**
     *
     */
    public function actionCopySocialAvatarsToS3()
    {
        $profiles = Profile::find()->where(['like', 'gravatar_email', 'http%', false]);
        $count = $profiles->count();
        echo "Total files found in uploads directory: " . $count . PHP_EOL;
        Console::startProgress(0, $count);

        $i = 0;
        $year = date('Y');
        $month = date('m');
        $dir = Yii::getAlias('@frontend') . "/web/uploads/user/{$year}/{$month}/";
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        foreach ($profiles->batch(50) as $batch) {
            foreach ($batch as $item) {
                //Console::output($item->gravatar_email);
                $content = file_get_contents($item->gravatar_email);
                $fileName = Yii::$app->utility->transliterate($item->getSellerName()) . '-' . $item->user_id . '.jpg';
                $name = Yii::getAlias('@frontend') . "/web/uploads/user/{$year}/{$month}/" . $fileName;
                $fp = fopen($name, "w");
                fwrite($fp, $content);
                fclose($fp);

                Image::getImagine()->open($name)
                    ->save($name, ['quality' => 60]);

                $mediaLayer = Yii::$app->get('mediaLayer');
                $mediaLayer->saveToAws("/uploads/user/{$year}/{$month}/{$fileName}");

                $item->updateAttributes(['gravatar_email' => "/uploads/user/{$year}/{$month}/{$fileName}"]);

                Console::updateProgress($i, $count);
                $i++;
            }

            Console::endProgress("end" . PHP_EOL);
            break;
        }
    }

    /**
     *
     */
    public function actionClearBrokenAttachments()
    {
        $attachments = Attachment::find()->all();
        $count = count($attachments);

        echo "Total attachments to check: " . $count . PHP_EOL;
        Console::startProgress(0, $count);

        $s3 = Yii::$app->s3;
        foreach ($attachments as $key => $attachment) {
            if (!$s3->exist(Yii::$app->utility->fixAwsMediaPath($attachment->content))) {
                $attachment->delete();
            }
            $key++;
            Console::updateProgress($key, $count);
        }

        Console::endProgress("end" . PHP_EOL);
    }
}