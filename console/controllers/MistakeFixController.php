<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.01.2017
 * Time: 15:39
 */

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class MistakeFixController
 * @package console\controllers
 */
class MistakeFixController extends Controller
{

    public function actionClearDuplicateTags()
    {
        $entities = [
            'job_id' => '\\common\\models\\JobAttribute',
            'page_id' => '\\common\\models\\PageAttribute',
            'user_portfolio_id' => '\\common\\models\\UserPortfolioAttribute',
            'product_id' => '\\common\\modules\\board\\models\\ProductAttribute'
        ];

        foreach($entities as $relationField => $entity) {
            $duplicates = $entity::find()
                ->select("count(id) as dups, value, id, {$relationField}")
                ->groupBy("{$relationField}, value")
                ->having('dups > 1')
                ->all();

            $count = count($duplicates);
            Console::output("Found {$count} duplicates");
            Console::startProgress(0, $count);

            foreach($duplicates as $key => $duplicate) {
                $entity::deleteAll(['and', [
                    $relationField => $duplicate->$relationField,
                    'value' => $duplicate->value
                ], ['not', ['id' => $duplicate->id]]]);

                Console::updateProgress($key + 1, $count);
            }
            Console::endProgress("End entity" . PHP_EOL);
        }

        Console::output("global end of universe" . PHP_EOL);
    }
}