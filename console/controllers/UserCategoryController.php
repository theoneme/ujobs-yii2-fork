<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.04.2017
 * Time: 19:11
 */

namespace console\controllers;

use common\models\Job;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserCategory;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class UserCategoryController
 * @package console\controllers
 */
class UserCategoryController extends Controller
{
    public function actionSetUserCategories()
    {
        $users = User::find()
            ->joinWith(['jobs', 'profile'])
            ->where(['profile.status' => Profile::STATUS_ACTIVE])
            ->andWhere(['not exists', UserCategory::find()->where('user_category.user_id = user.id')])
            //->andWhere(['exists', Job::find()->where('job.user_id = user.id')->andWhere(['job.status' => Job::STATUS_ACTIVE, 'job.type' => Job::TYPE_JOB])])
            ->andWhere(['job.status' => Job::STATUS_ACTIVE, 'job.type' => Job::TYPE_JOB])
            ->orderBy('user.id')
            ->groupBy('user.id');

        $totalUsers = $users->count();
        echo "Total new requests: " . $totalUsers . PHP_EOL;
        Console::startProgress(0, $totalUsers);
        $i = 0;
        foreach ($users->batch(50) as $batch) {
            foreach ($batch as $user) {
                $categories = [];

                foreach ($user->jobs as $job) { //
                    if ($job->status == Job::STATUS_ACTIVE && $job->type == Job::TYPE_JOB) {
                        $categories[] = $job->category_id;
                    }
                }

                $categories = array_unique($categories);

                foreach ($categories as $category) {
                    $userCategory = new UserCategory();
                    $userCategory->user_id = $user->id;
                    $userCategory->category_id = $category;
                    $userCategory->save();
                }

                $i++;
                Console::updateProgress($i, $totalUsers);
            }

        }
        Console::endProgress("end" . PHP_EOL);
    }
}