<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.03.2017
 * Time: 10:17
 */

namespace console\controllers;

use common\models\Category;
use common\models\ContentTranslation;
use common\models\DynamicForm;
use common\models\EmailImport;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeGroup;
use common\modules\attribute\models\AttributeToGroup;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\frontend\PostProductForm;
use common\modules\board\models\Product;
use common\services\YandexTranslatorService;
use Yii;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 * Class ParseController
 * @package console\controllers
 */
class ParseController extends Controller
{
    /**
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionTranslations()
    {
        $separator = '----------';
        $fileName = '';
        $firstDataRowIndex = 2;
        $availableFiles = [
            'app',
            'account',
            'document',
            'email',
            'filter',
            'seo',
            'model',
            'labels',
            'order',
            'notifications',
            'livechat',
            'board',
            'store',
            'wizard',
            'messenger'
        ];
        $result = [];
        $replaces = [];

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/translations1.xlsx');

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        foreach ($rowIterator as $key => $row) {
            if ($key < $firstDataRowIndex) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            foreach ($cellIterator as $cell) {
                $column = $cell->getColumn();
                $val = $cell->getCalculatedValue();
                if ($val == $separator) {
                    break;
                }
                switch ($column) {
                    case 'A':
                        if (in_array($val, $availableFiles)) {
                            $fileName = $val . '.php';
                            break 2;
                        }
                        $en = $val;
                        break;
                    case 'B':
                        break;
                    case 'C':
                        $result['zh-CN'][$fileName][$en] = $val;
                        break;
//                    case 'D':
//                        $result['ru-RU'][$fileName][$en] = $val;
//                        break;
//                    case 'E':
//                        $result['uk-UA'][$fileName][$en] = $val;
//                        break;
//                    case 'F':
//                        $result['es-ES'][$fileName][$en] = $val;
//                        break;
                }
            }
        }
        foreach ($result as $locale => $files) {
            $dir = Yii::getAlias('@console') . '/import/translations/' . $locale . '/';
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            foreach ($files as $name => $array) {
                file_put_contents($dir . $name, "<?php \n return " . VarDumper::export($array) . ";");
            }
        }
        file_put_contents(Yii::getAlias('@console') . '/import/translations/replaces.php', VarDumper::export($replaces) . ";");
    }

    /**
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionMissingTranslations()
    {
        $firstDataRowIndex = 2;
        $result = [];
        $result2 = [];

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/missing.xlsx');

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        foreach ($rowIterator as $key => $row) {
            if ($key < $firstDataRowIndex) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            foreach ($cellIterator as $cell) {
                $column = $cell->getColumn();
                $val = $cell->getCalculatedValue();

                switch ($column) {
                    case 'A':
                        $ru = $val;
                        break;
                    case 'B':
                        $en = $val;
                        break;
                    case 'C':
                        $es = $val;
                        break;
                    case 'D':
                        $ua = $val;
                        break;
                    case 'E':
                        $page = $val;
                        break;
                }
            }
            $result2[$ru] = $en;
            $result['ru-RU'][$en] = $ru;
            $result['es-ES'][$en] = $es;
            $result['uk-UA'][$en] = $ua;
        }
        foreach ($result as $locale => $array) {
            $dir = Yii::getAlias('@console') . '/import/translations/' . $locale . '/';
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            file_put_contents($dir . 'missing.php', "<?php \n return " . VarDumper::export($array) . ";");
        }
        file_put_contents(Yii::getAlias('@console') . '/import/translations/missing.php', VarDumper::export($result2));
    }


    /**
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionReplaceTranslations()
    {
        $array = eval("return " . file_get_contents(Yii::getAlias('@console') . '/import/translations/replaces.php'));
        $files = array_merge(FileHelper::findFiles(Yii::getAlias('@common'), ['only' => ['*.php']]), FileHelper::findFiles(Yii::getAlias('@frontend'), ['only' => ['*.php']]));
        $f = 0;
        $nf = 0;
        foreach ($array as $scourceFile => $replaces) {
            $scource = str_replace('.php', '', $scourceFile);
            foreach ($replaces as $from => $to) {
                $found = false;
                foreach ($files as $file) {
                    $content = file_get_contents($file);
                    $pattern = '/(Yii::t\([\',"]' . $scource . '[\',"],\s*[\',"])' . preg_quote($from, "/") . '([\',"](\s*,\s*\[[\s\S]*?\]\s*)?\);?)/';
                    if (preg_match($pattern, $content)) {
                        file_put_contents($file, preg_replace($pattern, '${1}' . addslashes($to) . '${2}', $content));
                        $f++;
                        $found = true;
                    }
                }
                if (!$found) {
                    $nf++;
                    var_dump("NOT FOUND " . $from . " IN SCOURCE " . $scource);
                }
            }
        }
        var_dump($f);
        var_dump($nf);
    }

    /**
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionProductCategories()
    {
        Category::deleteAll(['type' => 'product_category']);
        $root = new Category([
            'id' => 2000,
            'type' => 'product_category',
            'name' => 'Каталог товаров',
            'alias' => 'product_root',
            'translationsArr' => [
                new ContentTranslation(['slug' => 'product_root', 'title' => 'Каталог товаров', 'locale' => 'ru-RU']),
                new ContentTranslation(['slug' => 'product_root', 'title' => 'Каталог товарів', 'locale' => 'uk-UA']),
                new ContentTranslation(['slug' => 'product_root', 'title' => 'Catálogo de productos', 'locale' => 'es-ES']),
                new ContentTranslation(['slug' => 'product_root', 'title' => 'Product catalog', 'locale' => 'en-EN'])
            ]
        ]);
        $root->makeRoot();
        $categories = [];

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/product_categories.xlsx');

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        foreach ($rowIterator as $key => $row) {

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $ru = null;
            foreach ($cellIterator as $cell) {
                $column = $cell->getColumn();
                $val = $cell->getCalculatedValue();

                switch ($column) {
                    case 'A':
                        if (empty($val)) {
                            break 2;
                        }
                        $ru = $val;
                        $categories[$ru]['locales']['ru-RU'] = $ru;
                        break;
                    case 'B':
                        $childRu = $val;
                        break;
                    case 'C':
                        $categories[$ru]['locales']['uk-UA'] = $val;
                        break;
                    case 'D':
                        $childUa = $val;
                        break;
                    case 'E':
                        $categories[$ru]['locales']['en-GB'] = $val;
                        break;
                    case 'F':
                        $childEn = $val;
                        break;
                    case 'G':
                        $categories[$ru]['locales']['es-ES'] = $val;
                        break;
                    case 'H':
                        $childEs = $val;
                        break;
                }
            }
            if ($ru !== null) {
                $categories[$ru]['children'][] = [
                    'ru-RU' => $childRu,
                    'uk-UA' => $childUa,
                    'en-GB' => $childEn,
                    'es-ES' => $childEs,
                ];
            }
        }
        foreach ($categories as $category) {
            $translations = [];
            foreach ($category['locales'] as $locale => $title) {
                $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
            }
            $item = new Category([
                'type' => 'product_category',
                'name' => $category['locales']['ru-RU'],
                'translationsArr' => $translations
            ]);
            $item->appendTo($root);
            foreach ($category['children'] as $child) {
                $childTranslations = [];
                foreach ($child as $childLocale => $childTitle) {
                    $childTranslations[] = new ContentTranslation(['title' => $childTitle, 'locale' => $childLocale]);
                }
                $childItem = new Category([
                    'type' => 'product_category',
                    'name' => $child['ru-RU'],
                    'translationsArr' => $childTranslations
                ]);
                $childItem->appendTo($item);
            }
        }
        var_dump('success');
        die;
    }

    /**
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionAddProductCategories()
    {
        $root = Category::find()->where(['alias' => 'product_root'])->one();
        $categories = [];
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/product_categories.xlsx');

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        $rowsRange = [412 => 429, 503 => 515, 517 => 528];
        foreach ($rowIterator as $key => $row) {
            $skipRow = true;
            foreach ($rowsRange as $min => $max) {
                if ($key >= $min && $key <= $max) {
                    $skipRow = false;
                    break;
                }
            }
            if ($skipRow) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $ru = null;
            foreach ($cellIterator as $cell) {
                $column = $cell->getColumn();
                $val = $cell->getCalculatedValue();

                switch ($column) {
                    case 'A':
                        if (empty($val)) {
                            break 2;
                        }
                        $ru = $val;
                        $categories[$ru]['locales']['ru-RU'] = $ru;
                        break;
                    case 'B':
                        $childRu = $val;
                        break;
                    case 'C':
                        $categories[$ru]['locales']['uk-UA'] = $val;
                        break;
                    case 'D':
                        $childUa = $val;
                        break;
                    case 'E':
                        $categories[$ru]['locales']['en-GB'] = $val;
                        break;
                    case 'F':
                        $childEn = $val;
                        break;
                    case 'G':
                        $categories[$ru]['locales']['es-ES'] = $val;
                        break;
                    case 'H':
                        $childEs = $val;
                        break;
                }
            }
            if ($ru !== null) {
                $categories[$ru]['children'][] = [
                    'ru-RU' => $childRu,
                    'uk-UA' => $childUa,
                    'en-GB' => $childEn,
                    'es-ES' => $childEs,
                ];
            }
        }
        foreach ($categories as $category) {
            $translations = [];
            foreach ($category['locales'] as $locale => $title) {
                $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
            }
            $item = new Category([
                'type' => 'product_category',
                'name' => $category['locales']['ru-RU'],
                'translationsArr' => $translations
            ]);
            $item->appendTo($root);
            foreach ($category['children'] as $child) {
                $childTranslations = [];
                foreach ($child as $childLocale => $childTitle) {
                    $childTranslations[] = new ContentTranslation(['title' => $childTitle, 'locale' => $childLocale]);
                }
                $childItem = new Category([
                    'type' => 'product_category',
                    'name' => $child['ru-RU'],
                    'translationsArr' => $childTranslations
                ]);
                $childItem->appendTo($item);
            }
        }
        var_dump('success');
        die;
    }

    /**
     * @param $firstDataRow
     * @param $lastDataRow
     */
    public function actionParseJobAttributes($firstDataRow, $lastDataRow)
    {
        $types = [
            'текстовое' => 'textbox',
            'цифры' => 'numberbox',
            'текстовое+' => 'multitextbox',
            'радио' => 'radiolist',
            'чекбоксы' => 'checkboxlist',
        ];
        $common = [
            'Теги',
        ];

        $translator = new YandexTranslatorService();
        $locales = Yii::$app->params['supportedLocales'];
        if (isset($locales['uk-UA'])) {
            $locales['uk-UA'] = 'uk';
        }

        $firstDataRowIndex = $firstDataRow;
        $lastDataRowIndex = $lastDataRow;

        /* @var \PHPExcel_Reader_Excel2007 $objReader */
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(FALSE);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/job_attributes.xlsx');

        $attributes = [];

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator($firstDataRowIndex);
        foreach ($rowIterator as $key => $row) {
            Console::output("Row: {$key}. ");
            if ($key < $firstDataRowIndex) {
                continue;
            }

            if ($key > $lastDataRowIndex) {
                break;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $categoryId = 0;
            $iterator = -1;

            $root = Category::find()->where(['type' => 'job_category', 'lvl' => 0])->one();

            foreach ($cellIterator as $k => $cell) {
//                Console::output("{$k} | ");
                /* @var \PHPExcel_Cell $cell */
                $cellIndex = \PHPExcel_Cell::columnIndexFromString($cell->getColumn());

                if ($k === 'A') {
                    $title = $cell->getCalculatedValue();
                    $upperCategory = Category::find()->joinWith(['translations'])->where(['lvl' => 1, 'type' => 'job_category', 'content_translation.title' => $title])->one();
                    if ($upperCategory === null) {
                        $translations = [];
                        foreach (Yii::$app->params['languages'] as $locale => $language) {
                            $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
                        }
                        $upperCategory = new Category([
                            'type' => 'job_category',
                            'name' => $title,
                            'translationsArr' => $translations
                        ]);
                        $upperCategory->appendTo($root);
                    }
                }

                if ($k === 'B') {
                    $title = $cell->getCalculatedValue();
                    $middleCategory = Category::find()->joinWith(['translations'])->where(['lvl' => 2, 'type' => 'job_category', 'content_translation.title' => $title])->andWhere(['>', 'lft', $upperCategory->lft])->andWhere(['<', 'rgt', $upperCategory->rgt])->one();

                    if ($middleCategory === null) {
                        $translations = [];
                        foreach (Yii::$app->params['languages'] as $locale => $language) {
                            $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
                        }
                        $middleCategory = new Category([
                            'type' => 'job_category',
                            'name' => $title,
                            'translationsArr' => $translations
                        ]);
                        $middleCategory->appendTo($upperCategory);
                    }

                    $categoryId = $middleCategory->id;
                }

                if ($k === 'C') {
                    $title = trim($cell->getCalculatedValue());
                    if ($title) {
                        $lowerCategory = Category::find()->joinWith(['translations'])->where(['lvl' => 3, 'type' => 'job_category', 'content_translation.title' => $title])->andWhere(['>', 'lft', $middleCategory->lft])->andWhere(['<', 'rgt', $middleCategory->rgt])->one();

                        if ($lowerCategory === null) {
                            $translations = [];
                            foreach (Yii::$app->params['languages'] as $locale => $language) {
                                $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
                            }
                            $lowerCategory = new Category([
                                'type' => 'job_category',
                                'name' => $title,
                                'sort_order' => $key,
                                'translationsArr' => $translations
                            ]);
                            $lowerCategory->appendTo($middleCategory);
                        }

                        $categoryId = $lowerCategory->id;
                    }
                }

                if ($cellIndex > 3) {
                    $iterator++;
                    if ($iterator % 3 === 0) {
                        if (!$cell->getCalculatedValue()) continue;
//                        if (!array_key_exists($cell->getCalculatedValue(), $types)) continue;

                        $color = $objPHPExcel->getActiveSheet()->getStyle("{$k}{$key}")->getFill()->getStartColor()->getRGB();

                        $attributes[$categoryId][$iterator] = [
                            'title' => trim($cell->getCalculatedValue()),
//                            'type' => $types[$cell->getCalculatedValue()],
                            'values' => [],
                            'isPrimary' => $color == 'D9D2E9' ? false : true
                        ];
                    }

                    if ($iterator % 3 === 1) {
                        if (!$cell->getCalculatedValue()) continue;
                        $attributes[$categoryId][$iterator - 1]['type'] = $types[trim($cell->getCalculatedValue())];
                    }

                    if ($iterator % 3 === 2) {
                        if (!$cell->getCalculatedValue()) continue;
                        $attributes[$categoryId][$iterator - 2]['values'] = $this->getValues($attributes[$categoryId][$iterator - 2]['type'], trim($cell->getCalculatedValue()));
                    }
                }
            }
//            $attributes[$categoryId][] = [
//                'title' => 'Теги',
//                'type' => 'multitextbox',
//                'values' => []
//            ];
//            print_r($attributes); die;
        }

        if (!empty($attributes)) {
            foreach ($attributes as $a => $category) {
                $foundAttributes = [];
                $cat = Category::find()->where(['category.id' => $a])->joinWith(['translation'])->one();
                if ($cat === null) continue;
                $group = AttributeGroup::find()->where(['title' => "{$cat->id}_{$cat->translation->title}"])->one();
                if ($group === null) {
                    $group = new AttributeGroup([
                        'translationsArr' => [
                            new AttributeDescription([
                                'locale' => 'ru-RU',
                                'title' => $cat->translation->title,
                            ])
                        ],
                        'title' => "{$cat->id}_{$cat->translation->title}",
                        'sort_order' => 0
                    ]);

                    $group->save();
                }

                Category::updateAll(['attribute_set_id' => $group->id], ['id' => $a]);
                $sortIterator = 0;
                foreach ($category as $b => $set) {
                    $sortIterator++;
                    if (!array_key_exists('type', $set)) continue;
                    $title = $set['title'];

                    if (in_array($title, $common)) {
                        $attribute = Attribute::find()->joinWith(['translations'])->where(['name' => $title])->one();
                    } else {
                        $attribute = Attribute::find()->where(['alias' => "{$a}-" . Yii::$app->utility->transliterate($title)])->one();
                        if ($attribute === null) {
                            $attribute = new Attribute([
                                'is_system' => false,
                                'is_searchable' => true,
                                'name' => "{$cat->translation->title}-" . $title,
                                'type' => $set['type'],
                                'alias' => "{$a}-" . Yii::$app->utility->transliterate($title),
                                'is_parsed' => true,
                                'is_primary' => $set['isPrimary']
                            ]);
                            foreach ($locales as $locale => $code) {
                                $attribute->translationsArr[$locale]->attributes = [
                                    'title' => $locale === 'ru-RU' ? $title : $translator->translate($title, 'ru', $code),
                                    'description' => ''
                                ];
                            }

                            $attribute->save();
                        } else {
                            $attribute->updateAttributes(['is_primary' => $set['isPrimary']]);
                        }
                    }
                    $foundAttributes[] = $attribute->id;

                    $gr = AttributeToGroup::find()->where(['attribute_id' => $attribute->id, 'attribute_group_id' => $group->id])->one();
                    if ($gr === null) {
                        $gr = new AttributeToGroup([
                            'attribute_group_id' => $group->id,
                            'attribute_id' => $attribute->id,
                            'sort_order' => $sortIterator,
                        ]);

                        $gr->save();
                    } else {
                        $gr->updateAttributes(['sort_order' => $sortIterator]);
                    }

                    $foundValues = [];
                    if (is_array($set['values'])) {
                        foreach ($set['values'] as $c => $value) {
                            $attrValue = AttributeValue::find()->where(['alias' => Yii::$app->utility->transliterate($value), 'attribute_id' => $attribute->id])->one();
                            if ($attrValue === null) {
                                $translationsArr = [];
                                foreach ($locales as $locale => $code) {
                                    $translationsArr[] = [
                                        'title' => $locale === 'ru-RU' ? $value : $translator->translate($value, 'ru', $code),
                                        'description' => '',
                                        'locale' => $locale
                                    ];
                                }
                                $attrValue = new AttributeValue([
                                    'attribute_id' => $attribute->id,
                                    'status' => AttributeValue::STATUS_APPROVED,
                                    'translationsArr' => $translationsArr
                                ]);
                                $attrValue->save();
                            }

                            $foundValues[] = $attrValue->id;
                        }
                    }
                    if (!empty($foundValues) && !strstr($attribute->alias, 'tag')) {
                        $valuesToDelete = AttributeValue::find()->where(['attribute_id' => $attribute->id])->andWhere(['not', ['id' => $foundValues]])->all();
                        if ($valuesToDelete !== null) {
                            foreach ($valuesToDelete as $vtd) {
                                $vtd->delete();
                            }
                        }
                    }
                }
                $atg = AttributeToGroup::find()->where(['attribute_group_id' => $group->id])->andWhere(['not', ['attribute_id' => $foundAttributes]])->all();
                if ($atg !== null) {
                    foreach ($atg as $item) {
                        $attribute = Attribute::findOne([$item->attribute_id]);
                        if ($attribute !== null) {
                            $attribute->delete();
                        }
                        $item->delete();
                    }
                }
            }
        }
    }

    /**
     *
     */
    public function actionParseProductAttributesTwo()
    {
        $types = [
            'текстовое' => 'textbox',
            'цифры' => 'numberbox',
            'текстовое+' => 'multitextbox',
            'радио' => 'radiolist'
        ];

        $common = [
            'Теги',
        ];

        $firstDataRowIndex = 574;
        $lastDataRowIndex = 592;

        /* @var \PHPExcel_Reader_Excel2007 $objReader */
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(FALSE);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/product_attributes_two.xlsx');

        $attributes = [];

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator($firstDataRowIndex);
        foreach ($rowIterator as $key => $row) {
            Console::output("Row: {$key}. ");
            if ($key < $firstDataRowIndex) {
                continue;
            }

            if ($key > $lastDataRowIndex) {
                break;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $categoryId = 0;
            $iterator = -1;

            $root = Category::find()->where(['type' => 'product_category', 'lvl' => 0])->one();

            foreach ($cellIterator as $k => $cell) {
//                Console::output("{$k} | ");
                /* @var \PHPExcel_Cell $cell */
                $cellIndex = \PHPExcel_Cell::columnIndexFromString($cell->getColumn());

                if ($k === 'A') {
                    $title = $cell->getCalculatedValue();
                    $upperCategory = Category::find()->joinWith(['translations'])->where(['lvl' => 1, 'type' => 'product_category', 'content_translation.title' => $title])->one();
                    if ($upperCategory === null) {
                        $translations = [];
                        foreach (Yii::$app->params['languages'] as $locale => $language) {
                            $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
                        }
                        $upperCategory = new Category([
                            'type' => 'product_category',
                            'name' => $title,
                            'translationsArr' => $translations
                        ]);
                        $upperCategory->appendTo($root);
                    }
                }

                if ($k === 'B') {
                    $title = $cell->getCalculatedValue();
                    $middleCategory = Category::find()->joinWith(['translations'])->where(['lvl' => 2, 'type' => 'product_category', 'content_translation.title' => $title])->andWhere(['>', 'lft', $upperCategory->lft])->andWhere(['<', 'rgt', $upperCategory->rgt])->one();

                    if ($middleCategory === null) {
                        $translations = [];
                        foreach (Yii::$app->params['languages'] as $locale => $language) {
                            $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
                        }
                        $middleCategory = new Category([
                            'type' => 'product_category',
                            'name' => $title,
                            'translationsArr' => $translations
                        ]);
                        $middleCategory->appendTo($upperCategory);
                    }

                    $categoryId = $middleCategory->id;
                }

                if ($k === 'C') {
                    $title = trim($cell->getCalculatedValue());
                    if ($title) {
                        $lowerCategory = Category::find()->joinWith(['translations'])->where(['lvl' => 3, 'type' => 'product_category', 'content_translation.title' => $title])->andWhere(['>', 'lft', $middleCategory->lft])->andWhere(['<', 'rgt', $middleCategory->rgt])->one();

                        if ($lowerCategory === null) {
                            $translations = [];
                            foreach (Yii::$app->params['languages'] as $locale => $language) {
                                $translations[] = new ContentTranslation(['title' => $title, 'locale' => $locale]);
                            }
                            $lowerCategory = new Category([
                                'type' => 'product_category',
                                'name' => $title,
                                'sort_order' => $key,
                                'translationsArr' => $translations
                            ]);
                            $lowerCategory->appendTo($middleCategory);
                        }

                        $categoryId = $lowerCategory->id;
                    }
                }

                if ($cellIndex > 3) {
                    $iterator++;
                    if ($iterator % 3 === 0) {
                        if (!$cell->getCalculatedValue()) continue;
//                        if (!array_key_exists($cell->getCalculatedValue(), $types)) continue;

                        $color = $objPHPExcel->getActiveSheet()->getStyle("{$k}{$key}")->getFill()->getStartColor()->getRGB();

                        $attributes[$categoryId][$iterator] = [
                            'title' => trim($cell->getCalculatedValue()),
//                            'type' => $types[$cell->getCalculatedValue()],
                            'values' => [],
                            'isPrimary' => $color == 'D9D2E9' ? false : true
                        ];
                    }

                    if ($iterator % 3 === 1) {
                        if (!$cell->getCalculatedValue()) continue;
                        $attributes[$categoryId][$iterator - 1]['type'] = $types[trim($cell->getCalculatedValue())];
                    }

                    if ($iterator % 3 === 2) {
                        if (!$cell->getCalculatedValue()) continue;
                        $attributes[$categoryId][$iterator - 2]['values'] = $this->getValues($attributes[$categoryId][$iterator - 2]['type'], trim($cell->getCalculatedValue()));
                    }
                }
            }
            $attributes[$categoryId][] = [
                'title' => 'Теги',
                'type' => 'multitextbox',
                'values' => []
            ];
//            print_r($attributes); die;
        }

        if (!empty($attributes)) {
            foreach ($attributes as $a => $category) {
                $foundAttributes = [];
                $cat = Category::find()->where(['category.id' => $a])->joinWith(['translation'])->one();
                if ($cat === null) continue;
                $group = AttributeGroup::find()->where(['title' => "{$cat->id}_{$cat->translation->title}"])->one();
                if ($group === null) {
                    $group = new AttributeGroup([
                        'translationsArr' => [
                            new AttributeDescription([
                                'locale' => 'ru-RU',
                                'title' => $cat->translation->title,
                            ])
                        ],
                        'title' => "{$cat->id}_{$cat->translation->title}",
                        'sort_order' => 0
                    ]);

                    $group->save();
                }

                Category::updateAll(['attribute_set_id' => $group->id], ['id' => $a]);
                $sortIterator = 0;
                foreach ($category as $b => $set) {
                    $sortIterator++;
                    if (!array_key_exists('type', $set)) continue;
                    $title = $set['title'];

                    if (in_array($title, $common)) {
                        $attribute = Attribute::find()->joinWith(['translations'])->where(['name' => $title])->one();
                    } else {
                        $attribute = Attribute::find()->where(['alias' => "{$a}-" . Yii::$app->utility->transliterate($title)])->one();
                        if ($attribute === null) {
                            $attribute = new Attribute([
                                'is_system' => false,
                                'is_searchable' => true,
                                'name' => "{$cat->translation->title}-" . $title,
                                'type' => $set['type'],
                                'alias' => "{$a}-" . Yii::$app->utility->transliterate($title),
                                'is_parsed' => true,
                                'is_primary' => $set['isPrimary']
                            ]);
                            $attribute->translationsArr['ru-RU']->attributes = [
                                'title' => $title,
                                'description' => 'Чем больше деталей вы укажете, тем больше шансов, что покупатель найдет именно ваш товар.'
                            ];
                            $attribute->save();
                        } else {
                            $attribute->updateAttributes(['is_primary' => $set['isPrimary']]);
                        }
                    }
                    $foundAttributes[] = $attribute->id;

                    $gr = AttributeToGroup::find()->where(['attribute_id' => $attribute->id, 'attribute_group_id' => $group->id])->one();
                    if ($gr === null) {
                        $gr = new AttributeToGroup([
                            'attribute_group_id' => $group->id,
                            'attribute_id' => $attribute->id,
                            'sort_order' => $sortIterator,
                        ]);

                        $gr->save();
                    } else {
                        $gr->updateAttributes(['sort_order' => $sortIterator]);
                    }

                    $foundValues = [];
                    if (is_array($set['values'])) {
                        foreach ($set['values'] as $c => $value) {
                            $attrValue = AttributeValue::find()->where(['alias' => Yii::$app->utility->transliterate($value), 'attribute_id' => $attribute->id])->one();
                            if ($attrValue === null) {
                                $attrValue = new AttributeValue([
                                    'attribute_id' => $attribute->id,
                                    'status' => AttributeValue::STATUS_APPROVED,
                                    'translationsArr' => [
                                        [
                                            'title' => $value,
                                            'locale' => 'ru-RU'
                                        ],
                                    ]
                                ]);
                                $attrValue->save();
                            }

                            $foundValues[] = $attrValue->id;
                        }
                    }
                    if (!empty($foundValues) && !strstr($attribute->alias, 'tag')) {
                        $valuesToDelete = AttributeValue::find()->where(['attribute_id' => $attribute->id])->andWhere(['not', ['id' => $foundValues]])->all();
                        if ($valuesToDelete !== null) {
                            foreach ($valuesToDelete as $vtd) {
                                $vtd->delete();
                            }
                        }
                    }
                }
                $atg = AttributeToGroup::find()->where(['attribute_group_id' => $group->id])->andWhere(['not', ['attribute_id' => $foundAttributes]])->all();
                if ($atg !== null) {
                    foreach ($atg as $item) {
                        $attribute = Attribute::findOne([$item->attribute_id]);
                        if ($attribute !== null) {
                            $attribute->delete();
                        }
                        $item->delete();
                    }
                }
            }
        }
    }

    public function actionEmails()
    {
        EmailImport::deleteAll();
        $users = file_get_contents(Yii::getAlias('@console/') . '/import/emails.csv');
        $users = explode("\n", $users);
        foreach ($users as $user) {
            $attributes = explode(',', $user);
            (new EmailImport([
                'email' => $attributes[0],
                'first_name' => $attributes[1],
                'last_name' => $attributes[2],
                'source' => $attributes[3],
                'ip' => $attributes[4],
                'date_added' => strtotime(str_replace('"', '', $attributes[5])),
            ]))->save();
        }
    }

    public function actionParseEagentProperty($user_id = 83, $limit = 5, $offset = 0)
    {
        $iterator = 0;
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        Yii::$app->utility->makeDir(Yii::getAlias('@frontend') . "/web/uploads/temp/{$year}/{$month}/{$day}");

        $transaction = Yii::$app->db->beginTransaction();
        $success = true;

        $s3 = Yii::$app->s3Temp;

        $properties = Yii::$app->db1->createCommand("SELECT * FROM `property` WHERE entity_type = 'property_sale' and lat is not null and lon is not null and updated BETWEEN '2016-12-25 00:00:00' AND '2017-03-25 23:59:59'")->queryAll();

        foreach ($properties as $key => $property) {
            Console::output("Starting property {$property['id']}");
            $translation = Yii::$app->db1->createCommand("SELECT * FROM `property_translation` WHERE translatable_id = {$property['id']} and locale = 'en'")->queryOne();
            $description = Yii::$app->db1->createCommand("SELECT * FROM `property_sale` WHERE id = {$property['id']}")->queryOne();
            $medias = Yii::$app->db1->createCommand("SELECT * FROM `media__media` left join `property_media` on (`property_media`.`media_id` = `media__media`.`id`) WHERE `property_media`.`property_id` = {$property['id']} limit 15")->queryAll();
            $amenities = Yii::$app->db1->createCommand("SELECT * FROM `property_amenitie` left join `amenitie` on (amenitie.id = property_amenitie.amenitie_id) WHERE property_amenitie.property_id = {$property['id']}")->queryAll();

            if (empty($medias)) {
                continue;
            }

            $attachments = [];

            foreach ($medias as $media) {
                Console::output("Downloading media {$media['id']}");
                $dir1 = (int)($media['id'] / 100000);
                $dir2 = (int)(($media['id'] - ($dir1 * 100000)) / 1000);
                $dirPath = sprintf('%s/%04s/%02s', 'default', $dir1 + 1, $dir2 + 1);

                $finalPath = "https://s3-us-west-2.amazonaws.com/eroom-property-images/{$dirPath}/{$media['provider_reference']}";
                $pathInfo = pathinfo($finalPath);

//                $content = file_get_contents($finalPath);
                $file = uniqid();

                $path = "/uploads/temp/{$year}/{$month}/{$day}/{$file}.{$pathInfo['extension']}";
                $res = $s3->commands()->get("{$dirPath}/{$media['provider_reference']}")->saveAs(Yii::getAlias('@frontend') . "/web" . $path)->execute();
                $attachments[]['content'] = $path;

//                $fp = fopen(Yii::getAlias('@frontend') . "/web{$path}", 'w');
//                fwrite($fp, $content);
//                fclose($fp);


            }

            if (empty($attachments)) {
                continue;
            }

            $item['PostProductForm'] = [
                'user_id' => $user_id,
                'lat' => $property['lat'],
                'long' => $property['lon'],
                'price' => $property['price'],
                'currency_code' => 'USD',
                'address' => $property['address'],
                'parent_category_id' => 2477,
                'category_id' => 2480,
                'status' => 30,
                'secure' => 1,
                'type' => Product::TYPE_PRODUCT,
                'locale' => 'en-GB',
                'title' => StringHelper::truncate(trim(preg_replace('/\-.*$/', '', $translation['sluggable'])), 75),
                'description' => $this->renderPartial('property', [
                    'property' => $property,
                    'description' => $description,
                    'amenities' => $amenities
                ])
            ];
            $item['Attachment'] = $attachments;
            if ($property['areaSqFt']) {
                $item['DynamicForm']['attribute_2791'] = (int)($property['areaSqFt'] / 10.7639);
            }

            if ($property['yearBuilt']) {
                $item['DynamicForm']['attribute_2794'] = $property['yearBuilt'];
            }

            $postProductForm = new PostProductForm();
            $postProductForm->product = new Product(['isBlameable' => false]);
            $postProductForm->dynamic_form = new DynamicForm();
            $postProductForm->myLoad($item);
            $postProductForm->product->user_id = $item['PostProductForm']['user_id'];

            Console::output("Submit form for property {$property['id']}");
            if (!$postProductForm->save()) {
                $success = false;
            }

            $iterator++;
            if ($iterator >= $limit) {
                break;
            }
        }
        if ($success === true) {
            $transaction->commit();
            return true;
        }

        $transaction->rollback();
        return false;
    }

    /**
     * @param $type
     * @param $string
     * @return array
     */
    private function getValues($type, $string)
    {
        $values = null;

        if ($type !== null && $string !== null) {
            switch ($type) {
                case 'checkboxlist':
                case 'radiolist':
                case 'multitextbox':
                case 'numberbox':
                case 'textbox':
                    $values = StringHelper::explode($string, ',', true, true);
                    break;
            }
        }

        return $values;
    }
}