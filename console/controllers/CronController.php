<?php

namespace console\controllers;

use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\ContentTranslation;
use common\models\EmailImport;
use common\models\EmailLog;
use common\models\Event;
use common\models\Job;
use common\models\JobAttribute;
use common\models\JobPackage;
use common\models\Referral;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserAttribute;
use common\models\Video;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\Product;
use common\modules\board\models\ProductAttribute;
use common\services\GoogleMapsService;
use common\services\YandexTranslatorService;
use frontend\models\PostJobForm;
use frontend\models\PostRequestForm;
use frostealth\yii2\aws\s3\Service;
use MaxMind\Db\Reader;
use Yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 * Class CronController
 * @package console\controllers
 */
class CronController extends Controller
{

    public function actionIpToCity()
    {
        $users = User::find()->all();
        $cities = AttributeValue::find()
            ->select(['mod_attribute_value.id', 'mod_attribute_value.attribute_id', 'mod_attribute_value.alias', 'mod_attribute_description.title'])
            ->joinWith(['translation', 'relatedAttribute'])
            ->where(['mod_attribute.alias' => 'city'])
            ->indexBy('title')
            ->asArray()
            ->all();
        $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-City.mmdb');
        $i = 0;
        $userAttributes = [];
        $jobAttributes = [];
        $productAttributes = [];
        foreach ($users as $user) {
            /* @var $user User */
            if (!empty($user->registration_ip) && $user->registration_ip != '127.0.0.1') {
                $data = $geoIpReader->get($user->registration_ip);
                $city = $data['city']['names']['ru'] ?? $data['city']['names']['en'] ?? null;
                if (isset($cities[$city])) {
                    $i++;
                    if (!UserAttribute::find()->where(['user_id' => $user->id, 'entity_alias' => 'city'])->exists()) {
                        $userAttributes[] = [
                            $user->id,
                            '42',
                            $cities[$city]['id'],
                            'ru-RU',
                            'city',
                            $cities[$city]['alias']
                        ];
                    }
                    foreach ($user->jobs as $job) {
                        if (!JobAttribute::find()->where(['job_id' => $job->id, 'entity_alias' => 'city'])->exists()) {
                            $jobAttributes[] = [
                                $job->id,
                                '42',
                                $cities[$city]['id'],
                                'ru-RU',
                                'city',
                                $cities[$city]['alias']
                            ];
                        }
                    }
                    foreach ($user->products as $product) {
                        if (!ProductAttribute::find()->where(['product_id' => $product->id, 'entity_alias' => 'city'])->exists()) {
                            $productAttributes[] = [
                                $product->id,
                                '42',
                                $cities[$city]['id'],
                                'ru-RU',
                                'city',
                                $cities[$city]['alias']
                            ];
                        }
                    }
                }
            }
        }
        Yii::$app->db->createCommand()->batchInsert('user_attribute',
            ['user_id', 'attribute_id', 'value', 'locale', 'entity_alias', 'value_alias'],
            $userAttributes
        )->execute();
        Yii::$app->db->createCommand()->batchInsert('job_attribute',
            ['job_id', 'attribute_id', 'value', 'locale', 'entity_alias', 'value_alias'],
            $jobAttributes
        )->execute();
        Yii::$app->db->createCommand()->batchInsert('product_attribute',
            ['product_id', 'attribute_id', 'value', 'locale', 'entity_alias', 'value_alias'],
            $productAttributes
        )->execute();
        var_dump($i);
        var_dump($i / count($users) * 100 . '%');
        die;
    }

    /**
     * Automatically translate jobs
     */
    public function actionTranslate()
    {
        $translator = new YandexTranslatorService();
        $languageAttributes = [
            'en-GB' => 'english',
//            'ua-UA' => 'ukrainian',
            'es-ES' => 'spanish',
//            'ka-GE' => 'georgian',
            'zh-CN' => 'chinese',
        ];
        /* @var $jobs Job[] */
        $jobs = Job::find()->joinWith(['translation', 'attachments', 'extras', 'extra', 'packages', 'user.userAttributes'])
            ->where([
                'job.type' => Job::TYPE_JOB,
                'auto_translated' => false,
                'job.status' => Job::STATUS_ACTIVE,
                'job.locale' => 'ru-RU',
                'user_attribute.value_alias' => $languageAttributes,
            ])
            ->andWhere(['not',
                ['or',
                    ['ct.content' => ''],
                    ['ct.content' => null]
                ]
            ])
            ->andWhere(['not exists',
                JobPackage::find()->from(['jp2' => 'job_package'])
                    ->andWhere("jp2.job_id = job.id")
                    ->andWhere(["<", "jp2.price", 200])
                    ->andWhere(["jp2.currency_code" => 'RUB'])
            ])
            ->limit(5)
            ->groupBy('job.id')
            ->all();
        if (!count($jobs)) {
            Yii::error('Работы закончились, айайай');
            die;
        }
        $toTranslate = [
            'PostJobForm' => ['title', 'description', 'requirements'],
            'JobQa' => ['question', 'answer'],
            'JobExtra' => ['title', 'description'],
            'JobAttribute' => ['value'],
            'JobPackage' => ['title', 'description', 'included', 'excluded'],
        ];
        // Создаем временную папку для картинок
        $path = '/uploads/temp/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $dir = Yii::getAlias('@frontend') . '/web' . $path;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
            chown($dir, 'www-data');
            chgrp($dir, 'www-data');
        }

        $locales = Yii::$app->params['bigLocales'];
        unset($locales['ru-RU']);
//        unset($locales['uk-UA']);
//        $locales['ua-UA'] = 'uk';
        foreach ($jobs as $job) {
            $userLanguages = ArrayHelper::getColumn(array_filter($job->user->userAttributes, function ($var) {
                return $var->entity_alias === 'language';
            }), 'value_alias');
            foreach ($locales as $locale => $code) {
                if (in_array($languageAttributes[$locale], $userLanguages)) {
                    // Создаем новую форму
                    $form = new PostJobForm();
                    $form->scenario = PostJobForm::SCENARIO_DEFAULT;
                    $form->job = new Job(['blameable' => false, 'user_id' => $job->user_id, 'auto_translated' => true]);

                    // Копируем аттрибуты из старой работы и связанных моделей
                    $input = [];
                    $input['PostJobForm'] = array_diff_key($job->attributes, ['id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    $input['JobQa'] = array_map(function ($var) {
                        return array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    }, $job->faq);

                    $input['JobExtra'] = array_map(function ($var) {
                        return array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    }, $job->extras);

                    $input['Attachment'] = array_map(function ($var) {
                        return array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    }, $job->attachments);

                    $input['JobAttribute'] = array_map(function ($var) {
                        return array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    }, $job->extra);

                    $input['JobPackage'] = array_reduce(
                        array_map(function ($var) {
                            return [$var->type => array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0])];
                        }, $job->packages),
                        'array_merge',
                        []
                    );
                    $input['PostJobForm']['title'] = $job->translation->title;
                    $input['PostJobForm']['description'] = $job->translation->content;
                    $input['PostJobForm']['three_packages'] = (count($job->packages) > 1);
                    $input['PostJobForm']['locale'] = $locale;

                    // Прогоняем поля, которые нужно перевести, через переводчик
                    $translationErrors = 0;
                    foreach ($toTranslate as $model => $fields) {
                        foreach ($fields as $field) {
                            if ($model === 'PostJobForm') {
                                $translated = $translator->translate($input[$model][$field], 'ru', $code);
                                if ($translated) {
                                    $input[$model][$field] = $translated;
                                    if ($field === 'title') {
                                        $input[$model][$field] = StringHelper::truncate($input[$model][$field], 79, '');
                                    }
                                    if ($field === 'requirements') {
                                        $input[$model][$field] = StringHelper::truncate($input[$model][$field], 599, '');
                                    }
                                } else {
                                    $translationErrors++;
                                }
                            } else {
                                foreach ($input[$model] as $id => $object) {
                                    if ($model === 'JobAttribute' && $field === 'value') {
                                        $input[$model][$id]['locale'] = $locale;
                                        continue;
                                    }
                                    if (!empty($input[$model][$id][$field])) {
                                        $translated = $translator->translate($input[$model][$id][$field], 'ru', $code);
                                        if ($translated) {
                                            $input[$model][$id][$field] = $translated;
                                        } else {
                                            $translationErrors++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($translationErrors > 5) {// Если слишком много ошибок перевода - не сохраняем
                        Yii::error($translationErrors . " ошибок при переводе, отмена сохранения \n" . VarDumper::export($input));
                    } else {
                        // Создаем копии картинок
                        foreach ($input['Attachment'] as $id => $attachment) {
                            $pathInfo = pathinfo($attachment['content']);
                            $newName = hash('crc32b', $pathInfo['filename'] . time()) . '.' . $pathInfo['extension'];
                            if (file_exists(Yii::getAlias('@frontend') . '/web' . $attachment['content'])) {
                                copy(Yii::getAlias('@frontend') . '/web' . $attachment['content'], $dir . $newName);
                                $input['Attachment'][$id]['content'] = $path . $newName;
                            } else {
                                $s3 = Yii::$app->get('s3');
                                if ($s3->exist(Yii::$app->utility->fixAwsMediaPath($attachment['content']))) {
                                    $fullUrl = Yii::$app->mediaLayer->tryLoadFromAws($attachment['content']);
                                    $file = file_get_contents($fullUrl);
                                    if ($file) {
                                        file_put_contents($dir . $newName, $file);
                                        $input['Attachment'][$id]['content'] = $path . $newName;
                                    }
                                    else {
                                        unset($input['Attachment'][$id]);
                                    }
                                }
                                else {
                                    unset($input['Attachment'][$id]);
                                }
                            }
                        }

                        // Сохраняем работу
                        try {
                            $form->myLoad($input);
                            if (!$form->save()) {
                                Yii::error("Ошибка при сохранении работы \n" . VarDumper::export($form->validateAll()));
                            } else {
                                $job->updateAttributes(['auto_translated' => true]);
                            }
                        } catch (\Exception $e) {
                            Yii::error("Исключение при сохранении работы \n" . $e->getTraceAsString());
                        }
                    }
                }
            }
        }
        var_dump("success");
        die;
    }

    /**
     * Automatically translate tenders
     */
    public function actionTranslateTenders()
    {
        $translator = new YandexTranslatorService();
        $languageAttributes = [
            'en-GB' => 'english',
//            'uk-UA' => 'ukrainian',
            'es-ES' => 'spanish',
//            'ka-GE' => 'georgian',
            'zh-CN' => 'chinese',
        ];
        /* @var $jobs Job[] */
        $jobs = Job::find()->joinWith(['translation', 'attachments', 'extra', 'user.userAttributes'])
            ->where([
                'job.type' => Job::TYPE_TENDER,
                'auto_translated' => false,
                'job.status' => Job::STATUS_ACTIVE,
                'job.locale' => 'ru-RU',
                'user_attribute.value_alias' => $languageAttributes
            ])
            ->andWhere(['not',
                ['or',
                    ['ct.content' => ''],
                    ['ct.content' => null]
                ]
            ])
            ->limit(5)
            ->groupBy('job.id')
            ->all();
        if (!count($jobs)) {
            Yii::error('Заявки закончились, айайай');
            die;
        }

        $toTranslate = [
            'PostRequestForm' => ['title', 'description'],
            'JobAttribute' => ['value'],
        ];
        // Создаем временную папку для картинок
        $path = '/uploads/temp/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $dir = Yii::getAlias('@frontend') . '/web' . $path;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
            chown($dir, 'www-data');
            chgrp($dir, 'www-data');
        }

        $locales = Yii::$app->params['bigLocales'];
        unset($locales['ru-RU']);
//        $locales['uk-UA'] = 'uk';
        foreach ($jobs as $job) {
            $userLanguages = ArrayHelper::getColumn(array_filter($job->user->userAttributes, function ($var) {
                return $var->entity_alias === 'language';
            }), 'value_alias');
            foreach ($locales as $locale => $code) {
                if (in_array($languageAttributes[$locale], $userLanguages)) {
                    // Создаем новую форму
                    $form = new PostRequestForm();
                    $form->job = new Job(['blameable' => false, 'user_id' => $job->user_id, 'auto_translated' => true, 'status' => Job::STATUS_ACTIVE]);

                    // Копируем аттрибуты из старой работы и связанных моделей
                    $input = [];
                    $input['PostRequestForm'] = array_diff_key($job->attributes, ['id' => 0, 'created_at' => 0, 'updated_at' => 0]);

                    $input['Attachment'] = array_map(function ($var) {
                        return array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    }, $job->attachments);

                    $input['JobAttribute'] = array_map(function ($var) {
                        return array_diff_key($var->attributes, ['id' => 0, 'job_id' => 0, 'created_at' => 0, 'updated_at' => 0]);
                    }, $job->extra);

                    $input['PostRequestForm']['title'] = $job->translation->title;
                    $input['PostRequestForm']['description'] = $job->translation->content;
                    $input['PostRequestForm']['locale'] = $locale;

                    // Прогоняем поля, которые нужно перевести, через переводчик
                    $translationErrors = 0;
                    foreach ($toTranslate as $model => $fields) {
                        foreach ($fields as $field) {
                            if ($model === 'PostRequestForm') {
                                $translated = $translator->translate($input[$model][$field], 'ru', $code);
                                if ($translated) {
                                    $input[$model][$field] = $translated;
                                    if ($field === 'title') {
                                        $input[$model][$field] = StringHelper::truncate($input[$model][$field], 79, '');
                                    }
                                } else {
                                    $translationErrors++;
                                }
                            } else {
                                foreach ($input[$model] as $id => $object) {
                                    if ($model === 'JobAttribute' && $field === 'value') {
                                        $input[$model][$id]['locale'] = $locale;
                                        continue;
                                    }
                                    if (!empty($input[$model][$id][$field])) {
                                        $translated = $translator->translate($input[$model][$id][$field], 'ru', $code);
                                        if ($translated) {
                                            $input[$model][$id][$field] = $translated;
                                        } else {
                                            $translationErrors++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($translationErrors > 5) {// Если слишком много ошибок перевода - не сохраняем
                        Yii::error($translationErrors . " ошибок при переводе, отмена сохранения \n" . VarDumper::export($input));
                    } else {
                        // Создаем копии картинок
                        foreach ($input['Attachment'] as $id => $attachment) {
                            $pathInfo = pathinfo($attachment['content']);
                            $newName = hash('crc32b', $pathInfo['filename'] . time()) . '.' . $pathInfo['extension'];
                            if (file_exists(Yii::getAlias('@frontend') . '/web' . $attachment['content'])) {
                                copy(Yii::getAlias('@frontend') . '/web' . $attachment['content'], $dir . $newName);
                                $input['Attachment'][$id]['content'] = $path . $newName;
                            } else {
                                $s3 = Yii::$app->get('s3');
                                if ($s3->exist(Yii::$app->utility->fixAwsMediaPath($attachment['content']))) {
                                    $fullUrl = Yii::$app->mediaLayer->tryLoadFromAws($attachment['content']);
                                    $file = file_get_contents($fullUrl);
                                    if ($file) {
                                        file_put_contents($dir . $newName, $file);
                                        $input['Attachment'][$id]['content'] = $path . $newName;
                                    }
                                    else {
                                        unset($input['Attachment'][$id]);
                                    }
                                }
                                else {
                                    unset($input['Attachment'][$id]);
                                }
                            }
                        }

                        // Сохраняем работу
                        try {
                            $form->myLoad($input);
                            if (!$form->save()) {
                                Yii::error("Ошибка при сохранении заявки \n" . VarDumper::export($form->validateAll()));
                            } else {
                                $job->updateAttributes(['auto_translated' => true]);
                            }
                        } catch (\Exception $e) {
                            Yii::error("Исключение при сохранении заявки \n" . $e->getTraceAsString());
                        }
                    }
                }
            }
        }
        var_dump("success");
        die;
    }

    /**
     * Automatically translate tenders
     */
    public function actionTranslateProfiles()
    {
        $translator = new YandexTranslatorService();
        $languageAttributes = [
            'en-GB' => 'english',
//            'uk-UA' => 'ukrainian',
            'es-ES' => 'spanish',
//            'ka-GE' => 'georgian',
            'zh-CN' => 'chinese',
        ];
        $locales = Yii::$app->params['bigLocales'];
        unset($locales['ru-RU']);
//        $locales['uk-UA'] = 'uk';
        /* @var $profiles Profile[] */
        $profiles = Profile::find()
            ->joinWith(['user.userAttributes', 'translations'])
            ->where(['profile.status' => Profile::STATUS_ACTIVE, 'user_attribute.value_alias' => $languageAttributes])
            ->andWhere(['not exists', ContentTranslation::find()
                ->from(['ct' => 'content_translation'])
                ->andWhere("profile.user_id = ct.entity_id")
                ->andWhere(["ct.entity" => 'profile', 'ct.locale' => array_keys($locales)])
            ])
            ->andWhere(['or',
                ['and',
                    ['not', ['bio' => '']],
                    ['not', ['bio' => null]],
                    ['not', ['name' => '']],
                    ['not', ['name' => null]]
                ],
                ['exists', ContentTranslation::find()
                    ->from(['ct' => 'content_translation'])
                    ->andWhere("profile.user_id = ct.entity_id")
                    ->andWhere(["ct.entity" => 'profile', 'ct.locale' => 'ru-RU'])
                ]
            ])
            ->limit(5)
            ->groupBy('profile.user_id')
            ->all();
        if (!count($profiles)) {
            Yii::error('Профили закончились, айайай');
            die;
        }
        foreach ($profiles as $profile) {
            $userLanguages = ArrayHelper::getColumn(array_filter($profile->user->userAttributes, function ($var) {
                return $var->entity_alias === 'language';
            }), 'value_alias');
            foreach ($locales as $locale => $code) {
                if (in_array($languageAttributes[$locale], $userLanguages)) {
                    $name = $profile->translations['ru-RU']->title ?? $profile->name;
                    $bio = $profile->translations['ru-RU']->content ?? $profile->bio;
                    $translation = new ContentTranslation([
                        'entity' => 'profile',
                        'entity_id' => $profile->user_id,
                        'locale' => $locale,
                        'title' => StringHelper::truncate($translator->translate($name, 'ru', $code), 79, ''),
                        'content' => $translator->translate($bio, 'ru', $code)
                    ]);

                    // Сохраняем перевод
                    try {
                        if (!$translation->save()) {
                            Yii::error("Ошибка при сохранении перевода профиля \n" . VarDumper::export($translation->errors));
                        }
                    } catch (\Exception $e) {
                        Yii::error("Исключение при сохранении перевода профиля \n" . $e->getTraceAsString());
                    }
                }
            }
            $profile->updateElastic();
        }
        var_dump("success");
        die;
    }

    public function actionRestoreAttachments()
    {
        /* @var Service $s3 */
        /* @var Job|Product $entity */
        $year = date('Y');
        $month = date('m');
        $s3 = Yii::$app->get('s3');
        $utility = Yii::$app->get('utility');
        $jobs = Job::find()->joinWith(['translation', 'attachments'])->where(['like', 'attachment.content', 'temp'])->indexBy(function ($row) {
            return 'j' . $row['id'];
        })->all();
        $products = Product::find()->joinWith(['translation', 'attachments'])->where(['like', 'attachment.content', 'temp'])->indexBy(function ($row) {
            return 'p' . $row['id'];
        })->all();
        $entities = array_merge($jobs, $products);

        foreach ($entities as $index => $entity) {
            $folder = ($entity instanceof Job) ? 'job' : 'product';
            $changed = false;
            foreach ($entity->attachments as $id => $attachment) {
                $path = Yii::getAlias('@frontend') . '/web' . $attachment->content;
                if (preg_match('/^\/uploads\/temp\/.*/', $attachment->content) && !file_exists($path)) {
                    if ($s3->commands()->exist($utility->fixAwsMediaPath($attachment->content))->execute()) {
                        $newDir = Yii::getAlias('@frontend') . '/web' . "/uploads/{$folder}/{$year}/{$month}/";
                        if (!is_dir($newDir)) {
                            mkdir($newDir, 0777, true);
                            chown($newDir, 'www-data');
                            chgrp($newDir, 'www-data');
                        }

                        $pathInfo = pathinfo($attachment->content);
                        $newPath = "/uploads/{$folder}/{$year}/{$month}/" . $utility->transliterate(
                                StringHelper::truncateWords(preg_replace('/\s+/', ' ', trim($entity->getLabel())), 5, '')
                            ) . "-" . ($entity->id) . "-" . ($id + 1) . "." . ($pathInfo['extension'] ? $pathInfo['extension'] : 'jpg');
                        $s3->commands()->get($utility->fixAwsMediaPath($attachment->content))->saveAs(Yii::getAlias("@frontend") . '/web' . $newPath)->execute();
                        $newAttachment = new Attachment([
                            'entity' => $folder,
                            'entity_id' => $entity->id,
                            'content' => $newPath
                        ]);
                        $newAttachment->save();
                        var_dump('downloaded');
                    } else {
                        var_dump('deleted');
                    }
                    $attachment->delete();
                    $changed = true;
                }
            }
            if ($changed) {
                unset($entity->attachments);
                if ($entity->type == Job::TYPE_JOB) {
                    $entity->defineStatus();
                    $entity->defineRating();
                }
                if ($entity->type == Product::TYPE_PRODUCT) {
                    $entity->defineStatus();
                }
                $entity->updateElastic();
            }
        }
        var_dump('success');
        die;
    }

    public function actionResendReferralMessage()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        $time = time();
        $limit = 10;
        /* @var $referralsWeek Referral[] */
        /* @var $referralsMonth Referral[] */
        $referralsWeek = Referral::find()
            ->where(['status' => Referral::STATUS_SENT])
            ->andWhere(['<=', 'created_at', $time - 60 * 60 * 24 * 7])
            ->andWhere([
                'not exists',
                Event::find()->where('event.entity_content = referral.id')->andWhere(['entity' => 'referral', 'code' => Event::REFERRAL_MESSAGE_WEEK])
            ])
            ->limit($limit)
            ->all();
        foreach ($referralsWeek as $referral) {
            (new Event(['blameable' => false, 'entity' => 'referral', 'entity_content' => (string)$referral->id, 'user_id' => $referral->created_by, 'code' => Event::REFERRAL_MESSAGE_WEEK]))->save();
            $mailer->compose(['html' => 'referral'], ['user' => $referral->createdBy])
                ->setTo([$referral->email])
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]))
                ->send();
        }
        $limit -= count($referralsWeek);
        if ($limit > 0) {
            $referralsMonth = Referral::find()
                ->where(['status' => Referral::STATUS_SENT])
                ->andWhere(['<=', 'created_at', $time - 60 * 60 * 24 * 30])
                ->andWhere(['exists', Event::find()
                    ->where('event.entity_content = referral.id')
                    ->andWhere(['entity' => 'referral', 'code' => Event::REFERRAL_MESSAGE_WEEK])
                    ->andWhere(['<=', 'event.created_at', $time - 60 * 60 * 24 * 21])
                ])
                ->andWhere([
                    'not exists',
                    Event::find()->where('event.entity_content = referral.id')->andWhere(['entity' => 'referral', 'code' => Event::REFERRAL_MESSAGE_MONTH])
                ])
                ->limit($limit)
                ->all();
            foreach ($referralsMonth as $referral) {
                (new Event(['blameable' => false, 'entity' => 'referral', 'entity_content' => (string)$referral->id, 'user_id' => $referral->created_by, 'code' => Event::REFERRAL_MESSAGE_MONTH]))->save();
                $mailer->compose(['html' => 'referral'], ['user' => $referral->createdBy])
                    ->setTo([$referral->email])
                    ->setFrom(Yii::$app->params['ujobsNoty'])
                    ->setSubject(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]))
                    ->send();
            }
        }
        var_dump('success');
    }

    /**
     * @param int $limit
     */
    public function actionUploadAttachments($limit = 20)
    {
        $offset = 0;
        /* @var $attachments Attachment[] */
        while($limit > 0) {
            $attachments = Attachment::find()->where(['like', 'content', '/uploads/temp/%', false])->limit($limit)->offset($offset)->all();
            $count = count($attachments);
            if ($count === 0) {
                break;
            }
            $i = 0;

            Console::output("Trying to upload {$count} attachments...");
            Console::startProgress(0, $count);
            foreach ($attachments as $attachment) {
                if (file_exists(Yii::getAlias('@frontend') . '/web' . $attachment->content)) {
                    $year = date('Y');
                    $month = date('m');
                    $basePath = "/uploads/{$attachment->entity}/{$year}/{$month}/";
                    $dir = Yii::getAlias('@frontend') . '/web' . $basePath;
                    if (!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                        chown($dir, 'www-data');
                        chgrp($dir, 'www-data');
                    }
                    $pathInfo = pathinfo($attachment->content);
                    $newPath = $basePath . $attachment->entity . "-" . $attachment->entity_id . "-" . $attachment->id . "." . ($pathInfo['extension'] ? $pathInfo['extension'] : 'jpg');
                    rename(Yii::getAlias("@frontend") . '/web' . $attachment->content, Yii::getAlias("@frontend") . '/web' . $newPath);

                    $attachment->content = $newPath;
                    if ($attachment->save()) {
                        Yii::$app->mediaLayer->saveToAws($attachment->content);
                        $limit--;
                    }
                    $i++;
                    Console::updateProgress($i, $count);
                }
            }
            $offset+= $limit;
            Console::endProgress();
        }
        var_dump("success");
    }

    /**
     * @param int $limit
     */
    public function actionUploadVideos($limit = 5)
    {
        $offset = 0;
        /* @var $videos Video[] */
        while($limit > 0) {
            $videos = Video::find()->where(['like', 'video', '/uploads/temp/%', false])->limit($limit)->offset($offset)->all();
            $count = count($videos);
            if ($count === 0) {
                break;
            }
            $i = 0;

            Console::output("Trying to upload {$count} videos...");
            Console::startProgress(0, $count);
            foreach ($videos as $video) {
                if (file_exists(Yii::getAlias('@frontend') . '/web' . $video->video)) {
                    $year = date('Y');
                    $month = date('m');
                    $basePath = "/uploads/video/{$year}/{$month}/";
                    $dir = Yii::getAlias('@frontend') . '/web' . $basePath;
                    if (!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                        chown($dir, 'www-data');
                        chgrp($dir, 'www-data');
                    }
                    $pathInfo = pathinfo($video->video);
                    $newPath = $basePath . "video-" . $video->id . '-' . uniqid() . "." . $pathInfo['extension'];
                    rename(Yii::getAlias("@frontend") . '/web' . $video->video, Yii::getAlias("@frontend") . '/web' . $newPath);

                    $video->video = $newPath;
                    if ($video->save()) {
                        Yii::$app->mediaLayer->saveToAws($video->video);
                        $limit--;
                    }
                    $i++;
                    Console::updateProgress($i, $count);
                }
            }
            $offset+= $limit;
            Console::endProgress();
        }
        var_dump("success");
    }

    public function actionIpToCountry()
    {
        $users = User::find()->all();
        $countries = AttributeDescription::find()
            ->select(['mod_attribute_value.id', 'mod_attribute_description.title'])
            ->joinWith(['attributeValue.relatedAttribute'])
            ->where(['mod_attribute.alias' => 'country'])
            ->indexBy('title')
            ->column();
        $aliases = AttributeValue::find()
            ->select(['mod_attribute_value.alias', 'mod_attribute_value.id'])
            ->joinWith(['relatedAttribute'])
            ->where(['mod_attribute.alias' => 'country'])
            ->indexBy('id')
            ->column();
        $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-City.mmdb');
        $i = 0;
        $userAttributes = [];
        foreach ($users as $user) {
            /* @var $user User */
            if (!empty($user->registration_ip) && $user->registration_ip != '127.0.0.1') {
                $data = $geoIpReader->get($user->registration_ip);
                $country = $data['country']['names']['ru'] ?? $data['country']['names']['en'] ?? null;
                if (isset($countries[$country])) {
                    $i++;
                    if (!UserAttribute::find()->where(['user_id' => $user->id, 'entity_alias' => 'country'])->exists()) {
                        $userAttributes[] = [
                            $user->id,
                            '51',
                            $countries[$country],
                            'ru-RU',
                            'country',
                            $aliases[$countries[$country]]
                        ];
                    }
                }
            }
        }
        Yii::$app->db->createCommand()->batchInsert('user_attribute',
            ['user_id', 'attribute_id', 'value', 'locale', 'entity_alias', 'value_alias'],
            $userAttributes
        )->execute();
        var_dump($i);
        var_dump($i / count($users) * 100 . '%');
        die;
    }

    /**
     * Get missing translations for addresses from geocoder
     * @param int $limit
     */
    public function actionAddressTranslations($limit = 50)
    {
        $gmapsService = new GoogleMapsService();
        $locales = Yii::$app->params['supportedLocales'];
        $translations = AddressTranslation::find()
            ->groupBy(['lat', 'long'])
            ->having(['<', 'count(locale)', count($locales)])
            ->limit($limit)
            ->all();
        $count = count($translations);
        Console::output("Trying to translate {$count} addresses...");
        Console::startProgress(0, $count);
        $i = 1;
        foreach ($translations as $translation) {
            /* @var $translation AddressTranslation*/
            foreach (array_diff($locales, [$translation->locale]) as $locale => $code) {
                if (!AddressTranslation::find()->where(['lat' => $translation->lat, 'long' => $translation->long, 'locale' => $locale])->exists()) {
                    $address = $gmapsService->reverseGeocode($translation->lat, $translation->long, $code);
                    if ($address !== false) {
                        (new AddressTranslation(['lat' => $translation->lat, 'long' => $translation->long, 'locale' => $locale, 'title' => $address]))->save();
                    }
                }
            }
            Console::updateProgress($i, $count);
            $i++;
        }
        Console::endProgress();
        var_dump("success");
    }

    public function actionEmailInvites($limit = 100) {
        /* @var $emails EmailImport[]*/
        /* @var $user User*/
        $receivers = EmailImport::find()->where(['mail_sent' => false])->limit($limit)->all();
        $user = User::findOne(36461);
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@frontend/views/email';
        foreach ($receivers as $receiver) {
            $receiver->updateAttributes(['mail_sent' => true]);
            $email = trim($receiver->email);
            $referral = Referral::findOrCreate(['email' => $email, 'created_by' => $user->getCurrentId()], false);
            if ($referral->isNewRecord) {
                $referral->save();
            } else {
                (new Event(['entity' => 'referral', 'entity_content' => (string)$referral->id, 'code' => Event::REFERRAL_RESENT]))->save();
            }

            $emailLog = new EmailLog([
                'email' => $email,
                'customDataArray' => [
                    'mailer' => 'mailer',
                    'viewPath' => '@frontend/views/email',
                    'view' => 'referral',
                    'subject' => Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]),
                    'receiverEmail' => $email,
                    'user' => $user->getCurrentId(),
                    'language' => 'ru-RU'
                ]
            ]);
            $emailLog->save();

            $mailer->compose(['html' => 'referral'], ['user' => $user, 'language' => 'ru-RU'])
                ->setTo([$email])
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'Invitation to register on {site}', ['site' => Yii::$app->name]))
                ->send();
        }
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionFixLatLong($limit = 10)
    {
        $profiles = Profile::find()->joinWith(['userAttributes.attrValue', 'userAttributes.attrValueDescriptions'])->where(['or',
            ['lat' => null],
            ['lat' => ''],
            ['long' => null],
            ['long' => ''],
        ])->andWhere(['or',
            ['and',
                ['exists', UserAttribute::find()
                    ->select('user_attribute.id')
                    ->where('user_attribute.user_id = profile.user_id')
                    ->andWhere(['user_attribute.entity_alias' => 'city'])
                ],
                ['exists', UserAttribute::find()
                    ->select('user_attribute.id')
                    ->where('user_attribute.user_id = profile.user_id')
                    ->andWhere(['user_attribute.entity_alias' => 'country'])
                ]
            ],
            ['exists', UserAttribute::find()
                ->select('user_attribute.id')
                ->joinWith('attrValue')
                ->where('user_attribute.user_id = profile.user_id')
                ->andWhere(['user_attribute.entity_alias' => 'city'])
                ->andWhere(['not', ['mod_attribute_value.parent_value_id' => '']])
                ->andWhere(['not', ['mod_attribute_value.parent_value_id' => null]])
            ],
        ])->groupBy('profile.user_id')->limit($limit)->all();
        $count = count($profiles);
        Console::output("{$count} profiles to fix");

        $gmapsService = new GoogleMapsService();
        foreach($profiles as $profile) {
            /* @var Profile $profile*/
            $attributes = ArrayHelper::map($profile->userAttributes, 'value_alias', function($var){
                /* @var UserAttribute $var*/
                $translation = $var->attrValueDescriptions['ru-RU'] ?? array_pop($var->attrValueDescriptions);
                $var->attrValue->parent_value_id;
                return [
                    'title' => $translation->title ?? '',
                    'parent' => $var->attrValue->parent_value_id
                ];
            }, 'entity_alias');
            if (!empty($attributes['city'])) {
                $cityItem = array_pop($attributes['city']);
                $city = $cityItem['title'];
                if (!empty($attributes['country'])) {
                    $country = array_pop($attributes['country'])['title'];
                }
                else {
                    $country = AttributeDescription::find()
                        ->select(['title', 'locale', 'entity', 'entity_content'])
                        ->where(['entity' => 'attribute-value', 'entity_content' => $cityItem['parent']])
                        ->orderBy(new Expression("FIELD (locale, 'ru-RU') DESC"))
                        ->scalar();
                }
                $address = $country . ($country && $city ? ", {$city}" : $city);
                $result = $gmapsService->geocode($address);
                if (isset($result['lat'], $result['long'])) {
                    $profile->updateAttributes(['lat' => $result['lat'], 'long' => $result['long']]);
                }
            }
        }

        Console::output("Done");
        return true;
    }
}