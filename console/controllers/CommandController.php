<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.11.2016
 * Time: 16:13
 */

namespace console\controllers;

use common\models\Category;
use common\models\ContentTranslation;
use common\models\Notification;
use common\models\Tariff;
use common\models\user\Profile;
use common\models\UserTariff;
use common\modules\attribute\models\AttributeGroup;
use common\modules\board\models\Product;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class CommandController
 * @package console\controllers
 */
class CommandController extends Controller
{
    /**
     * @param int $userId
     * @param int $tariffId
     * @param int $days
     * @return bool
     */
    public function actionAddTariff(int $userId, int $tariffId, int $days)
    {
        /** @var Profile $profile */
        $profile = Profile::find()->joinWith(['jobs', 'products'])->where(['profile.user_id' => $userId])->one();

        /** @var Tariff $tariff */
        $tariff = Tariff::find()->where(['id' => $tariffId])->one();

        if ($profile !== null && $tariff !== null) {
            Yii::$app->db->createCommand()->insert('user_tariff',
                [
                    'user_id' => $profile->user_id,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'cost' => $tariff->cost * $days,
                    'duration' => $days * 60 * 60 * 24,
                    'expires_at' => $days * 60 * 60 * 24 + time(),
                    'starts_at' => time(),
                    'tariff_id' => $tariffId,
                    'currency_code' => 'RUB',
                    'status' => 10
                ]
            )->execute();

            $profile->updateElastic();
            foreach ($profile->jobs as $job) {
                $job->updateElastic();
            }

            foreach ($profile->products as $product) {
                $product->updateElastic();
            }

            return true;
        }

        return false;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function actionRemoveTariff($userId)
    {
        /** @var Profile $profile */
        $profile = Profile::find()->joinWith(['jobs'])->where(['profile.user_id' => $userId])->one();

        if ($profile !== null) {
            UserTariff::updateAll(['status' => UserTariff::STATUS_EXPIRED], ['user_id' => $userId]);

            $profile->updateElastic();
            foreach ($profile->jobs as $job) {
                $job->updateElastic();
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool|false|int
     */
    public function actionMergeCategories()
    {
        $result = false;
        $merges = [
            2041 => 2546,
        ];

        foreach ($merges as $key => $merge) {
            /** @var Category $category */
            $category = Category::findOne($merge);
            /** @var Category $oldCategory */
            $oldCategory = Category::findOne($key);

            $set = null;
            $condition = null;

            if ($category !== null && $oldCategory !== null) {
                if ($category->lvl === 3) {
                    $condition = [
                        'subcategory_id' => $oldCategory->id
                    ];

                    $parents = $category->parents()->andWhere(['not', ['lvl' => 0]])->select('id')->orderBy('lvl asc')->indexBy('lvl')->column();

                    $set = [
                        'subcategory_id' => $category->id,
                        'category_id' => $parents[2],
                        'parent_category_id' => $parents[1]
                    ];
                } else if ($category->lvl === 2) {
                    $condition = [
                        'category_id' => $oldCategory->id
                    ];

                    $parents = $category->parents()->andWhere(['not', ['lvl' => 0]])->select('id')->orderBy('lvl asc')->indexBy('lvl')->column();

                    $set = [
                        'category_id' => $category->id,
                        'parent_category_id' => $parents[1]
                    ];
                } else if ($category->lvl === 1) {
                    $condition = [
                        'parent_category_id' => $oldCategory->id
                    ];

                    $set = [
                        'parent_category_id' => $category->id
                    ];
                }

                if (!empty($set) && !empty($condition)) {
                    Product::updateAll($set, $condition);
                }

                $result = $oldCategory->delete();
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function actionClearCategoryTranslations()
    {
        $translations = ContentTranslation::find()->where(['entity' => 'category'])->all();

        foreach ($translations as $translation) {
            $category = Category::findOne(['id' => $translation->entity_id]);

            if ($category === null) {
                $translation->delete();

                Console::output("Removed translation {$translation->slug} {$translation->entity_id}");
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionNotificationConversationLinks()
    {
        /* @var $notifications Notification[] */
        $notifications = Notification::find()->where(['like', 'link', '/conversation/'])->all();

        foreach ($notifications as $notification) {
            if (preg_match('/^\/conversation\/(\d+)\/(\d+)(#message-\d+)?$/ui', $notification->link)) {
                $notification->updateAttributes(['link' => preg_replace('/^\/conversation\/(\d+)\/(\d+)(#message-\d+)?$/ui', "/conversation/\\2\\3", $notification->link)]);
            } else if (preg_match('/^\/conversation\/(\d+)(#message-\d+)?$/ui', $notification->link)) {
                $notification->updateAttributes(['link' => preg_replace('/^\/conversation\/(\d+)(#message-\d+)?$/ui', "/account/inbox/start-conversation?user_id=\\1", $notification->link)]);
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionClearAttributeGroups()
    {
        $connectedAttributeGroups = Category::find()->select('attribute_set_id')->where(['not', ['attribute_set_id' => null]])->column();

        $groupsToDelete = AttributeGroup::find()->where(['not', ['id' => $connectedAttributeGroups]])->all();
        $count = count($groupsToDelete);
        Console::output("{$count} groups to delete");

        foreach($groupsToDelete as $group) {
            $group->delete();
        }

        Console::output("Done");
        return true;
    }
}