<?php

use yii\db\Migration;

class m170403_135813_add_updated_at_to_cart extends Migration
{
    public function up()
    {
        $this->addColumn('cart', 'updated_at', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('cart', 'updated_at');
    }
}
