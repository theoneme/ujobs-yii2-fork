<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 */
class m161216_143756_create_review_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('review', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'text' => $this->text(),
            'rating' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);


        $this->addForeignKey('fk_review_created_by', 'review', 'created_by', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_review_updated_by', 'review', 'updated_by', 'user', 'id', 'CASCADE');

        $this->createIndex('review_rating', 'review', 'rating');
        $this->createIndex('review_created_at', 'review', 'created_at');
        $this->createIndex('review_updated_at', 'review', 'updated_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('review');
    }
}
