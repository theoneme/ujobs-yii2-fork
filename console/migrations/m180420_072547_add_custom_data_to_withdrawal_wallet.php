<?php

use yii\db\Migration;

/**
 * Class m180420_072547_add_custom_data_to_withdrawal_wallet
 */
class m180420_072547_add_custom_data_to_withdrawal_wallet extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('withdrawal_wallet', 'custom_data', $this->binary());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('withdrawal_wallet', 'custom_data');
    }
}
