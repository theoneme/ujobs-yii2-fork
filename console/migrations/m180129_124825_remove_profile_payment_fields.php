<?php

use common\models\user\Profile;
use common\models\WithdrawalWallet;
use yii\db\Migration;

/**
 * Class m180129_124825_remove_profile_payment_fields
 */
class m180129_124825_remove_profile_payment_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        WithdrawalWallet::deleteAll();
        $profiles = Profile::find()->where(['or',
            ['not', ['payment_qiwi' => null]],
            ['not', ['payment_yandex_money' => null]],
            ['not', ['payment_card' => null]],
            ['not', ['payment_webmoney' => null]],
        ])->all();
        foreach ($profiles as $profile) {
            /* @var $profile Profile*/
            if (!empty($profile->payment_qiwi)) {
                (new WithdrawalWallet(['user_id' => $profile->user_id, 'type' => WithdrawalWallet::TYPE_QIWI, 'number' => $profile->payment_qiwi]))->save();
            }
            if (!empty($profile->payment_yandex_money)) {
                (new WithdrawalWallet(['user_id' => $profile->user_id, 'type' => WithdrawalWallet::TYPE_YANDEX, 'number' => $profile->payment_yandex_money]))->save();
            }
            if (!empty($profile->payment_card)) {
                (new WithdrawalWallet(['user_id' => $profile->user_id, 'type' => WithdrawalWallet::TYPE_CREDIT_CARD, 'number' => $profile->payment_card]))->save();
            }
            if (!empty($profile->payment_webmoney)) {
                (new WithdrawalWallet(['user_id' => $profile->user_id, 'type' => WithdrawalWallet::TYPE_WEBMONEY, 'number' => $profile->payment_webmoney]))->save();
            }
        }
        $this->dropColumn('profile', 'payment_qiwi');
        $this->dropColumn('profile', 'payment_yandex_money');
        $this->dropColumn('profile', 'payment_card');
        $this->dropColumn('profile', 'payment_webmoney');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('profile', 'payment_qiwi', $this->string(100));
        $this->addColumn('profile', 'payment_yandex_money', $this->string(100));
        $this->addColumn('profile', 'payment_card', $this->string(100));
        $this->addColumn('profile', 'payment_webmoney', $this->string(100));

        $wallets = WithdrawalWallet::find()->all();
        foreach ($wallets as $wallet) {
            /* @var $wallet WithdrawalWallet*/
            switch ($wallet->type) {
                case WithdrawalWallet::TYPE_QIWI:
                    $wallet->profile->updateAttributes(['payment_qiwi' => $wallet->number]);
                    break;
                case WithdrawalWallet::TYPE_YANDEX:
                    $wallet->profile->updateAttributes(['payment_yandex_money' => $wallet->number]);
                    break;
                case WithdrawalWallet::TYPE_WEBMONEY:
                    $wallet->profile->updateAttributes(['payment_webmoney' => $wallet->number]);
                    break;
                case WithdrawalWallet::TYPE_CREDIT_CARD:
                    $wallet->profile->updateAttributes(['payment_card' => $wallet->number]);
                    break;
            }
        }
    }
}
