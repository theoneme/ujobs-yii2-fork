<?php

use yii\db\Migration;

class m170315_082732_order_alter extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'is_price_approved', $this->boolean());
        $this->createIndex('is_price_approved_index', 'order', 'is_price_approved');
    }

    public function down()
    {
        $this->dropColumn('order', 'is_price_approved');
    }
}
