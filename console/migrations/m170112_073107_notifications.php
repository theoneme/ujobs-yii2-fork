<?php

use yii\db\Migration;

class m170112_073107_notifications extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification}}', [
            'id' => $this->primaryKey(),
            'to_id' => $this->integer(),
            'from_id' => $this->integer(),
            'link' => $this->string(125),
            'thumb' => $this->string(155),
            'subject' => $this->integer(),
            'message' => $this->text(),
            'sender' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->createIndex('subject', 'notification', 'subject');
        $this->createIndex('to_id', 'notification', 'to_id');
        $this->createIndex('from_id', 'notification', 'from_id');
        $this->createIndex('created_at', 'notification', 'created_at');
        $this->createIndex('updated_at', 'notification', 'updated_at');

        $this->addForeignKey('fk_notification_to_user', 'notification', 'to_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_notification_from_user', 'notification', 'from_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('notification');
    }
}
