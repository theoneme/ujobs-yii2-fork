<?php

use yii\db\Migration;

class m170915_102522_db_fix extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('product_attribute', 'entity_alias', $this->string(55));
    }

    public function safeDown()
    {
        $this->alterColumn('product_attribute', 'entity_alias', $this->string(35));
    }
}
