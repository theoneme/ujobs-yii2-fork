<?php

use yii\db\Migration;

class m170302_064027_conversation_rules extends Migration
{
    public function up()
    {
        $this->insert('tariff_rule', [
            'tariff_id' => 1,
            'code' => 'new_conversations_per_day',
            'value' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'tariff_id' => 2,
            'code' => 'new_conversations_per_day',
            'value' => -1,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'tariff_id' => 3,
            'code' => 'new_conversations_per_day',
            'value' => -1,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);
    }

    public function down()
    {

    }
}
