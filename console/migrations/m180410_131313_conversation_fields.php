<?php

use yii\db\Migration;

/**
 * Class m180410_131313_conversation_fields
 */
class m180410_131313_conversation_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('conversation', 'avatar', $this->string(155)->defaultValue(null));
        $this->addColumn('conversation', 'title', $this->string(75)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('conversation', 'avatar');
        $this->dropColumn('conversation', 'title');
    }
}
