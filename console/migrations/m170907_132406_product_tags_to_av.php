<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use yii\db\Migration;

class m170907_132406_product_tags_to_av extends Migration
{
    public function safeUp()
    {
        $textAttributes = ProductAttribute::find()->where(['entity_alias' => 'condition'])->all();

        $conditionAttribute = Attribute::find()->where(['alias' => 'condition'])->one();
        $conditionNew = AttributeValue::find()->where(['attribute_id' => $conditionAttribute->id, 'alias' => 'new'])->one();
        $conditionOld = AttributeValue::find()->where(['attribute_id' => $conditionAttribute->id, 'alias' => 'used'])->one();
        if($textAttributes !== null) {
            foreach($textAttributes as $textAttribute) {
                switch($textAttribute->value) {
                    case 'Новый':
                        $textAttribute->updateAttributes(['value' => $conditionNew->id, 'is_text' => false, 'value_alias' => 'new']);
                        break;
//                    case 'Б/у':
//                        $textAttribute->updateAttributes(['value' => $conditionOld->id, 'is_text' => false, 'value_alias' => 'used']);
//                        break;
                }
            }
        }

        $colors = ProductAttribute::find()->where(['entity_alias' => 'color'])->all();
        $attribute = Attribute::find()->where(['alias' => 'color'])->one();

        if(!empty($colors) && $attribute !== null) {
            foreach($colors as $color) {
                $currentValue = AttributeValue::find()->where(['alias' => $color->value_alias])->one();

                if($currentValue !== null) {
                    $color->updateAttributes(['value' => $currentValue->id, 'is_text' => false, 'entity_alias' => 'color']);
                } else {
                    $attrValue = new AttributeValue([
                        'attribute_id' => $attribute->id,
                        'translationsArr' => [
                            [
                                'title' => Yii::$app->utility->upperFirstLetter($color->value),
                                'locale' => $color->locale
                            ],
                        ]
                    ]);
                    if($attrValue->save()) {
                        $color->updateAttributes(['value' => $attrValue->id, 'is_text' => false, 'entity_alias' => 'color']);
                    }
                }
            }
        }

        $materials = ProductAttribute::find()->where(['entity_alias' => 'material'])->all();
        $attribute = Attribute::find()->where(['alias' => 'material'])->one();

        if(!empty($materials) && $attribute !== null) {
            foreach($materials as $material) {
                $currentValue = AttributeValue::find()->where(['alias' => $material->value_alias])->one();

                if($currentValue !== null) {
                    $material->updateAttributes(['value' => $currentValue->id, 'is_text' => false]);
                } else {
                    $attrValue = new AttributeValue([
                        'attribute_id' => $attribute->id,
                        'translationsArr' => [
                            [
                                'title' => Yii::$app->utility->upperFirstLetter($material->value),
                                'locale' => $material->locale
                            ],
                        ]
                    ]);
                    if($attrValue->save()) {
                        $material->updateAttributes(['value' => $attrValue->id, 'is_text' => false]);
                    }
                }
            }
        }

        $brands = ProductAttribute::find()->where(['entity_alias' => 'brand_a'])->all();
        $attribute = Attribute::find()->where(['alias' => 'brand_a'])->one();

        if(!empty($brands) && $attribute !== null) {
            foreach($brands as $brand) {
                $currentValue = AttributeValue::find()->where(['alias' => $brand->value_alias])->one();

                if($currentValue !== null) {
                    $brand->updateAttributes(['value' => $currentValue->id, 'is_text' => false]);
                } else {
                    $attrValue = new AttributeValue([
                        'attribute_id' => $attribute->id,
                        'translationsArr' => [
                            [
                                'title' => Yii::$app->utility->upperFirstLetter($brand->value),
                                'locale' => $brand->locale
                            ],
                        ]
                    ]);
                    if($attrValue->save()) {
                        $brand->updateAttributes(['value' => $attrValue->id, 'is_text' => false]);
                    }
                }
            }
        }

        Yii::$app->db->createCommand('delete `product_attribute` from `product_attribute` left join mod_attribute_value as v
            on product_attribute.value_alias = v.alias and product_attribute.attribute_id = v.attribute_id
            WHERE v.alias is NULL')->execute();
        $this->addForeignKey('fk_product_attribute_value_alias', 'product_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        AttributeValue::updateAll(['status' => AttributeValue::STATUS_APPROVED]);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_attribute_value_alias', 'product_attribute');
    }
}
