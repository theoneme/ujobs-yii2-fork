<?php

use yii\db\Migration;

class m170206_064155_order_product_alter extends Migration
{
    public function up()
    {
        $this->alterColumn('order_product', 'title', $this->string(255));
    }

    public function down()
    {
        $this->alterColumn('order_product', 'title', $this->string(75));
    }
}
