<?php

use yii\db\Migration;

class m170227_135846_user_tariff_alter extends Migration
{
    public function up()
    {
        $this->addColumn('user_tariff', 'payment_id', $this->integer());
        $this->addForeignKey('fk_user_tariff_payment', 'user_tariff', 'payment_id', 'payment', 'id', 'CASCADE');

        $this->addColumn('user_tariff', 'status', $this->integer(2));
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_tariff_payment', 'user_tariff');
        $this->dropColumn('user_tariff', 'payment_id');
        $this->dropColumn('user_tariff', 'status');
    }
}
