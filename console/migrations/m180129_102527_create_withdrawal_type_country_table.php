<?php

use yii\db\Migration;

/**
 * Handles the creation of table `withdrawal_type_country`.
 */
class m180129_102527_create_withdrawal_type_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('withdrawal_type_country', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'country_id' => $this->integer(),
        ]);
        $this->addForeignKey('fk_withdrawal_type_country_country_id', 'withdrawal_type_country', 'country_id', 'mod_attribute_value', 'id', 'CASCADE');
        $this->createIndex('withdrawal_type_country_type_index', 'withdrawal_type_country', 'type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('withdrawal_type_country');
    }
}
