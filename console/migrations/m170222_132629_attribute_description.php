<?php

use yii\db\Migration;

class m170222_132629_attribute_description extends Migration
{
    public function up()
    {
        $this->addColumn('mod_attribute_description', 'description', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('mod_attribute_description', 'description');
    }
}
