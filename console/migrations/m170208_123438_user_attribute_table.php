<?php

use yii\db\Migration;

class m170208_123438_user_attribute_table extends Migration
{
    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable('{{%user_attribute}}', [
            'id' => $this->primaryKey(11),
            'entity' => $this->string(55)->notNull(),
            'entity_content' => $this->string(75)->null()->defaultValue(null),
            'user_id' => $this->integer(11)->notNull(),
            'key' => $this->string(55)->null()->defaultValue(null),
            'value' => $this->text()->null()->defaultValue(null),
            'locale' => $this->string(15)->notNull(),
            'is_text' => $this->integer(11)->null()->defaultValue(0),
            'entity_alias' => $this->string(75)->null()->defaultValue(null),
            'value_alias' => $this->string(75)->null()->defaultValue(null),
            'weight' => $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);
        $this->createIndex('entity', '{{%user_attribute}}', 'entity', false);
        $this->createIndex('entity_content', '{{%user_attribute}}', 'entity_content', false);
        $this->createIndex('is_text', '{{%user_attribute}}', 'is_text', false);
        $this->createIndex('entity_alias', '{{%user_attribute}}', 'entity_alias', false);
        $this->createIndex('value_alias', '{{%user_attribute}}', 'value_alias', false);
        $this->createIndex('fk_user_attr_job', '{{%user_attribute}}', 'user_id', false);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_attribute}}');
    }
}
