<?php

use yii\db\Migration;

/**
 * Handles the creation of table `support_request`.
 */
class m161220_072435_create_support_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('support_request', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'name' => $this->string(),
            'email' => $this->string(),
            'subject' => $this->string(),
            'status' => $this->integer(),
            'type' => $this->integer(),
            'description' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('fk_support_request_created_by', 'support_request', 'created_by', 'user', 'id', 'CASCADE');

        $this->createIndex('support_request_email', 'support_request', 'email');
        $this->createIndex('support_request_status', 'support_request', 'status');
        $this->createIndex('support_request_type', 'support_request', 'type');
        $this->createIndex('support_request_created_at', 'support_request', 'created_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('support_request');
    }
}
