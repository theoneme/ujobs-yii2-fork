<?php

use yii\db\Migration;

/**
 * Class m180221_113149_add_authorize_payment_method
 */
class m180221_113149_add_authorize_payment_method extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('payment_method', [
            'code' => 'authorize',
            'enabled' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('payment_method', ['code' => 'authorize']);
    }
}
