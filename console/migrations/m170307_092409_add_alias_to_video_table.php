<?php

use yii\db\Migration;

class m170307_092409_add_alias_to_video_table extends Migration
{
    public function up()
    {
        $this->addColumn('video', 'alias', $this->string());
    }

    public function down()
    {
        $this->dropColumn('video', 'alias');
    }
}
