<?php

use yii\db\Migration;

class m170228_122755_tariff_alter extends Migration
{
    public function up()
    {
        $this->addColumn('tariff', 'description', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('tariff', 'description');
    }
}
