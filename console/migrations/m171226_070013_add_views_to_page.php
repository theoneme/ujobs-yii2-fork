<?php

use yii\db\Migration;

/**
 * Class m171226_070013_add_views_to_page
 */
class m171226_070013_add_views_to_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('page', 'views', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('page', 'views');
    }
}
