<?php

use yii\db\Migration;

class m170710_131723_webpush extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'push_key', $this->string(100)->null());
        $this->addColumn('user', 'push_token', $this->string(100)->null());
        $this->addColumn('user', 'push_id', $this->string(255)->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'push_key');
        $this->dropColumn('user', 'push_token');
        $this->dropColumn('user', 'push_id');
    }
}
