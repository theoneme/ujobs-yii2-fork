<?php

use yii\db\Migration;

class m171116_073913_add_measure_to_job extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job', 'price_per_unit', $this->boolean()->defaultValue(false));
        $this->addColumn('job', 'measure', $this->string(50));
    }

    public function safeDown()
    {
        $this->dropColumn('job', 'price_per_unit');
        $this->dropColumn('job', 'measure');
    }
}
