<?php

use yii\db\Migration;

class m170407_101412_profile_gravater extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'gravatar_id', $this->string(32));
    }

    public function down()
    {
        $this->dropColumn('profile', 'gravatar_id');
    }
}
