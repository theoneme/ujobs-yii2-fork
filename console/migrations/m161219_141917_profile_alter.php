<?php

use yii\db\Migration;

class m161219_141917_profile_alter extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'rcv_sms_messages', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('profile', 'rcv_sms_messages');
    }
}
