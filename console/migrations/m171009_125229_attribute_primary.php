<?php

use yii\db\Migration;

class m171009_125229_attribute_primary extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('mod_attribute','is_parsed', 'is_primary');
        $this->alterColumn('mod_attribute', 'is_primary', $this->boolean()->defaultValue(true));
        \common\modules\attribute\models\Attribute::updateAll(['is_primary' => true]);
    }

    public function safeDown()
    {
        $this->renameColumn('mod_attribute','is_primary', 'is_parsed');
    }
}
