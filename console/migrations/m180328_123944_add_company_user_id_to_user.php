<?php

use yii\db\Migration;

/**
 * Class m180328_123944_add_company_user_id_to_user
 */
class m180328_123944_add_company_user_id_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'company_user_id', $this->integer());
        $this->addForeignKey('fk_user_company_user_id', 'user', 'company_user_id', 'user', 'id', 'SET NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_company_user_id', 'user');
        $this->dropColumn('user', 'company_user_id');
    }
}
