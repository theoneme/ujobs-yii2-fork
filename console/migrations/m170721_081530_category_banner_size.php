<?php

use yii\db\Migration;

class m170721_081530_category_banner_size extends Migration
{
    public function safeUp()
    {
        $this->addColumn('category', 'banner_size', $this->integer()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('category', 'banner_size');
    }
}
