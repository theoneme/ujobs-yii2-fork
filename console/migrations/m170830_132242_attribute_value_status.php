<?php

use yii\db\Migration;

class m170830_132242_attribute_value_status extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mod_attribute_value', 'status', $this->integer());

        $this->createIndex('mod_attribute_value_status_index', 'mod_attribute_value', 'status');
    }

    public function safeDown()
    {
        $this->dropColumn('mod_attribute_value', 'status');
    }
}
