<?php

use yii\db\Migration;

class m170215_080013_remove_username_unique_index extends Migration
{
    public function up()
    {
        $this->dropIndex('user_unique_username', 'user');
        $this->createIndex('user_username_index', 'user', 'username');
    }

    public function down()
    {
        $this->dropIndex('user_username_index', 'user');
        $this->createIndex('user_unique_username', 'user', 'username', true);
    }
}
