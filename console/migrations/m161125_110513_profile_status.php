<?php

use yii\db\Migration;

class m161125_110513_profile_status extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'status_text', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('profile', 'status_text');
    }
}
