<?php

use yii\db\Migration;

class m170811_102527_add_auto_translated_flag_to_job extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job', 'auto_translated', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('job', 'auto_translated');
    }
}
