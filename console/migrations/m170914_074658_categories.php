<?php

use common\models\Category;
use yii\db\Migration;

class m170914_074658_categories extends Migration
{
    public function safeUp()
    {
        $cat = Category::find()->where(['lvl' => 2, 'alias' => 'mebel'])->one();
        \common\models\ContentTranslation::updateAll(['slug' => 'mebel_old'], ['entity' => 'category', 'entity_id' => $cat->id]);
        Category::updateAll(['alias' => 'mebel_old'], ['lvl' => 2, 'alias' => 'mebel']);
    }

    public function safeDown()
    {

    }
}
