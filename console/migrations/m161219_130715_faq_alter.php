<?php

use yii\db\Migration;

class m161219_130715_faq_alter extends Migration
{
    public function up()
    {
        $this->alterColumn('job_qa', 'question', $this->text());
        $this->alterColumn('job_qa', 'answer', $this->text());
    }

    public function down()
    {
        $this->alterColumn('job_qa', 'question', $this->string(150));
        $this->alterColumn('job_qa', 'answer', $this->string(300));
    }
}
