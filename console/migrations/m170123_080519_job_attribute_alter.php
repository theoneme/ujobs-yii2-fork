<?php

use yii\db\Migration;

class m170123_080519_job_attribute_alter extends Migration
{
    public function up()
    {
        $this->createIndex('entity', 'job_attribute', 'entity');
        $this->createIndex('entity_content', 'job_attribute', 'entity_content');
        $this->createIndex('is_text', 'job_attribute', 'is_text');
        $this->createIndex('entity_alias', 'job_attribute', 'entity_alias');
        $this->createIndex('value_alias', 'job_attribute', 'value_alias');

        $this->addForeignKey('fk_job_attr_job', 'job_attribute', 'job_id', 'job', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropIndex('entity', 'job_attribute');
        $this->dropIndex('entity_content', 'job_attribute');
        $this->dropIndex('is_text', 'job_attribute');
        $this->dropIndex('entity_alias', 'job_attribute');
        $this->dropIndex('value_alias', 'job_attribute');

        $this->dropForeignKey('fk_job_attr_job', 'job_attribute');
    }
}
