<?php

use yii\db\Schema;
use yii\db\Migration;

class m161205_164011_job_requirement extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%job_requirement}}',
            [
                'id'=> $this->primaryKey(11),
                'requirement'=> $this->string(450)->notNull(),
                'answer_required'=> $this->smallInteger(1)->null()->defaultValue(null),
                'type'=> $this->string(15)->notNull(),
                'config'=> $this->text()->notNull(),
            ],$tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%job_requirement}}');
    }
}