<?php

use yii\db\Migration;

class m170913_103139_content_translation_cleanuo extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('delete `content_translation` from `content_translation` left join category as c
            on content_translation.entity_id = c.id
            WHERE c.id is NULL and content_translation.entity = "category"')->execute();
    }

    public function safeDown()
    {

    }
}
