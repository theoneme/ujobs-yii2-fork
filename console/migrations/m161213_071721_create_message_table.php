<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m161213_071721_create_message_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'status' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'message' => $this->text()
        ], $tableOptions);

        $this->addForeignKey('fk_message_from', 'message', 'from_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_message_to', 'message', 'to_id', 'user', 'id', 'CASCADE');

        $this->createIndex('message_status', 'message', 'status');
        $this->createIndex('message_created_at', 'message', 'created_at');
        $this->createIndex('message_updated_at', 'message', 'updated_at');
    }

    public function down()
    {
        $this->dropTable('message');
    }
}
