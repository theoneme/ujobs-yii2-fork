<?php

use yii\db\Migration;

class m170519_143958_skype_uid extends Migration
{
    public function up()
    {
	    $this->addColumn('user', 'skype_uid', $this->string(50));
	    $this->addColumn('user', 'skype_hash', $this->string(50));
    }

    public function down()
    {
	    $this->dropColumn('user', 'skype_uid');
	    $this->dropColumn('user', 'skype_hash');
    }
}
