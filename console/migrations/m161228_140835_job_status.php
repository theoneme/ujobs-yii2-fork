<?php

use yii\db\Migration;

class m161228_140835_job_status extends Migration
{
    public function up()
    {
        $this->alterColumn('job', 'status', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('job', 'status', $this->string(55));
    }
}
