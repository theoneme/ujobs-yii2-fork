<?php

use yii\db\Migration;

class m170324_093101_add_icon_to_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('tariff', 'icon', $this->string());
    }

    public function down()
    {
        $this->dropColumn('tariff', 'icon');
    }
}
