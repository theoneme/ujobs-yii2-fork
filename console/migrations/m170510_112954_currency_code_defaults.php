<?php

use common\models\Offer;
use common\models\PaymentHistory;
use common\modules\store\models\Order;
use yii\db\Migration;

class m170510_112954_currency_code_defaults extends Migration
{
    public function up()
    {
        Order::updateAll(['currency_code' => 'RUB'], ['currency_code' => null]);
        Offer::updateAll(['currency_code' => 'RUB'], ['currency_code' => null]);
        PaymentHistory::updateAll(['currency_code' => 'RUB'], ['currency_code' => null]);
    }

    public function down()
    {
        // Not sure if need to revert this
    }
}
