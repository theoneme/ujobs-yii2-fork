<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\db\Migration;

class m171019_122819_secure_filter_attribute extends Migration
{
    public function safeUp()
    {
        $attribute = new Attribute([
            'is_system' => true,
            'is_searchable' => true,
            'name' => 'С безопасной сделкой',
            'type' => 'switcher',
            'alias' => 'secure'
        ]);
        $attribute->translationsArr['ru-RU']->title = 'С безопасной сделкой';
        $attribute->translationsArr['en-GB']->title = 'With secure transaction';
        $attribute->translationsArr['es-ES']->title = 'With secure transaction';
        $attribute->translationsArr['uk-UA']->title = 'С безопасной сделкой';
        $attribute->save();

        $attrValue = (new AttributeValue([
            'attribute_id' => $attribute->id,
            'alias' => 'with_secure',
            'translationsArr' => [
                [
                    'title' => 'С безопасной сделкой',
                    'locale' => 'ru-RU'
                ],
            ]
        ]))->save();
    }

    public function safeDown()
    {
        $secureAttribute = Attribute::findOne(['alias' => 'secure']);
        if ($secureAttribute !== null) {
            $secureAttribute->delete();
        }
    }
}
