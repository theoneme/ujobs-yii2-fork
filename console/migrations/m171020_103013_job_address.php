<?php

use yii\db\Migration;

class m171020_103013_job_address extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job', 'address', $this->string(150)->null());
        $this->addColumn('job', 'lat', $this->string(15)->null());
        $this->addColumn('job', 'long', $this->string(15)->null());
    }

    public function safeDown()
    {
        $this->dropColumn('job', 'address');
        $this->dropColumn('job', 'lat');
        $this->dropColumn('job', 'long');
    }
}
