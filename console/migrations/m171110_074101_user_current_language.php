<?php

use common\models\user\User;
use yii\db\Migration;

class m171110_074101_user_current_language extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'site_language', $this->string(5)->null()->after('site_currency'));
        User::updateAll(['site_language' => 'ru-RU']);
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'site_language');
    }
}
