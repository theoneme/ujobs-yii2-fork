<?php

use yii\db\Migration;

/**
 * Class m180215_090057_job_period_type_default_value
 */
class m180215_090057_job_period_type_default_value extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('job', 'period_type', $this->string(15)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('job', 'period_type', $this->string(15));
    }
}
