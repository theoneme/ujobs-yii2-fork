<?php

use yii\db\Migration;

class m170713_103437_add_custom_data_to_seo_advanced extends Migration
{
    public function safeUp()
    {
        $this->addColumn('seo_advanced', 'custom_data', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('seo_advanced', 'custom_data');
    }
}
