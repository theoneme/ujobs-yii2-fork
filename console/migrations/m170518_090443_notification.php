<?php

use yii\db\Migration;

class m170518_090443_notification extends Migration
{
    public function up()
    {
		$this->addColumn('notification', 'is_visible', $this->boolean()->null()->defaultValue(1));
	    $this->createIndex('notification_is_visible_index', 'notification', 'is_visible');
    }

    public function down()
    {
        $this->dropColumn('notification', 'is_visible');
    }
}
