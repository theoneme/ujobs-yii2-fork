<?php

use yii\db\Migration;

/**
 * Handles the creation of table `property_client`.
 */
class m180716_063051_create_property_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('property_client', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(),
            'user_id' => $this->integer(),
            'email' => $this->string(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'phone' => $this->string(),
            'mortgage' => $this->boolean(),
            'info' => $this->text(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('property_client_status', 'property_client', 'status');

        $this->addForeignKey('fk_property_client_created_by', 'property_client', 'created_by', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_property_client_user_id', 'property_client', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('property_client');
    }
}
