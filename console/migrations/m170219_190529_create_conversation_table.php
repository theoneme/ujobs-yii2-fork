<?php

use yii\db\Migration;

/**
 * Handles the creation of table `conversation`.
 */
class m170219_190529_create_conversation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('conversation', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('conversation_created_at', 'conversation', 'created_at');
        $this->createIndex('conversation_updated_at', 'conversation', 'updated_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('conversation');
    }
}
