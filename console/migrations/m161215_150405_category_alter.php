<?php

use yii\db\Migration;

class m161215_150405_category_alter extends Migration
{
    public function up()
    {
        $this->addColumn('category', 'job_type', $this->string(20));
        $this->addColumn('category', 'job_shipment_available', $this->boolean());
        $this->addColumn('category', 'job_office_available', $this->boolean());
        $this->addColumn('category', 'job_remote_available', $this->boolean());
        $this->addColumn('category', 'order_asap_available', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('category', 'job_type');
        $this->dropColumn('category', 'job_shipment_available');
        $this->dropColumn('category', 'job_office_available');
        $this->dropColumn('category', 'job_remote_available');
        $this->dropColumn('category', 'order_asap_available');
    }
}
