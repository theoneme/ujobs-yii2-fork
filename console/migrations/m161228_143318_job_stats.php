<?php

use yii\db\Migration;

class m161228_143318_job_stats extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'click_count', $this->integer());
        $this->addColumn('job', 'views_count', $this->integer());
        $this->addColumn('job', 'orders_count', $this->integer());
        $this->addColumn('job', 'cancellations_count', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('job', 'click_count');
        $this->dropColumn('job', 'views_count');
        $this->dropColumn('job', 'orders_count');
        $this->dropColumn('job', 'cancellations_count');
    }
}
