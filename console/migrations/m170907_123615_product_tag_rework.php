<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use yii\db\Migration;

class m170907_123615_product_tag_rework extends Migration
{
    public function safeUp()
    {
        $tags = ProductAttribute::find()->where(['entity_alias' => 'product_tag'])->all();
        $attribute = Attribute::find()->where(['alias' => 'product_tag'])->one();

        if(!empty($tags) && $attribute !== null) {
            foreach($tags as $tag) {
                $currentValue = AttributeValue::find()->where(['alias' => $tag->value_alias])->one();

                if($currentValue !== null) {
                    $tag->updateAttributes(['value' => $currentValue->id, 'is_text' => false]);
                } else {
                    $attrValue = new AttributeValue([
                        'attribute_id' => $attribute->id,
                        'translationsArr' => [
                            [
                                'title' => Yii::$app->utility->upperFirstLetter($tag->value),
                                'locale' => $tag->locale
                            ],
                        ]
                    ]);
                    if($attrValue->save()) {
                        $tag->updateAttributes(['value' => $attrValue->id, 'is_text' => false]);
                    }
                }
            }
        }

        AttributeValue::updateAll(['status' => AttributeValue::STATUS_APPROVED]);
    }

    public function safeDown()
    {
        $attribute = Attribute::find()->where(['alias' => 'product_tag'])->one();

        if($attribute !== null) {
            $values = AttributeValue::find()->where(['attribute_id' => $attribute->id])->select('id, attribute_id')->all();

            if(!empty($values)) {
                foreach($values as $value) {
                    $value->delete();
                }
            }
        }
    }
}
