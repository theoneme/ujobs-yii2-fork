<?php

use common\models\user\User;
use common\models\UserWallet;
use yii\db\Migration;

class m171012_064334_add_usd_eur_wallets_for_everyone extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('user_wallet', ['user_id', 'currency_code'],
            array_map(
                function($var){ return [$var, 'EUR'];},
                User::find()->select('id')->where(['not exists', UserWallet::find()->where('user_wallet.user_id = user.id')->andWhere(['currency_code' => 'EUR'])])->column()
            )
        );
        $this->batchInsert('user_wallet', ['user_id', 'currency_code'],
            array_map(
                function($var){ return [$var, 'USD'];},
                User::find()->select('id')->where(['not exists', UserWallet::find()->where('user_wallet.user_id = user.id')->andWhere(['currency_code' => 'USD'])])->column()
            )
        );
    }

    public function safeDown()
    {
        $this->delete('user_wallet', ['currency_code' => ['USD', 'EUR'], 'balance' => 0]);
    }
}
