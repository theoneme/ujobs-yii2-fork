<?php

use yii\db\Migration;

class m170125_115705_order_extra_alter extends Migration
{
    public function up()
    {
        $this->addColumn('order_extra', 'price', $this->integer());
        $this->addColumn('order_extra', 'currency_code', $this->string(3));
        $this->addColumn('order_extra', 'title', $this->string(155));
    }

    public function down()
    {
        $this->dropColumn('order_extra', 'price');
        $this->dropColumn('order_extra', 'currency_code');
        $this->dropColumn('order_extra', 'title');
    }
}
