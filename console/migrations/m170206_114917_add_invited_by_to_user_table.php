<?php

use yii\db\Migration;

class m170206_114917_add_invited_by_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'invited_by', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'invited_by');
    }
}
