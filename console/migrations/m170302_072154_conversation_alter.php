<?php

use yii\db\Migration;

class m170302_072154_conversation_alter extends Migration
{
    public function up()
    {
        $this->addColumn('conversation', 'is_blank', $this->integer(1));

        $this->createIndex('conversation_blank', 'conversation', 'is_blank');
    }

    public function down()
    {
        $this->dropColumn('conversation', 'is_blank');
    }
}