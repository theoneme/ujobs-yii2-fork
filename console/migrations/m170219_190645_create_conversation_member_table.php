<?php

use yii\db\Migration;

/**
 * Handles the creation of table `conversation_member`.
 */
class m170219_190645_create_conversation_member_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('conversation_member', [
            'conversation_id' => $this->integer(),
            'user_id' => $this->integer(),
            'status' => $this->string(),
            'starred' => $this->boolean(),
            'viewed_at' => $this->integer()->defaultValue(0)
        ]);

        $this->createIndex('conversation_member_status', 'conversation_member', 'status');
        $this->createIndex('conversation_member_viewed_at', 'conversation_member', 'viewed_at');

        $this->addPrimaryKey('pk_conversation_member', 'conversation_member', ['conversation_id', 'user_id']);
        $this->addForeignKey('fk_conversation_member_conversation_id', 'conversation_member', 'conversation_id', 'conversation', 'id', 'CASCADE');
        $this->addForeignKey('fk_conversation_member_user_id', 'conversation_member', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('conversation_member');
    }
}
