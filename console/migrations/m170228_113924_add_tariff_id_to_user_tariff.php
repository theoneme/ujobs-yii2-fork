<?php

use yii\db\Migration;

class m170228_113924_add_tariff_id_to_user_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('user_tariff', 'tariff_id', $this->integer());
        $this->addForeignKey('fk_user_tariff_tariff_id', 'user_tariff', 'tariff_id', 'tariff', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_tariff_tariff_id', 'user_tariff');
        $this->dropColumn('user_tariff', 'tariff_id');
    }
}
