<?php

use common\models\Video;
use yii\db\Migration;

/**
 * Class m180423_120348_set_parent_categories_for_videos
 */
class m180423_120348_set_parent_categories_for_videos extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        Video::updateAll(['parent_category_id' => 173], ['parent_category_id' => null]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Video::updateAll(['parent_category_id' => null], ['parent_category_id' => 173]);
    }
}
