<?php

use common\models\AddressTranslation;
use common\models\Job;
use yii\db\Migration;

/**
 * Class m180215_115559_remove_address_from_job
 */
class m180215_115559_remove_address_from_job extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $jobs = Job::find()->where(['not',
            ['or',
                ['lat' => null],
                ['lat' => ''],
                ['long' => null],
                ['long' => ''],
            ]
        ])->all();
        foreach ($jobs as $job) {
            /* @var $job Job*/
            if ($job->lat && $job->long) {
                if (!AddressTranslation::find()->where(['lat' => $job->lat, 'long' => $job->long, 'locale' => 'ru-RU'])->exists()) {
                    $this->insert('address_translation', ['lat' => $job->lat, 'long' => $job->long, 'locale' => 'ru-RU', 'title' => $job->address]);
                }
            }
        }
        $this->dropColumn('job', 'address');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('job', 'address', $this->string(150)->null());
        $jobs = Job::find()->where(['not',
            ['or',
                ['lat' => null],
                ['lat' => ''],
                ['long' => null],
                ['long' => ''],
            ]
        ])->all();
        foreach ($jobs as $job) {
            /* @var $job Job*/
            if ($job->lat && $job->long) {
                /* @var $address AddressTranslation*/
                $address = AddressTranslation::find()->where(['lat' => $job->lat, 'long' => $job->long])->one();
                if ($address !== null) {
                    $job->updateAttributes(['address' => $address->title]);
                }
            }
        }
    }
}
