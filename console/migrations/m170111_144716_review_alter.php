<?php

use yii\db\Migration;

class m170111_144716_review_alter extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'type', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('review', 'type');
    }
}
