<?php

use yii\db\Migration;

class m170918_093222_attachment_description extends Migration
{
    public function safeUp()
    {
        $this->addColumn('attachment', 'description', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('attachment', 'description');
    }
}
