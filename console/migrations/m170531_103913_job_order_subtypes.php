<?php

use yii\db\Migration;

class m170531_103913_job_order_subtypes extends Migration
{
    public function up()
    {
		$this->addColumn('job', 'subtype', $this->string(25)->after('type')->null());
		$this->addColumn('order', 'product_subtype', $this->string(25)->after('product_type')->null());
    }

    public function down()
    {
        $this->dropColumn('job', 'subtype');
        $this->dropColumn('order', 'product_subtype');
    }
}
