<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_favourite`.
 */
class m170119_105618_create_user_favourite_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_favourite', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'job_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk_user_favourite_user_id', 'user_favourite', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_favourite_job_id', 'user_favourite', 'job_id', 'job', 'id', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_favourite');
    }
}
