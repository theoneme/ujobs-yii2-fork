<?php

use yii\db\Migration;

class m170105_083824_alter_job_package_table extends Migration
{
    public function up()
    {
        $this->alterColumn('job_package', 'description', $this->text()->notNull());
        $this->alterColumn('job_package', 'title', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->alterColumn('job_package', 'description', $this->string(250)->notNull());
        $this->alterColumn('job_package', 'title', $this->string(155)->notNull());
    }
}
