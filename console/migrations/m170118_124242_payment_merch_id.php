<?php

use yii\db\Migration;

class m170118_124242_payment_merch_id extends Migration
{
    public function up()
    {
        $this->addColumn('payment', 'code', $this->string(155));
    }

    public function down()
    {
        $this->dropColumn('payment', 'code');
    }
}
