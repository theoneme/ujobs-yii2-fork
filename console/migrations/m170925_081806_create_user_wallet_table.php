<?php

use common\models\user\User;
use yii\db\Migration;

/**
 * Handles the creation of table `user_wallet`.
 */
class m170925_081806_create_user_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_wallet', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'currency_code' => $this->string(3)->null(),
            'balance' => $this->integer()->defaultValue(0)
        ]);
        $this->execute("ALTER TABLE user_wallet AUTO_INCREMENT=100000000;");
        $this->addForeignKey('fk_user_wallet_user_id', 'user_wallet', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_wallet_currency_code', 'user_wallet', 'currency_code', 'currency', 'code', 'SET NULL');

        $this->batchInsert('user_wallet',
            ['user_id', 'balance', 'currency_code'],
            array_map(
                function($var){ return [$var['id'], $var['balance'], 'RUB'];},
                User::find()->select(['id', 'balance'])->indexBy('id')->asArray()->all()
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_wallet');
    }
}
