<?php

use yii\db\Migration;

/**
 * Class m180425_123506_add_fields_to_video
 */
class m180425_123506_add_fields_to_video extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('video', 'status', $this->integer());
        $this->addColumn('video', 'locale', $this->string(5));
        $this->addColumn('video', 'currency_code', $this->string(3)->null());

        $this->createIndex('video_locale', 'video', 'locale');
        $this->createIndex('video_status', 'video', 'status');
        $this->addForeignKey('fk_video_currency_code', 'video', 'currency_code', 'currency', 'code', 'SET NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_video_currency_code', 'video');
        $this->dropColumn('video', 'status');
        $this->dropColumn('video', 'locale');
        $this->dropColumn('video', 'currency_code');
    }
}
