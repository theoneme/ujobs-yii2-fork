<?php

use yii\db\Migration;

class m170504_115431_currencies2 extends Migration
{
	public function up()
	{
		$this->execute('ALTER TABLE job_package CONVERT TO CHARACTER SET utf8mb4');
		$this->alterColumn('job_package', 'currency_code', $this->string(3)->null());
		$this->addForeignKey('fk_job_package_currency_code', 'job_package', 'currency_code', 'currency', 'code', 'SET NULL');

		$this->execute('ALTER TABLE job CONVERT TO CHARACTER SET utf8mb4');
		$this->alterColumn('job', 'currency_code', $this->string(3)->null());
		$this->addForeignKey('fk_job_currency_code', 'job', 'currency_code', 'currency', 'code', 'SET NULL');
		$this->execute('ALTER TABLE job_extra CONVERT TO CHARACTER SET utf8mb4');
		$this->addForeignKey('fk_job_extra_currency_code', 'job_extra', 'currency_code', 'currency', 'code', 'SET NULL');

		$this->addColumn('order', 'currency_code', $this->string(3)->after('total'));
		$this->addForeignKey('fk_order_currency_code', 'order', 'currency_code', 'currency', 'code', 'SET NULL');

		$this->execute('ALTER TABLE order_extra CONVERT TO CHARACTER SET utf8mb4');
		$this->addForeignKey('fk_order_extra_currency_code', 'order_extra', 'currency_code', 'currency', 'code', 'SET NULL');

		$this->addColumn('offer', 'currency_code', $this->string(3)->null());
		$this->addForeignKey('fk_offer_currency_code', 'offer', 'currency_code', 'currency', 'code', 'SET NULL');
		$this->alterColumn('payment_history', 'currency_code', $this->string(3)->null());
		$this->update('payment_history', [
			'currency_code' => null
		]);
		$this->execute('ALTER TABLE payment_history CONVERT TO CHARACTER SET utf8mb4');
		$this->addForeignKey('fk_payment_history_currency_code', 'payment_history', 'currency_code', 'currency', 'code', 'SET NULL');

		$this->alterColumn('payment', 'currency_code', $this->string(3)->null());
		$this->execute('ALTER TABLE payment CONVERT TO CHARACTER SET utf8mb4');
		$this->addForeignKey('fk_payment_currency_code', 'payment', 'currency_code', 'currency', 'code', 'SET NULL');
	}

	public function down()
	{
		$this->dropForeignKey('fk_job_package_currency_code', 'job_package');
		$this->dropForeignKey('fk_job_currency_code', 'job');
		$this->dropForeignKey('fk_job_extra_currency_code', 'job_extra');

		$this->dropForeignKey('fk_order_currency_code', 'order');
		$this->dropColumn('order', 'currency_code');

		$this->dropForeignKey('fk_order_extra_currency_code', 'order_extra');

		$this->dropForeignKey('fk_offer_currency_code', 'offer');
		$this->dropColumn('offer', 'currency_code');

		$this->dropForeignKey('fk_payment_history_currency_code', 'payment_history');

		$this->dropForeignKey('fk_payment_currency_code', 'payment');
	}
}
