<?php

use common\modules\store\models\Currency;
use yii\db\Migration;

class m170504_113324_currencies extends Migration
{
	public function up()
	{
		Currency::deleteAll();

		$this->execute('ALTER TABLE currency MODIFY id INT NOT NULL');
		try {$this->dropPrimaryKey('PRIMARY', 'currency'); } catch(Exception $e) {}
		try {$this->dropPrimaryKey('id_primary_key', 'currency');} catch(Exception $e) {}

		$this->dropColumn('currency', 'id');
		$this->addPrimaryKey('code_primary_key', 'currency', 'code');

		$this->insert('currency', [
				'title' => 'Ruble',
				'code' => 'RUB',
				'symbol_left' => '',
				'symbol_right' => 'р.',
			]
		);
	}

	public function down()
	{
		Currency::deleteAll();

		$this->dropPrimaryKey('code_primary_key', 'currency');
		$this->addColumn('currency', 'id', $this->integer());
		$this->addPrimaryKey('id_primary_key', 'currency', 'id');
	}
}
