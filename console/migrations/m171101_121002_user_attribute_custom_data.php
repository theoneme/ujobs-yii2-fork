<?php

use yii\db\Migration;

class m171101_121002_user_attribute_custom_data extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_attribute', 'custom_data', $this->binary());
    }

    public function safeDown()
    {
        $this->dropColumn('user_attribute', 'custom_data');
    }
}
