<?php

use yii\db\Migration;

class m170108_073830_add_county_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'country', $this->string(255)->defaultValue('Россия'));
    }

    public function down()
    {
        $this->dropColumn('profile', 'country');
    }
}
