<?php

use common\models\UserAttribute;
use yii\db\Migration;

class m171025_075034_user_attribute_refactoring extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        UserAttribute::deleteAll(['value' => null]);

        $this->dropColumn('user_attribute', 'entity');
        $this->renameColumn('user_attribute', 'entity_content', 'attribute_id');
        $this->alterColumn('user_attribute', 'attribute_id', $this->integer());

        Yii::$app->db->createCommand('delete `user_attribute` from `user_attribute` left join mod_attribute as a
            on user_attribute.attribute_id = a.id
            WHERE a.id is NULL')->execute();
        $this->addForeignKey('fk_user_attribute_attribute_id', 'user_attribute', 'attribute_id', 'mod_attribute', 'id', 'CASCADE', 'CASCADE');

        Yii::$app->db->createCommand('delete `user_attribute` from `user_attribute` left join mod_attribute as a
            on user_attribute.entity_alias = a.alias
            WHERE a.alias is NULL')->execute();
        $this->addForeignKey('fk_user_attribute_entity_alias', 'user_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE', 'CASCADE');

        $this->dropColumn('user_attribute', 'key');
        $this->dropColumn('user_attribute', 'is_text');

        Yii::$app->db->createCommand('delete `user_attribute` from `user_attribute` left join mod_attribute_value as v
            on user_attribute.value_alias = v.alias and user_attribute.attribute_id = v.attribute_id
            WHERE v.alias is NULL')->execute();
        $this->addForeignKey('fk_user_attribute_value_alias', 'user_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        Yii::$app->db->createCommand('delete `user_attribute` from `user_attribute` left join mod_attribute_value as v
            on user_attribute.value = v.id
            WHERE v.alias is NULL')->execute();
        $this->alterColumn('user_attribute', 'value', $this->integer());
        $this->addForeignKey('fk_user_attribute_value', 'user_attribute', 'value', 'mod_attribute_value', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->addColumn('user_attribute', 'entity', $this->string(20));
        $this->addColumn('user_attribute', 'key', $this->string(20));

        $this->dropForeignKey('fk_user_attribute_attribute_id', 'user_attribute');
        $this->alterColumn('user_attribute', 'attribute_id', $this->string(10));
        $this->renameColumn('user_attribute', 'attribute_id', 'entity_content');
        $this->dropForeignKey('fk_user_attribute_entity_alias', 'user_attribute');

        $this->addColumn('user_attribute', 'is_text', $this->integer());

        $this->dropForeignKey('fk_user_attribute_value', 'user_attribute');
        $this->alterColumn('user_attribute', 'value', $this->text());
        $this->dropForeignKey('fk_user_attribute_value_alias', 'user_attribute');
    }
}
