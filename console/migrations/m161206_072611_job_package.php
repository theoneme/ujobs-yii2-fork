<?php

use yii\db\Schema;
use yii\db\Migration;

class m161206_072611_job_package extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%job_package}}',
            [
                'id'=> $this->primaryKey(11),
                'job_id'=> $this->integer(11)->notNull(),
                'title'=> $this->string(155)->notNull(),
                'description'=> $this->string(250)->notNull(),
                'price'=> $this->integer(11)->notNull(),
                'currency_code'=> $this->string(8)->notNull(),
                'included'=> $this->text()->notNull(),
                'excluded'=> $this->text()->notNull(),
            ],$tableOptions
        );
        $this->createIndex('job_id','{{%job_package}}','job_id',false);
    }

    public function safeDown()
    {
        $this->dropIndex('job_id', '{{%job_package}}');
        $this->dropTable('{{%job_package}}');
    }
}