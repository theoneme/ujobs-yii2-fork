<?php

use yii\db\Migration;

class m161227_095549_job_requirement extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'requirements', $this->text());
    }

    public function down()
    {
        $this->dropColumn('job', 'requirements');
    }
}
