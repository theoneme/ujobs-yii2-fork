<?php

use yii\db\Migration;

class m161129_102023_education_alter extends Migration
{
    public function up()
    {
        $this->addColumn('user_education', 'specialization', $this->string(155));
        $this->addColumn('user_education', 'country_title', $this->string(155));
    }

    public function down()
    {
        $this->dropColumn('user_education', 'country_title');
        $this->dropColumn('user_education', 'specialization');
    }
}
