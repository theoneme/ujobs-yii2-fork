<?php

use yii\db\Migration;

class m161215_124323_order_changes extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'full_name', $this->string(75));
        $this->addColumn('order', 'location_option', $this->string(25));
        $this->addColumn('order', 'address_id', $this->integer());
        $this->addColumn('order', 'date_option', $this->string(25));
        $this->addColumn('order', 'want_asap', $this->boolean());
        $this->addColumn('order', 'wish_date', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order', 'full_name');
        $this->dropColumn('order', 'location_option');
        $this->dropColumn('order', 'address_id');
        $this->dropColumn('order', 'date_option');
        $this->dropColumn('order', 'want_asap');
        $this->dropColumn('order', 'wish_date');
    }
}
