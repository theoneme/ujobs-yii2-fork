<?php

use yii\db\Migration;

/**
 * Handles the creation of table `property_client_house`.
 */
class m180718_120004_create_property_client_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('property_client_house', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'entity' => $this->string(),
            'entity_id' => $this->integer(),
        ]);

        $this->addForeignKey('fk_property_client_house_client_id', 'property_client_house', 'client_id', 'property_client', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('property_client_house');
    }
}
