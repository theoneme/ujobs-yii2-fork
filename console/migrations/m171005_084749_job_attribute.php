<?php

use common\models\JobAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\db\Migration;

class m171005_084749_job_attribute extends Migration
{
    public function safeUp()
    {
        JobAttribute::deleteAll(['value' => null]);

        $this->dropColumn('job_attribute', 'entity');
        $this->renameColumn('job_attribute', 'entity_content', 'attribute_id');
        $this->alterColumn('job_attribute', 'attribute_id', $this->integer());

        Yii::$app->db->createCommand('delete `job_attribute` from `job_attribute` left join mod_attribute as a
            on job_attribute.attribute_id = a.id
            WHERE a.id is NULL')->execute();
        $this->addForeignKey('fk_job_attribute_attribute_id', 'job_attribute', 'attribute_id', 'mod_attribute', 'id', 'CASCADE', 'CASCADE');

        Yii::$app->db->createCommand('delete `job_attribute` from `job_attribute` left join mod_attribute as a
            on job_attribute.entity_alias = a.alias
            WHERE a.alias is NULL')->execute();
        $this->addForeignKey('fk_job_attribute_entity_alias', 'job_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE', 'CASCADE');

        $this->dropColumn('job_attribute', 'key');
        $this->dropColumn('job_attribute', 'is_text');
//
        $tags = JobAttribute::find()->where(['entity_alias' => 'tag'])->all();
        $attribute = Attribute::find()->where(['alias' => 'tag'])->one();

        if(!empty($tags) && $attribute !== null) {
            foreach($tags as $tag) {
                $currentValue = AttributeValue::find()->where(['alias' => $tag->value_alias, 'attribute_id' => $attribute->id])->one();

                if($currentValue !== null) {
                    $tag->updateAttributes(['value' => $currentValue->id]);
                } else {
                    $attrValue = new AttributeValue([
                        'attribute_id' => $attribute->id,
                        'translationsArr' => [
                            [
                                'title' => Yii::$app->utility->upperFirstLetter($tag->value),
                                'locale' => $tag->locale
                            ],
                        ]
                    ]);
                    if($attrValue->save()) {
                        $tag->updateAttributes(['value' => $attrValue->id]);
                    }
                }
            }
        }

        Yii::$app->db->createCommand('delete `job_attribute` from `job_attribute` left join mod_attribute_value as v
            on job_attribute.value_alias = v.alias and job_attribute.attribute_id = v.attribute_id
            WHERE v.alias is NULL')->execute();
        $this->addForeignKey('fk_job_attribute_value_alias', 'job_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        Yii::$app->db->createCommand('delete `job_attribute` from `job_attribute` left join mod_attribute_value as v
            on job_attribute.value = v.id
            WHERE v.alias is NULL')->execute();
        $this->alterColumn('job_attribute', 'value', $this->integer());
        $this->addForeignKey('fk_job_attribute_value', 'job_attribute', 'value', 'mod_attribute_value', 'id', 'CASCADE', 'CASCADE');


        AttributeValue::updateAll(['status' => AttributeValue::STATUS_APPROVED], ['attribute_id' => 36]);
    }

    public function safeDown()
    {
        $this->addColumn('job_attribute', 'entity', $this->string(20));
        $this->addColumn('job_attribute', 'key', $this->string(20));

        $this->dropForeignKey('fk_job_attribute_attribute_id', 'job_attribute');
        $this->alterColumn('job_attribute', 'attribute_id', $this->string(10));
        $this->renameColumn('job_attribute', 'attribute_id', 'entity_content');
        $this->dropForeignKey('fk_job_attribute_entity_alias', 'job_attribute');

        $this->addColumn('job_attribute', 'is_text', $this->integer());

        $this->dropForeignKey('fk_job_attribute_value', 'job_attribute');
        $this->alterColumn('job_attribute', 'value', $this->text());
        $this->dropForeignKey('fk_job_attribute_value_alias', 'job_attribute');

        $attribute = Attribute::find()->where(['alias' => 'tag'])->one();
        if($attribute !== null) {
            AttributeValue::deleteAll(['attribute_id' => $attribute->id]);
            Yii::$app->db->createCommand('delete `mod_attribute_description` from `mod_attribute_description` left join mod_attribute_value as a
            on mod_attribute_description.entity_content = a.id
            WHERE a.id is NULL and mod_attribute_description.entity = "attribute-value"')->execute();
        }
    }
}
