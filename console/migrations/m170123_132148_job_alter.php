<?php

use yii\db\Migration;

class m170123_132148_job_alter extends Migration
{
    public function up()
    {
        $this->alterColumn('job', 'execution_time', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('job', 'execution_time', $this->string(155));
    }
}
