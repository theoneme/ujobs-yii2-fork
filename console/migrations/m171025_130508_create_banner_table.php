<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banner`.
 */
class m171025_130508_create_banner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('banner', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'url' => $this->string(),
            'type' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('banner');
    }
}