<?php

use yii\db\Migration;

class m170403_131857_session_table extends Migration
{
    public function up()
    {
        $this->createTable('session', [
            'id' => $this->char(40)->notNull(),
            'expire' => $this->integer(),
            'user_id' => $this->integer(),
            'last_write' => $this->integer()->notNull(),
            'data' => $this->binary(),
        ]);

        $this->createIndex('index_expire', 'session', 'expire');
        $this->addForeignKey('fk_session_user_id', 'session', 'user_id', 'user', 'id', 'SET NULL');
        $this->addPrimaryKey('pk_session_id', 'session', 'id');
    }

    public function down()
    {
        $this->dropTable('session');
    }
}
