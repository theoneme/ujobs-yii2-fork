<?php

use yii\db\Migration;

class m161227_091612_alter_profile_table extends Migration
{
    public function up()
    {
        $this->dropColumn('profile', 'rcv_inbox_messages');
        $this->dropColumn('profile', 'rcv_order_messages');
        $this->dropColumn('profile', 'rcv_sms_messages');
        $this->addColumn('profile', 'email_notifications', $this->boolean()->defaultValue(true));
        $this->addColumn('profile', 'sms_notifications', $this->boolean()->defaultValue(true));
    }

    public function down()
    {
        $this->dropColumn('profile', 'email_notifications');
        $this->dropColumn('profile', 'sms_notifications');
        $this->addColumn('profile', 'rcv_inbox_messages', $this->boolean());
        $this->addColumn('profile', 'rcv_order_messages', $this->boolean());
        $this->addColumn('profile', 'rcv_sms_messages', $this->boolean());
    }
}
