<?php

use yii\db\Migration;

/**
 * Class m180419_120115_add_created_by_to_conversation
 */
class m180419_120115_add_created_by_to_conversation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('conversation', 'created_by', $this->integer());
        $this->addForeignKey('fk_conversation_created_by', 'conversation', 'created_by', 'user', 'id', 'SET NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_conversation_created_by', 'conversation');
        $this->dropColumn('conversation', 'created_by');
    }
}
