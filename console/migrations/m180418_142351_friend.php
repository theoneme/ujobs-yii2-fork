<?php

use yii\db\Migration;

/**
 * Class m180418_142351_friend
 */
class m180418_142351_friend extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_friend', [
            'user_id' => $this->integer(),
            'target_user_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_friend_users_unique_index', 'user_friend', ['user_id', 'target_user_id']);
        $this->addForeignKey('fk_friend_user_id', 'user_friend', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_friend_target_user_id', 'user_friend', 'target_user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_friend_user_id', 'user_friend');
        $this->dropForeignKey('fk_friend_target_user_id', 'user_friend');

        $this->dropTable('user_friend');
    }
}
