<?php

use yii\db\Migration;

class m171113_140246_payment_message_type extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_history', 'template', $this->integer()->after('type'));
    }

    public function safeDown()
    {
        $this->dropColumn('payment_history', 'template');
    }
}
