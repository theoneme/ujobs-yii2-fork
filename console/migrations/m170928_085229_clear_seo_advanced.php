<?php

use yii\db\Migration;

class m170928_085229_clear_seo_advanced extends Migration
{
    public function safeUp()
    {
        \common\models\SeoAdvanced::deleteAll(['entity' => 'profile']);
        \common\models\SeoAdvanced::deleteAll(['entity' => 'job']);
        \common\models\SeoAdvanced::deleteAll(['entity' => 'tender']);
    }

    public function safeDown()
    {

    }
}
