<?php

use yii\db\Migration;

class m171102_121610_drop_tables extends Migration
{
    public function safeUp()
    {
        $this->dropTable('user_skill');
        $this->dropTable('user_language');
        $this->dropTable('user_specialty');
    }

    public function safeDown()
    {
        $this->createTable('user_specialty', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'salary' => $this->integer(),
        ]);
        $this->addForeignKey('fk_user_specialty_user_id', 'user_specialty', 'user_id', 'user', 'id', 'CASCADE');

        $this->createTable('user_skill', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'level' => $this->string(),
        ]);
        $this->addForeignKey('fk_user_skill_user_id', 'user_skill', 'user_id', 'user', 'id', 'CASCADE');

        $this->createTable('user_language', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'language' => $this->string(),
            'level' => $this->integer(),
        ]);
        $this->addForeignKey('fk_user_language_user_id', 'user_language', 'user_id', 'user', 'id', 'CASCADE');
    }
}
