<?php

use yii\db\Migration;

class m161226_081954_payment_order extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%payment}}',
            [
                'id' => $this->primaryKey(11),
                'user_id' => $this->integer(11)->notNull(),
                'created_at' => $this->integer(11)->notNull(),
                'updated_at' => $this->integer(11)->notNull(),
                'total' => $this->integer(11)->notNull(),
                'currency_code' => $this->string(8)->notNull(),
            ], $tableOptions
        );
        $this->createIndex('user_id', '{{%payment}}', 'user_id', false);

        $this->addColumn('order', 'payment_id', $this->integer());
        $this->createIndex('payment_id', '{{%order}}', 'payment_id', false);
    }

    public function down()
    {
        $this->dropTable('payment');
        $this->dropColumn('order', 'payment_id');
    }
}
