<?php

use yii\db\Migration;

/**
 * Class m180316_070031_add_locale_to_banner
 */
class m180316_070031_add_locale_to_banner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('banner', 'locale', $this->string(5));
        $this->createIndex('banner_locale', 'banner', 'locale');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('banner_locale', 'banner');
        $this->dropColumn('banner', 'locale');
    }
}
