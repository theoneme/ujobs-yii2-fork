<?php

use common\modules\attribute\models\Attribute;
use yii\db\Migration;

/**
 * Class m171213_123256_add_page_tag_attribute
 */
class m171213_123256_add_page_tag_attribute extends Migration
{
    public function safeUp()
    {
        $productTag = new Attribute([
            'is_system' => false,
            'name' => 'Тег',
            'type' => 'checkboxlist',
            'alias' => 'page_tag',
            'is_searchable' => 1,
        ]);
        $productTag->translationsArr['ru-RU']->title = 'Тег';
        $productTag->translationsArr['uk-UA']->title = 'Тег';
        $productTag->translationsArr['en-GB']->title = 'Tag';
        $productTag->translationsArr['es-ES']->title = 'Etiqueta';
        $productTag->translationsArr['ka-GE']->title = 'Tag';

        $productTag->save();
    }

    public function safeDown()
    {
        $productTag = Attribute::findOne(['alias' => 'page_tag']);
        if ($productTag !== null) {
            $productTag->delete();
        }
    }
}
