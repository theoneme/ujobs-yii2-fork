<?php

use yii\db\Migration;

class m170407_081202_add_type_to_message extends Migration
{
    public function up()
    {
        $this->addColumn('message', 'type', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('message', 'type');
    }
}
