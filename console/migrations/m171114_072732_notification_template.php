<?php

use yii\db\Migration;

class m171114_072732_notification_template extends Migration
{
    public function safeUp()
    {
        $this->addColumn('notification', 'template', $this->integer()->after('subject'));
    }

    public function safeDown()
    {
        $this->dropColumn('notification', 'template');
    }
}
