<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_member`.
 */
class m180327_080218_create_company_member_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_member', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'company_id' => $this->integer(),
            'role' => $this->integer()
        ]);

        $this->addForeignKey('fk_company_member_user_id', 'company_member', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_company_member_company_id', 'company_member', 'company_id', 'company', 'id', 'CASCADE');
        $this->createIndex('company_member_role_index', 'company_member', 'role');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_member');
    }
}
