<?php

use common\models\PaymentHistory;
use common\models\UserWallet;
use common\models\user\User;
use yii\db\Migration;

class m171002_100108_create_cointy_wallets_for_everyone extends Migration
{
    public function safeUp()
    {
        foreach (User::find()->select('id')->column() as $userId) {
            $ph = new PaymentHistory([
                'user_id' => $userId,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_FRIEND_REGISTER_BONUS,
                'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                'amount' => 1,
            ]);
            $ph->save();
        }
    }

    public function safeDown()
    {
        $this->delete('user_wallet', ['currency_code' => 'CTY']);
        $this->delete('payment_history', ['type' => PaymentHistory::TYPE_OPERATION_BONUS]);
    }
}
