<?php

use yii\db\Migration;

class m170324_142038_user_attribute_FK extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_user_attribute_user', 'user_attribute', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_attribute_user', 'user_attribute');
    }
}
