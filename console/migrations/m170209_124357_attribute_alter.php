<?php

use yii\db\Migration;

class m170209_124357_attribute_alter extends Migration
{
    public function up()
    {
        $this->addColumn('mod_attribute', 'attribute_entity_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('mod_attribute', 'attribute_entity_id');
    }
}
