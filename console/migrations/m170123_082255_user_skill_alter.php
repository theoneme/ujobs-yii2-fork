<?php

use yii\db\Migration;

class m170123_082255_user_skill_alter extends Migration
{
    public function up()
    {
        $this->createIndex('alias', 'user_skill', 'alias');

        $this->addForeignKey('fk_skill_user', 'user_skill', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropIndex('alias', 'user_skill');

        $this->dropForeignKey('fk_skill_user', 'user_skill');
    }
}
