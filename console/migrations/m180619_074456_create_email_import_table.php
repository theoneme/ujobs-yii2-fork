<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email_import`.
 */
class m180619_074456_create_email_import_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('email_import', [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'source' => $this->string(),
            'ip' => $this->string(),
            'date_added' => $this->integer(),
            'mail_sent' => $this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('email_import');
    }
}
