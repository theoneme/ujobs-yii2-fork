<?php

use yii\db\Migration;

class m170310_101653_add_contract_price_to_job extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'contract_price', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('job', 'contract_price');
    }
}
