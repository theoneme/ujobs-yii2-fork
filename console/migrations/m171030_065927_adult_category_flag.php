<?php

use yii\db\Migration;

class m171030_065927_adult_category_flag extends Migration
{
    public function safeUp()
    {
        $this->addColumn('category', 'is_adult', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('category', 'is_adult');
    }
}
