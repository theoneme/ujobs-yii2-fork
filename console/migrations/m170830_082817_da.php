<?php

use common\models\Category;
use yii\db\Migration;

class m170830_082817_da extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('mod_attribute', 'category_dependant');
        $this->dropColumn('mod_attribute', 'attribute_entity_id');

        $this->addColumn('mod_attribute', 'is_parsed', $this->boolean());

        Category::updateAll(['attribute_set_id' => null]);
        $this->addForeignKey('fk_category_attribute_set_id', 'category', 'attribute_set_id', 'mod_attribute_group', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('mod_attribute', 'is_parsed');

        $this->addColumn('mod_attribute', 'category_dependant', $this->integer());
        $this->addColumn('mod_attribute', 'attribute_entity_id', $this->integer());

        $this->dropForeignKey('fk_category_attribute_set_id', 'category');
    }
}
