<?php

use common\models\ContentTranslation;
use common\models\JobSearchStep;
use yii\db\Migration;

class m171114_061155_move_tree_title_to_step extends Migration
{
    public function safeUp()
    {
        /* @var $steps JobSearchStep[]*/
        $steps = JobSearchStep::find()->joinWith(['tree.translations', 'translations'])->all();
        foreach ($steps as $step) {
            foreach ($step->translations as $translation) {
                $translation->updateAttributes(['content_prev' => $translation->title, 'title' => $step->tree->translations[$translation->locale]->title ?? '']);
            }

        }
        ContentTranslation::deleteAll(['entity' => 'job_search_tree']);
    }

    public function safeDown()
    {
        /* @var $steps JobSearchStep[]*/
        $steps = JobSearchStep::find()->joinWith(['tree.translations', 'translations'])->all();
        foreach ($steps as $step) {
            foreach ($step->translations as $translation) {
                $condition = ['entity' => 'job_search_tree', 'entity_id' => $step->tree_id, 'locale' => $translation->locale, 'title' => $translation->title];
                if (!ContentTranslation::find()->where($condition)->exists()) {
                    (new ContentTranslation($condition))->save();
                }
                $translation->updateAttributes(['title' => $translation->content_prev, 'content_prev' => '']);
            }
        }
    }
}
