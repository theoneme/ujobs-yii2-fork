<?php

use yii\db\Migration;

class m170510_150940_add_currency_code_to_order_product extends Migration
{
    public function up()
    {
        $this->addColumn('order_product', 'currency_code', $this->string(3)->null());
        $this->addForeignKey('fk_order_product_currency_code', 'order_product', 'currency_code', 'currency', 'code', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('fk_order_product_currency_code', 'order_product');
        $this->dropColumn('order_product', 'currency_code');
    }
}
