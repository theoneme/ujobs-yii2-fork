<?php

use yii\db\Migration;

class m170918_134922_portfolio_image extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_portfolio', 'image', $this->string(125)->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user_portfolio', 'image');
    }
}
