<?php

use yii\db\Migration;

class m171030_140459_new_profile_contacts extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'whatsapp', $this->string(55)->null());
        $this->addColumn('profile', 'viber', $this->string(55)->null());
        $this->addColumn('profile', 'telegram', $this->string(55)->null());
        $this->addColumn('profile', 'facebook_messenger', $this->string(55)->null());
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'whatsapp');
        $this->dropColumn('profile', 'viber');
        $this->dropColumn('profile', 'telegram');
        $this->dropColumn('profile', 'facebook_messenger');
    }
}
