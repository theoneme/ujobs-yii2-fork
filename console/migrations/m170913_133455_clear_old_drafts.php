<?php

use common\models\Job;
use yii\db\Migration;

class m170913_133455_clear_old_drafts extends Migration
{
    public function safeUp()
    {
        $drafts = Job::find()->where(['status' => Job::STATUS_DRAFT])->andWhere(['<', 'created_at', 1504272953])->all();

        if($drafts !== null) {
            foreach($drafts as $draft) {
                $draft->delete();
            }
        }
    }

    public function safeDown()
    {

    }
}
