<?php

use yii\db\Migration;

class m170206_112254_money_withdraw extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_withdrawal}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'amount' => $this->integer(),
            'currency_code' => $this->string(3),
            'target_method' => $this->integer(),
            'target_account' => $this->string(155),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->createIndex('user_id', 'user_withdrawal', 'user_id');

        $this->addForeignKey('fk_withdrawal_user', 'user_withdrawal', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('user_withdrawal');
    }
}
