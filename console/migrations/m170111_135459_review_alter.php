<?php

use yii\db\Migration;

class m170111_135459_review_alter extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'created_for', $this->integer());

        $this->createIndex('created_for', '{{%review}}', 'created_for', false);
    }

    public function down()
    {
        $this->dropColumn('review', 'created_for');
    }
}
