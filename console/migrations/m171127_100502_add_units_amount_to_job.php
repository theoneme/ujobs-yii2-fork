<?php

use common\models\Job;
use yii\db\Migration;

/**
 * Class m171127_100502_add_units_amount_to_job
 */
class m171127_100502_add_units_amount_to_job extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('job', 'units_amount', $this->integer());
        foreach (Job::find()->where(['price_per_unit' => true])->all() as $job) {
            /* @var $job Job*/
            $array = explode(' ', $job->measure, 2);
            if (count($array) == 2) {
                $job->updateAttributes(['units_amount' => $array[0], 'measure' => $array[1]]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Yii::$app->db->createCommand('UPDATE job SET measure = CONCAT(units_amount, " ", measure) WHERE price_per_unit = 1;')->execute();
        $this->dropColumn('job', 'units_amount');
    }
}
