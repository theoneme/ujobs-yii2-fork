<?php

use yii\db\Migration;

/**
 * Class m180307_124536_add_lat_long_to_profile
 */
class m180307_124536_add_lat_long_to_profile extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'lat', $this->string(50));
        $this->addColumn('profile', 'long', $this->string(50));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'lat');
        $this->dropColumn('profile', 'long');
    }
}
