<?php

use yii\db\Migration;

class m161220_133018_category_alter extends Migration
{
    public function up()
    {
        $this->addColumn('category', 'sort_order', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('category', 'sort_order');
    }
}
