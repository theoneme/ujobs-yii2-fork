<?php

use yii\db\Migration;

class m161031_131436_job_attribute_flag extends Migration
{
    public function up()
    {
        $this->addColumn('job_attribute', 'is_text', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('job_attribute', 'is_text');
    }
}
