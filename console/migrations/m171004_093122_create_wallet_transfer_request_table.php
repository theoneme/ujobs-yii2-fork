<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wallet_transfer_request`.
 */
class m171004_093122_create_wallet_transfer_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('wallet_transfer_request', [
            'id' => $this->primaryKey(),
            'wallet_from_id' => $this->integer(),
            'wallet_to_id' => $this->integer(),
            'amount' => $this->integer(),
            'token' => $this->string(32),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'type' => $this->integer(),
            'status' => $this->integer(),
        ]);

        $this->addForeignKey('fk_wallet_transfer_request_wallet_from_id', 'wallet_transfer_request', 'wallet_from_id', 'user_wallet', 'id', 'CASCADE');
        $this->addForeignKey('fk_wallet_transfer_request_wallet_to_id', 'wallet_transfer_request', 'wallet_to_id', 'user_wallet', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('wallet_transfer_request');
    }
}
