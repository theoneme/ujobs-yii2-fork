<?php

use yii\db\Migration;

class m171027_113702_user_specialty_mod extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('user_specialty', 'attribute_id');
        $this->dropColumn('user_specialty', 'alias');
    }

    public function safeDown()
    {
        $this->addColumn('user_specialty', 'attribute_id', $this->integer());
        $this->addColumn('user_specialty', 'alias', $this->string(55));
    }
}
