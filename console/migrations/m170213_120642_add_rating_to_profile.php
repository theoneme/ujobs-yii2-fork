<?php

use yii\db\Migration;

class m170213_120642_add_rating_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'rating', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('profile', 'rating');
    }
}
