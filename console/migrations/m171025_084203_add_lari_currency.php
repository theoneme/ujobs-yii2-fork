<?php

use yii\db\Migration;

class m171025_084203_add_lari_currency extends Migration
{
    public function safeUp()
    {
        $this->insert('currency', [
            'title' => 'Lari',
            'code' => 'GEL',
            'symbol_right' => '₾',
        ]);
    }

    public function safeDown()
    {
        $this->delete('currency', ['code' => 'GEL']);
    }
}
