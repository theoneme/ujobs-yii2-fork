<?php

use yii\db\Migration;

class m170208_154310_attribute_to_attribute_set extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable('{{%mod_attribute_to_group}}', [
            'id' => $this->primaryKey(11),
            'attribute_id' => $this->integer()->notNull(),
            'attribute_group_id' => $this->integer()->null()->defaultValue(null),

        ], $tableOptions);

        $this->addForeignKey('mod_attribute_id_to_attribute', 'mod_attribute_to_group', 'attribute_id', 'mod_attribute', 'id', 'CASCADE');
        $this->addForeignKey('mod_attribute_group_id_to_attribute_group', 'mod_attribute_to_group', 'attribute_group_id', 'mod_attribute_group', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('mod_attribute_to_group');
    }
}
