<?php

use yii\db\Migration;

class m170301_101042_new_tariff_rule extends Migration
{
    public function up()
    {
        $this->insert('tariff_rule', [
            'tariff_id' => 2,
            'code' => 'time_since_tender_post_to_respond',
            'value' => 60 * 60 * 24,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);
    }

    public function down()
    {

    }
}
