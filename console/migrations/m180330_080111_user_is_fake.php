<?php

use yii\db\Migration;

/**
 * Class m180330_080111_user_is_fake
 */
class m180330_080111_user_is_fake extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_fake', $this->boolean()->defaultValue(false));
        $this->createIndex('user_is_fake_index', 'user', 'is_fake');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_fake');
    }
}
