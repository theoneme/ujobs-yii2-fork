<?php

use yii\db\Migration;

class m170320_104953_insert_balance_payment_method extends Migration
{
    public function up()
    {
        $this->insert('payment_method', [
            'name' => 'Оплата с баланса uJobs',
            'code' => 'balance',
            'sort' => 2,
            'enabled' => true
        ]);
    }

    public function down()
    {
        $this->delete('payment_method', ['code' => 'balance']);
    }
}
