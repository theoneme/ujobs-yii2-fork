<?php

use common\models\JobAttribute;
use common\models\Page;
use common\models\SeoAdvanced;
use yii\db\Migration;

class m170405_121608_db_cleanup extends Migration
{
    public function safeUp()
    {
        try {
            $this->dropTable('job_address');
        } catch (Exception $e) {}

        try {
            $this->dropTable('address');
        } catch (Exception $e) {}

        try {
            $this->dropTable('employment');
        } catch (Exception $e) {}

        try {
            $this->dropTable('job_notification');
        } catch (Exception $e) {}

        try {
            $this->dropTable('job_requirement');
        } catch (Exception $e) {}

        try {
            $this->dropTable('job_tag');
        } catch (Exception $e) {}

        try {
            $this->dropTable('order_status');
        } catch (Exception $e) {}

        try {
            $this->dropTable('order_total');
        } catch (Exception $e) {}

        try {
            $this->dropTable('q_a');
        } catch (Exception $e) {}

        SeoAdvanced::deleteAll(['entity' => 'tag']);
        SeoAdvanced::deleteAll(['entity' => 'skill']);
        $pages = Page::find()->where(['type' => ['tag-page', 'skill-page']])->all();
        if($pages) {
            foreach($pages as $page) {
                $page->delete();
            }
        }

        $this->createIndex('cart_updated_at_index', 'cart', 'updated_at');

        $this->createIndex('category_type_index', 'category', 'type');
        $this->createIndex('category_alias_index', 'category', 'alias');

        $this->dropIndex('user_id', 'comment');
        $this->addForeignKey('fk_comment_user_id', 'comment', 'user_id', 'user', 'id', 'CASCADE');

        $this->dropIndex('conversation_created_at', 'conversation');
        $this->dropIndex('conversation_updated_at', 'conversation');
        $this->createIndex('conversation_type_index', 'conversation', 'type');

        $this->alterColumn('conversation_member', 'status', $this->string(30));

        $this->createIndex('currency_code_index', 'currency', 'code');
        $this->createIndex('currency_status_index', 'currency', 'status');

        $this->dropIndex('to_id', 'email_notification');
        Yii::$app->db->createCommand('delete `email_notification` from `email_notification` left join user as u
            on email_notification.to_id = u.id
            WHERE u.id is NULL')->execute();
        $this->addForeignKey('fk_email_notification_to_id', 'email_notification', 'to_id', 'user', 'id', 'CASCADE');

        $this->createIndex('event_created_at_index', 'event', 'created_at');
        $this->alterColumn('event', 'entity', $this->string(20));
        $this->createIndex('event_entity_index', 'event', 'entity');
        $this->createIndex('event_entity_content_index', 'event', 'entity_content');

        $this->dropColumn('job', 'is_active');
        $this->dropColumn('job', 'is_moderated');
        $this->dropColumn('job', 'datetime');
        $this->dropColumn('job', 'anytime');
        $this->dropColumn('job', 'shipping_available');
        $this->dropColumn('job', 'shipping_only');
        $this->dropColumn('job', 'offline_available');
        $this->dropColumn('job', 'online_available');
        $this->dropColumn('job', 'shipment_available');
        $this->dropColumn('job', 'office_available');
        $this->dropColumn('job', 'asap_available');
        $this->dropColumn('job', 'remote_available');
        $this->dropIndex('user_id', 'job');
        Yii::$app->db->createCommand('delete `job` from `job` left join user as u
            on job.user_id = u.id
            WHERE u.id is NULL')->execute();
        $this->addForeignKey('fk_job_user_id', 'job', 'user_id', 'user', 'id', 'CASCADE');
        $this->createIndex('job_status_index', 'job', 'status');
        $this->addForeignKey('fk_job_category_id', 'job', 'category_id', 'category', 'id', 'CASCADE');
        Yii::$app->db->createCommand('update job set parent_category_id = 116 where id = 174')->execute();
        $this->addForeignKey('fk_parent_category_id', 'job', 'parent_category_id', 'category', 'id', 'CASCADE');

        $this->dropColumn('job_attribute', 'weight');
        $this->alterColumn('job_attribute', 'entity', $this->string(20));
        $this->alterColumn('job_attribute', 'entity_content', $this->string(10));
        $this->alterColumn('job_attribute', 'key', $this->string(25));
        JobAttribute::deleteAll(['key' => ['attribute_6', 'attribute_8']]);

        $this->alterColumn('job_extra', 'price', $this->integer());

        $this->dropIndex('job_id', 'job_package');
        Yii::$app->db->createCommand('delete `job_package` from `job_package` left join job as j
            on job_package.job_id = j.id
            WHERE j.id is NULL')->execute();
        $this->addForeignKey('fk_job_package_job_id', 'job_package', 'job_id', 'job', 'id', 'CASCADE');

        $this->dropIndex('job_id', 'job_qa');
        Yii::$app->db->createCommand('delete `job_qa` from `job_qa` left join job as j
            on job_qa.job_id = j.id
            WHERE j.id is NULL')->execute();
        $this->addForeignKey('fk_job_qa_job_id', 'job_qa', 'job_id', 'job', 'id', 'CASCADE');

        $this->createIndex('mod_attribute_alias_index', 'mod_attribute', 'alias');

        $this->dropIndex('idx-attr-id', 'mod_attribute_value');
        Yii::$app->db->createCommand('delete `mod_attribute_value` from `mod_attribute_value` left join mod_attribute as a
            on mod_attribute_value.attribute_id = a.id
            WHERE a.id is NULL')->execute();
        $this->addForeignKey('fk_mod_attribute_value_attribute_id', 'mod_attribute_value', 'attribute_id', 'mod_attribute', 'id', 'CASCADE');
        $this->dropColumn('mod_attribute_value', 'parent_value_entity');
        $this->dropColumn('mod_attribute_value', 'parent_value_id');
        $this->createIndex('mod_attribute_value_alias_index', 'mod_attribute_value', 'alias');

        try {
            $this->dropForeignKey('fk_order_currency_id', 'order');
        } catch (Exception $e) {}
        $this->dropColumn('order', 'currency_id');
        $this->dropColumn('order', 'firstname');
        $this->dropColumn('order', 'lastname');
        $this->dropColumn('order', 'middlename');
        $this->dropColumn('order', 'postcode');
        $this->dropColumn('order', 'country');
        $this->dropColumn('order', 'region');
        $this->dropColumn('order', 'city');
        $this->dropColumn('order', 'street');
        $this->dropColumn('order', 'house');
        $this->dropColumn('order', 'housing');
        $this->dropColumn('order', 'room');
        $this->dropColumn('order', 'location_option');
        $this->dropColumn('order', 'address_id');
        $this->dropColumn('order', 'date_option');
        $this->dropColumn('order', 'want_asap');
        $this->dropColumn('order', 'wish_date');
        $this->dropIndex('payment_id', 'order');
        Yii::$app->db->createCommand('delete `order` from `order` left join payment as p
            on order.payment_id = p.id
            WHERE p.id is NULL and order.payment_id is not null')->execute();
        $this->addForeignKey('fk_order_payment_id', 'order', 'payment_id', 'payment', 'id', 'CASCADE');
        $this->dropIndex('seller_id', 'order');
        $this->addForeignKey('fk_order_seller_id', 'order', 'seller_id', 'user', 'id', 'CASCADE');

        $this->dropColumn('order_history', 'is_approved');

        Yii::$app->db->createCommand('delete `order_product` from `order_product` left join `order` as o
            on `order_product`.order_id = o.id
            WHERE o.id is NULL')->execute();
        $this->addForeignKey('fk_order_product_order_id', 'order_product', 'order_id', 'order', 'id', 'CASCADE');
        $this->createIndex('order_product_product_id_index', 'order_product', 'product_id');

        $this->dropColumn('page', 'name');
        $this->dropIndex('category_id', 'page');
        $this->addForeignKey('fk_page_category_id', 'page', 'category_id', 'category', 'id', 'CASCADE');
        $this->addForeignKey('fk_page_user_id', 'page', 'user_id', 'user', 'id', 'CASCADE');
        $this->dropColumn('page', 'relation_alias');

        $this->dropIndex('user_id', 'payment');
        $this->dropIndex('payment_method_id', 'payment');
        $this->addForeignKey('fk_payment_user_id', 'payment', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_payment_payment_method_id', 'payment', 'payment_method_id', 'payment_method', 'id', 'CASCADE');
        $this->createIndex('payment_status_index', 'payment', 'status');
        $this->createIndex('payment_type_index', 'payment', 'type');

        $this->dropColumn('profile', 'location');
        $this->dropColumn('profile', 'website');
        $this->dropColumn('profile', 'latitude');
        $this->dropColumn('profile', 'longitude');
        $this->dropColumn('profile', 'is_company');
        $this->dropColumn('profile', 'gravatar_id');
        $this->createIndex('profile_status_index', 'profile', 'status');

        $this->dropIndex('job_id', 'review');
        Yii::$app->db->createCommand('delete `review` from `review` left join job as j
            on review.job_id = j.id
            WHERE j.id is NULL')->execute();
        $this->addForeignKey('fk_review_job_id', 'review', 'job_id', 'job', 'id', 'CASCADE');

        $this->alterColumn('seo_advanced', 'type', $this->string(30));
        $this->alterColumn('seo_advanced', 'entity', $this->string(30));

        try {
            $this->dropForeignKey('fk_session_user_id', 'session');
        } catch(Exception $e) {}

        try {
            $this->dropIndex('user_id', 'session');
        } catch(Exception $e) {}

        $this->addForeignKey('fk_session_user_id', 'session', 'user_id', 'user', 'id', 'CASCADE');

        $this->createIndex('tariff_cost_index', 'tariff', 'cost');

        $this->dropColumn('user', 'last_action_at');

        $this->dropColumn('user_attribute', 'weight');
        Yii::$app->db->createCommand('delete `user_attribute` from `user_attribute` left join user as u
            on user_attribute.user_id = u.id
            WHERE u.id is NULL')->execute();
        try {
            $this->dropForeignKey('fk_user_attribute_user', 'user_attribute');
        } catch(Exception $e) {}

        try {
            $this->dropForeignKey('fk_user_attr_user', 'user_attribute');
        } catch(Exception $e) {}

        $this->addForeignKey('fk_user_attribute_user_id', 'user_attribute', 'user_id', 'user', 'id', 'CASCADE');
        $this->alterColumn('user_attribute', 'entity', $this->string(20));
        $this->alterColumn('user_attribute', 'entity_content', $this->string(10));
        $this->alterColumn('user_attribute', 'key', $this->string(25));

        $this->createIndex('user_tariff_starts_at_index', 'user_tariff', 'starts_at');
        $this->createIndex('user_tariff_expires_at_index', 'user_tariff', 'expires_at');
        $this->createIndex('user_tariff_cost_index', 'user_tariff', 'cost');
    }

    public function safeDown()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'full_address' => $this->string(255),
            'zipcode' => $this->string(10),
            'latitude' => $this->string(25),
            'longitude' => $this->string(25),
            'region' => $this->string(55),
            'city' => $this->string(25),
            'street' => $this->string(25),
            'house' => $this->string(6),
            'flat' => $this->integer(2),
            'housing' => $this->integer(5),
            'user_id' => $this->integer()->notNull(),
            'is_default' => $this->boolean()
        ]);
        $this->createIndex('address_is_default_index', 'address', 'is_default');
        $this->addForeignKey('fk_address_user_id', 'address', 'user_id', 'user', 'id', 'CASCADE');

        $this->dropIndex('cart_updated_at_index', 'cart');

        $this->dropIndex('category_type_index', 'category');
        $this->dropIndex('category_alias_index', 'category');

        $this->dropForeignKey('fk_comment_user_id', 'comment');
        $this->createIndex('user_id', 'comment', 'user_id');

        $this->createIndex('conversation_created_at', 'conversation', 'created_at');
        $this->createIndex('conversation_updated_at', 'conversation', 'updated_at');
        $this->dropIndex('conversation_type_index', 'conversation');

        $this->alterColumn('conversation_member', 'status', $this->string(255));

        $this->dropIndex('currency_code_index', 'currency');
        $this->dropIndex('currency_status_index', 'currency');

        $this->dropForeignKey('fk_email_notification_to_id', 'email_notification');
        $this->createIndex('to_id', 'email_notification', 'to_id');

        $this->dropIndex('event_created_at_index', 'event');
        $this->alterColumn('event', 'entity', $this->string(75));
        $this->dropIndex('event_entity_index', 'event');
        $this->dropIndex('event_entity_content_index', 'event');

        $this->addColumn('job', 'is_active', $this->boolean());
        $this->addColumn('job', 'is_moderated', $this->boolean());
        $this->addColumn('job', 'datetime', $this->integer());
        $this->addColumn('job', 'anytime', $this->boolean());
        $this->addColumn('job', 'shipping_available', $this->boolean());
        $this->addColumn('job', 'shipping_only', $this->boolean());
        $this->addColumn('job', 'offline_available', $this->boolean());
        $this->addColumn('job', 'online_available', $this->boolean());
        $this->addColumn('job', 'shipment_available', $this->boolean());
        $this->addColumn('job', 'office_available', $this->boolean());
        $this->addColumn('job', 'asap_available', $this->boolean());
        $this->addColumn('job', 'remote_available', $this->boolean());
        $this->dropForeignKey('fk_job_user_id', 'job');
        $this->createIndex('user_id', 'job', 'user_id');
        $this->dropIndex('job_status_index', 'job');
        $this->dropForeignKey('fk_job_category_id', 'job');
        $this->dropForeignKey('fk_parent_category_id', 'job');

        $this->addColumn('job_attribute', 'weight', $this->integer());
        $this->alterColumn('job_attribute', 'entity_content', $this->string(75));
        $this->alterColumn('job_attribute', 'entity', $this->string(55));
        $this->alterColumn('job_attribute', 'key', $this->string(55));

        $this->alterColumn('job_extra', 'price', $this->string(55));

        $this->dropForeignKey('fk_job_package_job_id', 'job_package');
        $this->createIndex('job_id', 'job_package', 'job_id');

        $this->dropForeignKey('fk_job_qa_job_id', 'job_qa');
        $this->createIndex('job_id', 'job_qa', 'job_id');

        $this->dropIndex('mod_attribute_alias_index', 'mod_attribute');

        $this->dropForeignKey('fk_mod_attribute_value_attribute_id', 'mod_attribute_value');
        $this->createIndex('idx-attr-id', 'mod_attribute_value', 'attribute_id');
        $this->addColumn('mod_attribute_value', 'parent_value_entity', $this->string(75));
        $this->addColumn('mod_attribute_value', 'parent_value_id', $this->integer());
        $this->dropIndex('mod_attribute_value_alias_index', 'mod_attribute_value');

        $this->addColumn('order', 'firstname', $this->string(255));
        $this->addColumn('order', 'lastname', $this->string(255));
        $this->addColumn('order', 'middlename', $this->string(255));
        $this->addColumn('order', 'postcode', $this->string(10));
        $this->addColumn('order', 'country', $this->string(55));
        $this->addColumn('order', 'region', $this->string(55));
        $this->addColumn('order', 'city', $this->string(55));
        $this->addColumn('order', 'street', $this->string(55));
        $this->addColumn('order', 'house', $this->string(6));
        $this->addColumn('order', 'housing', $this->integer());
        $this->addColumn('order', 'room', $this->integer());
        $this->addColumn('order', 'location_option', $this->integer());
        $this->addColumn('order', 'address_id', $this->integer());
        $this->addColumn('order', 'date_option', $this->integer());
        $this->addColumn('order', 'want_asap', $this->boolean());
        $this->addColumn('order', 'wish_date', $this->boolean());
        $this->addColumn('order', 'currency_id', $this->integer());
        $this->dropForeignKey('fk_order_payment_id', 'order');
        $this->dropForeignKey('fk_order_seller_id', 'order');
        $this->createIndex('payment_id', 'order', 'payment_id');
        $this->createIndex('seller_id', 'order', 'seller_id');
        $this->addForeignKey('fk_order_currency_id', 'order', 'currency_id', 'currency', 'id', 'CASCADE');

        $this->addColumn('order_history', 'is_approved', $this->boolean());

        $this->dropForeignKey('fk_order_product_order_id', 'order_product');
        $this->dropIndex('order_product_product_id_index', 'order_product');

        $this->addColumn('page', 'name', $this->string(255));
        $this->dropForeignKey('fk_page_category_id', 'page');
        $this->createIndex('category_id', 'page', 'category_id');
        $this->dropForeignKey('fk_page_user_id', 'page');
        $this->addColumn('page', 'relation_alias', $this->string(255));

        $this->dropForeignKey('fk_payment_user_id', 'payment');
        $this->dropForeignKey('fk_payment_payment_method_id', 'payment');
        $this->createIndex('user_id', 'payment', 'user_id');
        $this->createIndex('payment_method_id', 'payment', 'payment_method_id');
        $this->dropIndex('payment_status_index', 'payment');
        $this->dropIndex('payment_type_index', 'payment');

        $this->addColumn('profile', 'location', $this->string(255));
        $this->addColumn('profile', 'website', $this->string(255));
        $this->addColumn('profile', 'latitude', $this->string(25));
        $this->addColumn('profile', 'longitude', $this->string(25));
        $this->addColumn('profile', 'is_company', $this->boolean());
        $this->addColumn('profile', 'gravatar_id', $this->string(32));
        $this->dropIndex('profile_status_index', 'profile');

        $this->dropForeignKey('fk_review_job_id', 'review');
        $this->createIndex('job_id', 'review', 'job_id');

        $this->alterColumn('seo_advanced', 'type', $this->string(255));
        $this->alterColumn('seo_advanced', 'entity', $this->string(55));

        $this->dropForeignKey('fk_session_user_id', 'session');
        $this->addForeignKey('fk_session_user_id', 'session', 'user_id', 'user', 'id', 'CASCADE');

        $this->dropIndex('tariff_cost_index', 'tariff');

        $this->addColumn('user', 'last_action_at', $this->integer());

        $this->addColumn('user_attribute', 'weight', $this->integer());
        try {
            $this->dropForeignKey('fk_user_attribute_user_id', 'user_attribute');
        } catch (Exception $e) {}

        $this->addForeignKey('fk_user_attr_user', 'user_attribute', 'user_id', 'user', 'id', 'CASCADE');
        $this->alterColumn('user_attribute', 'entity', $this->string(55));
        $this->alterColumn('user_attribute', 'entity_content', $this->string(55));
        $this->alterColumn('user_attribute', 'key', $this->string(55));

        $this->dropIndex('user_tariff_starts_at_index', 'user_tariff');
        $this->dropIndex('user_tariff_expires_at_index', 'user_tariff');
        $this->dropIndex('user_tariff_cost_index', 'user_tariff');
    }
}
