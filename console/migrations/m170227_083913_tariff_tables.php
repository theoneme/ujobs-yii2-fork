<?php

use yii\db\Migration;

class m170227_083913_tariff_tables extends Migration
{
    public function up()
    {
        $this->createTable('tariff', [
            'id' => $this->primaryKey(),
            'title' => $this->string(75)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'cost' => $this->integer()->notNull()->comment('per day'),
            'currency_code' => $this->string(3)->notNull()
        ]);

        $this->createTable('user_tariff', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'duration' => $this->integer(),
            'expires_at' => $this->integer(),
            'cost' => $this->integer()->notNull()->comment('per day'),
            'currency_code' => $this->string(3)->notNull()
        ]);

        $this->createTable('tariff_rule', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer(),
            'code' => $this->string(75)->notNull(),
            'value' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'is_enabled' => $this->integer(1),
        ]);

        $this->addForeignKey('fk_user_tariff_user', 'user_tariff', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_tariff_rule_tariff', 'tariff_rule', 'tariff_id', 'tariff', 'id', 'CASCADE');

        $this->insert('tariff', [
            'title' => 'Ученик',
            'created_at' => time(),
            'updated_at' => time(),
            'cost' => 0,
            'currency_code' => 'RUB'
        ]);

        $this->insert('tariff', [
            'title' => 'Мастер',
            'created_at' => time(),
            'updated_at' => time(),
            'cost' => 15,
            'currency_code' => 'RUB'
        ]);

        $this->insert('tariff', [
            'title' => 'Про',
            'created_at' => time(),
            'updated_at' => time(),
            'cost' => 30,
            'currency_code' => 'RUB'
        ]);
    }

    public function down()
    {
        $this->dropTable('tariff');
        $this->dropTable('user_tariff');
        $this->dropTable('tariff_rule');
    }
}
