<?php

use common\models\JobSearchOption;
use yii\db\Migration;

class m171103_120825_add_type_to_job_search_option extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job_search_option', 'type', $this->integer());
        /* @var $option JobSearchOption*/
        foreach (JobSearchOption::find()->all() as $option) {
            $option->updateAttributes(['type' => $option->defineType()]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('job_search_option', 'type');
    }
}
