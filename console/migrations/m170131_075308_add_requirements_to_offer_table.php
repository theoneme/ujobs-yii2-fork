<?php

use yii\db\Migration;

class m170131_075308_add_requirements_to_offer_table extends Migration
{
    public function up()
    {
        $this->addColumn('offer', 'requirements', $this->text());
    }

    public function down()
    {
        $this->dropColumn('offer', 'requirements');
    }
}
