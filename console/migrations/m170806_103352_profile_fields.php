<?php

use yii\db\Migration;

class m170806_103352_profile_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'is_customer', $this->boolean());
        $this->addColumn('profile', 'is_product_seller', $this->boolean());
        $this->addColumn('profile', 'is_service_seller', $this->boolean());
        $this->addColumn('profile', 'is_freelancer', $this->boolean());
        $this->addColumn('profile', 'city', $this->string(55));

        $this->createIndex('profile_is_customer_index', 'profile', 'is_customer');
        $this->createIndex('profile_is_product_seller_index', 'profile', 'is_product_seller');
        $this->createIndex('profile_is_service_seller_index', 'profile', 'is_service_seller');
        $this->createIndex('profile_is_freelancer_index', 'profile', 'is_freelancer');
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'is_customer');
        $this->dropColumn('profile', 'is_product_seller');
        $this->dropColumn('profile', 'is_service_seller');
        $this->dropColumn('profile', 'is_freelancer');
        $this->dropColumn('profile', 'city');
    }
}
