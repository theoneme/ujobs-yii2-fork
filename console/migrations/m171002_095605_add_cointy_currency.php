<?php

use yii\db\Migration;

class m171002_095605_add_cointy_currency extends Migration
{
    public function safeUp()
    {
        $this->insert('currency', ['code' => 'CTY', 'title' => 'Cointy', 'symbol_right' => ' CTY']);
    }

    public function safeDown()
    {
        $this->delete('currency', ['code' => 'CTY']);
    }
}
