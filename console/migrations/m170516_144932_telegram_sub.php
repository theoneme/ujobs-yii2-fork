<?php

use yii\db\Migration;

class m170516_144932_telegram_sub extends Migration
{
	public function up()
	{
		$this->addColumn('user', 'telegram_uid', $this->integer());
	}

	public function down()
	{
		$this->dropColumn('user', 'telegram_uid');
	}
}
