<?php

use yii\db\Migration;

class m170206_114043_add_balance_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'balance', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('user', 'balance');
    }

}
