<?php

use yii\db\Migration;

class m170210_084025_alter_order_table extends Migration
{
    public function up()
    {
        $this->alterColumn('order', 'total', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('order', 'total', $this->decimal(15, 4));
    }
}
