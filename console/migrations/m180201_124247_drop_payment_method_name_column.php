<?php

use yii\db\Migration;

/**
 * Class m180201_124247_drop_payment_method_name_column
 */
class m180201_124247_drop_payment_method_name_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('payment_method', 'name');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('payment_method', 'name', $this->string());
    }
}
