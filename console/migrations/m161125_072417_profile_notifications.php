<?php

use yii\db\Migration;

class m161125_072417_profile_notifications extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'rcv_order_messages', $this->boolean());
        $this->addColumn('profile', 'rcv_inbox_messages', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('profile', 'rcv_order_messages', $this->boolean());
        $this->dropColumn('profile', 'rcv_inbox_messages', $this->boolean());
    }
}
