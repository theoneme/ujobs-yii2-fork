<?php

use yii\db\Migration;

class m170907_213630_mav_clear extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('delete `mod_attribute_description` from `mod_attribute_description` left join mod_attribute as a
            on mod_attribute_description.entity_content = a.id
            WHERE a.id is NULL and mod_attribute_description.entity = "attribute"')->execute();

        Yii::$app->db->createCommand('delete `mod_attribute_description` from `mod_attribute_description` left join mod_attribute_value as a
            on mod_attribute_description.entity_content = a.id
            WHERE a.id is NULL and mod_attribute_description.entity = "attribute-value"')->execute();
    }

    public function safeDown()
    {

    }
}
