<?php

use yii\db\Migration;

class m161220_074856_package_alter extends Migration
{
    public function up()
    {
        $this->alterColumn('job_package', 'title', $this->text());
        $this->alterColumn('job_package', 'description', $this->text());
    }

    public function down()
    {
        $this->alterColumn('job_package', 'title', $this->string(155));
        $this->alterColumn('job_package', 'description', $this->string(250));
    }
}
