<?php

use yii\db\Migration;

class m170112_114156_review_alter extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'job_id', $this->integer());

        $this->createIndex('job_id', 'review', 'job_id');
    }

    public function down()
    {
        $this->dropColumn('review', 'job_id');
    }
}
