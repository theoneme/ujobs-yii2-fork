<?php

use yii\db\Migration;

/**
 * Handles the creation of table `measure_to_category`.
 */
class m171128_055909_create_measure_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('measure_to_category', [
            'measure_id' => $this->integer(),
            'category_id' => $this->integer()
        ]);

        $this->addPrimaryKey('', 'measure_to_category', ['measure_id', 'category_id']);
        $this->addForeignKey('fk_measure_to_category_measure_id', 'measure_to_category', 'measure_id', 'measure', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_measure_to_category_category_id', 'measure_to_category', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('measure_to_category');
    }
}
