<?php

use yii\db\Migration;

class m170828_115950_user_last_action_at extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'last_action_at', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'last_action_at');
    }
}
