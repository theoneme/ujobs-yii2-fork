<?php

use yii\db\Migration;

class m170322_125230_new_tariff_rules extends Migration
{
    public function up()
    {
        $this->insert('tariff_rule', [
            'id' => 13,
            'tariff_id' => 1,
            'code' => 'allowed_tender_responses',
            'value' => 3,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'id' => 14,
            'tariff_id' => 2,
            'code' => 'allowed_tender_responses',
            'value' => -1,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'id' => 15,
            'tariff_id' => 3,
            'code' => 'allowed_tender_responses',
            'value' => -1,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);
    }

    public function down()
    {
        $this->delete('tariff_rule', [
            'id' => [13, 14 ,15]
        ]);
    }
}
