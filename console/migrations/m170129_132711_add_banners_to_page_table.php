<?php

use yii\db\Migration;

class m170129_132711_add_banners_to_page_table extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'banner', $this->string());
        $this->addColumn('page', 'banner_small', $this->string());
    }

    public function down()
    {
        $this->dropColumn('page', 'banner');
        $this->dropColumn('page', 'banner_small');
    }
}
