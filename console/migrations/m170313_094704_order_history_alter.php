<?php

use common\modules\store\models\Order;
use yii\db\Migration;

class m170313_094704_order_history_alter extends Migration
{
    public function up()
    {
        \common\modules\store\models\OrderHistory::deleteAll([
            'not exists', Order::find()->where('order.id = order_history.order_id')
        ]);

        $this->addForeignKey('fk_order_history_id_order', 'order_history', 'order_id', 'order', 'id', 'CASCADE');
        $this->addForeignKey('fk_order_history_seller_order', 'order_history', 'seller_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_order_history_customer_order', 'order_history', 'customer_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_order_history_id_order', 'order_history');
        $this->dropForeignKey('fk_order_history_seller_order', 'order_history');
        $this->dropForeignKey('fk_order_history_customer_order', 'order_history');
    }
}
