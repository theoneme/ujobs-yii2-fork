<?php

use yii\db\Migration;

class m170301_104254_user_tariff_starts_at extends Migration
{
    public function up()
    {
        $this->addColumn('user_tariff', 'starts_at', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user_tariff', 'starts_at');
    }
}
