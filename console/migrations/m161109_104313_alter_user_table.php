<?php

use yii\db\Migration;

class m161109_104313_alter_user_table extends Migration
{
    public function up()
    {
		$this->dropIndex("user_unique_email", "user");
	    $this->addColumn("user", "phone", $this->string() . " AFTER email");
	    $this->createIndex("user_unique_login", "user", ["email", "phone"], true);
    }

    public function down()
    {
	    $this->dropIndex("user_unique_login", "user");
	    $this->dropColumn("user", "phone");
	    $this->createIndex("user_unique_email", "user", "email", true);
    }

}
