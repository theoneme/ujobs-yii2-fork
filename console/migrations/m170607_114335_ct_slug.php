<?php

use yii\db\Migration;

class m170607_114335_ct_slug extends Migration
{
    public function up()
    {
		$this->addColumn('content_translation', 'slug', $this->string(55)->null());
	    $this->createIndex('translation_slug_index', 'content_translation', 'slug');
    }

    public function down()
    {
        $this->dropColumn('content_translation', 'slug');
    }
}
