<?php

use yii\db\Migration;

class m180115_131145_job_requirements extends Migration
{
    public function safeUp()
    {
        $this->createTable('job_requirement_step', [
            'id' => $this->primaryKey(),
            'job_id' => $this->integer(),
            'title' => $this->string(125),
        ]);
        $this->addForeignKey('fk_job_requirement_step_job_id', 'job_requirement_step', 'job_id', 'job', 'id', 'CASCADE');

        $this->createTable('job_requirement', [
            'id' => $this->primaryKey(),
            'job_id' => $this->integer(),
            'step_id' => $this->integer()->null(),
            'is_required' => $this->boolean(),
            'type' => $this->integer(),
            'title' => $this->string(125),
            'custom_data' => $this->boolean()
        ]);
        $this->addForeignKey('fk_job_requirement_step_id', 'job_requirement', 'step_id', 'job_requirement_step', 'id', 'CASCADE');
        $this->addForeignKey('fk_job_requirement_job_id', 'job_requirement', 'job_id', 'job', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_job_requirement_step_id', 'job_requirement');
        $this->dropForeignKey('fk_job_requirement_job_id', 'job_requirement');
        $this->dropForeignKey('fk_job_requirement_step_job_id', 'job_requirement_step');
        $this->dropTable('job_requirement');
        $this->dropTable('job_requirement_step');
    }
}
