<?php

use yii\db\Migration;

/**
 * Class m180418_144353_friend_status
 */
class m180418_144353_friend_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_friend', 'status', $this->integer()->defaultValue(null));

        $this->createIndex('friend_users_status_index', 'user_friend', 'status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user_friend', 'status');
    }
}
