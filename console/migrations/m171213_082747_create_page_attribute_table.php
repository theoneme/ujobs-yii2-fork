<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page_attribute`.
 */
class m171213_082747_create_page_attribute_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        Yii::$app->db->createCommand('ALTER TABLE page ENGINE=INNODB;')->execute();
        $this->createTable('page_attribute', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'page_id' => $this->bigInteger(),
            'value' => $this->integer(),
            'entity_alias' => $this->string(35),
            'value_alias' => $this->string(75),
            'locale' => $this->string(5)->notNull()
        ]);

        $this->addForeignKey('fk_page_attribute_page_id', 'page_attribute', 'page_id', 'page', 'id', 'CASCADE');
        $this->addForeignKey('fk_page_attribute_value', 'page_attribute', 'value', 'mod_attribute_value', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('page_attribute_entity_alias_index', 'page_attribute', 'entity_alias');
        $this->createIndex('page_attribute_value_alias_index', 'page_attribute', 'value_alias');
        $this->createIndex('page_attribute_locale_index', 'page_attribute', 'locale');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('page_attribute');
        Yii::$app->db->createCommand('ALTER TABLE page ENGINE=MyISAM;')->execute();
    }
}
