<?php

use yii\db\Migration;

class m170208_140743_user_attribute_attribute extends Migration
{
    public function up()
    {
        $this->addColumn('mod_attribute', 'is_system', $this->boolean());

        $this->createIndex('mod_attribute_index', 'mod_attribute', 'is_system');
    }

    public function down()
    {
        $this->dropColumn('mod_attribute', 'is_system');
    }
}
