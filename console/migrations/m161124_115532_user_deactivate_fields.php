<?php

use yii\db\Migration;

class m161124_115532_user_deactivate_fields extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'disabled', $this->boolean());
        $this->addColumn('user', 'disable_reason_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'disabled');
        $this->dropColumn('user', 'disable_reason_id');
    }
}
