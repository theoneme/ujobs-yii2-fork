<?php

use yii\db\Migration;

class m170320_122710_email_alter extends Migration
{
    public function up()
    {
        $this->addColumn('email_notification', 'entity', $this->string(55));
        $this->addColumn('email_notification', 'entity_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('email_notification', 'entity');
        $this->dropColumn('email_notification', 'entity_id');
    }
}
