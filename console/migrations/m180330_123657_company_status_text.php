<?php

use yii\db\Migration;

/**
 * Class m180330_123657_company_status_text
 */
class m180330_123657_company_status_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('company', 'status_text', $this->string(155)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'status_text');
    }
}
