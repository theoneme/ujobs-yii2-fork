<?php

use yii\db\Migration;

class m170109_132131_add_background_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'background', $this->string());
    }

    public function down()
    {
        $this->dropColumn('profile', 'background');
    }
}
