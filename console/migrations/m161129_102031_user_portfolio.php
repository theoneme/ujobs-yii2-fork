<?php

use yii\db\Migration;

class m161129_102031_user_portfolio extends Migration
{
    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user_portfolio}}',
            [
                'id'=> $this->primaryKey(11),
                'title'=> $this->string(155)->notNull(),
                'description'=> $this->string(255)->notNull(),
                'user_id'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_portfolio}}');
    }
}
