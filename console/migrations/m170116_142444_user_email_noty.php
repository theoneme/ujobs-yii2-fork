<?php

use yii\db\Migration;

class m170116_142444_user_email_noty extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%email_notification}}', [
            'id' => $this->primaryKey(),
            'to_id' => $this->integer(),
            'type' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->createIndex('created_at', 'email_notification', 'created_at');
        $this->createIndex('to_id', 'email_notification', 'to_id');
    }

    public function down()
    {
        $this->dropTable('email_notification');
    }
}
