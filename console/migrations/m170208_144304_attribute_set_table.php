<?php

use yii\db\Migration;

class m170208_144304_attribute_set_table extends Migration
{
    public function up()
    {
        $this->addColumn('mod_attribute_group', 'title', $this->string(55));
    }

    public function down()
    {
        $this->dropColumn('mod_attribute_group', 'title');
    }
}
