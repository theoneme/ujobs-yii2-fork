<?php

use yii\db\Migration;

class m161216_152739_job_alter extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'remote_available', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('job', 'remote_available');
    }
}
