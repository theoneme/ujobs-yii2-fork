<?php

use yii\db\Migration;

/**
 * Class m180423_115837_video_parent_category_id
 */
class m180423_115837_video_parent_category_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('video', 'parent_category_id', $this->integer()->defaultValue(null));

        $this->addForeignKey('fk_video_parent_category_id', 'video', 'parent_category_id', 'category', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_video_parent_category_id', 'video');

        $this->dropColumn('video', 'parent_category_id');
    }
}
