<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency_exchange`.
 */
class m170505_122613_create_currency_exchange_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('currency_exchange', [
            'code_from' => $this->string(3),
            'code_to' => $this->string(3),
            'ratio' => $this->decimal(8, 4)
        ], 'CHARACTER SET utf8mb4');

        $this->addPrimaryKey('', 'currency_exchange', ['code_from', 'code_to']);
        $this->addForeignKey('fk_currency_exchange_code_from', 'currency_exchange', 'code_from', 'currency', 'code', 'CASCADE');
        $this->addForeignKey('fk_currency_exchange_code_to', 'currency_exchange', 'code_to', 'currency', 'code', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('currency_exchange');
    }
}
