<?php

use yii\db\Migration;

class m170925_141927_portfolio_locale extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_portfolio', 'locale', $this->string(5)->defaultValue('ru-RU'));

        $this->createIndex('user_portfolio_locale_index', 'user_portfolio', 'locale');
    }

    public function safeDown()
    {
        $this->dropColumn('user_portfolio', 'locale');
    }
}
