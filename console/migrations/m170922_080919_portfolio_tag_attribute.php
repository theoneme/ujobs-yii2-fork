<?php

use common\modules\attribute\models\Attribute;
use yii\db\Migration;

class m170922_080919_portfolio_tag_attribute extends Migration
{
    public function safeUp()
    {
        $attribute = new Attribute([
            'is_system' => true,
            'is_searchable' => true,
            'name' => 'Теги',
            'type' => 'textbox',
            'alias' => 'portfolio_tag'
        ]);
        $attribute->translationsArr['ru-RU']->title = 'Теги';
        $attribute->translationsArr['en-GB']->title = 'Tags';
        $attribute->translationsArr['es-ES']->title = 'Tags';
        $attribute->translationsArr['uk-UA']->title = 'Теги';
        $attribute->save();
    }

    public function safeDown()
    {
        $attribute = Attribute::find()->where(['alias' => 'portfolio_tag'])->one();

        if($attribute !== null) {
            $attribute->delete();
        }
    }
}
