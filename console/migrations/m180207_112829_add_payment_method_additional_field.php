<?php

use yii\db\Migration;

/**
 * Class m180207_112829_add_payment_method_additional_field
 */
class m180207_112829_add_payment_method_additional_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('payment_method', 'additional', $this->boolean());
        $this->createIndex('payment_method_additional', 'payment_method', 'additional');

        $this->update('payment_method', ['additional' => true], ['code' => [
            'alfa_robo',
            'samsung_robo',
            'mts_robo',
            'beeline_robo',
            'tele2_robo',
            'euro_robo',
            'svyaznoy_robo',
            'prom_paw',
            'faktura_paw',
            'ybrir_paw',
            'post_paw',
            'transfer_paw',
            'rus_standard_paw',
            'forward_paw',
            'moscow_credit_paw',
            'elexnet_paw',
            'city_paw',
            'oplata_paw',
            'leader_paw',
        ]]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('payment_method', 'additional');
    }
}
