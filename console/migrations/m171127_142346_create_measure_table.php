<?php

use yii\db\Migration;

/**
 * Handles the creation of table `measure`.
 */
class m171127_142346_create_measure_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('measure', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('measure');
    }
}
