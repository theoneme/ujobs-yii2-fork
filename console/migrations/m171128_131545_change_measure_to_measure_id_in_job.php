<?php

use common\models\Job;
use common\models\Measure;
use yii\db\Migration;

/**
 * Class m171128_131545_change_measure_to_measure_id_in_job
 */
class m171128_131545_change_measure_to_measure_id_in_job extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('job', 'measure_id', $this->integer());
        $this->addForeignKey('fk_job_measure_id', 'job', 'measure_id', 'measure', 'id', 'SET NULL', 'CASCADE');
        $this->dropColumn('job', 'measure');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('job', 'measure', $this->string(50));
        $this->dropForeignKey('fk_job_measure_id', 'job');
        $this->dropColumn('job', 'measure_id');
    }
}