<?php

use yii\db\Migration;

class m161216_151350_job_alter extends Migration
{
    public function up()
    {
        $this->dropColumn('job', 'job_type_option');
        $this->dropColumn('job', 'place_option');
        $this->dropColumn('job', 'time_option');

        $this->addColumn('job', 'offline_available', $this->boolean());
        $this->addColumn('job', 'online_available', $this->boolean());
        $this->addColumn('job', 'shipment_available', $this->boolean());
        $this->addColumn('job', 'office_available', $this->boolean());
        $this->addColumn('job', 'asap_available', $this->boolean());
    }

    public function down()
    {
        $this->addColumn('job', 'job_type_option', $this->string(25));
        $this->addColumn('job', 'place_option', $this->string(25));
        $this->addColumn('job', 'time_option', $this->string(25));

        $this->dropColumn('job', 'offline_available');
        $this->dropColumn('job', 'online_available');
        $this->dropColumn('job', 'shipment_available');
        $this->dropColumn('job', 'office_available');
        $this->dropColumn('job', 'asap_available');
    }
}
