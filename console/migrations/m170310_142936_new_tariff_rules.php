<?php

use yii\db\Migration;

class m170310_142936_new_tariff_rules extends Migration
{
    public function up()
    {
        $this->insert('tariff_rule', [
            'tariff_id' => 1,
            'code' => 'allowed_to_respond_on_advanced_tender',
            'value' => false,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'tariff_id' => 2,
            'code' => 'allowed_to_respond_on_advanced_tender',
            'value' => true,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'tariff_id' => 3,
            'code' => 'allowed_to_respond_on_advanced_tender',
            'value' => true,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);
    }

    public function down()
    {

    }
}
