<?php

use yii\db\Migration;

class m170125_114658_order_extra_alter extends Migration
{
    public function up()
    {
        $this->addColumn('order_extra', 'order_product_id', $this->integer());

        $this->addForeignKey('fk_order_extra_order_product_id', 'order_extra', 'order_product_id', 'order_product', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_order_extra_order_product_id', 'order_extra');

        $this->dropColumn('order_extra', 'order_product_id');
    }
}
