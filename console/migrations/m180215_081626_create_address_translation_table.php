<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address_translation`.
 */
class m180215_081626_create_address_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('address_translation', [
            'lat' => $this->string(50),
            'long' => $this->string(50),
            'locale' => $this->string(5)->notNull(),
            'title' => $this->string(),
        ]);
        $this->addPrimaryKey('', 'address_translation', ['lat', 'long', 'locale']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('address_translation');
    }
}
