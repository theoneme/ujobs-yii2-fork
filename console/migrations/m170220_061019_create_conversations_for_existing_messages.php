<?php

use common\models\Conversation;
use common\models\ConversationMember;
use common\models\Message;
use yii\db\Migration;

class m170220_061019_create_conversations_for_existing_messages extends Migration
{
    public function up()
    {
        $messages = Message::find()->select(['from_id', 'to_id', 'starred'])->groupBy(['from_id', 'to_id'])->asArray()->all();
        $messagesFiltered = [];
        foreach ($messages as $message) {
            if (!isset($messagesFiltered[$message['to_id'] . '_' . $message['from_id']])) {
                $messagesFiltered[$message['from_id'] . '_' . $message['to_id']] = $message;
            }
        }
        $time = time();
        foreach ($messagesFiltered as $message2) {
            $conversation = new Conversation();
            $conversation->save();
            $this->batchInsert('conversation_member', ['conversation_id', 'user_id', 'status', 'starred', 'viewed_at'],[
                [$conversation->id, $message2['from_id'], ConversationMember::STATUS_ACTIVE, ($message2['starred'] == 1 || $message2['starred'] == 11), $time],
                [$conversation->id, $message2['to_id'], ConversationMember::STATUS_ACTIVE, ($message2['starred'] == 10 || $message2['starred'] == 11), $time]
            ]);
            Message::updateAll(['conversation_id' => $conversation->id], ['or',
                ['and',
                    ['from_id' => $message2['from_id']],
                    ['to_id' => $message2['to_id']]
                ],
                ['and',
                    ['from_id' => $message2['to_id']],
                    ['to_id' => $message2['from_id']]
                ]
            ]);
        }
    }

    public function down()
    {
        $messages = Message::find()->joinWith(['conversationMembers'])->all();
        foreach ($messages as $message) {
            /* @var $message Message*/
            foreach ($message->conversationMembers as $member) {
                if ($member->user_id != $message->from_id) {
                    $message->updateAttributes(['to_id' => $member->user_id]);
                    break;
                }
            }
        }
        Conversation::deleteAll();
    }
}
