<?php

use yii\db\Migration;

class m180115_141028_job_requirements_flag extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job', 'requirements_type', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('job', 'requirements_type');
    }
}
