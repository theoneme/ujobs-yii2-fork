<?php

use yii\db\Migration;

class m170411_095540_db_cleanup_2 extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand('delete `job_attribute` from `job_attribute` left join mod_attribute as a
            on job_attribute.entity_content = a.id and job_attribute.entity = "attribute"
            WHERE a.id is NULL')->execute();
    }

    public function down()
    {
        return false;
    }
}
