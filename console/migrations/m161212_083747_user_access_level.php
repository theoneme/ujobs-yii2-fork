<?php

use yii\db\Migration;

class m161212_083747_user_access_level extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'access', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'access');
    }
}
