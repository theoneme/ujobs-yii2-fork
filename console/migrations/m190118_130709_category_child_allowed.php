<?php

use yii\db\Migration;

/**
 * Class m190118_130709_category_child_allowed
 */
class m190118_130709_category_child_allowed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'child_allowed', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'child_allowed');
    }
}
