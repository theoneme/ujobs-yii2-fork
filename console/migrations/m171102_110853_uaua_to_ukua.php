<?php

use yii\db\Migration;

class m171102_110853_uaua_to_ukua extends Migration
{
    public function safeUp()
    {
        \common\models\ContentTranslation::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\models\JobAttribute::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\modules\board\models\ProductAttribute::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\models\UserAttribute::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\modules\attribute\models\AttributeDescription::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\models\SeoAdvanced::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\models\Job::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\modules\board\models\Product::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\models\UserPortfolioAttribute::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
        \common\models\UserPortfolio::updateAll(['locale' => 'uk-UA'], ['locale' => 'ua-UA']);
    }

    public function safeDown()
    {
        \common\models\ContentTranslation::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\models\JobAttribute::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\modules\board\models\ProductAttribute::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\models\UserAttribute::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\modules\attribute\models\AttributeDescription::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\models\SeoAdvanced::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\models\Job::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\modules\board\models\Product::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\models\UserPortfolioAttribute::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
        \common\models\UserPortfolio::updateAll(['locale' => 'ua-UA'], ['locale' => 'uk-UA']);
    }
}
