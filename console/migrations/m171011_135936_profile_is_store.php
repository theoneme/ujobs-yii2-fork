<?php

use yii\db\Migration;

class m171011_135936_profile_is_store extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'is_store', $this->boolean()->defaultValue(false));
        $this->addColumn('profile', 'store_name', $this->string(55));
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'is_store');
        $this->dropColumn('profile', 'store_name');
    }
}
