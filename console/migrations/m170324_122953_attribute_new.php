<?php

use common\modules\attribute\models\Attribute;
use yii\db\Migration;

class m170324_122953_attribute_new extends Migration
{
    public function up()
    {
        $attributes = Attribute::find()->where(['not in', 'id', [6, 7, 8, 36]])->all();

        foreach($attributes as $attribute) {
            $attribute->delete();
        }

        $skillAttribute = new Attribute([
            'name' => 'pro_Навык',
            'id' => 38,
            'type' => 'checkbox',
            'alias' => 'skill',
            'is_searchable' => true,
            'is_system' => true,
        ]);
        $skillAttribute->translationsArr = [
            [
                'title' => 'Навык',
                'description' => '',
                'locale' => 'ru-RU'
            ]
        ];
        $skillAttribute->save();

        $isNewAttribute = new Attribute([
            'name' => 'job_Отметка новый',
            'id' => 39,
            'type' => 'radio',
            'alias' => 'is_new',
            'is_searchable' => true,
            'is_system' => true,
        ]);
        $isNewAttribute->translationsArr = [
            [
                'title' => 'Отметка "новый"',
                'description' => '',
                'locale' => 'ru-RU'
            ]
        ];
        $isNewAttribute->save();

        $languageAttribute = new Attribute([
            'name' => 'pro_Язык',
            'id' => 40,
            'type' => 'checkbox',
            'alias' => 'language',
            'is_searchable' => true,
            'is_system' => true,
        ]);
        $languageAttribute->translationsArr = [
            [
                'title' => 'Язык"',
                'description' => '',
                'locale' => 'ru-RU'
            ]
        ];
        $languageAttribute->save();

        $specialtyAttribute = new Attribute([
            'name' => 'pro_Специальность',
            'id' => 41,
            'type' => 'checkbox',
            'alias' => 'specialty',
            'is_searchable' => true,
            'is_system' => true,
        ]);
        $specialtyAttribute->translationsArr = [
            [
                'title' => 'Язык"',
                'description' => '',
                'locale' => 'ru-RU'
            ]
        ];
        $specialtyAttribute->save();
    }

    public function down()
    {
        $attributes = Attribute::find()->where(['not in', 'id', [6, 7, 8, 36]])->all();

        foreach($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
