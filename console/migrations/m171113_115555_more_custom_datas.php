<?php

use yii\db\Migration;

class m171113_115555_more_custom_datas extends Migration
{
    public function safeUp()
    {
        $this->addColumn('notification', 'custom_data', $this->binary());
        $this->addColumn('payment_history', 'custom_data', $this->binary());
    }

    public function safeDown()
    {
        $this->dropColumn('notification', 'custom_data');
        $this->dropColumn('payment_history', 'custom_data');
    }
}
