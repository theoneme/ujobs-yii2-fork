<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_search_option`.
 */
class m170919_085043_create_job_search_option_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_search_option', [
            'id' => $this->primaryKey(),
            'step_id' => $this->integer(),
            'next_step_id' => $this->integer()->null(),
            'job_id' => $this->integer()
        ]);

        $this->addForeignKey('fk_job_search_option_step_id', 'job_search_option', 'step_id', 'job_search_step', 'id', 'CASCADE');
        $this->addForeignKey('fk_job_search_option_next_step_id', 'job_search_option', 'next_step_id', 'job_search_step', 'id', 'SET NULL');
        $this->addForeignKey('fk_job_search_option_job_id', 'job_search_option', 'job_id', 'job', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('job_search_option');
    }
}
