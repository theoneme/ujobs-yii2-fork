<?php

use yii\db\Migration;

class m170428_114313_add_locale_to_job extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'locale', $this->string(10)->defaultValue('ru-RU'));
        $this->createIndex('job_locale', 'job', 'locale');
    }

    public function down()
    {
        $this->dropColumn('job', 'locale');
    }
}
