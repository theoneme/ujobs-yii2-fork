<?php

use yii\db\Migration;

class m161229_132041_add_message2_flag_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'message2_flag', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('profile', 'message2_flag');
    }
}
