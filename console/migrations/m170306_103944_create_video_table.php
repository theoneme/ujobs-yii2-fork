<?php

use yii\db\Migration;

/**
 * Handles the creation of table `video`.
 */
class m170306_103944_create_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('video', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),

            'title' => $this->string(),
            'description' => $this->text(),
            'price' => $this->integer(),
            'image' => $this->string(),
            'video' => $this->string(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('fk_video_user_id', 'video', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('video');
    }
}
