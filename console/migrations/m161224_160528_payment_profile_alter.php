<?php

use yii\db\Migration;

class m161224_160528_payment_profile_alter extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'payment_qiwi', $this->string(100));
        $this->addColumn('profile', 'payment_yandex_money', $this->string(100));
        $this->addColumn('profile', 'payment_card', $this->string(100));
    }

    public function down()
    {
        $this->dropColumn('profile', 'payment_qiwi');
        $this->dropColumn('profile', 'payment_yandex_money');
        $this->dropColumn('profile', 'payment_card');
    }
}
