<?php

use common\models\user\Profile;
use yii\db\Migration;

class m170118_092413_alter_profile_table extends Migration
{
    public function up()
    {
        $this->renameColumn('profile', 'is_worker', 'status');
        Profile::updateAll(['status' => Profile::STATUS_ACTIVE], ['status' => 1]);
    }

    public function down()
    {
        Profile::updateAll(['status' => 1], ['status' => Profile::STATUS_ACTIVE]);
        $this->renameColumn('profile', 'status', 'is_worker');
    }
}