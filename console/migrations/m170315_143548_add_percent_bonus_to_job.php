<?php

use yii\db\Migration;

class m170315_143548_add_percent_bonus_to_job extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'percent_bonus', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('job', 'percent_bonus');
    }
}
