<?php

use yii\db\Migration;

class m170301_102918_event extends Migration
{
    public function up()
    {
        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'code' => $this->string(75)->notNull(),
            'created_at' => $this->integer(),
            'user_id' => $this->integer(),
            'entity' => $this->string(75),
            'entity_content' => $this->string(75)
        ]);

        $this->addForeignKey('fk_event_user', 'event', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('event');
    }
}
