<?php

use yii\db\Migration;

class m170728_123807_enable_city_filter extends Migration
{
    public function safeUp()
    {
        $this->update('mod_attribute', ['type' => 'autocomplete', 'is_searchable' => true], ['alias' => 'city']);
        $this->insert('setting', ['key' => 'filter_attribute_42', 'value' => 'autocomplete']);
    }

    public function safeDown()
    {
        $this->update('mod_attribute', ['type' => 'radio', 'is_searchable' => false], ['alias' => 'city']);
        $this->delete('setting', ['key' => 'filter_attribute_42']);
    }
}
