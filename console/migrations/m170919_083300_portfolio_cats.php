<?php

use yii\db\Migration;

class m170919_083300_portfolio_cats extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_portfolio', 'parent_category_id', $this->integer());
        $this->addColumn('user_portfolio', 'category_id', $this->integer());
        $this->addColumn('user_portfolio', 'subcategory_id', $this->integer());

        $this->addForeignKey('fk_user_portfolio_parent_category_id', 'user_portfolio', 'parent_category_id', 'category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_category_id', 'user_portfolio', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_subcategory_id', 'user_portfolio', 'subcategory_id', 'category', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_portfolio_parent_category_id', 'user_portfolio');
        $this->dropForeignKey('fk_user_portfolio_category_id', 'user_portfolio');
        $this->dropForeignKey('fk_user_portfolio_subcategory_id', 'user_portfolio');

        $this->dropColumn('user_portfolio', 'parent_category_id');
        $this->dropColumn('user_portfolio', 'category_id');
        $this->dropColumn('user_portfolio', 'subcategory_id');
    }
}
