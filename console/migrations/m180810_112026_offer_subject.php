<?php

use yii\db\Migration;

/**
 * Class m180810_112026_offer_subject
 */
class m180810_112026_offer_subject extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('offer', 'subject', $this->string(155));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('offer', 'subject');
    }
}
