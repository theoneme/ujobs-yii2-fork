<?php

use yii\db\Migration;

class m161207_102737_package_type extends Migration
{
    public function up()
    {
        $this->addColumn('job_package', 'type', $this->string(35));
    }

    public function down()
    {
        $this->dropColumn('job_package', 'type', $this->string(35));
    }
}
