<?php

use yii\db\Migration;

class m161229_071253_comment extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%comment}}',
            [
                'id' => $this->primaryKey(11),
                'entity' => $this->string(55)->notNull(),
                'entity_id' => $this->integer(11)->notNull(),
                'parent_id' => $this->integer(11)->null(),
                'user_id' => $this->integer(11)->notNull(),
                'name' => $this->string(75)->notNull(),
                'email' => $this->string(75)->notNull(),
                'comment' => $this->text(),
                'created_at' => $this->integer()->null(),
                'updated_at' => $this->integer()->null(),
                'is_approved' => $this->integer()->null()
            ], $tableOptions
        );
        $this->createIndex('entity_id', '{{%comment}}', 'entity_id', false);
        $this->createIndex('parent_id', '{{%comment}}', 'parent_id', false);
        $this->createIndex('user_id', '{{%comment}}', 'user_id', false);
        $this->createIndex('entity', '{{%comment}}', 'entity', false);
    }

    public function down()
    {
        $this->dropTable('comment');
    }
}
