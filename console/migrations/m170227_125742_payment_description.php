<?php

use yii\db\Migration;

class m170227_125742_payment_description extends Migration
{
    public function up()
    {
        $this->addColumn('payment', 'description', $this->string(155));
    }

    public function down()
    {
        $this->dropColumn('payment', 'description');
    }
}
