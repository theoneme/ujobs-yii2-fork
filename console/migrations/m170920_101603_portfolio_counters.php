<?php

use yii\db\Migration;

class m170920_101603_portfolio_counters extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_portfolio', 'views_count', $this->integer()->defaultValue(0));
        $this->addColumn('user_portfolio', 'likes_count', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_portfolio', 'views_count');
        $this->dropColumn('user_portfolio', 'likes_count');
    }
}
