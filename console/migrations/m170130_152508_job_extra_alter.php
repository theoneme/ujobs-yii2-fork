<?php

use yii\db\Migration;

class m170130_152508_job_extra_alter extends Migration
{
    public function up()
    {
        $this->alterColumn('job_extra', 'title', $this->string(255));
    }

    public function down()
    {
        $this->alterColumn('job_extra', 'title', $this->string(150));
    }
}
