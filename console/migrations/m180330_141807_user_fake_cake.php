<?php

use yii\db\Migration;

/**
 * Class m180330_141807_user_fake_cake
 */
class m180330_141807_user_fake_cake extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('user', 'is_fake', 'is_company');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('user', 'is_company', 'is_fake');
    }
}
