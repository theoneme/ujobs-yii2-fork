<?php

use yii\db\Migration;

class m171013_075203_attribute_is_parsed extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mod_attribute', 'is_parsed', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('mod_attribute', 'is_parsed');
    }
}
