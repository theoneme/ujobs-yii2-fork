<?php

use common\models\JobSearchOption;
use yii\db\Migration;

class m171031_063049_job_search_option_custom_data extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job_search_option', 'custom_data', $this->text());
        /* @var $options JobSearchOption[]*/
        $options = JobSearchOption::find()->all();
        foreach ($options as $option) {
            $option->updateAttributes(['custom_data' => json_encode(['ru-RU' => $option->job_id])]);
        }
        $this->dropForeignKey('fk_job_search_option_job_id', 'job_search_option');
        $this->dropColumn('job_search_option', 'job_id');
    }

    public function safeDown()
    {
        $this->addColumn('job_search_option', 'job_id', $this->integer());
        $this->addForeignKey('fk_job_search_option_job_id', 'job_search_option', 'job_id', 'job', 'id', 'CASCADE');
        /* @var $options JobSearchOption[]*/
        $options = JobSearchOption::find()->all();
        foreach ($options as $option) {
            $job_id = json_decode($option->custom_data, true)['ru-RU'] ?? null;
            $option->updateAttributes(['job_id' => $job_id]);
            if (!$option->job_id && !$option->next_step_id) {
                $option->delete();
            }
        }
        $this->dropColumn('job_search_option', 'custom_data');
    }
}
