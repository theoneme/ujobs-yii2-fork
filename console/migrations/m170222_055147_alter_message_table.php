<?php

use yii\db\Migration;

class m170222_055147_alter_message_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_message_from', 'message');
        $this->renameColumn('message', 'from_id', 'user_id');
        $this->addForeignKey('fk_message_user_id', 'message', 'user_id', 'user', 'id', 'CASCADE');

        $this->dropForeignKey('fk_message_to', 'message');
        $this->dropColumn('message', 'to_id');

        $this->dropColumn('message', 'status');

        $this->dropColumn('message', 'starred');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_user_id', 'message');
        $this->renameColumn('message', 'user_id', 'from_id');
        $this->addForeignKey('fk_message_from', 'message', 'from_id', 'user', 'id', 'CASCADE');

        $this->addColumn('message', 'to_id', $this->integer());
        $this->addForeignKey('fk_message_to', 'message', 'to_id', 'user', 'id', 'CASCADE');

        $this->addColumn('message', 'status', $this->string());
        $this->createIndex('message_status', 'message', 'status');

        $this->addColumn('message', 'starred', $this->integer());
        $this->createIndex('message_starred', 'message', 'starred');
    }
}
