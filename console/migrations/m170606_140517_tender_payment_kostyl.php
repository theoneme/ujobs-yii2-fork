<?php

use yii\db\Migration;

class m170606_140517_tender_payment_kostyl extends Migration
{
    public function up()
    {
		$this->addColumn('payment', 'old_order_id', $this->integer()->null());
	    $this->addForeignKey('fk_payment_old_order_id', 'payment', 'old_order_id', 'order', 'id', 'CASCADE');
    }

    public function down()
    {
	    $this->dropForeignKey('fk_payment_old_order_id', 'payment');
        $this->dropColumn('payment', 'old_order_id');
    }
}
