<?php

use yii\db\Migration;

class m161216_071507_add_starred_to_message_table extends Migration
{
    public function up()
    {
        $this->addColumn('message', 'starred', $this->integer());
        $this->createIndex('message_starred', 'message', 'starred');
    }

    public function down()
    {
        $this->dropColumn('message', 'starred');
    }
}
