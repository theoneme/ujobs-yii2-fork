<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscription`.
 */
class m180117_125251_create_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subscription', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'entity' => $this->string(25)->notNull(),
            'entity_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_subscription_user_id', 'subscription', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subscription');
    }
}