<?php

use yii\db\Migration;

/**
 * Class m180319_101351_attribute_group_mods
 */
class m180319_101351_attribute_group_mods extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('mod_attribute_group', 'title', $this->string(200));
        $this->addColumn('mod_attribute_group', 'is_generated', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('mod_attribute_group', 'title', $this->string(55));
        $this->dropColumn('mod_attribute_group', 'is_generated');
    }
}
