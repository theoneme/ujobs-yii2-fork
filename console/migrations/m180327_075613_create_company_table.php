<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m180327_075613_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'logo' => $this->string(),
            'banner' => $this->string(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_company_user_id', 'company', 'user_id', 'user', 'id', 'CASCADE');
        $this->createIndex('company_status_index', 'company', 'status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company');
    }
}
