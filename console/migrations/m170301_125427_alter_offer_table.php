<?php

use common\models\Offer;
use yii\db\Migration;

class m170301_125427_alter_offer_table extends Migration
{
    public function up()
    {
        $this->addColumn('offer', 'type', $this->integer());
        $this->createIndex('offer_type', 'offer', 'type');
        $offers = Offer::find()->all();
        foreach ($offers as $offer) {
            /* @var $offer Offer*/
            if ($offer->from_id == $offer->message->user_id) {
                $offer->updateAttributes(['type' => Offer::TYPE_OFFER]);
            }
            else {
                $offer->updateAttributes(['type' => Offer::TYPE_REQUEST]);
            }
        }
        Offer::updateAll(['status' => Offer::STATUS_SENT], [
            'status' => [Offer::STATUS_REQUESTED, Offer::STATUS_OFFERED]
        ]);
    }

    public function down()
    {
        Offer::updateAll(['status' => Offer::STATUS_REQUESTED], [
            'status' => Offer::STATUS_SENT,
            'type' => Offer::TYPE_REQUEST
        ]);
        Offer::updateAll(['status' => Offer::STATUS_OFFERED], [
            'status' => Offer::STATUS_SENT,
            'type' => Offer::TYPE_OFFER
        ]);
        $this->dropColumn('offer', 'type');
    }
}
