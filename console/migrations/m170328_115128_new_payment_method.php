<?php

use yii\db\Migration;

class m170328_115128_new_payment_method extends Migration
{
    public function up()
    {
        $this->insert('payment_method', [
            'name' => 'Получить счет на оплату',
            'code' => 'invoice',
            'logo' => null,
            'sort' => 3,
            'enabled' => 1
        ]);
    }

    public function down()
    {
        $this->delete('payment_method', ['code' => 'invoice']);
    }
}
