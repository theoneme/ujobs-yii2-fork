<?php

use common\modules\store\models\OrderHistory;
use yii\db\Migration;

class m171115_105523_old_order_history_trade_type extends Migration
{
    public function safeUp()
    {
        OrderHistory::updateAll(['type' => OrderHistory::TYPE_CUSTOM_MESSAGE, 'status' => 146], ['type' => OrderHistory::TYPE_TENDER_RESPONSE_PRICE]);
    }

    public function safeDown()
    {
        OrderHistory::updateAll(['type' => OrderHistory::TYPE_TENDER_RESPONSE_PRICE, 'status' => 0], ['type' => OrderHistory::TYPE_CUSTOM_MESSAGE, 'status' => 146]);
    }
}
