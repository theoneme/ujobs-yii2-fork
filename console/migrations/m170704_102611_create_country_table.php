<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m170704_102611_create_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'code' => $this->string(3),
            'currency_code' => $this->string(3)->null()
        ], 'CHARACTER SET utf8mb4');
        $this->addForeignKey('fk_country_currency_code', 'country', 'currency_code', 'currency', 'code', 'SET NULL');
        $this->createIndex('country_code_unique', 'country', 'code', true);
        $this->execute(file_get_contents(__DIR__ . '/data/countries.sql'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('country');
    }
}
