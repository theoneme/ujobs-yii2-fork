<?php

use yii\db\Migration;

class m170127_081239_add_product_type_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'product_type', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order', 'product_type');
    }
}
