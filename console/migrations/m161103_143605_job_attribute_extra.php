<?php

use yii\db\Migration;

class m161103_143605_job_attribute_extra extends Migration
{
    public function up()
    {
        $this->addColumn('job_attribute', 'entity_alias', $this->string(75));
        $this->addColumn('job_attribute', 'value_alias', $this->string(75));
    }

    public function down()
    {
        $this->dropColumn('job_attribute', 'entity_alias');
        $this->dropColumn('job_attribute', 'value_alias');
    }
}
