<?php

use yii\db\Migration;

class m161226_110316_payment_status extends Migration
{
    public function up()
    {
        $this->addColumn('payment', 'status', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('payment', 'status');
    }
}
