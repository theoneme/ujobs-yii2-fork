<?php

use yii\db\Migration;

/**
 * Class m180313_090137_add_cauri_payment_method
 */
class m180313_090137_add_cauri_payment_method extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('payment_method', [
            'code' => 'cauri',
            'enabled' => false
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('payment_method', ['code' => 'cauri']);
    }
}
