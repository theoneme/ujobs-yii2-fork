<?php

use yii\db\Migration;

class m171110_073820_add_galochka_to_job_search_step extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job_search_step', 'show_prices', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('job_search_step', 'show_prices');
    }
}
