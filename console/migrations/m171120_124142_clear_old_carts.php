<?php

use common\modules\store\models\Cart;
use yii\db\Migration;

class m171120_124142_clear_old_carts extends Migration
{
    public function safeUp()
    {
        Cart::deleteAll(['<', 'updated_at', time() - 60 * 60 * 24 * 20]);
    }

    public function safeDown()
    {

    }
}
