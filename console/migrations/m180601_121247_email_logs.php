<?php

use yii\db\Migration;

/**
 * Class m180601_121247_email_logs
 */
class m180601_121247_email_logs extends Migration
{
    public $tableName = '{{%email_log}}';
    public $tableOptions;

    /**
     *
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'data' => $this->binary()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex('email', $this->tableName, 'email');
    }

    /**
     *
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
