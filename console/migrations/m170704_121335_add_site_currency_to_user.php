<?php

use yii\db\Migration;

class m170704_121335_add_site_currency_to_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'site_currency', $this->string(3)->null());
        $this->addForeignKey('fk_user_site_currency', 'user', 'site_currency', 'currency', 'code', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_site_currency', 'user');
        $this->dropColumn('user', 'site_currency');
    }
}