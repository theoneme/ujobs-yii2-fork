<?php

use yii\db\Migration;

class m170329_102846_invoices extends Migration
{
    public function up()
    {
        $this->createTable('invoice', [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'cost' => $this->integer()->notNull(),
            'currency_code' => $this->string(3)->notNull(),
            'invoice_file' => $this->string(155)
        ]);

        $this->addForeignKey('fk_invoice_payment_id', 'invoice', 'payment_id', 'payment', 'id', 'CASCADE');
        $this->addForeignKey('fk_invoice_user_id', 'invoice', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('invoice');
    }
}
