<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_favourite_product`.
 */
class m170816_075027_create_user_favourite_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_favourite_product', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk_user_favourite_product_user_id', 'user_favourite_product', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_favourite_product_product_id', 'user_favourite_product', 'product_id', 'product', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_favourite_product');
    }
}
