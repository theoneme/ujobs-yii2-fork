<?php

use yii\db\Migration;

class m161108_151755_translation_table extends Migration
{
    public function up()
    {
        $this->alterColumn('content_translation', 'seo_title', 'varchar(255)');
        $this->alterColumn('content_translation', 'seo_description', 'varchar(255)');
        $this->alterColumn('content_translation', 'seo_keywords', 'varchar(255)');
    }

    public function down()
    {
        $this->alterColumn('content_translation', 'seo_title', 'varchar(100)');
        $this->alterColumn('content_translation', 'seo_description', 'varchar(215)');
        $this->alterColumn('content_translation', 'seo_keywords', 'varchar(100)');
    }
}
