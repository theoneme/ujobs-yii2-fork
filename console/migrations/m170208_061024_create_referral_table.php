<?php

use yii\db\Migration;

/**
 * Handles the creation of table `referral`.
 */
class m170208_061024_create_referral_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('referral', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(),
            'referral_id' => $this->integer(),
            'email' => $this->string(),
            'status' => $this->integer(),
            'total_bonus' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_referral_created_by', 'referral', 'created_by', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_referral_referral_id', 'referral', 'referral_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('referral');
    }
}
