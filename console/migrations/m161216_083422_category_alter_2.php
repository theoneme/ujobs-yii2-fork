<?php

use yii\db\Migration;

class m161216_083422_category_alter_2 extends Migration
{
    public function up()
    {
        $this->dropColumn('category', 'job_type');
        $this->addColumn('category', 'job_online', $this->boolean());
        $this->addColumn('category', 'job_offline', $this->boolean());
    }

    public function down()
    {
        $this->addColumn('category', 'job_type', $this->string(25));
        $this->dropColumn('category', 'job_online');
        $this->dropColumn('category', 'job_offline');
    }
}
