<?php

use yii\db\Schema;
use yii\db\Migration;

class m161205_164211_job_tag extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%job_tag}}',
            [
                'id'=> $this->primaryKey(11),
                'job_id'=> $this->integer(11)->notNull(),
                'tag'=> $this->string(45)->notNull(),
            ],$tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%job_tag}}');
    }
}