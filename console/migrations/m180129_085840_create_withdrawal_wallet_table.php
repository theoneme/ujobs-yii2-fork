<?php

use yii\db\Migration;

/**
 * Handles the creation of table `withdrawal_wallet`.
 */
class m180129_085840_create_withdrawal_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('withdrawal_wallet', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type' => $this->integer(),
            'number' => $this->string(100),
        ]);
        $this->addForeignKey('fk_withdrawal_wallet_user_id', 'withdrawal_wallet', 'user_id', 'user', 'id', 'CASCADE');
        $this->createIndex('withdrawal_wallet_type_index', 'withdrawal_wallet', 'type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('withdrawal_wallet');
    }
}
