<?php

use yii\db\Migration;

/**
 * Class m180206_060929_new_payment_methods
 */
class m180206_060929_new_payment_methods extends Migration
{
    public function up()
    {
        $this->insert('payment_method', [
            'code' => 'webmoney_robo',
            'enabled' => true
        ]);

        $this->insert('payment_method', [
            'code' => 'yandex_robo',
            'enabled' => true
        ]);

        $this->insert('payment_method', [
            'code' => 'qiwi_robo',
            'enabled' => true
        ]);
    }

    public function down()
    {
        $this->delete('payment_method', ['code' => 'webmoney_robo']);
        $this->delete('payment_method', ['code' => 'yandex_robo']);
        $this->delete('payment_method', ['code' => 'qiwi_robo']);
    }
}
