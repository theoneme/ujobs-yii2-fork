<?php

use yii\db\Migration;

class m171109_092742_job_search_tree_to_entity_entity_content_length extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('job_search_tree_to_entity', 'entity_content', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('job_search_tree_to_entity', 'entity_content', $this->string(20));
    }
}
