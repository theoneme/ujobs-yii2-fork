<?php

use yii\db\Migration;

class m170130_075301_job_attribute_weight extends Migration
{
    public function up()
    {
        $this->addColumn('job_attribute', 'weight', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('job_attribute', 'weight');
    }
}
