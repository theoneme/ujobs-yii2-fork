<?php

use yii\db\Migration;

/**
 * Class m180405_125206_add_status_to_company_member
 */
class m180405_125206_add_status_to_company_member extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('company_member', 'status', $this->integer());
        $this->createIndex('company_member_status', 'company_member', 'status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('company_member', 'status');
    }
}
