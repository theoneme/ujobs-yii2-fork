<?php

use yii\db\Migration;

class m161226_112036_payment_payment_method extends Migration
{
    public function up()
    {
        $this->addColumn('payment', 'payment_method_id', $this->integer());

        $this->createIndex('payment_method_id', '{{%payment}}', 'payment_method_id', false);
    }

    public function down()
    {
        $this->dropColumn('payment', 'payment_method_id');
    }
}
