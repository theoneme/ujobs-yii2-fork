<?php

use yii\db\Migration;

class m161223_142723_database_cleanup extends Migration
{
    public function up()
    {
        $this->execute("SET foreign_key_checks = 0;");
        $this->dropTable('eav_attribute');
        $this->dropTable('eav_attribute_option');
        $this->dropTable('eav_attribute_rules');
        $this->dropTable('eav_attribute_type');
        $this->dropTable('eav_attribute_value');
        $this->dropTable('eav_entity');
        $this->dropTable('ea_appointments');
        $this->dropTable('ea_roles');
        $this->dropTable('ea_secretaries_providers');
        $this->dropTable('ea_services');
        $this->dropTable('ea_services_providers');
        $this->dropTable('ea_service_categories');
        $this->dropTable('ea_settings');
        $this->dropTable('ea_users');
        $this->dropTable('ea_user_settings');
        $this->execute("SET foreign_key_checks = 1;");
    }

    public function down()
    {
//        return false;
        $this->createTable('eav_attribute', ['id' => $this->primaryKey()]);
        $this->createTable('eav_attribute_option', ['id' => $this->primaryKey()]);
        $this->createTable('eav_attribute_rules', ['id' => $this->primaryKey()]);
        $this->createTable('eav_attribute_type', ['id' => $this->primaryKey()]);
        $this->createTable('eav_attribute_value', ['id' => $this->primaryKey()]);
        $this->createTable('eav_entity', ['id' => $this->primaryKey()]);
        $this->createTable('ea_appointments', ['id' => $this->primaryKey()]);
        $this->createTable('ea_roles', ['id' => $this->primaryKey()]);
        $this->createTable('ea_secretaries_providers', ['id' => $this->primaryKey()]);
        $this->createTable('ea_services', ['id' => $this->primaryKey()]);
        $this->createTable('ea_services_providers', ['id' => $this->primaryKey()]);
        $this->createTable('ea_service_categories', ['id' => $this->primaryKey()]);
        $this->createTable('ea_settings', ['id' => $this->primaryKey()]);
        $this->createTable('ea_users', ['id' => $this->primaryKey()]);
        $this->createTable('ea_user_settings', ['id' => $this->primaryKey()]);
    }
}
