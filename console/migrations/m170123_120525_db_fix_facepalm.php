<?php

use yii\db\Migration;

class m170123_120525_db_fix_facepalm extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_language_user', 'user_language', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_certificate_user', 'user_certificate', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_education_user', 'user_education', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_portfolio_user', 'user_portfolio', 'user_id', 'user', 'id', 'CASCADE');

        $this->addForeignKey('fk_job_address_address', 'job_address', 'address_id', 'address', 'id', 'CASCADE');
        $this->addForeignKey('fk_job_address_job', 'job_address', 'job_id', 'job', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_language_user', 'user_language');
        $this->dropForeignKey('fk_certificate_user', 'user_certificate');
        $this->dropForeignKey('fk_education_user', 'user_education');
        $this->dropForeignKey('fk_portfolio_user', 'user_portfolio');

        $this->dropForeignKey('fk_job_address_address', 'job_address');
        $this->dropForeignKey('fk_job_address_job', 'job_address');
    }
}
