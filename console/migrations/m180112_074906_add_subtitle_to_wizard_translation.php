<?php

use yii\db\Migration;

/**
 * Class m180112_074906_add_subtitle_to_wizard_translation
 */
class m180112_074906_add_subtitle_to_wizard_translation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('wizard_translation', 'subtitle', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('wizard_translation', 'subtitle');
    }
}
