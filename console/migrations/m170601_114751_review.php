<?php

use common\modules\store\models\Order;
use yii\db\Migration;

class m170601_114751_review extends Migration
{
    public function up()
    {
		Order::updateAll(['status' => Order::STATUS_AWAITING_REVIEW], ['status' => [Order::STATUS_AWAITING_SELLER_REVIEW, Order::STATUS_AWAITING_CUSTOMER_REVIEW]]);
		Order::updateAll(['status' => Order::STATUS_TENDER_AWAITING_REVIEW], ['status' => [Order::STATUS_TENDER_AWAITING_CUSTOMER_REVIEW, Order::STATUS_TENDER_AWAITING_SELLER_REVIEW]]);
    }

    public function down()
    {
        $orders = Order::findAll(['status' => Order::STATUS_AWAITING_REVIEW]);

        /* @var Order $orders[] */
        foreach($orders as $order) {
	        $customerReview = $order->customerReview;
	        $sellerReview = $order->sellerReview;

	        if ($customerReview && !$sellerReview) {
		        $order->updateAttributes(['status' => Order::STATUS_AWAITING_SELLER_REVIEW]);
	        } else if ($sellerReview && !$customerReview) {
		        $order->updateAttributes(['status' => Order::STATUS_AWAITING_CUSTOMER_REVIEW]);
	        } else {
	        	$order->updateAttributes(['status' => Order::STATUS_AWAITING_SELLER_REVIEW]);
	        }
        }

	    $orders = Order::findAll(['status' => Order::STATUS_TENDER_AWAITING_REVIEW]);

	    /* @var Order $orders[] */
	    foreach($orders as $order) {
		    $customerReview = $order->customerReview;
		    $sellerReview = $order->sellerReview;

		    if ($customerReview && !$sellerReview) {
			    $order->updateAttributes(['status' => Order::STATUS_TENDER_AWAITING_SELLER_REVIEW]);
		    } else if ($sellerReview && !$customerReview) {
			    $order->updateAttributes(['status' => Order::STATUS_TENDER_AWAITING_CUSTOMER_REVIEW]);
		    } else {
			    $order->updateAttributes(['status' => Order::STATUS_TENDER_AWAITING_SELLER_REVIEW]);
		    }
	    }
    }
}
