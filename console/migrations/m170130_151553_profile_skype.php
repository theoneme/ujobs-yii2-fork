<?php

use yii\db\Migration;

class m170130_151553_profile_skype extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'skype', $this->string(75));
    }

    public function down()
    {
        $this->dropColumn('profile', 'skype');
    }
}
