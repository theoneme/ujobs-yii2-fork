<?php

use yii\db\Migration;

class m170106_112047_add_admin_comment_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'admin_comment', $this->text());
    }

    public function down()
    {
        $this->dropColumn('user', 'admin_comment');
    }
}
