<?php

use yii\db\Migration;

class m170206_073657_order_history_alter extends Migration
{
    public function up()
    {
        $this->addColumn('order_history', 'sender_id', $this->integer());

        $this->addForeignKey('fk_order_history_sender_user', 'order_history', 'sender_id', 'user', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn('order_history', 'sender_id');
    }
}
