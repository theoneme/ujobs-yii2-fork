<?php

use yii\db\Migration;

class m171026_125131_add_parent_id_to_attribute_value extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mod_attribute_value', 'parent_value_id', $this->integer());
        $this->addForeignKey('fk_mod_attribute_value_parent_value_id', 'mod_attribute_value', 'parent_value_id', 'mod_attribute_value', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_mod_attribute_value_parent_value_id', 'mod_attribute_value');
        $this->dropColumn('mod_attribute_value', 'parent_value_id');
    }
}
