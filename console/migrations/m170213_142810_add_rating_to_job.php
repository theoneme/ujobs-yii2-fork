<?php

use yii\db\Migration;

class m170213_142810_add_rating_to_job extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'rating', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('job', 'rating');
    }
}
