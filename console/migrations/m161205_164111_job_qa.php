<?php

use yii\db\Schema;
use yii\db\Migration;

class m161205_164111_job_qa extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%job_qa}}',
            [
                'id'=> $this->primaryKey(11),
                'job_id'=> $this->integer(11)->notNull(),
                'question'=> $this->string(125)->notNull(),
                'answer'=> $this->string(300)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('job_id','{{%job_qa}}','job_id',false);
    }

    public function safeDown()
    {
        $this->dropIndex('job_id', '{{%job_qa}}');
        $this->dropTable('{{%job_qa}}');
    }
}