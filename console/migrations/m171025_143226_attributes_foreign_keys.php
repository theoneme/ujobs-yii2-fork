<?php

use yii\db\Migration;

class m171025_143226_attributes_foreign_keys extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_user_attribute_entity_alias', 'user_attribute');
        $this->dropForeignKey('fk_user_attribute_value_alias', 'user_attribute');

        $this->dropForeignKey('fk_job_attribute_entity_alias', 'job_attribute');
        $this->dropForeignKey('fk_job_attribute_value_alias', 'job_attribute');

        $this->dropForeignKey('fk_product_attribute_entity_alias', 'product_attribute');
        $this->dropForeignKey('fk_product_attribute_value_alias', 'product_attribute');

        $this->dropForeignKey('fk_user_portfolio_attribute_entity_alias', 'user_portfolio_attribute');
        $this->dropForeignKey('fk_user_portfolio_attribute_value_alias', 'user_portfolio_attribute');
    }

    public function safeDown()
    {
        $this->addForeignKey('fk_user_attribute_entity_alias', 'user_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_attribute_value_alias', 'user_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_job_attribute_entity_alias', 'job_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_job_attribute_value_alias', 'job_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_product_attribute_entity_alias', 'product_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_attribute_value_alias', 'product_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_user_portfolio_attribute_entity_alias', 'user_portfolio_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_attribute_value_alias', 'user_portfolio_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE');
    }
}
