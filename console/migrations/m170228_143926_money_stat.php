<?php

use yii\db\Migration;

class m170228_143926_money_stat extends Migration
{
    public function up()
    {
        $this->createTable('money_stat', [
            'id' => $this->primaryKey(),
            'year' => $this->integer(),
            'month' => $this->integer(),
            'day' => $this->integer(),
            'advertisement' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('money_stat');
    }
}
