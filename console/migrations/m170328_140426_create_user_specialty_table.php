<?php

use yii\db\Migration;

class m170328_140426_create_user_specialty_table extends Migration
{
    public function up()
    {
        $this->createTable('user_specialty', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'attribute_id' => $this->integer(),

            'title' => $this->string(),
            'alias' => $this->string(),
            'salary' => $this->integer(),
        ]);

        $this->addForeignKey('fk_user_specialty_user_id', 'user_specialty', 'user_id', 'user', 'id', 'CASCADE');
        $this->createIndex('user_specialty_alias', 'user_specialty', 'alias');
    }

    public function down()
    {
        $this->dropTable('user_specialty');
    }
}
