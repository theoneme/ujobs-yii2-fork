<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment_method_country`.
 */
class m180131_142112_create_payment_method_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payment_method_country', [
            'id' => $this->primaryKey(),
            'method_id' => $this->integer(),
            'country_id' => $this->integer(),
        ]);
        $this->addForeignKey('fk_payment_method_country_country_id', 'payment_method_country', 'country_id', 'mod_attribute_value', 'id', 'CASCADE');
        $this->addForeignKey('fk_payment_method_country_method_id', 'payment_method_country', 'method_id', 'payment_method', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('payment_method_country');
    }
}
