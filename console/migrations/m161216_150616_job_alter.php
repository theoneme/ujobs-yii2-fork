<?php

use yii\db\Migration;

class m161216_150616_job_alter extends Migration
{
    public function up()
    {
        $this->addColumn('job', 'job_type_option', $this->string(25));
        $this->addColumn('job', 'place_option', $this->string(25));
        $this->addColumn('job', 'time_option', $this->string(25));
    }

    public function down()
    {
        $this->dropColumn('job', 'job_type_option');
        $this->dropColumn('job', 'place_option');
        $this->dropColumn('job', 'time_option');
    }
}
