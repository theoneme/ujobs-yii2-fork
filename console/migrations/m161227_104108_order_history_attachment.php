<?php

use yii\db\Migration;

class m161227_104108_order_history_attachment extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%order_history}}',
            [
                'id' => $this->primaryKey(11),
                'order_id' => $this->integer(11)->notNull(),
                'seller_id' => $this->integer(11)->notNull(),
                'customer_id' => $this->integer(11)->notNull(),
                'content' => $this->text()->notNull(),
                'status' => $this->integer()->notNull(),
                'type' => $this->integer()->notNull(),
                'created_at' => $this->integer()->null(),
                'updated_at' => $this->integer()->null(),
                'is_approved' => $this->integer()->null()
            ], $tableOptions
        );
        $this->createIndex('order_id', '{{%order_history}}', 'order_id', false);
        $this->createIndex('seller_id', '{{%order_history}}', 'seller_id', false);
        $this->createIndex('customer_id', '{{%order_history}}', 'customer_id', false);

        $this->addColumn('order', 'seller_id', $this->integer());
        $this->createIndex('seller_id', '{{%order}}', 'seller_id', false);
    }

    public function down()
    {
        $this->dropTable('order_history');
        $this->dropColumn('order', 'seller_id');
    }
}
