<?php

use yii\db\Migration;

/**
 * Class m180705_133655_test_result_table
 */
class m180705_133655_test_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('test', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer()->notNull(),
            'alias' => $this->string(75)->notNull(),
            'title' => $this->string(200)->notNull(),
            'status' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex('test_alias_index', 'test', 'alias');
        $this->createIndex('test_status_index', 'test', 'status');
        $this->createIndex('test_group_id_index', 'test', 'group_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('test');
    }
}
