<?php

use yii\db\Migration;

class m171218_114039_user_portfolio_description extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_portfolio', 'description', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('user_portfolio', 'description', $this->string(255));
    }
}
