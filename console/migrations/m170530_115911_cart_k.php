<?php

use yii\db\Migration;

class m170530_115911_cart_k extends Migration
{
    public function up()
    {
		$this->alterColumn('cart', 'cartData', $this->binary());
    }

    public function down()
    {
        $this->alterColumn('cart', 'cartData', $this->text());
    }
}
