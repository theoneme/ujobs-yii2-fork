<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offer`.
 */
class m170123_140448_create_offer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('offer', [
            'id' => $this->primaryKey(),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'message_id' => $this->integer(),
            'description' => $this->text(),
            'status' => $this->integer(),
            'price' => $this->integer()
        ]);

        $this->addForeignKey('fk_offer_from', 'offer', 'from_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_offer_to', 'offer', 'to_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_offer_message', 'offer', 'message_id', 'message', 'id', 'SET NULL');

        $this->createIndex('offer_status', 'offer', 'status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('offer');
    }
}
