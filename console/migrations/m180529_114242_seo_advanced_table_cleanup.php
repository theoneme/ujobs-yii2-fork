<?php

use yii\db\Migration;

/**
 * Class m180529_114242_seo_advanced_table_cleanup
 */
class m180529_114242_seo_advanced_table_cleanup extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('seo_advanced', 'seo_title');
        $this->dropColumn('seo_advanced', 'seo_description');
        $this->dropColumn('seo_advanced', 'seo_keywords');
        $this->dropColumn('seo_advanced', 'heading');
        $this->dropColumn('seo_advanced', 'subheading');
        $this->dropColumn('seo_advanced', 'custom_text_1');
        $this->dropColumn('seo_advanced', 'custom_text_2');
        $this->dropColumn('seo_advanced', 'custom_text_3');
        $this->dropColumn('seo_advanced', 'custom_text_4');
        $this->dropColumn('seo_advanced', 'custom_text_5');
        $this->dropColumn('seo_advanced', 'custom_text_6');
        $this->dropColumn('seo_advanced', 'custom_text_7');
        $this->dropColumn('seo_advanced', 'heading_template');
        $this->dropColumn('seo_advanced', 'title_template');
        $this->dropColumn('seo_advanced', 'description_template');
        $this->dropColumn('seo_advanced', 'keywords_template');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('seo_advanced', 'seo_title', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'seo_description', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'seo_keywords', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'heading', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'subheading', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_1', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_2', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_3', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_4', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_5', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_6', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'custom_text_7', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'heading_template', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'title_template', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'description_template', $this->string(255)->null());
        $this->addColumn('seo_advanced', 'keywords_template', $this->string(255)->null());
    }
}
