<?php

use yii\db\Migration;

class m161226_115626_order_product_alter extends Migration
{
    public function up()
    {
        $this->addColumn('order_product', 'title', $this->string(75));
        $this->addColumn('order_product', 'price', $this->integer());
        $this->addColumn('order_product', 'quantity', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order_product', 'title');
        $this->dropColumn('order_product', 'price');
        $this->dropColumn('order_product', 'quantity');
    }
}
