<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_search_step`.
 */
class m170919_083804_create_job_search_step_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_search_step', [
            'id' => $this->primaryKey(),
            'tree_id' => $this->integer(),
            'previous_step_id' => $this->integer()->null(),
        ]);
        $this->addForeignKey('fk_job_search_step_tree_id', 'job_search_step', 'tree_id', 'job_search_tree', 'id', 'CASCADE');
        $this->addForeignKey('fk_job_search_step_previous_step_id', 'job_search_step', 'previous_step_id', 'job_search_step', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('job_search_step');
    }
}
