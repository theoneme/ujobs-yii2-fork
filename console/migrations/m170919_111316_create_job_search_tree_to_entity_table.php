<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_search_tree_to_entity`.
 */
class m170919_111316_create_job_search_tree_to_entity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_search_tree_to_entity', [
            'id' => $this->primaryKey(),
            'tree_id' => $this->integer(),
            'entity' => $this->string(20),
            'entity_content' => $this->string(20)
        ]);
        $this->addForeignKey('fk_job_search_tree_to_entity_tree_id', 'job_search_tree_to_entity', 'tree_id', 'job_search_tree', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('job_search_tree_to_entity');
    }
}
