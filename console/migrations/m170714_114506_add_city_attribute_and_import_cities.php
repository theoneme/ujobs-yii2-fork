<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\db\Migration;

class m170714_114506_add_city_attribute_and_import_cities extends Migration
{
    public function safeUp()
    {
        $this->insert('mod_attribute', [
            'name' => 'Город',
            'type' => 'radio',
            'alias' => 'city',
            'id' => 42,
            'is_searchable' => true,
            'is_system' => true
        ]);
        $this->insert('mod_attribute_description', [
            'entity' => 'attribute',
            'entity_content' => 42,
            'title' => 'Город',
            'locale' => 'ru-RU'
        ]);
        $cities = explode(',', file_get_contents(Yii::getAlias('@console') . '/import/cities/cities.txt'));
        foreach ($cities as $city){
            $attrValue = new AttributeValue([
                'attribute_id' => 42,
                'translationsArr' => [
                    [
                        'title' => $city,
                        'locale' => 'ru-RU'
                    ]
                ]
            ]);
            $attrValue->save();
        }
    }

    public function safeDown()
    {
        $valueIds = AttributeValue::find()->select('id')->where(['attribute_id' => 42])->column();
        $this->delete('mod_attribute_description', ['entity' => 'attribute-value', 'entity_content' => $valueIds]);
        $this->delete('mod_attribute_description', ['entity' => 'attribute', 'entity_content' => 42]);
        $this->delete('mod_attribute', ['alias' => 'city']);
    }
}
