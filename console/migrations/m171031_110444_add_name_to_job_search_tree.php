<?php

use yii\db\Migration;

class m171031_110444_add_name_to_job_search_tree extends Migration
{
    public function safeUp()
    {
        $this->addColumn('job_search_tree', 'name', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('job_search_tree', 'name');
    }
}
