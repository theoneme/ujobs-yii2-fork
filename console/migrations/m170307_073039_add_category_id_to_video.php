<?php

use yii\db\Migration;

class m170307_073039_add_category_id_to_video extends Migration
{
    public function up()
    {
        $this->addColumn('video', 'category_id', $this->integer());
        $this->addForeignKey('fk_video_category_id', 'video', 'category_id', 'category', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_video_category_id', 'video');
        $this->dropColumn('video', 'category_id');
    }
}
