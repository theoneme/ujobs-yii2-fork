<?php

use yii\db\Migration;

class m170111_141442_review_alter extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_created_for', 'review', 'created_for', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_order_id', 'review', 'order_id', 'order', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_created_for', 'review');
        $this->dropForeignKey('fk_order_id', 'review');
    }
}
