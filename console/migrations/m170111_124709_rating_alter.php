<?php

use yii\db\Migration;

class m170111_124709_rating_alter extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'communication', $this->integer());
        $this->addColumn('review', 'service', $this->integer());
        $this->addColumn('review', 'recommend', $this->integer());
        $this->addColumn('review', 'order_id', $this->integer());

        $this->createIndex('order_id', '{{%review}}', 'order_id', false);
    }

    public function down()
    {
        $this->dropColumn('review', 'communication');
        $this->dropColumn('review', 'service');
        $this->dropColumn('review', 'recommend');
        $this->dropColumn('review', 'order_id');
    }
}
