<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_search_tree`.
 */
class m170919_083454_create_job_search_tree_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_search_tree', [
            'id' => $this->primaryKey()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('job_search_tree');
    }
}
