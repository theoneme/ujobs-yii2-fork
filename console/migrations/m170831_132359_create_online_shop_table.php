<?php

use yii\db\Migration;

/**
 * Handles the creation of table `online_shop`.
 */
class m170831_132359_create_online_shop_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('online_shop', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'city' => $this->string(),
            'email' => $this->string(),
            'site' => $this->string(),
            'vk' => $this->string(),
            'mail_sent' => $this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('online_shop');
    }
}
