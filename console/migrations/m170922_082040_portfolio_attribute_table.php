<?php

use yii\db\Migration;

class m170922_082040_portfolio_attribute_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_portfolio_attribute', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'user_portfolio_id' => $this->integer(),
            'value' => $this->integer(),
            'locale' => $this->string(5)->notNull(),
            'entity_alias' => $this->string(55),
            'value_alias' => $this->string(75)
        ], 'CHARACTER SET utf8');

        $this->addForeignKey('fk_user_portfolio_attribute_user', 'user_portfolio_attribute', 'user_portfolio_id', 'user_portfolio', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_attribute_attribute', 'user_portfolio_attribute', 'attribute_id', 'mod_attribute', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_attribute_entity_alias', 'user_portfolio_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_attribute_value_alias', 'user_portfolio_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE');
        $this->addForeignKey('fk_user_portfolio_attribute_value', 'user_portfolio_attribute', 'value', 'mod_attribute_value', 'id', 'CASCADE');

        $this->createIndex('user_portfolio_attribute_locale_index', 'user_portfolio_attribute', 'locale');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_portfolio_attribute_user', 'user_portfolio_attribute');
        $this->dropForeignKey('fk_user_portfolio_attribute_attribute', 'user_portfolio_attribute');
        $this->dropForeignKey('fk_user_portfolio_attribute_entity_alias', 'user_portfolio_attribute');
        $this->dropForeignKey('fk_user_portfolio_attribute_value_alias', 'user_portfolio_attribute');
        $this->dropForeignKey('fk_user_portfolio_attribute_value', 'user_portfolio_attribute');

        $this->dropTable('user_portfolio_attribute');
    }
}
