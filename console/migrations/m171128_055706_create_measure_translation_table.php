<?php

use yii\db\Migration;

/**
 * Handles the creation of table `measure_translation`.
 */
class m171128_055706_create_measure_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('measure_translation', [
            'id' => $this->primaryKey(),
            'measure_id' => $this->integer(),
            'title' => $this->string(50),
            'locale' => $this->string(10)
        ]);

        $this->addForeignKey('fk_measure_translation_measure_id', 'measure_translation', 'measure_id', 'measure', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('measure_translation');
    }
}
