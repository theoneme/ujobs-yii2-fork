<?php

use yii\db\Migration;

/**
 * Class m180330_114228_profile_default_status
 */
class m180330_114228_profile_default_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('profile', 'status', $this->integer()->defaultValue(10));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('profile', 'status', $this->integer()->defaultValue(null));
    }
}
