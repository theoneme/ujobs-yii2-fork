<?php

use common\models\Page;
use yii\db\Migration;

/**
 * Class m171215_101814_add_status_to_page
 */
class m171215_101814_add_status_to_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('page', 'status', $this->integer());
        $this->createIndex('page_status_index', 'page', 'status');
        $this->dropColumn('page', 'is_published');
        $this->update('page', ['status' => Page::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('page', 'status');
        $this->addColumn('page', 'is_published', $this->integer(4));
        $this->update('page', ['is_published' => 1]);
    }
}
