<?php

use yii\db\Migration;

class m170510_081758_seo_text extends Migration
{
    public function up()
    {
	    $this->addColumn('seo_advanced', 'custom_text_6', $this->string(255)->null());
	    $this->addColumn('seo_advanced', 'custom_text_7', $this->string(255)->null());
		$this->addColumn('seo_advanced', 'locale', $this->string(5)->null()->defaultValue('ru-RU'));
	    $this->addColumn('seo_advanced', 'content', $this->text()->null());
    }

    public function down()
    {
        $this->dropColumn('seo_advanced', 'locale');
	    $this->dropColumn('seo_advanced', 'content');
	    $this->dropColumn('seo_advanced', 'custom_text_6');
	    $this->dropColumn('seo_advanced', 'custom_text_7');
    }
}
