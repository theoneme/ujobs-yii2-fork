<?php

use yii\db\Migration;

class m170403_115336_user_last_action extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'last_action_at', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'last_action_at');
    }
}
