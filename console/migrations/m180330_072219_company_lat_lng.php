<?php

use yii\db\Migration;

/**
 * Class m180330_072219_company_lat_lng
 */
class m180330_072219_company_lat_lng extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('company', 'lat', $this->string(40));
        $this->addColumn('company', 'long', $this->string(40));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'lat');
        $this->dropColumn('company', 'long');
    }
}
