<?php

use yii\db\Migration;

class m170112_143229_add_alias_to_user_skill extends Migration
{
    public function up()
    {
        $this->addColumn('user_skill', 'alias', $this->string());
    }

    public function down()
    {
        $this->dropColumn('user_skill', 'alias');
    }
}
