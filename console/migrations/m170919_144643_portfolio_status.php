<?php

use yii\db\Migration;

class m170919_144643_portfolio_status extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_portfolio', 'status', $this->integer()->defaultValue(\common\models\UserPortfolio::STATUS_ACTIVE));
        $this->createIndex('user_portfolio_status_index', 'user_portfolio', 'status');
    }

    public function safeDown()
    {
        $this->dropColumn('user_portfolio', 'status');
    }
}
