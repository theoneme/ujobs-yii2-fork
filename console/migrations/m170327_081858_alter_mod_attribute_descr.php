<?php

use yii\db\Migration;

class m170327_081858_alter_mod_attribute_descr extends Migration
{
    public function up()
    {
        $this->alterColumn('mod_attribute_description', 'entity_content', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('mod_attribute_description', 'entity_content', $this->string());
    }
}
