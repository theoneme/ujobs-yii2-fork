<?php

use yii\db\Migration;

class m170213_102323_review_fake extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'is_fake', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('review', 'is_fake');
    }
}
