<?php

use yii\db\Migration;

class m170113_073933_job_order_extra extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%job_extra}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(150),
            'description' => $this->string(500),
            'price' => $this->string(125),
            'currency_code' => $this->string(3),
            'additional_days' => $this->integer(),
            'job_id' => $this->integer()
        ], $tableOptions);

        $this->createTable('{{%order_extra}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'job_extra_id' => $this->integer(),
            'quantity' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('job_id', 'job_extra', 'job_id');
        $this->createIndex('order_id', 'order_extra', 'order_id');
        $this->createIndex('job_extra_id', 'order_extra', 'job_extra_id');

        $this->addForeignKey('fk_job_id', 'job_extra', 'job_id', 'job', 'id', 'CASCADE');

        $this->addForeignKey('fk_order_extra_order_id', 'order_extra', 'order_id', 'order', 'id', 'CASCADE');
        $this->addForeignKey('fk_job_extra_id', 'order_extra', 'job_extra_id', 'job_extra', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('order_extra');
        $this->dropTable('job_extra');
    }
}
