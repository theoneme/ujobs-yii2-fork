<?php

use yii\db\Migration;

class m170227_132318_payment_type extends Migration
{
    public function up()
    {
        $this->addColumn('payment', 'type', $this->integer(2));

        $this->update('payment', [
            'type' => 1
        ]);
    }

    public function down()
    {
        $this->dropColumn('payment', 'type');
    }
}
