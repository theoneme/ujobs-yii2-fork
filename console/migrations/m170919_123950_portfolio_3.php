<?php

use yii\db\Migration;

class m170919_123950_portfolio_3 extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_portfolio', 'created_at', $this->integer());
        $this->addColumn('user_portfolio', 'updated_at', $this->integer());
        $this->addColumn('user_portfolio', 'version', $this->integer()->defaultValue(\common\models\UserPortfolio::VERSION_OLD));

        $this->createIndex('user_portfolio_version_index', 'user_portfolio', 'version');
    }

    public function safeDown()
    {
        $this->dropColumn('user_portfolio', 'created_at');
        $this->dropColumn('user_portfolio', 'updated_at');
        $this->dropColumn('user_portfolio', 'version');
    }
}
