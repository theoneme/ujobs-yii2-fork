<?php

use yii\db\Migration;

class m170403_085053_alter_cart_table extends Migration
{
    public function up()
    {
        $this->alterColumn('cart', 'sessionId', $this->string(40));
    }

    public function down()
    {
        $this->alterColumn('cart', 'sessionId', $this->integer());
    }
}