<?php

use yii\db\Migration;

class m170921_070907_user_like extends Migration
{
    public function safeUp()
    {
        $this->createTable('like', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'entity' => $this->string(25)->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_like_user_id', 'like', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_like_user_id', 'like');

        $this->dropTable('like');
    }
}
