<?php

use common\modules\store\models\Currency;
use yii\db\Migration;

class m170504_142504_currency3 extends Migration
{
	public function up()
	{
		$this->alterColumn('currency', 'symbol_left', $this->string(12)->null());
		$this->alterColumn('currency', 'symbol_right', $this->string(12)->null());
		$this->alterColumn('currency', 'decimal_place', $this->string(12)->null());

		$this->insert('currency', [
			'title' => 'Hryvnia',
			'code' => 'UAH',
			'symbol_left' => '',
			'symbol_right' => 'грн.',
		]);
		$this->insert('currency', [
			'title' => 'Dollar',
			'code' => 'USD',
			'symbol_left' => '$',
			'symbol_right' => '',
		]);
		$this->insert('currency', [
			'title' => 'Euro',
			'code' => 'EUR',
			'symbol_left' => '€',
			'symbol_right' => '',
		]);
	}

	public function down()
	{
		Currency::deleteAll(['code' => ['UAH', 'USD', 'EUR']]);

		$this->alterColumn('currency', 'symbol_left', $this->string(12)->notNull());
		$this->alterColumn('currency', 'symbol_right', $this->string(12)->notNull());
		$this->alterColumn('currency', 'decimal_place', $this->string(12)->notNull());
	}
}
