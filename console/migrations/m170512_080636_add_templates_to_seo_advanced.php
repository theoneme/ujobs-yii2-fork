<?php

use yii\db\Migration;

class m170512_080636_add_templates_to_seo_advanced extends Migration
{
    public function up()
    {
        $this->addColumn('seo_advanced', 'template_category', $this->integer());
        $this->addColumn('seo_advanced', 'heading_template', $this->string());
        $this->addColumn('seo_advanced', 'title_template', $this->string());
        $this->addColumn('seo_advanced', 'description_template', $this->string());
        $this->addColumn('seo_advanced', 'keywords_template', $this->string());
    }

    public function down()
    {
        $this->dropColumn('seo_advanced', 'template_category');
        $this->dropColumn('seo_advanced', 'heading_template');
        $this->dropColumn('seo_advanced', 'title_template');
        $this->dropColumn('seo_advanced', 'description_template');
        $this->dropColumn('seo_advanced', 'keywords_template');
    }
}
