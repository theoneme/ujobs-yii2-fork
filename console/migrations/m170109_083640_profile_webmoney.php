<?php

use yii\db\Migration;

class m170109_083640_profile_webmoney extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'payment_webmoney', $this->string(100));
    }

    public function down()
    {
        $this->dropColumn('profile', 'payment_webmoney');
    }
}
