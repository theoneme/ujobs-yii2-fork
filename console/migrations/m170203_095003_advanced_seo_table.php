<?php

use yii\db\Migration;

class m170203_095003_advanced_seo_table extends Migration
{
    public function up()
    {
        $this->createTable('seo_advanced', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(55)->notNull(),
            'entity_content' => $this->string(55)->notNull(),
            'type' => $this->string(),
            'seo_title' => $this->string(255),
            'seo_description' => $this->string(255),
            'seo_keywords' => $this->string(255),
            'heading' => $this->string(255),
            'subheading' => $this->string(255),
            'custom_text_1' => $this->string(255),
            'custom_text_2' => $this->string(255),
            'custom_text_3' => $this->string(255),
            'custom_text_4' => $this->string(255),
            'custom_text_5' => $this->string(255)
        ]);

        $this->createIndex('entity', 'seo_advanced', 'entity');
        $this->createIndex('entity_content', 'seo_advanced', 'entity_content');
        $this->createIndex('type', 'seo_advanced', 'type');
    }

    public function down()
    {
        $this->dropTable('seo_advanced');
    }
}
