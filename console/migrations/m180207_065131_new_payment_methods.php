<?php

use yii\db\Migration;

/**
 * Class m180207_065131_new_payment_methods
 */
class m180207_065131_new_payment_methods extends Migration
{
    public function up()
    {
        $this->batchInsert('payment_method', ['code', 'enabled'], [
            ['webmoney_paw', false],
            ['alfa_robo', false],
            ['card_robo', false],
            ['samsung_robo', false],
            ['mts_robo', false],
            ['beeline_robo', false],
            ['tele2_robo', false],
            ['euro_robo', false],
            ['svyaznoy_robo', false],
            ['prom_paw', false],
            ['faktura_paw', false],
            ['ybrir_paw', false],
            ['post_paw', false],
            ['transfer_paw', false],
            ['rus_standard_paw', false],
            ['forward_paw', false],
            ['moscow_credit_paw', false],
            ['elexnet_paw', false],
            ['city_paw', false],
            ['oplata_paw', false],
            ['leader_paw', false],
        ]);
    }

    public function down()
    {
        $this->delete('payment_method', [
            'code' => [
                'webmoney_paw',
                'alfa_robo',
                'card_robo',
                'samsung_robo',
                'mts_robo',
                'beeline_robo',
                'tele2_robo',
                'euro_robo',
                'svyaznoy_robo',
                'prom_paw',
                'faktura_paw',
                'ybrir_paw',
                'post_paw',
                'transfer_paw',
                'rus_standard_paw',
                'forward_paw',
                'moscow_credit_paw',
                'elexnet_paw',
                'city_paw',
                'oplata_paw',
                'leader_paw',
            ]
        ]);
    }
}
