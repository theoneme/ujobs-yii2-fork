<?php

use yii\db\Migration;

class m161226_134904_order_alter extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'tbd_at', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order', 'tbd_at');
    }
}
