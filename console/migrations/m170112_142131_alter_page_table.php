<?php

use yii\db\Migration;

class m170112_142131_alter_page_table extends Migration
{
    public function up()
    {
        $this->renameColumn('page', 'tag_alias', 'relation_alias');
    }

    public function down()
    {
        $this->renameColumn('page', 'relation_alias', 'tag_alias');
    }
}
