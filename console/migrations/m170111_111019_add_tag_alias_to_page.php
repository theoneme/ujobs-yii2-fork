<?php

use yii\db\Migration;

class m170111_111019_add_tag_alias_to_page extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'tag_alias', $this->string());
        $this->createIndex('page_tag_alias', 'page', 'tag_alias');
    }

    public function down()
    {
        $this->dropIndex('page_tag_alias', 'page');
        $this->dropColumn('page', 'tag_alias');
    }
}
