<?php

use yii\db\Migration;

class m161124_120806_user_deactivate_table extends Migration
{
    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user_deactivation}}',
            [
                'id'=> $this->primaryKey(11),
                'user_id'=> $this->integer(11)->notNull(),
                'reason_id'=> $this->integer(11)->notNull(),
                'reason_additional'=> $this->text()->notNull(),
            ],$tableOptions
        );
        $this->createIndex('reason_id','{{%user_deactivation}}','reason_id',false);
        $this->createIndex('user_id','{{%user_deactivation}}','user_id',false);

        $this->dropColumn('user', 'disable_reason_id');
    }

    public function safeDown()
    {
        $this->dropIndex('reason_id', '{{%user_deactivation}}');
        $this->dropIndex('user_id', '{{%user_deactivation}}');
        $this->dropTable('{{%user_deactivation}}');

        $this->addColumn('user', 'disable_reason_id', $this->integer());
    }
}
