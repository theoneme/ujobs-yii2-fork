<?php

use yii\db\Migration;

/**
 * Class m170413_150638_user_category
 */
class m170413_150638_user_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_category', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk_user_category_user_id', 'user_category', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_category_category_id', 'user_category', 'category_id', 'category', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_favourite');
    }
}
