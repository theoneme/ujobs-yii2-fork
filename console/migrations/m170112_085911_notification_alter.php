<?php

use yii\db\Migration;

class m170112_085911_notification_alter extends Migration
{
    public function up()
    {
        $this->addColumn('notification', 'is_read', $this->boolean());

        $this->createIndex('is_read', 'notification', 'is_read');
    }

    public function down()
    {
        $this->dropColumn('notification', 'is_read');
    }
}
