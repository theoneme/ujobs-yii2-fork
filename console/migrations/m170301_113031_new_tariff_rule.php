<?php

use yii\db\Migration;

class m170301_113031_new_tariff_rule extends Migration
{
    public function up()
    {
        $this->insert('tariff_rule', [
            'tariff_id' => 1,
            'code' => 'time_since_tender_post_to_respond',
            'value' => 60 * 60 * 24 * 2,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);

        $this->insert('tariff_rule', [
            'tariff_id' => 3,
            'code' => 'time_since_tender_post_to_respond',
            'value' => 0,
            'created_at' => time(),
            'updated_at' => time(),
            'is_enabled' => true
        ]);
    }

    public function down()
    {

    }
}
