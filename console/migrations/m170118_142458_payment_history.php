<?php

use yii\db\Migration;

class m170118_142458_payment_history extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_history}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'order_id' => $this->integer(),
            'type' => $this->integer(),
            'description' => $this->string(255),
            'balance_change' => $this->integer(),
            'amount' => $this->integer(),
            'currency_code' => $this->string(3),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->createIndex('user_id', 'payment_history', 'user_id');
        $this->createIndex('order_id', 'payment_history', 'order_id');
        $this->createIndex('type', 'payment_history', 'type');

        $this->addForeignKey('fk_payment_history_to_user', 'payment_history', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_payment_history_to_order', 'payment_history', 'order_id', 'order', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('payment_history');
    }
}
