<?php

use common\modules\store\models\OrderHistory;
use yii\db\Migration;

class m170602_060631_order_history_fix extends Migration
{
    public function up()
    {
		OrderHistory::updateAll(['sender_id' => new \yii\db\Expression('seller_id')], ['or', ['sender_id' => null], ['sender_id' => '']]);
    }

    public function down()
    {

    }
}
