<?php

use common\modules\attribute\models\Attribute;
use common\modules\board\models\ProductAttribute;
use yii\db\Migration;

class m170831_111209_pa_refactoring extends Migration
{
    public function safeUp()
    {
        \common\modules\board\models\ProductAttribute::deleteAll(['value' => null]);

        $this->dropColumn('product_attribute', 'entity');

        $this->renameColumn('product_attribute', 'entity_content', 'attribute_id');
//
//        $conditionAttribute = Attribute::find()->where(['alias' => 'condition'])->one();
//        ProductAttribute::updateAll(['attribute_id' => $conditionAttribute->id], ['entity_alias' => 'condition']);

        Yii::$app->db->createCommand('delete `product_attribute` from `product_attribute` left join mod_attribute as a
            on product_attribute.attribute_id = a.id
            WHERE a.id is NULL')->execute();
        $this->addForeignKey('fk_product_attribute_attribute_id', 'product_attribute', 'attribute_id', 'mod_attribute', 'id', 'CASCADE', 'CASCADE');

        Yii::$app->db->createCommand('delete `product_attribute` from `product_attribute` left join mod_attribute as a
            on product_attribute.entity_alias = a.alias
            WHERE a.alias is NULL')->execute();
        $this->addForeignKey('fk_product_attribute_entity_alias', 'product_attribute', 'entity_alias', 'mod_attribute', 'alias', 'CASCADE', 'CASCADE');

//        Yii::$app->db->createCommand('delete `product_attribute` from `product_attribute` left join mod_attribute_value as v
//            on product_attribute.value_alias = v.alias and product_attribute.attribute_id = v.attribute_id
//            WHERE v.alias is NULL')->execute();
//        $this->addForeignKey('fk_product_attribute_value_alias', 'product_attribute', 'value_alias', 'mod_attribute_value', 'alias', 'CASCADE', 'CASCADE');

        $this->dropColumn('product_attribute', 'key');
    }

    public function safeDown()
    {
        $this->addColumn('product_attribute', 'entity', $this->string(20));

        $this->renameColumn('product_attribute', 'attribute_id', 'entity_content');
        $this->dropForeignKey('fk_product_attribute_attribute_id', 'product_attribute');
        $this->dropForeignKey('fk_product_attribute_entity_alias', 'product_attribute');
        //$this->dropForeignKey('fk_product_attribute_value_alias', 'product_attribute');

        $this->addColumn('product_attribute', 'key', $this->string(25));
    }
}
