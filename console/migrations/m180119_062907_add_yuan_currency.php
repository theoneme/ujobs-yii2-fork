<?php

use yii\db\Migration;

/**
 * Class m180119_062907_add_yuan_currency
 */
class m180119_062907_add_yuan_currency extends Migration
{
    public function safeUp()
    {
        $this->insert('currency', [
            'title' => 'Yuan',
            'code' => 'CNY',
            'symbol_right' => '¥',
        ]);
    }

    public function safeDown()
    {
        $this->delete('currency', ['code' => 'CNY']);
    }
}
