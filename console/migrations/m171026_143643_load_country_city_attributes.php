<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\db\Migration;

class m171026_143643_load_country_city_attributes extends Migration
{
    public function safeUp()
    {
        $countryAttrId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
        $cityAttrId = Attribute::find()->where(['alias' => 'city'])->select('id')->scalar();
        $countries = include(Yii::getAlias('@console') . '/import/cities/countries_cities.php');
        foreach ($countries as $country => $cities) {
            $countryAttribute = new AttributeValue([
                'attribute_id' => $countryAttrId,
                'alias' => preg_replace("/[^\w-+]/", "", Yii::$app->utility->transliterate(trim($country))),
                'status' => AttributeValue::STATUS_APPROVED,
                'translationsArr' => [
                    ['title' => $country, 'locale' => 'ru-RU']
                ]
            ]);
            $countryAttribute->save();
            foreach ($cities as $city) {
                $alias = preg_replace("/[^\w-+]/", "", Yii::$app->utility->transliterate(trim($city)));
                $exists = AttributeValue::find()->joinWith('translations')
                    ->where(['attribute_id' => $cityAttrId])
                    ->andWhere(['or', ['alias' => $alias], ['mod_attribute_description.title' => $city]])
                    ->exists();
                if (!$exists) {
                    $cityAttribute = new AttributeValue([
                        'attribute_id' => $cityAttrId,
                        'alias' => $alias,
                        'status' => AttributeValue::STATUS_APPROVED,
                        'parent_value_id' => $countryAttribute->id,
                        'translationsArr' => [
                            ['title' => $city, 'locale' => 'ru-RU']
                        ]
                    ]);
                    $cityAttribute->save();
                }
            }
        }
        $russiaCountryId = AttributeValue::find()
            ->select('mod_attribute_value.id')
            ->joinWith('translations')
            ->where(['attribute_id' => $countryAttrId, 'mod_attribute_description.title' => 'Россия'])
            ->scalar();
        AttributeValue::updateAll(['parent_value_id' => $russiaCountryId], [
            'attribute_id' => $cityAttrId,
            'parent_value_id' => null
        ]);
    }

    public function safeDown()
    {
        $countryAttrId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
        $cityAttrId = Attribute::find()->where(['alias' => 'city'])->select('id')->scalar();
        $russiaCountryId = AttributeValue::find()
            ->select('mod_attribute_value.id')
            ->joinWith('translations')
            ->where(['attribute_id' => $countryAttrId, 'mod_attribute_description.title' => 'Россия'])
            ->scalar();
        AttributeValue::updateAll(['parent_value_id' => null], [
            'attribute_id' => $cityAttrId,
            'parent_value_id' => $russiaCountryId
        ]);
        AttributeValue::deleteAll(['and', ['attribute_id' => $cityAttrId], ['not', ['parent_value_id' => null]]]);
        AttributeValue::deleteAll(['attribute_id' => $countryAttrId]);
    }
}
