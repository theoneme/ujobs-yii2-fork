<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_contact`.
 */
class m170808_125044_create_user_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_contact', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'value' => $this->string(),
            'type' => $this->integer(),
            'confirmed' => $this->boolean()
        ]);
        $this->addForeignKey('fk_user_contact_user_id', 'user_contact', 'user_id', 'user', 'id', 'CASCADE');
        $this->createIndex('user_contact_type', 'user_contact', 'type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_contact');
    }
}
