<?php

use yii\db\Migration;

/**
 * Handles the creation of table `freelancers`.
 */
class m170404_132514_create_freelancers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('freelancer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'specialties' => $this->text(),
            'skype' => $this->string(),
            'email' => $this->string(),
            'mail_sent' => $this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('freelancer');
    }
}
