<?php

use yii\db\Migration;

class m170209_102754_attribute_table_cleanup extends Migration
{
    public function up()
    {
        $this->dropColumn('mod_attribute', 'root');
        $this->dropColumn('mod_attribute', 'lft');
        $this->dropColumn('mod_attribute', 'rgt');
        $this->dropColumn('mod_attribute', 'lvl');
        $this->dropColumn('mod_attribute', 'icon');
        $this->dropColumn('mod_attribute', 'icon_type');
        $this->dropColumn('mod_attribute', 'active');
        $this->dropColumn('mod_attribute', 'selected');
        $this->dropColumn('mod_attribute', 'disabled');
        $this->dropColumn('mod_attribute', 'readonly');
        $this->dropColumn('mod_attribute', 'visible');
        $this->dropColumn('mod_attribute', 'collapsed');
        $this->dropColumn('mod_attribute', 'movable_u');
        $this->dropColumn('mod_attribute', 'movable_d');
        $this->dropColumn('mod_attribute', 'movable_r');
        $this->dropColumn('mod_attribute', 'movable_l');
        $this->dropColumn('mod_attribute', 'removable');
        $this->dropColumn('mod_attribute', 'removable_all');
    }

    public function down()
    {
        $this->addColumn('mod_attribute', 'root', $this->integer());
        $this->addColumn('mod_attribute', 'lft', $this->integer());
        $this->addColumn('mod_attribute', 'rgt', $this->integer());
        $this->addColumn('mod_attribute', 'lvl', $this->integer());
        $this->addColumn('mod_attribute', 'icon', $this->string(255));
        $this->addColumn('mod_attribute', 'icon_type', $this->integer(1));
        $this->addColumn('mod_attribute', 'active', $this->integer(1));
        $this->addColumn('mod_attribute', 'selected', $this->integer(1));
        $this->addColumn('mod_attribute', 'disabled', $this->integer(1));
        $this->addColumn('mod_attribute', 'readonly', $this->integer(1));
        $this->addColumn('mod_attribute', 'visible', $this->integer(1));
        $this->addColumn('mod_attribute', 'collapsed', $this->integer(1));
        $this->addColumn('mod_attribute', 'movable_d', $this->integer(1));
        $this->addColumn('mod_attribute', 'movable_u', $this->integer(1));
        $this->addColumn('mod_attribute', 'movable_r', $this->integer(1));
        $this->addColumn('mod_attribute', 'movable_l', $this->integer(1));
        $this->addColumn('mod_attribute', 'removable', $this->integer(1));
        $this->addColumn('mod_attribute', 'removable_all', $this->integer(1));
    }
}
