<?php

use yii\db\Migration;

class m170907_214451_attribute_to_group_sort_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mod_attribute_to_group', 'sort_order', $this->integer());
        $this->createIndex('mod_attribute_to_group_index', 'mod_attribute_to_group', 'sort_order');
    }

    public function safeDown()
    {
        $this->dropColumn('mod_attribute_to_group', 'sort_order');
    }
}
