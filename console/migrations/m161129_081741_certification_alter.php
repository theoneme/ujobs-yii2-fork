<?php

use yii\db\Migration;

class m161129_081741_certification_alter extends Migration
{
    public function up()
    {
        $this->addColumn('user_certificate', 'year', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user_certificate', 'year');
    }
}
