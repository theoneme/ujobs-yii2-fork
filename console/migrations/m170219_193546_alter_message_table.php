<?php

use yii\db\Migration;

class m170219_193546_alter_message_table extends Migration
{
    public function up()
    {
        $this->addColumn('message', 'conversation_id', $this->integer());
        $this->addForeignKey('fk_message_conversation_id', 'message', 'conversation_id', 'conversation', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_conversation_id', 'message');
        $this->dropColumn('message', 'conversation_id');
    }
}
