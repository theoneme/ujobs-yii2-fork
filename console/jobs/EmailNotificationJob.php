<?php

namespace console\jobs;

use common\models\EmailLog;
use common\models\user\User;
use Yii;
use yii\swiftmailer\Mailer;

/**
 * Class EmailNotificationJob.
 */
class EmailNotificationJob extends \yii\base\BaseObject implements \yii\queue\RetryableJob
{
    public $unSubUrl;

    public $mailer;

    public $viewPath;

    public $view;

    public $receiverEmail;

    public $subject;

    public $params;

    public $user;

    public $user2;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $params = json_decode($this->params, true);
        $params['user'] = $this->user ? User::find()->joinWith(['profile'])->where(['id' => $this->user])->one() : null;
        $params['user2'] = $this->user2 ? User::find()->joinWith(['profile'])->where(['id' => $this->user2])->one() : null;

        /** @var Mailer $mailer */
        $mailer = Yii::$app->{$this->mailer};
        $mailer->viewPath = $this->viewPath;

        $result = $mailer->compose([
            'html' => $this->view
        ], $params)
            ->setTo($this->receiverEmail)
            ->setFrom($params['from'])
            ->setSubject($this->subject)
            ->addHeader('List-Unsubscribe', "<{$this->unSubUrl}>")
            ->send();

        if($result === true) {
            $emailLog = new EmailLog([
                'email' => $this->receiverEmail,
                'customDataArray' => [
                    'unSubUrl' => $this->unSubUrl,
                    'mailer' => $this->mailer,
                    'viewPath' => $this->viewPath,
                    'params' => $this->params,
                    'view' => $this->view,
                    'subject' => $this->subject,
                    'receiverEmail' => $this->receiverEmail,
                    'user' => $this->user,
                    'user2' => $this->user2
                ]
            ]);

            $emailLog->save();
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 60;
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
