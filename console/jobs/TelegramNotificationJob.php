<?php

namespace console\jobs;

use frontend\modules\telegram\services\TelegramFactory;
use Longman\TelegramBot\Request;

/**
 * Class TelegramNotificationJob.
 */
class TelegramNotificationJob extends \yii\base\BaseObject implements \yii\queue\RetryableJob
{
    public $chatId;

    public $text;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $telegram = TelegramFactory::getInstance();
        $response = Request::sendMessage([
            'chat_id' => $this->chatId,
            'text' => $this->text,
        ]);

        return $response->isOk();
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 60;
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}


