<?php

namespace console\jobs;

use Minishlink\WebPush\WebPush;

/**
 * Class PushNotificationJob.
 */
class PushNotificationJob extends \yii\base\BaseObject implements \yii\queue\RetryableJob
{
    public $pushId;

    public $pushKey;

    public $pushToken;

    public $text;

    private $auth = ['VAPID' => [
        'subject' => 'mailto:devouryo@gmail.com',
        'publicKey' => 'BMuUgP6qOgp0onw4MyAN2vvESGyNC-WPtbfzUgRJ-xMon7ouL3NkL8Mzfwe4oafPxOcOqQLlk6OFcyNbaiZIk2E',
        'privateKey' => '53TxV0smrnRzQthVB71eUaPlRkIZLNkYAyIwMEsQxjo',
    ]];

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $push = new WebPush($this->auth);

        return (bool)$push->sendNotification(
            $this->pushId,
            $this->text,
            $this->pushKey,
            $this->pushToken,
            true
        );
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 60;
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}


