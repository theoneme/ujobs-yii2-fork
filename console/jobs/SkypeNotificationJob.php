<?php

namespace console\jobs;

use frontend\modules\skype\services\SkypeFactory;
use SkypeBot\Command\SendMessage;

/**
 * Class SkypeNotificationJob.
 */
class SkypeNotificationJob extends \yii\base\BaseObject implements \yii\queue\RetryableJob
{
    public $chatId;

    public $text;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $skype = SkypeFactory::getInstance();
        $response = $skype->getApiClient()->call(
            new SendMessage(
                $this->text,
                $this->chatId
            )
        );

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 60;
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}


