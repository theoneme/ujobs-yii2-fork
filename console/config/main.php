<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'attribute', 'store', 'board', 'queue'],
    'name' => 'uJobs',
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'utility' => [
            'class' => 'common\components\Utility',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'baseUrl' => 'https://ujobs.me/',
            'hostInfo' => 'https://ujobs.me/',
            'rules' => [
                'sitemap.xml' => 'sitemap/default/index',
                'sitemap-<action:\w+>.xml' => 'sitemap/default/<action>',

                '/nachat-zarabativat' => 'page/start-selling',
                '/news' => 'news/list',
                '/catalog-pro' => 'category/pro-catalog',
                '/catalog-job' => 'category/job-catalog',
                '/catalog-tender' => 'category/tender-catalog',

                'static/<category_1:[\w-]+>/<category_2:[\w-]+>/<category_3:[\w-]+>/<alias:[\w-]+>' => 'page/static',
                'static/<category_1:[\w-]+>/<category_2:[\w-]+>/<alias:[\w-]+>' => 'page/static',
                'static/<category_1:[\w-]+>/<alias:[\w-]+>' => 'page/static',
                'static/<alias:[\w-]+>' => 'page/static',

                'conversation/<user_id:\d+>/<conversation_id:\d+>' => 'account/inbox/conversation',
                'conversation/<user_id:\d+>' => 'account/inbox/conversation',
                'account/inbox/<action:(star|view|delete)>' => 'account/inbox/<action>',
                'account/inbox/<status:[\w-]+>' => 'account/inbox/index',
                'news/<alias:[\w-]+>' => 'news/view',

                '<alias:[\w-]+>-<action:(tender|job)>' => '<action>/view',
                '<category_1:[\w-]+>-<action:(category|jobs|tenders|pros)>' => 'category/<action>',

                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'notifications*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'notifications' => 'notifications.php',
                    ],
                ],
                'account*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'account' => 'account.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'rbac' => 'dektrium\rbac\RbacConsoleModule',
        'treemanager' => [
            'class' => 'kartik\tree\Module',
        ],
        'attribute' => [
            'class' => 'common\modules\attribute\Module',
        ],
        'user' => [
            'layout' => '@frontend/views/layouts/main',
            'class' => 'dektrium\user\Module',
        ],
	    'store' => [
		    'class' => 'common\modules\store\Module',
	    ],
        'board' => [
            'class' => 'common\modules\board\Module'
        ]
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\faker\FixtureController',
        ],
//        'migrate' => [
//            'class' => 'yii\console\controllers\MigrateController',
//            'migrationPath' => '@console/migrations',
//            'migrationNamespaces' => [
////                'common\modules\board\migrations',
////                'common\modules\store\migrations',
//                'common\modules\wizard\migrations',
//                'yii\queue\db\migrations',
//            ],
//        ],
    ],
    'params' => $params,
];
