<?php
return [
    'adminEmail' => 'admin@example.com',

    'images' => [
        'catalog' => [
            'width' => 206,
            'height' => 206
        ],
        'product' => [
            'width' => 1000,
            'height' => 1000
        ],
        'news' => [
            'width' => 420,
            'height' => 150
        ]
    ],
    'app_currency_code' => 'RUB'
];
