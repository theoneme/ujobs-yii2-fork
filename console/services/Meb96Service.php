<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2017
 * Time: 14:06
 */

namespace console\services;

use common\models\Category;
use common\models\user\Profile;
use common\modules\board\models\Product;
use DOMDocument;
use Yii;
use yii\base\InvalidParamException;
use yii\console\Controller;

/**
 * Class Meb96Service
 * @package console\services
 */
class Meb96Service extends Reader
{
    /**
     * @var array
     */
    private $config = [
        'categories' => [
            69 => [
                'id' => 2594,
                'lvl' => 3
            ],
            70 => [
                'id' => 2580,
                'lvl' => 3
            ],
            71 => [
                'id' => 2597,
                'lvl' => 3
            ],
            79 => [
                'id' => 2610,
                'lvl' => 3
            ],
        ],
        'base' => [
            'title' => [
                'name'
            ]
        ],
        'attributes' => [
            69 => [
                'vendor' => 762,
            ],
            70 => [
                'vendor' => 571
            ],
            71 => [
                'vendor' => 791
            ],
            79 => [
                'vendor' => 924
            ]
        ]
    ];

    /**
     * @var array
     */
    private $params = [
        'user_id' => 19013,
        'address' => 'г. Волгоград, ул. Никитина 2',
        'lat' => '48.598934',
        'long' => '44.434440',
        'status' => Product::STATUS_ACTIVE,
        'type' => Product::TYPE_PRODUCT,
        'currency_code' => 'RUB',
        'secure' => true
    ];

    const URL = 'https://kupimebel34.ru/index.php?route=feed/yandex_yml';
    const LOCAL_URL = 'console/import/stores/kupimebel.xml';

    /**
     * Meb96Service constructor.
     * @param array $config
     * @param array $params
     */
    public function __construct(array $config = [], array $params = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->params = array_merge($this->params, $params);
    }

    /**
     * @return bool
     */
    public function load()
    {
        $dom = new DOMDocument();
        $dom->load(self::LOCAL_URL);

        Product::updateAll(['status' => Product::STATUS_DELETED], ['user_id' => $this->params['user_id']]);
        Profile::updateAll(['is_store' => 1, 'store_name' => 'KupiMebel'], ['user_id' => $this->params['user_id']]);

        $items = $this->readOffers($dom, $this->config);

        $writer = new Writer($items, $this->params);
        return $writer->save();
    }

    /**
     * @param bool $physical
     * @return int
     */
    public function remove($physical = false)
    {
        Profile::updateAll(['is_store' => 0], ['user_id' => $this->params['user_id']]);
        if($physical === false) {
            return Product::updateAll(['status' => Product::STATUS_DELETED], ['user_id' => $this->params['user_id']]);
        } else {
            $products = Product::find()->where(['user_id' => $this->params['user_id']])->all();
            $result = true;
            if($products !== null) {
                foreach($products as $product) {
                    $result = $result && $product->delete();
                }
            }
        }

        return $result;
    }
}