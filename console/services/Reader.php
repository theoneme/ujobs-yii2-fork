<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2017
 * Time: 15:05
 */

namespace console\services;

use common\models\Category;
use common\modules\board\models\frontend\PostProductForm;
use common\modules\board\models\Product;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Yii;
use yii\base\InvalidParamException;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class Reader
 * @package console\services
 */
class Reader
{
    /**
     * @param DOMDocument $dom
     * @return array
     */
    protected function readCategories(DOMDocument $dom)
    {
        $output = [];

        $categories = $dom->getElementsByTagName('category');
        if (!empty($categories)) {
            /* @var \DOMNode[] $categories */
            foreach ($categories as $category) {
                $output[] = [
                    'id' => $category->getAttribute('id'),
                    'parentId' => $category->getAttribute('parentId'),
                    'title' => trim($category->nodeValue)
                ];
            }
        }

        return $output;
    }

    /**
     * @param array $items
     * @param int $parentId
     * @return array
     */
    protected function buildTree(array $items = [], $parentId = 0)
    {
        $branch = [];
        foreach ($items as $item) {
            if ($item['parentId'] == $parentId) {
                $children = $this->buildTree($items, $item['id']);
                if ($children) {
                    $item['children'] = $children;
                }
                $branch[] = $item;
            }
        }

        return $branch;
    }

    /**
     * @param DOMDocument $dom
     * @param array $config
     * @return array
     */
    protected function readOffers(DOMDocument $dom, array $config = [])
    {
        $result = [];
        $xpath = new DomXpath($dom);

        Console::output('Start Reading');

        /* @var DOMElement[] $offers */
        $offers = $dom->getElementsByTagName('offer');
        $count = $offers->length;

        Console::output("Offers found: {$count}");
        Console::startProgress(0, $count);

        if (!empty($offers)) {
            foreach ($offers as $key => $offer) {
                Console::updateProgress($key, $count);

                $categoryId = $offer->getElementsByTagName('categoryId')->item(0)->nodeValue;
                if (!array_key_exists($categoryId, $config['categories'])) continue;

                $attachments = [];

                $loadedItemExists = Product::find()->where(['external_id' => $offer->getAttribute('id')])->exists();

                if ($loadedItemExists === false) {
                    $domAttachments = $offer->getElementsByTagName('picture');
                    $attachments = $this->processImages($domAttachments);
                }

                $attributes = $this->processAttributes($offer, $categoryId, $config);
                $attributes = array_merge($attributes, $this->processOptions($offer, $categoryId, $config));
                $attributes = array_merge($attributes, $this->processCategoryToAttribute($xpath, $categoryId, $config));

                $item = [
                    'id' => $offer->getAttribute('id'),
                    'price' => $offer->getElementsByTagName('price')->item(0)->nodeValue,
                    //'currency_code' => $offer->getElementsByTagName('currencyId')->item(0)->nodeValue,
                    'title' => $this->defineItemName($offer, $config),
                    'description' => $offer->getElementsByTagName('description')->item(0)->nodeValue,
                    'attachments' => $attachments,
                    'attributes' => $attributes,
                    'locale' => 'ru-RU'
                ];
                $item = array_merge($item, $this->defineCategories($offer, $config['categories']));

                $result[] = $item;
//
//                break;
            }
        }

        Console::endProgress("Reading done" . PHP_EOL);

        return $result;
    }

    /**
     * @param \DOMNodeList $pictures
     * @return array
     */
    private function processImages(\DOMNodeList $pictures)
    {
        $result = [];

        $year = date('Y');
        $month = date('m');
        $day = date('d');
        Yii::$app->utility->makeDir(Yii::getAlias('@frontend') . "/web/uploads/temp/{$year}/{$month}/{$day}");

        if (!empty($pictures)) {
            foreach ($pictures as $attachment) {
                $image = $attachment->nodeValue;

                $pathInfo = pathinfo($image);
                $content = file_get_contents($image);

                $file = uniqid();

                $path = "/uploads/temp/{$year}/{$month}/{$day}/{$file}.{$pathInfo['extension']}";
                $fp = fopen(Yii::getAlias('@frontend') . "/web{$path}", 'w');
                fwrite($fp, $content);
                fclose($fp);

                $result[] = $path;
            }
        }

        return $result;
    }

    /**
     * @param DOMElement $offer
     * @param int $categoryId
     * @param array $config
     * @return array
     */
    private function processAttributes(DOMElement $offer, int $categoryId, array $config = [])
    {
        $result = [];

        if (!empty($config['attributes'][$categoryId])) {
            foreach ($config['attributes'][$categoryId] as $key => $attribute) {
                $source = trim(htmlspecialchars_decode($offer->getElementsByTagName($key)->item(0)->nodeValue));

                $result["attribute_{$attribute}"] = $source;
            }
        }

        return $result;
    }

    /**
     * @param DOMElement $offer
     * @param int $categoryId
     * @param array $config
     * @return array
     */
    private function processOptions(DOMElement $offer, int $categoryId, array $config = [])
    {
        $result = [];

        if (!empty($config['options'][$categoryId])) {
            $params = $offer->getElementsByTagName('param');
            foreach($params as $param) {
                $paramName = trim(htmlspecialchars_decode($param->getAttribute('name')));
                $value = $param->nodeValue;

                if(array_key_exists($paramName, $config['options'][$categoryId])) {
                    $result["attribute_{$config['options'][$categoryId][$paramName]}"] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * @param DOMXPath $xpath
     * @param int $categoryId
     * @param array $config
     * @return array
     */
    private function processCategoryToAttribute(DOMXPath $xpath, int $categoryId, array $config = [])
    {
        $result = [];

        if (!empty($config['categoryToAttribute'][$categoryId])) {
            $categoryTitle = trim(htmlspecialchars_decode($xpath->query('//category[@id="' . $categoryId . '"][1]')->item(0)->nodeValue));

            $result["attribute_{$config['categoryToAttribute'][$categoryId]}"] = $categoryTitle;
        }

        return $result;
    }

    /**
     * @param DOMElement $offer
     * @param array $availableCategories
     * @return array
     */
    private function defineCategories(DOMElement $offer, array $availableCategories = [])
    {
        $output = [];

        $offerCategory = $offer->getElementsByTagName('categoryId')->item(0)->nodeValue;
        if (array_key_exists($offerCategory, $availableCategories)) {
            switch ($availableCategories[$offerCategory]['lvl']) {
                case 3:
                    $subCategory = Category::find()->where(['id' => $availableCategories[$offerCategory]['id']])->one();
                    $parent = $subCategory->parents(1)->one();
                    $upper = $parent->parents(1)->one();
                    $output['subcategory_id'] = $subCategory->id;
                    $output['category_id'] = $parent->id;
                    $output['parent_category_id'] = $upper->id;

                    break;
                case 2:
                    $parent = Category::find()->where(['id' => $availableCategories[$offerCategory]['id']])->one();
                    $upper = $parent->parents(1)->one();
                    $output['category_id'] = $parent->id;
                    $output['parent_category_id'] = $upper->id;

                    break;
            }
        }

        return $output;
    }

    /**
     * @param DOMElement $offer
     * @param array $config
     * @return null|string
     */
    private function defineItemName(DOMElement $offer, array $config = [])
    {
        $result = null;

        $titleConfig = $config['base']['title'];
        if(is_array($titleConfig)) {
            foreach($titleConfig as $item) {
                $temp[] = $offer->getElementsByTagName($item)->item(0)->nodeValue;
            }

            $result = implode(' ', $temp);
        }

        return $result;
    }
}