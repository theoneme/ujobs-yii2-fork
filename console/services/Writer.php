<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2017
 * Time: 17:57
 */

namespace console\services;

use common\models\Category;
use common\modules\board\models\frontend\PostProductForm;
use common\modules\board\models\Product;
use DOMDocument;
use Yii;
use yii\base\InvalidParamException;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class Writer
 * @package console\services
 */
class Writer
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * @var int|null
     */
    private $params = [];

    /**
     * Writer constructor.
     * @param array $items
     * @param array $params
     */
    public function __construct(array $items = [], array $params = [])
    {
        $this->items = $items;
        $this->params = $params;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!empty($this->items)) {
            Console::output('Start Writing');

            $count = count($this->items);

            Console::output("Offers to be saved: {$count}");
            Console::startProgress(0, $count);

            foreach ($this->items as $key => $item) {
                Console::updateProgress($key, $count);

                $loadedItem = Product::find()->where(['user_id' => $this->params['user_id'], 'external_id' => $item['id']])->one();
                $model = new PostProductForm();
                $input = ["PostProductForm" => array_merge($item, $this->params)];
                if ($loadedItem !== null) {
                    $model->loadProduct($loadedItem->id);
                } else {
                    $model->product = new Product(['user_id' => $this->params['user_id'], 'isBlameable' => false, 'external_id' => $item['id']]);
                }

                $input = $this->formatInput($input, $loadedItem, $item);

                $model->myLoad($input);

                $model->product->status = $this->params['status'];

                if ($model->validate()) {
                    $model->save();
                    $model->product->updateAttributes(['status' => $this->params['status']]);
                } else {
                    print_r($model->validateAll());
//                    die;
                }
            }

            Console::endProgress("Writing done" . PHP_EOL);

            return true;
        }

        return false;
    }

    /**
     * @param array $input
     * @param Product $loadedItem
     * @param array $item
     * @return array
     */
    private function formatInput(array $input = [], $loadedItem, array $item = [])
    {
        if ($loadedItem !== null) {
            if (is_array($loadedItem->attachments)) {
                foreach ($loadedItem->attachments as $attachment) {
                    $input['Attachment'][] = [
                        'content' => $attachment->content
                    ];
                }
            }
        } else {
            if (is_array($item['attachments'])) {
                foreach ($item['attachments'] as $attachment) {
                    $input['Attachment'][] = [
                        'content' => $attachment
                    ];
                }
            }
        }

        if (is_array($item['attributes'])) {
            foreach ($item['attributes'] as $key => $attribute) {
                $input["DynamicForm"][$key] = $attribute;
            }
        }

        return $input;
    }
}