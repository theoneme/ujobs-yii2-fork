<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.10.2017
 * Time: 10:45
 */

namespace console\services;

use common\models\user\Profile;
use common\modules\board\models\Product;
use DOMDocument;

/**
 * Class Akb96Service
 * @package console\services
 */
class Akb96Service extends Reader
{
    /**
     * @var array
     */
    private $config = [
        'categories' => [
            1 => [
                'id' => 2694,
                'lvl' => 3
            ],
            2 => [
                'id' => 2694,
                'lvl' => 3
            ],
            3 => [
                'id' => 2694,
                'lvl' => 3
            ],
            4 => [
                'id' => 2694,
                'lvl' => 3
            ],
        ],
        'base' => [
            'title' => [
                'typePrefix',
                'vendor',
                'model'
            ]
        ],
        'attributes' => [
            1 => [
                'vendor' => 1959,
                'model' => 1958
            ],
            2 => [
                'vendor' => 1959,
                'model' => 1958
            ],
            3 => [
                'vendor' => 1959,
                'model' => 1958
            ],
            4 => [
                'vendor' => 1959,
                'model' => 1958
            ],
        ],
        'options' => [
            1 => [
                'Длина' => 1952,
                'Ширина' => 1953,
                'Высота' => 1954,
                'Емкость' => 1955,
                'Полярность' => 1957,
                'Пусковой ток' => 1956,
            ],
            2 => [
                'Длина' => 1952,
                'Ширина' => 1953,
                'Высота' => 1954,
                'Емкость' => 1955,
                'Полярность' => 1957,
                'Пусковой ток' => 1956,
            ],
            3 => [
                'Длина' => 1952,
                'Ширина' => 1953,
                'Высота' => 1954,
                'Емкость' => 1955,
                'Полярность' => 1957,
                'Пусковой ток' => 1956,
            ],
            4 => [
                'Длина' => 1952,
                'Ширина' => 1953,
                'Высота' => 1954,
                'Емкость' => 1955,
                'Полярность' => 1957,
                'Пусковой ток' => 1956,
            ]
        ],
        'categoryToAttribute' => [
            1 => 1951,
            2 => 1951,
            3 => 1951,
            4 => 1951
        ]
    ];

    /**
     * @var array
     */
    private $params = [
        'user_id' => 23629,
        'address' => 'г. Екатеринбург, ул. Гагарина 27',
        'lat' => '56.846159',
        'long' => '60.644792',
        'status' => Product::STATUS_ACTIVE,
        'type' => Product::TYPE_PRODUCT,
        'currency_code' => 'RUB',
        'locale' => 'ru-RU',
        'secure' => true
    ];

    const URL = 'https://akb96.ru/yml_to_market.php';
    const LOCAL_URL = 'console/import/stores/akb96.xml';

    /**
     * Meb96Service constructor.
     * @param array $config
     * @param array $params
     */
    public function __construct(array $config = [], array $params = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->params = array_merge($this->params, $params);
    }

    /**
     * @return bool
     */
    public function load()
    {
        $dom = new DOMDocument();
        $dom->load(self::LOCAL_URL);

        Product::updateAll(['status' => Product::STATUS_DELETED], ['user_id' => $this->params['user_id']]);
        Profile::updateAll(['is_store' => 1, 'store_name' => 'АвтоАккумуляторы'], ['user_id' => $this->params['user_id']]);

        $items = $this->readOffers($dom, $this->config);

        $writer = new Writer($items, $this->params);
        return $writer->save();
    }

    /**
     * @param bool $physical
     * @return int
     */
    public function remove($physical = false)
    {
        Profile::updateAll(['is_store' => 0], ['user_id' => $this->params['user_id']]);
        if($physical === false) {
            return Product::updateAll(['status' => Product::STATUS_DELETED], ['user_id' => $this->params['user_id']]);
        } else {
            $products = Product::find()->where(['user_id' => $this->params['user_id']])->all();
            $result = true;
            if($products !== null) {
                foreach($products as $product) {
                    $result = $result && $product->delete();
                }
            }
        }

        return $result;
    }
}