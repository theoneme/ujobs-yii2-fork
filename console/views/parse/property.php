<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.08.2018
 * Time: 15:33
 */
use yii\helpers\Html;

/**
 * @var array $property
 * @var array $description
 * @var array $amenities
 */

?>

<p style="font-size: 16px;"><b>Facts:</b></p>

<ul>
    <?= $property['bedroom'] ? ("<li><b>Bedrooms:</b> " . Yii::t('app', '{count, plural, one{# bedroom} other{# bedrooms}}', ['count' => $property['bedroom']]) . "</li>") : null ?>
    <?= $property['yearBuilt'] ? ("<li><b>Year built:</b> {$property['yearBuilt']}</li>") : null ?>
    <?= $property['areaSqFt'] ? ("<li><b>Size:</b> {$property['areaSqFt']} SqFt</li>") : null ?>
    <?= $property['furnished'] ? ("<li><b>Furnished:</b> Yes</li>") : null ?>
    <?= $property['garageSpaces'] ? ("<li><b>Garage spaces:</b> " . Yii::t('app', '{count, plural, one{# space} other{# spaces}}', ['count' => $property['garageSpaces']]) . "</li>") : null ?>
    <?= $property['type'] ? ("<li><b>Property type:</b> {$property['type']}</li>") : null ?>
    <?= $property['balcony'] ? ("<li><b>Balcony:</b> Yes</li>") : null ?>
    <?= $property['petsAllowed'] ? ("<li><b>Pets allowed:</b> Yes</li>") : null ?>
    <?= $property['petRestrictions'] ? ("<li><b>Pet restrictions:</b> {$property['petRestrictions']}</li>") : null ?>
    <?= $property['coolingDescription'] ? ("<li><b>Cooling description:</b> {$property['coolingDescription']}</li>") : null ?>
    <?= $property['heatingDescription'] ? ("<li><b>Heating description:</b> {$property['heatingDescription']}</li>") : null ?>
    <?= $property['waterfrontDescription'] ? ("<li><b>Waterfront description:</b> {$property['waterfrontDescription']}</li>") : null ?>
    <?= $property['fBath'] ? ("<li><b>Bathrooms:</b> " . Yii::t('app', '{count, plural, one{# bathroom} other{# bathrooms}}', ['count' => $property['fBath']]) . "</li>") : null ?>
</ul>

<p style="font-size: 16px;"><b>Description:</b></p>
<?= ($description['remarks'] ? Html::tag('p', $property['remarks']) : '') ?>
<ul>
    <?php if ($description['bedroomDescription']) { ?>
        <li><b>Bedroom description: </b><?= $description['bedroomDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['designDescription']) { ?>
        <li><b>Design description: </b><?= $description['designDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['diningDescription']) { ?>
        <li><b>Dining description: </b><?= $description['diningDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['floorDescription']) { ?>
        <li><b>Floor description: </b><?= $description['floorDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['poolDescription']) { ?>
        <li><b>Pool description: </b><?= $description['poolDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['roofDescription']) { ?>
        <li><b>Roof description: </b><?= $description['roofDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['roomsDescription']) { ?>
        <li><b>Rooms description: </b><?= $description['roomsDescription'] ?></p></li>
    <?php } ?>
    <?php if ($description['garageDescription']) { ?>
        <li><b>Garage description: </b><?= $description['garageDescription'] ?></p></li>
    <?php } ?>
</ul>

<?php if(!empty($amenities)) { ?>
    <p style="font-size: 16px;"><b>Amenities:</b></p>

    <ul>
        <?php foreach($amenities as $amenitie) { ?>
            <li>
                <?= $amenitie['name'] ?>
            </li>
        <?php } ?>
    </ul>
<?php } ?>

