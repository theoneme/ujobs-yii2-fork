<?php
return [
    'id' => 'app-livechat-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
    ],
];
