<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-livechat',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'livechat\controllers',
    'defaultRoute' => '/livechat/admin/index',
    'bootstrap' => ['log', 'livechat'],
    'name' => 'uJobs',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'Account' => 'common\models\user\Account',
                'User' => 'common\models\user\User',
                'Profile' => 'common\models\user\Profile',
            ],
            'controllerMap' => [
                'security' => 'livechat\controllers\user\SecurityController',
            ],
            'mailer' => [
                'sender' => ['support@ujobs.me' => 'Команда uJobs'],
                'viewPath' => '@frontend/views/email'
            ],
            'admins' => ['devour', 'ujobs-support', 'warden'],
            'enableConfirmation' => false,
            'enableRegistration' => false,
            'rememberFor' => 252000,
        ],
	    'livechat' => [
		    'class' => 'common\modules\livechat\Module'
	    ],
    ],
    'components' => [
        'user' => [
            'identityCookie' => [
                'name' => '_livechatUser',
                'path' => '/'
            ],
            'authTimeout' => 60 * 60 * 24 * 10,
        ],
        'request' => [
            'csrfParam' => '_csrf-livechat',
        ],
        'utility' => [
            'class' => 'common\components\Utility',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
            ],
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => [
                'httponly' => true,
                'lifetime' => 3600 * 8,
            ],
            'timeout' => 3600 * 24 * 30,
            'useCookies' => true,
            'name' => 'livechatSession'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'livechat/admin/error',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@livechat/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'account*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'account' => 'account.php',
                    ],
                ],
                'email*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'email' => 'email.php',
                    ],
                ],
                'notifications*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'notifications' => 'notifications.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                //'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>'
            ],
        ],
        'urlManagerFrontEnd' => [
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'baseUrl' => 'https://ujobs.me',
            'rules' => [
                '<alias:[\w-]+>-<action:(tender|job|video)>' => '<action>/view',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'assetsAutoCompress' => [
            'class' => '\skeeks\yii2\assetsAuto\AssetsAutoCompressComponent',
            'enabled' => false,

            'readFileTimeout' => 3,

            'jsCompress' => true,
            'jsCompressFlaggedComments' => true,

            'cssCompress' => true,

            'cssFileCompile' => true,
            'cssFileRemouteCompile' => false,
            'cssFileCompress' => true,
            'cssFileBottom' => false,
            'cssFileBottomLoadOnJs' => false,

            'jsFileCompile' => false,
            'jsFileRemouteCompile' => false,
            'jsFileCompress' => true,
            'jsFileCompressFlaggedComments' => true,

            'noIncludeJsFilesOnPjax' => true,

            'htmlCompress' => true,
            'htmlCompressOptions' => [
                'extra' => false,
                'no-comments' => true
            ],
        ],
    ],
    'params' => $params,
];
