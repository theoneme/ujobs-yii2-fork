<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.05.2018
 * Time: 17:43
 */

namespace common\helpers;

use common\assets\bundles\FontSizeAsset;
use common\assets\bundles\FullscreenAsset;
use Yii;
use yii\web\JsExpression;

/**
 * Class ImperaviHelper
 * @package common\helpers
 */
class ImperaviHelper
{
    /**
     * @return array
     */
    public static function getDefaultConfig()
    {
        $imperaviLocale = Yii::$app->params['imperaviLocales'][Yii::$app->language] ?? 'en';

        return [
            'settings' => [
                'lang' => $imperaviLocale,
                'minHeight' => 200,
                'plugins' => [
                    'fontcolor'
                ],
                'callbacks' => [
                    'start' => new JsExpression(
                        '$.Redactor.opts.langs["' . $imperaviLocale. '"].alignment = "' . Yii::t('app', 'Text Alignment') . '"'
                    )
                ],
                'buttonsHide' => [
                    'html',
                    'formatting'
                ],
                'allowedTags' => ['h2', 'h3', 'h4', 'h5', 'ul', 'ol', 'li', 'blockquote', 'p', 'span', 'strong', 'em'],
                'cleanOnPaste' => true,
//                'toolbarFixedTopOffset' => 62
            ],
            'plugins' => [
                'fontsize' => FontSizeAsset::class,
                'fullscreen' => FullscreenAsset::class,
            ],
        ];
    }
}