<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 30.01.2017
 * Time: 12:46
 */


namespace common\helpers;

use common\models\JobAttribute;
use common\modules\attribute\models\Attribute;
use Yii;

/**
 * Class AttributeWeightsHelper
 * @package common\helpers
 */
class AttributeWeightsHelper
{
    private $batchSize = 50;

    /**
     * @var integer
     */
    private $attribute = null;

    const ATTRIBUTE_TAG = 36;

    public function __construct($attribute = null)
    {
        if($attribute != null) {
            $this->attribute = Attribute::findOne($attribute);
        } else {
            $this->attribute = new Attribute();
        }
    }

    public function process()
    {
        switch($this->attribute->id) {
            case self::ATTRIBUTE_TAG:
                $this->calculateTagWeights();
                break;
            default:
                break;
        }
    }

    public function calculateTagWeights()
    {
        $jobAttributes = JobAttribute::find()->joinWith(['job'])->where(['entity' => 'attribute', 'entity_content' => self::ATTRIBUTE_TAG, 'job.id' => 113]);

        foreach($jobAttributes->batch($this->batchSize) as $jobAttributes) {
            foreach($jobAttributes as $jobAttribute) {
                d($jobAttribute);
            }
        }
    }
}