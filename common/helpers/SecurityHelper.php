<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.05.2017
 * Time: 18:43
 */

namespace common\helpers;

use Yii;

/**
 * Class SecurityHelper
 * @package common\modules\livechat\helpers
 */
class SecurityHelper
{
	/**
	 * @param string $value
	 * @return string
	 */
	public static function encrypt($value)
	{
		return base64_encode(openssl_encrypt($value, 'aes-128-cbc', Yii::$app->params['aes_pass'], true, Yii::$app->params['aes_iv']));
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public static function decrypt($value)
	{
		return openssl_decrypt(base64_decode($value), 'aes-128-cbc', Yii::$app->params['aes_pass'], true, Yii::$app->params['aes_iv']);
	}
}