<?php
namespace common\helpers;

class RobokassaHelper {
  public static $client_id = 'uJobs';
  public static $login = 'admin-uJobs';
  // Test passwords
//  public static $password1 = 'Mfdwa375uiOrhJSg54yV';
//  public static $password2 = 'CpHpx8wq6q5GQQ5E4HEj';
  // Real passwords
  public static $password1 = 'QF3nRuyF71UXoh6cKh0n';
  public static $password2 = 'MTqPhetg03j1PFBg1x3O';

  public static $yandexLabel = 'YandexMerchantQiwiR';
  public static $webmoneyLabel = 'WMR30RM';
  public static $qiwiLabel = 'Qiwi40QiwiRM';
  public static $alfaLabel = 'AlfaBankQiwiR';
  public static $cardLabel = 'QCardR';
  public static $samsungLabel = 'SamsungPayQiwiR';
  public static $mtsLabel = 'MixplatMTSQiwiR';
  public static $beelineLabel = 'MixplatBeelineQiwiR';
  public static $tele2Label = 'MixplatTele2QiwiR';
  public static $euroLabel = 'RapidaQiwiEurosetR';
  public static $svyaznoyLabel = 'RapidaQiwiSvyaznoyR';
}