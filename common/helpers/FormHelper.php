<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.05.2017
 * Time: 15:45
 */

namespace common\helpers;

use Yii;
use yii\helpers\Html;

/**
 * Class SecurityHelper
 * @package common\modules\livechat\helpers
 */
class FormHelper
{
	/**
	 * @return string
	 */
	public static function generateTokenInput()
	{
		$token = base64_encode(Yii::$app->getSecurity()->generateRandomString());

		Yii::$app->session->set('zxToken', $token);
		return $token;
	}
}