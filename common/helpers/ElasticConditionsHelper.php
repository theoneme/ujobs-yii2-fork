<?php

namespace common\helpers;

use common\modules\store\models\Order;

/**
 * Class ElasticConditionsHelper
 * @package common\helpers
 */
class ElasticConditionsHelper
{
    /**
     * @param $fields
     * @param $request
     * @return array
     */
    public static function getSimpleSearchQuery($fields, $request)
    {
        return [
            'query' => [
                'query_string' => [
                    'fields' => $fields,
                    'query' => $request . '~',
                    'fuzzy_prefix_length' => 2,
                    'fuzziness' => 2,
                ]
            ],
        ];
    }

    /**
     * @param $relation
     * @param $fields
     * @param $request
     * @param null $boost
     * @return array
     */
    public static function getNestedSearchQuery($relation, $fields, $request)
    {
        return [
            [
                'nested' => [
                    'path' => $relation,
                    'query' => [
                        'multi_match' => [
                            'query' => $request,
                            'fields' => $fields,
                            'type' => 'best_fields',
                            'minimum_should_match' => '75%',
                            'analyzer' => 'edgeNGram',
                        ]
                    ],
                ]
            ],
            [
                'nested' => [
                    'path' => $relation,
                    'query' => [
                        'multi_match' => [
                            'query' => $request,
                            'fuzziness' => 1,
                            'fields' => $fields,
                            'type' => 'best_fields',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getNoOrdersQuery()
    {
        return [
            'should' => [
                [
                    'nested' => [
                        'path' => 'orders',
                        'query' => [
                            'range' => [
                                'orders.status' => [
                                    'lt' => Order::STATUS_TENDER_ACTIVE,
                                    'gte' => Order::STATUS_TENDER_UNAPPROVED
                                ]
                            ]
                        ],
                    ]
                ],
                [
                    'not' => [
                        'exists' => [
                            'field' => 'orders'
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * @param $categoryIds
     * @return array
     */
    public static function getCategoriesCondition($categoryIds)
    {
        return [
            'nested' => [
                'path' => 'categories',
                'query' => [
                    'bool' => [
                        'filter' => [
                            'terms' => [
                                'categories.id' => $categoryIds,
                            ],
                        ]
                    ]
                ],
            ]
        ];
    }
}