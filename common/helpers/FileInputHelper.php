<?php

namespace common\helpers;

use common\models\Attachment;
use Yii;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class FileInputHelper
 * @package common\helpers
 */
class FileInputHelper
{
    /**
     * @return array
     */
    public static function getDefaultConfig()
    {
        return [
            'name' => 'uploaded_images[]',
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/image/upload']),
                'allowedFileExtensions' => [
                    'jpg', 'gif', 'png', 'jpeg'
                ],
                'encodeUrl' => false,
                'minImageHeight' => 100,
                'minImageWidth' => 100,
                'maxFileCount' => 25,
                'showPreview' => true,
                'dropZoneEnabled' => true,
                'initialPreviewAsData' => true,
                'showCancel' => false,
                'showRemove' => false,
                'showUpload' => false,
                'fileActionSettings' => [
                    'showZoom' => false,
                    'showDrag' => false,
                    'showUpload' => false
                ],
                'previewThumbTags' => [
                    '{actions}' => '{actions}',
                ],
                'uploadExtraData' => new JsExpression("
                    function (previewId, index) {
                        if (previewId !== undefined) {
                            var obj = $('[data-fileid=\"' + previewId + '\"]').data();
                            return obj;
                        }
                    }
                "),
                'otherActionButtons' =>
                    '<button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('app', 'Crop image') . '">
                        <i class="fa fa-crop" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('app', 'Rotate image') . '">
                        <i class="fa fa-rotate-left" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('app', 'Rotate image') . '">
                        <i class="fa fa-rotate-right" aria-hidden="true"></i>
                    </button>',
            ]
        ];
    }

    /**
     * @param array $attachments
     * @return array
     */
    public static function buildAttachmentPreviews(array $attachments)
    {
        $result = [];

        foreach($attachments as $attachment) {
            if($attachment instanceof Attachment) {
                $result[] = $attachment->getThumb();
            }
        }

        return $result;
    }
}