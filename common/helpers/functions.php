<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;

function d($what, $die = true)
{
    echo '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title></title></head><body>';
    VarDumper::dump($what, 15, true);
    if ($die) {
        die();
    }
    echo '</body></html>';
}

function url($url = '', $scheme = false)
{
    return Url::to($url, $scheme);
}

function a($text, $url)
{
    return Html::a($text, $url);
}

function e($text)
{
    return Html::encode($text);
}

function isGuest()
{
    return Yii::$app->user->isGuest;
}

function userId()
{
    return isGuest() ? 0 : Yii::$app->user->id;
}

function hasAccess($userId)
{
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->hasAccess($userId);
}

function hd($what, $die = true)
{
    echo '<div style=display:none;background:#fff;z-index:10000><pre>';
    VarDumper::dump($what, 10, true);
    if ($die) {
        die();
    }
    echo '</pre></div>';
}

function isPagespeed()
{
    $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    return strpos($userAgent, 'Page Speed');
}

function debugMemory()
{
    return memory_get_usage() / 1024 / 1024 . "MB.";
}