<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "job_requirements_steps".
 *
 * @property integer $id
 * @property integer $job_id
 * @property string $title
 *
 * @property JobRequirement[] $jobRequirements
 * @property Job $job
 *
 * @mixin LinkableBehavior
 */
class JobRequirementStep extends ActiveRecord
{
    /**
     * @var array
     */
    public $requirementsData = [];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => LinkableBehavior::class,
                'relations' => ['jobRequirements'],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_requirement_step';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id'], 'integer'],
            [['title'], 'string', 'max' => 125],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::class, 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobRequirements()
    {
        return $this->hasMany(JobRequirement::class, ['step_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::class, ['id' => 'job_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->requirementsData)) {
            foreach ($this->requirementsData as $item) {
                $this->bind('jobRequirements')->attributes = $item;
            }

            $this->requirementsData = [];
        }

        return parent::beforeValidate();
    }
}
