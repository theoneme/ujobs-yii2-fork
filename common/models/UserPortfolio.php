<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.11.2016
 * Time: 14:26
 */

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\BlameableBehavior;
use common\behaviors\LinkableBehavior;
use common\models\elastic\UserPortfolioElastic;
use common\models\user\Profile;
use common\models\user\User;
use common\services\elastic\PortfolioElasticService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "user_portfolio".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $user_id
 * @property string $image
 * @property integer $parent_category_id
 * @property integer $category_id
 * @property integer $subcategory_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $version
 * @property integer $status
 * @property integer $views_count
 * @property integer $likes_count
 *
 * @property Attachment[] $attachments
 * @property User $user
 * @property Profile $profile
 * @property Category $category
 * @property Like[] $likes
 * @property Profile[] $likeUsers
 * @property UserPortfolioAttribute[] $userPortfolioAttributes
 * @property UserPortfolioAttribute[] $tags
 *
 * @method bind($relationName = null, $id = null, $index = null)
 * @method bindAttachment($id = null)
 * @method validateWithRelations()
 */
class UserPortfolio extends ActiveRecord
{
    const VERSION_OLD = 0;
    const VERSION_NEW = 1;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;
    /**
     * @var bool
     */
    public $blameable = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_portfolio';
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasAccess($id)
    {
        return Yii::$app->user->isGuest ? false : UserPortfolio::find()->where(['user_id' => Yii::$app->user->identity->getId(), 'id' => $id])->exists();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['userPortfolioAttributes'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'portfolio'
            ],
        ];
        if ($this->blameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ];
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'user_id'], 'required'],
            [['user_id', 'status', 'views_count', 'likes_count'], 'integer'],
            [['title'], 'string', 'max' => 155],
            [['description'], 'string'],
            ['image', 'string'],
            ['locale', 'string', 'max' => 5],

            ['version', 'default', 'value' => self::VERSION_NEW],
            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION],

            [['user_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['parent_category_id'], 'exist', 'skipOnError' => false, 'targetClass' => Category::class, 'targetAttribute' => ['parent_category_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => false, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => false, 'targetClass' => Category::class, 'targetAttribute' => ['subcategory_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'user_id' => Yii::t('model', 'User ID'),
            'image' => Yii::t('model', 'Image')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'portfolio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        if ($this->subcategory_id !== null) {
            return $this->hasOne(Category::class, ['id' => 'subcategory_id']);
        } else {
            return $this->hasOne(Category::class, ['id' => 'category_id']);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(UserPortfolioAttribute::class, ['user_portfolio_id' => 'id'])->where([
            'user_portfolio_attribute.entity_alias' => 'portfolio_tag'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPortfolioAttributes()
    {
        return $this->hasMany(UserPortfolioAttribute::class, ['user_portfolio_id' => 'id']);
    }

    /**
     * @return int|string
     */
    public function getLikes()
    {
        return $this->hasMany(Like::class, ['entity_id' => 'id'])->andOnCondition(['like.entity' => Like::ENTITY_PORTFOLIO]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeUsers()
    {
        return $this->hasMany(Profile::class, ['user_id' => 'user_id'])->via('likes')->limit(5)->from('profile prf')->indexBy('user_id');
    }

    /**
     * @return int|string
     */
    public function getLikesCount()
    {
        return $this->hasMany(Like::class, ['entity_id' => 'id'])->andWhere(['like.entity' => Like::ENTITY_PORTFOLIO])->count();
    }

    /**
     * @return bool
     */
    public function hasLike()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return Like::find()->where(['like.entity' => Like::ENTITY_PORTFOLIO, 'entity_id' => $this->id, 'user_id' => Yii::$app->user->identity->getId()])->exists();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyLike()
    {
        return $this->hasOne(Like::class, ['entity_id' => 'id'])->andWhere(['like.entity' => Like::ENTITY_PORTFOLIO, 'user_id' => userId()]);
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     * @return bool
     */
    public function sendModerationNotification()
    {
        $subject = Yii::t('notifications', "Your portfolio album has passed moderation on {site}", ['site' => Yii::$app->name], $this->user->site_language);
        $message = Yii::t('notifications', "Your portfolio album has passed moderation and has been added to catalog.", [], $this->user->site_language);
        $route = ['/account/portfolio/view', 'id' => $this->id];

        $notification = new Notification([
            'to_id' => $this->user_id,
            'params' => [
                'subject' => $subject,
                'content' => $message,
            ],
            'template' => Notification::TEMPLATE_PORTFOLIO_MODERATED,
            'custom_data' => json_encode([
                'portfolio' => $this->title,
            ]),
            'linkRoute' => $route,
            'withEmail' => true,
            'is_visible' => true
        ]);

        return $notification->save();
    }

    /**
     * @param bool $doSave
     * @return bool|UserPortfolioElastic
     */
    public function updateElastic($doSave = true)
    {
        $portfolioElasticService = new PortfolioElasticService($this);
        return $portfolioElasticService->process($doSave);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();
        if (empty($this->user_id)) {
            $this->user_id = Yii::$app->user->identity->getId();
        }

        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->title = HtmlPurifier::process($this->title);
            $this->description = HtmlPurifier::process($this->description);

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\base\InvalidConfigException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($this->image) && array_key_exists('image', $changedAttributes)) {
            $oldPath = $changedAttributes['image'];
            if (file_exists(Yii::getAlias('@webroot') . $this->image) && preg_match('/^\/uploads\/temp\/.*/', $this->image)) {
                $year = date('Y');
                $month = date('m');
                $basePath = "/uploads/portfolio/{$year}/{$month}/";
                $dir = Yii::getAlias('@webroot') . $basePath;
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }
                $usernameParts = explode('@', $this->user->username);
                $pathInfo = pathinfo($this->image);
                $extension = $pathInfo['extension'] ? $pathInfo['extension'] : 'jpg';
                $newName = Yii::$app->utility->transliterate(
                        preg_replace('/\s+/', ' ', trim($this->title ? $this->title : $usernameParts[0]))
                    ) . "-" . ($this->user_id) . "-" . uniqid();
                $newPath = $basePath . $newName . "." . $extension;

                rename(Yii::getAlias("@webroot") . $this->image, Yii::getAlias("@webroot") . $newPath);
                Yii::$app->mediaLayer->saveToAws($newPath, true, false);
                $this->updateAttributes(['image' => $newPath]);
                if ($oldPath && $newPath != $oldPath && file_exists(Yii::getAlias('@webroot') . $oldPath)) {
                    unlink(Yii::getAlias('@webroot') . $oldPath);
                }
            } else {
                $this->updateAttributes(['image' => $oldPath]);
            }
        }

        if (array_key_exists('status', $changedAttributes) && $changedAttributes['status'] !== self::STATUS_ACTIVE && (int)$this->status === self::STATUS_ACTIVE) {
            $this->sendModerationNotification();

            if (!Event::find()->where(['code' => Event::PORTFOLIO_CREATED, 'user_id' => $this->user_id])->exists()) {
                (new Event(['code' => Event::PORTFOLIO_CREATED, 'user_id' => $this->user_id]))->save();
                $bonus = new PaymentHistory([
                    'user_id' => $this->user_id,
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_POST_PORTFOLIO_BONUS,
                    'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                    'amount' => 2,
                ]);
                $bonus->save();
            }

            if (!Event::find()->where(['code' => Event::ALBUM_MODERATED, 'user_id' => $this->user_id, 'entity' => 'portfolio', 'entity_content' => $this->id])->exists()) {
                (new Event(['code' => Event::ALBUM_MODERATED, 'user_id' => $this->user_id, 'entity' => 'portfolio', 'entity_content' => $this->id]))->save();
                $bonus = new PaymentHistory([
                    'user_id' => $this->user_id,
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_POST_ALBUM_BONUS,
                    'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                    'amount' => 1,
                ]);
                $bonus->save();
            }

            /** @var UserFriend[] $friends */
            $friends = $this->user->friends;
            if (!empty($friends)) {
                foreach ($friends as $friend) {
                    $notification = new Notification([
                        'to_id' => $friend->target_user_id,
                        'from_id' => $this->user_id,
                        'thumb' => $this->getThumb('catalog'),
                        'custom_data' => json_encode([
                            'user' => $this->user->getSellerName(),
                        ]),
                        'template' => Notification::TEMPLATE_FRIEND_POSTED_PORTFOLIO,
                        'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                        'linkRoute' => ['/account/portfolio/view', 'id' => $this->id],
                        'withEmail' => false,
                        'is_visible' => true
                    ]);

                    $notification->save();
                }
            }
        }
    }

    /**
     * interface implementations
     */

    /**
     * @return string
     */
    public function getLabel()
    {
        return trim(preg_replace('/\s\s+/', ' ', $this->title));
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->image, $target);
    }

    /**
     * @param $userIndex
     * @param null $target
     * @return mixed
     */
    public function getLikeUserThumb($userIndex, $target = null)
    {
        return Yii::$app->mediaLayer->getThumb(
            isset($this->likeUsers[$userIndex]) ? $this->likeUsers[$userIndex]->gravatar_email : null,
            $target
        );
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->user->getSellerUrl();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->user->getSellerName();
    }

    /**
     * @param null $target
     * @return mixed
     */
    public function getSellerThumb($target = null)
    {
        return $this->user->getThumb($target);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->profile->activeTariff) {
            return $this->profile->activeTariff->tariff->icon;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->profile->activeTariff) {
            return $this->profile->activeTariff->tariff->title;
        }

        return null;
    }
}