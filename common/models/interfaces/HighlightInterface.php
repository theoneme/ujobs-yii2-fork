<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.04.2017
 * Time: 15:37
 */

namespace common\models\interfaces;

interface HighlightInterface
{
    /**
     * Returns if item is in favourites list
     * @return boolean
     */
    public function isFavourite();

    /**
     * Returns active tariff icon of item`s owned
     * @return string
     */
    public function getActiveTariffIcon();

    /**
     * Returns active tariff title of item`s owned
     * @return string
     */
    public function getActiveTariffTitle();
}