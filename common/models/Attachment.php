<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "attachment".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $content
 * @property string $description
 */
class Attachment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * @param $content
     * @param $target
     * @return string
     */
    public static function getOuterThumb($content, $target)
    {
        if ($content === null) return $content;

        if (!in_array(pathinfo($content, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'png', 'bmp'])) {
            return '/images/new/attachment-icon.png';
        }
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($content) {
            $image = $content;
        }

        $width = $height = null;
        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'content'], 'required'],
            [['entity_id'], 'integer'],
            [['entity'], 'string', 'max' => 25],
            [['content'], 'string', 'max' => 155],
            ['description', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'content' => Yii::t('model', 'Content'),
            'description' => Yii::t('model', 'Description')
        ];
    }

    /**
     * @param null $target
     * @return string
     */
    public function getThumb($target = null)
    {
        if (!in_array(pathinfo($this->content, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'png', 'bmp'])) {
            return '/images/new/attachment-icon.png';
        }
        if (preg_match('/^\/uploads\/temp\/.*/', $this->content)) {
            return $this->content;
        }
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($this->content && file_exists(Yii::getAlias("@frontend") . "/web" . $this->content)) {
            $image = $this->content;
        }

        $width = $height = null;
        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            Yii::$app->mediaLayer->saveToAws($this->content);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        Yii::$app->mediaLayer->removeMedia($this->content);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->description = HtmlPurifier::process($this->description);

            return true;
        }

        return false;
    }
}
