<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tariff_rule".
 *
 * @property integer $id
 * @property integer $tariff_id
 * @property string $code
 * @property integer $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_enabled
 *
 * @property Tariff $tariff
 */
class TariffRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'value', 'created_at', 'updated_at', 'is_enabled'], 'integer'],
            [['code', 'value'], 'required'],
            [['code'], 'string', 'max' => 75],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariff::class, 'targetAttribute' => ['tariff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'tariff_id' => Yii::t('model', 'Tariff ID'),
            'code' => Yii::t('model', 'Code'),
            'value' => Yii::t('model', 'Value'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'is_enabled' => Yii::t('model', 'Is Enabled'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::class, ['id' => 'tariff_id']);
    }
}
