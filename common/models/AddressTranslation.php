<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "address_translation".
 *
 * @property string $lat
 * @property string $long
 * @property string $locale
 * @property string $title
 */
class AddressTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'long', 'locale'], 'required'],
            [['lat', 'long'], 'string', 'max' => 50],
            [['locale'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 255],
            [['lat', 'long', 'locale'], 'unique', 'targetAttribute' => ['lat', 'long', 'locale']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lat' => Yii::t('model', 'Lat'),
            'long' => Yii::t('model', 'Long'),
            'locale' => Yii::t('model', 'Locale'),
            'title' => Yii::t('model', 'Title'),
        ];
    }
}
