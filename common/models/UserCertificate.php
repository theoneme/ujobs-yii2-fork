<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "user_certificate".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property integer $year
 * @property string $description
 */
class UserCertificate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_certificate';
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasAccess($id)
    {
        return Yii::$app->user->isGuest ? false : UserCertificate::find()->where(['user_id' => Yii::$app->user->identity->getId(), 'id' => $id])->exists();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'description', 'year'], 'required'],
            [['user_id', 'year'], 'integer'],
            [['title'], 'string', 'max' => 75],
            [['image'], 'string', 'max' => 175],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'image' => Yii::t('model', 'Image'),
            'year' => Yii::t('model', 'Year')
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();
        if (empty ($this->user_id)) {
            $this->user_id = Yii::$app->user->identity->getId();
        }
        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->title = HtmlPurifier::process($this->title);
            $this->description = HtmlPurifier::process($this->description);
            $this->year = HtmlPurifier::process($this->year);

            return true;
        }
        return false;
    }
}
