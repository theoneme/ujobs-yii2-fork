<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.11.2017
 * Time: 15:06
 */

namespace common\models\forms;

use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use Yii;

class UserSpecialtyForm extends BaseAttributeForm
{
    /**
     * @var string
     */
    public $salary;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['salary', 'required'],
            ['salary', 'integer']
//            ['level', 'in', 'range' => [
//
//            ]],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'salary' => Yii::t('model', 'Salary'),
        ]);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
//        $this->salary = intval($this->salary);
        return parent::beforeValidate();
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $specialtyAttribute = Attribute::findOne(['alias' => 'specialty']);
        if($this->id > 0) {
            $object = UserAttribute::findOne(['id' => $this->id, 'user_id' => Yii::$app->user->identity->getId()]);
            if($object === null) return false;
        } else {
            $object = AttributeValueObjectBuilder::makeAttributeObject(UserAttribute::class, $specialtyAttribute->id, $this->title, ['locale' => Yii::$app->language]);
            $object->user_id = $this->user_id ?? Yii::$app->user->identity->getId();
        }
        $object->custom_data = json_encode(['salary' => $this->salary]);

        if ($object->save()) {
            $this->id = $object->id;
            return true;
        }

        return false;
    }
}