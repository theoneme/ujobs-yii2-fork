<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.11.2017
 * Time: 15:06
 */

namespace common\models\forms;

use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use Yii;

class UserLanguageForm extends BaseAttributeForm
{
    const MAX_LANGUAGES_AVAILABLE = 10;

    /**
     * @var string
     */
    public $level;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['level', 'required'],
//            ['level', 'in', 'range' => [
//
//            ]],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'level' => Yii::t('model', 'Level'),
        ]);
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $languageAttribute = Attribute::findOne(['alias' => 'language']);
        if($languageAttribute !== null) {
            if($this->id > 0) {
                $object = UserAttribute::findOne(['id' => $this->id, 'user_id' => Yii::$app->user->identity->getId()]);
                if($object === null) return false;
            } else {
                $object = AttributeValueObjectBuilder::makeAttributeObject(UserAttribute::class, $languageAttribute->id, $this->title, ['locale' => Yii::$app->language]);
                $object->user_id = $this->user_id ?? Yii::$app->user->identity->getId();
            }
            $object->custom_data = json_encode(['level' => $this->level]);

            if ($object->save()) {
                $this->id = $object->id;
                return true;
            }
        }

        return false;
    }
}