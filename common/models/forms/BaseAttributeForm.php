<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.11.2017
 * Time: 17:16
 */


namespace common\models\forms;

use common\models\UserAttribute;
use Yii;
use yii\base\Model;

class BaseAttributeForm extends Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var integer
     */
    public $user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Title')
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        return false;
    }

    /**
     * @return bool|false|int
     */
    public function delete()
    {
        if ($this->id !== null) {
            $object = UserAttribute::findOne($this->id);
            if ($object !== null) {
                return $object->delete();
            }
        }

        return false;
    }

    /**
     * @param $condition
     * @return BaseAttributeForm
     */
    public static function findOne($condition)
    {
        $attributeObject = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescriptions'])->where($condition)->one();
        $descriptions = $attributeObject->attrValueDescriptions ?? [];
        $title = array_key_exists(Yii::$app->language,  $descriptions) ? $descriptions[Yii::$app->language]->title : array_pop($descriptions)->title;
        $object = new static(['title' => $title, 'id' => $attributeObject->id]);
        $object->attributes = $attributeObject->customDataArray;
        return $object;
    }

    /**
     * @param $condition
     * @return BaseAttributeForm[]
     */
    public static function findAll($condition)
    {
        $attributeObjects = UserAttribute::find()->joinWith(['attrValue', 'attrValueDescriptions'])->where($condition)->all();
        return array_map(function ($attributeObject) {
            $descriptions = $attributeObject->attrValueDescriptions;
            $title = array_key_exists(Yii::$app->language,  $descriptions) ? $descriptions[Yii::$app->language]->title : array_pop($descriptions)->title;
            $object = new static(['title' => $title, 'id' => $attributeObject->id]);
            $object->attributes = $attributeObject->customDataArray;
            return $object;
        }, $attributeObjects);
    }
}