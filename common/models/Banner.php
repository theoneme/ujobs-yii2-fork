<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $image
 * @property string $url
 * @property integer $type
 * @property string $locale
 */
class Banner extends ActiveRecord
{
    const TYPE_DEFAULT = 0;
    public $uploadedImage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['image', 'url'], 'string', 'max' => 255],

            ['type', 'default', 'value' => self::TYPE_DEFAULT],
            ['locale', 'string'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'image' => Yii::t('model', 'Image'),
            'url' => Yii::t('model', 'Url'),
            'type' => Yii::t('model', 'Type'),
            'locale' => Yii::t('model', 'Locale'),
        ];
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->image, $target);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!$insert) {
            $oldImage = $this->getOldAttribute('image');
        }
        $path = '/uploads/qwgdhvsa/' . date('Y') . '/' . date('m') . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $this->uploadedImage = UploadedFile::getInstance($this, 'uploadedImage');
        if ($this->uploadedImage) {
            $mediaLayer = Yii::$app->get('mediaLayer');
            $name = $path . $this->uploadedImage->baseName . '-' . hash('crc32b', $this->uploadedImage->name . time()) . "." . $this->uploadedImage->extension;
            if ($this->uploadedImage->saveAs(Yii::getAlias('@webroot') . $name)) {
                if (!empty($oldImage)) {
                    $mediaLayer->removeMedia($oldImage);
                }
                $this->image = $name;
                $mediaLayer->saveToAws($this->image);
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (!empty($this->image)) {
            $mediaLayer = Yii::$app->get('mediaLayer');
            $mediaLayer->removeMedia($this->image);
        }
        return parent::beforeDelete();
    }
}
