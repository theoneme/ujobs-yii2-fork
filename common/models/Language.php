<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $locale
 */
class Language extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'locale'], 'required'],
            [['title'], 'string', 'max' => 55],
            [['description'], 'string', 'max' => 155],
            [['locale'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'locale' => Yii::t('model', 'Locale'),
        ];
    }
}
