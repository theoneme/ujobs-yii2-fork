<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $key
 * @property string $description
 * @property string $value
 * @property bool $is_visible
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value', 'is_visible'], 'required'],
            [['key'], 'string', 'max' => 55],
            [['description', 'value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'key' => Yii::t('model', 'Key'),
            'description' => Yii::t('model', 'Description'),
            'value' => Yii::t('model', 'Value'),
            'is_visible' => Yii::t('model', 'Is visible')
        ];
    }
}
