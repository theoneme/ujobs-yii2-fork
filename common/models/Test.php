<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property int $group_id
 * @property string $alias
 * @property string $title
 * @property int $status
 */
class Test extends \yii\db\ActiveRecord
{
    const STATUS_SUCCESS = 10;
    const STATUS_FAIL = 20;

    const GROUP_UNIT = 10;
    const GROUP_FUNCTIONAL = 20;
    const GROUP_ACCEPTANCE = 30;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'status'], 'integer'],
            [['group_id', 'title', 'alias', 'status'], 'required'],
            [['alias'], 'string', 'max' => 75],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'group_id' => Yii::t('model', 'Group ID'),
            'alias' => Yii::t('model', 'Alias'),
            'title' => Yii::t('model', 'Title'),
            'status' => Yii::t('model', 'Status'),
        ];
    }
}
