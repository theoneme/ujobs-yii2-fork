<?php

namespace common\models;

use common\behaviors\BlameableBehavior;
use common\models\user\User;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use common\modules\store\models\PaymentMethod;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $payment_method_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $total
 * @property string $code
 * @property string $currency_code
 * @property string $description
 * @property integer $status
 * @property integer $type
 * @property integer $old_order_id
 *
 * @property PaymentMethod $paymentMethod
 * @property Order[] $orders
 * @property Order $order
 * @property UserTariff[] $tariffs
 * @property UserTariff $tariff
 * @property User $user
 */
class Payment extends ActiveRecord
{
    const STATUS_PENDING = 10;
    const STATUS_REFUSED = 20;
    const STATUS_PAID = 30;

    const TYPE_JOB_PAYMENT = 1;
    const TYPE_ACCESS_PAYMENT = 2;
    const TYPE_TENDER_PAYMENT = 3;
    const TYPE_ACCOUNT_REPLENISHMENT = 4;
    const TYPE_PRODUCT_PAYMENT = 5;
    const TYPE_PRODUCT_TENDER_PAYMENT = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'total', 'payment_method_id'], 'required'],
            [['user_id', 'created_at', 'updated_at', 'total', 'status', 'payment_method_id', 'old_order_id'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['code', 'description'], 'string', 'max' => 155],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => [self::STATUS_PENDING, self::STATUS_REFUSED, self::STATUS_PAID]],
            ['type', 'default', 'value' => self::TYPE_JOB_PAYMENT],

            [['payment_method_id'], 'exist', 'skipOnError' => false, 'targetClass' => PaymentMethod::class, 'targetAttribute' => ['payment_method_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'total' => Yii::t('model', 'Total'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'description' => Yii::t('model', 'Description'),
        ];
    }

    /**
     * @param bool|false $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_PAID => ($colored ? Html::tag('div', Yii::t('labels', 'Paid'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Paid')),
            self::STATUS_PENDING => ($colored ? Html::tag('div', Yii::t('labels', 'Pending'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Pending')),
            self::STATUS_REFUSED => ($colored ? Html::tag('div', Yii::t('labels', 'Refused'), ['class' => 'btn btn-danger btn-xs']) : Yii::t('labels', 'Refused')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::class, ['id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(UserTariff::class, ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(UserTariff::class, ['payment_id' => 'id']);
    }

    /**
     * Mark payment as complete and ordered items as paid
     */
    public function markComplete()
    {
        $this->updateAttributes(['status' => Payment::STATUS_PAID]);
        switch ($this->type) {
            case Payment::TYPE_JOB_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_MISSING_DETAILS);
                break;
            case Payment::TYPE_ACCESS_PAYMENT:
                $this->markRelatedTariffs(UserTariff::STATUS_ACTIVE);
                break;
            case Payment::TYPE_TENDER_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_TENDER_PAID);
                break;
            case Payment::TYPE_PRODUCT_TENDER_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_PRODUCT_TENDER_PAID);
                break;
            case Payment::TYPE_ACCOUNT_REPLENISHMENT:
                $this->replenishAccount();
                break;
            case Payment::TYPE_PRODUCT_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION);
                break;
        }
    }

    /**
     * Mark payment as refused and ordered items as canceled
     */
    public function markRefused()
    {
        $this->updateAttributes(['status' => Payment::STATUS_REFUSED]);
        switch ($this->type) {
            case Payment::TYPE_JOB_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_CANCELLED);
                break;
            case Payment::TYPE_ACCESS_PAYMENT:
                $this->markRelatedTariffs(UserTariff::STATUS_REQUIRES_PAYMENT);
                break;
            case Payment::TYPE_TENDER_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_TENDER_APPROVED);
                break;
            case Payment::TYPE_PRODUCT_TENDER_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_PRODUCT_TENDER_APPROVED);
                break;
            case Payment::TYPE_PRODUCT_PAYMENT:
                $this->markRelatedOrders(Order::STATUS_CANCELLED);
                break;
        }
    }

    /**
     * Get return url based on payment type
     */
    public function getReturnUrl()
    {
        $returnUrl = null;
        switch ($this->type) {
            case Payment::TYPE_JOB_PAYMENT:
                $returnUrl = Url::to(['/account/order/history', 'id' => $this->order->id]);
                break;
            case Payment::TYPE_ACCESS_PAYMENT:
                $returnUrl = Url::to(['/account/profile/show', 'id' => $this->user_id]);
                break;
            case Payment::TYPE_TENDER_PAYMENT:
                $returnUrl = Url::to(['/account/order/tender-history', 'id' => $this->order->id]);
                break;
            case Payment::TYPE_PRODUCT_TENDER_PAYMENT:
                $returnUrl = Url::to(['/account/order/product-tender-history', 'id' => $this->order->id]);
                break;
            case Payment::TYPE_ACCOUNT_REPLENISHMENT:
                $returnUrl = Url::to(['/account/payment/index']);
                break;
            case Payment::TYPE_PRODUCT_PAYMENT:
                $returnUrl = Url::to(['/account/order/product-history', 'id' => $this->order->id]);
                break;
        }
        return $returnUrl;
    }

    /**
     * @param int $status
     */
    public function markRelatedOrders($status = null)
    {
        if ($status !== null) {
            $orders = $this->orders;

            if ($orders !== null) {
                /* @var Order $order */
                foreach ($orders as $order) {
                    $order->status = $status;

                    $order->save();
                }
            }
        }
    }

    /**
     * @param int $status
     */
    public function markRelatedTariffs($status = null)
    {
        if ($status !== null) {
            if ($this->tariff !== null) {
                /* @var UserTariff $tariff */
                $this->tariff->status = $status;
                $this->tariff->save();
            }
        }
    }

    /**
     *
     */
    public function transformCartItemsToOrders()
    {
        $cart = Yii::$app->cart;

        $items = $cart->getItems();
        foreach ($items as $cartItem) {
            $order = new Order();
            $total = $cartItem['item']['price'] * $cartItem['quantity'];

            if (!empty($options = $cartItem['options'])) {
                foreach ($options as $option) {
                    $total += $option['price'] * $option['quantity'];
                }
            }

            $order->total = $total;
            $order->payment_method_id = $this->payment_method_id;
            $order->cartItems[] = $cartItem;
            $order->seller_id = $cartItem['item']->getSellerId();
            switch (true) {
                case $cartItem['item'] instanceof Job:
                    if ($subtype = $cartItem['item']->subtype) {
                        $order->product_subtype = $subtype;
                    }
                    $order->product_type = Order::TYPE_JOB;
                    break;

                case $cartItem['item'] instanceof JobPackage:
                    if ($subtype = $cartItem['item']->job->subtype) {
                        $order->product_subtype = $subtype;
                    }
                    $order->product_type = Order::TYPE_JOB_PACKAGE;
                    break;

                case $cartItem['item'] instanceof Offer:
                    $order->product_type = Order::TYPE_OFFER;
                    break;

                case $cartItem['item'] instanceof Product:
                    $order->product_type = Order::TYPE_PRODUCT;
                    break;
            }
            $order->payment_id = $this->id;
            $order->save();
        }
    }

    /**
     * @param Tariff|null $tariff
     * @param int $quantity
     * @return bool
     */
    public function transformTariff($tariff = null, $quantity = 0)
    {
        if ($tariff !== null && $quantity > 0) {
            $userTariff = new UserTariff();
            $userTariff->cost = $tariff->cost * $quantity;
            $userTariff->duration = $quantity * 60 * 60 * 24;
            $userTariff->expires_at = time() + $userTariff->duration;
            $userTariff->payment_id = $this->id;
            $userTariff->tariff_id = $tariff->id;

            return $userTariff->save();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function replenishAccount()
    {
        $paymentHistory = new PaymentHistory();
        $paymentHistory->amount = $this->total;
        $paymentHistory->currency_code = $this->currency_code;
        $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_POS;
        $paymentHistory->template = PaymentHistory::TEMPLATE_PAYMENT_ACCOUNT_REPLENISH;
        $paymentHistory->type = PaymentHistory::TYPE_OPERATION_REPLENISH;
        $paymentHistory->user_id = $this->user_id;
        return $paymentHistory->save();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert && empty($this->currency_code)) {
            $this->currency_code = Yii::$app->params['app_currency_code'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $orders = Order::find()->where(['payment_id' => $this->id])->all();
            if ($orders) {
                foreach ($orders as $order) {
                    $order->delete();
                }
            }
            return true;
        } else {
            return false;
        }
    }
}