<?php

namespace common\models;

use common\modules\attribute\models\AttributeValue;
use common\traits\FindOrCreateTrait;
use Yii;

/**
 * This is the model class for table "withdrawal_type_country".
 *
 * @property int $id
 * @property int $type
 * @property int $country_id
 *
 * @property AttributeValue $country
 */
class WithdrawalTypeCountry extends \yii\db\ActiveRecord
{
    use FindOrCreateTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'withdrawal_type_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'country_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'type' => Yii::t('model', 'Type'),
            'country_id' => Yii::t('model', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'country_id']);
    }
}
