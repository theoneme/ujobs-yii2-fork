<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 24.11.2016
 * Time: 16:14
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_deactivation".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $reason_id
 * @property string $reason_additional
 */
class UserDeactivation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_deactivation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'reason_id', 'reason_additional'], 'required'],
            [['user_id', 'reason_id'], 'integer'],
            [['reason_additional'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'reason_id' => Yii::t('model', 'Reason ID'),
            'reason_additional' => Yii::t('model', 'Reason Additional'),
        ];
    }
}