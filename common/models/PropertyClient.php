<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use common\models\user\User;
use common\traits\FindOrCreateTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "property_client".
 *
 * @property int $id
 * @property int $created_by
 * @property int $user_id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $info
 * @property int $mortgage
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $createdBy
 * @property User $user
 * @property PropertyClientHouse[] $houses
 *
 * @mixin LinkableBehavior
 */
class PropertyClient extends ActiveRecord
{
    use FindOrCreateTrait;

    const STATUS_SENT = 0;
    const STATUS_REGISTERED = 10;

    /**
     * @var array
     */
    public $housesData = [];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['houses'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['email', 'first_name', 'last_name', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            ['info', 'string'],
            [['mortgage'], 'string', 'max' => 1],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_SENT],
            ['status', 'in', 'range' => [self::STATUS_SENT, self::STATUS_REGISTERED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'created_by' => Yii::t('model', 'Created By'),
            'user_id' => Yii::t('model', 'User ID'),
            'email' => Yii::t('model', 'Email'),
            'first_name' => Yii::t('model', 'First Name'),
            'last_name' => Yii::t('model', 'Last Name'),
            'phone' => Yii::t('model', 'Phone'),
            'mortgage' => Yii::t('model', 'Mortgage'),
            'status' => Yii::t('model', 'Status'),
            'houses' => Yii::t('model', 'Houses'),
            'info' => Yii::t('model', 'Additional information'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouses()
    {
        return $this->hasMany(PropertyClientHouse::class, ['client_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_SENT => Yii::t('model', 'Invitation sent'),
            self::STATUS_REGISTERED => Yii::t('model', 'Registered'),
        ];
    }

    /**
     * @return string
     */
    public function getClientName()
    {
        if($this->user !== null) {
            return $this->user->getSellerName();
        }

        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->housesData)) {
            foreach ($this->housesData as $item) {
                $this->bind('houses')->attributes = $item;
            }
            $this->housesData = [];
        }
        return parent::beforeValidate();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            /* @var $user User*/
            $user = Yii::$app->user->identity;
            $emailLog = new EmailLog([
                'email' => $this->email,
                'customDataArray' => [
                    'mailer' => 'mailer',
                    'viewPath' => '@frontend/views/email',
                    'view' => 'realtor',
                    'subject' => Yii::t('notifications', 'Invitation to {site}', ['site' => Yii::$app->name]),
                    'receiverEmail' => $this->email,
                    'user' => $user->getCurrentId(),
                    'language' => $user->site_language,
                    'client_id' => $this->id,
                ]
            ]);
            $emailLog->save();

            $mailer = Yii::$app->mailer;
            $mailer->viewPath = '@frontend/views/email';
            $mailer->compose(['html' => 'realtor'], ['user' => $user->company_user_id ? $user->companyUser : $user, 'language' => $user->site_language, 'client_id' => $this->id])
                ->setTo([$this->email])
                ->setFrom(Yii::$app->params['ujobsNoty'])
                ->setSubject(Yii::t('notifications', 'Invitation to {site}', ['site' => Yii::$app->name]))
                ->send();
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
