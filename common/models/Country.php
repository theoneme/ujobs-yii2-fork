<?php

namespace common\models;

use common\modules\store\models\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property string $currency_code
 *
 * @property Currency $currencyCode
 */
class Country extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['code', 'currency_code'], 'string', 'max' => 3],
            [['code'], 'unique'],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'code' => Yii::t('model', 'Code'),
            'currency_code' => Yii::t('model', 'Currency Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyCode()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }
}
