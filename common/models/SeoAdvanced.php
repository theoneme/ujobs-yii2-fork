<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.02.2017
 * Time: 14:04
 */

namespace common\models;

use common\components\CurrencyHelper;
use common\traits\FindOrCreateTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "seo_advanced".
 *
 * @property integer $id
 * @property string $entity
 * @property string $entity_content
 * @property string $type
 * @property string $locale
 * @property string $content
 * @property string $template_category
 * @property string $custom_data
 *
 * @method static SeoAdvanced findOrCreate($condition, $save = true)
 */
class SeoAdvanced extends ActiveRecord
{
    use FindOrCreateTrait;

    const TEMPLATE_PRO_ROOT_CATEGORY = 0;
    const TEMPLATE_PRO_CATEGORY = 1;
    const TEMPLATE_PRO_SUBCATEGORY = 2;
    const TEMPLATE_PRO_OTHER_SUBCATEGORY = 3;
    const TEMPLATE_PRO = 4;

    const TEMPLATE_TENDER_ROOT_CATEGORY = 10;
    const TEMPLATE_TENDER_CATEGORY = 11;
    const TEMPLATE_TENDER_SUBCATEGORY = 12;
    const TEMPLATE_TENDER = 13;

    const TEMPLATE_JOB_ROOT_CATEGORY = 20;
    const TEMPLATE_JOB_CATEGORY = 21;
    const TEMPLATE_JOB_SUBCATEGORY = 22;
    const TEMPLATE_JOB_OTHER_SUBCATEGORY = 23;
    const TEMPLATE_JOB = 24;

    const TEMPLATE_VIDEO_ROOT_CATEGORY = 30;
    const TEMPLATE_VIDEO_CATEGORY = 31;
    const TEMPLATE_VIDEO = 32;

    const TEMPLATE_SKILL = 40;
    const TEMPLATE_TAG = 50;
    const TEMPLATE_SPECIALTY = 60;
    const TEMPLATE_SPECIALTY_PRO = 70;

    const TEMPLATE_PRODUCT_ROOT_CATEGORY = 80;
    const TEMPLATE_PRODUCT_CATEGORY = 81;
    const TEMPLATE_PRODUCT_SUBCATEGORY = 82;
    const TEMPLATE_PRODUCT_TAG = 83;
    const TEMPLATE_PRODUCT = 84;
    const TEMPLATE_PRODUCT_TENDER = 85;

    const TEMPLATE_PORTFOLIO_ROOT_CATEGORY = 90;
    const TEMPLATE_PORTFOLIO_CATEGORY = 91;
    const TEMPLATE_PORTFOLIO_SUBCATEGORY = 92;
    const TEMPLATE_PORTFOLIO_TAG = 93;

    const TEMPLATE_STORE_ROOT_CATEGORY = 100;
    const TEMPLATE_STORE_CATEGORY = 110;
    const TEMPLATE_STORE_SUBCATEGORY = 115;

    const TEMPLATE_PAGE_ROOT_CATEGORY = 120;
    const TEMPLATE_PAGE_CATEGORY = 121;
    const TEMPLATE_PAGE_TAG = 123;

    const TEMPLATE_COMPANY = 130;

    /**
     * @var string
     */
    public $keyword = null;

    /**
     * @var array
     */
    public $templates = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_advanced';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entity_content'], 'required'],
            [['locale'], 'string', 'max' => 5],
            [['content', 'custom_data'], 'string'],
            [['type'], 'string', 'max' => 255],
            [['entity', 'entity_content'], 'safe'],

            ['locale', 'default', 'value' => 'ru-RU']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_content' => Yii::t('model', 'Entity Content'),
            'type' => Yii::t('model', 'Type'),
        ];
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyTemplates($params = [])
    {
        $params['keyword'] = $params['title'] ?? $params['category'] ?? '';
        foreach (['price', 'minPrice', 'maxPrice'] as $priceParam) {
            if (isset($params[$priceParam])) {
                $params[$priceParam] = CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $params[$priceParam]);
            }
        }
        $defaultTemplates = self::getDefaultTemplates($this->template_category);
        $templates = [
            'heading' => $defaultTemplates['heading'],
            'seo_title' => $defaultTemplates['title'],
            'seo_description' => $defaultTemplates['description'],
            'seo_keywords' => $defaultTemplates['keywords'],
            'subheading' => $defaultTemplates['subheading'],
            'custom_text_1' => $defaultTemplates['upperBlockHeading'],
            'custom_text_2' => $defaultTemplates['upperBlockSubheading'],
            'custom_text_3' => $defaultTemplates['upperBlockColumn1'],
            'custom_text_4' => $defaultTemplates['upperBlockColumn2'],
            'custom_text_5' => $defaultTemplates['upperBlockColumn3'],
            'custom_text_6' => $defaultTemplates['textHeading'],
            'custom_text_7' => $defaultTemplates['textSubheading'],
            'content' => ''
        ];
//        $templates = array_filter($templates, function ($var) {
//            return !empty($var);
//        });
        foreach ($templates as $k => $v) {
            $templates[$k] = Yii::t('seo', $v, $params);
            $templates[$k] = preg_replace('/{([^{}]+?({[^{}]*?})?)*}/', '', $templates[$k]);
            $templates[$k] = preg_replace("/\s{2,}/", ' ', $templates[$k]);
        }
        $viewDir = Yii::getAlias('@frontend') . '/views/category/seo/';
        $contentTemplate = !empty($this->content) ? $this->content : $defaultTemplates['textView'];
        if ($contentTemplate && file_exists($viewDir . $contentTemplate . '.php')) {
            $templates['content'] = preg_replace('/\s\s+/', ' ', Yii::$app->controller->renderPartial('@frontend/views/category/seo/' . $contentTemplate, ['params' => $params]));
        }

        $this->templates = $templates;
        return $this->templates;
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public static function getDefaultTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $subheading = '';
        $upperBlockHeading = '';
        $upperBlockSubheading = '';
        $upperBlockColumn1 = '';
        $upperBlockColumn2 = '';
        $upperBlockColumn3 = '';
        $textHeading = '';
        $textSubheading = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_PRO_ROOT_CATEGORY:
                $heading = 'Catalog of professionals{request}{tattributes}';
                $title = "{count, plural, one{# professional} other{# professionals}} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me";
                $description = "{count, plural, one{# professional} other{# professionals}} from the various spheres awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!";
                $keywords = "freelancers services, order freelancer services, freelancers exchange, cheap freelancers{kequest}{kattributes}";
                $subheading = "Choose a specialist and assign him for a job";
                $upperBlockHeading = "Are you looking for job to order?";
                $upperBlockSubheading = "{count, plural, one{# professional} other{# professionals}}{city} awaiting your tasks.";
                $upperBlockColumn1 = "On our website you can choose a specialist {city} according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                break;
            case self::TEMPLATE_PRO_CATEGORY:
                $heading = "{category} specialists{city}{request}{tattributes}";
                $title = "{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me";
                $description = "{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!";
                $keywords = "{category}{city}, order {category}{city}, {category}{city} cheap, {category} freelancer{city}{kequest}{kattributes}";
                $subheading = "Choose from {count, plural, one{# professional} other{# professionals}} in {category}{city}";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_PRO_SUBCATEGORY:
                $heading = "{category} specialists{city}{request}{tattributes}";
                $title = "{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me";
                $description = "{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!";
                $keywords = "{category}{city}, order {category}{city}, {category}{city} cheap, {category} freelancer{city}{kequest}{kattributes}";
                $upperBlockHeading = "Are you looking for {category} to order?";
                $upperBlockSubheading = "{count, plural, one{# professional} other{# professionals}} in «{category}»{city} awaiting your tasks.";
                $upperBlockColumn1 = "On our website you can choose a specialist in «{category}»{city} according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_PRO_OTHER_SUBCATEGORY:
                $heading = "{category} specialists{city}{request}{tattributes}";
                $title = "{count, plural, one{# professional} other{# professionals}} of other services in «{parentCategory}»{city} awaiting your tasks{Request}{Attributes} | Site of professionals uJobs.me";
                $description = "{count, plural, one{# professional} other{# professionals}} of other services in «{parentCategory}»{city} awaiting your tasks{request}{tattributes}. Order now on the site of professionals uJobs.me!";
                $keywords = "other services in {parentCategory}{city}{kequest}{kattributes}";
                $subheading = "Order other services in {parentCategory}{city} from {count, plural, one{# freelancer} other{# freelancers}}";
                $upperBlockHeading = "Are you looking for other services in {parentCategory}{city}?";
                $upperBlockSubheading = "{count, plural, one{# professional} other{# professionals}}{city} awaiting your response.";
                $upperBlockColumn1 = "On our website you can choose a specialist according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_PRO:
                $title = "Profile page of {title} on site uJobs.me";
                $description = "I invite you to check services and products i offer on site uJobs.me";
//                $keywords = '{skills}';
                break;

            case self::TEMPLATE_TENDER_ROOT_CATEGORY:
                $heading = 'Request catalog{request}{tattributes}';
//                $title = "{count, plural, one{# request} other{# requests}} | Send an application, complete the assignment and earn money | Site of professionals uJobs.me";
                $title = "{count} Requests{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count, plural, one{# request} other{# requests}} and earn on the site of professionals uJobs.me{request}{tattributes}. {countPro, plural, one{# customer} other{# customers}} waiting for your responses!";
                $keywords = "distant work, part-time at home, freelance, freelance market, jobs market{kequest}{kattributes}";
                $subheading = "Choose a task and earn money";
                $upperBlockHeading = "Are you looking for a job{city}?";
                $upperBlockSubheading = "{countPro, plural, one{# customer} other{# customer}} ready to give you a task";
                $upperBlockColumn1 = "You can find a job on our site according to your skills and abilities";
                $upperBlockColumn2 = "Respond to request and get task.";
                $upperBlockColumn3 = "Complete the job and when the customer approves it receive a payment.";
                break;
            case self::TEMPLATE_TENDER_CATEGORY:
                $heading = "Choose orders from {category} category{city}{request}{tattributes}";
//                $title = "Work in {category}{city} | {count, plural, one{# request} other{# requests}} in category «{category}»{city} | Site of professionals uJobs.me";
                $title = "{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count, plural, one{# request} other{# requests}} in category «{category}»{city}{request}{tattributes}. Earn from {minPrice} on the site of professionals uJobs.me.";
                $keywords = "work {category}{city}, vacancy {category}{city}, freelance {category}{city}{kequest}{kattributes}";
                $upperBlockHeading = "Are you looking for a job in {category}{city}?";
                $upperBlockSubheading = "{countPro, plural, one{# customer} other{# customer}} ready to give you a task in {category}";
                $upperBlockColumn1 = "You can find a job in category “{category}” on our site according to your skills and abilities";
                $upperBlockColumn2 = "Respond to request and get task in “{category}”.";
                $upperBlockColumn3 = "Complete the job and when the customer approves it receive a payment.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_TENDER_SUBCATEGORY:
                $heading = "Choose orders from {category} category{city}{request}{tattributes}";
//                $title = "Work in {category}{city} | {count, plural, one{# request} other{# requests}} in category «{category}»{city} | Site of professionals uJobs.me";
                $title = "{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count, plural, one{# request} other{# requests}} in category «{category}»{city}{request}{tattributes}. Earn from {minPrice} on the site of professionals uJobs.me.";
                $keywords = "work {category}{city}, vacancy {category}{city}, freelance {category}{city}{kequest}{kattributes}";
                $subheading = "{count, plural, one{# request} other{# requests}} in «{category}»{city}";
                $upperBlockHeading = "Are you looking for a job in {category}{city}?";
                $upperBlockSubheading = "{countPro, plural, one{# customer} other{# customer}} ready to give you a task in {category}";
                $upperBlockColumn1 = "You can find a job in category “{category}” on our site according to your skills and abilities";
                $upperBlockColumn2 = "Respond to request and get task in “{category}”.";
                $upperBlockColumn3 = "Complete the job and when the customer approves it receive a payment.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_TENDER:
                $title = "{Title} (Price {Price})";
                $description = "«{title}» service order {price} on the site of professionals uJobs.me. Do the task and earn money!";
                $keywords = "{title}, order {title}";
                break;

            case self::TEMPLATE_JOB_ROOT_CATEGORY:
                $heading = "Service catalog{request}{tattributes}";
//                $title = "{count, plural, one{# service} other{# services}} from {countPro, plural, one{# professional} other{# professionals}} | Choose and order now | Site of professionals uJobs.me";
                $title = "{count} Services{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count, plural, one{# service} other{# services}} from {countPro, plural, one{# professional} other{# professionals}} on the site of professionals uJobs.me{request}{tattributes}.";
                $keywords = "order services{city}, freelancer services{city}, order services{city} cheap{kequest}{kattributes}";
                $subheading = "Thousands of Professionals ready to fulfill your request(s)";
                $upperBlockHeading = "Are you looking for job to order?";
                $upperBlockSubheading = "{countPro, plural, one{# professional} other{# professionals}}{city} waiting for your orders.";
                $upperBlockColumn1 = "On our website you can choose a specialist {city} according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                break;
            case self::TEMPLATE_JOB_CATEGORY:
                $heading = "Order {category}{city}{request}{tattributes}";
//                $title = "{category}{city} | {count, plural, one{# service} other{# services}} category «{category}»{city} | Site of professionals uJobs.me";
                $title = "{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count, plural, one{# service} other{# services}} in category «{category}»{city} at the price from {minPrice} on the site of professionals uJobs.me{request}{tattributes}.";
                $keywords = "order {category}{city}, {category} services{city}, freelancer {category}{city}, order {category} from freelancer{city}, order {category}{city} cheap{kequest}{kattributes}";
                $upperBlockHeading = "Are you looking for {category} to order?";
                $upperBlockSubheading = "{countPro, plural, one{# professional} other{# professionals}} in {category}{city} waiting for your orders.";
                $upperBlockColumn1 = "On our website you can choose a specialist in «{category}»{city} according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_JOB_SUBCATEGORY:
                $heading = "Order {category}{city}{request}{tattributes}";
//                $title = "{category}{city} | {count, plural, one{# service} other{# services}} category «{category}»{city} | Site of professionals uJobs.me";
                $title = "{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count, plural, one{# service} other{# services}} in category «{category}»{city} at the price from {minPrice} on the site of professionals uJobs.me{request}{tattributes}.";
                $keywords = "order {category}{city}, {category} services{city}, freelancer {category}{city}, order {category} from freelancer{city}, order {category}{city} cheap{kequest}{kattributes}";
                $upperBlockHeading = "Are you looking for {category} to order?";
                $upperBlockSubheading = "{countPro, plural, one{# professional} other{# professionals}} in {category}{city} waiting for your orders.";
                $upperBlockColumn1 = "On our website you can choose a specialist in «{category}»{city} according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Post an order in a few simple steps, provide the necessary details, and the selected professional will take over.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_JOB_OTHER_SUBCATEGORY:
                $heading = "Order {category}{city}{request}{tattributes}";
//                $title = "Other services from «{parentCategory}»{city} | Choose from {count, plural, one{# service} other{# services}} | Site of professionals uJobs.me";
                $title = "{count} Other Services from «{parentCategory}»{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Choose from {count} other {count, plural, one{service} other{services}} in «{parentCategory}»{city} at the price from {minPrice} on the site of professionals uJobs.me{request}{tattributes}.";
                $keywords = "other services in {parentCategory}{city}{kequest}{kattributes}";
                $subheading = "Order other services in {parentCategory} from {countPro, plural, one{# freelancer} other{# freelancers}}{city}";
                $upperBlockHeading = "Are you looking for {parentCategory} to order?";
                $upperBlockSubheading = "{countPro, plural, one{# professional} other{# professionals}} awaiting your response.";
                $upperBlockColumn1 = "On our website you can choose a specialist according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Submit an order in a few simple steps and describe the details, so the chosen professional would get down to business.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_JOB:
                $title = "{Title} (Price {Price})";
                $description = "Order the service «{title}» at the price {price} on the site of professionals uJobs.me. Professional {username} waiting for your applications!";
                $keywords = "{title}, service {title}, order {title}";
                break;

            case self::TEMPLATE_VIDEO_ROOT_CATEGORY:
                $title = "Training videos | Choose and order now | Site of professionals uJobs.me";
                $description = "Choose training video on the site of professionals uJobs.me";
                $heading = 'Training video catalog';
                $subheading = 'Choose the topic of study that interests you';
                $upperBlockHeading = 'Do you want to learn a new specialty?';
                $upperBlockSubheading = 'With our video lessons it\'s easy to do';
                $upperBlockColumn1 = "Choose the topic of study that interests you";
                $upperBlockColumn2 = "Choose the video course of interest";
                $upperBlockColumn3 = "Pay and get new knowledge";
                break;

            case self::TEMPLATE_VIDEO_CATEGORY:
                $title = "Training video catalog about {category} | Choose and order now | Site of professionals uJobs.me";
                $description = "Choose Training video on the site of professionals uJobs.me";
                $heading = 'Training video catalog about {category}';
                $subheading = 'Choose the video courses you are interested in';
                $upperBlockHeading = 'Want to get new knowledge about {category}?';
                $upperBlockSubheading = 'With our video lessons it\'s easy to do';
                $upperBlockColumn1 = "Choose the course about {category}";
                $upperBlockColumn2 = "Make payment for course about {category}";
                $upperBlockColumn3 = "Get new knowledge about {category}";
                break;

            case self::TEMPLATE_VIDEO:
                $title = "Order learning video «{title}» | Site of professionals uJobs.me";
                $description = "Order learning video «{title}» on the site of professionals uJobs.me.";
                $keywords = "{title}, video {title}";
                break;

            case self::TEMPLATE_SKILL:
                $heading = "{title} specialist{city}";
                $subheading = 'Thousands of Professionals ready to fulfill your request(s)';
                $title = "{count, plural, one{# professional} other{# professionals}} in «{title}»{city} awaiting your tasks | Site of professionals uJobs.me";
                $description = "{count, plural, one{# professional} other{# professionals}} in «{title}»{city} awaiting your tasks. Order now on the site of professionals uJobs.me!";
                $keywords = "{title}{city}, order {title}{city}, {title} cheap, {title} freelancer{city};";
                $upperBlockHeading = "Are you looking for {title} to order{city}?";
                $upperBlockSubheading = "{count, plural, one{# professional} other{# professionals}} in {title}{city} awaiting your response.";
                $upperBlockColumn1 = "On our website you can choose a specialist in {title} according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Submit an order in a few simple steps and describe the details, so the chosen professional would get down to business.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_TAG:
                $heading = "Order {title}{city}";
                $subheading = 'Thousands of Professionals ready to fulfill your request(s)';
                $title = "{title}{city} | {count, plural, one{# service} other{# services}} in «{title}»{city} | Site of professionals uJobs.me";
                $description = "Choose from {count, plural, one{# service} other{# services}} in «{title}» category at the price from {minPrice} on the site of professionals uJobs.me.";
                $keywords = "order {title}{city}, {title} services{city}, freelancer {title}, order {title} from freelancer{city}";
                $upperBlockHeading = "Are you looking for {title} to order{city}?";
                $upperBlockSubheading = "{count, plural, one{# service} other{# services}} in {title}{city} awaiting your response.";
                $upperBlockColumn1 = "On our website you can choose a specialist according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Submit an order in a few simple steps and describe the details, so the chosen professional would get down to business.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                $textHeading = 'How to order “{keyword}”{city} on services marketplace uJobs';
                $textSubheading = 'With our marketplace you can inexpensively order “{keyword}” {city}';
                $textView = 'universal';
                break;
            case self::TEMPLATE_SPECIALTY:
                $heading = "Are you looking for a job such as {title}{city}?";
                $title = "{title} is required | Need {count} {title}{city} salary up to {maxPrice} | Site of professionals uJobs.me";
                $description = "Choose from {count, plural, one{# vacancy} other{# vacancy}} in «{title}» specialty{city} and earn to {maxPrice} on the site of professionals uJobs.me.";
                $keywords = "job {title}{city}, vacancies {title}{city}, {title} is required, need {title}{city}";
                $subheading = "Our Freelance marketplace offers: remote work, flexible hours and the guarantee of timely payment";
                $upperBlockHeading = "Are you looking for a job such as {title}{city}?";
                $upperBlockSubheading = "{count, plural, one{# customer} other{# customers}}{city} ready for give you a task {title}";
                $upperBlockColumn1 = "Choose a job from submitted vacancies in {title} choose suitable to you.";
                $upperBlockColumn2 = "Respond to request and tell why do the customer must choose you.";
                $upperBlockColumn3 = "Complete the job and when the customer approves it receive a payment.";
                break;
            case self::TEMPLATE_SPECIALTY_PRO:
                $heading = "Are you looking for a {title} professional{city}?";
                $title = "Professionals with {title} specialty{city} | Site of professionals uJobs.me";
                $description = "Choose from {count, plural, one{# professional} other{# professionals}} «{title}» specialty{city}.";
                $keywords = "worker {title}{city}, {title} is required, need {title}{city}";
                $subheading = "Our Freelance marketplace offers: search for remote workers and ensure timely performance of work";
                $upperBlockHeading = "Are you looking for a {title} professional{city}?";
                $upperBlockSubheading = "{count, plural, one{# professional} other{# professionals}}{city} awaiting your response.";
                $upperBlockColumn1 = "On our website you can choose a specialist according to your requirements and with reasonable price.";
                $upperBlockColumn2 = "Submit an order in a few simple steps and describe the details, so the chosen professional would get down to business.";
                $upperBlockColumn3 = "Professional will receive a payment only after he completes the job and you approve it.";
                break;

            case self::TEMPLATE_PRODUCT_ROOT_CATEGORY:
                $heading = "Board of private announcements{city}{request}{tattributes}";
                $subheading = "It is easy to buy or sell products{city} from individuals through secure transaction";
//                $title = "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} about selling products on marketplace{city} | Easy to buy or sell product on uJobs.me";
                $title = "{count} Products{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "Private announcements from {countSellers, plural, =0{sellers} one{# seller} other{# sellers}} on free board{city}{request}{tattributes}. Choose new or used product among {count, plural, =0{announcements} one{# announcement} other{# announcements}} cheap on uJobs.me.";
                $keywords = "Private announcements{city}, marketplace of services and goods{city}, free marketplace{city}{kequest}{kattributes}";
                $upperBlockHeading = "How does the marketplace work{city}?";
                $upperBlockSubheading = "We created a secure transaction to protect your interests.";
                $upperBlockColumn1 = "Choose product and add it to cart";
                $upperBlockColumn2 = "Pay deposit equal to cost of the product.";
                $upperBlockColumn3 = "Seller receives money right after customer receives product.";
                $textHeading = 'Board of private announcements{city}.';
                $textSubheading = 'We provide secure transaction for selling products for individuals.';
                $textView = 'product/product-root';
                break;
            case self::TEMPLATE_PRODUCT_CATEGORY:
                $heading = "Buy {category}{city}{request}{tattributes}";
                $subheading = "Buy {category}{attributes}{city} through secure transactions";
//                $title = "Buying {category}{attributes}{city} is easy through secure transaction | Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} about selling «{category}»{attributes}{city} on uJobs.me";
                $title = "{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} buy or sell in category «{category}»{attributes}{city} at the price from {minPrice} on uJobs.me{request}{tattributes}.";
                $keywords = "buy {category}{attributes}{city}, sell {category}{attributes}{city}, buy {category}{attributes}{city} cheap{kequest}{kattributes}";
                $upperBlockHeading = "Looking for a place to buy {category}{attributes}{city} through a secure transaction?";
                $upperBlockSubheading = "We created a secure transaction to protect your interests.";
                $upperBlockColumn1 = "Choose product in {category}{attributes}{city} and add it to cart";
                $upperBlockColumn2 = "Pay deposit equal to cost of the product.";
                $upperBlockColumn3 = "Seller receives money right after customer receives product.";
                $textHeading = 'Buy and sell products in {category}{attributes}{city}.';
                $textSubheading = 'We present private announcements about selling products in {category}{attributes}{city} via secure transaction.';
                $textView = 'product/product-category';
                break;
            case self::TEMPLATE_PRODUCT_SUBCATEGORY:
                $heading = "Buy {category}{city}{request}{tattributes}";
                $subheading = "A secure transaction guarantees a 100% refund if you are not satisfied with the product";
//                $title = "Buying {category}{attributes}{city} is easy through secure transaction | Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} about selling «{category}»{attributes}{city} on uJobs.me";
                $title = "{count} {category}{tags}{Attributes}{City}{Request} | Price from {minPrice}";
                $description = "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} buy or sell in category «{category}»{attributes}{city} at the price from {minPrice} on uJobs.me{request}{tattributes}.";
                $keywords = "buy {category}{attributes}{city}, sell {category}{attributes}{city}, buy {category}{attributes}{city} cheap{kequest}{kattributes}";
                $upperBlockHeading = "Looking for a place to buy {category}{attributes}{city} through a secure transaction?";
                $upperBlockSubheading = "We created a secure transaction to protect your interests.";
                $upperBlockColumn1 = "Choose product in {category}{attributes}{city} and add it to cart";
                $upperBlockColumn2 = "Pay deposit equal to cost of the product.";
                $upperBlockColumn3 = "Seller receives money right after customer receives product.";
                $textHeading = 'Buy and sell products in {category}{attributes}{city}.';
                $textSubheading = 'We present private announcements about selling products in {category}{attributes}{city}.';
                $textView = 'product/product-subcategory';
                break;
            case self::TEMPLATE_PRODUCT_TAG:
                $heading = "Buy {title}{city}";
                $subheading = "Secure transaction guarantees 100% money refund, is you are not satisfied by product";
                $title = "Buying {title}{attributes}{city} is easy through secure transaction. Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} on uJobs.me";
                $description = "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} buy and sell {title}{attributes}{city} by price {minPrice} from individuals and companies on uJobs.me.";
                $keywords = "buy {title}{attributes}{city}, sell {title}{attributes}{city}, buy {title}{attributes}{city} cheap";
                $upperBlockHeading = "How to buy {title}{attributes}{city} through a secure transaction?";
                $upperBlockSubheading = "We created a secure transaction to protect your interests.";
                $upperBlockColumn1 = "Choose product in {title}{attributes}{city} and add it to cart";
                $upperBlockColumn2 = "Pay deposit equal to cost of the product.";
                $upperBlockColumn3 = "Seller receives money right after customer receives product.";
                $textHeading = 'Buy and sell products in {title}{attributes}{city}.';
                $textSubheading = 'We present private announcements about selling products in {title}{attributes}{city}.';
                $textView = 'product/product-tag';
                break;
            case self::TEMPLATE_PRODUCT:
                $title = "{Title} (Price {Price})";
                $description = "{description}";
                $keywords = "{title}";
                break;
            case self::TEMPLATE_PRODUCT_TENDER:
                $title = "{Title} (Price {Price})";
                $description = "{description}";
                $keywords = "{title}";
                break;

            case self::TEMPLATE_PORTFOLIO_ROOT_CATEGORY:
                $heading = "Albums of our users{request}{tattributes}";
                $subheading = "{count, plural, =0{albums} one{# album} other{# albums}} from users are waiting for your ratings";
                $title = "You may create your portfolio album for free, or may see photos and examples of works from uJobs users{Request}{Attributes}";
                $description = "Here are {count, plural, =0{albums} one{# album} other{# albums}} with examples of works and photos from {countPro, plural, =0{authors} one{# author} other{# authors}} on uJobs.me{request}{tattributes}";
                $keywords = "Create portfolio{kequest}{kattributes}";
                $upperBlockHeading = "Albums of our users";
                $upperBlockSubheading = "{count, plural, =0{albums} one{# album} other{# albums}} from users are waiting for your ratings";
                $upperBlockColumn1 = "If you are not registered - do it now! If you are already registered - please log in.";
                $upperBlockColumn2 = "Use this <a href='/account/portfolio/create'>link</a> and follow all steps of the form.";
                $upperBlockColumn3 = "The more photos and likes your album has - the higher position it takes in catalog. Share your album with your friends!";
                $textHeading = 'Albums of our users';
                $textSubheading = 'We bring to your attention photos and examples of works from our users';
                $textView = 'portfolio/portfolio-root';
                break;
            case self::TEMPLATE_PORTFOLIO_CATEGORY:
            case self::TEMPLATE_PORTFOLIO_SUBCATEGORY:
                $heading = "Portfolio albums in category {category}{request}{tattributes}";
                $subheading = "{count, plural, =0{albums} one{# album} other{# albums}} from {countPro, plural, =0{users} one{# user} other{# users}} in category {category}";
                $title = "View examples of works and photos in category {category} on uJobs.me{Request}{Attributes}";
                $description = "{category} photos and examples of works from {countPro, plural, =0{users} one{# user} other{# users}} on uJobs.me{request}{tattributes}";
                $keywords = "{category} photos{kequest}{kattributes}";
                $upperBlockHeading = "Portfolio albums in category {category}";
                $upperBlockSubheading = "{count, plural, =0{albums} one{# album} other{# albums}} from {countPro, plural, =0{users} one{# user} other{# users}} in category {category}";
                $upperBlockColumn1 = "If you are not registered - do it now! If you are already registered - please log in.";
                $upperBlockColumn2 = "Use this <a href='/account/portfolio/create'>link</a> and follow all steps of the form.";
                $upperBlockColumn3 = "The more photos and likes your album has - the higher position it takes in catalog. Share your album with your friends!";
                $textHeading = 'Portfolio albums from users in category {category}';
                $textSubheading = 'Here are presented albums in category {category}';
                $textView = 'portfolio/portfolio-category';
                break;
            case self::TEMPLATE_PORTFOLIO_TAG:
                $heading = "Portfolio albums with tag {title}";
                $subheading = "{count, plural, =0{albums} one{# album} other{# albums}} with tag {title}";
                $title = "View examples of works and photos with tag {title} on uJobs.me";
                $description = "{title} photos and examples of works from {countPro, plural, =0{users} one{# user} other{# users}} on uJobs.me";
                $keywords = "{title} photos";
                $upperBlockHeading = "Portfolio albums with tag {title}";
                $upperBlockSubheading = "{count, plural, =0{albums} one{# album} other{# albums}} with tag {title}";
                $upperBlockColumn1 = "If you are not registered - do it now! If you are already registered - please log in.";
                $upperBlockColumn2 = "Use this <a href='/account/portfolio/create'>link</a> and follow all steps of the form.";
                $upperBlockColumn3 = "The more photos and likes your album has - the higher position it takes in catalog. Share your album with your friends!";
                $textHeading = 'Portfolio albums from users with tag {title}';
                $textSubheading = 'Here are presented albums with tag {title}';
                $textView = 'portfolio/portfolio-tag';
                break;

            case self::TEMPLATE_STORE_ROOT_CATEGORY:
                $heading = "Page of store {store} on site uJobs.me{request}{tattributes}";
                $subheading = "It is easy to buy or sell products{city} from individuals through secure transaction";
                $title = "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} about selling products on store page {store}{Request}{Attributes} | Easy to buy or sell product on uJobs.me";
                $description = "Private announcements from {store} on site uJobs.me. Choose product among {count, plural, =0{announcements} one{# announcement} other{# announcements}} cheap on uJobs.me{request}{tattributes}.";
                $keywords = "{store}{kequest}{kattributes}";
                $upperBlockHeading = "How does the marketplace work{city}?";
                $upperBlockSubheading = "We created a secure transaction to protect your interests.";
                $upperBlockColumn1 = "Choose product and add it to cart";
                $upperBlockColumn2 = "Pay deposit equal to cost of the product.";
                $upperBlockColumn3 = "Seller receives money right after customer receives product.";
                $textHeading = 'Page of store {store} on uJobs.me';
                $textSubheading = 'We provide secure transaction for selling products for individuals.';
                $textView = 'product/store-root';
                break;

            case self::TEMPLATE_STORE_CATEGORY:
            case self::TEMPLATE_STORE_SUBCATEGORY:
                $heading = "Products in {category} on store page {store}{request}{tattributes}";
                $subheading = "Buy {category}{attributes}{city} through secure transactions";
                $title = "Buying {category}{attributes}{city} is easy through secure transaction{Request} | Choose among {count, plural, =0{announcements} one{# announcement} other{# announcements}} in store {store} about selling «{category}»{attributes}{city} on uJobs.me";
                $description = "{count, plural, =0{private announcements} one{# private announcement} other{# private announcements}} buy or sell in category «{category}»{attributes}{city} at the price from {minPrice} on uJobs.me{request}{tattributes}.";
                $keywords = "buy {category}{attributes}{city}, sell {category}{attributes}{city}, buy {category}{attributes}{city} cheap{kequest}{kattributes}";
                $upperBlockHeading = "Looking for a place to buy {category}{attributes}{city} through a secure transaction?";
                $upperBlockSubheading = "We created a secure transaction to protect your interests.";
                $upperBlockColumn1 = "Choose product in {category}{attributes}{city} and add it to cart";
                $upperBlockColumn2 = "Pay deposit equal to cost of the product.";
                $upperBlockColumn3 = "Seller receives money right after customer receives product.";
                $textHeading = 'Buy and sell products in {category}{attributes}{city}.';
                $textSubheading = 'We present private announcements about selling products in {category}{attributes}{city} via secure transaction.';
                $textView = 'product/store-category';
                break;

            case self::TEMPLATE_PAGE_ROOT_CATEGORY:
                $heading = "Articles from our users{request}{tattributes}";
                $subheading = "{count, plural, =0{Articles} one{# article} other{# articles}} from our users are waiting for your ratings";
                $title = "{count, plural, =0{Articles} one{# article} other{# articles}} from {countPro, plural, =0{authors} one{# author} other{# authors}}{Request}{Attributes} | Read and rate now | Site of professionals uJobs.me";
                $description = "Here are {count, plural, =0{articles} one{# article} other{# articles}} from {countPro, plural, =0{authors} one{# author} other{# authors}} on uJobs.me{request}{tattributes}";
                $keywords = "Create article{kequest}{kattributes}";
                $upperBlockHeading = "Articles from our users";
                $upperBlockSubheading = "{count, plural, =0{Articles} one{# article} other{# articles}} from our users are waiting for your ratings";
                $upperBlockColumn1 = "If you are not registered - do it now! If you are already registered - please log in.";
                $upperBlockColumn2 = "Use this <a href='/article/form'>link</a> and follow all steps of the form.";
                $upperBlockColumn3 = "Share your article with your friends!";
                break;
            case self::TEMPLATE_PAGE_CATEGORY:
                $heading = "{category}{request}{tattributes}";
                $subheading = "Articles and news about «{category}»";
                $title = "Articles and news  about «{category}» from members of the site uJobs.me{Request}{Attributes}";
                $description = "{count, plural, =0{Articles and news} one{# article} other{# articles and news}} about «{category}» from members of the portal uJobs {count, plural, =one{is} other{are}} presented on this page{request}{tattributes}";
                $keywords = "{category} articles; {category} news{kequest}{kattributes}";
                $upperBlockHeading = "Articles in category {category}";
                $upperBlockSubheading = "{count, plural, =0{Articles} one{# article} other{# articles}} from {countPro, plural, =0{users} one{# user} other{# users}} in category {category}";
                $upperBlockColumn1 = "If you are not registered - do it now! If you are already registered - please log in.";
                $upperBlockColumn2 = "Use this <a href='/article/form'>link</a> and follow all steps of the form.";
                $upperBlockColumn3 = "Share your article with your friends!";
                $textHeading = 'Articles and news about «{category}»';
                $textSubheading = 'We prepared the most interesting collections from our participants about «{category}»';
                $textView = 'page/category';
                break;
            case self::TEMPLATE_PAGE_TAG:
                $heading = "{title}";
                $subheading = "Articles and news about «{title}»";
                $title = "Articles and news  about «{title}» from members of the site uJobs.me";
                $description = "{count, plural, =0{Articles and news} one{# article} other{# articles and news}} about «{title}» from members of the portal uJobs {count, plural, =one{is} other{are}} presented on this page";
                $keywords = "{title} articles; {title} news";
                $upperBlockHeading = "Articles with tag {title}";
                $upperBlockSubheading = "{count, plural, =0{Articles} one{# article} other{# articles}} with tag {title}";
                $upperBlockColumn1 = "If you are not registered - do it now! If you are already registered - please log in.";
                $upperBlockColumn2 = "Use this <a href='/article/form'>link</a> and follow all steps of the form.";
                $upperBlockColumn3 = "Share your article with your friends!";
                $textHeading = 'Articles and news about «{title}»';
                $textSubheading = 'We prepared the most interesting collections from our participants about «{title}»';
                $textView = 'page/tag';
                break;
            case self::TEMPLATE_COMPANY:
                $title = "Page of company {title} on site uJobs.me";
                $description = "View services and product, requests on services and products, and news from this company. Buying and selling is easy with secure transaction on uJobs.me";
                $keywords = "{title}, products {title}, services {title}";
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'subheading' => $subheading,
            'upperBlockHeading' => $upperBlockHeading,
            'upperBlockSubheading' => $upperBlockSubheading,
            'upperBlockColumn1' => $upperBlockColumn1,
            'upperBlockColumn2' => $upperBlockColumn2,
            'upperBlockColumn3' => $upperBlockColumn3,
            'textHeading' => $textHeading,
            'textSubheading' => $textSubheading,
            'textView' => $textView
        ];
    }

    /**
     * @return string
     */
    public function getEntityUrl()
    {
        if (defined('YII_BACKEND')) {
            $urlManager = Yii::$app->urlManagerFrontEnd;
        } else {
            $urlManager = Yii::$app->urlManager;
        }
        switch ($this->entity) {
            case 'job':
                return $urlManager->createUrl(['/job/view', 'alias' => Job::find()->where(['id' => $this->entity_content])->select('alias')->column()]);
                break;
            case 'tender':
                return $urlManager->createUrl(['/job/view', 'alias' => Job::find()->where(['id' => $this->entity_content])->select('alias')->column()]);
                break;
            case 'profile':
                return $urlManager->createUrl(['/account/profile/show', 'id' => $this->entity_content]);
                break;
            case 'category':
                $category = Category::findOne(['id' => $this->entity_content]);
                if ($category->alias == 'root') {
                    return $urlManager->createUrl(["/category/{$this->type}-catalog"]);
                } else {
                    return $urlManager->createUrl(["/category/{$this->type}s", 'category_1' => $category->alias]);
                }
                break;
            case 'skill':
                return $urlManager->createUrl(['/category/skill', 'reserved_skill' => $this->entity_content]);
                break;
            case 'tag':
                return $urlManager->createUrl(['/category/tag', 'reserved_job_tag' => $this->entity_content]);
                break;
            case 'specialty':
                return $urlManager->createUrl(['/category/specialty-tender', 'reserved_specialty' => $this->entity_content]);
                break;
            case 'specialty-pro':
                return $urlManager->createUrl(['/category/specialty-pro', 'reserved_specialty' => $this->entity_content]);
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * @param $string
     * @param array $params
     * @return mixed|string
     */
    private function applyParams($string, $params = [])
    {
        $placeholders = [];
        foreach ($params as $name => $value) {
            $placeholders['{' . $name . '}'] = $value;
        }
        $string = ($placeholders === []) ? $string : strtr($string, $placeholders);
        $string = preg_replace("/{.*?}/", "", $string);
        $string = preg_replace("/\s{2,}/", " ", $string);
        return $string;
    }
}