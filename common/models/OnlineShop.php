<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "online_shop".
 *
 * @property integer $id
 * @property string $title
 * @property string $city
 * @property string $email
 * @property string $site
 * @property string $vk
 * @property integer $mail_sent
 */
class OnlineShop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'online_shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['mail_sent', 'integer'],
            ['mail_sent', 'default', 'value' => false],
            ['email', 'required'],
            ['email', 'unique'],
            [['title', 'city', 'email', 'site', 'vk'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'city' => Yii::t('model', 'City'),
            'email' => Yii::t('model', 'Email'),
            'site' => Yii::t('model', 'Site'),
            'vk' => Yii::t('model', 'Vk'),
            'mail_sent' => Yii::t('model', 'Mail Sent'),
        ];
    }
}
