<?php

namespace common\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "job_qa".
 *
 * @property integer $id
 * @property integer $job_id
 * @property string $question
 * @property string $answer
 */
class JobQa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_qa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'required'],
            [['job_id'], 'integer'],
            [['question'], 'string', 'max' => 300],
            [['answer'], 'string', 'max' => 800],

            [['job_id'], 'exist', 'skipOnError' => false, 'targetClass' => Job::class, 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'job_id' => Yii::t('model', 'Job ID'),
            'question' => Yii::t('model', 'Question'),
            'answer' => Yii::t('model', 'Answer'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->question = HtmlPurifier::process($this->question);
            $this->answer = HtmlPurifier::process($this->answer);

            return true;
        }
        return false;
    }
}
