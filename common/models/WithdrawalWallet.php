<?php

namespace common\models;

use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "withdrawal_wallet".
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property string $number
 * @property mixed $custom_data
 *
 * @property User $user
 * @property Profile $profile
 * @property AttributeValue[] $countries
 */
class WithdrawalWallet extends ActiveRecord
{
    const TYPE_QIWI = 10;
    const TYPE_YANDEX = 20;
    const TYPE_WEBMONEY = 30;
    const TYPE_CREDIT_CARD = 40;
    const TYPE_PAYPAL = 50;
    const TYPE_STRIPE = 60;
    const TYPE_SWIFT = 70;
    const TYPE_MONEY_GRAM = 80;
    const TYPE_ALIPAY = 90;
    const TYPE_WECHAT_PAY = 100;
    const TYPE_UNION_PAY = 110;
    const TYPE_BILL_DECK = 120;
    const TYPE_UNIBANCO = 130;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'withdrawal_wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'integer'],
            ['number', 'required'],
            [['number'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['type', 'in', 'range' => [
                self::TYPE_QIWI, 
                self::TYPE_YANDEX, 
                self::TYPE_WEBMONEY, 
                self::TYPE_CREDIT_CARD,
                self::TYPE_PAYPAL,
                self::TYPE_STRIPE,
                self::TYPE_SWIFT,
                self::TYPE_MONEY_GRAM,
                self::TYPE_ALIPAY,
                self::TYPE_WECHAT_PAY,
                self::TYPE_UNION_PAY,
                self::TYPE_BILL_DECK,
                self::TYPE_UNIBANCO,
            ]],
            ['customDataArray', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'type' => Yii::t('model', 'Type'),
            'number' => Yii::t('model', 'Wallet number'),
            'payment_system' => Yii::t('model', 'Payment system'),
            'bank' => Yii::t('model', 'Bank name'),
            'receiver' => Yii::t('model', 'Receiver name'),
            'bic' => Yii::t('model', 'Bank SWIFT-Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(AttributeValue::class, ['id' => 'country_id'])->viaTable('withdrawal_type_country', ['type' => 'type']);
    }
    
    public static function getTypeLabels() {
        return [
            self::TYPE_QIWI => Yii::t('labels', 'Qiwi Wallet'),
            self::TYPE_YANDEX => Yii::t('labels', 'Yandex Wallet'),
            self::TYPE_WEBMONEY => Yii::t('labels', 'Webmoney Wallet'),
            self::TYPE_CREDIT_CARD => Yii::t('labels', 'Credit Card'),
            self::TYPE_PAYPAL => Yii::t('labels', 'PayPal'),
            self::TYPE_STRIPE => Yii::t('labels', 'Stripe'),
            self::TYPE_SWIFT => Yii::t('labels', 'SWIFT'),
            self::TYPE_MONEY_GRAM => Yii::t('labels', 'MoneyGram'),
            self::TYPE_ALIPAY => Yii::t('labels', 'Alipay'),
            self::TYPE_WECHAT_PAY => Yii::t('labels', 'WeChat Pay'),
            self::TYPE_UNION_PAY => Yii::t('labels', 'UnionPay Online Payments'),
            self::TYPE_BILL_DECK => Yii::t('labels', 'BillDesk'),
            self::TYPE_UNIBANCO => Yii::t('labels', 'Itaú Unibanco'),
        ];
    }

    /**
     * @return string
     */
    public function getTypeLabel()
    {
        $labels = static::getTypeLabels();
        return isset($labels[$this->type]) ? $labels[$this->type] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @return array
     */
    public function getCustomDataFields(){
        switch ($this->type) {
            case self::TYPE_CREDIT_CARD:
                return ['payment_system', 'bank'];
                break;
            case self::TYPE_SWIFT:
                return ['receiver', 'bic'];
                break;
            default:
                return [];
                break;
        }
    }

    /**
     * @param $field
     * @return null|string
     */
    public function getFieldPlaceholder($field) {
        switch ($field) {
            case 'number':
                return Yii::t('account', 'For example: {example}', ['example' => $this->getTypeExample()]);
                break;
            case 'payment_system':
                return Yii::t('app', 'Visa, MasterCard, etc.');
                break;
//            case 'bank':
//                return Yii::t('account', 'Visa, MasterCard, etc.');
//                break;
//            case 'receiver':
//                return Yii::t('account', 'Visa, MasterCard, etc.');
//                break;
            case 'bic':
                return Yii::t('account', 'For example: {example}', ['example' => 'AAAA BB CC DDD']);
                break;
            default:
                return null;
                break;
        }
    }

    /**
     * @return null|string
     */
    public function getTypeExample(){
        switch ($this->type) {
            case self::TYPE_QIWI:
                return '+9(999)999-99-99';
                break;
            case self::TYPE_YANDEX:
                return '12345678901';
                break;
            case self::TYPE_WEBMONEY:
                return 'Z123456789012';
                break;
            case self::TYPE_PAYPAL:
                return 'mypaypal@gmail.com';
                break;
            case self::TYPE_CREDIT_CARD:
            case self::TYPE_SWIFT:
            case self::TYPE_UNION_PAY:
            case self::TYPE_UNIBANCO:
                return '1234-5678-9012-3456';
                break;
            case self::TYPE_ALIPAY:
            case self::TYPE_BILL_DECK:
            case self::TYPE_MONEY_GRAM:
            case self::TYPE_WECHAT_PAY:
            case self::TYPE_STRIPE:
                return '123456789012';
                break;
            default:
                return null;
                break;
        }
    }

    /**
     * @return array
     */
    public static function getMasks(){
        return [
            'qiwi-mask' => '+000000000009999',
            'yandex-mask' => '0000000000099999',
            'wm-mask' => 'S000000000000',
            'credit-card-mask' => '0000-0000-0000-0000999',
        ];
    }

    /**
     * @return null|string
     */
    public function getTypeMask(){
        switch ($this->type) {
            case self::TYPE_QIWI:
                return 'qiwi-mask';
                break;
            case self::TYPE_YANDEX:
                return 'yandex-mask';
                break;
            case self::TYPE_WEBMONEY:
                return 'wm-mask';
                break;
            case self::TYPE_CREDIT_CARD:
            case self::TYPE_SWIFT:
            case self::TYPE_UNION_PAY:
            case self::TYPE_UNIBANCO:
                return 'credit-card-mask';
                break;
            default:
                return null;
                break;
        }
    }

    public static function getTypesByCountry($user_id) {
        $countryAttributeId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
        $countryValueId = UserAttribute::find()->where(['attribute_id' => $countryAttributeId, 'user_id' => $user_id])->select('value')->scalar();
        if ($countryValueId === false) {
            $countryValueId = AttributeValue::find()
                ->joinWith(['translations'])
                ->where(['attribute_id' => $countryAttributeId])
                ->andWhere(['mod_attribute_description.title' => 'Россия'])
                ->select('mod_attribute_value.id')
                ->scalar();
        }
        $allTypes = WithdrawalWallet::getTypeLabels();
        $types = WithdrawalTypeCountry::find()
            ->where(['not exists', WithdrawalTypeCountry::find()
                ->from(['wtc' => 'withdrawal_type_country'])
                ->where('withdrawal_type_country.type = wtc.type')
                ->andWhere(['country_id' => $countryValueId])
            ])
            ->select('type')
            ->groupBy('type')
            ->indexBy('type')
            ->column();
        return [
            'preferredTypes' => array_diff_key($allTypes, $types),
            'otherTypes' => array_intersect_key($allTypes, $types)
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->custom_data !== null) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->custom_data !== null && empty($this->customDataArray)) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->custom_data = json_encode(array_intersect_key($this->customDataArray, array_flip($this->getCustomDataFields())));
        return parent::beforeSave($insert);
    }
}
