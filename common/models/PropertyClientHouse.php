<?php

namespace common\models;

use common\modules\board\models\Product;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "property_client_house".
 *
 * @property int $id
 * @property int $client_id
 * @property string $entity
 * @property int $entity_id
 *
 * @property PropertyClient $client
 * @property Job|Product $house
 */
class PropertyClientHouse extends ActiveRecord
{
    const TYPE_JOB = 'job';
    const TYPE_PRODUCT = 'product';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_client_house';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'entity_id'], 'integer'],
            [['entity'], 'string', 'max' => 255],
            ['entity', 'in', 'range' => [self::TYPE_JOB, self::TYPE_PRODUCT]],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyClient::class, 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'client_id' => Yii::t('model', 'Client ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(PropertyClient::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        switch ($this->entity) {
            case self::TYPE_JOB :
                return $this->hasOne(Job::class, ['id' => 'entity_id']);
                break;
            case self::TYPE_PRODUCT:
                return $this->hasOne(Product::class, ['id' => 'entity_id']);
                break;
            default :
                return null;
                break;
        }
    }

    /**
     * @return string
     */
    public function getLabel(){
        return $this->house->getLabel();
    }
}
