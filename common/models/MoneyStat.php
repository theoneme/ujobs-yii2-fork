<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "money_stat".
 *
 * @property integer $id
 * @property integer $year
 * @property integer $month
 * @property integer $day
 * @property integer $advertisement
 */
class MoneyStat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money_stat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'month', 'day', 'advertisement'], 'integer'],
            ['advertisement', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'year' => Yii::t('model', 'Year'),
            'month' => Yii::t('model', 'Month'),
            'day' => Yii::t('model', 'Day'),
            'advertisement' => Yii::t('model', 'Advertisement'),
        ];
    }
}
