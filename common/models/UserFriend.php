<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "user_friend".
 *
 * @property int $user_id
 * @property int $target_user_id
 * @property int $status
 *
 * @property User $user
 * @property User $targetUser
 */
class UserFriend extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 10;
    const STATUS_APPROVED = 20;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_friend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'target_user_id', 'status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            [['user_id', 'target_user_id'], 'unique', 'targetAttribute' => ['user_id', 'target_user_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['target_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['target_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('model', 'First User ID'),
            'target_user_id' => Yii::t('model', 'Second User ID'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTargetUser()
    {
        return $this->hasOne(User::class, ['id' => 'target_user_id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert === true) {
            Yii::$app->db->createCommand()
                ->insert('user_friend', [
                    'user_id' => $this->target_user_id,
                    'target_user_id' => $this->user_id,
                    'status' => $this->status
                ])->execute();

            $notification = new Notification([
                'to_id' => $this->target_user_id,
                'from_id' => $this->user_id,
                'thumb' => $this->user->getThumb('catalog'),
                'custom_data' => json_encode([
                    'action' => null,
                    'user' => $this->user->getSellerName(),
                ]),
                'template' => Notification::TEMPLATE_FRIENDSHIP_REQUEST,
                'subject' => Notification::SUBJECT_FRIENDSHIP_REQUEST,
                'linkRoute' => ['/account/profile/show', 'id' => $this->user_id],
                'withEmail' => false,
                'is_visible' => true
            ]);

            $notification->save();
        }
    }

    /**
     * @return int
     */
    public function afterDelete()
    {
        parent::afterDelete();

        return self::deleteAll(['user_id' => $this->target_user_id, 'target_user_id' => $this->user_id]);
    }
}
