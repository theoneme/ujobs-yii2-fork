<?php

namespace common\models;

use common\models\user\Profile;
use common\models\user\User;
use common\services\elastic\PageElasticService;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "like".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $entity
 * @property integer $entity_id
 * @property integer $created_at
 *
 * @property User $user
 * @property Profile $profile
 */
class Like extends ActiveRecord
{
    const ENTITY_PORTFOLIO = 'portfolio';
    const ENTITY_PAGE = 'page';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'like';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entity_id'], 'required'],
            [['user_id', 'entity_id', 'created_at'], 'integer'],
            [['entity'], 'string', 'max' => 25],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'created_at' => Yii::t('model', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->entity === self::ENTITY_PORTFOLIO) {
            /** @var UserPortfolio $portfolio */
            $portfolio = UserPortfolio::find()->where(['id' => $this->entity_id])->one();

            if($portfolio !== null) {
                (new Notification([
                    'to_id' => $portfolio->user_id,
                    'from_id' => $this->user_id,
                    'thumb' => $portfolio->getThumb('catalog'),
                    'custom_data' => json_encode([
                        'user' => $this->user->getSellerName(),
                    ]),
                    'template' => Notification::TEMPLATE_PORTFOLIO_LIKED,
                    'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                    'linkRoute' => ["/account/portfolio/show", 'id' => $this->entity_id],
                    'withEmail' => false,
                    'is_visible' => true
                ]))->save();
            }
        }

        return $this->updateEntityElastic();
    }

    /**
     * @return bool
     */
    public function updateEntityElastic()
    {
        switch ($this->entity) {
            case self::ENTITY_PORTFOLIO:
                $item = UserPortfolio::findOne($this->entity_id);
                if ($item !== null) {
                    return $item->updateElastic();
                }
                break;
            case self::ENTITY_PAGE:
                $item = Page::findOne($this->entity_id);
                if ($item !== null) {
                    return (new PageElasticService($item))->updateLikes();
                }
                break;
        }

        return false;
    }
}
