<?php

namespace common\models;

use common\components\CurrencyHelper;
use common\models\user\User;
use common\traits\FindOrCreateTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "referral".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $referral_id
 * @property integer $total_bonus
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $createdBy
 * @property User $referral
 *
 * @mixin FindOrCreateTrait
 */
class Referral extends ActiveRecord
{
    use FindOrCreateTrait;

    const STATUS_SENT = 0;
    const STATUS_REGISTERED = 10;
    const STATUS_BOUGHT = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referral';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'referral_id', 'status', 'total_bonus'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['referral_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['referral_id' => 'id']],

            ['status', 'default', 'value' => self::STATUS_SENT],
            ['status', 'in', 'range' => [self::STATUS_SENT, self::STATUS_REGISTERED, self::STATUS_BOUGHT]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'created_by' => Yii::t('model', 'Created By'),
            'referral_id' => Yii::t('model', 'Referral_id'),
            'email' => Yii::t('model', 'Email'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'total_bonus' => Yii::t('model', 'Total bonus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferral()
    {
        return $this->hasOne(User::class, ['id' => 'referral_id']);
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $this->total_bonus);
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_SENT => Yii::t('model', 'Invitation sent'),
            self::STATUS_REGISTERED => Yii::t('model', 'Registered'),
            self::STATUS_BOUGHT => Yii::t('model', 'Made purchase'),
        ];
    }

    /** @inheritdoc */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($changedAttributes['status'] == self::STATUS_REGISTERED && $this->status > self::STATUS_REGISTERED) {
            $bonus = new PaymentHistory([
                'user_id' => $this->created_by,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_FRIEND_REGISTER_BONUS,
                'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                'amount' => 1,
            ]);
            $bonus->save();
        }
    }
}
