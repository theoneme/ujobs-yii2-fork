<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "job_requirements".
 *
 * @property integer $id
 * @property integer $job_id
 * @property integer $step_id
 * @property integer $is_required
 * @property integer $type
 * @property string $title
 * @property integer $custom_data
 *
 * @property Job $job
 * @property JobRequirementStep $step
 */
class JobRequirement extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_requirement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'step_id', 'is_required', 'type', 'custom_data'], 'integer'],
            [['title'], 'string', 'max' => 125],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::class, 'targetAttribute' => ['job_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobRequirementStep::class, 'targetAttribute' => ['step_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'is_required' => Yii::t('model', 'Is Required'),
            'type' => Yii::t('model', 'Type'),
            'title' => Yii::t('model', 'Requirement'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::class, ['id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(JobRequirementStep::class, ['id' => 'step_id']);
    }
}
