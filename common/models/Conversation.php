<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\models\user\User;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "conversation".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $is_blank
 * @property string $title
 * @property string $avatar
 *
 * @property User $createdBy
 * @property ConversationMember[] $conversationMembers
 * @property ConversationMember[] $otherMembers
 * @property User[] $users
 * @property Message $firstMessage
 * @property Message $lastMessage
 * @property Message[] $messages
 * @property ConversationMember $otherMember
 *
 * @mixin LinkableBehavior
 */
class Conversation extends ActiveRecord
{
    const TYPE_DEFAULT = 0;
    const TYPE_ORDER = 10;
    const TYPE_GROUP = 20;
    const TYPE_INFO = 30;

    /**
     * @var ConversationMember
     */
    private $_otherMember = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conversation';
    }

    /**
     * @param array $members
     * @return \yii\db\ActiveQuery
     */
    public static function findByMembers($members)
    {
        $query = Conversation::find();
        foreach ($members as $k => $v) {
            $query->andWhere(['exists', ConversationMember::find()
                ->from(['cm' . $k => 'conversation_member'])
                ->andWhere("conversation.id = cm{$k}.conversation_id")
                ->andWhere(["cm{$k}.user_id" => $v])
            ]);
        }
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => false,
            ],
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['conversationMembers'],
            ],
            'avatar' => [
                'class' => ImageBehavior::class,
                'folder' => 'conversation',
                'imageField' => 'avatar'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'created_at', 'updated_at'], 'integer'],
            ['is_blank', 'boolean'],
            ['type', 'default', 'value' => self::TYPE_DEFAULT],
            ['type', 'in', 'range' => [self::TYPE_DEFAULT, self::TYPE_ORDER, self::TYPE_GROUP, self::TYPE_INFO]],
            ['is_blank', 'default', 'value' => true],
            ['title', 'string', 'max' => 75],
            ['avatar', 'string', 'max' => 155],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'type' => Yii::t('model', 'Type'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversationMembers()
    {
        return $this->hasMany(ConversationMember::class, ['conversation_id' => 'id'])->indexBy('user_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('conversation_member', ['conversation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::class, ['conversation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstMessage()
    {
        return $this->hasOne(Message::class, ['conversation_id' => 'id'])->orderBy(['message.created_at' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastMessage()
    {
        return $this->hasOne(Message::class, ['conversation_id' => 'id'])->orderBy(['message.created_at' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastMessages()
    {
        return $this->hasMany(Message::class, ['conversation_id' => 'id'])
            ->andOnCondition(['message.type' => Message::TYPE_MESSAGE])
            ->orderBy(['message.created_at' => SORT_DESC])
            ->limit(30);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtherMembers()
    {
        return $this->hasMany(ConversationMember::class, ['conversation_id' => 'id'])->andWhere(['not', ['conversation_member.user_id' => Yii::$app->user->identity->getId()]])->indexBy('user_id');
    }


    /**
     * @param null $user_id
     * @return ConversationMember|null
     */
    public function getOtherMember($user_id = null)
    {
        if ($this->_otherMember === null && count($this->conversationMembers) > 1) {
            /** @var $otherMember ConversationMember*/
            $this->_otherMember = array_values(array_diff_key($this->conversationMembers, [$user_id ?? Yii::$app->user->identity->getId() => 0]))[0];
        }
        return $this->_otherMember;
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->avatar, $target);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->title;
    }
}
