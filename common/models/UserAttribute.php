<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.02.2017
 * Time: 17:44
 */

namespace common\models;

use common\models\elastic\UserAttributeElastic;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\services\elastic\UserAttributeElasticService;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "user_attribute".
 *
 * @property integer $id
 * @property string $attribute_id
 * @property integer $user_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 * @property string $custom_data
 *
 * @property Profile $profile
 * @property User $user
 * @property Attribute $attr
 * @property AttributeValue $attrValue
 * @property AttributeDescription $attrDescription
 * @property AttributeDescription $attrValueDescription
 * @property AttributeDescription[] $attrValueDescriptions
 * @property SeoAdvanced $meta
 */
class UserAttribute extends ActiveRecord
{
    /**
     * @var int
     */
    public $relatedCount = 0;

    /**
     * @var SeoAdvanced|null
     */
    public $_meta = null;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_attribute';
    }

    /**
     * @param $condition
     * @param $save bool
     * @return UserAttribute
     */
    public static function findOrCreate($condition, $save = true)
    {
        $attribute = UserAttribute::find()->where($condition)->one();
        if ($attribute === null) {
            $attribute = new UserAttribute($condition);
            if ($save) {
                $attribute->save();
            }
        }
        return $attribute;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locale'], 'required'],
            [['user_id', 'relatedCount', 'attribute_id'], 'integer'],
            [['value'], 'integer'],
            [['entity_alias', 'value_alias'], 'string', 'max' => 75],
            [['locale'], 'string', 'max' => 15],
            [['user_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            ['custom_data', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_content' => Yii::t('model', 'Entity Content'),
            'user_id' => Yii::t('model', 'User ID'),
            'key' => Yii::t('model', 'Key'),
            'value' => Yii::t('model', 'Value'),
            'locale' => Yii::t('model', 'Locale'),
            'is_text' => Yii::t('model', 'Is Text'),
            'entity_alias' => Yii::t('model', 'Entity Alias'),
            'value_alias' => Yii::t('model', 'Value Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue');
        $relation->from('mod_attribute_description mad_2')->indexBy('locale');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1')->indexBy('locale');

        return $relation;
    }

    /**
     * @return SeoAdvanced|null
     */
    public function getMeta($queryParams)
    {
        if ($this->_meta === null) {
            $entity = $this->attr->alias === 'specialty' ? 'specialty-pro' : $this->attr->alias;
            $this->_meta = SeoAdvanced::find()->where(['entity' => $entity, 'entity_content' => $this->value_alias])->one();
            if ($this->_meta !== null) {
                $data = json_decode($this->_meta->custom_data, true);
                $data = is_array($data) ? $data : [];
                $data['title'] = Html::encode(preg_replace("/[,.;]/", "", $this->getTitle()));

                $data['city'] = '';

                $cityParam = ArrayHelper::remove($queryParams, 'city');
                if($cityParam !== null) {
                    /** @var AttributeValue $city */
                    $city = AttributeValue::find()->where(['attribute_id' => 42, 'alias' => $cityParam])->one();

                    if($city !== null) {
                        $data['city'] = $city ? Yii::t('seo', ' in {city}', ['city' => $city->getTitle()]) : '';
                    }
                }

                $this->_meta->applyTemplates($data);
            }
        }

        return $this->_meta;
    }

    /**
     * Get attribute's title
     */
    public function getTitle()
    {
        return $this->attrValueDescriptions[Yii::$app->language]->title ?? array_values($this->attrValueDescriptions)[0]->title;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
//        $this->updateElastic();
    }

    /**
     * Updates related UserAttributeElastic
     * @param bool $doSave
     * @return bool|UserAttributeElastic
     */
    public function updateElastic($doSave = true)
    {
        $userAttributeElasticService = new UserAttributeElasticService($this);
        return $userAttributeElasticService->process($doSave);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            UserAttributeElastic::deleteAll(['id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->custom_data !== null) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }
    }
}
