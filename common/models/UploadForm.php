<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    /**
     * @var string
     */
    public $path = '/uploads/';

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 8],
        ];
    }

    /**
     * @return array|bool
     */
    public function upload()
    {
        $mediaLayer = Yii::$app->get('mediaLayer');

        $names = [];
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $dir = Yii::getAlias('@webroot') . $this->path;

                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }

                $name = $this->path . hash('crc32b', $file->name . time()) . "." . $file->extension;
                $file->saveAs(Yii::getAlias('@webroot') . $name);

                Image::getImagine()->open(Yii::getAlias('@webroot') . $name)
                    ->save(Yii::getAlias('@webroot') . $name, ['quality' => 80]);

                $mediaLayer->saveToAws($name);
                $names[] = $name;
            }
            return $names;
        } else {
            return false;
        }
    }
}