<?php

namespace common\models;

use common\models\elastic\UserPortfolioAttributeElastic;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\services\elastic\PortfolioAttributeElasticService;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_portfolio_attribute".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $user_portfolio_id
 * @property integer $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property UserPortfolio $userPortfolio
 * @property Attribute $attr
 * @property AttributeValue $attrValue
 * @property AttributeDescription $attrValueDescription
 * @property AttributeDescription[] $attrValueDescriptions
 * @property AttributeDescription $attrDescription
 * @property AttributeDescription[] $attrDescriptions
 */
class UserPortfolioAttribute extends ActiveRecord
{
    public $relatedCount = 0;

    /**
     * @var SeoAdvanced|null
     */
    public $_meta = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_portfolio_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'user_portfolio_id', 'value'], 'integer'],
            [['locale'], 'required'],
            [['locale'], 'string', 'max' => 5],
            [['entity_alias'], 'string', 'max' => 55],
            [['value_alias'], 'string', 'max' => 75],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            [['entity_alias'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['entity_alias' => 'alias']],
            [['user_portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserPortfolio::class, 'targetAttribute' => ['user_portfolio_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['value' => 'id']],
            [['value_alias'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['value_alias' => 'alias']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'user_portfolio_id' => Yii::t('model', 'User Portfolio ID'),
            'value' => Yii::t('model', 'Value'),
            'locale' => Yii::t('model', 'Locale'),
            'entity_alias' => Yii::t('model', 'Entity Alias'),
            'value_alias' => Yii::t('model', 'Value Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPortfolio()
    {
        return $this->hasOne(UserPortfolio::class, ['id' => 'user_portfolio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue')->indexBy('locale');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1')->indexBy('locale');

        return $relation;
    }

    /**
     * @return SeoAdvanced|null
     */
    public function getMeta()
    {
        if ($this->_meta === null) {
            $entity = $this->attr->alias;
            $this->_meta = SeoAdvanced::find()->where(['entity' => $entity, 'entity_content' => $this->value_alias])->one();
            if ($this->_meta !== null) {
                $data = json_decode($this->_meta->custom_data, true);
                $data = is_array($data) ? $data : [];
                $data['title'] = preg_replace("/[,.;]/", "", $this->getTitle());
                $this->_meta->applyTemplates($data);
            }
        }

        return $this->_meta;
    }

    /**
     * Get attribute's title
     */
    public function getTitle()
    {
        return $this->attrValueDescriptions[Yii::$app->language]->title ?? array_values($this->attrValueDescriptions)[0]->title;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
//        $this->updateElastic();
    }

    /**
     * @param bool $doSave
     * @return bool|UserPortfolioAttributeElastic
     */
    public function updateElastic($doSave = true)
    {
        $portfolioAttributeElasticService = new PortfolioAttributeElasticService($this);
        return $portfolioAttributeElasticService->process($doSave);
    }
}
