<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "email_logs".
 *
 * @property int $id
 * @property string $email
 * @property resource $data
 * @property int $created_at
 */
class EmailLog extends \yii\db\ActiveRecord
{
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['data'], 'string'],
            [['created_at'], 'integer'],
            [['email'], 'string', 'max' => 255],
            ['customDataArray', 'safe']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'email' => Yii::t('model', 'Email'),
            'data' => Yii::t('model', 'Data'),
            'created_at' => Yii::t('model', 'Created At'),
        ];
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->customDataArray = json_decode($this->data, true);

        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->data = json_encode($this->customDataArray);

        return parent::beforeValidate();
    }
}
