<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "measure_translation".
 *
 * @property integer $id
 * @property integer $measure_id
 * @property string $title
 * @property string $locale
 *
 * @property Measure $measure
 */
class MeasureTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measure_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['measure_id'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['locale'], 'string', 'max' => 10],
            [['measure_id'], 'exist', 'skipOnError' => true, 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'measure_id' => Yii::t('model', 'Measure ID'),
            'title' => Yii::t('model', 'Title'),
            'locale' => Yii::t('model', 'Locale'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::class, ['id' => 'measure_id']);
    }
}
