<?php

namespace common\models;

use common\components\CurrencyHelper;
use common\models\user\User;
use common\modules\store\models\CartItemInterface;
use common\modules\store\models\Order;
use common\modules\store\models\OrderProduct;
use common\modules\store\models\PaymentMethod;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "offer".
 *
 * @property integer $id
 * @property integer $from_id
 * @property integer $to_id
 * @property integer $message_id
 * @property string $description
 * @property string $subject
 * @property string $requirements
 * @property integer $status
 * @property integer $type
 * @property integer $price
 * @property string $currency_code
 *
 * @property User $from
 * @property User $to
 * @property Message $message
 */
class Offer extends ActiveRecord implements CartItemInterface
{
    const STATUS_REQUESTED = 0;
    const STATUS_OFFERED = 10;
    const STATUS_SENT = 15;
    const STATUS_ACCEPTED = 20;
    const STATUS_DECLINED = 30;
    const STATUS_CANCELED = 40;
    const STATUS_PAID = 50;

    const TYPE_OFFER = 0;
    const TYPE_REQUEST = 10;

    public static $typeString = [
        self::TYPE_OFFER => 'Offer',
        self::TYPE_REQUEST => 'Request',
    ];

    public static $senderString = [
        self::TYPE_OFFER => 'Worker',
        self::TYPE_REQUEST => 'Customer',
    ];

    public static $receiverString = [
        self::TYPE_REQUEST => 'Worker',
        self::TYPE_OFFER => 'Customer',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'to_id', 'message_id', 'status', 'price'], 'integer'],
            ['price', 'required'],
            ['price', 'number'],
            [['description'], 'string'],
            [['requirements'], 'string', 'max' => 700],
            [['subject'], 'string', 'max' => 155],

            [['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['from_id' => 'id']],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::class, 'targetAttribute' => ['message_id' => 'id']],
            [['to_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['to_id' => 'id']],

            ['status', 'default', 'value' => self::STATUS_SENT],

            ['type', 'in', 'range' => [self::TYPE_OFFER, self::TYPE_REQUEST]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'from_id' => Yii::t('model', 'From ID'),
            'to_id' => Yii::t('model', 'To ID'),
            'message_id' => Yii::t('model', 'Message ID'),
            'description' => Yii::t('model', 'Description'),
            'status' => Yii::t('model', 'Status'),
            'type' => Yii::t('model', 'Type'),
            'price' => Yii::t('model', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::class, ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::class, ['id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::class, ['id' => 'to_id']);
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels();
        $label = isset($labels[$this->status]) ? Yii::t('labels', self::$typeString[$this->type] . ' ' . $labels[$this->status]) : Yii::t('labels', 'Unknown status');
        return ($colored ? '<span style="color: ' . $this->getStatusColor() . '">' . $label . '</span>' : $label);
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_SENT => 'Sent',
            self::STATUS_ACCEPTED => 'Accepted',
            self::STATUS_CANCELED => 'Withdrawn',
            self::STATUS_DECLINED => 'Declined',
            self::STATUS_PAID => 'Paid',
        ];
    }

    /**
     * @return string
     */
    public function getStatusColor()
    {
        switch ($this->status) {
            case self::STATUS_SENT :
                return '#2d618c';
                break;
            case self::STATUS_CANCELED :
                return '#ac4137';
                break;
            case self::STATUS_DECLINED :
                return '#ac4137';
                break;
            case self::STATUS_ACCEPTED :
                return '#3ab845';
                break;
            case self::STATUS_PAID :
                return '#3ab845';
                break;
            default :
                return '#000';
                break;
        }
    }

    /**
     * @return string
     */
    public function getStatusButtons()
    {
        if (hasAccess($this->from_id)) {
            return $this->getWorkerStatusButtons();
        } else {
            return $this->getCustomerStatusButtons();
        }
    }

    /**
     * @return string
     */
    public function getWorkerStatusButtons()
    {
        switch ($this->status) {
            case self::STATUS_SENT :
                if ($this->type === self::TYPE_REQUEST) {
                    return Html::a(Yii::t('order', 'Accept request'), ['/account/inbox-ajax/accept-offer', 'id' => $this->id], [
                            'class' => 'btn-small offer-action',
                            'data-pjax' => 0
                        ]) .
                        Html::a(Yii::t('order', 'Decline request'), ['/account/inbox-ajax/decline-offer', 'id' => $this->id], [
                            'class' => 'btn-small red offer-action',
                            'data-pjax' => 0
                        ]);
                } else {
                    return Html::a(Yii::t('order', 'Cancel offer'), ['/account/inbox-ajax/cancel-offer', 'id' => $this->id], [
                        'class' => 'blue-but offer-action',
                        'data-pjax' => 0
                    ]);
                }
                break;
            case self::STATUS_DECLINED:
                return Html::a(Yii::t('order', 'Post Job'), ['/job/create'], [
                    'data-pjax' => 0,
                    'class' => 'btn-small'
                ]);
                break;
            case self::STATUS_PAID:
                $order_id = OrderProduct::find()
                    ->joinWith(['order'])
                    ->where(['product_id' => $this->id, 'product_type' => Order::TYPE_OFFER])
                    ->select('order.id')
                    ->scalar();
                return Html::a(Yii::t('order', 'Go to order'), ['/account/order/history', 'id' => $order_id], [
                    'data-pjax' => 0,
                    'class' => 'btn-small'
                ]);
                break;
            default :
//                return "<span class='btn-small disabled offer-action'>" . $this->getStatusLabel() . "</span>";
                return "";
                break;
        }
    }

    /**
     * @return string
     */
    public function getCustomerStatusButtons()
    {
        switch ($this->status) {
            case self::STATUS_SENT :
                if ($this->type == self::TYPE_REQUEST) {
                    return Html::a(Yii::t('order', 'Cancel request'), ['/account/inbox-ajax/cancel-offer', 'id' => $this->id], [
                        'class' => 'blue-but offer-action',
                        'data-pjax' => 0,
                    ]);
                } else {
                    return Html::a(Yii::t('order', 'Accept offer'), ['/account/inbox-ajax/accept-offer', 'id' => $this->id], [
                            'class' => 'btn-small offer-action',
                            'data-pjax' => 0,
                            'data-id' => $this->id,
                        ]) .
                        Html::a(Yii::t('order', 'Decline offer'), ['/account/inbox-ajax/decline-offer', 'id' => $this->id], [
                            'class' => 'btn-small red offer-action',
                            'data-pjax' => 0,
                        ]);
                }
                break;
            case self::STATUS_ACCEPTED:
                $balanceAllowed = Yii::$app->user->identity->getBalance() >= CurrencyHelper::convert($this->currency_code, Yii::$app->params['app_currency_code'], $this->price);
                $paymentMethods = PaymentMethod::getMethodsByCountry(Yii::$app->user->identity->id, $balanceAllowed);
                return Yii::$app->view->render('@frontend/modules/account/views/inbox/_offer_payment', [
                    'offer_id' => $this->id,
                    'paymentMethods' => $paymentMethods,
                    'total' => $this->price,
                    'currency_code' => $this->currency_code
                ]);
                break;
            case self::STATUS_DECLINED:
                return Html::a(Yii::t('order', 'Post request'), ['/tender/create'], [
                    'data-pjax' => 0,
                    'class' => 'btn-small'
                ]);
                break;
            case self::STATUS_PAID:
                $order_id = OrderProduct::find()
                    ->joinWith(['order'])
                    ->where(['product_id' => $this->id, 'product_type' => Order::TYPE_OFFER])
                    ->select('order.id')
                    ->scalar();
                return Html::a(Yii::t('order', 'Go to order'), ['/account/order/history', 'id' => $order_id], [
                    'data-pjax' => 0,
                    'class' => 'btn-small',
                ]);
                break;
            default :
//                return "<span class='btn-small disabled offer-action'>" . $this->getStatusLabel() . "</span>";
                return "";
                break;
        }
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $title = $this->subject ?? $this->description;

        return Yii::t('order', 'Paying deposit for individual job') . ": \"{$title}\"";
    }


    /**
     * interface implementations
     */

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return 'offer-' . $this->id;
    }

    /**
     * @param null $target
     * @param boolean $fromAws
     * @return string
     */
    public function getThumb($target = null, $fromAws = false)
    {
        return '/images/new/job-icon.png';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to([
            '/account/inbox/conversation',
            'id' => $this->message->conversation_id,
            '#' => 'message-' . $this->message_id
        ]);
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @return int|string
     */
    public function getSellerId()
    {
        return $this->from_id;
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return Url::to([
            '/account/profile/show',
            'id' => hasAccess($this->to_id) ? $this->from_id : $this->to_id,
        ]);
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return hasAccess($this->to_id) ? $this->from->getSellerName() : $this->to->getSellerName();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->currency_code = Yii::$app->params['app_currency_code'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->createStatusMessage();
    }

    /**
     * @return bool
     */
    public function createStatusMessage()
    {
        $message = new Message([
            'user_id' => $this->from_id,
            'conversation_id' => $this->message->conversation_id,
            'type' => $this->getStatusMessageType(),
            'message' => '.'
        ]);

        return $message->save();
    }

    /**
     * @return int
     */
    public function getStatusMessageType()
    {
        switch ($this->status) {
            case self::STATUS_CANCELED :
                return Message::TYPE_OFFER_CANCELED;
                break;
            case self::STATUS_DECLINED :
                return Message::TYPE_OFFER_DECLINED;
                break;
            case self::STATUS_ACCEPTED :
                return Message::TYPE_OFFER_ACCEPTED;
                break;
            case self::STATUS_PAID :
                return Message::TYPE_OFFER_PAID;
                break;
            default :
                return Message::TYPE_OFFER_SENT;
                break;
        }
    }

    /**
     * @param $offerId
     * @return bool
     */
    public static function hasAccess($offerId)
    {
        if (isGuest() === false) {
            return Offer::find()
                ->where(['or', ['to_id' => userId()], ['from_id' => userId()]])
                ->andWhere(['id' => $offerId])
                ->exists();
        }

        return false;
    }
}
