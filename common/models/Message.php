<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use common\models\user\Profile;
use common\models\user\User;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $conversation_id
 * @property string $status
 * @property integer $type
 * @property integer $starred
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $message
 *
 * @property User $user
 * @property Profile $profile
 * @property ConversationMember $anotherMember
 * @property Conversation $conversation
 * @property ConversationMember[] $conversationMembers
 * @property Attachment[] $attachments
 * @property Offer $offer
 *
 * @mixin LinkableBehavior
 */
class Message extends ActiveRecord
{
    const TYPE_MESSAGE = 0;
    const TYPE_OFFER_SENT = 10;
    const TYPE_OFFER_ACCEPTED = 20;
    const TYPE_OFFER_DECLINED = 30;
    const TYPE_OFFER_CANCELED = 40;
    const TYPE_OFFER_PAID = 50;
    const TYPE_GROUP_CHAT_INIT = 60;

    public $uploadedFiles;

    public $_anotherMember = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['offer'],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'conversation_id'], 'integer'],

            ['user_id', 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['conversation_id', 'exist', 'skipOnError' => true, 'targetClass' => Conversation::class, 'targetAttribute' => ['conversation_id' => 'id']],

            ['message', 'required'],
            ['message', 'string', 'max' => 5000],

            ['uploadedFiles',
                'file',
                'maxFiles' => 0,
                'maxSize' => 1024 * 1024 * 30,
                'tooBig' => Yii::t('app', 'The file "{file}" is too big. Its size cannot exceed 30MB.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'status' => Yii::t('model', 'Status'),
            'starred' => Yii::t('model', 'Starred'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'message' => Yii::t('model', 'Message'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['entity' => 'message']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offer::class, ['message_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversation()
    {
        return $this->hasOne(Conversation::class, ['id' => 'conversation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversationMembers()
    {
        return $this->hasMany(ConversationMember::class, ['conversation_id' => 'conversation_id'])->indexBy('user_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnotherMember()
    {
        if ($this->_anotherMember === null) {
            $members = $this->conversationMembers;
            unset($members[$this->user_id]);
            $this->_anotherMember = array_shift($members);
        }
        return $this->_anotherMember;
    }

    /**
     * @return string
     */
    public function getTypeIcon()
    {
        switch ($this->type) {
            case self::TYPE_OFFER_SENT :
                return 'osh-icon-horn';
                break;
            case self::TYPE_OFFER_ACCEPTED :
                return 'osh-icon-created';
                break;
            case self::TYPE_OFFER_CANCELED :
                return 'osh-icon-cancel';
                break;
            case self::TYPE_OFFER_DECLINED :
                return 'osh-icon-cancel';
                break;
            case self::TYPE_OFFER_PAID :
                return 'osh-icon-paid';
                break;
            default :
                return '';
                break;
        }
    }

    /**
     * @return string
     */
    public function getTypeView()
    {
        switch ($this->type) {
            case self::TYPE_MESSAGE:
            case self::TYPE_GROUP_CHAT_INIT:
                return 'message';
                break;
            default :
                return 'offer_progress';
                break;
        }
    }

    /**
     * @param $fromMe
     * @param $offerType
     * @return string
     */
    public function getTypeMessage($fromMe, $offerType)
    {
        switch ($this->type) {
            case self::TYPE_OFFER_SENT:
                return $fromMe
                    ? "Your " . Offer::$typeString[$offerType] . " has been sent"
                    : "You received " . strtolower(Offer::$typeString[$offerType]);
                break;
            case self::TYPE_OFFER_ACCEPTED:
                $string = $fromMe ? Offer::$receiverString[$offerType] : "You";
                $string .= " accepted";
                return $string;
                break;
            case self::TYPE_OFFER_DECLINED:
                $string = $fromMe ? Offer::$receiverString[$offerType] : "You";
                $string .= " declined";
                return $string;
                break;
            case self::TYPE_OFFER_CANCELED:
                $string = Offer::$senderString[$offerType];
                $string .= " canceled individual job ";
                $string .= strtolower(Offer::$typeString[$offerType]);
                return $string;
                break;
            case self::TYPE_OFFER_PAID:
                return (($offerType == Offer::TYPE_OFFER && $fromMe) || ($offerType == Offer::TYPE_REQUEST && !$fromMe) ? "Customer" : "You") . " made deposit";
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * @param $fromMe
     * @param $offerType
     * @return string
     */
    public function getTypeSubMessage($fromMe, $offerType)
    {
        switch ($this->type) {
            case self::TYPE_OFFER_SENT:
                return $fromMe
                    ? "Waiting for " . strtolower(Offer::$receiverString[$offerType]) . "`s response"
                    : "You need to accept or decline";
                break;
            case self::TYPE_OFFER_ACCEPTED:
                return ($offerType == Offer::TYPE_OFFER && $fromMe) || ($offerType == Offer::TYPE_REQUEST && !$fromMe)
                    ? "Waiting for customer to make deposit for work"
                    : "To get started, you need to make a deposit of {price}, which will be on your account until you accept the work. After that, the money will be transferred to the worker";
                break;
            case self::TYPE_OFFER_DECLINED:
                return ($offerType == Offer::TYPE_OFFER && $fromMe) || ($offerType == Offer::TYPE_REQUEST && !$fromMe)
                    ? "To simplify the customer search, you can create a job and more customers will see your offer"
                    : "To simplify the worker selection, you can create a request and more workers will see your request";
                break;
            case self::TYPE_OFFER_CANCELED:
                return '';
                break;
            case self::TYPE_OFFER_PAID:
                return "To continue conversation about this order click the button below";
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->created_at) {
            $this->created_at = Yii::$app->formatter->asDatetime($this->created_at, 'dd.MM.y HH:mm');
        }
        if ($this->updated_at) {
            $this->updated_at = Yii::$app->formatter->asDatetime($this->updated_at, 'dd.MM.y HH:mm');
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->message = HtmlPurifier::process($this->message);
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            if (!in_array($this->conversation->type, [Conversation::TYPE_GROUP, Conversation::TYPE_INFO], true)) {
                if (!empty($this->user->email) && $this->user->profile->email_notifications && (int)$this->type === self::TYPE_MESSAGE) {
                    $template = $this->offer === null ? '{sender} left you inbox message' : '{sender} sent you individual job ' . strtolower(Offer::$typeString[$this->offer->type]);
                    $message = Yii::t('notifications', $template, ['sender' => $this->user->getSellerName()], $this->anotherMember->user->site_language);

                    $notification = new Notification([
                        'to_id' => $this->anotherMember->user_id,
                        'from_id' => $this->user_id,
                        'params' => [
                            'subject' => Yii::t('notifications', 'New inbox message on {site}', ['site' => Yii::$app->name], $this->anotherMember->user->site_language),
                            'view' => 'inbox',
                            'content' => $this->message,
                            'heading' => $message,
                            'user2' => $this->user
                        ],
                        'custom_data' => json_encode([
                            'user' => $this->user->getSellerName()
                        ]),
                        'template' => Notification::TEMPLATE_NEW_INBOX_MESSAGE,
                        'linkRoute' => ['/account/inbox/conversation', 'id' => $this->conversation_id, '#' => 'message-' . $this->id],
                        'withEmail' => true,
                        'is_visible' => false
                    ]);

                    $notification->save();
                }
            }

            if ((boolean)$this->conversation->is_blank === true) {
                $this->conversation->updateAttributes(['is_blank' => false]);
                if (!isset($this->conversation->conversationMembers[Yii::$app->params['supportId']])) {
                    Event::addEvent(Event::CREATE_CONVERSATION);
                }
            }
        }
    }
}
