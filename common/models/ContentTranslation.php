<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "content_translation".
 *
 * @property integer $id
 * @property string $title
 * @property string $content_prev
 * @property string $content
 * @property string $entity
 * @property integer $entity_id
 * @property string $locale
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_robots
 * @property string $slug
 */
class ContentTranslation extends ActiveRecord
{
    const SCENARIO_ARTICLE = 'article';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_translation';
    }

    /**
     * @param $condition
     * @param $save bool
     * @return ContentTranslation
     */
    public static function findOrCreate($condition, $save = true)
    {
        $item = ContentTranslation::find()->where($condition)->one();
        if ($item === null) {
            $item = new ContentTranslation($condition);
            if ($save) {
                $item->save();
            }
        }

        return $item;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'locale'], 'required'],
            [['content'], 'string'],
            ['slug', 'string', 'max' => 55],
            [['entity_id'], 'integer'],
            [['title'], 'string', 'max' => 200],
            [['content_prev'], 'string', 'max' => 500],
            [['entity', 'seo_title', 'seo_keywords', 'seo_robots', 'seo_description'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 10],
            [['content'], 'required', 'on' => self::SCENARIO_ARTICLE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'content_prev' => Yii::t('model', 'Content Preview/Subtitle'),
            'content' => Yii::t('model', 'Content'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'locale' => Yii::t('model', 'Locale'),
            'seo_title' => Yii::t('model', 'Seo Title'),
            'seo_description' => Yii::t('model', 'Seo Description'),
            'seo_keywords' => Yii::t('model', 'Seo Keywords'),
            'seo_robots' => Yii::t('model', 'Seo Robots'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->title = HtmlPurifier::process($this->title);
            $this->seo_title = HtmlPurifier::process($this->seo_title);
            $this->content_prev = HtmlPurifier::process($this->content_prev);
            $this->content = HtmlPurifier::process($this->content);

            if (empty($this->slug) && $this->entity === 'page') {
                $this->slug = Yii::$app->utility->generateSlug($this->title);
            }
            return true;
        }

        return false;
    }
}
