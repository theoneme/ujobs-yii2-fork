<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_import".
 *
 * @property int $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $source
 * @property string $ip
 * @property int $date_added
 * @property int $mail_sent
 */
class EmailImport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_import';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_added'], 'integer'],
            [['email', 'first_name', 'last_name', 'source', 'ip'], 'string', 'max' => 255],
            [['mail_sent'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'source' => 'Source',
            'ip' => 'Ip',
            'date_added' => 'Date Added',
            'mail_sent' => 'Mail Sent',
        ];
    }
}
