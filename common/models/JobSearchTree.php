<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "job_search_tree".
 *
 * @property integer $id
 * @property string $name
 *
 * @property JobSearchStep[] $jobSearchSteps
 * @property JobSearchStep[] $jobSearchRootSteps
 * @property JobSearchTreeToEntity[] $entities
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 *
 * @mixin LinkableBehavior
 */
class JobSearchTree extends ActiveRecord
{
    /**
     * @var string
     */
    public $entityData;

    /**
     * @var array
     */
    public $translationsData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_search_tree';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'entities'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['entityData', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'name' => Yii::t('model', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobSearchSteps()
    {
        return $this->hasMany(JobSearchStep::class, ['tree_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobSearchRootSteps()
    {
        return $this->hasMany(JobSearchStep::class, ['tree_id' => 'id'])->andOnCondition(['job_search_step.previous_step_id' => null]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(JobSearchTreeToEntity::class, ['tree_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jst_ct.entity' => 'job_search_tree'])->from('content_translation jst_ct')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jst_ct.entity' => 'job_search_tree'])->from('content_translation jst_ct')->andWhere(['jst_ct.locale' => Yii::$app->language]);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->translation->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translation->content;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->entityData)) {
            $entityData = explode(',', $this->entityData);
            foreach ($entityData as $item) {
                list($entity, $entityContent) = explode('-', $item, 2);
                $this->bind('entities')->attributes = ['entity' => $entity, 'entity_content' => $entityContent];
            }
        }
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'job_search_tree';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        ContentTranslation::deleteAll(['entity' => 'job_search_tree', 'entity_id' => $this->id]);
        ContentTranslation::deleteAll(['entity' => 'job_search_step', 'entity_id' => array_map(function($var){return $var->id;}, $this->jobSearchSteps)]);
        $optionIds = array_reduce(
            array_map(function($var){
                return array_map(function($var2){
                    return $var2->id;
                }, $var->options);
            }, $this->jobSearchSteps),
            'array_merge',
            []
        );
        ContentTranslation::deleteAll(['entity' => 'job_search_option', 'entity_id' => $optionIds]);
        Attachment::deleteAll(['entity' => 'job_search_option', 'entity_id' => $optionIds]);
        return parent::beforeDelete();
    }
}
