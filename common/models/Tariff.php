<?php

namespace common\models;

use common\components\CurrencyHelper;
use Yii;

/**
 * This is the model class for table "tariff".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $cost
 * @property string $currency_code
 * @property string $description
 * @property string $icon
 *
 * @property TariffRule[] $tariffRules
 */
class Tariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * @param $price
     * @param $days
     * @return string
     */
    public static function calculatePricePerDay($price, $days)
    {
        $ratio = (Yii::$app->params['app_currency_code'] === 'RUB' ? 1 : CurrencyHelper::getRatios()[Yii::$app->params['app_currency_code']]['RUB']);
        return round($price * (1 - Tariff::getDiscount($days)) / $ratio, 2);
    }

    /**
     * @param $quantity
     * @return float|int
     */
    public static function getDiscount($quantity)
    {
        if ($quantity >= 360) {
            return 0.5;
        } else if ($quantity >= 180) {
            return 0.3;
        } else if ($quantity >= 90) {
            return 0.1;
        } else {
            return 0;
        }
    }

    /**
     * @param $price
     * @param $days
     * @return string
     */
    public static function calculateTotalPrice($price, $days)
    {
        return CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $price * $days * (1 - Tariff::getDiscount($days)));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'cost', 'currency_code'], 'required'],
            [['created_at', 'updated_at', 'cost'], 'integer'],
            [['title'], 'string', 'max' => 75],
            [['description'], 'string', 'max' => 255],
            [['currency_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'cost' => Yii::t('model', 'Cost'),
            'currency_code' => Yii::t('model', 'Currency Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffRules()
    {
        return $this->hasMany(TariffRule::class, ['tariff_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        $ratio = (Yii::$app->params['app_currency_code'] === 'RUB' ? 1 : CurrencyHelper::getRatios()[Yii::$app->params['app_currency_code']]['RUB']);
        return CurrencyHelper::format(Yii::$app->params['app_currency_code'], round($this->cost / $ratio, 2), 2);
    }
}
