<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property integer $parent_id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_approved
 */
class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    public function behaviors()
    {
        $behaviors = [TimestampBehavior::class];
        $behaviors[] = [
            'class' => BlameableBehavior::class,
            'createdByAttribute' => 'user_id',
            'updatedByAttribute' => false,
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entity_id', 'name', 'email'], 'required'],
            [['entity_id', 'parent_id', 'user_id', 'created_at', 'updated_at', 'is_approved'], 'integer'],
            [['comment'], 'string'],
            [['entity'], 'string', 'max' => 55],
            [['name', 'email'], 'string', 'max' => 75],

            [['user_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'parent_id' => Yii::t('model', 'Parent ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'name' => Yii::t('model', 'Name'),
            'email' => Yii::t('model', 'Email'),
            'comment' => Yii::t('model', 'Comment'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'is_approved' => Yii::t('model', 'Is Approved'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->comment = HtmlPurifier::process($this->comment);
            $this->name = HtmlPurifier::process($this->name);
            $this->email = HtmlPurifier::process($this->email);

            return true;
        }

        return false;
    }
}
