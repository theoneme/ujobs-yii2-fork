<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 16:18
 */

namespace common\models;

use common\models\user\User;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "user_withdrawal".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $amount
 * @property string $currency_code
 * @property integer $target_method
 * @property string $target_account
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserWithdrawal extends ActiveRecord
{
    const STATUS_WAITING = 0;
    const STATUS_DECLINED = 1;
    const STATUS_APPROVED = 2;
    const STATUS_COMPLETED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_withdrawal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['user_id', 'amount', 'target_method', 'status', 'created_at', 'updated_at'], 'integer'],
            [['target_account'], 'string', 'max' => 155],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_WAITING]
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'amount' => Yii::t('model', 'Amount'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'target_method' => Yii::t('model', 'Target Method'),
            'target_account' => Yii::t('model', 'Target Account'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @param bool|false $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_WAITING => ($colored ? Html::tag('div', Yii::t('labels', 'Waiting'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Waiting')),
            self::STATUS_APPROVED => ($colored ? Html::tag('div', Yii::t('labels', 'Approved'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Approved')),
            self::STATUS_DECLINED => ($colored ? Html::tag('div', Yii::t('labels', 'Declined'), ['class' => 'btn btn-danger btn-xs']) : Yii::t('labels', 'Declined')),
            self::STATUS_COMPLETED => ($colored ? Html::tag('div', Yii::t('labels', 'Completed'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Completed')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
//            $this->currency_code = Yii::$app->params['app_currency_code'];
//            $this->currency_code = 'RUB';
        }
        if ($this->getOldAttribute('status') != self::STATUS_COMPLETED && $this->status == self::STATUS_COMPLETED) {
            if ($this->amount > $this->user->wallets[$this->currency_code]->balance) {
                Yii::$app->session->setFlash('warning', Yii::t('account', 'Not enough money on your balance for withdrawal'));
                return false;
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (isset($changedAttributes['status']) && ($changedAttributes['status'] != self::STATUS_COMPLETED && $this->status == self::STATUS_COMPLETED)) {
            $sameWithdrawal = PaymentHistory::find()->where([
                'user_id' => $this->user_id,
                'type' => PaymentHistory::TYPE_OPERATION_WITHDRAW,
                'amount' => $this->amount,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS
            ])->one();
            if (!$sameWithdrawal) {
                $paymentHistory = new PaymentHistory();
                $paymentHistory->user_id = $this->user_id;
                $paymentHistory->amount = $this->amount;
                $paymentHistory->type = PaymentHistory::TYPE_OPERATION_WITHDRAW;
                $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_NEG;
                $paymentHistory->template = PaymentHistory::TEMPLATE_PAYMENT_MONEY_WITHDRAWAL;
                $paymentHistory->currency_code = $this->currency_code;
                $paymentHistory->save();
            }
        }
    }
}