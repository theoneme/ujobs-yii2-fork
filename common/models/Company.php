<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\models\forms\BaseAttributeForm;
use common\models\user\User;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\services\GoogleMapsService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property int $user_id
 * @property string $logo
 * @property string $banner
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $lat
 * @property string $long
 * @property string $status_text
 *
 * @property User $user
 * @property CompanyMember[] $companyMembers
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property UserAttribute[] $userAttributes
 *
 * @mixin LinkableBehavior
 */
class Company extends ActiveRecord
{
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_DISABLED = 20;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    /**
     * @var string
     */
    private $_address = null;

    /**
     * @var array
     */
    public $companyActivities = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'logo' => [
                'class' => ImageBehavior::class,
                'folder' => 'company',
                'imageField' => 'logo'
            ],
            'banner' => [
                'class' => ImageBehavior::class,
                'folder' => 'company',
                'imageField' => 'banner'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['logo', 'banner'], 'string', 'max' => 255],
            ['status_text', 'string', 'max' => 155],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['lat', 'long'], 'string', 'max' => 40],
            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'logo' => Yii::t('model', 'Logo'),
            'banner' => Yii::t('model', 'Banner'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMembers()
    {
        return $this->hasMany(CompanyMember::class, ['company_id' => 'id'])->indexBy('user_id');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andWhere(['content_translation.entity' => 'company'])->andWhere(['content_translation.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['content_translation.entity' => 'company'])->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAttributes()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'user_id']);
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->logo, $target);
    }

    /**
     * @param string $target
     * @return string
     */
    public function getBanner($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->banner, $target);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert === true) {
            $fakeUser = new User([
                'email' => time() . uniqid() . "@fake.cake",
                'is_company' => true,
                'phone' => "",
                'password_hash' => crypt(time() . "@fake.cake", time() . "@fake.cake")
            ]);

            if($fakeUser->save() === true) {
                $companyMember = new CompanyMember([
                    'company_id' => $this->id,
                    'user_id' => userId(),
                    'role' => CompanyMember::ROLE_OWNER,
                    'status' => CompanyMember::STATUS_ACTIVE
                ]);

                if($companyMember->save() === true) {
                    $this->updateAttributes(['user_id' => $fakeUser->id]);
                }
            }
        }
        if (is_array($this->companyActivities)) {
            UserAttribute::deleteAll(['user_id' => $this->user_id, 'entity_alias' => 'field-of-activity']);
            $attributeId = Attribute::find()->where(['alias' => 'field-of-activity'])->select('id')->scalar();
            foreach ($this->companyActivities as $activity) {
                $attributeObject = new UserAttribute(['user_id' => $this->user_id]);
                $attributeObject->setAttributes(
                    array_filter(
                        AttributeValueObjectBuilder::makeAttributeObject(UserAttribute::class, $attributeId, $activity, ['locale' => Yii::$app->language])->attributes
                    )
                );
                $attributeObject->save();
            }
        }
    }

    /**
     * @param $companyId
     * @return bool
     */
    public static function hasAccess($companyId)
    {
        return Yii::$app->user->isGuest
            ? false
            : CompanyMember::find()->where(['company_id' => $companyId, 'user_id' => userId(), 'role' => CompanyMember::ROLE_OWNER])->exists();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0] ?? null;
        if($translation !== null) {
            return $translation->title;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getAddress()
    {
        if ($this->_address === null && $this->lat && $this->long) {
            /* @var $addressTranslation AddressTranslation */
            $addressTranslation = AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->one();
            if ($addressTranslation !== null) {
                $this->_address = $addressTranslation->title;
            } else {
                $gmapsService = new GoogleMapsService();
                $address = $gmapsService->reverseGeocode($this->lat, $this->long, Yii::$app->params['supportedLocales'][Yii::$app->language]);
                if ($address !== false) {
                    (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $address]))->save();
                    $this->_address = $address;
                }
            }
        }
        return $this->_address;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0] ?? null;
        if($translation !== null) {
            return $translation->title;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0] ?? null;
        if($translation !== null) {
            return $translation->content;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        $translation = $this->translations[Yii::$app->language] ?? null;
        if($translation !== null) {
            return !empty($translation->content_prev) ? $translation->content_prev : strip_tags(StringHelper::truncate($translation->content, 200));
        }

        return null;
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $templateCategory = SeoAdvanced::TEMPLATE_COMPANY;
        $templates = SeoAdvanced::getDefaultTemplates($templateCategory);
        $params = [
            'title' => $this->user->getSellerName()
        ];

        return [
            'title' => Yii::t('seo', $templates['title'], $params, Yii::$app->language),
            'description' => Yii::t('seo', $templates['description'], $params, Yii::$app->language),
            'keywords' => ''
        ];
    }
}
