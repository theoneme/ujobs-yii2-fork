<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "support_request".
 *
 * @property integer $id
 * @property integer $created_by
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property integer $status
 * @property integer $type
 * @property string $description
 * @property integer $created_at
 *
 * @property User $createdBy
 * @property Attachment[] $attachments
 */
class SupportRequest extends ActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_RESOLVED = 10;
    const STATUS_CANCELED = 20;

    const TYPE_ACCOUNT = 0;
    const TYPE_JOB = 10;
    const TYPE_ORDER = 20;
    const TYPE_FEATURE = 30;
    const TYPE_BUG = 40;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'support_request';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'description'], 'required'],

            [['description'], 'string', 'max' => 65535],
            [['name', 'email', 'subject'], 'string', 'max' => 255],

            ['email', 'email'],

            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => [self::STATUS_RESOLVED, self::STATUS_CANCELED, self::STATUS_PENDING]],

            ['type', 'in', 'range' => [self::TYPE_ACCOUNT, self::TYPE_JOB, self::TYPE_ORDER, self::TYPE_FEATURE, self::TYPE_BUG]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'created_by' => Yii::t('model', 'Created By'),
            'name' => Yii::t('model', 'Name'),
            'email' => Yii::t('model', 'Email'),
            'subject' => Yii::t('model', 'Subject'),
            'status' => Yii::t('model', 'Status'),
            'type' => Yii::t('model', 'Type'),
            'description' => Yii::t('model', 'Description'),
            'created_at' => Yii::t('model', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andWhere(['entity' => 'support_request']);
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_PENDING => Yii::t('model', 'Pending'),
            self::STATUS_RESOLVED => Yii::t('model', 'Resolved'),
            self::STATUS_CANCELED => Yii::t('model', 'Canceled'),
        ];
    }

    /**
     * @return string
     */
    public function getTypeLabel()
    {
        $labels = static::getTypeLabels();
        return isset($labels[$this->type]) ? $labels[$this->type] : Yii::t('model', 'Type not selected');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            self::TYPE_ACCOUNT => Yii::t('model', 'Account Support'),
            self::TYPE_JOB => Yii::t('model', 'Job Support'),
            self::TYPE_ORDER => Yii::t('model', 'Order Support'),
            self::TYPE_FEATURE => Yii::t('model', 'Feature Support'),
            self::TYPE_BUG => Yii::t('model', 'Report a Bug'),
        ];
    }
}
