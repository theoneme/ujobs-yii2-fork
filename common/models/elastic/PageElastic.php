<?php

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;
use common\components\Utility;
use Yii;
use yii\helpers\Url;


/**
 *
 * @property integer $id
 * @property string $alias
 * @property string $image
 * @property string $banner
 * @property string $banner_small
 * @property integer $publish_date
 * @property integer $update_date
 * @property integer $views
 * @property integer $category_id
 * @property integer $user_id
 * @property string $layout
 * @property string $template
 * @property string $type
 *
 * @property array $translation
 * @property array $profileTranslation
 * @property array $translations
 * @property array $likes
 * @property array $lang
 * @property array $user
 * @property array $tariff
 */
class PageElastic extends ActiveRecord
{
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    const TYPE_DEFAULT = 'default-page';
    const TYPE_ARTICLE = 'article-page';

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'page';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'user_id' => ['type' => 'long'],
                    'category_id' => ['type' => 'long'],
                    'publish_date' => ['type' => 'long'],
                    'update_date' => ['type' => 'long'],
                    'views' => ['type' => 'long'],
                    'status' => ['type' => 'long'],
                    'alias' => ['type' => 'string'],
                    'image' => ['type' => 'string'],
                    'type' => ['type' => 'string'],
                    'translations' => [
                        'type' => 'nested',
                        'properties' => [
                            'title' => ['type' => 'string'],
                            'description' => ['type' => 'string'],
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'slug' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'category' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'title' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed']
                        ]
                    ],
                    'attrs' => [
                        'type' => 'nested',
                        'properties' => [
                            'attribute_id' => ['type' => 'long'],
                            'value' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'title' => ['type' => 'string'],
                            'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'user' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'username' => ['type' => 'string'],
                            'is_company' => ['type' => 'boolean'],
                            'company_id' => ['type' => 'long']
                        ]
                    ],
                    'profile' => [
                        'type' => 'object',
                        'properties' => [
                            'user_id' => ['type' => 'long'],
                            'name' => ['type' => 'string'],
                            'gravatar_email' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'status' => ['type' => 'long'],
                            'rating' => ['type' => 'long'],
                            'translations' => [
                                'type' => 'nested',
                                'properties' => [
                                    'title' => ['type' => 'string'],
                                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                                ]
                            ]
                        ]
                    ],
                    'tariff' => [
                        'type' => 'object',
                        'properties' => [
                            'price' => ['type' => 'long'],
                            'icon' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'expires_at' => ['type' => 'long']
                        ]
                    ],
                    'likes' => [
                        'type' => 'nested',
                        'properties' => [
                            'name' => ['type' => 'string'],
                            'avatar' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'lang' => [
                        'type' => 'nested',
                        'properties' => [
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        static::getDb()->createCommand()->deleteIndex(static::index());
    }

    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'user_id',
            'category_id',
            'publish_date',
            'update_date',
            'views',
            'status',
            'type',
            'alias',
            'image',
            'translations',
            'lang',
            'category',
            'attrs',
            'user',
            'profile',
            'tariff',
            'likes',
            'lang'
        ];
    }

    /**
     * @return string
     */
    public function getTranslation()
    {
        $t = null;
        foreach ($this->translations as $translation) {
            if ($translation['locale'] === Yii::$app->language) {
                $t = $translation;
                break;
            }
        }

        return $t ?? array_values($this->translations)[0];
    }

    /**
     * @return string
     */
    public function getProfileTranslation()
    {
        $t = null;
        foreach ($this->profile['translations'] as $translation) {
            if ($translation['locale'] === Yii::$app->language) {
                $t = $translation;
                break;
            }
        }

        return $t ?? array_values($this->profile['translations'])[0];
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        /* @var Utility $utility */
        $utility = Yii::$app->get('utility');

        return $utility->upperFirstLetter($this->translation['title']);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->translation['description'];
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($this->image && file_exists(Yii::getAlias("@frontend") . "/web" . $this->image)) {
            $image = $this->image;
        }

        $width = $height = null;

        if ($target != null && Yii::$app->params['images'][$target] != null) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @param string $target
     * @return string
     */
    public function getSellerThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->profile['gravatar_email'], $target);
    }

    /**
     * @param $userIndex
     * @param null $target
     * @return mixed
     */
    public function getLikeUserThumb($userIndex, $target = null)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($this->likes[$userIndex]['avatar'] && file_exists(Yii::getAlias("@frontend") . "/web" . $this->likes[$userIndex]['avatar'])) {
            $image = $this->likes[$userIndex]['avatar'];
        }

        $width = $height = null;

        if ($target != null && Yii::$app->params['images'][$target] != null) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @param $userIndex
     * @return string
     */
    public function getLikeSellerName($userIndex)
    {
        return $this->likes[$userIndex]['name'];
    }

    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(["/article/view", 'alias' => !empty($this->translation['slug']) ? $this->translation['slug'] : $this->alias]);
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return (boolean)$this->user['is_company'] === true
            ? Url::to(['/company/profile/show', 'id' => $this->user['company_id']])
            : Url::to(['/account/profile/show', 'id' => $this->user_id]);
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return Yii::$app->utility->upperFirstLetter(
            $this->profileTranslation['title'] ?? ($this->profile['name'] ? $this->profile['name'] : $this->user['username'])
        );
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->tariff['expires_at'] && $this->tariff['expires_at'] > time()) {
            return $this->tariff['icon'];
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->tariff['expires_at'] && $this->tariff['expires_at'] > time()) {
            return $this->tariff['title'];
        }

        return null;
    }
}
