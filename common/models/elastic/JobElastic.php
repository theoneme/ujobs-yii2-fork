<?php

namespace common\models\elastic;

use common\components\CurrencyHelper;
use common\components\elasticsearch\ActiveRecord;
use common\components\Utility;
use common\models\interfaces\HighlightInterface;
use common\models\JobPackage;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $category_id
 * @property integer $date_start
 * @property integer $date_end
 * @property integer execution_time
 * @property string $status
 * @property integer $price
 * @property boolean $contract_price
 * @property integer $currency_code
 * @property string $place
 * @property string $alias
 * @property string $requirements
 * @property integer $click_count
 * @property integer $views_count
 * @property integer $orders_count
 * @property integer $cancellations_count
 * @property integer $rating
 * @property string $type
 * @property integer $percent_bonus
 * @property integer $count_orders
 *
 * @property array $translation
 * @property array $packages
 * @property array $category
 * @property array $attachments
 * @property array $attrs
 * @property array $user
 * @property array $profile
 * @property array $tariff
 */
class JobElastic extends ActiveRecord implements HighlightInterface
{
    const STATUS_DRAFT = -50;
    const STATUS_DENIED = -10;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    const TYPE_JOB = 'job';
    const TYPE_TENDER = 'tender';
    const TYPE_ADVANCED_TENDER = 'advanced-tender';

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'job';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'user_id' => ['type' => 'long'],
                    'created_at' => ['type' => 'long'],
                    'updated_at' => ['type' => 'long'],
                    'status' => ['type' => 'long'],
                    'root_score' => ['type' => 'long'],
                    'upper_category_score' => ['type' => 'long'],
                    'category_score' => ['type' => 'long'],
                    'contract_price' => ['type' => 'boolean'],
                    'price' => ['type' => 'long'],
                    'currency_code' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'default_price' => ['type' => 'long'],
                    'category_id' => ['type' => 'long'],
                    'alias' => ['type' => 'string'],
                    'type' => ['type' => 'string'],
                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'count_orders' => ['type' => 'integer'],
                    'translation' => [
                        'type' => 'nested',
                        'properties' => [
                            'title' => ['type' => 'string'],
                            'description' => ['type' => 'string'],
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed']
                        ]
                    ],
                    'lang' => [
                        'type' => 'nested',
                        'properties' => [
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'category' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attachments' => [
                        'type' => 'nested',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'content' => ['type' => 'string', 'index' => 'not_analyzed']
                        ]
                    ],
                    'packages' => [
                        'type' => 'nested',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'title' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'description' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'price' => ['type' => 'long'],
                            'currency_code' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'included' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'excluded' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'type' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attrs' => [
                        'type' => 'nested',
                        'properties' => [
                            'attribute_id' => ['type' => 'long'],
                            'value' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'title' => ['type' => 'string'],
                            'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'user' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'username' => ['type' => 'string'],
                            'is_company' => ['type' => 'boolean'],
                            'company_id' => ['type' => 'long']
                        ]
                    ],
                    'profile' => [
                        'type' => 'object',
                        'properties' => [
                            'user_id' => ['type' => 'long'],
                            'name' => ['type' => 'string'],
                            'gravatar_email' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'status' => ['type' => 'long'],
                            'rating' => ['type' => 'long'],
                            'translations' => [
                                'type' => 'nested',
                                'properties' => [
                                    'title' => ['type' => 'string'],
                                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                                ]
                            ]
                        ]
                    ],
                    'orders' => [
                        'type' => 'nested',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'status' => ['type' => 'long'],
                        ]
                    ],
                    'tariff' => [
                        'type' => 'object',
                        'properties' => [
                            'price' => ['type' => 'long'],
                            'icon' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'expires_at' => ['type' => 'long']
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DENIED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Denied') . '</span>' : Yii::t('labels', 'Denied')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DRAFT => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Draft') . '</span>' : Yii::t('labels', 'Draft')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'user_id',
            'created_at',
            'updated_at',
            'status',
            'root_score',
            'upper_category_score',
            'category_score',
            'contract_price',
            'price',
            'currency_code',
            'default_price',
            'locale',
            'count_orders',
            'category_id',
            'alias',
            'type',
            'rating',
            'translation',
            'category',
            'attachments',
            'packages',
            'attrs',
            'extra',
            'user',
            'profile',
            'qa',
            'orders',
            'tariff',
            'lang'
        ];
    }

    /**
     * @return bool
     */
    public function isTenderNew()
    {
        return in_array($this->type, [self::TYPE_TENDER, self::TYPE_ADVANCED_TENDER]) && $this->created_at > (time() - 60 * 60 * 24 * 2);
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->attachments[0]['content'] ?? null, $target);
    }

    /**
     * @return int
     */
    public function getActualPackagesCount()
    {
        $count = 0;
        $packages = $this->packages;
        if ($packages) {
            /* @var JobPackage $package */
            foreach ($packages as $package) {
                if ($package['id']) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * interface implementations
     */

    /**
     * @return int|string
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        /* @var Utility $utility */
        $utility = Yii::$app->get('utility');

        return $utility->upperFirstLetter($this->translation['title']);
    }

    /**
     * @return string
     */
    public function getProfileTranslation()
    {
        $t = null;
        foreach ($this->profile['translations'] as $translation) {
            if ($translation['locale'] === Yii::$app->language) {
                $t = $translation;
                break;
            }
        }

        return $t ?? array_values($this->profile['translations'])[0];
    }

    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translation['content'];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $type = $this->type == self::TYPE_ADVANCED_TENDER ? 'tender' : $this->type;
        return Url::to(["/{$type}/view", 'alias' => $this->alias]);
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @return int
     */
    public function getSellerId()
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return isset($this->user['is_company']) && (boolean)$this->user['is_company'] === true
            ? Url::to(['/company/profile/show', 'id' => $this->user['company_id']])
            : Url::to(['/account/profile/show', 'id' => $this->user_id]);
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return Yii::$app->utility->upperFirstLetter(
            $this->profileTranslation['title'] ?? ($this->profile['name'] ? $this->profile['name'] : $this->user['username'])
        );
    }

    /**
     * @param null $target
     * @return mixed
     */
    public function getSellerThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->profile['gravatar_email'], $target);
    }

    /**
     * @return array|bool
     */
    public function isFavourite()
    {
        return !isGuest() && isset(Yii::$app->user->identity->favourites[$this->id]);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->tariff['expires_at'] && $this->tariff['expires_at'] > time()) {
            return $this->tariff['icon'];
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->tariff['expires_at'] && $this->tariff['expires_at'] > time()) {
            return $this->tariff['title'];
        }

        return null;
    }
}
