<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.04.2017
 * Time: 11:46
 */

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;

/**
 * This is the model class for table "job_attribute".
 *
 * @property integer $id
 * @property string $attribute_id
 * @property integer $job_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property array $job
 * @property array $attribute
 * @property array $attributeValue
 */
class JobAttributeElastic extends ActiveRecord
{
    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'job_attribute';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'attribute_id' => ['type' => 'integer'],
                    'value' => ['type' => 'string'],
                    'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'job' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'category_id' => ['type' => 'long'],
                            'status' => ['type' => 'long'],
                            'type' => ['type' => 'string'],
                            'currency_code' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'default_price' => ['type' => 'long'],
                            'price' => ['type' => 'long'],
                            'title' => ['type' => 'string'],
                            'description' => ['type' => 'string']
                        ]
                    ],
                    'attribute' => [
                        'type' => 'object',
                        'properties' => [
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attributeValue' => [
                        'type' => 'object',
                        'properties' => [
                            'status' => ['type' => 'long'],
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'attribute_id',
            'value',
            'entity_alias',
            'value_alias',
            'locale',
            'job',
            'attribute',
            'attributeValue'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }
}
