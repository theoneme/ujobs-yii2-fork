<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.02.2017
 * Time: 17:44
 */

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;

/**
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $user_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property array $user
 * @property array $attribute
 * @property array $attributeValue
 * @property array $categories
 */
class UserAttributeElastic extends ActiveRecord
{
    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'user_attribute';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'attribute_id' => ['type' => 'integer'],
                    'value' => ['type' => 'string'],
                    'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'user' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'status' => ['type' => 'long'],
                        ]
                    ],
                    'attribute' => [
                        'type' => 'object',
                        'properties' => [
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attributeValue' => [
                        'type' => 'object',
                        'properties' => [
                            'status' => ['type' => 'long'],
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe']
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'attribute_id',
            'value',
            'entity_alias',
            'value_alias',
            'user',
            'attribute',
            'attributeValue',
            'categories'
        ];
    }
}
