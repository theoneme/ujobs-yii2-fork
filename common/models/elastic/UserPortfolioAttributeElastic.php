<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.09.2017
 * Time: 16:39
 */

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;

/**
 * Class UserPortfolioAttributeElastic
 * @package common\models\elastic
 *
 * @property array $portfolio
 * @property array $attribute
 * @property array $attributeValue
 */
class UserPortfolioAttributeElastic extends ActiveRecord
{
    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'user_portfolio_attribute';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'attribute_id' => ['type' => 'integer'],
                    'value' => ['type' => 'string'],
                    'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'portfolio' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'category_id' => ['type' => 'long'],
                            'status' => ['type' => 'long'],
                            'title' => ['type' => 'string'],
                            'description' => ['type' => 'string']
                        ]
                    ],
                    'attribute' => [
                        'type' => 'object',
                        'properties' => [
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attributeValue' => [
                        'type' => 'object',
                        'properties' => [
                            'status' => ['type' => 'long'],
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'attribute_id',
            'value',
            'entity_alias',
            'value_alias',
            'locale',
            'portfolio',
            'attribute',
            'attributeValue'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }
}
