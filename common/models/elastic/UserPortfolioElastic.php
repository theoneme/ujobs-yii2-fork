<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.09.2017
 * Time: 16:39
 */

namespace common\models\elastic;

use Yii;
use common\components\elasticsearch\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Class UserPortfolioElastic
 * @package common\models\elastic
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property string $image
 * @property integer $category_id
 * @property integer $version
 * @property string $title
 * @property string $description
 * @property integer $likes_count
 * @property integer $views_count
 * @property string $pictures
 * @property integer $
 *
 * @property array $category
 * @property array $attachments
 * @property array $attrs
 * @property array $user
 * @property array $profile
 * @property array $tariff
 * @property array $likes
 */

class UserPortfolioElastic extends ActiveRecord
{
    const STATUS_DRAFT = -50;
    const STATUS_DENIED = -10;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'user_portfolio';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'user_id' => ['type' => 'long'],
                    'created_at' => ['type' => 'long'],
                    'updated_at' => ['type' => 'long'],
                    'status' => ['type' => 'long'],
                    'image' => ['type' => 'string'],
                    'category_id' => ['type' => 'long'],
                    'version' => ['type' => 'long'],
                    'title' => ['type' => 'string'],
                    'description' => ['type' => 'string'],
                    'likes_count' => ['type' => 'long'],
                    'views_count' => ['type' => 'long'],
                    'root_score' => ['type' => 'long'],
                    'upper_category_score' => ['type' => 'long'],
                    'category_score' => ['type' => 'long'],
                    'pictures' => ['type' => 'long'],
                    'category' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'title' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'type' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'lang' => [
                        'type' => 'nested',
                        'properties' => [
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'likes' => [
                        'type' => 'nested',
                        'properties' => [
                            'name' => ['type' => 'string'],
                            'avatar' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attachments' => [
                        'type' => 'nested',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'content' => ['type' => 'string', 'index' => 'not_analyzed']
                        ]
                    ],
                    'attrs' => [
                        'type' => 'nested',
                        'properties' => [
                            'attribute_id' => ['type' => 'long'],
                            'value' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'is_text' => ['type' => 'boolean'],
                            'title' => ['type' => 'string'],
                            'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'user' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'username' => ['type' => 'string'],
                        ]
                    ],
                    'profile' => [
                        'type' => 'object',
                        'properties' => [
                            'user_id' => ['type' => 'long'],
                            'name' => ['type' => 'string'],
                            'gravatar_email' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'status' => ['type' => 'long'],
                            'rating' => ['type' => 'long'],
                            'translations' => [
                                'type' => 'nested',
                                'properties' => [
                                    'title' => ['type' => 'string'],
                                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                                ]
                            ]
                        ]
                    ],
                    'tariff' => [
                        'type' => 'object',
                        'properties' => [
                            'price' => ['type' => 'long'],
                            'icon' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'expires_at' => ['type' => 'long']
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DENIED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Denied') . '</span>' : Yii::t('labels', 'Denied')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DRAFT => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Draft') . '</span>' : Yii::t('labels', 'Draft')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'user_id',
            'created_at',
            'updated_at',
            'status',
            'version',
            'title',
            'likes_count',
            'views_count',
            'root_score',
            'upper_category_score',
            'category_score',
            'pictures',
            'description',
            'image',
            'category_id',
            'translation',
            'category',
            'attachments',
            'attrs',
            'likes',
            'user',
            'profile',
            'tariff',
            'lang'
        ];
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->image, $target);
    }

    /**
     * @param $userIndex
     * @param null $target
     * @return mixed
     */
    public function getLikeUserThumb($userIndex, $target = null)
    {
        return Yii::$app->mediaLayer->getThumb(
            isset($this->likes[$userIndex]['avatar']) ? $this->likes[$userIndex]['avatar'] : null,
            $target
        );
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return Url::to(['/account/profile/show', 'id' => $this->user_id]);
    }

    /**
     * @return string
     */
    public function getProfileTranslation()
    {
        $t = null;
        foreach($this->profile['translations'] as $translation) {
            if($translation['locale'] === Yii::$app->language) {
                $t = $translation;
                break;
            }
        }

        return $t ?? array_values($this->profile['translations'])[0];
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return Yii::$app->utility->upperFirstLetter(
            Html::encode(
                $this->profileTranslation['title'] ?? ($this->profile['name'] ? $this->profile['name'] : $this->user['username'])
            )
        );
    }

    /**
     * @param null $target
     * @return mixed
     */
    public function getSellerThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->profile['gravatar_email'], $target);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->tariff['expires_at'] && $this->tariff['expires_at'] > time()) {
            return $this->tariff['icon'];
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->tariff['expires_at'] && $this->tariff['expires_at'] > time()) {
            return $this->tariff['title'];
        }

        return null;
    }
}
