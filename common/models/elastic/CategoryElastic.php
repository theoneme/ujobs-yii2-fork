<?php

namespace common\models\elastic;

use common\components\CurrencyHelper;
use common\components\elasticsearch\ActiveRecord;
use common\components\Utility;
use common\models\interfaces\HighlightInterface;
use common\models\JobPackage;
use Yii;
use yii\helpers\Url;

/**
 * Class CategoryElastic
 * @package common\models\elastic
 */
class CategoryElastic extends ActiveRecord
{
    public const JOB_CATEGORY = 0;
    public const PRODUCT_CATEGORY = 10;

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'category';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'lvl' => ['type' => 'long'],
                    'root' => ['type' => 'long'],
                    'lft' => ['type' => 'long'],
                    'rgt' => ['type' => 'long'],
                    'category_type' => ['type' => 'long'],
                    'translations' => [
                        'type' => 'nested',
                        'properties' => [
                            'title' => ['type' => 'string'],
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed']
                        ]
                    ],
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'lft',
            'rgt',
            'root',
            'lvl',
            'category_type',
            'translations',
        ];
    }
}
