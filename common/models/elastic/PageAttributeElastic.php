<?php

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $attribute_id
 * @property integer $page_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property array $page
 * @property array $attribute
 * @property array $attributeValue
 */
class PageAttributeElastic extends ActiveRecord
{
    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'page_attribute';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'attribute_id' => ['type' => 'integer'],
                    'value' => ['type' => 'string'],
                    'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'page' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'category_id' => ['type' => 'long'],
                            'status' => ['type' => 'long'],
                            'type' => ['type' => 'string'],
                        ]
                    ],
                    'attribute' => [
                        'type' => 'object',
                        'properties' => [

                        ]
                    ],
                    'attributeValue' => [
                        'type' => 'object',
                        'properties' => [
                            'status' => ['type' => 'long'],
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        static::getDb()->createCommand()->deleteIndex(static::index());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'attribute_id',
            'value',
            'entity_alias',
            'value_alias',
            'locale',
            'page',
            'attribute',
            'attributeValue'
        ];
    }
}
