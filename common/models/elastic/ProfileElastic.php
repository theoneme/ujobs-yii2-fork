<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 18:22
 */

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;
use Yii;
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;

/**
 * Class User
 *
 * Database fields:
 * @property integer $user_id
 * @property string $name
 * @property string $gravatar_email
 * @property integer $status
 * @property integer $rating
 * @property string $bio
 *
 * @property array $translations
 * @property array $lang
 * @property array $categories
 */
class ProfileElastic extends ActiveRecord
{
    const ROLE_ADMIN = 10;
    const ROLE_MODERATOR = 8;
    public static $usernameRegexp = '/^[-a-zA-Zа-яА-ЯєЄіІїЇёЁ0-9_\.@+\(\)]+$/u';
    public $jobsCount = null;
    public $skillsCount = null;

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'profile';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'user_id' => ['type' => 'long'],
                    'name' => ['type' => 'string'],
                    'gravatar_email' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'status' => ['type' => 'long'],
                    'rating' => ['type' => 'long'],
                    'bio' => ['type' => 'string'],
                    'is_bio_empty' => ['type' => 'boolean'],
                    'created_at' => ['type' => 'long'],
                    'updated_at' => ['type' => 'long'],
                    'root_score' => ['type' => 'long'],
                    'upper_category_score' => ['type' => 'long'],
                    'sold_items' => ['type' => 'long'],
                    'category_score' => ['type' => 'long'],
                    'attrs' => [
                        'type' => 'nested',
                        'properties' => [
                            'attribute_id' => ['type' => 'long'],
                            'value' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'is_text' => ['type' => 'boolean'],
                            'title' => ['type' => 'string'],
                            'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'tariff' => [
                        'type' => 'object',
                        'properties' => [
                            'price' => ['type' => 'long'],
                            'icon' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'expires_at' => ['type' => 'long']
                        ]
                    ],
                    'user' => [
                        'type' => 'object',
                        'properties' => [
                            'username' => ['type' => 'string'],
                            'email' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'phone' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'categories' => [
                        'type' => 'nested',
                        'properties' => [
                            'id' => ['type' => 'integer'],
                        ]
                    ],
                    'translations' => [
                        'type' => 'nested',
                        'properties' => [
                            'title' => ['type' => 'string'],
                            'content' => ['type' => 'string'],
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'lang' => [
                        'type' => 'nested',
                        'properties' => [
                            'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],

            ['is_bio_empty', 'default', 'value' => false]
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'user_id',
            'name',
            'gravatar_email',
            'status',
            'bio',
            'is_bio_empty',
            'created_at',
            'updated_at',
            'sold_items',
            'rating',
            'phone',
            'root_score',
            'upper_category_score',
            'category_score',
            'attrs',
            'tariff',
            'user',
            'categories',
            'translations',
            'lang'
        ];
    }

    /**
     * @return string
     */
    public function getTranslation()
    {
        $t = null;
        foreach ($this->translations as $translation) {
            if ($translation['locale'] === Yii::$app->language) {
                $t = $translation;
                break;
            }
        }

        return $t ?? array_values($this->translations)[0];
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return Yii::$app->utility->upperFirstLetter($this->translation['title'] ?? (!empty($this->name) ? $this->name : $this->user['username']));
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return Yii::$app->utility->upperFirstLetter(BaseStringHelper::truncate(Html::encode($this->getDescription()), 50, '...', 'UTF-8'));
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return !empty($this->translation['description']) ? $this->translation['description'] : $this->bio;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if (preg_match('/^http(s)?:\/\/.*/', $this->gravatar_email) || ($this->gravatar_email && file_exists(Yii::getAlias("@frontend") . "/web" . $this->gravatar_email))) {
            $image = $this->gravatar_email;
        }

        $width = $height = null;

        if ($target !== null && Yii::$app->params['images'][$target] != null) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->tariff && $this->tariff['expires_at'] > time()) {
            return $this->tariff['icon'];
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->tariff && $this->tariff['expires_at'] > time()) {
            return $this->tariff['title'];
        }

        return null;
    }
}