<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property integer $user_id
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $cost
 * @property string $currency_code
 * @property string $invoice_file
 *
 * @property Payment $payment
 * @property User $user
 */
class Invoice extends \yii\db\ActiveRecord
{
    const TYPE_INDIVIDUAL = 1;
    const TYPE_LEGAL_ENTITY = 2;

    const STATUS_WAITING = 10;
    const STATUS_DECLINED = 20;
    const STATUS_APPROVED = 30;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'type', 'cost'], 'required'],
            [['payment_id', 'user_id', 'type', 'status', 'created_at', 'updated_at', 'cost'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['invoice_file'], 'string', 'max' => 155],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::class, 'targetAttribute' => ['payment_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['status'], 'default', 'value' => self::STATUS_WAITING],
            [['currency_code'], 'default', 'value' => 'RUB']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'payment_id' => Yii::t('model', 'Payment ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'type' => Yii::t('model', 'Type'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'cost' => Yii::t('model', 'Cost'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'invoice_file' => Yii::t('model', 'Invoice File'),
        ];
    }

    /**
     * @param bool|false $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_WAITING => ($colored ? Html::tag('div', Yii::t('labels', 'Waiting'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Waiting')),
            self::STATUS_DECLINED => ($colored ? Html::tag('div', Yii::t('labels', 'Declined'), ['class' => 'btn btn-danger btn-xs']) : Yii::t('labels', 'Declined')),
            self::STATUS_APPROVED => ($colored ? Html::tag('div', Yii::t('labels', 'Approved'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Approved')),
        ];
    }

    /**
     * @param bool|false $colored
     * @return string
     */
    public function getTypeLabel($colored = false)
    {
        $labels = static::getTypeLabels($colored);
        return isset($labels[$this->type]) ? $labels[$this->type] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getTypeLabels($colored = false)
    {
        return [
            self::TYPE_INDIVIDUAL => ($colored ? Html::tag('div', Yii::t('labels', 'Individual'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Individual')),
            self::TYPE_LEGAL_ENTITY => ($colored ? Html::tag('div', Yii::t('labels', 'Legal entity'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Legal entity')),
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (isset($changedAttributes['status']) && $changedAttributes['status'] != self::STATUS_APPROVED && $this->status == self::STATUS_APPROVED) {
            $payment = Payment::findOne($this->payment_id);

            if ($payment != null) {
                $payment->markComplete();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
