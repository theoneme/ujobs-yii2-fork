<?php

namespace common\models;

use common\models\user\User;
use common\modules\store\models\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_wallet".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $currency_code
 * @property integer $balance
 *
 * @property Currency $currency
 * @property User $user
 * @property PaymentHistory[] $paymentHistories
 */
class UserWallet extends ActiveRecord
{
    /**
     * @var array
     */
    public static $currencyToLetter = [
        'RUB' => 'R',
        'USD' => 'D',
        'EUR' => 'E',
        'UAH' => 'H',
        'GEL' => 'G',
        'CNY' => 'Y',
        'CTY' => 'C',
    ];

    /**
     * @var array
     */
    public static $currencyToName = [
        'RUB' => 'RR',
        'USD' => 'UD',
        'EUR' => 'EE',
        'UAH' => 'UH',
        'GEL' => 'GL',
        'CNY' => 'YY',
        'CTY' => 'CY',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_wallet';
    }

    /**
     * @param $condition
     * @return UserWallet
     */
    public static function findOrCreate($condition)
    {
        $wallet = UserWallet::find()->where($condition)->one();
        if ($wallet === null) {
            $wallet = new UserWallet($condition);
            $wallet->save();
        }
        return $wallet;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'balance'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'balance' => Yii::t('model', 'Balance'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getPaymentHistories()
    {
//        return $this->hasMany(PaymentHistory::class, ['user_id' => 'user_id'])->andOnCondition('user_wallet.currency_code = payment_history.currency_code');
        return $this->hasMany(PaymentHistory::class, ['currency_code' => 'currency_code', 'user_id' => 'user_id'])->orderBy('created_at desc');
    }

    /**
     * @param $type
     * @return int
     */
    public function getPaymentHistoryTotal($type)
    {
        return array_sum(
            array_column(
                array_filter($this->paymentHistories, function ($var) use ($type) {
                    /* @var $var PaymentHistory */
                    return $var->balance_change === $type;
                }),
                'amount'
            )
        );
    }

    /**
     * @return mixed
     */
    public function getLetter()
    {
        return self::$currencyToLetter[$this->currency_code];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return self::$currencyToName[$this->currency_code];
    }
}
