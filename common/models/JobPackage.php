<?php

namespace common\models;

use common\components\CurrencyHelper;
use common\modules\store\models\CartItemInterface;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

/**
 * This is the model class for table "job_package".
 *
 * @property integer $id
 * @property integer $job_id
 * @property string $title
 * @property string $description
 * @property integer $price
 * @property string $currency_code
 * @property string $included
 * @property string $excluded
 * @property string $alias
 * @property string $type
 *
 * @property Job $job
 */
class JobPackage extends ActiveRecord implements CartItemInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'price', 'price', 'type'], 'required'],
            [['job_id', 'price'], 'integer'],
            ['price', 'number'],
            ['price', 'validatePrice'],
            [['included', 'excluded'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['currency_code'], 'string', 'max' => 8],
            [['type'], 'string', 'max' => 35],

            [['job_id'], 'exist', 'skipOnError' => false, 'targetClass' => Job::class, 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validatePrice($attribute, $params)
    {
        if(CurrencyHelper::convert('RUB', $this->currency_code, 1) > $this->price) {
            $this->addError($attribute, Yii::t('app', 'Price must be greater than {0}', CurrencyHelper::convert('RUB', $this->currency_code, 1)));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'job_id' => Yii::t('model', 'Job ID'),
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'included' => Yii::t('model', 'Included'),
            'excluded' => Yii::t('model', 'Excluded'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->title = HtmlPurifier::process($this->title);
            $this->description = HtmlPurifier::process($this->description);
            $this->included = HtmlPurifier::process($this->included);
            $this->excluded = HtmlPurifier::process($this->excluded);

            return true;
        }

        return false;
    }

    public function isEmpty()
    {
        return $this->price === '' && $this->description === '' && $this->excluded === '' && $this->included === '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::class, ['id' => 'job_id']);
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->job->alias;
    }

    /**
     * interface implementations
     */

    /**
     * @return int
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return ($this->job->translation->title ?? '') . ' - ' . $this->title;
    }

    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @param string $target
     * @param boolean $fromAws
     * @return string
     */
    public function getThumb($target = null, $fromAws = true)
    {
        return $this->job->getThumb($target, $fromAws);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['/job/view', 'alias' => $this->job->alias]);
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->job->requirements;
    }

    /**
     * @return int
     */
    public function getSellerId()
    {
        return $this->job->user_id;
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->job->user->getSellerUrl();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->job->user->getSellerName();
    }
}