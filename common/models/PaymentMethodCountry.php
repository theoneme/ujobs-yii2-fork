<?php

namespace common\models;

use common\modules\attribute\models\AttributeValue;
use common\modules\store\models\PaymentMethod;
use common\traits\FindOrCreateTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment_method_country".
 *
 * @property int $id
 * @property int $method_id
 * @property int $country_id
 *
 * @property AttributeValue $country
 * @property PaymentMethod $method
 */
class PaymentMethodCountry extends ActiveRecord
{
    use FindOrCreateTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_method_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method_id', 'country_id'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['country_id' => 'id']],
            [['method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::class, 'targetAttribute' => ['method_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'method_id' => Yii::t('model', 'Method ID'),
            'country_id' => Yii::t('model', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(PaymentMethod::class, ['id' => 'method_id']);
    }
}
