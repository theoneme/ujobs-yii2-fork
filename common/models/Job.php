<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\BlameableBehavior;
use common\behaviors\LinkableBehavior;
use common\components\CurrencyHelper;
use common\models\elastic\JobAttributeElastic;
use common\models\elastic\JobElastic;
use common\models\interfaces\HighlightInterface;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\attribute\models\AttributeDescription;
use common\modules\store\models\CartItemInterface;
use common\modules\store\models\Currency;
use common\modules\store\models\Order;
use common\services\elastic\JobElasticService;
use common\services\GoogleMapsService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;


/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $parent_category_id
 * @property integer $category_id
 * @property integer $date_start
 * @property integer $date_end
 * @property integer execution_time
 * @property string $status
 * @property integer $price
 * @property boolean $contract_price
 * @property integer $currency_code
 * @property string $place
 * @property string $alias
 * @property string $requirements
 * @property integer $click_count
 * @property integer $views_count
 * @property integer $orders_count
 * @property integer $cancellations_count
 * @property integer $rating
 * @property string $type
 * @property integer $percent_bonus
 * @property string $locale
 * @property boolean $auto_translated
 * @property string $address
 * @property string $lat
 * @property string $long
 * @property boolean $price_per_unit
 * @property integer $measure_id
 * @property integer $units_amount
 * @property integer $requirements_type
 *
 * @property User $user
 * @property Profile $profile
 * @property Category $category
 * @property ContentTranslation $translation
 * @property Attachment $attachment
 * @property Attachment[] $attachments
 * @property ContentTranslation[] $translations
 * @property JobQa[] $faq
 * @property JobExtra[] $extras
 * @property JobAttribute[] $extra
 * @property JobPackage[] $packages
 * @property JobAttribute[] $tags
 * @property JobAttribute[] $specialties
 * @property UserFavourite[] $favourites
 * @property UserFavourite $favourite
 * @property AttributeDescription $attrDescription
 * @property AttributeDescription $attrValueDescription
 * @property Measure $measure
 * @property JobRequirement $simpleRequirements
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 *
 */
class Job extends ActiveRecord implements CartItemInterface, HighlightInterface
{
    const STATUS_DRAFT = -50;
    const STATUS_DENIED = -10;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    const TYPE_JOB = 'job';
    const TYPE_TENDER = 'tender';
    const TYPE_ADVANCED_TENDER = 'advanced-tender';

    const PERIOD_START_FROM = 'start';
    const PERIOD_FINISH_TO = 'end';
    const PERIOD_RANGE = 'range';

    const REQUIREMENTS_TYPE_DEFAULT = 0;
    const REQUIREMENTS_TYPE_SIMPLE = 1;
    const REQUIREMENTS_TYPE_ADVANCED = 2;

    /**
     * @var string
     */
    private $_address = null;

    /**
     * Determines where BlameableBehavior should be used
     * @var bool
     */
    public $blameable = true;
    /**
     * @var string
     */
    public $title = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job';
    }

    /**
     * @param $jobId
     * @return bool
     */
    public static function hasAccess($jobId)
    {
        return Yii::$app->user->isGuest ? false : Job::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'id' => $jobId])->exists();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'packages', 'faq', 'extras', 'extra'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
            ],
        ];
        if ($this->blameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ];
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rating', 'execution_time', 'user_id', 'category_id', 'parent_category_id', 'status', 'requirements_type'], 'integer'],
            [['price'], 'number'],
            [['type', 'subtype'], 'string', 'max' => 25],
            ['title', 'string'],
            [['period_type'], 'string', 'max' => 15],
            [['alias'], 'string', 'max' => 155],
            ['percent_bonus', 'number', 'min' => 0, 'max' => 100],

            [['contract_price', 'price_per_unit'], 'boolean'],

            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION],

            [['click_count', 'orders_count', 'views_count', 'cancellations_count', 'price'], 'default', 'value' => 0],
            [['requirements'], 'string', 'max' => 700],
            [['images', 'attributesArr', 'date_start', 'date_end'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => false, 'targetClass' => Profile::class, 'targetAttribute' => ['user_id' => 'user_id']],
            [['category_id'], 'exist', 'skipOnError' => false, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['parent_category_id'], 'exist', 'skipOnError' => false, 'targetClass' => Category::class, 'targetAttribute' => ['parent_category_id' => 'id']],

            ['locale', 'string'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
            ['currency_code', 'validateCurrency'],
            [['lat', 'long'], 'string'],
            [['units_amount', 'measure_id'], 'required', 'when' => function ($model) {
                return $model->price_per_unit === '1';
            }],
            ['units_amount', 'number', 'min' => 1],
            [['measure_id'], 'exist', 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if (!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'status' => Yii::t('model', 'Status'),
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'category_id' => Yii::t('model', 'Category'),
            'is_active' => Yii::t('model', 'Is active'),
            'is_moderated' => Yii::t('model', 'Is moderated'),
            'images' => Yii::t('model', 'Images'),
            'datetime' => Yii::t('model', 'Datetime'),
            'type' => Yii::t('model', 'Type'),
            'anytime' => Yii::t('model', 'Anytime'),
            'execution_time' => Yii::t('model', 'Average execution time'),
            'alias' => Yii::t('model', 'Alias'),
            'parentCategory' => Yii::t('model', 'Parent category'),
            'parent_category_id' => Yii::t('model', 'Parent category'),
            'translation.title' => Yii::t('model', 'Title'),
            'requirements' => Yii::t('model', 'Requirements'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        $relation = $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])
            ->from('content_translation ct')
            ->andOnCondition(['ct.entity' => 'job']);
//            ->andWhere(['ct.locale' => Yii::$app->language]);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['content_translation.entity' => Job::TYPE_JOB])->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['id' => 'order_id'])
            ->viaTable('order_product', ['product_id' => 'id'])
            ->andWhere(['order.product_type' => Order::TYPE_JOB]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedOrders()
    {
        return $this->hasMany(Order::class, ['id' => 'order_id'])
            ->viaTable('order_product', ['product_id' => 'id'])
            ->andWhere(['>', 'status', Order::STATUS_DELIVERED])
            ->andWhere(['order.product_type' => Order::TYPE_JOB]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'job']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstAttachment()
    {
        return $this->hasOne(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'job']);
    }

    /**
     * @return Attachment[]
     */
    public function getImages()
    {
        return $this->attachments;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachment()
    {
        return $this->hasOne(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'job']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimpleRequirements()
    {
        return $this->hasMany(JobRequirement::class, ['job_id' => 'id'])->andWhere(['step_id' => null]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvancedRequirements()
    {
        return $this->hasMany(JobRequirement::class, ['job_id' => 'id'])->andWhere(['not', ['step_id' => null]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllRequirements()
    {
        return $this->hasMany(JobRequirement::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequirementSteps()
    {
        return $this->hasMany(JobRequirementStep::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtra()
    {
        return $this->hasMany(JobAttribute::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtras()
    {
        return $this->hasMany(JobExtra::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaq()
    {
        return $this->hasMany(JobQa::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(JobAttribute::class, ['job_id' => 'id'])->where([
            'job_attribute.entity_alias' => 'tag'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialties()
    {
        return $this->hasMany(JobAttribute::class, ['job_id' => 'id'])->where([
            'job_attribute.attribute_id' => 41
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackages()
    {
        return $this->hasMany(JobPackage::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavourites()
    {
        return $this->hasMany(UserFavourite::class, ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavourite()
    {
        return $this->hasOne(UserFavourite::class, ['job_id' => 'id'])->from(['uf2' => 'user_favourite'])->onCondition(['uf2.user_id' => userId()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::class, ['id' => 'measure_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndexedAttributes()
    {
        return $this->hasMany(JobAttribute::class, ['job_id' => 'id'])->indexBy('entity_alias')->andWhere(['not', ['entity_alias' => 'tag']]);
    }

    /**
     * @return null|string
     */
    public function getAddress()
    {
        if ($this->_address === null && $this->lat && $this->long) {
            /* @var $addressTranslation AddressTranslation */
            $addressTranslation = AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->one();
            if ($addressTranslation !== null) {
                $this->_address = $addressTranslation->title;
            } else {
                $gmapsService = new GoogleMapsService();
                $address = $gmapsService->reverseGeocode($this->lat, $this->long, Yii::$app->params['supportedLocales'][Yii::$app->language]);
                if ($address !== false) {
                    (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $address]))->save();
                    $this->_address = $address;
                }
            }
        }
        return $this->_address;
    }

    /**
     * @param string $target
     * @return array
     */
    public function getThumbs($target = null)
    {
        $images = [];

        if (!empty($this->attachments)) {
            foreach ($this->attachments as $attachment) {
                $images[] = Yii::$app->mediaLayer->getThumb($attachment->content, $target);
            }
        }

        return $images;
    }

    /**
     * Determines whether column is empty in all packages
     * @param null $column
     * @return bool
     */
    public function isPackageColumnEmpty($column)
    {
        return ($column !== null) ? array_reduce($this->packages, function ($carry, $item) use ($column) {
            return $carry && empty($item->$column);
        }, true) : true;
    }

    /**
     * @return bool
     */
    public function isTenderNew()
    {
        return in_array($this->type, [self::TYPE_TENDER, self::TYPE_ADVANCED_TENDER]) && $this->created_at > (time() - 60 * 60 * 24 * 2);
    }

    /**
     * @return bool
     */
    public function isFavourite()
    {
        return !isGuest() && isset(Yii::$app->user->identity->favourites[$this->id]);
    }

    /**
     * @return int
     */
    public function getActualPackagesCount()
    {
        $count = 0;
        $packages = $this->packages;
        if ($packages) {
            /* @var JobPackage $package */
            foreach ($packages as $package) {
                if ($package->id) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->getThumb();
    }

    /**
     * @return array
     */
    public function getPriceRange()
    {
        $min = $max = $this->price;
        if (count($this->packages) > 1) {
            $packages = $this->packages;
            ArrayHelper::multisort($packages, 'price');
            $min = array_shift($packages)->price;
            $max = array_pop($packages)->price;
        }
        return [
            'min' => CurrencyHelper::convert($this->currency_code, Yii::$app->params['app_currency_code'], $min, true),
            'max' => CurrencyHelper::convert($this->currency_code, Yii::$app->params['app_currency_code'], $max, true)
        ];
    }

    /**
     * @return array
     */
    public function formatAttributes()
    {
        $exceptions = ['tag'];

        $result = [];
        $attributes = $this->extra;
        if (!empty($attributes)) {
            foreach ($attributes as $key => $attribute) {
                if (!in_array($attribute->entity_alias, $exceptions)) {
                    $translation = $attribute->attrDescriptions[Yii::$app->language] ?? array_values($attribute->attrDescriptions)[0];
                    $valueTranslation = $attribute->attrValueDescriptions[Yii::$app->language] ?? array_values($attribute->attrValueDescriptions)[0];
                    if (array_key_exists($translation->title, $result)) {
                        $result[$translation->title] = (array)$result[$translation->title];
                        $result[$translation->title][] = Html::encode($valueTranslation->title);
                    } else {
                        $result[$translation->title] = Html::encode($valueTranslation->title);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param null $target
     * @param bool $fromAws
     * @return string
     */
    public function getThumb($target = null, $fromAws = true)
    {
        return Yii::$app->mediaLayer->getThumb($this->attachments[0]->content ?? null, $target);
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DENIED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Denied') . '</span>' : Yii::t('labels', 'Denied')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DRAFT => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Draft') . '</span>' : Yii::t('labels', 'Draft')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     *
     */
    public function defineStatus()
    {
        if (in_array($this->status, [self::STATUS_ACTIVE, self::STATUS_REQUIRES_MODERATION, self::STATUS_REQUIRES_MODIFICATION])) {
            $letters = 'a-zA-Zа-яА-Яა-ჸєЄіІїЇёЁÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüŸ¡¿çÇŒœßØøÅåÆæÞþÐð';
            $pattern = '/^(\d{0,8}|[' . $letters . ']+)([\s\.\?,!;:\-#№%\*\(\)\"\'\/\\=+~<>–—✔“”]+(\d{0,8}|[' . $letters . ']+)){2,}[\s\.!\?]*$/u';
            if (count($this->attachments)) {
                if (!empty(strip_tags($this->translation->content)) && preg_match($pattern, strip_tags($this->translation->content))) {
                    if ($this->status != self::STATUS_ACTIVE) {
                        $this->sendModerationNotification();
                        if (!Event::find()->where(['code' => Event::JOB_MODERATED, 'user_id' => $this->user_id, 'entity' => 'job', 'entity_content' => $this->id])->exists()) {
                            (new Event(['code' => Event::JOB_MODERATED, 'user_id' => $this->user_id, 'entity' => 'job', 'entity_content' => $this->id]))->save();
                            $bonus = new PaymentHistory([
                                'user_id' => $this->user_id,
                                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                                'template' => PaymentHistory::TEMPLATE_PAYMENT_POST_JOB_BONUS,
                                'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                                'amount' => 1,
                            ]);
                            $bonus->save();
                        }
                    }
                    $this->updateAttributes(['status' => self::STATUS_ACTIVE]);
                } else {
                    $this->updateAttributes(['status' => self::STATUS_REQUIRES_MODERATION]);
                }
            } else {
                $this->updateAttributes(['status' => self::STATUS_REQUIRES_MODIFICATION]);
            }
        }
    }

    /**
     * @return bool
     */
    public function sendModerationNotification()
    {
        switch ($this->type) {
            case self::TYPE_TENDER:
            case self::TYPE_ADVANCED_TENDER:
                $message = Yii::t('notifications', "Your request has passed moderation and has been added to catalog.", [], $this->user->site_language);
                $subject = Yii::t('notifications', "Your request has passed moderation on {site}", ['site' => Yii::$app->name], $this->user->site_language);
                $template = Notification::TEMPLATE_REQUEST_MODERATED;
                $route = ['/tender/view', 'alias' => $this->alias];
                break;
            default:
                $subject = Yii::t('notifications', "Your job has passed moderation on {site}", ['site' => Yii::$app->name], $this->user->site_language);
                $message = Yii::t('notifications', "Your job has passed moderation and has been added to catalog. If you want more chances to be found and receive orders, you can post another job.", [], $this->user->site_language);
                $template = Notification::TEMPLATE_JOB_MODERATED;
                $route = ['/job/view', 'alias' => $this->alias];
        }

        $notification = new Notification([
            'to_id' => $this->user_id,
            'params' => [
                'subject' => $subject,
                'content' => $message,
            ],
            'template' => $template,
            'custom_data' => json_encode([
                'job' => $this->translation->title,
            ]),
            'linkRoute' => $route,
            'withEmail' => true,
            'is_visible' => true
        ]);

        return $notification->save();
    }

    /**
     *
     */
    public function defineRating()
    {
        $rating = 5;
        $rating += count($this->attachments) > 3 ? 1.5 : count($this->attachments) / 2;
        $rating += count($this->packages) / 2;
        if (count($this->packages)) {
            $l = 0;
            foreach ($this->packages as $package) {
                $l += mb_strlen($package->description);
            }
            $r = ($l / count($this->packages)) / 200;
            $rating += $r > 1 ? 1 : $r;
        }
        $r = mb_strlen($this->translation->content) / 200;
        $rating += $r > 2 ? 2 : $r;
        $rating = $rating > 10 ? 10 : round($rating);
        $this->updateAttributes(['rating' => $rating]);
    }

    /**
     * Updates related JobElastic
     * @param bool $doSave
     * @return JobElastic|bool
     */
    public function updateElastic($doSave = true)
    {
        $jobElasticService = new JobElasticService($this);
        return $jobElasticService->process($doSave);
    }

    /**
     * Updates related JobElastic
     * @return bool
     */
    public function updateElasticOrders()
    {
        $jobElasticService = new JobElasticService($this);
        return $jobElasticService->updateOrders();
    }

    /**
     * Updates related JobElastic
     * @return bool
     */
    public function updateElasticTariff()
    {
        $jobElasticService = new JobElasticService($this);
        return $jobElasticService->updateTariff();
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->date_start) {
            $this->date_start = Yii::$app->formatter->asDatetime($this->date_start, 'php:d-m-Y');
        }

        if ($this->date_end) {
            $this->date_end = Yii::$app->formatter->asDatetime($this->date_end, 'php:d-m-Y');
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert === true && !$this->alias) {
                $this->alias = Yii::$app->utility->generateSlug($this->title, 5, 155);
            }

            if ($this->date_start) {
                $this->date_start = strtotime($this->date_start);
            }

            if ($this->date_end) {
                $this->date_end = strtotime($this->date_end);
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->status === self::STATUS_ACTIVE && array_key_exists('status', $changedAttributes) && $changedAttributes['status'] !== self::STATUS_ACTIVE) {
            /** @var UserFriend[] $friends */
            $friends = $this->user->friends;
            if (!empty($friends)) {
                $template = $this->type === self::TYPE_JOB ? Notification::TEMPLATE_FRIEND_POSTED_JOB : Notification::TEMPLATE_FRIEND_POSTED_JOB_REQUEST;
                foreach ($friends as $friend) {
                    $notification = new Notification([
                        'to_id' => $friend->target_user_id,
                        'from_id' => $this->user_id,
                        'thumb' => $this->getThumb('catalog'),
                        'custom_data' => json_encode([
                            'user' => $this->user->getSellerName(),
                        ]),
                        'template' => $template,
                        'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                        'linkRoute' => ["/{$this->type}/view", 'alias' => $this->alias],
                        'withEmail' => false,
                        'is_visible' => true
                    ]);

                    $notification->save();
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->attachments as $attachment) {
                $attachment->delete();
            }
            JobAttributeElastic::deleteAll(['id' => ArrayHelper::getColumn($this->extra, 'id')]);
            JobAttribute::deleteAll('job_id = :id', [':id' => $this->id]);
            JobPackage::deleteAll('job_id = :id', [':id' => $this->id]);
            JobQa::deleteAll('job_id = :id', [':id' => $this->id]);
            JobExtra::deleteAll('job_id = :id', [':id' => $this->id]);
            JobElastic::deleteAll(['id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * interface implementations
     */

    /**
     * @return int|string
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return trim(preg_replace('/\s+/', ' ', $this->translation->title ?? ''));
    }

    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translation->content;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $type = $this->type == self::TYPE_ADVANCED_TENDER ? 'tender' : $this->type;
        return Url::to(["/{$type}/view", 'alias' => $this->alias]);
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @return int
     */
    public function getSellerId()
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->user->getSellerUrl();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->user->getSellerName();
    }

    /**
     * @param null $target
     * @return mixed
     */
    public function getSellerThumb($target = null)
    {
        return $this->user->getThumb($target);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->user->activeTariff) {
            return $this->user->activeTariff->tariff->icon;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->user->activeTariff) {
            return $this->user->activeTariff->tariff->title;
        }

        return null;
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $templateCategory = $this->type === self::TYPE_JOB ? SeoAdvanced::TEMPLATE_JOB : SeoAdvanced::TEMPLATE_TENDER;
        $templates = SeoAdvanced::getDefaultTemplates($templateCategory);
        if ($templateCategory === SeoAdvanced::TEMPLATE_TENDER) {
            $price = $this->contract_price
                ? Yii::t('seo', 'at the contract price', [], $this->locale)
                : Yii::t('seo', 'at the price {price}', ['price' => $this->getPrice()], $this->locale);
            $price2 = $this->contract_price ? Yii::t('seo', 'Contract', [], $this->locale) : $this->getPrice();
            if ($this->type === self::TYPE_ADVANCED_TENDER && !$this->contract_price) {
                $price .= ' ' . Yii::t('app', 'per month', [], $this->locale);
                $price2 .= ' ' . Yii::t('app', 'per month', [], $this->locale);
            }
        } else {
            $price2 = $price = $this->getPrice();
        }
        $params = [
            'title' => $this->getLabel(),
            'Title' => implode(' ', array_map(function($var){return Yii::$app->utility->upperFirstLetter($var);}, explode(' ', $this->getLabel()))),
            'description' => StringHelper::truncate(strip_tags($this->translation->content), 255),
            'price' => $price,
            'Price' => $price2,
            'username' => $this->getSellerName()
        ];

        return [
            'title' => Yii::t('seo', $templates['title'], $params, $this->locale),
            'description' => Yii::t('seo', $templates['description'], $params, $this->locale),
            'keywords' => Yii::t('seo', $templates['title'], $params, $this->locale)
        ];
    }
}