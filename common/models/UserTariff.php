<?php

namespace common\models;

use common\models\user\User;
use common\modules\store\models\PaymentMethod;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_tariff".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $tariff_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $duration
 * @property integer $starts_at
 * @property integer $expires_at
 * @property integer $cost
 * @property string $currency_code
 * @property integer $status
 * @property integer $payment_id
 *
 * @property User $user
 * @property Tariff $tariff
 * @property Payment $payment
 */
class UserTariff extends \yii\db\ActiveRecord
{
    const STATUS_REQUIRES_PAYMENT = 5;
    const STATUS_ACTIVE = 10;
    const STATUS_EXPIRED = 15;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_tariff';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cost'], 'required'],
            [['user_id', 'created_at', 'updated_at', 'duration', 'expires_at', 'cost', 'payment_id', 'status', 'starts_at'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::class, 'targetAttribute' => ['payment_id' => 'id']],
            ['currency_code', 'default', 'value' => 'RUB'],
            ['status', 'default', 'value' => self::STATUS_REQUIRES_PAYMENT]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'duration' => Yii::t('model', 'Duration'),
            'expires_at' => Yii::t('model', 'Expires At'),
            'cost' => Yii::t('model', 'Cost'),
            'currency_code' => Yii::t('model', 'Currency Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::class, ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'payment_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->starts_at = $this->created_at;
            }
            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($changedAttributes['status'] == self::STATUS_REQUIRES_PAYMENT && $this->status == self::STATUS_ACTIVE) {

            if ($this->payment->payment_method_id != PaymentMethod::find()->where(['code' => 'balance'])->select('id')->scalar()) {
                $paymentHistory = new PaymentHistory();
                $paymentHistory->amount = $this->payment->total;
                $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_POS;
                $paymentHistory->template = PaymentHistory::TEMPLATE_PAYMENT_ACCOUNT_REPLENISH;
                $paymentHistory->type = PaymentHistory::TYPE_OPERATION_REPLENISH;
                $paymentHistory->user_id = $this->user_id;
                $paymentHistory->currency_code = $this->payment->currency_code;
                $paymentHistory->save();
            }
            $paymentHistory = new PaymentHistory();
            $paymentHistory->amount = $this->payment->total;
            $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_NEG;
            $paymentHistory->template = PaymentHistory::TEMPLATE_PAYMENT_TARIFF_PURCHASE;
            $paymentHistory->type = PaymentHistory::TYPE_OPERATION_PURCHASE;
            $paymentHistory->user_id = $this->user_id;
            $paymentHistory->currency_code = $this->payment->currency_code;
            $paymentHistory->custom_data = json_encode([
                'tariff' =>  $this->tariff->title
            ]);
            $paymentHistory->save();

            $time = time();
            $activeTariffs = UserTariff::find()
                ->where(['tariff_id' => $this->tariff_id, 'user_id' => $this->user_id, 'status' => self::STATUS_ACTIVE])
                ->andWhere(['<', 'starts_at', $time])
                ->andWhere(['>', 'expires_at', $time])
                ->andWhere(['not', ['id' => $this->id]])
                ->all();
            $additionalTime = 0;
            foreach ($activeTariffs as $tariff) {
                /* @var $tariff UserTariff */
                $additionalTime += $tariff->expires_at - $time;
                $tariff->updateAttributes(['status' => self::STATUS_EXPIRED]);
            }
            $this->updateAttributes(['expires_at' => $this->expires_at + $additionalTime]);
            $this->user->profile->updateElastic();
            foreach ($this->user->profile->jobs as $job) {
                $job->updateElasticTariff();
            }
        }
    }
}
