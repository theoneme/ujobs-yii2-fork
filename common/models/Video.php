<?php

namespace common\models;

use common\behaviors\BlameableBehavior;
use common\behaviors\ImageBehavior;
use common\components\CurrencyHelper;
use common\models\user\Profile;
use common\models\user\User;
use common\modules\store\models\Currency;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property integer $price
 * @property string $image
 * @property string $video
 * @property integer $status
 * @property string $locale
 * @property string $currency_code
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property Profile $profile
 * @property Category $category
 */
class Video extends ActiveRecord
{
    const STATUS_DENIED = -10;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'video_images'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'price', 'category_id', 'parent_category_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['title', 'image', 'video', 'alias'], 'string', 'max' => 255],
            ['user_id', 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            ['parent_category_id', 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['parent_category_id' => 'id']],

            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION],
            ['locale', 'string'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
            ['currency_code', 'validateCurrency'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if (!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'price' => Yii::t('model', 'Price'),
            'image' => Yii::t('model', 'Image'),
            'video' => Yii::t('model', 'Video'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'category_id' => Yii::t('model', 'Category'),
            'parent_category_id' => Yii::t('model', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->image, $target);
    }

    /**
     * @return int
     */
    public function getPriceString()
    {
        return !empty($this->price) ? CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true) : Yii::t('app', 'Free');
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return trim(preg_replace('/\s\s+/', ' ', $this->title ?? ''));
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->user->getSellerName();
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $templateCategory = SeoAdvanced::TEMPLATE_VIDEO;
        $templates = SeoAdvanced::getDefaultTemplates($templateCategory);
        $params = [
            'title' => $this->getLabel()
        ];

        return [
            'title' => Yii::t('seo', $templates['title'], $params, Yii::$app->language),
            'description' => Yii::t('seo', $templates['description'], $params, Yii::$app->language),
            'keywords' => ''
        ];
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasAccess($id)
    {
        return Yii::$app->user->isGuest ? false : Video::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'id' => $id])->exists();
    }


    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DENIED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Denied') . '</span>' : Yii::t('labels', 'Denied')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            if (!$this->alias) {
                $this->alias = Yii::$app->utility->generateSlug($this->title, 5, 100);
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     *
     */
    public function afterDelete()
    {
        parent::afterDelete();
        Yii::$app->mediaLayer->removeMedia($this->image);
        Yii::$app->mediaLayer->removeMedia($this->video);
    }
}
