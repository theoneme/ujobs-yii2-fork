<?php

namespace common\models;

use common\components\CurrencyHelper;
use common\modules\store\models\OrderExtra;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "job_extra".
 *
 * @property integer $id
 * @property integer $job_id
 * @property integer $title
 * @property integer $description
 * @property integer $price
 * @property string $currency_code
 * @property integer $additional_days
 *
 * @property Job $job
 * @property OrderExtra[] $orderExtras
 */
class JobExtra extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_extra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'title'], 'required'],
            [['additional_days', 'job_id', 'price'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['currency_code'], 'string', 'max' => 3],

            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::class, 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'additional_days' => Yii::t('model', 'Additional Days'),
            'job_id' => Yii::t('model', 'Job ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::class, ['id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderExtras()
    {
        return $this->hasMany(OrderExtra::class, ['job_extra_id' => 'id']);
    }

    /**
     * @return int|string
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @return array
     */
    public function getQuantityList()
    {
        $output = [];
        for ($i = 1; $i < 12; $i++) {
            $output[$i] = $i . " (" . CurrencyHelper::format(Yii::$app->params['app_currency_code'], $i * CurrencyHelper::convert($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true)) . ")";
        }

        return $output;
    }
}
