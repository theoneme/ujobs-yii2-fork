<?php

namespace common\models;

use common\components\CurrencyHelper;
use common\services\MailerService;
use frontend\modules\skype\services\SkypeFactory;
use frontend\modules\telegram\Telegram;
use Longman\TelegramBot\Request;
use SkypeBot\Command\SendMessage;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wallet_transfer_request".
 *
 * @property integer $id
 * @property integer $wallet_from_id
 * @property integer $wallet_to_id
 * @property integer $amount
 * @property string $token
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $type
 * @property integer $status
 *
 * @property UserWallet $walletFrom
 * @property UserWallet $walletTo
 */
class WalletTransferRequest extends ActiveRecord
{
    const TYPE_EMAIL = 10;
    const TYPE_SKYPE = 20;
    const TYPE_TELEGRAM = 30;

    const STATUS_SENT = 10;
    const STATUS_CONFIRMED = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_transfer_request';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->wallet_to_id = preg_replace("/[^0-9]/", "", $this->wallet_to_id);
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wallet_from_id', 'wallet_to_id', 'type', 'amount'], 'required'],
            [['wallet_from_id', 'type', 'status', 'amount'], 'integer'],
            [['token'], 'string', 'max' => 32],
            [['wallet_from_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserWallet::class, 'targetAttribute' => ['wallet_from_id' => 'id'], 'filter' => ['user_id' => userId()]],
            [['wallet_to_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserWallet::class, 'targetAttribute' => ['wallet_to_id' => 'id']],
            ['wallet_to_id', 'compare', 'compareAttribute' => 'wallet_from_id', 'operator' => '!=='],

            ['status', 'default', 'value' => self::STATUS_SENT],
            ['type', 'in', 'range' => array_keys(self::getConfirmationMethods())],
            ['amount', 'number', 'min' => 1],
            ['amount', 'balanceValidator'],
        ];
    }

    /**
     * @return array
     */
    public static function getConfirmationMethods()
    {
        $methods = [];
        if (Yii::$app->user->identity->profile->email_notifications && (Yii::$app->user->identity->email || Yii::$app->user->identity->profile->public_email)) {
            $methods[self::TYPE_EMAIL] = Yii::t('account', 'Send confirmation by {what}', ['what' => 'email']);
        }
        if (Yii::$app->user->identity->skype_uid) {
            $methods[self::TYPE_SKYPE] = Yii::t('account', 'Send confirmation by {what}', ['what' => 'skype']);
        }
        if (Yii::$app->user->identity->telegram_uid) {
            $methods[self::TYPE_TELEGRAM] = Yii::t('account', 'Send confirmation by {what}', ['what' => 'telegram']);
        }
        return $methods;
    }

    public function balanceValidator($attribute, $params)
    {
        if ($this->$attribute > $this->walletFrom->balance) {
            $this->addError($attribute, Yii::t('account', 'Your account has not enough money to transfer'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'token' => Yii::t('model', 'Token'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'type' => Yii::t('model', 'Type'),
            'status' => Yii::t('model', 'Status'),
            'amount' => Yii::t('model', 'Transfer amount'),
            'wallet_from_id' => Yii::t('model', 'Sender wallet id'),
            'wallet_to_id' => Yii::t('model', 'Receiver wallet id'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletFrom()
    {
        return $this->hasOne(UserWallet::class, ['id' => 'wallet_from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletTo()
    {
        return $this->hasOne(UserWallet::class, ['id' => 'wallet_to_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->token = Yii::$app->security->generateRandomString(6);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->status === self::STATUS_SENT) {
            switch ($this->type) {
                case self::TYPE_EMAIL:
                    if ($this->walletFrom->user->profile->email_notifications && ($this->walletFrom->user->email || $this->walletFrom->user->profile->public_email)) {
                        MailerService::sendMail(Yii::$app->mailer,
                            Yii::$app->params['ujobsNoty'],
                            Yii::t('notifications', 'Notification from {site}', ['site' => Yii::$app->name]),
                            'custom-message',
                            [
                                'content' => Yii::t('notifications', 'Your transfer confirmation code: {code}', ['code' => $this->token]),
                                'user' => $this->walletFrom->user,
                            ],
                            $this->walletFrom->user
                        );
                    }
                    break;
                case self::TYPE_SKYPE:
                    if ($this->walletFrom->user->skype_uid) {
                        $skype = SkypeFactory::getInstance();
                        $response = $skype->getApiClient()->call(
                            new SendMessage(
                                Yii::t('notifications', 'Your transfer confirmation code: {code}', ['code' => $this->token]),
                                $this->walletFrom->user->skype_uid
                            )
                        );
                    }
                    break;
                case self::TYPE_TELEGRAM:
                    if ($this->walletFrom->user->telegram_uid) {
                        $telegram = new Telegram(Yii::$app->params['telegramKey'], Yii::$app->params['telegramBotName']);
                        Request::sendMessage([
                            'chat_id' => $this->walletFrom->user->telegram_uid,
                            'text' => Yii::t('notifications', 'Your transfer confirmation code: {code}', ['code' => $this->token])
                        ]);
                    }
                    break;
            }
        }
        if ($this->status === self::STATUS_CONFIRMED) {
            $ratios = CurrencyHelper::getRatios();
            $amountTo = $this->walletTo->currency_code === $this->walletFrom->currency_code ? $this->amount : floor($this->amount / $ratios[$this->walletTo->currency_code][$this->walletFrom->currency_code]);
            // Прибавляем на одном кошельке
            $paymentHistory = new PaymentHistory();
            $paymentHistory->amount = $amountTo;
            $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_POS;
            $paymentHistory->template = PaymentHistory::TEMPLATE_MONEY_TRANSFER;
            $paymentHistory->type = PaymentHistory::TYPE_OPERATION_TRANSFER;
            $paymentHistory->user_id = $this->walletTo->user_id;
            $paymentHistory->currency_code = $this->walletTo->currency_code;
            $paymentHistory->save();
            // Отнимаем на другом
            $paymentHistory = new PaymentHistory();
            $paymentHistory->amount = $this->amount;
            $paymentHistory->balance_change = PaymentHistory::TYPE_BALANCE_CHANGE_NEG;
            $paymentHistory->template = PaymentHistory::TEMPLATE_MONEY_TRANSFER;
            $paymentHistory->type = PaymentHistory::TYPE_OPERATION_TRANSFER;
            $paymentHistory->user_id = $this->walletFrom->user_id;
            $paymentHistory->currency_code = $this->walletFrom->currency_code;
            $paymentHistory->save();
        }
    }
}
