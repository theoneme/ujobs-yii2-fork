<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "freelancer".
 *
 * @property integer $id
 * @property string $name
 * @property string $specialties
 * @property string $skype
 * @property string $email
 * @property integer $mail_sent
 */
class Freelancer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'freelancer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['specialties'], 'string'],
            [['mail_sent'], 'integer'],
            [['name', 'skype', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'name' => Yii::t('model', 'Name'),
            'specialties' => Yii::t('model', 'Specialties'),
            'skype' => Yii::t('model', 'Skype'),
            'email' => Yii::t('model', 'Email'),
            'mail_sent' => Yii::t('model', 'Mail Sent'),
        ];
    }
}
