<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $code
 * @property integer $created_at
 * @property integer $user_id
 * @property string $entity
 * @property string $entity_content
 *
 * @property User $user
 */
class Event extends \yii\db\ActiveRecord
{
    const CREATE_CONVERSATION = 'CREATE_CONVERSATION';
    const RESPOND_ON_TENDER = 'RESPOND_ON_TENDER';
    const CART_RETURN_MESSAGE = 'CART_RETURN_MESSAGE';
    const AFTER_ORDER_MESSAGE = 'AFTER_ORDER_MESSAGE';
    const PROFILE_MODERATED = 'PROFILE_MODERATED';
    const JOB_MODERATED = 'JOB_MODERATED';
    const PORTFOLIO_CREATED = 'PORTFOLIO_CREATED';
    const ALBUM_MODERATED = 'ALBUM_MODERATED';
    const REFERRAL_MESSAGE_WEEK = 'REFERRAL_MESSAGE_WEEK';
    const REFERRAL_MESSAGE_MONTH = 'REFERRAL_MESSAGE_MONTH';
    const EMAIL_CLICKED = 'EMAIL_CLICKED';
    const REFERRAL_RESENT = 'REFERRAL_RESENT';
    const EMAIL_SHARE = 'EMAIL_SHARE';

    /**
     * Determines where BlameableBehavior should be used
     * @var bool
     */
    public $blameable = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null
            ],
        ];
        if ($this->blameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ];
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['created_at', 'user_id'], 'integer'],
            [['code', 'entity', 'entity_content'], 'string', 'max' => 75],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'code' => Yii::t('model', 'Code'),
            'created_at' => Yii::t('model', 'Created At'),
            'user_id' => Yii::t('model', 'User ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_content' => Yii::t('model', 'Entity Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param null $code
     * @return bool
     */
    public static function addEvent($code = null)
    {
        if (!isGuest()) {
            $event = new Event;
            $event->code = $code;

            return $event->save();
        }
        
        return false;
    }
}
