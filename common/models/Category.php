<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:22
 */

namespace common\models;

use common\models\elastic\CategoryElastic;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\Product;
use common\services\elastic\CategoryElasticService;
use creocoder\nestedsets\NestedSetsBehavior;
use kartik\tree\models\TreeTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Application;
use yii\web\UploadedFile;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $lvl
 * @property string $name
 * @property string $icon
 * @property string $alias;
 * @property string $image
 * @property integer $icon_type
 * @property integer $active
 * @property integer $selected
 * @property integer $disabled
 * @property integer $readonly
 * @property integer $visible
 * @property integer $collapsed
 * @property integer $movable_u
 * @property integer $movable_d
 * @property integer $movable_l
 * @property integer $movable_r
 * @property integer $removable
 * @property integer $removable_all
 * @property boolean $job_online
 * @property boolean $job_offline
 * @property boolean $job_shipment_available
 * @property boolean $job_office_available
 * @property boolean $job_remote_available
 * @property boolean $order_asap_available
 * @property integer $banner_size
 * @property integer $attribute_set_id
 * @property boolean $is_adult
 * @property boolean $child_allowed
 *
 * @property array $nextLevelChildren
 * @property array $allChildren
 * @property array $parents
 * @property Category $parent
 *
 * @property SeoAdvanced $meta
 * @property Job[] $jobs
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property Measure[] $measures
 *
 * @mixin NestedSetsBehavior
 */
class Category extends ActiveRecord
{
    public static $treeQueryClass;
    /**
     * @var integer
     */
    public $relatedCount = 0;
    /**
     * @var bool whether to HTML encode the tree node names. Defaults to `true`.
     */
    public $encodeNodeNames = true;

    /**
     * @var bool whether to HTML purify the tree node icon content before saving.
     * Defaults to `true`.
     */
    public $purifyNodeIcons = true;

    /**
     * @var array activation errors for the node
     */
    public $nodeActivationErrors = [];

    /**
     * @var array node removal errors
     */
    public $nodeRemovalErrors = [];

    /**
     * @var bool attribute to cache the `active` state before a model update. Defaults to `true`.
     */
    public $activeOrig = true;

    /**
     * @var ContentTranslation[]
     */
    public $translationsArr = [];

    /**
     * @var Category[]
     */
    public $children = [];

    /**
     * @var SeoAdvanced|null
     */
    public $_meta = null;

    use TreeTrait {
        isDisabled as parentIsDisabled;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function isDisabled()
    {
        return $this->parentIsDisabled();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root', 'lft', 'banner_size', 'rgt', 'lvl', 'icon_type', 'active', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all', 'per_page', 'attribute_set_id', 'relatedCount', 'child_allowed'], 'integer'],
            [['name'], 'required'],
            [['name', 'alias', 'pages_template', 'template', 'layout', 'type'], 'string', 'max' => 60],
            [['icon'], 'string', 'max' => 255],
            [['image'], 'safe'],
//            [['alias'], 'unique'],
            ['per_page', 'default', 'value' => 15],
            ['sort_field', 'in', 'range' => ['title', 'publish_date', 'all_views', 'm_views', 'lm_views']],
            ['sort_order', 'integer'],
            ['layout', 'default', 'value' => 'main'],
            ['template', 'default', 'value' => 'category'],
            ['pages_template', 'default', 'value' => 'page'],
            ['translationsArr', 'safe'],
            ['is_adult', 'boolean']
            /*[
                'alias', 'match', 'pattern' => '/[^a-zA-Z_-]/',
            ],*/
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        $relation = $this->hasOne(ContentTranslation::class, ['entity_id' => 'id']);
        $relation->andOnCondition(['content_translation.entity' => 'category']);
        $relation->andOnCondition(['content_translation.locale' => Yii::$app->language]);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['content_translation.entity' => 'category'])->indexBy('locale');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'root' => Yii::t('model', 'Root'),
            'name' => Yii::t('model', 'Name'),
            'per_page' => Yii::t('model', 'Per Page'),
            'layout' => Yii::t('model', 'Layout'),
            'template' => Yii::t('model', 'Category template'),
            'pages_template' => Yii::t('model', 'Pages template'),
            'sort_field' => Yii::t('model', 'Sort Field'),
            'sort_order' => Yii::t('model', 'Sort Order'),
            'alias' => Yii::t('model', 'Alias'),
            'image' => Yii::t('model', 'Image'),
            'type' => Yii::t('model', 'Type'),
            'attribute_set_id' => Yii::t('model', 'Attribute set')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolios()
    {
        return $this->hasMany(UserPortfolio::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasures()
    {
        return $this->hasMany(Measure::class, ['id' => 'measure_id'])->viaTable('measure_to_category', ['category_id' => 'id']);
    }

    /**
     * @param $locale
     * @return array
     */
    public function getMeasuresArray($locale)
    {
        $measures = ArrayHelper::map($this->measures, 'id', function ($var) use ($locale) {
            return $var->translations[$locale]->title;
        });
        if ($this->lvl > 0) {
            $measures = ArrayHelper::merge($measures, $this->parent->getMeasuresArray($locale));
        }
        return $measures;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];
        return Yii::$app->utility->upperFirstLetter($translation->title);
    }

    /**
     * @param null $type
     * @param string $entity
     * @param array $filters
     * @return array|SeoAdvanced|null|ActiveRecord
     */
    public function getMeta($type = null, $entity = 'category', $filters = [], $request = null)
    {
        if ($type !== null && $this->_meta === null) {
            $this->_meta = SeoAdvanced::find()->where(['entity' => $entity, 'entity_content' => $this->id, 'type' => $type])->one();

            if ($this->_meta !== null) {
                $data = json_decode($this->_meta->custom_data, true);
                $data = is_array($data) ? $data : [];
                if (array_key_exists('randomChildrenCategories', $data)) {
                    $childrenCats = $this->children(1)->joinWith(['translation'])->select('content_translation.title')->asArray()->column();
                    shuffle($childrenCats);
                    $parts = array_slice($childrenCats, 0, 5);
                    $data['randomChildrenCategories'] = implode(', ', $parts);
                }
                $data['category'] = preg_replace("/[,.;]/", "", $this->translation->title);
                if ($this->lvl === 2) {
                    $data['parentCategory'] = preg_replace("/[,.;]/", "", $this->parents(1)->one()->translation->title);
                }

                $data['city'] = $data['attributes'] = $data['tags'] = '';

                $tags = $filters[36] ?? $filters[48] ?? [];
                if (!empty($tags) && !empty($tags['values'])) {
                    $tagValues = implode(', ',
                        array_map(
                            function($var){return Yii::$app->utility->upperFirstLetter($var['title']);},
                            array_slice($tags['values'], 0, 3)
                        )
                    );
                    $data['tags'] = " ($tagValues)";
                }

                $cityParam = ArrayHelper::remove($filters, 42);

//                if(!empty($city) && !empty($city['checked'])) {
//                    $value = $city['values'][$city['checked']]['title'] ?? '';
//                    $data['city'] = Yii::t('seo', ' in {city}', ['city' => $value]);
//                    $data['City'] = " | $value";
//                }
                if(!empty($cityParam) && !empty($cityParam['checked'])) {
                    /** @var AttributeValue $city */
                    $city = AttributeValue::find()->where(['attribute_id' => 42, 'alias' => $cityParam['checked']])->one();
                    if ($city) {
                        $data['city'] = Yii::t('seo', ' in {city}', ['city' => $city->getTitle()]);
                        $data['City'] = " | " . $city->getTitle();
                    }
                }

                $selectedAttributes = array_filter(
                    array_map(function($attr){
                        $result = '';
                        if (!empty($attr['checked'])) {
                            $checked = is_array($attr['checked']) ? $attr['checked'] : [$attr['checked']];
                            $checkedValues = [];
                            foreach ($checked as $item) {
                                if (!empty($attr['values'][$item]['title'])) {
                                    $checkedValues[] = Yii::$app->utility->upperFirstLetter($attr['values'][$item]['title']);
                                }
                            }
                            if ($checkedValues) {
//                                $title = ($attr['alias'] === 'tag' ? Yii::t('app', 'Tags') : $attr['title']);
                                $result = implode(', ', $checkedValues);
                            }
                        }
                        return $result;
                    }, $filters)
                );
                if (count($selectedAttributes)) {
                    $data['attributes'] = Yii::t('seo', ' with params {attributes}', ['attributes' => implode('; ', $selectedAttributes)]);
                    $data['Attributes'] = " | " . implode(' | ', $selectedAttributes);
                    $data['tattributes'] = ". " . implode('. ', $selectedAttributes);
                    $data['kattributes'] = ", " . implode(', ', $selectedAttributes);
                }
                if (!empty($request)) {
//                    $data['request'] = ". " . Yii::t('seo', 'Search by request {request}', ['request' => $request]);
                    $data['request'] = ". " . Yii::$app->utility->upperFirstLetter($request);
                    $data['Request'] = " | " . Yii::$app->utility->upperFirstLetter($request);
                    $data['kequest'] = ", $request";
                }

                $this->_meta->applyTemplates($data);
            }
        }

        return $this->_meta;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $oldPhoto = null;
            if (!$insert) {
                $oldPhoto = $this->getOldAttribute('image');
            }

            if ($insert && empty($this->alias)) {
                $this->alias = Yii::$app->utility->transliterate($this->name);
            }

            $file = new UploadForm();
            $file->path = '/uploads/category/';

            if ($file->imageFiles = UploadedFile::getInstancesByName('Category[image]')) {
                $mediaLayer = Yii::$app->get('mediaLayer');
                $mediaLayer->removeMedia($oldPhoto);

                $res = $file->upload();
                $this->image = array_pop($res);
            } else {
                $this->image = $oldPhoto;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (Yii::$app instanceof Application) {
            $input = Yii::$app->request->post();

            if (isset($input['ContentTranslation'])) {
                foreach ($input['ContentTranslation'] as $key => $value) {
                    $translate = ContentTranslation::findOrCreate(['entity' => 'category', 'entity_id' => $this->id, 'locale' => $value['locale']], false);
                    $translate->attributes = $value;
                    $this->translationsArr[] = $translate;
                }
            }
        }

        if (!empty($this->translationsArr)) {
            foreach ($this->translationsArr as $key => $item) {
                /* @var ContentTranslation $item */
                $item->slug = $this->defineSlug($item);
                $item->entity = 'category';
                $item->entity_id = $this->id;
                $item->save();
            }
        }
    }

    /**
     * @param ContentTranslation $translation
     * @return null|string
     */
    protected function defineSlug(ContentTranslation $translation)
    {
        $result = null;

        if (empty($translation->slug)) {
            $slug = Yii::$app->utility->transliterate($translation->title);
            $dup = ContentTranslation::find()->where(['slug' => $slug, 'entity' => 'category', 'locale' => $translation->locale])->one();
            if ($dup !== null) {
                $parent = $this->parents(1)->joinWith(['translation'])->one();
                if ($parent !== null) {
                    $result = "{$parent->translation->slug}-{$slug}";
                }
            } else {
                $result = $slug;
            }
        } else {
            $result = $translation->slug;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        if (parent::afterDelete()) {
            ContentTranslation::deleteAll(['entity_id' => $this->id, 'entity' => 'category']);

            $mediaLayer = Yii::$app->get('mediaLayer');
            $mediaLayer->removeMedia($this->image);

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getNextLevelChildren()
    {
        $children = $this
            ->children(1)
            ->joinWith('translation')
            ->select('name, lvl, category.id, alias, content_translation.title')
            ->asArray()
            ->all();
        return $children;
    }

    /**
     * @return array
     */
    public function getParents()
    {
        $children = $this
            ->parents()
            ->joinWith('translation')
            ->select('name, lvl, category.id, alias, content_translation.title')
            ->asArray()
            ->all();

        return $children;
    }

    /**
     * @return array
     */
    public function getParent()
    {
        return $this->parents(1)->one();
    }

    /**
     * @return array
     */
    public function getChildrenIds()
    {
//        $childrenIds = [];
//        $children = $this->getCachedChildren();
//
//        if (is_array($children)) {
//            $childrenIds = array_column($children, 'id');
//        }
//
//        return $childrenIds;

        return $this
            ->children(1)
            ->select('id')
            ->asArray()
            ->column();
    }

    /**
     * @return array
     */
    public function getCachedChildren()
    {
//        $cache = Yii::$app->get('cacheLayer');
//
//        switch ($this->type) {
//            case 'job_category':
//            case 'service_category':
//                $path = $cache->get('category_path');
//                break;
//            case 'product_category':
//                $path = $cache->get('category_product_path');
//                break;
//            default:
//                $path = null;
//        }
//
//        if ($path != null) {
//            $pathItem = $path[$this->id];
//
//            return isset($pathItem['children']) ? $pathItem['children'] : [];
//        }

        return $this->getAllChildren();
    }

    /**
     * @return array
     */
    public function getAllChildren()
    {
        return $this
            ->children()
            ->joinWith('translation')
            ->select('name, lvl, category.id, alias, content_translation.title')
            ->asArray()
            ->all();
    }

    /**
     * @return string
     */
    public function getThumb()
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($this->image && file_exists(Yii::getAlias("@frontend") . "/web" . $this->image)) {
            $image = $this->image;
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image);
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];

        return [
            'title' => !empty($translation->seo_title) ? $translation->seo_title : $this->getLabel(),
            'description' => $translation->seo_description ?? '',
            'keywords' => $translation->seo_keywords ?? '',
        ];
    }

    /**
     * Updates related CategoryElastic
     * @param bool $doSave
     * @return CategoryElastic|bool
     */
    public function updateElastic($doSave = true)
    {
        $categoryElasticService = new CategoryElasticService($this);
        return $categoryElasticService->process($doSave);
    }
}