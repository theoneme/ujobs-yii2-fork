<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2017
 * Time: 12:20
 */

namespace common\models;

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeToGroup;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\base\DynamicModel;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * Class DynamicForm
 * @package common\models
 */
class DynamicForm extends DynamicModel
{
    /**
     * @var array
     */
    private $_labels = [];

    /**
     * @var array
     */
    private $_types = [];

    /**
     * @param Category $category
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAttributesListByCategory(Category $category)
    {
        $attributes = [];

        $attributeSetId = $category->attribute_set_id;
        if ($attributeSetId) {
            $attributeSetAttributes = AttributeToGroup::find()->where(['attribute_group_id' => $attributeSetId])->select('attribute_id')->orderBy('sort_order asc')->column();
            $attributes = Attribute::find()
                ->joinWith(['translations'])
                ->groupBy('mod_attribute.id')
                ->where(['mod_attribute.id' => $attributeSetAttributes])
                ->orderBy([new Expression('FIELD (mod_attribute.id, ' . implode(',', $attributeSetAttributes) . ') asc')])
                ->all();
        }

        return $attributes;
    }

    /**
     * @param Attribute[] $attributes
     * @param array $values
     * @return DynamicForm
     */
    public static function initModelFromGroup(array $attributes, array $values)
    {
        $model = new DynamicForm();

        if (!empty($attributes)) {
            foreach ($attributes as $attribute) {
                $model->defineAttribute("attribute_{$attribute->id}");
                $model->defineLabel("attribute_{$attribute->id}", $attribute->title);

                $model->_types["attribute_{$attribute->id}"] = $attribute->type;

                $model->addRule("attribute_{$attribute->id}", 'safe');
            }
        }

        $model->attributes = $values;

        return $model;
    }

    /**
     * @param $name
     * @param null $value
     */
    public function defineLabel($name, $value = null)
    {
        $this->_labels[$name] = $value;
    }

    /**
     * @param array $values
     * @return array
     */
    public static function formatValuesArray(array $values = [])
    {
        $result = [];

        if (!empty($values)) {
            foreach ($values as $value) {
                if (array_key_exists("attribute_{$value->attribute_id}", $result)) {
                    $result["attribute_{$value->attribute_id}"] = (array)$result["attribute_{$value->attribute_id}"];
                    $result["attribute_{$value->attribute_id}"][] = self::getValueByType($value);
                } else {
                    $result["attribute_{$value->attribute_id}"] = self::getValueByType($value);
                }
            }
        }

        return $result;
    }

    /**
     * @param $value
     * @return null
     */
    public static function getValueByType($value)
    {
        $result = null;

        switch ($value->attr->type) {
            case 'radiolist':
            case 'radio':
            case 'checkboxlist':
            case 'dropdown':
            case 'list':
                $result = Html::encode($value->value);
                break;
            case 'textbox':
            case 'multitextbox':
            case 'numberbox':
                $translations = $value->attrValueDescriptions;
                $result = $translations[Yii::$app->language] ? $translations[Yii::$app->language]->title : null;
                break;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return $this->_labels;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasAttribute($name)
    {
        return isset($this->attributes[$name]);
    }

    /**
     * @param $fieldKey
     * @return bool
     */
    public function isMultiValueField($fieldKey)
    {
        $result = false;

        if(array_key_exists($fieldKey, $this->_types)) {
            $type = $this->_types[$fieldKey];

            switch($type) {
                case 'multitextbox':
                    $result = true;
                    break;
            }
        }

        return $result;
    }
}