<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "job_search_step".
 *
 * @property integer $id
 * @property integer $tree_id
 * @property integer $previous_step_id
 * @property boolean $show_prices
 *
 * @property JobSearchOption[] $options
 * @property JobSearchStep $previousStep
 * @property JobSearchTree $tree
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property ContentTranslation $measureTranslation
 * @property ContentTranslation[] $measureTranslations
 *
 * @method bind($relationName = null, $id = null, $index = null)
 * @method validateWithRelations()
 */
class JobSearchStep extends ActiveRecord
{
    /**
     * @var array
     */
    public $translationsData = [];

    /**
     * @var array
     */
    public $measureTranslationsData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_search_step';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'measureTranslations'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tree_id', 'previous_step_id'], 'integer'],
            [['previous_step_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobSearchStep::class, 'targetAttribute' => ['previous_step_id' => 'id']],
            [['tree_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobSearchTree::class, 'targetAttribute' => ['tree_id' => 'id']],
            ['show_prices', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'tree_id' => Yii::t('model', 'Tree ID'),
            'previous_step_id' => Yii::t('model', 'Previous Step ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(JobSearchOption::class, ['step_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousStep()
    {
        return $this->hasOne(JobSearchStep::class, ['id' => 'previous_step_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTree()
    {
        return $this->hasOne(JobSearchTree::class, ['id' => 'tree_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jss_ct.entity' => 'job_search_step'])->from('content_translation jss_ct')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jss_ct.entity' => 'job_search_step'])->from('content_translation jss_ct')->andWhere(['jss_ct.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasureTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jss_ctm.entity' => 'job_search_step_measure'])->from('content_translation jss_ctm')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasureTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jss_ctm.entity' => 'job_search_step_measure'])->from('content_translation jss_ctm')->andWhere(['jss_ctm.locale' => Yii::$app->language]);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->translation->title;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->translation->content_prev;
    }

    /**
     * @return string
     */
    public function getMeasureTitle()
    {
        return isset($this->measureTranslation->title) && !empty($this->measureTranslation->title) ? $this->measureTranslation->title : null;
    }

    /**
     * @return string
     */
    public function getMeasureSubtitle()
    {
        return isset($this->measureTranslation->content_prev) && !empty($this->measureTranslation->content_prev) ? $this->measureTranslation->content_prev : null;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translation->content;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'job_search_step';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
        }
        if (!empty($this->measureTranslationsData)) {
            foreach ($this->measureTranslationsData as $item) {
                if (!empty($item['title'])) {
                    $item['entity'] = 'job_search_step_measure';
                    $this->bind('measureTranslations', (int)$item['id'] ?? null)->attributes = $item;
                }
            }
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        ContentTranslation::deleteAll(['entity' => 'job_search_step', 'entity_id' => $this->id]);
        ContentTranslation::deleteAll(['entity' => 'job_search_step_measure', 'entity_id' => $this->id]);
        $optionIds = array_map(function($var){return $var->id;}, $this->options);
        ContentTranslation::deleteAll(['entity' => 'job_search_option', 'entity_id' => $optionIds]);
        Attachment::deleteAll(['entity' => 'job_search_option', 'entity_id' => $optionIds]);
        return parent::beforeDelete();
    }
}
