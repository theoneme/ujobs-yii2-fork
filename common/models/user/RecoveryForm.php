<?php

namespace common\models\user;

use dektrium\user\models\RecoveryForm as BaseRecoveryForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class RecoveryForm
 * @package common\models\user
 */
class RecoveryForm extends BaseRecoveryForm
{
    /**
     * @var string
     */
    public $confirm;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => Yii::t('model', 'New Password'),
            'confirm' => Yii::t('model', 'Confirm Password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_REQUEST => ['email'],
            self::SCENARIO_RESET => ['password', 'confirm'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['confirm', 'required'],
            ['confirm', 'compare', 'compareAttribute' => 'password'],
        ]);
    }
}
