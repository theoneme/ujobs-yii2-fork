<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 18:22
 */

namespace common\models\user;

use dektrium\user\models\LoginForm as BaseLoginForm;

/**
 * Class LoginForm
 * @package common\models\user
 */
class LoginForm extends BaseLoginForm
{
    public $rememberMe = true;

    /** @inheritdoc */
    public function beforeValidate()
    {
        $this->user = User::findUserByPhoneOrEmail(trim($this->login));
        return true;
    }
}