<?php

namespace common\models\user;

use dektrium\user\models\Token as BaseToken;
use dektrium\user\traits\ModuleTrait;
use yii\helpers\Url;

/**
 * Class Token
 * @package common\models\user
 */
class Token extends BaseToken
{
    use ModuleTrait;

    const TARGET_EMAIL = 0;
    const TARGET_PHONE = 10;
    const TYPE_CONFIRMATION = 0;
    const TYPE_RECOVERY = 1;
    const TYPE_CONFIRM_NEW = 2;
    const TYPE_CONFIRM_OLD = 3;
    public $target;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne($this->module->modelMap['User'], ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        switch ($this->type) {
            case self::TYPE_CONFIRMATION:
                $route = '/user/registration/confirm';
                break;
            case self::TYPE_RECOVERY:
                $route = '/user/recovery/reset';
                break;
            case self::TYPE_CONFIRM_NEW:
            case self::TYPE_CONFIRM_OLD:
                $route = '/user/settings/confirm';
                break;
            default:
                throw new \RuntimeException();
        }

        return Url::to([$route, 'id' => $this->user_id, 'code' => $this->code], true);
    }

    /**
     * @return bool Whether token has expired.
     */
    public function getIsExpired()
    {
        switch ($this->type) {
            case self::TYPE_CONFIRMATION:
            case self::TYPE_CONFIRM_NEW:
            case self::TYPE_CONFIRM_OLD:
                $expirationTime = $this->module->confirmWithin;
                break;
            case self::TYPE_RECOVERY:
                $expirationTime = $this->module->recoverWithin;
                break;
            default:
                throw new \RuntimeException();
        }

        return ($this->created_at + $expirationTime) < time();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->target == self::TARGET_PHONE) {
            if ($insert) {
                static::deleteAll(['user_id' => $this->user_id, 'type' => $this->type]);
                $this->setAttribute('created_at', time());
                $this->setAttribute('code', random_int(1000, 9999));
            }
            return true;
        } else {
            return parent::beforeSave($insert);
        }
    }
}
