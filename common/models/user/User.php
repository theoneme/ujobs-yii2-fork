<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 18:22
 */

namespace common\models\user;

use common\components\CurrencyHelper;
use common\components\Sms;
use common\models\Company;
use common\models\CompanyMember;
use common\models\EmailNotification;
use common\models\Job;
use common\models\Notification;
use common\models\PaymentHistory;
use common\models\PropertyClient;
use common\models\Referral;
use common\models\Subscription;
use common\models\UserAttribute;
use common\models\UserCategory;
use common\models\UserFavourite;
use common\models\UserFavouriteProduct;
use common\models\UserFriend;
use common\models\UserPortfolio;
use common\models\UserTariff;
use common\models\UserWallet;
use common\models\WithdrawalWallet;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\board\models\Product;
use common\services\UserAccessService;
use common\traits\ModuleTrait;
use dektrium\user\helpers\Password;
use dektrium\user\Mailer;
use dektrium\user\models\User as BaseUser;
use MaxMind\Db\Reader;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\UserEvent;

/**
 * Class User
 *
 * @property string $phone
 * @property string $admin_comment
 * @property integer $disable_reason_id
 * @property boolean $disabled
 * @property integer $access
 * @property integer $jobsCount
 * @property integer $balance
 * @property integer $invited_by
 * @property integer $telegram_uid
 * @property string $skype_uid
 * @property string $skype_hash
 * @property string $push_key
 * @property string $push_token
 * @property string $push_id
 * @property integer $last_action_at
 * @property string $site_language
 * @property integer $company_user_id
 * @property boolean $is_company
 *
 * @property Profile $profile
 * @property User $invitedBy
 * @property Account[] $socials
 * @property UserFavourite[] $favourites
 * @property UserFavouriteProduct[] $favouriteProducts
 * @property User[] $invitedUsers
 * @property Token $token
 * @property UserTariff $activeTariff
 * @property UserAttribute[] $userAttributes
 * @property Job[] $jobs
 * @property Product[] $products
 * @property UserWallet[] $wallets
 * @property Subscription[] $subscriptions
 * @property WithdrawalWallet[] $withdrawalWallets
 * @property User $companyUser
 * @property Company $company
 * @property CompanyMember[] $companyMembers
 */
class User extends BaseUser
{
    use ModuleTrait;

    const ROLE_ADMIN = 10;
    const ROLE_MODERATOR = 8;
    public static $usernameRegexp = '/^[-a-zA-Zа-яА-ЯєЄіІїЇёЁ0-9_\.@+\(\)]+$/u';
    public $jobsCount = null;
    public $skillsCount = null;

    const REASON_OWN_ENTITY = 1;

    public $type;

    /**
     * Finds a user by the given phone or email.
     *
     * @param string $phoneOrEmail Phone or email to be used on search.
     *
     * @return User
     */
    public static function findUserByPhoneOrEmail($phoneOrEmail)
    {
        if (filter_var($phoneOrEmail, FILTER_VALIDATE_EMAIL)) {
            return User::findUserByEmail($phoneOrEmail);
        }
        return User::findUserByPhone($phoneOrEmail);
    }

    /**
     * Finds a user by the given phone.
     *
     * @param string $email Email to be used on search.
     *
     * @return User
     */
    public static function findUserByEmail($email)
    {
        return User::find()->where(['email' => $email])->one();
    }

    /**
     * Finds a user by the given phone.
     *
     * @param string $phone Phone to be used on search.
     *
     * @return User
     */
    public static function findUserByPhone($phone)
    {
        $phone = "+" . preg_replace("/[^0-9]/", "", $phone);
        return User::find()->where(['phone' => $phone])->one();
    }

    /**
     * @param UserEvent $event
     */
    public static function beforeLogin($event)
    {
        Yii::$app->cart->reassign(Yii::$app->session->getId(), $event->identity->getId());
        //LiveDialogMember::reassign(Yii::$app->session->getId(), $event->identity->getId());
    }

    public function rules()
    {
        $rules = parent::rules();
        unset($rules['usernameRequired']);
        unset($rules['emailRequired']);
        unset($rules['usernameUnique']);
        $rules[] = [['jobsCount', 'skillsCount', 'balance', 'access'], 'integer'];
        $rules[] = ['phone', 'string', 'min' => 9, 'max' => 15];
        $rules[] = ['phone', 'unique', 'message' => Yii::t('app', 'This phone has already been taken')];
        $rules[] = [['phone', 'email'], 'phoneOrEmail', 'skipOnEmpty' => false];
        $rules[] = ['disabled', 'boolean'];
        $rules[] = ['admin_comment', 'string', 'max' => 65535];
        $rules[] = ['balance', 'number', 'min' => 0];
        $rules[] = ['invited_by', 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => 'id'];
        $rules[] = ['telegram_uid', 'integer'];
        $rules[] = ['skype_uid', 'string'];
        $rules[] = ['skype_hash', 'string'];
        $rules[] = [['push_token', 'push_key'], 'string', 'max' => 100];
        $rules[] = ['push_id', 'string', 'max' => 255];
        $rules[] = ['type', 'in', 'range' => [
            RegisterForm::TYPE_REALTOR
        ]];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['phone'] = Yii::t('model', 'Phone');
        $labels['admin_comment'] = Yii::t('model', 'Admin comment');
        return $labels;
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function phoneOrEmail($attribute, $param)
    {
        if (empty($this->email) && empty($this->phone)) {
            $this->addError($attribute, Yii::t('user', 'Phone or Email must be set'));
        }
    }

    /**
     * This method is used to register new user account. If Module::enableConfirmation is set true, this method
     * will generate new confirmation token and use mailer to send it to the user.
     *
     * @return bool
     */
    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
//            $this->confirmed_at = $this->module->enableConfirmation ? null : time();
            $this->password = $this->module->enableGeneratingPassword ? Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_REGISTER);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }
            $target = !empty($this->phone) ? Token::TARGET_PHONE : Token::TARGET_EMAIL;
            if ($this->module->enableConfirmation) {
                /** @var Token $token */
                $token = \Yii::createObject([
                    'class' => Token::class,
                    'type' => Token::TYPE_CONFIRMATION,
                    'target' => $target
                ]);
                $token->link('user', $this);
            }

            try {
                $mailSent = $this->mailer->sendWelcomeMessage($this, isset($token) ? $token : null);
            } catch (\Swift_TransportException $e) {
                $mailSent = false;
            }
            $mailSent = true;

            if ($this->module->enableConfirmation) {
                if ($mailSent) {
                    if ($target == Token::TARGET_EMAIL) {
                        Yii::$app->session->setFlash('success', Yii::t('account', 'Message with further instructions has been sent to your email'));
                    } else {
                        Yii::$app->session->setFlash('success', Yii::t('account', 'Sms with confirmation code has been sent to your phone'));
                        Yii::$app->session->set('confirmUserId', $this->id);
                    }
                } else {
                    Yii::$app->session->setFlash('warning', Yii::t('account', 'Error occurred during sending message. You can confirm your account later in personal cabinet'));
                }
            }
            $this->trigger(self::AFTER_REGISTER);
            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::warning($e->getMessage());
            return false;
        }
    }

    /**
     * Attempts user confirmation.
     *
     * @param string $code Confirmation code.
     *
     * @return boolean
     */
    public function attemptConfirmation($code)
    {
        $token = $this->finder->findTokenByParams($this->id, $code, Token::TYPE_CONFIRMATION);

        if ($token instanceof Token && !$token->isExpired) {
            $token->delete();
            if (($success = $this->confirm())) {
                if (isGuest()) {
                    Yii::$app->user->login($this, $this->module->rememberFor);
                    $message = \Yii::t('user', 'Thank you, registration is now complete.');
                } else {
                    $message = \Yii::t('account', 'Your account is successfully confirmed.');
                }
            } else {
                $message = \Yii::t('user', 'Something went wrong and your account has not been confirmed.');
            }
        } else {
            $success = false;
            $message = \Yii::t('user', 'The confirmation link is invalid or expired. Please try requesting a new one.');
        }

        \Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);

        return $success;
    }

    /**
     * Generates new username based on email address, or creates new username
     * like "emailuser1".
     */
    public function generateUsername()
    {
        $this->username = !empty($this->email) ? explode('@', $this->email)[0] : $this->phone;
        if (!$this->validate(['username'])) {
            $this->username = 'user' . ($this->finder->userQuery->max('id') + 1);
        }
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->access == self::ROLE_ADMIN;
    }

    /**
     * @return int
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function hasAccess($userId)
    {
        return $this->id === $userId || $this->company_user_id === $userId;
    }

    /**
     * @param Job|Product $tender
     * @return array
     */
    public function isAllowedToRespondOnTender($tender)
    {
        $userAccessService = new UserAccessService($this);
        return $userAccessService->isAllowedToRespondOnTender($tender);
    }

    /**
     * @param User $target
     * @return array
     */
    public function isAllowedToCreateConversation(User $target)
    {
        $userAccessService = new UserAccessService($this);
        return $userAccessService->isAllowedToCreateConversation($target);
    }

    /**
     * @return bool
     */
    public function isAllowedToPost()
    {
        $userAccessService = new UserAccessService($this);
        return $userAccessService->isAllowedToPost();
    }

    /**
     * @return bool
     */
    public function isAllowedToOrder()
    {
        $userAccessService = new UserAccessService($this);
        return $userAccessService->isAllowedToOrder();
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isAllowedToUpdateOrder($userId)
    {
        $userAccessService = new UserAccessService($this);
        return $userAccessService->isAllowedToUpdateOrder($userId);
    }

    /**
     * @return bool
     */
    public function isAllowedToUseWallets()
    {
        $userAccessService = new UserAccessService($this);
        return $userAccessService->isAllowedToUseWallets();
    }

    /**
     * Total user balance from all wallets
     * @return int
     */
    public function getBalance()
    {
        return array_sum(
            array_map(function ($var) {
                /* @var $var UserWallet */
                return $var->currency_code === 'CTY' ? 0 : CurrencyHelper::convert($var->currency_code, Yii::$app->params['app_currency_code'], $var->balance);
            }, $this->company_user_id ? $this->companyUser->wallets : $this->wallets)
        );
    }

    /**
     * Relations
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveTariff()
    {
        return $this->hasOne(UserTariff::class, ['user_id' => 'id'])
            ->andOnCondition(['<', 'starts_at', time()])
            ->andOnCondition(['>', 'expires_at', time()])
            ->andOnCondition(['user_tariff.status' => UserTariff::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(UserTariff::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::class, ['user_id' => 'id'])->onCondition(['type' => 'job']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToken()
    {
        return $this->hasOne(Token::class, ['user_id' => 'id'])->orderBy('created_at desc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenders()
    {
        return $this->hasMany(Job::class, ['user_id' => 'id'])->from(['tenders' => 'job'])->onCondition(['tenders.type' => 'tender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPortfolios()
    {
        return $this->hasMany(UserPortfolio::class, ['user_id' => 'id'])->where(['version' => UserPortfolio::VERSION_NEW]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedBy()
    {
        return $this->hasOne(User::class, ['id' => 'invited_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedUsers()
    {
        return $this->hasMany(User::class, ['invited_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastNotification()
    {
        return $this->hasOne(EmailNotification::class, ['to_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastSimpleNotification()
    {
        return $this->hasOne(Notification::class, ['to_id' => 'id'])->orderBy('id desc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavourites()
    {
        return $this->hasMany(UserFavourite::class, ['user_id' => 'id'])->indexBy('job_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavouriteProducts()
    {
        return $this->hasMany(UserFavouriteProduct::class, ['user_id' => 'id'])->indexBy('product_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAttributes()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriends()
    {
        return $this->hasMany(UserFriend::class, ['user_id' => 'id'])->andWhere(['status' => UserFriend::STATUS_APPROVED]);
    }

    /**
     * @return Account[] Connected accounts
     */
    public function getSocials()
    {
        return $this->hasMany($this->module->modelMap['Account'], ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(UserCategory::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallets()
    {
        return $this->hasMany(UserWallet::class, ['user_id' => 'id'])
            ->orderBy([new Expression("FIELD (user_wallet.currency_code, 'USD', 'EUR', 'RUB', 'UAH', 'CNY', 'GEL', 'CTY') ASC")])
            ->indexBy('currency_code');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWithdrawalWallets()
    {
        return $this->hasMany(WithdrawalWallet::class, ['user_id' => 'id'])->indexBy('type');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(User::class, ['id' => 'company_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMembers()
    {
        return $this->hasMany(CompanyMember::class, ['user_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert === true) {
            if (!empty($this->email) && !$this->is_company && empty($this->profile->public_email)) {
                $this->profile->updateAttributes(['public_email' => $this->email]);
            }
            if (!empty($this->invited_by)) {
                /* @var $referral Referral */
                $referral = Referral::find()->where(['created_by' => $this->invited_by, 'email' => $this->email])->one();
                if ($referral === null) {
                    $referral = new Referral();
                    $referral->email = $this->email;
                    $referral->created_by = $this->invited_by;
                }
                $referral->referral_id = $this->id;
                $referral->status = Referral::STATUS_REGISTERED;
                $referral->save();
            }

            $client_id = Yii::$app->request->cookies->getValue('client_id');
            if ($client_id) {
                /* @var $client PropertyClient */
                $client = PropertyClient::find()->where(['id' => $client_id])->one();
                if ($client !== null) {
                    $client->updateAttributes([
                        'user_id' => $this->id,
                        'status' => PropertyClient::STATUS_REGISTERED
                    ]);
                }
            }

            $bonus = new PaymentHistory([
                'user_id' => $this->id,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_REGISTER_BONUS,
                'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                'amount' => 1,
            ]);
            $bonus->save();

            if (!empty($this->registration_ip) && $this->registration_ip !== '127.0.0.1') {
                $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-City.mmdb');
                $data = $geoIpReader->get($this->registration_ip);
                $country = $data['country']['names']['ru'] ?? $data['country']['names']['en'] ?? null;
                if (isset($countries[$country])) {
                    $countryAttributeId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
                    AttributeValueObjectBuilder::makeAttributeObject(UserAttribute::class, $countryAttributeId, $country, ['locale' => Yii::$app->language]);
                }
            }

            switch((int)$this->type) {
                case RegisterForm::TYPE_REALTOR:
                    $specialtyAttribute = Attribute::find()->where(['alias' => 'specialty'])->one();

                    if($specialtyAttribute !== null) {
                        $attribute = AttributeValueObjectBuilder::makeAttributeObject(UserAttribute::class, $specialtyAttribute->id, 'Риелтор');
                        $attribute->user_id = $this->id;
                        $attribute->save();
                    }

                    break;
            }
        }

        $this->profile->updateElastic();
    }

    /**
     * @return bool
     */
    public function isOnline()
    {
        $onlineVariable = 60 * 35; // 15 minutes

        return $this->last_action_at > time() - $onlineVariable;
    }

    /**
     * @param $entity
     * @param $entity_id
     * @return bool
     */
    public function isSubscribedTo($entity, $entity_id)
    {
        return count(array_filter($this->subscriptions, function ($var) use ($entity, $entity_id) {
            return $var->entity === $entity && $var->entity_id === $entity_id;
        }));
    }

    /**
     * @param $targetId
     * @return bool
     */
    public function hasFriendship($targetId)
    {
        return UserFriend::find()
            ->where([
                'user_id' => $this->id, 'target_user_id' => $targetId, 'status' => UserFriend::STATUS_APPROVED
            ])
            ->exists();
    }

    /**
     * @return Mailer | Sms
     * @throws \yii\base\InvalidConfigException
     */
    protected function getMailer($className = null)
    {
        if ($className !== null) {
            return \Yii::$container->get($className);
        } else {
            if (!empty($this->phone)) {
                return \Yii::$container->get(Sms::class);
            } else {
                return \Yii::$container->get(Mailer::class);
            }
        }
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->is_company ? $this->company->getSellerName() : $this->profile->getSellerName();
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->is_company ? Url::to(['/company/profile/show', 'id' => $this->company->id]) : Url::to(['/account/profile/show', 'id' => $this->id]);
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        return $this->is_company
            ? $this->company->getThumb($target)
            : $this->profile->getThumb($target);
    }

    /**
     * @return int
     */
    public function getCurrentId()
    {
        return $this->company_user_id ?? $this->id;
    }
}