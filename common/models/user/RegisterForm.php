<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 18:22
 */

namespace common\models\user;

use borales\extensions\phoneInput\PhoneInputValidator;
use dektrium\user\models\RegistrationForm;
use GeoIp2\Database\Reader;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Yii;

/**
 * Class RegisterForm
 * @package common\models\user
 */
class RegisterForm extends RegistrationForm
{
    const VALIDATE_AS_EMAIL = 0;
    const VALIDATE_AS_PHONE = 10;

    const TYPE_REALTOR = 10;

    public static $usernameRegexp = '/^[-a-zA-Zа-яА-ЯєЄіІїЇёЁ0-9_\.@+\(\)]+$/u';
    /**
     * Determines how login field must be validated
     * @var integer
     */
    public $validateAs;
    /**
     * @var string User phone
     */
    public $phone;
    /**
     * @var string User login
     */
    public $login;
    /**
     * @var integer
     */
    public $invited_by;
    /**
     * @var integer
     */
    public $type;

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (filter_var($this->login, FILTER_VALIDATE_EMAIL)) {
            $this->validateAs = self::VALIDATE_AS_EMAIL;
        } else {
            $this->validateAs = self::VALIDATE_AS_PHONE;
            $this->login = "+" . preg_replace("/[^0-9]/", "", $this->login);
            $phoneUtil = PhoneNumberUtil::getInstance();
            $valid = false;
            try {
                $phone = $phoneUtil->parse($this->login, null);
                if ($phoneUtil->isValidNumber($phone)) {
                    $valid = true;
                }
            } catch (NumberParseException $e) {
            }

            if (!$valid) {
                $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-Country.mmdb');
                $ip = Yii::$app->request->getUserIP();
//				$ip = '91.246.224.1';
                if ($ip !== '127.0.0.1' && strlen($ip) > 0) {
                    $region = $geoIpReader->country($ip)->country->isoCode;
                    if (!empty($region)) {
                        try {
                            $phone = $phoneUtil->parse(preg_replace("/[^0-9]/", "", $this->login), $region);
                            $this->login = $phoneUtil->format($phone, PhoneNumberFormat::E164);
                        } catch (NumberParseException $e) {
                        }
                    }
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function rules()
    {
        $user = $this->module->modelMap['User'];
        return [
            //username rules
            ['username', 'filter', 'filter' => 'trim'],
//            ['username', 'required'],
            ['username', 'string', 'min' => 3, 'max' => 255],
            ['username', 'match', 'pattern' => static::$usernameRegexp],
            //login rules
            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required'],
            // email rules
            'emailPattern' => ['login', 'email', 'when' => function ($model) {
                return $model->validateAs == self::VALIDATE_AS_EMAIL;
            }],
            'emailUnique' => [
                'login',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This email address has already been taken'),
                'targetAttribute' => 'email',
                'when' => function ($model) {
                    return $model->validateAs == self::VALIDATE_AS_EMAIL;
                }
            ],

            // phone rules
            ['login',
                'string',
                'min' => 9,
                'max' => 15,
                'tooShort' => Yii::t('app', 'Phone must contain from {0} numbers', 9),
                'tooLong' => Yii::t('app', 'Phone must contain up to {0} numbers', 15),
                'when' => function ($model) {
                    return $model->validateAs == self::VALIDATE_AS_PHONE;
                }
            ],
            ['login',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('app', 'This phone has already been taken'),
                'targetAttribute' => 'phone',
                'when' => function ($model) {
                    return $model->validateAs == self::VALIDATE_AS_PHONE;
                }
            ],
            ['login',
                PhoneInputValidator::class,
                'message' => Yii::t('app', 'Enter phone in full format, including county code. Example +7 (123) 456 78 90'),
                'when' => function ($model) {
                    return $model->validateAs == self::VALIDATE_AS_PHONE;
                }
            ],
            // password rules
            ['password', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            ['password', 'string', 'min' => 6, 'max' => 72],

            [['email', 'phone'], 'safe'], // one of them will be filled from login field
            ['invited_by', 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['type', 'in', 'range' => [
                self::TYPE_REALTOR
            ]]
        ];
    }

    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }
        if ($this->validateAs == self::VALIDATE_AS_EMAIL) {
            $this->email = $this->login;
            $this->phone = '';
        } else {
            $this->phone = $this->login;
            $this->email = '';
        }

        /** @var User $user */
        $user = Yii::createObject(User::class);
        $user->setScenario('register');
        $this->loadAttributes($user);
        $user->generateUsername();

        if (!$user->register()) {
            return false;
        }
        Yii::$app->session->setFlash('info', Yii::t('app', 'Your account has been created. Thank you for registering!'));

        return true;
    }
}