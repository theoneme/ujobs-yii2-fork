<?php

namespace common\models\user;

use dektrium\user\models\Account as BaseAccount;

/**
 * Class Account
 * @package common\models\user
 */
class Account extends BaseAccount
{
    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && $this->getIsConnected()) {
            $this->user->profile->setAvatarFromSocialNetwork($this->provider);
        }
    }

    /**
     * @return null|string
     */
    public function getProfileLink()
    {
        switch ($this->provider) {
            case 'facebook':
                if (!empty($this->decodedData['id'])) {
                    return 'https://facebook.com/' . $this->decodedData['id'];
                }
                break;
            case 'google':
                if (!empty($this->decodedData['id'])) {
                    return 'https://plus.google.com/' . $this->decodedData['id'];
                }
                break;
            default:
        }
        return null;
    }
}
