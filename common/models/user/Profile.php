<?php

namespace common\models\user;

use common\behaviors\ImageBehavior;
use common\models\ContentTranslation;
use common\models\elastic\ProfileElastic;
use common\models\elastic\UserAttributeElastic;
use common\models\Event;
use common\models\forms\BaseAttributeForm;
use common\models\forms\UserLanguageForm;
use common\models\forms\UserSkillForm;
use common\models\forms\UserSpecialtyForm;
use common\models\Job;
use common\models\Notification;
use common\models\PaymentHistory;
use common\models\SeoAdvanced;
use common\models\UserAttribute;
use common\models\UserCategory;
use common\models\UserCertificate;
use common\models\UserDeactivation;
use common\models\UserEducation;
use common\models\UserFavourite;
use common\models\UserFavouriteProduct;
use common\models\UserPortfolio;
use common\models\UserTariff;
use common\models\WithdrawalWallet;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use common\services\elastic\ProfileElasticService;
use common\services\GeoIpService;
use common\services\GoogleMapsService;
use dektrium\user\models\Profile as BaseProfile;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseStringHelper;
use yii\helpers\HtmlPurifier;

/**
 * @property string $status_text
 * @property string $country
 * @property boolean $email_notifications
 * @property boolean $sms_notifications
 * @property string $background
 * @property boolean $status
 * @property boolean $message2_flag
 * @property boolean $is_customer
 * @property boolean $is_service_seller
 * @property boolean $is_product_seller
 * @property boolean $is_freelancer
 * @property string $skype
 * @property integer $rating
 * @property boolean $is_store
 * @property string $whatsapp
 * @property string $telegram
 * @property string $viber
 * @property string $facebook_messenger
 * @property string $lat
 * @property string $long
 *
 * @property array $skillsArray
 * @property array $educationsArray
 * @property array $languagesArray
 * @property array $certificatesArray
 *
 * @property User $user
 * @property UserTariff $activeTariff
 * @property UserLanguageForm[] $languages
 * @property UserCertificate[] $certificates
 * @property UserEducation[] $educations
 * @property UserSkillForm[] $skills
 * @property UserPortfolio[] $portfolio
 * @property UserSpecialtyForm[] $specialties
 * @property Job[] $jobs
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property WithdrawalWallet[] $withdrawalWallets
 * @property UserAttribute[] $userAttributes
 */
class Profile extends BaseProfile
{
    const STATUS_REQUIRES_MODIFICATION = 0;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_ACTIVE = 30;
    const STATUS_NOT_WORKER = 40;
    const STATUS_PAUSED = 50;
    const STATUS_DISABLED = 100;

    /**
     * @var array
     */
    public $_skillsArray;
    /**
     * @var array
     */
    public $_educationsArray;
    /**
     * @var array
     */
    public $_languagesArray;
    /**
     * @var array
     */
    public $_certificatesArray;

    /**
     * @var array
     */
    public $locationData;

    public $userSkills = null;
    public $userPortfolio = null;
    public $userLanguages = null;
    public $userSpecialties = null;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'user',
                'imageField' => 'gravatar_email',
                'labelField' => 'sellerName'
            ],
        ]);
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        unset($rules['gravatarEmailPattern']);
        unset($rules['timeZoneValidation']);
        unset($rules['websiteLength']);
        unset($rules['websiteUrl']);
        unset($rules['locationLength']);
        $rules['notification_flags'] = [['email_notifications', 'sms_notifications'], 'boolean'];
        $rules['status_text'] = [['status_text'], 'string', 'max' => 255];
        $rules['country'] = [['country'], 'string', 'max' => 255];
        $rules['background'] = [['background'], 'string', 'max' => 255];
        //$rules['bio_required'] = ['bio', 'required'];
        $rules['safe'] = [['userSkills', 'userLanguages', 'userPortfolio', 'specialties', 'locationData'], 'safe'];
        $rules['status_default'] = ['status', 'default', 'value' => self::STATUS_REQUIRES_MODIFICATION];
        $rules['status'] = ['status', 'in', 'range' => [
            self::STATUS_REQUIRES_MODIFICATION,
            self::STATUS_REQUIRES_MODERATION,
            self::STATUS_ACTIVE,
            self::STATUS_DISABLED,
            self::STATUS_NOT_WORKER
        ]];
        $rules['skype'] = ['skype', 'string', 'max' => 75];
        $rules['rating'] = ['rating', 'integer'];
        $rules['flags'] = [['is_customer', 'is_service_seller', 'is_product_seller', 'is_freelancer'], 'boolean'];
        $rules['store'] = [['is_store'], 'boolean'];
        $rules['store_default'] = ['is_store', 'default', 'value' => false];
        $rules['store_name'] = ['store_name', 'string', 'max' => 55];
        $rules['city'] = [['city'], 'string', 'max' => 255];
        $rules['messengers'] = [['whatsapp', 'viber', 'telegram', 'facebook_messenger'], 'string', 'max' => 55];
        $rules['lat_long'] = [['lat', 'long'], 'string', 'max' => 50];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('model', 'Your Name'),
            'public_email' => Yii::t('model', 'Your Email'),
            'gravatar_email' => Yii::t('user', 'Photo'),
            'bio' => Yii::t('user', 'Bio'),
            'country' => Yii::t('model', 'Country'),
            'status' => Yii::t('model', 'Status'),
            'city' => Yii::t('model', 'City'),
            'whatsapp' => Yii::t('model', 'WhatsApp'),
        ];
    }

    /**
     * @return array
     */
    public function getCertificatesArray()
    {
        if ($this->_certificatesArray === null) {
            $this->_certificatesArray = ArrayHelper::toArray($this->certificates);
        }
        return $this->_certificatesArray;
    }

    /**
     * @return array
     */
    public function getEducationsArray()
    {
        if ($this->_educationsArray === null) {
            $this->_educationsArray = ArrayHelper::toArray($this->educations);
        }
        return $this->_educationsArray;
    }

    /**
     * Relations
     */
    public function getLanguages()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'user_id'])->andWhere(['entity_alias' => 'language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducations()
    {
        return $this->hasMany(UserEducation::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasMany(UserPortfolio::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkills()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'user_id'])->andWhere(['entity_alias' => 'skill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificates()
    {
        return $this->hasMany(UserCertificate::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialties()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'user_id'])->andWhere(['entity_alias' => 'specialty']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['user_id' => 'user_id']);
    }

    /**
     * @return int
     */
    public function getCategoriesCount()
    {
        return count(ArrayHelper::getColumn($this->jobs, 'category_id'));
    }

    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return min(ArrayHelper::getColumn($this->jobs, 'price'));
    }

    /**
     * @return mixed
     */
    public function getMaxPrice()
    {
        return max(ArrayHelper::getColumn($this->jobs, 'price'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAttributes()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCategories()
    {
        return $this->hasMany(UserCategory::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveTariff()
    {
        return $this->hasOne(UserTariff::class, ['user_id' => 'user_id'])
            ->andOnCondition(['<', 'starts_at', time()])
            ->andOnCondition(['>', 'expires_at', time()])
            ->andOnCondition(['user_tariff.status' => UserTariff::STATUS_ACTIVE]);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->activeTariff) {
            return $this->activeTariff->tariff->icon;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->activeTariff) {
            return $this->activeTariff->tariff->title;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(UserCategory::class, ['user_id' => 'user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'user_id'])
            ->from('content_translation pct')
            ->andOnCondition(['pct.entity' => 'profile'])
            ->andOnCondition(['pct.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'user_id'])->andOnCondition(['content_translation.entity' => 'profile'])->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWithdrawalWallets()
    {
        return $this->hasMany(WithdrawalWallet::class, ['user_id' => 'user_id'])->indexBy('type');
    }

    /**
     * @return array
     */
    public function getCategoryIds()
    {
        return ArrayHelper::getColumn($this->categories, 'category_id');
    }

    /**
     * @return int|string
     */
    public function getSoldServicesCount()
    {
        return Order::find()->where([
            'seller_id' => $this->user_id,
            'product_type' => [Order::TYPE_SIMPLE_TENDER, Order::TYPE_TENDER, Order::TYPE_JOB, Order::TYPE_JOB_PACKAGE, Order::TYPE_OFFER, Order::TYPE_PRODUCT],
            'status' => [Order::STATUS_COMPLETED, Order::STATUS_AWAITING_REVIEW, Order::STATUS_TENDER_AWAITING_REVIEW, Order::STATUS_TENDER_COMPLETED]
        ])->count();
    }

    /**
     * @return int|string
     */
    public function getSoldProductsCount()
    {
        return Order::find()->where(['seller_id' => $this->user_id, 'product_type' => Order::TYPE_PRODUCT, 'status' => [Order::STATUS_COMPLETED]])->count();
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->gravatar_email, $target);
    }

    /**
     * @return mixed
     */
    public function getBgThumb()
    {
        $image = '/images/new/bg-def.jpg';
        if ($this->background && (file_exists(Yii::getAlias("@frontend") . "/web" . $this->background) || preg_match('/^http(s)?:\/\/.*/', $this->background))) {
            $image = $this->background;
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image);
    }

    /**
     * @return string
     */
    public function getCity()
    {
        $city = UserAttribute::find()->select('mad_2.title')->joinWith(['attrValueDescription'])->where(['user_id' => $this->user_id, 'entity_alias' => 'city'])->scalar();
        return $city ?? $this->getCityByIp();
    }

    /**
     * @return null|string
     */
    public function getCityByIp()
    {
        $geoIpService = new GeoIpService($this->user->registration_ip);
        return $geoIpService->getCity() ?? Yii::t('model', 'City');
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        $country = UserAttribute::find()->select('mad_2.title')->joinWith(['attrValueDescription'])->where(['user_id' => $this->user_id, 'entity_alias' => 'country'])->scalar();
        return $country ?? (!empty($this->country) ? $this->country : Yii::t('model', 'Country'));
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        /* @var ActiveQuery $cityQuery */
        /* @var ActiveQuery $countryQuery */
        $hasLatLong = !empty($this->lat) && !empty($this->long);
        if ($hasLatLong) {
            $locale = Yii::$app->language;
            $city = UserAttribute::find()
                ->select(['mad_2.title', 'mad_2.locale'])
                ->joinWith(['attrValueDescription'])
                ->where(['user_id' => $this->user_id, 'entity_alias' => 'city', 'mad_2.locale' => $locale])
                ->scalar();
            $country = UserAttribute::find()
                ->select(['mad_2.title', 'mad_2.locale'])
                ->joinWith(['attrValueDescription'])
                ->where(['user_id' => $this->user_id, 'entity_alias' => 'country', 'mad_2.locale' => $locale])
                ->scalar();
            if (!$city || !$country) {
                $gmapsService = new GoogleMapsService();
                $addressData = $gmapsService->reverseGeocodeAsData($this->lat, $this->long, Yii::$app->params['supportedLocales'][$locale]);
                if ($addressData !== false) {
                    if (!$city && isset($addressData['city'])) {
                        $city = $addressData['city'];
//                        $cityAttributeId = UserAttribute::find()->select(['value'])->where(['entity_alias' => 'city', 'user_id' => $this->user_id])->scalar();
//                        if ($cityAttributeId) {
//                            (new AttributeDescription([
//                                'entity' => 'attribute-value',
//                                'entity_content' => $cityAttributeId,
//                                'title' => $city,
//                                'locale' => $locale
//                            ]))->save();
//                        }
                    }
                    if (!$country && isset($addressData['country'])) {
                        $country = $addressData['country'];
//                        $countryAttributeId = UserAttribute::find()->select(['value'])->where(['entity_alias' => 'country', 'user_id' => $this->user_id])->scalar();
//                        if ($countryAttributeId) {
//                            (new AttributeDescription([
//                                'entity' => 'attribute-value',
//                                'entity_content' => $countryAttributeId,
//                                'title' => $country,
//                                'locale' => $locale
//                            ]))->save();
//                        }
                    }
                }
            }
            return $country . ($country && $city ? ", {$city}" : $city);
        } else {
            return $this->getFromOld();
        }
    }

    /**
     * @return string
     */
    public function getFromOld()
    {
        $locale = Yii::$app->language;
        $city = UserAttribute::find()
            ->select(['mad_2.title', 'mad_2.locale'])
            ->joinWith(['attrValueDescription'])
            ->where(['user_id' => $this->user_id, 'entity_alias' => 'city'])
            ->orderBy(new Expression("FIELD (mad_2.locale, '{$locale}') DESC"))
            ->scalar();
        $city = $city ?? (new GeoIpService($this->user->registration_ip))->getCity();
        $country = UserAttribute::find()
            ->select(['mad_2.title', 'mad_2.locale'])
            ->joinWith(['attrValueDescription'])
            ->where(['user_id' => $this->user_id, 'entity_alias' => 'country'])
            ->orderBy(new Expression("FIELD (mad_2.locale, '{$locale}') DESC"))
            ->scalar();
        $country = $country ?? $this->country;
        return $country . ($country && $city ? ", {$city}" : $city);
    }

    /**
     * @param $provider
     */
    public function setAvatarFromSocialNetwork($provider)
    {
        if (empty($this->gravatar_email) && isset($this->user->accounts[$provider])) {
            switch ($provider) {
                case 'facebook':
                    if (!empty($this->user->accounts[$provider]->decodedData['picture'])) {
                        $this->updateAttributes([
//                            'gravatar_email' => $this->user->accounts[$provider]->decodedData['picture']['data']['url']
                            'gravatar_email' => 'https://graph.facebook.com/' . $this->user->accounts[$provider]->client_id . '/picture?width=600&height=600'
                        ]);
                    }
                    break;
                case 'google':
                    if (!empty($this->user->accounts[$provider]->decodedData['image']['url'])) {
                        $image = str_replace("sz=50", "sz=600", $this->user->accounts[$provider]->decodedData['image']['url']);
                        $this->updateAttributes([
                            'gravatar_email' => $image,
                            'name' => $this->user->accounts[$provider]->decodedData['displayName']
                        ]);
                    }
                default:
            }
        }
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->store_name ?? $this->getSellerName();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->translation->title ?? (!empty($this->name) ? $this->name : $this->user->username);
    }

    /**
     * @param bool|false $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_NOT_WORKER => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Not worker') . '</span>' : Yii::t('labels', 'Not worker')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
        ];
    }

    /**
     * @return string
     */
    public function getBio()
    {
        return Yii::$app->utility->upperFirstLetter(BaseStringHelper::truncate(strip_tags($this->getDescription()), 50, '...', 'UTF-8'));
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0] ?? null;
        return $translation->content ?? $this->bio;
    }

    /**
     * Updates related ProfileElastic
     * @return boolean
     */
    public function updateElasticTariff()
    {
        $profileElasticService = new ProfileElasticService($this);
        return $profileElasticService->updateTariff();
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $templateCategory = SeoAdvanced::TEMPLATE_PRO;
        $templates = SeoAdvanced::getDefaultTemplates($templateCategory);
        $params = [
            'title' => $this->user->getSellerName()
        ];

        return [
            'title' => Yii::t('seo', $templates['title'], $params, Yii::$app->language),
            'description' => Yii::t('seo', $templates['description'], $params, Yii::$app->language),
            'keywords' => ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->email_notifications = true;
            $this->sms_notifications = true;
        }

        if ($this->status == self::STATUS_ACTIVE && $this->getOldAttribute('status') != self::STATUS_ACTIVE && $this->email_notifications == true) {
            if (!empty($this->user->email)) {
                $notification = new Notification([
                    'to_id' => $this->user_id,
                    'params' => [
                        'subject' => Yii::t('notifications', 'Your profile has passed moderation on {site}', ['site' => Yii::$app->name], $this->user->site_language),
                        'content' => Yii::t('notifications', 'Your profile has passed moderation and has been added to professional`s catalog. If you want more chances to be found and receive orders, please post your jobs.', [], $this->user->site_language),
                        'user2' => $this->user
                    ],
                    'template' => Notification::TEMPLATE_PROFILE_MODERATED,
                    'linkRoute' => ['/entity/global-create'],
                    'withEmail' => true,
                    'is_visible' => true
                ]);
                $notification->save();
            }
        }

        $this->status_text = HtmlPurifier::process($this->status_text);
        $this->bio = HtmlPurifier::process($this->bio);
        if ($this->bio != '') {
            $this->bio = str_replace([' ,', ' .', '( ', ' )', '  '], [', ', '. ', '(', ')', ' '], $this->bio);
            $this->bio = preg_replace('/([.,;!?]+)/', '$1 ', $this->bio);
        }
        $this->name = HtmlPurifier::process($this->name);
        $this->public_email = HtmlPurifier::process($this->public_email);

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\base\InvalidConfigException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (is_array($this->userSkills)) {
            $this->updateRelated(UserSkillForm::class, $this->userSkills, 'skill');
        }
        if (is_array($this->userLanguages)) {
            $this->updateRelated(UserLanguageForm::class, $this->userLanguages, 'language');
        }
        if (is_array($this->userSpecialties)) {
            $this->updateRelated(UserSpecialtyForm::class, $this->userSpecialties, 'specialty');
        }
        $this->updateAddressAttributes();

        if (!defined('YII_BACKEND')) {
            $this->defineStatus();
        }
        $this->defineRating();
        $this->updateElastic();
//        foreach ($this->jobs as $job) {
//            $job->updateElastic();
//        }
    }

    /**
     * @return bool
     */
    public function updateAddressAttributes()
    {
        if (!empty($this->locationData) && !empty($this->locationData['city']) && !empty($this->locationData['country'])) {
            $countryAttributeId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
            $cityAttributeId = Attribute::find()->where(['alias' => 'city'])->select('id')->scalar();
            $countryObject = UserAttribute::findOne(['attribute_id' => $countryAttributeId, 'user_id' => $this->user_id]) ?? new UserAttribute(['user_id' => $this->user_id]);
            $countryObject->setAttributes(
                array_filter(
                    AttributeValueObjectBuilder::makeAttributeObject(
                        UserAttribute::class,
                        $countryAttributeId,
                        $this->locationData['country'],
                        ['locale' => Yii::$app->language]
                    )->attributes
                )
            );
            $countryObject->save();
            $this->locationData['city'] = str_replace(['город ', 'село ', 'поселок ', 'станица ', 'поселок городского типа '], '', $this->locationData['city']);
            $cityObject = UserAttribute::findOne(['attribute_id' => $cityAttributeId, 'user_id' => $this->user_id]) ?? new UserAttribute(['user_id' => $this->user_id]);
            $cityObject->setAttributes(
                array_filter(
                    AttributeValueObjectBuilder::makeAttributeObject(
                        UserAttribute::class,
                        $cityAttributeId,
                        $this->locationData['city'],
                        ['locale' => Yii::$app->language, 'parent_value_id' => $countryObject->value, 'status' => AttributeValue::STATUS_APPROVED]
                    )->attributes
                )
            );
            $cityObject->save();

            return true;
        }

        return false;
    }

    /**
     * @param BaseAttributeForm $className
     * @param array $data
     * @param $entity_alias
     */
    protected function updateRelated($className = null, $data = [], $entity_alias)
    {
        /* @var UserSkillForm $className */
        if ($className != null) {
            $ids = [];
            foreach ($data as $item) {
                $object = !empty($item['id']) ? $className::findOne(['user_attribute.id' => $item['id'], 'user_id' => $this->user_id, 'entity_alias' => $entity_alias]) : new $className;
                $object->attributes = $item;
                $object->user_id = $this->user_id;
                $object->save();
                $ids[] = $object->id;
            }

            $related = $className::findAll(['and', ['user_id' => $this->user_id, 'entity_alias' => $entity_alias], ['not', ['user_attribute.id' => array_values($ids)]]]);

            foreach ($related as $r) {
                $r->delete();
            }
        }
    }

    /**
     *
     */
    public function defineStatus()
    {
        if (in_array($this->status, [self::STATUS_ACTIVE, self::STATUS_PAUSED, self::STATUS_REQUIRES_MODERATION, self::STATUS_REQUIRES_MODIFICATION])) {
            $letters = 'a-zA-Zа-яА-Яა-ჸєЄіІїЇёЁÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüŸ¡¿çÇŒœßØøÅåÆæÞþÐð';
            $pattern = '/^(\d{0,8}|[' . $letters . ']+)([\s\.\?,!;:\-#№%\*\(\)\"\'\/\\=+~<>–—✔“”]+(\d{0,8}|[' . $letters . ']+)){2,}[\s\.!\?]*$/u';
            if (!empty($this->gravatar_email) && !empty($this->bio) && preg_match($pattern, $this->bio) && count($this->skills)) {
                $this->updateAttributes(['status' => self::STATUS_ACTIVE]);

                if (!Event::find()->where(['code' => Event::PROFILE_MODERATED, 'user_id' => $this->user_id])->exists()) {
                    (new Event(['code' => Event::PROFILE_MODERATED, 'user_id' => $this->user_id]))->save();
                    $bonus = new PaymentHistory([
                        'user_id' => $this->user_id,
                        'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                        'template' => PaymentHistory::TEMPLATE_PAYMENT_PROFILE_POST_BONUS,
                        'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                        'amount' => 5,
                    ]);
                    $bonus->save();
                }
            } elseif (!empty($this->gravatar_email) && ((!empty($this->bio) && preg_match($pattern, $this->bio)) || count($this->skills))) {
                $this->updateAttributes(['status' => self::STATUS_REQUIRES_MODERATION]);
            } else {
                $this->updateAttributes(['status' => self::STATUS_REQUIRES_MODIFICATION]);
            }
        }
    }

    /**
     *
     */
    public function defineRating()
    {
        if (!empty($this->gravatar_email)) {
            $skillsCount = count($this->skills);
            $length = mb_strlen($this->bio);

            if ($skillsCount && $length > 100) {
                $this->updateAttributes(['rating' => 10]);
            } elseif ($skillsCount && $length > 80) {
                $this->updateAttributes(['rating' => 9]);
            } elseif ($skillsCount && $length > 60) {
                $this->updateAttributes(['rating' => 8]);
            } elseif ($skillsCount && $length > 40) {
                $this->updateAttributes(['rating' => 7]);
            } else {
                $this->updateAttributes(['rating' => 6]);
            }
        } else {
            $this->updateAttributes(['rating' => 6]);
        }
    }

    /**
     * Updates related ProfileElastic
     * @param bool $doSave
     * @return boolean|ActiveRecord
     */
    public function updateElastic($doSave = true)
    {
        $profileElasticService = new ProfileElasticService($this);
        return $profileElasticService->process($doSave);
    }

    /**
     *
     */
    public function afterDelete()
    {
        parent::afterDelete();
        $mediaLayer = Yii::$app->get('mediaLayer');
        $mediaLayer->removeMedia($this->gravatar_email);
        $mediaLayer->removeMedia($this->background);

        UserCertificate::deleteAll(['user_id' => $this->user_id]);
        UserDeactivation::deleteAll(['user_id' => $this->user_id]);
        UserEducation::deleteAll(['user_id' => $this->user_id]);
        UserFavourite::deleteAll(['user_id' => $this->user_id]);
        UserFavouriteProduct::deleteAll(['user_id' => $this->user_id]);
        UserPortfolio::deleteAll(['user_id' => $this->user_id]);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            ProfileElastic::deleteAll(['user_id' => $this->user_id]);
            UserAttributeElastic::deleteAll(['id' => ArrayHelper::getColumn($this->userAttributes, 'id')]);
            return true;
        }

        return false;
    }
}