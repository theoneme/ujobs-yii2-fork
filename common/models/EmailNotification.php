<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "email_notification".
 *
 * @property integer $id
 * @property integer $to_id
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $entity
 * @property integer $entity_id
 */
class EmailNotification extends \yii\db\ActiveRecord
{
    const TYPE_PROFILE_PHOTO_MISSING = 0;
    const TYPE_PROFILE_BIO_MISSING = 1;
    const TYPE_MIXED = 2;
    const TYPE_SKILLS_MISSING = 3;
    const TYPE_JOBS_MISSING = 4;
    const TYPE_JOBS_WITHOUT_ATTACHMENTS_FOUND = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_id'], 'required'],
            ['entity', 'string', 'max' => 55],
            [['to_id', 'type', 'created_at', 'updated_at', 'entity_id'], 'integer'],

            [['to_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['to_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        $behaviors = [TimestampBehavior::class];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'to_id' => Yii::t('model', 'To ID'),
            'type' => Yii::t('model', 'Type'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }
}
