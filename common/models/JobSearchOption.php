<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "job_search_option".
 *
 * @property integer $id
 * @property integer $step_id
 * @property integer $next_step_id
 * @property integer $job_id
 * @property string|array $custom_data
 * @property integer $type
 *
 * @property Job $job
 * @property Job[] $jobs
 * @property JobSearchStep $step
 * @property JobSearchStep $nextStep
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property Attachment $attachment
 * @property Attachment[] $attachments
 *
 * @method bind($relationName = null, $id = null, $index = null)
 * @method bindAttachment($id = null)
 * @method validateWithRelations()
 */
class JobSearchOption extends ActiveRecord
{

    const TYPE_JOB = 10;
    const TYPE_NEXT_STEP = 20;
    const TYPE_ANOTHER_TREE_STEP = 30;

    /**
     * @var null|Job
     */
    private $_job = null;
    /**
     * @var Job[]
     */
    private $_jobs = null;
    /**
     * @var array
     */
    public $translationsData = [];
    /**
     * @var array
     */
    public $attachmentsData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_search_option';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'job-search-help'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['step_id', 'next_step_id'], 'integer'],
            [['next_step_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobSearchStep::class, 'targetAttribute' => ['next_step_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobSearchStep::class, 'targetAttribute' => ['step_id' => 'id']],
            ['custom_data', 'string'],
            ['type', 'integer'],
            ['type', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'step_id' => Yii::t('model', 'Step ID'),
            'next_step_id' => Yii::t('model', 'Next Step ID'),
        ];
    }


    /**
     * @return integer|null
     */
    public function getJobId()
    {
        return $this->custom_data[Yii::$app->language] ?? array_values($this->custom_data)[0] ?? null;
    }

    /**
     * @return null|Job
     */
    public function getJob()
    {
        if ($this->_job === null) {
            $jobId = $this->getJobId();
            if (!empty($jobId)) {
                $this->_job = Job::findOne($jobId);
            }
        }
        return $this->_job;
    }

    /**
     * @return Job[]
     */
    public function getJobs()
    {
        if ($this->_jobs === null) {
            $this->_jobs = Job::find()->where(['id' => $this->custom_data])->all();
//            $this->_jobs = [];
//            foreach ($this->custom_data as $locale => $jobId) {
//                $job = Job::findOne($jobId);
//                if ($job !== null) {
//                    $this->_jobs[$locale] = Job::findOne($jobId);
//                }
//            }
        }
        return $this->_jobs;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(JobSearchStep::class, ['id' => 'step_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNextStep()
    {
        return $this->hasOne(JobSearchStep::class, ['id' => 'next_step_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jso_ct.entity' => 'job_search_option'])->from('content_translation jso_ct')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['jso_ct.entity' => 'job_search_option'])->from('content_translation jso_ct')->andWhere(['jso_ct.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachment()
    {
        return $this->hasOne(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'job_search_option']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'job_search_option']);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->translation->title ?? $this->job->translation->title ?? '';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translation->content ?? $this->job->translation->content ?? '';
    }

    /**
     * @return int
     */
    public function defineType()
    {
        $type = self::TYPE_JOB;
        if ($this->next_step_id) {
            if ($this->nextStep->tree_id === $this->step->tree_id) {
                $type = self::TYPE_NEXT_STEP;
            } else {
                $type = self::TYPE_ANOTHER_TREE_STEP;
            }
        }
        return $type;
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            self::TYPE_JOB => 'Работа',
            self::TYPE_NEXT_STEP => 'Следующий шаг в данном дереве',
            self::TYPE_ANOTHER_TREE_STEP => 'Шаг из другого дерева',
        ];
    }

    /**
     * @return array
     */
    public function getTypeLabel()
    {
        return self::getTypeLabels()[$this->type];
    }

    public function clearRedundantFields()
    {
        if (in_array($this->type, [self::TYPE_NEXT_STEP, self::TYPE_ANOTHER_TREE_STEP])) {
            $this->custom_data = [];
        }
    }

    /**
     * @return array
     */
    public function getPriceRange()
    {
        if ($this->type === self::TYPE_JOB) {
            $priceRange = $this->job->getPriceRange();
        } else {
            $priceRange = ['min' => PHP_INT_MAX, 'max' => 0];
            foreach ($this->nextStep->options as $option) {
                $range = $option->getPriceRange();
                $priceRange['min'] = min($range['min'], $priceRange['min']);
                $priceRange['max'] = max($range['max'], $priceRange['max']);
            }
        }
        return $priceRange;
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($this->attachment && file_exists(Yii::getAlias("@frontend") . "/web" . $this->attachment->content)) {
            $image = $this->attachment->content;
        } else if ($this->job !== null) {
            return $this->job->getThumb($target);
        }

        $width = $height = null;

        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->custom_data) {
            $this->custom_data = json_decode($this->custom_data, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->clearRedundantFields();
        $this->custom_data = json_encode(array_filter($this->custom_data));
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'job_search_option';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
        }
        if (!empty($this->attachmentsData)) {
            foreach ($this->attachmentsData as $item) {
                $item['entity'] = 'job_search_option';
                $this->bindAttachment($item['content'])->attributes = $item;
            }
        }
        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\base\InvalidConfigException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (isset($changedAttributes['next_step_id']) && $changedAttributes['next_step_id'] !== $this->next_step_id) {
            $oldStep = JobSearchStep::findOne($changedAttributes['next_step_id']);
            if ($oldStep !== null && $oldStep->tree_id === $this->step->tree_id) {
                $oldStep->delete();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        ContentTranslation::deleteAll(['entity' => 'job_search_option', 'entity_id' => $this->id]);
        Attachment::deleteAll(['entity' => 'job_search_option', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        if (!empty($this->next_step_id)) {
            if ($this->nextStep->tree_id === $this->step->tree_id) {
                $this->nextStep->delete();
            }
        }
    }
}
