<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\models\elastic\PageAttributeElastic;
use common\models\elastic\PageElastic;
use common\models\user\Profile;
use common\models\user\User;
use common\services\elastic\PageElasticService;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $alias
 * @property string $image
 * @property string $banner
 * @property string $banner_small
 * @property integer $publish_date
 * @property integer $update_date
 * @property integer $all_views
 * @property integer $lm_views
 * @property integer $m_views
 * @property integer $views
 * @property integer $category_id
 * @property integer $user_id
 * @property string $layout
 * @property string $template
 * @property string $type
 * @property integer $status
 *
 * @property User $user
 * @property Profile $profile
 * @property Category $category
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property PageAttribute[] $extra
 * @property PageAttribute[] $tags
 * @property Comment[] $comments
 * @property Like[] $likes
 *
 * @mixin LinkableBehavior
 */
class Page extends ActiveRecord
{
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE_FOR_SUBSCRIBERS = 30;
    const STATUS_ACTIVE = 35;
    const STATUS_DELETED = 100;

    const TYPE_DEFAULT = 'default-page';
    const TYPE_ARTICLE = 'article-page';

    /**
     * @var array
     */
    public $translationsArr = [];

    /**
     * @var string
     */
//    public $title = null;

    /**
     * @var string
     */
    public $tagsString = null;

    public $relatedUserCount = 0;
    public $relatedJobCount = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date'],
                    ActiveRecord::EVENT_BEFORE_INSERT => ['publish_date'],
                ],
            ],
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'page'
            ],
            'banner' => [
                'class' => ImageBehavior::class,
                'folder' => 'page',
                'imageField' => 'banner'
            ],
            'banner_small' => [
                'class' => ImageBehavior::class,
                'folder' => 'page',
                'imageField' => 'banner_small'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'extra'],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publish_date', 'update_date', 'all_views', 'lm_views', 'm_views', 'category_id', 'user_id', 'status'], 'integer'],
            [['alias', 'template', 'layout'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 195],
            [['banner'], 'string', 'max' => 255],
            [['banner_small'], 'string', 'max' => 255],
            'uniqueAlias' => [['alias'], 'unique'],
            ['template', 'default', 'value' => 'page'],
            ['layout', 'default', 'value' => 'main'],
            [['image', 'all_views'], 'safe'],
            [['layout', 'template'], 'safe'],
            ['tagsString', 'string', 'max' => 260],

            ['type', 'default', 'value' => self::TYPE_DEFAULT],
            ['type', 'in', 'range' => [self::TYPE_DEFAULT, self::TYPE_ARTICLE]],
            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'image' => Yii::t('model', 'Image'),
            'banner' => Yii::t('model', 'Banner'),
            'banner_small' => Yii::t('model', 'Banner mobile'),
            'publish_date' => Yii::t('model', 'Publish date'),
            'update_date' => Yii::t('model', 'Update date'),
            'all_views' => Yii::t('model', 'All views'),
            'lm_views' => Yii::t('model', 'Lm views'),
            'm_views' => Yii::t('model', 'M views'),
            'views' => Yii::t('model', 'Views'),
            'category_id' => Yii::t('model', 'Belongs to category'),
            'template' => Yii::t('model', 'Template'),
            'layout' => Yii::t('model', 'Layout'),
            'user_id' => Yii::t('model', 'User'),
            'type' => Yii::t('model', 'Page type'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andWhere(['content_translation.entity' => 'page'])->andWhere(['content_translation.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['entity' => 'page'])->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['entity_id' => 'id'])->onCondition(['comment.entity' => 'comment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtra()
    {
        return $this->hasMany(PageAttribute::class, ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(PageAttribute::class, ['page_id' => 'id'])->where(['page_attribute.entity_alias' => 'page_tag']);
    }


    /**
     * @return int|string
     */
    public function getLikes()
    {
        return $this->hasMany(Like::class, ['entity_id' => 'id'])->andOnCondition(['like.entity' => Like::ENTITY_PAGE])->indexBy('user_id');
    }

    /**
     * @return int|string
     */
    public function getLikesCount()
    {
        return $this->hasMany(Like::class, ['entity_id' => 'id'])->andWhere(['like.entity' => Like::ENTITY_PAGE])->count();
    }

    /**
     * @return bool
     */
    public function hasLike()
    {
        return !isGuest() && Like::find()->where(['like.entity' => Like::ENTITY_PAGE, 'entity_id' => $this->id, 'user_id' => Yii::$app->user->identity->getId()])->exists();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];
        return Yii::$app->utility->upperFirstLetter($translation->title);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];
        return $translation->content;
    }

    /**
     * @param $userIndex
     * @param null $target
     * @return mixed
     */
    public function getLikeUserThumb($userIndex, $target = null)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if ($this->likes[$userIndex]->profile->gravatar_email && file_exists(Yii::getAlias("@frontend") . "/web" . $this->likes[$userIndex]->profile->gravatar_email)) {
            $image = $this->likes[$userIndex]->profile->gravatar_email;
        }

        $width = $height = null;

        if ($target != null && Yii::$app->params['images'][$target] != null) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @param $userIndex
     * @return string
     */
    public function getLikeSellerName($userIndex)
    {
        return $this->likes[$userIndex]->user->getSellerName();
    }

    /**
     * @param string $target
     * @return string
     */
    public function getSellerThumb($target = null)
    {
        return $this->user->getThumb($target);
    }


    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];
        return Url::to(["/article/view", 'alias' => !empty($translation->slug) ? $translation->slug : $this->alias]);
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->user->getSellerUrl();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->user->getSellerName();
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->profile->activeTariff) {
            return $this->profile->activeTariff->tariff->icon;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->profile->activeTariff) {
            return $this->profile->activeTariff->tariff->title;
        }

        return null;
    }

    /**
     * Updates related PageElastic
     * @param bool $doSave
     * @return PageElastic|bool
     */
    public function updateElastic($doSave = true)
    {
        $pageElasticService = new PageElasticService($this);
        return $pageElasticService->process($doSave);
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published for all') . '</span>' : Yii::t('labels', 'Published for all')),
            self::STATUS_ACTIVE_FOR_SUBSCRIBERS => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published for subscribers') . '</span>' : Yii::t('labels', 'Published for subscribers')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     * @param string $target
     * @return string
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->image, $target);
    }

    /**
     * @return bool
     */
    public function sendModerationNotification()
    {
        $subject = Yii::t('notifications', "Your article has passed moderation on {site}", ['site' => Yii::$app->name], $this->user->site_language);
        $message = Yii::t('notifications', "Your article has passed moderation and has been added to catalog.", [], $this->user->site_language);
        $template = Notification::TEMPLATE_ARTICLE_MODERATED;
        $translation = $this->translations[$this->user->site_language] ?? array_values($this->translations)[0];
        $route = ["/article/view", 'alias' => !empty($translation->slug) ? $translation->slug : $this->alias];

        $notification = new Notification([
            'to_id' => $this->user_id,
            'params' => [
                'subject' => $subject,
                'content' => $message,
            ],
            'template' => $template,
            'custom_data' => json_encode([
                'article' => $this->translations[$this->user->site_language]->title ?? array_values($this->translations)[0]->title,
            ]),
            'linkRoute' => $route,
            'withEmail' => true,
            'is_visible' => true
        ]);

        return $notification->save();
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];

        return [
            'title' => !empty($translation->seo_title) ? $translation->seo_title : $this->getLabel(),
            'description' => $translation->seo_description ?? '',
            'keywords' => $translation->seo_keywords ?? '',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (empty($this->alias)) {
            $this->alias = Yii::$app->utility->generateSlug($this->getLabel(), 3, 50);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if((int)$this->status === self::STATUS_ACTIVE && array_key_exists('status', $changedAttributes) && (int)$changedAttributes['status'] !== self::STATUS_ACTIVE) {
            /** @var UserFriend[] $friends */
            $friends = $this->user->friends;
            if (!empty($friends)) {
                foreach ($friends as $friend) {
                    $notification = new Notification([
                        'to_id' => $friend->target_user_id,
                        'from_id' => $this->user_id,
                        'thumb' => $this->getThumb('catalog'),
                        'custom_data' => json_encode([
                            'user' => $this->user->getSellerName(),
                        ]),
                        'template' => Notification::TEMPLATE_FRIEND_POSTED_ARTICLE,
                        'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                        'linkRoute' => ["/article/view", 'alias' => $this->alias],
                        'withEmail' => false,
                        'is_visible' => true
                    ]);

                    $notification->save();
                }
            }
        }

        if(
            $this->type === self::TYPE_ARTICLE
            && in_array((int)$this->status, [self::STATUS_ACTIVE, self::STATUS_ACTIVE_FOR_SUBSCRIBERS], true)
            && array_key_exists('status', $changedAttributes)
            && !in_array((int)$changedAttributes['status'], [self::STATUS_ACTIVE, self::STATUS_ACTIVE_FOR_SUBSCRIBERS], true)
        ) {
            $this->sendModerationNotification();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        ContentTranslation::deleteAll(['entity_id' => $this->id, 'entity' => 'page']);
        PageAttributeElastic::deleteAll(['id' => ArrayHelper::getColumn($this->extra, 'id')]);
        PageAttribute::deleteAll(['page_id' => $this->id]);
        PageElastic::deleteAll(['id' => $this->id]);
        return parent::beforeDelete();
    }
}
