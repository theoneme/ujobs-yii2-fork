<?php

namespace common\models;

use common\components\CurrencyHelper;
use common\models\user\User;
use common\modules\store\models\Order;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "payment_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $type
 * @property integer $template
 * @property string $description
 * @property integer $balance_change
 * @property integer $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $currency_code
 * @property string $custom_data
 *
 * @property Order $order
 * @property User $user
 */
class PaymentHistory extends \yii\db\ActiveRecord
{
    const TYPE_BALANCE_CHANGE_NEG = 0;
    const TYPE_BALANCE_CHANGE_POS = 1;
    const TYPE_BALANCE_INFO = 2;

    const TYPE_OPERATION_PURCHASE = 0;
    const TYPE_OPERATION_REFUND = 1;
    const TYPE_OPERATION_REVENUE = 2;
    const TYPE_OPERATION_WITHDRAW = 3;
    const TYPE_OPERATION_REPLENISH = 4;
    const TYPE_OPERATION_REFERRAL = 5;
    const TYPE_OPERATION_INFO = 6;
    const TYPE_OPERATION_TRANSFER = 7;
    const TYPE_OPERATION_BONUS = 8;

    const TEMPLATE_PAYMENT_JOB_PURCHASE = 1;
    const TEMPLATE_PAYMENT_ACCOUNT_REPLENISH = 2;
    const TEMPLATE_PAYMENT_REFUND = 3;
    const TEMPLATE_VIEW_ORDER_LABEL = 4;
    const TEMPLATE_PAYMENT_ORDER_COMPLETE = 5;
    const TEMPLATE_PAYMENT_REFERRAL_BONUS = 6;
    const TEMPLATE_PAYMENT_SALE_BONUS = 7;
    const TEMPLATE_PAYMENT_POST_JOB_BONUS = 8;
    const TEMPLATE_PAYMENT_PURCHASE_BONUS = 9;
    const TEMPLATE_PAYMENT_FRIEND_REGISTER_BONUS = 10;
    const TEMPLATE_PAYMENT_POST_PORTFOLIO_BONUS = 11;
    const TEMPLATE_PAYMENT_POST_ALBUM_BONUS = 12;
    const TEMPLATE_PAYMENT_TARIFF_PURCHASE = 13;
    const TEMPLATE_PAYMENT_MONEY_WITHDRAWAL = 14;
    const TEMPLATE_MONEY_TRANSFER = 15;
    const TEMPLATE_PAYMENT_PROFILE_POST_BONUS = 16;
    const TEMPLATE_PAYMENT_REGISTER_BONUS = 17;
    const TEMPLATE_PAYMENT_WITHDRAWAL_REQUEST = 18;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_history';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [TimestampBehavior::class];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'amount'], 'required'],
            [['user_id', 'order_id', 'type', 'balance_change', 'amount', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'order_id' => Yii::t('model', 'Order ID'),
            'type' => Yii::t('model', 'Type'),
            'description' => Yii::t('model', 'Description'),
            'balance_change' => Yii::t('model', 'Balance Change'),
            'amount' => Yii::t('model', 'Amount'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param null $currency_code
     * @return string
     */
    public function getColoredAmount($currency_code = null)
    {
        switch ($this->balance_change) {
            case self::TYPE_BALANCE_CHANGE_NEG :
                return Html::tag('strong', "-" . $this->getAmount($currency_code), ['class' => 'text-danger']);
                break;
            case self::TYPE_BALANCE_CHANGE_POS :
                return Html::tag('strong', "+" . $this->getAmount($currency_code), ['class' => 'text-success']);
                break;
            default :
                return '';
                break;
        }
    }

    /**
     * @param null $currency_code
     * @return string
     */
    public function getAmount($currency_code = null)
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, $currency_code ?? Yii::$app->params['app_currency_code'], $this->amount);
    }

    public function getHeader()
    {
        switch ($this->template) {
            case self::TEMPLATE_PAYMENT_ACCOUNT_REPLENISH:
                return Yii::t('account', 'Account replenishment');
            case self::TEMPLATE_PAYMENT_REFUND:
                return Yii::t('account', 'Refund for canceled order');
            case self::TEMPLATE_PAYMENT_JOB_PURCHASE:
                return Yii::t('account', 'Payment for Job purchase ({link})', ['link' => Html::a(Yii::t('account', 'View order'), $this->customDataArray['link'])]);
            case self::TEMPLATE_VIEW_ORDER_LABEL:
                return Yii::t('account', 'View order');
            case self::TEMPLATE_PAYMENT_ORDER_COMPLETE:
                return Yii::t('account', 'Payment for order completion ({link})', ['link' => Html::a(Yii::t('account', 'View order'), $this->customDataArray['link'])]);
            case self::TEMPLATE_PAYMENT_REFERRAL_BONUS:
                return Yii::t('account', 'Bonus for referral program');
            case self::TEMPLATE_PAYMENT_SALE_BONUS:
                return Yii::t('account', 'Sale bonus');
            case self::TEMPLATE_PAYMENT_POST_JOB_BONUS:
                return Yii::t('account', 'Bonus for posting Job');
            case self::TEMPLATE_PAYMENT_PURCHASE_BONUS:
                return Yii::t('account', 'Bonus for purchase');
            case self::TEMPLATE_PAYMENT_FRIEND_REGISTER_BONUS:
                return Yii::t('account', 'Bonus for friend registration');
            case self::TEMPLATE_PAYMENT_POST_PORTFOLIO_BONUS:
                return Yii::t('account', 'Bonus for created portfolio');
            case self::TEMPLATE_PAYMENT_POST_ALBUM_BONUS:
                return Yii::t('account', 'Bonus for created portfolio album');
            case self::TEMPLATE_PAYMENT_TARIFF_PURCHASE:
                return Yii::t('account', 'Payment for tariff ({tariff})', ['tariff' => $this->customDataArray['tariff']]);
            case self::TEMPLATE_PAYMENT_MONEY_WITHDRAWAL:
                return Yii::t('account', 'Money withdrawal from balance');
            case self::TEMPLATE_MONEY_TRANSFER:
                return Yii::t('account', 'Money transfer');
            case self::TEMPLATE_PAYMENT_PROFILE_POST_BONUS:
                return Yii::t('account', 'Bonus for posting your profile');
            case self::TEMPLATE_PAYMENT_REGISTER_BONUS:
                return Yii::t('account', 'Bonus for registration');
            case self::TEMPLATE_PAYMENT_WITHDRAWAL_REQUEST:
                return Yii::t('account', 'Withdrawal request ({amount})', ['amount' => $this->customDataArray['amount']]);
        }

        return null;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert && empty($this->currency_code)) {
            switch ($this->type) {
                case self::TYPE_OPERATION_REVENUE:
                case self::TYPE_OPERATION_REFERRAL:
                    $this->currency_code = $this->order->currency_code;
                    break;
                case self::TYPE_OPERATION_PURCHASE:
                case self::TYPE_OPERATION_REFUND:
                    $this->currency_code = $this->order->payment->currency_code;
                    break;
                case self::TYPE_OPERATION_REPLENISH:
                    $this->currency_code = $this->order !== null ? $this->order->payment->currency_code : Yii::$app->params['app_currency_code'];
                    break;
                case self::TYPE_OPERATION_INFO:
                    $this->currency_code = Yii::$app->params['app_currency_code'];
                    break;
                case self::TYPE_OPERATION_BONUS:
                    $this->currency_code = 'CTY';
                    break;
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->balance_change !== self::TYPE_BALANCE_INFO) {
            $amount = $this->balance_change == self::TYPE_BALANCE_CHANGE_POS ? $this->amount : -$this->amount;
            $wallet = UserWallet::findOrCreate(['user_id' => $this->user_id, 'currency_code' => $this->currency_code]);
            $wallet->updateCounters(['balance' => $amount]);

            if ($this->balance_change === self::TYPE_BALANCE_CHANGE_NEG) {
                $bonusTotal = floor(CurrencyHelper::convert($this->currency_code, 'RUB', $this->amount) / 1000);
                if ($bonusTotal) {
                    $bonus = new PaymentHistory([
                        'user_id' => $this->user_id,
                        'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                        'template' => PaymentHistory::TEMPLATE_PAYMENT_PURCHASE_BONUS,
                        'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                        'amount' => $bonusTotal * 5,
                    ]);
                    $bonus->save();
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->custom_data !== null) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }
    }
}
