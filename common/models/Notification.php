<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 11:37
 */

namespace common\models;

use common\components\UrlAdvanced;
use common\models\user\User;
use common\services\NotificationService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property integer $to_id
 * @property integer $from_id
 * @property string $link
 * @property string $thumb
 * @property integer $subject
 * @property integer $template
 * @property string $message
 * @property string $sender
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $is_read
 * @property mixed $custom_data
 *
 * @property User $to
 * @property User $from
 *
 */
class Notification extends ActiveRecord
{
    const SENDER_SYSTEM = 'system';

    const SUBJECT_DEFAULT = 0;
    const SUBJECT_YOU_HAVE_NEW_ORDER = 5;
    const SUBJECT_YOU_CREATED_NEW_ORDER = 10;
    const SUBJECT_ORDER_UPDATED = 15;
    const SUBJECT_JOB_OFFERED = 20;
    const SUBJECT_JOB_REQUESTED = 25;
    const SUBJECT_COMPANY_INVITE = 30;
    const SUBJECT_FRIENDSHIP_REQUEST = 35;

    const ACTION_ACCEPT = 1;
    const ACTION_DECLINE = 2;

    const TEMPLATE_HAVE_NEW_JOB_ORDER = 1;
    const TEMPLATE_CREATED_JOB_ORDER = 2;
    const TEMPLATE_HAVE_TENDER_RESPONSE = 3;
    const TEMPLATE_CREATED_TENDER_RESPONSE = 4;
    const TEMPLATE_JOB_MODERATED = 5;
    const TEMPLATE_NEW_INBOX_MESSAGE = 6;
    const TEMPLATE_PRODUCT_MODERATED = 7;
    const TEMPLATE_ORDER_CANCELLED = 8;
    const TEMPLATE_ORDER_AUTO_ACCEPTED = 9;
    const TEMPLATE_ORDER_UPDATED = 10;
    const TEMPLATE_CREATED_PRODUCT_ORDER = 11;
    const TEMPLATE_HAVE_NEW_PRODUCT_ORDER = 12;
    const TEMPLATE_PORTFOLIO_MODERATED = 13;
    const TEMPLATE_PROFILE_MODERATED = 14;
    const TEMPLATE_COMPANY_INVITE = 15;
    const TEMPLATE_REQUEST_MODERATED = 16;
    const TEMPLATE_PRODUCT_REQUEST_MODERATED = 17;
    const TEMPLATE_ARTICLE_MODERATED = 26;

    const TEMPLATE_FRIEND_POSTED_ARTICLE = 18;
    const TEMPLATE_FRIEND_POSTED_JOB_REQUEST = 19;
    const TEMPLATE_FRIEND_POSTED_PRODUCT = 20;
    const TEMPLATE_FRIEND_POSTED_PRODUCT_REQUEST = 21;
    const TEMPLATE_FRIEND_POSTED_PORTFOLIO = 22;
    const TEMPLATE_PORTFOLIO_LIKED = 23;
    const TEMPLATE_FRIENDSHIP_REQUEST = 24;
    const TEMPLATE_FRIEND_POSTED_JOB = 25;

    const PROVIDER_EMAIL = 'email';
    const PROVIDER_TELEGRAM = 'telegram';
    const PROVIDER_SKYPE = 'skype';
    const PROVIDER_PUSH = 'push';

    public $withEmail = false;
    public $withTelegram = true;
    public $withSkype = true;
    public $withWeb = true;

    /**
     * @var null
     */
    public $linkRoute = null;

    /**
     * @var array
     */
    public $params = [];

    /**
     * @var array
     */
    public $subjectsToViews = [
        self::SUBJECT_DEFAULT => 'default-notification',
        self::SUBJECT_JOB_OFFERED => 'order-notification',
        self::SUBJECT_JOB_REQUESTED => 'order-notification',
        self::SUBJECT_ORDER_UPDATED => 'order-notification',
        self::SUBJECT_YOU_HAVE_NEW_ORDER => 'you-have-new-order',
        self::SUBJECT_YOU_CREATED_NEW_ORDER => 'you-created-new-order',
        self::SUBJECT_COMPANY_INVITE => 'company-invite',
        self::SUBJECT_FRIENDSHIP_REQUEST => 'friendship-request',
    ];

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['message', 'string', 'max' => 65535],
            ['subject', 'integer'],
            [['sender'], 'string', 'max' => 255],
            [['thumb', 'link'], 'string', 'max' => 155],
            [['is_read', 'withEmail'], 'boolean'],
            ['is_read', 'default', 'value' => false],

            [['to_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['to_id' => 'id']],
            [['from_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::class, 'targetAttribute' => ['from_id' => 'id']],
            ['params', 'safe'],
            ['is_visible', 'default', 'value' => true],
            ['subject', 'default', 'value' => self::SUBJECT_DEFAULT]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'theme' => Yii::t('model', 'Subject'),
            'message' => Yii::t('model', 'Message'),
            'sender' => Yii::t('model', 'Sender'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::class, ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::class, ['id' => 'to_id']);
    }

    /**
     * @param null $locale
     * @return null|string
     */
    public function getHeader($locale = null)
    {
        switch ($this->template) {
            case self::TEMPLATE_HAVE_NEW_JOB_ORDER:
            case self::TEMPLATE_HAVE_NEW_PRODUCT_ORDER:
                return Yii::t('notifications', 'You have new order from user {user}', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_CREATED_PRODUCT_ORDER:
            case self::TEMPLATE_CREATED_JOB_ORDER:
                return Yii::t('notifications', 'You have created new order for user {user}', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_HAVE_TENDER_RESPONSE:
                return Yii::t('notifications', 'You have new response on request {tender} from {user}', ['tender' => $this->customDataArray['tender'], 'user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_CREATED_TENDER_RESPONSE:
                return Yii::t('notifications', 'You have left response on request {tender}', ['tender' => $this->customDataArray['tender']], $locale);
            case self::TEMPLATE_JOB_MODERATED:
                return Yii::t('notifications', 'Your job {job} has passed moderation and is published in catalog', ['job' => $this->customDataArray['job']], $locale);
            case self::TEMPLATE_NEW_INBOX_MESSAGE:
                return Yii::t('notifications', 'You have new inbox message from user {user}', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_PRODUCT_MODERATED:
                return Yii::t('notifications', 'Your product {product} has passed moderation and is published in catalog', ['product' => $this->customDataArray['product']], $locale);
            case self::TEMPLATE_ORDER_CANCELLED:
                return Yii::t('notifications', 'Order #{number} cancelled. The money will soon be returned to the balance.', ['number' => $this->customDataArray['number']], $locale);
            case self::TEMPLATE_ORDER_AUTO_ACCEPTED:
                return Yii::t('notifications', 'Order #{number} is automatically completed after {days, plural, one{# day} other{# days}} passed from delivery.', ['number' => $this->customDataArray['number']], $locale);
            case self::TEMPLATE_ORDER_UPDATED:
                return Yii::t('notifications', '{user} has updated order {order}', ['user' => $this->customDataArray['user'], 'order' => $this->customDataArray['order']], $locale);
            case self::TEMPLATE_PORTFOLIO_MODERATED:
                return Yii::t('notifications', 'Your portfolio album {portfolio} has passed moderation and is published in catalog', ['portfolio' => $this->customDataArray['portfolio']], $locale);
            case self::TEMPLATE_PROFILE_MODERATED:
                return Yii::t('notifications', 'Your profile has passed moderation on {site}', ['site' => Yii::$app->name], $locale);
            case self::TEMPLATE_COMPANY_INVITE:
                return Yii::t('notifications', 'You have been invited to join company «{company}» by user {user}', ['company' => $this->customDataArray['company'], 'user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIENDSHIP_REQUEST:
                return Yii::t('notifications', 'User {user} wants to add you to friend list', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIEND_POSTED_JOB:
                return Yii::t('notifications', 'User {user} has posted new service', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIEND_POSTED_JOB_REQUEST:
                return Yii::t('notifications', 'User {user} has posted new job request', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIEND_POSTED_PRODUCT:
                return Yii::t('notifications', 'User {user} has posted new product', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIEND_POSTED_PRODUCT_REQUEST:
                return Yii::t('notifications', 'User {user} has posted new product request', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIEND_POSTED_ARTICLE:
                return Yii::t('notifications', 'User {user} has posted new article', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_FRIEND_POSTED_PORTFOLIO:
                return Yii::t('notifications', 'User {user} has posted new portfolio album', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_PORTFOLIO_LIKED:
                return Yii::t('notifications', 'User {user} likes your portfolio album', ['user' => $this->customDataArray['user']], $locale);
            case self::TEMPLATE_REQUEST_MODERATED:
                return Yii::t('notifications', 'Your request {job} has passed moderation and is published in catalog', ['job' => $this->customDataArray['job']], $locale);
            case self::TEMPLATE_PRODUCT_REQUEST_MODERATED:
                return Yii::t('notifications', 'Your product request {product} has passed moderation and is published in catalog', ['product' => $this->customDataArray['product']], $locale);
            case self::TEMPLATE_ARTICLE_MODERATED:
                return Yii::t('notifications', 'Your article {article} has passed moderation and is published in catalog', ['article' => $this->customDataArray['article']], $locale);
        }

        return null;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->link = UrlAdvanced::to(array_merge($this->linkRoute, ['app_language' => 'ru']));
        if ($this->custom_data !== null && empty($this->customDataArray)) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->params = array_merge([
            'user' => $this->to,
            'link' => UrlAdvanced::to(array_merge($this->linkRoute, ['app_language' => 'ru']), true, 'urlManagerFrontEnd'),
            'from' => Yii::$app->params['ujobsNoty'],
            'content' => Yii::t('notifications', $this->getHeader($this->to->site_language), $this->customDataArray, $this->to->site_language)
        ], $this->params);

        $notificationService = new NotificationService($this);

        if ($this->withEmail === true && (bool)$this->to->profile->email_notifications === true && !empty($this->to->email)) {
            $notificationService->sendNotification(self::PROVIDER_EMAIL);
        }

        if ($this->withTelegram === true && !empty($this->to->telegram_uid)) {
            $notificationService->sendNotification(self::PROVIDER_TELEGRAM);
        }

        if ($this->withSkype === true && !empty($this->to->skype_uid)) {
            $notificationService->sendNotification(self::PROVIDER_SKYPE);
        }

        if ($this->withWeb === true && !empty($this->to->push_token)) {
            $notificationService->sendNotification(self::PROVIDER_PUSH);
        }
    }

    /**
     * @param string $target
     * @return mixed
     */
    public function getThumb($target = null)
    {
        return Yii::$app->mediaLayer->getThumb($this->thumb, $target);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->custom_data !== null) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }
    }
}