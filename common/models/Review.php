<?php

namespace common\models;

use common\models\user\User;
use common\modules\store\models\Order;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $text
 * @property integer $rating
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $communication
 * @property integer $service
 * @property integer $recommend
 * @property integer $created_for
 * @property integer $order_id
 * @property integer $job_id
 * @property integer $type
 * @property boolean $is_fake
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property User $createdFor
 * @property Order $order
 */
class Review extends ActiveRecord
{
    const TYPE_SELLER_REVIEW = 1;
    const TYPE_CUSTOMER_REVIEW = 2;

    /**
     * @var bool
     */
    public $isBlameable = true;

    /**
     * @var bool
     */
    public $isTimestampable = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    public function behaviors()
    {
        $behaviors = [];
        if ($this->isTimestampable) {
            $behaviors[] = [
                'class' => TimestampBehavior::class,
            ];
        }
        if ($this->isBlameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
            ];
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rating', 'communication', 'service', 'recommend', 'created_for', 'order_id', 'type', 'job_id'], 'integer'],
            //'uniqueOrderReview' => [['created_for', 'order_id'], 'unique', 'targetAttribute' => ['created_for', 'order_id']],
            [['text'], 'string'],
            ['is_fake', 'boolean'],
            ['is_fake', 'default', 'value' => false],

//            [['created_by', 'updated_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'created_by' => Yii::t('model', 'Created By'),
            'updated_by' => Yii::t('model', 'Updated By'),
            'text' => Yii::t('model', 'Text'),
            'rating' => Yii::t('model', 'Rating'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedFor()
    {
        return $this->hasOne(User::class, ['id' => 'created_for']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }
}
