<?php

namespace common\models;

use common\models\user\Profile;
use common\models\user\User;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "conversation_member".
 *
 * @property integer $conversation_id
 * @property integer $user_id
 * @property string $status
 * @property boolean $starred
 * @property integer $viewed_at
 *
 * @property Conversation $conversation
 * @property User $user
 */
class ConversationMember extends ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED = 'deleted';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conversation_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['conversation_id', 'user_id'], 'integer'],

            [['conversation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Conversation::class, 'targetAttribute' => ['conversation_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_ARCHIVED, self::STATUS_DELETED]],

            ['starred', 'default', 'value' => false],
            ['starred', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'conversation_id' => Yii::t('model', 'Conversation ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'status' => Yii::t('model', 'Status'),
            'starred' => Yii::t('model', 'Starred'),
            'viewed_at' => Yii::t('model', 'Viewed at'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversation()
    {
        return $this->hasOne(Conversation::class, ['id' => 'conversation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
            self::STATUS_ARCHIVED => Yii::t('model', 'Archived'),
            self::STATUS_DELETED => Yii::t('model', 'Deleted'),
        ];
    }
}
