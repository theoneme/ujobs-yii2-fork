<?php

namespace common\models;

use common\models\user\User;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_member".
 *
 * @property int $id
 * @property int $user_id
 * @property int $company_id
 * @property int $role
 * @property int $status
 *
 * @property Company $company
 * @property User $user
 */
class CompanyMember extends ActiveRecord
{
    const ROLE_OWNER = 0;
    const ROLE_ADMIN = 10;
    const ROLE_SUPPORT = 20;
    const ROLE_CONTENT_MANAGER = 30;

    const STATUS_INVITED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id'], 'required'],
            [['user_id', 'company_id', 'role', 'status'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['role', 'in', 'range' => [self::ROLE_OWNER, self::ROLE_ADMIN, self::ROLE_SUPPORT, self::ROLE_CONTENT_MANAGER]],
            ['status', 'in', 'range' => [self::STATUS_INVITED, self::STATUS_ACTIVE]],
            ['status', 'default', 'value' => self::STATUS_INVITED],
            ['user_id', 'unique', 'targetAttribute' => ['user_id', 'company_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'company_id' => Yii::t('model', 'Company ID'),
            'role' => Yii::t('model', 'Role'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return bool
     */
    public function isAllowedToManage()
    {
        return $this->status === self::STATUS_ACTIVE && (in_array($this->role, [self::ROLE_OWNER, self::ROLE_ADMIN]));
    }

    /**
     * @return bool
     */
    public function isAllowedToPost()
    {
        return $this->status === self::STATUS_ACTIVE && (in_array($this->role, [self::ROLE_OWNER, self::ROLE_ADMIN, self::ROLE_CONTENT_MANAGER]));
    }

    /**
     * @return bool
     */
    public function isAllowedToCorrespond()
    {
        return $this->status === self::STATUS_ACTIVE && (in_array($this->role, [self::ROLE_OWNER, self::ROLE_ADMIN, self::ROLE_SUPPORT]));
    }

    /**
     * @return bool
     */
    public function isAllowedToOrder()
    {
        return $this->status === self::STATUS_ACTIVE && (in_array($this->role, [self::ROLE_OWNER, self::ROLE_ADMIN]));
    }

    /**
     * @return bool
     */
    public function isAllowedToUseWallets()
    {
        return $this->status === self::STATUS_ACTIVE && (in_array($this->role, [self::ROLE_OWNER]));
    }

    /**
     * @return string
     */
    public function getRoleLabel()
    {
        $labels = static::getRoleLabels();
        return isset($labels[$this->role]) ? $labels[$this->role] : Yii::t('labels', 'Unknown role');
    }

    /**
     * @return array
     */
    public static function getRoleLabels()
    {
        return [
            self::ROLE_OWNER => Yii::t('labels', 'Company owner'),
            self::ROLE_ADMIN => Yii::t('labels', 'Administrator'),
            self::ROLE_SUPPORT => Yii::t('labels', 'Support service'),
            self::ROLE_CONTENT_MANAGER => Yii::t('labels', 'Content manager'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_INVITED => Yii::t('labels', 'Invited'),
            self::STATUS_ACTIVE => Yii::t('labels', 'Active'),
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert === true && $this->role !== self::ROLE_OWNER) {
            /** @var User $user */
            $user = Yii::$app->user->identity;

            $notification = new Notification([
                'to_id' => $this->user_id,
                'from_id' => $this->company->user_id,
                'thumb' => $this->company->logo,
                'custom_data' => json_encode([
                    'action' => null,
                    'user' => $user->getSellerName(),
                    'company' => $this->company->getSellerName(),
                    'company_id' => $this->company_id
                ]),
                'template' => Notification::TEMPLATE_COMPANY_INVITE,
                'subject' => Notification::SUBJECT_COMPANY_INVITE,
                'linkRoute' => ['/company/profile/show', 'id' => $this->company_id],
                'withEmail' => false,
                'is_visible' => true
            ]);

            return $notification->save();
        }

        return false;
    }
}
