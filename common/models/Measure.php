<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "measure".
 *
 * @property integer $id
 *
 * @property MeasureToCategory[] $measureToCategories
 * @property Category[] $categories
 * @property MeasureTranslation $translation
 * @property MeasureTranslation[] $translations
 */
class Measure extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasureToCategories()
    {
        return $this->hasMany(MeasureToCategory::class, ['measure_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->viaTable('measure_to_category', ['measure_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(MeasureTranslation::class, ['measure_id' => 'id'])->andWhere(['locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(MeasureTranslation::class, ['measure_id' => 'id'])->indexBy('locale');
    }
}
