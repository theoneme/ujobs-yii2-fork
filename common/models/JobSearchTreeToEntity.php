<?php

namespace common\models;

use common\modules\attribute\models\AttributeValue;
use Yii;

/**
 * This is the model class for table "job_search_tree_to_entity".
 *
 * @property integer $id
 * @property integer $tree_id
 * @property string $entity
 * @property string $entity_content
 */
class JobSearchTreeToEntity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_search_tree_to_entity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['entity', 'string', 'max' => 20],
            ['entity_content', 'string'],
            [['tree_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobSearchTree::class, 'targetAttribute' => ['tree_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_content' => Yii::t('model', 'Entity Content'),
            'tree_id' => Yii::t('model', 'Tree ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTree()
    {
        return $this->hasOne(JobSearchTree::class, ['id' => 'tree_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'entity_content']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(AttributeValue::class, ['alias' => 'entity_content'])->andOnCondition(['attribute_id' => 36]);
    }

    public function getEntityLabel() {
        return '(' . Yii::t('model', Yii::$app->utility->upperFirstLetter($this->entity)) . ') ' . $this->{$this->entity}->translation->title;
    }
}
