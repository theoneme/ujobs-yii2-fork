<?php

namespace common\models;

use common\models\elastic\PageAttributeElastic;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\services\elastic\PageAttributeElasticService;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "page_attribute".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $page_id
 * @property integer $value
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property Page $page
 * @property Attribute $attr
 * @property AttributeValue $attrValue
 * @property AttributeDescription $attrValueDescription
 * @property AttributeDescription[] $attrValueDescriptions
 * @property AttributeDescription $attrDescription
 * @property AttributeDescription[] $attrDescriptions
 */
class PageAttribute extends ActiveRecord
{
    public $relatedCount = 0;

    /**
     * @var SeoAdvanced|null
     */
    public $_meta = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'page_id', 'value'], 'integer'],
            [['entity_alias'], 'string', 'max' => 35],
            [['value_alias'], 'string', 'max' => 75],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::class, 'targetAttribute' => ['page_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['value' => 'id']],
            [['locale'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'page_id' => Yii::t('model', 'Page ID'),
            'value' => Yii::t('model', 'Value'),
            'entity_alias' => Yii::t('model', 'Entity Alias'),
            'value_alias' => Yii::t('model', 'Value Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::class, ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1')->indexBy('locale');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue')->indexBy('locale');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * Updates related PageAttributeElastic
     * @param bool $doSave
     * @return PageAttributeElastic|bool
     */
    public function updateElastic($doSave = true)
    {
        $elasticService = new PageAttributeElasticService($this);
        return $elasticService->process($doSave);
    }

    /**
     * @return SeoAdvanced|null
     */
    public function getMeta()
    {
        if ($this->_meta === null) {
            $this->_meta = SeoAdvanced::find()->where(['entity' => $this->entity_alias, 'entity_content' => $this->value_alias])->one();
            if ($this->_meta !== null) {
                $data = json_decode($this->_meta->custom_data, true);
                $data = is_array($data) ? $data : [];
                $data['title'] = Html::encode(preg_replace("/[,.;]/", "", $this->getTitle()));
                $this->_meta->applyTemplates($data);
            }
        }

        return $this->_meta;
    }

    /**
     * Get attribute's title
     */
    public function getTitle()
    {
        return $this->attrValueDescriptions[Yii::$app->language]->title ?? array_values($this->attrValueDescriptions)[0]->title;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
//        $this->updateElastic();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        PageAttributeElastic::deleteAll(['id' => $this->id]);
        return parent::beforeDelete();
    }

}
