<?php

namespace common\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "user_education".
 *
 * @property integer $id
 * @property integer $year_start
 * @property integer $year_end
 * @property integer $country_id
 * @property string $title
 * @property string $degree
 * @property string $specialization
 * @property integer $user_id
 */
class UserEducation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_education';
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasAccess($id)
    {
        return Yii::$app->user->isGuest ? false : UserEducation::find()->where(['user_id' => Yii::$app->user->identity->getId(), 'id' => $id])->exists();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year_start', 'country_title', 'title', 'degree', 'user_id', 'specialization'], 'required'],
            [['year_start', 'year_end', 'country_id', 'user_id'], 'integer'],
            ['country_id', 'default', 'value' => 0],
            [['title', 'degree'], 'string', 'max' => 75],
            [['specialization'], 'string', 'max' => 155]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'year_start' => Yii::t('model', 'Year Start'),
            'year_end' => Yii::t('model', 'Year End'),
            'country_id' => Yii::t('model', 'Country ID'),
            'title' => Yii::t('model', 'Title'),
            'degree' => Yii::t('model', 'Degree'),
            'user_id' => Yii::t('model', 'User ID'),
            'specialization' => Yii::t('model', 'Specialization'),
            'country_title' => Yii::t('model', 'Country Title')
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();
        if (empty ($this->user_id)) {
            $this->user_id = Yii::$app->user->identity->getId();
        }
        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->title = HtmlPurifier::process($this->title);
            $this->specialization = HtmlPurifier::process($this->specialization);
            $this->country_title = HtmlPurifier::process($this->country_title);
            $this->degree = HtmlPurifier::process($this->degree);

            return true;
        }
        return false;
    }
}
