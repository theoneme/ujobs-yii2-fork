<?php

namespace common\models;

use common\models\elastic\JobAttributeElastic;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\services\elastic\JobAttributeElasticService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "job_attribute".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $job_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property Job $job
 * @property Attribute $attr
 * @property AttributeValue $attrValue
 * @property AttributeDescription $attrDescription
 * @property AttributeDescription $attrValueDescription
 * @property AttributeDescription[] $attrDescriptions
 * @property AttributeDescription[] $attrValueDescriptions
 * @property SeoAdvanced $meta
 */
class JobAttribute extends ActiveRecord
{
    public $relatedJobsCount;
    public $relatedCount;

    /**
     * @var SeoAdvanced|null
     */
    public $_meta = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locale', 'attribute_id'], 'required'],
            [['job_id', 'attribute_id'], 'integer'],
            [['entity_alias', 'value_alias'], 'string', 'max' => 75],
            [['value'], 'integer'],
            [['locale'], 'string', 'max' => 15],

            [['job_id'], 'exist', 'skipOnError' => false, 'targetClass' => Job::class, 'targetAttribute' => ['job_id' => 'id']],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute'),
            'job_id' => Yii::t('model', 'Job ID'),
            'value' => Yii::t('model', 'Value'),
            'locale' => Yii::t('model', 'Locale'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue')->indexBy('locale');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1')->indexBy('locale');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::class, ['id' => 'job_id']);
    }

    /**
     * @param array $queryParams
     * @return array|SeoAdvanced|null|ActiveRecord
     */
    public function getMeta($queryParams = [])
    {
        if ($this->_meta === null) {
            $this->_meta = SeoAdvanced::find()->where(['entity' => $this->attr->alias, 'entity_content' => $this->value_alias])->one();
            if ($this->_meta !== null) {
                $data = json_decode($this->_meta->custom_data, true);
                $data = is_array($data) ? $data : [];
                $data['title'] = Html::encode(preg_replace("/[,.;]/", "", $this->getTitle()));

                $data['city'] = '';

                $cityParam = ArrayHelper::remove($queryParams, 'city');
                if($cityParam !== null) {
                    /** @var AttributeValue $city */
                    $city = AttributeValue::find()->where(['attribute_id' => 42, 'alias' => $cityParam])->one();

                    if($city !== null) {
                        $data['city'] = $city ? Yii::t('seo', ' in {city}', ['city' => $city->getTitle()]) : '';
                    }
                }

                $this->_meta->applyTemplates($data);
            }
        }

        return $this->_meta;
    }

    /**
     * Get attribute's title
     */
    public function getTitle()
    {
        return $this->attrValueDescriptions[Yii::$app->language]->title ?? array_values($this->attrValueDescriptions)[0]->title;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $attributeValueObject = AttributeValue::find()->select('id, alias')->where(['id' => $this->value])->one();

            if ($attributeValueObject != null) {
                $this->value_alias = $attributeValueObject->alias;
            }

            $this->value = HtmlPurifier::process($this->value);

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
//        $this->updateElastic();
    }

    /**
     * Updates related JobAttributeElastic
     * @param bool $doSave
     * @return bool|JobAttributeElastic
     */
    public function updateElastic($doSave = true)
    {
        $jobElasticService = new JobAttributeElasticService($this);
        return $jobElasticService->process($doSave);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            JobAttributeElastic::deleteAll(['id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }
}
