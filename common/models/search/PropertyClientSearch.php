<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PropertyClient;

/**
 * PropertyClientSearch represents the model behind the search form of `common\models\PropertyClient`.
 */
class PropertyClientSearch extends PropertyClient
{
    /**
     * @var string
     */
    public $realtor;

    /**
     * @var string
     */
    public $client;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'mortgage', 'status', 'created_at', 'updated_at'], 'integer'],
            [['email', 'first_name', 'last_name', 'phone', 'info'], 'safe'],
            [['realtor', 'client'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropertyClient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->realtor) {
            $this->created_by = UserFinder::getIdsByName($this->realtor);
        }

        if ($this->client) {
            $this->user_id = UserFinder::getIdsByName($this->client);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'user_id' => $this->user_id
        ]);

        return $dataProvider;
    }
}
