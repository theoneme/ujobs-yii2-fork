<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.11.2017
 * Time: 16:30
 */

namespace common\models\search\query;

use common\models\Category;
use common\models\UserAttribute;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class UserAttributeQuery
 * @package common\models\search\query
 */
class UserAttributeQuery extends UserAttribute
{
    public static function getByUser($user_id, $entity_alias)
    {

    }

    public static function findOne($condition)
    {
        return UserAttribute::findOne($condition);
    }
}
