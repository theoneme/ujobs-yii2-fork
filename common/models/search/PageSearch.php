<?php

namespace common\models\search;

use common\models\Page;
use common\models\user\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * PageSearch represents the model behind the search form about `common\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'all_views', 'lm_views', 'm_views', 'category_id', 'status'], 'integer'],
            [['alias', 'publish_date', 'title', 'username', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        $query->joinWith(['translations'])->groupBy('page.id');
//        $query->orderBy(['page.update_date' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'publish_date' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['title'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['content_translation.title' => SORT_ASC],
            'desc' => ['content_translation.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->username) {
            $query->andFilterWhere(['page.user_id' => UserFinder::getIdsByName($this->username)]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'publish_date' => $this->publish_date,
            'all_views' => $this->all_views,
            'lm_views' => $this->lm_views,
            'm_views' => $this->m_views,
            'status' => $this->status,
            'category_id' => !$this->category_id || $this->category->isLeaf() ? $this->category_id : $this->category->leaves()->select('id')->column(),
            'type' => $this->type
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias]);
        $query->andFilterWhere(['like', 'content_translation.title', $this->title]);

        return $dataProvider;
    }
}
