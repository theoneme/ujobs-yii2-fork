<?php

namespace common\models\search;

use common\models\user\User;
use common\models\UserPortfolio;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * UserPortfolioSearch represents the model behind the search form about `common\models\UserPortfolio`.
 */
class UserPortfolioSearch extends UserPortfolio
{
    /**
     * @var string
     */
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status'], 'integer'],
            [['title', 'description', 'image', 'profile.name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPortfolio::find()
            ->joinWith(['attachments', 'profile', 'user'])
//            ->andWhere(['version' => UserPortfolio::VERSION_NEW])
            ->groupBy('user_portfolio.id');

        if($this->version !== null) {
            $query->andFilterWhere([
                'user_portfolio.version' => $this->version,
            ]);
        } else {
            $query->andFilterWhere([
                'user_portfolio.version' => UserPortfolio::VERSION_NEW,
            ]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_portfolio.id' => $this->id,
            'user_portfolio.status' => $this->status
        ]);


        $username = $this->getAttribute('profile.name');
        if ($username) {
            $query->andFilterWhere(['user_portfolio.user_id' => UserFinder::getIdsByName($username)]);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['profile.name']);
    }
}