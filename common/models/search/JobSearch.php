<?php

namespace common\models\search;

use common\models\Job;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * JobSearch represents the model behind the search form about `common\models\Job`.
 */
class JobSearch extends Job
{
    public $name;

    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at', 'updated_at', 'price', 'currency_code'], 'integer'],
            [['status', 'type', 'username', 'title', 'locale', 'auto_translated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $pageSize
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pageSize = null)
    {
        $query = Job::find()
            ->joinWith(['profile', 'translation', 'user'])
            ->andWhere($this->status ? ['job.status' => $this->status] : ['not', ['job.status' => Job::STATUS_DRAFT]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize ? $pageSize : 20
            ],
            'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'created_at' => SORT_DESC]]
        ]);

//        $dataProvider->sort->attributes['profile.name'] = [
//            'asc' => ['profile.name' => SORT_ASC],
//            'desc' => ['profile.name' => SORT_DESC],
//        ];
//
//        $dataProvider->sort->attributes['translation.title'] = [
//            'asc' => ['translation.title' => SORT_ASC],
//            'desc' => ['translation.title' => SORT_DESC],
//        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'price' => $this->price,
            'currency_code' => $this->currency_code,
            'job.locale' => $this->locale
        ]);
        if ($this->auto_translated !== null) {
            $condition = ['or', ['auto_translated' => false], ['job.locale' => 'ru-RU']];
            $query->andWhere($this->auto_translated === false ? $condition : ['not', $condition]);
        }
        if ($this->username) {
            $query->andFilterWhere(['job.user_id' => UserFinder::getIdsByName($this->username)]);
        }

        $query->andFilterWhere(['job.status' => $this->status])
            ->andFilterWhere(['type' => $this->type === Job::TYPE_TENDER ? [Job::TYPE_TENDER, Job::TYPE_ADVANCED_TENDER] : $this->type])
            ->andFilterWhere(['like', 'ct.title', $this->title]);

        return $dataProvider;
    }
}
