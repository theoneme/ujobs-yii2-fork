<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.01.2017
 * Time: 17:45
 */

namespace common\models\search;

use common\models\Payment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form about `common\models\Payment`.
 */
class PaymentSearch extends Payment
{

    public $customer;
    public $seller;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at', 'updated_at', 'total', 'status', 'payment_method_id'], 'integer'],
            [['currency_code', 'code'], 'safe'],
            [['customer', 'seller'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find()
            ->joinWith(['orders', 'order', 'paymentMethod.translation', 'user', 'user.profile', 'tariff', 'tariff.tariff'])
            ->groupBy('payment.id');

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->customer) {
            $this->user_id = UserFinder::getIdsByName($this->customer);
        }
        if ($this->seller) {
            $query->andFilterWhere(['order.seller_id' => UserFinder::getIdsByName($this->seller)]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payment.id' => $this->id,
            'payment.user_id' => $this->user_id,
            'payment.created_at' => $this->created_at,
            'payment.updated_at' => $this->updated_at,
            'payment.total' => $this->total,
            'payment.status' => $this->status,
            'payment.payment_method_id' => $this->payment_method_id,
        ]);

        $query->andFilterWhere(['like', 'currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}