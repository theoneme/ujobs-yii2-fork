<?php

namespace common\models\search;

use common\models\Invoice;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * InvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_id', 'user_id', 'type', 'status', 'created_at', 'updated_at', 'cost'], 'integer'],
            [['currency_code', 'invoice_file', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find()->joinWith(['user.profile.translations', 'user.company.translations' => function($var){
            /* @var $var ActiveQuery*/
            $var->from('content_translation c_ct')->onCondition(['c_ct.entity' => 'company']);
        }])->groupBy('invoice.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'payment_id' => $this->payment_id,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'cost' => $this->cost,
        ]);

        $query->andFilterWhere(['or',
            ['like', 'user.username', $this->username],
            ['like', 'profile.name', $this->username],
            ['like', 'content_translation.title', $this->username],
            ['like', 'c_ct.title', $this->username],
        ])
            ->andFilterWhere(['like', 'currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'invoice_file', $this->invoice_file]);

        return $dataProvider;
    }
}
