<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.02.2017
 * Time: 16:26
 */

namespace common\models\search;

use common\models\UserWithdrawal;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * UserWithdrawalSearch represents the model behind the search form about `common\models\UserWithdrawal`.
 */
class UserWithdrawalSearch extends UserWithdrawal
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'amount', 'target_method', 'status', 'created_at', 'updated_at'], 'integer'],
            [['currency_code', 'target_account', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserWithdrawal::find()->joinWith(['user.profile.translations', 'user.company.translations' => function($var){
            /* @var $var ActiveQuery*/
            $var->from('content_translation c_ct')->onCondition(['c_ct.entity' => 'company']);
        }])->groupBy('user_withdrawal.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'target_method' => $this->target_method,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['or',
            ['like', 'user.username', $this->username],
            ['like', 'profile.name', $this->username],
            ['like', 'content_translation.title', $this->username],
            ['like', 'c_ct.title', $this->username],
        ])->andFilterWhere(['like', 'currency_code', $this->currency_code])
        ->andFilterWhere(['like', 'target_account', $this->target_account]);

        return $dataProvider;
    }
}