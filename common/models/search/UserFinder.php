<?php

namespace common\models\search;

use common\models\user\User;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Class UserFinder
 * @package common\models\search
 */
class UserFinder extends Model
{
    public static function getIdsByName($name) {
        return User::find()->joinWith(['profile.translations', 'company.translations' => function($var){
            /* @var $var ActiveQuery*/
            $var->from('content_translation c_ct')->onCondition(['c_ct.entity' => 'company']);
        }])->where(['or',
            ['like', 'user.username', $name],
            ['like', 'profile.name', $name],
            ['like', 'content_translation.title', $name],
            ['like', 'c_ct.title', $name],
        ])->select('user.id')->column();
    }
}