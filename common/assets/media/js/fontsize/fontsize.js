(function($)
{
    let keys = Object.keys($.Redactor.opts.langs);
    keys.forEach(function(element) {
        switch(element) {
            case 'es':
                $.Redactor.opts.langs['es'].fontsize = 'Cambia tamaño de fuente';
                $.Redactor.opts.langs['es'].fontsizeremove = 'Eliminar tamaño de fuente';
                break;
            case 'en':
                $.Redactor.opts.langs['en'].fontsize = 'Change Font Size';
                $.Redactor.opts.langs['en'].fontsizeremove = 'Remove Font Size';
                break;
            case 'ge':
                $.Redactor.opts.langs['ge'].fontsize = 'შრიფტის ზოლის შეცვლა';
                $.Redactor.opts.langs['ge'].fontsizeremove = 'ამოიღეთ შრიფტის ზომა';
                break;
            case 'zh_cn':
                $.Redactor.opts.langs['zh_cn'].fontsize = '更改字体大小';
                $.Redactor.opts.langs['zh_cn'].fontsizeremove = '删除字体大小';
                break;
            case 'ru':
                $.Redactor.opts.langs['ru'].fontsize = 'Изменить размер шрифта';
                $.Redactor.opts.langs['ru'].fontsizeremove = 'Убрать размер шрифта';
                break;
            case 'ua':
                $.Redactor.opts.langs['ua'].fontsize = 'Змінити розмір шрифту';
                $.Redactor.opts.langs['ua'].fontsizeremove = 'Видалити розмір шрифту';
                break;
        }
    });

    $.Redactor.prototype.fontsize = function()
    {
        return {
            init: function()
            {
                var fonts = [10, 11, 12, 14, 16, 18, 20, 24, 28, 30];
                var that = this;
                var dropdown = {};

                $.each(fonts, function(i, s)
                {
                    dropdown['s' + i] = { title: s + 'px', func: function() { that.fontsize.set(s); } };
                });

                dropdown.remove = { title: this.lang.get('fontsizeremove'), func: that.fontsize.reset };
                var button = this.button.add('fontsize', this.lang.get('fontsize'));

                this.button.addDropdown(button, dropdown);
            },
            set: function(size)
            {
                this.inline.format('span', 'style', 'font-size: ' + size + 'px;');
            },
            reset: function()
            {
                this.inline.removeStyleRule('font-size');
            }
        };
    };
})(jQuery);