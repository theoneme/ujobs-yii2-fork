<?php

namespace common\assets\bundles;

use yii\web\AssetBundle;

/**
 * Class ImperaviFontSize
 * @package common\assets\bundles
 */
class FontSizeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@common/assets/media';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/fontsize/fontsize.js',
    ];
}
