<?php

namespace common\assets\bundles;

use yii\web\AssetBundle;

/**
 * Class ImperaviFontSize
 * @package common\assets\bundles
 */
class FullscreenAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@common/assets/media';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/fullscreen/fullscreen.js',
    ];
}
