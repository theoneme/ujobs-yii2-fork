<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.01.2018
 * Time: 14:28
 */

namespace common\components\elasticsearch;

use yii\elasticsearch\Query as BaseQuery;

/**
 * Class Query
 * @package common\components\elasticsearch
 */
class Query extends BaseQuery
{
    /**
     * Manual set aggregations array
     * @param $aggregations
     * @return $this
     */
    public function aggregations($aggregations)
    {
        $this->aggregations = $aggregations;
        return $this;
    }
}
