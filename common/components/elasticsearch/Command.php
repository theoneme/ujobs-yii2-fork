<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 17.04.2017
 * Time: 20:52
 */

namespace common\components\elasticsearch;

use yii\elasticsearch\Command as BaseCommand;
use yii\helpers\Json;

class Command extends BaseCommand
{
    /**
     * Inserts a bulk action
     * @param string $index
     * @param string $type
     * @param string|array $data json string or array of data to store
     * @param array $options
     * @return mixed
     * @see http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/docs-bulk.html
     */
    public function bulk($index, $type, $data, $options = [])
    {
        if (empty($data)) {
            $body = '{}';
        } else if (is_array($data)) {
            $body = '';
            foreach ($data['body'] as $key => $value) {
                $body .= Json::encode($value). "\n";
            }
        } else {
            $body = $data;
        }

        return $this->db->put([$index, $type, "_bulk"], $options, $body);
    }
}