<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.01.2018
 * Time: 14:43
 */

namespace common\components\elasticsearch;

use Yii;
use yii\elasticsearch\ActiveRecord as BaseActiveRecord;

class ActiveRecord extends BaseActiveRecord
{
    /**
     * @return object
     */
    public static function find()
    {
        return Yii::createObject(ActiveQuery::class, [get_called_class()]);
    }
}