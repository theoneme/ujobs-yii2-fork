<?php

namespace common\components;

use common\helpers\SmsHelper;
use yii\base\Component;
use yii\httpclient\Client;

/**
 * Sms gateway
 */
class SmsAeroGateway extends Component
{
    /**
     * @param $to
     * @param $message
     * @param null $sender
     * @return bool
     */
    public static function sendMessage($to, $message, $sender = null)
    {
	    if ($sender === null) {
		    $sender = SmsHelper::$smsAeroSender;
		}
	    $url  = 'http://gate.smsaero.ru/send';
	    $client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
	    try {
		    $response = $client->post($url, [
				'user' => SmsHelper::$smsAeroLogin,
				'password' => md5(SmsHelper::$smsAeroPassword),
				'to' => preg_replace("/[^0-9]/", "", $to),
				'text' => $message,
				'from' => $sender,
				'answer' => 'json',
				'type' => 2
			])->send();
		    $responseData = $response->getData();
	    } catch (\Exception $e) {
		    \Yii::warning($e->getMessage());
		    return false;
	    }
	    return ($response->isOk && isset($responseData['result']) && $responseData['result'] == 'accepted');
    }
}