<?php

namespace common\components;

use common\helpers\SmsHelper;
use yii\base\Component;
use yii\httpclient\Client;

/**
 * Sms gateway
 */
class LetsAdsGateway extends Component
{
    /**
     * @param $to
     * @param $message
     * @param null $sender
     * @return bool
     */
    public static function sendMessage($to, $message, $sender = null)
    {
	    if ($sender === null) {
		    $sender = SmsHelper::$letsAdsSender;
		}
	    $url  = 'http://letsads.com/api';
        $letsAdsLogin = SmsHelper::$letsAdsLogin;
        $letsAdsPassword = SmsHelper::$letsAdsPassword;

	    $xml  = <<<xml
<?xml version="1.0" encoding="UTF-8"?>
            <request>
                <auth>
                    <login>$letsAdsLogin</login>
                    <password>$letsAdsPassword</password>
                </auth>
                <message>
                    <from>$sender</from>
                    <text>$message</text>
                    <recipient>$to</recipient>
                </message>
            </request>
xml;
	    $client = new Client([
		    'requestConfig' => ['format' => Client::FORMAT_XML],
		    'responseConfig' => ['format' => Client::FORMAT_XML]
	    ]);
	    try {
		    $response = $client->post($url, $xml)->setHeaders(['content-type' => 'text/xml'])->send();
		    $responseData = $response->getData();
	    } catch (\Exception $e) {
		    \Yii::warning($e->getMessage());
		    return false;
	    }

	    return ($response->isOk && isset($responseData['name']) && $responseData['name'] == 'Complete');
    }
}