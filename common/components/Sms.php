<?php

namespace common\components;

use common\models\user\Token;
use common\models\user\User;
use Yii;
use yii\base\Component;

/**
 * Sms.
 */
class Sms extends Component
{
    /** @var string */
    public $sender;

    /** @var string Default: `LetsAdsGateway::class`*/
    public $gateway;

	/** @var string */
	protected $welcomeTemplate;

	/** @var string */
	protected $confirmationTemplate;

	/** @var string */
	protected $reconfirmationTemplate;

	/** @var string */
	protected $recoveryTemplate;

    /** @var \dektrium\user\Module */
    protected $module;

    /**
     * @return string
     */
    public function getWelcomeTemplate()
    {
        if ($this->welcomeTemplate == null) {
            $this->setWelcomeTemplate('Welcome to {0}');
        }

        return $this->welcomeTemplate;
    }

    /**
     * @param string $welcomeTemplate
     */
    public function setWelcomeTemplate($welcomeTemplate)
    {
        $this->welcomeTemplate = $welcomeTemplate;
    }

    /**
     * @return string
     */
    public function getConfirmationTemplate()
    {
        if ($this->confirmationTemplate == null) {
            $this->setConfirmationTemplate('Account confirm code: {0}');
        }

        return $this->confirmationTemplate;
    }

    /**
     * @param string $confirmationTemplate
     */
    public function setConfirmationTemplate($confirmationTemplate)
    {
        $this->confirmationTemplate = $confirmationTemplate;
    }

    /**
     * @return string
     */
    public function getReconfirmationTemplate()
    {
        if ($this->reconfirmationTemplate == null) {
            $this->setReconfirmationTemplate('Phone change confirm code: {0}');
        }

        return $this->reconfirmationTemplate;
    }

    /**
     * @param string $reconfirmationTemplate
     */
    public function setReconfirmationTemplate($reconfirmationTemplate)
    {
        $this->reconfirmationTemplate = $reconfirmationTemplate;
    }

    /**
     * @return string
     */
    public function getRecoveryTemplate()
    {
        if ($this->recoveryTemplate == null) {
            $this->setRecoveryTemplate('Password reset code: {0}');
        }

        return $this->recoveryTemplate;
    }

    /**
     * @param string $recoveryTemplate
     */
    public function setRecoveryTemplate($recoveryTemplate)
    {
        $this->recoveryTemplate = $recoveryTemplate;
    }

    /** @inheritdoc */
    public function init()
    {
        $this->module = Yii::$app->getModule('user');
        parent::init();
    }

    /**
     * Sends an sms to a user after registration.
     *
     * @param User $user
     * @param Token $token
     * @param bool $showPassword
     * @return bool
     */
    public function sendWelcomeMessage(User $user, Token $token = null, $showPassword = false)
    {
        return true; // Не отправляем пока смс с приветствием

//	    $message = Yii::t('user', $this->getWelcomeTemplate(), Yii::$app->name);
//        if ($showPassword === true) {
//            $message .=  ". " . Yii::t('app', 'We have generated a password for you') . ": " . $user->password;
//        }
//	    if ($token !== null) {
//		    $message .=  ". " . Yii::t('app', $this->getConfirmationTemplate(), $token->code);
//	    }
//
//        return $this->sendMessage($user->phone, $message);
    }

    /**
     * Sends an sms to a user with confirmation token.
     *
     * @param User  $user
     * @param Token $token
     *
     * @return bool
     */
    public function sendConfirmationMessage(User $user, Token $token)
    {
        return $this->sendMessage($user->phone, Yii::t('app', $this->getConfirmationTemplate(), $token->code));
    }

    /**
     * Sends an sms to a user with reconfirmation token.
     *
     * @param User  $user
     * @param Token $token
     *
     * @return bool
     */
    public function sendReconfirmationMessage(User $user, Token $token)
    {
        return $this->sendMessage($user->phone, Yii::t('app', $this->getReconfirmationTemplate(), $token->code));
    }

    /**
     * Sends an sms to a user with recovery token.
     *
     * @param User  $user
     * @param Token $token
     *
     * @return bool
     */
    public function sendRecoveryMessage(User $user, Token $token)
    {
        return $this->sendMessage($user->phone, Yii::t('app', $this->getRecoveryTemplate(), $token->code));
    }

    /**
     * @param string $to
     * @param string $message
     *
     * @return bool
     */
    protected function sendMessage($to, $message)
    {
        $gateway = !empty($this->gateway) ? $this->gateway : SmsAeroGateway::class;
        return $gateway::sendMessage($to, $message, $this->sender);
    }
}