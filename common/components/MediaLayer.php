<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.11.2016
 * Time: 16:43
 */

namespace common\components;

use frostealth\yii2\aws\s3\Service;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Component;
use yii\imagine\Image;

/**
 * Class MediaLayer
 * @package common\components
 */
class MediaLayer extends Component
{
    /**
     * @param $media
     * @param int $width
     * @param int $height
     * @return string
     */
    public function tryLoadFromAws($media, $width = null, $height = null)
    {
        // Отключаем AWS
        return $media;

        if (preg_match('/^http(s)?:\/\/.*/', $media)) {
            return $media;
        }

        $cachePath = null;
        if (is_int($width) && is_int($height)) {
            $cachePath = "/cache/{$width}x{$height}";
        }

        $mode = Yii::$app->params['awsSource'];
        switch ($mode) {
            case 'cloudfront':
                $cloudfrontId = Yii::$app->params['awsCloudfrontId'];
                $completeUrl = "https://{$cloudfrontId}.cloudfront.net{$cachePath}{$media}";
                break;
            case 's3':
            default:
                $awsBucketName = Yii::$app->params['awsBucketName'];
                $awsRegion = Yii::$app->params['awsRegion'];
                $completeUrl = "https://s3.{$awsRegion}.amazonaws.com/{$awsBucketName}{$cachePath}{$media}";
        }

        return $completeUrl;
    }

    /**
     * @param $media
     * @param bool $makeThumbnails
     * @param bool $cropThumbnails
     * @return bool
     */
    public function saveToAws($media, $makeThumbnails = true, $cropThumbnails = true)
    {
        if ($media != null && file_exists(Yii::getAlias("@frontend") . "/web" . $media)) {

            if ($makeThumbnails) {
                $this->makeThumbnails($media, $cropThumbnails);
            }

            $this->applyWatermark(Yii::getAlias("@frontend") . "/web" . $media);

            // Отключаем AWS
//            /** @var Utility $utility */
//            $utility = Yii::$app->get('utility');
//            /** @var Service $s3 */
//            $s3 = Yii::$app->get('s3');
//            $fixedMediaPath = $utility->fixAwsMediaPath($media);
//            $promise = $s3->commands()
//                ->upload($fixedMediaPath, Yii::getAlias("@frontend") . "/web" . $media)
//                ->withParam('CacheControl', 'max-age=640000')
//                ->async()
//                ->execute();
//            $promise->wait();
        }

        return true;
    }

    /**
     * @param string $media
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function removeMedia($media)
    {
        if ($media != null) {
            if (file_exists(Yii::getAlias("@frontend") . "/web" . $media)) {
                unlink(Yii::getAlias("@frontend") . "/web" . $media);
            }

            // Отключаем AWS
//            /** @var Service $s3 */
//            $s3 = Yii::$app->get('s3');
//            /** @var Utility $utility */
//            $utility = Yii::$app->get('utility');
//            $fixedMediaPath = $utility->fixAwsMediaPath($media);
//            if ($s3->exist($fixedMediaPath)) {
//                $s3->delete($fixedMediaPath);
//            }

            $sizes = Yii::$app->params['images'];
            if (!empty($sizes)) {
                foreach ($sizes as $key => $size) {
                    $cachePath = "cache/{$size['width']}x{$size['height']}";

                    if (file_exists(Yii::getAlias("@frontend") . "/web/" . $cachePath . $media)) {
                        unlink(Yii::getAlias("@frontend") . "/web/" . $cachePath . $media);
                    }

                    // Отключаем AWS
//                    if ($s3->exist("{$cachePath}{$media}")) {
//                        $s3->delete("{$cachePath}{$media}");
//                    }
                }
            }
        }

        return true;
    }

    /**
     * @param $media
     * @param bool $crop
     * @return bool
     */
    public function makeThumbnails($media, $crop = true)
    {
        if ($media != null && in_array(pathinfo($media, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'png'])) {

            // Отключаем AWS
//            /** @var Service $s3 */
//            $s3 = Yii::$app->get('s3');
//            /** @var Utility $utility */
//            $utility = Yii::$app->get('utility');

            $pathinfo = pathinfo($media);
            $cachePath = "/cache/%size%" . $pathinfo['dirname'];

            foreach (Yii::$app->params['images'] as $key => $value) {
                $path = str_replace('%size%', "{$value['width']}x{$value['height']}", $cachePath);

                Yii::$app->utility->makeDir(Yii::getAlias("@frontend") . "/web" . $path);

                $originPath = Yii::getAlias("@frontend") . "/web" . $media;
                $newPath = Yii::getAlias("@frontend") . "/web" . $path . "/" . $pathinfo['basename'];

                $size = getimagesize($originPath);
                if ($crop) {
                    $minSize = min($size[0], $size[1]);
                    $x = ($size[0] - $minSize) / 2;
                    $y = ($size[1] - $minSize) / 2;

                    Image::crop($originPath, $minSize, $minSize, [$x, $y])->resize(new Box($value['width'], $value['height']))->save($newPath, ['quality' => 80]);
//                    Image::thumbnail($newPath, $value['width'], $value['height'])->save($newPath, ['quality' => 80]);
                } else {
                    Image::thumbnail($originPath, $value['width'], null)->save($newPath, ['quality' => 80]);
                }
                $this->applyWatermark($newPath);

                // Отключаем AWS
//                $fixedPath = $utility->fixAwsMediaPath($path);
//                $promise = $s3
//                    ->commands()
//                    ->upload($fixedPath . "/" . $pathinfo['basename'], $newPath)
//                    ->withParam('CacheControl', 'max-age=640000')
//                    ->async()
//                    ->execute();
//                $promise->wait();
            }

            return true;
        }

        return false;
    }

    /**
     * @param $media
     * @return bool
     */
    public function applyWatermark($media)
    {
        if ($media != null && in_array(pathinfo($media, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'png'])) {
            $image = Image::getImagine()->open($media);
            $watermark = Image::getImagine()->open(Yii::getAlias("@frontend") . "/web/images/new/watermark.png");
            $size = $image->getSize();
            $wSize = $watermark->getSize();
            $wWidth = $size->getWidth() * 0.2;
            $wHeight = $wSize->getHeight() / ($wSize->getWidth() / $wWidth);
            $watermark->resize(new Box($wWidth, $wHeight));
            $image->paste($watermark, new Point($size->getWidth() - $wWidth - 5, $size->getHeight() - $wHeight - 5))->save($media);

            return true;
        }

        return false;
    }

    /**
     * @param $link
     * @param null $target
     * @param bool $fromAws
     * @return string
     */
    public function getThumb($link, $target = null, $fromAws = true)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if (!empty($link) && file_exists(Yii::getAlias("@frontend") . "/web" . $link)) {
            $image = $link;
        }

        $fromAws = false; // Отключаем AWS
        if ($fromAws === false) {
            return $image;
        }

        $width = $height = null;

        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return $this->tryLoadFromAws($image, $width, $height);
    }
}