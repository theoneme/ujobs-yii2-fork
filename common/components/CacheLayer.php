<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 03.11.2016
 * Time: 13:00
 */

namespace common\components;

use common\models\Category;
use common\models\ContentTranslation;
use common\models\Job;
use common\models\Setting;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserPortfolio;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use Yii;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class CacheLayer
 * @package common\components
 */
class CacheLayer extends Component
{
    /**
     * @var array
     */
    public $cacheKeyToAction = [
//        'categoryAliases' => 'prepareCategoryAliasCache',
//        'category_path' => 'prepareCategoryPathCache',
//        'category_product_path' => 'prepareCategoryProductPathCache',
        'site_settings' => 'prepareSettingsCache',
        'index_cache' => 'prepareIndexCache',
        'user_cache' => 'prepareUserCache',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $categoryCacheRoutes = [];
        foreach (array_keys(Yii::$app->params['supportedLocales']) as $locale) {
            $categoryCacheRoutes["categories3_{$locale}"] = 'prepareCategoryCache';
            $categoryCacheRoutes["productCategories_{$locale}"] = 'prepareCategoryCache';
            $categoryCacheRoutes["categoryMeasures_{$locale}"] = 'prepareMeasureCache';
        }

        foreach (['job_category', 'product_category'] as $item) {
            foreach (array_keys(Yii::$app->params['supportedLocales']) as $locale) {
                $categoryCacheRoutes["categoryAliases_{$item}_{$locale}"] = 'prepareCategoryAliasCache';
            }
        }

        $this->cacheKeyToAction = array_merge($this->cacheKeyToAction, $categoryCacheRoutes);
    }

    /**
     * Loads data into cache
     */
    public function prepareCache()
    {
//        $this->prepareSettingsCache();
//        $this->prepareCategoryPathCache();
        $this->prepareCategoryCache();
        $this->prepareProductCategoryCache();
        $this->prepareIndexCache();
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        $cache = Yii::$app->cache;
        $data = $cache->get($key);

        if ($data == false) {
            $warmAction = $this->cacheKeyToAction[$key];

            $data = $this->$warmAction();
        }

        return $data;
    }

    /**
     * @param $key
     * @return bool
     */
    public function getCategoryFromCache($key)
    {
        $cache = Yii::$app->cache;
        $modifiedKey = "category_{$key}";

        $data = $cache->get($modifiedKey);

        if ($data == false) {
            $data = Category::find()
                ->select('category.id, alias, lvl, lft, rgt, root, type, image')
                ->joinWith(['translation' => function ($q) {
                    /* @var \yii\db\ActiveQuery $q */
                    $q->select('content_translation.title, content_translation.entity, content_translation.entity_id');
                }])
                ->where(['alias' => $key])
                ->one();

            $cache->set($modifiedKey, $data, 60 * 60 * 24);
        }

        return $data;
    }

    /**
     * @param int $length
     * @return mixed|string
     */
    public function getUserRandomString($length = 40)
    {
        $cache = Yii::$app->cache;

        $data = $cache->get(userId() . "_hash");
        if ($data == false) {
            $data = Yii::$app->security->generateRandomString(40);

            $cache->set(userId() . "_hash", $data, 3600);
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function prepareIndexCache()
    {
        $cache = Yii::$app->cache;
        $index = [];

        $exists = $cache->exists('index_cache');

        if ($exists == false) {
            foreach (array_keys(Yii::$app->params['supportedLocales']) as $locale) {
                $jobs = Job::find()
                    ->where(['job.type' => 'job', 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => $locale])
                    ->orderBy(new Expression('rand()'))
                    ->limit(5)
                    ->groupBy('job.id')
                    ->column();

                $tenders = Job::find()
                    ->where(['job.type' => 'tender', 'job.status' => Job::STATUS_ACTIVE, 'job.locale' => $locale])
                    ->andWhere(['exists', (new Query())
                        ->select('a.id')
                        ->from('attachment a')
                        ->where('a.entity_id = job.id')
                        ->andWhere(['a.entity' => Job::TYPE_JOB])
                    ])
                    ->orderBy(new Expression('rand()'))
                    ->limit(5)
                    ->groupBy('job.id')
                    ->column();

                $users = User::find()
                    ->joinWith(['profile'])
                    ->where(['profile.status' => Profile::STATUS_ACTIVE])
                    ->andWhere(['not', ['profile.gravatar_email' => null]])
                    ->andWhere(['not', ['profile.gravatar_email' => '']])
                    ->andWhere(['not', ['profile.name' => '']])
                    ->andWhere(['not', ['profile.bio' => '']])
                    ->andFilterWhere($locale === 'ru-RU' ? [] : ['exists', ContentTranslation::find()
                        ->from(['ct' => 'content_translation'])
                        ->andWhere("profile.user_id = ct.entity_id")
                        ->andWhere(["ct.entity" => 'profile'])
                        ->andWhere(['ct.locale' => $locale])
                    ])
                    ->orderBy(new Expression('rand()'))
                    ->limit(5)
                    ->groupBy('user.id')
                    ->column();

                $category1 = Category::find()->joinWith(['translation'])->where(['alias' => 'dizayn', 'type' => 'job_category'])->one();
                $category2 = Category::find()->joinWith(['translation'])->where(['alias' => 'reklama-marketing', 'type' => 'job_category'])->one();
                $category3 = Category::find()->joinWith(['translation'])->where(['alias' => 'yuridicheskaya-pomoshch', 'type' => 'job_category'])->one();
                $category4 = Category::find()->joinWith(['translation'])->where(['alias' => 'krasota-i-zdorove', 'type' => 'job_category'])->one();

                $jobsInCategory1 = Job::find()
                    ->select('job.id, job.alias, job.type, job.status, job.parent_category_id')
                    ->joinWith(['firstAttachment'])
                    ->where(['job.parent_category_id' => $category1->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->limit(2)
                    ->orderBy(new Expression('rand()'))
                    ->asArray()
                    ->all();

                $jobsInCategory2 = Job::find()
                    ->select('job.id, job.alias, job.type, job.status, job.parent_category_id')
                    ->joinWith(['firstAttachment'])
                    ->where(['job.parent_category_id' => $category2->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->limit(2)
                    ->orderBy(new Expression('rand()'))
                    ->asArray()
                    ->all();

                $jobsInCategory3 = Job::find()
                    ->select('job.id, job.alias, job.type, job.status, job.parent_category_id')
                    ->joinWith(['firstAttachment'])
                    ->where(['job.parent_category_id' => $category3->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->limit(2)
                    ->orderBy(new Expression('rand()'))
                    ->asArray()
                    ->all();

                $jobsInCategory4 = Job::find()
                    ->select('job.id, job.alias, job.type, job.status, job.parent_category_id')
                    ->joinWith(['firstAttachment'])
                    ->where(['job.parent_category_id' => $category4->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->limit(2)
                    ->orderBy(new Expression('rand()'))
                    ->asArray()
                    ->all();

                $jobsCountInCategory1 = Job::find()
                    ->joinWith(['attachments'])
                    ->where(['job.parent_category_id' => $category1->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->groupBy('job.id')
                    ->count();

                $jobsCountInCategory2 = Job::find()
                    ->joinWith(['attachments'])
                    ->where(['job.parent_category_id' => $category2->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->groupBy('job.id')
                    ->count();

                $jobsCountInCategory3 = Job::find()
                    ->joinWith(['attachments'])
                    ->where(['job.parent_category_id' => $category3->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->groupBy('job.id')
                    ->count();

                $jobsCountInCategory4 = Job::find()
                    ->joinWith(['attachments'])
                    ->where(['job.parent_category_id' => $category4->id, 'type' => Job::TYPE_JOB, 'status' => Job::STATUS_ACTIVE])
                    ->groupBy('job.id')
                    ->count();

                $index[$locale] = [
                    'users' => $users,
                    'jobs' => $jobs,
                    'tenders' => $tenders,
                    'jobsInCategory1' => $jobsInCategory1,
                    'jobsCountInCategory1' => $jobsCountInCategory1,
                    'jobsInCategory2' => $jobsInCategory2,
                    'jobsCountInCategory2' => $jobsCountInCategory2,
                    'jobsInCategory3' => $jobsInCategory3,
                    'jobsCountInCategory3' => $jobsCountInCategory3,
                    'jobsInCategory4' => $jobsInCategory4,
                    'jobsCountInCategory4' => $jobsCountInCategory4
                ];
            }

            $cache->set('index_cache', $index, 1800);
        }

        return $index;
    }

    /**
     * @return array
     */
    protected function prepareUserCache()
    {
        $cache = Yii::$app->cache;

        $userId = Yii::$app->user->identity->company_user_id ?? userId();
        $exists = $cache->exists('user_cache_' . $userId);
        if ($exists === false) {
            $userData = [
                'servicesCount' => Job::find()->select('id')
                    ->where(['user_id' => $userId, 'type' => Job::TYPE_JOB])
                    ->andWhere(['not', ['status' => Job::STATUS_DELETED]])
                    ->count(),
                'productsCount' => Product::find()->select('id')
                    ->where(['user_id' => $userId, 'type' => Product::TYPE_PRODUCT])
                    ->andWhere(['not', ['status' => Product::STATUS_DELETED]])
                    ->count(),
                'portfoliosCount' => UserPortfolio::find()->select('id')
                    ->where(['version' => UserPortfolio::VERSION_NEW, 'user_id' => $userId])
                    ->andWhere(['not', ['status' => UserPortfolio::STATUS_DELETED]])
                    ->count(),
                'ordersOnMyJobsCount' => Order::find()->select('id')->where(['seller_id' => $userId, 'product_type' => [Order::TYPE_JOB_PACKAGE, Order::TYPE_OFFER]])->count(),
                'ordersOnMyProducts' => Order::find()->select('id')->where(['seller_id' => $userId, 'product_type' => Order::TYPE_PRODUCT])->count(),
                'tendersIWorkOn' => Order::find()->select('id')->where([
                    'seller_id' => $userId,
                    'product_type' => [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER],
                    'status' => [
                        Order::STATUS_TENDER_ACTIVE,
                        Order::STATUS_TENDER_DELIVERED,
                        Order::STATUS_TENDER_REQUIRES_TASK_APPROVE,
                        Order::STATUS_TENDER_PAID,
                        Order::STATUS_TENDER_REQUIRES_WEEKLY_TASK,
                        Order::STATUS_TENDER_AWAITING_REVIEW
                    ]
                ])->andWhere(['not', ['status' => [
                    Order::STATUS_TENDER_UNAPPROVED,
                    Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
                    Order::STATUS_CANCELLED
                ]]])->count(),
                'myResponses' => Order::find()->select('id')->where([
                    'seller_id' => $userId,
                    'product_type' => [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER],
                ])->andWhere(['status' => [
                    Order::STATUS_TENDER_UNAPPROVED,
                    Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
                    Order::STATUS_TENDER_APPROVED,
                ]])->count(),
                'myProductResponses' => Order::find()->select('id')->where([
                    'seller_id' => $userId,
                    'product_type' => Order::TYPE_PRODUCT_TENDER,
                ])->andWhere(['status' => [
                    Order::STATUS_PRODUCT_TENDER_UNAPPROVED,
                    Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION,
                    Order::STATUS_PRODUCT_TENDER_APPROVED,
                ]])->count(),
                'productTendersCount' => Product::find()->select('id')
                    ->where(['user_id' => $userId, 'type' => Product::TYPE_TENDER])
                    ->andWhere(['not', ['status' => Product::STATUS_DELETED]])
                    ->count(),
                'jobTendersCount' => Job::find()->select('id')
                    ->where(['user_id' => $userId, 'type' => Job::TYPE_TENDER])
                    ->andWhere(['not', ['status' => Job::STATUS_DELETED]])
                    ->count(),
                'jobOrdersCount' => Order::find()->select('id')->where(['customer_id' => $userId, 'product_type' => [Order::TYPE_JOB_PACKAGE, Order::TYPE_OFFER]])->count(),
                'productOrdersCount' => Order::find()->select('id')->where(['customer_id' => $userId, 'product_type' => [Order::TYPE_PRODUCT]])->count(),
                'tendersCount' => Order::find()->select('id')->where([
                    'customer_id' => $userId,
                    'product_type' => [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER],
                ])->andWhere(['not', ['status' => [
                    Order::STATUS_TENDER_UNAPPROVED,
                    Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
                    Order::STATUS_CANCELLED
                ]]])->count(),
                'responsesCount' => Order::find()->select('id')->where([
                    'customer_id' => $userId,
                    'product_type' => [Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER],
                ])->andWhere(['status' => [
                    Order::STATUS_TENDER_UNAPPROVED,
                    Order::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION
                ]])->count(),
                'responsesProductsCount' => Order::find()->select('id')->where([
                    'customer_id' => $userId,
                    'product_type' => Order::TYPE_PRODUCT_TENDER,
                ])->andWhere(['status' => [
                    Order::STATUS_PRODUCT_TENDER_UNAPPROVED,
                    Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                    Order::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION,
                    Order::STATUS_PRODUCT_TENDER_APPROVED
                ]])->count()
            ];

            $cache->set('user_cache_' . $userId, $userData, 30 * 60);
        } else {
            $userData = $cache->get('user_cache_' . $userId);
        }

        return $userData;
    }

    /**
     * @return array
     */
    protected function prepareSettingsCache()
    {
        $cache = Yii::$app->cache;
        $settings = [];

        $exists = $cache->exists('site_settings');
        if ($exists == false) {
            $settingsAR = Setting::find()->all();
            $settings = ArrayHelper::map($settingsAR, 'key', 'value');

            $cache->set('site_settings', $settings, 60 * 60 * 24);
        }

        return $settings;
    }

    /**
     * @return array
     */
    protected function prepareCategoryPathCache()
    {
        $cache = Yii::$app->cache;
        $path = [];

        $exists = $cache->exists('category_path');
        if ($exists == false) {
            $path = [];
            $categories = Category::find()
                ->joinWith(['translations'])
                ->where(['type' => 'job_category'])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->all();

            foreach ($categories as $category) {
                $path[$category->id] = [];
                if ($category->lvl > 0) {
                    $parents = $category
                        ->parents()
                        ->select('category.id, alias, lvl, lft, rgt, type')
                        ->orderBy('lvl asc')
                        ->all();

                    $path[$category->id]['alias'] = $category->alias;
                    foreach ($parents as $parent) {
                        $path[$category->id]['parents'][] = [
                            'alias' => $parent->alias,
                            'id' => $parent->id
                        ];
                    }

                } else {
                    $path[$category->id] = [
                        'alias' => $category->alias,
                        'parents' => []
                    ];
                }
                $children = $category
                    ->children(1)
                    ->select('category.id, alias, lvl, lft, rgt, type')
                    ->orderBy('lvl asc')
                    ->all();

                foreach ($children as $child) {
                    $path[$category->id]['children'][] = [
                        'alias' => $child->alias,
                        'id' => $child->id
                    ];
                }
            }

            $catalogCategories = Category::find()
                ->joinWith(['translations'])
                ->where(['type' => 'service_category'])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->all();

            $allJobCategories = Category::find()
                ->joinWith(['translations'])
                ->where(['type' => 'job_category'])
                ->andWhere(['lvl' => 1])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->orderBy('sort_order asc')
                ->all();

            foreach ($catalogCategories as $category) {
                $path[$category->id] = [];

                foreach ($allJobCategories as $child) {
                    $path[$category->id]['children'][] = [
                        'alias' => $child->alias,
                        'id' => $child->id
                    ];
                }
            }

            $cache->set('category_path', $path, 60 * 60 * 24 * 10);
        }
        return $path;
    }

    /**
     * @return array
     */
    protected function prepareCategoryProductPathCache()
    {
        $cache = Yii::$app->cache;
        $path = [];

        $exists = $cache->exists('category_product_path');
        if ($exists == false) {
            $path = [];
            $categories = Category::find()
                ->joinWith(['translations'])
                ->where(['type' => 'product_category'])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->all();

            foreach ($categories as $category) {
                $path[$category->id] = [];
                if ($category->lvl > 0) {
                    $parents = $category
                        ->parents()
                        ->select('category.id, alias, lvl, lft, rgt, type')
                        ->orderBy('lvl asc')
                        ->all();

                    $path[$category->id]['alias'] = $category->alias;
                    foreach ($parents as $parent) {
                        $path[$category->id]['parents'][] = [
                            'alias' => $parent->alias,
                            'id' => $parent->id
                        ];
                    }

                } else {
                    $path[$category->id] = [
                        'alias' => $category->alias,
                        'parents' => []
                    ];
                }
                $children = $category
                    ->children(1)
                    ->select('category.id, alias, lvl, lft, rgt, type')
                    ->orderBy('lvl asc')
                    ->all();

                foreach ($children as $child) {
                    $path[$category->id]['children'][] = [
                        'alias' => $child->alias,
                        'id' => $child->id
                    ];
                }
            }

            $catalogCategories = Category::find()
                ->joinWith(['translations'])
                ->where(['alias' => 'product_root'])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->all();

            $allJobCategories = Category::find()
                ->joinWith(['translations'])
                ->where(['type' => 'product_category'])
                ->andWhere(['lvl' => 1])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->orderBy('sort_order asc')
                ->all();

            foreach ($catalogCategories as $category) {
                $path[$category->id] = [];

                foreach ($allJobCategories as $child) {
                    $path[$category->id]['children'][] = [
                        'alias' => $child->alias,
                        'id' => $child->id
                    ];
                }
            }

            $cache->set('category_product_path', $path, 60 * 60 * 24 * 10);
        }
        return $path;
    }

    /**
     * @return array
     */
    protected function prepareCategoryCache()
    {
        $cache = Yii::$app->cache;
        $categoryTree = [];

        $exists = $cache->exists('categories3_' . Yii::$app->language);
        if ($exists == false) {
            $categories = Category::find()
                ->joinWith(['translation' => function ($q) {
                    return $q
                        ->select('content_translation.slug, content_translation.title, content_translation.entity, content_translation.entity_id, content_translation.locale')
                        ->andWhere(['locale' => Yii::$app->language]);
                }])
                ->where(['root' => Category::find()->where(['alias' => 'root'])->select('id')->scalar()])
                ->andWhere(['type' => 'job_category'])
                ->andWhere(['lvl' => 1])
                ->select('category.id, lvl, lft, rgt, root')
                ->orderBy('sort_order asc')
                ->indexBy('id')
                ->asArray()
                ->all();
            $cache->set('categories3_' . Yii::$app->language, Yii::$app->utility->recursiveNestedTree($categories, 'job_category', Yii::$app->language), 60 * 60 * 24 * 10);
        }

        return $categoryTree;
    }

    /**
     * @return array
     */
    protected function prepareProductCategoryCache()
    {
        $cache = Yii::$app->cache;
        $categoryTree = [];

        $exists = $cache->exists('productCategories_' . Yii::$app->language);
        if ($exists == false) {
            $categories = Category::find()
                ->joinWith(['translation' => function ($q) {
                    return $q
                        ->select('content_translation.slug, content_translation.title, content_translation.entity, content_translation.entity_id, content_translation.locale')
                        ->andWhere(['locale' => Yii::$app->language]);
                }])
                ->where(['root' => Category::find()->where(['alias' => 'product_root'])->select('id')->scalar()])
                ->andWhere(['type' => 'product_category'])
                ->andWhere(['lvl' => 1])
                ->select('category.id, lvl, lft, rgt, root')
                ->orderBy('sort_order asc')
                ->indexBy('id')
                ->asArray()
                ->all();
            $cache->set('productCategories_' . Yii::$app->language, Yii::$app->utility->recursiveNestedTree($categories, 'product_category', Yii::$app->language), 60 * 60 * 24 * 10);
        }

        return $categoryTree;
    }

    /**
     * @param $source
     * @return array
     */
    public function getCategoryAliasCache($source)
    {
        $cache = Yii::$app->cache;
        $type = Yii::$app->controller->module->id == 'board' ? 'product_category' : 'job_category';
        $locale = Yii::$app->language;
        $result = $cache->get("categoryAliases_{$type}_{$source}_{$locale}");
        if ($result === false) {
            $category = Category::find()
                ->joinWith(['translations'])
                ->andWhere(['type' => $type, 'content_translation.slug' => $source])
                ->select('category.id, alias, lvl, lft, rgt, root, type')
                ->asArray()
                ->one();

            foreach (Yii::$app->params['supportedLocales'] as $locale2 => $code2) {
                if ($locale != $locale2 && isset($category['translations'][$locale]['slug']) && isset($category['translations'][$locale2])) {
                    $result[$locale2] = $category['translations'][$locale2]['slug'];
                }
            }

            $cache->set("categoryAliases_{$type}_{$source}_{$locale}", $result, 60 * 60 * 24 * 10);
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function prepareMeasureCache()
    {
        $cache = Yii::$app->cache;
        $locale = Yii::$app->language;
        $result = [];
        $exists = $cache->exists("categoryMeasures_{$locale}");
        if ($exists == false) {
            $categories = Category::find()->select('category.id, alias, lvl, lft, rgt, root, type')->all();
            foreach ($categories as $category) {
                /* @var $category Category */
                $measures = $category->getMeasuresArray($locale);
                if (count($measures)) {
                    $result[$category->id] = $measures;
                }
            }
            $cache->set("categoryMeasures_{$locale}", $result, 60 * 60 * 24 * 10);
        }

        return $result;
    }
}
