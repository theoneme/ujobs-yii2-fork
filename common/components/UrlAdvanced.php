<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.11.2017
 * Time: 16:35
 */

namespace common\components;

use common\models\user\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;
use yii\web\Session;

/**
 * Class UrlAdvanced
 * @package common\services
 */
class UrlAdvanced extends Url
{
    /**
     * @param string $url
     * @param bool $scheme
     * @param string $customManager
     * @return string
     */
    public static function to($url = '', $scheme = false, $customManager = 'prettyUrlManager')
    {
        if(isset(Yii::$app->$customManager)) {
            $url = Yii::$app->$customManager->createUrl($url);
            return $scheme === true ? parent::to($url, $scheme) : $url;
        } else {
            return parent::to($url, $scheme);
        }
    }
}