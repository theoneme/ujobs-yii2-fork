<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:34
 */

namespace common\components;

use common\modules\store\models\Currency;
use common\modules\store\models\CurrencyExchange;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class CurrencyHelper
 * @package common\components
 */
class CurrencyHelper extends Component
{
    /**
     * @param $from
     * @param $to
     * @param $amount
     * @param bool $naeb
     * @return float
     */
    public static function convert($from, $to, $amount, $naeb = false)
    {
        if ($from === $to) {
            return $amount;
        }

        if (!array_key_exists("{$from}-{$to}", Yii::$app->params['runtime']['currency']['ratios'])) {
            $ratio = CurrencyExchange::getDb()->cache(function ($db) use ($from, $to) {
                return CurrencyExchange::find()->where(['code_from' => $from, 'code_to' => $to])->select('ratio')->scalar();
            });

            Yii::$app->params['runtime']['currency']['ratios']["{$from}-{$to}"] = $ratio;
        } else {
            $ratio = Yii::$app->params['runtime']['currency']['ratios']["{$from}-{$to}"];
        }

        $result = ceil($amount * $ratio);

        return ($naeb && $result > 15) ? ceil($result / 5) * 5 : $result;
    }

    /**
     * @param $code
     * @param $amount
     * @param int $digits
     * @param string $template
     * @return string
     */
    public static function format($code, $amount, $digits = 0, $template = '{sl}{amount}{sr}')
    {
        if (!array_key_exists($code, Yii::$app->params['runtime']['currency']['currencies'])) {
            /* @var $currency Currency */
            $currency = Currency::getDb()->cache(function ($db) use ($code) {
                return Currency::find()->where(['code' => $code])->select('symbol_left, symbol_right')->asArray()->one();
            });

            Yii::$app->params['runtime']['currency']['currencies'][$code] = $currency;
        } else {
            $currency = Yii::$app->params['runtime']['currency']['currencies'][$code];
        }

        if ($currency === null) {
            return $amount;
        }

        return strtr($template, [
            '{sl}' => $currency['symbol_left'],
            '{amount}' => Yii::$app->formatter->asDecimal($amount, $digits),
            '{sr}' => $currency['symbol_right']
        ]);
    }

    /**
     * @param $from
     * @param $to
     * @param $amount
     * @param bool $naeb
     * @param int $digits
     * @param string $template
     * @return string
     */
    public static function convertAndFormat($from, $to, $amount, $naeb = false, $digits = 0, $template = '{sl}{amount}{sr}')
    {
        return self::format($to, self::convert($from, $to, $amount, $naeb), $digits, $template);
    }

    /**
     * @return array
     */
    public static function getRatios() {
        return ArrayHelper::map(
            CurrencyExchange::getDb()->cache(function ($db) { return CurrencyExchange::find()->select(['code_from', 'code_to', 'ratio'])->all();}),
            'code_to',
            'ratio',
            'code_from'
        );
    }
}