<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.01.2018
 * Time: 14:58
 */

namespace common\components\mailer;

use Yii;
use yii\swiftmailer\Mailer as BaseMailer;

/**
 * Class Mailer
 * @package common\components\mailer
 */
class Mailer extends BaseMailer
{
    /**
     * @inheritdoc
     */
    protected function sendMessage($message)
    {
        $address = $message->getTo();
        if (is_array($address)) {
            $address = implode(', ', array_keys($address));
        }
        Yii::info('Sending email "' . $message->getSubject() . '" to "' . $address . '"', __METHOD__);

        try {
            return $this->getSwiftMailer()->send($message->getSwiftMessage()) > 0;
        } catch (\Swift_TransportException $e) {
            Yii::error($e);
            return true;
        }
    }
}