<?php

namespace common\components;

use common\models\Attachment;
use common\models\Category;
use common\modules\store\models\Currency;
use Yii;
use yii\base\Component;
use yii\console\Application;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\AssetBundle;

class Utility extends Component
{
    /**
     * @param $inputString
     * @return string
     */
    public function transliterate($inputString)
    {
        $text = preg_replace("%www|http:\/\/|[^єіїáéíóúüña-zа-яა-ჸ0-9-\s]%ui", "", mb_strtolower($inputString));
        $text = trim($text);
        $transliterations = array_merge(
            self::$russianTransliterations,
            self::$georgianTransliterations,
            self::$ukrainianTransliterations,
            self::$spanishTransliterations,
            [
                " " => "-",
                "_" => "-"
            ]
        );
        if (preg_match("/[єіїáéíóúüñа-яა-ჸ\s+]/ui", $text)) {
            $text = strtr($text, $transliterations);
        }
        $text = preg_replace("/[\s]/", "-", $text);
        $text = preg_replace("/\-{2,}/", "-", $text);
        return trim($text);
    }

    /**
     * @param $word
     * @param $he
     * @param $she
     * @param $it
     * @param $they
     * @return mixed
     */
    public function kind($word, $he, $she, $it, $they)
    {
        switch (mb_substr($word, -1)) {
            case 'а':
            case 'я':
                return $she;
                break;
            case 'е':
            case 'о':
                return $it;
                break;
            case 'и':
            case 'ы':
                return $they;
                break;
            default:
                return $he;
        }
    }

    /**
     * @param array $parentCategories
     * @param \common\models\Category $categoryObj
     * @return array
     */
    public function buildCategoryBreadcrumbs($parentCategories, $categoryObj)        // Присоединить родительские категории (если они есть) к крошкам
    {
        $path = [];
        $urlRoute = ['category/static'];

        if ($parentCategories) {
            uasort($parentCategories, function ($first, $second) {
                if ($first['lvl'] == $second['lvl']) {
                    return 0;
                }
                return ($first['lvl'] > $second['lvl']) ? 1 : -1;
            });

            $categoryCounter = 0;
            foreach ($parentCategories as $key => $parent) {
                $extendedUrlRoute = $urlRoute;
                foreach ($path as $k => $item) {
                    $extendedUrlRoute['category_' . ($k + 1)] = $item['alias'];
                }

                $extendedUrlRoute['category_' . ($key + 1)] = $parent['alias'];

                $pathItem = [
                    'label' => $parent['title'],
                    'url' => $extendedUrlRoute,
                    //'alias' => $parent['alias']
                ];
                $path[] = $pathItem;

                $categoryCounter++;
            }
        }

        $path[] = [
            'label' => $categoryObj->translation->title
        ];

        return $path;
    }

    /**
     * @param array $parentCategories
     * @param \common\models\Page $pageObj
     * @param string $route
     * @return array
     */
    public function buildPageBreadcrumbs($parentCategories, $pageObj, $route = 'category/static')        // Присоединить родительские категории (если они есть) к крошкам
    {
        $path = [];
        $urlRoute = ["{$route}"];

        $categoryCounter = 0;

        if ($parentCategories) {
            uasort($parentCategories, function ($first, $second) {
                if ($first['lvl'] == $second['lvl']) {
                    return 0;
                }
                return ($first['lvl'] > $second['lvl']) ? 1 : -1;
            });

            $categoryCounter = 0;
            foreach ($parentCategories as $key => $parent) {
                $extendedUrlRoute = $urlRoute;
                foreach ($path as $k => $item) {
                    $extendedUrlRoute['category_' . ($k + 1)] = $item['alias'];
                }

                $extendedUrlRoute['category_' . ($key + 1)] = $parent['alias'];

                $pathItem = [
                    'label' => $parent['title'],
                    'url' => $extendedUrlRoute,
                    //'alias' => $parent['alias']
                ];
                $path[] = $pathItem;

                $categoryCounter++;
            }
        }

        if ($pageObj->category) {
            $extendedUrlRoute = $urlRoute;
            foreach ($path as $k => $item) {
                $extendedUrlRoute['category_' . ($k + 1)] = $item['alias'];
            }

            $extendedUrlRoute['category_' . ($categoryCounter + 1)] = $pageObj->category->alias;
            $pathItem = [
                'label' => $pageObj->category->translation->title,
                'url' => $extendedUrlRoute
            ];
            $path[] = $pathItem;
        }

        $path[] = [
            'label' => $pageObj->translation->title
        ];

        return $path;
    }

    /**
     * @param $currentCategory
     * @param string $currentPageTitle
     * @param string $route
     * @param array $extraParams
     * @return array
     */
    public function buildPageBreadcrumbsNoLevel($currentCategory, $currentPageTitle = '', $route = 'category/static', $extraParams = [])
    {
        $path = [];

        if ($currentCategory) {
            $parentCategories = $currentCategory->getParents();
            if ($parentCategories) {
                uasort($parentCategories, function ($first, $second) {
                    if ($first['lvl'] == $second['lvl']) {
                        return 0;
                    }
                    return ($first['lvl'] > $second['lvl']) ? 1 : -1;
                });
                foreach ($parentCategories as $key => $parent) {
                    $path[] = [
                        'label' => $parent['title'],
                        'url' => $this->getCategoryUrl($route, $parent['alias'], $extraParams),
                    ];
                }
            }

            if ($currentCategory->translation && $currentCategory->translation->title != $currentPageTitle) {
                $path[] = [
                    'label' => $currentCategory->translation->title,
                    'url' => $this->getCategoryUrl($route, $currentCategory->translation->slug, $extraParams)
                ];
            }
        }

        $path[] = [
            'label' => $currentPageTitle
        ];

        return $path;
    }

    /**
     * @param $route
     * @param $alias
     * @param $extraParams
     * @return array
     */
    public function getCategoryUrl($route, $alias, $extraParams)
    {
        if ($alias !== 'root' && $route === '/category/videos') {
            $extraParams['alias'] = $alias;
            $extraParams['category_1'] = null;
        }
        $url = array_merge([$route, 'category_1' => $alias], $extraParams);
        if ($alias === 'root' && in_array($route, ['/category/jobs', '/category/tenders', '/category/pros', '/category/videos', '/category/articles'])) {
            switch ($route) {
                case '/category/jobs':
                    $url = array_merge(['/category/job-catalog'], $extraParams);
                    break;
                case '/category/tenders':
                    $url = array_merge(['/category/tender-catalog'], $extraParams);
                    break;
                case '/category/pros':
                    $url = array_merge(['/category/pro-catalog'], $extraParams);
                    break;
                case '/category/articles':
                    $url = array_merge(['/category/article-catalog'], $extraParams);
                    break;
                case '/category/videos':
                    $url = array_merge(['/category/video-catalog'], $extraParams);
                    break;

            }
        } else if ($alias === 'product_root' && $route === '/board/category/products') {
            $url = array_merge([$route], $extraParams);
        } else if ($alias === 'product_root' && $route === '/board/category/store') {
            $url = array_merge([$route], $extraParams);
        }

        return $url;
    }

    /**
     * @param $value
     * @return string
     */
    public function convertMinutes($value)
    {
        $hours = $value / 60;

        if ($hours < 1) {
            return (int)$value . " " . Yii::t('app', '{minute, plural, one{minute} other{minutes}}', ['minute' => (int)$value]);
        } elseif ($hours <= 24) {
            return $hours . " " . Yii::t('app', '{hour, plural, one{hour} other{hours}}', ['hour' => $hours]);
        } else {
            return (int)($hours / 24) . " " . Yii::t('app', '{day, plural, one{day} other{days}}', ['day' => (int)($hours / 24)]);
        }
    }

    /**
     * @param $publish_date
     * @param $ttl
     * @return string
     */
    public function announcementTimestampToTime($publish_date, $ttl)
    {
        $currentTime = new \DateTime();

        $targetTime = \DateTime::createFromFormat('U', ($publish_date + $ttl * 60));

        $interval = $currentTime->diff($targetTime);
        $days = (int)$interval->format('%D');
        $hours = (int)$interval->format('%H');

        if ((int)$interval->format('%M') > 0) {
            $days += 30 * ((int)$interval->format('%M'));
        }

        if ($days == 0) {
            //return $interval->format('%H:%I:%S');
            return Yii::t('app', '{n} hour|{n} hours', $hours);
        } else {
            return Yii::t('app', '{n} day|{n} days', $days);
        }
    }

    /**
     * @param $objects Attachment[]
     * @return array
     */
    public function makeImagesFromObjects($objects)
    {
        $output = [];

        if (is_array($objects)) {
            foreach ($objects as $object) {
                $output[] = Html::img($object->content, ['class' => 'file-preview-image', 'alt' => '', 'title' => '']);
            }
        }

        return $output;
    }

    /**
     * @param $objects Attachment[]
     * @return array
     */
    public function makeImageConfigFromObjects($objects)
    {
        $output = [];

        if (is_array($objects)) {
            foreach ($objects as $object) {
                $output[] = [
                    'caption' => basename($object->content),
                    'url' => Url::toRoute('/image/delete'),
                    'key' => 'image_init_' . $object->id
                ];
            }
        }
        return $output;
    }

    /**
     * @param $key
     * @param $array
     * @return mixed
     */
    public function fetchElementFromArray($key, &$array)
    {
        $element = null;

        if (isset($array[$key])) {
            $element = $array[$key];

            unset($array[$key]);
        }

        return $element;
    }

    /**
     * @param $category
     * @return mixed
     */
    public function recursiveNestedTree($category, $type, $language)
    {
        foreach ($category as $id => $range) {
            $children = [];

            if ($range['lvl'] < 2) {
                $children = Category::find()
                    ->joinWith(['translation' => function ($q) use ($language) {
                        /* @var ActiveQuery $q */
                        return $q
                            ->select('content_translation.slug, content_translation.title, content_translation.entity, content_translation.entity_id, content_translation.locale')
                            ->andWhere(['locale' => $language]);
                    }])
                    ->where(['type' => $type, 'lvl' => $range['lvl'] + 1])
                    ->andWhere(['>', 'lft', $range['lft']])
                    ->andWhere(['<', 'rgt', $range['rgt']])
                    ->andWhere(['root' => $range['root']])
                    ->select('category.id, lvl, lft, rgt, root')
                    ->orderBy('lft asc')
                    ->indexBy('id')
                    ->groupBy('category.id')
                    ->limit(10)
                    ->asArray()
                    ->all();
            }

            $category[$id]['children'] = $this->recursiveNestedTree($children, $type, $language);
        }

        return $category;
    }

    /**
     * @param null $array
     * @param $key
     * @param $delimeter
     * @return null
     */
    public function getElementFromArrayModule($array = null, $key, $delimeter)
    {
        $value = null;

        if (!empty($array)) {
            $parts = explode($delimeter, $array);
            $elementsInArray = count($parts);

            $exactKey = $key % $elementsInArray;

            if (isset($parts[$exactKey])) {
                $value = $parts[$exactKey];
            }
        }

        return $value;
    }

    /**
     * @param $path
     * @return null|string
     */
    public function fixAwsMediaPath($path)
    {
        $fixedPath = null;

        if ($path != null) {
            $fixedPath = ltrim($path, '/');
        }

        return $fixedPath;
    }

    /**
     * @param $dir
     * @return bool
     */
    public function makeDir($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
            if (Yii::$app instanceof Application) {
                chown($dir, 'www-data');
                chgrp($dir, 'www-data');
            }
            return true;
        }
        return false;
    }

    /**
     * @param $input
     * @return string
     */
    public function lowerFirstLetter($input)
    {
        $result = '';
        if ($input != '') {
            $result = mb_strtolower(mb_substr($input, 0, 1)) . mb_substr($input, 1);
        }

        return $result;
    }

    /**
     * @param $input
     * @return string
     */
    public function upperFirstLetter($input)
    {
        $result = '';
        if ($input != '') {
            $result = mb_strtoupper(mb_substr($input, 0, 1)) . mb_substr($input, 1);
        }

        return $result;
    }

    /**
     * @param $num
     * @return string
     */
    public function num2str($num)
    {
        $nul = 'ноль';
        $ten = [
            ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
            ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ];
        $a20 = ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];
        $tens = [2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
        $hundred = ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];
        $unit = [
            ['копейка', 'копейки', 'копеек', 1],
            ['рубль', 'рубля', 'рублей', 0],
            ['тысяча', 'тысячи', 'тысяч', 1],
            ['миллион', 'миллиона', 'миллионов', 0],
            ['миллиард', 'милиарда', 'миллиардов', 0]
        ];
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = [];
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) {
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1;
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                $out[] = $hundred[$i1];
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                if ($uk > 1) $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            }
        } else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]);
        $out[] = $kop . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]);
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * @param integer $n
     * @param string $f1
     * @param string $f2
     * @param string $f5
     * @return string
     */
    protected function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    /**
     * @param null $city
     * @param null $street
     * @param null $house
     * @param null $flat
     * @return string
     */
    public function formatAddress($city = null, $street = null, $house = null, $flat = null)
    {
        if (!$flat) {
            return sprintf("г. %s, %s, д. %s", $city, $street, $house);
        } else {
            return sprintf("г. %s, %s, д. %s, кв. %s", $city, $street, $house, $flat);
        }
    }

    /**
     * @param null $firstname
     * @param null $lastname
     * @param null $middlename
     * @return string
     */
    public function formatName($firstname = null, $lastname = null, $middlename = null)
    {
        return sprintf("%s %s %s", $lastname, $firstname, $middlename);
    }

    /**
     * @param $url
     * @return mixed
     */
    public function cureUrl($url)
    {
        $url = preg_replace('/(%5B+[0-9]+%5D)/', '%5B%5D', $url);

        return $url;
    }

    /**
     * @param $code
     * @return string
     */
    public function localeCurrentUrl($code)
    {
        $locale = array_search($code, Yii::$app->params['supportedLocales'], true);
        if ($locale === false) {
            return Url::current();
        }
        $params = ['app_language' => $code];
        $alias = Yii::$app->request->get('category_1');
        if ($alias !== null) {
            $categoryAliases = Yii::$app->cacheLayer->getCategoryAliasCache($alias);

            if (isset($categoryAliases[$locale])) {
                $params['category_1'] = $categoryAliases[$locale];
            } else {
                $params[0] = '/site/index';
                return ($code === 'ru' ? '/ru' : Url::to(['/site/index', 'app_language' => $code]));
            }
        }

        $url = Url::current($params);
        $url = $url === '/' ? '' : $url;
        return ($code === 'ru' ? '/ru' : '') . $url;
    }

    /**
     * @param $price
     * @return string
     */
    public function formatBannerPrice($price)
    {
        $actualPrice = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $price);
        $stringPrice = (string)$actualPrice;

        $first = substr($stringPrice, 0, 1);
        $other = substr($stringPrice, 1, strlen($stringPrice));
        if (strlen($other) >= 3) {
            $other = " {$other}";
        }

        $currencySign = str_replace('р', 'руб', $this->lowerFirstLetter(Yii::$app->params['currencies'][Yii::$app->params['app_currency_code']]['sign']));

        $result = Html::tag('div', $first, ['class' => 'number-up']) . $other . Html::tag('div', $currencySign, ['class' => 'currency']);

        return $result;
    }

    /**
     * @param $currency_code
     * @param $min
     * @param $max
     * @return string
     */
    public function formatRangeWithThousands($currency_code, $min, $max)
    {
        $formatMin = $min < 1000 && $max >= 1000;
        $suffixMin = '';
        $suffixMax = '';
        if ($min >= 1000) {
            $min = $min / 1000;
            if (!in_array($currency_code, ['RUB', 'UAH'])) {
                $suffixMin = 'k';
            }
        }
        if ($max >= 1000) {
            $max = $max / 1000;
            switch ($currency_code) {
                case 'RUB':
                    $suffixMax = ' тыс.';
                    break;
                case 'UAH':
                    $suffixMax = ' тис.';
                    break;
                default:
                    $suffixMax = 'k';
                    break;
            }
        }
        /* @var $currency Currency */
        $currency = Currency::getDb()->cache(function ($db) use ($currency_code) {
            return Currency::find()->where(['code' => $currency_code])->one();
        });
        $min = $currency->symbol_left . Yii::$app->formatter->asDecimal($min, 0) . $suffixMin . ($formatMin ? $currency->symbol_right : '');
        $max = $currency->symbol_left . Yii::$app->formatter->asDecimal($max, 0) . $suffixMax . $currency->symbol_right;

        return $min . ' - ' . $max;
    }

    /**
     * @param $input
     * @return bool|mixed|string
     */
    public function stringToLink($input)
    {
        $ret = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $input);
//        $ret = ' ' . $text;

        $ret = preg_replace("/(http(s)?:\/\/[\w#%?&\/.\-_=\[\]+]*\.(?:jpg|jpeg|png|gif))(?!(?:(?:[^'<>]*')|(?:[^\"<>]*\"))[^<>]*>)/is", "<a href='$1' target='_blank' rel='nofollow'><img src='$1'></a>", $ret);
        $ret = preg_replace("/(http(s)?:\/\/[\w#%?&\/.\-_=\[\]+]*)(?!(?:(?:[^'<>]*')|(?:[^\"<>]*\"))[^<>]*>)/is", "<a href='$1' target='_blank' rel='nofollow'>$1</a>", $ret);

//        $ret = substr($ret, 1);

        return $ret;
    }

    /**
     * Generate slug of specified length
     *
     * @param $text
     * @param int $wordsLimit
     * @param int $charactersLimit
     * @return string
     */
    public function generateSlug($text, $wordsLimit = 5, $charactersLimit = 55)
    {
        $transliterated = Yii::$app->utility->transliterate(preg_replace('/\s+/', ' ', StringHelper::truncateWords($text, $wordsLimit, '')));
        $slug = preg_replace("/\-{2,}/", "-", StringHelper::truncate($transliterated, ($charactersLimit - 14), '') . '-' . uniqid());
        return $slug;
    }

    /**
     * @param $assetClass
     * @return string
     */
    public function getPublishedAssetPath($assetClass)
    {
        /** @var AssetBundle $assetClass */
        $assetClass = new $assetClass;

        return Yii::$app->assetManager->getPublishedUrl($assetClass->sourcePath);
    }

    /**
     * @param $array
     * @return mixed
     */
    public function hasUpperCase($array)
    {
        return array_reduce($array, function ($carry, $value) {
            return $carry || (is_array($value) ? $this->hasUpperCase($value) : preg_match('/[A-Z]/u', $value));
        }, false);
    }

    /**
     * @param $array
     * @return mixed
     */
    public function arrayToLower($array)
    {
        return array_map(
            function ($value) {
                return is_array($value) ? $this->arrayToLower($value) : mb_strtolower($value, 'UTF-8');
            },
            $array
        );
    }

    /**
     * @param $translations
     * @return mixed
     */
    public function extractTranslation($translations)
    {
        if (empty($translations)) {
            return null;
        }

        return isset($translations[Yii::$app->language]) ?
            $translations[Yii::$app->language]['title'] :
            array_values($translations)[0]['title'];
    }

    public static $russianTransliterations = [
        "а" => "a",
        "б" => "b",
        "в" => "v",
        "г" => "g",
        "д" => "d",
        "е" => "e",
        "ё" => "e",
        "ж" => "zh",
        "з" => "z",
        "и" => "i",
        "й" => "y",
        "к" => "k",
        "л" => "l",
        "м" => "m",
        "н" => "n",
        "о" => "o",
        "п" => "p",
        "р" => "r",
        "с" => "s",
        "т" => "t",
        "у" => "u",
        "ф" => "f",
        "х" => "kh",
        "ц" => "ts",
        "ч" => "ch",
        "ш" => "sh",
        "щ" => "shch",
        "ы" => "y",
        "э" => "e",
        "ю" => "yu",
        "я" => "ya",
        "ь" => "",
        "ъ" => "",
    ];

    public static $georgianTransliterations = [
        "ა" => "a",
        "ბ" => "b",
        "გ" => "g",
        "დ" => "d",
        "ე" => "e",
        "ვ" => "v",
        "ზ" => "z",
        "ჱ" => "",
        "თ" => "t",
        "ი" => "i",
        "კ" => "k",
        "ლ" => "l",
        "მ" => "m",
        "ნ" => "n",
        "ჲ" => "",
        "ო" => "o",
        "პ" => "p",
        "ჟ" => "zh",
        "რ" => "r",
        "ს" => "s",
        "ტ" => "t",
        "ჳ" => "",
        "უ" => "u",
        "ჷ" => "",
        "ფ" => "p",
        "ქ" => "k",
        "ღ" => "gh",
        "ყ" => "q",
        "ჸ" => "",
        "შ" => "sh",
        "ჩ" => "ch",
        "ც" => "ts",
        "ძ" => "dz",
        "წ" => "ts",
        "ჭ" => "ch",
        "ხ" => "kh",
        "ჴ" => "",
        "ჯ" => "j",
        "ჰ" => "h",
        "ჵ" => "",
        "ჶ" => "",
    ];

    public static $ukrainianTransliterations = [
        "є" => "e",
        "і" => "i",
        "ї" => "yi",
    ];

    public static $spanishTransliterations = [
        "á" => "a",
        "é" => "e",
        "í" => "i",
        "ó" => "o",
        "ú" => "u",
        "ü" => "u",
        "ñ" => "n",
        "¿" => " ",
        "¡" => " ",
    ];
}