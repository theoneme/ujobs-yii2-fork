<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 11:35
 */

namespace common\components\s3;

use frostealth\yii2\aws\s3\commands\UploadCommand;
use frostealth\yii2\aws\s3\base\handlers\Handler;
use GuzzleHttp\Psr7;
use Psr\Http\Message\StreamInterface;


/**
 * Class UploadCommandHandler
 *
 * @package frostealth\yii2\aws\s3\handlers
 */
final class UploadCommandHandler extends Handler
{
    /**
     * @param \frostealth\yii2\aws\s3\commands\UploadCommand $command
     *
     * @return \Aws\ResultInterface|\GuzzleHttp\Promise\PromiseInterface
     */
    public function handle(UploadCommand $command)
    {
        $source = $this->sourceToStream($command->getSource());
        $options = array_filter($command->getOptions());
        $options['params'] = [
            'CacheControl' => 'max-age=172800',
        ];

        $promise = $this->s3Client->uploadAsync(
            $command->getBucket(),
            $command->getFilename(),
            $source,
            $command->getAcl(),
            $options
        );
        return $command->isAsync() ? $promise : $promise->wait();
    }

    /**
     * Create a new stream based on the input type.
     *
     * @param resource|string|StreamInterface $source path to a local file, resource or stream
     *
     * @return StreamInterface
     */
    protected function sourceToStream($source): StreamInterface
    {
        if (is_string($source)) {
            $source = Psr7\try_fopen($source, 'r+');
        }

        return Psr7\stream_for($source);
    }
}