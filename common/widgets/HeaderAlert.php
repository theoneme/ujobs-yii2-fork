<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 02.08.2017
 * Time: 13:23
 */

namespace common\widgets;

use Yii;
use yii\base\Widget;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 */
class HeaderAlert extends Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'success' => 'success',
        'info'    => 'information',
        'warning' => 'warning',
        'error'   => 'error',
        'danger' => 'error'
    ];

    public $icons = [
        'success' => 'check',
        'info' => 'info',
        'warning' => 'info',
        'error' => 'times',
        'danger' => 'times'
    ];

    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];

    /**
     * @return string
     */
    public function run()
    {
        parent::init();

        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes(false);
        $html = '';

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array) $data;
                foreach ($data as $i => $message) {
                    $id = uniqid();
                    $html .= $this->render('alert', [
                        'message' => $message,
                        'type' => $this->alertTypes[$type],
                        'icon' => $this->icons[$type],
                        'id' => $id
                    ]);
                }
                $session->removeFlash($type);
                break;
            }
        }

        return $html;
    }
}
