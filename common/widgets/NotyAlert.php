<?php

namespace common\widgets;

use Yii;
use yii\base\Widget;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 */
class NotyAlert extends Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'success' => 'success',
        'info'    => 'information',
        'warning' => 'warning',
        'error'   => 'error',
        'danger' => 'error'
    ];
    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];

    /**
     *
     */
    public function init()
    {
        parent::init();

        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array) $data;
                foreach ($data as $i => $message) {
                    $this->view->registerJs('
                        $(function(){
                            noty({
                                layout: "top",
                                type: "' . $this->alertTypes[$type] . '",
                                text: "' . $message . '",
                                textAlign: "center",
                                easing: "swing",
                                animation: {
                                    open: {height: "toggle"},
                                    close: {height: "toggle"},
                                    easing: "swing",
                                    speed: 500
                                },
                                speed: 1500,
                                timeout: 2000,
                                closable: true,
                                closeOnSelfClick: true
                            })
                        })
                    ');
                }
                $session->removeFlash($type);
            }
        }
    }
}
