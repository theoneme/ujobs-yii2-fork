<?php
/**
 * @var $type string
 * @var $icon string
 * @var $message string
 */

?>

<div class="head-banner <?= $type ?>" id="notification-<?= $id?>">
    <div class="container-fluid">
        <div class="hb-body text-center">
            <div class="hb-img text-center">
                <i class="fa fa-<?= $icon ?>"></i>
            </div>
            <div class="hb-info">
                <p>
                    <?= $message ?>
                </p>
            </div>
        </div>
        <a class="close-banner" href="#"></a>
    </div>
</div>

<?php $script = <<<JS
    let id = '#notification-{$id}';

    setTimeout(function() {
        $(id).find('.close-banner').click();
    }, 20000);
JS;

$this->registerJs($script);
