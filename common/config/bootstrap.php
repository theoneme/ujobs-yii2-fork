<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@livechat', dirname(dirname(__DIR__)) . '/livechat');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');

use frontend\controllers\user\RegistrationController;
use frontend\controllers\user\SecurityController;
use yii\base\Event;

Event::on(SecurityController::class, SecurityController::EVENT_AFTER_AUTHENTICATE, function (\dektrium\user\events\AuthEvent $e) {
    if ($e->account->user === null) {
        return;
    }
    switch ($e->client->getName()) {
        case 'facebook':
            $e->account->user->profile->updateAttributes([
                'name' => $e->client->getUserAttributes()['name'],
                'public_email' => $e->client->getUserAttributes()['email'],
            ]);
    }
});

Event::on(RegistrationController::class, RegistrationController::EVENT_AFTER_CONNECT, function (\dektrium\user\events\ConnectEvent $e) {
    if ($e->account->user === null) {
        return;
    }
    switch ($e->account->provider) {
        case 'facebook':
            $e->account->user->profile->updateAttributes([
                'name' => $e->account->decodedData['name'],
                'public_email' => $e->account->decodedData['email'],
            ]);
    }
});