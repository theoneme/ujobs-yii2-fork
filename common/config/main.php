<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-GB',
    'components' => [
        'elasticsearch' => [
            'class' => 'common\components\elasticsearch\Connection',
            'connectionTimeout' => 200,
            'nodes' => [
                ['http_address' => '127.0.0.1:9200'],
                // configure more hosts if you have a cluster
            ],
        ],
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'db' => 'db',
            'tableName' => '{{%queue}}',
            'channel' => 'default',
            'mutex' => \yii\mutex\MysqlMutex::class,
        ],
        /*'cache' => [
            'class' => 'yii\redis\Cache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 5,
        ],*/
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        's3' => [
            'class' => 'frostealth\yii2\aws\s3\Service',
            'credentials' => [
                'key' => 'AKIAJKAMNOAZSPXQ3LMA',
                'secret' => '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',
            ],
            'region' => 'eu-central-1',
            'defaultBucket' => 'ujobs-media-s',
            'defaultAcl' => 'public-read',
        ],
        's3Temp' => [
            'class' => 'frostealth\yii2\aws\s3\Service',
            'credentials' => [
                'key' => 'AKIAJKAMNOAZSPXQ3LMA',
                'secret' => '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',
            ],
            'region' => 'us-west-2',
            'defaultBucket' => 'eroom-property-images',
            'defaultAcl' => 'public-read',
        ],
        'mediaLayer' => [
            'class' => 'common\components\MediaLayer'
        ],
        'cacheLayer' => [
            'class' => 'common\components\CacheLayer'
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'common\models\user\User',
                'Profile' => 'common\models\user\Profile'
            ],
            'admins' => ['devouryo@gmail.com'],
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'debug' => [
            'class' => 'yii\debug\Module',
            'panels' => [
                'elasticsearch' => [
                    'class' => 'yii\elasticsearch\DebugPanel',
                ],
            ],
        ],
    ],
];
