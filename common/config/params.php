<?php
return [
    'adminEmail' => 'admin@example.com',

    'user.passwordResetTokenExpire' => 3600,
    'languages' => [
        'en-GB' => 'English',
        'es-ES' => 'Spanish',
        'zh-CN' => 'Chinese',
        'ru-RU' => 'Russian',
        'uk-UA' => 'Ukrainian',
        'pt-PT' => 'Portuguese',
        'fr-FR' => 'French',
        'de-DE' => 'German',
        'it-IT' => 'Italian',
        'fi-FI' => 'Finnish',
        'sv-SV' => 'Swedish',
        'pl-PL' => 'Polish',
        'el-EL' => 'Greek',
        'be-BE' => 'Belarusian',
        'tr-TR' => 'Turkish',
        'ka-GE' => 'Georgian',
        'ja-JA' => 'Japanese',
        'ko-KO' => 'Korean',
        'vi-VI' => 'Vietnamese',
        'th-TH' => 'Thai',
        'ar-AR' => 'Arabic',
        'hi-HI' => 'Hindi'
    ],
    'supportedLocales' => [
        'en-GB' => 'en',
        'es-ES' => 'es',
        'zh-CN' => 'zh',
        'ru-RU' => 'ru',
        'uk-UA' => 'ua',
        'pt-PT' => 'pt',
        'fr-FR' => 'fr',
        'de-DE' => 'de',
        'it-IT' => 'it',
        'fi-FI' => 'fi',
        'sv-SV' => 'sv',
        'pl-PL' => 'pl',
        'el-EL' => 'el',
        'be-BE' => 'be',
        'tr-TR' => 'tr',
        'ka-GE' => 'ka',
        'ja-JA' => 'ja',
        'ko-KO' => 'ko',
        'vi-VI' => 'vi',
        'th-TH' => 'th',
        'ar-AR' => 'ar',
        'hi-HI' => 'hi'
    ],
    'imperaviLocales' => [
        'en-GB' => 'en',
        'es-ES' => 'es',
        'zh-CN' => 'zh_cn',
        'ru-RU' => 'ru',
        'uk-UA' => 'ua',
        'ka-GE' => 'ge',
    ],
    'bigLocales' => [
        'en-GB' => 'en',
        'zh-CN' => 'zh',
        'es-ES' => 'es',
        'ru-RU' => 'ru',
    ],
    'currencies' => [
        'USD' => ['title' => 'Dollar', 'sign' => '$'],
        'EUR' => ['title' => 'Euro', 'sign' => '€'],
        'RUB' => ['title' => 'Ruble', 'sign' => 'Р'],
        'GEL' => ['title' => 'Lari', 'sign' => '₾'],
        'UAH' => ['title' => 'Hryvnia', 'sign' => '₴'],
        'CNY' => ['title' => 'Yuan', 'sign' => '¥'],
    ],

    'notificationQueue' => true,
    'amazonUploadQueue' => false,

    'awsBucketName' => 'ujobs-media-s',
    'awsRegion' => 'eu-central-1',
    'awsCloudfrontId' => 'dwmh9kmdoyiua',
    'awsSource' => 'cloudfront', // cloudfront or s3

    'replicaLogin' => 'ujobs',
    'replicaPassword' => 'lrXBWL5MlmqAEFIeiVoa<>@#8econ3KrgraBmJTVCRjM',

    'sberbankLogin' => 'ujobs-api',
    'sberbankPass' => '1234Da4321aD',
    'sberbankUrl' => Voronkovich\SberbankAcquiring\Client::API_URI,

    'ujobsTeamMail' => ['message@ujobs.me' => 'Команда uJobs'],
    'ujobsNoty' => ['message@ujobs.me' => 'Команда uJobs'],
    'supportEmail' => 'support@ujobs.me',

	'telegramKey' => '331455808:AAHHRO1zo72tPe38dIA88KM4SuI-skM4Pms',
	'telegramBotName' => 'uJobs_bot',

	'skypeId' => 'e44c207a-078b-4659-91e5-0756bd7a20dd',
	'skypeSecret' => 'iLggspMAWYUawefJ4pEbvfC',

    'images' => [
        'catalog' => [
            'width' => 206,
            'height' => 206
        ],
//        'product' => [
//            'width' => 1000,
//            'height' => 1000
//        ],
//        'news' => [
//            'width' => 420,
//            'height' => 150
//        ]
    ],
    'runtime' => [
        'currency' => [
            'ratios' => [],
            'currencies' => []
        ]
    ],
    'supportId' => 301,
    'aes_pass' => '87^dbef?8-51-kyX',
    'aes_iv' => 'c300b3.0b,8893az',
];
