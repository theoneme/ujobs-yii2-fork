<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.11.2017
 * Time: 15:29
 */

namespace common\services;

use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class UserAttributeUpdateService
 * @package common\services
 */
class AttributeToFormService
{
    /**
     * @param $source
     * @param $targetClassName
     * @return array
     */
    public static function process($source, $targetClassName)
    {
        $output = [];

        if(!empty($source)) {
            foreach($source as $item) {
                $descriptions = $item->attrValueDescriptions;
                $title = array_key_exists(Yii::$app->language,  $descriptions) ? $descriptions[Yii::$app->language]->title : array_pop($descriptions)->title;
                $object = new $targetClassName([
                    'title' => $title,
                    'id' => $item->id
                ]);

                $customData = json_decode($item->custom_data, true);
                if(!empty($customData)) {
                    $object->attributes = $customData;
                }

                $output[] = $object;
            }
        }

        return $output;
    }
}