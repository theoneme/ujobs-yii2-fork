<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.10.2017
 * Time: 13:59
 */

namespace common\services;

use common\models\user\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;
use yii\web\Session;

class CounterService
{
    /**
     * @var ActiveRecord
     */
    private $_object;
    /**
     * @var Session
     */
    private $_session;
    /**
     * @var string
     */
    private $sessionKey;
    /**
     * @var string
     */
    private $tableKey;
    /**
     * @var int
     */
    private $step;
    /**
     * @var int
     */
    private $limit = 20;
    /**
     * @var bool
     */
    private $onlyForUsers = true;

    /**
     * CounterService constructor.
     * @param ActiveRecord $object
     * @param $sessionKey
     * @param $tableKey
     * @param int $step
     * @param Session $session
     */
    public function __construct(ActiveRecord $object, $tableKey, $step = 1, $sessionKey = null, Session $session = null)
    {
        $this->_session = $session;
        $this->_object = $object;
        $this->sessionKey = $sessionKey;
        $this->tableKey = $tableKey;
        $this->step = $step;
    }

    /**
     * @return bool
     */
    public function process()
    {
        if(($this->onlyForUsers === true && !Yii::$app->user->isGuest) || $this->onlyForUsers === false) {
            return $this->incrementCounter();
        }

        return false;
    }

    /**
     * @return bool
     */
    private function incrementCounter()
    {
        if ($this->_session instanceof Session && $this->sessionKey != null) {
            if (!$this->saveInSession()) {
               return false;
            }
        }

        return $this->_object->updateCounters([$this->tableKey => $this->step]);
    }


    /**
     * @return bool
     */
    private function saveInSession()
    {
        $viewed = $this->_session->get($this->sessionKey, []);
        if (!in_array($this->_object->getPrimaryKey(), $viewed)) {
            $viewed[] = $this->_object->getPrimaryKey();

            if (count($viewed) > $this->limit) {
                $viewed = array_slice($viewed, count($viewed) - $this->limit, $this->limit);
            }

            $this->_session->set($this->sessionKey, $viewed);

            return true;
        }

        return false;
    }
}