<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.10.2017
 * Time: 14:22
 */

namespace common\services;

use common\models\user\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;
use yii\web\Session;

class RememberViewService
{
    /**
     * @var ActiveRecord
     */
    private $_object;
    /**
     * @var Session
     */
    private $_session;
    /**
     * @var string
     */
    private $sessionKey;
    /**
     * @var int
     */
    private $limit = 20;
    /**
     * @var bool
     */
    private $onlyForUsers = true;

    /**
     * RememberViewService constructor.
     * @param ActiveRecord $object
     * @param null $sessionKey
     * @param Session|null $session
     */
    public function __construct(ActiveRecord $object, $sessionKey, Session $session = null)
    {
        $this->_session = $session;
        $this->_object = $object;
        $this->sessionKey = $sessionKey;
    }

    /**
     * @return bool
     */
    public function saveInSession()
    {
        if (($this->onlyForUsers === true && !Yii::$app->user->isGuest) || $this->onlyForUsers === false) {
            $viewed = $this->_session->get($this->sessionKey, []);
            if (!in_array($this->_object->getPrimaryKey(), $viewed)) {
                $viewed[] = $this->_object->getPrimaryKey();

                if (count($viewed) > $this->limit) {
                    $viewed = array_slice($viewed, count($viewed) - $this->limit, $this->limit);
                }

                $this->_session->set($this->sessionKey, $viewed);

                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function removeFromSession()
    {
        if (($this->onlyForUsers === true && !Yii::$app->user->isGuest) || $this->onlyForUsers === false) {
            $viewed = $this->_session->get($this->sessionKey);
            if ($viewed !== null) {
                if (($key = array_search($this->_object->getPrimaryKey(), $viewed)) !== false) {
                    unset($viewed[$key]);
                }

                $this->_session->set($this->sessionKey, $viewed);

                return true;
            }
        }

        return false;
    }
}