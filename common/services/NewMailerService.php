<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.11.2017
 * Time: 15:02
 */

namespace common\services;

use common\components\UrlAdvanced;
use common\models\user\User;
use console\jobs\EmailNotificationJob;
use Yii;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;
use yii\validators\EmailValidator;
use yii\web\Application;
use yii\web\UrlManager;

/**
 * Class NewMailerService
 * @package common\services
 */
class NewMailerService
{
    /**
     * @var User
     */
    private $receiver;

    /**
     * @var string
     */
    private $mailerName;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $viewPath = '@frontend/views/email';

    /**
     * NewMailerService constructor.
     * @param string $mailer
     * @param User $receiver
     */
    public function __construct($mailer, User $receiver)
    {
        $this->receiver = $receiver;
        $this->mailerName = $mailer;
        $this->mailer = Yii::$app->{$mailer};
    }

    /**
     * @param $params
     * @return bool
     */
    public function sendMail($params)
    {
        $receiverEmail = $this->receiver->email ?? $this->receiver->profile->public_email;
        $emailValidator = new EmailValidator();
        if($emailValidator->validate($receiverEmail) !== true) {
            return false;
        }

        $unSubUrl = UrlAdvanced::to(['/site/unsubscribe', 'id' => $this->receiver->id, 'created_at' => $this->receiver->created_at, 'updated_at' => $this->receiver->updated_at], true);
        $view = $params['view'] ?? 'custom-message';
        $subject = $params['subject'] ?? Yii::t('notifications', 'Notification from {site}', ['site' => Yii::$app->name], $this->receiver->site_language);

        if(Yii::$app->params['notificationQueue'] === true) {
            return Yii::$app->queue->push(new EmailNotificationJob([
                'unSubUrl' => $unSubUrl,
                'mailer' => $this->mailerName,
                'viewPath' => $this->viewPath,
                'params' => json_encode($params),
                'view' => $view,
                'subject' => $subject,
                'receiverEmail' => $receiverEmail,
                'user' => array_key_exists('user', $params) ? $params['user']->id : null,
                'user2' => array_key_exists('user2', $params) ? $params['user2']->id : null
            ]));
        }

        $mailer = $this->mailer;
        $mailer->viewPath = $this->viewPath;

        return $mailer->compose([
            'html' => $view
        ], $params)
            ->setTo($receiverEmail)
            ->setFrom($params['from'])
            ->setSubject($subject)
            ->addHeader('List-Unsubscribe', "<{$unSubUrl}>")
            ->send();
    }
}