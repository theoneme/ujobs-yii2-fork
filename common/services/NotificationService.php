<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.11.2017
 * Time: 14:05
 */

namespace common\services;

use common\components\UrlAdvanced;
use common\models\Notification;
use common\models\user\User;
use frontend\modules\push\services\PushService;
use frontend\modules\skype\services\SkypeFactory;
use frontend\modules\skype\services\SkypeService;
use frontend\modules\telegram\services\TelegramService;
use SkypeBot\Command\SendMessage;
use Yii;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;

/**
 * Class NotificationService
 * @package common\services
 */
class NotificationService
{
    /**
     * @var Notification
     */
    private $notification;

    /**
     * NotificationService constructor.
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @param $provider
     * @return bool
     */
    public function sendNotification($provider)
    {
        switch($provider) {
            case 'email':
                $mailerService = new NewMailerService('mailer', $this->notification->to);
                $params = $this->notification->params;
                $params['link'] = UrlAdvanced::to(array_merge($this->notification->linkRoute, [
                    'app_language' => 'ru',
                    'email-action' => 1
                ]), true, 'urlManagerFrontEnd');
                return $mailerService->sendMail($params);
            case 'telegram':
                $telegramService = new TelegramService($this->notification->to);
                return $telegramService->sendMessage($this->notification->params);
            case 'skype':
                $skypeService = new SkypeService($this->notification->to);
                return $skypeService->sendMessage($this->notification->params);
            case 'push':
                $pushService = new PushService($this->notification->to);
                return $pushService->sendNotification($this->notification->params);
        }

        return false;
    }
}