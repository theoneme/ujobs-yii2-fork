<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.11.2017
 * Time: 15:50
 */

namespace common\services;

use common\models\Company;
use common\models\Conversation;
use common\models\Event;
use common\models\Job;
use common\models\TariffRule;
use common\models\user\Profile;
use common\models\user\User;
use common\models\UserTariff;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use Yii;

/**
 * Class NotificationService
 * @package common\services
 */
class UserAccessService
{
    const REASON_OWN_ENTITY = 1;
    const REASON_PROFILE_NOT_MODERATED = 2;
    const REASON_TARIFF_RESPONSES_EXPIRED = 3;
    const REASON_TARIFF_CONVERSATIONS_EXPIRED = 4;
    const REASON_NOT_ALLOWED_BY_COMPANY = 5;

    /**
     * @var User
     */
    private $user;

    /**
     * UserAccessService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Job|Product $tender
     * @return array
     */
    public function isAllowedToRespondOnTender($tender)
    {
        $user = $this->user;
        if (hasAccess($tender->user_id)) {
            return [
                'allowed' => false,
                'reason' => self::REASON_OWN_ENTITY
            ];
        }
        if ($user->company_user_id) {
            /* @var $company Company */
            $company = Company::find()->joinWith(['companyMembers', 'user'])->where(['company.user_id' => $user->company_user_id])->one();
            if ($company !== null && isset($company->companyMembers[$user->id]) && $company->companyMembers[$user->id]->isAllowedToManage()) {
                $user = $company->user;
            } else {
                return [
                    'allowed' => false,
                    'reason' => self::REASON_NOT_ALLOWED_BY_COMPANY
                ];
            }
        } else {
            if ($user->profile->status !== Profile::STATUS_ACTIVE) {
                $result = [
                    'allowed' => false,
                    'reason' => self::REASON_PROFILE_NOT_MODERATED,
                ];

                return $result;
            }
        }

        $activeTariff = $user->activeTariff ? $user->activeTariff : new UserTariff();
        $tariffRule = TariffRule::find()->where(['tariff_id' => $activeTariff->tariff_id, 'code' => 'allowed_tender_responses'])->one();
        if ($tariffRule === null) {
            $tariffRule = TariffRule::find()->where(['tariff_id' => 1, 'code' => 'allowed_tender_responses'])->one();
        }

        $responsesToday = Event::find()->where([
            'code' => EVENT::RESPOND_ON_TENDER,
            'user_id' => $user->id
        ])->andWhere(['>', 'created_at', time() - 60 * 60 * 24])->count('id');
        $quantityRestriction = $tariffRule->value;
        if ($responsesToday < $quantityRestriction || $quantityRestriction == -1) {
            $result = [
                'allowed' => true,
            ];
        } else {
            $result = [
                'allowed' => false,
                'reason' => self::REASON_TARIFF_RESPONSES_EXPIRED
            ];
        }

        return $result;
    }

    /**
     * @param User $target
     * @return array
     */
    public function isAllowedToCreateConversation(User $target)
    {
        $user = $this->user;
        if (isGuest()) {
            return ['allowed' => false];
        } else {
            if (hasAccess($target->id)) {
                return [
                    'allowed' => false,
                    'reason' => self::REASON_OWN_ENTITY
                ];
            }
            if ($target->id === Yii::$app->params['supportId']) {
                return ['allowed' => true];
            }
            if ($user->company_user_id) {
                /* @var $company Company */
                $company = Company::find()->joinWith(['companyMembers', 'user'])->where(['company.user_id' => $user->company_user_id])->one();
                if ($company !== null && isset($company->companyMembers[$user->id]) && $company->companyMembers[$user->id]->isAllowedToCorrespond()) {
                    $user = $company->user;
                } else {
                    return [
                        'allowed' => false,
                        'reason' => self::REASON_NOT_ALLOWED_BY_COMPANY
                    ];
                }
            }
            $activeTariff = $user->activeTariff ? $user->activeTariff : new UserTariff();
            $tariffRule = TariffRule::find()->where(['tariff_id' => $activeTariff->tariff_id, 'code' => 'new_conversations_per_day'])->one();
            if ($tariffRule === null) {
                $tariffRule = TariffRule::find()->where(['tariff_id' => 1, 'code' => 'new_conversations_per_day'])->one();
            }
            $conversationRestriction = $tariffRule->value;

            $conversationExists = Conversation::findByMembers([$target->id, $user->id])->andWhere(['is_blank' => false])->exists();
            $orderExists = Order::find()->where(['or',
                ['seller_id' => $user->id, 'customer_id' => $target->id],
                ['customer_id' => $user->id, 'seller_id' => $target->id]
            ])->exists();
            if ($conversationExists || $orderExists) {
                return ['allowed' => true];
            } else {
                $conversationsToday = Event::find()->where(['code' => 'CREATE_CONVERSATION', 'user_id' => $user->id])->andWhere(['>', 'created_at', time() - 60 * 60 * 24])->count('id');

                if ($conversationsToday < $conversationRestriction || $conversationRestriction == -1) {
                    $result = ['allowed' => true];
                } else {
                    $result = [
                        'allowed' => false,
                        'reason' => self::REASON_TARIFF_CONVERSATIONS_EXPIRED
                    ];
                }
            }

            return $result;
        }
    }

    /**
     * @return bool
     */
    public function isAllowedToPost()
    {
        $user = $this->user;
        if ($user->company_user_id) {
            /* @var $company Company */
            $company = Company::find()->joinWith(['companyMembers'])->where(['company.user_id' => $user->company_user_id])->one();
            return $company !== null && isset($company->companyMembers[$user->id]) && $company->companyMembers[$user->id]->isAllowedToPost();
        } else {
            return true;
        }
    }

    /**
     * @return bool
     */
    public function isAllowedToOrder()
    {
        $user = $this->user;
        if ($user->company_user_id) {
            /* @var $company Company */
            $company = Company::find()->joinWith(['companyMembers'])->where(['company.user_id' => $user->company_user_id])->one();
            return $company !== null && isset($company->companyMembers[$user->id]) && $company->companyMembers[$user->id]->isAllowedToOrder();
        } else {
            return true;
        }
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isAllowedToUpdateOrder($userId)
    {
        $user = $this->user;
        if ($user->company_user_id) {
            /* @var $company Company */
            $company = Company::find()->joinWith(['companyMembers'])->where(['company.user_id' => $user->company_user_id])->one();
            return $company !== null && isset($company->companyMembers[$user->id]) && $company->companyMembers[$user->id]->isAllowedToOrder() && $user->company_user_id === $userId;
        } else {
            return $user->id === $userId;
        }
    }

    /**
     * @return bool
     */
    public function isAllowedToUseWallets()
    {
        $user = $this->user;
        if ($user->company_user_id) {
            /* @var $company Company */
            $company = Company::find()->joinWith(['companyMembers'])->where(['company.user_id' => $user->company_user_id])->one();
            return $company !== null && isset($company->companyMembers[$user->id]) && $company->companyMembers[$user->id]->isAllowedToUseWallets();
        } else {
            return true;
        }
    }
}