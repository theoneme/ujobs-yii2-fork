<?php

namespace common\services;

use common\components\CurrencyHelper;
use common\helpers\CauriHelper;
use common\models\user\User;
use frontend\models\CauriForm;
use Yii;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\httpclient\Response;

class CauriPaymentService
{
    /**
     * @var string
     */
    private $client;
    /**
     * @var string
     */
    private $url;

    /**
     * CauriPaymentService constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
    }

    /**
     * @param User $user
     * @return array|bool
     */
    public function resolve($user)
    {
        try {
            $this->url = 'https://api.cauri.uk/rest-v1/user/resolve';
//            $email = !empty($user->profile->public_email) ? $user->profile->public_email : $user->email;
//            if (empty($email) || !$user->confirmed_at) {
                $email = 'user' . $user->id . '@ujobs.me'; //
//            }
            $locales = Yii::$app->params['supportedLocales'];
            $locales['uk-UA'] = 'uk';
            /* @var Response $response */
            $params = [
                'identifier' => $user->id,
                'display_name' => $user->getSellerName(),
                'email' => $email,
                'phone' => preg_replace("/[^0-9]/", "", $user->phone),
                'locale' => $locales[Yii::$app->language],
                'ip' => Yii::$app->request->getUserIP(),
                'project' => CauriHelper::$publicKey,
            ];
            $params['signature'] = $this->generateSignature($params);
            $response = $this->client->post($this->url, $params)->send();
            $responseData = $response->getData();
        } catch (Exception $e) {
            return false;
        }
        if (!empty($responseData['id'])) {
            if (isset($responseData['error'])) {
                Yii::error($responseData);
            }
            return $responseData['id'];
        } else {
            return false;
        }
    }

    /**
     * @param CauriForm $form
     * @return array|bool
     */
    public function process($form)
    {
        try {
            $this->url = 'https://api.cauri.uk/rest-v1/card/process';
            /* @var Response $response */
            $currencyCode = $form->payment->currency_code;
            $price = $form->payment->total;
            if (!in_array($currencyCode, ['RUB', 'USD', 'USD'])) {
                $price = CurrencyHelper::convert($currencyCode, 'USD', $price);
                $currencyCode = 'USD';
            }
            $params = [
                'order_id' => $form->payment->id,
                'description' => $form->payment->description,
                'user' => $form->userId,
                'card_token' => $form->token,
                'price' => Yii::$app->formatter->asDecimal($price, 2),
                'currency' => $currencyCode,
                'recurring' => 0,
                'acs_return_url' => Url::to(['/payment/cauri-authenticate'], true),
                'project' => CauriHelper::$publicKey,
            ];
            $params['signature'] = $this->generateSignature($params);
            $response = $this->client->post($this->url, $params)->send();
            $responseData = $response->getData();
        } catch (Exception $e) {
            return false;
        }
        if (!empty($responseData)) {
            if (isset($responseData['error'])) {
                Yii::error($responseData);
            }
            return $responseData;
        } else {
            return false;
        }
    }

    /**
     * @param string $rares
     * @param string $md
     * @return array|bool
     */
    public function authenticate($rares, $md)
    {
        try {
            $this->url = 'https://api.cauri.uk/rest-v1/card/authenticate';
            /* @var Response $response */
            $params = [
                'PaRes' => $rares,
                'MD' => $md,
                'project' => CauriHelper::$publicKey,
            ];
            $params['signature'] = $this->generateSignature($params);
            $response = $this->client->post($this->url, $params)->send();
            $responseData = $response->getData();
        } catch (Exception $e) {
            return false;
        }
        if (!empty($responseData)) {
            if (isset($responseData['error'])) {
                Yii::error($responseData);
            }
            return $responseData;
        } else {
            return false;
        }
    }

    /**
     * @param array $params
     * @return string
     */
    public function generateSignature($params)
    {
        sort($params, SORT_STRING);
        return hash_hmac('sha256', implode('|', $params), CauriHelper::$privateKey);
    }
}