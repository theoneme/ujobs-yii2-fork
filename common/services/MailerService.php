<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.05.2017
 * Time: 12:14
 */

namespace common\services;

use common\models\user\User;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;

class MailerService
{
	public static function sendMail(Mailer $mailer, $from, $subject, $view, array $params, User $to)
	{
		if ($mailer !== null) {
			$mailer->viewPath = '@frontend/views/email';
			return $mailer->compose([
				'html' => $view
			], $params)
				->setTo($to->email ?? $to->profile->public_email)
				->setFrom($from)
				->setSubject($subject)
				->addHeader('List-Unsubscribe', '<' . Url::to(['/site/unsubscribe', 'id' => $to->id, 'created_at' => $to->created_at, 'updated_at' => $to->updated_at], true) . '>')
				->send();
		}

		return false;
	}
}