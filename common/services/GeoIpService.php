<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.10.2017
 * Time: 18:27
 */

namespace common\services;

use common\models\user\User;
use Exception;
use MaxMind\Db\Reader;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\swiftmailer\Mailer;
use yii\web\Session;

/**
 * Class GeoIpService
 * @package common\services
 */
class GeoIpService
{
    /**
     * @var string
     */
    private $ip;

    /**
     * GeoIpService constructor.
     * @param $ip
     */
    public function __construct($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return null
     */
    public function getCity()
    {
        try {
            $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-City.mmdb');

            $data = $geoIpReader->get($this->ip);
            $city = $data['city']['names']['ru'] ?? $data['city']['names']['en'] ?? null;

            return $city;
        }
        catch (Exception $e){
            return null;
        }
    }
}