<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.07.2018
 * Time: 18:38
 */

namespace common\services;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class TestService
 * @package common\services
 */
class TestService
{
    /**
     * @var Client
     */
    private $_client;

    /**
     * @var string
     */
    private $_url;

    /**
     * @var string
     */
    private $_method = 'post';

    /**
     * @var integer
     */
    private $_group;

    /**
     * TestService constructor.
     * @param $group
     */
    public function __construct($group)
    {
        $this->_client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
        $this->_url = Yii::$app->params['syncTestUrl'];
        $this->_group = $group;
    }

    /**
     * @param $alias
     * @param $title
     * @param $result
     * @return bool
     */
    public function logTestResult($alias, $title, $result): bool
    {
        if (array_key_exists('syncTestResults', Yii::$app->params) && Yii::$app->params['syncTestResults'] === true) {
            try {
                /* @var Response $response */
                $response = $this->_client->{$this->_method}($this->_url, [
                    'alias' => $alias,
                    'status' => $result,
                    'title' => $title,
                    'group_id' => $this->_group
                ])->send();
                $responseData = $response->getData();
                $isOk = $response->isOk;
            } catch (\Exception $e) {
                return false;
            }

            if ($isOk && !empty($responseData['text'][0])) {
                return $responseData['text'][0];
            }
        }

        return false;
    }
}