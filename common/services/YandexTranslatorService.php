<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.10.2017
 * Time: 13:57
 */

namespace common\services;

use common\helpers\YandexHelper;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\httpclient\Response;

class YandexTranslatorService
{
    /**
     * @var string
     */
    private $client;
    /**
     * @var string
     */
    private $method = 'get';
    /**
     * @var string
     */
    private $url;

    /**
     * YandexTranslatorService constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
        $this->url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';
    }

    /**
     * @param $text
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function translate($text, $from = 'ru', $to = 'en')
    {
        $apiKey = YandexHelper::$apiKeys[array_rand(YandexHelper::$apiKeys)];
        try {
            /* @var Response $response */
            $response = $this->client->{$this->method}($this->url, [
                'key' => $apiKey,
                'text' => $text,
                'lang' => "{$from}-{$to}",
                'format' => 'html'
            ])->send();
            $responseData = $response->getData();
            $isOk = $response->isOk;
        } catch (Exception $e) {
            return false;
        }
        if ($isOk && !empty($responseData['text'][0])) {
            return $responseData['text'][0];
        } else {
            return false;
        }
    }

    /**
     * @param $text
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function translateWithVariables($text, $from = 'ru', $to = 'en')
    {
        $hasVariables = preg_match_all("/{.+?(?:{.*?})*}/u", $text, $matches);
        if ($hasVariables) {
            $variables = $matches[0];
            $replaces = array_map(
                function ($var) {
                    return "{" . $var . "}";
                },
                array_flip($variables)
            );
            $text = strtr($text, $replaces);

            foreach ($replaces as $key => $value) {
                if (strpos($key, 'plural') !== false) {
                    preg_match_all("/(?:one|few|many|other|=0|=1|=2|=3)({.*?})/u", $key, $plurals);
                    $newKey = strtr($key,
                        array_map(
                            function ($v) use ($from, $to) {
                                return $this->translate($v, $from, $to);
                            },
                            array_combine($plurals[1], $plurals[1])
                        )
                    );
                    $replaces[$newKey] = $value;
                    unset($replaces[$key]);
                }
            }
        }
        $translated = $this->translate($text, $from, $to);

        if ($hasVariables) {
            $translated = strtr($translated, array_flip($replaces));
        }
        return $translated;
    }
}