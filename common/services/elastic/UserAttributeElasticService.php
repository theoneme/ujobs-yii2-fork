<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:57
 */

namespace common\services\elastic;

use common\models\elastic\UserAttributeElastic;
use common\models\UserAttribute;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class JobAttributeElasticService
 * @package common\services
 */
class UserAttributeElasticService
{
    /**
     * @var UserAttribute
     */
    private $userAttribute;

    /**
     * UserAttributeElasticService constructor.
     * @param UserAttribute $userAttribute
     */
    public function __construct(UserAttribute $userAttribute)
    {
        $this->userAttribute = $userAttribute;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $userAttribute = $this->userAttribute;

        $userAttributeElastic = UserAttributeElastic::find()->where(['id' => $userAttribute->id])->one();

        /* @var UserAttributeElastic $userAttributeElastic */
        if ($userAttributeElastic === null) {
            $userAttributeElastic = new UserAttributeElastic();
            $userAttributeElastic->setPrimaryKey((int)$userAttribute->id);
        }
        $userAttributeElastic->attributes = array_intersect_key($userAttribute->attributes, $userAttributeElastic->attributes);

        $userAttributeElastic->user = [
            'id' => $userAttribute->user_id,
            'status' => $userAttribute->profile->status,
        ];

        $userAttributeElastic->attribute = [
            'translations' => ArrayHelper::map($userAttribute->attrDescriptions, 'locale', 'title')
        ];

        $userAttributeElastic->attributeValue = [
            'translations' => ArrayHelper::map($userAttribute->attrValueDescriptions, 'locale', 'title'),
            'status' => $userAttribute->attrValue->status
        ];

        return $userAttributeElastic;
    }
}