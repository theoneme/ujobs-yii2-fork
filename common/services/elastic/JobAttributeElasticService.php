<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:52
 */

namespace common\services\elastic;

use common\components\CurrencyHelper;
use common\models\elastic\JobAttributeElastic;
use common\models\JobAttribute;
use Yii;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class JobAttributeElasticService
 * @package common\services
 */
class JobAttributeElasticService
{
    /**
     * @var JobAttribute
     */
    private $jobAttribute;

    /**
     * JobAttributeElasticService constructor.
     * @param JobAttribute $jobAttribute
     */
    public function __construct(JobAttribute $jobAttribute)
    {
        $this->jobAttribute = $jobAttribute;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $jobAttribute = $this->jobAttribute;

        $jobAttributeElastic = JobAttributeElastic::find()->where(['id' => $jobAttribute->id])->one();
        /* @var JobAttributeElastic $jobAttributeElastic */
        if ($jobAttributeElastic === null) {
            $jobAttributeElastic = new JobAttributeElastic();
            $jobAttributeElastic->setPrimaryKey((int)$jobAttribute->id);
        }
        $jobAttributeElastic->attributes = array_intersect_key($jobAttribute->attributes, $jobAttributeElastic->attributes);

        $jobAttributeElastic->job = [
            'id' => $jobAttribute->job_id,
            'category_id' => $jobAttribute->job->category_id,
            'status' => $jobAttribute->job->status,
            'type' => $jobAttribute->job->type,
            'price' => $jobAttribute->job->price,
            'currency_code' => $jobAttribute->job->currency_code,
            'default_price' => CurrencyHelper::convert($jobAttribute->job->currency_code, 'RUB', $jobAttribute->job->price),
            'title' => isset($jobAttribute->job->translation->title) ? $jobAttribute->job->translation->title : '',
            'description' => isset($jobAttribute->job->translation->content) ? strip_tags($jobAttribute->job->translation->content) : ''
        ];

        $jobAttributeElastic->attribute = [
            'translations' => ArrayHelper::map($jobAttribute->attrDescriptions, 'locale', 'title')
        ];

        $jobAttributeElastic->attributeValue = [
            'translations' => ArrayHelper::map($jobAttribute->attrValueDescriptions, 'locale', 'title'),
            'status' => $jobAttribute->attrValue->status
        ];

        return $jobAttributeElastic;
    }

}