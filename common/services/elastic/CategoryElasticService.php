<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 13:55
 */

namespace common\services\elastic;

use common\models\Category;
use common\models\elastic\CategoryElastic;
use yii\elasticsearch\ActiveRecord;

/**
 * Class CategoryElasticService
 * @package common\services
 */
class CategoryElasticService
{
    /**
     * @var Category
     */
    private $category;

    /**
     * CategoryElasticService constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        }

        return $object;
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $category = $this->category;

        $categoryElastic = CategoryElastic::find()->where(['id' => $category->id])->one();
        if ($categoryElastic === null) {
            /* @var CategoryElastic $categoryElastic */
            $categoryElastic = new CategoryElastic();
            $categoryElastic->setPrimaryKey((int)$category->id);
        }
        $categoryElastic->attributes = array_intersect_key($category->attributes, $categoryElastic->attributes);
        $categoryElastic->category_type = $category->type === 'job_category' ? CategoryElastic::JOB_CATEGORY : CategoryElastic::PRODUCT_CATEGORY;

        $translations = [];
        foreach ($category->translations as $translation) {
            $translations[] = ['title' => $translation->title, 'description' => $translation->content, 'locale' => $translation->locale, 'slug' => $translation->slug];
        }
        $categoryElastic->translations = $translations;

        return $categoryElastic;
    }
}