<?php

namespace common\services\elastic;

use common\models\elastic\PageElastic;
use common\models\Like;
use common\models\Page;
use Yii;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class PageElasticService
 * @package common\services
 */
class PageElasticService
{
    /**
     * @var Page
     */
    private $page;

    /**
     * PageElasticService constructor.
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $page = $this->page;
        $attributes = [];
        $locales = [];

        /* @var PageElastic $pageElastic */
        $pageElastic = PageElastic::find()->where(['id' => $page->id])->one();
        if ($pageElastic === null) {
            $pageElastic = new PageElastic();
            $pageElastic->setPrimaryKey((int)$page->id);
            $pageElastic->user = [
                'id' => $page->user->id,
                'username' => $page->user->username,
                'is_company' => false,
                'company_id' => null
            ];
        }
        $pageElastic->attributes = array_intersect_key($page->attributes, $pageElastic->attributes);

        if ((boolean)$page->user->is_company === true) {
            $pageElastic->user = [
                'id' => $page->user->id,
                'username' => $page->user->username,
                'is_company' => true,
                'company_id' => $page->user->company->id
            ];

            $pageElastic->profile = [
                'user_id' => $page->user->id,
                'name' => $page->user->company->getSellerName(),
                'gravatar_email' => $page->user->company->logo,
                'status' => $page->user->company->status,
                'rating' => 0,
                'translations' => array_map(function ($var) {
                    return ['title' => $var->title, 'locale' => $var->locale];
                }, $page->user->company->translations ?? [])
            ];
        } else {
            $pageElastic->profile = [
                'user_id' => $page->user->id,
                'name' => $page->profile->getSellerName(),
                'gravatar_email' => $page->profile->gravatar_email,
                'status' => $page->profile->status,
                'rating' => $page->profile->rating,
                'translations' => array_map(function ($var) {
                    return ['title' => $var->title, 'locale' => $var->locale];
                }, $page->profile->translations ?? [])
            ];
        }

        $pageElastic->category = [
            'title' => $page->category->translation->title,
            'id' => $page->category_id,
            'alias' => $page->category->alias,
            'type' => $page->category->type
        ];

        foreach ($page->extra as $extra) {
            $attributes[] = [
                'attribute_id' => $extra->attribute_id,
                'value' => $extra->value,
                'title' => $extra->attrDescription->title,
                'entity_alias' => $extra->entity_alias,
                'value_alias' => $extra->value_alias
            ];
        }
        $pageElastic->attrs = $attributes;

        $pageElastic->likes = ArrayHelper::map($page->likes,
            'user_id',
            function($var) {
                /* @var $var Like*/
                return [
                    'name' => $var->user->getSellerName(),
                    'avatar' => $var->user->getThumb('catalog')
                ];
            }
        );

        $translations = [];
        foreach($page->translations as $translation) {
            $translations[] = ['title' => $translation->title, 'description' => $translation->content, 'locale' => $translation->locale, 'slug' => $translation->slug];
        }
        $pageElastic->translations = $translations;

        foreach ($page->translations as $locale => $translation) {
            $locales[] = ['locale' => strtolower(Yii::$app->params['languages'][$locale])];
        }
        $pageElastic->lang = $locales;

        if ($page->user->profile->activeTariff) {
            $pageElastic->tariff = [
                'price' => $page->user->profile->activeTariff->tariff->cost,
                'icon' => $page->user->profile->activeTariff->tariff->icon,
                'expires_at' => $page->user->profile->activeTariff->expires_at,
                'title' => $page->user->profile->activeTariff->tariff->title
            ];
        } else {
            $pageElastic->tariff = [
                'price' => 0,
                'icon' => null,
                'expires_at' => null,
                'title' => null
            ];
        }

        return $pageElastic;
    }

    /**
     * @return bool
     */
    public function updateLikes()
    {
        $page = $this->page;

        /* @var PageElastic $pageElastic */
        $pageElastic = PageElastic::find()->where(['id' => $page->id])->one();
        if ($pageElastic !== null) {
            $pageElastic->likes = ArrayHelper::map($page->likes,
                'user_id',
                function($var) {
                    /* @var $var Like*/
                    return [
                        'name' => $var->user->getSellerName(),
                        'avatar' => $var->user->getThumb('catalog')
                    ];
                }
            );
            return $pageElastic->save();
        }

        return false;
    }
}