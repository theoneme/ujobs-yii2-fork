<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:34
 */

namespace common\services\elastic;

use common\models\elastic\ProfileElastic;
use common\models\user\Profile;
use common\models\UserPortfolio;
use common\modules\store\models\Order;
use Yii;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class ProfileElasticService
 * @package common\services
 */
class ProfileElasticService
{
    /**
     * @var Profile
     */
    private $profile;

    /**
     * ProfileElasticService constructor.
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $profile = $this->profile;
        $attributes = [];
        $cats = [];
        $translations = [];
        $locales = [];

        $profileElastic = ProfileElastic::find()->where(['user_id' => $profile->user_id])->one();
        /* @var ProfileElastic $profileElastic */
        if ($profileElastic === null) {
            $profileElastic = new ProfileElastic();
            $profileElastic->setPrimaryKey((int)$profile->user_id);
        }

        $profileElastic->attributes = array_intersect_key($profile->attributes, $profileElastic->attributes);
        $profileElastic->is_bio_empty = empty($profile->getDescription());
        $profileElastic->updated_at = $profile->user->updated_at;
        $profileElastic->created_at = $profile->user->created_at;
        $profileElastic->sold_items = $profile->getSoldServicesCount();

        $hasCompletedOrders = Order::find()->where(['seller_id' => $profile->user_id, 'status' => [Order::STATUS_PRODUCT_COMPLETED, Order::STATUS_COMPLETED, Order::STATUS_TENDER_COMPLETED]])->exists();
        $hasPortfolio = UserPortfolio::find()->where(['user_id' => $profile->user_id, 'version' => UserPortfolio::VERSION_NEW, 'status' => UserPortfolio::STATUS_ACTIVE])->exists();
        $verified = $profile->user->confirmed_at > 0 && $profile->user->confirmed_at !== $profile->user->created_at;

        $globalScore = $hasCompletedOrders ? 3 : 0;
        $globalScore += $hasPortfolio ? 3 : 0;
        $globalScore += $verified ? 5 : 0;
        if ($profile->activeTariff) {
            $globalScore += 10;
        }
        $profileElastic->root_score = $globalScore;
        $profileElastic->upper_category_score = $globalScore;
        $profileElastic->category_score = $globalScore;

        foreach ($profile->userAttributes as $attr) {
            $attributes[] = [
                'attribute_id' => $attr->attribute_id,
                'value' => $attr->value,
                'title' => implode(' | ', array_map(function($value) {
                    return $value->title;
                }, $attr->attrValueDescriptions)),
                'entity_alias' => $attr->entity_alias,
                'value_alias' => $attr->value_alias
            ];
        }
        $profileElastic->attrs = $attributes;

        if($profile->activeTariff) {
            $profileElastic->tariff = [
                'price' => $profile->activeTariff->tariff->cost,
                'icon' => $profile->activeTariff->tariff->icon,
                'expires_at' => $profile->activeTariff->expires_at,
                'title' => $profile->activeTariff->tariff->title
            ];
        } else {
            $profileElastic->tariff = [
                'price' => 0,
                'icon' => null,
                'expires_at' => null,
                'title' => null
            ];
        }

        $profileElastic->user = [
            'username' => $profile->user->username,
            'email' => $profile->user->email,
            'phone' => $profile->user->phone
        ];

        $categories = $profile->categories;
        foreach ($categories as $category) {
            $cats[] = [
                'id' => $category->category_id
            ];
        }
        $profileElastic->categories = $cats;

        $translations = [];
        foreach($profile->translations as $translation) {
            $translations[] = ['title' => $translation->title, 'description' => $translation->content, 'locale' => $translation->locale];
        }
        $profileElastic->translations = $translations;

        foreach ($profile->translations as $locale => $translation) {
            if(!empty($translation->content)) {
                $locales[] = ['locale' => strtolower(Yii::$app->params['languages'][$locale])];
            }
        }
        $profileElastic->lang = $locales;

        return $profileElastic;
    }

    /**
     * @return bool
     */
    public function updateTariff()
    {
        $profile = $this->profile;

        $profileElastic = ProfileElastic::find()->where(['user_id' => $profile->user_id])->one();
        if ($profileElastic !== null) {
            $profileElastic->tariff = [
                'price' => $profile->activeTariff->tariff->cost,
                'icon' => $profile->activeTariff->tariff->icon,
                'expires_at' => $profile->activeTariff->expires_at
            ];

            return $profileElastic->save();
        }

        return false;
    }
}