<?php

namespace common\services\elastic;

use common\models\elastic\PageAttributeElastic;
use common\models\PageAttribute;
use common\models\UserPortfolioAttribute;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class PortfolioAttributeElasticService
 * @package common\services
 */
class PageAttributeElasticService
{
    /**
     * @var PageAttribute
     */
    private $pageAttribute;

    /**
     * PageAttributeElasticService constructor.
     * @param PageAttribute $pageAttribute
     */
    public function __construct(PageAttribute $pageAttribute)
    {
        $this->pageAttribute = $pageAttribute;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $pageAttribute = $this->pageAttribute;

        $pageAttributeElastic = PageAttributeElastic::find()->where(['id' => $pageAttribute->id])->one();
        /* @var PageAttributeElastic $pageAttributeElastic */
        if ($pageAttributeElastic === null) {
            $pageAttributeElastic = new PageAttributeElastic();
            $pageAttributeElastic->setPrimaryKey((int)$pageAttribute->id);
        }
        $pageAttributeElastic->attributes = array_intersect_key($pageAttribute->attributes, $pageAttributeElastic->attributes);

        $pageAttributeElastic->page = [
            'id' => $pageAttribute->page_id,
            'category_id' => $pageAttribute->page->category_id,
            'status' => $pageAttribute->page->status,
            'type' => $pageAttribute->page->type,
        ];

        $pageAttributeElastic->attribute = [
            'translations' => ArrayHelper::map($pageAttribute->attrDescriptions, 'locale', 'title')
        ];

        $pageAttributeElastic->attributeValue = [
            'status' => $pageAttribute->attrValue->status,
            'translations' => ArrayHelper::map($pageAttribute->attrValueDescriptions, 'locale', 'title')
        ];

        return $pageAttributeElastic;
    }

}