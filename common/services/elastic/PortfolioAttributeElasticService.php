<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:58
 */

namespace common\services\elastic;

use common\models\elastic\UserPortfolioAttributeElastic;
use common\models\UserPortfolioAttribute;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class PortfolioAttributeElasticService
 * @package common\services
 */
class PortfolioAttributeElasticService
{
    /**
     * @var UserPortfolioAttribute
     */
    private $portfolioAttribute;

    /**
     * PortfolioAttributeElasticService constructor.
     * @param UserPortfolioAttribute $portfolioAttribute
     */
    public function __construct(UserPortfolioAttribute $portfolioAttribute)
    {
        $this->portfolioAttribute = $portfolioAttribute;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $portfolioAttribute = $this->portfolioAttribute;

        $portfolioAttributeElastic = UserPortfolioAttributeElastic::find()->where(['id' => $portfolioAttribute->id])->one();
        /* @var UserPortfolioAttributeElastic $portfolioAttributeElastic */
        if ($portfolioAttributeElastic === null) {
            $portfolioAttributeElastic = new UserPortfolioAttributeElastic();
            $portfolioAttributeElastic->setPrimaryKey((int)$portfolioAttribute->id);
        }
        $portfolioAttributeElastic->attributes = array_intersect_key($portfolioAttribute->attributes, $portfolioAttributeElastic->attributes);

        $portfolioAttributeElastic->portfolio = [
            'id' => $portfolioAttribute->user_portfolio_id,
            'category_id' => $portfolioAttribute->userPortfolio->subcategory_id ?? $portfolioAttribute->userPortfolio->category_id,
            'status' => $portfolioAttribute->userPortfolio->status,
            'title' => isset($portfolioAttribute->userPortfolio->title) ? $portfolioAttribute->userPortfolio->title : '',
            'description' => isset($portfolioAttribute->userPortfolio->content) ? strip_tags($portfolioAttribute->userPortfolio->content) : '',
            'version' => $portfolioAttribute->userPortfolio->version
        ];

        $portfolioAttributeElastic->attribute = [
            'translations' => ArrayHelper::map($portfolioAttribute->attrDescriptions, 'locale', 'title')
        ];

        $portfolioAttributeElastic->attributeValue = [
            'status' => $portfolioAttribute->attrValue->status,
            'translations' => ArrayHelper::map($portfolioAttribute->attrValueDescriptions, 'locale', 'title')
        ];

        return $portfolioAttributeElastic;
    }

}