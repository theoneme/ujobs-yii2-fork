<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:58
 */

namespace common\services\elastic;

use common\models\elastic\UserPortfolioElastic;
use common\models\user\Profile;
use common\models\UserPortfolio;
use Yii;
use yii\elasticsearch\ActiveRecord;

/**
 * Class PortfolioElasticService
 * @package common\services
 */
class PortfolioElasticService
{
    /**
     * @var UserPortfolio
     */
    private $portfolio;

    /**
     * PortfolioElasticService constructor.
     * @param UserPortfolio $portfolio
     */
    public function __construct(UserPortfolio $portfolio)
    {
        $this->portfolio = $portfolio;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        }

        return $object;

    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $portfolio = $this->portfolio;
        $globalScore = $rootScore = $upperCategoryScore = $categoryScore = 0;
        $attachments = [];
        $attributes = [];
        $likes = [];

        $portfolioElastic = UserPortfolioElastic::find()->where(['id' => $portfolio->id])->one();
        if ($portfolioElastic === null) {
            /* @var UserPortfolioElastic $portfolioElastic */
            $portfolioElastic = new UserPortfolioElastic();
            $portfolioElastic->setPrimaryKey((int)$portfolio->id);
            $portfolioElastic->user = [
                'id' => $portfolio->user->id,
                'username' => $portfolio->user->username
            ];
        }
        $portfolioElastic->attributes = array_intersect_key($portfolio->attributes, $portfolioElastic->attributes);
        $portfolioElastic->category_id = $portfolio->subcategory_id ?? $portfolio->category_id;
        $portfolioElastic->likes_count = count($portfolio->likes);
        $portfolioElastic->pictures = count($portfolio->attachments);

        if ($portfolio->profile->status === Profile::STATUS_ACTIVE) {
            $globalScore += 20;
        }
        $globalScore += (int)$portfolio->likesCount * 15;
        $globalScore += count($portfolio->attachments);
        /******************/
//        $totalPortfolios = UserPortfolio::find()->where(['user_id' => $portfolio->user_id, 'status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW])->count();
//        $laterPortfolios = UserPortfolio::find()->where(['user_id' => $portfolio->user_id, 'status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW])->andWhere(['>=', 'id', $portfolio->id])->count();
//        $order = $totalPortfolios - $laterPortfolios;
//
//        if($order === 0) {
////            $rootScore = $globalScore + 10;
//        } else {
////            $rootScore = $globalScore - 10 * $order;
//        }
        $rootScore = $globalScore;
        $portfolioElastic->root_score = $rootScore;
        /******************/
//        $totalPortfolios = UserPortfolio::find()->where(['user_id' => $portfolio->user_id, 'status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW, 'category_id' => $portfolio->category_id])->andWhere(['or', ['category_id' => $portfolio->category_id], ['subcategory_id' => $portfolio->category_id]])->count();
//        $laterPortfolios = UserPortfolio::find()->where(['user_id' => $portfolio->user_id, 'status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW, 'category_id' => $portfolio->category_id])->andWhere(['or', ['category_id' => $portfolio->category_id], ['subcategory_id' => $portfolio->category_id]])->andWhere(['>=', 'id', $portfolio->id])->count();
//        $order = $totalPortfolios - $laterPortfolios;
//
//        if($order === 0) {
//            $categoryScore = $globalScore + 10;
//        } else {
//            $categoryScore = $globalScore - 10 * $order;
//        }
        $categoryScore = $globalScore;
        $portfolioElastic->category_score = $categoryScore;
        /******************/
//        $parentCategory = Category::findOne($portfolio->parent_category_id);
//        $siblings = $parentCategory->children()->select('id')->column();
//        $totalPortfolios = UserPortfolio::find()->where(['user_id' => $portfolio->user_id, 'status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW])->andWhere(['or', ['category_id' => $siblings], ['subcategory_id' => $siblings]])->count();
//        $laterPortfolios = UserPortfolio::find()->where(['user_id' => $portfolio->user_id, 'status' => UserPortfolio::STATUS_ACTIVE, 'version' => UserPortfolio::VERSION_NEW])->andWhere(['or', ['category_id' => $siblings], ['subcategory_id' => $siblings]])->andWhere(['>=', 'id', $portfolio->id])->count();
//        $order = $totalPortfolios - $laterPortfolios;
//
//        if($order === 0) {
//            $upperCategoryScore = $globalScore + 10;
//        } else {
//            $upperCategoryScore = $globalScore - 10 * $order;
//        }
        $upperCategoryScore = $globalScore;
        $portfolioElastic->upper_category_score = $upperCategoryScore;

        $portfolioElastic->lang = [
            'locale' => strtolower(Yii::$app->params['languages'][$portfolio->locale])
        ];

        $portfolioElastic->profile = [
            'user_id' => $portfolio->user->id,
            'name' => $portfolio->profile->name,
            'gravatar_email' => $portfolio->profile->gravatar_email,
            'status' => $portfolio->profile->status,
            'rating' => $portfolio->profile->rating,
            'translations' => array_map(function($var) {
                return ['title' => $var->title, 'locale' => $var->locale];
            }, $portfolio->profile->translations ?? [])
        ];

        $portfolioElastic->category = [
            'title' => $portfolio->category->translation->title,
            'id' => $portfolio->category_id,
            'alias' => $portfolio->category->alias,
            'type' => $portfolio->category->type
        ];

        foreach ($portfolio->attachments as $attachment) {
            $attachments[] = [
                'id' => $attachment->id,
                'content' => $attachment->content
            ];
        }
        $portfolioElastic->attachments = $attachments;

        foreach ($portfolio->likeUsers as $likeUser) {
            $likes[$likeUser->user_id] = [
                'name' => $likeUser->getSellerName(),
                'avatar' => $likeUser->gravatar_email,
            ];
        }
        $portfolioElastic->likes = $likes;

        foreach ($portfolio->userPortfolioAttributes as $extra) {
            $attributes[] = [
                'attribute_id' => $extra->attribute_id,
                'value' => $extra->value,
                'title' => $extra->attrDescription->title,
                'entity_alias' => $extra->entity_alias,
                'value_alias' => $extra->value_alias
            ];
        }
        $portfolioElastic->attrs = $attributes;

        if ($portfolio->profile->activeTariff) {
            $portfolioElastic->tariff = [
                'price' => $portfolio->profile->activeTariff->tariff->cost,
                'icon' => $portfolio->profile->activeTariff->tariff->icon,
                'expires_at' => $portfolio->profile->activeTariff->expires_at,
                'title' => $portfolio->profile->activeTariff->tariff->title
            ];
        } else {
            $portfolioElastic->tariff = [
                'price' => 0,
                'icon' => null,
                'expires_at' => null,
                'title' => null
            ];
        }

        return $portfolioElastic;
    }
}