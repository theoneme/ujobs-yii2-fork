<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 13:55
 */

namespace common\services\elastic;

use common\components\CurrencyHelper;
use common\models\Category;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\models\JobPackage;
use common\models\user\Profile;
use common\modules\store\models\Order;
use Yii;
use yii\elasticsearch\ActiveRecord;

/**
 * Class JobElasticService
 * @package common\services
 */
class JobElasticService
{
    /**
     * @var Job
     */
    private $job;

    /**
     * JobElasticService constructor.
     * @param Job $job
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        }

        return $object;
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $job = $this->job;

        $packages = [];
        $attachments = [];

        $jobElastic = JobElastic::find()->where(['id' => $job->id])->one();
        if ($jobElastic === null) {
            /* @var JobElastic $jobElastic */
            $jobElastic = new JobElastic();
            $jobElastic->setPrimaryKey((int)$job->id);
            $jobElastic->user = [
                'id' => $job->user->id,
                'username' => $job->user->username,
                'is_company' => false,
                'company_id' => null
            ];
        }
        $jobElastic->attributes = array_intersect_key($job->attributes, $jobElastic->attributes);
        $jobElastic->default_price = CurrencyHelper::convert($job->currency_code, 'RUB', $job->price);

        $jobElastic->count_orders = (int)Order::find()
            ->where(['seller_id' => $job->user_id])
            ->select('order.id')
            ->andWhere(['status' => Order::STATUS_COMPLETED])
            ->count();

        $globalScore = $rootScore = $upperCategoryScore = $categoryScore = 0;
        if ($job->status === Job::STATUS_ACTIVE) {
            $packageIds = JobPackage::find()->where(['job_id' => $job->id])->select('id')->scalar();
            $orders = Order::find()
                ->joinWith(['orderProduct'])
                ->where(['seller_id' => $job->user_id, 'order_product.product_id' => $packageIds, 'product_type' => [Order::TYPE_JOB, Order::TYPE_JOB_PACKAGE]])
                ->select('order.id')
                ->andWhere(['status' => Order::STATUS_COMPLETED])
                ->count();

            if ($job->profile->status === Profile::STATUS_ACTIVE) {
                $globalScore += 20;
            }
            if ($orders > 0) {
                $globalScore += 10;
            }
            if ($job->user->confirmed_at > 0 && $job->user->confirmed_at !== $job->user->created_at) {
                $globalScore += 10;
            }
            if ($job->profile->activeTariff) {
                $globalScore += 15; //(int) ($job->profile->activeTariff->tariff->cost / 2);
            }
            /******************/
            $totalJobs = Job::find()->select('id, user_id, status, type')->where(['user_id' => $job->user_id, 'status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB])->count();
            $laterJobs = Job::find()->select('id, user_id, status, type')->where(['user_id' => $job->user_id, 'status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB])->andWhere(['>=', 'id', $job->id])->count();
            $order = $totalJobs - $laterJobs;

            if ($order === 0) {
                $rootScore = $globalScore + 10;
            } else {
                $rootScore = $globalScore - 10 * $order;
            }
            $jobElastic->root_score = $rootScore;
            /******************/
            $totalJobs = Job::find()->select('id, user_id, status, type')->where(['user_id' => $job->user_id, 'status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB, 'category_id' => $job->category_id])->count();
            $laterJobs = Job::find()->select('id, user_id, status, type')->where(['user_id' => $job->user_id, 'status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB, 'category_id' => $job->category_id])->andWhere(['>=', 'id', $job->id])->count();
            $order = $totalJobs - $laterJobs;

            if ($order === 0) {
                $categoryScore = $globalScore + 10;
            } else {
                $categoryScore = $globalScore - 10 * $order;
            }
            $jobElastic->category_score = $categoryScore;
            /******************/
            $parentCategory = Category::findOne($job->parent_category_id);
            $siblings = $parentCategory->children()->select('id')->column();
            $totalJobs = Job::find()->select('id, user_id, status, type')->where(['user_id' => $job->user_id, 'status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB, 'category_id' => $siblings])->count();
            $laterJobs = Job::find()->select('id, user_id, status, type')->where(['user_id' => $job->user_id, 'status' => Job::STATUS_ACTIVE, 'type' => Job::TYPE_JOB, 'category_id' => $siblings])->andWhere(['>=', 'id', $job->id])->count();
            $order = $totalJobs - $laterJobs;

            if ($order === 0) {
                $upperCategoryScore = $globalScore + 10;
            } else {
                $upperCategoryScore = $globalScore - 10 * $order;
            }
            $jobElastic->upper_category_score = $upperCategoryScore;
        }

        $jobElastic->lang = [
            'locale' => strtolower(Yii::$app->params['languages'][$job->locale])
        ];

        if ((boolean)$job->user->is_company === true) {
            $jobElastic->user = [
                'id' => $job->user->id,
                'username' => $job->user->username,
                'is_company' => true,
                'company_id' => $job->user->company->id
            ];

            $jobElastic->profile = [
                'user_id' => $job->user->id,
                'name' => $job->user->company->getSellerName(),
                'gravatar_email' => $job->user->company->logo,
                'status' => $job->user->company->status,
                'rating' => 0,
                'translations' => array_map(function ($var) {
                    return ['title' => $var->title, 'locale' => $var->locale];
                }, $job->user->company->translations ?? [])
            ];
        } else {
            $jobElastic->profile = [
                'user_id' => $job->user->id,
                'name' => $job->profile->getSellerName(),
                'gravatar_email' => $job->profile->gravatar_email,
                'status' => $job->profile->status,
                'rating' => $job->profile->rating,
                'translations' => array_map(function ($var) {
                    return ['title' => $var->title, 'locale' => $var->locale];
                }, $job->profile->translations ?? [])
            ];
        }

        $jobElastic->category = [
            'id' => $job->category_id,
            'alias' => $job->category->alias,
        ];

        foreach ($job->attachments as $attachment) {
            $attachments[] = [
                'id' => $attachment->id,
                'content' => $attachment->content
            ];
        }
        $jobElastic->attachments = $attachments;

        foreach ($job->packages as $package) {
            $packages[] = [
                'id' => $package->id,
                'title' => $package->title,
                'description' => $package->description,
                'price' => $package->price,
                'currency_code' => $package->currency_code,
                'included' => $package->included,
                'excluded' => $package->excluded,
                'type' => $package->type
            ];
        }
        $jobElastic->packages = $packages;

        $attributes = [];
        foreach ($job->extra as $extra) {
            $attributes[] = [
                'attribute_id' => $extra->attribute_id,
                'value' => $extra->value,
                'title' => $extra->attrDescription->title,
                'entity_alias' => $extra->entity_alias,
                'value_alias' => $extra->value_alias
            ];
        }
        $jobElastic->attrs = $attributes;

        $jobElastic->translation = ['title' => $job->translation->title];

        $jobElastic->orders = Order::find()
            ->select('order.id, order.status')
            ->joinWith(['orderProduct'])
            ->where(['order_product.product_id' => $job->id])
            ->asArray()
            ->all();

        if ($job->profile->activeTariff) {
            $jobElastic->tariff = [
                'price' => $job->profile->activeTariff->tariff->cost,
                'icon' => $job->profile->activeTariff->tariff->icon,
                'expires_at' => $job->profile->activeTariff->expires_at,
                'title' => $job->profile->activeTariff->tariff->title
            ];
        } else {
            $jobElastic->tariff = [
                'price' => 0,
                'icon' => null,
                'expires_at' => null,
                'title' => null
            ];
        }

        return $jobElastic;
    }

    /**
     * @return bool
     */
    public function updateTariff()
    {
        $job = $this->job;

        $jobElastic = JobElastic::find()->where(['id' => $job->id])->one();
        if ($jobElastic !== null) {
            $jobElastic->tariff = [
                'price' => $job->profile->activeTariff->tariff->cost,
                'icon' => $job->profile->activeTariff->tariff->icon,
                'expires_at' => $job->profile->activeTariff->expires_at
            ];
            return $jobElastic->save();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function updateOrders()
    {
        $job = $this->job;

        $jobElastic = JobElastic::find()->where(['id' => $job->id])->one();
        if ($jobElastic !== null) {
            $jobElastic->orders = Order::find()
                ->select('order.id, order.status')
                ->joinWith(['orderProduct'])
                ->where(['order_product.product_id' => $job->id])
                ->asArray()
                ->all();
            return $jobElastic->save();
        }

        return false;
    }
}