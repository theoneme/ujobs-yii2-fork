<?php

namespace common\services;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\httpclient\Response;

class GoogleMapsService
{
    /**
     * @var string
     */
    private $client;
    /**
     * @var string
     */
    private $method = 'get';
    /**
     * @var string
     */
    private $url;

    /**
     * YandexTranslatorService constructor.
     */
    public function __construct()
    {
        $this->client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
        $this->url = 'https://maps.googleapis.com/maps/api/geocode/json';
    }

    /**
     * @param $address
     * @param null $language
     * @return array|bool
     */
    public function geocode($address, $language = null)
    {
        try {
            /* @var Response $response */
            if ($language === 'ua') {
                $language = 'uk';
            }
            $response = $this->client->{$this->method}($this->url, [
                'key' => Yii::$app->params['googleAPIKey'],
                'address' => $address,
                'language' => $language,
            ])->send();
            $responseData = $response->getData();
            $isOk = $response->isOk;
        } catch (Exception $e) {
            return false;
        }
        if ($isOk && !empty($responseData['results'])) {
            return [
                'lat' => $responseData['results'][0]['geometry']['location']['lat'],
                'long' => $responseData['results'][0]['geometry']['location']['lng'],
            ];
        } else {
            return false;
        }
    }

    /**
     * @param $lat
     * @param $long
     * @param null $language
     * @return bool|string
     */
    public function reverseGeocode($lat, $long, $language = null)
    {
        try {
            /* @var Response $response */
            if ($language === 'ua') {
                $language = 'uk';
            }
            $response = $this->client->{$this->method}($this->url, [
                'key' => Yii::$app->params['googleAPIKey'],
                'latlng' => $lat . ',' . $long,
                'language' => $language,
            ])->send();
            $responseData = $response->getData();
            $isOk = $response->isOk;
        } catch (Exception $e) {
            return false;
        }
        if ($isOk && !empty($responseData['results'])) {
            $fullAddress = [];
            foreach ($responseData['results'][0]['address_components'] as $component) {
                $addressType = $component['types'][0];
                switch ($addressType) {
                    case 'route':
                        $fullAddress[0] = $component['long_name'];
                        break;
                    case 'street_number':
                        $fullAddress[1] = $component['long_name'];
                        break;
                    case 'locality':
                        $fullAddress[2] = $component['long_name'];
                        break;
                    case 'administrative_area_level_1':
                        $fullAddress[3] = $component['long_name'];
                        break;
                    case 'country':
                        $fullAddress[4] = $component['long_name'];
                        break;
                }
            }
            ksort($fullAddress);
            return implode(', ', $fullAddress);
        } else {
            return false;
        }
    }

    /**
     * @param $lat
     * @param $long
     * @param null $language
     * @return bool|array
     */
    public function reverseGeocodeAsData($lat, $long, $language = null)
    {
        try {
            /* @var Response $response */
            if ($language === 'ua') {
                $language = 'uk';
            }
            $response = $this->client->{$this->method}($this->url, [
                'key' => Yii::$app->params['googleAPIKey'],
                'latlng' => $lat . ',' . $long,
                'language' => $language,
            ])->send();
            $responseData = $response->getData();
            $isOk = $response->isOk;
        } catch (Exception $e) {
            return false;
        }
        if ($isOk && !empty($responseData['results'])) {
            $addressData = [];
            foreach ($responseData['results'][0]['address_components'] as $component) {
                $addressType = $component['types'][0];
                switch ($addressType) {
                    case 'route':
                        $addressData['street'] = $component['long_name'];
                        break;
                    case 'street_number':
                        $addressData['street_number'] = $component['long_name'];
                        break;
                    case 'locality':
                        $addressData['city'] = $component['long_name'];
                        break;
                    case 'administrative_area_level_1':
                        $addressData['region'] = $component['long_name'];
                        break;
                    case 'country':
                        $addressData['country'] = $component['long_name'];
                        break;
                }
            }
            return $addressData;
        } else {
            return false;
        }
    }
}