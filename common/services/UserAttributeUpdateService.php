<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 16:26
 */

namespace common\services;

use common\modules\attribute\models\Attribute;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class UserAttributeUpdateService
 * @package common\services
 */
class UserAttributeUpdateService
{
    /**
     * @var Attribute
     */
    private $attribute;
    /**
     * @var string
     */
    private $className;
    /**
     * @var string
     */
    private $locale;
    /**
     * @var string
     */
    private $relationField;
    /**
     * @var integer
     */
    private $relationValue;
    /**
     * @var integer
     */
    private $itemId;

    /**
     * UserAttributeUpdateService constructor.
     * @param $attributeAlias
     * @param $className
     * @param $relationField
     * @param $relationValue
     */
    public function __construct($attributeAlias, $className, $relationField, $relationValue)
    {
        $attribute = Attribute::findOne(['alias' => $attributeAlias]);
        if($attribute === null) {
            throw new InvalidParamException('Wrong attribute alias');
        }
        if(!class_exists($className)) {
            throw new InvalidParamException("Class {$className} does not exists");
        }

        $this->attribute = $attribute;
        $this->locale = Yii::$app->language;
        $this->className = $className;
        $this->relationField = $relationField;
        $this->relationValue = $relationValue;
    }

    /**
     * @param $oldValue
     * @param $value
     * @return bool
     */
    public function update($oldValue, $value)
    {
        $utility = Yii::$app->get('utility');

        if(strlen($value) > 0) {
            $newAttributeObject = AttributeValueObjectBuilder::makeAttributeObject($this->className, $this->attribute->id, $value, ['locale' => $this->locale]);
            if($oldValue !== null) {
                $attributeObject = $this->className::find()
                    ->where(['attribute_id' => $this->attribute->id, $this->relationField => $this->relationValue, 'value_alias' => $utility->transliterate($oldValue)])
                    ->one();

                if($attributeObject !== null) {
                    $attributeObject->attributes = $newAttributeObject->attributes;
                    return $attributeObject->save();
                }
            }

            $valueExists = $this->className::find()->where(['attribute_id' => $this->attribute->id, $this->relationField => $this->relationValue, 'value_alias' => $utility->transliterate($value)])->exists();
            if($valueExists === false) {
                $newAttributeObject->{$this->relationField} = $this->relationValue;
                return $newAttributeObject->save();
            }

            return true;
        }

        return false;
    }

    /**
     * @param $value
     * @return bool
     */
    public function delete($value)
    {
        if(strlen($value) > 0) {
            $utility = Yii::$app->get('utility');
            $value = $this->className::find()->where(['attribute_id' => $this->attribute->id, $this->relationField => $this->relationValue, 'value_alias' => $utility->transliterate($value)])->one();
            if($value !== null) {
                return $value->delete();
            }

            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->itemId;
    }
}