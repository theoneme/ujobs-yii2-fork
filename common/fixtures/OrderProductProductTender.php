<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 07.08.2018
 * Time: 16:28
 */

namespace common\fixtures;

use common\modules\board\models\Product as ProductModel;
use common\modules\store\models\Order as OrderModel;
use common\modules\store\models\OrderProduct as OrderProductModel;
use yii\test\ActiveFixture;

/**
 * Class OrderProduct
 * @package common\fixtures
 */

class OrderProductProductTender extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\OrderProduct';
    public $depends = ['common\fixtures\Order', 'common\fixtures\Product'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $product = ProductModel::findOne(['alias' => 'fixture_alias']);
            $dataRow['product_id'] = $product->id;
            $order = OrderModel::findOne(['email' => "zxc-order-tester@test.com"]);
            $dataRow['order_id'] = $order->id;
            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                OrderProductModel::deleteAll(['price' => $item['price'], 'title' => $item['title']]);
            }
        }
    }
}