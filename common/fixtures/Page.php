<?php

namespace common\fixtures;

use common\models\Page as PageModel;
use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

/**
 * Class Page
 * @package common\fixtures
 */
class Page extends ActiveFixture
{
    /**
     * @var string
     */
    public $modelClass = 'common\models\Page';
    public $depends = ['common\fixtures\User', 'common\fixtures\Category'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        $user = UserModel::findOne(['email' => "test_email_1@gmail.com"]);

        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;
            $dataRow['user_id'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);

        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                $page = PageModel::findOne(['alias' => $item['alias']]);

                if ($page) {
                    $page->delete();
                }
            }
        }
    }
}