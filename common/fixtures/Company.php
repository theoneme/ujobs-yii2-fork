<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.07.2018
 * Time: 14:04
 */

namespace common\fixtures;

use common\models\Company as CompanyModel;
use common\models\CompanyMember;
use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

class Company extends ActiveFixture
{
    public $modelClass = 'common\models\Company';
    public $depends = ['common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        $iterator = 1;
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $user = UserModel::findOne(['email' => "test_email_{$iterator}@gmail.com"]);
            $dataRow['user_id'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
            $this->db->schema->insert(
                'company_member',
                ['user_id' => $user->id, 'company_id' => $this->data[$alias]['id'], 'role' => CompanyMember::ROLE_OWNER, 'status' => CompanyMember::STATUS_ACTIVE]
            );

            $iterator++;
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                CompanyModel::deleteAll(['status_text' => $item['status_text']]);
            }
        }
    }
}