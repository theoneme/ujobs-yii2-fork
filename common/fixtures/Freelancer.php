<?php

namespace common\fixtures;

use common\models\Freelancer as FreelancerModel;
use yii\test\ActiveFixture;

class Freelancer extends ActiveFixture
{
    public $modelClass = 'common\models\Freelancer';

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                $freelancer = FreelancerModel::findOne(['name' => $item['name'], 'email' => $item['email']]);

                if ($freelancer) {
                    $freelancer->delete();
                }
            }
        }
    }
}