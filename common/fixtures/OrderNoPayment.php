<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 27.04.2017
 * Time: 13:43
 */

namespace common\fixtures;

use common\models\Payment as PaymentModel;
use common\models\user\User as UserModel;
use common\modules\store\models\Order as OrderModel;
use yii\test\ActiveFixture;

/**
 * Class OrderNoPayment
 * @package common\fixtures
 */
class OrderNoPayment extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\Order';
    public $depends = ['common\fixtures\User', 'common\fixtures\Payment'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $seller = UserModel::findOne(['email' => 'test_email_1@gmail.com']);
            $customer = UserModel::findOne(['email' => 'test_email_2@gmail.com']);

            if($seller !== null && $customer !== null) {
                $dataRow['seller_id'] = $seller->id;
                $dataRow['customer_id'] = $customer->id;
            }

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                OrderModel::deleteAll(['email' => $item['email']]);
            }
        }
    }
}