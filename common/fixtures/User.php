<?php

namespace common\fixtures;

use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

/**
 * Class User
 * @package common\fixtures
 */
class User extends ActiveFixture
{
    /**
     * @var string
     */
    public $modelClass = 'common\models\user\User';

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();
        $data[] = ['email' => 'some_email@example.com'];
        $data[] = ['phone' => '380951234567'];

        if ($data) {
            foreach ($data as $key => $item) {
                $user = UserModel::findOne(!empty($item['email']) ? ['email' => $item['email']] : ['phone' => $item['phone']]);

                if ($user) {
                    $user->delete();
                }
            }
        }
    }
}