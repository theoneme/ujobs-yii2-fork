<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 27.04.2017
 * Time: 13:44
 */

namespace common\fixtures;

use common\models\JobExtra as JobExtraModel;
use common\modules\store\models\Order as OrderModel;
use common\modules\store\models\OrderExtra as OrderExtraModel;
use common\modules\store\models\OrderProduct as OrderProductModel;
use yii\test\ActiveFixture;

class OrderExtra extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\OrderExtra';
    public $depends = ['common\fixtures\Order', 'common\fixtures\OrderProduct', 'common\fixtures\JobExtra'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $order = OrderModel::findOne(['total' => "999998"]);
            $dataRow['order_id'] = $order->id;
            $orderProduct = OrderProductModel::findOne(['title' => "test title for fixture"]);
            $dataRow['order_product_id'] = $orderProduct->id;
            $extra = JobExtraModel::findOne(['title' => "test title for extra fixture"]);
            $dataRow['job_extra_id'] = $extra->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                OrderExtraModel::deleteAll(['price' => $item['price'], 'title' => $item['title']]);
            }
        }
    }
}