<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 16.07.2018
 * Time: 15:38
 */

namespace common\fixtures;

use common\modules\board\models\Product as ProductModel;
use common\modules\board\models\ProductAttribute as ProductAttributeModel;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\test\ActiveFixture;

class ProductAttribute extends ActiveFixture
{
    public $modelClass = 'common\modules\board\models\ProductAttribute';
    public $depends = ['common\fixtures\Product', 'common\fixtures\ModAttributeValue'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $product = ProductModel::findOne(['shipping_info' => "test shipping_info product for fixture"]);
            $dataRow['product_id'] = $product->id;
            $attribute = Attribute::findOne(['alias' => $row['entity_alias']]);
            $dataRow['attribute_id'] = $attribute->id;

            $attributeValue = AttributeValue::findOne(['alias' => $row['value_alias'], 'attribute_id' => $attribute->id]);
            $dataRow['value'] = $attributeValue->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                ProductAttributeModel::deleteAll(['value_alias' => $item['value_alias'], 'entity_alias' => $item['entity_alias']]);
            }
        }
    }
}