<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 19.07.2018
 * Time: 13:16
 */

namespace common\fixtures;


use common\models\UserPortfolio as PortfolioModel;
use common\models\UserPortfolioAttribute as PortfolioAttributeModel;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\test\ActiveFixture;

class PortfolioAttribute extends ActiveFixture
{
    public $modelClass = 'common\models\UserPortfolioAttribute';
    public $depends = ['common\fixtures\Portfolio', 'common\fixtures\ModAttributeValue'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $portfolio = PortfolioModel::findOne(['title' => "test title portfolio for fixture"]);
            $dataRow['user_portfolio_id'] = $portfolio->id;
            $attribute = Attribute::findOne(['alias' => $row['entity_alias']]);
            $dataRow['attribute_id'] = $attribute->id;
            $attributeValue = AttributeValue::findOne(['alias' => $row['value_alias'], 'attribute_id' => $attribute->id]);
            $dataRow['value'] = $attributeValue->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                PortfolioAttributeModel::deleteAll(['value_alias' => $item['value_alias'], 'entity_alias' => $item['entity_alias']]);
            }
        }
    }
}