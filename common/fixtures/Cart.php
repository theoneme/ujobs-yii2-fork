<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 29.05.2017
 * Time: 17:44
 */

namespace common\fixtures;

use common\models\user\User as UserModel;
use common\modules\store\models\Cart as CartModel;
use yii\test\ActiveFixture;

class Cart extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\Cart';
    public $depends = ['common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $user = UserModel::findOne(['email' => 'test_email_1@gmail.com']);
            $dataRow['sessionId'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                CartModel::deleteAll(['updated_at' => $item['updated_at']]);
            }
        }
    }
}