<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 16.07.2018
 * Time: 14:43
 */

namespace common\fixtures;

use common\modules\board\models\Product as ProductModel;
use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

class Product extends ActiveFixture
{
    public $modelClass = 'common\modules\board\models\Product';
    public $depends = ['common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        $iterator = 1;
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $user = UserModel::findOne(['email' => "test_email_{$iterator}@gmail.com"]);
            $dataRow['user_id'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);

            $iterator++;
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                ProductModel::deleteAll(['price' => $item['price'], 'shipping_info' => $item['shipping_info'], 'alias' => $item['alias']]);
            }
        }
    }
}