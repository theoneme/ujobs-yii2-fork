<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 10.03.2017
 * Time: 15:08
 */

namespace common\fixtures;

use common\models\Job as JobModel;
use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

class Job extends ActiveFixture
{
    public $modelClass = 'common\models\Job';
    public $depends = ['common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        $iterator = 1;
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $user = UserModel::findOne(['email' => "test_email_{$iterator}@gmail.com"]);
            $dataRow['user_id'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);

            $iterator++;
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                JobModel::deleteAll(['requirements' => $item['requirements']]);
            }
        }
    }
}