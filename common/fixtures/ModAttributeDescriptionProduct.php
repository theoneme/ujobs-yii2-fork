<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 17.07.2018
 * Time: 15:00
 */

namespace common\fixtures;


use common\modules\attribute\models\AttributeDescription as AttributeDescriptionModel;
use common\modules\attribute\models\AttributeValue as AttributeValueModel;
use yii\test\ActiveFixture;

class ModAttributeDescriptionProduct extends ActiveFixture
{
    public $modelClass = 'common\modules\attribute\models\AttributeDescription';
    public $depends = ['common\fixtures\ModAttributeValue'];

    public $help = [
        'Тег товар1' => 'tag1-fixture',
        'Тег товар2' => 'tag2-fixture',
        'Белый фиксутра' => 'beliy-fixture',
        'Коттон фикстура' => 'cotton-fixture',
        'Новый фикстура' => 'new-fixture',
        'Возможна доставка фикстура' => 'vozmozhna-dostavka-fixture',
        'apple фикстура' => 'apple-fixture',
    ];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $attrDescr = AttributeValueModel::findOne(['alias' => $this->help[$row['title']]]);
            $dataRow['entity_content'] = $attrDescr->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                AttributeDescriptionModel::deleteAll(['title' => $item['title']]);
            }
        }
    }
}