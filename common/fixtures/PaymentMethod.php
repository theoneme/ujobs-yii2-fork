<?php

namespace common\fixtures;

use common\modules\store\models\PaymentMethod as PaymentMethodModel;
use yii\test\ActiveFixture;

/**
 * Class PaymentMethod
 * @package common\fixtures
 */
class PaymentMethod extends ActiveFixture
{
    /**
     * @var string
     */
    public $modelClass = 'common\modules\store\models\PaymentMethod';

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                $paymentMethod = PaymentMethodModel::findOne(['code' => $item['code']]);

                if ($paymentMethod) {
                    $paymentMethod->delete();
                }
            }
        }
    }
}