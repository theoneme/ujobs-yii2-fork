<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 02.05.2017
 * Time: 18:09
 */

namespace common\fixtures;

use common\modules\attribute\models\AttributeValue as AttributeValueModel;
use yii\test\ActiveFixture;

class ModAttributeValue extends ActiveFixture
{
    public $modelClass = 'common\modules\attribute\models\AttributeValue';
    public $depends = ['common\fixtures\ModAttribute'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);

            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }

    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                AttributeValueModel::deleteAll(['alias' => $item['alias']]);
            }
        }
    }
}