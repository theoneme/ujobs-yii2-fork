<?php

namespace common\fixtures;

use common\models\JobPackage as JobPackageModel;
use common\modules\store\models\Order as OrderModel;
use common\modules\store\models\OrderProduct as OrderProductModel;
use yii\test\ActiveFixture;

/**
 * Class OrderProductPackage
 * @package common\fixtures
 */
class OrderProductPackage extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\OrderProduct';
    public $depends = ['common\fixtures\Order', 'common\fixtures\Currency', 'common\fixtures\JobPackage'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $package = JobPackageModel::findOne(['type' => "basic"]);
            $dataRow['product_id'] = $package->id;
            $order = OrderModel::findOne(['email' => "zxc-order-tester@test.com"]);
            $dataRow['order_id'] = $order->id;
            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                OrderProductModel::deleteAll(['price' => $item['price'], 'title' => $item['title']]);
            }
        }
    }
}