<?php

namespace common\fixtures;

use common\models\ContentTranslation as ContentTranslationModel;
use common\models\Page as PageModel;
use yii\test\ActiveFixture;

/**
 * Class ContentTranslationPage
 * @package common\fixtures
 */
class ContentTranslationPage extends ActiveFixture
{
    public $modelClass = 'common\models\ContentTranslation';
    public $depends = ['common\fixtures\Page'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();

        $page = PageModel::find()->all();
        foreach ($page as $key => $item) {
            foreach ($this->getData() as $alias => $row) {
                $dataRow = $row;

                $dataRow['entity_id'] = $item->id;
                $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
                $this->data[$key] = array_merge($dataRow, $primaryKeys);
            }
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        ContentTranslationModel::deleteAll(['entity' => 'page']);
    }
}