<?php

namespace common\fixtures;

use common\modules\attribute\models\Attribute as AttributeModel;
use common\modules\attribute\models\AttributeDescription;
use yii\test\ActiveFixture;

class ModAttribute extends ActiveFixture
{
    public $modelClass = 'common\modules\attribute\models\Attribute';
    public $depends = [];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);

            $this->data[$alias] = array_merge($dataRow, $primaryKeys);

            $this->db->schema->insert(AttributeDescription::tableName(), ['entity' => 'attribute', 'entity_content' => $this->data[$alias]['id'], 'title' => $row['name'], 'locale' => 'ru-RU']);
        }

    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            AttributeDescription::deleteAll(['entity' => 'attribute']);
            foreach ($data as $key => $item) {
                AttributeModel::deleteAll(['alias' => $item['alias']]);
            }
        }
    }
}