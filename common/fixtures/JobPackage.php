<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.03.2017
 * Time: 14:41
 */

namespace common\fixtures;

use common\models\Job as JobModel;
use common\models\JobPackage as JobPacModel;
use yii\test\ActiveFixture;

class JobPackage extends ActiveFixture
{
    public $modelClass = 'common\models\JobPackage';
    public $depends = ['common\fixtures\Job'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $job = JobModel::findOne(['requirements' => "Заранее планируется расписание уборки test 5 лет опыта Пашка"]);
            $dataRow['job_id'] = $job->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                JobPacModel::deleteAll(['type' => $item['type']]);
            }
        }
    }
}