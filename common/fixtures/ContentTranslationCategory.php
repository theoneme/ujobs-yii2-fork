<?php

namespace common\fixtures;

use common\models\ContentTranslation as ContentTranslationModel;
use common\models\Category as CategoryModel;
use yii\test\ActiveFixture;

/**
 * Class ContentTranslationCategory
 * @package common\fixtures
 */
class ContentTranslationCategory extends ActiveFixture
{
    public $modelClass = 'common\models\ContentTranslation';
    public $depends = ['common\fixtures\Category'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();

        $category = CategoryModel::find()->all();
        foreach ($category as $key => $item) {
            foreach ($this->getData() as $alias => $row) {
                $dataRow = $row;

                $dataRow['entity_id'] = $item->id;
                $dataRow['slug'] = $item->alias;
                $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
                $this->data[$key] = array_merge($dataRow, $primaryKeys);
            }
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        ContentTranslationModel::deleteAll(['entity' => 'category']);
    }
}