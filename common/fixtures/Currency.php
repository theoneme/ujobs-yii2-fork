<?php

namespace common\fixtures;

use common\modules\store\models\Currency as CurrencyModel;
use yii\test\ActiveFixture;

/**
 * Class Currency
 * @package common\fixtures
 */
class Currency extends ActiveFixture
{
    /**
     * @var string
     */
    public $modelClass = 'common\modules\store\models\Currency';

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                $currency = CurrencyModel::findOne(['code' => $item['code']]);

                if ($currency) {
                    $currency->delete();
                }
            }
        }
    }
}