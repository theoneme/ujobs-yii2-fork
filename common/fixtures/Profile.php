<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.03.2017
 * Time: 22:22
 */

namespace common\fixtures;

use common\models\user\Profile as ProfileModel;
use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

class Profile extends ActiveFixture
{
    public $modelClass = 'common\models\user\Profile';
    public $depends = ['common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $user = UserModel::findOne(['email' => $row['public_email']]);
            $dataRow['user_id'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                ProfileModel::deleteAll(['name' => $item['name'], 'bio' => $item['bio'], 'public_email' => $item['public_email']]);
            }
        }
    }
}