<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.03.2017
 * Time: 14:42
 */

namespace common\fixtures;

use common\models\ContentTranslation as ContentTranslationModel;
use common\models\Job as JobModel;
use yii\test\ActiveFixture;

/**
 * Class ContentTranslation
 * @package common\fixtures
 */
class ContentTranslation extends ActiveFixture
{
    public $modelClass = 'common\models\ContentTranslation';
    public $depends = ['common\fixtures\Job'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $job = JobModel::findAll(['requirements' => "Заранее планируется расписание уборки test 5 лет опыта Пашка"]);

            foreach ($job as $key => $item) {
                $dataRow['entity_id'] = $item->id;
                $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
                $this->data[$alias] = array_merge($dataRow, $primaryKeys);
            }
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        ContentTranslationModel::deleteAll(['entity' => 'job']);
    }
}