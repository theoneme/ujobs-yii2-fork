<?php

namespace common\fixtures;

use common\models\TariffRule as TariffRuleModel;
use yii\test\ActiveFixture;


class TariffRule extends ActiveFixture
{
    public $modelClass = 'common\models\TariffRule';
    public $depends = ['common\fixtures\Tariff'];

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                TariffRuleModel::deleteAll(['code' => $item['code']]);
            }
        }
    }
}