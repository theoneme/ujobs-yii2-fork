<?php

namespace common\fixtures;

use common\models\Category as CategoryModel;
use yii\test\ActiveFixture;

/**
 * Class Category
 * @package common\fixtures
 */
class Category extends ActiveFixture
{
    /**
     * @var string
     */
    public $modelClass = 'common\models\Category';

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                $category = CategoryModel::findOne(['id' => $item['id']]);

                if ($category) {
                    $category->delete();
                }
            }
        }
    }
}