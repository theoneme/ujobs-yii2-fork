<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 13.03.2017
 * Time: 14:42
 */

namespace common\fixtures;

use common\models\Job as JobModel;
use common\models\JobAttribute as JobAttributeModel;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use yii\test\ActiveFixture;

class JobTag extends ActiveFixture
{
    public $modelClass = 'common\models\JobAttribute';
    public $depends = ['common\fixtures\Job', 'common\fixtures\ModAttributeValue'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $job = JobModel::findOne(['requirements' => "Заранее планируется расписание уборки test 5 лет опыта Пашка"]);
            $dataRow['job_id'] = $job->id;
            $attribute = Attribute::findOne(['alias' => $row['entity_alias']]);
            $dataRow['attribute_id'] = $attribute->id;
            $attributeValue = AttributeValue::findOne(['alias' => $row['value_alias'], 'attribute_id' => $attribute->id]);
            $dataRow['value'] = $attributeValue->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                JobAttributeModel::deleteAll(['value_alias' => $item['value_alias'], 'entity_alias' => $item['entity_alias']]);
            }
        }
    }
}