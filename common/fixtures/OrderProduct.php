<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 27.04.2017
 * Time: 13:45
 */

namespace common\fixtures;

use common\models\Job as JobModel;
use common\modules\store\models\Order as OrderModel;
use common\modules\store\models\OrderProduct as OrderProductModel;
use yii\test\ActiveFixture;

/**
 * Class OrderProduct
 * @package common\fixtures
 */
class OrderProduct extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\OrderProduct';
    public $depends = ['common\fixtures\Order', 'common\fixtures\Currency', 'common\fixtures\Job'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $job = JobModel::findOne(['requirements' => "Заранее планируется расписание уборки test 5 лет опыта Пашка"]);
            $dataRow['product_id'] = $job->id;
            $order = OrderModel::findOne(['email' => "zxc-order-tester@test.com"]);
            $dataRow['order_id'] = $order->id;
            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                OrderProductModel::deleteAll(['price' => $item['price'], 'title' => $item['title']]);
            }
        }
    }
}