<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 27.04.2017
 * Time: 13:44
 */

namespace common\fixtures;

use common\models\user\User as UserModel;
use common\modules\store\models\Order as OrderModel;
use common\modules\store\models\OrderHistory as OrderHistoryModel;
use yii\test\ActiveFixture;

class OrderHistory extends ActiveFixture
{
    public $modelClass = 'common\modules\store\models\OrderHistory';
    public $depends = ['common\fixtures\Order', 'common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $order = OrderModel::findOne(['email' => "zxc-order-tester@test.com"]);
            $seller = UserModel::findOne(['email' => 'test_email_1@gmail.com']);
            $customer = UserModel::findOne(['email' => 'test_email_2@gmail.com']);

            $dataRow['order_id'] = $order->id;
            $dataRow['seller_id'] = $seller->id;
            $dataRow['customer_id'] = $customer->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                OrderHistoryModel::deleteAll(['created_at' => $item['created_at'], 'content' => $item['content']]);
            }
        }
    }
}