<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.07.2018
 * Time: 14:14
 */

namespace common\fixtures;


use common\models\ContentTranslation as ContentTranslationCompanyModel;
use common\models\Company as CompanyModel;
use yii\test\ActiveFixture;

/**
 * Class ContentTranslation
 * @package common\fixtures
 */
class ContentTranslationCompany extends ActiveFixture
{
    public $modelClass = 'common\models\ContentTranslation';
    public $depends = ['common\fixtures\Company'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $company = CompanyModel::findAll(['status_text' => "test status_text company for fixture"]);

            foreach ($company as $key => $item) {
                $dataRow['entity_id'] = $item->id;
                $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
                $this->data[$alias] = array_merge($dataRow, $primaryKeys);
            }
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        ContentTranslationCompanyModel::deleteAll(['entity' => 'company']);
    }
}