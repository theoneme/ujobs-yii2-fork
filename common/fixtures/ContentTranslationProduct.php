<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 16.07.2018
 * Time: 16:37
 */

namespace common\fixtures;

use common\models\ContentTranslation as ContentTranslationProductModel;
use common\modules\board\models\Product as ProductModel;
use yii\test\ActiveFixture;

/**
 * Class ContentTranslation
 * @package common\fixtures
 */
class ContentTranslationProduct extends ActiveFixture
{
    public $modelClass = 'common\models\ContentTranslation';
    public $depends = ['common\fixtures\Product'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $product = ProductModel::findAll(['alias' => 'fixture_alias']);

            foreach ($product as $key => $item) {
                $dataRow['entity_id'] = $item->id;
                $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
                $this->data[$alias] = array_merge($dataRow, $primaryKeys);
            }
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        ContentTranslationProductModel::deleteAll(['entity' => 'product']);
    }
}
