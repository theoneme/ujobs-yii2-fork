<?php

namespace common\fixtures;

use common\models\Tariff as TariffModel;
use yii\test\ActiveFixture;

/**
 * Class Tariff
 * @package common\fixtures
 */
class Tariff extends ActiveFixture
{
    /**
     * @var string
     */
    public $modelClass = 'common\models\Tariff';

    public $depends = ['common\fixtures\Currency'];

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                TariffModel::deleteAll(['id' => $item['id']]);
            }
        }
    }
}