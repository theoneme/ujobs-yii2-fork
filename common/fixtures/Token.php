<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 09.03.2017
 * Time: 22:57
 */

namespace common\fixtures;

use common\models\user\Token as TokenModel;
use common\models\user\User as UserModel;
use yii\test\ActiveFixture;

class Token extends ActiveFixture
{
    public $modelClass = 'common\models\user\Token';
    public $depends = ['common\fixtures\User'];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function load()
    {
        $this->resetTable();
        $this->data = [];
        $table = $this->getTableSchema();
        $iterator = 1;
        foreach ($this->getData() as $alias => $row) {
            $dataRow = $row;

            $user = UserModel::findOne(['email' => "test_email_{$iterator}@gmail.com"]);
            $dataRow['user_id'] = $user->id;

            $primaryKeys = $this->db->schema->insert($table->fullName, $dataRow);
            $this->data[$alias] = array_merge($dataRow, $primaryKeys);

            $iterator++;
        }
    }

    /**
     * Removes all existing data from the specified table and resets sequence number to 1 (if any).
     * This method is called before populating fixture data into the table associated with this fixture.
     */
    protected function resetTable()
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $item) {
                TokenModel::deleteAll(['code' => $item['code']]);
            }
        }
    }
}