<?php 
 return [
    '<strong> uJobs.me </strong> is a freelance market, where you can place your services for free, get new customers or hire a freelancer for the job you need to do.' => '<strong>uJobs.me</strong> — c\'est la bourse du travail à la pige, où Vous pouvez héberger gratuitement leurs services, recevoir de nouveaux clients ou d\'embaucher un artiste sur le travail que Vous avez à faire.',
    'Activate Your Account' => 'Activez Votre compte',
    'Additional income' => 'Les gains supplémentaires',
    'Auto-reply' => 'Auto-réponse',
    'Change Password' => 'Changer Le Mot De Passe',
    'Check the latest offers' => 'Voir les dernières offres',
    'Click on the link below to confirm your email and get started.' => 'Cliquez sur le lien ci-dessous pour confirmer Votre mail et commencer à travailler.',
    'Deal safety!' => 'La sécurité de la transaction!',
    'Dear {username}' => 'Cher(ère) {username}',
    'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!' => 'Vous êtes capable de faire quelque chose de bien? Racontez-nous sur notre site, installez le prix que vous êtes prêt à travailler. Nous omission votre service: vous recevrez des nouvelles commandes et gagnez de l\'argent!',
    'Do you need to get a job done?' => 'Vous avez besoin de faire le travail?',
    'Find service that you need!' => 'Recherchez le service',
    'Go to cart' => 'Aller à votre panier',
    'Go to view' => 'Passer à l\'affichage de',
    'Greetings! You have attempted to subscribe on notifications via {provider}. Please, do this from {link}' => 'Bienvenue! Avez-vous essayé de s\'abonner à des notifications par {provider}. S\'il vous plaît, faites-le ici {link}',
    'Hi, {username}' => 'Salut, {username}',
    'Hi, {username}!' => 'Salut, {username}!',
    'Hi, {username}, <br> There are some things you can fix on {site}!' => 'Salut, {username}, <br> Il ya quelques choses que Vous pouvez faire avec votre profil sur uJobs!',
    'Hi, {username}, <br> Welcome to {site}!' => 'Salut, {username}, <br> bienvenue sur {site}',
    'Hi, {username}, Welcome to {site}!' => 'Salut, {username}, bienvenue sur {site}!',
    'If the button above does not work, copy and paste the following URL into your browser\'s address bar.' => 'Si le bouton ci-dessus ne fonctionne pas, copiez et collez l\'URL dans la barre d\'adresse de votre navigateur.',
    'If the link above does not work, copy and paste the URL in a new browser tab. For security reasons, the link is valid for 24 hours.' => 'Si le lien ci-dessus ne fonctionne pas, copiez et collez l\'URL dans un nouvel onglet de votre navigateur. Pour votre sécurité, le lien est valable pendant 24 heures',
    'If you have any troubles with payment, you can {contact_our_support}.' => 'Si vous rencontrez des problèmes avec le paiement, vous pouvez {contact_our_support}',
    'If you wish to post announcement on our site for free, please press the following button.' => 'Si vous souhaitez publier une annonce sur notre site web, s\'il vous plaît cliquez sur le bouton',
    'Invitation to register on {site}' => 'Invitation à vous inscrire sur {site}',
    'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).' => 'Aussi une bonne idée d\'ajouter {email} dans Votre carnet d\'adresse afin de recevoir nos e-mails (pas de spam, promis!)',
    'It is never been so easy to get it done' => 'Notre service rassemble les Clients et les Artistes de différents services',
    'It is really that simple.' => 'Nous assurerons la sécurité, Vous êtes assuré d\'obtenir le paiement pour une commande réalisée.',
    'It seems your photo and profile description are not set. Please fix this!' => 'Il semble que vous n\'avez pas installé la photo et remplir la description de votre profil. Merci de corriger cela!',
    'It seems your profile description is not set. Please fix this!' => 'Il semble que vous n\'avez pas rempli la description du profil. Merci de corriger cela!',
    'It seems your profile photo is not set. Please fix this!' => 'Il semble que vous n\'avez pas installé une photo de profil. Merci de corriger cela!',
    'Join Now' => 'Joignez-vous maintenant',
    'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.' => 'Vous pouvez sélectionner tous les types de travaux, à partir d\'aide ménagère jusqu\'à la fabrication d\'un site ou de la promotion. Nous avons fusionné en ligne et hors ligne pigistes, en créant le une plate-forme pratique pour la recherche des artistes!',
    'Looking for service or order' => 'La recherche de service ou de l\'artiste',
    'New Message' => 'Nouveau message',
    'New inbox message on {site}' => 'Nouveau message sur {site}',
    'New request on service' => 'Une nouvelle demande de service',
    'Notification from {site}' => 'Avis {site}',
    'Or you can unsubscribe from email messages by this link {link}).' => 'Ou vous pouvez Vous désabonner de vos emails sur ce lien {link}',
    'Order #{number} cancelled. The money will soon be returned to the balance.' => 'La commande #{number} est annulé. L\'argent seront bientôt sur la balance.',
    'Order #{number} is automatically completed after {days, plural, one{# day} other{# days}} passed from delivery.' => 'La commande #{number} est automatiquement complété plus tard {days, plural, one{# jour} few{# journée} other{# jours}} à compter de la livraison.',
    'Order for {price}' => 'Commander pour {price}',
    'Post announcement about selling services or products on uJobs.me for free' => 'Publier gratuitement votre annonce sur la bourse uJobs sur la vente de biens ou de services',
    'Post for free' => 'Publier gratuitement',
    'Press button to return to cart' => 'Appuyez sur pour revenir à votre panier',
    'Ready to get started?' => 'Prêt à commencer à gagner?',
    'Recommendations about your profile' => 'Recommandations selon votre profil',
    'Sounds too good to be true? See for yourself!' => 'Semble trop beau pour être vrai? Regardez vous-même!',
    'Start Earning' => 'Commencer à gagner',
    'Thanks for registration on our portal!' => 'Merci de vous inscrire sur notre portail!',
    'Thanks for using our service! We offer you similar services you may be interested in:' => 'Merci pour le travail avec notre service! Vous proposons des services similaires qui peuvent vous intéresser:',
    'Thanks for your message. We will contact you as soon as possible!' => 'Merci pour votre message. Nous prendrons contact avec vous prochainement!',
    'The uJobs Team' => 'L\'équipe de uJobs',
    'These jobs are missing their images. Please, add them!' => 'De ce travail sont absents de l\'image. S\'il vous plaît, ajoutez-les!',
    'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}' => 'Cet Email est envoyé par uJobs.me.',
    'To start the process, please click the following link:<br>{link}' => 'Pour commencer, veuillez cliquer sur le lien suivant:<br>{link}',
    'User {user} has posted new article' => 'L\'utilisateur {user} a publié un nouvel article',
    'User {user} has posted new job request' => 'L\'utilisateur {user} a publié une nouvelle demande de service',
    'User {user} has posted new portfolio album' => 'L\'utilisateur {user} a publié un nouvel album de portefeuille',
    'User {user} has posted new product' => 'L\'utilisateur {user} a publié un nouveau produit',
    'User {user} has posted new product request' => 'L\'utilisateur {user} a publié une nouvelle demande d\'inscription sur les marchandises',
    'User {user} has posted new service' => 'L\'utilisateur {user} a publié un nouveau service',
    'User {user} likes your portfolio album' => 'L\'utilisateur {user} a évalué votre album portfolio',
    'User {user} shared with you a link on {site} site' => 'L\'utilisateur {user} a partagé avec vous un lien sur le site de {site}',
    'User {user} wants to add you to friend list' => 'L\'utilisateur {user} veut vous ajouter comme ami',
    'View and respond' => 'Afficher et répondre',
    'View by link: {link}' => 'Voir le lien: {link}',
    'Visit Website' => 'Aller sur le site',
    'Visit profile settings' => 'Passer à l\'édition',
    'We got a request to reset your uJobs password.' => 'Nous avons reçu une demande de réinitialisation de Votre mot de passe de votre compte uJobs',
    'Welcome to {site}' => 'Bienvenue sur {site}',
    'Which you can change in your account settings' => 'Vous pouvez la modifier dans la section Paramètres/Gestion de cabinet',
    'You are welcome on {site}' => 'Bienvenue sur {site}',
    'You did order {title} recently.' => 'Vous avez récemment commandé {title}',
    'You have accepted friendship' => 'Vous avez pris la demande d\'amitié',
    'You have accepted invitation' => 'Vous avez accepté l\'invitation',
    'You have been invited to join company «{company}» by user {user}' => 'L\'utilisateur {user} vous a invité à la société «{company}»',
    'You have created new order for user {user}' => 'Vous avez créé une nouvelle commande pour l\'utilisateur {user}',
    'You have declined friendship' => 'Vous avez rejeté une demande d\'amitié',
    'You have declined invitation' => 'Vous avez refusé l\'invitation',
    'You have just become part of the largest, most affordable and easiest to use marketplace for services and products!' => 'Vous venez de vous inscrire sur le site uJobs.me et de l\'acier membre d\'une grande communauté de professionnels!',
    'You have left response on request {tender}' => 'Vous avez répondu à une demande {tender}',
    'You have new inbox message from user {user}' => 'Vous avez un nouveau message de l\'utilisateur {user}',
    'You have new notification from {site}' => 'Vous avez une nouvelle notification sur le site {site}',
    'You have new order from user {user}' => 'Vous avez une nouvelle commande de l\'utilisateur {user}',
    'You have new response on request {tender} from {user}' => 'Vous avez une nouvelle réponse sur la demande d\' {tender} par l\'utilisateur {user}',
    'You have not set your skills for your profile. You can do it now!' => 'Vous tous n\'avez pas encore rempli les compétences dans votre profil. Vous pouvez le faire maintenant!',
    'You have subscribed on notifications via {provider}' => 'Vous êtes abonné aux notifications par {provider}',
    'You have unsubscribed from notifications via {provider}, but it`s never too late to come back :)' => 'Vous êtes désinscrit de la notification par {provider}, mais n\'est jamais trop tard de revenir :)',
    'You still have not added jobs. You can do it now!' => 'Vous tous n\'avez pas encore d\'aucun travail. Vous pouvez le faire maintenant!',
    'You still have services in cart that are awaiting for payment. You wanted to buy: {items}' => 'Dans votre panier ne sont pas payées dans les services. Vous voulez acheter: {items}',
    'Your article has passed moderation and has been added to catalog.' => 'Votre article est passé modérés, et publiée dans le répertoire.',
    'Your article has passed moderation on {site}' => 'Votre article a passé la modération sur {site}',
    'Your article {article} has passed moderation and is published in catalog' => 'Votre article {article} est passé modérés, et publiée dans le répertoire',
    'Your job has passed moderation and has been added to catalog. If you want more chances to be found and receive orders, you can post another job.' => 'Votre travail est passé modérés, et publiée dans le répertoire. Si vous voulez améliorer vos chances d\'obtenir des commandes, vous pouvez placer d\'autres services.',
    'Your job has passed moderation on {site}' => 'Votre travail est passé la modération sur {site}',
    'Your job {job} has passed moderation and is published in catalog' => 'Votre service {job} est passé modérés, et publiée dans le répertoire',
    'Your portfolio album has passed moderation and has been added to catalog.' => 'Votre album de portefeuille a été animée et publié dans le répertoire',
    'Your portfolio album has passed moderation on {site}' => 'Votre album de portefeuille a été animée sur {site}',
    'Your portfolio album {portfolio} has passed moderation and is published in catalog' => 'Votre album de portefeuille {portfolio} a été animée et publié dans le répertoire',
    'Your product has passed moderation and has been added to catalog.' => 'Votre article a été animée et publié dans le catalogue.',
    'Your product has passed moderation on {site}' => 'Votre article a été animée sur {site}',
    'Your product request has passed moderation and has been added to catalog.' => 'Votre demande d\'inscription sur la marchandise a passé modérés, et ajouté au catalogue.',
    'Your product request has passed moderation on {site}' => 'Votre demande d\'inscription sur la marchandise a passé modérés, et publiée sur le site {site}',
    'Your product request {product} has passed moderation and is published in catalog' => 'Votre demande pour le produit {product} est passé modérés, et publiée dans le répertoire',
    'Your product {product} has passed moderation and is published in catalog' => 'Votre article {product} a été animée et publié dans le répertoire',
    'Your profile has passed moderation and has been added to professional`s catalog. If you want more chances to be found and receive orders, please post your jobs.' => 'Votre profil a été animée et publié dans le catalogue. Si Vous souhaitez améliorer vos chances d\'obtenir des commandes, placez Vos services.',
    'Your profile has passed moderation on {site}' => 'Votre profil a été animée sur {site}',
    'Your request has passed moderation and has been added to catalog.' => 'Votre demande a passé la modération et publiée dans le répertoire.',
    'Your request has passed moderation on {site}' => 'Votre demande a passé la modération sur {site}',
    'Your request {job} has passed moderation and is published in catalog' => 'Votre demande de {job} est passé modérés, et publiée dans le répertoire',
    'Your transfer confirmation code: {code}' => 'Votre code de confirmation pour la traduction: {code}',
    'contact our support' => 'contactez notre équipe d\'assistance',
    'uJobs is an online marketplace for services where you can find <br>everything you need, and in unrivaled quality.' => 'uJobs est un marché en ligne pour les services, où vous pouvez <br>trouvez tout ce que vous avez besoin, et dans l\'excellence et la qualité.',
    '{0} is tired of keeping uJobs in secret.' => '{0} fatigué de garder uJobs secret',
    '{sender} left you inbox message' => 'L\'utilisateur {sender} a envoyé un message',
    '{sender} sent you individual job offer' => '{sender} a) une offre individuelle de travail',
    '{sender} sent you individual job request' => '{sender} a) la demande individuelle de travail',
    '{user} has updated order {order}' => '{user} mise à commande {order}',
    '{user} sent you invitation to join uJobs.' => '{user} vous a envoyé une invitation à participer à uJobs.',
    'Greetings!' => 'Félicitons!',
    'Invitation to {site}' => 'Une invitation à {site}',
    'On {site} you can find a lot of services for construction or buy a ready property' => '{site} vous pouvez trouver de nombreux services de construction ou d\'acheter un prêt immobilier',
];