<?php 
 return [
    'Accept offer' => 'प्रस्ताव को स्वीकार करने',
    'Accept price' => 'सहमत',
    'Accept product' => 'को स्वीकार करने के लिए माल',
    'Accept request' => 'को स्वीकार करने के लिए अनुरोध',
    'Accept work' => 'काम ले',
    'Additional information' => 'अतिरिक्त जानकारी',
    'Additional information for this order has to be filled.' => 'यह स्पष्ट करने के लिए आवश्यक विवरण के इस आदेश के',
    'Address: {address}' => 'पता: {address}',
    'All details are clarified, I can start' => 'सभी विवरण स्पष्ट कर रहे हैं, आप शुरू कर सकते हैं',
    'Answers on requirements:' => 'जवाब के लिए आवश्यकताएँ:',
    'Approve seller' => 'समर्थन विक्रेता',
    'Approve worker' => 'अनुमोदन के लिए ठेकेदार',
    'Cancel' => 'रद्द',
    'Cancel offer' => 'रद्द की पेशकश',
    'Cancel request' => 'रद्द करने के लिए अनुरोध',
    'Change conditions' => 'शर्तों को बदलने के लिए',
    'Click continue to finish this wizard and send answers to seller.' => 'क्लिक करें जारी रखने के लिए भेजने के लिए अपनी प्रतिक्रियाओं के विक्रेता.',
    'Complete order' => 'को पूरा करने के लिए आदेश',
    'Complete task' => 'कार्य पूरा करने के लिए',
    'Continue working' => 'काम जारी रखने के लिए',
    'Create Invoice' => 'प्राप्तियों बनाने',
    'Create invoice' => 'प्राप्तियों बनाने',
    'Customer accepted' => 'ग्राहक स्वीकार किए जाते हैं',
    'Customer accepted individual job offer' => 'ग्राहक स्वीकार किए जाते हैं की पेशकश की अलग-अलग काम',
    'Customer accepted job completion. Now you can left reviews for each other.' => 'ग्राहक काम लिया है । आप कर सकते हैं अब समीक्षा छोड़ देते हैं, एक दूसरे के बारे में.',
    'Customer canceled individual job request' => 'गायक रद्द के अनुरोध के लिए अलग-अलग काम',
    'Customer chose shipping method. Is everything ok?' => 'खरीदार चुना शिपिंग विधि है. सब कुछ क्रम में है?',
    'Customer declined' => 'ग्राहक से इनकार कर दिया',
    'Customer declined individual job offer' => 'ग्राहक मना कर दिया की पेशकश की अलग-अलग काम',
    'Customer has accepted task completion' => 'ग्राहक स्वीकार किए जाते हैं प्रदर्शन किया काम',
    'Customer has approved seller' => 'खरीदार विक्रेता को मंजूरी दे दी',
    'Customer has approved worker' => 'ग्राहक ठेकेदार को मंजूरी दे दी',
    'Customer has denied seller' => 'खरीदार विक्रेता को खारिज कर दिया',
    'Customer has denied worker' => 'ग्राहक को खारिज कर दिया है ठेकेदार',
    'Customer has paid deposit' => 'ग्राहक एक जमा कर दिया',
    'Customer made deposit' => 'ग्राहक एक जमा कर दिया',
    'Customer paid for individual job' => 'ग्राहक को भुगतान के लिए अलग-अलग काम',
    'Customer refused job completion' => 'ग्राहक स्वीकार नहीं किया काम',
    'Customer refused order' => 'खरीदार आदेश रद्द कर दिया',
    'Customer selected shipping method: Pickup.' => 'खरीदार चुना प्रसव की विधि: ',
    'Customer selected shipping method: Shipping.' => 'खरीदार चुना शिपिंग विधि: ',
    'Customer withdrew individual job request' => 'ग्राहक वापस ले लिया अनुरोध के लिए अलग-अलग काम',
    'Customers' => 'ग्राहकों',
    'Dear customers! If someone offer to you pay service outside uJobs, remember that in this case {secure} becomes unavailable, so the probability of losing money and time increases.' => 'प्रिय ग्राहकों. याद रखें, अगर किसी को प्रदान करता है आप के लिए सेवा के लिए भुगतान बाहर uJobs, इस मामले में {secure} अनुपलब्ध है, और खोने की संभावना पैसे और समय बढ़ जाती है । ',
    'Decline offer' => 'अस्वीकार करने के लिए प्रस्ताव',
    'Decline request' => 'अनुरोध को अस्वीकार कर',
    'Deny product' => 'अस्वीकार करने के लिए माल',
    'Deny seller' => 'अस्वीकार विक्रेता',
    'Deny worker' => 'अस्वीकार करने के लिए ठेकेदार के',
    'Describe task in details' => 'का वर्णन में विस्तार से',
    'Describe weekly task in details' => 'वर्णन काम के लिए सप्ताह में विस्तार से',
    'Enter information required for order execution.' => 'निर्दिष्ट आवश्यक विवरण के लिए आदेश है । ',
    'Enter the reason for canceling the order' => 'प्रवेश के इनकार के लिए कारण के एक आदेश',
    'Fill requirements' => 'आवश्यकताओं को पूरा',
    'Finish' => 'पूरा',
    'Finish working' => 'काम खत्म करने के लिए',
    'Firstly, you have to pay deposit equal to {value}, which will be used to reward the worker.' => 'शुरू करने के लिए सहयोग की जरूरत है तुम बनाने के लिए एक जमा की राशि में {value} इस्तेमाल किया जाएगा जो पारिश्रमिक के लिए ठेकेदार की है । ',
    'Go to order' => 'जाने के लिए आदेश',
    'Greetings! I am interested in your request.' => 'नमस्कार! मैं में दिलचस्पी थी अपने आवेदन पत्र है । ',
    'Have troubles with order?' => 'होने के साथ समस्याओं का आदेश है?',
    'I don`t have all information' => 'नहीं पर्याप्त विवरण',
    'I offer price: {price}' => 'मैं बोली: {price}',
    'I send product' => 'मैं भेजा(एक) उत्पाद',
    'If the tasks will not be completed, or will be completed partly, you may send task for revision, or refuse further cooperation.' => 'अगर काम पूरा नहीं कर रहा है या नहीं पूरी तरह से पूरा हो गया है, आप भेज सकते हैं की सेवा करने के लिए परिवर्तन बनाने के लिए या आगे सहयोग के मना.',
    'In process' => 'काम में',
    'Individual' => 'एक प्राकृतिक व्यक्ति',
    'Inform about order completion' => 'को सूचित करने के लिए आदेश के कार्यान्वयन',
    'Invoice #{number}' => 'रसीद #{number}',
    'Invoice #{number} is created' => 'रसीद #{number} बनाया',
    'Invoice for order payment' => 'के भुगतान के लिए रसीद के आदेश',
    'It seems customer sent details of this request.<br> If you do not have additional questions regards task, you can start job. <br> You can also ask customer for additional information, or cancel this order.' => 'ऐसा लगता है कि ग्राहक के काम को निर्धारित करता है आवश्यकताओं. <br> अगर सभी आवश्यक जानकारी प्रदान की गई है, आगे बढ़ना करने के लिए काम. <br>भी निर्दिष्ट कर सकते हैं विवरण यदि आवश्यक हो तो, या मना करने के लिए सहयोग.',
    'It seems customer sent requirements for this order. What is your decision?' => 'ऐसा लगता है कि खरीदार विशिष्ट आवश्यकताओं के लिए अपने काम है । अपने निर्णय है?',
    'It seems customer set weekly task. If you do not have additional questions regards task, you can start job. You can also ask customer for additional information, or cancel this order.' => 'ऐसा लगता है कि ग्राहक है, कार्यों की एक सूची के लिए सप्ताह. <br> यदि इस जानकारी के लिए पर्याप्त है, आगे बढ़ना करने के लिए काम करते हैं. <br>आप भी कर सकते हैं स्पष्ट सभी विवरण (प्रश्नों के मामले में) या से वापस लेने के लिए लेन-देन.',
    'Job is done' => 'काम किया जाता है',
    'Legal entity' => 'कानूनी इकाई',
    'Mark this order as completed when product is sent or handed over to a customer.' => 'इस निशान के रूप में आदेश को पूरा करने के बाद हम माल भेजने के लिए, या देने के लिए उसे व्यक्तिगत रूप से.',
    'Needs adjustments' => 'सुधार की आवश्यकता',
    'New response on request' => 'एक नया एक अनुरोध के जवाब',
    'Now customer has to write the task for next week or finish this order.' => 'अब ग्राहक की जरूरत है लिखने के लिए एक कार्य के लिए अगले सप्ताह, या आदेश समाप्त.',
    'Now you can reach an agreement about shipping or details of purchase.' => 'अब तुम पर सहमत कर सकते हैं के विवरण की खरीद और वितरण.',
    'Order cancelled. The money will soon be returned to the balance.' => 'आदेश रद्द कर दिया गया है. पैसे जल्दी ही हो जाएगा करने के लिए लौट आए संतुलन.',
    'Order details' => 'आदेश विवरण',
    'Order has been created' => 'आदेश बनाई गई है',
    'Order has been created. Checkout process will be continued when we receive payment for this order.' => 'आदेश बनाई गई है. चेकआउट प्रक्रिया को जारी रखा जाएगा के बाद भुगतान प्राप्त है.',
    'Order has been paid' => 'आदेश भुगतान किया जाता है',
    'Order has been paid. Seller will receive money when customer will receive product and confirm that it`s ok.' => 'आदेश भुगतान किया जाता है. विक्रेता प्राप्त होगा पैसे तुरंत माल की प्राप्ति के बाद क्रेता द्वारा और पुष्टि से खरीदार.',
    'Order has been paid. Seller will receive money when order will be completed and accepted by customer.' => 'आदेश भुगतान किया जाता है. ठेकेदार धन प्राप्त करता है के बाद तुरंत आदेश के पूरा होने और उसके ग्राहक द्वारा स्वीकृति.',
    'Order has been stopped' => 'आदेश देने बंद कर दिया',
    'Order is completed by system.' => 'आदेश पूरा सिस्टम',
    'Pay by Credit or Debit card' => 'भुगतान करने के लिए क्रेडिट या डेबिट कार्ड द्वारा',
    'Pay with {what}' => 'भुगतान करने के लिए {what}',
    'Paying deposit for individual job' => 'जमा कस्टम आदेश के लिए',
    'Paying deposit for service on {site}' => 'एक जमा सेवा के लिए पर {site}',
    'Payment for service on {0} site' => 'भुगतान सेवाओं साइट पर {0}',
    'Payments for worker occur every week, when he completes your tasks.' => 'बस्तियों में पाए जाते हैं, हर हफ्ते के बाद ठेकेदार को खत्म करने का काम है.',
    'Pickup' => 'पिक',
    'Post Job' => 'प्रकाशित',
    'Post request' => 'आवेदन प्रकाशित करने के लिए',
    'Price has been set at {price}' => 'मूल्य पर सेट किया जाता है के आसपास {price}',
    'Refuse' => 'करने के लिए मना कर दिया',
    'Refuse order' => 'आदेश को रद्द करने की',
    'Require more info' => 'के लिए अतिरिक्त जानकारी का अनुरोध',
    'Response is cancelled' => 'प्रतिक्रिया वापस ले लिया',
    'Reviews' => 'समीक्षा',
    'Seller has accepted task' => 'ठेकेदार स्वीकार किए जाते हैं एक नौकरी में काम',
    'Seller has additional questions regards this order' => 'ठेकेदार आगे के प्रश्न के बारे में आदेश',
    'Seller has marked this order as completed' => 'विक्रेता के रूप में चिह्नित के रूप में आदेश को पूरा',
    'Seller marked this order as completed. Your decision?' => 'विक्रेता के रूप में चिह्नित के रूप में आदेश को पूरा किया । अपने निर्णय है?',
    'Seller requested additional information' => 'विक्रेता के अनुरोध अतिरिक्त जानकारी',
    'Seller sent product to customer' => 'विक्रेता माल भेजा करने के लिए खरीदार',
    'Seller will receive money from deposit when customer will receive product and will confirm this.' => 'विक्रेता प्राप्त होगा से पैसे जमा करने के बाद खरीदार स्वीकार करता है माल और यह पुष्टि करते हैं । ',
    'Send a message when you finish the order. You also may attach photos or other files, especially if it was online job.' => 'एक संदेश भेजने के लिए जब समाप्त हो गया है निष्पादन के आदेश. आप कर सकते हैं भी संलग्न छवियों या अन्य फ़ाइलें. यह विशेष रूप से आवश्यक है, तो काम के proizvodilas ऑनलाइन.',
    'Send invoice to this Email' => 'करने के लिए एक चालान भेजने के लिए इस ईमेल',
    'Send message' => 'एक संदेश भेजने के लिए',
    'Send product and then send message via this form. You can also attach documents about shipping.' => 'आइटम भेजने के लिए और फिर एक संदेश भेजने के लिए इस प्रपत्र के माध्यम से. तुम भी दस्तावेजों को संलग्न करने के लिए संबंधित वितरण । ',
    'Shipping' => 'शिपिंग',
    'Solve them now' => 'अब उन्हें तय',
    'Something wrong' => 'कुछ गलत',
    'Start job' => 'काम करने के लिए मिल',
    'Task text' => 'पाठ के कार्य',
    'Thanks for choosing seller with our site.' => 'चुनने के लिए धन्यवाद विक्रेता हमारी वेबसाइट पर.',
    'Thanks for choosing worker with our site.' => 'चुनने के लिए धन्यवाद कलाकार के हमारे आदान प्रदान.',
    'This order will be marked as completed in {count, plural, =0{days} one{# job} few{# days} many{# days} other{# days}}.' => 'इस आदेश के रूप में चिह्नित किया {count, plural, =0{दिन} one{# दिन} few{# दिन} many{# दिनों} other{# दिनों}}.',
    'To begin, you have to pay deposit equal to {value}, which will be paid to seller when you receive the product.' => 'शुरू करने के लिए सहयोग की जरूरत है तुम बनाने के लिए एक जमा की राशि में {value} जमा की जाएगी, जो विक्रेता के लिए, जब आप अपने उत्पाद है । ',
    'To begin, you have to pay deposit equal to {value}, which will be paid to worker when you will accept his job.' => 'शुरू करने के लिए सहयोग की जरूरत है तुम बनाने के लिए एक जमा की राशि में {value} किया जाएगा, जो इस्तेमाल के लिए पारिश्रमिक के ठेकेदार, जब आप स्वीकार करते हैं अपने काम है.',
    'To continue conversation about this order click the button below' => 'जारी रखने के लिए पत्राचार के साथ जुड़े आदेश पर क्लिक करें नीचे दिए गए बटन',
    'To continue, customer has to pay deposit for this request.' => 'जारी रखने के लिए आप एक जमा करना चाहिए के लिए आवेदन.',
    'To get started, you need to make a deposit of {price}, which will be on your account until you accept the work. After that, the money will be transferred to the worker' => 'शुरू करने के लिए, आप की जरूरत है बनाने के लिए एक जमा की राशि में {price} है, जो अपने खाते में किया जाएगा जब तक जब तक आप स्वीकार करते हैं । उसके बाद, पैसा स्थानांतरित कर दिया जाएगा करने के लिए ठेकेदार',
    'To simplify the customer search, you can create a job and more customers will see your offer' => 'के लिए एक सफल खोज के लिए आप कर सकते हैं बनाने के लिए काम करते हैं, और ग्राहकों की एक बड़ी संख्या देखेंगे अपने प्रस्ताव',
    'To simplify the worker selection, you can create a request and more workers will see your request' => 'के लिए एक सफल ठेकेदार का चयन, आप कर सकते हैं एक टिकट बनाने के लिए, और फ्रीलांसरों की एक बड़ी संख्या देखेंगे अपने आवेदन पत्र है । ',
    'Total' => 'बस',
    'Type here, what is wrong with customer`s information.' => 'क्या संकेत मिलता विवरण से याद कर रहे हैं खरीदार.',
    'Type your message here...' => 'अपना संदेश दर्ज करें...',
    'Waiting for customer to make deposit for work' => 'प्रतीक्षा करें जब तक ग्राहक एक जमा करता है के लिए काम',
    'Waiting for customer`s response' => 'उम्मीद की प्रतिक्रिया के ग्राहक',
    'Waiting for worker`s response' => 'उम्मीद की प्रतिक्रिया के द्वारा',
    'What is missing in additional details?' => 'याद आ रही है क्या में अतिरिक्त जानकारी?',
    'What is the reason of your refusal?' => 'के लिए कारण निर्दिष्ट करें अपने रद्द',
    'What is wrong with order?' => 'समस्या क्या है?',
    'What is wrong with product?' => 'समस्या क्या है के साथ माल उठता है?',
    'What shipping method do you prefer?' => 'क्या प्रसव के विधि आप पसंद करते हैं?',
    'When are you planning to finish this order?' => 'जब आप की उम्मीद को पूरा करने के लिए आदेश?',
    'Worker accepted' => 'ठेकेदार पर सहमत हुए',
    'Worker canceled individual job offer' => 'गायक रद्द कर दिया गया है प्रस्ताव अलग-अलग काम',
    'Worker declined' => 'ठेकेदार से इनकार कर दिया',
    'Worker marked this order as completed. Your decision?' => 'ठेकेदार के रूप में चिह्नित है के रूप में आदेश पूरा हो गया है । अपने निर्णय है?',
    'Worker will be gaining gain money weekly, just after task is completed and accepted by customer.' => 'ठेकेदार धन प्राप्त होगा, साप्ताहिक प्रदर्शन के तुरंत बाद काम है और इसका स्वागत ग्राहक द्वारा.',
    'Worker will gain money just after task is completed and accepted by customer.' => 'ठेकेदार धन प्राप्त होगा के बाद तुरंत कार्य निष्पादन, और इसके स्वागत के लिए ग्राहक द्वारा',
    'You accepted' => 'आप सहमत हैं',
    'You can add a comment. You also may attach photos or other files.' => 'आप एक टिप्पणी जोड़ सकते हैं. आप कर सकते हैं भी तस्वीरें देते हैं या अन्य फ़ाइलें.',
    'You can approve or reject the seller. To clarify the necessary information, we recommend using the function of sending a message.' => 'आप कर सकते हैं स्वीकार या अस्वीकार की उम्मीदवारी इस विक्रेता. स्पष्ट करने के लिए आवश्यक जानकारी अनुशंसा करते हैं कि आप का उपयोग के साथ संदेश भेजने की सुविधा है.',
    'You can approve or reject the worker. To clarify the necessary information, we recommend using the function of sending a message.' => 'आप कर सकते हैं स्वीकार या अस्वीकार उम्मीदवारी के कलाकार । स्पष्ट करने के लिए आवश्यक जानकारी अनुशंसा करते हैं कि आप का उपयोग के साथ संदेश भेजने की सुविधा है.',
    'You can get your money back to use them, to buy other services or to pay for the work of another freelancer.' => 'आप करने में सक्षम हो जाएगा अपने पैसे वापस पाने के लिए, उन का उपयोग करने के लिए अन्य खरीद सेवाओं के लिए या काम के लिए भुगतान की एक और कलाकार है । ',
    'You can post, when do you plan to pickup product from seller (day and time)' => 'आप निर्दिष्ट कर सकते हैं जब आप करने के लिए योजना लेने से माल के विक्रेता (दिन और समय)',
    'You declined' => 'आप से इनकार कर दिया',
    'You have to fill seller`s requirements to continue this order' => 'के सवालों का जवाब करने के लिए, विक्रेता के साथ आगे बढ़ने के क्रम',
    'You made deposit' => 'आप जमा कर दिया है',
    'You may accept current price, or propose your price' => 'आप स्वीकार कर सकते हैं वर्तमान मूल्य की पेशकश करने के लिए उनके है.',
    'You may add a comment' => 'आप एक टिप्पणी जोड़ सकते हैं',
    'You may post some additional information. For example: "I`am not at home on saturdays and sundays"' => 'आप निर्दिष्ट कर सकते हैं अतिरिक्त जानकारी. उदाहरण के लिए: मैं नहीं कर रहा हूँ घर पर शनिवार और रविवार को',
    'You may write a simple message, or inform customer that you have completed order' => 'आप भेज सकते हैं एक नियमित रूप से संदेश या सूचित करने के लिए खरीदार के बारे में आदेश निष्पादन',
    'You need to accept or decline' => 'आप को स्वीकार करना चाहिए या गिरावट',
    'You received offer' => 'आप एक प्रस्ताव प्राप्त हुआ',
    'You received request' => 'आप एक अनुरोध प्राप्त हुआ',
    'Your Offer has been sent' => 'अपने प्रस्ताव भेजा है',
    'Your Request has been sent' => 'आपका अनुरोध भेज दिया गया है',
    'Your deposit is expired. You have to pay new deposit for next month to continue working with this seller.' => 'अपनी जमा राशि से अधिक है । जारी रखने के लिए काम कर रहे ठेकेदार के साथ आवश्यक करने के लिए एक जमा भुगतान अगले महीने के लिए.',
    'Your invoice is created. You can download it, or send to your email' => 'रसीद बनाई गई है. आप इसे डाउनलोड कर सकते हैं, या भेजने के लिए ईमेल द्वारा अपने आप',
    '{user} has sent response on request {tender}' => '{user} प्रतिक्रिया व्यक्त करने के लिए अनुरोध {tender}',
    '{who} will soon start working on your order.<br/><br/>This order is expected to be completed on {date}' => '{who} शुरू कर दिया है(एक) के आदेश पर काम कर रहे हैं । <br /><br />ठेकेदार की योजना को पूरा करने के लिए आदेश देने के लिए {date}',
    'Customer has provided an additional information for this order' => 'ग्राहक प्रदान करने के लिए अतिरिक्त जानकारी इस क्रम में',
];