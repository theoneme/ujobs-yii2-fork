<?php 
 return [
    '<strong> uJobs.me </strong> is a freelance market, where you can place your services for free, get new customers or hire a freelancer for the job you need to do.' => '<strong>uJobs.मुझे</strong> एक बाजार है फ्रीलांसरों के लिए जहाँ आप पोस्ट कर सकते हैं अपनी सेवाओं, नए ग्राहकों को पाने के लिए या करने के लिए एक ठेकेदार को किराए पर करने के लिए क्या काम है कि तुम क्या करने की जरूरत है । ',
    'Activate Your Account' => 'अपने खाते को सक्रिय',
    'Additional income' => 'अतिरिक्त आय',
    'Auto-reply' => 'ऑटो उत्तर',
    'Change Password' => 'पासवर्ड बदलने के लिए',
    'Check the latest offers' => 'देखें नवीनतम सौदों',
    'Click on the link below to confirm your email and get started.' => 'नीचे लिंक पर क्लिक करने के लिए अपने ईमेल की पुष्टि और काम करने के लिए मिलता है । ',
    'Deal safety!' => 'सुरक्षा के लेन-देन!',
    'Dear {username}' => 'प्रिय(ध) के {username}',
    'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!' => 'आप जानते हैं कि कुछ करने के लिए कैसे अच्छी तरह से? हमें बताओ के बारे में हमारी वेबसाइट पर यह सेट, एक कीमत है जो आप कर रहे हैं काम करने के लिए तैयार है । हम आपकी सेवा को विज्ञापित आप प्राप्त करेंगे नए आदेश और अतिरिक्त पैसे कमाने!',
    'Do you need to get a job done?' => 'आप की जरूरत है काम करने के लिए?',
    'Find service that you need!' => 'सही सेवा मिल',
    'Go to cart' => 'जाने के लिए टोकरी',
    'Go to view' => 'देखें',
    'Greetings! You have attempted to subscribe on notifications via {provider}. Please, do this from {link}' => 'में आपका स्वागत है! आप की कोशिश करने के लिए सदस्यता लें करने के लिए सूचनाओं के लिए {provider}. तो कृपया यहाँ {link}',
    'Hi, {username}' => 'हैलो, {username}',
    'Hi, {username}!' => 'हैलो, {username}!',
    'Hi, {username}, <br> There are some things you can fix on {site}!' => 'हैलो, {username}, <br> वहाँ रहे हैं कुछ चीजें आप कर सकते हैं के साथ अपने प्रोफ़ाइल पर uJobs!',
    'Hi, {username}, <br> Welcome to {site}!' => 'हैलो, {username}, <br> आपका स्वागत है {site}',
    'Hi, {username}, Welcome to {site}!' => 'हैलो, {username}, के लिए आपका स्वागत है {site}!',
    'If the button above does not work, copy and paste the following URL into your browser\'s address bar.' => 'यदि बटन के ऊपर काम नहीं करता है, कॉपी और पेस्ट निम्नलिखित यूआरएल पता पट्टी में ब्राउज़र के.',
    'If the link above does not work, copy and paste the URL in a new browser tab. For security reasons, the link is valid for 24 hours.' => 'यदि उपरोक्त लिंक काम नहीं करता है, कॉपी और पेस्ट URL में एक नया ब्राउज़र टैब. सुरक्षा उद्देश्यों के लिए, लिंक के लिए मान्य है 24 घंटे',
    'If you have any troubles with payment, you can {contact_our_support}.' => 'यदि आप किसी भी समस्याओं के भुगतान के साथ, आप कर सकते हैं {contact_our_support}',
    'If you wish to post announcement on our site for free, please press the following button.' => 'यदि आप चाहते हैं एक विज्ञापन स्थान के लिए, हमारी वेबसाइट पर क्लिक करें',
    'Invitation to register on {site}' => 'निमंत्रण पर रजिस्टर करने के लिए {site}',
    'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).' => 'यह भी एक अच्छा विचार जोड़ने के लिए {email} के लिए आपके पते की किताब प्राप्त करने के लिए हमारे ईमेल (स्पैम नहीं है, वादा करता हूँ!)',
    'It is never been so easy to get it done' => 'हमारी सेवा को एकजुट करती है ग्राहकों और प्रदाताओं की विभिन्न सेवाओं के',
    'It is really that simple.' => 'हम सुरक्षा सुनिश्चित करेगा, आप तय कर रहे हैं भुगतान प्राप्त करने के लिए आदेश के लिए है । ',
    'It seems your photo and profile description are not set. Please fix this!' => 'ऐसा लगता है कि आप निर्दिष्ट नहीं किया है एक तस्वीर नहीं भरने के विवरण के साथ अपने प्रोफ़ाइल की पहुंच है. कृपया इसे ठीक करें!',
    'It seems your profile description is not set. Please fix this!' => 'आप की तरह लग रहा था नहीं भरने में प्रोफ़ाइल विवरण. कृपया इसे ठीक करें!',
    'It seems your profile photo is not set. Please fix this!' => 'आप की तरह लग रहा है नहीं अपने प्रोफ़ाइल तस्वीर सेट करें. कृपया इसे ठीक करें!',
    'Join Now' => 'अब सम्मिलित हों',
    'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.' => 'आप चुन सकते हैं किसी भी प्रकार के काम की मदद से, घर के आसपास बनाने से पहले एक वेबसाइट या विज्ञापन अभियानों. हम संयुक्त ऑनलाइन और ऑफलाइन फ्रीलांसरों बनाने, सबसे सुविधाजनक मंच खोज करने के लिए कलाकारों के लिए!',
    'Looking for service or order' => 'सेवाओं की खोज के द्वारा या',
    'New Message' => 'नया संदेश',
    'New inbox message on {site}' => 'नए संदेश से: {site}',
    'New request on service' => 'एक नया आवेदन के लिए सेवा',
    'Notification from {site}' => 'अधिसूचना {site}',
    'Or you can unsubscribe from email messages by this link {link}).' => 'या आप कर सकते हैं से सदस्यता समाप्त ईमेल न्यूज़लेटर के लिए इस लिंक पर {link}',
    'Order #{number} cancelled. The money will soon be returned to the balance.' => 'आदेश #{number} रद्द कर दिया है । पैसे के लिए जाना जाएगा संतुलन.',
    'Order #{number} is automatically completed after {days, plural, one{# day} other{# days}} passed from delivery.' => 'आदेश #{number} है स्वचालित रूप से के बाद समाप्त हो गया {days, plural, one{# दिन} few{# दिन} other{# दिनों}} डिलीवरी की तारीख है । ',
    'Order for {price}' => 'उपलब्ध के लिए {price}',
    'Post announcement about selling services or products on uJobs.me for free' => 'अपने नि: शुल्क विज्ञापन पर विनिमय uJobs पर वस्तुओं या सेवाओं की बिक्री',
    'Post for free' => 'जगह करने के लिए नि: शुल्क',
    'Press button to return to cart' => 'क्लिक करने के लिए लौटने के लिए गाड़ी',
    'Ready to get started?' => 'तैयार कमाई शुरू करने के लिए?',
    'Recommendations about your profile' => 'सिफारिशों पर अपनी प्रोफाइल',
    'Sounds too good to be true? See for yourself!' => 'लगता भी सच्चा होना अच्छा? खुद के लिए देखें!',
    'Start Earning' => 'कमाई शुरू',
    'Thanks for registration on our portal!' => 'पंजीकरण के लिए धन्यवाद हमारे पोर्टल पर!',
    'Thanks for using our service! We offer you similar services you may be interested in:' => 'अपने काम के लिए धन्यवाद के साथ हमारी सेवा! आप की पेशकश की इसी तरह की सेवाओं है कि हो सकता है ब्याज की आप के लिए:',
    'Thanks for your message. We will contact you as soon as possible!' => 'अपने संदेश के लिए धन्यवाद. हम जल्द ही आपसे संपर्क करेंगे!',
    'The uJobs Team' => 'टीम uJobs',
    'These jobs are missing their images. Please, add them!' => 'इस काम में, छवियों याद कर रहे हैं. कृपया उन्हें जोड़ने!',
    'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}' => 'इस ईमेल द्वारा भेजा कंपनी uJobs.मुझे.',
    'To start the process, please click the following link:<br>{link}' => 'शुरू करने के लिए, कृपया निम्न लिंक पर क्लिक करें:<br>{link}',
    'User {user} has posted new article' => 'उपयोगकर्ता {user} ने एक नया लेख प्रकाशित',
    'User {user} has posted new job request' => 'उपयोगकर्ता {user} में प्रकाशित किया गया है एक नया आवेदन के लिए सेवा',
    'User {user} has posted new portfolio album' => 'उपयोगकर्ता {user} जारी किया गया है एक नए एल्बम, पोर्टफोलियो',
    'User {user} has posted new product' => 'उपयोगकर्ता {user} पोस्ट किया गया है एक नया आइटम',
    'User {user} has posted new product request' => 'उपयोगकर्ता {user} में प्रकाशित किया गया है एक नया आवेदन के लिए उत्पाद',
    'User {user} has posted new service' => 'उपयोगकर्ता {user} में प्रकाशित किया गया है एक नई सेवा',
    'User {user} likes your portfolio album' => 'उपयोगकर्ता {user} है लोग अपने एल्बम पोर्टफोलियो',
    'User {user} shared with you a link on {site} site' => 'उपयोगकर्ता {user} ने एक लिंक साझा के साथ आप वेबसाइट पर {site}',
    'User {user} wants to add you to friend list' => 'उपयोगकर्ता {user} जोड़ने के लिए चाहता है के रूप में आप एक दोस्त',
    'View and respond' => 'देखने के लिए और उत्तर',
    'View by link: {link}' => 'देखने पर लिंक: {link}',
    'Visit Website' => 'वेबसाइट पर जाने के लिए',
    'Visit profile settings' => 'जाने के लिए संपादित करें',
    'We got a request to reset your uJobs password.' => 'हम एक अनुरोध प्राप्त हुआ है के लिए अपना पासवर्ड रीसेट खाते से uJobs',
    'Welcome to {site}' => 'आपका स्वागत है {site}',
    'Which you can change in your account settings' => 'आप बदल सकते हैं सेटिंग्स में/नियंत्रण कैबिनेट',
    'You are welcome on {site}' => 'आपका स्वागत है {site}',
    'You did order {title} recently.' => 'आप हाल ही में आदेश दिया है {title}',
    'You have accepted friendship' => 'आप दोस्ती का अनुरोध स्वीकार किए जाते हैं',
    'You have accepted invitation' => 'आप निमंत्रण स्वीकार किए जाते हैं',
    'You have been invited to join company «{company}» by user {user}' => 'उपयोगकर्ता {user} को आमंत्रित किया गया है आप में शामिल होने के लिए कंपनी "{company}"',
    'You have created new order for user {user}' => 'आपने बनाया है एक नए आदेश के लिए उपयोगकर्ता {user}',
    'You have declined friendship' => 'आप दोस्त के अनुरोध को खारिज कर दिया',
    'You have declined invitation' => 'आप निमंत्रण मना कर दिया',
    'You have just become part of the largest, most affordable and easiest to use marketplace for services and products!' => 'तुम सिर्फ वेबसाइट पर पंजीकृत uJobs.मुझे और बन गया एक भागीदार के एक बड़े समुदाय के पेशेवरों!',
    'You have left response on request {tender}' => 'आप जवाब के लिए अनुरोध {tender}',
    'You have new inbox message from user {user}' => 'आप नए संदेश से {user}',
    'You have new notification from {site}' => 'आप एक नई अधिसूचना वेबसाइट पर {site}',
    'You have new order from user {user}' => 'आप एक नया आदेश से {user}',
    'You have new response on request {tender} from {user}' => 'आप एक नए प्रतिक्रिया के लिए अनुरोध {tender} से उपयोगकर्ता {user}',
    'You have not set your skills for your profile. You can do it now!' => 'आप अभी भी नहीं भरा कौशल में अपने प्रोफ़ाइल की पहुंच है. आप अब यह कर सकते हैं!',
    'You have subscribed on notifications via {provider}' => 'आप सदस्यता के लिए सूचनाओं के लिए {provider}',
    'You have unsubscribed from notifications via {provider}, but it`s never too late to come back :)' => 'आप से हट सूचनाओं के लिए {provider}, लेकिन यह कभी नहीं बहुत देर हो चुकी वापस जाने के लिए :)',
    'You still have not added jobs. You can do it now!' => 'आप अभी भी नहीं जोड़ा है एक ही काम है. आप अब यह कर सकते हैं!',
    'You still have services in cart that are awaiting for payment. You wanted to buy: {items}' => 'अपनी गाड़ी में नहीं थे के लिए भुगतान सेवाओं. होगा आप की तरह की खरीद करने के लिए: {items}',
    'Your article has passed moderation and has been added to catalog.' => 'अपने लेख किया गया है संचालित किया और प्रकाशित किया गया था निर्देशिका में.',
    'Your article has passed moderation on {site}' => 'अपने लेख पारित कर दिया गया है, कम मात्रा में {site}',
    'Your article {article} has passed moderation and is published in catalog' => 'अपने लेख है {article} संचालित बीत चुके हैं और निर्देशिका में रखा गया है',
    'Your job has passed moderation and has been added to catalog. If you want more chances to be found and receive orders, you can post another job.' => 'अपने काम के पारित कर दिया गया है और मॉडरेशन में रखा गया है निर्देशिका. अगर आप चाहते हैं की संभावना बढ़ाने के लिए प्राप्त करने के आदेश में, आप होस्ट कर सकते हैं अन्य सेवाओं । ',
    'Your job has passed moderation on {site}' => 'अपने काम के पारित कर दिया गया है, कम मात्रा में {site}',
    'Your job {job} has passed moderation and is published in catalog' => 'आपकी सेवा {job} अनुमोदित किया गया है और निर्देशिका में रखा गया है',
    'Your portfolio album has passed moderation and has been added to catalog.' => 'अपने एल्बम विभागों पारित संयम और निर्देशिका में रखा गया है',
    'Your portfolio album has passed moderation on {site}' => 'अपने एल्बम विभागों पारित मात्रा में {site}',
    'Your portfolio album {portfolio} has passed moderation and is published in catalog' => 'अपने एल्बम पोर्टफोलियो {portfolio} संचालित बीत चुके हैं और प्रकाशित सूची में',
    'Your product has passed moderation and has been added to catalog.' => 'अपने उत्पाद को पारित कर दिया और मॉडरेशन में रखा गया है निर्देशिका.',
    'Your product has passed moderation on {site}' => 'अपने उत्पाद को पारित कर दिया, कम मात्रा में {site}',
    'Your product request has passed moderation and has been added to catalog.' => 'आपके अनुरोध के माल के लिए किया गया है संचालित किया और जोड़ा गया करने के लिए सूची है । ',
    'Your product request has passed moderation on {site}' => 'अपने अनुरोध करने के लिए उत्पाद संचालित बीत चुके हैं और वेबसाइट पर प्रकाशित {site}',
    'Your product request {product} has passed moderation and is published in catalog' => 'के लिए अपने अनुरोध आइटम {product} संचालित किया गया था और निर्देशिका में रखा गया है',
    'Your product {product} has passed moderation and is published in catalog' => 'अपने उत्पाद {product} पारित संयम और निर्देशिका में रखा गया है',
    'Your profile has passed moderation and has been added to professional`s catalog. If you want more chances to be found and receive orders, please post your jobs.' => 'अपने प्रोफ़ाइल संचालित बीत चुके हैं और प्रकाशित सूची में. यदि आप चाहें तो आप की संभावना बढ़ाने के लिए प्राप्त करने के आदेश जगह है, अपनी सेवाओं को.',
    'Your profile has passed moderation on {site}' => 'अपने प्रोफ़ाइल पारित मात्रा में {site}',
    'Your request has passed moderation and has been added to catalog.' => 'आपके अनुरोध किया गया है संचालित किया और प्रकाशित किया गया था निर्देशिका में.',
    'Your request has passed moderation on {site}' => 'अपने आवेदन मिल गया है अतीत में संयम {site}',
    'Your request {job} has passed moderation and is published in catalog' => 'आपके अनुरोध {job} संचालित किया गया था और निर्देशिका में रखा गया है',
    'Your transfer confirmation code: {code}' => 'आपकी पुष्टि कोड के लिए अनुवाद: {code}',
    'contact our support' => 'हमारी सहायता टीम से संपर्क',
    'uJobs is an online marketplace for services where you can find <br>everything you need, and in unrivaled quality.' => 'uJobs — एक ऑनलाइन बाजार के लिए सेवाओं, जहां आप कर सकते हैं <br>सब कुछ आप की जरूरत है, और नायाब गुणवत्ता । ',
    '{0} is tired of keeping uJobs in secret.' => '{0} uJobs रखने के थक गए एक गुप्त',
    '{sender} left you inbox message' => 'उपयोगकर्ता {sender} तुम्हें एक संदेश भेजा',
    '{sender} sent you individual job offer' => '{sender} भेजा(एक) आप की पेशकश की अलग-अलग काम',
    '{sender} sent you individual job request' => '{sender} भेजा(एक) के लिए एक अनुरोध के अलग-अलग काम',
    '{user} has updated order {order}' => '{user} अद्यतन आदेश {order}',
    '{user} sent you invitation to join uJobs.' => '{user} तुम्हें भेजा एक निमंत्रण में शामिल होने के लिए uJobs.',
    'Greetings!' => 'में आपका स्वागत है!',
    'Invitation to {site}' => 'निमंत्रण के लिए {site}',
    'On {site} you can find a lot of services for construction or buy a ready property' => 'करने के लिए {site} आप पा सकते हैं सेवाओं की एक किस्म के निर्माण के लिए या खरीदने के लिए तैयार संपत्ति',
];