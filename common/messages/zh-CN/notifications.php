<?php 
 return [
    '<strong> uJobs.me </strong> is a freelance market, where you can place your services for free, get new customers or hire a freelancer for the job you need to do.' => '<strong>uJobs.me</strong> 是自由职业者平台，这里您可以免费发布自己的服务，收到新的客户或者雇员工。',
    'Activate Your Account' => '激活您的帐户',
    'Additional income' => '补充的收入',
    'Auto-reply' => '自动回复',
    'Change Password' => '改变密码',
    'Check the latest offers' => '查看最新的供应',
    'Click on the link below to confirm your email and get started.' => '确认电子邮件和开始工作，请点击下面的链接。',
    'Deal safety!' => '安全交易！',
    'Dear {username}' => '亲爱的{username}',
    'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!' => '您会做有些事情很好吗？在我们的网站介绍自己，制定工作价格。我们对您的服务做广告：您会受到新的订单和赚更多钱！',
    'Do you need to get a job done?' => '您找人做您的工作？',
    'Find service that you need!' => '找您需要的服务',
    'Go to cart' => '到购物车',
    'Go to view' => '开始查看',
    'Greetings! You have attempted to subscribe on notifications via {provider}. Please, do this from {link}' => '欢迎！ 您想订阅通过{provider}. 请做这个在这里{link}',
    'Hi, {username}' => '你好，{username}',
    'Hi, {username}!' => '你好，{username}!',
    'Hi, {username}, <br> There are some things you can fix on {site}!' => '你好，{username}, <br> 有些事情您可以跟您的档案做uJobs!',
    'Hi, {username}, <br> Welcome to {site}!' => '你好，{username}, <br> 欢迎来到{site}',
    'Hi, {username}, Welcome to {site}!' => '你好，{username}欢迎来到{site}!',
    'If the button above does not work, copy and paste the following URL into your browser\'s address bar.' => '如果上按钮点不了，请您复制和粘贴下面的URL到浏览器的地址栏。',
    'If the link above does not work, copy and paste the URL in a new browser tab. For security reasons, the link is valid for 24 hours.' => '如果以上链接无效，请将该网址复制并粘贴到新的浏览器标签中。 出于安全原因，该链接有效期为24小时',
    'If you have any troubles with payment, you can {contact_our_support}.' => '如果您支付时碰到问题，您可以{contact_our_support}',
    'If you wish to post announcement on our site for free, please press the following button.' => '如果你想要的地方一个广告在我们的网站上，请点击',
    'Invitation to register on {site}' => '邀请注册{site}',
    'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).' => '很好的主意把您的{email}加到您的地址簿(没有垃圾邮件，承诺！)',
    'It is never been so easy to get it done' => '我们的网站联合起来客户和员工在一起',
    'It is really that simple.' => '我们保证安全，您保证的收到工作的付款。',
    'It seems your photo and profile description are not set. Please fix this!' => '看来，您没上传照片和没有填写您的档案。请您修改！',
    'It seems your profile description is not set. Please fix this!' => '看来，您没填写您的档案。请您修改！',
    'It seems your profile photo is not set. Please fix this!' => '看来，您没上传档案的照片。请您修改！',
    'Join Now' => '现在加入',
    'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.' => '您可以选择任何的工作，从家务帮助者起到创作网站或者进行广告服务。我们联合起来了在线的和无线的自由职业者，我们创作了最方便的找员工的平台！',
    'Looking for service or order' => '找服务或者雇员',
    'New Message' => '新消息',
    'New inbox message on {site}' => '新消息:{site}',
    'New request on service' => '新的服务请求',
    'Notification from {site}' => '通知{site}',
    'Or you can unsubscribe from email messages by this link {link}).' => '或者您可以取消订阅电子邮件通讯，通过这个链接{link}',
    'Order #{number} cancelled. The money will soon be returned to the balance.' => '订单#{number}取消。 钱马上到账户。',
    'Order #{number} is automatically completed after {days, plural, one{# day} other{# days}} passed from delivery.' => '订单#{number}自动完成，因为过了{days, plural, one{#天} few{#天} other{#天}}从发货日期。',
    'Order for {price}' => '订单价{price}',
    'Post announcement about selling services or products on uJobs.me for free' => '把你的免费广告上交换uJobs上销售的货物或服务',
    'Post for free' => '把免费的',
    'Press button to return to cart' => '点击返回购物车',
    'Ready to get started?' => '准备好了开始赚钱吗？',
    'Recommendations about your profile' => '您的档案建议',
    'Sounds too good to be true? See for yourself!' => '听起来太好就像假话？自己看！',
    'Start Earning' => '开始赚钱',
    'Thanks for registration on our portal!' => '谢谢您在我们网站注册!',
    'Thanks for using our service! We offer you similar services you may be interested in:' => '谢谢您和我们的网站工作！ 我们推荐同样的服务，您会有兴趣：',
    'Thanks for your message. We will contact you as soon as possible!' => '感谢您对您的消息。 我们会联系你快！',
    'The uJobs Team' => 'uJobs团队',
    'These jobs are missing their images. Please, add them!' => '这个工作缺少照片。请您上传！',
    'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}' => '这个邮件被uJobs.me发送的。',
    'To start the process, please click the following link:<br>{link}' => '要开始，请点击：<br>{link}',
    'User {user} has posted new article' => '用户{user}发布了新的文章',
    'User {user} has posted new job request' => '用户{user}发布了新的服务请求',
    'User {user} has posted new portfolio album' => '用户{user}发布了新的企业样本',
    'User {user} has posted new product' => '用户{user}发布了新的产品',
    'User {user} has posted new product request' => '用户{user}发布了新的产品请求',
    'User {user} has posted new service' => '用户{user}发布了新的服务',
    'User {user} likes your portfolio album' => '用户{user}评价您的企业样本',
    'User {user} shared with you a link on {site} site' => '用户{user}有共享一个链接的网站{site}',
    'User {user} wants to add you to friend list' => '用户{user}想加您为好友',
    'View and respond' => '查看和回答',
    'View by link: {link}' => '通过连接查看：{link}',
    'Visit Website' => '到网站',
    'Visit profile settings' => '到编辑',
    'We got a request to reset your uJobs password.' => '我们收到了您的uJobs帐户重置密码的请求',
    'Welcome to {site}' => '欢迎来到{site}',
    'Which you can change in your account settings' => '您可以在设置/账户管理部分修改',
    'You are welcome on {site}' => '欢迎来到{site}',
    'You did order {title} recently.' => '您最近订单{title}',
    'You have accepted friendship' => '您接受好友请求',
    'You have accepted invitation' => '你接受邀请',
    'You have been invited to join company «{company}» by user {user}' => '用户{user}邀请您加入公司"{company}"',
    'You have created new order for user {user}' => '您创建了新的订单给用户{user}',
    'You have declined friendship' => '您拒绝了好友请求',
    'You have declined invitation' => '您拒绝了邀请',
    'You have just become part of the largest, most affordable and easiest to use marketplace for services and products!' => '您刚刚在uJobs.me网站注册了。您成为一个很大的专家社会的成员！',
    'You have left response on request {tender}' => '您回应请求了{tender}',
    'You have new inbox message from user {user}' => '您收到了新的信息被{user}发的',
    'You have new notification from {site}' => '您收到了新的通知在网站{site}',
    'You have new order from user {user}' => '您收到了新的订单从{user}',
    'You have new response on request {tender} from {user}' => '您收到了新的请求回应{tender}从用户{user}',
    'You have not set your skills for your profile. You can do it now!' => '您还没填写在您的档案的您的能力。您现在可以去做！',
    'You have subscribed on notifications via {provider}' => '您订购通知{provider}',
    'You have unsubscribed from notifications via {provider}, but it`s never too late to come back :)' => '您取消订阅通知通过{provider}但是从来不晚再回去:)',
    'You still have not added jobs. You can do it now!' => '您还没添加工作。您现在可以去做！',
    'You still have services in cart that are awaiting for payment. You wanted to buy: {items}' => '您的购物车还称下没付款的服务。 您想购买:{items}',
    'Your article has passed moderation and has been added to catalog.' => '你的文章已经放缓，并张贴在目录。',
    'Your article has passed moderation on {site}' => '你的文章已经通过审核{site}',
    'Your article {article} has passed moderation and is published in catalog' => '你的文章{article}通过主持，并放在目录',
    'Your job has passed moderation and has been added to catalog. If you want more chances to be found and receive orders, you can post another job.' => '您的工作过了批准。如果您想有更多接受预订单机会，请您发布其他服务。',
    'Your job has passed moderation on {site}' => '您的工作过了批准{site}',
    'Your job {job} has passed moderation and is published in catalog' => '您的服务{job}已被批准，并放在目录',
    'Your portfolio album has passed moderation and has been added to catalog.' => '您的企业样本已被批准，并放在目录',
    'Your portfolio album has passed moderation on {site}' => '您的企业样本已被批准{site}',
    'Your portfolio album {portfolio} has passed moderation and is published in catalog' => '您的企业样本{portfolio}已被批准，并放在目录',
    'Your product has passed moderation and has been added to catalog.' => '您的产品已被批准，并放在目录',
    'Your product has passed moderation on {site}' => '您的产品已被批准在{site}网站',
    'Your product request has passed moderation and has been added to catalog.' => '您的产品请求已被批准，并放在目录',
    'Your product request has passed moderation on {site}' => '您的产品请求已被批准，并放在网站上{site}',
    'Your product request {product} has passed moderation and is published in catalog' => '你的要求对项目{product}主持，并放在目录',
    'Your product {product} has passed moderation and is published in catalog' => '您的产品{product}已被批准，并放在目录',
    'Your profile has passed moderation and has been added to professional`s catalog. If you want more chances to be found and receive orders, please post your jobs.' => '您的档案已被批准，并放在目录。如果您想提高受订单的机会，请发布您的服务。',
    'Your profile has passed moderation on {site}' => '您的档案已被批准{site}',
    'Your request has passed moderation and has been added to catalog.' => '您的请求已被批准，并放在目录',
    'Your request has passed moderation on {site}' => '您的请求已被批准在{site}网站',
    'Your request {job} has passed moderation and is published in catalog' => '你的请求{job}主持，并放在目录',
    'Your transfer confirmation code: {code}' => '转钱验证码:{code}',
    'contact our support' => '联系服务支持中心',
    'uJobs is an online marketplace for services where you can find <br>everything you need, and in unrivaled quality.' => 'uJobs—一个在线服务市场，你可以 <br>找到你需要的一切，和无与伦比的质量。',
    '{0} is tired of keeping uJobs in secret.' => '{0}uJobs累了保持一个秘密',
    '{sender} left you inbox message' => '用户{sender}给您发了信息',
    '{sender} sent you individual job offer' => '{sender}给您发了个性工作的供应',
    '{sender} sent you individual job request' => '{sender}给您发了个性工作的请求',
    '{user} has updated order {order}' => '{user}更新了订单{order}',
    '{user} sent you invitation to join uJobs.' => '{user}给你一个邀请加入uJobs的。',
    'Greetings!' => '欢迎！',
    'Invitation to {site}' => '邀请{site}',
    'On {site} you can find a lot of services for construction or buy a ready property' => '{site}你可以找到各种各样的服务，用于建造或购买一个现成的酒店',
];