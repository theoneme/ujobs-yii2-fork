<?php 
 return [
    'Account Support' => '帐户支持',
    'Account replenishment' => '填充',
    'Active' => '激活',
    'Additional Days' => '更多日子',
    'Additional Information' => '更多信息',
    'Additional info' => '更多信息',
    'Address' => '地址',
    'Admin comment' => '管理员的评论',
    'Advertisement' => '广告',
    'Album Title' => '图册名称',
    'Alias' => '别名',
    'All' => '所有',
    'All views' => '总观看',
    'Amount' => '金额',
    'Answer' => '答案',
    'Anytime' => '随时',
    'Archived' => '存档',
    'Are you sure you want to delete this item?' => '确认要删除这个？',
    'Attachments' => '附加文件',
    'Attribute' => '特征',
    'Attribute ID' => '特征ID',
    'Attribute set' => '特征套',
    'Average execution time' => '平均时间',
    'Balance' => '金额',
    'Balance Change' => '改变金额',
    'Bank SWIFT-Code' => 'SWIFT代码的银行',
    'Bank name' => '银行的名称',
    'Banner' => '横幅',
    'Banner mobile' => '手机横幅',
    'Belongs to category' => '属于类别',
    'Canceled' => '取消',
    'Card Number' => '卡号',
    'Category' => '类别',
    'Category ID' => '类别的ID',
    'Category template' => '类别的模板',
    'City' => '城市',
    'Code' => '代码',
    'Comment' => '评论',
    'Company Address' => '公司地址',
    'Company Banner' => '公司的横幅',
    'Company Description' => '公司的说明',
    'Company ID' => null,
    'Company Logo' => '公司的标志',
    'Company Name' => '公司名称',
    'Company status text' => '公司的状况',
    'Completion Date' => '完成日期',
    'Completion time' => '完成时间',
    'Confirm Password' => '确认密码',
    'Confirmed' => '证实了',
    'Content' => '主要内容',
    'Content Preview/Subtitle' => '小题目',
    'Conversation ID' => '对话ID',
    'Cost' => '价格',
    'Country' => '国家',
    'Country ID' => '国家',
    'Country Title' => '国家',
    'Create' => '创建',
    'Create User Withdrawal' => '取款请求',
    'Created At' => '创建日期',
    'Created By' => '创建好了',
    'Currency Code' => '货币码',
    'Currency for your prices' => '您价格的货币',
    'Customer' => '买家',
    'Data' => '',
    'Datetime' => '日期和时间',
    'Day' => '日子',
    'Degree' => '程度',
    'Delete' => '删除',
    'Deleted' => '已删除',
    'Description' => '主要内容',
    'Duration' => '长期性',
    'Email' => '邮箱',
    'Entity' => null,
    'Entity Alias' => null,
    'Entity Content' => null,
    'Entity ID' => null,
    'Excluded' => '排除',
    'Execution time' => '执行时间',
    'Expiration Date' => '截止日期',
    'Expires At' => '过期',
    'Feature Support' => '功能支持',
    'First User ID' => null,
    'Firstname' => '名字',
    'From ID' => '发件人的ID',
    'House' => '房子',
    'Housing' => '弄',
    'ID' => 'ID',
    'INN' => '纳税人识别号码',
    'Image' => '图像',
    'Images' => '图像',
    'Included' => '包括',
    'Invitation sent' => '邀请发送成功',
    'Invoice' => '帐户',
    'Invoice File' => '帐户的文件',
    'Invoice ID' => '账户的ID',
    'Is Approved' => '批准',
    'Is Enabled' => '包括',
    'Is Required' => null,
    'Is Text' => '文本',
    'Is active' => '激活',
    'Is moderated' => '修改成功',
    'Is visible' => '可见',
    'Job ID' => '工作的ID',
    'Job Support' => '工作支持',
    'Key' => null,
    'Language' => '语言',
    'Lastname' => '姓',
    'Lat' => null,
    'Layout' => '框架',
    'Level' => '水平',
    'Lm views' => '上个月观看',
    'Locale' => '区域设置',
    'Logo' => null,
    'Long' => null,
    'M views' => '本月观看',
    'Made purchase' => '购买了',
    'Mail Sent' => '该信已发送',
    'Measure ID' => null,
    'Measure unit' => '单元',
    'Message' => '信息',
    'Message ID' => '信息的ID',
    'Method ID' => null,
    'Middlename' => '父',
    'Month' => '月',
    'Name' => '名字',
    'New Password' => '新密码',
    'Next Step ID' => '下一步的ID',
    'OGRN' => '姓名：                                                                            账号：                                                        手机号码：',
    'Old Password' => '旧密码',
    'Order' => '订单',
    'Order Date' => '订单日期',
    'Order ID' => '订单ID',
    'Order Support' => '订单支持',
    'Page ID' => null,
    'Page type' => '页面类',
    'Pages template' => '网页模板',
    'Parent ID' => '父ID',
    'Parent category' => '父类别',
    'Payment ID' => '付款的ID',
    'Payment method' => '支付方式',
    'Payment system' => '付款系统',
    'Payments' => '付款',
    'Pending' => '等待',
    'Per Page' => '页面上的元件',
    'Percent bonus' => '奖励的利率',
    'Phone' => '电话号码',
    'Photo' => '照片',
    'Previous Step ID' => '前一步的ID',
    'Price' => '价格',
    'Product' => '产品',
    'Product ID' => '产品的ID',
    'Publish date' => '出版日期',
    'Published' => '发布成功(过了批准)',
    'Quantity' => '数量',
    'Question' => '问题',
    'Rating' => '评级',
    'Read' => '阅读的',
    'Reason Additional' => '另外原因',
    'Reason ID' => '原因的ID',
    'Receiver name' => '收件人的姓名',
    'Receiver wallet id' => '收件人的钱包的ID',
    'Referral_id' => '推荐的人的ID',
    'Refund for canceled order' => '取消订单退款',
    'Registered' => '注册',
    'Report a Bug' => '报告错误',
    'Request' => '请求',
    'Requirement' => '要求',
    'Requirements' => '要求',
    'Requirements type' => null,
    'Resolved' => '解决了',
    'Response' => '回应',
    'Role' => '作用',
    'Root' => '根',
    'Salary' => '薪水',
    'Second User ID' => null,
    'Seller' => '卖家',
    'Sender' => '发送者',
    'Sender wallet id' => '送件人钱包的ID',
    'Seo Description' => 'Seo说明',
    'Seo Keywords' => 'Seo关键词',
    'Seo Robots' => 'Seo机器人',
    'Seo Title' => 'SEO标题',
    'Service' => '服务',
    'Short Description' => '简短的说明',
    'Site' => '网站',
    'Skills' => '能力',
    'Skype' => 'Skype',
    'Something about you' => '关于您',
    'Something wrong with this email' => '这个电子邮件出现了问题',
    'Sort Field' => '排序字段',
    'Sort Order' => '排序顺序',
    'Specialization' => '专业',
    'Specialties' => '专业',
    'Specialty' => '专业',
    'Starred' => '收藏',
    'State/Region' => '国家/地区',
    'Status' => '状态',
    'Step ID' => '步骤ID',
    'Subject' => '主题',
    'Summary' => '一共',
    'Tags' => '标记',
    'Target Account' => '目标帐户',
    'Target Method' => '目标的方法',
    'Tariff ID' => '套餐的ID',
    'Template' => '模板',
    'Text' => '文本',
    'Title' => '名字',
    'To ID' => '收件的ID',
    'Token' => '令牌',
    'Total' => '金额',
    'Total bonus' => '奖金额',
    'Transfer amount' => '量的转移',
    'Tree ID' => '树ID',
    'Type' => '类型',
    'Type not selected' => '种类没有选择',
    'Unknown filter' => '未知的过滤器',
    'Unknown status' => '未知的状态',
    'Unread' => '未读',
    'Update' => '更新',
    'Update date' => '更新',
    'Update {modelClass}: ' => '更新{modelClass}:',
    'Updated At' => '改变日期',
    'Updated By' => '被谁更新了',
    'Url' => 'Url',
    'User' => '用户',
    'User ID' => '用户',
    'User Portfolio ID' => '业绩案例的ID',
    'User Withdrawals' => '请求取款',
    'Value' => '值',
    'Value Alias' => '别值',
    'Verify code' => '验证码',
    'Video' => '视频',
    'Viewed at' => '日访问',
    'Views' => '观看',
    'Vk' => null,
    'Wallet number' => '帐户号码',
    'WhatsApp' => null,
    'Year' => '年',
    'Year End' => '结束年份',
    'Year Start' => '开始年份',
    'Your Email' => '您的电子邮件',
    'Your Name' => '您的名字',
    'Zip Code' => '邮政编码
',
    'Execution time (days)' => '运行时(日)',
    'First Name' => '名称',
    'Last Name' => '姓',
    'Mortgage' => '抵押贷款',
    'Additional information' => '额外信息',
    'Houses' => '家',
];