<?php 
 return [
    'Accept offer' => 'Accettare l\'offerta',
    'Accept price' => 'D\'accordo',
    'Accept product' => 'Prendere la merce',
    'Accept request' => 'Accettare la richiesta',
    'Accept work' => 'Prendere il lavoro',
    'Additional information' => 'Ulteriori informazioni',
    'Additional information for this order has to be filled.' => 'È necessario chiarire i dettagli su questo ordine',
    'Address: {address}' => 'Indirizzo: {address}',
    'All details are clarified, I can start' => 'Tutti i dettagli raffinati, posso iniziare',
    'Answers on requirements:' => 'Le risposte alle richieste:',
    'Approve seller' => 'Di approvare il venditore',
    'Approve worker' => 'Di approvare',
    'Cancel' => 'La cancellazione',
    'Cancel offer' => 'Annullare l\'offerta',
    'Cancel request' => 'Annullare la richiesta',
    'Change conditions' => 'Modificare le condizioni di',
    'Click continue to finish this wizard and send answers to seller.' => 'Fare clic su continua per inviare le risposte al venditore.',
    'Complete order' => 'Completare l\'ordine',
    'Complete task' => 'Completare l\'operazione',
    'Continue working' => 'Continuare a lavorare',
    'Create Invoice' => 'La creazione di ricevute',
    'Create invoice' => 'La creazione di ricevute',
    'Customer accepted' => 'Il cliente ha accettato',
    'Customer accepted individual job offer' => 'Il cliente ha accettato l\'offerta individuale di lavoro',
    'Customer accepted job completion. Now you can left reviews for each other.' => 'Il cliente ha accettato il lavoro. Ora è possibile lasciare commenti sulla vicenda.',
    'Customer canceled individual job request' => 'L\'artista ha annullato la richiesta di lavoro individuale',
    'Customer chose shipping method. Is everything ok?' => 'L\'acquirente ha scelto il metodo di spedizione. Se va tutto bene?',
    'Customer declined' => 'Il cliente ha rifiutato di',
    'Customer declined individual job offer' => 'Il cliente ha rifiutato l\'offerta di lavoro individuale',
    'Customer has accepted task completion' => 'Il cliente ha accettato il lavoro svolto',
    'Customer has approved seller' => 'L\'acquirente ha approvato il venditore',
    'Customer has approved worker' => 'Il cliente ha approvato di',
    'Customer has denied seller' => 'L\'acquirente ha respinto il venditore',
    'Customer has denied worker' => 'Il cliente ha rifiutato di',
    'Customer has paid deposit' => 'Il cliente ha effettuato un deposito',
    'Customer made deposit' => 'Il cliente ha effettuato un deposito',
    'Customer paid for individual job' => 'Il cliente ha pagato il lavoro individuale',
    'Customer refused job completion' => 'Il cliente non ha accettato il lavoro',
    'Customer refused order' => 'L\'acquirente ha annullato l\'ordine',
    'Customer selected shipping method: Pickup.' => 'L\'acquirente ha scelto un metodo di spedizione: Pickup',
    'Customer selected shipping method: Shipping.' => 'L\'acquirente ha scelto un metodo di spedizione: Spedizione',
    'Customer withdrew individual job request' => 'Il cliente ha ritirato la richiesta di lavoro individuale',
    'Customers' => 'I clienti',
    'Dear customers! If someone offer to you pay service outside uJobs, remember that in this case {secure} becomes unavailable, so the probability of losing money and time increases.' => 'Cari clienti. Ricordate, se qualcuno si offre di pagare il servizio al di fuori uJobs, in questo caso, {secure} non è più disponibile, e la probabilità di perdita di tempo e denaro aumenta.',
    'Decline offer' => 'Rifiutare l\'offerta',
    'Decline request' => 'Rifiutare la richiesta',
    'Deny product' => 'Penalizzare la merce',
    'Deny seller' => 'Rifiutare venditore',
    'Deny worker' => 'Rifiutare di',
    'Describe task in details' => 'Descrivere il processo in dettaglio',
    'Describe weekly task in details' => 'Descrivi il compito della settimana in dettaglio',
    'Enter information required for order execution.' => 'Specificare i dettagli necessari per l\'esecuzione dell\'ordine.',
    'Enter the reason for canceling the order' => 'Specificare il motivo del rifiuto dell\'ordine',
    'Fill requirements' => 'Compila i requisiti',
    'Finish' => 'Completare',
    'Finish working' => 'Finire il lavoro',
    'Firstly, you have to pay deposit equal to {value}, which will be used to reward the worker.' => 'Per avviare una cooperazione dovrete versare un deposito pari a {value}, che sarà utilizzato per la remunerazione dell\'artista.',
    'Go to order' => 'Andare per ordine',
    'Greetings! I am interested in your request.' => 'Ciao!!! Mi interessa la vostra applicazione.',
    'Have troubles with order?' => 'Qualche problema con l\'ordine?',
    'I don`t have all information' => 'Non manca di dettagli',
    'I offer price: {price}' => 'Offro prezzo: {price}',
    'I send product' => 'Ho postato(e) la merce',
    'If the tasks will not be completed, or will be completed partly, you may send task for revision, or refuse further cooperation.' => 'Se il lavoro non viene effettuata o completata non completamente, è possibile inviare un servizio di apportare modifiche o rifiutare un\'ulteriore cooperazione.',
    'In process' => 'Nel lavoro',
    'Individual' => 'Persona fisica',
    'Inform about order completion' => 'Informare sull\'esecuzione di un ordine',
    'Invoice #{number}' => 'Ricevuta #{number}',
    'Invoice #{number} is created' => 'Ricevuta #{number} creata',
    'Invoice for order payment' => 'Ricevuta per il pagamento dell\'ordine',
    'It seems customer sent details of this request.<br> If you do not have additional questions regards task, you can start job. <br> You can also ask customer for additional information, or cancel this order.' => 'Sembra che il cliente ha deciso con i requisiti di lavoro. <br> Se tutti i dati sono indicati, procedere alla esecuzione di un processo. <br>È anche possibile affinare i dettagli, se necessario, o di rifiutare la cooperazione.',
    'It seems customer sent requirements for this order. What is your decision?' => 'Sembra che l\'acquirente ha i requisiti per il Vostro lavoro. La vostra decisione?',
    'It seems customer set weekly task. If you do not have additional questions regards task, you can start job. You can also ask customer for additional information, or cancel this order.' => 'Sembra che il cliente ha indicato un elenco di attività a settimana. <br> Se queste informazioni, è sufficiente iniziare a svolgere il lavoro. <br>Inoltre è possibile chiarire i dettagli interessanti (in caso di domande) o di recedere dal contratto.',
    'Job is done' => 'Il lavoro è fatto',
    'Legal entity' => 'Persona giuridica',
    'Mark this order as completed when product is sent or handed over to a customer.' => 'Contrassegnare questo ordine come "completato" dopo aver spedito la merce, o consegnarlo personalmente.',
    'Needs adjustments' => 'Insispensabile',
    'New response on request' => 'Nuova risposta alla domanda',
    'Now customer has to write the task for next week or finish this order.' => 'Ora il cliente deve scrivere il compito per la prossima settimana, o completare l\'ordine.',
    'Now you can reach an agreement about shipping or details of purchase.' => 'Ora si può essere d\'accordo sui dettagli di acquisto e consegna.',
    'Order cancelled. The money will soon be returned to the balance.' => 'Ordine annullato. Presto i soldi saranno restituiti al saldo.',
    'Order details' => 'I dettagli dell\'ordine',
    'Order has been created' => 'Ordine creato',
    'Order has been created. Checkout process will be continued when we receive payment for this order.' => 'Ordine creato. Il processo di checkout proseguirà dopo la ricezione del pagamento.',
    'Order has been paid' => 'L\'ordine è pagato',
    'Order has been paid. Seller will receive money when customer will receive product and confirm that it`s ok.' => 'Ordine pagato. Il venditore riceverà il denaro immediatamente dopo il ricevimento della merce l\'acquirente, e la conferma da parte dell\'acquirente.',
    'Order has been paid. Seller will receive money when order will be completed and accepted by customer.' => 'Ordine pagato. Artista получет denaro immediatamente dopo il completamento dell\'ordine e la sua ricezione da parte del cliente.',
    'Order has been stopped' => 'Ordine fermato',
    'Order is completed by system.' => 'Ordine completato il sistema di',
    'Pay by Credit or Debit card' => 'Pagare con carta di credito o carta di debito',
    'Pay with {what}' => 'Pagare per {what}',
    'Paying deposit for individual job' => 'Un deposito per singolo ordine',
    'Paying deposit for service on {site}' => 'Un deposito per il servizio di {site}',
    'Payment for service on {0} site' => 'Servizi di pagamento sul sito {0}',
    'Payments for worker occur every week, when he completes your tasks.' => 'I calcoli si svolgono ogni settimana, dopo che l\'artista completare il compito.',
    'Pickup' => 'Pickup',
    'Post Job' => 'Pubblicare il lavoro',
    'Post request' => 'Invia la tua richiesta',
    'Price has been set at {price}' => 'Il prezzo è a circa {price}',
    'Refuse' => 'Rinunciare',
    'Refuse order' => 'Rifiutare l\'ordine',
    'Require more info' => 'Per richiedere ulteriori informazioni',
    'Response is cancelled' => 'La risposta è revocato',
    'Reviews' => 'Recensioni',
    'Seller has accepted task' => 'L\'artista ha accettato un incarico di lavoro',
    'Seller has additional questions regards this order' => 'Artista di ulteriori domande riguardo a un ordine',
    'Seller has marked this order as completed' => 'Il venditore ha contrassegnato come completato l\'ordine',
    'Seller marked this order as completed. Your decision?' => 'Il venditore ha contrassegnato questo ordine come completato. La vostra decisione?',
    'Seller requested additional information' => 'Il venditore ha richiesto ulteriori informazioni',
    'Seller sent product to customer' => 'Il venditore ha spedito la merce all\'acquirente',
    'Seller will receive money from deposit when customer will receive product and will confirm this.' => 'Il venditore riceve i soldi dal deposito dopo che l\'acquirente ha accettato la merce e confermare.',
    'Send a message when you finish the order. You also may attach photos or other files, especially if it was online job.' => 'Invia un messaggio, quando ha finito l\'esecuzione dell\'ordine. È inoltre possibile allegare immagini o altri file. In particolare è necessario, se il lavoro производитлась in modalità online.',
    'Send invoice to this Email' => 'Inviare la fattura a questa Email',
    'Send message' => 'Invia un messaggio',
    'Send product and then send message via this form. You can also attach documents about shipping.' => 'Inviare la merce e quindi inviare un messaggio tramite questo modulo. Si può anche allegare i documenti relativi alla consegna.',
    'Shipping' => 'Consegna',
    'Solve them now' => 'Decidere adesso',
    'Something wrong' => 'Qualcosa che non va',
    'Start job' => 'Iniziare a lavorare',
    'Task text' => 'Il testo del compito',
    'Thanks for choosing seller with our site.' => 'Grazie per aver scelto un venditore sul nostro sito.',
    'Thanks for choosing worker with our site.' => 'Grazie per aver scelto di nostro scambio.',
    'This order will be marked as completed in {count, plural, =0{days} one{# job} few{# days} many{# days} other{# days}}.' => 'Questo l\'ordine è contrassegnato come realizzato attraverso {count, plural, =0{giorni} one{# giorno} few{# giorno} many{# giorni} other{# giorni}}.',
    'To begin, you have to pay deposit equal to {value}, which will be paid to seller when you receive the product.' => 'Per avviare una cooperazione dovrete versare un deposito pari a {value}, che verrà assegnato al venditore, quando si accetta la sua merce.',
    'To begin, you have to pay deposit equal to {value}, which will be paid to worker when you will accept his job.' => 'Per avviare una cooperazione dovrete versare un deposito pari a {value}, che sarà utilizzato per la remunerazione dell\'artista, quando prenderete il suo lavoro.',
    'To continue conversation about this order click the button below' => 'Per continuare la corrispondenza relativa a questo ordine, fare clic sul pulsante qui sotto',
    'To continue, customer has to pay deposit for this request.' => 'Per continuare, è necessario versare un deposito di tale domanda.',
    'To get started, you need to make a deposit of {price}, which will be on your account until you accept the work. After that, the money will be transferred to the worker' => 'Per cominciare, dovrete versare un deposito pari a {price}, che sarà sul vostro conto, finché non si accetta il lavoro. Dopo di che, i soldi saranno trasferiti artista',
    'To simplify the customer search, you can create a job and more customers will see your offer' => 'Per una ricerca di successo dei clienti è possibile creare lavoro, e un gran numero di clienti vedrà la vostra offerta',
    'To simplify the worker selection, you can create a request and more workers will see your request' => 'Per eseguire con successo la selezione di un artista si può creare la domanda, e un gran numero di liberi professionisti vedrà la tua richiesta.',
    'Total' => 'Tutto',
    'Type here, what is wrong with customer`s information.' => 'Specificare quali parti manca da parte dell\'acquirente.',
    'Type your message here...' => 'Inserisci il tuo messaggio...',
    'Waiting for customer to make deposit for work' => 'Aspettiamo fino a quando il cliente versa il deposito per il lavoro',
    'Waiting for customer`s response' => 'Aspettiamo la risposta del cliente',
    'Waiting for worker`s response' => 'Aspettiamo la risposta di',
    'What is missing in additional details?' => 'Cosa manca per ulteriori informazioni?',
    'What is the reason of your refusal?' => 'Specificare il motivo Del rifiuto',
    'What is wrong with order?' => 'Che razza di problema è?',
    'What is wrong with product?' => 'Che problema con la merce si è verificato?',
    'What shipping method do you prefer?' => 'Quale metodo di spedizione che si preferisce?',
    'When are you planning to finish this order?' => 'Quando Si prevede di completare l\'ordine?',
    'Worker accepted' => 'L\'artista ha scelto di',
    'Worker canceled individual job offer' => 'L\'artista ha annullato l\'offerta individuale di lavoro',
    'Worker declined' => 'L\'artista ha rifiutato',
    'Worker marked this order as completed. Your decision?' => 'L\'artista ha contrassegnato questo ordine come completato. La vostra decisione?',
    'Worker will be gaining gain money weekly, just after task is completed and accepted by customer.' => 'L\'artista riceverà denaro settimanale, una volta dopo l\'esecuzione di compiti e la sua ricezione da parte del cliente.',
    'Worker will gain money just after task is completed and accepted by customer.' => 'L\'artista riceverà il denaro immediatamente dopo l\'operazione, e la sua ricezione il cliente',
    'You accepted' => 'Hai accettato',
    'You can add a comment. You also may attach photos or other files.' => 'È possibile aggiungere un commento. Inoltre è possibile allegare foto o altri file.',
    'You can approve or reject the seller. To clarify the necessary information, we recommend using the function of sending a message.' => 'Si può approvare o rifiutare la candidatura del venditore. Per ulteriori informazioni consigliamo prima di utilizzare la funzione di invio messaggi.',
    'You can approve or reject the worker. To clarify the necessary information, we recommend using the function of sending a message.' => 'Si può approvare o rifiutare la candidatura di questo artista. Per ulteriori informazioni consigliamo prima di utilizzare la funzione di invio messaggi.',
    'You can get your money back to use them, to buy other services or to pay for the work of another freelancer.' => 'Sarete in grado di recuperare i loro soldi, per utilizzarli per l\'acquisto di altri servizi o per pagare il lavoro di un altro artista.',
    'You can post, when do you plan to pickup product from seller (day and time)' => 'È possibile specificare quando avete intenzione di ritirare la merce presso il venditore (giorno e ora)',
    'You declined' => 'Si è rifiutato di',
    'You have to fill seller`s requirements to continue this order' => 'Rispondi alle domande del venditore, per procedere con l\'ordine',
    'You made deposit' => 'Hai fatto un deposito',
    'You may accept current price, or propose your price' => 'Si può essere d\'accordo sulla il prezzo corrente o di offrire loro.',
    'You may add a comment' => 'È possibile aggiungere un commento',
    'You may post some additional information. For example: "I`am not at home on saturdays and sundays"' => 'È possibile specificare ulteriori informazioni. Ad esempio: non sono a casa il sabato e la domenica',
    'You may write a simple message, or inform customer that you have completed order' => 'Potete inviare un messaggio normale o informare l\'acquirente di esecuzione dell\'ordine',
    'You need to accept or decline' => 'È necessario accettare o rifiutare',
    'You received offer' => 'Hai ricevuto un\'offerta',
    'You received request' => 'Ti è arrivata la richiesta',
    'Your Offer has been sent' => 'La vostra proposta inviata',
    'Your Request has been sent' => 'La tua richiesta è stata inviata',
    'Your deposit is expired. You have to pay new deposit for next month to continue working with this seller.' => 'Il deposito è finita. Per continuare con l\'esecutore è necessario effettuare un versamento per il mese successivo.',
    'Your invoice is created. You can download it, or send to your email' => 'Ricevuta creato. È possibile scaricare o inviare via Email',
    '{user} has sent response on request {tender}' => '{user} risposto alla domanda {tender}',
    '{who} will soon start working on your order.<br/><br/>This order is expected to be completed on {date}' => '{who} iniziato(e) lavorare su ordine.<br /><br />Artista prevede di completare l\'ordine a {date}',
    'Customer has provided an additional information for this order' => 'Il cliente ha fornito ulteriori informazioni per l\'ordine',
];