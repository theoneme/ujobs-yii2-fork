<?php 
 return [
    'Accept offer' => '引き受ける',
    'Accept price' => '同意',
    'Accept product' => '受け入れ品',
    'Accept request' => 'リクエストを受け付けるため',
    'Accept work' => 'の仕事',
    'Additional information' => '追加情報',
    'Additional information for this order has to be filled.' => '機構を明らかにする必要がある内容をこの順に',
    'Address: {address}' => '住所:{address}',
    'All details are clarified, I can start' => 'すべての詳細は明らかに動作しませんのでご留意ください',
    'Answers on requirements:' => 'に対する回答の要件:',
    'Approve seller' => '賛販売',
    'Approve worker' => '承認者の',
    'Cancel' => '消',
    'Cancel offer' => 'Cancelを提供',
    'Cancel request' => '解除の請求',
    'Change conditions' => '条件を変更',
    'Click continue to finish this wizard and send answers to seller.' => 'Continue続行するをクリックし送信への対応の商品です。',
    'Complete order' => 'の順',
    'Complete task' => '完了のタスク',
    'Continue working' => 'に続き、',
    'Create Invoice' => '領収書の作成',
    'Create invoice' => '領収書の作成',
    'Customer accepted' => 'お客様の受け入れ',
    'Customer accepted individual job offer' => 'お客様に受け入れ、個人の仕事内容',
    'Customer accepted job completion. Now you can left reviews for each other.' => 'お客様の仕事です。 では、今までの休暇のレビュています。',
    'Customer canceled individual job request' => 'きゃりーぱみゅぱみゅは解除の請求個々の作業',
    'Customer chose shipping method. Is everything ok?' => 'のバイヤーを選びました配送方法です。 すべてのもいいのですか？',
    'Customer declined' => 'お客様の拒否',
    'Customer declined individual job offer' => 'お客様の個人の仕事内容',
    'Customer has accepted task completion' => 'お客様の受け入れを行う',
    'Customer has approved seller' => 'バイヤーに承認され販売',
    'Customer has approved worker' => 'お客様の承認の請負',
    'Customer has denied seller' => 'を拒否されたバイヤーの販売',
    'Customer has denied worker' => 'お客様の拒否され、契約者の',
    'Customer has paid deposit' => 'お客様のた預金',
    'Customer made deposit' => 'お客様のた預金',
    'Customer paid for individual job' => 'お客様のお支払のために個々の作業',
    'Customer refused job completion' => 'お客様のになれませんの仕事',
    'Customer refused order' => 'の買取消しの',
    'Customer selected shipping method: Pickup.' => 'のバイヤーを選んだの方法で納入 ',
    'Customer selected shipping method: Shipping.' => 'のバイヤーを選びました配送方法：',
    'Customer withdrew individual job request' => 'お客様の撤退を求めら個々の作業',
    'Customers' => 'お客様の',
    'Dear customers! If someone offer to you pay service outside uJobs, remember that in this case {secure} becomes unavailable, so the probability of losing money and time increases.' => 'お客様です。 覚えていれば、誰かには、金を納入していただくためのサービス出uJobs、この場合には{secure}はできませんが、確率が時間とお金が増加します。',
    'Decline offer' => 'を拒否する提案',
    'Decline request' => '要求を拒否',
    'Deny product' => 'を拒否する物品',
    'Deny seller' => '拒否する売',
    'Deny worker' => 'を拒否するのは、契約者の',
    'Describe task in details' => 'で記述するジョブ詳細',
    'Describe weekly task in details' => '記譲渡の詳細',
    'Enter information required for order execution.' => '指定の詳細に。',
    'Enter the reason for canceling the order' => '入力の理由に拒絶のための',
    'Fill requirements' => '完了の要件',
    'Finish' => '完全',
    'Finish working' => '仕上げと仕上がり',
    'Firstly, you have to pay deposit equal to {value}, which will be used to reward the worker.' => 'の開始に協力する必要があり預金額{value}が行われる報酬を受領して実施したものです。',
    'Go to order' => 'ご注文はこちらから',
    'Greetings! I am interested in your request.' => 'ハロー！ に興味を持っていました。',
    'Have troubles with order?' => 'この問題を注文すか？',
    'I don`t have all information' => '不十分な詳細',
    'I offer price: {price}' => '私は札:{price}',
    'I send product' => '送った商品',
    'If the tasks will not be completed, or will be completed partly, you may send task for revision, or refuse further cooperation.' => '場合の作業が完了していない完成し、送信できるサービスへの変更はお断りさらに協力します。',
    'In process' => '作品の',
    'Individual' => '自然人',
    'Inform about order completion' => 'お知らせの実施順序',
    'Invoice #{number}' => '受#{number}',
    'Invoice #{number} is created' => '受#{number}の作成',
    'Invoice for order payment' => '領収書の',
    'It seems customer sent details of this request.<br> If you do not have additional questions regards task, you can start job. <br> You can also ask customer for additional information, or cancel this order.' => 'もっともお客様での仕事が決まります。 <br> すべての必要情報の提供はしており、進業務完了報告書を作成し、提出します。 <br>でも詳細を指定し、必要な場合は拒否を協力します。',
    'It seems customer sent requirements for this order. What is your decision?' => 'このバイヤーが指定された要件のためのお仕事です。 決定すか？',
    'It seems customer set weekly task. If you do not have additional questions regards task, you can start job. You can also ask customer for additional information, or cancel this order.' => 'もっともお客様の課題をします。 <br> この情報は十分に進み、作業をします。 <br>きものを明確にすべての内容（質問）からの撤退を本取引の対象となります。',
    'Job is done' => '同作業を行ってい',
    'Legal entity' => '法人',
    'Mark this order as completed when product is sent or handed over to a customer.' => 'マークをこの順にしていますので送商品は、彼の個人的ます。',
    'Needs adjustments' => '改善の必要性',
    'New response on request' => '新しい要請を受け',
    'Now customer has to write the task for next week or finish this order.' => '現在、お客様のニーズに合わせを書くの課題は来週、または終了することです。',
    'Now you can reach an agreement about shipping or details of purchase.' => '現在までに同意の内容を購入してください。',
    'Order cancelled. The money will soon be returned to the balance.' => 'のオーダーがキャンセルされます。 お金をすぐに返されます。',
    'Order details' => '注文内容に間違いがなければ',
    'Order has been created' => 'の作成',
    'Order has been created. Checkout process will be continued when we receive payment for this order.' => '順が作成されます。 チェックアウトのプロセス、その支払いを受信します。',
    'Order has been paid' => 'の注文の支払額',
    'Order has been paid. Seller will receive money when customer will receive product and confirm that it`s ok.' => 'の支給しています。 売りを受ける金銭を直ちに受領した後の商品の買付け等を行なう者および確認からの購入先をご案内いたします。',
    'Order has been paid. Seller will receive money when order will be completed and accepted by customer.' => 'の支給しています。 の受託者に金銭を直ちに終了したに受け止のお客様です。',
    'Order has been stopped' => '発注停止',
    'Order is completed by system.' => '注文完了システム',
    'Pay by Credit or Debit card' => '支払ットカードまたはデビットカード',
    'Pay with {what}' => 'を支払うための{what}',
    'Paying deposit for individual job' => '預金のためのカスタムオーダー',
    'Paying deposit for service on {site}' => '預金のためのサービス{site}',
    'Payment for service on {0} site' => 'お支払いサービスの提供{0}',
    'Payments for worker occur every week, when he completes your tasks.' => '集落の発生毎週間後に業者に委託が終了しました。',
    'Pickup' => 'ピックアップ',
    'Post Job' => '公開',
    'Post request' => '公開申請',
    'Price has been set at {price}' => 'の価格は約{price}',
    'Refuse' => 'を拒否',
    'Refuse order' => 'をただくか、注文を取り消される',
    'Require more info' => 'る追加的な情報の提供を求め',
    'Response is cancelled' => '対応を取り下げ',
    'Reviews' => 'レビュー',
    'Seller has accepted task' => 'の契約者を受け入れの仕事の',
    'Seller has additional questions regards this order' => 'の委託先がさらにつめ',
    'Seller has marked this order as completed' => '売りの注文として完成',
    'Seller marked this order as completed. Your decision?' => '売りの注文として完成します。 決定すか？',
    'Seller requested additional information' => '当該要求追加情報',
    'Seller sent product to customer' => '当該の物品、買い',
    'Seller will receive money from deposit when customer will receive product and will confirm this.' => 'の販売、金、預金、購入されていることを確認します。',
    'Send a message when you finish the order. You also may attach photos or other files, especially if it was online job.' => 'メッセージを送完成時の実行ます。 わせて着せ替えも楽しめます。画像やその他のファイルです。 ここに特に必要がある場合にproizvodilasオンラインです。',
    'Send invoice to this Email' => '請求書を送付電子メール',
    'Send message' => 'メッセージ',
    'Send product and then send message via this form. You can also attach documents about shipping.' => 'Sendの商品をメッセージを送をするようになります。 わせて着せ替えも楽しめます。に関する書類を納入します。',
    'Shipping' => '海運',
    'Solve them now' => 'を決定している',
    'Something wrong' => '異物混入など異常が認',
    'Start job' => '仕事に',
    'Task text' => 'テキストのタスク',
    'Thanks for choosing seller with our site.' => 'に選んで頂きありがとうござい売ります。',
    'Thanks for choosing worker with our site.' => 'に選んで頂きありがとうござい出演者の交流です。',
    'This order will be marked as completed in {count, plural, =0{days} one{# job} few{# days} many{# days} other{# days}}.' => 'この印をつけていき{count, plural, =0{日} one{#日} few{#日} many{#日} other{#日}}のようになります。',
    'To begin, you have to pay deposit equal to {value}, which will be paid to seller when you receive the product.' => 'の開始に協力する必要があり預金額{value}は、積算され、セラーには、きっと見つかりました。',
    'To begin, you have to pay deposit equal to {value}, which will be paid to worker when you will accept his job.' => 'の開始に協力する必要があり預金額{value}が行われる報酬は受託者が受け入れます。',
    'To continue conversation about this order click the button below' => 'への対応に伴い、上のリンクボタンをクリックし',
    'To continue, customer has to pay deposit for this request.' => '続きしないものとすることを願います。',
    'To get started, you need to make a deposit of {price}, which will be on your account until you accept the work. After that, the money will be transferred to the worker' => 'を開始する必要があり預金額{price}は、お客様のアカウントでまでまで仕事を引き受けます。 その後、送金資金が入金されるの契約者',
    'To simplify the customer search, you can create a job and more customers will see your offer' => '成功の検索お客様を作成でき、多数のお客様はお客様の提供',
    'To simplify the worker selection, you can create a request and more workers will see your request' => '成功託業者の選定と契約を作成できるチケット、多数のフリーが見えます。',
    'Total' => 'だけで',
    'Type here, what is wrong with customer`s information.' => 'からのバイオエタノールの詳細は欠からの購入先をご案内いたします。',
    'Type your message here...' => '入力メッセージが---',
    'Waiting for customer to make deposit for work' => 'までお待ちのお客様からの預金の',
    'Waiting for customer`s response' => 'の対応、お客様の',
    'Waiting for worker`s response' => 'の対応',
    'What is missing in additional details?' => 'に何が欠ける追加情報か。',
    'What is the reason of your refusal?' => '指定した理由のみでのキャンセル',
    'What is wrong with order?' => '何が問題なのでしょうか。',
    'What is wrong with product?' => '何が問題なのは、品ある。',
    'What shipping method do you prefer?' => 'どの方法で納品いたしますのでご了承ください。',
    'When are you planning to finish this order?' => 'きを完了する見込みすることにあります。',
    'Worker accepted' => 'る契約の同意',
    'Worker canceled individual job offer' => 'きゃりーぱみゅぱみゅが解除は各々の作業',
    'Worker declined' => 'る契約の拒否',
    'Worker marked this order as completed. Your decision?' => 'の契約者は、注文として完成します。 決定すか？',
    'Worker will be gaining gain money weekly, just after task is completed and accepted by customer.' => 'していますので、お受取りであり、直後に行うタスクとその受付よりお時間がかかります。',
    'Worker will gain money just after task is completed and accepted by customer.' => 'の契約者のお金を直後のタスク実行し、その受け取りのお客様',
    'You accepted' => 'に同意する',
    'You can add a comment. You also may attach photos or other files.' => 'を追加することができますコメントします。 また写真添付その他のファイルです。',
    'You can approve or reject the seller. To clarify the necessary information, we recommend using the function of sending a message.' => 'できる承認または拒否の立候補の商品です。 を明らかに必要な情報をご覧いただくことをお勧めの最初の項目を入力して送信してください。',
    'You can approve or reject the worker. To clarify the necessary information, we recommend using the function of sending a message.' => 'できる承認または拒否の立候補のアーティスト。 を明らかに必要な情報をご覧いただくことをお勧めの最初の項目を入力して送信してください。',
    'You can get your money back to use them, to buy other services or to pay for the work of another freelancer.' => 'だくことができますその後、を利用するその他のサービスの支払いのために他のアーティスト。',
    'You can post, when do you plan to pickup product from seller (day and time)' => '指定できますプランの商品については、一切責任を負わな(日時)',
    'You declined' => 'ま拒否',
    'You have to fill seller`s requirements to continue this order' => 'の質問に答える、販売業を進めました。',
    'You made deposit' => 'または預託',
    'You may accept current price, or propose your price' => 'できる受け入れの現在の価格を提供します。',
    'You may add a comment' => 'を追加することができますコメント',
    'You may post some additional information. For example: "I`am not at home on saturdays and sundays"' => 'を指定できる追加情報です。 例えば、私は家の土-日-祝日',
    'You may write a simple message, or inform customer that you have completed order' => '送信できる通常のメッセージまたはお知らせしバイヤーのためのデータ',
    'You need to accept or decline' => '必ず受け入れ継続的に見直しを行い改善に努',
    'You received offer' => 'お申し出を受けました',
    'You received request' => 'ご依頼を受けて',
    'Your Offer has been sent' => 'ご提供が送信され',
    'Your Request has been sent' => 'ご要求を送信',
    'Your deposit is expired. You have to pay new deposit for next month to continue working with this seller.' => 'お預かり金ます。 後も引き続き、委託先に必要な金の支払いを用意しております。',
    'Your invoice is created. You can download it, or send to your email' => '領収書が作成されます。 ダウンロードできますので、送信自分自身にメール',
    '{user} has sent response on request {tender}' => '{user}この要請に応じ{tender}',
    '{who} will soon start working on your order.<br/><br/>This order is expected to be completed on {date}' => '{who}の開始について(a)の順です。<br /><br />の委託先への発注は{date}',
    'Customer has provided an additional information for this order' => 'お客様の情報をこの順',
];