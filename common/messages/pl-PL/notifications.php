<?php 
 return [
    '<strong> uJobs.me </strong> is a freelance market, where you can place your services for free, get new customers or hire a freelancer for the job you need to do.' => '<strong>uJobs.me</strong> — to giełda freelancing, gdzie możesz za darmo umieścić swoje usługi, zdobyć nowych klientów lub wynająć wykonawcy na pracę, którą trzeba zrobić.',
    'Activate Your Account' => 'Aktywuj Swoje konto',
    'Additional income' => 'Dodatkowy zarobek',
    'Auto-reply' => 'Auto-odpowiedzi',
    'Change Password' => 'Zmień Hasło',
    'Check the latest offers' => 'Zobacz najnowsze propozycje',
    'Click on the link below to confirm your email and get started.' => 'Kliknij na poniższy link, aby potwierdzić Swój mail i przystąpić do pracy.',
    'Deal safety!' => 'Bezpieczeństwo transakcji!',
    'Dear {username}' => 'Kochanie, {username}',
    'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!' => 'Umiesz robić coś dobrze? Opowiedz o tym na naszej stronie internetowej, zaznacz cenę, za którą są gotowi pracować. Jesteśmy прорекламируем swoją usługę: dostaniesz nowe zamówienia i zarobić dodatkowe pieniądze!',
    'Do you need to get a job done?' => 'Trzeba by zrobili pracę?',
    'Find service that you need!' => 'Znajdź odpowiednią usługę',
    'Go to cart' => 'Przejdź do koszyka',
    'Go to view' => 'Przejdź do oglądania',
    'Greetings! You have attempted to subscribe on notifications via {provider}. Please, do this from {link}' => 'Zapraszamy! Próbujesz zapisać się na powiadomienia przez {provider}. Proszę, zrób to stąd {link}',
    'Hi, {username}' => 'Cześć, {username}',
    'Hi, {username}!' => 'Cześć, {username}!',
    'Hi, {username}, <br> There are some things you can fix on {site}!' => 'Cześć, {username}, <br> Istnieje kilka rzeczy, które możesz zrobić ze swoim profilem na uJobs!',
    'Hi, {username}, <br> Welcome to {site}!' => 'Cześć, {username}, <br> witamy na {site}',
    'Hi, {username}, Welcome to {site}!' => 'Cześć, {username}, zapraszamy na {site}!',
    'If the button above does not work, copy and paste the following URL into your browser\'s address bar.' => 'Jeśli przycisk powyżej nie działa, skopiuj i wklej następujący adres URL w pasku adresu przeglądarki.',
    'If the link above does not work, copy and paste the URL in a new browser tab. For security reasons, the link is valid for 24 hours.' => 'Jeśli link powyżej nie działa, skopiuj i wklej adres URL w nowej karcie przeglądarki. Ze względów bezpieczeństwa, link jest aktywny w ciągu 24 godzin',
    'If you have any troubles with payment, you can {contact_our_support}.' => 'Jeśli masz jakieś problemy z płatnością, można {contact_our_support}',
    'If you wish to post announcement on our site for free, please press the following button.' => 'Jeśli chcesz za darmo umieścić ogłoszenie na naszej stronie internetowej, należy nacisnąć przycisk',
    'Invitation to register on {site}' => 'Zaproszenie zarejestrować się na {site}',
    'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).' => 'Również dobry pomysł, aby dodać {email} do Swojej książki adresowej, aby otrzymywać nasze wiadomości (bez spamu, obiecujemy!)',
    'It is never been so easy to get it done' => 'Nasz serwis łączy Zleceniodawców i Wykonawców różnych usług',
    'It is really that simple.' => 'Zapewniamy bezpieczeństwo, Masz gwarancję, że otrzymasz zapłatę za wykonane zamówienie.',
    'It seems your photo and profile description are not set. Please fix this!' => 'Wygląda na to, że nie zainstalowałeś zdjęcie i nie wypełniali opis swojego profilu. Proszę, popraw to!',
    'It seems your profile description is not set. Please fix this!' => 'Wygląda na to, że nie wypełnili opis profilu. Proszę, popraw to!',
    'It seems your profile photo is not set. Please fix this!' => 'Wygląda na to, że nie zainstalowałeś zdjęcie profilu. Proszę, popraw to!',
    'Join Now' => 'Dołącz teraz',
    'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.' => 'Można wybrać wszystkie rodzaje prac, począwszy od pomocy w domu do budowy strony internetowej lub prowadzenia firm reklamowych. Połączyliśmy online i offline freelancerów, tworząc najbardziej wygodną platformę do wyszukiwania wykonawców!',
    'Looking for service or order' => 'Wyszukaj usługi lub wykonawcy',
    'New Message' => 'Nowa wiadomość',
    'New inbox message on {site}' => 'Nowa wiadomość na {site}',
    'New request on service' => 'Nowa oferta na usługę',
    'Notification from {site}' => 'Zawiadomienie z {site}',
    'Or you can unsubscribe from email messages by this link {link}).' => 'Czy można zrezygnować z otrzymywania E-mail korespondencji na ten link {link}',
    'Order #{number} cancelled. The money will soon be returned to the balance.' => 'Zamówienie #{number} odwołany. Pieniądze szybko trafią na równowagę.',
    'Order #{number} is automatically completed after {days, plural, one{# day} other{# days}} passed from delivery.' => 'Zamówienie #{number} automatycznie zakończony po upływie {days, plural, one{# dzień} few{# dnia} other{# dni}} od daty dostawy.',
    'Order for {price}' => 'Zamówić za {price}',
    'Post announcement about selling services or products on uJobs.me for free' => 'Zamieścić za darmo swoje ogłoszenie na giełdzie uJobs o sprzedaży towarów lub usług',
    'Post for free' => 'Umieścić za darmo',
    'Press button to return to cart' => 'Naciśnij, aby powrócić do koszyka',
    'Ready to get started?' => 'Gotowy, aby zacząć zarabiać?',
    'Recommendations about your profile' => 'Zalecenia dotyczące twojego profilu',
    'Sounds too good to be true? See for yourself!' => 'Brzmi zbyt dobrze, aby mogło być prawdziwe? Zobaczcie sami!',
    'Start Earning' => 'Zacznij zarabiać',
    'Thanks for registration on our portal!' => 'Dziękujemy za rejestrację na naszej stronie!',
    'Thanks for using our service! We offer you similar services you may be interested in:' => 'Dziękuję za pracę z naszym serwisem! Oferujemy państwu podobne usługi, które mogą cię zainteresować:',
    'Thanks for your message. We will contact you as soon as possible!' => 'Dziękuję za wiadomość. Skontaktujemy się z tobą w najbliższym czasie!',
    'The uJobs Team' => 'Zespół uJobs',
    'These jobs are missing their images. Please, add them!' => 'W tej pracy brak obrazu. Proszę, dodaj je!',
    'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}' => 'Ten Email został wysłany przez firmę uJobs.me.',
    'To start the process, please click the following link:<br>{link}' => 'Aby rozpocząć, proszę, kliknij poniższy link:<br>{link}',
    'User {user} has posted new article' => 'Użytkownik {user} opublikował nowy artykuł',
    'User {user} has posted new job request' => 'Użytkownik {user} opublikował nową ofertę na usługę',
    'User {user} has posted new portfolio album' => 'Użytkownik {user} opublikował nowy album portfolio',
    'User {user} has posted new product' => 'Użytkownik {user} opublikował nowy towar',
    'User {user} has posted new product request' => 'Użytkownik {user} opublikował nową aplikację na towar',
    'User {user} has posted new service' => 'Użytkownik {user} opublikował nową usługę',
    'User {user} likes your portfolio album' => 'Użytkownik {user} ocenił swój album portfolio',
    'User {user} shared with you a link on {site} site' => 'Użytkownik {user} podzielił się z wami linkiem na stronie {site}',
    'User {user} wants to add you to friend list' => 'Użytkownik {user} chce dodać cię do znajomych',
    'View and respond' => 'Zobaczyć i odpowiedzieć',
    'View by link: {link}' => 'Zobacz link: {link}',
    'Visit Website' => 'Odwiedź stronę',
    'Visit profile settings' => 'Odwiedź edycja',
    'We got a request to reset your uJobs password.' => 'Otrzymaliśmy prośbę o zresetowanie hasła z konta uJobs',
    'Welcome to {site}' => 'Witamy na {site}',
    'Which you can change in your account settings' => 'Który można zmienić w sekcji Ustawienia/Zarządzanie gabinetem',
    'You are welcome on {site}' => 'Zapraszamy na {site}',
    'You did order {title} recently.' => 'Ostatnio zamówiliśmy {title}',
    'You have accepted friendship' => 'Przyjęliście zaproszenie',
    'You have accepted invitation' => 'Przyjęliście zaproszenie',
    'You have been invited to join company «{company}» by user {user}' => 'Użytkownik {user} zaprosił cię do firmy "{company}"',
    'You have created new order for user {user}' => 'Stworzyliśmy nowe zamówienie dla użytkownika {user}',
    'You have declined friendship' => 'Nie przyjął pan zaproszenie',
    'You have declined invitation' => 'Nie przyjął pan zaproszenie',
    'You have just become part of the largest, most affordable and easiest to use marketplace for services and products!' => 'Dopiero co się zarejestrowałeś na stronie uJobs.me i członkiem wielkiej społeczności profesjonalistów!',
    'You have left response on request {tender}' => 'Pani odpowiedziała na wniosek {tender}',
    'You have new inbox message from user {user}' => 'Masz nową wiadomość od użytkownika {user}',
    'You have new notification from {site}' => 'Masz nowe powiadomienie na miejscu {site}',
    'You have new order from user {user}' => 'Masz nowe zamówienie od użytkownika {user}',
    'You have new response on request {tender} from {user}' => 'Masz nową odpowiedź na wniosek {tender} od użytkownika {user}',
    'You have not set your skills for your profile. You can do it now!' => 'Jeszcze nie wypełnili umiejętności w swoim profilu. Możesz zrobić to teraz!',
    'You have subscribed on notifications via {provider}' => 'Zapisałeś się na powiadomienia przez {provider}',
    'You have unsubscribed from notifications via {provider}, but it`s never too late to come back :)' => 'Można niesubskrybowane od powiadomienia przez {provider}, ale nigdy nie jest za późno wrócić :)',
    'You still have not added jobs. You can do it now!' => 'Jeszcze nie dodał żadnej pracy. Możesz zrobić to teraz!',
    'You still have services in cart that are awaiting for payment. You wanted to buy: {items}' => 'W twoim koszyku zostały opłacone usługi. Chcesz kupić: {items}',
    'Your article has passed moderation and has been added to catalog.' => 'Twój artykuł przeszedł moderowane i publikowane w katalogu.',
    'Your article has passed moderation on {site}' => 'Twój artykuł przeszedł moderowane na {site}',
    'Your article {article} has passed moderation and is published in catalog' => 'Twój artykuł {article} przeszedł moderowane i publikowane w katalogu',
    'Your job has passed moderation and has been added to catalog. If you want more chances to be found and receive orders, you can post another job.' => 'Twoja praca przeszedł moderowane i publikowane w katalogu. Jeśli chcesz zwiększyć szanse na uzyskanie zamówienia, można złożyć inne usługi.',
    'Your job has passed moderation on {site}' => 'Twoja praca przeszedł moderowane na {site}',
    'Your job {job} has passed moderation and is published in catalog' => 'Twoja usługa {job} przeszedł moderowane i publikowane w katalogu',
    'Your portfolio album has passed moderation and has been added to catalog.' => 'Twój album portfolio przeszedł moderowane i jest umieszczony w katalogu',
    'Your portfolio album has passed moderation on {site}' => 'Twój album portfolio przeszedł moderowane na {site}',
    'Your portfolio album {portfolio} has passed moderation and is published in catalog' => 'Twój album portfolio {portfolio} przeszedł moderowane i publikowane w katalogu',
    'Your product has passed moderation and has been added to catalog.' => 'Produkt przeszedł moderowane i jest umieszczony w katalogu.',
    'Your product has passed moderation on {site}' => 'Produkt przeszedł moderowane na {site}',
    'Your product request has passed moderation and has been added to catalog.' => 'Zgłoszenie na towar przeszedł moderowane i dodany do katalogu.',
    'Your product request has passed moderation on {site}' => 'Zgłoszenie na towar przeszedł moderowane i publikowane w witrynie {site}',
    'Your product request {product} has passed moderation and is published in catalog' => 'Zgłoszenie na towar {product} przeszedł moderowane i publikowane w katalogu',
    'Your product {product} has passed moderation and is published in catalog' => 'Twój towar {product} przeszedł moderowane i jest umieszczony w katalogu',
    'Your profile has passed moderation and has been added to professional`s catalog. If you want more chances to be found and receive orders, please post your jobs.' => 'Twój profil przeszedł moderowane i publikowane w katalogu. Jeśli chcesz zwiększyć szanse na uzyskanie zamówienia, umieść Swoje usługi.',
    'Your profile has passed moderation on {site}' => 'Twój profil przeszedł moderowane na {site}',
    'Your request has passed moderation and has been added to catalog.' => 'Wniosek przeszedł moderowane i publikowane w katalogu.',
    'Your request has passed moderation on {site}' => 'Wniosek przeszedł moderowane na {site}',
    'Your request {job} has passed moderation and is published in catalog' => 'Zgłoszenie {job} przeszedł moderowane i publikowane w katalogu',
    'Your transfer confirmation code: {code}' => 'Twój kod potwierdzenia do tłumaczenia: {code}',
    'contact our support' => 'skontaktuj się z naszym biurem obsługi klienta',
    'uJobs is an online marketplace for services where you can find <br>everything you need, and in unrivaled quality.' => 'uJobs — online rynek dla usług, których możesz <br>znajdź wszystko , czego potrzebujesz, i w doskonałej jakości.',
    '{0} is tired of keeping uJobs in secret.' => '{0} zmęczony utrzymać uJobs w tajemnicy',
    '{sender} left you inbox message' => 'Użytkownik {sender} wysłałem ci wiadomość',
    '{sender} sent you individual job offer' => '{sender} wysłał(a) ofertę pracy indywidualnej',
    '{sender} sent you individual job request' => '{sender} wysłał(a) do ciebie zapytanie o indywidualną pracę',
    '{user} has updated order {order}' => '{user} zaktualizował zamówienie {order}',
    '{user} sent you invitation to join uJobs.' => '{user} wysłał ci zaproszenie do przyłączenia się do uJobs.',
    'Greetings!' => 'Witamy!',
    'Invitation to {site}' => 'Zaproszenie na {site}',
    'On {site} you can find a lot of services for construction or buy a ready property' => 'Na {site} można znaleźć wiele usług w zakresie budowy lub kupić gotową nieruchomości',
];