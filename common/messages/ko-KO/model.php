<?php 
 return [
    'Account Support' => '정 지원',
    'Account replenishment' => '리필',
    'Active' => '활성',
    'Additional Days' => '추가적인 일',
    'Additional Information' => '추가 정보',
    'Additional info' => '추가 정보',
    'Address' => '주소',
    'Admin comment' => '관리자 설명',
    'Advertisement' => '광고',
    'Album Title' => '이름 앨범의',
    'Alias' => 'Alias',
    'All' => '모든',
    'All views' => '전체보기',
    'Amount' => '금액',
    'Answer' => '응답',
    'Anytime' => '언제든지',
    'Archived' => '아카이브',
    'Are you sure you want to delete this item?' => 'Are you sure you want 을 제거하는 이 품목은?',
    'Attachments' => '첨부 파일',
    'Attribute' => '특성',
    'Attribute ID' => 'ID 특성',
    'Attribute set' => '설정의 특성',
    'Average execution time' => '의 평균 시간',
    'Balance' => '균형',
    'Balance Change' => '에서 변경 균형',
    'Bank SWIFT-Code' => 'SWIFT code 의 은행',
    'Bank name' => '은행 이름',
    'Banner' => '배너',
    'Banner mobile' => '배너 모바일',
    'Belongs to category' => '그리고 카테고리',
    'Canceled' => '취소',
    'Card Number' => '카드 번호',
    'Category' => '카테고리',
    'Category ID' => '의 ID 카테고리',
    'Category template' => '템플릿 카테고리',
    'City' => '도시',
    'Code' => '코드',
    'Comment' => 'Comment',
    'Company Address' => '회사 주소',
    'Company Banner' => '배너 회사',
    'Company Description' => '회사명',
    'Company Logo' => '회사의 로고',
    'Company Name' => '회사 이름',
    'Company status text' => '의 상태를 회사',
    'Completion Date' => '완료 날짜',
    'Completion time' => '완료 시간',
    'Confirm Password' => '비밀번호 확인',
    'Confirmed' => '확인',
    'Content' => '설명',
    'Content Preview/Subtitle' => '자막',
    'Conversation ID' => 'ID dialogue',
    'Cost' => '비용',
    'Country' => '국가',
    'Country ID' => '국가',
    'Country Title' => '국가',
    'Create' => '을 만들기',
    'Create User Withdrawal' => '을 만들 탈퇴를 요청의 돈을',
    'Created At' => '생성 날짜',
    'Created By' => '성',
    'Currency Code' => '통화 코드',
    'Currency for your prices' => '통화에 대한 가격',
    'Customer' => '구매자',
    'Datetime' => '날짜와 시간',
    'Day' => '일',
    'Degree' => '도',
    'Delete' => '를 제거',
    'Deleted' => '삭제',
    'Description' => '설명',
    'Duration' => '기간',
    'Excluded' => '함',
    'Execution time' => '실행 시간',
    'Expiration Date' => '의 유효성',
    'Expires At' => 'Expires',
    'Feature Support' => '능 지원',
    'Firstname' => '이름',
    'From ID' => '보낸 사람 ID',
    'House' => '하우스',
    'Housing' => '케이스',
    'ID' => 'ID',
    'INN' => '인',
    'Image' => '이미지',
    'Images' => '이미지',
    'Included' => '포함',
    'Invitation sent' => '초대장 발송',
    'Invoice' => '계정',
    'Invoice File' => '파일이 계정',
    'Invoice ID' => '송장 ID',
    'Is Approved' => '승인',
    'Is Enabled' => '포함',
    'Is Text' => '텍스트',
    'Is active' => '적극적으로',
    'Is moderated' => '을 검토',
    'Is visible' => '명',
    'Job ID' => 'ID works',
    'Job Support' => '지원',
    'Language' => '언어',
    'Lastname' => '성',
    'Layout' => '프레임',
    'Level' => '준',
    'Lm views' => '타 지난 달',
    'Locale' => '로캘',
    'M views' => '타 이달',
    'Made purchase' => '구매가되었다',
    'Mail Sent' => '편지를 보냈',
    'Measure unit' => '단위',
    'Message' => '메시지',
    'Message ID' => '메시지-ID',
    'Middlename' => '아버지의 이름을 딴',
    'Month' => '달',
    'Name' => '이름',
    'New Password' => '새로운 비밀번호',
    'Next Step ID' => 'ID 다음 단계',
    'OGRN' => 'PPC',
    'Old Password' => '비밀번호',
    'Order' => '주문',
    'Order Date' => '주문 날짜',
    'Order ID' => '주문 ID',
    'Order Support' => '지원 주문',
    'Page type' => '페이지',
    'Pages template' => '페이지 템플릿',
    'Parent ID' => '부모 ID',
    'Parent category' => '상위 카테고리',
    'Payment ID' => 'ID 의 결제',
    'Payment method' => '결제 방법',
    'Payment system' => '결제 시스템',
    'Payments' => '결제',
    'Pending' => '리',
    'Per Page' => '페이지 요소에',
    'Percent bonus' => '보너스 관심사',
    'Phone' => '전화',
    'Photo' => '사진',
    'Previous Step ID' => 'ID 의 이전 단계',
    'Price' => '가격',
    'Product' => '제품',
    'Product ID' => 'Item ID',
    'Publish date' => '발행일',
    'Published' => '출판(전달되는 절도)',
    'Quantity' => '수',
    'Question' => '질문',
    'Rating' => '등급',
    'Read' => '읽기',
    'Reason Additional' => '추가적인 이유',
    'Reason ID' => 'ID 는 이유',
    'Receiver name' => '받는 사람의 이름을',
    'Receiver wallet id' => 'ID 받는 사람의 지갑',
    'Referral_id' => '추천인 ID',
    'Refund for canceled order' => '에 대한 환불을 취소 순서',
    'Registered' => '등록',
    'Report a Bug' => '보고서의 오류',
    'Request' => '응용 프로그램',
    'Requirement' => '구',
    'Requirements' => '요구사항',
    'Resolved' => '해결',
    'Response' => '응답',
    'Role' => '역할',
    'Root' => '루트',
    'Salary' => '급여',
    'Seller' => '판매자',
    'Sender' => 'Sender',
    'Sender wallet id' => 'ID 의 지갑을 보낸 사람의',
    'Seo Description' => 'Seo 설명',
    'Seo Keywords' => 'Seo 키워드',
    'Seo Robots' => 'Seo 로봇',
    'Seo Title' => 'SEO 제목',
    'Service' => '서비스',
    'Short Description' => '간단한 설명',
    'Site' => '웹사이트',
    'Skills' => '기술',
    'Skype' => 'Skype',
    'Something about you' => '조금은 당신에 대해',
    'Something wrong with this email' => '이메일 뭔가 잘못된',
    'Sort Field' => '분야에 의하여 종류',
    'Sort Order' => '정렬 순서',
    'Specialization' => '전문성',
    'Specialties' => '전문',
    'Specialty' => '전문',
    'Starred' => '즐겨찾기',
    'State/Region' => 'State/Province',
    'Status' => '상태',
    'Step ID' => 'ID 단계',
    'Subject' => '테마',
    'Summary' => '총',
    'Tags' => '태그',
    'Target Account' => '대상 계정',
    'Target Method' => '대상 방법',
    'Tariff ID' => 'ID 평가',
    'Template' => '템플릿',
    'Text' => '텍스트',
    'Title' => '이름',
    'To ID' => '의 ID 를 받는 사람',
    'Token' => '토큰',
    'Total' => '금액',
    'Total bonus' => '보너스 금액',
    'Transfer amount' => '의 양 전송',
    'Tree ID' => '의 ID 나무',
    'Type' => '유형',
    'Type not selected' => '입력하지 않 선택',
    'Unknown filter' => '알 수 없는 필터',
    'Unknown status' => '알 수 없는 상태',
    'Unread' => '읽지 않은',
    'Update' => '업데이트',
    'Update date' => '업데이트',
    'Update {modelClass}: ' => '를 업데이트{modelClass}:',
    'Updated At' => '날짜 변경',
    'Updated By' => '사람이 업데이트',
    'Url' => 'Url',
    'User' => '사용자',
    'User ID' => '사용자',
    'User Portfolio ID' => 'ID 포트폴리오',
    'User Withdrawals' => '요청을 철회',
    'Value' => '값',
    'Value Alias' => 'Alias 값',
    'Verify code' => '확인 코드',
    'Video' => '동영상',
    'Viewed at' => '날짜 액세스',
    'Wallet number' => '계좌 번호',
    'Year' => '년도',
    'Year End' => '졸업년도',
    'Year Start' => '올해의 시작',
    'Your Email' => '이메일',
    'Your Name' => '이름',
    'Zip Code' => '우편 번호',
    'First Name' => '이름',
    'Last Name' => '성',
    'Mortgage' => '담보',
    'Additional information' => '추가 정보',
    'Houses' => '홈',
    'Execution time (days)' => '실행 시간(일)',
];