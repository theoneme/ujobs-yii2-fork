<?php 
 return [
    'Active' => '활성',
    'Administrator' => '관리자',
    'All' => '모든',
    'Approved' => '승인',
    'Awaiting Moderation' => '검토를 기다리',
    'Awaiting My Review' => '기다리는 내 생각',
    'Awaiting Payment' => '을 기다리는 결제',
    'Awaiting Response' => '응답을 기다리',
    'Awaiting for Review' => '을 기다리고 피드백',
    'Awaiting new task' => '을 기다리고 새로운 작업',
    'Awaiting task approve' => '승인이 보류 중인의 작업',
    'Basic' => '기본',
    'Cancelled' => '취소',
    'Company owner' => '회사의 소유자',
    'Completed' => '완료',
    'Content manager' => '콘텐츠 관리',
    'Conversational' => '말',
    'Credit Card' => '신용 카드',
    'Declined' => '거',
    'Delete' => '를 제거',
    'Deleted' => '삭제',
    'Delivered' => '배달',
    'Delivery <br> Submitted' => '주문 <br> 배달',
    'Denied' => '부',
    'Details <br> Specified' => '상세정보 <br> 나',
    'Disabled' => '장애인',
    'Done' => '완료',
    'Draft' => '초안',
    'Drafts' => '초안',
    'Fluent' => '무료',
    'Freelancer <br> is found' => '계약자 <br> 발견',
    'Freelancer <br> is not found' => '계약자 <br> 발견되지 않았',
    'I responded on requests' => '내가 반응을 요청',
    'Individual' => '자연인',
    'Invited' => '초청',
    'Language' => '언어',
    'Legal entity' => '법인',
    'Minimize' => '축소',
    'Missing Details' => '필요한 설명',
    'Month paid' => '개월 지급',
    'Native' => 'Native',
    'Not worker' => '지원',
    'Notifications' => '알',
    'Offline' => '오프라인',
    'Online' => '온라인',
    'Order <br> is not paid' => '주문 <br> 지불하지 않은',
    'Order <br> is paid' => '주문 <br> 지불',
    'Order Completed' => '주문을 완료',
    'Paid' => '지불',
    'Paused' => '일시 중단',
    'Pending' => '리',
    'Portfolio is empty' => '빈 포트폴리오',
    'Price <br> approved' => '가격 <br> 설치',
    'Product <br> delivered' => '제품 <br> 배달',
    'Product not found' => '항목은 발견되지 않았',
    'Published' => '게시',
    'Published for all' => '된 모든',
    'Published for subscribers' => '출판을 위한 구내구독자',
    'Qiwi Wallet' => 'Kiwi 지갑',
    'Refused' => '거',
    'Request <br> paid' => '응용 프로그램 <br> 지불',
    'Requires Details' => '필요한 설명',
    'Requires moderation' => '검토를 기다리',
    'Requires modification' => '예상 변경',
    'Responses on my requests' => '응답 내 응용 프로그램',
    'Save' => '저장',
    'Seller <br> is found' => '판매자 <br> 발견',
    'Seller <br> is not found' => '판매자 <br> 발견되지 않았',
    'Sent' => '송',
    'Service <br> delivered' => '서비스 <br> 배달',
    'Service not found' => '서비스 발견되지 않았',
    'Support service' => '지원',
    'Trading. The customer`s response is awaited' => '등이 있습니다. 응답을 보류 중인 고객',
    'Trading. The performer`s response is awaited' => '등이 있습니다. 응답을 기다리에',
    'Unknown role' => '알 수 없는 역할',
    'Unknown status' => '알 수 없는 상태',
    'Unknown type' => '알 수 없음',
    'Unspecified' => '정의되지 않았',
    'Waiting' => '리',
    'Waiting for shipping decision' => '을 기다리고 배달',
    'Webmoney Wallet' => '웹머니 지갑',
    'Yandex Wallet' => 'Yandex 지갑',
];