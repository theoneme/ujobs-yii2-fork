<?php

return [
    'Active' => 'Активен',
    'Administrator' => 'Администратор',
    'Alipay' => '',
    'All' => 'Все',
    'Approved' => 'Одобрен',
    'Awaiting Moderation' => 'Ожидает модерации',
    'Awaiting My Review' => 'Ожидает моего отзыва',
    'Awaiting Payment' => 'Ожидает оплаты',
    'Awaiting Response' => 'Ожидает ответа',
    'Awaiting for Review' => 'Ожидает отзыва',
    'Awaiting new task' => 'Ожидает нового задания',
    'Awaiting task approve' => 'Ожидает одобрения задания',
    'Basic' => 'Базовый',
    'BillDesk' => '',
    'Cancelled' => 'Отменен',
    'Company owner' => 'Владелец компании',
    'Completed' => 'Завершен',
    'Content manager' => 'Контент-менеджер',
    'Conversational' => 'Разговорный',
    'Credit Card' => 'Кредитная карта',
    'Declined' => 'Отклонен',
    'Delete' => 'Удалить',
    'Deleted' => 'Удален',
    'Delivered' => 'Доставлен',
    'Delivery <br> Submitted' => 'Заказ <br> доставлен',
    'Denied' => 'Отказано',
    'Details <br> Specified' => 'Детали <br> указаны',
    'Disabled' => 'Отключен',
    'Done' => 'Завершен',
    'Draft' => 'Черновик',
    'Drafts' => 'Черновики',
    'Fluent' => 'Свободный',
    'Freelancer <br> is found' => 'Исполнитель <br> найден',
    'Freelancer <br> is not found' => 'Исполнитель <br> не найден',
    'I responded on requests' => 'Я откликнулся на заявки',
    'Individual' => 'Физическое лицо',
    'Invited' => 'Приглашен',
    'Itaú Unibanco' => '',
    'Language' => 'Язык',
    'Legal entity' => 'Юридическое лицо',
    'Minimize' => 'Свернуть',
    'Missing Details' => 'Необходимы уточнения',
    'MoneyGram' => '',
    'Month paid' => 'Месяц оплачен',
    'Native' => 'Родной',
    'Not worker' => 'Не работник',
    'Notifications' => 'Уведомления',
    'Offline' => 'Оффлайн',
    'Online' => 'Онлайн',
    'Order <br> is not paid' => 'Заказ <br> не оплачен',
    'Order <br> is paid' => 'Заказ <br> оплачен',
    'Order Completed' => 'Заказ завершен',
    'Paid' => 'Оплачен',
    'Paused' => 'Приостановлен',
    'PayPal' => '',
    'Pending' => 'Ожидает',
    'Portfolio is empty' => 'Портфолио пустое',
    'Price <br> approved' => 'Цена <br> установлена',
    'Product <br> delivered' => 'Товар <br> доставлен',
    'Product not found' => 'Товар не найден',
    'Published' => 'Опубликован',
    'Published for all' => 'Опубликован для всех',
    'Published for subscribers' => 'Опубликован для подписчиков',
    'Qiwi Wallet' => 'Киви кошелек',
    'Refused' => 'Отклонен',
    'Request <br> paid' => 'Заявка <br> оплачена',
    'Requires Details' => 'Необходимы уточнения',
    'Requires moderation' => 'Ожидает модерации',
    'Requires modification' => 'Ожидает изменения',
    'Responses on my requests' => 'Отклики на мои заявки',
    'SWIFT' => '',
    'Save' => 'Сохранить',
    'Seller <br> is found' => 'Продавец <br> найден',
    'Seller <br> is not found' => 'Продавец <br> не найден',
    'Sent' => 'Отправлено',
    'Service <br> delivered' => 'Услуга <br> доставлена',
    'Service not found' => 'Услуга не найдена',
    'Stripe' => '',
    'Support service' => 'Служба поддержки',
    'Trading. The customer`s response is awaited' => 'Торг. Ожидается ответ заказчика',
    'Trading. The performer`s response is awaited' => 'Торг. Ожидается ответ исполнителя',
    'UnionPay Online Payments' => '',
    'Unknown role' => 'Неизвестная роль',
    'Unknown status' => 'Неизвестный статус',
    'Unknown type' => 'Неизвестный тип',
    'Unspecified' => 'Не определено',
    'Waiting' => 'Ожидает',
    'Waiting for shipping decision' => 'Ожидает решения по доставке',
    'WeChat Pay' => '',
    'Webmoney Wallet' => 'Webmoney кошелек',
    'Yandex Wallet' => 'Яндекс кошелек',
];
