<?php

return [
    'Account Support' => 'Поддержка Учетной Записи',
    'Account replenishment' => 'Пополнение счёта',
    'Active' => 'Активный',
    'Additional Days' => 'Дополнительные дни',
    'Additional Information' => 'Дополнительная информация',
    'Additional info' => 'Дополнительная информация',
    'Address' => 'Адрес',
    'Admin comment' => 'Комментарий администратора',
    'Advertisement' => 'Реклама',
    'Album Title' => 'Название альбома',
    'Alias' => 'Alias',
    'All' => 'Все',
    'All views' => 'Всего просмотров',
    'Amount' => 'Сумма',
    'Answer' => 'Ответ',
    'Anytime' => 'В любое время',
    'Archived' => 'Архив',
    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить этот элемент?',
    'Attachments' => 'Прикреплённые файлы',
    'Attribute' => 'Атрибут',
    'Attribute ID' => 'ID атрибута',
    'Attribute set' => 'Набор атрибутов',
    'Average execution time' => 'Среднее время выполнения',
    'Balance' => 'Баланс',
    'Balance Change' => 'Изменение баланса',
    'Bank SWIFT-Code' => 'SWIFT-код банка',
    'Bank name' => 'Название банка',
    'Banner' => 'Баннер',
    'Banner mobile' => 'Баннер для мобильного',
    'Belongs to category' => 'Принадлежит категории',
    'Canceled' => 'Отменён',
    'Card Number' => 'Номер карты',
    'Category' => 'Категория',
    'Category ID' => 'ID категории',
    'Category template' => 'Шаблон категории',
    'City' => 'Город',
    'Code' => 'Код',
    'Comment' => 'Комментарий',
    'Company Address' => 'Адрес компании',
    'Company Banner' => 'Баннер компании',
    'Company Description' => 'Описание компании',
    'Company ID' => '',
    'Company Logo' => 'Лого компании',
    'Company Name' => 'Название компании',
    'Company status text' => 'Статус компании',
    'Completion Date' => 'Дата завершения',
    'Completion time' => 'Время завершения',
    'Confirm Password' => 'Подтвердите пароль',
    'Confirmed' => 'Подтвержден',
    'Content' => 'Описание',
    'Content Preview/Subtitle' => 'Подзаголовок',
    'Conversation ID' => 'ID диалога',
    'Cost' => 'Стоимость',
    'Country' => 'Страна',
    'Country ID' => 'Страна',
    'Country Title' => 'Страна',
    'Create' => 'Создать',
    'Create User Withdrawal' => 'Создать запрос на вывод денег',
    'Created At' => 'Дата создания',
    'Created By' => 'Создано',
    'Currency Code' => 'Код валюты',
    'Currency for your prices' => 'Валюта для цены',
    'Customer' => 'Покупатель',
    'Data' => '',
    'Datetime' => 'Дата и время',
    'Day' => 'День',
    'Degree' => 'Степень',
    'Delete' => 'Удалить',
    'Deleted' => 'Удалённые',
    'Description' => 'Описание',
    'Duration' => 'Длительность',
    'Email' => '',
    'Entity' => '',
    'Entity Alias' => '',
    'Entity Content' => '',
    'Entity ID' => '',
    'Excluded' => 'Исключено',
    'Execution time' => 'Время выполнения',
    'Expiration Date' => 'Срок действия',
    'Expires At' => 'Истекает',
    'Feature Support' => 'Функциональная поддержка',
    'First User ID' => '',
    'Firstname' => 'Имя',
    'From ID' => 'ID отправителя',
    'House' => 'Дом',
    'Housing' => 'Корпус',
    'ID' => 'ID',
    'INN' => 'ИНН',
    'Image' => 'Изображение',
    'Images' => 'Изображения',
    'Included' => 'Включено',
    'Invitation sent' => 'Приглашение отправлено',
    'Invoice' => 'Счет',
    'Invoice File' => 'Файл счета',
    'Invoice ID' => 'ID счета',
    'Is Approved' => 'Одобрено',
    'Is Enabled' => 'Включено',
    'Is Required' => '',
    'Is Text' => 'Текстовый',
    'Is active' => 'Активно',
    'Is moderated' => 'Отмодерировано',
    'Is visible' => 'Видимо',
    'Job ID' => 'ID работы',
    'Job Support' => 'Поддержка Работ',
    'Key' => '',
    'Language' => 'Язык',
    'Lastname' => 'Фамилия',
    'Lat' => '',
    'Layout' => 'Каркас',
    'Level' => 'Уровень',
    'Lm views' => 'Просмотров в прошлом месяце',
    'Locale' => 'Локаль',
    'Logo' => '',
    'Long' => '',
    'M views' => 'Просмотров в текущем месяце',
    'Made purchase' => 'Совершил покупку',
    'Mail Sent' => 'Письмо отправлено',
    'Measure ID' => '',
    'Measure unit' => 'Единица измерения',
    'Message' => 'Сообщение',
    'Message ID' => 'ID сообщения',
    'Method ID' => '',
    'Middlename' => 'Отчество',
    'Month' => 'Месяц',
    'Name' => 'Имя',
    'New Password' => 'Новый пароль',
    'Next Step ID' => 'ID следующего шага',
    'OGRN' => 'КПП',
    'Old Password' => 'Старый пароль',
    'Order' => 'Заказы',
    'Order Date' => 'Дата заказа',
    'Order ID' => 'ID заказа',
    'Order Support' => 'Поддержка Заказов',
    'Page ID' => '',
    'Page type' => 'Тип страницы',
    'Pages template' => 'Шаблон страниц',
    'Parent ID' => 'ID родителя',
    'Parent category' => 'Родительская категория',
    'Payment ID' => 'ID платежа',
    'Payment method' => 'Способ оплаты',
    'Payment system' => 'Платёжная система',
    'Payments' => 'Платежи',
    'Pending' => 'В ожидании',
    'Per Page' => 'Элементов на страницу',
    'Percent bonus' => 'Бонусный процент',
    'Phone' => 'Телефон',
    'Photo' => 'Фото',
    'Previous Step ID' => 'ID предыдущего шага',
    'Price' => 'Цена',
    'Product' => 'Товар',
    'Product ID' => 'ID товара',
    'Publish date' => 'Дата публикации',
    'Published' => 'Опубликован (прошел модерацию)',
    'Quantity' => 'Количество',
    'Question' => 'Вопрос',
    'Rating' => 'Рейтинг',
    'Read' => 'Прочитанные',
    'Reason Additional' => 'Дополнительная причина',
    'Reason ID' => 'ID причины',
    'Receiver name' => 'Имя получателя',
    'Receiver wallet id' => 'ID кошелька получателя',
    'Referral_id' => 'ID реферала',
    'Refund for canceled order' => 'Возврат средств за отменённый заказ',
    'Registered' => 'Зарегистрирован',
    'Report a Bug' => 'Сообщить об ошибке',
    'Request' => 'Заявка',
    'Requirement' => 'Требование',
    'Requirements' => 'Требования',
    'Requirements type' => '',
    'Resolved' => 'Решён',
    'Response' => 'Отклик',
    'Role' => 'Роль',
    'Root' => 'Корень',
    'Salary' => 'Зарплата',
    'Second User ID' => '',
    'Seller' => 'Продавец',
    'Sender' => 'Отправитель',
    'Sender wallet id' => 'ID кошелька отправителя',
    'Seo Description' => 'Seo Description',
    'Seo Keywords' => 'Seo Keywords',
    'Seo Robots' => 'Seo Robots',
    'Seo Title' => 'SEO заголовок',
    'Service' => 'Услуга',
    'Short Description' => 'Краткое описание',
    'Site' => 'Сайт',
    'Skills' => 'Навыки',
    'Skype' => 'Skype',
    'Something about you' => 'Немного о вас',
    'Something wrong with this email' => 'С этим Email что-то не так',
    'Sort Field' => 'Поле для сортировки',
    'Sort Order' => 'Порядок сортировки',
    'Specialization' => 'Специализация',
    'Specialties' => 'Специальность',
    'Specialty' => 'Специальность',
    'Starred' => 'Избранные',
    'State/Region' => 'Штат/Область',
    'Status' => 'Статус',
    'Step ID' => 'ID шага',
    'Subject' => 'Тема',
    'Summary' => 'Итого',
    'Tags' => 'Теги',
    'Target Account' => 'Целевая учетная запись',
    'Target Method' => 'Целевой метод',
    'Tariff ID' => 'ID тарифа',
    'Template' => 'Шаблон',
    'Text' => 'Текст',
    'Title' => 'Название',
    'To ID' => 'ID получателя',
    'Token' => 'Токен',
    'Total' => 'Сумма',
    'Total bonus' => 'Сумма бонуса',
    'Transfer amount' => 'Сумма перевода',
    'Tree ID' => 'ID дерева',
    'Type' => 'Тип',
    'Type not selected' => 'Тип не выбран',
    'Unknown filter' => 'Неизвестный фильтр',
    'Unknown status' => 'Неизвестный статус',
    'Unread' => 'Непрочитанные',
    'Update' => 'Обновить',
    'Update date' => 'Дата обновления',
    'Update {modelClass}: ' => 'Обновить {modelClass}:',
    'Updated At' => 'Дата изменения',
    'Updated By' => 'Кем обновлено',
    'Url' => 'Url',
    'User' => 'Пользователь',
    'User ID' => 'Пользователь',
    'User Portfolio ID' => 'ID портфолио',
    'User Withdrawals' => 'Запросы вывода денег',
    'Value' => 'Значение',
    'Value Alias' => 'Alias значения',
    'Verify code' => 'Код подтверждения',
    'Video' => 'Видео',
    'Viewed at' => 'Дата просмотра',
    'Views' => '',
    'Vk' => '',
    'Wallet number' => 'Номер счёта',
    'WhatsApp' => '',
    'Year' => 'Год',
    'Year End' => 'Год окончания',
    'Year Start' => 'Год начала',
    'Your Email' => 'Ваш Email',
    'Your Name' => 'Ваше Имя',
    'Zip Code' => 'Почтовый индекс',
    'First Name' => 'Имя',
    'Last Name' => 'Фамилия',
    'Mortgage' => 'Ипотека',
    'Additional information' => 'Дополнительная информация',
    'Houses' => 'Дома',
    'Execution time (days)' => 'Время выполнения (дни)',
];
