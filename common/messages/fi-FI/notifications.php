<?php 
 return [
    '<strong> uJobs.me </strong> is a freelance market, where you can place your services for free, get new customers or hire a freelancer for the job you need to do.' => '<strong>uJobs.minulle</strong> on markkinapaikka, freelancereita, jossa Voit lähettää palveluja, saada uusia asiakkaita tai palkata urakoitsijan tekemään työtä, että Sinun täytyy tehdä.',
    'Activate Your Account' => 'Aktivoida tilisi',
    'Additional income' => 'Lisää tulos',
    'Auto-reply' => 'Auto-vastaus',
    'Change Password' => 'Voit Vaihtaa Salasanan',
    'Check the latest offers' => 'Katso uusimmat tarjoukset',
    'Click on the link below to confirm your email and get started.' => 'Klikkaa alla olevaa linkkiä vahvistaaksesi sähköpostiosoitteesi ja töihin.',
    'Deal safety!' => 'Turvallisuus kaupan!',
    'Dear {username}' => 'Rakas(th) {username}',
    'Do you know how to do something well? Tell us about it on our website, set the price for which you are ready to work. We will advertise your service: you will receive new orders and earn extra money!' => 'Tiedät, miten tehdä jotain hyvin? Kerro siitä meille sivuillamme, asettaa hinnan olet valmis työskentelemään. Me mainostaa palvelun saat uusia tilauksia ja ansaita ylimääräistä rahaa!',
    'Do you need to get a job done?' => 'Sinun täytyy tehdä työtä?',
    'Find service that you need!' => 'Löytää oikea palvelu',
    'Go to cart' => 'Siirry koriin',
    'Go to view' => 'Näkymä',
    'Greetings! You have attempted to subscribe on notifications via {provider}. Please, do this from {link}' => 'Tervetuloa! Yritit tilata ilmoitukset {provider}. Tee se täällä {link}',
    'Hi, {username}' => 'Hei, {username}',
    'Hi, {username}!' => 'Hei, {username}!',
    'Hi, {username}, <br> There are some things you can fix on {site}!' => 'Hei, {username}, <br> On olemassa muutamia asioita, Voit tehdä oman profiilin uJobs!',
    'Hi, {username}, <br> Welcome to {site}!' => 'Hei, {username}, <br> tervetuloa {site}',
    'Hi, {username}, Welcome to {site}!' => 'Hei, {username}, tervetuloa {site}!',
    'If the button above does not work, copy and paste the following URL into your browser\'s address bar.' => 'Jos painike ei toimi, kopioi ja liitä seuraava URL-osoite osoiteriville selaimen.',
    'If the link above does not work, copy and paste the URL in a new browser tab. For security reasons, the link is valid for 24 hours.' => 'Jos yllä oleva linkki ei toimi, kopioi ja liitä URL-osoite uusi välilehti selaimessa. Turvallisuussyistä, linkki on voimassa 24 tuntia',
    'If you have any troubles with payment, you can {contact_our_support}.' => 'Jos sinulla on ongelmia maksun, voit {contact_our_support}',
    'If you wish to post announcement on our site for free, please press the following button.' => 'Jos haluat sijoittaa mainos sivuillamme, klikkaa',
    'Invitation to register on {site}' => 'Kutsu rekisteröityä {site}',
    'It is also a good idea to add {email} to your address book to ensure that you receive our messages (no spam, we promise!).' => 'On myös hyvä idea lisätä {email} osoitekirjaasi vastaanottaa sähköposteja (ei roskapostia, lupaan!)',
    'It is never been so easy to get it done' => 'Palvelumme yhdistää Asiakkaiden ja eri palvelujen tarjoajat',
    'It is really that simple.' => 'Meidän on varmistettava turvallisuus, Olet taattu saada maksu järjestyksessä.',
    'It seems your photo and profile description are not set. Please fix this!' => 'Näyttää siltä, että ei ole määritetty kuva ja ei täytä kuvaus oma profiili. Ota korjata sen!',
    'It seems your profile description is not set. Please fix this!' => 'Näyttää siltä, että et täytä profiilin kuvaus. Ota korjata sen!',
    'It seems your profile photo is not set. Please fix this!' => 'Näyttää siltä, että et ole määrittänyt profiilisi kuva. Ota korjata sen!',
    'Join Now' => 'Liity nyt',
    'Just order a job and the freelancer will get right to work! We make it easy to purchase and communicating with our sellers.' => 'Voit valita minkä tahansa tyyppisiä työtä, auttaa ympäri taloa, ennen kuin teet verkkosivuilla tai mainoskampanjoita. Me yhdistää online-ja offline-freelancereita, luoda mukavin alustan etsiä esiintyjiä!',
    'Looking for service or order' => 'Etsi palveluja, tai',
    'New Message' => 'Uusi viesti',
    'New inbox message on {site}' => 'Uusi viesti: {site}',
    'New request on service' => 'Uusi hakemus palvelu',
    'Notification from {site}' => 'Ilmoituksen {site}',
    'Or you can unsubscribe from email messages by this link {link}).' => 'Tai Voit peruuttaa uutiskirjeitä tästä linkistä {link}',
    'Order #{number} cancelled. The money will soon be returned to the balance.' => 'Jotta #{number} peruttu. Rahaa menee tasapaino.',
    'Order #{number} is automatically completed after {days, plural, one{# day} other{# days}} passed from delivery.' => 'Jotta #{number} on automaattisesti valmis, kun {days, plural, one{# päivä} few{# päivä} other{# päivää}}, jona toimitus.',
    'Order for {price}' => 'Saatavilla kohteelle {price}',
    'Post announcement about selling services or products on uJobs.me for free' => 'Aseta vapaa ad exchange uJobs tavaroiden myynnistä tai palveluja',
    'Post for free' => 'Paikka ilmainen',
    'Press button to return to cart' => 'Napsauttamalla tätä voit palata ostoskoriin',
    'Ready to get started?' => 'Valmis ansaita?',
    'Recommendations about your profile' => 'Suositukset profiilissasi',
    'Sounds too good to be true? See for yourself!' => 'Kuulostaa liian hyvältä ollakseen totta? Katso itse!',
    'Start Earning' => 'Ansaita',
    'Thanks for registration on our portal!' => 'Kiitos rekisteröitymisestä meidän portaali!',
    'Thanks for using our service! We offer you similar services you may be interested in:' => 'Kiitos työstänne meidän palvelu! Tarjoavat vastaavia palveluja, jotka saattavat kiinnostaa sinua:',
    'Thanks for your message. We will contact you as soon as possible!' => 'Kiitos viestin. Otamme yhteyttä pian!',
    'The uJobs Team' => 'Joukkue uJobs',
    'These jobs are missing their images. Please, add them!' => 'Tässä työssä, kuvat puuttuvat. Lisää niitä!',
    'This email was sent to you by uJobs. You are receiving this email because you signed up for {site}' => 'Tämä Sähköposti lähetetään yhtiön uJobs.minua.',
    'To start the process, please click the following link:<br>{link}' => 'Aloittaa, klikkaa seuraavaa linkkiä:<br>{link}',
    'User {user} has posted new article' => 'Käyttäjän {user} on julkaissut uuden artikkelin',
    'User {user} has posted new job request' => 'Käyttäjän {user} on julkaissut uuden hakemuksen palvelu',
    'User {user} has posted new portfolio album' => 'Käyttäjän {user} on julkaissut uuden albumin, portfolio',
    'User {user} has posted new product' => 'Käyttäjän {user} on lähetetty uusi tuote',
    'User {user} has posted new product request' => 'Käyttäjän {user} on julkaissut uuden sovelluksen, tuotteen',
    'User {user} has posted new service' => 'Käyttäjän {user} on julkaissut uuden palvelun',
    'User {user} likes your portfolio album' => 'Käyttäjän {user} on mitoitettu albumin portfolio',
    'User {user} shared with you a link on {site} site' => 'Käyttäjän {user} on jakanut linkin, jossa sinun verkkosivuilla {site}',
    'User {user} wants to add you to friend list' => 'Käyttäjän {user} haluaa lisätä sinut ystäväksi',
    'View and respond' => 'Voit katsoa ja vastata',
    'View by link: {link}' => 'Katso linkki: {link}',
    'Visit Website' => 'Mennä verkkosivuilla',
    'Visit profile settings' => 'Valitse muokkaa',
    'We got a request to reset your uJobs password.' => 'Saimme pyynnön palauttaa salasanan tilin uJobs',
    'Welcome to {site}' => 'Tervetuloa {site}',
    'Which you can change in your account settings' => 'Voit muuttaa Asetukset/Ohjaus Kaappi',
    'You are welcome on {site}' => 'Tervetuloa {site}',
    'You did order {title} recently.' => 'Olet äskettäin määräsi {title}',
    'You have accepted friendship' => 'Sinä hyväksytty ystävyys',
    'You have accepted invitation' => 'Olet hyväksynyt kutsun',
    'You have been invited to join company «{company}» by user {user}' => 'Käyttäjän {user} on kutsunut sinut liittymään yritys "{company}"',
    'You have created new order for user {user}' => 'Olet luonut uuden, jotta käyttäjän {user}',
    'You have declined friendship' => 'Olet hylännyt ystävä pyyntö',
    'You have declined invitation' => 'Sinun kieltäytyi kutsusta',
    'You have just become part of the largest, most affordable and easiest to use marketplace for services and products!' => 'Olet juuri rekisteröitynyt sivuston uJobs.minut ja tuli osallistuja suuri yhteisö ammattilaisia!',
    'You have left response on request {tender}' => 'Et vastannut pyyntöön {tender}',
    'You have new inbox message from user {user}' => 'Sinulla on uusi viesti {user}',
    'You have new notification from {site}' => 'Saat uuden ilmoituksen verkkosivuilla {site}',
    'You have new order from user {user}' => 'Sinulla on uusi tilaus {user}',
    'You have new response on request {tender} from {user}' => 'Sinulla on uusi vastaus pyyntöön {tender} käyttäjän {user}',
    'You have not set your skills for your profile. You can do it now!' => 'Et ole vielä täytetty taitoja oman profiilin. Voit tehdä sen nyt!',
    'You have subscribed on notifications via {provider}' => 'Voit tilata ilmoitukset {provider}',
    'You have unsubscribed from notifications via {provider}, but it`s never too late to come back :)' => 'Sinulla on peruuttanut ilmoitukset {provider}, mutta se ei ole koskaan liian myöhäistä mennä takaisin :)',
    'You still have not added jobs. You can do it now!' => 'Et ole vielä lisätty yksi työ. Voit tehdä sen nyt!',
    'You still have services in cart that are awaiting for payment. You wanted to buy: {items}' => 'Ostoskorissa ei ole maksettu palveluista. Haluaisitko ostaa: {items}',
    'Your article has passed moderation and has been added to catalog.' => 'Artikkeli on moderoitu, ja julkaistu hakemistossa.',
    'Your article has passed moderation on {site}' => 'Artikkeli on kulunut maltillisesti {site}',
    'Your article {article} has passed moderation and is published in catalog' => 'Artikkeli on {article} kulunut valvottu ja sijoitetaan hakemistoon',
    'Your job has passed moderation and has been added to catalog. If you want more chances to be found and receive orders, you can post another job.' => 'Työ on kulunut maltillisesti ja sijoitetaan hakemistoon. Jos haluat lisätä mahdollisuuksia saada tilauksia, voit isännöidä muita palveluja.',
    'Your job has passed moderation on {site}' => 'Työ on kulunut maltillisesti {site}',
    'Your job {job} has passed moderation and is published in catalog' => 'Palvelua {job} on hyväksytty ja sijoitetaan hakemistoon',
    'Your portfolio album has passed moderation and has been added to catalog.' => 'Albumin salkut kulunut maltillisesti ja sijoitetaan hakemistoon',
    'Your portfolio album has passed moderation on {site}' => 'Albumin salkut kulunut maltillisesti {site}',
    'Your portfolio album {portfolio} has passed moderation and is published in catalog' => 'Albumin portfolio {portfolio} kulunut valvottu ja julkaistu luettelo',
    'Your product has passed moderation and has been added to catalog.' => 'Tuotteen kulunut maltillisesti ja sijoitetaan hakemistoon.',
    'Your product has passed moderation on {site}' => 'Tuotteen kulunut maltillisesti {site}',
    'Your product request has passed moderation and has been added to catalog.' => 'Pyyntö tavarat on moderoitu, ja lisätään luettelo.',
    'Your product request has passed moderation on {site}' => 'Pyyntö tuote on kulunut valvottu ja julkaistu sivustolla {site}',
    'Your product request {product} has passed moderation and is published in catalog' => 'Pyynnön kohteen {product} on moderoitu, ja sijoitetaan hakemistoon',
    'Your product {product} has passed moderation and is published in catalog' => 'Tuotteen {product} kulunut maltillisesti ja sijoitetaan hakemistoon',
    'Your profile has passed moderation and has been added to professional`s catalog. If you want more chances to be found and receive orders, please post your jobs.' => 'Profiilisi kulunut valvottu ja julkaistu luettelo. Jos haluat lisätä mahdollisuuksia saada tilauksia, aseta palveluja.',
    'Your profile has passed moderation on {site}' => 'Profiilisi kulunut maltillisesti {site}',
    'Your request has passed moderation and has been added to catalog.' => 'Pyyntö on ollut moderoitu, ja julkaistu hakemistossa.',
    'Your request has passed moderation on {site}' => 'Hakemuksesi on mennyt ohi maltillisesti {site}',
    'Your request {job} has passed moderation and is published in catalog' => 'Pyyntö {job} on moderoitu, ja sijoitetaan hakemistoon',
    'Your transfer confirmation code: {code}' => 'Vahvistus koodi käännös: {code}',
    'contact our support' => 'ota yhteyttä asiakaspalveluumme',
    'uJobs is an online marketplace for services where you can find <br>everything you need, and in unrivaled quality.' => 'uJobs — online-palvelujen markkinat, jossa voit <br>etsi kaikki mitä tarvitset, ja vertaansa vailla laatua.',
    '{0} is tired of keeping uJobs in secret.' => '{0} uJobs väsynyt pitämään salassa',
    '{sender} left you inbox message' => 'Käyttäjän {sender} on lähettänyt sinulle viestin',
    '{sender} sent you individual job offer' => '{sender} lähetti(a) tarjota yksilöllistä työtä',
    '{sender} sent you individual job request' => '{sender} lähetti(a) pyynnön yksittäisten työtä',
    '{user} has updated order {order}' => '{user} on päivitetty, jotta {order}',
    '{user} sent you invitation to join uJobs.' => '{user} lähetti sinulle kutsun liittyä uJobs.',
    'Greetings!' => 'Tervetuloa!',
    'Invitation to {site}' => 'Kutsun {site}',
    'On {site} you can find a lot of services for construction or buy a ready property' => '{site} voit löytää erilaisia palveluja rakentamiseen tai ostaa valmiina omaisuutta',
];