<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.09.2017
 * Time: 13:47
 */

namespace common\modules\attribute\models\backend;

use common\models\JobAttribute;
use common\models\PageAttribute;
use common\models\UserAttribute;
use common\models\UserPortfolioAttribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\base\Model;

/**
 * Class MergeAttributeValueForm
 * @package common\modules\attribute\models\backend
 */
class MergeAttributeValueForm extends Model
{
    /**
     * @var integer
     */
    public $attribute_id = null;

    /**
     * @var integer
     */
    public $attribute_value_id = null;

    /**
     * @var boolean
     */
    public $merge = null;

    /**
     * @var integer
     */
    public $target_attribute_value_id = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'target_attribute_value_id', 'attribute_value_id'], 'required'],
            [['attribute_id', 'target_attribute_value_id', 'attribute_value_id'], 'integer'],
            [['merge'], 'boolean'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            [['target_attribute_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['target_attribute_value_id' => 'id']],
            [['attribute_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['attribute_value_id' => 'id']],
            ['attribute_value_id', 'compare', 'compareAttribute' => 'target_attribute_value_id', 'operator' => '!==', 'type' => 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('attribute', 'ID'),
            'attribute_id' => Yii::t('attribute', 'Attribute ID'),
            'target_attribute_value_id' => Yii::t('attribute', 'Start typing value title'),
            'attribute_value_id' => Yii::t('attribute', 'This value id'),
            'merge' => Yii::t('attribute', 'I want to merge this value with another'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $targetValue = AttributeValue::findOne($this->target_attribute_value_id);

        ProductAttribute::updateAll(['value' => $this->target_attribute_value_id, 'value_alias' => $targetValue->alias], ['value' => $this->attribute_value_id]);
        JobAttribute::updateAll(['value' => $this->target_attribute_value_id, 'value_alias' => $targetValue->alias], ['value' => $this->attribute_value_id]);
        UserAttribute::updateAll(['value' => $this->target_attribute_value_id, 'value_alias' => $targetValue->alias], ['value' => $this->attribute_value_id]);
        UserPortfolioAttribute::updateAll(['value' => $this->target_attribute_value_id, 'value_alias' => $targetValue->alias], ['value' => $this->attribute_value_id]);
        PageAttribute::updateAll(['value' => $this->target_attribute_value_id, 'value_alias' => $targetValue->alias], ['value' => $this->attribute_value_id]);

        $value = AttributeValue::findOne($this->attribute_value_id);
        $value->delete();

        return true;
    }

    /**
     *
     */
    public function getSimilarValues()
    {
        // TODO
    }
}
