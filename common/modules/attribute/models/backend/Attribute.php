<?php

namespace common\modules\attribute\models\backend;

use common\modules\attribute\models\AttributeToGroup;
use ReflectionClass;
use Yii;
use yii\helpers\Html;

/**
 * Class Attribute
 * @package common\modules\attribute\models\backend
 */
class Attribute extends \common\modules\attribute\models\Attribute
{
    /**
     * @return array
     */
    public function getInGroups()
    {
        $result = '';

        $groups = AttributeToGroup::find()->joinWith(['attributeGroup' => function ($q) {
            $q->select('title, id');
        }])->where(['attribute_id' => $this->id])->limit(50)->all();

        if ($groups !== null) {
            $groups = array_map(function ($value) {
                return Html::a($value->attributeGroup->title, ['/attribute/attribute-group/index', 'AttributeGroupSearch[id]' => $value->attribute_group_id], ['target' => '_blank']);
            }, $groups);

            $result = implode(' | ', $groups);
        }

        return $result;
    }

    /**
     * @return string
     */
    public function formatValues()
    {
        $values = $this->values;
        $output = '';

        if ($values !== null) {
            $values = array_map(function ($value) {
                return Html::a($value->title, ["/attribute/attribute-value/update", 'id' => $value->id], [
                    'title' => Yii::t('attribute', 'Update {0}', [(new ReflectionClass($value))->getShortName()]),
                    'data-pjax' => 0,
                    'class' => 'modal-edit'
                ]);
            }, $values);

            $output = implode(' | ', $values);
        }

        return $output;
    }
}
