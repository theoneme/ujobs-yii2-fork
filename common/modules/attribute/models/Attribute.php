<?php

namespace common\modules\attribute\models;

use common\models\JobAttribute;
use common\models\Setting;
use common\models\UserAttribute;
use common\modules\attribute\Module;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "mod_attribute".
 *
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property string $alias
 * @property boolean is_system
 * @property boolean is_searchable
 * @property boolean is_parsed
 *
 * @property AttributeDescription $translation
 * @property AttributeValue[] $values
 */
class Attribute extends ActiveRecord
{
    /**
     * @var AttributeDescription[]
     */
    public $translationsArr = [];

    /**
     * @var AttributeValue[]
     */
    public $valuesArr = [];

    /**
     * @var array
     */
    private $textAttributes = [
        'textbox',
        'textarea'
    ];

    /*
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mod_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_searchable', 'is_system', 'is_primary', 'is_parsed'], 'boolean'],
            [['name', 'type', 'alias'], 'required'],
            [['alias'], 'unique'],
            [['alias'], 'string'],
            [['is_system', 'is_parsed'], 'default', 'value' => false],
            [['is_primary'], 'default', 'value' => true],
            ['translationsArr', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('attribute', 'ID'),
            'attribute_group_id' => Yii::t('attribute', 'Attribute Group ID'),
            'type' => Yii::t('attribute', 'Type'),
            'alias' => Yii::t('attribute', 'Alias'),
            'is_searchable' => Yii::t('attribute', 'Is searchable'),
            'is_primary' => Yii::t('attribute', 'Is primary')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id']);
        $relation->andOnCondition(['mod_attribute_description.entity' => 'attribute', 'mod_attribute_description.locale' => Yii::$app->language]);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])->indexBy('locale');
        $relation->andOnCondition(['mod_attribute_description.entity' => 'attribute']);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeToGroups()
    {
        return $this->hasMany(AttributeToGroup::class, ['attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(AttributeGroup::class, ['id' => 'attribute_id'])->via('attributeToGroups');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(AttributeValue::class, ['attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLimitedValues()
    {
        return $this->hasMany(AttributeValue::class, ['attribute_id' => 'id'])->limit(20);
    }

    /**
     *
     */
    public function init()
    {
        foreach (Yii::$app->params['languages'] as $locale => $title) {
            $content = new AttributeDescription;
            $content->locale = $locale;
            $this->translationsArr[$locale] = $content;
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        foreach ($this->translationsArr as $key => $item) {
            $translate = new AttributeDescription();
            if (is_array($item)) {
                $translate->attributes = $item;
            } else if (is_object($item)) {
                $translate->attributes = $item->attributes;
            }

            $translate->id = isset($item['id']) ? $item['id'] : null;
            $this->translationsArr[$key] = $translate;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->updateRelated(AttributeDescription::class, $this->translationsArr);
    }

    /**
     *
     */
    public function afterDelete()
    {
        AttributeDescription::deleteAll('entity_content = :id and entity = :entityType', [':id' => $this->id, ':entityType' => 'attribute']);
        JobAttribute::deleteAll('attribute_id =:id', [':id' => $this->id]);
        ProductAttribute::deleteAll('attribute_id =:id', [':id' => $this->id]);
        UserAttribute::deleteAll('attribute_id =:id', [':id' => $this->id]);

        $attributeValues = AttributeValue::find()->where(['attribute_id' => $this->id])->all();
        foreach ($attributeValues as $value) {
            $value->delete();
        }

        parent::afterDelete();
    }

    /**
     * @param string $className
     * @param array $data
     */
    protected function updateRelated($className = null, $data = [])
    {
        /* @var ActiveRecord $className */
        if ($className != null) {
            $ids = [];
            foreach ($data as $item) {
                $object = $item->id ? $className::find()->where(['id' => $item['id']])->one() : new $className;
                $object->attributes = $item->attributes;
                $object->entity = 'attribute';
                $object->entity_content = $this->id;
                $object->save();

                $ids[] = $object->id;
            }

            if (empty($ids)) {
                $className::deleteAll(['entity' => 'attribute', 'entity_content' => $this->id]);
            } else {
                $className::deleteAll(['and', ['entity' => 'attribute', 'entity_content' => $this->id], ['not in', 'id', array_values($ids)]]);
            }
        }
    }

    /**
     * @return string
     */
    public function getValuePlaceholder()
    {
        $placeholder = Yii::t('app', 'Set attribute {attribute}', [
            'attribute' => $this->title
        ]);
        if(!empty($this->values)) {
            $temp = [];
            foreach($this->values as $key => $value) {
                if($value->status === AttributeValue::STATUS_APPROVED) {
                    $valueTranslations = $value->translations;

                    if(array_key_exists(Yii::$app->language, $valueTranslations)) {
                        $temp[] = $valueTranslations[Yii::$app->language]->title;
                    }
                }

                if($key > 1) {
                    $temp[] = '...';
                    break;
                }
            }
            $placeholder = Yii::t('app', 'For example:') . ' ' . implode(', ', $temp);
        }

        return $placeholder;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->translations[Yii::$app->language]->title ?? array_values($this->translations)[0]->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translations[Yii::$app->language]->description ?? array_values($this->translations)[0]->description;
    }

    /**
     * @return bool
     */
    public function isTextAttribute()
    {
        $result = false;
        if (in_array($this->type, $this->textAttributes)) {
            $result = true;
        }

        return $result;
    }
}
