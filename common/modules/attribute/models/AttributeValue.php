<?php

namespace common\modules\attribute\models;

use common\models\JobAttribute;
use common\models\UserAttribute;
use common\models\UserPortfolioAttribute;
use common\modules\attribute\Module;
use common\modules\board\models\ProductAttribute;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mod_attribute_value".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $parent_value_id
 * @property string $image
 * @property integer $sort_order
 * @property string $alias
 * @property integer $status
 *
 * @property Attribute $relatedAttribute
 * @property AttributeDescription[] $translations
 * @property AttributeDescription $translation
 * @property AttributeValue $parentValue
 */
class AttributeValue extends ActiveRecord
{
    const STATUS_NEW = 10;
    const STATUS_APPROVED = 20;

    /**
     * @var array
     */
    public $translationsArr = [];

    /**
     * @var int
     */
    public $relatedCount = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mod_attribute_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'parent_value_id', 'sort_order', 'relatedCount'], 'integer'],
            [['image', 'alias'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_NEW],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('attribute', 'ID'),
            'attribute_id' => Yii::t('attribute', 'Attribute ID'),
            'image' => Yii::t('attribute', 'Image'),
            'sort_order' => Yii::t('attribute', 'Sort Order'),
            'title' => Yii::t('attribute', 'Title'),
            'alias' => Yii::t('attribute', 'Machine name'),
            'status' => Yii::t('attribute', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id']);
        $relation->andOnCondition(['mod_attribute_description.entity' => 'attribute-value']); //TODO Вернуть как было, когда добавим значения атрибутов на других языках
//        $relation->andOnCondition(['entity' => 'attribute-value', 'locale' => Yii::$app->language]);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedAttribute()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])
            ->andOnCondition(['mod_attribute_description.entity' => 'attribute-value'])
            ->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobAttributes()
    {
        return $this->hasMany(JobAttribute::class, ['value' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'parent_value_id']);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->translations[Yii::$app->language]->title ?? array_values($this->translations)[0]->title ?? '';
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_NEW => Yii::t('attribute', 'Not moderated'),
            self::STATUS_APPROVED => Yii::t('attribute', 'Approved'),
        ];
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();

        foreach ($this->translationsArr as $key => $item) {
            $translate = new AttributeDescription();
            $translate->attributes = $item;
            if (isset($item['id'])) {
                $translate->id = $item['id'];
            }
            $this->translationsArr[$key] = $translate;
            $valid = $valid && $translate->validate();
        }

        if ($this->alias === null && !empty($this->translationsArr)) {
            $this->alias = Yii::$app->utility->transliterate($this->translationsArr[0]->title);
        }

        return $valid;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        AttributeDescription::deleteAll('entity_content = :id and entity = :entityType', [':id' => $this->id, ':entityType' => 'attribute-value']);

        foreach ($this->translationsArr as $key => $item) {
            /* @var AttributeDescription $item */
            $item->entity_content = $this->id;
            $item->entity = 'attribute-value';
            $item->save();
        }

        if(!$insert && !empty($changedAttributes['alias'])) {
            JobAttribute::updateAll(['value_alias' => $this->alias], ['value' => $this->id]);
            ProductAttribute::updateAll(['value_alias' => $this->alias], ['value' => $this->id]);
            UserAttribute::updateAll(['value_alias' => $this->alias], ['value' => $this->id]);
            UserPortfolioAttribute::updateAll(['value_alias' => $this->alias], ['value' => $this->id]);
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            AttributeDescription::deleteAll(['entity_content' => $this->id, 'entity' => 'attribute-value']);
            JobAttribute::deleteAll(['value' => $this->id]);
            ProductAttribute::deleteAll(['value' => $this->id]);
            UserAttribute::deleteAll(['value' => $this->id]);
            return true;
        }

        return false;
    }
}
