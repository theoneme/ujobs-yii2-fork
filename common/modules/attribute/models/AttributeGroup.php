<?php

namespace common\modules\attribute\models;

use common\modules\attribute\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mod_attribute_group".
 *
 * @property integer $id
 * @property integer $sort_order
 * @property string $title
 */
class AttributeGroup extends ActiveRecord
{
    /**
     * @var array
     */
    public $attributeToGroup = [];

    /**
     * @var array
     */
    public $translationsArr = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mod_attribute_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_order'], 'integer'],
            [['title'], 'string', 'max' => 200],
            ['is_generated', 'boolean'],
            ['attributeToGroup', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('attribute', 'ID'),
            'sort_order' => Yii::t('attribute', 'Sort Order'),
            'title' => Yii::t('attribute', 'Title')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id']);
        $relation->andWhere(['entity' => 'attribute-group', 'locale' => Yii::$app->language]);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id']);
        $relation->andWhere(['entity' => 'attribute-group']);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrToGroup()
    {
        return $this->hasMany(AttributeToGroup::class, ['attribute_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrs()
    {
        return $this->hasMany(Attribute::class, ['id' => 'attribute_id'])->via('attrToGroup');
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        AttributeDescription::deleteAll('entity_content = :id and entity = :entityType', [':id' => $this->id, ':entityType' => 'attribute-group']);

        foreach ($this->translationsArr as $key => $item) {
            /* @var AttributeDescription $item */
            $item->entity_content = $this->id;
            $item->entity = 'attribute-group';
            $item->save();
        }

        $this->updateRelated(AttributeToGroup::class, $this->attributeToGroup);
    }

    /**
     * @param string $className
     * @param array $data
     */
    protected function updateRelated($className = null, $data = [])
    {
        /* @var ActiveRecord $className */
        if ($className != null) {
            $ids = [];
            foreach ($data as $item) {
                $object = $item->id ? $className::find()->where(['id' => $item['id']])->one() : new $className;
                $object->sort_order = $item['sort_order'];
                $object->attribute_id = $item['attribute_id'];
                $object->attribute_group_id = $this->id;
                $object->save();

                $ids[] = $object->id;
            }

            if (empty($ids)) {
                $className::deleteAll(['attribute_group_id' => $this->id]);
            } else {
                $className::deleteAll(['and', ['attribute_group_id' => $this->id], ['not in', 'id', array_values($ids)]]);
            }
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            AttributeDescription::deleteAll('entity_content = :id and entity = :entityType', [':id' => $this->id, ':entityType' => 'attribute-group']);
            return true;
        } else {
            return false;
        }
    }
}
