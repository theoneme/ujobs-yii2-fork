<?php

namespace common\modules\attribute\models;

use common\modules\attribute\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mod_attribute_description".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_content
 * @property string $title
 * @property string $locale
 * @property string $description
 *
 * @property Attribute $attr
 * @property AttributeValue $attributeValue
 */
class AttributeDescription extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mod_attribute_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'locale'], 'required'],
            [['entity', 'title', 'locale', 'description'], 'string', 'max' => 255],
			[['entity_content'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('attribute', 'ID'),
            'entity' => Yii::t('attribute', 'Entity'),
            'entity_content' => Yii::t('attribute', 'Entity Content'),
            'title' => Yii::t('attribute', 'Title'),
            'locale' => Yii::t('attribute', 'Locale'),
            'description' => Yii::t('attribute', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'entity_content']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'entity_content']);
    }
}
