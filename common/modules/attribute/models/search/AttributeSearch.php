<?php

namespace common\modules\attribute\models\search;

use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\attribute\models\backend\Attribute;

/**
 * AttributeSearch represents the model behind the search form about `common\modules\attribute\models\Attribute`.
 */
class AttributeSearch extends Attribute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'/*, 'attribute_group_id', 'sort_order'*/], 'integer'],
            [['name', 'alias'], 'string'],
            [['type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attribute::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['name' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchNotModerated($params)
    {
        $query = Attribute::find()->joinWith(['translation', 'values' => function($q) {
            return $q->andWhere(['status' => AttributeValue::STATUS_NEW]);
        }])->where(['mod_attribute_value.status' => AttributeValue::STATUS_NEW])->groupBy('mod_attribute.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'label' => Yii::t('attribute', 'Title'),
                    'default' => SORT_ASC
                ],
                '*'
            ]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        //$query->andFilterWhere(['like', 'mod_attribute_description.title', $this->title]);

        return $dataProvider;
    }
}
