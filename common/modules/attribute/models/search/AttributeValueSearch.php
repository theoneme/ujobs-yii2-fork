<?php

namespace common\modules\attribute\models\search;

use common\modules\attribute\Module;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\attribute\models\AttributeValue;

/**
 * AttributeValueSearch represents the model behind the search form about `common\modules\attribute\models\AttributeValue`.
 */
class AttributeValueSearch extends AttributeValue
{
	public $title = null;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'attribute_id', 'sort_order', 'status'], 'integer'],
            [['image', 'title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttributeValue::find();
		//$query->with(['parent']);
		$query->joinWith(['translation']);
		$query->groupBy(['mod_attribute_value.id']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		$dataProvider->setSort([
			'attributes' => [
				'title' => [
					'asc' => ['title' => SORT_ASC],
					'desc' => ['title' => SORT_DESC],
					'label' => Yii::t('attribute', 'Title'),
					'default' => SORT_ASC
				],
				'*'
			]
		]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'attribute_id' => $this->attribute_id,
            'sort_order' => $this->sort_order,
            'status' => $this->status,
        ]);
		
		$query->andFilterWhere(['like', 'mod_attribute_description.title', $this->title]);

        return $dataProvider;
    }
}
