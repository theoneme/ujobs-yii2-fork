<?php

namespace common\modules\attribute\models;

use common\modules\attribute\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mod_attribute_to_group".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $attribute_group_id
 * @property integer $sort_order
 *
 * @property AttributeGroup $attributeGroup
 * @property Attribute $attribute
 */
class AttributeToGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mod_attribute_to_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id'], 'required'],
            [['attribute_id', 'attribute_group_id', 'sort_order'], 'integer'],
            [['attribute_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeGroup::class, 'targetAttribute' => ['attribute_group_id' => 'id']],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('attribute', 'ID'),
            'attribute_id' => Yii::t('attribute', 'Attribute ID'),
            'attribute_group_id' => Yii::t('attribute', 'Attribute Group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroup()
    {
        return $this->hasOne(AttributeGroup::class, ['id' => 'attribute_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }
}
