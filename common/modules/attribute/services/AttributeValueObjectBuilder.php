<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.10.2017
 * Time: 15:54
 */

namespace common\modules\attribute\services;

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;

/**
 * Class AttributeValueObjectBuilder
 * @package common\modules\attribute\services
 */
class AttributeValueObjectBuilder
{
    /**
     * @param $className
     * @param int $attributeId
     * @param string $value
     * @param array $params
     *
     * @return mixed
     */
    public static function makeAttributeObject($className, int $attributeId, $value, $params = [])
    {
        $attributeObject = null;
        $locale = $params['locale'] ?? 'ru-RU';
        $parent_value_id = $params['parent_value_id'] ?? null;
        $status = $params['status'] ?? AttributeValue::STATUS_NEW;
        $value = trim($value);

        /* @var Attribute $attribute */
        $attribute = Attribute::find()->where(['mod_attribute.id' => $attributeId])->one();
        if ($attribute === null || empty($value)) {
            return null;
        }

        $entityAlias = $attribute->alias;

        /* @var AttributeValue $attributeValue */
        $attributeValue = AttributeValue::find()
            ->joinWith(['translations'])
            ->where(['attribute_id' => $attributeId])
            ->andFilterWhere(['parent_value_id' => $parent_value_id])
            ->andWhere(['or',
                ['mod_attribute_description.title' => $value],
                ['attribute_id' => $attributeId, 'mod_attribute_value.id' => $value]
            ])->one();

        if ($attributeValue === null) {
            $attributeValue = new AttributeValue([
                'attribute_id' => $attribute->id,
                'parent_value_id' => $parent_value_id,
                'translationsArr' => [
                    [
                        'title' => $value,
                        'locale' => $locale
                    ],
                ],
                'status' => $status
            ]);
            $attributeValue->save();
        }
        $value = $attributeValue->id;
        $valueAlias = $attributeValue->alias;

        if(class_exists($className)) {
            $attributeObject = new $className([
                'attribute_id' => $attributeId,
                'locale' => $locale,
                'entity_alias' => $entityAlias,
                'value_alias' => $valueAlias,
                'value' => $value,
            ]);
        }

        return $attributeObject;
    }
}
