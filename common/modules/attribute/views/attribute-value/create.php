<?php

use common\modules\attribute\Module;

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\AttributeValue */

$this->title = Yii::t('attribute', 'Create Attribute Value');
$this->params['breadcrumbs'][] = ['label' => Yii::t('attribute', 'Attribute Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-value-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
