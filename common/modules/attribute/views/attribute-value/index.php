<?php

use backend\assets\JqueryUIAsset;
use backend\assets\SelectizeAsset;
use common\modules\attribute\models\AttributeValue;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\modules\attribute\models\Attribute;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\attribute\models\search\AttributeValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $currentAttribute Attribute */

SelectizeAsset::register($this);

if ($currentAttribute) {
    $this->title = Yii::t('attribute', 'Attribute Values for attribute') . ' ' . $currentAttribute->translation->title;
} else {
    $this->title = Yii::t('attribute', 'Attribute Values');
}

$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="attribute-value-index">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <p>
                    <?= Html::a(Yii::t('attribute', 'Create {what}', [
                        'what' => Yii::t('attribute', 'Attribute Value')
                    ]), ['create', 'attribute_id' => $_GET['AttributeValueSearch']['attribute_id']], [
                        'class' => 'btn btn-success modal-edit',
                        'data-pjax' => 0,
                        'title' => Yii::t('attribute', 'Create {what}', ['what' => Yii::t('attribute', 'Attribute Value')])
                    ]) ?>

                    <?= Html::a(Yii::t('attribute', 'Delete selected'), '#', [
                        'class' => 'btn btn-danger',
                        'id' => 'd-multiple',
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete these items?'),
                    ]) ?>
                </p>

                <?php Pjax::begin(['id' => 'grid-container', 'timeout' => 5000]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'id' => 'attribute-value-grid',
                    'columns' => [
                        ['class' => 'yii\grid\CheckboxColumn'],
                        'title',
                        'image',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /* @var $model AttributeValue*/
                                return $model->getStatusLabel(true);
                            },
                            'filter' => AttributeValue::getStatusLabels()
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Actions',
                            'template' => '{update} {delete}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('attribute', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                        'data-pjax' => 0,
                                        'class' => 'modal-edit'
                                    ]);
                                }
                            ],
                            'urlCreator' => function ($action, $model) {
                                $url = '';
                                switch ($action) {
                                    case 'update':
                                        $url = Url::toRoute(["/attribute/attribute-value/{$action}", 'id' => $model->id]);
                                        break;
                                    case 'delete':
                                        $url = Url::toRoute(["/attribute/attribute-value/{$action}", 'id' => $model->id]);
                                        break;
                                    default:
                                        break;
                                }
                                return $url;
                            }
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>

<?php $this->registerJs('
    $("#d-multiple").click(function() {
        var ids = $("#w0").yiiGridView("getSelectedRows");
        $.ajax({
            type: "POST",
            url: "' . Url::to('/backend/attribute/attribute-value/delete-multiple', true) . '",
            data: {
                pk: ids
            },
            success: function() {
                $.each(ids, function(index, value) {
                    var el = $("#w0").find("[data-key=" + value + "]");
                    el.remove();
                })
            }
        });
    });
');