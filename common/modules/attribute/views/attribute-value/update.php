<?php

use common\modules\attribute\Module;

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\AttributeValue */

$this->title = Yii::t('attribute', 'Update {modelClass}: ', [
    'modelClass' => 'Attribute Value',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('attribute', 'Attribute Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('attribute', 'Update');
?>

<div class="attribute-value-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
