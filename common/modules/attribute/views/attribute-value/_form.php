<?php

use common\modules\attribute\models\backend\MergeAttributeValueForm;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use common\modules\attribute\models\AttributeValue;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model AttributeValue */
/* @var $isAjax boolean */
/* @var MergeAttributeValueForm $mergeModel */

?>

    <div class="attribute-value-form">
        <div class="box box-primary">
            <!--		<div class="box-header with-border">-->
            <!--			<h3 class="box-title">--><?//= Html::encode($this->title) ?><!--</h3>-->
            <!--		</div>-->
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'id' => 'attribute-value-form',
                        'class' => ($isAjax ? 'ajax-submit' : ''),
                    ]
                ]); ?>
                <?php echo $form->errorSummary($model); ?>
                <?= $form->field($model, "attribute_id")->hiddenInput()->label(false); ?>

                <div class="row">
                    <?php foreach ($model->translationsArr as $item) { ?>
                        <div class="col-xs-3">
                            <?= $form->field($item, "[$item->locale]title", [
                                'addon' => ['append' => ['content' => $item->locale]]
                            ])->textInput(); ?>
                            <?= $form->field($item, "[$item->locale]locale", ["options" => ["value" => $item->locale]])->hiddenInput()->label(false); ?>
                            <?= $form->field($item, "[$item->locale]entity_content", ["options" => ["value" => $item->entity_content]])->hiddenInput()->label(false); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'alias')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'status')->radioList(AttributeValue::getStatusLabels()) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('attribute', 'Save'), ['class' => 'btn btn-success']) ?>
                    <?php if (!$model->isNewRecord) { ?>
                        <?= Html::a(Yii::t('attribute', 'Remove'), ['/attribute/attribute-value/delete/', 'id' => $model->id], ['class' => 'btn btn-danger', 'id' => 'remove_attr']) ?>
                    <?php } ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php if ($mergeModel !== null) { ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('attribute', 'Merge with other value') ?></h3>
                </div>

                <div class="box-body">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'id' => 'attribute-value-merge',
                            'class' => ($isAjax ? 'ajax-submit' : '')
                        ],
                        'action' => ['/attribute/attribute-value/merge']
                    ]); ?>
                    <?= $form->field($mergeModel, 'merge')->checkbox(['id' => 'merge-toggle-checkbox']); ?>
                    <div id="mergePart" class="hidden">
                        <div class="row">
                            <div class="col-md-8">
                                <?= $form->field($mergeModel, 'attribute_value_id')->hiddenInput()->label(false); ?>
                                <div class="selectize-input-container">
                                    <?= $form->field($mergeModel, 'target_attribute_value_id')->textarea(['id' => 'autocomplete-input',]); ?>
                                </div>
                            </div>
<!--                            <div class="col-md-4">-->
<!--                                <h4 class="box-title">--><?//= Yii::t('attribute', 'Similar attribute values') ?><!--</h4>-->
<!--                                Not yet implemented @TODO-->
<!--                            </div>-->
                        </div>
                        <?= $form->field($mergeModel, "attribute_id")->hiddenInput()->label(false); ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('attribute', 'Save'), ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php } ?>
    </div>

<?
$url = Url::to(['/ajax/attribute-list', 'id' => $mergeModel->attribute_id]);

$script = <<<JS
    $('#merge-toggle-checkbox').on('change', function() { 
        $('#mergePart').toggleClass('hidden'); 
    });

    $('#remove_attr').on('click', function() {
        let action = $(this).attr('href');
        
        $.get(action, function() {
            console.log('true');
        });
        
        return false;
    });

    $("#autocomplete-input").selectize({
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: false,
        maxItems: 1,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$url", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
        'onInitialize': function() { console.log('a');
            $('.selectize-control').removeClass('form-control');
        }
    });
JS;

$this->registerJs($script);
