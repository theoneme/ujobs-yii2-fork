<?php

use common\modules\attribute\models\Attribute;

/* @var Attribute $attribute */

?>

<div class="chover dynamic-attr">
    <input id="dynamicform-<?= $attribute->alias?>" type="checkbox" name="DynamicForm[attribute_<?= $attribute->id?>]" value="<?= $attribute->values[0]->id?>" />
    <label for="dynamicform-<?= $attribute->alias?>">
        <span class="ctext"><?= $attribute->values[0]->title?></span>
    </label>
</div>