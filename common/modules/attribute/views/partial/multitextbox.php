<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 12:57
 */

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;
use frontend\assets\SelectizeAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var Attribute $attribute */
/** @var array $values */
/** @var DynamicForm $model */
/** @var string $placeholder */

SelectizeAsset::register($this);

$attributeName = "attribute_{$attribute->id}";
$model->{$attributeName} = is_array($model->{$attributeName}) ? implode(',', $model->{$attributeName}) : $model->{$attributeName};

?>
    <div class="selectize-input-container">
        <?= Html::activeTextInput($model, $attributeName, [
            'class' => 'selectized maxed',
            'placeholder' => $placeholder,
            'data-at' => $attribute->id
        ]) ?>
    </div>

<?php $url = Url::to(['/ajax/attribute-list', 'alias' => $attribute->alias]);
$addTagMessage = Yii::t('app', 'Add tag');

$script = <<<JS
    $("#dynamicform-attribute_{$attribute->id}").selectize({
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class=\"create\">$addTagMessage&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$url", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    })
JS;

$this->registerJs($script);