<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.09.2016
 * Time: 16:33
 */

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;

/** @var Attribute $attribute */
/** @var DynamicForm $model */

$checked = (array) $model->{"attribute_{$attribute->id}"};

?>

<?php foreach ($attribute->values as $value) { ?>
    <div class="chover mini dynamic-attr">
        <input id="dynamicform-<?= $attribute->alias ?>-<?= $value->id ?>" type="checkbox"
               name="DynamicForm[attribute_<?= $attribute->id?>][]" value="<?= $value->id ?>" <?= in_array($value->id, $checked) ? 'checked' : '' ?>/>
        <label for="dynamicform-<?= $attribute->alias ?>-<?= $value->id ?>">
            <span class="ctext"><?= $value->title ?></span>
        </label>
    </div>
<?php } ?>