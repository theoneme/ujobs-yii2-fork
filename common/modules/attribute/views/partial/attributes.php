<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.02.2017
 * Time: 17:00
 */

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;
use yii\helpers\Html;

/** @var Attribute[] $attributes */
/** @var Attribute[] $additionalAttributes */
/** @var DynamicForm $model */
/** @var boolean $showAdditionalAtStart */

?>

<?php if (!empty($attributes)) { ?>
    <?php foreach ($attributes as $k => $attribute) { ?>
        <div class="row show-notes">
            <div class="form-group clearfix">
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        <label class="control-label"><?= $attribute->title ?></label>
                        <div class="mobile-question" data-toggle="modal"></div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    <?= $this->render($attribute->type, [
                        'values' => $attribute->values,
                        'model' => $model,
                        'attribute' => $attribute,
                        'placeholder' => $attribute->getValuePlaceholder()
                    ]) ?>
                </div>
                <div class="notes col-md-6 col-md-offset-12">
                    <div class="notes-head">
                        <div class="lamp text-center">
                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                        </div>
                        <div class="notes-title"><?= Yii::t('app', 'Set attribute {attribute}', ['attribute' => $attribute->title]) ?></div>
                    </div>
                    <div class="notes-descr">
                        <ul>
                            <li><?= $attribute->description ?></li>
                            <?php if ($placeholder = $attribute->getValuePlaceholder()) { ?>
                                <li>
                                    <?= $placeholder ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (!empty($additionalAttributes)) { ?>
        <div class="formgig-block show-notes">
            <div class="form-group clearfix">
                <div class="chover">
                    <?= Html::checkbox('entity', $showAdditionalAtStart, [
                        'value' => 'show',
                        'id' => 'help-me',
                    ]) .
                    Html::label(Yii::t('app', 'Show additional attributes'), 'help-me')
                    ?>
                </div>
            </div>
        </div>

        <div id="additional-attribute-info" class="<?= $showAdditionalAtStart === true ? '' : 'hidden' ?>">
            <?php foreach ($additionalAttributes as $k => $attribute) { ?>
                <div class="row show-notes">
                    <div class="form-group clearfix">
                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                            <div class="set-title">
                                <label class="control-label"><?= $attribute->title ?></label>
                                <div class="mobile-question" data-toggle="modal"></div>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                            <?= $this->render($attribute->type, [
                                'values' => $attribute->values,
                                'model' => $model,
                                'attribute' => $attribute,
                                'placeholder' => $attribute->getValuePlaceholder()
                            ]) ?>
                        </div>
                        <div class="notes col-md-6 col-md-offset-12">
                            <div class="notes-head">
                                <div class="lamp text-center">
                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                </div>
                                <div class="notes-title"><?= Yii::t('app', 'Set attribute {attribute}', ['attribute' => $attribute->title]) ?></div>
                            </div>
                            <div class="notes-descr">
                                <ul>
                                    <li><?= $attribute->description ?></li>
                                    <?php if ($placeholder = $attribute->getValuePlaceholder()) { ?>
                                        <li>
                                            <strong><?= Yii::t('app', 'For example:') ?></strong> <?= $placeholder ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>

<?php $script = <<<JS
    $('#help-me').on('change', function() {
        $('#additional-attribute-info').toggleClass('hidden');
    });
JS;

$this->registerJs($script);


