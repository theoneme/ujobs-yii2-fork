<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2017
 * Time: 15:05
 */

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;
use yii\helpers\Html;

/** @var Attribute $attribute */
/** @var array $values */
/** @var DynamicForm $model */
/** @var string $placeholder */

?>

<?= Html::activeTextInput($model, "attribute_{$attribute->id}", [
    'class' => 'form-control',
    'type' => 'number',
    'step' => '0.01',
    'prompt' => '-- ' . Yii::t('app', 'Select value'),
    'placeholder' => $placeholder,
]) ?>
