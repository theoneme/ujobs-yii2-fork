<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2017
 * Time: 14:22
 */

use common\models\DynamicForm;
use common\modules\attribute\models\Attribute;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var Attribute $attribute */
/** @var array $values */
/** @var DynamicForm $model */
/** @var string $placeholder */

?>
    <div class="selectize-input-container">
        <?= Html::activeTextInput($model, "attribute_{$attribute->id}", [
            'placeholder' => $placeholder,
            'data-at' => $attribute->id
        ]) ?>
    </div>

<?php $url = Url::to(['/ajax/attribute-list', 'alias' => $attribute->alias]);
$script = <<<JS
    new autoComplete({
        selector: "#dynamicform-attribute_{$attribute->id}",
        minChars: 2,
        autoFocus: false,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.post("{$url}", { query: request }, function(data) { 
                response($.map(data, function(item) {
                    return item.value;
                }));
            });     
        },
    });
JS;

$this->registerJs($script);
