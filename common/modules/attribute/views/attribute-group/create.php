<?php

use yii\helpers\Html;
use common\modules\attribute\Module;

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\AttributeGroup */

$this->title = Yii::t('attribute', 'Create Attribute Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('attribute', 'Attribute Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-group-form">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
