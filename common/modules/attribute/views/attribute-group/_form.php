<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\attribute\Module;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\AttributeGroup */
/* @var $form yii\widgets\ActiveForm */
/* @var $attributes array */

?>

<div class="attribute-group-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'attribute-form',
            'class' => ($isAjax ? 'ajax-submit' : ''),
        ]
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('attribute', 'Attribute Group') ?></h3>
                </div>
                <div class="box-body">
                    <?= $form->field($model, 'sort_order')->textInput() ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('attribute', 'Linked Attributes') ?></h3>
                </div>
                <div class="box-body">
                    <?php foreach($model->attrToGroup as $key => $attribute) { ?>
                        <?= $this->render('partial/attribute', [
                            'model' => $attribute,
                            'key' => (int)$key,
                            'attributes' => $attributes
                        ]) ?>
                    <?php } ?>
                    <div id="attribute-container">

                    </div>
                    <?= Html::a("<i class='fa fa-plus'></i>&nbsp;" . Yii::t('attribute', 'Add Attribute'), '#', ['id' => 'load-attribute']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('attribute', 'Create') : Yii::t('attribute', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php $this->registerJs('
    var attributes = ' . count($model->attrs) . ';

    $("#load-attribute").on("click", function(e) {
        e.preventDefault();
        $.get("'.Url::toRoute('/attribute/attribute-group/render-attribute').'", {
            id: attributes
        }, function(data) {
            if (data.success == true) {
                attributes++;
                $("#attribute-container").append(data.html);
            }
        }, "json");
    });
')?>