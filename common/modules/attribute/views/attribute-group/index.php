<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\attribute\models\search\AttributeGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('attribute', 'Attribute Groups');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="attribute-group-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('attribute', 'Create {what}', [
                    'what' => Yii::t('attribute', 'Attribute')
                ]), ['create'], [
                    'class' => 'btn btn-success modal-edit',
                    'data-pjax' => 0,
                    'title' => Yii::t('attribute', 'Create {what}', ['what' => Yii::t('attribute', 'Attribute')])
                ]) ?>
            </p>
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'title',
                    [
                        'header' => Yii::t('attribute', 'Linked Attributes'),
                        'format' => 'html',
                        'value' => function ($model) {
                            $attributes = $model->attrs;
                            $names = [];
                            if ($attributes != null) {
                                foreach ($attributes as $attribute) {
                                    $names[] = Html::tag('span', StringHelper::truncate($attribute->translation->title, 30), ['class' => 'text-info']);
                                }
                            }
                            $namesString = implode(', ', $names);

                            return StringHelper::truncate($namesString, 300);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('attribute', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                        'urlCreator' => function ($action, $model) {
                            $url = '';
                            switch ($action) {
                                case 'update':
                                    $url = Url::toRoute(["/attribute/attribute-group/{$action}", 'id' => $model->id]);
                                    break;
                                case 'delete':
                                    $url = Url::toRoute(["/attribute/attribute-group/{$action}", 'id' => $model->id]);
                                    break;
                                default:
                                    break;
                            }
                            return $url;
                        }
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>