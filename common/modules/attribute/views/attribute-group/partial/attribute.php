<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 08.02.2017
 * Time: 20:21
 */

use backend\assets\SelectizeAsset;
use yii\helpers\Html;

SelectizeAsset::register($this);

/* @var integer $key */
/* @var array $attributes */

?>
    <div class="row spacer" id="attribute-<?= $key ?>">
        <div class="col-xs-8">
            <?= Html::activeDropDownList($model, "[{$key}]attribute_id", $attributes, ['id' => "{$key}_selectizee", 'value' => $model->attribute_id]) ?>
        </div>
        <div class="col-xs-2">
            <?= Html::activeTextInput($model, "[{$key}]sort_order", ['value' => $model->sort_order ?? $model->attribute_id]) ?>
        </div>
        <div class="col-xs-2">
            <?= Html::a(Yii::t('attribute', 'Delete'), '#', ['class' => 'attribute-remove text-danger', 'data-target' => "attribute-{$key}", 'data-key' => $key]) ?>
        </div>
    </div>

<?
$script = <<<JS
    $(".attribute-remove").on("click", function() {
        var target = $(this).data("target");

        $("#" + target).remove();
    });
    
    $("#{$key}_selectizee").selectize();
JS;

$this->registerJs($script) ?>