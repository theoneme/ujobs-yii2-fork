<?php

use yii\helpers\Html;
use common\modules\attribute\Module;

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\AttributeGroup */
/* @var $attributes array */

$this->title = Yii::t('attribute', 'Update {modelClass}: ', [
        'modelClass' => 'Attribute Group',
    ]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('attribute', 'Attribute Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('attribute', 'Update');
?>

<div class="attribute-group-form">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'attributes' => $attributes
            ]) ?>
        </div>
    </div>
</div>
