<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.09.2017
 * Time: 12:18
 */

use backend\assets\SelectizeAsset;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

SelectizeAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel common\modules\attribute\models\search\AttributeGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('attribute', 'Attributes');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="attribute-group-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('attribute', 'Create {what}', [
                    'what' => Yii::t('attribute', 'Attribute')
                ]), ['create'], [
                    'class' => 'btn btn-success modal-edit',
                    'data-pjax' => 0,
                    'title' => Yii::t('attribute', 'Create {what}', ['what' => Yii::t('attribute', 'Attribute')])
                ]) ?>
            </p>
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'name',
                    'alias',
                    [
                        'header' => 'Находится в группах',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'max-width: 200px; white-space: pre-wrap; font-size: 12px;'],
                        'value' => function($model) {
                            return $model->getInGroups();
                        }
                    ],
                    [
                        'header' => 'Значения для проверки',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'white-space: pre-wrap; font-size: 12px; max-width: 600px;'],
                        'value' => function($model) {
                            return $model->formatValues();
                        }
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
