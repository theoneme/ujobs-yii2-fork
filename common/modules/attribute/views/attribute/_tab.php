<?php

use yii\widgets\ActiveForm;
use common\modules\attribute\models\AttributeDescription;

/* @var $model AttributeDescription */
/* @var $form ActiveForm */
/* @var string $locale */
?>

<div class="row">
	<div class="col-xs-12">
        <?= $form->field($model, "[$locale]title") ?>
	</div>
    <div class="col-xs-12">
        <?= $form->field($model, "[$locale]description") ?>
    </div>
    <?= $form->field($model, "[$locale]locale", ["options" => ["value"=> $locale] ])->hiddenInput()->label(false);?>
	<?= $form->field($model, "[$locale]entity_content")->hiddenInput()->label(false);?>
    <?= $form->field($model, "[$locale]id")->hiddenInput()->label(false);?>
</div>