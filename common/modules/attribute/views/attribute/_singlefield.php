<?php

use common\modules\attribute\models\Attribute;
use yii\helpers\Html;
use kartik\file\FileInput;

/* @var Attribute $model */

?>

<div class="row space" id="r_<?= $key ?>">
    <div class="col-md-5">
        <?php foreach ($model->translationsArr as $item) { ?>
            <div class="col-xs-12">

                <div class="input-group">
                    <div class="input-group-addon">
                        <?= $item->locale ?>
                    </div>
                    <?= Html::activeTextInput($item, "[value][$key][$item->locale]title", ['class' => 'form-control']) ?>
                </div>
            </div>
            <?= Html::activeHiddenInput($item, "[value][$key][$item->locale]locale") ?>
            <?= Html::activeHiddenInput($item, "[value][$key][$item->locale]entity_content") ?>
        <?php } ?>
    </div>
    <div class="col-md-6">
        <?= FileInput::widget([
            'model' => $model,
            'attribute' => "[$key]image",
            'options' => ['multiple' => false],
            'pluginOptions' => [
                'initialPreview' => [
                    !empty($model->image) ? Html::img($model->image, ['class' => 'file-preview-image', 'alt' => '', 'title' => '']) : null,
                ],
            ]
        ]); ?>

        <?php if ($model->image) { ?>
            <?= Html::activeHiddenInput($model, "[$key]image") ?>
        <?php } ?>
    </div>
    <div class="col-md-3">
        <?= Html::activeListBox($model, "[$key]sort_order", ['class' => 'form-control']) ?>
    </div>
    <div class="col-md-1">
        <a class="btn btn-danger btn-md remove-field" href="#"><?= Yii::t('attribute', 'Delete') ?></a>
    </div>
</div>