<?php

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\Attribute */

$this->title = Yii::t('attribute', 'Update {modelClass}: ', [
    'modelClass' => 'Attribute',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('attribute', 'Attributes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('attribute', 'Update');
?>
<div class="attribute-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
