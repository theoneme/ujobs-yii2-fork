<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\attribute\models\search\AttributeGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('attribute', 'Attributes');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="attribute-group-index">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('attribute', 'Create {what}', [
                    'what' => Yii::t('attribute', 'Attribute')
                ]), ['create'], [
                    'class' => 'btn btn-success modal-edit',
                    'data-pjax' => 0,
                    'title' => Yii::t('attribute', 'Create {what}', ['what' => Yii::t('attribute', 'Attribute')])
                ]) ?>
            </p>
            <?php Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'type',
                    'alias',
                    'is_searchable',
                    [
                        'header' => 'Находится в группах',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'max-width: 200px; white-space: pre-wrap; font-size: 12px;'],
                        'value' => function($model) {
                            return $model->getInGroups();
                        }
                    ],
                    [
                        'header' => 'Количество значений',
                        'value' => function($model) {
                            return count($model->values);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => '{values} {update} {delete}',
                        'buttons' => [
                            'values' => function ($url) {
                                return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, ['data-pjax' => 0]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('attribute', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                        'urlCreator' => function ($action, $model) {
                            $url = '';
                            switch ($action) {
                                case 'values':
                                    $url = Url::toRoute(["/attribute/attribute-value/index", 'AttributeValueSearch[attribute_id]' => $model->id]);
                                    break;
                                case 'update':
                                    $url = Url::toRoute(["/attribute/attribute/{$action}", 'id' => $model->id]);
                                    break;
                                case 'delete':
                                    $url = $model->is_system ? '#' : Url::toRoute(["/attribute/attribute/{$action}", 'id' => $model->id]);
                                    break;
                                default:
                                    break;
                            }
                            return $url;
                        }
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
