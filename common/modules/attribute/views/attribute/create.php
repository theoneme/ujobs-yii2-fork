<?php

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\Attribute */
/* @var $isAjax boolean */

$this->title = Yii::t('attribute', 'Create Attribute');
$this->params['breadcrumbs'][] = ['label' => Yii::t('attribute', 'Attributes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-create">

    <?= $this->render('_form', [
        'model' => $model,
        'isAjax' => $isAjax
    ]) ?>

</div>
