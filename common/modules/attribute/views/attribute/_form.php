<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Tabs;
use common\modules\attribute\Module;

/* @var $this yii\web\View */
/* @var $model common\modules\attribute\models\Attribute */
/* @var $form ActiveForm */
/* @var boolean $isAjax */

?>

    <div class="attribute-form">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'attribute-form',
                        'class' => ($isAjax ? 'ajax-submit' : ''),
                    ]
                ]); ?>

                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'name') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'is_system')->checkbox() ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'is_searchable')->checkbox() ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'is_primary')->checkbox() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map([
                            ['id' => 'list', 'text' => Yii::t('attribute', 'List'), 'group' => Yii::t('attribute', 'Select')],
                            ['id' => 'radiolist', 'text' => Yii::t('attribute', 'Radio button'), 'group' => Yii::t('attribute', 'Select')],
                            ['id' => 'checkbox', 'text' => Yii::t('attribute', 'Checkbox'), 'group' => Yii::t('attribute', 'Select')],
                            ['id' => 'checkboxlist', 'text' => Yii::t('attribute', 'Checkbox List'), 'group' => Yii::t('attribute', 'Select')],
                            ['id' => 'image', 'text' => Yii::t('attribute', 'Image'), 'group' => Yii::t('attribute', 'Select')],
                            ['id' => 'multitextbox', 'text' => Yii::t('attribute', 'Multiple Text Input'), 'group' => Yii::t('attribute', 'Input')],
                            ['id' => 'textbox', 'text' => Yii::t('attribute', 'Text input'), 'group' => Yii::t('attribute', 'Input')],
                            ['id' => 'numberbox', 'text' => Yii::t('attribute', 'Number input'), 'group' => Yii::t('attribute', 'Input')],
                            ['id' => 'numberbox-range', 'text' => Yii::t('attribute', 'Number input with range filter'), 'group' => Yii::t('attribute', 'Input')],
                            ['id' => 'textarea', 'text' => Yii::t('attribute', 'Text area'), 'group' => Yii::t('attribute', 'Input')],
                            ['id' => 'file', 'text' => Yii::t('attribute', 'File'), 'group' => Yii::t('attribute', 'File')],
                            ['id' => 'date', 'text' => Yii::t('attribute', 'Date'), 'group' => Yii::t('attribute', 'Date')],
                            ['id' => 'time', 'text' => Yii::t('attribute', 'Time'), 'group' => Yii::t('attribute', 'Date')],
                            ['id' => 'datetime', 'text' => Yii::t('attribute', 'Datetime'), 'group' => Yii::t('attribute', 'Date')]], 'id', 'text', 'group'),
                            ['prompt' => ' -- ' . Yii::t('attribute', 'Select option'), 'id' => 'field-dropdown']
                        )
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'alias')->textInput() ?>
                    </div>
                </div>

                <?php
                $tabs = [];
                foreach ($model->translationsArr as $key => $item) {
                    $tabItem = [
                        'label' => Yii::$app->params['languages'][$item->locale],
                        'content' => $this->render('_tab', ['model' => $item, 'form' => $form, 'locale' => $item->locale])
                    ];
                    if (Yii::$app->language == $item->locale) {
                        $tabItem['active'] = true;
                    }
                    $tabs[] = $tabItem;
                }
                ?>

                <?= Tabs::widget([
                    'items' => $tabs
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('attribute', 'Create') : Yii::t('attribute', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php $this->registerJs("
    var switcherEl = $('input[type=\'checkbox\']').switcher({language: 'ru'});
");