<?php

namespace common\modules\attribute;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package common\modules\attribute
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'common\modules\attribute\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        \Yii::$app->i18n->translations['attribute*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/modules/attribute/messages',

            'fileMap' => [
                'attribute' => 'attribute.php',
            ],
        ];
    }
}
