<?php

namespace common\modules\attribute\controllers;

use common\controllers\BackEndController;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeToGroup;
use Yii;
use common\modules\attribute\models\AttributeGroup;
use common\modules\attribute\models\search\AttributeGroupSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * AttributeGroupController implements the CRUD actions for AttributeGroup model.
 */
class AttributeGroupController extends BackEndController
{
    /**
     * Lists all AttributeGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributeGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AttributeGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AttributeGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttributeGroup();

        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $model->load($input);
            $model->attributeToGroup = $input['AttributeToGroup'];

            if($model->save()) {
                if (Yii::$app->request->isAjax) {
                    return true;
                }

                $this->redirect(['index']);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'isAjax' => true
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AttributeGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $attributes = ArrayHelper::map(Attribute::find()->where(['not', ['type' => 'core']])->select('id, name')->orderBy('name')->all(), 'id', 'name');

        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $model->load($input);
            $model->attributeToGroup = $input['AttributeToGroup'];

            if($model->save()) {
                if (Yii::$app->request->isAjax) {
                    return true;
                }

                return $this->redirect(['index']);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'isAjax' => true,
                'attributes' => $attributes
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'attributes' => $attributes
        ]);
    }

    /**
     * Deletes an existing AttributeGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AttributeGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttributeGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttributeGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionRenderAttribute($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $attributes = ArrayHelper::map(Attribute::find()->where(['not', ['type' => 'core']])->select('id, name')->orderBy('name')->all(), 'id', 'name');

        $model = new AttributeToGroup();

        $output = [
            'html' => $this->renderAjax('partial/attribute', [
                'model' => $model,
                'key' => (int)$id,
                'attributes' => $attributes
            ]),
            'success' => true
        ];

        return $output;
    }
}
