<?php

namespace common\modules\attribute\controllers;

use common\controllers\BackEndController;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\backend\MergeAttributeValueForm;
use common\modules\attribute\models\search\AttributeSearch;
use Yii;
use common\modules\attribute\models\AttributeValue;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\search\AttributeValueSearch;
use yii\web\NotFoundHttpException;

/**
 * AttributeValueController implements the CRUD actions for AttributeValue model.
 */
class AttributeValueController extends BackEndController
{
    /**
     * Lists all AttributeValue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributeValueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $currentAttribute = [];
        $currentAttributeId = (int)$_GET['AttributeValueSearch']['attribute_id'];
        if ($currentAttributeId) {
            $currentAttribute = Attribute::findOne($currentAttributeId);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currentAttribute' => $currentAttribute
        ]);
    }

    /**
     * Displays a single AttributeValue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AttributeValue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttributeValue();
        $model->attribute_id = Yii::$app->request->get('attribute_id', null);
        foreach (Yii::$app->params['languages'] as $locale => $title) {
            $content = new AttributeDescription();
            $content->locale = $locale;
            $model->translationsArr[] = $content;
        }

        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $model->load($input);
            $model->translationsArr = $input['AttributeDescription'];

            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    return true;
                }

                $this->redirect(['index', 'AttributeValueSearch[attribute_id]' => $model->attribute_id]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'isAjax' => true
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AttributeValue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->translationsArr = $model->translations;

        $mergeModel = new MergeAttributeValueForm(['attribute_id' => $model->attribute_id, 'attribute_value_id' => $id]);

        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $model->load($input);
            $model->translationsArr = $input['AttributeDescription'];

            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    return true;
                }

                return $this->redirect(['index', 'AttributeValueSearch[attribute_id]' => $model->attribute_id]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'isAjax' => true,
                'mergeModel' => $mergeModel
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'mergeModel' => $mergeModel
        ]);
    }

    /**
     * Deletes an existing AttributeValue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return bool
     */
    public function actionDeleteMultiple()
    {
        $pk = Yii::$app->request->post('pk');
        if (!$pk) {
            return false;
        }
        $models = AttributeValue::find()->where(['id' => $pk])->all();
        if ($models) {
            foreach ($models as $model) {
                $model->delete();
            }
        }
        return true;
    }

    /**
     * @return bool|\yii\web\Response
     */
    public function actionMerge()
    {
        $model = new MergeAttributeValueForm();

        $input = Yii::$app->request->post();
        if(!empty($input)) {
            $model->load($input);

            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Finds the AttributeValue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttributeValue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttributeValue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
