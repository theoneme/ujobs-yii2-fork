<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.06.2017
 * Time: 11:35
 */

namespace common\modules\store\events;

use yii\base\Event;

/**
 * Class CartEvent
 * @package common\modules\store\events
 */
class CartEvent extends Event
{
	public $cartData = [];
}