<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:35
 */

namespace common\modules\store\controllers;

use common\components\CurrencyHelper;
use common\controllers\FrontEndController;
use common\models\Offer;
use common\models\Payment;
use common\models\Tariff;
use common\modules\store\components\Cart;
use common\modules\store\models\PaymentMethod;
use dektrium\user\models\LoginForm;
use dektrium\user\models\RegistrationForm;
use common\modules\store\models\Order;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class CheckoutController
 * @package common\modules\store\controllers
 */
class CheckoutController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['order', 'tariff', 'replenish', 'offer'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['checkout', 'tariff-checkout', 'replenish-checkout', 'order', 'tariff', 'replenish', 'offer'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToOrder();
                        }
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'You are not allowed to access this page'));
                    return $this->redirect(['/site/index']);
                },
            ],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCheckout()
    {
        if (Yii::$app->cart->getCount() == 0) {
            return $this->redirect(['/store/cart/index']);
        }

        $this->layout = '@frontend/views/layouts/nomenu_layout';

        Url::remember(Url::current());

        $registerModel = Yii::createObject(RegistrationForm::class);
        $loginModel = Yii::createObject(LoginForm::class);

        $breadcrumbs = [Yii::t('store', 'Checkout')];

        $this->view->title = Yii::t('store', 'Checkout');

        $balanceAllowed = !isGuest() && Yii::$app->user->identity->getBalance() >= Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT);
        $paymentMethods = PaymentMethod::getMethodsByCountry(Yii::$app->user->identity->getCurrentId(), $balanceAllowed);

        $currentCheckoutStep = Yii::$app->user->isGuest ? 1 : 2;

        return $this->render('checkout', [
            'breadcrumbs' => $breadcrumbs,

            'loginModel' => $loginModel,
            'registerModel' => $registerModel,
            'paymentMethods' => $paymentMethods,
            'currentCheckoutStep' => $currentCheckoutStep
        ]);
    }

    /**
     * @param int $type
     * @param bool $shortcut
     * @return array
     */
    public function actionOrder($type = Payment::TYPE_JOB_PAYMENT, $shortcut = false)
    {
        /* @var $cart Cart */
        $cart = Yii::$app->cart;
        $response = ['success' => false, 'error' => Yii::t('store', 'Error occurred during order saving')];
        if ($cart->getCount() == 0) {
            $response['error'] = Yii::t('store', 'Your cart is empty');
        } else {
            $paymentMethod = PaymentMethod::find()->where(['id' => Yii::$app->request->post('payment_method_id')])->one();
            if($paymentMethod !== null) {

                /* @var ActiveRecord $firstItem */
                $cartItems = $cart->getItems();
                $firstItem = array_pop($cartItems);
                $className = $firstItem['item']::tableName();
                switch($className) {
                    case 'product':
                        $type = Payment::TYPE_PRODUCT_PAYMENT;
                        break;
                }

                $payment = new Payment();
                $payment->description = Yii::t('store', 'Payment for service on {0} site', Yii::$app->name);
                $payment->payment_method_id = $paymentMethod->id;
                $payment->user_id = Yii::$app->user->identity->getCurrentId();
                $payment->total = $cart->getTotal(Cart::ITEM_PRODUCT);
                $payment->type = $type;

                if ($payment->save()) {
                    $payment->transformCartItemsToOrders();
                    $response = ['success' => true];
                    if ($shortcut) {
                        $response['paymentId'] = $payment->id;
                    }
                    else {
                        $response['form'] = $this->renderPartial('@frontend/views/payments_forms/' . $payment->paymentMethod->code, [
                            'payment' => $payment
                        ]);
                    }
                }
            }
        }
        return $response;
    }

    /**
     * @return string
     */
    public function actionTariffCheckout()
    {
        /* @var $tariff Tariff*/
        $id = Yii::$app->request->post('id', 0);
        $tariff = Tariff::find()->where(['id' => $id])->one();
        if ($tariff !== null){
            $quantity = Yii::$app->request->post('quantity', 1);
            $total = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $tariff->cost * $quantity);
            $totalWithDiscount = Tariff::calculateTotalPrice($tariff->cost, $quantity);
            $discount = $total - $totalWithDiscount;
            $balanceAllowed = !isGuest() && Yii::$app->user->identity->getBalance() >= $totalWithDiscount;
            $paymentMethods = PaymentMethod::getMethodsByCountry(Yii::$app->user->identity->getCurrentId(), $balanceAllowed);

            return $this->render('tariff-checkout', [
                'item' => $tariff,
                'quantity' => $quantity,
                'total' => $total,
                'discount' => $discount,
                'paymentMethods' => $paymentMethods,
            ]);
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));
            return $this->redirect(['/']);
        }
    }

    /**
     * @param bool $shortcut
     * @return array
     */
    public function actionTariff($shortcut = false)
    {
        $response = ['success' => false, 'error' => Yii::t('store', 'Error occurred during order saving')];
        /* @var $tariff Tariff*/
        $tariff = Tariff::find()->where(['id' => Yii::$app->request->post('id')])->one();
        $paymentMethod = PaymentMethod::find()->where(['id' => Yii::$app->request->post('payment_method_id')])->one();
        $quantity = Yii::$app->request->post('quantity', 1);
        if($tariff !== null && $paymentMethod !== null) {
            $payment = new Payment();
            $payment->description = Yii::t('store', 'Getting premium on {0} site', Yii::$app->name);
            $payment->payment_method_id = $paymentMethod->id;
            $payment->type = Payment::TYPE_ACCESS_PAYMENT;
            $payment->user_id = Yii::$app->user->identity->getCurrentId();

            $totalWithDiscount = Tariff::calculateTotalPrice($tariff->cost, $quantity);
            $payment->total = $totalWithDiscount;

            if ($payment->save()) {
                $payment->transformTariff($tariff, $quantity);
                Yii::$app->session->setFlash('order_id', Yii::t('store', $payment->id));
                $response = ['success' => true];
                if ($shortcut) {
                    $response['paymentId'] = $payment->id;
                }
                else {
                    $response['form'] = $this->renderPartial('@frontend/views/payments_forms/' . $payment->paymentMethod->code, [
                        'payment' => $payment
                    ]);
                }
            }
        }
        return $response;
    }

    /**
     * @return string
     */
    public function actionReplenishCheckout()
    {
        $amount = Yii::$app->request->post('amount', 0);
        $currency_code = Yii::$app->request->post('currency_code');
        if ($amount > 0 && array_key_exists($currency_code, Yii::$app->params['currencies'])) {
            $paymentMethods = PaymentMethod::getMethodsByCountry(Yii::$app->user->identity->getCurrentId(), false);
            return $this->render('replenish-checkout', [
                'amount' => $amount,
                'paymentMethods' => $paymentMethods,
                'currency_code' => $currency_code
            ]);
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Please set the amount of replenishment'));
            return $this->redirect(['/account/payment/index']);
        }
    }

    /**
     * @param bool $shortcut
     * @return array
     */
    public function actionReplenish($shortcut = false)
    {
        $response = ['success' => false, 'error' => Yii::t('store', 'Error occurred during order saving')];
        $paymentMethod = PaymentMethod::find()->where(['id' => Yii::$app->request->post('payment_method_id')])->one();
        $currency_code = Yii::$app->request->post('currency_code', 'RUB');
        $amount = Yii::$app->request->post('amount', '0');
        if($amount > 0 && $paymentMethod !== null && array_key_exists($currency_code, Yii::$app->params['currencies'])) {
            $payment = new Payment();
            $payment->description = Yii::t('store', 'Account replenishment on {0} site', Yii::$app->name);
            $payment->type = Payment::TYPE_ACCOUNT_REPLENISHMENT;
            $payment->user_id = Yii::$app->user->identity->getCurrentId();
            $payment->total = $amount;
            $payment->currency_code = $currency_code;
            $payment->payment_method_id = $paymentMethod->id;

            if ($payment->save()) {
                Yii::$app->session->setFlash('order_id', Yii::t('store', $payment->id));
                $response = ['success' => true];
                if ($shortcut) {
                    $response['paymentId'] = $payment->id;
                }
                else {
                    $response['form'] = $this->renderPartial('@frontend/views/payments_forms/' . $payment->paymentMethod->code, [
                        'payment' => $payment
                    ]);
                }
            }
        }
        return $response;
    }

    /**
     * @param bool $shortcut
     * @return array
     */
    public function actionOffer($shortcut = false)
    {
        /* @var $offer Offer*/
        $response = ['success' => false, 'error' => Yii::t('store', 'Error occurred during order saving')];
        $offer = Offer::find()->where(['id' => Yii::$app->request->post('id')])->one();
        $paymentMethod = PaymentMethod::find()->where(['id' => Yii::$app->request->post('payment_method_id')])->one();
        if($offer !== null && $paymentMethod !== null) {
            $payment = new Payment();
            $payment->description = Yii::t('store', 'Payment for service on {0} site', Yii::$app->name);
            $payment->payment_method_id = $paymentMethod->id;
            $payment->type = Payment::TYPE_JOB_PAYMENT;
            $payment->user_id = Yii::$app->user->identity->getCurrentId();
            $payment->total = CurrencyHelper::convert($offer->currency_code, Yii::$app->params['app_currency_code'], $offer->price, true);
            if ($payment->save()) {
                $order = new Order();
                $order->total = $offer->price;
                $order->payment_method_id = $payment->payment_method_id;
                $order->cartItems[] = ['item' => $offer, 'quantity' => 1, 'options' => []];
                $order->seller_id = $offer->from_id;
                $order->product_type = Order::TYPE_OFFER;
                $order->payment_id = $payment->id;
                $order->save();
                Yii::$app->session->setFlash('order_id', Yii::t('store', $payment->id));
                $response = ['success' => true];
                if ($shortcut) {
                    $response['paymentId'] = $payment->id;
                }
                else {
                    $response['form'] = $this->renderPartial('@frontend/views/payments_forms/' . $paymentMethod->code, [
                        'payment' => $payment
                    ]);
                }
            }
        }
        return $response;
    }
}