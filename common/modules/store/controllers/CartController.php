<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:35
 */

namespace common\modules\store\controllers;

use common\components\CurrencyHelper;
use common\controllers\FrontEndController;
use common\models\JobExtra;
use common\models\JobPackage;
use common\models\Offer;
use common\models\Page;
use common\models\Tariff;
use common\modules\board\models\Product;
use common\modules\store\components\Cart;
use Yii;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class CartController
 * @package common\modules\store\controllers
 */
class CartController extends FrontEndController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $fairPlayPage = Page::find()->joinWith(['translations'])->where(['alias' => 'fair-play'])->one();

        $cart = Yii::$app->cart;
        $items = $cart->getItems(Cart::ITEM_PRODUCT);
        $items = $this->appendTotalsToCartItems($items);
        Url::remember(Url::to(['/store/checkout/checkout']), 'returnUrl3');

        $breadcrumbs = [
            Yii::t('store', 'Cart')
        ];

        $this->view->title = Yii::t('store', 'Cart');

        return $this->render('cart', [
            'items' => $items,
            'breadcrumbs' => $breadcrumbs,
            'fairPlayPage' => $fairPlayPage
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionTariff($id)
    {
        /* @var $tariff Tariff */
        $tariff = Tariff::find()->where(['id' => $id])->one();
        if ($tariff !== null) {
            $quantity = Yii::$app->request->post('quantity', 1);
            $total = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $tariff->cost * $quantity);
            $totalWithDiscount = Tariff::calculateTotalPrice($tariff->cost, $quantity);
            $discount = $total - $totalWithDiscount;
            return $this->render('tariff', [
                'item' => $tariff,
                'quantity' => $quantity,
                'total' => $total,
                'discount' => $discount
            ]);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested page is not found'));
            return $this->redirect(['/']);
        }
    }

    /**
     * @return mixed
     */
    public function actionAdd()
    {
        $cart = Yii::$app->cart;

        $entity = Yii::$app->request->post('entity', 'job');
        switch ($entity) {
            case 'product':
                /* @var Product $item */
                $item = Product::findOne((int)Yii::$app->request->post('id', null));
                $options = [];
                break;
            default:
                /* @var JobPackage $item */
                $item = JobPackage::findOne((int)Yii::$app->request->post('id', null));

                parse_str($_POST['options'] ?? '', $parsedOpts);
                $options = [];
                if ($parsedOpts != null) {
                    foreach ($parsedOpts['extra'] as $key => $option) {
                        if ($option['enabled']) {
                            /* @var JobExtra $extra */
                            $extra = JobExtra::find()->where(['id' => $key])->one();

                            if ($extra && $extra->job_id == $item->job->id) {
                                $options[$key] = [
                                    'id' => $key,
                                    'title' => $extra->title,
                                    'description' => $extra->description,
                                    'quantity' => $option['quantity'],
                                    'price' => $extra->price,
                                ];
                            }
                        }
                    }
                }
                break;
        }

        if ($item !== null) {
            $cart->add($item, true, Yii::$app->request->post('quantity', 1), $options);
        }

        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        return $this->renderPartial('@common/modules/store/components/views/popup-cart', [
            'items' => $items
        ]);
    }

    /**
     * @return mixed
     */
    public function actionAddPixel()        // temporary
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $item = JobPackage::findOne((int)Yii::$app->request->post('id', null));
        $cart = Yii::$app->cart;
        parse_str($_POST['options'] ?? '', $parsedOpts);
        $options = [];
        if ($parsedOpts != null) {
            foreach ($parsedOpts['extra'] as $key => $option) {
                if ($option['enabled']) {
                    /* @var JobExtra $extra */
                    $extra = JobExtra::findOne($key);

                    if ($extra && $extra->job_id == $item->job->id) {
                        $options[$key] = [
                            'id' => $key,
                            'title' => $extra->title,
                            'description' => $extra->description,
                            'quantity' => $option['quantity'],
                            'price' => $extra->getPrice(),
                        ];
                    }
                }
            }
        }

        if ($item !== null) {
            $cart->add($item, true, Yii::$app->request->post('quantity', 1), $options);
        }

        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        return [
            'html' => $this->renderPartial('@common/modules/store/components/views/popup-cart', [
                'items' => $items
            ]),
            'pixelInfo' => [
                'price' => $item->price,
                'content_type' => 'job',
                'content_ids' => $item->job->id
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function actionUpdate()
    {
        $cart = Yii::$app->cart;
        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        $entity = Yii::$app->request->post('entity', 'job');
        switch ($entity) {
            case 'product':
                /* @var Product $item */
                $item = Product::findOne((int)Yii::$app->request->post('id', null));
                $options = [];
                break;
            default:
                /* @var JobPackage $item */
                $item = JobPackage::findOne((int)Yii::$app->request->post('id', null));

                $cartItem = $items[$item->id];
                $options = $cartItem['options'];

                $optionId = Yii::$app->request->post('optionId', null);
                $optionItem = $optionId ? JobExtra::find()->where(['id' => $optionId, 'job_id' => $item->job->id])->one() : null;

                if ($optionItem != null) {
                    $options[$optionItem->id] = [
                        'id' => $optionItem->id,
                        'title' => $optionItem->title,
                        'description' => $optionItem->description,
                        'quantity' => Yii::$app->request->post('optionQuantity', 1),
                        'price' => $optionItem->price,
                    ];
                }
                break;
        }

        if ($item !== null) {
            $cart->update($item, true, Yii::$app->request->post('quantity', 1), $options);
        }

        return $this->renderPartial('@common/modules/store/components/views/popup-cart', [
            'items' => $items
        ]);
    }

    /**
     * @return mixed
     */
    public function actionRemove()
    {
        $cart = Yii::$app->cart;
        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        /* @var JobPackage $item */
        $item = JobPackage::findOne((int)Yii::$app->request->post('id', null));

        $itemId = Yii::$app->request->post('id', null);
        $optionId = Yii::$app->request->post('optionId', null);
        if ($optionId != null) {
            $cartItem = $items[$item->id];
            $options = $cartItem['options'];
            unset($options[$optionId]);

            $cart->update($item, true, Yii::$app->request->post('quantity', 1), $options);
        } elseif ($itemId) {
            $cart->remove($itemId);
        }

        return $this->renderPartial('@common/modules/store/components/views/popup-cart', [
            'items' => $items
        ]);
    }

    /**
     * @return mixed
     */
    public function actionAddOffer()
    {
        $cart = Yii::$app->cart;
        $item = Offer::findOne((int)Yii::$app->request->post('id', null));
        if ($item !== null) {
            $cart->add($item, true, (int)Yii::$app->request->post('quantity', 1));
        }
        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        return $this->renderPartial('@common/modules/store/components/views/popup-cart', [
            'items' => $items
        ]);
    }

    /**
     * @return mixed
     */
    public function actionUpdateOffer()
    {
        $cart = Yii::$app->cart;
        $item = Offer::findOne((int)Yii::$app->request->post('id', null));
        if ($item !== null) {
            $cart->update($item, true, (int)Yii::$app->request->post('quantity', 1));
        }
        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        return $this->renderPartial('@common/modules/store/components/views/popup-cart', [
            'items' => $items
        ]);
    }

    /**
     * @return mixed
     */
    public function actionRemoveOffer()
    {
        $cart = Yii::$app->cart;
        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        /* @var Offer $item */
        $item = Offer::findOne((int)Yii::$app->request->post('id', null));
        if ($item !== null) {
            $cart->remove($item->getUniqueId());
        }

        return $this->renderPartial('@common/modules/store/components/views/popup-cart', [
            'items' => $items
        ]);
    }

    /**
     *
     */
    public function actionClear()
    {
        Yii::$app->cart->clear();
    }

    /**
     * @param array $items
     * @return array
     */
    protected function appendTotalsToCartItems($items = [])
    {
        if (!empty($items)) {
            foreach ($items as $key => $item) {
                $totals = CurrencyHelper::convert($item['item']->currency_code, Yii::$app->params['app_currency_code'], $item['item']->price, true) * $item['quantity'];

                if (!empty($options = $item['options'])) {
                    foreach ($options as $option) {
                        $totals += CurrencyHelper::convert($item['item']->currency_code, Yii::$app->params['app_currency_code'], $option['price'], true) * $option['quantity'];
                    }
                }
                $items[$key]['total'] = $totals;
            }
        }
        return $items;
    }
}