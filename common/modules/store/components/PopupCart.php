<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 15:36
 */

namespace common\modules\store\components;

use Yii;
use yii\base\Widget;

/**
 * Class PopupCart
 * @package common\modules\store\components
 */
class PopupCart extends Widget
{
    /**
     * @var string
     */
    public $template = 'popup-cart';

    /**
     * @return string
     */
    public function run()
    {
        $cart = Yii::$app->cart;

        $items = $cart->getItems(Cart::ITEM_PRODUCT);

        return $this->render($this->template, [
            'items' => $items
        ]);
    }
}