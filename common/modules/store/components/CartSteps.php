<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2017
 * Time: 14:50
 */

namespace common\modules\store\components;

use Yii;
use yii\base\Widget;

/**
 * Class CartSteps
 * @package common\modules\store\components
 */
class CartSteps extends Widget
{
    const STEP_CHOOSE_GOODS = 1;
    const STEP_CART = 2;
    const STEP_CHOOSE_PAYMENT_METHOD = 3;
    const STEP_START_ORDER = 4;

    const STEP_STATUS_COMPLETED = 'completed';
    const STEP_STATUS_ACTIVE = 'active';

    /**
     * @var string
     */
    public $template = 'cart-steps';

    /**
     * @var array
     */
    public $steps = [];

    /**
     * @var array
     */
    private $mobileSteps = [
        self::STEP_CART,
        self::STEP_CHOOSE_PAYMENT_METHOD
    ];

    /**
     * @var int
     */
    public $step = self::STEP_CHOOSE_GOODS;

    /**
     * @var bool
     */
    public $isMobile = false;

    /**
     * CartSteps constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $cart = Yii::$app->cart;
        $itemsCount = count($cart->getItems(Cart::ITEM_PRODUCT));

        $this->steps = [
            self::STEP_CHOOSE_GOODS => Yii::t('store', 'Choose goods'),
            self::STEP_CART => Yii::t('store', 'Cart ({count})', ['count' => $itemsCount]),
            self::STEP_CHOOSE_PAYMENT_METHOD => Yii::t('store', 'Payment method'),
            self::STEP_START_ORDER => Yii::t('store', 'Submit requirements and start order')
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        $config = [];
        foreach ($this->steps as $key => $step) {
            if($this->isMobile === false || ($this->isMobile === true && in_array($key, $this->mobileSteps))) {
                $config[$key] = [
                    'status' => $key < $this->step ? self::STEP_STATUS_COMPLETED : ($key === $this->step ? self::STEP_STATUS_ACTIVE : ''),
                    'title' => $step
                ];
            }
        }

        return $this->render($this->template, [
            'steps' => $config,
            'isMobile' => $this->isMobile
        ]);
    }
}