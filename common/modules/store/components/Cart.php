<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:38
 */

namespace common\modules\store\components;

use common\components\CurrencyHelper;
use common\modules\store\events\CartEvent;
use common\modules\store\models\CartItemInterface;
use common\modules\store\models\storage\DatabaseStorage;
use common\modules\store\models\storage\SessionStorage;
use common\modules\store\models\storage\StorageInterface;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class Cart extends Component
{
    const ITEM_PRODUCT = 'common\modules\store\models\CartItemInterface';
    const EVENT_CART_UPDATE = 'cartUpdate';
    public $storageClass = 'common\modules\store\models\storage\DatabaseStorage';
//    public $storageClass = 'common\modules\store\models\storage\SessionStorage';
    protected $items;
    /**
     * @var DatabaseStorage|SessionStorage
     */
    private $storage = null;

    /**
     * Assigns cart to logged in user
     *
     * @param string
     * @param string
     *
     * @return void
     */
    public function reassign($sessionId, $userId)
    {
        if (get_class($this->getStorage()) === 'common\modules\store\models\storage\DatabaseStorage') {
            if (!empty($this->items)) {
                $storage = $this->getStorage();
                $storage->reassign($sessionId, $userId);
                self::init();
            }
        }
    }

    /**
     * @return DatabaseStorage|SessionStorage
     */
    protected function getStorage()
    {
        return $this->storage;
    }

    /**
     * Setter for the storage component
     *
     * @param StorageInterface|string $storage
     *
     * @return Cart
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->clear(false);
        $this->setStorage(Yii::createObject($this->storageClass));
        $this->items = $this->storage->load($this);
    }

    /**
     * Delete all items from the cart
     *
     * @param bool $save
     *
     * @return $this
     */
    public function clear($save = true)
    {
        $this->items = [];
        $save && $this->storage->save($this);
        return $this;
    }

    /**
     * Add an item to the cart
     *
     * @param CartItemInterface $item
     * @param bool $save
     * @param integer $quantity
     * @param array $options
     *
     * @return $this
     */
    public function add(CartItemInterface $item, $save = true, $quantity = 1, $options = [])
    {
        $quantity = (int)$quantity;
        $quantity = $quantity > 0 ? $quantity : 1;
        $this->addItem($item, $quantity, $options);
        $save && $this->storage->save($this);
        return $this;
    }

    /**
     * @param CartItemInterface $item
     * @param integer $quantity
     * @param array $options
     */
    protected function addItem(CartItemInterface $item, $quantity = 1, $options = [])
    {
        $uniqueId = $item->getUniqueId();
        $cartEvent = new CartEvent([
            'cartData' => $this->items
        ]);

        $this->trigger(self::EVENT_CART_UPDATE, $cartEvent);
        if (isset($this->items[$uniqueId])) {
            $this->items[$uniqueId]['quantity'] += $quantity;
        } else {
            $this->items[$uniqueId] = [
                'item' => $item,
                'quantity' => $quantity,
                'options' => $options
            ];
        }
    }

    /**
     * Update item quantity
     *
     * @param CartItemInterface $item
     * @param bool $save
     * @param integer $quantity
     *
     * @return $this
     */
    public function update(CartItemInterface $item, $save = true, $quantity = 1, $options = [])
    {
        $quantity = (int)$quantity;
        $quantity = $quantity > 0 ? $quantity : 1;
        $uniqueId = $item->getUniqueId();
        $this->items[$uniqueId] = [
            'item' => $item,
            'quantity' => $quantity,
            'options' => $options
        ];
        $save && $this->storage->save($this);
        return $this;
    }

    /**
     * Removes an item from the cart
     *
     * @param string $uniqueId
     * @param bool $save
     * @return $this
     */
    public function remove($uniqueId, $save = true)
    {
        if (isset($this->items[$uniqueId])) {
            unset($this->items[$uniqueId]);
            $save && $this->storage->save($this);
            //throw new InvalidParamException('Item not found');
        }
        return $this;
    }

    /**
     * @param string $itemType If specified, only items of that type will be counted
     *
     * @return int
     */
    public function getCount($itemType = null)
    {
        return array_sum(ArrayHelper::getColumn($this->getItems($itemType), 'quantity'));
    }

    /**
     * Returns all items of a given type from the cart
     *
     * @param string $itemType One of self::ITEM_ constants
     *
     * @return CartItemInterface[]
     */
    public function getItems($itemType = null)
    {
        $items = $this->items;
        if (!is_null($itemType)) {
            $items = array_filter($items,
                function ($item) use ($itemType) {
                    return is_subclass_of($item['item'], $itemType);
                }
            );
        }
        return $items;
    }

    /**
     * Finds all items of type $itemType, sums the values of $attribute of all models and returns the sum.
     *
     * @param string $attribute
     * @param string|null $itemType
     * @param bool|null $with_quantity
     *
     * @return integer
     */
    public function getTotal($itemType = null)
    {
        $sum = 0;
        foreach ($this->getItems($itemType) as $item) {
            $sum += CurrencyHelper::convert($item['item']->currency_code, Yii::$app->params['app_currency_code'], $item['item']->price, true) * $item['quantity'];
            if (!empty($item['options'])) {
                foreach ($item['options'] as $key => $option) {
                    $sum += CurrencyHelper::convert($item['item']->currency_code, Yii::$app->params['app_currency_code'], $option['price'], true) * (int) $option['quantity'];
                }
            }
        }

        return $sum;
    }

    /**
     * Finds all items of type $itemType, sums the values of $attribute of all models and returns the sum.
     *
     * @param string $attribute
     * @param string|null $itemType
     * @param bool|null $with_quantity
     *
     * @return integer
     */
    public function getAttributeTotal($attribute, $itemType = null, $with_quantity = false)
    {
        $sum = 0;
        foreach ($this->getItems($itemType) as $item) {
            $sum += $item['item']->{$attribute} * ($with_quantity ? $item['quantity'] : 1);
            if (!empty($item['options'])) {
                foreach ($item['options'] as $key => $option) {
                    $sum += $option['price'] * $option['quantity'];
                }
            }
        }
        return $sum;
    }
}