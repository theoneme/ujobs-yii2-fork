<?php
use common\components\CurrencyHelper;
use common\modules\store\components\Cart;
use common\modules\store\models\CartItemInterface;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $total integer
 */
?>

    <div id="cart-head-container" class="cart-has-items">
        <a id="carthead-but" class="icon-profnav text-center hint--bottom" data-hint="<?= Yii::t('app', 'Shopping cart') ?>">
            <?php if (!empty($items)) { ?>
                <div class="header-cart-amount text-center">
                    <?= Yii::$app->cart->getCount(Cart::ITEM_PRODUCT) ?>
                </div>
            <?php } ?>
            <i class="fa fa-shopping-cart icon-act" aria-hidden="true"></i>
        </a>
        <div class="cart-head">
            <?php if (!empty($items)) { ?>
                <?php foreach ($items as $item) {
                    /* @var $item_model CartItemInterface */
                    $item_model = $item['item'];
                    ?>
                    <div class="cart-head-item">
                        <div class="ch-img">
                            <?= Html::img($item_model->getThumb('catalog'), [
                                'alt' => $item_model->getLabel()
                            ]) ?>
                        </div>
                        <div class="ch-name">
                            <a href="<?= $item_model->getUrl() ?>">
                                <?= $item_model->getLabel() . " x " . $item['quantity'] ?>
                            </a>
                        </div>
                        <div class="ch-price text-right">
                            <?= CurrencyHelper::convertAndFormat($item_model->currency_code, Yii::$app->params['app_currency_code'], $item_model->price * $item['quantity'], true) ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <p>&nbsp;</p>
                <div class="text-center"><?= Yii::t('store', 'Cart Is Empty') ?></div>
            <?php } ?>
            <div class="view-cart text-right">
                <a class="btn-small"
                   href="<?= Url::to(['/store/cart/index']) ?>"><?= Yii::t('store', 'View Cart') ?></a>
            </div>
        </div>
    </div>

<?php $this->registerJs("
    $(document).on('mouseenter', '.cart-ico', function() {
        $('.cart-ico .cart-show').show();
    });

    $(document).on('mouseleave', '.cart-ico', function() {
        $('.cart-ico .cart-show').hide();
    });

    ",
    yii\web\View::POS_READY,
    'cart-hover');
