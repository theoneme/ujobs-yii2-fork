<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2017
 * Time: 14:50
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $steps array */
/* @var $isMobile boolean */

?>

<div class="grey-steps <?= $isMobile === false ? 'hidden-xs' : 'hidden visible-xs' ?>">
    <div class="container-fluid">
        <ul class="gig-steps cart-steps">
            <?php foreach ($steps as $step) { ?>
                <li class="<?= $step['status'] ?>">
                    <a href="#"><?= $step['title'] ?></a>
                </li>
            <?php } ?>
        </ul>
        <div class="step-continue hidden-xs">
            <?= Html::a(Yii::t('store', 'Keep Shopping'), Url::to(['/category/job-catalog']), [
                'class' => 'link-blue bright',
            ]) ?>
        </div>
    </div>
</div>
