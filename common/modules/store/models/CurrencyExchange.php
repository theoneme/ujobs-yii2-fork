<?php

namespace common\modules\store\models;

use Yii;

/**
 * This is the model class for table "currency_exchange".
 *
 * @property string $code_from
 * @property string $code_to
 * @property string $ratio
 *
 * @property Currency $codeFrom
 * @property Currency $codeTo
 */
class CurrencyExchange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_exchange';
    }

    /**
     * @param $condition
     * @param $save bool
     * @return CurrencyExchange
     */
    public static function findOrCreate($condition, $save = true)
    {
        $attribute = CurrencyExchange::find()->where($condition)->one();
        if ($attribute === null) {
            $attribute = new CurrencyExchange($condition);
            if ($save) {
                $attribute->save();
            }
        }
        return $attribute;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_from', 'code_to'], 'required'],
            [['ratio'], 'number'],
            [['code_from', 'code_to'], 'string', 'max' => 3],
            [['code_from'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['code_from' => 'code']],
            [['code_to'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['code_to' => 'code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_from' => Yii::t('store', 'Code From'),
            'code_to' => Yii::t('store', 'Code To'),
            'ratio' => Yii::t('store', 'Ratio'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeFrom()
    {
        return $this->hasOne(Currency::class, ['code' => 'code_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeTo()
    {
        return $this->hasOne(Currency::class, ['code' => 'code_to']);
    }
}
