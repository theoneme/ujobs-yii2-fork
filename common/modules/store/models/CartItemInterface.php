<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:36
 */

namespace common\modules\store\models;

/**
 * Interface CartItemInterface
 * @package common\modules\store\models
 */
interface CartItemInterface
{
    /**
     * Returns the price for the cart item
     * @return integer
     */
    public function getPrice();

    /**
     * Returns the label for the cart item (displayed in cart etc)
     * @return string
     */
    public function getLabel();

    /**
     * Returns unique id to associate cart item with product
     * @return string
     */
    public function getUniqueId();

    /**
     * Returns image for the cart item
     * @param string $target
     * @param boolean $fromAws
     * @return mixed
     */
    public function getThumb($target = null, $fromAws = true);

    /**
     * Returns description for the cart item
     * @return string
     */
    public function getDescription();

    /**
     * Returns url for the cart item
     * @return string
     */
    public function getUrl();

    /**
     * Returns requirements for the cart item
     * @return string
     */
    public function getRequirements();

    /**
     * Returns seller id for the cart item
     * @return string
     */
    public function getSellerId();

    /**
     * Returns seller profile url for the cart item
     * @return string
     */
    public function getSellerUrl();

    /**
     * Returns seller profile name for the cart item
     * @return string
     */
    public function getSellerName();
}