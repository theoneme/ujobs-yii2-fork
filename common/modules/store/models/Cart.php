<?php

namespace common\modules\store\models;

/**
 * This is the model class for table "cart".
 *
 * @property string $sessionId
 * @property string $cartData
 * @property integer $updated_at
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sessionId', 'cartData'], 'required'],
            [['cartData'], 'string'],
            [['updated_at'], 'integer'],
            [['sessionId'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sessionId' => 'Session ID',
            'cartData' => 'Cart Data',
            'updated_at' => 'Updated At',
        ];
    }
}
