<?php

namespace common\modules\store\models;

use common\behaviors\BlameableBehavior;
use common\components\CurrencyHelper;
use common\models\Attachment;
use common\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $seller_id
 * @property integer $customer_id
 * @property string $content
 * @property integer $status
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sender_id
 * @property string $custom_data
 *
 * @property Order $order
 * @property User $sender
 */
class OrderHistory extends \yii\db\ActiveRecord
{
    const TYPE_MANUAL_CANCEL = -10;
    const TYPE_CANCEL = -5;
    const TYPE_CUSTOM_MESSAGE = 0;
    const TYPE_ORDER_CREATED = 3;
    const TYPE_ORDER_PAID = 4;
    const TYPE_REQUIREMENTS_HEADER = 5;
    const TYPE_NEED_REQUIREMENTS = 10;
    const TYPE_POST_REQUIREMENTS = 15;
    const TYPE_REQUIREMENTS_DENY = 20;
    const TYPE_REQUIREMENTS_ACCEPT = 25;
    const TYPE_START_HEADER = 30;
    const TYPE_JOB_START = 35;
    const TYPE_JOB_COMPLETED = 40;
    const TYPE_MANUAL_COMPLETE = 41;
    const TYPE_JOB_DENY = 45;
    const TYPE_JOB_ACCEPT = 50;
    const TYPE_DELIVERY_HEADER = 55;
    const TYPE_DELIVERY = 60;
    const TYPE_REVIEW_HEADER = 65;
    const TYPE_REVIEW_SELLER = 70;
    const TYPE_REVIEW_BUYER = 75;

    const TYPE_TENDER_ADVANCED_STOP = -105;
    const TYPE_TENDER_RESPONSE_CANCEL = -6;
    const TYPE_TENDER_RESPONSE_HEADER = 100;
    const TYPE_TENDER_RESPONSE_MESSAGE = 105;
    const TYPE_TENDER_RESPONSE_PRICE = 106;
    const TYPE_TENDER_RESPONSE_PRICE_ACCEPT = 107;
    const TYPE_TENDER_WORKER_APPROVED_HEADER = 110;
    const TYPE_TENDER_SELLER_APPROVED_HEADER = 111;
    const TYPE_TENDER_WORKER_DENIED_HEADER = 115;
    const TYPE_TENDER_SELLER_DENIED_HEADER = 116;
    const TYPE_TENDER_PAID = 120;
    const TYPE_TENDER_PRODUCT_PAID = 121;
    const TYPE_TENDER_WEEKLY_TASK = 125;
    const TYPE_TENDER_PRODUCT_DETAILS = 126;
    const TYPE_TENDER_WEEKLY_TASK_ACCEPT = 130;
    const TYPE_TENDER_WEEKLY_TASK_DENY = 135;
    const TYPE_TENDER_WEEKLY_REPORT_HEADER = 140;
    const TYPE_TENDER_WEEKLY_REPORT = 145;
    const TYPE_TENDER_REPORT_ACCEPT_HEADER = 146;
    const TYPE_TENDER_PRODUCT_REPORT = 147;
    const TYPE_TENDER_WEEKLY_REPORT_ACCEPT_HEADER = 150;
    const TYPE_TENDER_WEEKLY_REPORT_DENY_HEADER = 155;
    const TYPE_TENDER_PRODUCT_REPORT_DENY_HEADER = 156;

    const TYPE_PRODUCT_CANCEL = 200;
    const TYPE_PRODUCT_SET_SHIPPING = 205;
    const TYPE_PRODUCT_SET_SHIPPING_DENY = 206;
    const TYPE_PRODUCT_SEND_PRODUCT = 210;
    const TYPE_PICKUP_PRODUCT = 215;
    const TYPE_ACCEPT_PRODUCT = 220;
    const TYPE_DENY_PRODUCT = 225;

    const ICON_CREATED = 'created';
    const ICON_PAID = 'paid';
    const ICON_REQUIRE = 'require';
    const ICON_CANCEL = 'cancel';
    const ICON_START = 'start';
    const ICON_DELIVERY = 'delivery';
    const ICON_REVIEW = 'review';
    /**
     * @var array
     */
    public $customDataArray = [];
    /**
     * @var bool
     */
    public $isBlameable = true;

    public $statusesToViews = [
        self::TYPE_MANUAL_CANCEL => 'header',
        self::TYPE_MANUAL_COMPLETE => 'header',
        self::TYPE_CANCEL => 'message-with-header',
        self::TYPE_CUSTOM_MESSAGE => 'simple-message',
        self::TYPE_REQUIREMENTS_HEADER => 'header',
        self::TYPE_NEED_REQUIREMENTS => 'greetings',
        self::TYPE_POST_REQUIREMENTS => 'message-with-header',
        self::TYPE_REQUIREMENTS_DENY => 'message-with-header',
        self::TYPE_REQUIREMENTS_ACCEPT => 'message-with-header',
        self::TYPE_JOB_DENY => 'message-with-header',
        self::TYPE_JOB_ACCEPT => 'header',
        self::TYPE_DELIVERY => 'message-with-header',
        self::TYPE_DELIVERY_HEADER => null, //deprecated
        self::TYPE_REVIEW_SELLER => 'review-seller',
        self::TYPE_REVIEW_BUYER => 'review-buyer',
        self::TYPE_ORDER_CREATED => 'header',
        self::TYPE_ORDER_PAID => 'header',

        self::TYPE_TENDER_ADVANCED_STOP => 'header',
        self::TYPE_TENDER_RESPONSE_CANCEL => 'header',
        self::TYPE_TENDER_RESPONSE_HEADER => 'header',
        self::TYPE_TENDER_RESPONSE_MESSAGE => 'greetings',
        self::TYPE_TENDER_RESPONSE_PRICE => 'trade-message',
        self::TYPE_TENDER_RESPONSE_PRICE_ACCEPT => 'header',
        self::TYPE_TENDER_WORKER_APPROVED_HEADER => 'header',
        self::TYPE_TENDER_SELLER_APPROVED_HEADER => 'header',
        self::TYPE_TENDER_WORKER_DENIED_HEADER => 'header',
        self::TYPE_TENDER_SELLER_DENIED_HEADER => 'header',
        self::TYPE_TENDER_PAID => 'header',
        self::TYPE_TENDER_PRODUCT_PAID => 'header',
        self::TYPE_TENDER_WEEKLY_TASK => 'simple-message',
        self::TYPE_TENDER_PRODUCT_DETAILS => 'simple-message',
        self::TYPE_TENDER_WEEKLY_TASK_ACCEPT => 'header',
        self::TYPE_TENDER_WEEKLY_TASK_DENY => 'tender-weekly-task-deny-header',
        self::TYPE_TENDER_WEEKLY_REPORT => 'message-with-header',
        self::TYPE_TENDER_PRODUCT_REPORT => 'message-with-header',
        self::TYPE_TENDER_REPORT_ACCEPT_HEADER => 'header',
        self::TYPE_TENDER_WEEKLY_REPORT_ACCEPT_HEADER => 'header',
        self::TYPE_TENDER_WEEKLY_REPORT_DENY_HEADER => 'message-with-header',
        self::TYPE_TENDER_PRODUCT_REPORT_DENY_HEADER => 'message-with-header',

        self::TYPE_PRODUCT_SET_SHIPPING => 'message-with-header',
        self::TYPE_PRODUCT_SET_SHIPPING_DENY => 'message-with-header',
        self::TYPE_PRODUCT_SEND_PRODUCT => 'message-with-header',
        self::TYPE_ACCEPT_PRODUCT => 'header',
        self::TYPE_DENY_PRODUCT => 'message-with-header',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_history';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = [TimestampBehavior::class];
        if ($this->isBlameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'sender_id',
                'updatedByAttribute' => false,
            ];
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'seller_id', 'customer_id', 'type'], 'required'],
            [['order_id', 'seller_id', 'customer_id', 'status', 'type', 'created_at', 'updated_at', 'sender_id'], 'integer'],
            [['content'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['customer_id' => 'id']],
            [['seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['seller_id' => 'id']],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['sender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'order_id' => Yii::t('store', 'Order ID'),
            'seller_id' => Yii::t('store', 'Seller ID'),
            'customer_id' => Yii::t('store', 'Customer ID'),
            'content' => Yii::t('store', 'Content'),
            'status' => Yii::t('store', 'Status'),
            'type' => Yii::t('store', 'Type'),
            'created_at' => Yii::t('store', 'Created At'),
            'updated_at' => Yii::t('store', 'Updated At'),
            'is_approved' => Yii::t('store', 'Is Approved'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::class, ['id' => 'sender_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andWhere(['attachment.entity' => 'order-history']);
    }

    /**
     * @return null|string
     */
    public function getHeader()
    {
        $header = null;

        switch ($this->type) {
            case self::TYPE_ORDER_CREATED:
                $header = Yii::t('order', 'Order has been created');
                break;
            case self::TYPE_ORDER_PAID:
                $header = Yii::t('order', 'Order has been paid');
                break;
            case self::TYPE_REQUIREMENTS_HEADER:
                $header = Yii::t('order', 'Additional information');
                break;
            case self::TYPE_REQUIREMENTS_DENY:
                $header = Yii::t('order', 'Seller has additional questions regards this order');
                break;
            case self::TYPE_POST_REQUIREMENTS:
                $header = Yii::t('order', 'Customer has provided an additional information for this order');
                break;
            case self::TYPE_CANCEL:
            case self::TYPE_MANUAL_CANCEL:
                $header = Yii::t('order', 'Order cancelled. The money will soon be returned to the balance.');
                break;
            case self::TYPE_REQUIREMENTS_ACCEPT:
                $header = Yii::t('order', 'In process');
                break;
            case self::TYPE_DELIVERY:
            case self::TYPE_TENDER_WEEKLY_REPORT:
                $header = Yii::t('order', 'Job is done');
                break;
            case self::TYPE_TENDER_PRODUCT_REPORT:
                $header = Yii::t('order', 'Seller has marked this order as completed');
                break;
            case self::TYPE_JOB_DENY:
            case self::TYPE_TENDER_WEEKLY_REPORT_DENY_HEADER:
                $header = Yii::t('order', 'Customer refused job completion');
                break;
            case self::TYPE_TENDER_PRODUCT_REPORT_DENY_HEADER:
                $header = Yii::t('order', 'Customer refused order');
                break;
            case self::TYPE_JOB_ACCEPT:
            case self::TYPE_TENDER_REPORT_ACCEPT_HEADER:
            case self::TYPE_ACCEPT_PRODUCT:
                $header = Yii::t('order', 'Reviews');
                break;
            case self::TYPE_TENDER_RESPONSE_HEADER:
                $header = Yii::t('order', 'New response on request');
                break;
            case self::TYPE_TENDER_RESPONSE_CANCEL:
                $header = Yii::t('order', 'Response is cancelled');
                break;
            case self::TYPE_TENDER_WORKER_DENIED_HEADER:
                $header = Yii::t('order', 'Customer has denied worker');
                break;
            case self::TYPE_TENDER_SELLER_DENIED_HEADER:
                $header = Yii::t('order', 'Customer has denied seller');
                break;
            case self::TYPE_TENDER_RESPONSE_PRICE_ACCEPT:
                $header = Yii::t('order', 'Price has been set at {price}', ['price' => $this->order->getTotal()]);
                break;
            case self::TYPE_TENDER_WORKER_APPROVED_HEADER:
                $header = Yii::t('order', 'Customer has approved worker');
                break;
            case self::TYPE_TENDER_SELLER_APPROVED_HEADER:
                $header = Yii::t('order', 'Customer has approved seller');
                break;
            case self::TYPE_TENDER_PAID:
            case self::TYPE_TENDER_PRODUCT_PAID:
                $header = Yii::t('order', 'Customer has paid deposit');
                break;
            case self::TYPE_TENDER_WEEKLY_TASK_ACCEPT:
                $header = Yii::t('order', 'Seller has accepted task');
                break;
            case self::TYPE_TENDER_WEEKLY_REPORT_ACCEPT_HEADER:
                $header = Yii::t('order', 'Customer has accepted task completion');
                break;
            case self::TYPE_TENDER_ADVANCED_STOP:
                $header = Yii::t('order', 'Order has been stopped');
                break;
            case self::TYPE_PRODUCT_SET_SHIPPING:
                $header = '';

                if (array_key_exists('type', $this->customDataArray)) {
                    switch ($this->customDataArray['type']) {
                        case 'pickup':
                            $header = Yii::t('order', 'Pickup');
                            break;
                        case 'shipping':
                            $header = Yii::t('order', 'Shipping');
                            break;
                    }
                }
                break;
            case self::TYPE_PRODUCT_SET_SHIPPING_DENY:
                $header = Yii::t('order', 'Seller requested additional information');
                break;
            case self::TYPE_PRODUCT_SEND_PRODUCT:
                $header = Yii::t('order', 'Seller sent product to customer');
                break;
        }

        return $header;
    }

    /**
     * @return null|string
     */
    public function getSubHeader()
    {
        $subHeader = null;

        switch ($this->type) {
            case self::TYPE_ORDER_CREATED:
                $subHeader = Yii::t('order', 'Order has been created. Checkout process will be continued when we receive payment for this order.');
                break;
            case self::TYPE_ORDER_PAID:
                $subHeader = $this->order->product_type == Order::TYPE_PRODUCT ? Yii::t('order', 'Order has been paid. Seller will receive money when customer will receive product and confirm that it`s ok.') : Yii::t('order', 'Order has been paid. Seller will receive money when order will be completed and accepted by customer.');
                break;
            case self::TYPE_REQUIREMENTS_HEADER:
                $subHeader = Yii::t('order', 'Additional information for this order has to be filled.');
                break;
            case self::TYPE_NEED_REQUIREMENTS:
                $subHeader = Yii::t('order', 'Enter information required for order execution.');
                break;
            case self::TYPE_TENDER_RESPONSE_PRICE:
                $subHeader = Yii::t('order', 'I offer price: {price}', ['price' => CurrencyHelper::convertAndFormat($this->customDataArray['price']['from'], Yii::$app->params['app_currency_code'], $this->customDataArray['price']['amount'], true)]);
                break;
            case self::TYPE_TENDER_PAID:
                $subHeader = $this->order->product_type == Order::TYPE_TENDER ? Yii::t('order', 'Worker will be gaining gain money weekly, just after task is completed and accepted by customer.') : Yii::t('order', 'Worker will gain money just after task is completed and accepted by customer.');
                break;
            case self::TYPE_TENDER_PRODUCT_PAID:
                $subHeader = Yii::t('order', 'Seller will receive money from deposit when customer will receive product and will confirm this.');
                break;
            case self::TYPE_MANUAL_COMPLETE:
                $subHeader = Yii::t('order', 'Order is completed by system.');
                break;
            case self::TYPE_TENDER_RESPONSE_MESSAGE:
                $subHeader = Yii::t('order', 'Greetings! I am interested in your request.');
                break;
            case self::TYPE_REQUIREMENTS_ACCEPT:
                $subHeader = Yii::t('order', '{who} will soon start working on your order.<br/><br/>This order is expected to be completed on {date}', [
                    'who' => $this->order->seller->getSellerName(),
                    'date' => Yii::$app->formatter->asDate($this->order->tbd_at)
                ]);
                break;
            case self::TYPE_DELIVERY:
                $subHeader = Yii::t('order', 'This order will be marked as completed in {count, plural, =0{days} one{# job} few{# days} many{# days} other{# days}}.', ['count' => 3]);
                break;
            case self::TYPE_JOB_ACCEPT:
            case self::TYPE_TENDER_REPORT_ACCEPT_HEADER:
                $subHeader = Yii::t('order', 'Customer accepted job completion. Now you can left reviews for each other.');
                break;
            case self::TYPE_TENDER_RESPONSE_CANCEL:
            case self::TYPE_TENDER_WORKER_DENIED_HEADER:
            case self::TYPE_TENDER_SELLER_DENIED_HEADER:
            case self::TYPE_TENDER_RESPONSE_PRICE_ACCEPT:
            case self::TYPE_TENDER_WEEKLY_TASK_ACCEPT:
            case self::TYPE_ACCEPT_PRODUCT:
                $subHeader = ' ';
                break;
            case self::TYPE_TENDER_WEEKLY_REPORT_ACCEPT_HEADER:
                $subHeader = Yii::t('order', 'Now customer has to write the task for next week or finish this order.');
                break;
            case self::TYPE_TENDER_WORKER_APPROVED_HEADER:
            case self::TYPE_TENDER_SELLER_APPROVED_HEADER:
                $subHeader = Yii::t('order', 'To continue, customer has to pay deposit for this request.');
                break;
            case self::TYPE_PRODUCT_SET_SHIPPING:
                $subHeader = '';

                if (array_key_exists('type', $this->customDataArray)) {
                    switch ($this->customDataArray['type']) {
                        case 'pickup':
                            $subHeader = Yii::t('order', 'Customer selected shipping method: Pickup.');
                            break;
                        case 'shipping':
                            $subHeader = Yii::t('order', 'Customer selected shipping method: Shipping.') . '<br>';
                            $subHeader .= Yii::t('order', 'Address: {address}', ['address' => $this->customDataArray['address']]);
                            break;
                    }
                }
                break;
            case self::TYPE_TENDER_RESPONSE_HEADER:
                $subHeader = Yii::t('order', '{user} has sent response on request {tender}', $this->customDataArray);
                break;
        }

        return $subHeader;
    }

    /**
     * @return null|string
     */
    public function getIcon()
    {
        $icon = null;

        switch ($this->type) {
            case self::TYPE_ORDER_CREATED:
            case self::TYPE_TENDER_RESPONSE_PRICE_ACCEPT:
            case self::TYPE_TENDER_WORKER_APPROVED_HEADER:
            case self::TYPE_TENDER_SELLER_APPROVED_HEADER:
            case self::TYPE_MANUAL_COMPLETE:
            case self::TYPE_TENDER_WEEKLY_REPORT_ACCEPT_HEADER:
                $icon = self::ICON_CREATED;
                break;
            case self::TYPE_ORDER_PAID:
            case self::TYPE_TENDER_PAID:
            case self::TYPE_TENDER_PRODUCT_PAID:
                $icon = self::ICON_PAID;
                break;
            case self::TYPE_REQUIREMENTS_HEADER:
            case self::TYPE_JOB_DENY:
            case self::TYPE_TENDER_RESPONSE_HEADER:
            case self::TYPE_REQUIREMENTS_DENY:
            case self::TYPE_PRODUCT_SET_SHIPPING_DENY:
            case self::TYPE_POST_REQUIREMENTS:
                $icon = self::ICON_REQUIRE;
                break;
            case self::TYPE_CANCEL:
            case self::TYPE_TENDER_RESPONSE_CANCEL:
            case self::TYPE_TENDER_WORKER_DENIED_HEADER:
            case self::TYPE_TENDER_SELLER_DENIED_HEADER:
            case self::TYPE_TENDER_WEEKLY_REPORT_DENY_HEADER:
            case self::TYPE_TENDER_PRODUCT_REPORT_DENY_HEADER:
            case self::TYPE_MANUAL_CANCEL:
            case self::TYPE_TENDER_ADVANCED_STOP:
                $icon = self::ICON_CANCEL;
                break;
            case self::TYPE_REQUIREMENTS_ACCEPT:
            case self::TYPE_TENDER_WEEKLY_TASK_ACCEPT:
                $icon = self::ICON_START;
                break;
            case self::TYPE_DELIVERY:
            case self::TYPE_TENDER_WEEKLY_REPORT:
            case self::TYPE_PRODUCT_SET_SHIPPING:
            case self::TYPE_PRODUCT_SEND_PRODUCT:
            case self::TYPE_TENDER_PRODUCT_REPORT:
                $icon = self::ICON_DELIVERY;
                break;
            case self::TYPE_JOB_ACCEPT:
            case self::TYPE_TENDER_REPORT_ACCEPT_HEADER:
            case self::TYPE_ACCEPT_PRODUCT:
                $icon = self::ICON_REVIEW;
                break;
        }

        return $icon;
    }

    /**
     * @return null|string
     */
    public function getAlert()
    {
        $alert = null;

        switch ($this->type) {
            case self::TYPE_TENDER_WEEKLY_TASK:
                $alert = Yii::t('order', 'Task text');
                break;
            case self::TYPE_TENDER_PRODUCT_DETAILS:
                $alert = Yii::t('order', 'Order details');
                break;
        }

        return $alert;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->custom_data !== null) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }
    }
}
