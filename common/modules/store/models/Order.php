<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 17:46
 */

namespace common\modules\store\models;

use common\behaviors\BlameableBehavior;
use common\components\CurrencyHelper;
use common\models\Job;
use common\models\JobPackage;
use common\models\Offer;
use common\models\Payment;
use common\models\Review;
use common\models\user\User;
use common\modules\board\models\Product;
use common\modules\store\services\ProductOrderService;
use common\modules\store\services\ProductTenderOrderService;
use common\modules\store\services\SimpleOrderService;
use common\modules\store\services\TenderOrderService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $seller_id
 * @property integer $payment_method_id
 * @property integer $status
 * @property integer $address_id
 * @property string $email
 * @property string $phone
 * @property integer $total
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $tbd_at
 * @property integer $payment_id
 * @property integer $product_type
 * @property string $product_subtype
 * @property boolean $is_price_approved
 * @property string $currency_code
 *
 * @property CartItemInterface $product
 * @property OrderProduct $orderProduct
 * @property User $customer
 * @property User $seller
 * @property PaymentMethod $paymentMethod
 * @property Payment $payment
 */
class Order extends ActiveRecord
{
    const STATUS_CANCELLED = -10;
    const STATUS_REQUIRES_PAYMENT = 5;
    const STATUS_MISSING_DETAILS = 10;
    const STATUS_ACTIVE = 20;
    const STATUS_DELIVERED = 30;
    const STATUS_AWAITING_REVIEW = 35;
    const STATUS_AWAITING_SELLER_REVIEW = 40;
    const STATUS_AWAITING_CUSTOMER_REVIEW = 45;
    const STATUS_COMPLETED = 50;

    const STATUS_TENDER_UNAPPROVED = 100;
    const STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION = 101;
    const STATUS_TENDER_WAITING_SELLER_PRICE_DECISION = 102;
    const STATUS_TENDER_APPROVED = 105;
    const STATUS_TENDER_PAID = 110;
    const STATUS_TENDER_REQUIRES_WEEKLY_TASK = 115;
    const STATUS_TENDER_REQUIRES_TASK_APPROVE = 120;
    const STATUS_TENDER_ACTIVE = 125;
    const STATUS_TENDER_DELIVERED = 130;
    const STATUS_TENDER_AWAITING_REVIEW = 134;
    const STATUS_TENDER_AWAITING_SELLER_REVIEW = 135;
    const STATUS_TENDER_AWAITING_CUSTOMER_REVIEW = 140;
    const STATUS_TENDER_COMPLETED = 145;

    const STATUS_PRODUCT_REQUIRES_PAYMENT = 205;
    const STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION = 210;
    const STATUS_PRODUCT_WAITING_FOR_SEND = 215;
    const STATUS_PRODUCT_SENT = 220;
    const STATUS_PRODUCT_WAITING_FOR_PICKUP = 221;
    const STATUS_PRODUCT_DELIVERED = 225;
    const STATUS_PRODUCT_DENIED = 235;
    const STATUS_PRODUCT_AWAITING_REVIEW = 240;
    const STATUS_PRODUCT_COMPLETED = 260;

    const STATUS_PRODUCT_TENDER_UNAPPROVED = 300;
    const STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION = 301;
    const STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION = 302;
    const STATUS_PRODUCT_TENDER_APPROVED = 305;
    const STATUS_PRODUCT_TENDER_PAID = 310;
    const STATUS_PRODUCT_TENDER_ACTIVE = 325;
    const STATUS_PRODUCT_TENDER_DELIVERED = 330;
    const STATUS_PRODUCT_TENDER_AWAITING_REVIEW = 334;
    const STATUS_PRODUCT_TENDER_COMPLETED = 345;

    const TYPE_JOB_PACKAGE = 0;
    const TYPE_OFFER = 10;
    const TYPE_JOB = 20;
    const TYPE_DOCUMENT = 25;
    const TYPE_TENDER = 30; // TODO поменять на какой-нибудь advanced
    const TYPE_SIMPLE_TENDER = 40;
    const TYPE_PRODUCT = 50;
    const TYPE_PRODUCT_TENDER = 60;

    const ROLE_SELLER = 'seller';
    const ROLE_CUSTOMER = 'customer';

    /**
     * @var bool
     */
    public $isBlameable = true;
    /**
     * @var string
     */
    public $customComment = '';
    /**
     * @var null
     */
    public $attachments = null;
    /*
     * @var array
     */
    public $cartItems = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [TimestampBehavior::class];
        if ($this->isBlameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'customer_id',
                'updatedByAttribute' => false,
            ];
        }
        return $behaviors;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'payment_method_id', 'payment_id', 'tbd_at', 'seller_id', 'total'], 'integer'],
            [['email'], 'email'],
            [['email', 'phone'], 'string', 'max' => 255],
            ['product_subtype', 'string', 'max' => 25],
            [['customComment'], 'string', 'max' => 500],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['customer_id' => 'id']],
            [['seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['seller_id' => 'id']],
            [['payment_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::class, 'targetAttribute' => ['payment_method_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::class, 'targetAttribute' => ['payment_id' => 'id']],
            [['is_price_approved'], 'boolean'],

            ['status', 'default', 'value' => self::STATUS_REQUIRES_PAYMENT],
            ['status', 'in', 'range' => [
                self::STATUS_CANCELLED,
                self::STATUS_REQUIRES_PAYMENT,
                self::STATUS_MISSING_DETAILS,
                self::STATUS_ACTIVE,
                self::STATUS_DELIVERED,
                self::STATUS_AWAITING_REVIEW,
                self::STATUS_COMPLETED,

                self::STATUS_TENDER_UNAPPROVED,
                self::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                self::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION,
                self::STATUS_TENDER_APPROVED,
                self::STATUS_TENDER_PAID,
                self::STATUS_TENDER_REQUIRES_WEEKLY_TASK,
                self::STATUS_TENDER_REQUIRES_TASK_APPROVE,
                self::STATUS_TENDER_ACTIVE,
                self::STATUS_TENDER_DELIVERED,
                self::STATUS_TENDER_AWAITING_REVIEW,
                self::STATUS_TENDER_COMPLETED,

                self::STATUS_PRODUCT_REQUIRES_PAYMENT,
                self::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION,
                self::STATUS_PRODUCT_WAITING_FOR_SEND,
                self::STATUS_PRODUCT_WAITING_FOR_PICKUP,
                self::STATUS_PRODUCT_SENT,
                self::STATUS_PRODUCT_DELIVERED,
                self::STATUS_PRODUCT_COMPLETED,
                self::STATUS_PRODUCT_DENIED,
                self::STATUS_PRODUCT_AWAITING_REVIEW,

                self::STATUS_PRODUCT_TENDER_UNAPPROVED,
                self::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION,
                self::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION,
                self::STATUS_PRODUCT_TENDER_APPROVED,
                self::STATUS_PRODUCT_TENDER_PAID,
                self::STATUS_PRODUCT_TENDER_ACTIVE,
                self::STATUS_PRODUCT_TENDER_DELIVERED,
                self::STATUS_PRODUCT_TENDER_AWAITING_REVIEW,
                self::STATUS_PRODUCT_TENDER_COMPLETED,
            ]],
            ['is_price_approved', 'default', 'value' => true],
            ['attachments', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'customer_id' => Yii::t('store', 'Customer ID'),
            'seller_id' => Yii::t('store', 'Seller ID'),
            'payment_method_id' => Yii::t('store', 'Payment Method ID'),
            'status' => Yii::t('store', 'Status'),
            'email' => Yii::t('store', 'Email'),
            'phone' => Yii::t('store', 'Phone'),
            'total' => Yii::t('store', 'Total'),
            'created_at' => Yii::t('store', 'Created At'),
            'updated_at' => Yii::t('store', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        /* @var $user User */
        if (isset(Yii::$app->user)) {
            $user = Yii::$app->user->identity;
            if ($user) {
                $this->email = $user->email;
                $this->phone = $user->phone;
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Job::class, ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeller()
    {
        return $this->hasOne(User::class, ['id' => 'seller_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(User::class, ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(OrderHistory::class, ['order_id' => 'id'])->orderBy('id asc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        switch ($this->product_type) {
            case self::TYPE_JOB_PACKAGE :
                return $this->hasOne(JobPackage::class, ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
                break;
            case self::TYPE_OFFER :
                return $this->hasOne(Offer::class, ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
                break;
            case self::TYPE_JOB :
                return $this->hasOne(Job::class, ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
                break;
            case self::TYPE_TENDER:
            case self::TYPE_SIMPLE_TENDER:
                return $this->hasOne(Job::class, ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
                break;
            case self::TYPE_PRODUCT:
            case self::TYPE_PRODUCT_TENDER:
                return $this->hasOne(Product::class, ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
                break;
            default :
                return null;
                break;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProduct()
    {
        return $this->hasOne(OrderProduct::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::class, ['id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellerReview()
    {
        $relation = $this->hasOne(Review::class, ['order_id' => 'id']);

        return $relation->andWhere(['type' => Review::TYPE_SELLER_REVIEW]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerReview()
    {
        $relation = $this->hasOne(Review::class, ['order_id' => 'id']);

        return $relation->andWhere(['type' => Review::TYPE_CUSTOMER_REVIEW]);
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->total, true);
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return $this->status > self::STATUS_REQUIRES_PAYMENT;
    }

    /**
     * @return bool
     */
    public function isProFound()
    {
        return $this->status > self::STATUS_TENDER_UNAPPROVED;
    }

    /**
     * @return bool
     */
    public function isPriceApproved()
    {
        return $this->status >= self::STATUS_TENDER_APPROVED;
    }

    /**
     * @return bool
     */
    public function isTenderPaid()
    {
        return $this->status >= self::STATUS_TENDER_PAID;
    }

    /**
     * @return bool
     */
    public function isTenderDelivered()
    {
        return $this->status > self::STATUS_TENDER_DELIVERED;
    }

    /**
     * @return bool
     */
    public function isTenderCompleted()
    {
        return $this->status >= self::STATUS_TENDER_COMPLETED;
    }

    /**
     * @return bool
     */
    public function areRequirementSubmitted()
    {
        return $this->status > self::STATUS_MISSING_DETAILS;
    }

    /**
     * @return bool
     */
    public function isDelivered()
    {
        return $this->status >= self::STATUS_DELIVERED;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->status >= self::STATUS_AWAITING_REVIEW;
    }

    /**
     * @param bool|false $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_PAYMENT => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting Payment'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Awaiting Payment')),
            self::STATUS_CANCELLED => ($colored ? Html::tag('div', Yii::t('labels', 'Cancelled'), ['class' => 'btn btn-danger btn-xs']) : Yii::t('labels', 'Cancelled')),
            self::STATUS_MISSING_DETAILS => ($colored ? Html::tag('div', Yii::t('labels', 'Missing Details'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Missing Details')),
            self::STATUS_ACTIVE => ($colored ? Html::tag('div', Yii::t('labels', 'Active'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Active')),
            self::STATUS_DELIVERED => ($colored ? Html::tag('div', Yii::t('labels', 'Delivered'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Delivered')),
            self::STATUS_AWAITING_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),
            self::STATUS_AWAITING_SELLER_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),
            self::STATUS_AWAITING_CUSTOMER_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),
            self::STATUS_COMPLETED => ($colored ? Html::tag('div', Yii::t('labels', 'Completed'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Completed')),

            self::STATUS_TENDER_UNAPPROVED => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting Response'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Awaiting Response')),
            self::STATUS_TENDER_WAITING_CUSTOMER_PRICE_DECISION => ($colored ? Html::tag('div', Yii::t('labels', 'Trading. The customer`s response is awaited'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Trading. The customer`s response is awaited')),
            self::STATUS_TENDER_WAITING_SELLER_PRICE_DECISION => ($colored ? Html::tag('div', Yii::t('labels', 'Trading. The performer`s response is awaited'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Trading. The performer`s response is awaited')),
            self::STATUS_TENDER_APPROVED => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting Payment'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Awaiting Payment')),
            self::STATUS_TENDER_ACTIVE => ($colored ? Html::tag('div', Yii::t('labels', 'Active'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Active')),
            self::STATUS_TENDER_PAID => ($colored ? Html::tag('div', Yii::t('labels', 'Month paid'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Month paid')),
            self::STATUS_TENDER_REQUIRES_WEEKLY_TASK => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting new task'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting new task')),
            self::STATUS_TENDER_REQUIRES_TASK_APPROVE => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting task approve'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting task approve')),
            self::STATUS_TENDER_COMPLETED => ($colored ? Html::tag('div', Yii::t('labels', 'Done'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Done')),
            self::STATUS_TENDER_DELIVERED => ($colored ? Html::tag('div', Yii::t('labels', 'Delivered'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Delivered')),
            self::STATUS_TENDER_AWAITING_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),
            self::STATUS_TENDER_AWAITING_SELLER_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),
            self::STATUS_TENDER_AWAITING_CUSTOMER_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),

            self::STATUS_PRODUCT_REQUIRES_PAYMENT => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting Payment'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Awaiting Payment')),
            self::STATUS_PRODUCT_REQUIRES_SHIPMENT_DECISION => ($colored ? Html::tag('div', Yii::t('labels', 'Missing Details'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Missing Details')),
            self::STATUS_PRODUCT_WAITING_FOR_SEND => ($colored ? Html::tag('div', Yii::t('labels', 'Waiting for shipping decision'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Waiting for shipping decision')),
            self::STATUS_PRODUCT_WAITING_FOR_PICKUP => ($colored ? Html::tag('div', Yii::t('labels', 'Waiting for shipping decision'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Waiting for shipping decision')),
            self::STATUS_PRODUCT_SENT => ($colored ? Html::tag('div', Yii::t('labels', 'Sent'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Sent')),
            self::STATUS_PRODUCT_DELIVERED => ($colored ? Html::tag('div', Yii::t('labels', 'Delivered'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Delivered')),
            self::STATUS_PRODUCT_COMPLETED => ($colored ? Html::tag('div', Yii::t('labels', 'Completed'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Completed')),
            self::STATUS_PRODUCT_DENIED => ($colored ? Html::tag('div', Yii::t('labels', 'Denied'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Denied')),
            self::STATUS_PRODUCT_AWAITING_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting for Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting for Review')),

            self::STATUS_PRODUCT_TENDER_UNAPPROVED => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting Response'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Awaiting Response')),
            self::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION => ($colored ? Html::tag('div', Yii::t('labels', 'Trading. The customer`s response is awaited'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Trading. The customer`s response is awaited')),
            self::STATUS_PRODUCT_TENDER_WAITING_SELLER_PRICE_DECISION => ($colored ? Html::tag('div', Yii::t('labels', 'Trading. The performer`s response is awaited'), ['class' => 'btn btn-primary btn-xs']) : Yii::t('labels', 'Trading. The performer`s response is awaited')),
            self::STATUS_PRODUCT_TENDER_APPROVED => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting Payment'), ['class' => 'btn btn-warning btn-xs']) : Yii::t('labels', 'Awaiting Payment')),
            self::STATUS_PRODUCT_TENDER_ACTIVE => ($colored ? Html::tag('div', Yii::t('labels', 'Active'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Active')),
            self::STATUS_PRODUCT_TENDER_PAID => ($colored ? Html::tag('div', Yii::t('labels', 'Month paid'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Month paid')),
            self::STATUS_PRODUCT_TENDER_COMPLETED => ($colored ? Html::tag('div', Yii::t('labels', 'Done'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Done')),
            self::STATUS_PRODUCT_TENDER_DELIVERED => ($colored ? Html::tag('div', Yii::t('labels', 'Delivered'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Delivered')),
            self::STATUS_PRODUCT_TENDER_AWAITING_REVIEW => ($colored ? Html::tag('div', Yii::t('labels', 'Awaiting My Review'), ['class' => 'btn btn-success btn-xs']) : Yii::t('labels', 'Awaiting My Review')),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert && empty($this->currency_code) && !empty($this->cartItems)) {
            $item = $this->cartItems[0];
            if (array_key_exists('item', $item)) {
                $this->currency_code = $item['item']->currency_code;
            } else {
                $this->currency_code = $item->currency_code;
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        switch ($this->product_type) {
            case self::TYPE_JOB_PACKAGE:
            case self::TYPE_JOB:
            case self::TYPE_OFFER:
                $this->saveSimpleOrder($insert, $changedAttributes);
                break;
            case self::TYPE_TENDER:
            case self::TYPE_SIMPLE_TENDER:
                $this->saveAdvancedOrder($insert, $changedAttributes);
                break;
            case self::TYPE_PRODUCT:
                $this->saveProductOrder($insert, $changedAttributes);
                break;
            case self::TYPE_PRODUCT_TENDER:
                $this->saveProductTenderOrder($insert, $changedAttributes);
                break;
        }
        switch ($this->product_type) {
            case self::TYPE_JOB_PACKAGE:
                $this->product->job->updateElasticOrders();
                break;
            case self::TYPE_TENDER:
            case self::TYPE_SIMPLE_TENDER:
                $this->product->updateElasticOrders();
                break;
        }
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    private function saveSimpleOrder($insert, $changedAttributes)
    {
        $simpleOrderService = new SimpleOrderService($this);
        $simpleOrderService->process($insert, $changedAttributes);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    private function saveAdvancedOrder($insert, $changedAttributes)
    {
        $tenderOrderService = new TenderOrderService($this);
        $tenderOrderService->process($insert, $changedAttributes);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    private function saveProductOrder($insert, $changedAttributes)
    {
        $productOrderService = new ProductOrderService($this);
        $productOrderService->process($insert, $changedAttributes);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    private function saveProductTenderOrder($insert, $changedAttributes)
    {
        $productTenderOrderService = new ProductTenderOrderService($this);
        $productTenderOrderService->process($insert, $changedAttributes);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            OrderHistory::deleteAll('order_id = :id', [':id' => $this->id]);
            OrderProduct::deleteAll('order_id = :id', [':id' => $this->id]);
            Payment::deleteAll('id = :id', [':id' => $this->payment_id]);
            return true;
        } else {
            return false;
        }
    }
}