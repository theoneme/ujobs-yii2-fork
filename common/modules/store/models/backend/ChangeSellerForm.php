<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.09.2017
 * Time: 14:35
 */

namespace common\modules\store\models\backend;

use common\models\JobPackage;
use common\models\Notification;
use common\models\user\User;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use common\modules\store\models\OrderProduct;
use Yii;
use yii\base\Model;

/**
 * Class ChangeSellerForm
 * @package common\modules\store\models\backend
 */
class ChangeSellerForm extends Model
{
    /**
     * @var integer
     */
    public $order_id = null;

    /**
     * @var integer
     */
    public $new_seller_id = null;

    /**
     * @var integer
     */
    public $new_job_id = null;

    /**
     * @var bool
     */
    public $merge = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'new_seller_id', 'new_job_id'], 'integer'],
            [['merge'], 'boolean'],
            [['new_seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['new_seller_id' => 'id']],
            [['new_job_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobPackage::class, 'targetAttribute' => ['new_job_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('store', 'Order'),
            'new_seller_id' => Yii::t('store', 'New Seller'),
            'new_job_id' => Yii::t('store', 'Job'),
            'merge' => Yii::t('store', 'I want to change seller or service in order'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $order = Order::find()->joinWith('orderProduct')->where(['order.id' => $this->order_id])->one();

        $oldSellerId = $order->seller_id;
        if ($this->new_seller_id > 0) {
            Order::updateAll(['seller_id' => $this->new_seller_id], ['id' => $this->order_id]);
            OrderHistory::updateAll(['seller_id' => $this->new_seller_id], ['seller_id' => $oldSellerId]);
            OrderHistory::updateAll(['sender_id' => $this->new_seller_id], ['sender_id' => $oldSellerId]);
            Notification::updateAll(['created_at' => time(), 'to_id' => $this->new_seller_id, 'is_read' => 0], ['link' => Yii::$app->urlManagerFrontEnd->createUrl(['/account/order/history', 'id' => $order->id])]);
        }

        if ($this->new_job_id > 0) {
            OrderProduct::updateAll(['product_id' => $this->new_job_id], ['order_id' => $order->id]);
        }

        return true;
    }
}
