<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:47
 */

namespace common\modules\store\models\storage;

use common\modules\store\components\Cart;
use Yii;
use yii\base\BaseObject;

class SessionStorage extends BaseObject implements StorageInterface
{
    /**
     * @var string
     */
    public $key = 'cart';

    /**
     * @inheritdoc
     */
    public function load(Cart $cart)
    {
        $cartData = [];
        if (false !== ($session = ($this->session->get($this->key, false)))) {
            $cartData = unserialize($session);
        }
        return $cartData;
    }

    /**
     * @inheritdoc
     */
    public function save(Cart $cart)
    {
        $sessionData = serialize($cart->getItems());
        $this->session->set($this->key, $sessionData);
    }

    /**
     * @return \yii\web\Session
     */
    public function getSession()
    {
        return Yii::$app->get('session');
    }
}