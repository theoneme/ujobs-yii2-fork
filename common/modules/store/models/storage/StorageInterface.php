<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:36
 */

namespace common\modules\store\models\storage;

use common\modules\store\components\Cart;

interface StorageInterface
{
    public function load(Cart $cart);

    public function save(Cart $cart);
}