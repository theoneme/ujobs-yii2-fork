<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 12:44
 */

namespace common\modules\store\models\storage;

use common\modules\store\components\Cart;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\BaseObject;
use yii\db\Connection;
use yii\db\Query;
use yii\web\User;

class DatabaseStorage extends BaseObject implements StorageInterface
{
    /**
     * @var string Name of the user component
     */
    public $userComponent = 'user';
    /**
     * @var string Name of the database component
     */
    public $dbComponent = 'db';
    /**
     * @var string Name of the cart table
     */
    public $table = 'cart';
    /**
     * @var string Name of the
     */
    public $idField = 'sessionId';
    /**
     * @var string Name of the field holding serialized session data
     */
    public $dataField = 'cartData';
    /**
     * @var bool If set to true, empty cart entries will be deleted
     */
    public $deleteIfEmpty = true;
    /**
     * @var Connection
     */
    private $db;
    /**
     * @var User
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->db = Yii::$app->get($this->dbComponent);
        if (isset($this->userComponent)) {
            $this->user = Yii::$app->get($this->userComponent);
        }
        if (!isset($this->table)) {
            throw new InvalidConfigException('Please specify "table" in cart configuration');
        }
    }

    /**
     * @param Cart $cart
     *
     * @return mixed
     */
    public function load(Cart $cart)
    {
        $identifier = $this->getIdentifier(Yii::$app->session->getId());
        $query = new Query();
        $query->select($this->dataField)
            ->from($this->table)
            ->where([$this->idField => $identifier]);
        $items = [];
        if ($data = $query->createCommand($this->db)->queryScalar()) {
            $items = unserialize($data);
        }
        return $items;
    }

    /**
     * @param string $default
     *
     * @return string
     */
    protected function getIdentifier($default)
    {
        $id = $default;
        if ($this->user instanceof User && !$this->user->getIsGuest()) {
            $id = $this->user->getId();
        }
        return $id;
    }

    /**
     * @param \common\modules\store\components\Cart $cart
     *
     * @return void
     */
    public function save(Cart $cart)
    {
        $identifier = $this->getIdentifier(Yii::$app->session->getId());
        $items = $cart->getItems();
        $sessionData = serialize($items);
        $command = $this->db->createCommand();
        if (empty($items) && true === $this->deleteIfEmpty) {
            $command->delete($this->table, [$this->idField => "$identifier"]);
        } else {
            $command->setSql("
                REPLACE {{{$this->table}}}
                SET
                    {{{$this->dataField}}} = :val,
                    {{{$this->idField}}} = :id,
                    updated_at = :time
            ")->bindValues([
                ':id' => $identifier,
                ':val' => $sessionData,
                ':time' => time(),
            ]);
        }
        $command->execute();
    }

    /**
     * @param $sourceId
     * @param $destinationId
     */
    public function reassign($sourceId, $destinationId)
    {
        $command = $this->db->createCommand();
        $command->delete($this->table, [$this->idField => "$destinationId"])->execute();
        $command->setSql("
                UPDATE {{{$this->table}}}
                SET
                    {{{$this->idField}}} = :userId
                WHERE
                    {{{$this->idField}}} = :sessionId
            ")->bindValues([
            ':userId' => $destinationId,
            ':sessionId' => $sourceId
        ])->execute();
    }
}