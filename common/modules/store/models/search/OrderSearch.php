<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.01.2017
 * Time: 17:05
 */

namespace common\modules\store\models\search;

use common\models\search\UserFinder;
use common\modules\store\models\Order;
use yii\data\ActiveDataProvider;

/**
 * OrderSearch represents the model behind the search form about Order.
 */
class OrderSearch extends Order
{
    public $name;

    public $customer;
    public $seller;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'seller_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['customer', 'seller'], 'string']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $pageSize
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pageSize = null)
    {
        $this->load($params);
        $query = Order::find()->joinWith(['customer'])->andWhere(['order.product_type' => $this->product_type]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize ? $pageSize : 20
            ],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if ($this->customer) {
            $this->customer_id = UserFinder::getIdsByName($this->customer);
        }
        if ($this->seller) {
            $this->seller_id = UserFinder::getIdsByName($this->seller);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order.id' => $this->id,
            'seller_id' => $this->seller_id,
            'customer_id' => $this->customer_id,
            'order.created_at' => $this->created_at,
            'order.updated_at' => $this->updated_at,
            'order.status' => $this->status,
        ]);

        return $dataProvider;
    }
}
