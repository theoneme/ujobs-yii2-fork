<?php

namespace common\modules\store\models;

use Yii;

/**
 * This is the model class for table "order_product".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $title
 * @property integer $quantity
 * @property integer $price
 * @property string $currency_code
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    public $options = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'price', 'quantity', 'title'], 'required'],
            [['order_id', 'product_id', 'price', 'quantity'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'order_id' => Yii::t('store', 'Order ID'),
            'product_id' => Yii::t('store', 'Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtra()
    {
        return $this->hasMany(OrderExtra::class, ['order_product_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if (!empty($this->options)) {
                foreach ($this->options as $option) {
                    $extra = new OrderExtra();
                    $extra->order_product_id = $this->id;
                    $extra->order_id = $this->order_id;
                    $extra->job_extra_id = $option['id'];
                    $extra->quantity = $option['quantity'];
                    $extra->price = $option['price'];
                    $extra->title = $option['title'];
                    $extra->currency_code = $option['currency_code'];
                    $extra->save();
                }
            }
        }
    }
}