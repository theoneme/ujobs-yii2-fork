<?php

namespace common\modules\store\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property string $symbol_left
 * @property string $symbol_right
 * @property string $decimal_place
 * @property double $value
 * @property integer $status
 * @property integer $updated_at
 *
 * @property Order[] $orders
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @return array
     */
    public static function getFormattedCurrencies()
    {
//		$currencies = Currency::find()->select('title')->indexBy('code')->column();
//
//		array_walk($currencies, function(&$item, $key) {
//			$item = Yii::t('app', $item);
//		});
//        ArrayHelper::getColumn(Yii::$app->params['currencies'], function($var){ return Yii::t('app', $var['title']); });
//		return $currencies;
        return ArrayHelper::getColumn(Yii::$app->params['currencies'], function ($var) {
            return Yii::t('app', $var['title']);
        });
    }

    /**
     * @return array
     */
    public static function getCurrencySymbols()
    {
//		$currencies = Currency::find()->indexBy('code')->all();
//		$currencySymbols = [];
//
//		/* @var Currency $currency */
//		foreach($currencies as $key => $currency) {
//			$currencySymbols[$key] = $currency->symbol_left ? $currency->symbol_left : $currency->symbol_right;
//		}
//
//		return $currencySymbols;
        return ArrayHelper::getColumn(Yii::$app->params['currencies'], 'sign');
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'status'], 'required'],
            [['value'], 'number'],
            [['status', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 32],
            [['code'], 'string', 'max' => 3],
            [['symbol_left', 'symbol_right'], 'string', 'max' => 12],
            [['decimal_place'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'title' => Yii::t('store', 'Title'),
            'code' => Yii::t('store', 'Code'),
            'symbol_left' => Yii::t('store', 'Symbol Left'),
            'symbol_right' => Yii::t('store', 'Symbol Right'),
            'decimal_place' => Yii::t('store', 'Decimal Place'),
            'value' => Yii::t('store', 'Value'),
            'status' => Yii::t('store', 'Status'),
            'updated_at' => Yii::t('store', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['currency_id' => 'id']);
    }
}