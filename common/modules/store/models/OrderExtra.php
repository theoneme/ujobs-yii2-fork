<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 13.01.2017
 * Time: 12:39
 */

namespace common\modules\store\models;

use common\models\JobExtra;
use Yii;


/**
 * This is the model class for table "order_extra".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $job_extra_id
 * @property integer $quantity
 * @property integer $order_product_id
 * @property integer $price
 * @property string $currency_code
 * @property string $title
 *
 * @property JobExtra $jobExtra
 * @property Order $order
 */
class OrderExtra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_extra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'job_extra_id', 'quantity', 'order_product_id', 'price'], 'integer'],
            ['currency_code', 'string', 'max' => 3],
            ['title', 'string', 'max' => 155],
            [['order_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderProduct::class, 'targetAttribute' => ['order_product_id' => 'id']],
            [['job_extra_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobExtra::class, 'targetAttribute' => ['job_extra_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'order_id' => Yii::t('store', 'Order ID'),
            'job_extra_id' => Yii::t('store', 'Job Extra ID'),
            'quantity' => Yii::t('store', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobExtra()
    {
        return $this->hasOne(JobExtra::class, ['id' => 'job_extra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }
}