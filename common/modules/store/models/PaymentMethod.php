<?php

namespace common\modules\store\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\models\ContentTranslation;
use common\models\PaymentMethodCountry;
use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment_method".
 *
 * @property integer $id
 * @property string $code
 * @property string $logo
 * @property integer $sort
 * @property integer $enabled
 * @property boolean $additional
 *
 * @property Order[] $orders
 * @property AttributeValue[] $countries
 * @property PaymentMethodCountry[] $methodCountries
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 *
 * @mixin LinkableBehavior
 * @mixin ImageBehavior
 */
class PaymentMethod extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_method';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['methodCountries', 'translations'],
            ],
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'payment-method',
                'imageField' => 'logo'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort', 'enabled', 'additional'], 'integer'],
            [['code', 'logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'code' => Yii::t('store', 'Code'),
            'logo' => Yii::t('store', 'Logo'),
            'sort' => Yii::t('store', 'Sort'),
            'enabled' => Yii::t('store', 'Enabled'),
            'additional' => Yii::t('store', 'Additional'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['payment_method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethodCountries()
    {
        return $this->hasMany(PaymentMethodCountry::class, ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(AttributeValue::class, ['id' => 'country_id'])->viaTable('payment_method_country', ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])->andWhere(['pm_ct.entity' => 'payment_method'])->from('content_translation pm_ct')->andWhere(['pm_ct.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andOnCondition(['pm_ct.entity' => 'payment_method'])->from('content_translation pm_ct')->indexBy('locale');
    }

    /**
     * @return string
     */
    public function getName()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];
        return $translation->title;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        $translation = $this->translations[Yii::$app->language] ?? array_values($this->translations)[0];
        return $translation->content_prev;
    }

    /**
     * @param $user_id
     * @param $balanceAllowed
     * @return array
     */
    public static function getMethodsByCountry($user_id, $balanceAllowed) {
        $countryAttributeId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
        $countryValueId = UserAttribute::find()->where(['attribute_id' => $countryAttributeId, 'user_id' => $user_id])->select('value')->scalar();
        if ($countryValueId === false) {
            $countryValueId = AttributeValue::find()
                ->joinWith(['translations'])
                ->where(['attribute_id' => $countryAttributeId])
                ->andWhere(['mod_attribute_description.title' => 'Россия'])
                ->select('mod_attribute_value.id')
                ->scalar();
        }
        $allMethods = PaymentMethod::find()
            ->select(['payment_method.id', 'logo', 'code', 'additional'])
            ->joinWith('translations')
            ->where(['enabled' => 1])
            ->orderBy('sort')
            ->indexBy('id');
        if (!$balanceAllowed) {
            $allMethods->andWhere(['not', ['code' => 'balance']]);
        }
        $allMethods = $allMethods->all();
        $methods = PaymentMethodCountry::find()
            ->where(['not exists', PaymentMethodCountry::find()
                ->from(['pmc' => 'payment_method_country'])
                ->where('payment_method_country.method_id = pmc.method_id')
                ->andWhere(['country_id' => $countryValueId])
            ])
            ->select('method_id')
            ->groupBy('method_id')
            ->indexBy('method_id')
            ->column();
        $preferredMethods = array_filter(array_diff_key($allMethods, $methods), function($var){ return !$var->additional;});
        if (count($preferredMethods)) {
            $additionalMethods = array_filter(array_diff_key($allMethods, $methods), function($var){ return $var->additional;});
            $otherMethods = array_intersect_key($allMethods, $methods);
        }
        else {
            $preferredMethods = array_filter($allMethods, function($var){ return !$var->additional;});
            $additionalMethods = [];
            $otherMethods = [];
        }
        return [
            'preferredMethods' => $preferredMethods,
            'additionalMethods' => $additionalMethods,
            'otherMethods' => $otherMethods,
        ];
    }
}