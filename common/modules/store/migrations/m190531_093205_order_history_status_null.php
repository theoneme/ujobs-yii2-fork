<?php

use yii\db\Migration;

/**
 * Class m190531_093205_order_history_status_null
 */
class m190531_093205_order_history_status_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order_history', 'status', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('order_history', 'status', $this->integer()->notNull());
    }
}
