<?php

use yii\db\Migration;

class m161010_114511_alter_order_total_table extends Migration
{
    public function up()
    {
	    $this->renameColumn('order_total', 'order_total_id', 'id');

    }

    public function down()
    {
	    $this->renameColumn('order_total', 'id', 'order_total_id');
    }
}