<?php

use yii\db\Migration;

class m170811_121814_custom_history_data extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_history', 'custom_data', $this->binary());
    }

    public function safeDown()
    {
        $this->dropColumn('order_history', 'custom_data');
    }
}
