<?php

use yii\db\Schema;
use yii\db\Migration;

class m161006_110619_Mass extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('{{%order_product}}', [
            'order_product_id' => $this->primaryKey(11),
            'order_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull(),
            'model' => $this->string(64)->notNull(),
            'quantity' => $this->integer(4)->notNull(),
            'price' => $this->decimal(15, 4)->notNull()->defaultValue('0.0000'),
            'total' => $this->decimal(15, 4)->notNull()->defaultValue('0.0000'),
            'tax' => $this->decimal(15, 4)->notNull()->defaultValue('0.0000'),
            'reward' => $this->integer(8)->notNull(),
        ], $tableOptions);
        $this->createTable('{{%order_status}}', [
            'order_status_id' => $this->primaryKey(11),
            'language_id' => $this->integer(11)->notNull(),
            'name' => $this->string(32)->notNull(),
        ], $tableOptions);
        $this->createTable('{{%currency}}', [
            'currency_id' => $this->primaryKey(11),
            'title' => $this->string(32)->notNull()->defaultValue(''),
            'code' => $this->string(3)->notNull()->defaultValue(''),
            'symbol_left' => $this->string(12)->notNull(),
            'symbol_right' => $this->string(12)->notNull(),
            'decimal_place' => $this->char(1)->notNull(),
            'value' => $this->float(15, 8)->notNull(),
            'status' => $this->smallInteger(1)->notNull(),
            'date_modified' => $this->datetime()->notNull()->defaultValue('0000-00-00 00:00:00'),
        ], $tableOptions);
        $this->createTable('{{%cart}}', [
            'sessionId' => $this->primaryKey(11),
            'cartData' => $this->text()->notNull(),
        ], $tableOptions);
        $this->createTable('{{%order_total}}', [
            'order_total_id' => $this->primaryKey(10),
            'order_id' => $this->integer(11)->notNull(),
            'code' => $this->string(32)->notNull(),
            'title' => $this->string(255)->notNull(),
            'text' => $this->string(255)->notNull(),
            'value' => $this->decimal(15, 4)->notNull()->defaultValue('0.0000'),
            'sort_order' => $this->integer(3)->notNull(),
        ], $tableOptions);
        $this->createIndex('idx_orders_total_orders_id', '{{%order_total}}', 'order_id', false);
    }

    public function safeDown()
    {
        $this->dropTable('{{%order_product}}');
        $this->dropTable('{{%order_status}}');
        $this->dropTable('{{%language}}');
        $this->dropTable('{{%currency}}');
        $this->dropTable('{{%cart}}');
        $this->dropTable('{{%order_total}}');
    }
}