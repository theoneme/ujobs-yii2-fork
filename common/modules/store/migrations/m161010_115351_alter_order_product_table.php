<?php

use yii\db\Migration;

class m161010_115351_alter_order_product_table extends Migration
{
	public function up()
	{
		$this->renameColumn('order_product', 'order_product_id', 'id');
		$this->dropColumn('order_product', 'name');
		$this->dropColumn('order_product', 'model');
		$this->dropColumn('order_product', 'quantity');
		$this->dropColumn('order_product', 'price');
		$this->dropColumn('order_product', 'total');
		$this->dropColumn('order_product', 'tax');
		$this->dropColumn('order_product', 'reward');
	}

	public function down()
	{
		$this->renameColumn('order_product', 'id', 'order_product_id');
		$this->addColumn('order_product', 'name', $this->string());
		$this->addColumn('order_product', 'model', $this->string(64));
		$this->addColumn('order_product', 'quantity', $this->integer(4));
		$this->addColumn('order_product', 'price', $this->decimal(15,4));
		$this->addColumn('order_product', 'total', $this->decimal(15,4));
		$this->addColumn('order_product', 'tax', $this->decimal(15,4));
		$this->addColumn('order_product', 'reward', $this->integer(8));
	}
}
