<?php

use yii\db\Migration;

class m161010_083902_alter_currency_table extends Migration
{
    public function up()
    {
	    $this->renameColumn('currency', 'currency_id', 'id');
	    $this->dropColumn('currency', 'date_modified');
	    $this->addColumn('currency', 'updated_at', $this->integer());

    }

    public function down()
    {
	    $this->renameColumn('currency', 'id', 'currency_id');
	    $this->addColumn('currency', 'date_modified', $this->dateTime());
	    $this->dropColumn('currency', 'updated_at');
    }
}
