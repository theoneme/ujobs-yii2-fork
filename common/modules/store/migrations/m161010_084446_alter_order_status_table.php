<?php

use yii\db\Migration;

class m161010_084446_alter_order_status_table extends Migration
{
	public function up()
	{
		$this->renameColumn('order_status', 'order_status_id', 'id');

	}

	public function down()
	{
		$this->renameColumn('order_status', 'id', 'order_status_id');
	}
}
