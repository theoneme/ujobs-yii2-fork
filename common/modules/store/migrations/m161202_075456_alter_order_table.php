<?php

use yii\db\Migration;

class m161202_075456_alter_order_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_order_status_id', 'order');
        $this->dropColumn('order', 'status_id');
        $this->addColumn('order','status', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order','status');
        $this->addColumn('order','status_id', $this->integer());
        $this->addForeignKey('fk_order_status_id', 'order', 'status_id', 'order_status', 'id', 'SET NULL');
    }
}
