<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order`.
 */
class m161010_085725_create_order_table extends Migration
{
	public function up()
	{
		$this->createTable('{{%order}}', [
			'id' => $this->primaryKey(),
			'customer_id' => $this->integer(),
			'payment_method_id' => $this->integer(),
			'currency_id' => $this->integer(),
			'status_id' => $this->integer(),

			'firstname' => $this->string(),
			'lastname' => $this->string(),
			'middlename' => $this->string(),
			'email' => $this->string(),
			'phone' => $this->string(),
			'postcode' => $this->string(),
			'country' => $this->string(),
			'region' => $this->string(),
			'city' => $this->string(),
			'street' => $this->string(),
			'house' => $this->string(10),
			'housing' => $this->string(10),
			'room' => $this->string(10),
			'total' => $this->decimal(15, 4),

			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
		]);

		$this->createIndex('firstname', 'order', 'firstname');
		$this->createIndex('lastname', 'order', 'lastname');
		$this->createIndex('middlename', 'order', 'middlename');
		$this->createIndex('total', 'order', 'total');
		$this->createIndex('created_at', 'order', 'created_at');
		$this->createIndex('updated_at', 'order', 'updated_at');

		$this->addForeignKey('fk_order_customer_id', 'order', 'customer_id', 'user', 'id', 'CASCADE');
		$this->addForeignKey('fk_order_payment_method_id', 'order', 'payment_method_id', 'payment_method', 'id', 'SET NULL');
		$this->addForeignKey('fk_order_currency_id', 'order', 'currency_id', 'currency', 'id', 'SET NULL');
		$this->addForeignKey('fk_order_status_id', 'order', 'status_id', 'order_status', 'id', 'SET NULL');
	}

	public function down()
	{
		$this->dropTable('{{%order}}');
	}
}
