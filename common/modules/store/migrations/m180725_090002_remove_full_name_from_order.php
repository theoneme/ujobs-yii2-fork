<?php

use yii\db\Migration;

/**
 * Class m180725_090002_remove_full_name_from_order
 */
class m180725_090002_remove_full_name_from_order extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('order', 'full_name');
    }

    public function safeDown()
    {
        $this->addColumn('order', 'full_name', $this->string(75));
    }
}
