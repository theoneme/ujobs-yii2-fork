<?php

use yii\db\Migration;

/**
 * Class m190531_092018_order_history_content_null
 */
class m190531_092018_order_history_content_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order_history', 'content', $this->text()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('order_history', 'content', $this->text()->notNull());
    }
}
