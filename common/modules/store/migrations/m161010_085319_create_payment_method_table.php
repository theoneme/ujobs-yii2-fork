<?php

use yii\db\Migration;

/**
 * Handles the creation for table `payment_method`.
 */
class m161010_085319_create_payment_method_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payment_method', [
            'id' => $this->primaryKey(),
	        'name' => $this->string(),
	        'code' => $this->string(),
	        'logo' => $this->string(),
	        'sort' => $this->smallInteger(),
	        'enabled' => $this->smallInteger()->notNull()->defaultValue(1)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('payment_method');
    }
}
