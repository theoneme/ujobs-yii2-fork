<?php

namespace common\modules\store;

use Yii;
use yii\base\BootstrapInterface;

/**
 * cart module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\store\controllers';

	/**
	 * @param \yii\base\Application $app
	 */
	public function bootstrap($app)
	{
		\Yii::$app->i18n->translations['store*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@common/modules/store/messages',

			'fileMap' => [
				'store' => 'store.php',
			],
		];
	}
}
