<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 06.10.2016
 * Time: 16:23
 */

namespace common\modules\store\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class StoreAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/store/web';

    public $css = [
        'css/cart.css',
    ];
    public $js = [
    ];
    public $depends = [
        'frontend\assets\CommonAsset'
    ];
}
