<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 07.10.2016
 * Time: 14:10
 */

namespace common\modules\store\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CheckoutAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/store/web';

    public $css = [
        'css/checkout.css',
        'css/cart.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
