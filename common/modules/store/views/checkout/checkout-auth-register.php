<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 07.10.2016
 * Time: 15:18
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;


/* @var $registerModel \dektrium\user\models\RegistrationForm */

?>
<div>
    <?php $form = ActiveForm::begin([
        'id' => 'registration-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'action' => ['/user/registration/register-ajax'],
    ]); ?>

    <?= $form->field($registerModel, 'username') ?>

    <?= $form->field($registerModel, 'email') ?>

    <?= $form->field($registerModel, 'password')->passwordInput() ?>

    <div class="text-right">
        <?= Html::submitButton(Yii::t('app', 'Continue'), ['class' => 'btn btn-md btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>