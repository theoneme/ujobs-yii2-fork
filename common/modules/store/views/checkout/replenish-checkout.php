<?php

use common\components\CurrencyHelper;
use common\modules\store\assets\StoreAsset;
use frontend\assets\CatalogAsset;
use yii\helpers\Url;
use yii\helpers\Html;

StoreAsset::register($this);
CatalogAsset::register($this);

/* @var $amount integer*/
/* @var $paymentMethods array */
/* @var $currency_code string */

$this->title = Yii::t('model', 'Account replenishment');
?>
<div class="container-fluid">
    <div class="cart-header row">
        <div class="cart-header-title"><?= Yii::t('model', 'Account replenishment')?></div>
        <?= Html::a(Yii::t('store', 'Keep Shopping'), Url::to(['/category/job-catalog']), [
            'class' => 'link-blue bright',
        ]) ?>
    </div>
    <div class="cart-box row">
        <div class="cart-left col-md-8 col-sm-8 col-xs-12">
            <?= $this->render('@common/modules/store/views/checkout/checkout-payment-methods', [
                'paymentMethods' => $paymentMethods,
                'checkoutUrl' => Url::to(['/store/checkout/replenish']),
                'total' => $amount,
                'currency_code' => $currency_code,
                'params' => ['amount' => $amount, 'currency_code' => $currency_code]
            ]) ?>
        </div>
        <div class="cart-right col-md-4 col-sm-4 col-xs-12">
            <div class="total-box">
                <div class="price-line">
                    <div class="price-total-title text-left">
                        <?=Yii::t('store', 'Summary')?>
                    </div>
                    <div class="price-total-sum text-right">
                        <?= CurrencyHelper::format($currency_code, $amount) ?>
                    </div>
                </div>
                <div class="price-line total-all">
                    <div class="price-total-title text-left">
                        <?=Yii::t('store', 'Total')?>
                    </div>
                    <div class="price-total-sum text-right">
                        <?= CurrencyHelper::format($currency_code, $amount) ?>
                    </div>
                </div>
            </div>
            <div class="payment-methods">
                <i class="fa fa-cc-paypal"></i>
                <i class="fa fa-cc-visa"></i>
                <i class="fa fa-cc-mastercard"></i>
                <i class="fa fa-cc-amex"></i>
                <i class="fa fa-cc-diners-club"></i>
                <i class="fa fa-cc-discover"></i>
                <i class="fa fa-cc-jcb"></i>
                <i class="fa fa-btc"></i>
                <div class="secure hint--top-right" data-hint="Your payment is secure. SSL (Secure Sockets Layer) is a method of securing payment information sent over the Internet, by encrypting personal information. This means that your name and payment information is protected and cannot be read or intercepted during transit.">
                    <i class="fa fa-lock"></i>
                    <span>Secure<br>Server</span>
                </div>
            </div>
        </div>
    </div>
</div>