<?php

use common\modules\store\models\PaymentMethod;
use yii\helpers\Html;

/* @var $method PaymentMethod*/

$title = Html::tag('div', $method->name, ['class' => 'pay-cell-title']);
$img = Html::img($method->logo, ['alt' => $method->code]);
//$subtitle = Html::tag('div', $method->subtitle, ['class' => 'pay-cell-subtitle']);
$subtitle = '';
$options = Yii::$app->user->isGuest
    ? ['class' => 'pay-cell', 'data-id' => $method->id, 'data-toggle' => 'modal', 'data-target' => '#ModalLogin']
    : ['class' => 'pay-cell', 'data-id' => $method->id];
echo Html::a($title . $img . $subtitle, null, $options);