<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.10.2016
 * Time: 17:58
 */

use common\modules\store\Module;
use kartik\datetime\DateTimePicker;

/* @var $this \yii\web\View */
/* @var $form \yii\widgets\ActiveForm */
/* @var $order \common\modules\store\models\Order */

$showPersonalLine = false;

?>
<div class="cart-header-block"><?= Yii::t('store', 'Additional Order Info') ?></div>
<div class="payment-options">
    <div class="payment-item">
        <?php if($order->full_name == null) {
            $showPersonalLine = true; ?>
            <div class="row">
                <div class="col-xs-12">
                    <?= $form->field($order, 'full_name') ?>
                </div>
            </div>
        <?php } ?>
        <?php if($order->email == null) {
            $showPersonalLine = true; ?>
            <div class="row">
                <div class="col-xs-12">
                    <?= $form->field($order, 'email') ?>
                </div>
            </div>
        <?php } ?>
        <?php if($order->phone == null) {
            $showPersonalLine = true; ?>
            <div class="row">
                <div class="col-xs-12">
                    <?= $form->field($order, 'phone') ?>
                </div>
            </div>
        <?php } ?>
        <?php if($showPersonalLine) { ?>
            <hr/>
        <?php } ?>
        <div class="row">
            <?= $form->field($order, 'location_option')->radioList([
                'my_address' => Yii::t('app', 'I want it to be done in my house'),
                'your_address' => Yii::t('app', 'I Go To Your Address')
            ], [
                'class' => 'checkbox-enchance text-left',
                'id' => 'address-type-radio',
                'data-name' => 'addr',
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    return "<div class='radio'><input {$chk} value=\"{$value}\" id=\"addr_{$index}\" type='radio' name=\"{$name}\"/><label for=\"addr_{$index}\">{$label}</label></div>";
                }
            ])->label(false)?>
        </div>

        <div id="myAddress" class="hidden">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($order, 'postcode') ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($order, 'region') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($order, 'city') ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($order, 'street') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <?= $form->field($order, 'house') ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($order, 'housing') ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($order, 'room') ?>
                </div>
            </div>
        </div>

        <div id="yourAddress" class="hidden">
            <?= $form->field($order, 'addressesArr')->listBox([0 => 'test'],
                ['class' => 'form-control', 'multiple' => true]
            ); ?>
        </div>

        <hr/>

        <div class="row">
            <?= $form->field($order, 'date_option')->radioList([
                'asap' => Yii::t('app', 'I Want As Soon As Possible'),
                'custom' => Yii::t('app', 'I Set Up Date')
            ], [
                'class' => 'checkbox-enchance text-left',
                'id' => 'date-type-radio',
                'data-name' => 'date',
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    return "<div class='radio'><input {$chk} value=\"{$value}\" id=\"date_{$index}\" type='radio' name=\"{$name}\"/><label for=\"date_{$index}\">{$label}</label></div>";
                }
            ])->label(false)?>
        </div>

        <div id="customDate" class="hidden">
            <?=$form->field($order, 'wish_date')->widget(DateTimePicker::class, [
                'options' => ['placeholder' => Yii::t('app', 'Set Your Wish Date'), 'class' => 'col-sm-4'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ])->label(false);?>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $(document).on("change", "#address-type-radio", function() {
        if($("div[data-name=addr] input:checked").val() == "your_address") {
            $("#myAddress").hide().addClass("hidden");
            $("#yourAddress").show().removeClass("hidden");
        } else {
            $("#myAddress").show().removeClass("hidden");
            $("#yourAddress").hide().addClass("hidden");
        }
    });

    $(document).on("change", "#date-type-radio", function() {
        if($("div[data-name=date] input:checked").val() == "custom") {
            $("#customDate").removeClass("hidden");
        } else {
            $("#customDate").addClass("hidden");
        }
    });
');
