<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 07.10.2016
 * Time: 11:26
 */

use common\components\CurrencyHelper;
use common\modules\store\assets\CheckoutAsset;
use common\modules\store\components\Cart;
use common\modules\store\components\CartSteps;
use common\modules\store\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $order Order */
/* @var $paymentMethods array */
/* @var $currentCheckoutStep integer */

CheckoutAsset::register($this);

?>
<?php Pjax::begin(['id' => 'pjax-reloader', 'linkSelector' => false]); ?>
    <?= CartSteps::widget(['step' => CartSteps::STEP_CHOOSE_PAYMENT_METHOD]); ?>
    <?= CartSteps::widget(['step' => CartSteps::STEP_CHOOSE_PAYMENT_METHOD, 'isMobile' => true]); ?>
    <div class="container-fluid">
        <div class="cart-box row">
            <div class="cart-left col-md-8 col-sm-8 col-xs-12">
                <?= $this->render('checkout-payment-methods', [
                    'paymentMethods' => $paymentMethods,
                    'checkoutUrl' => Url::to(['/store/checkout/order']),
                    'total' => Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT),
                    'currency_code' => Yii::$app->params['app_currency_code']
                ]) ?>

            </div>
            <div class="cart-right col-md-4 col-sm-4 col-xs-12">
                <div class="cart-header-block"><?=Yii::t('store', 'Summary')?></div>
                <div class="total-box">
                    <div class="price-line">
                        <div class="price-total-title text-left">
                            <?=Yii::t('store', 'Summary')?>
                        </div>
                        <div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT))?>
                        </div>
                    </div>
                    <div class="price-line total-all">
                        <div class="price-total-title text-left">
                            <?=Yii::t('store', 'Total')?>
                        </div>
                        <div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT))?>
                        </div>
                    </div>
                </div>
                <div class="cart-fair-play">
                    <?= Html::a(
                        Html::tag('div', Yii::t('store', 'Payment through the site is protected by secure transaction'), ['class' => 'message-container', 'style' => 'margin: 0']).
                        Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
                        ['/page/static', 'alias' => 'fair-play'],
                        [
                            'data-pjax' => 0,
                            'class' => 'image-container'
                        ]
                    ) ?>
                    <div class="post-mes" style="margin-top: 20px;">
                        <?= Html::a(Yii::t('app', 'We guarantee the execution of each order or we will refund your money back'),
                            ['/page/static', 'alias' => 'fair-play'],
                            [
                                'data-pjax' => 0,
                                'class' => 'message-container'
                            ]
                        ) ?>
                    </div>
                </div>
                <div class="payment-methods">
                    <i class="fa fa-cc-visa"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <div class="secure hint--top-right" data-hint="Your payment is secure. SSL (Secure Sockets Layer) is a method of securing payment information sent over the Internet, by encrypting personal information. This means that your name and payment information is protected and cannot be read or intercepted during transit.">
                        <i class="fa fa-lock"></i>
                        <span>Secure<br>Server</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>
<?php $this->registerJs("
      $('input[name=auth_type]').click(function () {
        $(this).tab('show');
      });
    ",
    yii\web\View::POS_READY,
    'login-tabs');

$this->registerJs("
      var currentCheckoutStep = {$currentCheckoutStep};
      $('#step_' + currentCheckoutStep + ' .step-content').removeClass('hidden');
    ",
    yii\web\View::POS_READY,
    'activate-checkout-step');

$this->registerJs("
    $('.next-trigger').click(function() {
        var inputsOnCurrentStep = $(this).parent().parent().find('input:visible');
        var currentStepObject = $(this).parent().parent();

        var isNextStepAllowed = true;

        $.each(inputsOnCurrentStep, function() {
            $('#checkout-form').yiiActiveForm('validateAttribute', $(this).attr('id'), true, 0);
        }).promise().done(function() {
            var errors = currentStepObject.find('.has-error');
            if (errors.length > 0) {
                isNextStepAllowed = false;
            }
        });

        if(isNextStepAllowed) {
            $('.checkout-steps .step-content').addClass('hidden');
            $('#step_' + currentCheckoutStep).addClass('success');

            currentCheckoutStep++;
            $('#step_' + currentCheckoutStep + ' .step-content').removeClass('hidden');
        } else {
            $('#step_' + currentCheckoutStep).removeClass('success');
        }
    });
", View::POS_END);
