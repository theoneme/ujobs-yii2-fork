<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.10.2016
 * Time: 18:47
 */

use common\modules\store\models\PaymentMethod;
use yii\helpers\Json;
use yii\web\View;

/* @var $this View */
/* @var $paymentMethods array */
/* @var $checkoutUrl string */
/* @var $params array */
/* @var $method PaymentMethod */
/* @var $currency_code string $ */
/* @var $total integer */

?>
    <div class="cart-header-block"><?=Yii::t('store', 'Choose Payment Method')?></div>
    <div class="payment-options">
        <div class="container-fluid">
            <?php if (count($paymentMethods['preferredMethods'])) { ?>
                <div class="pay-big-block clearfix">
                    <div class="pay-title">
<!--                        --><?//=Yii::t('store', 'Payment Methods')?><!--:-->
                    </div>
                    <div class="pay-cells">
                        <?php foreach ($paymentMethods['preferredMethods'] as $id => $method) {
                            if ($method->code === 'authorize') {
                                echo $this->render('@common/modules/store/views/checkout/authorize-shortcut-form', ['currency_code' => $currency_code, 'total' => $total, 'method' => $method]);
                            }
                            else {
                                echo $this->render('@common/modules/store/views/checkout/payment-method-item', ['method' => $method]);
                            }
                        } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (count($paymentMethods['additionalMethods'])) { ?>
                <div class="pay-big-block more-container clearfix">
                    <a class="pay-title show-more">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <?= Yii::t('store', 'Additional Payment Methods')?>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </a>
                    <div class="more">
                        <div class="pay-cells">
                            <?php foreach ($paymentMethods['additionalMethods'] as $id => $method) {
                                echo $this->render('@common/modules/store/views/checkout/payment-method-item', ['method' => $method]);
                            } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (count($paymentMethods['otherMethods'])) { ?>
                <div class="pay-big-block more-container clearfix">
                    <a class="pay-title show-more">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <?= Yii::t('store', 'Other Payment Methods')?>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </a>
                    <div class="more">
                        <div class="pay-cells">
                            <?php foreach ($paymentMethods['otherMethods'] as $id => $method) {
                                echo $this->render('@common/modules/store/views/checkout/payment-method-item', ['method' => $method]);
                            } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="payment-form-container"></div>
<?php
$params = Json::encode(!empty($params) ? $params : [], 320 | JSON_FORCE_OBJECT);
$isGuest = Json::encode(Yii::$app->user->isGuest);
$script = <<<JS
    $(document).on('click', '.pay-big-block .pay-cell:not([data-toggle])', function() {
        let params = $params;
        params.payment_method_id = $(this).data('id');
        $.ajax({
            url: '$checkoutUrl',
            type: 'post',
            data: params,
            dataType: 'json',
            success: function(json) {
                if (json['success'] == true) {
                    if(typeof fbq != 'undefined') {
                    	fbq('track', 'InitiateCheckout');
					}
                    $('#payment-form-container').html(json['form']);
                    $('#payment-form-container form').submit();
                }
                else {
                    alertCall('top', 'warning', json['error']);
                }
            },
        });
        return false;
    });
    $(document).on('click', '.shortcut-form-container .shortcut-submit', function() {
        if (!$isGuest) { 
            let params = $params;
            let form = $(this).closest('.shortcut-form-container');
            params.payment_method_id = form.data('id');
            $.ajax({
                url: '$checkoutUrl?shortcut=1',
                type: 'post',
                data: params,
                dataType: 'json',
                success: function(json) {
                    if (json['success'] == true) {
                        if(typeof fbq != 'undefined') {
                            fbq('track', 'InitiateCheckout');
                        }
                        form.find('.shortcut-payment-id').val(json['paymentId']);
                        form.find('form').submit();
                    }
                    else {
                        alertCall('top', 'warning', json['error']);
                    }
                },
            });
        }
        else {
            $('#ModalLogin').modal('show');
        }
        return false;
    });
    
    // $('.pay-cells').each(function(){
    //     let totalHeight = 0;
    //     let itemHeight = $('.pay-cell').outerHeight(true);
    //     $(this).find('.pay-cell').each(function(){
    //         totalHeight += $(this).outerHeight(true) + 10;
    //     });
    //     $(this).find('.pay-cell-big').each(function(){
    //         totalHeight += $(this).outerHeight(true) * 2 + 20;
    //     });
    //     totalHeight = Math.ceil((totalHeight/3)/itemHeight) * itemHeight;
    //     $(this).css('max-height', totalHeight);
    // });
JS;
$this->registerJs($script);
?>
