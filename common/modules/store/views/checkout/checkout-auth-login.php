<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 07.10.2016
 * Time: 15:18
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $loginModel \dektrium\user\models\LoginForm */

?>

<div>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'action' => ['/user/registration/login-ajax'],
    ]); ?>

    <?= $form->field($loginModel, 'login') ?>

    <?= $form->field($loginModel, 'password')->passwordInput() ?>

    <div class="text-right">
        <?= Html::submitButton(Yii::t('app', 'Continue'), ['class' => 'btn btn-md btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>