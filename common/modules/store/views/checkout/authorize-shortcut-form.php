<?php

use common\components\CurrencyHelper;
use common\modules\store\models\PaymentMethod;
use frontend\models\AuthorizeForm;

/* @var $currency_code string $ */
/* @var $total integer */
/* @var $method PaymentMethod */

$form = new AuthorizeForm();
$total = CurrencyHelper::convert($currency_code, 'USD', $total, false);
$form->scenario = $total <= 20 ? AuthorizeForm::SCENARIO_BASIC : AuthorizeForm::SCENARIO_ADVANCED;
?>
<div class="shortcut-form-container pay-cell-big" data-id="<?= $method->id?>">
    <div>
        <?= Yii::t('order', 'Pay by Credit or Debit card')?>
    </div>
    <?= $this->render('@frontend/views/authorize/' . $form->getTemplate(), ['model' => $form]);?>
</div>