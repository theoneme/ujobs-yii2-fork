<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 12.10.2016
 * Time: 15:55
 */

use common\modules\store\Module;

/* @var $this \yii\web\View */
/* @var $orderId integer */
/* @var $breadcrumbs Array */

?>

<div class="container">
    <?= yii\widgets\Breadcrumbs::widget([
        'itemTemplate' => "{link}<span></span>", // template for all links
        'activeItemTemplate' => "{link}",
        'links' => $breadcrumbs,
        'options' =>
            [
                'class' => 'breadcrumbs'
            ],
        'tag' => 'div'
    ]); ?>
    <h1><?= Yii::t('store', 'Результат') ?></h1>

    <p>
        <?=Yii::t('store', "Ваш заказ №{$orderId} успешно оформлен.")?>
    </p>
    <p>
        <?=Yii::t('store', "Вы можете вернуться на ") . \yii\helpers\Html::a(Yii::t('store', 'Главную страницу'), \yii\helpers\Url::to(['/site/index']))?>
    </p>
</div>