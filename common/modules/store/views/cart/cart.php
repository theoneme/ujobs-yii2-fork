<?php

use common\components\CurrencyHelper;
use common\models\JobPackage;
use common\models\Offer;
use common\models\Page;
use common\modules\store\assets\StoreAsset;
use common\modules\store\components\Cart;
use common\modules\store\components\CartSteps;
use common\modules\store\models\CartItemInterface;
use frontend\assets\CatalogAsset;
use frontend\assets\FairPlayAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $fairPlayPage Page */

StoreAsset::register($this);
CatalogAsset::register($this);
FairPlayAsset::register($this);
?>

<?php Pjax::begin([
    'enablePushState' => false,
    'id' => 'cart-pjax',
    'scrollTo' => false,
    'linkSelector' => false
]); ?>

<?= CartSteps::widget(['step' => CartSteps::STEP_CART]); ?>
<?= CartSteps::widget(['step' => CartSteps::STEP_CART, 'isMobile' => true]); ?>

    <div class="container-fluid">
        <div class="how-works text-center">
            <a class="close-how" href=""></a>
            <?= $this->render('@frontend/views/page/partial/fair-play', [
                'page' => $fairPlayPage
            ]) ?>
        </div>
        <div class="cart-box row">
            <div class="cart-left col-md-8 col-sm-8 col-xs-12">
                <?php if (!empty($items)) { ?>
                    <?= Html::beginForm(['/store/cart/update'], 'post', ['data-pjax' => 1]) ?>
                    <?php foreach ($items as $item) {
                        /* @var $item_model CartItemInterface */
                        $item_model = $item['item'];
                        ?>
                        <div class="product-item" data-id="<?= $item_model->id ?>">
                            <div class="cart-info-job">
                                <?= Html::a('<i class="fa fa-times fa-lg"></i>', null, [
                                    'class' => 'delete-job cart-item-remove delete-job-cross',
                                    'data-url' => Url::to(['/store/cart/remove' . ($item_model instanceof Offer ? '-offer' : '')])
                                ]) ?>
                                <?= Html::a(Html::img($item_model->getThumb()), $item_model->getUrl(), ['class' => 'cart-img']) ?>
                                <div class="cart-name">
                                    <?= Html::a($item_model->getLabel(), $item_model->getUrl(), [
                                        'class' => 'job-name',
                                    ]) ?>
                                    <?= Html::a(Html::tag('span', Yii::t('store', 'Seller')) . ": {$item_model->getSellerName()}", $item_model->getSellerUrl(), ['class' => 'job-name']) ?>
                                    <?= Html::a(Yii::t('store', 'Remove'), null, [
                                        'class' => 'delete-job cart-item-remove',
                                        'data-url' => Url::to(['/store/cart/remove' . ($item_model instanceof Offer ? '-offer' : '')])
                                    ]) ?>
                                </div>
                            </div>
                            <div class="cart-opts">
                                <div class="delete-cart-opts">
                                </div>
                                <div class="cart-opt-descr">
                                    <?= Html::encode($item_model->getLabel()) ?>
                                    <br>
                                    <?= Html::encode(strip_tags($item_model->getDescription())) ?>
                                </div>
                                <div class="cart-input-cell">
                                    <div class="cart-input">
                                        <input
                                                data-url="<?= Url::to(['/store/cart/update' . ($item_model instanceof Offer ? '-offer' : '')]) ?>"
                                                data-entity="<?= $item_model::tableName() ?>"
                                                class="count-opt product-counter" type="number"
                                                value="<?= $item['quantity'] ?>">
                                        <a class="cart-plus"><i class="fa fa-caret-up"></i></a>
                                        <a class="cart-minus"><i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                                <div class="cart-price">
                                    <?= $item_model->getPrice() ?>
                                </div>
                            </div>
                            <?php if (!empty($item['options'])) { ?>
                                <?php foreach ($item['options'] as $option) { ?>
                                    <div class="cart-opts option-item" data-id="<?= $option['id'] ?>">
                                        <div class="delete-cart-opts">
                                            <a class="delete-opts-but option-item-remove" href="">
                                                <i class="fa fa-minus-circle"></i>
                                            </a>
                                        </div>
                                        <div class="cart-opt-descr">
                                            <?= Html::encode($option['title']) ?>
                                        </div>
                                        <div class="cart-input-cell">
                                            <div class="cart-input">
                                                <input class="count-opt option-counter" type="number" value="<?= $option['quantity'] ?>">
                                                <a href="" class="cart-plus"><i class="fa fa-caret-up"></i></a>
                                                <a href="" class="cart-minus"><i class="fa fa-caret-down"></i></a>
                                            </div>
                                        </div>
                                        <div class="cart-price">
                                            <?= CurrencyHelper::convertAndFormat($item_model->currency_code, Yii::$app->params['app_currency_code'], $option['price'], true) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="job-total clearfix">
                                <span class="hidden-xs">
                                    <i class="fa fa-shield text-success"></i>
                                    <span>&nbsp;
                                        <?= Yii::t('store', 'This deal is protected by <a href=# class=how-work>secure transaction</a>') ?>
                                    </span>
                                </span>
                                <span class="fright text-uppercase">
                                    <?= Yii::t('store', 'Total') . ': ' . CurrencyHelper::format(Yii::$app->params['app_currency_code'], $item['total']) ?>
                                </span>
                            </div>
                            <?php if ($item['item'] instanceof JobPackage && !empty($item['item']->job->extras)) { ?>
                                <div class="col-md-12">
                                    <div class="job-ups">
                                        <div class="ups-title"><?= Yii::t('store', 'Available upgrades:') ?></div>
                                        <?php foreach ($item['item']->job->extras as $extra) { ?>
                                            <div class="ups-item option-item" data-id="<?= $extra->id ?>">
                                                <div class="ups-descr">
                                                    <div><?= $extra->title ?></div>
                                                    <div><small><?= $extra->description ?></small></div>
                                                </div>
                                                <div class="cart-add-opt">
                                                    <?php if(array_key_exists($extra->id, $item['options'])) { ?>
                                                        <a class="btn-middle option-item-remove" href="#" disabled>
                                                            <?= Yii::t('store', 'Remove option') ?>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a class="btn-middle add-option" href="#">
                                                            <?= Yii::t('store', 'Add for {price}', [
                                                                'price' => CurrencyHelper::convertAndFormat($extra->currency_code, Yii::$app->params['app_currency_code'], $extra->price, true)
                                                            ]) ?>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?= Html::endForm() ?>
                <?php } else { ?>
                    <p>
                        <i class="fa fa-shopping-cart text-warning"></i><span>&nbsp;<?= Yii::t('store', 'Your cart is empty') ?></span>
                    </p>
                <?php } ?>
                <div class="visible-xs">
                    <div class="add-to-cart-mob pos-bottom">
                        <?php if (!Yii::$app->user->isGuest) {
                            echo Html::a(Yii::t('store', 'Checkout'), ['/store/checkout/checkout'], [
                                'class' => 'btn-big',
                                'data-pjax' => 0
                            ]);
                        } else {
                            echo Html::a(Yii::t('store', 'Checkout'), null, [
                                'class' => 'btn-big',
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalLogin'
                            ]);
                        } ?>
                    </div>
                </div>
            </div>
            <div class="cart-right col-md-4 col-sm-4 col-xs-12">
                <div class="total-box">
                    <div class="price-line">
                        <div class="price-total-title text-left">
                            <?= Yii::t('store', 'Summary') ?>
                        </div>
                        <div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT)) ?>
                        </div>
                    </div>
                    <!--<div class="price-line">
                        <div class="price-total-title text-left">
                            <?= Yii::t('store', 'Processing fee') ?>
                        </div>
                        <div class="price-total-sum text-right">$3.50</div>
                    </div>-->
                    <div class="price-line total-all">
                        <div class="price-total-title text-left">
                            <?= Yii::t('store', 'Total') ?>
                        </div>
                        <div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], Yii::$app->cart->getTotal(Cart::ITEM_PRODUCT)) ?>
                        </div>
                        <?php if (!Yii::$app->user->isGuest) {
                            echo Html::a(Yii::t('store', 'Checkout'), ['/store/checkout/checkout'], [
                                'class' => 'btn-big hidden-xs',
                                'data-pjax' => 0
                            ]);
                        } else {
                            echo Html::a(Yii::t('store', 'Checkout'), null, [
                                'class' => 'btn-big hidden-xs',
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalLogin'
                            ]);
                        } ?>
                    </div>
                </div>
                <div class="cart-fair-play">
                    <?= Html::a(
                        Html::tag('div', Yii::t('store', 'Payment through the site is protected by secure transaction'), ['class' => 'message-container', 'style' => 'margin: 0']) .
                        Html::img('/images/new/refund/' . Yii::$app->language . '/icon.png', ['class' => 'refund-icon']),
                        ['/page/static', 'alias' => 'fair-play'],
                        [
                            'data-pjax' => 0,
                            'class' => 'image-container'
                        ]
                    ) ?>
                    <div class="post-mes" style="margin-top: 20px;">
                        <?= Html::a(Yii::t('app', 'We guarantee the execution of each order or we will refund your money back'),
                            ['/page/static', 'alias' => 'fair-play'],
                            [
                                'data-pjax' => 0,
                                'class' => 'message-container'
                            ]
                        ) ?>
                    </div>
                </div>
                <div class="payment-methods">
                    <i class="fa fa-cc-paypal"></i>
                    <i class="fa fa-cc-visa"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <i class="fa fa-cc-amex"></i>
                    <i class="fa fa-cc-diners-club"></i>
                    <i class="fa fa-cc-discover"></i>
                    <i class="fa fa-cc-jcb"></i>
                    <i class="fa fa-btc"></i>

                    <!--<div class="secure hint--top-right"
                         data-hint="Your payment is secure. SSL (Secure Sockets Layer) is a method of securing payment information sent over the Internet, by encrypting personal information. This means that your name and payment information is protected and cannot be read or intercepted during transit.">
                        <i class="fa fa-lock"></i>
                        <span>Secure<br>Server</span>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
<?php Pjax::end() ?>

<?php
$updateCartUrl = Url::to(['/store/cart/update']);
$removeCartUrl = Url::to(['/store/cart/remove']);
$script = <<<JS
  $(window).scroll(function() {
        let btn = $('.add-to-cart-mob'),
            scrollpos = $('.mobile-one-pack');
        if($(window).scrollTop()+$(window).height()>=(scrollpos.offset().top+scrollpos.height()+200) && !btn.hasClass('pos-bottom')){
            btn.addClass('pos-bottom');
        } else {
            if($(window).scrollTop()+$(window).height()<(scrollpos.offset().top+scrollpos.height()+200) && btn.hasClass('pos-bottom')) {
                btn.removeClass('pos-bottom');
            }
        }
    });
    $(document).on('click', '.cart-item-remove', function() {
        let productItem = $(this).closest('.product-item'),
        	id = productItem.attr('data-id'),
        	url = $(this).data('url'),
        	thumb = productItem.find('.cart-img img').attr('src'),
        	href = productItem.find('.cart-img').attr('href'),
        	price = productItem.find('.cart-price').html(),
        	label = productItem.find('.job-name').html();
        if(typeof Live !== 'undefined') {
            Live.removeFromCart({
                id: id,
                thumb: thumb,
                label: label,
                url: href,
                price: price
            });
        }

        $.ajax({
            url: url,
            type: 'post',
            data: {id: id},
            success: function(response) {
                $('#cart-head-container').replaceWith(response);

                if($('#cart-pjax').length) {
                    $.pjax.reload({container:'#cart-pjax'});
                }
            },
        });
    });
    $(document).on('click', '.option-item-remove', function() {
        let id = $(this).closest('.product-item').attr('data-id'),
            optionId = $(this).closest('.option-item').attr('data-id'),
            quantity = $(this).closest('.product-item').find('.product-counter').val();

        $.ajax({
            url: '$removeCartUrl',
            type: 'post',
            data: {
                optionId: optionId,
                id: id,
                quantity: quantity
            },
            success: function(response) {
                $('#cart-head-container').replaceWith(response);

                if($('#cart-pjax').length) {
                    $.pjax.reload({container:'#cart-pjax'});
                }
            },
        });
    });
    $(document).on('change', '.product-counter', function() {
        let id = $(this).closest('.product-item').attr('data-id'),
            quantity = $(this).val(),
            url = $(this).data('url'),
            entity = $(this).data('entity');
        
        $.ajax({
            url: url,
            type: 'post',
            data: {
                id: id,
                quantity: quantity,
                entity: entity
            },
            success: function(response) {
                $('#cart-head-container').replaceWith(response);

                if($('#cart-pjax').length) {
                    $.pjax.reload({container:'#cart-pjax'});
                }
            },
        });
    });
    
    $(document).on('change', '.option-counter', function() {
        let id = $(this).closest('.product-item').attr('data-id'),
            quantity = $(this).closest('.product-item').find('.product-counter').val(),
            optionId = $(this).closest('.option-item').attr('data-id'),
            optionQuantity = $(this).val();
        
        $.ajax({
            url: '$updateCartUrl',
            type: 'post',
            data: {
                id: id,
                quantity: quantity,
                optionId: optionId,
                optionQuantity: optionQuantity
            },
            success: function(response) {
                $('#cart-head-container').replaceWith(response);

                if($('#cart-pjax').length) {
                    $.pjax.reload({container:'#cart-pjax'});
                }
            },
        });
    });
    
    $(document).on('click', '.add-option', function() {
        let id = $(this).closest('.product-item').attr('data-id'),
            quantity = $(this).closest('.product-item').find('.product-counter').val(),
            optionId = $(this).closest('.option-item').attr('data-id');
        
        $.ajax({
            url: '$updateCartUrl',
            type: 'post',
            data: {
                id: id,
                quantity: quantity,
                optionId: optionId,
                optionQuantity: 1
            },
            success: function(response) {
                $('#cart-head-container').replaceWith(response);

                if($('#cart-pjax').length) {
                    $.pjax.reload({container:'#cart-pjax'});
                }
            },
        });
    });
JS;

$this->registerJs($script);