<?php

use common\components\CurrencyHelper;
use common\models\Tariff;
use common\modules\store\assets\StoreAsset;
use common\modules\store\models\PaymentMethod;
use frontend\assets\CatalogAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

StoreAsset::register($this);
CatalogAsset::register($this);

/* @var $item Tariff */
/* @var $quantity integer */
/* @var $total integer */
/* @var $discount integer */
/* @var $paymentMethods PaymentMethod[] */
/* @var $paymentMethodId integer */

$this->title = Yii::t('store', 'Tariff Cart');
?>

<?php Pjax::begin([
	'enablePushState' => false,
	'id' => 'cart-pjax',
	'scrollTo' => false,
    'linkSelector' => false
]); ?>
	<div class="container-fluid">
		<div class="cart-header row">
			<div class="cart-header-title"><?= Yii::t('store', 'Tariff Cart') ?></div>
			<?= Html::a(Yii::t('store', 'Keep Shopping'), Url::to(['/category/job-catalog']), [
				'class' => 'link-blue bright',
			]) ?>
		</div>
		<div class="cart-box row">
			<div class="cart-left col-md-8 col-sm-8 col-xs-12">
				<?= Html::beginForm(['/store/cart/tariff', 'id' => $item->id], 'post', ['data-pjax' => 1]) ?>
				<div class="product-item" data-id="<?= $item->id ?>">
					<div class="cart-info-job">
						<!--                                --><?//= Html::a(Html::img($item->getThumb()), $item->getUrl(), [
						//                                    'class' => 'cart-img',
						//                                ])
						?>
						<div class="cart-name">
							<?= Html::a(Yii::t('store', 'Tariff') . '&nbsp;' . $item->title, '#', ['class' => 'job-name']) ?>
						</div>
					</div>
					<div class="cart-opts">
						<div class="delete-cart-opts"></div>
						<div class="cart-opt-descr">
							<?= Html::encode($item->description) ?>
							<br>
						</div>
						<div class="cart-input-cell">
							<div class="cart-input">
								<div class="tariff-days">
									<?= Yii::t('store', 'Days') ?>:
								</div>
								<input class="count-opt product-counter" type="number" value="<?= $quantity ?>" name="quantity">
								<a class="cart-plus"><i class="fa fa-caret-up"></i></a>
								<a class="cart-minus"><i class="fa fa-caret-down"></i></a>
							</div>
						</div>
						<div class="cart-price">
							<?= $item->getPrice() ?>
						</div>
					</div>
					<div class="job-total">
						<?= Yii::t('store', 'Total') . ': ' . CurrencyHelper::format(Yii::$app->params['app_currency_code'], $total) ?>
					</div>
				</div>
				<div class="cart-header-block"><?= Yii::t('store', 'Discounts') ?></div>
				<div class="payment-options">
					<div class="chover">
						<div class="radio">
							<input id="discount1" type="radio" data-value="90" name="days" class="discount-days" <?= ($quantity >= 90 && $quantity < 180) ? 'checked="checked"' : '' ?>>
							<label for="discount1">
                                <span class="ctext">
                                    <?= Yii::t('store', 'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}', [
                                        'month' => 3,
                                        'discount' => Tariff::getDiscount(90) * 100,
                                        'price' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], Tariff::calculatePricePerDay($item->cost, 90), 2),
                                        'total' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], Tariff::calculateTotalPrice($item->cost, 90)),
                                    ], Yii::$app->language) ?>
                                </span>
                                <span class="item-count"></span>
							</label>
						</div>
						<div class="radio">
							<input id="discount2" type="radio" data-value="180" name="days" class="discount-days" <?= ($quantity >= 180 && $quantity < 360) ? 'checked="checked"' : '' ?>>
							<label for="discount2">
                                <span class="ctext">
                                    <?= Yii::t('store', 'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}', [
                                        'month' => 6,
                                        'discount' => Tariff::getDiscount(180) * 100,
                                        'price' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], Tariff::calculatePricePerDay($item->cost, 180), 2),
                                        'total' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], Tariff::calculateTotalPrice($item->cost, 180)),
                                    ], Yii::$app->language) ?>
                                </span>
                                <span class="item-count"></span>
                            </label>
						</div>
						<div class="radio">
							<input id="discount3" type="radio" data-value="360" name="days" class="discount-days" <?= $quantity >= 360 ? 'checked="checked"' : '' ?>>
							<label for="discount3">
                                <span class="ctext">
                                    <?= Yii::t('store', 'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}', [
                                        'month' => 12,
                                        'discount' => Tariff::getDiscount(360) * 100,
                                        'price' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], Tariff::calculatePricePerDay($item->cost, 360), 2),
                                        'total' => CurrencyHelper::format(Yii::$app->params['app_currency_code'], Tariff::calculateTotalPrice($item->cost, 360)),
                                    ], Yii::$app->language) ?>
                                </span>
                                <span class="item-count"></span>
                            </label>
						</div>
					</div>
				</div>
				<?= Html::endForm() ?>
			</div>
			<div class="cart-right col-md-4 col-sm-4 col-xs-12">
				<div class="total-box">
					<div class="price-line">
						<div class="price-total-title text-left">
							<?= Yii::t('store', 'Summary') ?>
						</div>
						<div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], $total) ?>
                        </div>
					</div>
					<?php if ($discount) { ?>
						<div class="price-line">
							<div class="price-total-title text-left">
								<?= Yii::t('store', 'Discount') ?>
							</div>
							<div class="price-total-sum text-right">
                                <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], $discount) ?>
                            </div>
						</div>
					<?php } ?>
					<div class="price-line total-all">
						<div class="price-total-title text-left">
							<?= Yii::t('store', 'Total') ?>
						</div>
						<div class="price-total-sum text-right">
                            <?= CurrencyHelper::format(Yii::$app->params['app_currency_code'], $total - $discount) ?>
						</div>
						<?= Html::beginForm(['/store/checkout/tariff-checkout'], 'post') ?>
						<input type="hidden" value="<?= $item->id ?>" name="id">
						<input type="hidden" value="<?= $quantity ?>" name="quantity">
						<?php if (!Yii::$app->user->isGuest) {
							echo Html::a(Yii::t('store', 'Checkout'), '#', [
								'class' => 'btn-big checkout',
								'data-pjax' => 0
							]);
						} else {
							echo Html::a(Yii::t('store', 'Checkout'), null, [
								'class' => 'btn-big',
								'data-toggle' => 'modal',
								'data-target' => '#ModalLogin'
							]);
						}?>
						<?= Html::endForm() ?>
					</div>
				</div>
				<div class="payment-methods">
					<i class="fa fa-cc-paypal"></i>
					<i class="fa fa-cc-visa"></i>
					<i class="fa fa-cc-mastercard"></i>
					<i class="fa fa-cc-amex"></i>
					<i class="fa fa-cc-diners-club"></i>
					<i class="fa fa-cc-discover"></i>
					<i class="fa fa-cc-jcb"></i>
					<i class="fa fa-btc"></i>

					<div class="secure hint--top-right"
					     data-hint="Your payment is secure. SSL (Secure Sockets Layer) is a method of securing payment information sent over the Internet, by encrypting personal information. This means that your name and payment information is protected and cannot be read or intercepted during transit.">
						<i class="fa fa-lock"></i> <span>Secure<br>Server</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php Pjax::end() ?>
	<div id="payment-form-container"></div>
<?php
$this->registerJs("
    $(document).on('change', '.product-counter', function() {
        $(this).closest('form').submit();
    });
    $(document).on('change', '.discount-days', function(){
        $('.product-counter').val($(this).data('value'));
        $(this).closest('form').submit();
    });
    $(document).on('click', '.checkout', function(){
        $(this).closest('form').submit();
        return false;
    });
");