<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.11.2017
 * Time: 18:14
 */

namespace common\modules\store\services;

use common\components\UrlAdvanced;
use common\models\Notification;
use common\models\PaymentHistory;
use common\models\Referral;
use common\modules\board\models\Product;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use common\modules\store\models\OrderProduct;
use common\modules\store\models\PaymentMethod;
use yii\helpers\StringHelper;

/**
 * Class SimpleOrderService
 * @package common\modules\store\services
 */
class ProductOrderService
{
    /**
     * @var Order
     */
    private $order;

    /**
     * SimpleOrderService constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $insertMode
     * @param $changedAttributes
     */
    public function process($insertMode, $changedAttributes)
    {
        $order = $this->order;

        if ($insertMode === true && !empty($order->cartItems)) {
            foreach ($order->cartItems as $cartItem) {
                (new OrderProduct([
                    'title' => StringHelper::truncate($cartItem['item']->getLabel(), 100),
                    'price' => $cartItem['item']->price,
                    'quantity' => $cartItem['quantity'],
                    'product_id' => $cartItem['item']->id,
                    'order_id' => $this->order->id,
                    'options' => $cartItem['options'],
                    'currency_code' => $order->currency_code
                ]))->save();
            }

            (new OrderHistory([
                'isBlameable' => false,
                'type' => OrderHistory::TYPE_ORDER_CREATED,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
            ]))->save();
        }

        if ($changedAttributes['status'] == Order::STATUS_REQUIRES_PAYMENT && $order->status > Order::STATUS_REQUIRES_PAYMENT) {
            (new OrderHistory([
                'isBlameable' => false,
                'type' => OrderHistory::TYPE_ORDER_PAID,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'sender_id' => $order->seller_id
            ]))->save();

            (new OrderHistory([
                'isBlameable' => false,
                'type' => OrderHistory::TYPE_REQUIREMENTS_HEADER,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'sender_id' => $order->seller_id
            ]))->save();

            (new OrderHistory([
                'isBlameable' => false,
                'type' => OrderHistory::TYPE_NEED_REQUIREMENTS,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'sender_id' => $order->seller_id
            ]))->save();

            (new Notification([
                'from_id' => $order->customer_id,
                'to_id' => $order->seller_id,
                'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                'template' => Notification::TEMPLATE_HAVE_NEW_PRODUCT_ORDER,
                'sender' => Notification::SENDER_SYSTEM,
                'linkRoute' => ['/account/order/product-history', 'id' => $order->id],
                'thumb' => $order->product->getThumb('catalog', false),
                'withEmail' => true,
                'custom_data' => json_encode([
                    'user' => $order->customer->getSellerName(),
                ])
            ]))->save();

            (new Notification([
                'to_id' => $order->customer_id,
                'subject' => Notification::SUBJECT_YOU_CREATED_NEW_ORDER,
                'template' => Notification::TEMPLATE_CREATED_PRODUCT_ORDER,
                'sender' => Notification::SENDER_SYSTEM,
                'linkRoute' => ['/account/order/product-history', 'id' => $order->id],
                'thumb' => $this->order->product->getThumb('catalog', false),
                'custom_data' => json_encode([
                    'user' => $order->seller->getSellerName(),
                ])
            ]))->save();

            if ($order->payment_method_id != PaymentMethod::find()->where(['code' => 'balance'])->select('id')->scalar()) {
                (new PaymentHistory([
                    'amount' => $order->payment->total,
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'type' => PaymentHistory::TYPE_OPERATION_REPLENISH,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_ACCOUNT_REPLENISH,
                    'user_id' => $order->customer_id,
                    'order_id' => $order->id
                ]))->save();
            }

            (new PaymentHistory([
                'amount' => $order->payment->total,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_NEG,
                'type' => PaymentHistory::TYPE_OPERATION_PURCHASE,
                'user_id' => $order->customer_id,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_JOB_PURCHASE,
                'order_id' => $order->id,
                'custom_data' => json_encode([
                    'linkLabel' => PaymentHistory::TEMPLATE_VIEW_ORDER_LABEL,
                    'link' => UrlAdvanced::to(['/account/order/product-history', 'id' => $order->id])
                ])
            ]))->save();
        }

        if (isset($changedAttributes['status']) && $changedAttributes['status'] != Order::STATUS_CANCELLED && $order->status == Order::STATUS_CANCELLED) {
            (new PaymentHistory([
                'amount' => $order->payment->total,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'type' => PaymentHistory::TYPE_OPERATION_REFUND,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_REFUND,
                'user_id' => $order->customer_id,
                'order_id' => $order->id
            ]))->save();
        }

        if (isset($changedAttributes['status']) && $changedAttributes['status'] != Order::STATUS_PRODUCT_COMPLETED && $order->status == Order::STATUS_PRODUCT_COMPLETED) {
            $sameHistory = PaymentHistory::findOne([
                'order_id' => $order->id,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'user_id' => $order->seller_id
            ]);
            if (!$sameHistory) {
                (new PaymentHistory([
                    'amount' => (int)($order->total * 0.8),
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'type' => PaymentHistory::TYPE_OPERATION_REVENUE,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_ORDER_COMPLETE,
                    'user_id' => $order->seller_id,
                    'order_id' => $order->id,
                    'custom_data' => json_encode([
                        'linkLabel' => PaymentHistory::TEMPLATE_VIEW_ORDER_LABEL,
                        'link' => UrlAdvanced::to(['/account/order/product-history', 'id' => $order->id])
                    ])
                ]))->save();
            }

            if ($order->customer->invited_by) {
                (new PaymentHistory([
                    'amount' => (int)($order->total * 0.05),
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'type' => PaymentHistory::TYPE_OPERATION_REFERRAL,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_REFERRAL_BONUS,
                    'user_id' => $order->customer->invited_by,
                    'order_id' => $order->id
                ]))->save();
                /* @var $referral Referral */
                $referral = Referral::find()->where(['referral_id' => $order->customer_id])->one();
                if ($referral !== null) {
                    $referral->updateAttributes([
                        'total_bonus' => $referral->total_bonus + $order->total * 0.05,
                        'status' => Referral::STATUS_BOUGHT
                    ]);
                }
            }
            if ($order->seller->invited_by) {
                (new PaymentHistory([
                    'amount' => (int)($order->total * 0.05),
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'type' => PaymentHistory::TYPE_OPERATION_REFERRAL,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_REFERRAL_BONUS,
                    'user_id' => $order->seller->invited_by,
                    'order_id' => $order->id
                ]))->save();
                /* @var $referral Referral */
                $referral = Referral::find()->where(['referral_id' => $order->seller_id])->one();
                if ($referral !== null) {
                    $referral->updateAttributes([
                        'total_bonus' => $referral->total_bonus + $order->total * 0.05,
                        'status' => Referral::STATUS_BOUGHT
                    ]);
                }
            }

            (new PaymentHistory([
                'amount' => 1,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'type' => PaymentHistory::TYPE_OPERATION_BONUS,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_SALE_BONUS,
                'user_id' => $order->seller_id,
                'order_id' => $order->id
            ]))->save();

            /* @var $product Product*/
            $product = Product::find()->where(['id' => $order->orderProduct->product_id])->one();
            $product->updateAttributes(['amount' => max(0, $product->amount - $order->orderProduct->quantity)]);
        }
    }
}