<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.11.2017
 * Time: 18:14
 */

namespace common\modules\store\services;

use common\components\UrlAdvanced;
use common\models\Attachment;
use common\models\Notification;
use common\models\PaymentHistory;
use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use common\modules\store\models\OrderProduct;
use common\modules\store\models\PaymentMethod;
use yii\db\Expression;
use yii\helpers\StringHelper;

/**
 * Class ProductTenderOrderService
 * @package common\modules\store\services
 */
class ProductTenderOrderService
{
    /**
     * @var Order
     */
    private $order;

    /**
     * SimpleOrderService constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $insertMode
     * @param $changedAttributes
     */
    public function process($insertMode, $changedAttributes)
    {
        $order = $this->order;

        if ($insertMode === true && !empty($order->cartItems)) {
            foreach ($order->cartItems as $cartItem) {
                (new OrderProduct([
                    'title' => StringHelper::truncate($cartItem['item']->getLabel(), 100),
                    'price' => $cartItem['item']->price,
                    'quantity' => $cartItem['quantity'],
                    'product_id' => $cartItem['item']->id,
                    'order_id' => $order->id,
                    'currency_code' => $order->currency_code
                ]))->save();
            }

            (new OrderHistory([
                'isBlameable' => false,
                'type' => OrderHistory::TYPE_TENDER_RESPONSE_HEADER,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'custom_data' => json_encode([
                    'user' => $order->seller->getSellerName(),
                    'tender' => $order->product->getLabel()
                ])
            ]))->save();

            (new Notification([
                'from_id' => $order->seller_id,
                'to_id' => $order->customer_id,
                'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                'template' => Notification::TEMPLATE_HAVE_TENDER_RESPONSE,
                'sender' => Notification::SENDER_SYSTEM,
                'linkRoute' => ['/account/order/product-tender-history', 'id' => $order->id],
                'thumb' => $order->product->getThumb('catalog', false),
                'withEmail' => true,
                'custom_data' => json_encode([
                    'user' => $order->seller->getSellerName(),
                    'tender' => $order->product->getLabel()
                ])
            ]))->save();

            (new Notification([
                'to_id' => $order->seller_id,
                'subject' => Notification::SUBJECT_YOU_CREATED_NEW_ORDER,
                'template' => Notification::TEMPLATE_CREATED_TENDER_RESPONSE,
                'sender' => Notification::SENDER_SYSTEM,
                'linkRoute' => ['/account/order/product-tender-history', 'id' => $order->id],
                'thumb' => $this->order->product->getThumb('catalog', false),
                'custom_data' => json_encode([
                    'tender' => $order->product->getLabel(),
                ])
            ]))->save();

            (new OrderHistory([
                'type' => OrderHistory::TYPE_TENDER_RESPONSE_MESSAGE,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
                'content' => $order->customComment,
            ]))->save();

            if ($order->customComment || $order->is_price_approved === false) {
                $responseHistory = new OrderHistory([
                    'type' => (boolean)$order->is_price_approved === false ? OrderHistory::TYPE_TENDER_RESPONSE_PRICE : OrderHistory::TYPE_CUSTOM_MESSAGE,
                    'seller_id' => $order->seller_id,
                    'customer_id' => $order->customer_id,
                    'order_id' => $order->id,
                    'content' => $order->customComment,
                    'custom_data' => json_encode([
                        'price' => [
                            'from' => $order->currency_code,
                            'amount' => $order->total
                        ]
                    ])
                ]);
                if ($responseHistory->save()) {
                    if (!empty($order->attachments)) {
                        /** @var Attachment $attachment */
                        foreach ($order->attachments as $attachment) {
                            $attachment->entity_id = $responseHistory->id;
                            $attachment->save();
                        }
                    }
                }
            }
        }

        if (isset($changedAttributes['status']) && $changedAttributes['status'] === Order::STATUS_PRODUCT_TENDER_DELIVERED && in_array($order->status, [Order::STATUS_PRODUCT_TENDER_PAID, Order::STATUS_PRODUCT_TENDER_AWAITING_REVIEW])) {
            (new PaymentHistory([
                'amount' => (int)($order->total * 0.8),
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'type' => PaymentHistory::TYPE_OPERATION_REVENUE,
                'user_id' => $order->seller_id,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_ORDER_COMPLETE,
                'order_id' => $order->id,
                'custom_data' => json_encode([
                    'linkLabel' => PaymentHistory::TEMPLATE_VIEW_ORDER_LABEL,
                    'link' => UrlAdvanced::to(['/account/order/product-tender-history', 'id' => $order->id])
                ])
            ]))->save();
        }

        if (isset($changedAttributes['status']) && $changedAttributes['status'] >= Order::STATUS_PRODUCT_TENDER_PAID && $order->status == Order::STATUS_CANCELLED) {
            $total = $order->payment->total - PaymentHistory::find()
                    ->leftJoin('currency_exchange', "code_from = payment_history.currency_code AND code_to = '" . $order->payment->currency_code . "'")
                    ->where(['order_id' => $order->id, 'type' => PaymentHistory::TYPE_OPERATION_REVENUE])
                    ->sum(new Expression('(CASE WHEN (ratio IS NULL) THEN payment_history.amount ELSE payment_history.amount * ratio END)'));
            (new PaymentHistory([
                'amount' => $total,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                'type' => PaymentHistory::TYPE_OPERATION_REFUND,
                'user_id' => $order->customer_id,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_REFUND,
                'order_id' => $order->id,
                'custom_data' => json_encode([
                    'linkLabel' => PaymentHistory::TEMPLATE_VIEW_ORDER_LABEL,
                    'link' => UrlAdvanced::to(['/account/order/product-tender-history', 'id' => $order->id])
                ])
            ]))->save();
        }

        if ((isset($changedAttributes['status']) && $changedAttributes['status'] < Order::STATUS_PRODUCT_TENDER_PAID && $order->status == Order::STATUS_PRODUCT_TENDER_PAID)
            || (isset($changedAttributes['payment_id']) && $changedAttributes['payment_id'] != $order->payment_id && $order->status == Order::STATUS_PRODUCT_TENDER_PAID)
        ) {
            (new OrderHistory([
                'isBlameable' => false,
                'type' => OrderHistory::TYPE_TENDER_PRODUCT_PAID,
                'seller_id' => $order->seller_id,
                'customer_id' => $order->customer_id,
                'order_id' => $order->id,
            ]))->save();

            if ($order->payment_method_id != PaymentMethod::find()->where(['code' => 'balance'])->select('id')->scalar()) {
                (new PaymentHistory([
                    'amount' => $order->payment->total,
                    'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_POS,
                    'type' => PaymentHistory::TYPE_OPERATION_REPLENISH,
                    'user_id' => $order->customer_id,
                    'template' => PaymentHistory::TEMPLATE_PAYMENT_ACCOUNT_REPLENISH,
                    'order_id' => $order->id,
                ]))->save();
            }

            (new PaymentHistory([
                'amount' => $order->payment->total,
                'balance_change' => PaymentHistory::TYPE_BALANCE_CHANGE_NEG,
                'type' => PaymentHistory::TYPE_OPERATION_PURCHASE,
                'user_id' => $order->customer_id,
                'template' => PaymentHistory::TEMPLATE_PAYMENT_JOB_PURCHASE,
                'order_id' => $order->id,
                'custom_data' => json_encode([
                    'linkLabel' => PaymentHistory::TEMPLATE_VIEW_ORDER_LABEL,
                    'link' => UrlAdvanced::to(['/account/order/product-tender-history', 'id' => $order->id])
                ])
            ]))->save();
        }
    }
}