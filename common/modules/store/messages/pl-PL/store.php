<?php 
 return [
    'Account replenishment on {0} site' => 'Dodawanie konta na stronie {0}',
    'Add for {price}' => 'Dodaj za {price}',
    'Additional Order Info' => 'Dodatkowe informacje o zamówieniu',
    'Available upgrades:' => 'Dostępne opcje:',
    'Cart' => 'Kosz',
    'Cart ({count})' => 'Koszyk ({count})',
    'Cart Is Empty' => 'Koszyk jest pusty',
    'Checkout' => 'Realizacja zamówienia',
    'Choose goods' => 'Wybór usługi lub towaru',
    'Code' => 'Kod',
    'Days' => 'Dni',
    'Discount' => 'Rabat',
    'Discounts' => 'Rabaty',
    'Enabled' => 'Włączony',
    'Error occurred during order saving' => 'Wystąpił błąd podczas zapisywania zamówienia',
    'Getting premium on {0} site' => 'Płatność premium dostępu na stronie {0}',
    'Go to payment' => 'Przejdź do płatności',
    'I want to change seller or service in order' => 'Chcę zmienić sprzedawcę lub usługę w zamówieniu',
    'Keep Shopping' => 'Kontynuuj zakupy',
    'Logo' => 'Logo',
    'Name' => 'Nazwa',
    'New Seller' => 'Nowy sprzedawca',
    'Order' => 'Zamówienie',
    'Payment Methods' => 'Sposoby płatności',
    'Payment for service on {0} site' => 'Płatność za usługi na miejscu {0}',
    'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}' => 'Płatność za {month, plural, one{# miesiąc} few{# miesiąca} many{# miesięcy} other{# miesięcy}}. Rabat {discount}%. {price} w dzień. Ogółem: {total}',
    'Payment method' => 'Sposób płatności',
    'Payment through the site is protected by secure transaction' => 'Płatność za pośrednictwem strony internetowej są chronione bezpiecznym transakcją',
    'Phone' => 'Telefon',
    'Quantity' => 'Ilość',
    'Remove' => 'Usunąć',
    'Remove option' => 'Usuń opcję',
    'Seller' => 'Sprzedawca',
    'Sort' => 'Sortowanie',
    'Status' => 'Status',
    'Submit requirements and start order' => 'Objaśnienie części i zamówienia rozpoczęcie',
    'Summary' => 'Wyniki',
    'Tariff' => 'Taryfa',
    'Tariff Cart' => 'Kosz taryf',
    'This deal is protected by <a href=# class=how-work>secure transaction</a>' => 'Zakup zabezpieczona <a href="#" class="how-work">bezpiecznym transakcją</a>',
    'Title' => 'Nazwa',
    'Total' => 'Wszystkim',
    'Value' => 'Wartość',
    'View Cart' => 'Do koszyka',
    'Your cart is empty' => 'Twój koszyk jest pusty',
    'Other Payment Methods' => 'Inne metody płatności',
    'Additional Payment Methods' => 'Dodatkowe metody płatności',
    'Additional' => 'Dodatkowy',
    'Choose Payment Method' => 'Wybierz sposób płatności',
];