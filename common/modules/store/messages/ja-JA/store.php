<?php 
 return [
    'Account replenishment on {0} site' => '詰替えのためのサイトは{0}',
    'Add for {price}' => '追加{price}',
    'Additional Order Info' => '追加情報注',
    'Available upgrades:' => '利用可能なオプション:',
    'Cart' => 'バスケット',
    'Cart ({count})' => 'バスケット({count})',
    'Cart Is Empty' => 'カートは空です',
    'Checkout' => 'ご注文',
    'Choose goods' => '選択により良い商品-サービス',
    'Code' => 'コード',
    'Days' => '日',
    'Discount' => '割引',
    'Discounts' => '割引',
    'Enabled' => '含まれま',
    'Error occurred during order saving' => 'エラーが発生した省令',
    'Getting premium on {0} site' => 'の支払プレミアムアクセスのサイトは{0}',
    'Go to payment' => '決済に進む',
    'I want to change seller or service in order' => 'を変えたいのですが、セラー又はサービス',
    'Keep Shopping' => '買い物を続け',
    'Logo' => 'ロゴ',
    'Name' => '名称',
    'New Seller' => '新たなセラー',
    'Order' => 'ご注文',
    'Payment Methods' => 'お支払い方法',
    'Payment for service on {0} site' => 'お支払いサービスの提供{0}',
    'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}' => 'お支払いのための{month, plural, one{#月} few{#ヶ月} many{#ヶ月} other{#ヶ月}}のようになります。 割引{discount}%ます。 {price}です。 合計:{total}',
    'Payment method' => 'お支払い方法',
    'Payment through the site is protected by secure transaction' => 'お支払いwebサイトでは保護されて安全な取引',
    'Phone' => '電話',
    'Quantity' => 'の数',
    'Remove' => '除',
    'Remove option' => 'オプションを外し',
    'Seller' => 'セラー',
    'Sort' => 'ソート',
    'Status' => '状況',
    'Submit requirements and start order' => '精密部品の受注開始',
    'Summary' => 'その結果',
    'Tariff' => '率',
    'Tariff Cart' => 'バスケットは関税',
    'This deal is protected by <a href=# class=how-work>secure transaction</a>' => '購入を保護し、 <a href="#" class="how-work">安全な取引</a>',
    'Title' => '名称',
    'Total' => 'だけで',
    'Value' => 'の価値',
    'View Cart' => 'バスケットに',
    'Your cart is empty' => 'おバスケットが空である',
    'Other Payment Methods' => 'その他の方法でのお支払い',
    'Additional Payment Methods' => '追加の支払い方法',
    'Additional' => '追加',
    'Choose Payment Method' => 'お支払方法の選択',
];