<?php 
 return [
    'Account replenishment on {0} site' => 'Поповнення рахунку на сайті {0}',
    'Add for {price}' => 'Додати за {price}',
    'Additional Order Info' => 'Додаткова інформація по замовленню',
    'Available upgrades:' => 'Доступні опції:',
    'Cart' => 'Кошик',
    'Cart ({count})' => 'Кошик ({count})',
    'Cart Is Empty' => 'Кошик порожній',
    'Checkout' => 'Оформлення замовлення',
    'Choose goods' => 'Вибір послуги або товару',
    'Code' => 'Код',
    'Code From' => '',
    'Code To' => '',
    'Content' => '',
    'Created At' => '',
    'Customer ID' => '',
    'Days' => 'Днів',
    'Decimal Place' => '',
    'Discount' => 'Знижка',
    'Discounts' => 'Знижки',
    'Email' => '',
    'Enabled' => 'Включений',
    'Error occurred during order saving' => 'Сталася помилка при збереженні замовлення',
    'Getting premium on {0} site' => 'Оплата premium доступу на сайті {0}',
    'Go to payment' => 'Перейти до оплати',
    'I want to change seller or service in order' => 'Я хочу змінити продавця чи сервіс у замовленнi',
    'ID' => '',
    'Is Approved' => '',
    'Job' => '',
    'Job Extra ID' => '',
    'Keep Shopping' => 'Продовжити покупки',
    'Logo' => 'Логотип',
    'Name' => 'Ім\'я',
    'New Seller' => 'Новий продавець',
    'Order' => 'Замовлення',
    'Order ID' => '',
    'Payment Method ID' => '',
    'Payment Methods' => 'Способи оплати',
    'Payment for service on {0} site' => 'Оплата послуги на сайті {0}',
    'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}' => 'Оплата за {month, plural, one {# місяць} few {# місяці} many {# місяців} other {# місяців}}. Знижка {discount}%. {Price} в день. Разом: {total}',
    'Payment method' => 'Спосіб оплати',
    'Payment through the site is protected by secure transaction' => 'Оплата через сайт захишена безпечною угодою',
    'Phone' => 'Телефон',
    'Processing fee' => '',
    'Product ID' => '',
    'Quantity' => 'Кількість',
    'Ratio' => '',
    'Remove' => 'Видалити',
    'Remove option' => 'Видалити опцію',
    'Seller' => 'Продавець',
    'Seller ID' => '',
    'Sort' => 'Сортування',
    'Status' => 'Статус',
    'Submit requirements and start order' => 'Уточнення деталей і початок замовлення',
    'Summary' => 'Підсумки',
    'Symbol Left' => '',
    'Symbol Right' => '',
    'Tariff' => 'Тариф',
    'Tariff Cart' => 'Кошик тарифів',
    'This deal is protected by <a href=# class=how-work>secure transaction</a>' => 'Ця покупка захищена <a href=# class=how-work>безпечною угодою</a>',
    'Title' => 'Назва',
    'Total' => 'Усього',
    'Type' => '',
    'Updated At' => '',
    'Value' => 'Значення',
    'View Cart' => 'В кошик',
    'Your cart is empty' => 'Ваш кошик порожній',
    'Вы можете вернуться на ' => '',
    'Главную страницу' => '',
    'Не найдено доступных способов оплаты' => '',
    'После прохождения процедуры оплаты, Вы получите готовый документ на E-mail адрес, указанный при оформлении данного заказа.' => '',
    'Проверьте правильность введенных данных и подтвердите Ваш заказ.' => '',
    'Результат' => '',
    'Other Payment Methods' => 'Інші способи оплати',
    'Additional Payment Methods' => 'Додаткові способи оплати',
    'Additional' => 'Додатковий',
    'Choose Payment Method' => 'Виберіть спосіб оплати',
];