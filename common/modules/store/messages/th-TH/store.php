<?php 
 return [
    'Account replenishment on {0} site' => 'เติมสำหรับเว็บไซต์{0}',
    'Add for {price}' => 'เพิ่ม{price}',
    'Additional Order Info' => 'ข้อมูลเพิ่มเติมเรื่องกำลังจะออกคำสั่ง',
    'Available upgrades:' => 'ที่มีอยู่ตัวเลือก:',
    'Cart' => 'ตะกร้า',
    'Cart ({count})' => 'ตะกร้า({count})',
    'Cart Is Empty' => 'รถว่างเปล่า',
    'Checkout' => 'กำลังจะออกคำสั่ง',
    'Choose goods' => 'เลือกบริการหรือผลิตภัณฑ์',
    'Code' => 'รหัส',
    'Days' => 'วัน',
    'Discount' => 'ส่วนลด',
    'Discounts' => 'สติกเกอร์ราคา',
    'Enabled' => 'รวมอ',
    'Error occurred during order saving' => 'เกิดข้อผิดพลาดขึ้นระหว่างทำการบันทึกคำสั่ง',
    'Getting premium on {0} site' => 'ค่าจ้างของชั้นยอดการเข้าถึงบนเว็บไซต์{0}',
    'Go to payment' => 'ดำเนินการเพื่อจ่ายเงิน',
    'I want to change seller or service in order' => 'ฉันอยากจะเปลี่ยนคนขายหรือบริการอยู่ในคำสั่ง',
    'Keep Shopping' => 'ทำต่อไปช้อปปิ้ง',
    'Logo' => 'โลโก้',
    'Name' => 'ชื่อ',
    'New Seller' => 'คนใหม่หนังสือขาย',
    'Order' => 'กำลังจะออกคำสั่ง',
    'Payment Methods' => 'วิธีการของจ่ายเงิน',
    'Payment for service on {0} site' => 'เงินบริการบนเว็บไซต์{0}',
    'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}' => 'จ่ายเงินสำหรับ{month, plural, one{#เดือน} few{#เดือน} many{#เดือน} other{#เดือน}}. ส่วนลด{discount}%น {price}วันเดียว ทั้งหมด:{total}',
    'Payment method' => 'วิธีการจ่ายเงิน',
    'Payment through the site is protected by secure transaction' => 'จ่ายผ่านทางเว็บไซต์ปกป้องด้วยความปลอดภัยไว้ซึ่งการต่อรอง',
    'Phone' => 'โทรศัพท์',
    'Quantity' => 'จำนวน',
    'Remove' => 'เพื่อลบ',
    'Remove option' => 'เพื่อลบเลือกใช้ตัวเลือก',
    'Seller' => 'หนังสือขาย',
    'Sort' => 'การเรียงลำดับ',
    'Status' => 'สถานะ',
    'Submit requirements and start order' => 'Refinement ของส่วนและเริ่มต้นขอสั่งให้',
    'Summary' => 'ผล',
    'Tariff' => 'อัตราการ',
    'Tariff Cart' => 'ตะกร้าของ tariffs',
    'This deal is protected by <a href=# class=how-work>secure transaction</a>' => 'การซื้องได้รับการคุ้มครอง <a href="#" class="how-work">ปลอดภัยการต่อรอง</a>',
    'Title' => 'ชื่อ',
    'Total' => 'แค่',
    'Value' => 'ค่า',
    'View Cart' => 'อยู่ในตะกร้า',
    'Your cart is empty' => 'คุณตะกร้างว่างเปล่า',
    'Other Payment Methods' => 'อีกวิธีการของจ่ายเงิน',
    'Additional Payment Methods' => 'เพิ่มเติมวิธีการของจ่ายเงิน',
    'Additional' => 'เพิ่มเติม',
    'Choose Payment Method' => 'เลือกวิธีการจ่ายเงิน',
];