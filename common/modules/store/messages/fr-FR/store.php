<?php 
 return [
    'Account replenishment on {0} site' => 'La recharge sur le site {0}',
    'Add for {price}' => 'Ajouter {price}',
    'Additional Order Info' => 'Des informations sur la commande',
    'Available upgrades:' => 'Les options disponibles sont:',
    'Cart' => 'Panier',
    'Cart ({count})' => 'Panier ({count})',
    'Cart Is Empty' => 'Votre panier est vide',
    'Checkout' => 'La prsentation de la commande',
    'Choose goods' => 'Le choix du service ou du produit',
    'Code' => 'Code',
    'Days' => 'Jours',
    'Discount' => 'Discount',
    'Discounts' => 'Rabais',
    'Enabled' => 'Inclus',
    'Error occurred during order saving' => 'Une erreur s\'est produite lors de l\'enregistrement de la commande',
    'Getting premium on {0} site' => 'Le paiement premium d\'accès sur le site {0}',
    'Go to payment' => 'Procéder au paiement',
    'I want to change seller or service in order' => 'Je veux changer le vendeur ou le service dans l\'ordre',
    'Keep Shopping' => 'Continuer les achats',
    'Logo' => 'Le logo',
    'Name' => 'Le nom de',
    'New Seller' => 'Un nouveau vendeur',
    'Order' => 'Commande',
    'Payment Methods' => 'Moyens de paiement',
    'Payment for service on {0} site' => 'Le paiement du service sur le site {0}',
    'Payment for {month, plural, one{# month} other{# months}}. Discount {discount}%. {price} per day. Total: {total}' => 'Le paiement pour le {month, plural, one{# mois} few{# mois} many{# mois} other{# mois}}. Réduction de {discount}%. {price} par jour. Total: {total}',
    'Payment method' => 'Mode de paiement',
    'Payment through the site is protected by secure transaction' => 'Le paiement via le site web est protégé par une transaction sécurisée',
    'Phone' => 'Téléphone',
    'Quantity' => 'Le nombre de',
    'Remove' => 'Supprimer',
    'Remove option' => 'Supprimer l\'option',
    'Seller' => 'Le vendeur',
    'Sort' => 'Le tri',
    'Status' => 'Le statut de',
    'Submit requirements and start order' => 'La clarification des détails et prise de commande',
    'Summary' => 'Résultats',
    'Tariff' => 'Tarif',
    'Tariff Cart' => 'Panier de tarifs',
    'This deal is protected by <a href=# class=how-work>secure transaction</a>' => 'L\'achat est protégé par <a href="#" class="how-work">une transaction sécurisée</a>',
    'Title' => 'Le nom de',
    'Total' => 'Tout',
    'Value' => 'La valeur',
    'View Cart' => 'Dans le panier',
    'Your cart is empty' => 'Votre panier est vide',
    'Other Payment Methods' => 'D\'autres moyens de paiement',
    'Additional Payment Methods' => 'D\'autres moyens de paiement',
    'Additional' => 'Supplémentaire',
    'Choose Payment Method' => 'Sélectionnez le mode de paiement',
];