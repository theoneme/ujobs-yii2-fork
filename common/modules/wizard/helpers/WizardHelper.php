<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.12.2017
 * Time: 11:05
 */

namespace common\modules\wizard\helpers;

use common\modules\wizard\models\entity\Room;
use common\modules\wizard\models\WizardItem;
use yii\helpers\ArrayHelper;


/**
 * Class Room
 * @package common\modules\wizard\models\entity
 */
class WizardHelper
{
    /**
     * @param $item
     * @param Room $room
     * @return array
     */
    public static function buildWizardTree($item, Room $room)
    {
        if (!is_array($item)) {
            $item = [$item->attributes];
        }

        foreach ($item as $id => $range) {
            /** @var WizardItem[] $children */
            $childrenQuery = WizardItem::find()
                ->joinWith([
                    'wizardOptions.translations',
                    'wizardExtras.translations',
                    'wizardOptions.wizardDetailCategories.translations',
                    'wizardOptions.wizardDetailCategories.wizardDetails.translations',
                    'translations'
                ])
                ->where(['>', 'lft', $range['lft']])
                ->andWhere(['<', 'rgt', $range['rgt']])
                ->andWhere(['lvl' => ($range['lvl'] + 1)])
                ->indexBy('id')
                ->asArray();
            if ($range->lvl === 1) {
                $childrenQuery->andWhere(['entity' => 'room', 'entity_id' => $room->type]);
            }
            $children = $childrenQuery->all();

            $item[$id]['children'] = self::buildWizardTree($children, $room);
        }

        return $item;
    }

    /**
     * @param $array
     * @param $searchKey
     * @param string $indexBy
     * @return array
     */
    public static function getElementsByKey($array, $searchKey, $indexBy = null)
    {
        $results = [];

        foreach ($array as $key => $item) {
            if ($key === $searchKey && !empty($item)) {
                if($indexBy !== null) {
                    $results[$item[0][$indexBy]] = $item;
                } else {
                    $results[] = $item;
                }
            } else if (is_array($item)) {
                $results = ArrayHelper::merge($results, self::getElementsByKey($item, $searchKey, $indexBy));
            }
        }

        return $results;
    }
}
