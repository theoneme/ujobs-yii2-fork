<?php 
 return [
    'Add Room' => 'Thêm phòng',
    'Apartment Area (square meters)' => 'Kích thước của căn hộ (mét vuông)',
    'Apartment Details' => 'Các thông số của căn hộ',
    'Apartment Style' => 'Phong cách của căn hộ',
    'Apartment sizes' => 'Kích thước của căn hộ',
    'Base price' => 'Các cơ sở giá',
    'Bathroom' => 'Bồn tắm',
    'Bedroom' => 'Phòng ngủ',
    'Checkout' => 'Vấn đề',
    'City' => 'Các thành phố',
    'Classic' => 'Cổ điển',
    'Content' => 'Các nội dung',
    'Currency Code' => 'Tệ mã',
    'Default detail' => 'Mục tiêu chuẩn',
    'Default option' => 'Chuẩn lựa chọn',
    'Details' => 'Chi tiết',
    'Eclectic' => 'Pha',
    'Hide' => 'Hide',
    'Image' => 'Ảnh',
    'Kitchen' => 'Nhà bếp',
    'Living Room' => 'Phòng khách',
    'Modern' => 'Hiện đại',
    'Options' => 'Lựa chọn',
    'Options price' => 'Giá lựa chọn',
    'Please, select room type' => 'Lựa chọn các loại phòng',
    'Price' => 'Giá',
    'Room: {room}' => 'Phòng: {room}',
    'Rooms' => 'Phòng',
    'Save & Continue' => 'Tiết kiệm và tiếp tục',
    'Step' => 'Bước',
    'Style' => 'Phong cách',
    'Summary' => 'Tổng',
    'Title' => 'Tên',
    'Unknown step' => 'Không rõ bước',
    'Value' => 'Các giá trị',
    'Wall Height (meters)' => 'Trần cao (vuông)',
    '{price} for {measure}' => '{price} cho {measure}',
    'Subtitle' => 'Subtitle',
];