<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 13:46
 */

return [
    'color' => null,
    'interactive' => true,
    'sourcePath' => 'common/modules/wizard',
    'messagePath' => 'common/modules/wizard/messages',
    'languages' => ['ru-RU', 'es-ES', 'uk-UA', 'ka-GE'],
    'translator' => 'Yii::t',
    'sort' => true,
    'overwrite' => true,
    'removeUnused' => true,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'format' => 'php',
    'db' => 'db',
    'sourceMessageTable' => '{{%source_message}}',
    'messageTable' => '{{%message}}',
    'catalog' => 'messages',
    'ignoreCategories' => [],
];