<?php 
 return [
    'Add Room' => 'Lägg till rummet',
    'Apartment Area (square meters)' => 'Storleken på lägenheten (kvadratmeter)',
    'Apartment Details' => 'Parametrar av lägenheten',
    'Apartment Style' => 'Stilen i lägenheten',
    'Apartment sizes' => 'Storleken på lägenheten',
    'Base price' => 'Basen priset',
    'Bathroom' => 'Badkar',
    'Bedroom' => 'Sovrum',
    'Checkout' => 'Frågan',
    'City' => 'Staden',
    'Classic' => 'Klassiska',
    'Content' => 'Innehåll',
    'Currency Code' => 'Valuta kod',
    'Default detail' => 'Standard objekt',
    'Default option' => 'Standard alternativ',
    'Details' => 'Detaljer',
    'Eclectic' => 'Eklektisk',
    'Hide' => 'Dölja',
    'Image' => 'Bild',
    'Kitchen' => 'Kök',
    'Living Room' => 'Vardagsrum',
    'Modern' => 'Moderna',
    'Options' => 'Alternativ',
    'Options price' => 'Pris alternativ',
    'Please, select room type' => 'Välj den typ av rum',
    'Price' => 'Pris',
    'Room: {room}' => 'Rum: {room}',
    'Rooms' => 'Rummet',
    'Save & Continue' => 'Spara och fortsätt',
    'Step' => 'Steg',
    'Style' => 'Stil',
    'Summary' => 'Totalt',
    'Title' => 'Namn',
    'Unknown step' => 'Okänd steg',
    'Value' => 'Värdet',
    'Wall Height (meters)' => 'Tak höjd (meter)',
    '{price} for {measure}' => '{price} för {measure}',
    'Subtitle' => 'Textning',
];