<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.12.2017
 * Time: 13:37
 */


namespace common\modules\wizard\services;

use common\components\CurrencyHelper;
use common\modules\wizard\models\entity\Style;
use common\modules\wizard\models\frontend\ApartmentForm;
use common\modules\wizard\models\frontend\WizardForm;
use Yii;

/**
 * Class PriceCalculatorService
 * @package common\modules\wizard\dto
 */
class PriceCalculatorService
{
    /**
     * @var ApartmentForm
     */
    private $form;

    /**
     * @var array
     */
    private $tree;

    /**
     * PriceCalculatorService constructor.
     * @param WizardForm $form
     * @param array $tree
     */
    public function __construct(WizardForm $form, array $tree)
    {
        $this->tree = $tree;
        $this->form = $form;
    }

    /**
     * @return array
     */
    public function calculateBase()
    {
        $total = 0;
        $options = $this->form->getOptions();

        $styleWizardItem = $options['tree']->wizardItem;
        $style = new Style([
            'type' => $styleWizardItem->entity_id
        ]);

        $styleOption = $this->form->getOptions()['tree'];
        $areaPrice = ($this->form->apartment_area * $styleOption->price) * pow($style->getStyleCoefficient(), $this->form->wall_height);
        $total += $areaPrice;

        $itemPricesSum = array_sum(array_map(function ($value) {
            return array_pop($value);
        }, $this->form->_default_options));
        $total += $itemPricesSum;
        return [
            'total' => $total,
            'area' => $areaPrice
        ];
    }

    /**
     * @return array
     */
    public function calculateDetails()
    {
        $options = $this->form->getOptions();
        $details = $this->form->getDetails();
        $extras = $this->form->getExtras();
        $defaultOptions = array_map(function ($value) {
            return array_pop($value);
        }, $this->form->_default_options);
        $defaultDetails = array_map(function ($value) {
            return array_pop($value);
        }, $this->form->_default_details);
        $total = 0;

        unset($options['tree']);
        foreach ($options as $key => $option) {
            if (array_key_exists($option->wizard_item_id, $defaultOptions)) {
                $basePrice = $defaultOptions[$option->wizard_item_id];
                $currentOptionPrice = $option->price;

                $total += $currentOptionPrice - $basePrice;
            }
        }

        foreach ($details as $key => $detail) {
            if (array_key_exists($detail->wizard_detail_category_id, $defaultDetails)) {
                $basePrice = $defaultDetails[$detail->wizard_detail_category_id];
                $currentOptionPrice = $detail->price;

                $total += $currentOptionPrice - $basePrice;
            }
        }

        foreach ($extras as $key => $extra) {
            $total += $extra->price;
        }

        return [
            'total' => $total,
            'options' => []
        ];
    }

    /**
     * @return array
     */
    public function calculateSummary()
    {
        $res = [];
        $options = $this->form->getOptions();
        $details = $this->form->getDetails();
        $extras = $this->form->getExtras();
        $defaultDetails = array_map(function ($value) {
            return array_pop(array_flip($value));
        }, $this->form->_default_details);

        foreach ($options as $key => $option) {
            $defaultWizardItem = $this->form->_default_options[$option->wizard_item_id] ?? [];

            if (is_numeric($key)) {
                $optionDetails = [];
                $detailCategories = $option->wizardDetailCategories;
                foreach ($detailCategories as $detailCategory) {
                    foreach ($detailCategory->wizardDetails as $detail) {
                        if (array_key_exists($detail->id, $details)) {
                            $optionDetails[] = [
                                'title' => $detail->translation->title,
                                'image' => $detail->getThumb('catalog'),
                                'price' => array_key_exists($detail->wizard_detail_category_id, $defaultDetails) ?
                                    CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $detail->price) :
                                    Yii::t('wizard', 'Default detail'),
                            ];
                        }
                    }
                }

                $res[$option->wizard_item_id] = [
                    'title' => $option->wizardItem->translation->title,
                    'option' => [
                        'title' => $option->translation->title,
                        'image' => $option->getThumb('catalog'),
                        'price' => !array_key_exists($key, $defaultWizardItem) ?
                            CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $option->price) :
                            Yii::t('wizard', 'Default option'),
                        'details' => $optionDetails
                    ]
                ];
            }
        }

        foreach ($extras as $key => $extra) {
            $res[$extra->wizard_item_id]['extras'][] = [
                'title' => $extra->translation->title,
                'image' => $extra->getThumb('catalog'),
                'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $extra->price)
            ];
        }

        return [
            'total' => $this->form->details_price_data['total'] + $this->form->base_price_data['total'],
            'data' => $res
        ];
    }
}
