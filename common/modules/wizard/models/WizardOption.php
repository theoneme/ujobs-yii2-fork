<?php

namespace common\modules\wizard\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\components\CurrencyHelper;
use common\modules\store\models\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wizard_option".
 *
 * @property integer $id
 * @property integer $wizard_item_id
 * @property integer $price
 * @property integer $price_operation
 * @property string $currency_code
 * @property string $image
 *
 * @property WizardDetailCategory[] $wizardDetailCategories
 * @property WizardItem $wizardItem
 *
 * @property WizardTranslation $translation
 * @property WizardTranslation[] $translations
 *
 * @mixin LinkableBehavior
 * @mixin ImageBehavior
 */
class WizardOption extends ActiveRecord
{
    /**
     * @var array
     */
    public $translationsData = [];

    /**
     * @var array
     */
    public $detailCategoryData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wizard_option';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'wizardDetailCategories'],
            ],
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'wizard'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wizard_item_id', 'price', 'price_operation'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['image'], 'string', 'max' => 200],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['wizard_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => WizardItem::class, 'targetAttribute' => ['wizard_item_id' => 'id']],
            ['currency_code', 'default', 'value' => 'RUB'],
            [['translationsData', 'detailCategoryData'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('wizard', 'ID'),
            'wizard_item_id' => Yii::t('wizard', 'Wizard Item ID'),
            'price' => Yii::t('wizard', 'Price'),
            'price_operation' => Yii::t('wizard', 'Price Operation'),
            'currency_code' => Yii::t('wizard', 'Currency Code'),
            'image' => Yii::t('wizard', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardDetailCategories()
    {
        return $this->hasMany(WizardDetailCategory::class, ['wizard_item_option_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardItem()
    {
        return $this->hasOne(WizardItem::class, ['id' => 'wizard_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wo_wt.entity' => 'wizard_option'])->from('wizard_translation wo_wt')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wo_wt.entity' => 'wizard_option'])->from('wizard_translation wo_wt')->andWhere(['wo_wt.locale' => Yii::$app->language]);
    }

    /**
     * @return int|string
     */
    public function getLabel()
    {
        return $this->translation->title;
    }

    /**
     * @return int|string
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @param null $target
     * @param bool $fromAws
     * @return string
     */
    public function getThumb($target = null, $fromAws = true)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if (!empty($this->image) && file_exists(Yii::getAlias("@frontend") . "/web" . $this->image)) {
            $image = $this->image;
        }

        if ($fromAws === false) {
            return $image;
        }

        $width = $height = null;

        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'wizard_option';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->translationsData = [];
        }
        if (!empty($this->detailCategoryData)) {
            foreach ($this->detailCategoryData as $item) {
                $this->bind('wizardDetailCategories', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->detailCategoryData = [];
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        WizardTranslation::deleteAll(['entity' => 'wizard_option', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
