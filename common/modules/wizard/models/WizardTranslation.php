<?php

namespace common\modules\wizard\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wizard_translation".
 *
 * @property integer $id
 * @property string $entity
 * @property integer $entity_id
 * @property string $title
 * @property string $subtitle
 * @property string $content
 * @property string $locale
 */
class WizardTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wizard_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['content'], 'string'],
            [['entity'], 'string', 'max' => 35],
            [['title', 'subtitle'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('wizard', 'ID'),
            'entity' => Yii::t('wizard', 'Entity'),
            'entity_id' => Yii::t('wizard', 'Entity ID'),
            'title' => Yii::t('wizard', 'Title'),
            'subtitle' => Yii::t('wizard', 'Subtitle'),
            'content' => Yii::t('wizard', 'Content'),
            'locale' => Yii::t('wizard', 'Locale'),
        ];
    }
}
