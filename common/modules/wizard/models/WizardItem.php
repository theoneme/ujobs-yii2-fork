<?php

namespace common\modules\wizard\models;

use common\behaviors\LinkableBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wizard_item".
 *
 * @property integer $id
 * @property integer $lvl
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property string $name
 * @property string $entity
 * @property integer $entity_id
 * @property string $type
 *
 * @property WizardTranslation $translation
 * @property WizardExtra[] $wizardExtras
 * @property WizardOption[] $wizardOptions
 * @property WizardTranslation[] $translations
 *
 * @mixin NestedSetsBehavior
 * @mixin LinkableBehavior
 */
class WizardItem extends ActiveRecord
{
    const TYPE_OPTIONS = 100;
    const TYPE_DETAILS = 200;
    const TYPE_DETAILS_ADVANCED = 300;

    public $children = [];

    /**
     * @var array
     */
    public $translationsData = [];
    /**
     * @var array
     */
    public $optionsData = [];
    /**
     * @var array
     */
    public $extrasData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wizard_item';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'root',
                'depthAttribute' => 'lvl',
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'wizardOptions', 'wizardExtras'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lvl', 'root', 'lft', 'rgt', 'entity_id', 'type'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['entity'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('wizard', 'ID'),
            'lvl' => Yii::t('wizard', 'Lvl'),
            'root' => Yii::t('wizard', 'Root'),
            'lft' => Yii::t('wizard', 'Lft'),
            'rgt' => Yii::t('wizard', 'Rgt'),
            'name' => Yii::t('wizard', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardExtras()
    {
        return $this->hasMany(WizardExtra::class, ['wizard_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardOptions()
    {
        return $this->hasMany(WizardOption::class, ['wizard_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardOption()
    {
        return $this->hasOne(WizardOption::class, ['wizard_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wi_wt.entity' => 'wizard_item'])->from('wizard_translation wi_wt')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wi_wt.entity' => 'wizard_item'])->from('wizard_translation wi_wt')->andWhere(['wi_wt.locale' => Yii::$app->language]);
    }

    /**
     * @return mixed
     */
    public function getTypeLabel()
    {
        return (self::getTypeLabels())[$this->type];
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            self::TYPE_OPTIONS => Yii::t('wizard', 'Options'),
            self::TYPE_DETAILS => Yii::t('wizard', 'Details'),
            self::TYPE_DETAILS_ADVANCED => Yii::t('wizard', 'Details advanced'),
        ];
    }

    public function customSave($parent_id = null, $runValidation = true, $attributes = null)
    {
        if (!$this->isNewRecord) {
            $saved = $this->save($runValidation, $attributes);
        } else if ($parent_id !== null && $parent = WizardItem::findOne($parent_id)) {
            $saved = $this->appendTo($parent, $runValidation, $attributes);
        } else {
            $saved = $this->makeRoot($runValidation, $attributes);
        }
        return $saved;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'wizard_item';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->translationsData = [];
        }
        if (!empty($this->optionsData)) {
            foreach ($this->optionsData as $item) {
                $this->bind('wizardOptions', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->optionsData = [];
        }
        if (!empty($this->extrasData)) {
            foreach ($this->extrasData as $item) {
                $this->bind('wizardExtras', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->extrasData = [];
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        WizardTranslation::deleteAll(['entity' => 'wizard_item', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
