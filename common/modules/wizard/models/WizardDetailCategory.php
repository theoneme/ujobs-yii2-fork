<?php

namespace common\modules\wizard\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wizard_detail_category".
 *
 * @property integer $id
 * @property integer $wizard_item_option_id
 * @property string $name
 *
 * @property WizardDetail[] $wizardDetails
 * @property WizardOption $wizardItemOption
 * @property WizardTranslation $translation
 * @property WizardTranslation[] $translations
 *
 * @mixin LinkableBehavior
 */
class WizardDetailCategory extends ActiveRecord
{
    /**
     * @var array
     */
    public $translationsData = [];

    /**
     * @var array
     */
    public $detailsData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wizard_detail_category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'wizardDetails'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wizard_item_option_id'], 'integer'],
            [['name'], 'string', 'max' => 55],
            [['wizard_item_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => WizardOption::class, 'targetAttribute' => ['wizard_item_option_id' => 'id']],
            [['translationsData', 'detailsData'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('wizard', 'ID'),
            'wizard_item_option_id' => Yii::t('wizard', 'Wizard Item Option ID'),
            'name' => Yii::t('wizard', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardDetails()
    {
        return $this->hasMany(WizardDetail::class, ['wizard_detail_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardItemOption()
    {
        return $this->hasOne(WizardOption::class, ['id' => 'wizard_item_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wdc_wt.entity' => 'wizard_detail_category'])->from('wizard_translation wdc_wt')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wdc_wt.entity' => 'wizard_detail_category'])->from('wizard_translation wdc_wt')->andWhere(['wdc_wt.locale' => Yii::$app->language]);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'wizard_detail_category';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->translationsData = [];
        }
        if (!empty($this->detailsData)) {
            foreach ($this->detailsData as $item) {
                $this->bind('wizardDetails', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->detailsData = [];
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        WizardTranslation::deleteAll(['entity' => 'wizard_detail_category', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
