<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.12.2017
 * Time: 18:15
 */

namespace common\modules\wizard\models\frontend;

use common\modules\wizard\dto\WizardStepDTO;
use common\modules\wizard\helpers\WizardHelper;
use common\modules\wizard\models\entity\Room;
use common\modules\wizard\models\WizardDetail;
use common\modules\wizard\models\WizardExtra;
use common\modules\wizard\models\WizardItem;
use common\modules\wizard\models\WizardOption;
use Yii;

/**
 * Class ApartmentForm
 * @package common\modules\wizard\models\frontend
 */
class LightApartmentForm extends WizardForm
{
    const STEP_APARTMENT_DETAILS = 100;
    const STEP_APARTMENT_STYLE = 200;
    const STEP_SUMMARY = 300;

    /**
     * @var int
     */
    public $apartment_area = 0;
    /**
     * @var int
     */
    public $wall_height = 0;
    /**
     * @var int
     */
    public $city_id = null;

    /**
     * @var Room[]
     */
    private $_rooms;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['apartment_area'], 'number', 'min' => 10],
            [['apartment_area', 'wall_height', 'city_id', 'rooms'], 'required'],
            [['wall_height'], 'number']
        ]);
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate() === false) { print_r($this);
            return false;
        }

        foreach ($this->_rooms as $key => $room) {
            if (in_array(self::STEP_APARTMENT_DETAILS, array_keys($this->completed_steps))) {
                $style = $this->_options['tree']->wizardItem;
                $tree = Yii::$app->cache->get("wizard_{$style->id}_{$room->type}");
                if ($tree === false) {
                    $tree = WizardHelper::buildWizardTree($style, $room);
                    Yii::$app->cache->set("wizard_{$style->id}_{$room->type}", $tree, 86400);
                }
                $this->defineDefaultDetails($tree);
                $this->defineDefaultOptions($tree);
                $this->calculatePrices($tree);
            }
        }

        return true;
    }

    /**
     * @return Room[]
     */
    public function getRooms()
    {
        return $this->_rooms;
    }

    /**
     * @param $rooms
     */
    public function setRooms($rooms)
    {
        $this->_rooms = $rooms;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->attributes = $input['ApartmentForm'];
        if (array_key_exists('options', $input['ApartmentForm'])) {
            $optionObjects = WizardOption::find()
                ->joinWith(['translation', 'wizardItem.translation', 'wizardDetailCategories.wizardDetails'])
                ->where(['wizard_option.id' => $input['ApartmentForm']['options']])
                ->indexBy('id')->all();

            foreach ($input['ApartmentForm']['options'] as $key => $option) {
                if ($key === 'tree') {
                    $this->_options['tree'] = $optionObjects[$option];
                } else {
                    $this->_options[$option] = $optionObjects[$option];
                }
            }
        }
        if((int)$this->step !== self::STEP_APARTMENT_STYLE) {
            if (array_key_exists('extras', $input['ApartmentForm'])) {
                $extraIds = array_reduce($input['ApartmentForm']['extras'], 'array_merge', []);
                $extraObjects = WizardExtra::find()
                    ->joinWith(['translation'])
                    ->where(['wizard_extra.id' => $extraIds])
                    ->indexBy('id')->all();

                foreach ($extraObjects as $key => $extraObject) {
                    $this->_extras[$key] = $extraObject;
                }
            }

            if (array_key_exists('details', $input['ApartmentForm'])) {
                $detailIds = array_reduce($input['ApartmentForm']['details'], 'array_merge', []);
                /** @var WizardDetail[] $detailObjects */
                $detailObjects = WizardDetail::find()->joinWith(['translation'])->where(['wizard_detail.id' => $detailIds])->all();
                foreach ($detailObjects as $detailObject) {
                    $this->_details[$detailObject->id] = $detailObject;
                }
            }
        }

        if (array_key_exists('Room', $input) && is_array($input['Room'])) {
            $rooms = [];
            foreach ($input['Room'] as $key => $item) {
                $roomObject = new Room($item);
                $rooms[] = $roomObject;
            }

            $this->_rooms = $rooms;
        }
    }
}
