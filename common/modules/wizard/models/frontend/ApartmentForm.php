<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2017
 * Time: 18:01
 */

namespace common\modules\wizard\models\frontend;

use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\wizard\dto\WizardStepDTO;
use common\modules\wizard\helpers\WizardHelper;
use common\modules\wizard\models\entity\Room;
use common\modules\wizard\models\WizardDetail;
use common\modules\wizard\models\WizardExtra;
use common\modules\wizard\models\WizardItem;
use common\modules\wizard\models\WizardOption;
use Yii;

/**
 * Class ApartmentForm
 * @package common\modules\wizard\models\frontend
 *
 * @property AttributeValue $city
 */
class ApartmentForm extends WizardForm
{
    const STEP_APARTMENT_DETAILS = 100;
    const STEP_APARTMENT_STYLE = 200;
    const STEP_SUMMARY = 300;

    /**
     * @var int
     */
    public $apartment_area = null;
    /**
     * @var int
     */
    public $wall_height = null;
    /**
     * @var int
     */
    public $city_id = null;

    /**
     * @var AttributeValue
     */
    private $_city;
    /**
     * @var Room[]
     */
    private $_rooms;
    /**
     * @var WizardItem
     */
    private $_styles;

    /**
     * @var array
     */
    public $locationData;

    /**
     * ApartmentForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $styles = WizardItem::find()->joinWith(['wizardOption.translation'])->where(['lvl' => 1])->orderBy('wizard_option.price asc')->all();
        $root = WizardItem::find()->joinWith(['translation'])->where(['lvl' => 0])->one();
        $wizardStepDTO = new WizardStepDTO($this->_default_options, $this->_default_details);
        $wizardStepDTO->treeItemsToDTO($styles, $this->_options);
        $this->_styles = $wizardStepDTO;

        $this->steps_menu = $this->steps_content = [
            self::STEP_APARTMENT_DETAILS => [
                'type' => 'apartment-details',
                'label' => Yii::t('wizard', 'Apartment Details'),
                'data' => null
            ],
            self::STEP_APARTMENT_STYLE => [
                'type' => 'step-options',
                'label' => $root->translation->title,
                'data' => $this->_styles,
                'subheading' => $root->translation->subtitle
            ],
            self::STEP_SUMMARY => [
                'type' => 'step-summary',
                'label' => Yii::t('wizard', 'Summary'),
                'data' => null
            ]
        ];

        $this->_rooms = [
            new Room(['type' => Room::ROOM_KITCHEN]),
            new Room(['type' => Room::ROOM_BATHROOM]),
            new Room(['type' => Room::ROOM_LIVING]),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['apartment_area'], 'number', 'min' => 10],
            [['apartment_area', 'wall_height', 'rooms'], 'required'],
            [['wall_height'], 'number'],
            ['locationData', 'safe'],
            ['city_id', 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'step' => Yii::t('wizard', 'Step'),
            'apartment_area' => Yii::t('wizard', 'Apartment Area (square meters)'),
            'wall_height' => Yii::t('wizard', 'Wall Height (meters)'),
            'city_id' => Yii::t('wizard', 'City')
        ]);
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate() === false) {
            return false;
        }

        $this->completed_steps[$this->step] = 1;
        $iterator = self::STEP_APARTMENT_STYLE + 1;
        foreach ($this->_rooms as $key => $room) {
            $roomStepItem = [
                'type' => 'step-options-details',
                'label' => $room->getRoomType(),
            ];

//            if (count(array_intersect(array_keys($this->completed_steps), [self::STEP_APARTMENT_STYLE, self::STEP_APARTMENT_DETAILS])) == 2) {
            if (in_array(self::STEP_APARTMENT_DETAILS, array_keys($this->completed_steps))) {
                $style = $this->_options['tree']->wizardItem;
                $tree = Yii::$app->cache->get("wizard_{$style->id}_{$room->type}");
                if ($tree === false) {
                    $tree = WizardHelper::buildWizardTree($style, $room);
                    Yii::$app->cache->set("wizard_{$style->id}_{$room->type}", $tree, 86400);
                }
                $this->defineDefaultDetails($tree);
                $this->defineDefaultOptions($tree);
                $this->calculatePrices($tree);

                $this->attachStepsFromTree($tree[0]['children'], $iterator);

            } else {
                $roomStepItem['data'] = new WizardStepDTO($this->_default_options, $this->_default_details);
                $this->steps_menu[$iterator] = $this->steps_content[$iterator] = $roomStepItem;

                $iterator++;
            }
        }

        ksort($this->steps_menu);
        ksort($this->steps_content);
        $this->step = min(array_filter(array_keys($this->steps_content), function ($value) {
            return $value > $this->step;
        }));

        return true;
    }

    /**
     * @return AttributeValue
     */
    public function getCity()
    {
        return $this->_city;
    }

    /**
     * @return Room[]
     */
    public function getRooms()
    {
        return $this->_rooms;
    }

    /**
     * @param $rooms
     */
    public function setRooms($rooms)
    {
        $this->_rooms = $rooms;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);
        if (array_key_exists('options', $input['ApartmentForm'])) {
            $optionObjects = WizardOption::find()
                ->joinWith(['translation', 'wizardItem.translation', 'wizardDetailCategories.wizardDetails'])
                ->where(['wizard_option.id' => $input['ApartmentForm']['options']])
                ->indexBy('id')->all();

            foreach ($input['ApartmentForm']['options'] as $key => $option) {
                if ($key === 'tree') {
                    $this->_options['tree'] = $optionObjects[$option];

                    $styles = WizardItem::find()->joinWith(['wizardOption.translation'])->where(['lvl' => 1])->orderBy('wizard_option.price asc')->all();
                    $wizardStepDTO = new WizardStepDTO($this->_default_options, $this->_default_details);
                    $wizardStepDTO->treeItemsToDTO($styles, $this->_options);
                    $this->_styles = $wizardStepDTO;
                    $this->steps_content[self::STEP_APARTMENT_STYLE]['data'] = $this->_styles;

                } else {
                    $this->_options[$option] = $optionObjects[$option];
                }
            }
        }
        if((int)$this->step !== self::STEP_APARTMENT_STYLE) {
            if (array_key_exists('extras', $input['ApartmentForm'])) {
                $extraIds = array_reduce($input['ApartmentForm']['extras'], 'array_merge', []);
                $extraObjects = WizardExtra::find()
                    ->joinWith(['translation'])
                    ->where(['wizard_extra.id' => $extraIds])
                    ->indexBy('id')->all();

                foreach ($extraObjects as $key => $extraObject) {
                    $this->_extras[$key] = $extraObject;
                }
            }

            if (array_key_exists('details', $input['ApartmentForm'])) {
                $detailIds = array_reduce($input['ApartmentForm']['details'], 'array_merge', []);
                /** @var WizardDetail[] $detailObjects */
                $detailObjects = WizardDetail::find()->joinWith(['translation'])->where(['wizard_detail.id' => $detailIds])->all();
                foreach ($detailObjects as $detailObject) {
                    $this->_details[$detailObject->id] = $detailObject;
                }
            }
        }

        if (array_key_exists('Room', $input) && is_array($input['Room'])) {
            $rooms = [];
            foreach ($input['Room'] as $key => $item) {
                $roomObject = new Room($item);
                $rooms[] = $roomObject;
            }

            $this->_rooms = $rooms;
        }

        if (!empty($this->locationData) && !empty($this->locationData['city']) && !empty($this->locationData['country'])) {
            $countryAttributeId = Attribute::find()->where(['alias' => 'country'])->select('id')->scalar();
            $cityAttributeId = Attribute::find()->where(['alias' => 'city'])->select('id')->scalar();
            /* @var AttributeValue $attributeValue */
            $countryAttribute = AttributeValue::find()
                ->joinWith(['translations'])
                ->where(['attribute_id' => $countryAttributeId])
                ->andWhere(['mod_attribute_description.title' => trim($this->locationData['country'])])
                ->one();

            if ($countryAttribute === null) {
                $countryAttribute = new AttributeValue([
                    'attribute_id' => $countryAttributeId,
                    'translationsArr' => [
                        [
                            'title' => trim($this->locationData['country']),
                            'locale' => 'ru-RU'
                        ],
                    ]
                ]);
                $countryAttribute->save();
            }

            $this->locationData['city'] = str_replace(['город ', 'село '], '', $this->locationData['city']);
            $cityAttribute = AttributeValue::find()
                ->joinWith(['translations'])
                ->where(['attribute_id' => $cityAttributeId])
                ->andFilterWhere(['parent_value_id' => $countryAttribute->id])
                ->andWhere(['mod_attribute_description.title' => trim($this->locationData['city'])])
                ->one();

            if ($cityAttribute === null) {
                $cityAttribute = new AttributeValue([
                    'attribute_id' => $cityAttributeId,
                    'parent_value_id' => $countryAttribute->id,
                    'translationsArr' => [
                        [
                            'title' => trim($this->locationData['city']),
                            'locale' => 'ru-RU'
                        ],
                    ]
                ]);
                $cityAttribute->save();
            }
            $this->_city = $cityAttribute;
            $this->city_id = $cityAttribute->id;
        }
        else {
            if($this->city_id !== null && !$this->_city) {
                $this->_city = AttributeValue::find()->joinWith(['translation'])->where(['mod_attribute_value.id' => $this->city_id])->one();
            }
        }
    }
}
