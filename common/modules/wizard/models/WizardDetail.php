<?php

namespace common\modules\wizard\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\components\CurrencyHelper;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wizard_detail".
 *
 * @property integer $id
 * @property integer $wizard_detail_category_id
 * @property integer $price
 * @property integer $price_operation
 * @property string $currency_code
 * @property string $image
 *
 * @property WizardDetailCategory $wizardDetailCategory
 * @property WizardTranslation $translation
 * @property WizardTranslation[] $translations
 *
 * @mixin LinkableBehavior
 */
class WizardDetail extends ActiveRecord
{
    /**
     * @var array
     */
    public $translationsData = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wizard_detail';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'wizard'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wizard_detail_category_id', 'price', 'price_operation'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['image'], 'string', 'max' => 200],
            [['wizard_detail_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => WizardDetailCategory::class, 'targetAttribute' => ['wizard_detail_category_id' => 'id']],
            ['currency_code', 'default', 'value' => 'RUB'],
            [['translationsData'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('wizard', 'ID'),
            'wizard_detail_category_id' => Yii::t('wizard', 'Wizard Detail Category ID'),
            'price' => Yii::t('wizard', 'Price'),
            'price_operation' => Yii::t('wizard', 'Price Operation'),
            'currency_code' => Yii::t('wizard', 'Currency Code'),
            'image' => Yii::t('wizard', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWizardDetailCategory()
    {
        return $this->hasOne(WizardDetailCategory::class, ['id' => 'wizard_detail_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wd_wt.entity' => 'wizard_detail'])->from('wizard_translation wd_wt')->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(WizardTranslation::class, ['entity_id' => 'id'])->andOnCondition(['wd_wt.entity' => 'wizard_detail'])->from('wizard_translation wd_wt')->andWhere(['wd_wt.locale' => Yii::$app->language]);
    }

    /**
     * @return int|string
     */
    public function getLabel()
    {
        return $this->translation->title;
    }

    /**
     * @return int|string
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @param null $target
     * @param bool $fromAws
     * @return string
     */
    public function getThumb($target = null, $fromAws = true)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if (!empty($this->image) && file_exists(Yii::getAlias("@frontend") . "/web" . $this->image)) {
            $image = $this->image;
        }

        if ($fromAws === false) {
            return $image;
        }

        $width = $height = null;

        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }


    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->translationsData)) {
            foreach ($this->translationsData as $item) {
                $item['entity'] = 'wizard_detail';
                $this->bind('translations', (int)$item['id'] ?? null)->attributes = $item;
            }
            $this->translationsData = [];
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        WizardTranslation::deleteAll(['entity' => 'wizard_detail', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
