<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.12.2017
 * Time: 13:49
 */

namespace common\modules\wizard\models\entity;

use Yii;
use yii\base\Model;

/**
 * Class Style
 * @package common\modules\wizard\models\entity
 */
class Style extends Model
{
    const STYLE_MODERN = 1;
    const STYLE_CLASSIC = 2;
    const STYLE_ECLECTIC = 3;

    /**
     * @var integer
     */
    public $type;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'range', 'in' => [
                self::STYLE_MODERN,
                self::STYLE_CLASSIC,
                self::STYLE_ECLECTIC,
            ]]
        ];
    }

    /**
     * @return int
     */
    public function getStylePrice()
    {
        return ($this->getStylePrices())[$this->type] ?? 0;
    }

    /**
     * @return array
     */
    public function getStylePrices()
    {
        return [
            self::STYLE_MODERN => 14000,
            self::STYLE_ECLECTIC => 18000,
            self::STYLE_CLASSIC => 20000,
        ];
    }

    /**
     * @return int
     */
    public function getStyleCoefficient()
    {
        return ($this->getStyleCoefficients())[$this->type] ?? 1;
    }

    /**
     * @return array
     */
    public function getStyleCoefficients()
    {
        return [
            self::STYLE_MODERN => 1.15,
            self::STYLE_ECLECTIC => 1.16,
            self::STYLE_CLASSIC => 1.17,
        ];
    }

    /**
     * @return mixed
     */
    public function getStyleType()
    {
        return ($this->getStyleTypes())[$this->type];
    }

    /**
     * @return array
     */
    public static function getStyleTypes()
    {
        return [
            self::STYLE_MODERN => Yii::t('wizard', 'Modern'),
            self::STYLE_ECLECTIC => Yii::t('wizard', 'Eclectic'),
            self::STYLE_CLASSIC => Yii::t('wizard', 'Classic'),
        ];
    }
}
