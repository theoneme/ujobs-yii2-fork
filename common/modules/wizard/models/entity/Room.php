<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 12:54
 */

namespace common\modules\wizard\models\entity;

use Yii;
use yii\base\Model;

/**
 * Class Room
 * @package common\modules\wizard\models\entity
 */
class Room extends Model
{
    const ROOM_KITCHEN = 1;
    const ROOM_BATHROOM = 2;
    const ROOM_BEDROOM = 3;
    const ROOM_LIVING = 4;

    /**
     * @var integer
     */
    public $number;

    /**
     * @var integer
     */
    public $type;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'range', 'in' => [
                self::ROOM_KITCHEN,
                self::ROOM_BATHROOM,
                self::ROOM_BEDROOM,
                self::ROOM_LIVING
            ]],
            ['number', 'integer']
        ];
    }

    /**
     * @return mixed
     */
    public function getRoomType()
    {
        return ($this->getRoomTypes())[$this->type];
    }

    /**
     * @return array
     */
    public static function getRoomTypes()
    {
        return [
            self::ROOM_KITCHEN => Yii::t('wizard', 'Kitchen'),
            self::ROOM_BATHROOM => Yii::t('wizard', 'Bathroom'),
            self::ROOM_BEDROOM => Yii::t('wizard', 'Bedroom'),
            self::ROOM_LIVING => Yii::t('wizard', 'Living Room')
        ];
    }
}
