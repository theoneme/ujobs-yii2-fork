<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.12.2017
 * Time: 16:32
 */

namespace common\modules\wizard\dto;

use common\components\CurrencyHelper;
use common\modules\wizard\helpers\WizardHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class WizardStepDTO
 * @package common\modules\wizard\dto
 */
class WizardStepDTO
{
    /**
     * @var int
     */
    public $wizardItem = 0;
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $extras = [];
    /**
     * @var array
     */
    private $defaultOptions = [];
    /**
     * @var array
     */
    private $defaultDetails = [];

    /**
     * WizardStepDTO constructor.
     * @param $defaultOptions
     * @param $defaultDetails
     */
    public function __construct($defaultOptions, $defaultDetails)
    {
        $this->defaultDetails = array_map(function ($value) {
            return array_pop(array_flip($value));
        }, $defaultDetails);
        $this->defaultOptions = array_map(function ($value) {
            return array_pop(array_flip($value));
        }, $defaultOptions);
    }

    /**
     * @param array $treeItems
     * @param $options
     */
    public function treeItemsToDTO(array $treeItems, $options)
    {
        foreach ($treeItems as $key => $item) {
            $childrenImages = $item->children()->joinWith(['wizardOptions'])->select('wizard_option.image')->column();
            if ($item->wizardOption !== null) {
                $this->options[] = [
                    'id' => $item->wizardOption->id,
                    'title' => $item->wizardOption->translation->title,
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $item->wizardOption->price),
                    'price_raw' => CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $item->wizardOption->price),
                    'currency_code' => $item->wizardOption->currency_code,
                    'image' => $item->wizardOption->getThumb(),
                    'thumb' => $item->wizardOption->getThumb('catalog'),
                    'thumbs' => array_map(function ($image) {
                        return Yii::$app->mediaLayer->getThumb($image, 'catalog');
                    }, array_filter(array_merge($childrenImages, [$item->wizardOption->getThumb('catalog')]))),
                    'images' => array_map(function ($image) {
                        return Yii::$app->mediaLayer->getThumb($image, 'image');
                    }, array_filter(array_merge($childrenImages, [$item->wizardOption->getThumb()]))),
                    'tree_item' => 'tree',
                    'checked' => !empty($options) ? $item->wizardOption->id === $options['tree']->id : $key === 0
                ];
            }
        }
    }

    /**
     * @param array $treeItem
     * @param $options
     * @param $details
     */
    public function treeArrayItemToDTO(array $treeItem, $options, $details, $extras)
    {
        $optionsArray = ArrayHelper::map($options, 'wizard_item_id', 'id');
        $mergedOptionsArray = $optionsArray + $this->defaultOptions;

        $detailsArray = ArrayHelper::map($details, 'wizard_detail_category_id', 'id');
        $mergedDetailsArray = $detailsArray + $this->defaultDetails;

        if ($treeItem['wizardOptions'] !== null) {
            $this->options = array_map(function ($value) use ($treeItem, $mergedDetailsArray, $mergedOptionsArray) {
                $childrenImages = WizardHelper::getElementsByKey($value, 'image');
                return [
                    'id' => $value['id'],
                    'title' => $value['translations'][Yii::$app->language]['title'],
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $value['price']),
                    'price_raw' => CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $value['price']),
                    'thumb' => Yii::$app->mediaLayer->getThumb($value['image'], 'catalog'),
                    'image' => Yii::$app->mediaLayer->getThumb($value['image']),
                    'thumbs' => array_map(function ($image) {
                        return Yii::$app->mediaLayer->getThumb($image, 'catalog');
                    }, array_filter(array_merge($childrenImages, [Yii::$app->mediaLayer->getThumb($value['image'], 'catalog')]))),
                    'images' => array_map(function ($image) {
                        return Yii::$app->mediaLayer->getThumb($image, 'image');
                    }, array_filter(array_merge($childrenImages, [Yii::$app->mediaLayer->getThumb($value['image'])]))),
                    'tree_item' => $treeItem['id'],
                    'checked' => in_array($value['id'], $mergedOptionsArray),
                    'detail_categories' => array_map(function ($detailCategoryValue) use ($mergedDetailsArray) {
                        return [
                            'title' => $detailCategoryValue['translations'][Yii::$app->language]['title'],
                            'details' => array_map(function ($detailValue) use ($detailCategoryValue, $mergedDetailsArray) {
                                return [
                                    'id' => $detailValue['id'],
                                    'title' => $detailValue['translations'][Yii::$app->language]['title'],
                                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $detailValue['price']),
                                    'price_raw' => CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $detailValue['price']),
                                    'thumb' => Yii::$app->mediaLayer->getThumb($detailValue['image'], 'catalog'),
                                    'image' => Yii::$app->mediaLayer->getThumb($detailValue['image']),
                                    'option_item' => $detailCategoryValue['wizard_item_option_id'],
                                    'detail_category_item' => $detailCategoryValue['id'],
                                    'checked' => in_array($detailValue['id'], $mergedDetailsArray),
                                ];
                            }, $detailCategoryValue['wizardDetails'])
                        ];
                    }, $value['wizardDetailCategories'])
                ];
            }, $treeItem['wizardOptions']);
        }

        if ($treeItem['wizardExtras'] !== null) {
            $this->extras = array_map(function ($value) use ($treeItem, $extras) {
                return [
                    'id' => $value['id'],
                    'title' => $value['translations'][Yii::$app->language]['title'],
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $value['price']),
                    'price_raw' => CurrencyHelper::convert('RUB', Yii::$app->params['app_currency_code'], $value['price']),
                    'thumb' => Yii::$app->mediaLayer->getThumb($value['image'], 'catalog'),
                    'image' => Yii::$app->mediaLayer->getThumb($value['image']),
                    'thumbs' => [Yii::$app->mediaLayer->getThumb($value['image'], 'catalog')],
                    'images' => [Yii::$app->mediaLayer->getThumb($value['image'])],
                    'tree_item' => $treeItem['id'],
                    'checked' => array_key_exists($value['id'], $extras),
                ];
            }, $treeItem['wizardExtras']);
        }
    }
}
