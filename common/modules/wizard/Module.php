<?php

namespace common\modules\wizard;

use Yii;

/**
 * Class Module
 * @package common\modules\wizard
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\wizard\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$app->i18n->translations['wizard*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/modules/wizard/messages',

            'fileMap' => [
                'wizard' => 'wizard.php',
            ],
        ];
    }
}
