<?php

use common\modules\wizard\models\entity\Room;
use common\modules\wizard\models\entity\Style;
use common\modules\wizard\models\WizardItem;
use common\modules\wizard\models\WizardTranslation;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model WizardItem */
/* @var $form ActiveForm */
/* @var $create bool */
/* @var $parent_id integer */

$entities = ['style' => 'Стиль', 'room' => 'Комната'];
$entityIds = ['style' => Style::getStyleTypes(), 'room' => Room::getRoomTypes()];
?>
<?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['/wizard/admin/item', 'id' => $model->id, 'parent_id' => $parent_id],
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
        ],
        'id' => 'tree-form',
    ]); ?>
        <div class="kv-detail-heading">
            <div class="kv-detail-crumbs">
                <span class="kv-crumb-active"><?= $create ? 'Добавление' : 'Редактирование' ?> элемента дерева</span>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary" title="" data-original-title="Сохранить">
                        <i class="glyphicon glyphicon-floppy-disk"></i>
                    </button>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="category-form">
            <div class="box">
                <div class="box-body">
                    <div class="col-xs-6">
                        <?= $form->field($model, 'name') ?>
                    </div>
                    <div class="col-xs-6">
                        <?= $form->field($model, 'type')->dropDownList(WizardItem::getTypeLabels()) ?>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="col-xs-6">
                        <?= $form->field($model, 'entity')->dropDownList($entities, ['id' => 'item-entity-select', 'prompt' => '--']) ?>
                    </div>
                    <div class="col-xs-6">
                        <?php foreach ($entityIds as $entity => $ids) {?>
                            <div class="item-entity <?= $model->entity === $entity ? 'active' : ''?>" data-entity="<?= $entity?>">
                                <?= $form->field($model, 'entity_id')->dropDownList($ids, ['disabled' => $model->entity !== $entity]) ?>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="box box-danger">
                <div class="box-body">
                    <div class="col-xs-12">
                        <?= Tabs::widget([
                            'id' => 'tabs-item-' . $model->id,
                            'items' =>
                                array_map(function ($locale, $language) use ($form, $model) {
                                    return [
                                        'label' => $language,
                                        'content' => $this->renderAjax('_translation', [
                                            'model' => $model->translations[$locale] ?? new WizardTranslation(),
                                            'form' => $form,
                                            'locale' => $locale,
                                            'group' => 'WizardItem',
                                            'index' => ''
                                        ]),
                                        'active' => $locale === 'ru-RU'
                                    ];
                                }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="box box-danger wizard-options">
                <div class="box-header with-border">
                    <h3 class="box-title">Опции</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?= Html::a('<i class="fa fa-plus"></i> Добавить Опцию', ['/wizard/admin/option'], [
                        'class' => 'add-element',
                        'data-type' => 'option',
                    ])?>
                    <div class="elements-container" data-index="<?= count($model->wizardOptions) ? max(array_column($model->wizardOptions, 'id')) : 0?>">
                        <?php foreach ($model->wizardOptions as $option) {
                            echo $this->renderAjax('list-items/option', ['model' => $option, 'form' => $form]);
                        }?>
                    </div>
                </div>
            </div>
            <div class="box box-danger wizard-options">
                <div class="box-header with-border">
                    <h3 class="box-title">Экстра</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?= Html::a('<i class="fa fa-plus"></i> Добавить Экстра', ['/wizard/admin/extra'], [
                        'class' => 'add-element',
                        'data-type' => 'extra',
                    ])?>
                    <div class="elements-container" data-index="<?= count($model->wizardExtras) ? max(array_column($model->wizardExtras, 'id')) : 0?>">
                        <?php foreach ($model->wizardExtras as $extra) {
                            echo $this->renderAjax('list-items/extra', ['model' => $extra, 'form' => $form]);
                        }?>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12">
                        <?= Html::submitButton('Сохранить', ['class' => 'pull-right btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
    ActiveForm::end();
Pjax::end();
?>

