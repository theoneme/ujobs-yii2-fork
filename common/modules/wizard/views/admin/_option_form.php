<?php

use common\helpers\FileInputHelper;
use common\modules\wizard\models\WizardOption;
use common\modules\wizard\models\WizardTranslation;
use kartik\file\FileInput;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model WizardOption */
/* @var $form ActiveForm */
/* @var $createForm bool */
/* @var $index integer */

$widgetId = strtr($index, ['][' => '-', '[' => '', ']' => '']);
?>
<?php if ($createForm) {
    $form = new ActiveForm(['id' => 'tree-form']); ob_end_clean ();
}?>
<div class="element-form">
    <div class="file-input-container">
        <?= $form->field($model, "{$index}id")->hiddenInput()->label(false) ?>
        <?= FileInput::widget(ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
            'id' => 'file-upload-input-option-' . $widgetId,
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'overwriteInitial' => true,
                'initialPreview' => !empty($model->image) ? $model->getThumb() : [],
                'initialPreviewConfig' => !empty($model->image) ? [[
                    'caption' => basename($model->image),
                    'url' => Url::toRoute('/image/delete'),
                    'key' => 'extra_image_init_' . $model->id
                ]] : [],
            ]
        ])) ?>
        <div class="images-container" data-model="WizardOption" data-index="<?= $index?>">
            <?= $form->field($model, "{$index}image")->hiddenInput([
                    'value' => $model->image,
                    'data-key' => 'option_image_init_' . $model->id,
                    'class' => 'image-source'
                ])->label(false);
            ?>
        </div>
        <?= Tabs::widget([
            'id' => 'tabs-option-' . $widgetId,
            'items' =>
                array_map(function ($locale, $language) use ($form, $model, $index) {
                    return [
                        'label' => $language,
                        'content' => $this->renderAjax('_translation', [
                            'model' => $model->translations[$locale] ?? new WizardTranslation(),
                            'form' => $form,
                            'locale' => $locale,
                            'group' => "WizardOption",
                            'index' => "{$index}"
                        ]),
                        'active' => $locale === 'ru-RU'
                    ];
                }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
        ]);
        ?>
        <?= $form->field($model, "{$index}price")->textInput(['class' => 'price-source form-control']) ?>

        <div class="form-buttons clearfix form-group">
            <?= Html::button('Сохранить', ['class' => 'pull-right btn btn-success submit-button']) ?>
            <?= Html::button('Отменить', ['class' => 'pull-right btn btn-warning cancel-button']) ?>
        </div>
    </div>
</div>
<div class="element-children" <?= $createForm ? 'style="display: none;"' : '';?>>
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Категории деталей</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить Категорию деталей', ['/wizard/admin/detail-category'], [
                'class' => 'add-element',
                'data-type' => 'detail-category',
            ])?>
            <div class="elements-container" data-index="<?= count($model->wizardDetailCategories) ? max(array_column($model->wizardDetailCategories, 'id')) : 0?>">
                <?php foreach ($model->wizardDetailCategories as $detailCategory) {
                    echo $this->renderAjax('list-items/detail_category', ['model' => $detailCategory, 'form' => $form]);
                }?>
            </div>
        </div>
    </div>
</div>
<?php
if ($createForm) {
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    let attributes = $attributes;
    $.each(attributes, function() {
        $("#tree-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);

//    $form->registerClientScript();
}?>