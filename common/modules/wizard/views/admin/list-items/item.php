<?php
use common\modules\wizard\models\WizardItem;
use yii\helpers\Url;

/* @var $model WizardItem */
$editUrl = Url::to(['/wizard/admin/item', 'id' => $model->id]);
$childUrl = Url::to(['/wizard/admin/item', 'parent_id' => $model->id]);
$deleteUrl = Url::to(['/wizard/admin/delete-item', 'id' => $model->id]);
?>
<li id="item-<?= $model->id ?>" class="<?= !$model->isLeaf() ? 'kv-parent kv-collapsed' : '' ?>"
    data-url="<?= $editUrl ?>" data-child-url="<?= $childUrl ?>" data-delete-url="<?= $deleteUrl ?>">
    <div class="kv-tree-list" tabindex="-1">
        <div class="kv-node-indicators">
            <span class="text-muted kv-node-toggle">
                <span class="kv-node-expand">
                    <span class="fa fa-plus-square-o"></span>
                </span>
                <span class="kv-node-collapse">
                    <span class="fa fa-minus-square-o"></span>
                </span>
            </span>
        </div>
        <div class="kv-node-detail" tabindex="-1">
            <span class="text-warning kv-node-icon kv-icon-parent">
                <span class="fa fa-folder kv-node-closed"></span>
                <span class="fa fa-folder-open kv-node-opened"></span>
            </span>
            <span class="text-info kv-node-icon kv-icon-child">
                <span class="fa fa-<?= $model->isRoot() ? 'tree' : 'file' ?>"></span>
            </span>
            <span class="kv-node-label">
                <?= $model->name ?>
            </span>
        </div>
    </div>
    <ul>
        <?php
        foreach ($model->children(1)->all() as $child) {
            echo $this->render('/admin/list-items/item', ['model' => $child]);
        } ?>
    </ul>
</li>