<?php

use common\modules\wizard\models\WizardOption;
use yii\helpers\Html;

/* @var $model WizardOption */
?>
<div class="element-item" data-id="<?= $model->id?>">
    <div class="element-preview">
        <?= Html::img($model->getThumb(), ['class' => 'element-image'])?>
        <div class="element-name">
            <?= $model->translation->title?>
        </div>
        <div class="element-actions pull-right">
            <?= Html::a('<i class="fa fa-pencil"></i>', null, ['class' => 'edit-element'])?>
            <?= Html::a('<i class="fa fa-times"></i>', null, ['class' => 'remove-element'])?>
        </div>
        <div class="element-price pull-right">
            <?= $model->getPrice()?>
        </div>
    </div>
    <?= $this->renderAjax('/admin/_option_form', ['model' => $model, 'form' => $form, 'index' => "[{$model->id}]", 'createForm' => false]);?>
</div>