<?php

use common\modules\wizard\models\WizardDetailCategory;
use yii\helpers\Html;

/* @var $model WizardDetailCategory */
?>
<div class="element-item" data-id="<?= $model->id?>">
    <div class="element-preview category-preview">
        <div class="element-name">
            <?= $model->translation->title?>
        </div>
        <div class="element-actions pull-right">
            <?= Html::a('<i class="fa fa-pencil"></i>', null, ['class' => 'edit-element'])?>
            <?= Html::a('<i class="fa fa-times"></i>', null, ['class' => 'remove-element'])?>
        </div>
    </div>
    <?= $this->renderAjax('/admin/_detail_category_form', [
        'model' => $model,
        'form' => $form,
        'index' => "[{$model->wizard_item_option_id}][{$model->id}]",
        'createForm' => false
    ]);?>
</div>