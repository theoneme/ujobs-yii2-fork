<?php

use vova07\imperavi\Widget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContentTranslation */
/* @var $locale string */
/* @var $form ActiveForm */
/* @var $group $string */
/* @var $index $string */
?>
<div class="asdf">
    <?= $form->field($model, "[{$group}]{$index}[{$locale}]id")->hiddenInput()->label(false); ?>
    <?= $form->field($model, "[{$group}]{$index}[{$locale}]title")->textInput(['class' => 'form-control ' . ($locale === 'ru-RU' ? 'title-source' : '')]) ?>
    <?= $group === 'WizardItem' ? $form->field($model, "[{$group}]{$index}[{$locale}]subtitle")->textInput(['class' => 'form-control ' . ($locale === 'ru-RU' ? 'title-source' : '')]) : '' ?>
    <?= $group === 'WizardItem' ? $form->field($model, "[{$group}]{$index}[{$locale}]content")->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'fontcolor'
            ],
            'buttonsHide' => ['link', 'formatting']
        ]
    ]) : '';
    ?>
    <?= $form->field($model, "[{$group}]{$index}[{$locale}]locale")->hiddenInput(['value' => $locale])->label(false); ?>
</div>