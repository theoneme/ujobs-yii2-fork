<?php

use common\modules\wizard\models\WizardDetailCategory;
use common\modules\wizard\models\WizardTranslation;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model WizardDetailCategory */
/* @var $form ActiveForm */
/* @var $createForm bool */
/* @var $index integer */

$widgetId = strtr($index, ['][' => '-', '[' => '', ']' => '']);
?>
<?php if ($createForm) {
    $form = new ActiveForm(['id' => 'tree-form']); ob_end_clean ();
}?>
<div class="element-form">
    <div class="file-input-container">
        <?= $form->field($model, "{$index}id")->hiddenInput()->label(false) ?>
        <?= Tabs::widget([
            'id' => 'tabs-detail-category-' . $widgetId,
            'items' =>
                array_map(function ($locale, $language) use ($form, $model, $index) {
                    return [
                        'label' => $language,
                        'content' => $this->renderAjax('_translation', [
                            'model' => $model->translations[$locale] ?? new WizardTranslation(),
                            'form' => $form,
                            'locale' => $locale,
                            'group' => "WizardDetailCategory",
                            'index' => "{$index}"
                        ]),
                        'active' => $locale === 'ru-RU'
                    ];
                }, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages'])
        ]);
        ?>
        <div class="form-buttons clearfix form-group">
            <?= Html::button('Сохранить', ['class' => 'pull-right btn btn-success submit-button']) ?>
            <?= Html::button('Отменить', ['class' => 'pull-right btn btn-warning cancel-button']) ?>
        </div>
    </div>
</div>
<div class="element-children" <?= $createForm ? 'style="display: none;"' : '';?>>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Детали</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?= Html::a('<i class="fa fa-plus"></i> Добавить Деталь', ['/wizard/admin/detail'], [
                'class' => 'add-element',
                'data-type' => 'detail',
            ])?>
            <div class="elements-container" data-index="<?= count($model->wizardDetails) ? max(array_column($model->wizardDetails, 'id')) : 0?>">
                <?php foreach ($model->wizardDetails as $detail) {
                    echo $this->renderAjax('list-items/detail', ['model' => $detail, 'form' => $form]);
                }?>
            </div>
        </div>
    </div>
</div>
<?php
if ($createForm) {
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    let attributes = $attributes;
    $.each(attributes, function() {
        $("#tree-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);

//    $form->registerClientScript();
}?>