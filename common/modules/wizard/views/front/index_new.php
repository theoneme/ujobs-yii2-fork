<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2017
 * Time: 17:20
 */

use common\modules\wizard\assets\WizardAsset;
use common\modules\wizard\models\frontend\ApartmentForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

WizardAsset::register($this);

/** @var ApartmentForm $model */

?>
<?php Pjax::begin(['id' => 'wizard-pjax']); ?>
    <div class="container-fluid" style="margin-top: 10px;">
        <div class="block-repair-menu">
            <div class="repair-menu-mobile-title text-center">
                <div id="repair-current-step">
                    <?= $model->getCurrentStep() ?>
                </div>
                <div class="close-repair-menu">&times;</div>
            </div>
            <ul class="repair-menu">
                <?php foreach ($model->steps_menu as $key => $step) { ?>
                    <li class="<?= $model->step === $key ? 'active' : '' ?>">
                        <?= Html::a($step['label'], "#step{$key}", [
                            'data-toggle' => 'tab',
                            'data-step' => $key,
                            'data-role' => 'step-menu-item',
                            'class' => in_array($key, $model->completed_steps) ? 'completed' : ''
                        ]) ?>
                        <?php if (!empty($step['children'])) { ?>
                            <ul <?= $step['active'] === true || $key === $model->step ? 'style="display: block"' : '' ?>>
                                <?php foreach ($step['children'] as $childKey => $child) { ?>
                                    <li class="<?= $model->step === $childKey ? 'active' : '' ?>">
                                        <?= Html::a($child['label'], "#step{$childKey}", [
                                            'data-toggle' => 'tab',
                                            'data-step' => $childKey,
                                            'data-role' => 'step-menu-item',
                                            'class' => in_array($childKey, $model->completed_steps) ? 'completed' : ''
                                        ]) ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php $form = ActiveForm::begin([
            'options' => [
                'data-step' => $model->step,
                'data-final-step' => $model->getLastStep(),
                'data-pjax' => '',
                'enctype' => 'application/x-www-form-urlencoded'
            ],
            'id' => 'wizard-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            //            'validateOnChange' => false,
            //            'validateOnBlur' => false,
        ]); ?>
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
        <?php foreach ($model->completed_steps as $key => $isCompleted) { ?>
            <?= $form->field($model, "completed_steps[{$key}]")->hiddenInput(['value' => $isCompleted])->label(false) ?>
        <?php } ?>
        <div class="tab-content tab-repair-steps">
            <?php foreach ($model->steps_content as $key => $step) { ?>
                <div class="repair-content <?= $step['type'] === 'step-options-details-advanced' ? 'with-option' : '' ?> tab-pane fade in <?= $model->step === $key ? 'active' : '' ?>"
                     data-step="<?= $key ?>" id="step<?= $key ?>">
                    <?= $this->render("steps/{$step['type']}", [
                        'model' => $model,
                        'form' => $form,
                        'data' => $step['data'],
                        'step' => $key,
                        'label' => $step['label'],
                        'subheading' => $step['subheading']
                    ]); ?>
                </div>
            <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
<?php $steps = json_encode(array_keys($model->steps_content));
$script = <<<JS
    let menuList = $('.repair-menu'),
        stepInput = $('#apartmentform-step'),
        windowWidth = $(window).width(),
        viewer = new Viewer(),
        calculator = new NewCalculator();

    menuList.on("click", "li a", function(){
        let step = $(this).data('step');
        stepInput.val(step);
    });
    menuList.on("click", "li", function(){        
        menuList.find('li ul').hide();
        $(this).find('ul').show();
        if (windowWidth <= 768) {
            $(".block-repair-menu").removeClass("open");
        }
    });
    
    let optionList = $('input[data-nav="1"]'); 
    let priceOptionsList = $('input[data-price-trigger=1]');
    let imageOptionList = $('input[data-slider="1"]');
    let advancedOptionList = $('a[data-advanced-option=1]');
    let detailLinks = $('.pack-detail');
    let detailWindows = $('.detail');
    optionList.on('change', function() {        
        let packDetails = $(this).closest('.repair-pack-box').find('.pack-detail');
        let valueId = $(this).val();
        packDetails.addClass('hidden');
        packDetails.find('span').hide();
        detailWindows.hide();
        $(this).closest('.repair-pack-item').find('.pack-detail').removeClass('hidden');
        $(this).closest('.repair-packs').find('.detail:not(#details-' + valueId + ')').find('input').prop('disabled', true);
        $('#details-' + valueId).find('input').prop('disabled', false);
        
        $('.step-content:visible a[data-advanced-option=1]').removeClass('active');
        $('a[data-option-id=option_' + valueId).addClass('active');
    });
    
    priceOptionsList.on('change', function() {
        calculator.recalculate($('#wizard-form').serialize());
    });
    
    advancedOptionList.on('click', function() {
        let targetOption = $(this).data('option-id');
        $('.step-content:visible a[data-advanced-option=1]').removeClass('active');
        $(this).addClass('active');
        $('#' + targetOption).click();
    });
    
    $.each(optionList, function(i, val) {
        if($(this).is(':checked')) {
            let valueId = $(this).val();
            $('#details-' + valueId).find('input').prop('disabled', false);
        }
    });
   
    imageOptionList.on('change', function() { 
        let id = $(this).data('id'),
            layerObject = mediaLayer[id];
        
        viewer.reset('.step-content:visible .repair-slider');
        let html = viewer.buildSliderHtml(layerObject, 'slick-big-' + id, 'slick-small-' + id);
        $('.step-content:visible .repair-slider').html(html);
        
        viewer.initBigSlider('#slick-big-' + id, '#slick-small-' + id);
        viewer.initMobileSlider('#slick-small-' + id, '#slick-big-' + id);
    });
    
    detailLinks.on('click', function() {
        let detainWindowId = $(this).data('id'),
            detailWindow = $("#details-" + detainWindowId);
        
        if(windowWidth > 768) {
            $(".pack-detail span").hide();
            $(".detail").slideUp();
            if(detailWindow.is(':hidden')) {
                detailWindow.slideToggle();
                $(this).find("span").show();
            }
        } else {
            detailWindow.addClass('open');
            $('body').css('overflow','hidden');
        }
        
        return false;
    });
    
    let detailCloseLinks = $('.detail-close');
    detailCloseLinks.on('click', function() {
        if(windowWidth > 768) {
            $(this).closest(".detail").slideUp();
            $(".pack-detail span").hide();
        } else {
            $(this).closest(".detail").removeClass("open");
            $('body').css('overflow','auto');
        }
    });
    
    menuList.on('shown.bs.tab', 'li a', function(event) {        
        let id = $('.step-content:visible').find('input[data-nav=1]:checked').data('id'),
            layerObject = mediaLayer[id];
        
        viewer.reset('.step-content:visible .repair-slider');
        let html = viewer.buildSliderHtml(layerObject, 'slick-big-' + id, 'slick-small-' + id);
        $('.step-content:visible .repair-slider').html(html);
        
        viewer.initBigSlider('#slick-big-' + id, '#slick-small-' + id);
        viewer.initMobileSlider('#slick-small-' + id, '#slick-big-' + id);
    });
    
    $('#wizard-pjax').on('pjax:success', function(event, data, status, xhr, options) {
        let id = $('.step-content:visible').find('input[data-nav=1]:checked').data('id'),
            layerObject = mediaLayer[id];
        
        let html = viewer.buildSliderHtml(layerObject, 'slick-big-' + id, 'slick-small-' + id);
        $('.step-content:visible .repair-slider').html(html);
        
        viewer.initBigSlider('#slick-big-' + id, '#slick-small-' + id);
        viewer.initMobileSlider('#slick-small-' + id, '#slick-big-' + id);
    });
   
    if ($(window).width() <= 768) {
        $(".block-repair-menu li ul li").removeClass("active");
    }
    
     $(".repair-menu li").on("click", function(e) {
         $(".repair-menu li ul").hide();
         $(this).find("ul").show();
         if ($(window).width() <= 768) {
             $(".block-repair-menu").removeClass("open");
         }
     });
    $("body").on("click", ".repair-menu-mobile-title", function(e) {
         if (!$(this).closest(".block-repair-menu").hasClass("open")) {
             $(this).closest(".block-repair-menu").addClass("open");
         }
    });
     $("body").on("click", ".close-repair-menu", function() {
         setTimeout(function() {
             $(".block-repair-menu").removeClass("open");
         }, 100);
     });
     $("body").on("click", ".repair-sum-arrow", function() {
         $(this).closest(".repair-sum-block").find(".repair-sum-over-block").toggle();
         $(this).find(".fa").toggleClass("fa-chevron-down fa-chevron-up");
     });
    // function slickOption() {
    //     $(".repair-option-box").slick("unslick");
    //     $(".repair-option-box").slick({
    //         slidesToShow: 6,
    //         slidesToScroll: 1,
    //         autoplay: false,
    //         infinite: false,
    //         arrows: true,
    //         dots: false,
    //         responsive: [{
    //             breakpoint: 600,
    //             settings: {
    //                 slidesToShow: 5,
    //             }
    //         }, {
    //             breakpoint: 480,
    //             settings: {
    //                 slidesToShow: 3,
    //             }
    //         }, {
    //             breakpoint: 320,
    //             settings: {
    //                 slidesToShow: 2,
    //             }
    //         }, ]
    //     });
    // }
    // //говно-скрипты 
    // slickinit();
    // if ($(window).width() <= 768) {
    //     $(".block-repair-menu li ul li").removeClass("active");
    //     $(".repair-option-box").slick({
    //         slidesToShow: 2,
    //         slidesToScroll: 1,
    //         autoplay: false,
    //         infinite: false,
    //         arrows: true,
    //         dots: false,
    //     });
    // }
    //
    // $(".repair-menu li",).on("click", function(e) {
    //     $(".repair-menu li ul").hide();
    //     $(this).find("ul").show();
    //     if ($(window).width() <= 768) {
    //         $(".block-repair-menu").removeClass("open");
    //     }
    // });
    //
    // $("body").on("click", ".repair-menu ul li", function() {
    //     $(this).closest("ul").find("li").removeClass("active");
    //     $(this).addClass("active");
    //     var dId = $(this).attr("data-id");
    //     $("#" + dId).closest(".repair-content").find(".repair-content-option").removeClass("open");
    //     $("#" + dId).addClass("open");
    //     if ($(window).width() <= 768) {
    //         $(this).closest("ul").closest("li").children("a").tab("show");
    //         setTimeout(slickOption, 200);
    //     }
    // });
    // $("body").on("click", ".repair-menu-mobile-title", function(e) {
    //     if (!$(this).closest(".block-repair-menu").hasClass("open")) {
    //         $(this).closest(".block-repair-menu").addClass("open");
    //     }
    // });
    // $("body").on("click", ".close-repair-menu", function() {
    //     setTimeout(function() {
    //         $(".block-repair-menu").removeClass("open");
    //     }, 100);
    // });
    // $("body").on("click", ".repair-sum-arrow", function() {
    //     $(this).closest(".repair-sum-block").find(".repair-sum-over-block").toggle();
    //     $(this).find(".fa").toggleClass("fa-chevron-down fa-chevron-up");
    // });
    // $("body").on("click", ".repair-change", function(e) {
    //     e.preventDefault();
    //     $(".repair-menu li ul li").removeClass("active");
    //     var dId = $(this).attr("data-id");
    //     var step = $(this).attr("data-step");
    //     $(".repair-menu li ul").find("[data-id=\'" + dId + "\']").addClass("active");
    //     $("#" + dId).closest(".repair-content").find(".repair-content-option").removeClass("open");
    //     $("#" + dId).addClass("open");
    //     $(".repair-menu").find("a[href=\'#" + step + "\']").closest("li").find("ul").show();
    //     $(".repair-menu").find("a[href=\'#" + step + "\']").tab("show");
    //     setTimeout(slickReinit, 200);
    // });
    // $("body").on("change", ".repair-option-box input[type=\'checkbox\']", function() {
    //     var tit = $(this).closest(".check-none").find(".roi-title").html();
    //     $(this).closest(".repair-content-option").find(".ofm-title").html(tit);
    //     var price = $(this).closest(".check-none").find(".roi-price").html();
    //     $(this).closest(".repair-content-option").find(".ofm-price").html(price);
    // });
JS;

$this->registerJs($script);
Pjax::end(); ?>