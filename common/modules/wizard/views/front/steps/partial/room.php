<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 17:50
 */

use common\modules\wizard\models\entity\Room;
use yii\helpers\Html;

/**
 * @var Room $model
 * @var integer $index
 */

?>

<div class="row room-row" id="room_<?= $index ?>">
    <div class="col-md-2 wizard-room-line">
        <?= Yii::t('wizard', 'Please, select room type') ?>
    </div>
    <div class="col-md-8">
        <?= Html::activeDropDownList($model, "[$index]type", $model::getRoomTypes(), ['class' => 'form-control']) ?>
    </div>
    <div class="col-md-2">
        <?= Html::a('<i class="fa fa-close"></i>', '#', ['data-id' => $index, 'class' => 'wizard-room-line remove-room']) ?>
    </div>
</div>
