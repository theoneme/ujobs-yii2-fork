<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 13:35
 */
use common\components\CurrencyHelper;
use common\modules\wizard\dto\WizardStepDTO;
use common\modules\wizard\models\frontend\ApartmentForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var WizardStepDTO $data
 * @var ApartmentForm $model
 * @var integer $step
 * @var string $label
 * @var string $subheading
 */

?>
    <div class="step-content">
        <div class="repair-head-info">
            <div class="title-repair">
                <?= $label ?>
            </div>
            <div class="cost-repair clearfix">
                <div class="cost-repair-item">
                    <div class="cost-repair-title">
                        <?= Yii::t('wizard', 'Base price') ?>
                    </div>
                    <div class="cost-repair-price">
                        <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->base_price_data['total']) ?>
                    </div>
                </div>
                <div class="cost-repair-item">
                    <div class="cost-repair-title">
                        <?= Yii::t('wizard', 'Options price') ?>
                    </div>
                    <div class="cost-repair-price">
                        <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->details_price_data['total']) ?>
                    </div>
                </div>
                <div class="cost-repair-item">
                    <div class="cost-repair-title">
                        <?= Yii::t('wizard', 'Summary') ?>
                    </div>
                    <div class="cost-repair-price">
                        <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->summary_price_data['total']) ?>
                    </div>
                </div>
                <div class="cost-repair-item repair-next-step">
                    <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'btn-big']) ?>
                </div>
            </div>
        </div>
        <div class="repair-slider">

        </div>
        <div class="repair-packs">
            <div class="repair-pack-box">
                <?php if($subheading !== null) { ?>
                    <p class="pack-title"><?= $subheading?></p>
                <?php } ?>
                <?php foreach ($data->options as $key => $option) { ?>
                    <div class="repair-pack-item">
                        <div class="repair-pack-img">
                            <?= Html::img($option['thumb']) ?>
                        </div>
                        <div class="chover check-pack-repair">
                            <?= Html::radio("ApartmentForm[options][{$option['tree_item']}]", $option['checked'], [
                                'class' => 'radio-checkbox',
                                'id' => "option_{$option['id']}",
                                'data-slider' => 1,
                                'data-nav' => 1,
                                'data-price-trigger' => 1,
                                'data-id' => $option['id'],
                                'value' => $option['id'],
                            ]) ?>
                            <label for="option_<?= $option['id'] ?>">
                                <div>
                                    <span class="repair-pack-title"><?= $option['title'] ?></span>
                                    <span class="repair-pack-price"><?= $option['price'] ?></span>
                                </div>
                            </label>
                        </div>
                        <a class="pack-detail text-right <?= !$option['checked'] ? 'hidden' : '' ?>" href="#"
                           data-id="<?= $option['id'] ?>">
                            <span><?= Yii::t('wizard', 'Hide') ?></span>
                            <?= Yii::t('wizard', 'Details') ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="repair-pack-details">
                <?php foreach ($data->options as $key => $option) { ?>
                    <div class="detail" id="details-<?= $option['id'] ?>">
                        <div class="detail-close">&times;</div>
                        <div class="detail-pack-title">
                            <?= $option['title'] ?>
                            <div class="detail-pack-price">
                                <?= $option['price'] ?>
                            </div>
                        </div>
                        <div class="repair-pack-bigimg">
                            <?= Html::img($option['image']) ?>
                        </div>
                        <?php foreach ($option['detail_categories'] as $detailCategory) { ?>
                            <div class="repair-pack-block">
                                <div class="repair-pack-block-title">
                                    <?= $detailCategory['title'] ?>
                                </div>
                                <div class="repair-pack-opts">
                                    <?php foreach ($detailCategory['details'] as $detail) { ?>
                                        <div class="chover">
                                            <?= Html::radio("ApartmentForm[details][{$detail['option_item']}][{$detail['detail_category_item']}]", $detail['checked'], [
                                                'class' => 'radio-checkbox',
                                                'id' => "detail_{$detail['id']}",
//                                            'data-slider' => 1,
                                                'data-price-trigger' => 1,
                                                'value' => $detail['id'],
                                                'disabled' => 'disabled'
                                            ]) ?>
                                            <label for="detail_<?= $detail['id'] ?>">
                                                <span class="repair-opt-img">
                                                    <?= Html::img($detail['image']) ?>
                                                </span>
                                                <span class="repair-opt-title"><?= $detail['title'] ?>
                                                    <span class="opt-price-mob">
                                                        <?= $detail['price'] ?>
                                                    </span>
                                                </span>
                                                <span class="repair-opt-price text-right">
                                                    <?= $detail['price'] ?>
                                                </span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php if (!empty($data->extras)) { ?>
            <div class="repair-extras">
                <?php foreach ($data->extras as $key => $extra) { ?>
                    <div class="repair-pack-item">
                        <div class="repair-pack-img">
                            <?= Html::img($extra['thumb']) ?>
                        </div>
                        <div class="chover check-pack-repair">
                            <?= Html::checkbox("ApartmentForm[extras][{$extra['tree_item']}][]", $extra['checked'], [
                                'class' => 'radio-checkbox',
                                'id' => "extra_{$extra['id']}",
                                'data-id' => $extra['id'],
                                'value' => $extra['id'],
                                'data-price-trigger' => 1,
                            ]) ?>
                            <label for="extra_<?= $extra['id'] ?>">
                                <div>
                                    <span class="repair-pack-title"><?= $extra['title'] ?></span>
                                    <span class="repair-pack-price"><?= $extra['price'] ?></span>
                                </div>
                            </label>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="repair-packs">
            <div class="text-right">
                <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'btn-big']) ?>
            </div>
        </div>
        <div class="repair-content-option open">
            <div class="content-option-title"><?= $label ?></div>
            <div class="repair-option-box">
                <?php foreach ($data->options as $key => $option) { ?>
                    <div class="chover check-none">
                        <a href="#" data-option-id="option_<?= $option['id'] ?>" data-advanced-option="1" <?= $option['checked'] ? 'class="active"' : '' ?>>
                            <div class="roi-img">
                                <?= Html::img($option['thumb']) ?>
                            </div>
                            <div class="roi-title"><?= $option['title'] ?></div>
                            <div class="roi-price"><?= $option['price'] ?></div>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="opt-for-mobile">
                <div class="ofm-title"></div>
                <div class="ofm-price text-right"></div>
            </div>
        </div>
    </div>

<?php $images = ArrayHelper::map($data->options, 'id', function ($option) {
    return ['images' => $option['images'], 'thumbs' => $option['thumbs']];
});
$jsonImages = json_encode($images);

$script = <<<JS
    $.each($jsonImages, function(i, val) {
        if(mediaLayer[i] !== 'undefined') {
            mediaLayer[i] = val;
        }
    });
JS;
$this->registerJs($script);
