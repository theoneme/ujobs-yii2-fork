<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 12:48
 */

use common\modules\wizard\models\frontend\ApartmentForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var ApartmentForm $model
 * @var ActiveForm $form
 */

\frontend\assets\SelectizeAsset::register($this);

?>

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Yii::t('wizard', 'Apartment sizes') ?></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'apartment_area')->textInput(['type' => 'number', 'step' => 'any']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'wall_height')->textInput(['type' => 'number', 'step' => 'any']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><?= Yii::t('wizard', 'Rooms') ?></div>
            <div class="panel-body">
                <?= $form->field($model, 'rooms')->hiddenInput()->label(false); ?>
                <div id="rooms">
                    <?php if (!empty($model->getRooms())) {
                        foreach ($model->getRooms() as $key => $room) {
                            echo $this->render('partial/room', [
                                'model' => $room,
                                'index' => $key,
                            ]);
                        }
                    } ?>
                </div>
                <div class="text-right">
                    <?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('wizard', 'Add Room'), '#', [
                        'id' => 'add-room', 'class' => 'add-room'
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><?= Yii::t('wizard', 'City') ?></div>
            <div class="panel-body">
                <div class="selectize-input-container">
                    <!--                    --><?//= $form->field($model, 'city_id')->dropDownList($model->city_id ? [$model->city_id => $model->getCity()->translation->title] : [], ['class' => 'noscript-sel']) ?>
                    <?= Html::activeHiddenInput($model, 'city_id') ?>
                    <?= Html::activeHiddenInput($model, '[locationData]country', ['id' => 'wizard-location-country', 'value' => false]) ?>
                    <?= Html::activeHiddenInput($model, '[locationData]city', ['id' => 'wizard-location-city', 'value' => false]) ?>
                    <?= Html::textInput('location_input', $model->city !== null ? $model->city->translation->title : null, ['id' => 'wizard-location-input']); ?>
                </div>
            </div>
        </div>

        <div class="step-submit text-right">
            <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'btn-big']) ?>
        </div>
    </div>

<?php
$renderRoomRoute = Url::to(['/wizard/front/render-room']);
$rooms = count($model->getRooms());
$attrListUrl = Url::to(['/ajax/attribute-list', 'alias' => 'city']);
$apiKey = Yii::$app->params['googleAPIKey'];
$language = Yii::$app->params['supportedLocales'][Yii::$app->language];
if ($language === 'ua') {
    $language = 'uk';
}
$script = <<<JS
    let rooms = $rooms + 1;
    $('#add-room').on('click', function() {
         $.get('$renderRoomRoute', {index: rooms}, function(result) {
            if(result.success === true) {
                $('#rooms').append(result.html);
                rooms++;
            }             
         });
    });
    
    $('.remove-room').on('click', function() {
        let id = $(this).data('id');
        
        $('#room_' + id).remove();
        // rooms--;
    });
   
   var googleAutocomplete;
    if (typeof (googleAutocomplete) === 'undefined') {
        $.getScript('https://maps.googleapis.com/maps/api/js?key={$apiKey}&libraries=places&language={$language}', function() {
            initAutocomplete();
        });
    }
    else {
        initAutocomplete();
    }
    
    function initAutocomplete() {
        googleAutocomplete = new google.maps.places.Autocomplete(
            $("#wizard-location-input").get(0),
            {types: ['locality']}
        );
        googleAutocomplete.addListener('place_changed', function() {
            // Get the place details from the autocomplete object.
            let place = googleAutocomplete.getPlace();
            let country = '';
            let city = '';
            if (typeof place.address_components !== "undefined") {
                for (let i = 0; i < place.address_components.length; i++) {
                    let addressType = place.address_components[i].types[0];
                    switch (addressType) {
                        case 'locality': 
                            city = place.address_components[i].long_name;
                            break;
                        case 'country': 
                            country = place.address_components[i].long_name;
                            break;
                    }
                }
            }
            $('#wizard-location-city').val(city);
            $('#wizard-location-country').val(country);
        });
    }
JS;

$this->registerJs($script);

