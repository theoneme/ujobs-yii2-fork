<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 13:42
 */
use common\components\CurrencyHelper;
use common\modules\wizard\dto\WizardStepDTO;
use common\modules\wizard\models\frontend\ApartmentForm;
use yii\helpers\Html;

/**
 * @var WizardStepDTO $data
 * @var ApartmentForm $model
 */

?>

<div class="repair-head-info">
    <div class="title-repair">
        <?= $model->getCurrentStep() ?>
    </div>
    <div class="cost-repair clearfix">
        <div class="cost-repair-item">
            <div class="cost-repair-title">
                <?= Yii::t('wizard', 'Base price') ?>
            </div>
            <div class="cost-repair-price">
                <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->base_price_data['total']) ?>
            </div>
        </div>
        <div class="cost-repair-item">
            <div class="cost-repair-title">
                <?= Yii::t('wizard', 'Options price') ?>
            </div>
            <div class="cost-repair-price">
                <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->details_price_data['total']) ?>
            </div>
        </div>
        <div class="cost-repair-item">
            <div class="cost-repair-title">
                <?= Yii::t('wizard', 'Summary') ?>
            </div>
            <div class="cost-repair-price">
                <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->summary_price_data['total']) ?>
            </div>
        </div>
    </div>
</div>
<!--<div class="repair-slider">-->
<!--    <div class="repair-big-slider">-->
<!--        <div><img src="/images/new/dom1.jpg" alt="photo"></div>-->
<!--        <div><img src="/images/new/dom2.jpg" alt="photo"></div>-->
<!--        <div><img src="/images/new/dom3.jpg" alt="photo"></div>-->
<!--    </div>-->
<!--    <div class="clearfix">-->
<!--        <div class="repair-small-slider">-->
<!--            <div class="repair-slide"><img src="/images/new/dom1.jpg" alt="photo"></div>-->
<!--            <div class="repair-slide"><img src="/images/new/dom2.jpg" alt="photo"></div>-->
<!--            <div class="repair-slide"><img src="/images/new/dom3.jpg" alt="photo"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="repair-sum-big">
    <div class="repair-sum-block">
        <div class="repair-sum-title">
            <?= Yii::t('wizard', 'Apartment Details') ?>
            <div class="repair-sum-arrow text-center">
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
            </div>
        </div>
        <div class="repair-sum-over-block">
            <div class="repair-sum-table-over">
                <div class="rst-price-text"><?= Yii::t('wizard', 'Value') ?></div>
                <div class="repair-sum-table">
                    <div class="rst-row">
                        <div class="rst-title">
                            <?= $model->getAttributeLabel('apartment_area') ?>
                        </div>
                        <div class="rst-price"><?= $model->apartment_area ?></div>
                    </div>
                    <div class="rst-row">
                        <div class="rst-title">
                            <?= $model->getAttributeLabel('wall_height') ?>
                        </div>
                        <div class="rst-price"><?= $model->wall_height ?></div>
                    </div>
                    <?php foreach ($model->getRooms() as $room) { ?>
                        <div class="rst-row">
                            <div class="rst-title">
                                <?= Yii::t('wizard', 'Room: {room}', ['room' => $room->getRoomType()]) ?>
                            </div>
                            <div class="rst-price">-</div>
                        </div>
                    <?php } ?>
                    <div class="rst-row">
                        <div class="rst-title">
                            <?= $model->getAttributeLabel('city_id') ?>
                        </div>
                        <div class="rst-price"><?= $model->getCity()->translation->title ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="repair-sum-block">
        <div class="repair-sum-title">
            <?= Yii::t('wizard', 'Style') ?>
            <div class="repair-sum-arrow text-center">
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
            </div>
        </div>
        <div class="repair-sum-over-block">
            <div class="repair-sum-table-over">
                <div class="rst-price-text"><?= Yii::t('wizard', 'Price') ?></div>
                <div class="repair-sum-table">
                    <div class="rst-row">
                        <div class="rst-title">
                            <?= $model->getOptions()['tree']->translation->title ?>
                        </div>
                        <div class="rst-price">
                            <?= Yii::t('wizard', '{price} for {measure}', [
                                'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->getOptions()['tree']->price),
                                'measure' => 'm2'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (is_array($model->summary_price_data['data'])) { ?>
        <?php foreach ($model->summary_price_data['data'] as $key => $item) { ?>
            <div class="repair-sum-block">
                <div class="repair-sum-title">
                    <?= $item['title'] ?>
                    <div class="repair-sum-arrow text-center">
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="repair-sum-over-block">
                    <div class="repair-sum-table-over">
                        <div class="rst-price-text"><?= Yii::t('wizard', 'Price') ?></div>
                        <!--                        --><?php // foreach($item['option'] as $option) { ?>
                        <div class="repair-sum-table">
                            <div class="rst-row">
                                <div class="rst-img">
                                    <div>
                                        <?= Html::img($item['option']['image']) ?>
                                    </div>
                                </div>
                                <div class="rst-title">
                                    <?= $item['option']['title'] ?>
                                </div>
                                <div class="rst-price">
                                    <?= $item['option']['price'] ?>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($item['option']['details'] as $detail) { ?>
                            <div class="repair-sum-table">
                                <div class="rst-row">
                                    <div class="rst-img">
                                        <div>
                                            <?= Html::img($detail['image']) ?>
                                        </div>
                                    </div>
                                    <div class="rst-title">
                                        <?= $detail['title'] ?>
                                    </div>
                                    <div class="rst-price">
                                        <?= $detail['price'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (is_array($item['extras'])) { ?>
                            <?php foreach ($item['extras'] as $extra) { ?>
                                <div class="repair-sum-table">
                                    <div class="rst-row">
                                        <div class="rst-img">
                                            <div>
                                                <?= Html::img($extra['image']) ?>
                                            </div>
                                        </div>
                                        <div class="rst-title">
                                            <?= $extra['title'] ?>
                                        </div>
                                        <div class="rst-price">
                                            <?= $extra['price'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<div class="text-right repair-btn">
    <a class="btn-big" href=""><?= Yii::t('wizard', 'Checkout') ?></a>
</div>
<div class="repair-sum-small">
    <div class="rss-row rss-grey">
        <div class="rss-bigtitle"><?= Yii::t('wizard', 'Summary') ?></div>
        <div class="rss-smalltitle"><?= Yii::t('wizard', 'Price') ?></div>
    </div>
    <div class="rss-row rss-inner">
        <div><?= Yii::t('wizard', 'Base price') ?></div>
        <div><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->base_price_data['total']) ?></div>
    </div>
    <div class="rss-row rss-grey">
        <div><?= Yii::t('wizard', 'Options price') ?></div>
        <div><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->details_price_data['total']) ?></div>
    </div>
    <div class="rss-row">
        <div><?= Yii::t('wizard', 'Summary') ?></div>
        <div><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->summary_price_data['total']) ?></div>
    </div>
</div>
