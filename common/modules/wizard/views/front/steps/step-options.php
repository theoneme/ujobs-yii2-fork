<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.11.2017
 * Time: 13:35
 */

use common\components\CurrencyHelper;
use common\modules\wizard\dto\WizardStepDTO;
use common\modules\wizard\models\frontend\ApartmentForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var WizardStepDTO $data
 * @var ApartmentForm $model
 * @var string $label
 * @var string $subheading
 */

?>
<div class="step-content">
    <div class="repair-head-info">
        <div class="title-repair">
            <?= $label ?>
        </div>
        <div class="cost-repair clearfix">
            <div class="cost-repair-item">
                <div class="cost-repair-title">
                    <?= Yii::t('wizard', 'Base price') ?>
                </div>
                <div class="cost-repair-price">
                    <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->base_price_data['total']) ?>
                </div>
            </div>
            <div class="cost-repair-item">
                <div class="cost-repair-title">
                    <?= Yii::t('wizard', 'Options price') ?>
                </div>
                <div class="cost-repair-price">
                    <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->details_price_data['total']) ?>
                </div>
            </div>
            <div class="cost-repair-item">
                <div class="cost-repair-title">
                    <?= Yii::t('wizard', 'Summary') ?>
                </div>
                <div class="cost-repair-price">
                    <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->summary_price_data['total']) ?>
                </div>
            </div>
            <div class="cost-repair-item repair-next-step">
                <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'btn-big']) ?>
            </div>
        </div>
    </div>
    <div class="repair-slider">

    </div>

    <div class="repair-packs">
        <?php if($subheading !== null) { ?>
            <p class="pack-title"><?= $subheading?></p>
        <?php } ?>
        <div class="repair-pack-box">
            <?php foreach ($data->options as $key => $option) { ?>
                <div class="repair-pack-item">
                    <div class="repair-pack-img">
                        <?= Html::img($option['image']) ?>
                    </div>
                    <div class="chover check-pack-repair">
                        <?= Html::radio("ApartmentForm[options][{$option['tree_item']}]", $option['checked'], [
                            'class' => 'radio-checkbox',
                            'id' => "option_{$option['id']}",
                            'data-id' => $option['id'],
                            'data-nav' => 1,
                            'data-slider' => 1,
                            'data-price-trigger' => 1,
                            'value' => $option['id'],
                        ]) ?>
                        <label for="option_<?= $option['id'] ?>">
                            <div>
                                <span class="repair-pack-title"><?= $option['title'] ?></span>
                                <span class="repair-pack-price"><?= $option['price'] ?></span>
                            </div>
                        </label>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="repair-packs">
        <div class="text-right">
            <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'btn-big']) ?>
        </div>
    </div>
</div>

<?php $images = ArrayHelper::map($data->options, 'id', function($option) {
    return ['images' => $option['images'], 'thumbs' => $option['thumbs']];
});
$jsonImages = json_encode($images);

$script = <<<JS
    $.each($jsonImages, function(i, val) {
        if(mediaLayer[i] !== 'undefined') {
            mediaLayer[i] = val;
        }
    });
JS;
$this->registerJs($script);