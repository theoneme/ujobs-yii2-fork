<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.12.2017
 * Time: 14:49
 */

?>

<div class="repair-slider">
    <div class="repair-big-slider" id="slick-big-<?= $step ?>">
        <?php foreach ($data->options as $option) { ?>
            <?php if ($option['image'] !== null) { ?>
                <div>
                    <?= Html::img($option['image']) ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="clearfix">
        <div class="repair-small-slider" id="slick-small-<?= $step ?>">
            <?php foreach ($data->options as $option) { ?>
                <?php if ($option['image'] !== null) { ?>
                    <div class="repair-slide">
                        <?= Html::img($option['image']) ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'repair-next-step btn-big']) ?>
    </div>
</div>
