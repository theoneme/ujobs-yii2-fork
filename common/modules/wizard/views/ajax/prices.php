<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.12.2017
 * Time: 18:23
 */
use common\components\CurrencyHelper;
use yii\helpers\Html;

/** @var \common\modules\wizard\models\frontend\LightApartmentForm $model */

?>


<div class="cost-repair">
    <div class="cost-repair-item">
        <div class="cost-repair-title">
            <?= Yii::t('wizard', 'Base price') ?>
        </div>
        <div class="cost-repair-price">
            <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->base_price_data['total']) ?>
        </div>
    </div>
    <div class="cost-repair-item">
        <div class="cost-repair-title">
            <?= Yii::t('wizard', 'Options price') ?>
        </div>
        <div class="cost-repair-price">
            <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->details_price_data['total']) ?>
        </div>
    </div>
    <div class="cost-repair-item">
        <div class="cost-repair-title">
            <?= Yii::t('wizard', 'Summary') ?>
        </div>
        <div class="cost-repair-price">
            <?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency_code'], $model->summary_price_data['total']) ?>
        </div>
    </div>
    <div class="cost-repair-item repair-next-step">
        <?= Html::submitButton(Yii::t('wizard', 'Save & Continue'), ['class' => 'btn-big']) ?>
    </div>
</div>

