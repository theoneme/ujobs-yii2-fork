<?php

use yii\db\Migration;

class m171206_103409_item_connections extends Migration
{
    public function safeUp()
    {
        $this->addColumn('wizard_item', 'entity', $this->string(20)->null());
        $this->addColumn('wizard_item', 'entity_id', $this->integer()->null());

        $this->createIndex('wizard_item_entity_index', 'wizard_item', 'entity');
        $this->createIndex('wizard_item_entity_id_index', 'wizard_item', 'entity_id');
    }

    public function safeDown()
    {
        $this->dropColumn('wizard_item', 'entity');
        $this->dropColumn('wizard_item', 'entity_id');
    }
}
