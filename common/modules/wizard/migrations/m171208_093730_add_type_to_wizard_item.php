<?php

use yii\db\Migration;

/**
 * Class m171208_093730_add_type_to_wizard_item
 */
class m171208_093730_add_type_to_wizard_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('wizard_item', 'type', $this->integer());

        $this->createIndex('wizard_item_type_index', 'wizard_item', 'type');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('wizard_item', 'type');
    }
}
