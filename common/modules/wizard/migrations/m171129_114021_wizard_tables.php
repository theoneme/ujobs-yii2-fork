<?php

use yii\db\Migration;

class m171129_114021_wizard_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('wizard_item', [
            'id' => $this->primaryKey(),
            'lvl' => $this->integer(),
            'root' => $this->integer(),
            'lft' => $this->integer(),
            'rgt' => $this->integer(),
            'name' => $this->string(50),
        ]);

        $this->createTable('wizard_option', [
            'id' => $this->primaryKey(),
            'wizard_item_id' => $this->integer(),
            'price' => $this->integer(),
            'price_operation' => $this->integer(),
            'currency_code' => $this->string(3),
            'image' => $this->string(200),
        ]);

        $this->createTable('wizard_detail_category', [
            'id' => $this->primaryKey(),
            'wizard_item_option_id' => $this->integer(),
            'name' => $this->string(55)
        ]);

        $this->createTable('wizard_detail', [
            'id' => $this->primaryKey(),
            'wizard_detail_category_id' => $this->integer(),
            'price' => $this->integer(),
            'price_operation' => $this->integer(),
            'currency_code' => $this->string(3),
            'image' => $this->string(200),
        ]);

        $this->createTable('wizard_extra', [
            'id' => $this->primaryKey(),
            'wizard_item_id' => $this->integer(),
            'price' => $this->integer(),
            'price_operation' => $this->integer(),
            'currency_code' => $this->string(3),
            'image' => $this->string(200),
        ]);

        $this->createTable('wizard_translation', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(35)->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'content' => $this->text(),
            'locale' => $this->string(10)
        ]);

        $this->createIndex('wizard_item_root_index', 'wizard_item', 'root');
        $this->createIndex('wizard_item_lft_index', 'wizard_item', 'lft');
        $this->createIndex('wizard_item_rgt_index', 'wizard_item', 'rgt');

        $this->createIndex('wizard_translation_entity_index', 'wizard_translation', 'entity');
        $this->createIndex('wizard_translation_entity_id_index', 'wizard_translation', 'entity_id');
        $this->createIndex('wizard_translation_locale_index', 'wizard_translation', 'locale');

        $this->addForeignKey('fk_wizard_option_item_id', 'wizard_option', 'wizard_item_id', 'wizard_item', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_wizard_option_currency_code', 'wizard_option', 'currency_code', 'currency', 'code', 'SET NULL');

        $this->addForeignKey('fk_wizard_extra_item_id', 'wizard_extra', 'wizard_item_id', 'wizard_item', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_wizard_extra_currency_code', 'wizard_extra', 'currency_code', 'currency', 'code', 'SET NULL');

        $this->addForeignKey('fk_wizard_detail_category_item', 'wizard_detail_category', 'wizard_item_option_id', 'wizard_option', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_wizard_detail_category_id', 'wizard_detail', 'wizard_detail_category_id', 'wizard_detail_category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_wizard_detail_currency_code', 'wizard_detail', 'currency_code', 'currency', 'code', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropTable('wizard_translation');
        $this->dropTable('wizard_extra');
        $this->dropTable('wizard_detail');
        $this->dropTable('wizard_detail_category');
        $this->dropTable('wizard_option');
        $this->dropTable('wizard_item');
    }
}
