/**
 * Created by Devour on 04.12.2017.
 */

let mediaLayer = [];
var slickDir = false;
if($('body').hasClass('rtl')){
    slickDir = true;
}

function Viewer() {
    this.initBigSlider = function (selector, asNavForSelector) {
        if ($(selector).hasClass('slick-initialized') === false) {
            $(selector).slick({
                autoplay: false,
                lazyLoad: 'ondemand',
                slidesToShow: 1,
                slidesToScroll: 1,
                asNavFor: asNavForSelector,
                arrows: false,
                dots: false,
                rtl: slickDir,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        dots: false,
                        arrows: true
                    }
                },]
            });

            $(selector).slickLightbox({
                src: 'src',
                itemSelector: 'img'
            });
        }
    };

    this.initMobileSlider = function (selector, asNavForSelector) {
        if ($(selector).hasClass('slick-initialized') === false) {
            $(selector).slick({
                slidesToShow: 10,
                lazyLoad: 'ondemand',
                slidesToScroll: 1,
                autoplay: false,
                asNavFor: asNavForSelector,
                focusOnSelect: true,
                dots: false,
                arrows: false,
                rtl: slickDir
            });

            $(selector).on('afterChange', function(event, slick, currentSlide, nextSlide){
                $(selector + '  .slick-slide').removeClass('slick-current');
                $(selector + '  .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');
            });
        }
    };


    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });


    this.reset = function(selector) {
        $(selector).html('');
    };

    this.buildSliderHtml = function(mediaLayerItem, bigId, smallId) {
        let html = '<div class="repair-big-slider" id="' + bigId +'">';
        $.each(mediaLayerItem['images'], function(i, value) {
            html += '<div><img src="' + value + '"/></div>';
        });
        html += '</div>';
         html += '<div class="clearfix">';
        html += '<div class="repair-small-slider" id="' + smallId + '">';
        $.each(mediaLayerItem['thumbs'], function(i, value) {
            html += '<div class="repair-slide"><img src="' + value + '"/></div>';
        });
        html += '</div>';
        html += '</div>';
        return html;
    };
}

function Calculator(defaultOptions = [], defaultDetails = []) {
    this.basePrice = 0;
    this.detailsPrice = 0;
    this.summaryPrice = 0;

    this.area = 0;
    this.height = 0;
    this.meterPrice = 0;
    this.coefficient = 0;

    this.defaultOptions = defaultOptions;
    this.defaultDetails = defaultDetails;

    this.currencyCode = 'RUB';

    this.setParams = function(area, height, coefficient) {
        this.area = area;
        this.height = height;
        this.coefficient = coefficient;
    };

    this.recalculate = function(options, details, extras) {
        let basePrice = (this.area * this.meterPrice) * Math.pow(this.height, this.height);
        $.each(this.defaultOptions, function(i, val) {
            basePrice += parseInt(val);
        });

        console.log(this.area);
        console.log(this.meterPrice);
        console.log(basePrice + ' ');
    };
}

function NewCalculator() {
    this.recalculate = function(data, replaceSelector) {
        $.post('/wizard/ajax/render-price', data, function(response) {
           if(response.success === true) {
               $('.step-content:visible .cost-repair').replaceWith(response.html);
           }
        });
    }
}