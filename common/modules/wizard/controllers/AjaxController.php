<?php

namespace common\modules\wizard\controllers;

use common\modules\wizard\models\frontend\ApartmentForm;
use common\modules\wizard\models\frontend\LightApartmentForm;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class AjaxController
 * @package common\modules\wizard\controllers
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-price'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRenderPrice()
    {
        $apartmentForm = new LightApartmentForm([
            'step' => ApartmentForm::STEP_APARTMENT_DETAILS
        ]);
        $input = Yii::$app->request->post();

        if (Yii::$app->request->isAjax) {
            $apartmentForm->myLoad($input);
            if ($apartmentForm->save()) {
                return [
                    'success' => true,
                    'html' => $this->renderPartial('prices', [
                        'model' => $apartmentForm
                    ])
                ];
            }
        }

        return ['success' => false];
    }
}
