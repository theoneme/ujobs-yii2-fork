<?php

namespace common\modules\wizard\controllers;

use common\controllers\BackEndController;
use common\modules\wizard\models\entity\Room;
use common\modules\wizard\models\WizardDetail;
use common\modules\wizard\models\WizardDetailCategory;
use common\modules\wizard\models\WizardExtra;
use common\modules\wizard\models\WizardItem;
use common\modules\wizard\models\WizardOption;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\Response;

/**
 * AdminController implements the CRUD actions for Wizard models.
 */
class AdminController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete-tree' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all WizardItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $trees = WizardItem::find()->where(['lvl' => 0])->all();

        return $this->render('index', ['models' => $trees]);
    }

    /**
     * Creates or updates WizardItem model.
     * @param null $id
     * @param null $parent_id
     * @return mixed
     */
    public function actionItem($id = null, $parent_id = null)
    {
        $model = WizardItem::findOne($id) ?? new WizardItem();

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $translationData = Yii::$app->request->post('WizardTranslation', []);
            $extraData = Yii::$app->request->post('WizardExtra', []);
            $optionData = Yii::$app->request->post('WizardOption', []);
            $dcData = Yii::$app->request->post('WizardDetailCategory', []);
            $detailData = Yii::$app->request->post('WizardDetail', []);
            $model->translationsData = $translationData['WizardItem'];
            $model->optionsData = $optionData;
            $model->extrasData = $extraData;
            foreach (array_keys($model->optionsData) as $optionId) {
                $model->optionsData[$optionId]['translationsData'] = $translationData['WizardOption'][$optionId];
                $model->optionsData[$optionId]['detailCategoryData'] = $dcData[$optionId] ?? [];
                foreach (array_keys($model->optionsData[$optionId]['detailCategoryData']) as $dcId) {
                    $model->optionsData[$optionId]['detailCategoryData'][$dcId]['translationsData'] = $translationData['WizardDetailCategory'][$optionId][$dcId];
                    $model->optionsData[$optionId]['detailCategoryData'][$dcId]['detailsData'] = $detailData[$optionId][$dcId] ?? [];
                    foreach (array_keys($model->optionsData[$optionId]['detailCategoryData'][$dcId]['detailsData']) as $detailId) {
                        $model->optionsData[$optionId]['detailCategoryData'][$dcId]['detailsData'][$detailId]['translationsData'] = $translationData['WizardDetail'][$optionId][$dcId][$detailId];
                    }
                }
            }
            foreach (array_keys($model->extrasData) as $extraId) {
                $model->extrasData[$extraId]['translationsData'] = $translationData['WizardExtra'][$extraId];
            }
            $errors = $model->validateWithRelations();
            if (empty($errors)) {
                return ['success' => $model->customSave($parent_id, false), 'id' => 'item-' . $model->id];
            } else {
                return ['success' => false, 'errors' => $errors];
            }
        }
        return $this->renderAjax('_item_form', ['model' => $model, 'create' => $model->isNewRecord, 'parent_id' => $parent_id]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionOption($id = null)
    {
        $index = Yii::$app->request->post('index');
        return $this->renderAjax('_option_form', ['model' => WizardOption::findOne($id) ?? new WizardOption(), 'createForm' => true, 'index' => $index]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionDetailCategory($id = null)
    {
        $index = Yii::$app->request->post('index');
        return $this->renderAjax('_detail_category_form', ['model' => WizardDetailCategory::findOne($id) ?? new WizardDetailCategory(), 'createForm' => true, 'index' => $index]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionDetail($id = null)
    {
        $index = Yii::$app->request->post('index');
        return $this->renderAjax('_detail_form', ['model' => WizardDetail::findOne($id) ?? new WizardDetail(), 'createForm' => true, 'index' => $index]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionExtra($id = null)
    {
        $index = Yii::$app->request->post('index');
        return $this->renderAjax('_extra_form', ['model' => WizardExtra::findOne($id) ?? new WizardExtra(), 'createForm' => true, 'index' => $index]);
    }

    /**
     * Deletes an existing WizardItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     */
    public function actionDeleteItem($id)
    {
        WizardItem::findOne($id)->deleteWithChildren();
    }

    /**
     * @return bool
     */
    public function actionFlushCache()
    {
        Yii::$app->cache->flush();
        $folders = [
            Yii::getAlias('@frontend') . '/runtime/cache/*',
        ];
        foreach ($folders as $folder) {
            $files = glob($folder);
            foreach ($files as $file) {
                FileHelper::removeDirectory($file);
            }
        }

        return true;
    }
}
