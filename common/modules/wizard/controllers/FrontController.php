<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2017
 * Time: 17:20
 */

namespace common\modules\wizard\controllers;

use common\controllers\FrontEndController;
use common\modules\wizard\models\entity\Room;
use common\modules\wizard\models\frontend\ApartmentForm;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class FrontController
 * @package common\modules\wizard\controllers
 */
class FrontController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-room'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $apartmentForm = new ApartmentForm([
            'step' => ApartmentForm::STEP_APARTMENT_DETAILS
        ]);
        $input = Yii::$app->request->post();

        if (Yii::$app->request->isPjax) {
            $apartmentForm->myLoad($input);
            $apartmentForm->save();
        } else if (Yii::$app->request->isAjax) {
            $apartmentForm->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($apartmentForm);
        } else {
            if (!empty($input)) {
                $apartmentForm->load($input);
                return $apartmentForm->save();
            }
        }
        return $this->render('index_new', [
            'model' => $apartmentForm
        ]);
    }

    /**
     * @param $index
     * @return array
     */
    public function actionRenderRoom($index)
    {
        $model = new Room();

        return [
            'html' => $this->renderPartial('steps/partial/room', [
                'model' => $model,
                'index' => $index
            ]),
            'success' => true
        ];
    }
}
