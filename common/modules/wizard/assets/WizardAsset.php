<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 20.11.2017
 * Time: 16:50
 */

namespace common\modules\wizard\assets;

use yii\web\AssetBundle;

/**
 * Class WizardAsset
 * @package common\modules\wizard\assets
 */
class WizardAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/wizard/web';

    public $css = [
        'css/wizard.css',
        'css/slick.css',
        'css/slick-theme.css',
        'css/slick-lightbox.min.css'
    ];

    public $js = [
        'js/slick.min.js',
        'js/script.js',
        'js/slick-lightbox.min.js'
    ];

    public $depends = [
        'frontend\assets\CommonAsset'
    ];
}