<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 20.11.2017
 * Time: 16:50
 */

namespace common\modules\wizard\assets;

use yii\web\AssetBundle;

/**
 * Class WizardAdminAsset
 * @package common\modules\wizard\assets
 */
class WizardAdminAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/wizard/web';

    public $css = [
        'css/wizard-admin.css',
    ];

    public $js = [
    ];

    public $depends = [
        'backend\assets\AppAsset'
    ];
}