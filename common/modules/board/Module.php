<?php

namespace common\modules\board;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package common\modules\board
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\board\controllers';

	/**
	 * @param \yii\base\Application $app
	 */
	public function bootstrap($app)
	{
		\Yii::$app->i18n->translations['board*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@common/modules/board/messages',

			'fileMap' => [
				'board' => 'board.php',
			],
		];
	}
}
