<?php

namespace common\modules\board\controllers;

use common\models\Category;
use common\models\DynamicForm;
use common\models\user\Profile;
use common\modules\board\models\backend\PostProductForm;
use common\modules\board\models\backend\PostRequestForm;
use common\modules\board\models\ProductAttribute;
use common\modules\board\models\search\TenderSearchAdmin;
use common\modules\store\models\Currency;
use Yii;
use common\modules\board\models\Product;
use common\modules\board\models\search\ProductSearchAdmin;
use common\controllers\BackEndController;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdminController implements the CRUD actions for Product model.
 */
class AdminController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearchAdmin(['is_store' => false]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $user_id
     * @return string
     */
    public function actionStore($user_id = null)
    {
        $searchModel = new ProductSearchAdmin(['user_id' => $user_id, 'is_store' => true]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $storesList = Profile::find()->where(['is_store' => true, 'status' => Profile::STATUS_ACTIVE])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'storesList' => $storesList
        ]);
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndexTender()
    {
        $searchModel = new TenderSearchAdmin();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-tender', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new PostProductForm();
        $model->loadProduct($id);

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'product_root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'product_category'])->all(),
            'id',
            'translation.title'
        );
        $currencies = Currency::getFormattedCurrencies();
        $currencySymbols = Currency::getCurrencySymbols();

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);

            $isSaved = $model->save();
            var_dump($model->errors);die;

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/board/admin/index', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        $measures = Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => $measures
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => $measures
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateTender($id)
    {
        $model = new PostRequestForm();
        $model->loadProduct($id);

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'product_root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'product_category'])->all(),
            'id',
            'translation.title'
        );
        $currencies = Currency::getFormattedCurrencies();
        $currencySymbols = Currency::getCurrencySymbols();

        if ($input = Yii::$app->request->post()) {
            $model->myLoad($input);

            $isSaved = $model->save();

            if (Yii::$app->request->isAjax) {
                return $isSaved;
            }
            if ($userId = Yii::$app->request->get('user_id')) {
                return $this->redirect(Url::to(['/board/admin/index-tender', 'user_id' => $userId]));
            } else {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        $measures = Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form-tender', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'isAjax' => true,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => $measures
            ]);
        } else {
            return $this->render('update-tender', [
                'model' => $model,
                'rootCategoriesList' => $rootCategoriesList,
                'currencies' => $currencies,
                'currencySymbols' => $currencySymbols,
                'measures' => $measures
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Product::STATUS_DELETED]);
        $model->updateElastic();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $category_id
     * @param $entity
     * @return null|string
     */
    public function actionRenderAttributes($category_id, $entity)
    {
        /* @var Category $category */
        $category = Category::findOne($category_id);
        if($category === null  || !$category->isLeaf()) {
            return null;
        }

        $values = ProductAttribute::find()->where(['product_id' => $entity])->joinWith(['attr', 'attrValueDescription', 'attrValue'])->all();

        $values = DynamicForm::formatValuesArray($values);
        $attributes = DynamicForm::getAttributesListByCategory($category);
        $model = DynamicForm::initModelFromGroup($attributes, $values);

        return $this->renderAjax('partial/attributes', [
            'model' => $model,
            'attributes' => $attributes
        ]);
    }
}
