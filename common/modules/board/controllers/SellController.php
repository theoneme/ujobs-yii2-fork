<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.08.2017
 * Time: 10:56
 */

namespace common\modules\board\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\modules\board\models\frontend\PostRequestForm;
use common\modules\board\models\Product;
use common\modules\board\models\search\TenderSearch;
use common\modules\store\models\Currency;
use common\modules\store\models\Order;
use frontend\modules\account\models\OrderSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * Class TenderController
 * @package frontend\controllers
 */
class SellController extends FrontEndController
{
    public $layout = '@frontend/views/layouts/account_layout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => [
                        'list',
                    ], 'roles' => ['@']],
                ],
            ],
        ];
    }


    /** TABS BEGIN */

    public function actionList()
    {
        $searchModel = new OrderSearch([
            'seller_id' => Yii::$app->user->identity->getId(),
            'product_type' => [Order::TYPE_PRODUCT],
            'type' => OrderSearch::TYPE_PRODUCT
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('orders', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param $searchModel OrderSearch
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = OrderSearch::getProductStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a($label . "&nbsp;(" . $searchModel->getStatusCount($status) . ")", ['/board/sell/list', 'status' => $status]),
                ['class' => $status == $searchModel->status ? 'active' : '']
            );
        }
        return $html;
    }
}