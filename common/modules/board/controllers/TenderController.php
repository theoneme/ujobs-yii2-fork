<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.08.2017
 * Time: 16:05
 */

namespace common\modules\board\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\Event;
use common\models\user\User;
use common\models\UserAttribute;
use common\modules\board\models\frontend\PostRequestForm;
use common\modules\board\models\Product;
use common\modules\board\models\search\TenderSearch;
use common\modules\store\models\Currency;
use common\modules\store\models\Order;
use common\services\CounterService;
use common\services\RememberViewService;
use common\modules\board\models\frontend\ProductTenderResponseForm;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * Class TenderController
 * @package frontend\controllers
 */
class TenderController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['respond', 'list'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['view'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                    [
                        'actions' => ['update', 'delete', 'pause', 'resume'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToPost() && Product::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $searchModel = new TenderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param TenderSearch $searchModel
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = TenderSearch::getStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a($label . "&nbsp;(" . $searchModel->getStatusCount($status) . ")", ['/board/product/list', 'status' => $status]),
                ['class' => $status == $searchModel->status ? 'active' : '']
            );
        }
        return $html;
    }

    /**
     * @param $alias
     * @return string
     */
    public function actionView($alias)
    {
        /* @var Product $model */
        $model = Product::find()
            ->where(['alias' => $alias, 'type' => Product::TYPE_TENDER])
            ->andWhere(['or',
                ['not', ['product.status' => Product::STATUS_DELETED]],
                ['product.user_id' => userId()]
            ])
            ->one();

        if ($model === null) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested product is not found'));
            return $this->redirect(['/board/category/products']);
        }

        /* @var User $user */
        $targetUser = $model->user;
        $user = Yii::$app->user->identity;
        if ($user) {
            $accessInfo = $user->isAllowedToRespondOnTender($model);
            $contactAccessInfo = $user->isAllowedToCreateConversation($targetUser);
        } else {
            $accessInfo = ['allowed' => false];
            $contactAccessInfo = ['allowed' => false];
        }

        $this->registerMetaTags($model);

        $counterServiceClick = new CounterService( $model, 'click_count');
        $counterServiceClick->process();

        $counterServiceView = new CounterService( $model, 'views_count', 1, 'viewed_product_clicks', Yii::$app->session);
        $counterServiceView->process();

        $rememberViewService = new RememberViewService($model, 'viewed_products', Yii::$app->session);
        $rememberViewService->saveInSession();

        $breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($model->category, $model->getLabel(), '/board/category/ptenders');

        $tenderResponseForm = new ProductTenderResponseForm();
        $tenderResponseForm->maxQuantity = $model->amount;
        $tenderResponseForm->scenario = $model->contract_price ? ProductTenderResponseForm::SCENARIO_CONTRACT_PRICE : ProductTenderResponseForm::SCENARIO_NOT_CONTRACT_PRICE;

        return $this->render('view', [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'accessInfo' => $accessInfo,
            'contactAccessInfo' => $contactAccessInfo,
            'tenderResponseForm' => $tenderResponseForm
        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionCreate()
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $input = Yii::$app->request->post();

        $model = new PostRequestForm();
        $model->product = new Product();

        if ($input) {
            if (Yii::$app->request->isAjax) {
                $model->myLoad($input);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model->validateAll();
            } else {
                if (!empty($input['id'])) {
                    $model->loadProduct($input['id']);
                    $model->myLoad($input);
                    $model->save();
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Request successfully created. Thank you for posting a request!'));
                    return $this->redirect(['/board/tender/view', 'alias' => $model->product->alias]);
                } else {
                    $model->myLoad($input);

                    if ($model->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Request successfully created. Thank you for posting a request!'));
                        return $this->redirect(['/board/tender/view', 'alias' => $model->product->alias]);
                    }

                }
            }
        }
        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'product_root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'product_category'])
                ->all(),
            'id',
            'translation.title'
        );
        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'action' => 'create',
            'currencies' => Currency::getFormattedCurrencies(),
            'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     */
    public function actionUpdate($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $model = new PostRequestForm();
        $model->loadProduct($id);
        $input = Yii::$app->request->post();

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'product_root'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'product_category'])
                ->all(),
            'id',
            'translation.title'
        );

        if (Yii::$app->request->isAjax) {
            $model->myLoad($input);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        } else {
            if (!empty($input)) {
                $model->myLoad($input);

                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Request successfully updated.'));
                    return $this->redirect(['/board/tender/view', 'alias' => $model->product->alias]);
                }
            }
        }
        return $this->render('_form', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'currencies' => Currency::getFormattedCurrencies(),
            'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
        ]);
    }

    /**
     * @return array
     */
    public function actionRespond()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [
            'success' => false,
            'message' => Yii::t('app', 'An error is encountered')
        ];

        if (isGuest()) {
            return $output;
        }

        $tenderResponseForm = new ProductTenderResponseForm();
        $input = Yii::$app->request->post();
        $tenderResponseForm->load($input);

        $model = Product::findOne(['id' => $tenderResponseForm->id, 'type' => Product::TYPE_TENDER]);

        $tenderResponseForm->maxQuantity = $model->amount;
        if ($model && $tenderResponseForm->validate()) {
            $attachments = $tenderResponseForm->save();

            $price = $tenderResponseForm->price;
            $comment = $tenderResponseForm->content;

            $similarOrder = Order::find()
                ->where(['order.product_type' => Order::TYPE_PRODUCT_TENDER, 'seller_id' => userId(), 'customer_id' => $model->user_id])
                ->andWhere(['>', 'created_at', time() - 60 * 60 * 24 * 3])
                ->andWhere(['exists', (new Query())
                    ->select('op.product_id')
                    ->from('order_product op')
                    ->where('op.order_id = order.id')
                    ->andWhere(['op.product_id' => $model->id])
                ])
                ->one();

            if ($similarOrder !== null) {
                $output['message'] = Yii::t('app', 'You have already sent response on this request');
                return $output;
            }

            if (hasAccess($model->user_id)) {
                $output['message'] =Yii::t('app', 'You cant send response on your request');
                return $output;
            }

            $quantity = max(1, $tenderResponseForm->quantity);
            $order = new Order([
                'total' => ($model->contract_price ? $price : $model->price) * $quantity,
                'customComment' => $comment,
                'attachments' => $attachments,
                'is_price_approved' => $model->contract_price ? false : true,
                'product_type' => Order::TYPE_PRODUCT_TENDER,
                'cartItems' => [
                    [
                        'item' => $model,
                        'quantity' => $quantity
                    ]
                ],
                'seller_id' => userId(),
                'customer_id' => $model->user_id,
                'isBlameable' => false,
                'status' => $model->contract_price ? Order::STATUS_PRODUCT_TENDER_WAITING_CUSTOMER_PRICE_DECISION : Order::STATUS_PRODUCT_TENDER_UNAPPROVED
            ]);
            $order->currency_code = $model->contract_price ? Yii::$app->params['app_currency_code'] : $model->currency_code;

            if ($order->save()) {
                Event::addEvent(Event::RESPOND_ON_TENDER);
                $output = [
                    'success' => true,
                    'message' => Yii::t('app', 'Response is sent'),
                    'order_id' => $order->id
                ];
            }
        }

        return $output;
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionPause($id)
    {
        $product = $this->findModel($id);
        if ($product->status == Product::STATUS_ACTIVE) {
            $product->updateAttributes(['status' => Product::STATUS_PAUSED]);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionResume($id)
    {
        $product = $this->findModel($id);
        if ($product->status == Product::STATUS_PAUSED) {
            $product->updateAttributes(['status' => Product::STATUS_ACTIVE]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Product::STATUS_DELETED]);
        $model->updateElastic();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param $id
     * @return Product
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}