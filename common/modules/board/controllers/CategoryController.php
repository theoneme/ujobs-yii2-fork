<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:23
 */

namespace common\modules\board\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\user\Profile;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use common\modules\board\models\ProductAttribute;
use frontend\modules\elasticfilter\components\FilterProduct;
use frontend\modules\elasticfilter\models\BaseFilter;
use frontend\modules\elasticfilter\models\FilterProductStore;
use frontend\modules\elasticfilter\models\FilterProductTag;
use Yii;
use yii\elasticsearch\ActiveQuery;

/**
 * Class CategoryController
 * @package common\modules\board\controllers
 */
class CategoryController extends FrontEndController
{
    /**
     * @param string $category_1
     * @return string|\yii\web\Response
     */
    public function actionProducts($category_1 = 'product_root')
    {
        if (Yii::$app->request->get('category_1') === 'product_root') {
            $get = Yii::$app->request->get();
            unset($get['category_1']);
            $get[0] = '/board/category/products';
            return $this->redirect($get);
        }
        $category = $this->defineCategory($category_1);

        if ($category !== null) {
            /** @var ActiveQuery $products */
            $products = ProductElastic::find()
                ->query([
                    'bool' => [
                        'must_not' => [
                            ['term' => ['profile.status' => Profile::STATUS_PAUSED]],
                            ['term' => ['profile.status' => Profile::STATUS_DISABLED]],
                        ]
                    ]
                ])
                ->where([
                    'status' => Product::STATUS_ACTIVE,
                    'type' => Product::TYPE_PRODUCT
                ]);

            if ($category->lvl > 0) {
                $allChildrenCategoriesIds = $category->children()->select('id')->column();
                $allChildrenCategoriesIds[] = $category->id;

                $products->andWhere(['category_id' => $allChildrenCategoriesIds]);
            }

            $search = Yii::$app->request->get('request');

            $filter = new FilterProduct([
                'query' => $products,
                'type' => Product::TYPE_PRODUCT,
                'category' => $category,
                'filterClass' => \frontend\modules\elasticfilter\models\FilterProduct::class,
                'storeId' => null
            ]);

            $seo = $this->getCategorySeo($category, Product::TYPE_PRODUCT, $filter->attributeFilters, 'category', $search);

            $result = $this->render('products', [
                'category' => $category,
                'filter' => $filter,
                'seo' => $seo,
                'search' => $search,
                'children' => $category->children(1)->joinWith(['translation'])->all(),
                'entity' => Product::TYPE_PRODUCT
            ]);
        } else {
            $category_2 = $this->defineCategoryByAnyLocale($category_1);
            if ($category_2 !== null) {
                return $this->redirect(['/board/category/products', 'category_1' => $category_2]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested category is not found'));
            return $this->redirect(['/board/category/products']);
        }

        return $result;
    }

    /**
     * @param string $category_1
     * @param null $store_id
     * @return string|\yii\web\Response
     */
    public function actionStore($category_1 = 'product_root', $store_id)
    {
        if (Yii::$app->request->get('category_1') === 'product_root') {
            $get = Yii::$app->request->get();
            unset($get['category_1']);
            $get[0] = '/board/category/products';
            return $this->redirect($get);
        }
        $category = $this->defineCategory($category_1);

        if ($category) {
            /** @var ActiveQuery $products */
            $products = ProductElastic::find()
                ->where([
                    'status' => Product::STATUS_ACTIVE,
                    'type' => Product::TYPE_PRODUCT,
                    'user_id' => $store_id
                ])
                ->addOrderBy('tariff.price desc');

            if ($category->lvl > 0) {
                $allChildrenCategoriesIds = $category->children()->select('id')->column();
                $allChildrenCategoriesIds[] = $category->id;

                $products->andWhere(['category_id' => $allChildrenCategoriesIds]);
            }

            $search = Yii::$app->request->get('request');

            $filter = new FilterProduct([
                'query' => $products,
                'type' => Product::TYPE_PRODUCT,
                'category' => $category,
                'filterClass' => FilterProductStore::class,
                'storeId' => $store_id
            ]);

            $seo = $this->getCategorySeo($category, $store_id, $filter->attributeFilters, 'store', $search);

            $result = $this->render('products', [
                'category' => $category,
                'filter' => $filter,
                'seo' => $seo,
                'search' => $search,
                'children' => $category->children(1)->joinWith(['translation'])->all(),
                'entity' => Product::TYPE_PRODUCT
            ]);
        } else {
            $category_2 = $this->defineCategoryByAnyLocale($category_1);
            if ($category_2 !== null) {
                return $this->redirect(['/board/category/products', 'category_1' => $category_2]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested category is not found'));
            return $this->redirect(['/board/category/products']);
        }

        return $result;
    }

    /**
     * @param string $category_1
     * @return string|\yii\web\Response
     */
    public function actionPtenders($category_1 = 'product_root')
    {
        if (Yii::$app->request->get('category_1') === 'product_root') {
            $get = Yii::$app->request->get();
            unset($get['category_1']);
            $get[0] = '/board/category/ptenders';
            return $this->redirect($get);
        }

        $category = $this->defineCategory($category_1);
        if ($category !== null) {
            $allChildrenCategoriesIds = $category->children()->select('id')->column();
            $allChildrenCategoriesIds[] = $category->id;

            $jobs = ProductElastic::find()
                ->where([
                    'category_id' => $allChildrenCategoriesIds,
                    'status' => Product::STATUS_ACTIVE,
                    'type' => Product::TYPE_TENDER
                ])
                ->addOrderBy('tariff.price desc');

            $search = Yii::$app->request->get('request');

            $filter = new FilterProduct([
                'query' => $jobs,
                'type' => 'ptender',
                'category' => $category,
                'filterClass' => \frontend\modules\elasticfilter\models\FilterProduct::class,
                'storeId' => null
            ]);

            $seo = $this->getCategorySeo($category, Product::TYPE_PRODUCT, $filter->attributeFilters, 'category', $search);

            $result = $this->render('products', [
                'category' => $category,
                'filter' => $filter,
                'seo' => $seo,
                'search' => $search,
                'children' => $category->children(1)->joinWith(['translation'])->all(),
                'entity' => 'ptender'
            ]);
        } else {
            $category_2 = $this->defineCategoryByAnyLocale($category_1);
            if ($category_2 !== null) {
                return $this->redirect(['/board/category/ptenders', 'category_1' => $category_2]);
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Requested category is not found'));
            return $this->redirect(['/board/category/ptenders']);
        }

        return $result;
    }

    /**
     * @param null $reserved_product_tag
     * @return string|\yii\web\Response
     */
    public function actionTag($reserved_product_tag = null)
    {
        $cacheLayer = Yii::$app->get('cacheLayer');
        $category = $cacheLayer->getCategoryFromCache('product_root');

        /* @var $tagAttribute ProductAttribute */
        $tagAttribute = ProductAttribute::find()->select('id, product_id, value, value_alias, attribute_id')->where(['entity_alias' => 'product_tag', 'value_alias' => $reserved_product_tag])->one();
        if ($tagAttribute !== null) {
            $query = ProductElastic::find()->where([
                'type' => Product::TYPE_PRODUCT,
                'status' => Product::STATUS_ACTIVE,
            ]);

            $seoAdvanced = $tagAttribute->getMeta(Yii::$app->request->queryParams);
            $this->registerMetaTags($category, $seoAdvanced->templates);
            $seo = $this->buildSeoArray($category, $seoAdvanced->templates);

            $filter = new FilterProduct([
                'query' => $query,
                'category' => $category,
                'filterClass' => FilterProductTag::class,
                'request' => $tagAttribute->value_alias,
                'type' => Product::TYPE_PRODUCT,
                'requestType' => BaseFilter::MODE_TYPE_RESERVED
            ]);

            return $this->render('tag', [
                'tagAttribute' => $tagAttribute,
                'category' => $category,
                'seo' => $seo,
                'filter' => $filter
            ]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Requested tag is not found'));

        return $this->redirect(['/']);
    }

    /**
     * @param null $category_1
     * @return Category
     */
    protected function defineCategory($category_1 = null)
    {
        /* @var $category Category */
        $category = Category::find()
            ->select('category.id, type, alias, lvl, lft, rgt, root, template, image, attribute_set_id')
            ->joinWith(['translation'])
            ->where(['content_translation.slug' => $category_1])
            ->one();

        return $category;
    }

    /**
     * @param null $category_1
     * @return string
     */
    protected function defineCategoryByAnyLocale($category_1 = null)
    {
        $category = Category::find()
            ->joinWith(['translations'])
            ->where(['content_translation.slug' => $category_1, 'type' => 'product_category'])
            ->one();

        return $category->translations[Yii::$app->language]->slug ?? null;
    }

    /**
     * @param Category $category
     * @param string $type
     * @param array $filters
     * @param string $entity
     * @param string $request
     * @return array
     */
    protected function getCategorySeo($category, $type = 'product', $filters = [], $entity = 'category', $request = null){
        $seoAdvanced = $category->getMeta($type, $entity, $filters, $request);
        $templates = [];
        if($seoAdvanced !== null) {
            $seoAdvanced->keyword = $category->translation->title;
            $templates = $seoAdvanced->templates;
        }
        $this->registerMetaTags($category, $templates);
        $seo = $this->buildSeoArray($category, $templates);
        return $seo;
    }
}