<?php

namespace common\modules\board\controllers;

use common\controllers\FrontEndController;
use common\models\Category;
use common\models\DynamicForm;
use common\models\UserAttribute;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeToGroup;
use common\modules\board\models\frontend\PostProductForm;
use common\modules\board\models\ProductAttribute;
use common\modules\store\models\Currency;
use common\models\user\User;
use common\services\CounterService;
use common\services\RememberViewService;
use Yii;
use common\modules\board\models\Product;
use common\modules\board\models\search\ProductSearch;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Cookie;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * AdminController implements the CRUD actions for Product model.
 */
class ProductController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['render-faq', 'render-additional', 'render-extra', 'render-attributes', 'draft', 'list', 'shipping-details'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['view', 'remember-adult'], 'roles' => ['@', '?']],
                    [
                        'actions' => ['create', 'ajax-create', 'render-attributes'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToPost();
                        }
                    ],
                    [
                        'actions' => ['update', 'delete', 'pause', 'resume'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAllowedToPost() && Product::hasAccess(Yii::$app->request->get('id'));
                        }
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-extra', 'render-faq', 'draft', 'ajax-create', 'shipping-details'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @param $alias
     * @return string|Response
     */
    public function actionView($alias)
    {
        /* @var Product $model */
        $model = Product::find()
            ->joinWith(['user' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('user.id, user.username, user.email, user.phone, user.is_company');
            }, 'profile' => function ($q) {
                /* @var ActiveQuery $q */
                $q->select('profile.user_id, profile.status, profile.name, profile.gravatar_email, profile.rating, profile.bio');
            }, 'attachments', 'productAttributes'])
            ->where(['product.alias' => $alias, 'type' => Product::TYPE_PRODUCT])
            ->andWhere(['or',
                ['not', ['product.status' => Product::STATUS_DELETED]],
                ['product.user_id' => userId()]
            ])
            ->one();

        if ($model === null) {
            Yii::$app->session->setFlash('error', Yii::t('board', 'Requested product is not found'));
            return $this->redirect(['/board/category/products']);
        }

        $this->registerMetaTags($model);

        $counterServiceClick = new CounterService($model, 'click_count');
        $counterServiceClick->process();

        $counterServiceView = new CounterService($model, 'views_count', 1, 'viewed_product_clicks', Yii::$app->session);
        $counterServiceView->process();

        $rememberViewService = new RememberViewService($model, 'viewed_products', Yii::$app->session);
        $rememberViewService->saveInSession();

        $breadcrumbs = Yii::$app->utility->buildPageBreadcrumbsNoLevel($model->category, $model->getLabel(), '/board/category/products');

        /* @var User $user */
        $user = Yii::$app->user->identity;
        $targetUser = $model->user;

        if ($user) {
            $accessInfo = $user->isAllowedToCreateConversation($targetUser);
        } else {
            $accessInfo = ['allowed' => false];
        }

        return $this->render('view', [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'accessInfo' => $accessInfo,
        ]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @param $searchModel ProductSearch
     * @return string
     */
    protected function buildTabHeader($searchModel)
    {
        $html = '';
        $statuses = ProductSearch::getStatusLabels();
        foreach ($statuses as $status => $label) {
            $html .= Html::tag('li',
                Html::a($label . "&nbsp;(" . $searchModel->getStatusCount($status) . ")", ['/board/product/list', 'status' => $status]),
                ['class' => $status == $searchModel->status ? 'active' : '']
            );
        }
        return $html;
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $input = Yii::$app->request->post();
        $availableLocales = Yii::$app->params['languages'];
        if (!$input) { // Нулевой шаг - выбор локалей
            return $this->render('locale-step', ['locales' => $availableLocales]);
        } else {
            $model = new PostProductForm();
            $model->dynamic_form = new DynamicForm();
            $model->product = new Product();
            $model->scenario = PostProductForm::SCENARIO_DEFAULT;
            $locales = !empty($input['locales']) ? $input['locales'] : [];
            $locales = array_filter($locales, function ($locale) use ($availableLocales) {
                return isset($availableLocales[$locale]);
            });
            if ($locales) { // Если в посте есть локали - попадаем на первый шаг
                $model->locale = array_shift($locales);
                $currencies = Currency::getFormattedCurrencies();
                $currencySymbols = Currency::getCurrencySymbols();

                $rootCategoriesList = ArrayHelper::map(
                    Category::find()
                        ->where(['root' => Category::find()->where(['alias' => 'product_root', 'type' => 'product_category', 'disabled' => false])->select('id')->scalar()])
                        ->andWhere(['lvl' => 1])
                        ->joinWith(['translation'])
                        ->andWhere(['type' => 'product_category'])
                        ->orderBy('category.sort_order')
                        ->all(),
                    'id',
                    'translation.title'
                );

                return $this->render('create', [
                    'model' => $model,
                    'rootCategoriesList' => $rootCategoriesList,
                    'action' => 'create',
                    'locales' => $locales,
                    'currencies' => $currencies,
                    'currencySymbols' => $currencySymbols,
                    'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
                ]);
            } else { // Иначе попадаем на валиацию\сабмит
                if (Yii::$app->request->isAjax) {
                    $model->myLoad($input);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $model->validateAll();
                } else {
                    if (!empty($input['id'])) { // Если работа уже есть в базе благодаря драфту (примерно всегда)
                        $model->loadProduct($input['id']);
                        $model->product->status = Product::STATUS_ACTIVE;
                    }
                    $model->myLoad($input);

                    if ($model->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('board', 'Product has been created. Thank you for posting an announcement!'));
                        return $this->redirect(['/board/product/view', 'alias' => $model->product->alias]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', Yii::t('board', 'Error occurred during Product creation.'));
                        return $this->render('locale-step', ['locales' => $availableLocales]);
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    public function actionDraft()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [
            'success' => false
        ];
        $attachments = [];

        $model = new PostProductForm();
        $model->scenario = PostProductForm::SCENARIO_DRAFT;
        if ($input = Yii::$app->request->post()) {
            if (!empty($input['id'])) {
                $model->loadProduct($input['id']);
            } else {
                $model->product = new Product();
            }
            $model->dynamic_form = new DynamicForm();
            $model->myLoad($input);
            if ($model->save()) {
                if (!empty($model->product->attachments)) {
                    foreach ($model->product->attachments as $key => $attachment) {
                        $attachments['initialPreview'][$key] = $attachment->content . '?' . time();
                        $attachments['initialPreviewConfig'][] = [
                            'caption' => basename($attachment->content),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => $key,
                        ];
                    }
                }
                $output = [
                    'success' => true,
                    'product_id' => $model->product->id,
                    'attachments' => $attachments
                ];
            }
        }

        return $output;
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = '@frontend/views/layouts/nomenu_layout';

        $model = new PostProductForm();
        $model->loadProduct($id);
        $input = Yii::$app->request->post();
        if ($input) {
            if (Yii::$app->request->isAjax) {
                $model->myLoad($input);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model->validateAll();
            } else {
                $model->myLoad($input);
                $model->product->status = Product::STATUS_REQUIRES_MODERATION;

                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('board', 'Product has been updated.'));
                    return $this->redirect(['/board/product/view', 'alias' => $model->product->alias]);
                }
            }
        }

        $rootCategoriesList = ArrayHelper::map(
            Category::find()
                ->where(['root' => Category::find()->where(['alias' => 'product_root', 'type' => 'product_category'])->select('id')->scalar()])
                ->andWhere(['lvl' => 1])
                ->joinWith(['translation'])
                ->andWhere(['type' => 'product_category'])
                ->orderBy('category.sort_order')
                ->all(),
            'id',
            'translation.title'
        );
        $currencies = Currency::getFormattedCurrencies();
        $currencySymbols = Currency::getCurrencySymbols();

        return $this->render('create', [
            'model' => $model,
            'rootCategoriesList' => $rootCategoriesList,
            'action' => 'update',
            'currencies' => $currencies,
            'currencySymbols' => $currencySymbols,
            'measures' => Yii::$app->cacheLayer->get('categoryMeasures_' . Yii::$app->language)
        ]);
    }

    /**
     * @param $category_id
     * @param $entity
     * @return null|string
     */
    public function actionRenderAttributes($category_id, $entity)
    {
        /* @var Category $category */
        $category = Category::findOne($category_id);
        if ($category === null || !$category->isLeaf()) {
            return null;
        }

        $values = ProductAttribute::find()
            ->where(['product_id' => $entity])
            ->joinWith(['attr', 'attrValueDescriptions', 'attrValue'])
            ->groupBy('product_attribute.value')
            ->all();

        $values = DynamicForm::formatValuesArray($values);
        $attributes = DynamicForm::getAttributesListByCategory($category);
        $model = DynamicForm::initModelFromGroup($attributes, $values);

        $showAdditionalAtStart = array_intersect(array_column($values, 'attribute_id'), array_column($attributes, 'id'));

        $temp = ArrayHelper::map($attributes, 'id', function ($model) {
            return $model;
        }, 'is_primary');
        $attributes = $temp[1] ?? [];
        $additionalAttributes = $temp[0] ?? [];

        return $this->renderAjax('@common/modules/attribute/views/partial/attributes', [
            'model' => $model,
            'attributes' => $attributes,
            'additionalAttributes' => $additionalAttributes,
            'showAdditionalAtStart' => $showAdditionalAtStart,
        ]);
    }

    /**
     * @param $category_id
     * @return array
     */
    public function actionShippingDetails($category_id)
    {
        $categoriesWithoutShipping = [
            2478,
            2479,
            2480,
            2481,
            2482,
            2483,
            2484,
            2485,
            2778,
            2779
        ];

        if(!in_array($category_id, $categoriesWithoutShipping)) {
            return ['available' => true];
        }

        return ['available' => false];
    }

    /**
     * @return bool
     * @throws MethodNotAllowedHttpException
     */
    public function actionRememberAdult()
    {
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'adultCookie',
                'value' => time(),
            ]));

            return true;
        }

        throw new MethodNotAllowedHttpException('Not supported');
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionPause($id)
    {
        $product = $this->findModel($id);
        if ($product->status == Product::STATUS_ACTIVE) {
            $product->updateAttributes(['status' => Product::STATUS_PAUSED]);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionResume($id)
    {
        $product = $this->findModel($id);
        if ($product->status == Product::STATUS_PAUSED) {
            $product->updateAttributes(['status' => Product::STATUS_ACTIVE]);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Product::STATUS_DELETED]);
        $model->updateElastic();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
