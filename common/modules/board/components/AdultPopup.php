<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.10.2017
 * Time: 10:00
 */

namespace common\modules\board\components;

use common\models\Banner;
use common\models\Category;
use Yii;
use yii\base\Widget;
use yii\db\Expression;
use yii\web\Cookie;

/**
 * Class AdultPopup
 * @package common\modules\board\components
 */
class AdultPopup extends Widget
{
    /**
     * @var Category
     */
    public $category;

    /**
     * @var mixed|\yii\web\CookieCollection
     */
    private $requestCookies;

    /**
     * AdultPopup constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->requestCookies = Yii::$app->request->cookies;
    }

    /**
     * @return null|string
     */
    public function run()
    {
        if((boolean) $this->category->is_adult === true) {
            $adultCookie = $this->requestCookies->getValue('adultCookie', null);

            if($adultCookie === null || (time() - $adultCookie) > (60 * 60 * 24 * 30)) {
                return $this->render('adult-popup');
            }
        }

        return null;
    }
}