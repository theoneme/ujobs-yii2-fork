<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.10.2017
 * Time: 10:00
 */
use common\modules\board\assets\AdultAsset;
use yii\helpers\Url;

AdultAsset::register($this);

?>

<div class="modal fade in" id="age" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true" style="padding-right: 17px;">
    <div class="modal-dialog age21">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close not-age">×</button>
            </div>
            <div class="modal-body">
                <div class="title-age">
                    21+
                    <span>
                            <?= Yii::t('board', 'Welcome to uJobs. You must confirm that you are adult to gain access for this section.') ?>
                        </span>
                </div>
                <div class="text-age">
                    <p class="age-high">
                        <?= Yii::t('board', 'Category contains information for adults') ?>
                        <?= Yii::t('board', 'Information on this site is not advertising, is purely informative, and intended only for personal use.') ?>
                    </p>
                    <p class="age-low">
                        <?= Yii::t('board', 'Dear visitor, we have to disable your access to products in this category. We are categorically against the use of alcohol by minors.') ?>
                    </p>
                </div>
                <div class="text-center">
                    <a class="button white no-size age-go" id="i-am-adult" href="#" data-dismiss="modal" aria-hidden="true"><?= Yii::t('board', 'I`am {age, plural, one{# year} other{# years}}', ['age' => 21]) ?></a>
                    <a class="button white no-size age-home" href="/"><?= Yii::t('board', 'To homepage') ?></a>
                </div>
                <div class="warning-age">
                    <?= Yii::t('board', 'Attention') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $rememberAdultUrl = Url::to(['/board/product/remember-adult']);
$script = <<<JS
    $('#age').modal('show');

    $('.not-age').on('click', function(e) {
        $('.age21 p.age-high').hide();
        $('.age21 p.age-low').show();
        $('.age21 .age-go').hide();
        $('.age21 .age-home').show();
    });
    
    $('#i-am-adult').on('click', function() {
        $.post('{$rememberAdultUrl}', function(response) {
            
        });
    });
JS;

$this->registerJs($script);

