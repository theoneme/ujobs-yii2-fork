<?php

use common\modules\board\models\Product;
use yii\db\Migration;

class m170804_114702_product_type extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'type', $this->string(20));
        Product::updateAll(['type' => Product::TYPE_PRODUCT]);
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'type');
    }
}
