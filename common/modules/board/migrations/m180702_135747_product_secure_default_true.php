<?php

use yii\db\Migration;

/**
 * Class m180702_135747_product_secure_default_true
 */
class m180702_135747_product_secure_default_true extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('product', 'secure', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->alterColumn('product', 'secure', $this->boolean());
    }
}
