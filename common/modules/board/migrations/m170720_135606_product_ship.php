<?php

use yii\db\Migration;

class m170720_135606_product_ship extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'shipping_info', $this->string(512));
        $this->addColumn('product', 'secure', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'shipping_info');
        $this->dropColumn('product', 'secure');
    }
}
