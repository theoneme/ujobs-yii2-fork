<?php

use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\ProductAttribute;
use yii\db\Migration;

class m170728_100501_attribute_2 extends Migration
{
    public function safeUp()
    {
        $shipping = new Attribute([
            'is_system' => false,
            'name' => 'Тип доставки',
            'type' => 'radio',
            'alias' => 'shipping_type',
            'category_dependant' => 0,
            'is_searchable' => 1,
        ]);

        $shipping->translationsArr['ru-RU']->title = 'Тип доставки';
        $shipping->translationsArr['en-GB']->title = 'Shipping type';
        $shipping->translationsArr['es-ES']->title = 'Tipo de entrega';
        $shipping->translationsArr['uk-UA']->title = 'Тип доставки';

        $shipping->save();

        $attrValue = new AttributeValue([
            'attribute_id' => $shipping->id,
            'translationsArr' => [
                [
                    'title' => 'Возможна доставка',
                    'locale' => 'ru-RU'
                ]
            ]
        ]);
        $attrValue->save();

        $attrValue = new AttributeValue([
            'attribute_id' => $shipping->id,
            'translationsArr' => [
                [
                    'title' => 'Самовывоз',
                    'locale' => 'ru-RU'
                ]
            ]
        ]);
        $attrValue->save();

        ProductAttribute::updateAll(['value' => Yii::t('board', 'New')], ['value_alias' => 'new']);
        ProductAttribute::updateAll(['value' => Yii::t('board', 'Used')], ['value_alias' => 'old']);

        $productTag = new Attribute([
            'is_system' => false,
            'name' => 'Тег',
            'type' => 'checkboxlist',
            'alias' => 'product_tag',
            'category_dependant' => 0,
            'is_searchable' => 1,
        ]);

        $productTag->translationsArr['ru-RU']->title = 'Тег';
        $productTag->translationsArr['en-GB']->title = 'Tag';
        $productTag->translationsArr['es-ES']->title = 'Tag';
        $productTag->translationsArr['uk-UA']->title = 'Тег';

        $productTag->save();

        ProductAttribute::updateAll(['entity_content' => $productTag->id, 'key' => "attribute_{$productTag->id}", 'entity_alias' => 'product_tag'], ['key' => "attribute_36"]);
    }

    public function safeDown()
    {
        $shipping = Attribute::findOne(['alias' => 'shipping_type']);
        if ($shipping !== null) {
            $shipping->delete();
        }

        $productTag = Attribute::findOne(['alias' => 'product_tag']);
        if ($productTag !== null) {
            $productTag->delete();
        }
    }
}
