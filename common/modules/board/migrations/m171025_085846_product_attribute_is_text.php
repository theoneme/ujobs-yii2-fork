<?php

use yii\db\Migration;

class m171025_085846_product_attribute_is_text extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('product_attribute', 'is_text');
    }

    public function safeDown()
    {
        $this->addColumn('product_attribute', 'is_text', $this->boolean());
    }
}
