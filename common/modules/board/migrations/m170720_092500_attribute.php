<?php

use common\modules\attribute\models\Attribute;
use yii\db\Migration;

class m170720_092500_attribute extends Migration
{
    public function safeUp()
    {
        $brand = new Attribute([
            'is_system' => false,
            'name' => 'Бренд',
            'type' => 'textbox',
            'alias' => 'brand_a',
            'category_dependant' => 0,
            'is_searchable' => 0,
        ]);

        $brand->translationsArr['ru-RU']->title = 'Бренд';
        $brand->translationsArr['en-GB']->title = 'Brand';
        $brand->translationsArr['es-ES']->title = 'Marca de fábrica';
        $brand->translationsArr['uk-UA']->title = 'Бренд';

        $color = new Attribute([
            'is_system' => false,
            'name' => 'Цвет',
            'type' => 'textbox',
            'alias' => 'color_a',
            'category_dependant' => 0,
            'is_searchable' => 0,
        ]);

        $color->translationsArr['ru-RU']->title = 'Цвет';
        $color->translationsArr['en-GB']->title = 'Color';
        $color->translationsArr['es-ES']->title = 'Color';
        $color->translationsArr['uk-UA']->title = 'Колір';

        $condition = new Attribute([
            'is_system' => false,
            'name' => 'Состояние',
            'type' => 'radio',
            'alias' => 'condition',
            'category_dependant' => 0,
            'is_searchable' => 0,
        ]);

        $condition->translationsArr['ru-RU']->title = 'Материал';
        $condition->translationsArr['en-GB']->title = 'Condition';
        $condition->translationsArr['es-ES']->title = 'Estado';
        $condition->translationsArr['uk-UA']->title = 'Cтан';

        $material = new Attribute([
            'is_system' => false,
            'name' => 'Материал',
            'type' => 'textbox',
            'alias' => 'material',
            'category_dependant' => 0,
            'is_searchable' => 0,
        ]);

        $material->translationsArr['ru-RU']->title = 'Состояние';
        $material->translationsArr['en-GB']->title = 'Material';
        $material->translationsArr['es-ES']->title = 'Material';
        $material->translationsArr['uk-UA']->title = 'Матеріал';

        $res = $condition->save() && $color->save() && $material->save() && $brand->save();
    }

    public function safeDown()
    {
        $brand = Attribute::findOne(['alias' => 'brand_a']);
        if($brand !== null) {
            $brand->delete();
        }
        $material = Attribute::findOne(['alias' => 'material']);
        if($material !== null) {
            $material->delete();
        }
        $color = Attribute::findOne(['alias' => 'color_a']);
        if($color !== null) {
            $color->delete();
        }
        $condition = Attribute::findOne(['alias' => 'condition']);
        if($condition !== null) {
            $condition->delete();
        }
    }
}
