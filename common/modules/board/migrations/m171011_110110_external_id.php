<?php

use yii\db\Migration;

class m171011_110110_external_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'external_id', $this->integer()->defaultValue(null));

        $this->createIndex('product_external_id_index', 'product', 'external_id');
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'external_id');
    }
}
