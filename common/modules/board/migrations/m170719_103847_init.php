<?php

use yii\db\Migration;

class m170719_103847_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => $this->integer()->notNull(),
            'price' => $this->integer()->null(),
            'currency_code' => $this->string(3)->null(),
            'contract_price' => $this->boolean(),
            'parent_category_id' => $this->integer()->null(),
            'category_id' => $this->integer()->null(),
            'click_count' => $this->integer()->defaultValue(0),
            'views_count' => $this->integer()->defaultValue(0),
            'alias' => $this->string(125)->notNull(),
            'locale' => $this->string(5)->notNull()
        ], 'CHARACTER SET utf8');

        $this->addForeignKey('fk_product_user', 'product', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_product_parent_category_id', 'product', 'parent_category_id', 'category', 'id', 'SET NULL');
        $this->addForeignKey('fk_product_category_id', 'product', 'category_id', 'category', 'id', 'SET NULL');
        $this->addForeignKey('fk_product_currency_code', 'product', 'currency_code', 'currency', 'code', 'SET NULL');
        $this->createIndex('product_status_index', 'product', 'status');
        $this->createIndex('product_locale_index', 'product', 'locale');

        $this->createTable('product_attribute', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(20),
            'entity_content' => $this->integer(),
            'product_id' => $this->integer(),
            'key' => $this->string(25),
            'value' => $this->text(),
            'locale' => $this->string(5)->notNull(),
            'is_text' => $this->boolean(),
            'entity_alias' => $this->string(35),
            'value_alias' => $this->string(75)
        ], 'CHARACTER SET utf8');

        $this->addForeignKey('fk_product_attribute_user', 'product_attribute', 'product_id', 'product', 'id', 'CASCADE');
        $this->createIndex('product_attribute_entity_index', 'product_attribute', 'entity');
        $this->createIndex('product_attribute_entity_content_index', 'product_attribute', 'entity_content');
        $this->createIndex('product_attribute_is_text_index', 'product_attribute', 'is_text');
        $this->createIndex('product_attribute_entity_alias_index', 'product_attribute', 'entity_alias');
        $this->createIndex('product_attribute_value_alias_index', 'product_attribute', 'value_alias');
        $this->createIndex('product_attribute_locale_index', 'product_attribute', 'locale');
    }

    public function safeDown()
    {
        $this->execute("SET foreign_key_checks = 0;");
        $this->dropForeignKey('fk_product_user', 'product');
        $this->dropForeignKey('fk_product_parent_category_id', 'product');
        $this->dropForeignKey('fk_product_category_id', 'product');
        $this->dropForeignKey('fk_product_currency_code', 'product');

        $this->dropTable('product');

        $this->dropForeignKey('fk_product_attribute_user', 'product_attribute');
        $this->dropTable('product_attribute');
        $this->execute("SET foreign_key_checks = 1;");
    }
}
