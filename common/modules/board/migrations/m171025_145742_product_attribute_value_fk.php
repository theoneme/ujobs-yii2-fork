<?php

use yii\db\Migration;

class m171025_145742_product_attribute_value_fk extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('product_attribute', 'value', $this->integer());

        Yii::$app->db->createCommand('delete `product_attribute` from `product_attribute` left join mod_attribute_value as v
            on product_attribute.value = v.id
            WHERE v.alias is NULL')->execute();

        $this->addForeignKey('fk_product_attribute_value', 'product_attribute', 'value', 'mod_attribute_value', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_attribute_value', 'product_attribute');

        $this->alterColumn('product_attribute', 'value', $this->text());
    }
}
