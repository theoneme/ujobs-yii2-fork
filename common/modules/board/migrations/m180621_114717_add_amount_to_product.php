<?php

use yii\db\Migration;

/**
 * Class m180621_114717_add_amount_to_product
 */
class m180621_114717_add_amount_to_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'amount', $this->integer()->defaultValue(1));
        $this->addColumn('product', 'measure_id', $this->integer());
        $this->addForeignKey('fk_product_measure_id', 'product', 'measure_id', 'measure', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_measure_id', 'product');
        $this->dropColumn('product', 'measure_id');
        $this->dropColumn('product', 'amount');
    }
}
