<?php

use yii\db\Migration;

class m170801_134506_add_shipping_data_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'length', $this->integer());
        $this->addColumn('product', 'width', $this->integer());
        $this->addColumn('product', 'height', $this->integer());
        $this->addColumn('product', 'lat', $this->string());
        $this->addColumn('product', 'long', $this->string());
        $this->addColumn('product', 'address', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'length');
        $this->dropColumn('product', 'width');
        $this->dropColumn('product', 'height');
        $this->dropColumn('product', 'lat');
        $this->dropColumn('product', 'long');
        $this->dropColumn('product', 'address');
    }
}
