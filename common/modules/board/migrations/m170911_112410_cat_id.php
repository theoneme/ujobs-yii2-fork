<?php

use yii\db\Migration;

class m170911_112410_cat_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'subcategory_id', $this->integer()->after('category_id'));

        $this->addForeignKey('fk_product_subcategory_id', 'product', 'subcategory_id', 'category', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_subcategory_id', 'product');

        $this->dropColumn('product', 'subcategory_id');
    }
}
