<?php

use common\models\AddressTranslation;
use common\modules\board\models\Product;
use yii\db\Migration;

/**
 * Class m180215_121148_remove_address_from_product
 */
class m180215_121148_remove_address_from_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $products = Product::find()->where(['not',
            ['or',
                ['lat' => null],
                ['lat' => ''],
                ['long' => null],
                ['long' => ''],
            ]
        ])->all();
        foreach ($products as $product) {
            /* @var $product Product*/
            if ($product->lat && $product->long) {
                if (!AddressTranslation::find()->where(['lat' => $product->lat, 'long' => $product->long, 'locale' => 'ru-RU'])->exists()) {
                    $this->insert('address_translation', ['lat' => $product->lat, 'long' => $product->long, 'locale' => 'ru-RU', 'title' => $product->address]);
                }
            }
        }
        $this->dropColumn('product', 'address');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('product', 'address', $this->string());
        $products = Product::find()->where(['not',
            ['or',
                ['lat' => null],
                ['lat' => ''],
                ['long' => null],
                ['long' => ''],
            ]
        ])->all();
        foreach ($products as $product) {
            /* @var $product Product*/
            if ($product->lat && $product->long) {
                /* @var $address AddressTranslation*/
                $address = AddressTranslation::find()->where(['lat' => $product->lat, 'long' => $product->long])->one();
                if ($address !== null) {
                    $product->updateAttributes(['address' => $address->title]);
                }
            }
        }
    }
}
