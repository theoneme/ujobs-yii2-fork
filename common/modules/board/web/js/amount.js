$(document).on('click', '.product-amount-plus', function (e) {
    var val = parseInt($(this).siblings('input.product-amount-input').val());
    var max = parseInt($('.product-measure-amount-block').data('max'));
    if (val < max) {
        $(this).siblings('input.product-amount-input').val(val + 1).change();
    }
    return false;
});
$(document).on('click', '.product-amount-minus', function (e) {
    var val = parseInt($(this).siblings('input.product-amount-input').val());
    if (val > 1) {
        $(this).siblings('input.product-amount-input').val(val - 1).change();
    }
    return false;
});
$(document).on('keyup', '.product-amount-input', function (e) {
    var val = parseInt($(this).val());
    var max = parseInt($(this).closest('.product-measure-amount-block').data('max'));
    if (val < 1) {
        $(this).val(1);
    }
    if (val > max){
        $(this).val(max);
    }
    $(this).change();
    return false;
});