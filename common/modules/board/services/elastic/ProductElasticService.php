<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:58
 */

namespace common\modules\board\services\elastic;

use common\components\CurrencyHelper;
use common\models\Category;
use common\models\user\Profile;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\models\Product;
use Yii;
use yii\elasticsearch\ActiveRecord;

/**
 * Class ProductElasticService
 * @package common\services
 */
class ProductElasticService
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductElasticService constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $product = $this->product;
        $globalScore = $rootScore = $upperCategoryScore = $categoryScore = 0;
        $attachments = [];
        $attributes = [];

        $productElastic = ProductElastic::find()->where(['id' => $product->id])->one();
        if ($productElastic === null) {
            /* @var ProductElastic $productElastic */
            $productElastic = new ProductElastic();
            $productElastic->setPrimaryKey((int)$product->id);
            $productElastic->user = [
                'id' => $product->user->id,
                'username' => $product->user->username,
                'is_company' => false,
                'company_id' => null
            ];
        }
        $productElastic->attributes = array_intersect_key($product->attributes, $productElastic->attributes);
        $productElastic->default_price = CurrencyHelper::convert($product->currency_code, 'RUB', $product->price);
        $productElastic->category_id = $product->subcategory_id ?? $product->category_id;

        if($product->status === Product::STATUS_ACTIVE) {
//            if($product->profile->status === Profile::STATUS_ACTIVE) {
//                $globalScore += 20;
//            }
            if($product->profile->activeTariff) {
                $globalScore += 15;
            }
            if($product->created_at > (time() - 60 * 60 * 24 * 2)) {
                $globalScore += 20;
            }
            if($product->user->confirmed_at > 0 && $product->user->confirmed_at !== $product->user->created_at) {
                $globalScore += 10;
            }

            /******************/
            $totalProducts = Product::find()->select('id, user_id, status, type')->where(['user_id' => $product->user_id, 'status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT])->count();
            $previousProducts = Product::find()->select('id, user_id, status, type')->where(['user_id' => $product->user_id, 'status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT])->andWhere(['<=', 'id', $product->id])->count();
            $order = $totalProducts - $previousProducts;

            if($order === 0) {
                $rootScore = $globalScore + 10;
            } else {
                $rootScore = $globalScore - 10 * $order;
            }
            $productElastic->root_score = $rootScore;
            /******************/
            $totalProducts = Product::find()->select('id, user_id, status, type')->where(['user_id' => $product->user_id, 'status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT, 'category_id' => $product->category_id])->count();
            $previousProducts = Product::find()->select('id, user_id, status, type')->where(['user_id' => $product->user_id, 'status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT, 'category_id' => $product->category_id])->andWhere(['<=', 'id', $product->id])->count();
            $order = $totalProducts - $previousProducts;

            if($order === 0) {
                $categoryScore = $globalScore + 10;
            } else {
                $categoryScore = $globalScore - 10 * $order;
            }
            $productElastic->category_score = $categoryScore;
            /******************/
            $parentCategory = Category::findOne($product->parent_category_id);
            $siblings = $parentCategory->children()->select('id')->column();
            $totalProducts = Product::find()->select('id, user_id, status, type')->where(['user_id' => $product->user_id, 'status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT, 'category_id' => $siblings])->count();
            $previousProducts = Product::find()->select('id, user_id, status, type')->where(['user_id' => $product->user_id, 'status' => Product::STATUS_ACTIVE, 'type' => Product::TYPE_PRODUCT, 'category_id' => $siblings])->andWhere(['<=', 'id', $product->id])->count();
            $order = $totalProducts - $previousProducts;

            if($order === 0) {
                $upperCategoryScore = $globalScore + 10;
            } else {
                $upperCategoryScore = $globalScore - 10 * $order;
            }
            $productElastic->upper_category_score = $upperCategoryScore;
        }

        $productElastic->helpers = [
            'locale' => strtolower(Yii::$app->params['languages'][$product->locale]),
            'secure' => $product->secure
        ];

        if ((boolean)$product->user->is_company === true) {
            $productElastic->user = [
                'id' => $product->user->id,
                'username' => $product->user->username,
                'is_company' => true,
                'company_id' => $product->user->company->id
            ];

            $productElastic->profile = [
                'user_id' => $product->user->id,
                'name' => $product->user->company->getSellerName(),
                'gravatar_email' => $product->user->company->logo,
                'status' => $product->user->company->status,
                'rating' => 0,
                'translations' => array_map(function ($var) {
                    return ['title' => $var->title, 'locale' => $var->locale];
                }, $product->user->company->translations ?? [])
            ];
        } else {
            $productElastic->profile = [
                'user_id' => $product->user->id,
                'name' => $product->profile->getSellerName(),
                'gravatar_email' => $product->profile->gravatar_email,
                'status' => $product->profile->status,
                'rating' => $product->profile->rating,
                'translations' => array_map(function ($var) {
                    return ['title' => $var->title, 'locale' => $var->locale];
                }, $product->profile->translations ?? [])
            ];
        }

        $productElastic->category = [
            'title' => $product->category->translation->title,
            'id' => $product->category_id,
            'alias' => $product->category->alias
        ];

        foreach ($product->attachments as $attachment) {
            $attachments[] = [
                'id' => $attachment->id,
                'content' => $attachment->content
            ];
        }
        $productElastic->attachments = $attachments;

        foreach ($product->productAttributes as $extra) {
            $attributes[] = [
                'attribute_id' => $extra->attribute_id,
                'value' => $extra->value,
                'title' => $extra->attrDescription->title,
                'entity_alias' => $extra->entity_alias,
                'value_alias' => $extra->value_alias
            ];
        }
        $productElastic->attrs = $attributes;
        $productElastic->translation = ['title' => $product->translation->title];

        if ($product->profile->activeTariff) {
            $productElastic->tariff = [
                'price' => $product->profile->activeTariff->tariff->cost,
                'icon' => $product->profile->activeTariff->tariff->icon,
                'expires_at' => $product->profile->activeTariff->expires_at,
                'title' => $product->profile->activeTariff->tariff->title
            ];
        } else {
            $productElastic->tariff = [
                'price' => 0,
                'icon' => null,
                'expires_at' => null,
                'title' => null
            ];
        }

        return $productElastic;
    }
}