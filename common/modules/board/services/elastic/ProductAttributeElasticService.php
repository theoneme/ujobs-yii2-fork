<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2017
 * Time: 14:58
 */

namespace common\modules\board\services\elastic;

use common\components\CurrencyHelper;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\models\ProductAttribute;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class ProductAttributeElasticService
 * @package common\services
 */
class ProductAttributeElasticService
{
    /**
     * @var ProductAttribute
     */
    private $productAttribute;

    /**
     * ProductAttributeElasticService constructor.
     * @param ProductAttribute $productAttribute
     */
    public function __construct(ProductAttribute $productAttribute)
    {
        $this->productAttribute = $productAttribute;
    }

    /**
     * @param bool $save
     * @return bool|ActiveRecord
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        } else {
            return $object;
        }
    }

    /**
     * @return boolean|ActiveRecord
     */
    private function makeObject()
    {
        $productAttribute = $this->productAttribute;

        $productAttributeElastic = ProductAttributeElastic::find()->where(['id' => $productAttribute->id])->one();
        /* @var ProductAttributeElastic $productAttributeElastic */
        if ($productAttributeElastic === null) {
            $productAttributeElastic = new ProductAttributeElastic();
            $productAttributeElastic->setPrimaryKey((int)$productAttribute->id);
        }
        $productAttributeElastic->attributes = array_intersect_key($productAttribute->attributes, $productAttributeElastic->attributes);

        $productAttributeElastic->product = [
            'id' => $productAttribute->product_id,
            'category_id' => $productAttribute->product->subcategory_id ?? $productAttribute->product->category_id,
            'status' => $productAttribute->product->status,
            'price' => $productAttribute->product->price,
            'currency_code' => $productAttribute->product->currency_code,
            'default_price' => CurrencyHelper::convert($productAttribute->product->currency_code, 'RUB', $productAttribute->product->price),
            'title' => isset($productAttribute->product->translation->title) ? $productAttribute->product->translation->title : '',
            'description' => isset($productAttribute->product->translation->content) ? strip_tags($productAttribute->product->translation->content) : '',
            'type' => $productAttribute->product->type,
            'user_id' => $productAttribute->product->user_id
        ];

        $productAttributeElastic->attribute = [
            'translations' => ArrayHelper::map($productAttribute->attrDescriptions, 'locale', 'title')
        ];

        $productAttributeElastic->attributeValue = [
            'translations' => ArrayHelper::map($productAttribute->attrValueDescriptions, 'locale', 'title'),
            'status' => $productAttribute->attrValue->status
        ];

        return $productAttributeElastic;
    }

}