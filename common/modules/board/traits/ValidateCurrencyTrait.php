<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.09.2017
 * Time: 17:00
 */

namespace common\modules\board\traits;

use common\modules\store\models\Currency;

trait ValidateCurrencyTrait
{
    public $currency_code;

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });
    }
}