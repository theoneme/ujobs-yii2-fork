<?php 
 return [
    '(Here you can upload photos)' => '(คุณสามารถอัพโหลดภาพถ่าย)',
    'One bedroom apartment of 80 square meters in the center of Moscow.' => 'สองห้องอพาร์ทเม้นของ 80 อร์เมตรอยู่ในศูนย์กลางของมอสโคว์ก่อนทาน',
    'About This Product' => 'เรื่องนี้ผลิตภัณฑ์',
    'About This Request' => 'เรื่องซอส',
    'Active' => 'ที่ทำงานอยู่',
    'Add Photo' => 'เพิ่มภาพถ่าย',
    'Add tag' => 'เพิ่มป้ายกำกับ',
    'Address' => 'ที่อยู่',
    'Alias' => 'เชื่อมโยง',
    'All' => 'ทั้',
    'Allowed formats: JPEG, JPG and PNG' => 'ได้รับอนุญาตรูปแบบ:แฟ้มภาพประเภท JPEGNAME,JPG และ PNG',
    'It\'s almost ready...' => 'เกือบพร้อม...',
    'Are you sure you want to delete this item?' => 'คุณแน่ใจหรือว่าคุณต้องการจะลบ?',
    'Are you sure?' => 'คุณแน่ใจหรือไม่?',
    'Attribute' => 'แอททริบิวต์',
    'Attributes' => 'แอททริบิวต์ต่างๆ',
    'Awaiting Moderation' => 'รอเนื่อง',
    'Back' => 'ก่อน',
    'Blue shirt' => 'เสื้อสีน้ำเงิน',
    'Blue shirt with size M.' => 'เสื้อสีน้ำเงินขนาดของ M',
    'Brand' => 'แบรนด์',
    'Buy for {price}' => 'ซื้อสำหรับ{price}',
    'By defining your product properly it will be easier for buyers to find and buy.' => 'ลูกค้าของคุณจะง่ายกว่าที่จะค้นหาของรายการถ้าคุณให้รอบคอบก่อนกำหนดรายละเอียดทั้งหมดออก',
    'Category' => 'หมวดหมู่',
    'Category ID' => 'หมายเลขระบุตัวของผู้หมวดหมู่',
    'Category contains information for adults' => 'หมวดหมู่บรรจุข้อมูลสำหรับผู้ใหญ่',
    'Check out this product on {site}:' => 'มุมมองนี่ผลิตภัณฑ์บนเว็บไซต์{site}:',
    'Choose Subcategory' => 'เลือก Subcategory',
    'Choose category and subcategory' => 'เลือกหมวดหมู่และ subcategory',
    'Click Count' => 'จำนวนของเห็นมันเลย',
    'Clicks' => 'ลิ',
    'Color' => 'สี',
    'Condition' => 'เงื่อนไข',
    'Contact Me' => 'ต้องติดต่อฉัน',
    'Contact seller' => 'การเขียนไปยังหนังสือขาย',
    'Contract Price' => 'มีไว้ให้ต่อรองราคา',
    'Create Product' => 'จะตีพิมพ์ข่าว',
    'Create Product: ' => 'จะตีพิมพ์ข่าว',
    'Create Request: ' => 'จะตีพิมพ์เป็นโปรแกรม: ',
    'Created At' => 'สร้างเดท',
    'Currency Code' => 'สัญลักษณ์ของเงินตรารหัส',
    'Currency and price' => 'สัญลักษณ์ของเงินตราและราคา',
    'Currency for price' => 'คนเงินตราต่างประเทศสำหรับราคา',
    'Dear visitor, we have to disable your access to products in this category. We are categorically against the use of alcohol by minors.' => 'ที่รักมาเยี่ยมพวกเราบังคับให้ต้องปฏิเสธรตรวจสอบเพื่อนขอผลิตภัณฑ์อยู่ในนี้หมวดหมู่ได้ เราแน่ใจแน่ใจกับผู้เยาองดื่มนะ',
    'Delete' => 'เพื่อลบ',
    'Delivery type' => 'ส่งของประเภท',
    'Denied' => 'ถูกปฏิเสธ',
    'Describe in more detail what exactly you want to buy. Provide information for the sellers.' => 'อธิบายในรายละเอียดอะไรกันแน่คุณต้องการซื้อ. ให้ข้อมูลสำหรับ sellers น',
    'Describe product that you need.' => 'อธิบายผลิตภัณฑ์ที่คุณต้องการ',
    'Describe what do you want to buy' => 'อธิบายสิ่งที่คุณต้องการที่จะซื้อ',
    'Description' => 'รายละเอียด',
    'Description of product that you need' => 'รายละเอียดของผลิตภัณฑ์ที่คุณต้องการ',
    'Details of shipping' => 'ส่งรายละเอียด',
    'Didn\'t find the product at the right price or  don\'t have time to search?' => 'ไม่ได้เจอผลิตภัณฑ์ในราคายุติธรรมหรือไม่มีเวลาที่จะค้นหา?',
    'Dimensions: more - better' => 'ภาพถ่ายขนาดที่มากขึ้น-ดีขึ้น',
    'Disabled' => 'ปิดการใช้งาน',
    'Do not agree on deposit, if you are not sure on seller`s reliability. When paying with cash, check product right on receiving. If you use «Secure transaction», then check product before completing order, or right on receiving on shipping.' => 'ไม่ตกลงสำหรับค่าจ้างถ้าเธอไม่มั่นใจของ reliability ของหนังสือขายนะ ถ้าจ่ายเงินโดยเงินสดผลิตภัณฑ์ดูทันทีที่สังเกตเห็นจากใบเสร็จนะ ถ้าคุณใช้คำว่า"ปลอดภัยการต่อรอง"ตรวจดูตรงผลิตภัณฑ์ก่อนการเติมคำให้สมบูรณ์ของคำสั่งหรือทันทีหลังจากการส่งของหน่อยค่ะ',
    'Double bed 200x180. Made in Slovenia.' => 'เตียงสอง 200х180 น ทำให้อยู่ในสโลเวเนียน',
    'Drafts' => 'ข่าวร่าง',
    'Edit' => 'แก้ไข',
    'Error occurred during Product creation.' => 'ตอนที่กำลังสร้างเป็นผลิตภัณฑ์ที่เกิดข้อผิดพลาดเกิดขึ้น',
    'For example:' => 'สำหรับตัวอย่าง:',
    'Extra information' => 'รายละเอียดเพิ่มเติม@label',
    'Max File size: {count} MB' => 'ขนาดแฟ้มไม่ควรจะเกิ{count}MB',
    'For example' => 'ตัวอย่างเช่น',
    'Get your product' => 'ได้รับรายการของคุณ',
    'Height' => 'ความสูง',
    'How much are you ready to pay for this service?' => 'ว่าคุณจะจ่ายสำหรับบริการนี้?',
    'How the service works' => 'วิธีการทำงาน',
    'How uJobs work' => 'ยังไง uJobs',
    'I do not want to pay the commission. It is enough for me that my contacts are visible, I alone decide the payment options for my product' => 'ไม่ต้องการจะจ่ายค่าธรรมเนียม เพียงพอสำหรับฉันที่จะเจอของฉันที่อยู่ติดต่อ,ฉันจะตัดสินใจที่จ่ายเงินตัวเลือกสำหรับผลิตภัณฑ์ของฉัน',
    'I want to sell through a Secure Transaction and I am ready to pay a service commission of {percent}% of the value of my goods. (Commission includes the cost of processing payments)' => 'ต้องการจะขายผ่านทางปลอดภัยการต่อรองและมักจะยอมจ่ายค่าจ้างบริการ{percent}%ของมูลค่าของสินค้าฉัน (ค่าจ้างถึงราคาของการประมวลผลจ่ายเงิน)',
    'I`am {age, plural, one{# year} other{# years}}' => 'ฉัน{age, plural, one{#ปี} few{#ปี} many{#ปี} other{#ปี}}',
    'If you can not upload a photo, our system will install a general picture or leave your profile without photo.' => 'ถ้าคุณไม่สามารถอัพโหลดรูปที่ระบบจะออกจากอยู่ในสถานที่ของมันเป็นมาตรฐานของภาพได้',
    'Information on this site is not advertising, is purely informative, and intended only for personal use.' => 'ข้อมูลโพสบนเว็บไซต์เป็นฝืสำหรับ informational ประสงค์และเป็นแรงสำหรับส่วนตัวที่ใช้อย่างเดียว',
    'It`s important to choose the right category for your announcement. This helps sellers to find your product faster.' => 'มันคือสิ่งสำคัญสำหรับกำหนซึ่งในส่วนของเว็บไซต์ของจะเป็นพ่อนายด้วย นี่จะช่วยลูกค้าอย่างง่ายดายตามหาพ่อนายด้วย',
    'It’s photo time! You can upload as many images as you want.' => 'เวลาถ่ายรูป! คุณสามารถอัพโหลดเป็นหลายภาพอย่างที่คุณต้องการ',
    'Language' => 'ภาษา',
    'Latitude' => 'เส้นรุ้ง',
    'Length' => 'ความยาว',
    'Let`s publish your product and <br/> attract the attention of buyers.' => 'ปล่อยให้พวกเราตีพิมพ์ของคุณผลิตภัณฑ์แล้ว <br /> จะดึงดูดความสนใจของผู้ซื้อสักราย',
    'Let`s publish your request and <br/> attract the attention of sellers.' => 'ปล่อยให้เป็นการตีพิมพ์คำขอของคุณและจะพาเธอออ sellers น',
    'Locale' => 'ภาษา',
    'Longitude' => 'เส้นแวง',
    'Make choice' => 'เลือก',
    'Make your product easier to find' => 'บรรเทาโพรเซสของค้นหาของคุณผลิตภัณฑ์',
    'Manage Products' => 'การจัดการสินค้า',
    'Material' => 'วัสดุ',
    'Max' => 'แม็กซ์',
    'Member from {date}' => 'ผู้มีส่วนร่วม@item participation is optional{date}',
    'Moscow, Novy Arbat 12' => 'มอสโคว์ Novy Arbat ถนน 12',
    'Name of product' => 'ชื่อรายการ',
    'Name of product that you need. For example: Videocard' => 'ชื่อของผู้ผลิตภัณฑ์ที่คุณต้องการ สำหรับตัวอย่าง:กราฟิกการ์ด',
    'Name of your product' => 'ชื่อของของคุณผลิตภัณฑ์',
    'New' => 'คนใหม่',
    'Next language' => 'คอยตามภาษา',
    'Note:' => 'ข้อควรทราบ:',
    'Options of selling' => 'ตัวเลือกขาย',
    'Options of shipping' => 'ส่งตัวเลือก',
    'Overview' => 'มุมมองภาพรวม',
    'Parent Category ID' => 'หมายเลขของพ่อแม่หมวดหมู่',
    'Pause' => 'หยุดชั่วคราว',
    'Paused' => 'ถูกพักการเรี',
    'Pay deposit' => 'เพื่อให้เป็นมัดจำ',
    'Percent bonus' => 'โบนัสนใจ',
    'Photo and details' => 'ภาพถ่ายและรายละเอียด',
    'Photos' => 'รูปถ่าย',
    'Place an order' => 'ต้องการจะสั่งซื้อ',
    'Please select a category and subcategory most relevant for your request.' => 'โปรดเลือกหมวดหมู่และ subcategory ส่วนใหญ่มีส่วนเกี่ยวข้องกับโปรแกรมของคุณ.',
    'Post a Request' => 'จะตีพิมพ์ของโปรแกรม',
    'Price' => 'ราคา',
    'Price and currency' => 'ราคาและสัญลักษณ์ของเงินตรา',
    'Price and shipping' => 'ราคาและส่งของ',
    'Product ID' => 'รายการชื่อ',
    'Product Name' => 'ชื่อรายการ',
    'Product has been created. Thank you for posting an announcement!' => 'รายการถูกสร้างขึ้นเรียบร้อยแล้ว ขอบคุณสำหรับโพสกาโฆษณา!',
    'Product has been updated.' => 'ผลิตภัณฑ์ปรับปรุงเรียบร้อยแล้ว',
    'Product sizes' => 'ดูจากขนาดของสินค้าที่',
    'Product {title} in category {category}' => 'รายการ{title}ในหมวดหมู่{category}',
    'Products' => 'ผลิตภัณฑ์',
    'Publish' => 'โฆษณา',
    'Publish Request' => 'จะตีพิมพ์ของโปรแกรม',
    'Publish product' => 'จะตีพิมพ์ข่าว',
    'Quantity' => 'จำนวน',
    'Requested product is not found' => 'ขอผลิตภัณฑ์ไม่พบ',
    'Requires Details' => 'ต้องการช่วยทำให้ชัดเจน',
    'Reset' => 'ปรับค่า',
    'Resume' => 'ทำงานต่อ',
    'Sale options' => 'ตัวเลือกขาย',
    'Save & Continue' => 'บันทึกและทำต่อไป',
    'Say, that you found his announcement on {site}' => 'บอกเขาว่าคุณพบโฆษณาของเค้าอยู่{site}',
    'Search' => 'การค้นหา',
    'Search My History...' => 'ค้นหาในประวัติศาสตร์...',
    'See less' => 'ย่อเก็บเธร',
    'See more' => 'มุมมองที่ชัดเจนขึ้น',
    'Select A Category' => 'เลือกหมวดหมู่',
    'Select A Subcategory' => 'เลือก subcategory',
    'Select An Upper Category' => 'เลือกบนหมวดหมู่',
    'Select the languages in which you want to post products' => 'เลือกภาษาที่คุณต้องการจะถานที่โฆษณาเรื่องผลิตภัณฑ์',
    'Set Your Budget' => 'คุณกำหนดงบประมาณ',
    'Set address where product can be viewed, or address for shipping calculation. No need to set your apartment number, or house number, if you don`t wish to.' => 'ป้อนที่อยู่ที่ไหนคุณจะเห็นรายการหรือที่อยู่ของเพื่อคำนวณถูกส่งองใช้จ่ายเยอะหน่อย บ้านหรืออพาร์ทเม้นจำนวนเป็นเลือกหรอกนะ',
    'Set how product can be sent and who pays for shipping' => 'บ่งบอกถึงรายการอาจจะส่งและใครเป็นคนจ่ายสำหรับส่ง',
    'Set in details who pays for shipping and how you can send product to other cities.' => 'รายละเอียดของใครจ่ายสำหรับส่งแล้วคุณสามารถส่งสินค้าที่ต้องเมืองอื่นทีไรนะ',
    'Set the price for the measure unit specified above. Also set currency for your price.' => 'ถามสำหรับหน่วยของ measurement ระบุไว้ด้านบน แล้วก็สำหรับกำหนดสัญลักษณ์ของเงินตราซึ่งคุณตั้งราคาค่างวด',
    'Set quantity if you have more than one item for sale.' => 'สำหรับกำหนดจำนวนหากคุณมีมากกว่าหนึ่งคัดลอกสำหรับขายค่ะ',
    'Indicate what you are selling' => 'สำหรับกำหนดสิ่งที่คุณขาย',
    'Setting that shipping is possible means that you can send product to other cities via courier service.' => 'ระบุว่าส่งของเป็นไปได้ที่คุณยืนยันว่าคุณสามารถส่งสินค้าที่ต้องอีกเมืองผ่านทางคนส่งของของบริการ.',
    'Shipping details' => 'ส่งรายละเอียด',
    'Shipping type' => 'ส่งของประเภท',
    'Show Customer`s Contacts' => 'ที่อยู่ติดต่อเพื่อแสดงคนซื้อ',
    'Show Seller\'s Contacts' => 'ต้องติดต่อหนังสือขาย',
    'Specialties' => 'ความสามารถพิเศษ',
    'Status' => 'สถานะ',
    'Subcategory ID' => 'หมายเลขของ subcategory',
    'Submit an order for the product and hundreds of sellers will see your request.' => 'ที่เป็นประมูลผู้ผลิตภัณฑ์คุณต้องการที่จะซื้อและหลายร้อยของ sellers จะได้เห็นของคุณอยากขอให้ช่วยอะไรหน่อย',
    'Submit product' => 'ส่งโปรแกรม',
    'Terms of delivery' => 'เงื่อนไขของส่งของ',
    'If you select more languages, more clients  in different countries will be able to see your offer and thus increase your profits. And if you want to order services or buy products you will have more options to choose from.' => 'ที่มากกว่าภาษาคุณสำหรับกำหนดมากลูกค้าจากประเทศที่ต่างกันสามารถมองเห็นข้อเสนอของคุณที่จะปรับปรุงของคุณพยายามแบล็กเมล์ผม...รายได้',
    'To homepage' => 'บนหลัก',
    'To respond' => 'เพื่อตอบสนอง',
    'In this field, try to describe your product in as much detail as possible. The more detailed your description is, the more likely it is that buyers will choose your offer.' => 'ในช่องข้อมูลนี้พยายามที่จะอธิบายของรายการที่แสดงรายละเอียดเท่าที่เป็นไปได้ ยิ่งกล่องแสดงรายละเอียดรายละเอียดคือคนที่สูงขึ้นโอกาสที่คนซื้อก็จะเลือกข้อเสนอของคุณ.',
    'Try to make it clear from the title what you are selling. You can use up to 80 characters.' => 'พยายามออกจากชื่อเจ้าหญิงบียอนเซ่หรอกชัดเจนแล้วว่าคุณขาย คุณสามารถใช้เพื่อ 80 ตัวอักษร',
    'Type' => 'ประเภท',
    'Update' => 'ปรับปรุง',
    'Update {modelClass}: ' => 'เพื่อปรับปรุง{modelClass}: ',
    'Updated At' => 'วันเปลี่ยนแปลง',
    'Used' => 'บี/u',
    'Unavailable' => 'ไม่ว่า',
    'Value' => 'ค่า',
    'Views' => 'มุมมอง',
    'Views Count' => 'จำนวนของมุมมอง',
    'Warning!' => 'ความสนใจ!',
    'We offer two options for sale: through the service or directly. If you sell through the service, we provide a Safe Transaction, which means that you will receive {percent}% of the value that you specified and the money will be received only after your buyer receives the goods that you sell and confirm receipt.' => 'เราขอเสนอสองตัวเลือกสำหรับขาย:ผ่านทางบริการหรือโดยตรง ถ้าคุณยอมขายผ่านทางบริการเราให้ปลอดภัยการต่อรองมันหมายความว่าคุณจะได้รับ{percent}%ของค่าคุณกำหนดและมีเงินมากพอที่จะถูกส่งมาเพื่อคุณแค่หลังจากที่คุณซื้ออย่างเราได้รับคำขู่แบบนี้พึ่งซึ่งคุณขายและยืนยันว่าใบเสร็จนะ',
    'Welcome to uJobs. You must confirm that you are adult to gain access for this section.' => 'ยินดีต้อนรับสู่ uJobs น สำหรับการเข้าถึงคุณต้องยืนยันอายุเท่านี้',
    'What is your budget for this request?' => 'อะไรคือบประมาณสำหรับโปรแกรมนี้?',
    'Where will your request be?' => 'ที่ที่โปรแกรมของคุณจะอาศัย?',
    'Width' => 'ความกว้าง',
    'You can select multiple languages' => 'คุณสามารถเลือกหลายภาษา',
    'You have to specify product subcategory' => 'เลือก subcategory ของผลิตภัณฑ์',
    'You may add a photo to your request. This will make your request more attractive.' => 'คุณสามารถเพิ่มรูปของคุณอยากขอให้ช่วยอะไรหน่อย นี่จะทำให้โปรแกรมของคุณมากกว่าที่มีเสน่ห์น',
    'Your address' => 'ที่อยู่ของคุณ',
    'Your product`s size. These fields are required if shipping is available for your product. You may leave these fields empty is only pickup is available.' => 'ดูจากขนาดของคุณผลิตภัณฑ์นะ พวกนี้ช่องข้อมูลต้องการที่จะเสร็จสมบูรณ์ถ้าของคุณผลิตภัณฑ์ส่งของมันเป็นไปได้ คุณสามารถทิ้งช่องข้อมูลว่างเปล่าถ้าคุณเพียงต้อนรัก',
    'cm' => 'cm',
    'Adult Signature Required: You must be at least 21 years of age to purchase this product. Adult signature is required on delivery.' => 'การยืนยันคือต้องการต้องซื้อรายการนี้,คุณต้องเป็นอย่างน้อยอายุ 21 ปีของอายุเท่านี้ ยืนยันการเกิดขึ้นเมื่อส่งของ',
    'Recommendation:' => 'หมายรับรอง:',
    'We recommend to set your price. Requests with contract price receive responses 7 times less than requests with exact price.' => 'เราขอแนะนำให้คุณตั้งค่าส่วนผสมที่แน่นอนรางวัล ใครให้สูงกว่านี้กับสัญญาราคาได้รับการตอบสนองใน 7 ครั้งน้อยกว่า..จะเพิ่มเดิมพันกับที่กำหนดราคาได้',
    'item' => 'หน่วยของผลิตภัณฑ์',
    'pc' => 'หมายเลข pct',
    'In stock' => 'ในหุ้นของ',
    'How much are you ready to pay for this product?' => 'ว่าคุณจะจ่ายเพื่อสิ่งนี้ผลิตภัณฑ์?',
    'Set quantity if you want to buy more than one item.' => 'สำหรับกำหนดจำนวนถ้าคุณต้องการสั่งมากกว่าหนึ่งรายการ',
    'Specify what you will pay for. Whether it is product item or a certain unit of measure.' => 'สำหรับกำหนดสิ่งที่คุณจะต้องชดใช้ สำหรับหน่วยของผลิตภัณฑ์หรือผลิตภัณฑ์นะ',
    'Specify what your buyers will pay for. Whether it is product item or a certain unit of measure.' => 'สำหรับกำหนดสิ่งที่จะจ่ายของผู้ซื้อสักราย สำหรับหน่วยของผลิตภัณฑ์หรือผลิตภัณฑ์นะ',
    'Required' => 'ต้องการ',
    '{price} per {amount} {measure}' => '{price}{amount}{measure}',
    'Enter address' => 'ป้อนที่อยู่',
];