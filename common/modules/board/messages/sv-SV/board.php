<?php 
 return [
    '(Here you can upload photos)' => '(Här kan du ladda upp bilder)',
    'One bedroom apartment of 80 square meters in the center of Moscow.' => 'Två-rums lägenhet på 80 kvadratmeter i centrum av Moskva.',
    'About This Product' => 'Om Denna Produkt',
    'About This Request' => 'Om denna sås',
    'Active' => 'Aktiv',
    'Add Photo' => 'Lägg till ett foto',
    'Add tag' => 'Lägg till tagg',
    'Address' => 'Adress',
    'Alias' => 'Länk',
    'All' => 'Alla',
    'Allowed formats: JPEG, JPG and PNG' => 'Tillåtna format: JPEG, JPG och PNG',
    'It\'s almost ready...' => 'Nästan klar...',
    'Are you sure you want to delete this item?' => 'Är du säker på att du vill ta bort?',
    'Are you sure?' => 'Är du säker?',
    'Attribute' => 'Attribut',
    'Attributes' => 'Attribut',
    'Awaiting Moderation' => 'Väntar på moderering',
    'Back' => 'Sedan',
    'Blue shirt' => 'Blå skjorta',
    'Blue shirt with size M.' => 'Skjorta blå storlek M',
    'Brand' => 'Varumärke',
    'Buy for {price}' => 'Köp för {price}',
    'By defining your product properly it will be easier for buyers to find and buy.' => 'Dina kunder kommer att vara lättare att hitta din artikel, om du noga ange alla detaljer.',
    'Category' => 'Kategori',
    'Category ID' => 'ID kategori',
    'Category contains information for adults' => 'Kategori innehåller information för vuxna',
    'Check out this product on {site}:' => 'Se här produkten på hemsidan {site}:',
    'Choose Subcategory' => 'Välj En Underkategori',
    'Choose category and subcategory' => 'Välj en kategori och en underkategori',
    'Click Count' => 'Antalet klick',
    'Clicks' => 'Klick',
    'Color' => 'Färg',
    'Condition' => 'Villkor',
    'Contact Me' => 'Kontakta mig',
    'Contact seller' => 'För att skriva till säljaren',
    'Contract Price' => 'Pris förhandlingsbart',
    'Create Product' => 'Att publicera artikeln',
    'Create Product: ' => 'Att publicera artikeln',
    'Create Request: ' => 'Att publicera ett program: ',
    'Created At' => 'Skapad datum',
    'Currency Code' => 'Valuta kod',
    'Currency and price' => 'Valuta och priset',
    'Currency for price' => 'Valuta för priser',
    'Dear visitor, we have to disable your access to products in this category. We are categorically against the use of alcohol by minors.' => 'Kära besökare, vi är tvungna att förneka inspektion av produkter i denna kategori. Vi är starkt emot minderåriga att dricka.',
    'Delete' => 'För att ta bort',
    'Delivery type' => 'Leverans typ',
    'Denied' => 'Avvisas',
    'Describe in more detail what exactly you want to buy. Provide information for the sellers.' => 'Att i detalj beskriva exakt vad du vill köpa. Ge information för säljare.',
    'Describe product that you need.' => 'För att beskriva den produkt som du behöver',
    'Describe what do you want to buy' => 'Beskriv vad du vill köpa',
    'Description' => 'Beskrivning',
    'Description of product that you need' => 'Beskrivning av den produkt som du behöver',
    'Details of shipping' => 'Leverans detalj',
    'Didn\'t find the product at the right price or  don\'t have time to search?' => 'Kunde inte hitta den produkt till rätt pris eller det finns ingen tid att söka?',
    'Dimensions: more - better' => 'Foto storlekar: ju mer - desto bättre',
    'Disabled' => 'Funktionshindrade',
    'Do not agree on deposit, if you are not sure on seller`s reliability. When paying with cash, check product right on receiving. If you use «Secure transaction», then check product before completing order, or right on receiving on shipping.' => 'Nöj dig inte med betalning om du inte är säker på tillförlitligheten av säljaren. Om du betalar kontant, in produkten på plats efter mottagandet. Om du använder "Säker transaktion", kontrollera produkten innan slutförande av beställning eller omedelbart efter leverans.',
    'Double bed 200x180. Made in Slovenia.' => 'Dubbelsäng 200х180. Tillverkad i Slovenien.',
    'Drafts' => 'Utkast',
    'Edit' => 'Ändra',
    'Error occurred during Product creation.' => 'När du skapar en produkt som felet uppstod.',
    'For example:' => 'Till exempel:',
    'Extra information' => 'Ytterligare information',
    'Max File size: {count} MB' => 'Storleken bör inte överstiga {count} MB',
    'For example' => 'Till exempel',
    'Get your product' => 'Ta emot dina objekt',
    'Height' => 'Höjd',
    'How much are you ready to pay for this service?' => 'Hur mycket skulle du betala för den här tjänsten?',
    'How the service works' => 'Hur fungerar tjänsten',
    'How uJobs work' => 'Hur fungerar uJobs',
    'I do not want to pay the commission. It is enough for me that my contacts are visible, I alone decide the payment options for my product' => 'Vill inte betala avgiften. Nog för mig att se mina kontakter, jag kommer att besluta om betalningsalternativ för min produkt',
    'I want to sell through a Secure Transaction and I am ready to pay a service commission of {percent}% of the value of my goods. (Commission includes the cost of processing payments)' => 'Vill sälja via en Säker transaktion och är villiga att betala expeditionsavgift på {percent}% av värdet av min produkt. (I avgiften ingår kostnaden för att behandla betalningar)',
    'I`am {age, plural, one{# year} other{# years}}' => 'Jag {age, plural, one{# year} few{# year} many{# åren} other{# åren}}',
    'If you can not upload a photo, our system will install a general picture or leave your profile without photo.' => 'Om du inte kan ladda upp foton, kommer systemet att lämna i dess ställe en vanlig bild.',
    'Information on this site is not advertising, is purely informative, and intended only for personal use.' => 'Information som publiceras på webbplatsen är endast för informationssyfte, och är endast avsett för personligt bruk.',
    'It`s important to choose the right category for your announcement. This helps sellers to find your product faster.' => 'Det är viktigt att bestämma i vilken del av webbplatsen kommer att vara din annons. Detta kommer att hjälpa kunderna lätt kan hitta din annons.',
    'It’s photo time! You can upload as many images as you want.' => 'Tid foton! Du kan ladda upp så många bilder som du önskar.',
    'Language' => 'Språk',
    'Latitude' => 'Latitud',
    'Length' => 'Längd',
    'Let`s publish your product and <br/> attract the attention of buyers.' => 'Låt oss publicera Din produkt och <br /> kommer dra till sig uppmärksamhet av köpare.',
    'Let`s publish your request and <br/> attract the attention of sellers.' => 'Låt oss publicera din begäran och kommer att få sin säljare.',
    'Locale' => 'Språk',
    'Longitude' => 'Longitud',
    'Make choice' => 'Välj',
    'Make your product easier to find' => 'Underlätta processen att hitta din produkt',
    'Manage Products' => 'Råvaru-förvaltning',
    'Material' => 'Material',
    'Max' => 'Max',
    'Member from {date}' => 'Deltagare {date}',
    'Moscow, Novy Arbat 12' => 'Moskva, Novy Arbat-Gatan 12',
    'Name of product' => 'Artikel namn',
    'Name of product that you need. For example: Videocard' => 'Namnet på den produkt som du behöver. Till exempel: grafik kort',
    'Name of your product' => 'Namnet på din produkt',
    'New' => 'Nya',
    'Next language' => 'Följande språk',
    'Note:' => 'Observera:',
    'Options of selling' => 'Försäljning alternativ',
    'Options of shipping' => 'Leveransalternativ',
    'Overview' => 'Översikt',
    'Parent Category ID' => 'ID: t för den förälder kategori',
    'Pause' => 'Paus',
    'Paused' => 'Tillfälligt',
    'Pay deposit' => 'För att göra en Insättning',
    'Percent bonus' => 'Bonus intresse',
    'Photo and details' => 'Bilder och information',
    'Photos' => 'Foton',
    'Place an order' => 'För att placera en beställning',
    'Please select a category and subcategory most relevant for your request.' => 'Vänligen välj en kategori och en underkategori som är mest relevant för din ansökan.',
    'Post a Request' => 'Att offentliggöra ansökan',
    'Price' => 'Pris',
    'Price and currency' => 'Pris och valuta',
    'Price and shipping' => 'Pris och leverans',
    'Product ID' => 'Artikel-ID',
    'Product Name' => 'Artikel namn',
    'Product has been created. Thank you for posting an announcement!' => 'Posten har skapats. Tack för att publicera annonser!',
    'Product has been updated.' => 'Produkten uppdaterat',
    'Product sizes' => 'De dimensioner av varor',
    'Product {title} in category {category}' => 'Item {title} i kategorin {category}',
    'Products' => 'Produkter',
    'Publish' => 'Publicering',
    'Publish Request' => 'Att offentliggöra ansökan',
    'Publish product' => 'Att publicera artikeln',
    'Quantity' => 'Antalet',
    'Requested product is not found' => 'Efterfrågad produkt som inte finns',
    'Requires Details' => 'Kräver en förklaring',
    'Reset' => 'Återställ',
    'Resume' => 'Återuppta',
    'Sale options' => 'Försäljning alternativ',
    'Save & Continue' => 'Spara och fortsätt',
    'Say, that you found his announcement on {site}' => 'Säg till honom att du hittade hans annons på {site}',
    'Search' => 'Sök',
    'Search My History...' => 'Sök i historien...',
    'See less' => 'Kollaps',
    'See more' => 'Se mer',
    'Select A Category' => 'Välj en kategori',
    'Select A Subcategory' => 'Välj en underkategori',
    'Select An Upper Category' => 'Välj den översta kategorin',
    'Select the languages in which you want to post products' => 'Välj på vilka språk du vill ha för att placera annonser om produkter',
    'Set Your Budget' => 'Ange din budget',
    'Set address where product can be viewed, or address for shipping calculation. No need to set your apartment number, or house number, if you don`t wish to.' => 'Ange den adress där du kan se punkt eller adress för att beräkna fraktkostnaden. Hus eller lägenhet nummer är valfritt.',
    'Set how product can be sent and who pays for shipping' => 'Ange hur posten kan skickas och vem som betalar för frakt',
    'Set in details who pays for shipping and how you can send product to other cities.' => 'Uppgifter om vem som betalar för frakt samt hur du kan skicka varorna till andra städer.',
    'Set the price for the measure unit specified above. Also set currency for your price.' => 'Fråga pris för måttenheter som anges ovan. Också anger den valuta i vilken du har satt priset.',
    'Set quantity if you have more than one item for sale.' => 'Ange nummer om du har mer än en kopia till salu.',
    'Indicate what you are selling' => 'Ange vad du säljer',
    'Setting that shipping is possible means that you can send product to other cities via courier service.' => 'Anger att leveransen är möjligt, bekräftar du att du kan skicka varor till andra städer via budfirma.',
    'Shipping details' => 'Leverans detalj',
    'Shipping type' => 'Leverans typ',
    'Show Customer`s Contacts' => 'Kontakter för att visa köparen',
    'Show Seller\'s Contacts' => 'För att kontakta säljaren',
    'Specialties' => 'Specialitet',
    'Status' => 'Status',
    'Subcategory ID' => 'ID underkategorin',
    'Submit an order for the product and hundreds of sellers will see your request.' => 'Lägga ett Bud på en produkt som du vill köpa och hundratals säljare kommer att se din begäran.',
    'Submit product' => 'Skicka ansökan',
    'Terms of delivery' => 'Leveransvillkor',
    'If you select more languages, more clients  in different countries will be able to see your offer and thus increase your profits. And if you want to order services or buy products you will have more options to choose from.' => 'Ju fler språk du anger, desto fler kunder i olika länder kommer att kunna se ditt erbjudande som kommer att förbättra ditt resultat',
    'To homepage' => 'På main',
    'To respond' => 'För att svara',
    'In this field, try to describe your product in as much detail as possible. The more detailed your description is, the more likely it is that buyers will choose your offer.' => 'I detta område, försök att beskriva ditt objekt så detaljerat som möjligt. Den mer detaljerade beskrivningen är, desto högre är chansen att köparen kommer att välja ditt erbjudande.',
    'Try to make it clear from the title what you are selling. You can use up to 80 characters.' => 'Försök att från namnet är det klart att du säljer. Du kan använda upp till 80 tecken.',
    'Type' => 'Typ',
    'Update' => 'Uppdatering',
    'Update {modelClass}: ' => 'För att uppdatera {modelClass}: ',
    'Updated At' => 'Ändring av datum',
    'Used' => 'B/u',
    'Unavailable' => 'Inte tillgänglig',
    'Value' => 'Värdet',
    'Views' => 'Visningar',
    'Views Count' => 'Antalet visningar',
    'Warning!' => 'Uppmärksamhet!',
    'We offer two options for sale: through the service or directly. If you sell through the service, we provide a Safe Transaction, which means that you will receive {percent}% of the value that you specified and the money will be received only after your buyer receives the goods that you sell and confirm receipt.' => 'Vi erbjuder två alternativ för försäljning: via tjänsten eller direkt. Om du säljer via tjänsten, vi ger Säker transaktion, det innebär att du kommer att få {percent}% av det värde som du har angett och pengarna kommer att levereras till dig först efter att köparen tar emot varor som du säljer och bekräfta mottagandet.',
    'Welcome to uJobs. You must confirm that you are adult to gain access for this section.' => 'Välkommen till uJobs. För tillgång måste du bekräfta ålder.',
    'What is your budget for this request?' => 'Vad är din budget för det här programmet?',
    'Where will your request be?' => 'Om din ansökan kommer att bo på?',
    'Width' => 'Bredd',
    'You can select multiple languages' => 'Du kan välja flera språk',
    'You have to specify product subcategory' => 'Välj en underkategori',
    'You may add a photo to your request. This will make your request more attractive.' => 'Du kan lägga till bilder till din begäran. Detta kommer att göra din ansökan mer attraktiv.',
    'Your address' => 'Din adress',
    'Your product`s size. These fields are required if shipping is available for your product. You may leave these fields empty is only pickup is available.' => 'Måtten på din produkt. Dessa fält måste vara ifyllda om din produkt leverans är möjlig. Du kan lämna fälten tomma om du bara har en pickup.',
    'cm' => 'cm',
    'Adult Signature Required: You must be at least 21 years of age to purchase this product. Adult signature is required on delivery.' => 'Bekräftelse krävs: att köpa detta objekt måste du vara minst 21 år gammal. Bekräftelse sker vid leverans.',
    'Recommendation:' => 'Rekommendation:',
    'We recommend to set your price. Requests with contract price receive responses 7 times less than requests with exact price.' => 'Vi rekommenderar dig att ställa in exakt pris. Anbuden med de avtalade priset får ett svar i 7 gånger mindre än budet med det angivna priset.',
    'item' => 'enhet av produkt',
    'pc' => 'St',
    'In stock' => 'Finns i lager',
    'How much are you ready to pay for this product?' => 'Hur mycket skulle du betala för den här produkten?',
    'Set quantity if you want to buy more than one item.' => 'Ange nummer om du vill beställa mer än ett objekt',
    'Specify what you will pay for. Whether it is product item or a certain unit of measure.' => 'Ange vad du kommer att betala. För den enhet av produkten eller någon produkt.',
    'Specify what your buyers will pay for. Whether it is product item or a certain unit of measure.' => 'Ange vad som kommer att betala din köpare. För den enhet av produkten eller någon produkt.',
    'Required' => 'Krävs',
    '{price} per {amount} {measure}' => '{price} {amount} {measure}',
    'Enter address' => 'Ange adress',
];