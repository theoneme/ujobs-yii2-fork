<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.10.2017
 * Time: 12:15
 */

namespace common\modules\board\assets;

use yii\web\AssetBundle;

/**
 * Class AdultAsset
 * @package common\modules\board\assets
 */
class AdultAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/board/web';
    public $css = [
        'css/adult.css'
    ];
    public $depends = [
        '\frontend\assets\CommonAsset'
    ];
}
