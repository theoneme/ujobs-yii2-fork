<?php

namespace common\modules\board\assets;

use yii\web\AssetBundle;

/**
 * Class ProductAmountAsset
 * @package common\modules\board\assets
 */
class ProductAmountAsset extends AssetBundle
{
	public $sourcePath = '@common/modules/board/web';
	public $css = [
		'css/amount.css'
	];
	public $js = [
        'js/amount.js'
	];
	public $depends = [
        'frontend\assets\CatalogAsset'
	];
}
