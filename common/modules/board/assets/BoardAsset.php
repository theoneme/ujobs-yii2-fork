<?php

namespace common\modules\board\assets;

use yii\web\AssetBundle;

/**
 * Class BoardAsset
 * @package common\modules\board\assets
 */
class BoardAsset extends AssetBundle
{
	public $sourcePath = '@common/modules/board/web';
	public $css = [
		'css/style.css'
	];
	public $js = [

	];
	public $depends = [
        'frontend\assets\CatalogAsset'
	];
}
