<?php

namespace common\modules\board\models;

use common\models\SeoAdvanced;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeDescription;
use common\modules\attribute\models\AttributeValue;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\services\elastic\ProductAttributeElasticService;
use Yii;
use yii\db\ActiveQuery;
use yii\elasticsearch\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "product_attribute".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $product_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property Product $product
 * @property Attribute $attr
 * @property AttributeValue $attrValue
 * @property AttributeDescription $attrDescription
 * @property AttributeDescription $attrValueDescription
 * @property AttributeDescription[] $attrDescriptions
 * @property AttributeDescription[] $attrValueDescriptions
 */
class ProductAttribute extends \yii\db\ActiveRecord
{
    public $relatedCount;

    /**
     * @var SeoAdvanced|null
     */
    public $_meta = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'product_id'], 'integer'],
            ['value', 'integer'],
            [['locale'], 'required'],
            [['locale'], 'string', 'max' => 5],
            [['entity_alias'], 'string', 'max' => 55],
            [['value_alias'], 'string', 'max' => 75],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('board', 'ID'),
            'attribute_id' => Yii::t('board', 'Attribute'),
            'product_id' => Yii::t('board', 'Product ID'),
            'value' => Yii::t('board', 'Value'),
            'locale' => Yii::t('board', 'Locale'),
            'entity_alias' => Yii::t('board', 'Entity Alias'),
            'value_alias' => Yii::t('board', 'Value Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrValueDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'id'])->via('attrValue')->indexBy('locale');
        $relation->from('mod_attribute_description mad_2');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescription()
    {
        $relation = $this->hasOne(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1');

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrDescriptions()
    {
        $relation = $this->hasMany(AttributeDescription::class, ['entity_content' => 'attribute_id']);

        $relation->andWhere(['mad_1.entity' => 'attribute']);
        $relation->from('mod_attribute_description mad_1')->indexBy('locale');

        return $relation;
    }

    /**
     * @param bool $doSave
     * @return bool|ActiveRecord
     */
    public function updateElastic($doSave = true)
    {
        $productAttributeElasticService = new ProductAttributeElasticService($this);
        return $productAttributeElasticService->process($doSave);
    }

    /**
     * @param array $queryParams
     * @return array|SeoAdvanced|null|\yii\db\ActiveRecord
     */
    public function getMeta($queryParams = [])
    {
        if ($this->_meta === null) {
            $this->_meta = SeoAdvanced::find()->where(['entity' => 'product-tag', 'entity_content' => $this->value_alias])->one();
            if ($this->_meta !== null) {
                $data = json_decode($this->_meta->custom_data, true);
                $data = is_array($data) ? $data : [];
                $data['title'] = Html::encode(preg_replace("/[,.;]/", "", $this->getTitle()));

                $data['city'] = $data['attributes'] = '';

                $cityParam = ArrayHelper::remove($queryParams, 'city');
                if($cityParam !== null) {
                    /** @var AttributeValue $city */
                    $city = AttributeValue::find()->where(['attribute_id' => 42, 'alias' => $cityParam])->one();

                    $data['city'] = $city ? Yii::t('seo', ' in {city}', ['city' => $city->getTitle()]) : '';
                }

                $this->_meta->applyTemplates($data);
            }
        }

        return $this->_meta;
    }

    /**
     * Get attribute's title
     */
    public function getTitle()
    {
        return $this->attrValueDescriptions[Yii::$app->language]->title ?? array_pop($this->attrValueDescriptions)->title;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
//        $this->updateElastic();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            ProductAttributeElastic::deleteAll(['id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }
}
