<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.07.2017
 * Time: 16:33
 */

namespace common\modules\board\models\frontend;

use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\Category;
use common\models\ContentTranslation;
use common\models\DynamicForm;
use common\models\Measure;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\board\models\Product;
use common\modules\board\models\ProductAttribute;
use common\modules\store\models\Currency;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class PostProductForm
 * @package common\modules\board\frontend\models
 * @property Product $product
 */
class PostProductForm extends Model
{
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var integer
     */
    public $subcategory_id;
    /**
     * @var integer
     */
    public $price;
    /**
     * @var int
     */
    public $quantity = 1;
    /**
     * @var string
     */
    public $shipping_info;
    /**
     * @var integer
     */
    public $length = null;
    /**
     * @var integer
     */
    public $width = null;
    /**
     * @var integer
     */
    public $height = null;
    /**
     * @var string
     */
    public $lat = null;
    /**
     * @var string
     */
    public $long = null;
    /**
     * @var string
     */
    public $address = null;
    /**
     * @var
     */
    public $shipping_type;
    /**
     * @var bool
     */
    public $is_new = true;
    /**
     * @var integer
     */
    public $execution_time;
    /**
     * @var integer
     */
    public $amount = 1;
    /**
     * @var integer
     */
    public $measure_id;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var string
     */
    public $currency_code = 'RUB';
    /**
     * @var string
     */
    public $requirements = null;
    /**
     * @var DynamicForm
     */
    public $dynamic_form = null;
    /**
     * @var Product
     */
    private $_product;
    /**
     * @var array
     */
    private $_productAttributes = null;
    /**
     * @var array
     */
    private $_attachments = null;

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_DRAFT = 'draft';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DRAFT] = [
            'locale',
            'title',
            'description',
            'currency_code',
            'parent_category_id',
            'category_id',
            'subcategory_id',
            'price',
            'quantity',
            'is_new',
            'shipping_info',
            'length',
            'width',
            'height',
            'lat',
            'long',
            'address',
            'shipping_type',
            'amount',
            'measure_id'
        ];
        $scenarios[self::SCENARIO_DEFAULT] = [
            'locale',
            'title',
            'description',
            'currency_code',
            'parent_category_id',
            'category_id',
            'subcategory_id',
            'price',
            'quantity',
            'is_new',
            'shipping_info',
            'length',
            'width',
            'height',
            'lat',
            'long',
            'address',
            'shipping_type',
            'amount',
            'measure_id'
        ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'categoryRequired' => [['category_id', 'parent_category_id'], 'required'],
            'categoryInteger' => [['category_id', 'parent_category_id'], 'integer'],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            [['description', 'shipping_info'], 'string', 'max' => 65535],
            ['locale', 'string'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
            ['currency_code', 'validateCurrency'],
            [['price', 'quantity', 'length', 'width', 'height'], 'integer'],
            [['is_new'], 'boolean'],
            [['lat', 'long', 'address'], 'string'],
            ['price', 'required'],
            ['description', 'required'],
            ['price', 'integer', 'min' => 1, 'when' => function ($model) {
                return in_array($model->currency_code, ['USD', 'EUR']);
            }],
            ['price', 'integer', 'min' => 100, 'when' => function ($model) {
                return in_array($model->currency_code, ['RUB', 'UAH']);
            }],
            ['subcategory_id', 'validateSubcategory', 'skipOnEmpty' => false],
            [['amount'], 'required'],
            ['amount', 'default', 'value' => 1],
            ['amount', 'number', 'min' => 1],
            [['measure_id'], 'exist', 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if (!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateSubcategory($attribute, $params)
    {
        $category = Category::getDb()->cache(function ($db) {
            return Category::findOne($this->category_id);
        });

        if ($category->rgt - $category->lft > 1 && !$this->subcategory_id) {
            $this->addError($attribute, Yii::t('board', 'You have to specify product subcategory'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('board', 'Product Name'),
            'category_id' => Yii::t('board', 'Category'),
            'parent_category_id' => Yii::t('board', 'Category'),
            'description' => Yii::t('board', 'Description'),
            'quantity' => Yii::t('board', 'Quantity'),
            'shipping_type' => Yii::t('board', 'Options of shipping'),
            'shipping_info' => Yii::t('board', 'Details of shipping'),
            'address' => Yii::t('board', 'Address'),
            'width' => Yii::t('board', 'Width'),
            'length' => Yii::t('board', 'Length'),
            'height' => Yii::t('board', 'Height')
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate() || !$this->dynamic_form->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->product->load($this->attributes, '');
//        $this->product->attributes = $this->attributes;

        if ($this->scenario == self::SCENARIO_DRAFT) {
            $this->product->status = Product::STATUS_DRAFT;
        }

        $isSaved = $this->product->save();

        $transaction->commit();

        if ($isSaved && $this->scenario != self::SCENARIO_DRAFT) {
            if ($this->lat && $this->long && $this->address) {
                if (!AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->exists()) {
                    (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $this->address]))->save();
                }
            }
            $this->product->defineStatus();
            $this->product->updateElastic();
        }

        return $isSaved;
    }

    /**
     * @return array|mixed
     */
    public function getProductAttributes()
    {
        if ($this->_productAttributes === null) {
            if ($this->product->isNewRecord) {
                $this->_productAttributes = [];
            } else {
                $this->_productAttributes = $this->product->extra;
            }
        }

        return $this->_productAttributes;
    }

    /**
     * @return array|null
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->product->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->product->attachments;
            }
        }

        return $this->_attachments;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->_product = $product;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);
        $category = Category::findOne(!empty($this->subcategory_id) ? $this->subcategory_id : $this->category_id);

        if ($category !== null) {
            $dynamicAttributes = DynamicForm::getAttributesListByCategory($category);
            $this->dynamic_form = DynamicForm::initModelFromGroup($dynamicAttributes, []);
            $this->dynamic_form->load($input);
        }

        if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
            foreach ($input['Attachment'] as $key => $item) {
                $attachmentObject = new Attachment();
                $attachmentObject->setAttributes($item);
                $attachmentObject->entity = 'product';

                $this->product->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
            }
        }

        if ($this->dynamic_form !== null) {
            $this->bindProductAttributes($this->dynamic_form);
        }

        $translationObject = new ContentTranslation([
            'title' => $this->title,
            'content' => $this->description,
            'locale' => Yii::$app->language,
            'entity' => 'product',
        ]);
        $this->product->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;

        $shippingTypeAttribute = Attribute::findOne(['alias' => 'shipping_type']);
        if ($shippingTypeAttribute !== null && $this->shipping_type) {
            $value = AttributeValue::findOne(['attribute_id' => $shippingTypeAttribute->id, 'id' => $this->shipping_type]);
            $this->product->bind('productAttributes')->attributes = [
                'attribute_id' => $shippingTypeAttribute->id,
                'is_text' => false,
                'value' => $this->shipping_type,
                'locale' => Yii::$app->language,
                'entity_alias' => 'shipping_type',
                'value_alias' => $value->alias
            ];
        }
    }

    /**
     * @return array
     */
    public function getShippingTypes()
    {
        $shippingAttribute = Attribute::find()->joinWith(['values.translations'])->where(['mod_attribute.alias' => 'shipping_type'])->one();
        return ArrayHelper::map($shippingAttribute->values, 'id', function ($value) {
            return $value->translations[Yii::$app->language] ? $value->translations[Yii::$app->language]->title : array_values($value->translations)[0]->title;
        });
    }

    /**
     * @param $id
     */
    public function loadProduct($id)
    {
        $this->product = Product::findOne((int)$id);
        $this->attributes = $this->product->attributes;
        $this->title = $this->product->translation->title;
        $this->description = $this->product->translation->content;

        $this->shipping_type = ProductAttribute::find()->where(['product_id' => $this->product->id, 'entity_alias' => 'shipping_type'])->select('value')->scalar();
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->product->validateWithRelations()
        );
    }

    /**
     * @param DynamicForm $dynamicForm
     */
    private function bindProductAttributes(DynamicForm $dynamicForm)
    {
        foreach ($dynamicForm->attributes as $key => $attribute) {
            if (!$attribute) continue;

            $parts = explode('_', $key);

            if (is_array($attribute)) {
                $values = $attribute;
            } else {
                if(!$dynamicForm->isMultiValueField($key)) {
                    $values = (array)$attribute;
                } else {
                    $valueParts = array_filter(explode(',', $attribute));
                    $values = $valueParts;
                }
            }

            foreach($values as $pa) {
                $productAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(ProductAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                if($productAttributeObject !== null) {
                    $this->product->bind('productAttributes', $productAttributeObject->id)->attributes = $productAttributeObject->attributes;
                }
            }
        }
    }
}