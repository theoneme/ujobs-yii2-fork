<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.08.2017
 * Time: 16:09
 */

namespace common\modules\board\models\frontend;

use common\components\CurrencyHelper;
use common\models\Attachment;
use common\models\Category;
use common\models\ContentTranslation;
use common\models\Measure;
use common\modules\board\models\Product;
use common\modules\store\models\Currency;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PostRequestForm
 * @package common\modules\board\models\frontend
 */
class PostRequestForm extends Model
{
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var integer
     */
    public $subcategory_id;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var boolean
     */
    public $contract_price;
    /**
     * @var integer
     */
    public $price;
    /**
     * @var integer
     */
    public $amount = 1;
    /**
     * @var integer
     */
    public $measure_id;
    /**
     * @var Product
     */
    private $_product;
    /**
     * @var array
     */
    private $_attachments = null;
    /**
     * @var string
     */
    public $currency_code = 'RUB';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'categoryRequired' => [['category_id', 'parent_category_id'], 'required'],
            'categoryInteger' => [['category_id', 'parent_category_id'], 'integer'],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            ['description', 'string', 'max' => 65535],
            'descriptionRequired' => ['description', 'required'],
            ['price', 'validatePrice', 'skipOnEmpty' => false],
            'contractPrice' => ['contract_price', 'boolean'],
            ['currency_code', 'validateCurrency'],
            ['subcategory_id', 'validateSubcategory', 'skipOnEmpty' => false],
            [['amount'], 'required'],
            ['amount', 'default', 'value' => 1],
            ['amount', 'number', 'min' => 1],
            [['measure_id'], 'exist', 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if(!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validatePrice($attribute, $params)
    {
        if (!is_numeric($this->price) && $this->price) {
            $this->addError($attribute, Yii::t('yii', '{attribute} must be a number.', ['attribute' => $this->getAttributeLabel($attribute)]));
        }

        if (CurrencyHelper::convert('RUB', $this->currency_code, 200) > $this->price && !$this->contract_price) {
            $this->addError($attribute, Yii::t('app', 'Price must be greater than {0}', [CurrencyHelper::convert('RUB', $this->currency_code, 200)]));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateSubcategory($attribute, $params)
    {
        $category = Category::getDb()->cache(function ($db) {
            return Category::findOne($this->category_id);
        });

        if ($category->rgt - $category->lft > 1 && !$this->subcategory_id) {
            $this->addError($attribute, Yii::t('board', 'You have to specify product subcategory'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('board', 'Product Name'),
            'category_id' => Yii::t('board', 'Category'),
            'parent_category_id' => Yii::t('board', 'Category'),
            'subcategory_id' => Yii::t('board', 'Category'),
            'type' => Yii::t('board', 'Type'),
            'specialties' => Yii::t('board', 'Specialties'),
            'description' => Yii::t('board', 'Description'),
            'price' => Yii::t('board', 'Price'),
            'percent_bonus' => Yii::t('board', 'Percent bonus'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $valid = parent::beforeValidate();
        $this->price = str_replace(' ', '', $this->price);
        if (empty($this->price)) {
            $this->contract_price = true;
        }
        return $valid;
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $this->product->attributes = $this->attributes;
        $this->product->locale = Yii::$app->language;
        $this->product->type = Product::TYPE_TENDER;
        $this->product->status = Product::STATUS_REQUIRES_MODERATION;
        if($this->product->save()) {
            $transaction->commit();

            $this->product->updateElastic();
            $this->product->profile->updateElastic();

            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @return array|Attachment[]
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->product->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->product->attachments;
            }
        }
        return $this->_attachments;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->_product = $product;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);

        if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
            foreach ($input['Attachment'] as $key => $item) {
                $attachmentObject = new Attachment();
                $attachmentObject->setAttributes($item);
                $attachmentObject->entity = Product::TYPE_PRODUCT;

                $this->product->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
            }
        }

        $translationObject = new ContentTranslation([
            'title' => $this->title,
            'content' => $this->description,
            'locale' => Yii::$app->language,
            'entity' => Product::TYPE_PRODUCT,
        ]);
        $this->product->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;
    }

    /**
     * @param $id
     */
    public function loadProduct($id)
    {
        $this->product = Product::findOne((int)$id);
        $this->attributes = $this->product->attributes;
        $this->title = $this->product->translation->title;
        $this->description = $this->product->translation->content;
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->product->validateWithRelations()
        );
    }
}