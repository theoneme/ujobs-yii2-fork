<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.07.2017
 * Time: 14:05
 */

namespace common\modules\board\models\elastic;

use common\components\elasticsearch\ActiveRecord;

/**
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $product_id
 * @property string $value
 * @property string $locale
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property array $product
 * @property array $attribute
 * @property array $attributeValue
 */
class ProductAttributeElastic extends ActiveRecord
{
    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'ujobs';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'product_attribute';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'long'],
                    'attribute_id' => ['type' => 'integer'],
                    'value' => ['type' => 'string'],
                    'entity_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'value_alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'locale' => ['type' => 'string', 'index' => 'not_analyzed'],
                    'product' => [
                        'type' => 'object',
                        'properties' => [
                            'id' => ['type' => 'long'],
                            'category_id' => ['type' => 'long'],
                            'status' => ['type' => 'long'],
                            'type' => ['type' => 'string'],
                            'currency_code' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'default_price' => ['type' => 'long'],
                            'price' => ['type' => 'long'],
                            'title' => ['type' => 'string'],
                            'description' => ['type' => 'string'],
                            'user_id' => ['type' => 'long', 'index' => 'not_analyzed']
                        ]
                    ],
                    'attribute' => [
                        'type' => 'object',
                        'properties' => [
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                        ]
                    ],
                    'attributeValue' => [
                        'type' => 'object',
                        'properties' => [
                            'alias' => ['type' => 'string', 'index' => 'not_analyzed'],
                            'status' => ['type' => 'long'],
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'attribute_id',
            'value',
            'entity_alias',
            'value_alias',
            'is_text',
            'locale',
            'product',
            'attribute',
            'attributeValue'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }
}
