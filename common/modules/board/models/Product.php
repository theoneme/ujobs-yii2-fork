<?php

namespace common\modules\board\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use common\components\CurrencyHelper;
use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\Category;
use common\models\ContentTranslation;
use common\models\Measure;
use common\models\Notification;
use common\models\SeoAdvanced;
use common\models\UserFavouriteProduct;
use common\models\UserFriend;
use common\modules\board\models\elastic\ProductAttributeElastic;
use common\modules\board\models\elastic\ProductElastic;
use common\modules\board\services\elastic\ProductElasticService;
use common\modules\store\models\CartItemInterface;
use common\modules\store\models\Currency;
use common\models\user\Profile;
use common\models\user\User;
use common\services\GoogleMapsService;
use Yii;
use common\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $type
 * @property integer $price
 * @property string $currency_code
 * @property integer $contract_price
 * @property integer $parent_category_id
 * @property integer $category_id
 * @property integer $subcategory_id
 * @property integer $click_count
 * @property integer $views_count
 * @property integer $amount
 * @property integer $measure_id
 * @property string $alias
 * @property string $locale
 * @property string $shipping_info
 * @property integer $length
 * @property integer $width
 * @property integer $height
 * @property string $lat
 * @property string $long
 * @property string $address
 * @property boolean $secure
 *
 * @property Category $category
 * @property Currency $currencyCode
 * @property Category $parentCategory
 * @property User $user
 * @property Profile $profile
 * @property Measure $measure
 * @property ProductAttribute[] $productAttributes
 * @property ContentTranslation $translation
 * @property ContentTranslation[] $translations
 * @property UserFavouriteProduct[] $favourites
 * @property Attachment[] $attachments
 *
 * @method bind($relationName = null, $id = null, $index = null)
 * @method bindAttachment($id = null)
 * @method validateWithRelations()
 */
class Product extends ActiveRecord implements CartItemInterface
{
    const STATUS_DRAFT = -50;
    const STATUS_DENIED = -10;
    const STATUS_REQUIRES_MODERATION = 10;
    const STATUS_REQUIRES_MODIFICATION = 15;
    const STATUS_DISABLED = 20;
    const STATUS_PAUSED = 25;
    const STATUS_ACTIVE = 30;
    const STATUS_DELETED = 100;

    const TYPE_PRODUCT = 'product';
    const TYPE_TENDER = 'tender';

    /**
     * @var string
     */
    private $_address = null;

    /**
     * @var string
     */
    public $title = null;

    /**
     * Determines where BlameableBehavior should be used
     * @var bool
     */
    public $isBlameable = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        $behaviors = [
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'productAttributes'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'product'
            ],
        ];
        if ($this->isBlameable) {
            $behaviors[] = [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ];
        }
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locale'], 'required'],
            [['user_id', 'created_at', 'updated_at', 'status', 'price', 'contract_price', 'parent_category_id', 'category_id', 'subcategory_id', 'click_count', 'views_count', 'length', 'width', 'height', 'external_id'], 'integer'],
            [['price'], 'default', 'value' => 0],
            [['lat', 'long'], 'string'],
            [['currency_code'], 'string', 'max' => 3],
            [['alias', 'title'], 'string', 'max' => 125],
            [['locale'], 'string', 'max' => 5],
            [['secure'], 'boolean'],
            ['type', 'default', 'value' => self::TYPE_PRODUCT],
            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION],
            ['secure', 'default', 'value' => true],
            ['shipping_info', 'string', 'max' => 512],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['parent_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['parent_category_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['subcategory_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['amount', 'required'],
            ['amount', 'default', 'value' => 1],
            ['amount', 'number', 'min' => 1],
            [['measure_id'], 'exist', 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('board', 'ID'),
            'user_id' => Yii::t('board', 'User ID'),
            'created_at' => Yii::t('board', 'Created At'),
            'updated_at' => Yii::t('board', 'Updated At'),
            'status' => Yii::t('board', 'Status'),
            'price' => Yii::t('board', 'Price'),
            'currency_code' => Yii::t('board', 'Currency Code'),
            'contract_price' => Yii::t('board', 'Contract Price'),
            'parent_category_id' => Yii::t('board', 'Parent Category ID'),
            'category_id' => Yii::t('board', 'Category ID'),
            'subcategory_id' => Yii::t('board', 'Subcategory ID'),
            'click_count' => Yii::t('board', 'Click Count'),
            'views_count' => Yii::t('board', 'Views Count'),
            'alias' => Yii::t('board', 'Alias'),
            'locale' => Yii::t('board', 'Locale'),
            'length' => Yii::t('board', 'Length'),
            'width' => Yii::t('board', 'Width'),
            'height' => Yii::t('board', 'Height'),
            'lat' => Yii::t('board', 'Latitude'),
            'long' => Yii::t('board', 'Longitude'),
            'address' => Yii::t('board', 'Address'),
        ];
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasAccess($id)
    {
        return Yii::$app->user->isGuest ? false : Product::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'id' => $id])->exists();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        if (!empty($this->subcategory_id)) {
            return $this->hasOne(Category::class, ['id' => 'subcategory_id']);
        } else {
            return $this->hasOne(Category::class, ['id' => 'category_id']);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        $relation = $this->hasOne(ContentTranslation::class, ['entity_id' => 'id'])
            ->from('content_translation ct')
            ->andOnCondition(['ct.entity' => 'product']);
//            ->andOnCondition(['ct.locale' => Yii::$app->language]);

        return $relation;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentTranslation::class, ['entity_id' => 'id'])->andWhere(['content_translation.entity' => 'product'])->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtra()
    {
        return $this->hasMany(ProductAttribute::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(ProductAttribute::class, ['product_id' => 'id'])->where([
            'product_attribute.entity_alias' => 'product_tag',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => 'product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyCode()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'parent_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttribute::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndexedProductAttributes()
    {
        return $this->hasMany(ProductAttribute::class, ['product_id' => 'id'])->indexBy('entity_alias')->andWhere(['not', ['entity_alias' => 'product_tag']]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavourites()
    {
        return $this->hasMany(UserFavouriteProduct::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::class, ['id' => 'measure_id']);
    }

    /**
     * @return null|string
     */
    public function getAddress()
    {
        if ($this->_address === null && $this->lat && $this->long) {
            /* @var $addressTranslation AddressTranslation*/
            $addressTranslation = AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->one();
            if ($addressTranslation !== null) {
                $this->_address = $addressTranslation->title;
            }
            else {
                $gmapsService = new GoogleMapsService();
                $address = $gmapsService->reverseGeocode($this->lat, $this->long, Yii::$app->params['supportedLocales'][Yii::$app->language]);
                if ($address !== false) {
                    (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $address]))->save();
                    $this->_address = $address;
                }
            }
        }
        return $this->_address;
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_REQUIRES_MODERATION => ($colored ? '<span style="color: #2d618c">' . Yii::t('labels', 'Requires moderation') . '</span>' : Yii::t('labels', 'Requires moderation')),
            self::STATUS_REQUIRES_MODIFICATION => ($colored ? '<span style="color: #ffd288">' . Yii::t('labels', 'Requires modification') . '</span>' : Yii::t('labels', 'Requires modification')),
            self::STATUS_ACTIVE => ($colored ? '<span style="color: #3ab845">' . Yii::t('labels', 'Published') . '</span>' : Yii::t('labels', 'Published')),
            self::STATUS_PAUSED => ($colored ? '<span style="color: #000000">' . Yii::t('labels', 'Paused') . '</span>' : Yii::t('labels', 'Paused')),
            self::STATUS_DENIED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Denied') . '</span>' : Yii::t('labels', 'Denied')),
            self::STATUS_DISABLED => ($colored ? '<span style="color: #ac4137">' . Yii::t('labels', 'Disabled') . '</span>' : Yii::t('labels', 'Disabled')),
            self::STATUS_DRAFT => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Draft') . '</span>' : Yii::t('labels', 'Draft')),
            self::STATUS_DELETED => ($colored ? '<span style="color: #7a7c7c">' . Yii::t('labels', 'Deleted') . '</span>' : Yii::t('labels', 'Deleted')),
        ];
    }

    /**
     * @param bool $colored
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels($colored);
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @return array
     */
    public function getDefaultSeo()
    {
        $templateCategory = $this->type === Product::TYPE_PRODUCT ? SeoAdvanced::TEMPLATE_PRODUCT : SeoAdvanced::TEMPLATE_PRODUCT_TENDER;
        $templates = SeoAdvanced::getDefaultTemplates($templateCategory);
        if ($templateCategory === SeoAdvanced::TEMPLATE_PRODUCT_TENDER) {
            $price = $this->contract_price
                ? Yii::t('seo', 'at the contract price', [], $this->locale)
                : Yii::t('seo', 'at the price {price}', ['price' => $this->getPrice()], $this->locale);
            $price2 = $this->contract_price ? Yii::t('seo', 'Contract', [], $this->locale) : $this->getPrice();
        } else {
            $price2 = $price = $this->getPrice();
        }
        $params = [
            'title' => $this->translation->title,
            'Title' => implode(' ', array_map(function($var){return Yii::$app->utility->upperFirstLetter($var);}, explode(' ', $this->translation->title))),
            'description' => StringHelper::truncate(strip_tags($this->translation->content), 255),
            'price' => $price,
            'Price' => $price2,
            'username' => $this->getSellerName()
        ];

        return [
            'title' => Yii::t('seo', $templates['title'], $params, $this->locale),
            'description' => Yii::t('seo', $templates['description'], $params, $this->locale),
            'keywords' => Yii::t('seo', $templates['title'], $params, $this->locale)
        ];
    }

    /**
     * @return bool
     */
    public function isFavourite()
    {
        return !isGuest() && isset(Yii::$app->user->identity->favouriteProducts[$this->id]);
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->created_at > (time() - 60 * 60 * 24 * 2);
    }

    /**
     *
     */
    public function defineStatus()
    {
        if (in_array($this->status, [self::STATUS_ACTIVE, self::STATUS_REQUIRES_MODERATION, self::STATUS_REQUIRES_MODIFICATION])) {
            $letters = 'a-zA-Zа-яА-Яა-ჸєЄіІїЇёЁÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüŸ¡¿çÇŒœßØøÅåÆæÞþÐð';
            $pattern = '/^(\d{0,8}|[' . $letters . ']+)([\s\.\?,!;:\-#№%\*\(\)\"\'\/\\=+~<>–—✔“”]+(\d{0,8}|[' . $letters . ']+)){2,}[\s\.!\?]*$/u';
            if (count($this->attachments)) {
                if (!empty(strip_tags($this->translation->content)) && preg_match($pattern, strip_tags($this->translation->content))) {
                    if ($this->status != self::STATUS_ACTIVE) {
                        $this->sendModerationNotification();
                    }
                    $this->updateAttributes(['status' => self::STATUS_ACTIVE]);
                } else {
                    $this->updateAttributes(['status' => self::STATUS_REQUIRES_MODERATION]);
                }
            } else {
                $this->updateAttributes(['status' => self::STATUS_REQUIRES_MODIFICATION]);
            }
        }
    }

    /**
     * @return bool
     */
    public function sendModerationNotification()
    {
        switch ($this->type) {
            case self::TYPE_TENDER:
                $message = Yii::t('notifications', "Your product request has passed moderation and has been added to catalog.", [], $this->user->site_language);
                $subject = Yii::t('notifications', "Your product request has passed moderation on {site}", ['site' => Yii::$app->name], $this->user->site_language);
                $template = Notification::TEMPLATE_PRODUCT_REQUEST_MODERATED;
                $route = ['/board/tender/view', 'alias' => $this->alias];
                break;
            default:
                $message = Yii::t('notifications', "Your product has passed moderation and has been added to catalog.", [], $this->user->site_language);
                $subject = Yii::t('notifications', "Your product has passed moderation on {site}", ['site' => Yii::$app->name], $this->user->site_language);
                $template = Notification::TEMPLATE_PRODUCT_MODERATED;
                $route = ['/board/product/view', 'alias' => $this->alias];
        }

        $notification = new Notification([
            'to_id' => $this->user_id,
            'params' => [
                'subject' => $subject,
                'content' => $message,
            ],
            'template' => $template,
            'custom_data' => json_encode([
                'product' => $this->translation->title,
            ]),
            'linkRoute' => $route,
            'withEmail' => true,
            'is_visible' => true
        ]);

        return $notification->save();
    }

    /**
     * @param bool $doSave
     * @return bool|ProductElastic
     */
    public function updateElastic($doSave = true)
    {
        $productElasticService = new ProductElasticService($this);

        return $productElasticService->process($doSave);
    }

    /**
     * @return array
     */
    public function formatAttributes()
    {
        $exceptions = ['product_tag'];

        $result = [];
        $attributes = $this->productAttributes;
        if (!empty($attributes)) {
            foreach ($attributes as $key => $attribute) {
                if (!in_array($attribute->entity_alias, $exceptions)) {
                    $translation = $attribute->attrDescriptions[Yii::$app->language] ?? array_values($attribute->attrDescriptions)[0];
                    $valueTranslation = $attribute->attrValueDescriptions[Yii::$app->language] ?? array_values($attribute->attrValueDescriptions)[0];
                    if (array_key_exists($translation->title, $result)) {
                        $result[$translation->title] = (array)$result[$translation->title];
                        $result[$translation->title][] = Html::encode($valueTranslation->title);
                    } else {
                        $result[$translation->title] = Html::encode($valueTranslation->title);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (!$this->alias) {
                    $title = $this->title;

                    $truncatedTitle = BaseStringHelper::truncateWords($title, 5, '');
                    $truncatedTitle = preg_replace('/\s+/', ' ', $truncatedTitle);

                    $this->alias = Yii::$app->utility->transliterate($truncatedTitle) . '-' . uniqid();
                }
            }

            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->status === self::STATUS_ACTIVE && array_key_exists('status', $changedAttributes) && $changedAttributes['status'] !== self::STATUS_ACTIVE) {
            /** @var UserFriend[] $friends */
            $friends = $this->user->friends;
            if (!empty($friends)) {
                $template = $this->type === self::TYPE_PRODUCT ? Notification::TEMPLATE_FRIEND_POSTED_PRODUCT : Notification::TEMPLATE_FRIEND_POSTED_PRODUCT_REQUEST;
                foreach ($friends as $friend) {
                    $notification = new Notification([
                        'to_id' => $friend->target_user_id,
                        'from_id' => $this->user_id,
                        'thumb' => $this->getThumb('catalog'),
                        'custom_data' => json_encode([
                            'user' => $this->user->getSellerName(),
                        ]),
                        'template' => $template,
                        'subject' => Notification::SUBJECT_YOU_HAVE_NEW_ORDER,
                        'linkRoute' => ["/board/{$this->type}/view", 'alias' => $this->alias],
                        'withEmail' => false,
                        'is_visible' => true
                    ]);

                    $notification->save();
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->attachments as $attachment) {
                $attachment->delete();
            }
            ProductAttributeElastic::deleteAll(['id' => ArrayHelper::getColumn($this->productAttributes, 'id')]);
            ProductAttribute::deleteAll(['product_id' => $this->id]);
            ProductElastic::deleteAll(['id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * interface implementations
     */

    /**
     * @return int|string
     */
    public function getPrice()
    {
        return CurrencyHelper::convertAndFormat($this->currency_code, Yii::$app->params['app_currency_code'], $this->price, true);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return trim(preg_replace('/\s\s+/', ' ', $this->translation->title ?? ''));
    }

    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->translation->content;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(["/board/product/view", 'alias' => $this->alias]);
    }

    /**
     * @return int
     */
    public function getSellerId()
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->user->getSellerUrl();
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->user->getSellerName();
    }

    /**
     * @param null $target
     * @return mixed
     */
    public function getSellerThumb($target = null)
    {
        return $this->user->getThumb($target);
    }

    /**
     * @return null|string
     */
    public function getActiveTariffIcon()
    {
        if ($this->user->activeTariff) {
            return $this->user->activeTariff->tariff->icon;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getActiveTariffTitle()
    {
        if ($this->user->activeTariff) {
            return $this->user->activeTariff->tariff->title;
        }

        return null;
    }

    /**
     *
     */
    public function getRequirements()
    {
        // TODO: Implement getRequirements() method.
    }

    /**
     * @param null $target
     * @param bool $fromAws
     * @return string
     */
    public function getThumb($target = null, $fromAws = true)
    {
        $image = '/images/new/locale/' . Yii::$app->language . '/no-image.png';
        if (!empty($this->attachments) && file_exists(Yii::getAlias("@frontend") . "/web" . $this->attachments[0]['content'])) {
            $image = $this->attachments[0]->content;
        }

        if ($fromAws === false) {
            return $image;
        }

        $width = $height = null;

        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return Yii::$app->mediaLayer->tryLoadFromAws($image, $width, $height);
    }
}
