<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.05.2017
 * Time: 12:31
 */

namespace common\modules\board\models\history;

use common\modules\store\models\Order;
use common\modules\store\models\OrderHistory;
use frontend\modules\account\models\history\DecisionForm;
use Yii;
use yii\helpers\Json;

/**
 * Class ShippingDecisionForm
 * @package frontend\modules\account\models\history
 */
class ShippingDecisionForm extends DecisionForm
{
    /**
     * @var string
     */
    public $address;

    /**
     * @var string
     */
    public $shipping_type = 'shipping';

    /**
     * @var string
     */
    public $lat;

    /**
     * @var string
     */
    public $long;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = array_merge(parent::rules(), [
            ['address', 'string'],
            ['address', 'required', 'when' => function ($model) {
                return $model->shipping_type === 'shipping';
            }],
            ['shipping_type', 'string']
        ]);

        unset($rules['contentRequired']);
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'address' => Yii::t('model', 'Address')
        ]);
    }

    /**
     * @return bool
     */
    public function save()
    {
        /* @var Order $order */
        $order = Order::findOne($this->order_id);

        if ((hasAccess($order->seller_id) || hasAccess($order->customer_id)) && $this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();

            $history = new OrderHistory();
            $history->attributes = $this->attributes;
            $history->seller_id = $order->seller_id;
            $history->customer_id = $order->customer_id;
            $history->type = $this->t;
            $history->custom_data = Json::encode([
                'type' => $this->shipping_type,
                'address' => $this->address
            ]);
            if ($history->save()) {
                $order->status = $this->h;
                $order->save();

                $this->addNotification($order, $history, $transaction);

                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }
        return false;
    }
}