<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.08.2017
 * Time: 13:35
 */

namespace common\modules\board\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\board\models\Product;
use yii\db\ActiveQuery;

/**
 * ProductSearch represents the model behind the search form about `common\modules\board\models\Product`.
 */
class TenderSearch extends Product
{
    const STATUS_ALL = 'all';
    const STATUS_ACTIVE = 'active';
    const STATUS_REQUIRE_MODERATION = 'require-moderation';
    const STATUS_REQUIRE_MODIFICATION = 'require-modification';
    const STATUS_DISABLED = 'disabled';
    const STATUS_DENIED = 'denied';
    const STATUS_PAUSED = 'paused';
    const STATUS_DRAFT = 'draft';

    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                self::STATUS_ACTIVE,
                self::STATUS_REQUIRE_MODERATION,
                self::STATUS_REQUIRE_MODIFICATION,
                self::STATUS_DISABLED,
                self::STATUS_DENIED,
                self::STATUS_PAUSED,
                self::STATUS_DRAFT,
                self::STATUS_ALL,
            ]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $query = Product::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'type' => Product::TYPE_TENDER]);
        $query = $this->addStatusCondition($query, $this->status);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            self::STATUS_ALL => Yii::t('board', 'All'),
            self::STATUS_ACTIVE => Yii::t('board', 'Active'),
            self::STATUS_REQUIRE_MODERATION => Yii::t('board', 'Awaiting Moderation'),
            self::STATUS_REQUIRE_MODIFICATION => Yii::t('board', 'Requires Details'),
            self::STATUS_DISABLED => Yii::t('board', 'Disabled'),
            self::STATUS_DENIED => Yii::t('board', 'Denied'),
            self::STATUS_PAUSED => Yii::t('board', 'Paused'),
            self::STATUS_DRAFT => Yii::t('board', 'Drafts'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ALL,
            self::STATUS_ACTIVE,
            self::STATUS_REQUIRE_MODERATION,
            self::STATUS_REQUIRE_MODIFICATION,
            self::STATUS_DISABLED,
            self::STATUS_DENIED,
            self::STATUS_PAUSED,
            self::STATUS_DRAFT,
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel($colored = false)
    {
        $labels = static::getStatusLabels();
        return isset($labels[$this->status]) ? $labels[$this->status] : Yii::t('model', 'Unknown status');
    }

    /**
     * @param $status
     * @return int|string
     */
    public function getStatusCount($status)
    {
        $query = Product::find()->where(['user_id' => Yii::$app->user->identity->getCurrentId(), 'type' => Product::TYPE_TENDER]);
        return $this->addStatusCondition($query, $status)->count();
    }

    /**
     * @param $query ActiveQuery
     * @param $status string
     * @return ActiveQuery
     */
    private function addStatusCondition($query, $status)
    {
        switch ($status) {
            case self::STATUS_ALL:
                $query->andWhere(['not', ['status' => Product::STATUS_DELETED]]);
                break;
            case self::STATUS_ACTIVE:
                $query->andWhere(['status' => Product::STATUS_ACTIVE]);
                break;
            case self::STATUS_REQUIRE_MODERATION:
                $query->andWhere(['status' => Product::STATUS_REQUIRES_MODERATION]);
                break;
            case self::STATUS_REQUIRE_MODIFICATION:
                $query->andWhere(['status' => Product::STATUS_REQUIRES_MODIFICATION]);
                break;
            case self::STATUS_DISABLED:
                $query->andWhere(['status' => Product::STATUS_DISABLED]);
                break;
            case self::STATUS_DENIED:
                $query->andWhere(['status' => Product::STATUS_DENIED]);
                break;
            case self::STATUS_PAUSED:
                $query->andWhere(['status' => Product::STATUS_PAUSED]);
                break;
            case self::STATUS_DRAFT:
                $query->andWhere(['status' => Product::STATUS_DRAFT]);
                break;
        }
        return $query;
    }
}
