<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.08.2017
 * Time: 14:30
 */

namespace common\modules\board\models\search;

use common\models\search\UserFinder;
use yii\data\ActiveDataProvider;
use common\modules\board\models\Product;

/**
 * Class TenderSearchAdmin
 * @package common\modules\board\models\search
 */
class TenderSearchAdmin extends Product
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([
            [['status', 'locale', 'username', 'title'], 'safe'],
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = Product::find()->joinWith(['profile', 'user', 'translation'])->andWhere(['type' => Product::TYPE_TENDER]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort'=> ['defaultOrder' => ['status' => SORT_ASC, 'created_at' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['title'] = [
            'asc' => ['title' => SORT_ASC],
            'desc' => ['title' => SORT_DESC],
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->username) {
            $query->andFilterWhere(['product.user_id' => UserFinder::getIdsByName($this->username)]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'product.status' => $this->status,
            'price' => $this->price,
            'contract_price' => $this->contract_price,
            'parent_category_id' => $this->parent_category_id,
            'category_id' => $this->category_id,
            'click_count' => $this->click_count,
            'views_count' => $this->views_count,
        ]);

        $query->andFilterWhere(['like', 'product.currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'product.alias', $this->alias])
            ->andFilterWhere(['like', 'product.locale', $this->locale])
            ->andFilterWhere(['like', 'ct.title', $this->title]);

        return $dataProvider;
    }
}
