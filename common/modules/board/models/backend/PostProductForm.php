<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.07.2017
 * Time: 10:55
 */

namespace common\modules\board\models\backend;

use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\Category;
use common\models\ContentTranslation;
use common\models\DynamicForm;
use common\models\Measure;
use common\modules\attribute\models\Attribute;
use common\modules\attribute\models\AttributeValue;
use common\modules\attribute\services\AttributeValueObjectBuilder;
use common\modules\board\models\Product;
use common\modules\board\models\ProductAttribute;
use common\modules\store\models\Currency;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PostProductForm
 * @package common\modules\board\models\backend
 * @property Product $product
 */
class PostProductForm extends Model
{
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var integer
     */
    public $subcategory_id;
    /**
     * @var integer
     */
    public $price;
    /**
     * @var int
     */
    public $quantity = 1;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var string
     */
    public $shipping_info;
    /**
     * @var string
     */
    public $shipping_type;
    /**
     * @var bool
     */
    public $is_new = true;
    /**
     * @var integer
     */
    public $execution_time;
    /**
     * @var integer
     */
    public $amount = 1;
    /**
     * @var integer
     */
    public $measure_id;
    /**
     * @var string
     */
    public $title = null;
    /**
     * @var string
     */
    public $description = null;
    /**
     * @var string
     */
    public $currency_code = 'RUB';
    /**
     * @var string
     */
    public $requirements = null;
    /**
     * @var string
     */
    public $length;
    /**
     * @var string
     */
    public $width;
    /**
     * @var string
     */
    public $height;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $long;
    /**
     * @var string
     */
    public $lat;
    /**
     * @var Product
     */
    private $_product;
    /**
     * @var array
     */
    private $_productAttributes = null;
    /**
     * @var array
     */
    private $_attachments = null;
    /**
     * @var DynamicForm
     */
    public $dynamic_form = null;

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_DRAFT = 'draft';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DRAFT] = [
            'locale',
            'title',
            'description',
            'currency_code',
            'parent_category_id',
            'category_id',
            'subcategory_id',
            'price',
            'quantity',
            'is_new',
            'shipping_info',
            'shipping_type',
            'status',
            'lat',
            'long',
            'address',
            'length',
            'width',
            'height',
            'amount',
            'measure_id'
        ];
        $scenarios[self::SCENARIO_DEFAULT] = [
            'locale',
            'title',
            'description',
            'currency_code',
            'parent_category_id',
            'category_id',
            'subcategory_id',
            'price',
            'quantity',
            'is_new',
            'shipping_info',
            'shipping_type',
            'status',
            'lat',
            'long',
            'address',
            'length',
            'width',
            'height',
            'amount',
            'measure_id'
        ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'categoryRequired' => [['category_id', 'parent_category_id'], 'required'],
            'categoryInteger' => [['category_id', 'parent_category_id'], 'integer'],
            'descriptionRequired' => ['description', 'required'],
            ['title', 'string', 'max' => 80],
            ['title', 'required'],
            [['description', 'shipping_info'], 'string', 'max' => 65535],
            ['locale', 'string'],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
            ['currency_code', 'validateCurrency'],
            [['price', 'quantity', 'status', 'length', 'width', 'height'], 'integer'],
            [['is_new'], 'boolean'],
            ['price', 'required'],
            [['lat', 'long', 'address'], 'string'],
            ['subcategory_id', 'validateSubcategory', 'skipOnEmpty' => false],
            [['amount'], 'required'],
            ['amount', 'default', 'value' => 1],
            ['amount', 'number', 'min' => 1],
            [['measure_id'], 'exist', 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateCurrency($attribute, $params)
    {
        $availableCurrencies = Currency::getDb()->cache(function ($db) {
            return Currency::find()->select('code')->column();
        });

        if (!in_array($this->currency_code, $availableCurrencies)) {
            $this->addError($attribute, Yii::t('app', 'This currency is unavailable'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     * @throws \Throwable
     */
    public function validateSubcategory($attribute, $params)
    {
        $category = Category::getDb()->cache(function ($db) {
            return Category::findOne($this->category_id);
        });

        if ($category->rgt - $category->lft > 1 && !$this->subcategory_id) {
            $this->addError($attribute, Yii::t('board', 'You have to specify product subcategory'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('board', 'Product Name'),
            'categoryId' => Yii::t('board', 'Category'),
            'parentCategoryId' => Yii::t('board', 'Category'),
            'description' => Yii::t('board', 'Description'),
            'quantity' => Yii::t('board', 'Quantity'),
            'shipping_type' => Yii::t('board', 'Options of shipping'),
            'shipping_info' => Yii::t('board', 'Details of shipping'),
            'address' => Yii::t('board', 'Address')
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        if (!$this->validate() || !$this->dynamic_form->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $this->product->load($this->attributes, '');
//        $this->product->attributes = $this->attributes;
        $this->product->user_id = Yii::$app->request->get('user_id');

        $isSaved = $this->product->save();

        if($isSaved === true) {
            $transaction->commit();
        }

        if ($this->product->getOldAttribute('status') == $this->status && $this->status != Product::STATUS_ACTIVE) {
            $this->product->defineStatus();
        } else {
            if ($this->product->getOldAttribute('status') !== Product::STATUS_ACTIVE && $this->status == Product::STATUS_ACTIVE) {
                $this->product->sendModerationNotification();
            }
            $this->product->updateAttributes(['status' => $this->status]);
        }

        if ($this->lat && $this->long && $this->address) {
            if (!AddressTranslation::find()->where(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language])->exists()) {
                (new AddressTranslation(['lat' => $this->lat, 'long' => $this->long, 'locale' => Yii::$app->language, 'title' => $this->address]))->save();
            }
        }

        if($isSaved === true) {
            $this->product->updateElastic();
        } else {
            $transaction->rollBack();
        }

        return $isSaved;
    }

    /**
     * @return array|mixed
     */
    public function getProductAttributes()
    {
        if ($this->_productAttributes === null) {
            if ($this->product->isNewRecord) {
                $this->_productAttributes = [];
            } else {
                $this->_productAttributes = $this->product->extra;
            }
        }
        return $this->_productAttributes;
    }

    /**
     * @return array|null
     */
    public function getAttachments()
    {
        if ($this->_attachments === null) {
            if ($this->product->isNewRecord) {
                $this->_attachments = [];
            } else {
                $this->_attachments = $this->product->attachments;
            }
        }
        return $this->_attachments;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->_product = $product;
    }

    /**
     * @param $input
     */
    public function myLoad($input)
    {
        $this->load($input);
        $dynamicAttributes = DynamicForm::getAttributesListByCategory(Category::findOne($this->subcategory_id ?? $this->category_id));
        $this->dynamic_form = DynamicForm::initModelFromGroup($dynamicAttributes, []);
        $this->dynamic_form->load($input);

        if (array_key_exists('Attachment', $input) && is_array($input['Attachment'])) {
            foreach ($input['Attachment'] as $key => $item) {
                $attachmentObject = new Attachment();
                $attachmentObject->setAttributes($item);
                $attachmentObject->entity = 'product';

                $this->product->bindAttachment($attachmentObject->content)->attributes = $attachmentObject->attributes;
            }
        }

        if ($this->dynamic_form !== null) {
            $this->bindProductAttributes($this->dynamic_form);
        }

        if (array_key_exists('ProductAttribute', $input) && is_array($input['ProductAttribute'])) {
            foreach ($input['ProductAttribute'] as $key => $item) {
                $productAttributeObject = new ProductAttribute();
                $productAttributeObject->setAttributes($item);

                $this->product->bind('productAttributes', $productAttributeObject->id)->attributes = $productAttributeObject->attributes;
            }
        }

        $translationObject = new ContentTranslation([
            'title' => $this->title,
            'content' => $this->description,
            'locale' => Yii::$app->language,
            'entity' => 'product',
        ]);
        $this->product->bind('translations', $translationObject->id)->attributes = $translationObject->attributes;

        $shippingTypeAttribute = Attribute::findOne(['alias' => 'shipping_type']);
        if ($shippingTypeAttribute !== null && $this->shipping_type) {
            $value = AttributeValue::findOne(['attribute_id' => $shippingTypeAttribute->id, 'id' => $this->shipping_type]);
            if($value !== null) {
                $this->product->bind('productAttributes')->attributes = [
                    'attribute_id' => $shippingTypeAttribute->id,
                    'key' => "attribute_{$shippingTypeAttribute->id}",
                    'is_text' => false,
                    'value' => $this->shipping_type,
                    'locale' => Yii::$app->language,
                    'entity_alias' => 'shipping_type',
                    'value_alias' => $value->alias
                ];
            }
        }
    }

    /**
     * @return array
     */
    public function getShippingTypes()
    {
        $shippingAttribute = Attribute::find()->joinWith(['values'])->where(['mod_attribute.alias' => 'shipping_type'])->one();
        return ArrayHelper::map($shippingAttribute->values, 'id', 'translation.title');
    }

    /**
     * @param $id
     */
    public function loadProduct($id)
    {
        $this->product = Product::findOne((int)$id);
        $this->attributes = $this->product->attributes;
        $this->title = $this->product->translation->title;
        $this->description = $this->product->translation->content;

        $this->shipping_type = ProductAttribute::find()->where(['product_id' => $this->product->id, 'entity_alias' => 'shipping_type'])->select('value')->scalar();
    }

    /**
     * @return array
     */
    public function validateAll()
    {
        return ArrayHelper::merge(
            ActiveForm::validate($this),
            $this->product->validateWithRelations()
        );
    }

    /**
     * @param DynamicForm $dynamicForm
     */
    private function bindProductAttributes(DynamicForm $dynamicForm)
    {
        foreach ($dynamicForm->attributes as $key => $attribute) {
            if (!$attribute) continue;

            $parts = explode('_', $key);

            if (is_array($attribute)) {
                $values = $attribute;
            } else {
                if(!$dynamicForm->isMultiValueField($key)) {
                    $values = (array)$attribute;
                } else {
                    $valueParts = array_filter(explode(',', $attribute));
                    $values = $valueParts;
                }
            }

            foreach($values as $pa) {
                $productAttributeObject = AttributeValueObjectBuilder::makeAttributeObject(ProductAttribute::class, $parts[1], $pa, ['locale' => $this->locale]);
                if($productAttributeObject !== null) {
                    $this->product->bind('productAttributes', $productAttributeObject->id)->attributes = $productAttributeObject->attributes;
                }
            }
        }
    }
}