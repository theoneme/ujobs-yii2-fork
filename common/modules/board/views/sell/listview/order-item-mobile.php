<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.05.2017
 * Time: 13:23
 */

use yii\helpers\Html;
use common\modules\store\models\Order;
use yii\web\View;

/* @var Order $model
 * @var View $this
 */
$baseUrl = '/account/order/product-history';
?>

<div class="jm-option">
    <div class="jm-img">
        <?= Html::a(Html::img($model->product->getThumb('catalog')), [$baseUrl, 'id' => $model->id], ['data-pjax' => 0]) ?>
    </div>
    <div class="jm-name">
        <?= Html::a($model->product->getLabel(), [$baseUrl, 'id' => $model->id], ['data-pjax' => 0]) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('model', 'Order Date')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::$app->formatter->asDate($model->created_at) ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('model', 'Total')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getTotal() ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('model', 'Status')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->getStatusLabel(true) ?>
    </div>
</div>