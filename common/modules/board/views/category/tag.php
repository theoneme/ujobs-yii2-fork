<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 07.08.2017
 * Time: 15:03
 */

use common\models\JobAttribute;
use common\models\Page;
use frontend\assets\CatalogAsset;
use frontend\modules\elasticfilter\components\BaseFilter;
use frontend\modules\elasticfilter\components\FilterJob;
use frontend\modules\elasticfilter\models\FilterProductTag;
use frontend\modules\elasticfilter\models\FilterJobTag;
use yii\base\Widget;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use common\models\Category;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $page Page */
/* @var $category Category */
/* @var $entity string */
/* @var $seo array */
/* @var $filterClass Widget */
/* @var $request string */
/* @var $query ActiveQuery */
/* @var $tagAttribute JobAttribute */
/* @var $filter FilterJob */

CatalogAsset::register($this);

?>
<?php Pjax::begin(['clientOptions' => ['maxCacheLength' => 0], 'id' => 'catalog-pjax']); ?>
    <div class="catalog-breadcrumbs-container">
        <?= Breadcrumbs::widget([
            'itemTemplate' => "{link}/", // template for all links
            'activeItemTemplate' => "&nbsp;{link}",
            'links' => $filter->breadcrumbs,
            'options' => ['class' => 'breadcrumbs hidden-xs container-fluid'],
            'tag' => 'div'
        ]); ?>
    </div>
    <div class="how-works text-center">
        <a class="close-how" href=""></a>

        <h2><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
        <div class="three-steps text-center">
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/products/screen1.png", [
                        'alt' => Yii::t('board', 'Make choice'),'title' => Yii::t('board', 'Make choice')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Make choice') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn1'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/products/screen2.png", [
                        'alt' => Yii::t('board', 'Pay deposit'),'title' => Yii::t('board', 'Pay deposit')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Pay deposit') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn2'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/products/screen3.png", [
                        'alt' => Yii::t('board', 'Get your product'),'title' => Yii::t('board', 'Get your product')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Get your product') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn3'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1><?= Html::encode($seo['h1']) ?></h1>
            <div class="banner-prod-descr text-center">
                <?= Html::encode($seo['subhead']) ?>
            </div>
            <a class="product-how-works hidden-xs" href="#" id="how-work">
                <?= Yii::t('app', 'How the service works'); ?>
            </a>
        </div>
    </div>

    <div class="container-fluid category-top">
        <?php if (!isPagespeed()) { ?>
            <div class="block-mob mar-fixedmob visible-xs">
                <?php
                $filter->template = 'filter-product-mobile';
                $filter->filterType = BaseFilter::FILTER_TYPE_MOBILE;
                echo $filter->run();
                ?>
            </div>
        <?php } ?>

        <div class="category-main row hidden-xs">
            <?php
            $filter->template = 'filter-product';
            $filter->filterType = BaseFilter::FILTER_TYPE_DEFAULT;
            echo $filter->run();
            ?>
            <div class="mark-container">
                <div class="mark row">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                    <div class="mark-text">
                        <?= Yii::t('app', 'Didn\'t find what you were looking for or don\'t have time to search?') ?>
                        <div class="mark-small">
                            <?= Yii::t('app', 'Submit Request and hundreds of Workers will be able to send you their proposals. You will have plenty to choose from.') ?>
                        </div>
                    </div>
                    <?php
                    if (isGuest()) {
                        echo Html::a(Yii::t('app', 'Submit request'), null, [
                            'class' => 'btn-big bright redirect-set',
                            'data-toggle' => 'modal',
                            'data-target' => '#ModalSignup',
                            'data-redirect' => Url::to(['/entity/global-create', 'type' => 'tender-product'])
                        ]);
                    } else {
                        echo Html::a(Yii::t('app', 'Submit request'), ['/entity/global-create', 'type' => 'tender-product'], [
                            'class' => 'btn-big bright',
                            'data-pjax' => 0
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php if (!empty($seo['textHeading'])) { ?>
    <div class="text-categories">
        <div class="container-fluid">
            <h2 class="text-left">
                <?= $seo['textHeading'] ?>
            </h2>
            <h3>
                <?= $seo['textSubheading'] ?>
            </h3>
            <div class="categories-descr">
                <?= nl2br($seo['text']) ?>
            </div>
        </div>
    </div>
<?php } ?>

<?= \frontend\components\CrosslinkWidget::widget() ?>

<?php Pjax::end(); ?>

<?php $howItWorks = Yii::t('app', 'How the service works');
$minimizeLabel = Yii::t('labels', 'Minimize');
$script = <<<JS
    $('body').on('click', '#how-work', function(e) {
        if($('.how-works').hasClass('open-how')) {
            $(this).html('$minimizeLabel');
        } else {
            $(this).html('$howItWorks');
        }
    });

	$(document).on('pjax:complete', function() {
		$('html, body').stop().animate({
            scrollTop: $('.category-top').offset().top - 50
        }, 500);
	})
JS;

$this->registerJs($script);