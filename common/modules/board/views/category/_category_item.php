<?php
use common\models\Category;
use yii\helpers\Html;

/* @var $category Category*/
/* @var $size string*/
?>
<div class="cat-img-block <?= $size?>">
    <?php $img = Html::img($category->getThumb(), ['alt' => $category->translation->title,'title' => $category->translation->title])?>
    <?= Html::a(
        '<div class="cat-img">' . $img . '</div><div class="cat-name">' . $category->translation->title . '</div>',
        ['/board/category/products', 'category_1' => $category->translation->slug],
        ['class' => 'cat-alias']
    )?>
</div>