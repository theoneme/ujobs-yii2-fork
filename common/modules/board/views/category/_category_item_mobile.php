<?php
/* @var $category \common\models\Category*/
use yii\helpers\Html;

?>
<div class="mob-work-item" data-key="">
    <a class="like" href="">
        <i class="fa fa-heart" aria-hidden="true"></i>
    </a>
    <div class="mob-work-table">
        <?= Html::a(
            Html::img($category->getThumb(), ['alt' => $category->translation->title, 'title' => $category->translation->title]),
            ['/board/category/products', 'alias' => $category->translation->slug],
            ['class' => 'mob-work-img', 'data-pjax' => 0]
        )?>

        <div class="mob-work-info">
            <?= Html::a($category->translation->title,
                ['/board/category/products', 'alias' => $category->translation->slug],
                ['class' => 'bf-descr', 'data-pjax' => 0]
            );?>
            <div class="markers">
                <div class="stick text-center feat"><?= Yii::t('app', 'Favorites')?></div>
            </div>
<!--            <div class="rate">-->
<!--                Тут будут звёздочки картика-->
<!--            </div>-->
        </div>
    </div>

<!--    Это по моему не нужно для категории-->
<!--    <div class="mob-block-bottom row">-->
<!--        <div class="col-md-12">-->
<!--            <div class="by">-->
<!--                от &nbsp;<a href="https://ujobs.me/account/profile/show/10127">Аутсорсинг от-->
<!--                    uJobs</a></div>-->
<!--            <a class="block-price"-->
<!--               href="https://ujobs.me/nastroyka-reklamy-v-yandeks-direkt-595b53f66c357-job"-->
<!--               data-pjax="0">3&nbsp;000р.</a></div>-->
<!--    </div>-->
</div>