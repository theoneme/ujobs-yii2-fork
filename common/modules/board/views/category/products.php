<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.07.2017
 * Time: 13:14
 */

use common\models\Category;
use common\models\Job;
use frontend\assets\CatalogAsset;
use frontend\modules\elasticfilter\components\BaseFilter;
use frontend\modules\elasticfilter\components\FilterProduct;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $category Category */
/* @var $children Category[] */
/* @var $entity string */
/* @var $seo array */
/* @var $filter FilterProduct */


CatalogAsset::register($this);

?>

<?php Pjax::begin(['enablePushState' => true, 'id' => 'catalog-pjax']); ?>

    <div class="catalog-breadcrumbs-container">
        <?= Breadcrumbs::widget([
            'itemTemplate' => "{link}/", // template for all links
            'activeItemTemplate' => "&nbsp;{link}",
            'links' => $filter->breadcrumbs,
            'options' => ['class' => 'breadcrumbs hidden-xs container-fluid'],
            'tag' => 'div'
        ]); ?>
    </div>
    <div class="how-works text-center">
        <a class="close-how" href=""></a>

        <h2><?= $seo['upperBlockHeading'] ?></h2>

        <div class="newb-descr-grey"><?= $seo['upperBlockSubheading'] ?></div>
        <div class="three-steps text-center">
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/products/screen1.png", [
                        'alt' => Yii::t('board', 'Make choice'),'title' => Yii::t('board', 'Make choice')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Make choice') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn1'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/products/screen2.png", [
                        'alt' => Yii::t('board', 'Pay deposit'),'title' => Yii::t('board', 'Pay deposit')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Pay deposit') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn2'] ?>
                </p>
            </div>
            <div class="three-steps-item">
                <div class="circle-works">
                    <div class="inner-circle"></div>
                    <?= Html::img("/images/new/catalog/" . Yii::$app->language . "/products/screen3.png", [
                        'alt' => Yii::t('board', 'Get your product'),'title' => Yii::t('board', 'Get your product')
                    ]) ?>
                </div>
                <div class="circle-title"><?= Yii::t('board', 'Get your product') ?></div>
                <p class="circle-descr text-center">
                    <?= $seo['upperBlockColumn3'] ?>
                </p>
            </div>
        </div>
    </div>
    <div class="banner-product text-center">
        <div class="container-fluid">
            <h1><?= Html::encode($seo['h1']) ?></h1>
            <div class="banner-prod-descr text-center">
                <?= Html::encode($seo['subhead']) ?>
            </div>
            <a class="product-how-works hidden-xs" href="#" id="how-work">
                <?= Yii::t('app', 'How the service works'); ?>
            </a>
        </div>
    </div>

    <div class="container-fluid category-top">
        <div class="block-mob mar-fixedmob visible-xs">
            <?php if (!isPagespeed()) {
                $filter->template = "filter-product-mobile";
                $filter->filterType = BaseFilter::FILTER_TYPE_MOBILE;
                echo $filter->run();
            } ?>
        </div>

        <div class="category-main row hidden-xs">
            <?php if (!isPagespeed()) {
                $filter->template = "filter-product";
                $filter->filterType = BaseFilter::FILTER_TYPE_DEFAULT;
                echo $filter->run();
            } ?>
            <?php if ($entity != Job::TYPE_TENDER) { ?>
                <div class="mark-container">
                    <div class="mark row">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                        <div class="mark-text">
                            <?= Yii::t('board', 'Didn\'t find the product at the right price or  don\'t have time to search?') ?>
                            <div class="mark-small"><?= Yii::t('board', 'Submit an order for the product and hundreds of sellers will see your request.') ?></div>
                        </div>
                        <?php
                        if (isGuest()) {
                            echo Html::a(Yii::t('board', 'Submit product'), null, [
                                'class' => 'btn-big bright redirect-set',
                                'data-toggle' => 'modal',
                                'data-target' => '#ModalSignup',
                                'data-redirect' => Url::to(['/entity/global-create', 'type' => 'tender-product'])
                            ]);
                        } else {
                            echo Html::a(Yii::t('board', 'Submit product'), ['/entity/global-create', 'type' => 'tender-product'], [
                                'class' => 'btn-big bright',
                                'data-pjax' => 0
                            ]);
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

<?php if ($seo['textHeading'] !== null) { ?>
    <div class="text-categories">
        <div class="container-fluid">
            <h2 class="text-left">
                <?= $seo['textHeading'] ?>
            </h2>
            <h3>
                <?= $seo['textSubheading'] ?>
            </h3>
            <div class="categories-descr">
                <?= nl2br($seo['text']) ?>
            </div>
        </div>
    </div>
<?php } ?>

<?= \frontend\components\CrosslinkWidget::widget() ?>

<?php Pjax::end(); ?>

<?php $howItWorks = Yii::t('app', 'How the service works');
$minimizeLabel = Yii::t('labels', 'Minimize');
$script = <<<JS
	var body = $('body');

	body.on('click', '#how-work', function(e) {
        if($('.how-works').hasClass('open-how')) {
            $(this).html('$minimizeLabel');
        } else {
            $(this).html('$howItWorks');
        }
    }).on('click', '.categories-read-more', function(e) {
        e.preventDefault();
        $('.cd-short').hide();
        $('.cd-all').show();
    });

	var text = $('.cd-all').html();
	var size = 900;
    if (text !== undefined && text.length > size) {
	    text = text.slice(0, 900);
		var lastSpace = text.lastIndexOf(" ");
		if( lastSpace > 0) {
		    text = text.substr(0, lastSpace);
		}
		$('.cd-short').html(text + '...<br/> <a class=\"categories-read-more\" href=\"\">Read more</a>');
	}

    $(document).on('pjax:complete', function(e) {
		var page = getParameterByName('page');
		if(page !== null) {
			$('html, body').stop().animate({
	            scrollTop: $('#jobs-pjax').offset().top
	        }, 500);
		}
	})
JS;

$this->registerJs($script);