<?php

use common\helpers\FileInputHelper;
use common\helpers\ImperaviHelper;
use common\models\Attachment;
use common\modules\board\models\frontend\PostProductForm;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\components\GmapsInputWidget;
use frontend\components\WizardSteps;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostProductForm
 * @var $exampleModel common\models\Job
 * @var $action string
 * @var $locales array
 * @var $currencySymbols array
 * @var $currencies array
 * @var $measures array
 */
CropperAsset::register($this);
SelectizeAsset::register($this);

?>
    <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="mobile-note-title"></div>
                </div>
                <div class="modal-body">
                    <div class="mobile-note-text"></div>
                    <div class="mobile-note-btn text-center">
                        <a class="button big green" href="#"><?= Yii::t('app', 'Continue') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('board', 'Description'),
    2 => Yii::t('board', 'Photo and details'),
    3 => Yii::t('board', 'Price and shipping'),
    4 => Yii::t('board', 'Publish'),
], 'step' => 1]); ?>
    <div class="profile">
        <div class="container-fluid">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a>
                        <?= Yii::t('app', Yii::$app->params['languages'][$model->locale]) ?>
                    </a>
                </li>
            </ul>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-step' => 1,
                            'data-final-step' => 4
                        ],
                        'id' => 'product-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]); ?>
                    <?= Html::hiddenInput('id', $model->product->id, ['id' => 'product-id']) ?>
                    <div class="tab-content nostyle-tab-content">
                        <div class="settings-content step tab-pane fade in active" id="step1" data-step="1">
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, 'locale')->hiddenInput()->label(false)->error(false); ?>
                                    <?= $form->field($model, "title", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                                <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                        <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('board', 'Max') . '</div>
                                    </div>'])->textarea([
                                        'class' => 'readsym bigsymb',
                                        'placeholder' => Yii::t('board', 'Name of your product'),
                                        'data-action' => 'category-suggest'
                                    ]) ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('board', 'Indicate what you are selling') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Try to make it clear from the title what you are selling. You can use up to 80 characters.') ?>
                                        </p>
                                        <p>
                                            <?= Yii::t('board', 'For example:') ?>
                                        </p>
                                        <p>
                                            - <?= Yii::t('board', 'One bedroom apartment of 80 square meters in the center of Moscow.') ?>
                                        </p>
                                        <p>
                                            - <?= Yii::t('board', 'Double bed 200x180. Made in Slovenia.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <?= $form->field($model, "description", [
                                        'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                                <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->widget(Widget::class, ArrayHelper::merge(ImperaviHelper::getDefaultConfig(), [
                                        'settings' => [
                                            'buttonsHide' => [
                                                'deleted',
                                                'horizontalrule',
                                                'link',
                                            ]
                                        ]
                                    ])); ?>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div
                                                class="notes-title"><?= Yii::t('board', 'Description') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'In this field, try to describe your product in as much detail as possible. The more detailed your description is, the more likely it is that buyers will choose your offer.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label class="control-label"
                                                       for="postproductform-category_id"><?= Yii::t('board', 'Category') ?></label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                                        'prompt' => ' -- ' . Yii::t('board', 'Select An Upper Category'),
                                                        'class' => 'form-control'
                                                    ])->label(false); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postproductform-parent_category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('board', 'Select A Category'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helper'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('board', 'Select A Category'),
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ]
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-md-offset-6 col-xs-12 select-margin hidden 3rd-level">
                                                    <?= $form->field($model, 'subcategory_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postproductform-category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helperx'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                                        ],
                                                        'pluginEvents' => [
                                                            "depdrop:change" => "function(event, id, value, count) { 
                                                                let container = $(this).closest('.3rd-level');
                                                                if(count > 0) {
                                                                    container.removeClass('hidden');
                                                                } else {
                                                                    container.addClass('hidden');
                                                                }
                                                            }",
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ]
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helperx', $model->subcategory_id, ['id' => 'depdrop-helperx']); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="suggestions-block" class="col-md-12">

                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('board', 'Choose category and subcategory') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'It`s important to choose the right category for your announcement. This helps sellers to find your product faster.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('board', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray']) ?>
                                <?= Html::submitButton(Yii::t('board', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane step fade in form-gig file-input-step" id="step2"
                             data-step="2">
                            <!--<div class="">
                                <div class="profileh">
                                    <?= Yii::t('board', 'Photo and details') ?>
                                </div>
                            </div>-->
                            <!--                            <div class="row">-->
                            <!--                                <div class="profileh">-->
                            <!--                                    --><? //= Yii::t('board', 'Photo and details') ?>
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <div class="row show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label"
                                                   for="postproductform-uploaded_images"><?= Yii::t('board', 'Photos') ?></label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="cgp-upload-files">
                                            <?= FileInput::widget(
                                                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                    'id' => 'file-upload-input',
                                                    'name' => 'uploaded_images[]',
                                                    'options' => ['multiple' => true],
                                                    'pluginOptions' => [
                                                        'dropZoneTitle' => Yii::t('app', 'Drag & drop photos here &hellip;'),
                                                        'overwriteInitial' => false,
                                                        'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                                            return $var->getThumb();
                                                        }, $model->attachments) : [],
                                                        'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                                                    ]
                                                ])
                                            ) ?>
                                            <div class="images-container">
                                                <?php foreach ($model->attachments as $key => $image) {
                                                    /* @var $image Attachment */
                                                    echo $form->field($image, "[{$key}]content")->hiddenInput([
                                                        'value' => $image->content,
                                                        'data-key' => 'image_init_' . $image->id
                                                    ])->label(false);
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('board', 'Photos') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('board', 'It’s photo time! You can upload as many images as you want.') ?>
                                            </p>
                                            <ul>
                                                <li><?= Yii::t('board', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                                                <li><?= Yii::t('board', 'Dimensions: more - better') ?></li>
                                                <li><?= Yii::t('board', 'Max File size: {count} MB', ['count' => 5]) ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            <div id="attributes-info"></div>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('board', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('board', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade step in" id="step3"
                             data-step="3">
                            <!--                            <div class="row">-->
                            <!--                                <div class="profileh">-->
                            <!--                                    --><?php //= Yii::t('board', 'Shipping information') ?>
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label">
                                                <?= Yii::t('app', 'Price for:') ?>
                                            </label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <?= $form->field($model, 'measure_id')->dropDownList(
                                            ArrayHelper::merge($measures[$model->category_id] ?? [], [null => Yii::t('board', 'item')]),
                                            ['class' => 'form-control measure-select']
                                        )->label(false); ?>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('app', 'Price type') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Specify what your buyers will pay for. Whether it is product item or a certain unit of measure.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label"
                                                   for="postproductform-currency_code"><?= Yii::t('board', 'Price and currency') ?></label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="cgb-head"
                                                     style="padding: 0"><?= Yii::t('board', 'Currency for price') ?></div>
                                                <?= $form->field($model, "currency_code")->dropDownList($currencies, [
                                                    'class' => 'form-control'
                                                ])->label(false); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="cgb-head" style="padding: 0"><?= Yii::t('board', 'Price') ?></div>
                                                <?= $form->field($model, "price")->textInput([
                                                    'type' => 'number',
                                                    'class' => ' form-control',
                                                    'placeholder' => '9 999'
                                                ])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('board', 'Currency and price') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('board', 'Set the price for the measure unit specified above. Also set currency for your price.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <?= $form->field($model, "amount", [
                                    'template' => '
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            {label}
                                                <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        {input}
                                        {error}
                                    </div>
                                '])->textInput([
                                    'type' => 'number',
                                    'class' => ' form-control'
                                ])->label(Yii::t('board', 'Quantity')) ?>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center">
                                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="notes-title"><?= Yii::t('board', 'Quantity') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Set quantity if you have more than one item for sale.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row formgig-block show-notes">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                        <div class="set-title">
                                            <label class="control-label" for="postproductform-currency_code">
                                                <?= Yii::t('board', 'Address') ?>
                                            </label>
                                            <div class="mobile-question" data-toggle="modal"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                        <?= GmapsInputWidget::widget(['model' => $model, 'inputOptions' => [
                                            'placeholder' => Yii::t('board', 'Moscow, Novy Arbat 12')]
                                        ]); ?>
                                    </div>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('board', 'Your address') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('board', 'Set address where product can be viewed, or address for shipping calculation. No need to set your apartment number, or house number, if you don`t wish to.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="shipping-details">
                                <div class="row formgig-block show-notes">
                                    <?= $form->field($model, 'shipping_type', [
                                        'template' => '
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                {label}
                                                    <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            {input}
                                            {error}
                                        </div>
                                    '])->radioList($model->getShippingTypes(), [
                                        'class' => 'text-left',
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                            $chk = $index === 0 ? 'checked' : '';
                                            $output = "<input id='type{$index}' {$chk} type='radio' value='{$value}' name='{$name}' class='radio-checkbox' />
                                                    <label for='type{$index}'>
                                                        <span class='ctext'>{$label}</span>
                                                    </label>";
                                            return $output;
                                        }
                                    ]) ?>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('board', 'Shipping type') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('board', 'Setting that shipping is possible means that you can send product to other cities via courier service.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row formgig-block show-notes">
                                    <?= $form->field($model, "shipping_info", [
                                        'template' => '
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                {label}
                                                    <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            {input}
                                            {error}
                                        </div>
                                    '])->textarea([
                                        'placeholder' => Yii::t('board', 'Set how product can be sent and who pays for shipping')
                                    ]) ?>
                                    <div class="notes col-md-6 col-md-offset-12">
                                        <div class="notes-head">
                                            <div class="lamp text-center">
                                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="notes-title"><?= Yii::t('board', 'Shipping details') ?></div>
                                        </div>
                                        <div class="notes-descr">
                                            <p>
                                                <?= Yii::t('board', 'Set in details who pays for shipping and how you can send product to other cities.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row formgig-block show-notes">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label class="control-label"
                                                       for="postproductform-length"><?= Yii::t('board', 'Product sizes') ?></label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="cgb-head"
                                                         style="padding: 0"><?= $model->getAttributeLabel('length') ?></div>
                                                    <?= $form->field($model, "length")->textInput([
                                                        'placeholder' => '100 ' . Yii::t('board', 'cm'),
                                                        'type' => 'number'
                                                    ])->label(false); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="cgb-head"
                                                         style="padding: 0"><?= $model->getAttributeLabel('width') ?></div>
                                                    <?= $form->field($model, "width")->textInput([
                                                        'placeholder' => '50 ' . Yii::t('board', 'cm'),
                                                        'type' => 'number'
                                                    ])->label(false); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="cgb-head"
                                                         style="padding: 0"><?= $model->getAttributeLabel('height') ?></div>
                                                    <?= $form->field($model, "height")->textInput([
                                                        'placeholder' => '200 ' . Yii::t('board', 'cm'),
                                                        'type' => 'number'
                                                    ])->label(false); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="notes col-md-6 col-md-offset-12">
                                            <div class="notes-head">
                                                <div class="lamp text-center">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="notes-title"><?= Yii::t('board', 'Product sizes') ?></div>
                                            </div>
                                            <div class="notes-descr">
                                                <p>
                                                    <?= Yii::t('board', 'Your product`s size. These fields are required if shipping is available for your product. You may leave these fields empty is only pickup is available.') ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block clearfix">
                                <?= Html::a(Yii::t('board', 'Back'), ['/entity/global-create'], ['class' => 'btn-mid-gray prev-step']) ?>
                                <?= Html::submitButton(Yii::t('board', 'Save & Continue'), ['class' => 'btn-mid fright next-step']) ?>
                            </div>
                        </div>
                        <div class="settings-content tab-pane fade in" id="step4" data-step="4">
                            <div class="publish-title text-center">
                                <?= Yii::t('board', 'It\'s almost ready...') ?>
                            </div>
                            <div class="publish-text text-center">
                                <?= Yii::t('board', "Let`s publish your product and <br/> attract the attention of buyers.") ?>
                            </div>
                            <div class="create-gig-block row">
                                <div class="col-md-12">
                                    <?= Html::button(Yii::t('board', 'Back'), ['class' => 'btn-mid-gray prev-step']) ?>
                                    <?= !empty($locales) ? Html::button(Yii::t('board', 'Next language'), ['class' => 'btn-mid fright next-language']) : '' ?>
                                    <?= Html::submitButton(Yii::t('board', 'Publish product'), ['class' => 'btn-mid fright']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
if (!empty($locales)) {
    echo Html::beginForm(['/board/product/create'], 'post', ['id' => 'product-locale-form']);
    foreach ($locales as $locale) {
        echo Html::hiddenInput('locales[]', $locale);
    }
    echo Html::endForm();
}
?>

<?= $this->render('@frontend/views/common/crop-bg.php')?>

<?php
$userId = Yii::$app->user->identity->getId();
$attachmentCount = count($model->attachments);
$ajaxCreateRoute = Url::toRoute('/board/product/ajax-create');
$draftUrl = Url::toRoute('/board/product/draft');
$categorySuggestRoute = Url::toRoute('/ajax/category-suggest');
$renderAttributesRoute = Url::toRoute('/board/product/render-attributes');
$shippingRoute = Url::toRoute('/board/product/shipping-details');
$renderExtraRoute = Url::toRoute('/board/product/render-extra');
$addTagMessage = Yii::t('board', 'Add tag');
$tagListUrl = Url::to(['/tag/product']);
$currencySymbolsJson = Json::encode($currencySymbols);
$measures = json_encode($measures);

?>

<?php if (isset($action) && $action === 'create') {
    $script = <<<JS
        function makeDraft() {
            let currentStep = $("#product-form").data('step');
            if(currentStep > 1) {
                isDraftProcessing = true;
                var form = $("#product-form").serialize();
                return $.ajax({
                    url: '$draftUrl',
                    type: "post",
                    data: form,
                    success: function(response) {
                        if(response.success === true) {
                            $("#product-id").val(response.product_id);
                            let atts = response.attachments;
                            if(Object.keys(atts).length) {
                                let fileInput = $(".file-upload-input");
                                let inputId = fileInput.data('krajee-fileinput');
                                let opts = window[inputId];
                                opts = atts ? $.extend(true, {}, opts, atts) : opts;
                                fileInput.fileinput('destroy');
                                fileInput.fileinput(opts);
        
                                $(".images-container").html("");
                                $.each(atts.initialPreview, function(key, value) {
                                    let kostyl = value.toString();
                                    let kostylParts = kostyl.split("?");
                                    $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + key+ "\' type=\'hidden\' value=\'" + kostylParts[0].toString() + "\'>");
                                    attachments++;
                                });
                            }
                        }
                        isDraftProcessing = false;
                    }
                });
            }
        }
JS;
    $this->registerJs($script);
} else {
    $script = <<<JS
        function makeDraft() {
            isDraftProcessing = false;
            return false;
        }
JS;
    $this->registerJs($script);
}

$script = <<<JS
    const PICKUP = 47936,
	    SHIPPING = 47935;

	var uid = $userId,
    	attachments = $attachmentCount,
    	isDraftProcessing = false,
    	measures = $measures,
    	currencySymbols = $currencySymbolsJson;

    function loadAttributes(categoryId, productId) {
        $.get('$renderAttributesRoute', {
            category_id: categoryId,
            entity: productId,
        }, function(data) {
            $("#attributes-info").html(data);
        }, "html");
    }
    
    function loadMeasures(categoryId) {
        $('.measure-select option:not([value=""])').remove();
        $.each(measures[categoryId], function(index, value) {
            $('.measure-select').append('<option value="' + index + '">' + value + '</option>');
        });
    }
    
    function loadShipping(categoryId) {
        $.get('$shippingRoute', {
            category_id: categoryId,
        }, function(data) { console.log(data);
            if(data.available === false) { console.log(data);
                $('#shipping-details').hide().find('input, textarea').attr('disabled', 'disabled');
            } else {
                $('#shipping-details').show().find('input, textarea').removeAttr('disabled');
            }
        });
    }

    $(document).on("click", ".next-language", function(){
        $.post('$ajaxCreateRoute',
            $("#product-form").serialize(),
            function(data) {
                if (data.success === true) {
                    $("#job-locale-form").submit();
                }
            },
            "json"
        );
        return false;
    });

    $(document).on('click', '.mobile-question', function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click', '.mobile-note-btn a', function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });

    $(document).on("change", "#postproductform-currency_code", function(e) {
    	var value = $(this).val();

		$('.currency-place').html(currencySymbols[value]);
    });
	$('#postproductform-currency_code').trigger('change');
	
	$(document).on('change', '#postproductform-shipping_type', function(e) {
	    let value = $(this).find('input:checked').val(),
	        infoBlock = $('#postproductform-shipping_info');
	    
	    switch(parseInt(value)) {
	        case PICKUP:
	            infoBlock.closest('.formgig-block').hide();
	            break;
            case SHIPPING:
                infoBlock.closest('.formgig-block').show();  
                break;
	    }
	});
	$('#postproductform-shipping_type').trigger('change');

    $(document).on("change", "#postproductform-category_id, #postproductform-subcategory_id", function(e) {
        let productId = $('#product-id').val();
        let categoryId = $(this).find('option:selected').val();
        if(categoryId) {
            loadAttributes(categoryId, productId);
            loadShipping(categoryId, productId);
            loadMeasures(categoryId);
        }
    });
    
    $(document).on("change", "#postproductform-parent_category_id", function(e) {
        $('#attributes-info').html('');
    });
    
    let suggestTimer = null;
    $(document).on("keyup", "[data-action=category-suggest]", function() {
        let self = $(this);
        clearTimeout(suggestTimer);
        suggestTimer = setTimeout(function() {
            let val = self.val();
            $.get('{$categorySuggestRoute}',
                {entity: 'product', request: val},
                function(data) {
                    if (data.success === true) {
                        $('#suggestions-block').html(data.html);
                    } else {
                        $('#suggestions-block').html('');
                    }
                },
                "json"
            );
        }, 700);
    });
    
    $(document).on('click', '[data-action=suggestion-select]', function() {
        let parentCategoryInput = $('#postproductform-parent_category_id');
        let categoryInput = $('#postproductform-category_id');
        let subcategoryInput = $('#postproductform-subcategory_id');
        let pId = $(this).data('p-id');
        let cId = $(this).data('c-id');
        let sId = $(this).data('s-id');
        
        categoryInput.attr('data-c-id', cId);
        subcategoryInput.attr('data-s-id', sId);
        parentCategoryInput.val(pId).trigger('change');
        
        return false;
    });
    
    $('#postproductform-category_id').on('depdrop:afterChange', function(event, id, value) {
        let cId = $(this).attr('data-c-id');
        if(cId) {
            $('#postproductform-category_id').val(cId).trigger('change');
        }
    });
    
    $('#postproductform-subcategory_id').on('depdrop:afterChange', function(event, id, value) {
        let sId = $(this).attr('data-s-id');
        if(sId) {
            $('#postproductform-subcategory_id').val(sId).trigger('change');
        }
    });
    
    if($('#depdrop-helperx').val()) {
        loadAttributes($('#depdrop-helperx').val(), $('#product-id').val());
    } else if($('#depdrop-helper').val()) {
        loadAttributes($('#depdrop-helper').val(), $('#product-id').val());
    }  

    $(document).on("click", ".closeeditt", function(e) {
        e.preventDefault();
        $(this).closest(".faq-block").remove();
        $(this).closest(".create-gig-block").find(".load-faq").show();
    });

    $(document).on("click", ".load-job-extra", function(e) {
        e.preventDefault();
        var container = $(this).closest(".create-gig-block").find(".extra-container");
        $.post('$renderExtraRoute', {
            iterator: extras
        }, function(data) {
            if (data.success === true) {
                extras++;
                container.append(data.html);

                $('#postproductform-currency_code').trigger('change');
            } else {
                alertCall('top', 'error', data.message);
            }
        }, "json");
    });

	$("#product-form").on("beforeValidateAttribute", function (event, attribute, messages) {
		let current_step = $(this).data("step");
		let input_step = $(attribute.container).closest(".step").data("step");
		if (current_step !== input_step) {
			return false;
		}
	}).on("afterValidate", function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
			let current_step = $(this).data("step");
			$.each(errorAttributes, function(k, v){
				$(v.container).closest(".faqedit-block").addClass("feb-open");
			});
            $("html, body").animate({
                scrollTop: $(this).find("div[data-step=\'" + current_step + "\'] .has-error").first().offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function (event) {
		let current_step = $(this).data("step");
		let self = $(this);
		if ($(this).find("div[data-step=\'" + current_step + "\']").hasClass("file-input-step")) {
		    let returnValue = true;
		    $(".file-upload-input").each(function(index){
                if ($(this).fileinput("getFilesCount") > 0) {
                    $(this).fileinput("upload");
                    returnValue = false;
                    return false;
                }
		    });
		    if (!returnValue) {
		        return false;
		    }
        }
		if (current_step !== $(this).data("final-step")) {
			if(!isDraftProcessing) {
				$.when(makeDraft()).done(function() {
					self.find("div[data-step=\'" + current_step + "\']").removeClass("active");
					$(".gig-steps li").removeClass("active");
					current_step++;
					self.data("step", current_step);
					self.find("div[data-step=\'" + current_step + "\']").addClass("active");
					$(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
					$("html, body").animate({
						scrollTop: 0
					}, "slow");
				});
			}
			return false;
		} else {
            // Live.postJob({
				// id: $("#product-id").val(),
				// label: $('#postproductform-title').val(),
				// url: '',
				// price: $('#jobpackage-basic-price').val() + " " + $('#postproductform-currency_code option:selected').val()
            // });
		}
	});

    $(".prev-step").on("click", function() {
    	let jobForm = $("#product-form");
		let current_step = jobForm.data("step");
		if (current_step > 1) {
			jobForm.find("div[data-step=\'" + current_step + "\']").removeClass("active");
			$(".gig-steps li").removeClass("active");
			current_step--;
			jobForm.data("step", current_step);
			jobForm.find("div[data-step=\'" + current_step + "\']").addClass("active");
			$(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
			$("html, body").animate({
				scrollTop: 0
			}, "slow");
		}
		return false;
    });

    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data) {
        let response = data.response;
        $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
        attachments++;
    }).on("filedeleted", function(event, key) {
        $(".images-container").find("input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() { 
        if(hasFileUploadError === false) {
            $("#product-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) { 
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });

    $(document).on("click", ".link-blue", function() {
        $(this).closest(".cgb-optbox").remove();
    });
JS;

$this->registerJs($script);