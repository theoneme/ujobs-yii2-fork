<?php

use yii\helpers\Html;

/**
 * @var $locales array
 */

$this->title = Yii::t('board', 'Create Product');
?>
<div class="profile">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?= Html::beginForm(['/board/product/create'], 'post', ['id' => 'job-locale-form'])?>
                <div class="tab-content nostyle-tab-content">
                    <div class="settings-content step tab-pane fade in active">
                        <div class="formgig-block" style="padding-top: 20px;">
                            <div class="row">
	                            <div class="col-md-4 col-sm-4 col-xs-12 leftset">
		                            <div class="form-group">
			                            <div class="set-title">
				                            <label>
					                            <?= Yii::t('board', 'Select the languages in which you want to post products')?>
				                            </label>
			                            </div>
		                            </div>
	                            </div>
	                            <div class="col-md-4 col-sm-4 col-xs-6 rightset">
		                            <?php
		                            $i = 1;
		                            foreach($locales as $locale => $title) {
			                            echo '<div class="chover">' .
				                            Html::radio('locales[]', $locale == Yii::$app->language, ['value' => $locale, 'id' => $locale, 'class' => 'radio-checkbox']) .
				                            Html::label(Yii::t('board', $title), $locale) .
				                            '</div>';
			                            if ($i == round(count($locales) / 2)){
				                            echo '</div><div class="col-md-4 col-sm-4 col-xs-6 rightset">';
			                            }
			                            $i++;
		                            } ?>
	                            </div>
                            </div>
                            <div class="notes col-md-6 col-md-offset-12">
                                <div class="notes-head">
                                    <div class="lamp text-center">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="notes-title"><?= Yii::t('board', 'You can select multiple languages')?></div>
                                </div>
                                <div class="notes-descr">
                                    <p>
                                        <?= Yii::t('board', 'If you select more languages, more clients  in different countries will be able to see your offer and thus increase your profits. And if you want to order services or buy products you will have more options to choose from.')?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="formgig-block">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <?=Html::submitButton(Yii::t('board', 'Save & Continue'), ['class' => 'btn-mid']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>