<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.07.2017
 * Time: 15:45
 */

use common\components\CurrencyHelper;
use common\modules\board\assets\ProductAmountAsset;
use common\modules\board\components\AdultPopup;
use common\modules\board\models\Product;
use frontend\assets\ProductAsset;
use frontend\components\GmapsViewWidget;
use frontend\components\product\OtherProducts;
use frontend\components\product\RecommendedProducts;
use frontend\components\product\ViewedProducts;
use frontend\components\SocialShareWidget;
use frontend\modules\account\components\MiniView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;

/**
 * @var Product $model
 * @var array $breadcrumbs
 * @var array $accessInfo
 * @var View $this
 */

ProductAsset::register($this);
ProductAmountAsset::register($this);

$tariffIcon = $model->getActiveTariffIcon();

?>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Product",
    "description": "<?= $model->translation->title ?>",
    "name": "<?= $this->title ?>",
    "image": "<?= $model->getThumb() ?>",
    "offers": {
        "@type": "Offer",
        "availability": "http://schema.org/InStock",
        "price": "<?= $model->price ?>",
        "priceCurrency": "<?= $model->currency_code ?>"
    }
}
</script>

    <div class="work-bg">
        <div class="nav-fixed">
            <div class="container-fluid">
                <div class="nav-work row">
                    <ul class="menu-work">
                        <li>
                            <a class="selected" href="#w-slides"><?= Yii::t('board', 'Overview') ?></a>
                        </li>
                        <li>
                            <a href="#w-info"><?= Yii::t('board', 'Description') ?></a>
                        </li>
                        <?php if ($model->shipping_info) { ?>
                            <li>
                                <a href="#w-extra"><?= Yii::t('board', 'Extra information') ?></a>
                            </li>
                        <?php } ?>
                        <?php if (!empty($model->lat) && !empty($model->long)) { ?>
                            <li>
                                <a href="#w-map"><?= Yii::t('board', 'Address') ?></a>
                            </li>
                        <?php } ?>
                        <?php if (count($model->indexedProductAttributes) > 0) { ?>
                            <li>
                                <a href="#w-delivery"><?= Yii::t('board', 'Terms of delivery') ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="work-like">
                        <?php
                        if (!Yii::$app->user->isGuest) {
                            echo Html::a('<i class="fa fa-heart-o" aria-hidden="true"></i>' . Yii::t('app', 'Favorites'), null, [
                                'class' => 'wl-box hint--bottom biglike toggle-favourite ' . ($model->isFavourite() ? 'active' : ''),
                                'data-hint' => Yii::t('app', 'Favourite'),
                                'data-hint-add' => Yii::t('app', 'Favourite'),
                                'data-hint-remove' => Yii::t('app', 'Favourite'),
                                'data-id' => $model->id,
                                'data-type' => 'product'
                            ]);
                        } ?>
                        <div class="wl-count text-center"><?= count($model->favourites) ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="work-content row">
                <div class="work-side col-md-8 col-sm-8 col-xs-12">
                    <div id="w-slides" class="work-section">
                        <div class="work-header">
                            <h1 class="work-title text-left">
                                <?= Html::encode($model->translation->title) ?>
                            </h1>
                            <?= Breadcrumbs::widget([
                                'itemTemplate' => "{link}/", // template for all links
                                'activeItemTemplate' => "&nbsp;{link}",
                                'links' => $breadcrumbs,
                                'options' => ['class' => 'breadcrumbs hidden-xs'],
                                'tag' => 'div'
                            ]); ?>
                        </div>
                        <div class="gallery-container" id="zoom">
                            <div class="zoom-desktop">
                                <div class="container">
                                    <div class="big-image-container">
                                        <a class="lens-image" data-lens-image="<?= $model->getThumb() ?>">
                                            <img src="<?= $model->getThumb('catalog') ?>" class="big-image lazy-load"
                                                 data-src="<?= $model->getThumb()?>" alt="<?=$model->category->translation->title?>" title="<?=$model->category->translation->title?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-prod-mob">
                                <div class="container">
                                    <div class="big-image-container slider-bigz">
                                        <?php
                                        if ($model->attachments) {
                                            foreach ($model->attachments as $attachment) { ?>
                                                <div data-lens-image="<?= $attachment->getThumb() ?>"
                                                     data-big-image="<?= $attachment->getThumb() ?>">
                                                    <?= Html::img($attachment->getThumb('catalog'), [
                                                        'alt' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                                        'title' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                                    ]) ?>
                                                </div>
                                            <?php }
                                        } else { ?>
                                            <div data-lens-image="<?= $model->getThumb() ?>"
                                                 data-big-image="<?= $model->getThumb() ?>">
                                                <?= Html::img($model->getThumb('catalog'), [
                                                    'alt' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                                    'title' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="thumbnails-container slider-navz">
                                <?php
                                if ($model->attachments) {
                                    foreach ($model->attachments as $attachment) { ?>
                                        <a href="#" class="thumbnail-wrapper"
                                           data-lens-image="<?= $attachment->getThumb() ?>"
                                           data-big-image="<?= $attachment->getThumb() ?>">
                                            <?= Html::img($attachment->getThumb('catalog'), [
                                                'alt' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                                'title' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                            ]) ?>
                                        </a>
                                    <?php }
                                } else { ?>
                                    <a href="#" class="thumbnail-wrapper" data-lens-image="<?= $model->getThumb() ?>"
                                       data-big-image="<?= $model->getThumb() ?>">
                                        <?= Html::img($model->getThumb('catalog'), [
                                            'alt' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                            'title' => Yii::t('board', 'Product {title} in category {category}', ['title' => $model->translation->title, 'category' => $model->category->translation->title]),
                                        ]) ?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $accessInfo,
                        'template' => 'mini_view_mobile',
                    ]) ?>

                    <div id="w-info" class="work-section">
                        <div class="head-work">
                            <?= Yii::t('board', 'About This Product') ?>
                        </div>
                        <div class="body-work mobile-description">
                            <?= preg_replace('/<a\s*.*?\s*href=\"(.*?)\".*?>(.*?)<\/a>/', "\\1", $model->translation->content) ?>
                            <div class="mobile-more-gradient">
                                <a class="mob-more-descr" href="#"><?= Yii::t('board', 'See more'); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php if (count($model->indexedProductAttributes) > 0) { ?>
                        <div id="w-extra" class="work-section">
                            <div class="head-work">
                                <?= Yii::t('board', 'Extra information') ?>
                            </div>
                            <div class="body-work">
                                <?php foreach ($model->formatAttributes() as $key => $attribute) { ?>
                                    <div class="ap-title">
                                        <?= $key ?>:
                                        <span><?= is_array($attribute) ? implode(', ', $attribute) : $attribute ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="work-flex-container">
                        <?php if (!empty($model->lat) && !empty($model->long)) { ?>
                            <div id="w-map" class="work-section map-flex-column">
                                <div class="head-work">
                                    <?= Yii::t('model', 'Address') ?>
                                </div>
                                <div class="body-work">
                                    <div class="ap-title"><?= Yii::t('model', 'Address') ?>
                                        : <?= $model->getAddress() ?></div>
                                    <br>
                                    <?= GmapsViewWidget::widget(['model' => $model]); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($model->shipping_info) { ?>
                            <div id="w-delivery" class="work-section">
                                <div class="head-work">
                                    <?= Yii::t('board', 'Terms of delivery') ?>
                                </div>
                                <div class="body-work">
                                    <p><?= Html::encode($model->shipping_info) ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ((boolean)$model->category->is_adult === true) { ?>
                            <p>
                                <i class="fa fa-exclamation-triangle text-danger"></i>
                                <span>
                                    <?= Yii::t('board', 'Adult Signature Required: You must be at least 21 years of age to purchase this product. Adult signature is required on delivery.') ?>
                                </span>
                            </p>
                        <?php } ?>
                        <?php if (!empty($model->tags)) { ?>
                            <div class="tags hidden-xs">
                                <?php foreach ($model->tags as $tag) { ?>
                                    <?php if (isset($tag->attrValue->translations[Yii::$app->language])) { ?>
                                        <?= Html::a('<h2>' . Html::encode($tag->attrValue->translations[Yii::$app->language]->title) . '</h2>', [
                                            '/board/category/tag', 'reserved_product_tag' => $tag->value_alias
                                        ]); ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <?php if ($model->amount > 0 && !hasAccess($model->user_id) && $model->secure) { ?>
                    <div class="add-to-cart-mob pos-bottom visible-xs">
                        <?php if ($model->status === Product::STATUS_ACTIVE) { ?>
                            <?= Html::a(Yii::t('board', 'Buy for {price}', ['price' => $model->getPrice()]), ['/store/cart/index'], [
                                'class' => 'btn-big add-to-cart',
                                'data-id' => $model->id,
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                'class' => 'btn-big add-to-cart',
                                'disabled' => 'disabled'
                            ]) ?>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="author-side col-md-4 col-sm-4 col-xs-12 hidden-xs">
                    <div class="aside-price">
                        <div class="ap-head text-left">
                            <div class="ap-type">
                                <span class="ap-typeprice"><?= $model->getPrice() ?></span>
                                <?= Html::encode($model->translation->title) ?>
                            </div>
                        </div>
                        <div class="ap-body show">
                            <?php foreach ($model->formatAttributes() as $key => $attribute) { ?>
                                <div class="ap-title">
                                    <?= $key ?>:
                                    <span><?= is_array($attribute) ? implode(', ', $attribute) : $attribute ?></span>
                                </div>
                            <?php } ?>
                            <div class="ap-title">
                                <?= Yii::t('model', 'Address') ?>:
                                <span><?= $model->lat && $model->long ? Html::encode($model->getAddress()) : Yii::t('yii', '(not set)') ?></span>
                            </div>
                            <div class="ap-title">
                                <?= Yii::t('board', 'In stock') ?>:
                                <span><?= $model->amount ?></span>
                            </div>
                            <?php if ($model->amount > 0 && $model->status === Product::STATUS_ACTIVE && !hasAccess($model->user_id) && $model->secure) { ?>
                                <div class="product-measure-amount-block" data-max="<?= $model->amount?>">
                                    <div class="product-amount-block">
                                        <a class="product-amount-minus">-</a>
                                        <a class="product-amount-plus">+</a>
                                        <input type="number" class="product-amount-input" name="product-amount" value="1">
                                    </div>
                                    <div class="product-measure-block">
                                        <?= $model->measure ? $model->measure->translation->title : Yii::t('board', 'pc')?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="ap-buttons text-center btn-prod">
                                <?php if ($model->amount > 0 && !hasAccess($model->user_id) && $model->secure) { ?>
                                    <?php if ($model->status === Product::STATUS_ACTIVE) { ?>
                                        <?= Html::a(
                                            Yii::t('app', 'Order for {price}', [
                                                'price' =>
                                                    CurrencyHelper::convertAndFormat(
                                                        $model->currency_code,
                                                        Yii::$app->params['app_currency_code'],
                                                        $model->price,
                                                        true,
                                                        0,
                                                        '{sl}<span class="product-price">{amount}</span>{sr}'
                                                    )
                                            ]),
                                            ['/store/cart/index'],
                                            ['class' => 'btn-big add-to-cart', 'data-id' => $model->id]
                                        ) ?>
                                    <?php } else { ?>
                                        <?= Html::a(Yii::t('app', 'Unavailable'), '#', [
                                            'class' => 'btn-big add-to-cart',
                                            'disabled' => 'disabled'
                                        ]) ?>
                                    <?php } ?>
                                <?php } ?>

                                <?= Html::a(Yii::t('board', 'Show Seller\'s Contacts'), null, [
                                    'class' => 'read-more',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#contacts'
                                ]); ?>
                                <?php if ((boolean)$model->category->is_adult === true) { ?>
                                    <p class="text-center">
                                        <i class="fa fa-exclamation-triangle text-danger"></i>
                                        <span>
                                            <?= Yii::t('board', 'Adult Signature Required: You must be at least 21 years of age to purchase this product. Adult signature is required on delivery.') ?>
                                        </span>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-card">
                        <div class="pcard-head">
                            <div class="pcard-title"><?= Yii::t('app', 'My portfolio') ?></div>
                            <?= Html::a(
                                Yii::t('app', 'View my portfolio'),
                                ['/account/portfolio/show', 'id' => $model->user_id],
                                ['class' => 'link-blue text-right']
                            ) ?>
                        </div>
                        <?php if (count($model->user->userPortfolios) > 0) { ?>
                            <a href="<?= Url::to(['/account/portfolio/show', 'id' => $model->user_id]) ?>"
                               class="portf-alias">
                                <ul data-portfolio="<?= Yii::t('app', 'View my portfolio') ?>">
                                    <?php foreach ($model->user->userPortfolios as $k => $portfolio) { ?>
                                        <li>
                                            <?= Html::img($portfolio->getThumb('catalog')) ?>
                                        </li>
                                        <?php if ($k === 2) break;
                                    } ?>
                                </ul>
                            </a>
                        <?php } else { ?>
                            <p>&nbsp;</p>
                            <div><?= Yii::t('labels', 'Portfolio is empty') ?></div>
                        <?php } ?>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $accessInfo,
                    ]) ?>

                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::current(['invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                            'twitterMessage' => Yii::t('board', 'Check out this product on {site}:', ['site' => Yii::$app->name]),
                            'mailItem' => [
                                'label' => $model->getLabel(),
                                'thumb' => $model->getThumb('catalog'),
                                'name' => $model->getSellerName()
                            ]
                        ]) ?>
                    </div>
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:100%;height:280px"
                         data-ad-client="ca-pub-7280089675102373"
                         data-ad-slot="8279148809">
                    </ins>
                </div>
            </div>
        </div>
    </div>
    <div class="work-sliders">
        <?= OtherProducts::widget(['userId' => $model->user->id, 'exceptId' => $model->id, 'type' => $model->type, 'sliderCount' => 6]) ?>
        <?= RecommendedProducts::widget(['category' => $model->category, 'exceptId' => $model->id, 'sliderCount' => 6]) ?>
        <?= ViewedProducts::widget(['type' => $model->type, 'sliderCount' => 5]) ?>
    </div>

    <?= \frontend\components\CrosslinkWidget::widget() ?>

<?php if ($accessInfo['allowed'] == false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>
    <div class="modal fade contact-user-form" id="contacts" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header contact-header">
                    <button type="button" class="close-contact" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="contact-head-avatar">
                        <?= Html::a(
                            Html::img($model->getSellerThumb()) . ($tariffIcon ? Html::img($tariffIcon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : ''),
                            $model->getSellerUrl()
                        ) ?>
                    </div>
                    <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['class' => 'contact-head-name']) ?>
                    <div class="contact-header-date text-center"><?= Yii::t('board', 'Member from {date}', ['date' => Yii::$app->formatter->asDate($model->created_at)]) ?></div>
                    <div class="contact-header-rate text-center">
                        <?= $model->profile ? $this->render('@frontend/widgets/views/rating', [
                            'rating' => $model->profile->rating / 2,
                            'size' => 14
                        ]) : '' ?>
                    </div>
                </div>
                <div class="modal-body contact-body">
                    <?php if ($model->user->phone !== null) {
                        echo Html::a($model->user->phone, 'tel:' . $model->user->phone, ['class' => 'contact-phone']);
                    } ?>

                    <?php
                    if ($model->profile !== null) {
                        if ($model->profile->public_email !== null) {
                            echo Html::a($model->profile->public_email, 'mailto:' . $model->profile->public_email, ['class' => 'contact-email']);
                        }
                    } else {
                        // TODO Здесь будет что-то для компании
                    }
                    ?>

                    <p class="text-center">
                        <?= Yii::t('board', 'Say, that you found his announcement on {site}', ['site' => 'uJobs']) ?>
                    </p>
                    <p class="warning text-center">
                        <span><?= Yii::t('board', 'Warning!') ?></span>
                        <?= Yii::t('board', 'Do not agree on deposit, if you are not sure on seller`s reliability. When paying with cash, check product right on receiving. If you use «Secure transaction», then check product before completing order, or right on receiving on shipping.') ?>
                    </p>
                    <?= Html::a(Yii::t('board', 'Contact seller'), ['/account/inbox/start-conversation', 'user_id' => $model->user_id], ['class' => 'button green no-size']) ?>
                </div>
            </div>
        </div>
    </div>

<?= AdultPopup::widget(['category' => $model->category]); ?>

<?php
$encodedName = Html::encode($model->getSellerName());
$escapedLabel = addslashes($model->getLabel());
$translations = json_encode([
    'see_less' => Yii::t('board', 'See less'),
    'see_more' => Yii::t('board', 'See more')
]);
$addToCartUrl = Url::to(['/store/cart/add']);
$currentUrl = Url::current([], true);
$basePrice = CurrencyHelper::convert($model->currency_code, Yii::$app->params['app_currency_code'], $model->price,true);

$script = <<<JS
	const basePrice = $basePrice;
	const translations = $translations;
	var slickDir = false;
    if($('body').hasClass('rtl')){
        slickDir = true;
    }
    if($(window).width()>992){
        $('#zoom .big-image').simpleLens({
            loading_image: '/images/new/loading.gif',
            //open_lens_event: 'click'
        });
         $('#zoom .thumbnails-container img').simpleGallery({
            loading_image: '/images/new/loading.gif',
            show_event: 'click'
        });
    } else {
        $(".slider-bigz").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            rtl: slickDir,
            asNavFor: ".slider-navz"
        });
        $(".slider-navz").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: ".slider-bigz",
            dots: false,
            centerMode: false,
            arrows: false,
            rtl: slickDir,
            focusOnSelect: true,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                 }
            ]
        });
    }
    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
    /*
	$(".slider-nav .slide-w").css("opacity",".3");
	$(".slider-nav .slide-w.slick-active").first().css("opacity","1");
	$('.slider-nav').on('afterChange', function(){
		$('.slider-nav .slide-w').css('opacity','.3');
		$('.slider-nav .slide-w.slick-active').first().css('opacity','1');
	});
	$('body').on('click','.slider-big', function(){
		$('.slider-nav .slide-w').css('opacity','.3');
		$('.slider-nav .slide-w').eq($('.slider-big .slick-active').index()).css('opacity','1');
	}).on('click','.slider-nav .slide-w',function () {
		$('.slider-nav .slide-w').css('opacity','.3');
		$('.slider-nav .slide-w.slick-active').first().css('opacity','1');*/
	$(document).on('click', '.mobile-btn', function(){
        $(this).closest('.work-section').toggleClass('mobile-open');
    });
    $(document).on('click', '.mob-more-descr', function(e){
        e.preventDefault();
        $(this).closest('.mobile-description').css('max-height','99999px');
        $(this).closest('.mobile-more-gradient').hide();
    });
    $(document).on('click', '.work-section .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.work-section').find('.mob-more-info').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { console.log(translations);
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
    $(document).on('click', '.author-intro .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.author-intro').find('.short-descr-desktop').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { 
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
    
    $(document).on("change", ".extra-trig", function() {
	    var type = $(this).data("type"),
	    	target = $(this).data("target");

        if(type === "checkbox") {
            var isChecked = $(this).is(":checked");

            $("#" + target).val(isChecked ? 1 : 0);
        } else if (type === "select") {
            var value = $(this).find("option:selected").val();
            $("#" + target).val(value);
        }
	});
    
    $(document).on('click', '.add-to-cart', function(){
        var id = $(this).attr('data-id'),
			self = $(this),
			quantity = parseInt($('input.product-amount-input').val()),
			options = $('#optionsForm').serialize();
        $.ajax({
            url: '$addToCartUrl',
            type: 'post',
            data: {id: id, quantity: quantity, options: options, entity: 'product'},
            success: function(response) {
                $('#cart-head-container').replaceWith(response);
                if(typeof Live !== 'undefined') {
                    Live.addToCart({
                        id: {$model->id},
                        thumb: '{$model->getThumb()}',
                        label: '{$escapedLabel}',
                        url: '{$currentUrl}',
                        price: '{$model->getPrice()}'
                    });
                }

				window.location.href = self.attr('href');
            },
        });
        return false;
    });

    function updateButtonPrices() {
        let amount = parseInt($('.product-amount-input').val());
        $('.product-price').each(function() {
            $(this).html(formatter.formatPrice(basePrice * amount));
        });
    }
    
    $(document).on("change", ".product-amount-input", function() {
        updateButtonPrices();
	});
JS;

$this->registerJs($script, \yii\web\View::POS_LOAD);