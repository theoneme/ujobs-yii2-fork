<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.07.2017
 * Time: 16:20
 */

use common\modules\board\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var Product $model
 * @var View $this
 */

?>
<td>
    <?= Html::a(Html::img($model->getThumb('catalog'), ['alt' => $model->getLabel(),'title' => $model->getLabel()]),
        ['/board/product/view', 'alias' => $model->alias],
        ['class' => 'tg-img']) ?>
</td>
<td>
    <?= Html::a($model->getLabel(), [
        '/board/product/view', 'alias' => $model->alias
    ], [
        'class' => 'tg-name',
        'data-pjax' => 0
    ]) ?>
</td>
<td><?= $model->click_count ?></td>
<td><?= $model->views_count ?></td>
<td><?= Yii::t('app', Yii::$app->params['languages'][$model->locale]) ?></td>
<td>
    <div class="tg-opt text-left">
        <div class="tg-optbut text-center">
            <i class="fa fa-caret-down"></i>
        </div>
        <div class="tg-opt-block">
            <a data-pjax="0"
               href="<?= Url::to(['/board/product/update', 'id' => $model->id]) ?>"><?= Yii::t('board', 'Edit') ?></a>
            <?php if ($model->status == Product::STATUS_ACTIVE) { ?>
                <a data-pjax="0"
                   href="<?= Url::to(['/board/product/pause', 'id' => $model->id]) ?>"><?= Yii::t('board', 'Pause') ?></a>
            <?php } ?>
            <?php if ($model->status == Product::STATUS_PAUSED) { ?>
                <a data-pjax="0"
                   href="<?= Url::to(['/board/product/resume', 'id' => $model->id]) ?>"><?= Yii::t('board', 'Resume') ?></a>
            <?php } ?>
            <a data-pjax="0" data-method="post" href="<?= Url::to(['/board/product/delete', 'id' => $model->id]) ?>"
               data-confirm="<?= Yii::t('board', 'Are you sure?') ?>"
               class="gig-del"><?= Yii::t('board', 'Delete') ?></a>
        </div>
    </div>
</td>