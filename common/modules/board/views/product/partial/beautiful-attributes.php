<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.02.2017
 * Time: 17:00
 */

use common\models\DynamicForm;

/* @var array $attributes */
/* @var DynamicForm $model */

?>

<?php if (!empty($attributes)) { ?>
    <div class="formgig-block step1-hid-tabs">
        <div class="row set-item">
            <div class="form-group">
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        <label class="control-label"
                               for="postproductform-attributes"><?= Yii::t('board', 'Attributes') ?></label>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    <div class="vert-gig-tabs">
                        <div class="vert-gig-tabs-ul">
                            <ul class="step1-tabs">
                                <?php foreach ($attributes as $k => $attribute) { ?>
                                    <li class="<?= $k == 0 ? 'active' : '' ?>">
                                        <a href="#a_<?= $attribute->id ?>" data-toggle="tab">
                                            <?= $attribute->translation->title ?>
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="vert-gig-tabs-pane">
                            <div class="tab-content nostyle-tab-content">
                                <?php foreach ($attributes as $k => $attribute) { ?>
                                    <div class="step1-tabscont tab-pane fade in <?= $k == 0 ? 'active' : '' ?> "
                                         id="a_<?= $attribute->id ?>">
                                        <div class="title">
                                            <?= $attribute->translation->description ?>
                                        </div>
                                        <?= $this->render($attribute->type, [
                                            'values' => $attribute->values,
                                            'model' => $model,
                                            'attribute' => $attribute
                                        ]) ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="notes col-md-6 col-md-offset-12">
                <div class="notes-head">
                    <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                    <div class="notes-title"><?= Yii::t('board', 'Make your product easier to find') ?></div>
                </div>
                <div class="notes-descr">
                    <p>
                        <?= Yii::t('board', 'By defining your product properly it will be easier for buyers to find and buy.') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

