<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.07.2017
 * Time: 16:21
 */

use yii\helpers\Html;
use common\models\Job;
use yii\helpers\Url;
use yii\web\View;
/* @var Job $model
 * @var View $this
 */
?>
<div class="jm-option">
    <div class="jm-img">
        <?= Html::a(Html::img($model->getThumb('catalog')),
            ['/board/tender/view', 'alias' => $model->alias])?>
    </div>
    <div class="jm-name">
        <?= Html::a($model->translation->title, [
            '/board/tender/view', 'alias' => $model->alias
        ],[
            'data-pjax' => 0
        ]) ?>
        <div class="tg-opt text-left">
            <div class="tg-optbut text-center">
                <i class="fa fa-caret-down"></i>
            </div>
            <div class="tg-opt-block">
                <a data-pjax="0" href="<?=Url::to(['/board/tender/update', 'id' => $model->id])?>"><?=Yii::t('board', 'Edit')?></a>
                <a data-method="post" data-pjax="0" href="<?=Url::to(['/board/tender/delete', 'id' => $model->id])?>" data-confirm="<?=Yii::t('board', 'Are you sure?')?>" class="gig-del"><?=Yii::t('board', 'Delete')?></a>
            </div>
        </div>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('board', 'Clicks')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->click_count ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('board', 'Views')) ?>
    </div>
    <div class="jm-optionParam">
        <?= $model->views_count ?>
    </div>
</div>
<div class="jm-option">
    <div class="jm-optionName">
        <?= mb_strtoupper(Yii::t('board', 'Language')) ?>
    </div>
    <div class="jm-optionParam">
        <?= Yii::t('board', Yii::$app->params['languages'][$model->locale]) ?>
    </div>
</div>