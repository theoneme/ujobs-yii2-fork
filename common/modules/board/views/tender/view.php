<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 20.07.2017
 * Time: 15:45
 */

use common\modules\board\models\Product;
use frontend\assets\ProductAsset;
use frontend\components\SocialShareWidget;
use common\modules\board\models\frontend\ProductTenderResponseForm;
use frontend\modules\account\components\MiniView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/**
 * @var Product $model
 * @var array $breadcrumbs
 * @var array $accessInfo
 * @var View $this
 * @var ProductTenderResponseForm $tenderResponseForm
 * @var array $contactAccessInfo
 * @var array $accessInfo
 */

$price = $model->contract_price
    ? Yii::t('app', 'contract') :
    Yii::t('board', '{price} per {amount} {measure}', [
        'price' => $model->getPrice(),
        'amount' => 1,
        'measure' => $model->measure ? $model->measure->translation->title : Yii::t('board', 'pc')
    ]);
ProductAsset::register($this);
$this->registerCssFile('/css/new/profile.css');

if (isGuest()) {
    $respondParams = [
        'class' => 'btn-big',
        'data-toggle' => 'modal',
        'data-id' => $model->id,
        'data-target' => '#ModalSignup'
    ];
} else {
    if ($accessInfo['allowed'] === true) {
        if ($contactAccessInfo['allowed'] === true) {
            $respondParams = [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-id' => $model->id,
                'data-target' => '#PriceModal'
            ];
        } else {
            $respondParams = [
                'class' => 'btn-big',
                'data-toggle' => 'modal',
                'data-id' => $model->id,
                'data-target' => '#tariffContactModal'
            ];
        }
    } else {
        $respondParams = [
            'class' => 'btn-big',
            'data-toggle' => 'modal',
            'data-id' => $model->id,
            'data-target' => '#tariffModal'
        ];
    }
}

?>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Product",
    "description": "<?= $model->translation->title ?>",
    "name": "<?= $this->title ?>",
    "image": "<?= $model->getThumb() ?>",
    "offers": {
        "@type": "Offer",
        "availability": "http://schema.org/InStock",
        "price": "<?= $model->price ?>",
        "priceCurrency": "<?= $model->currency_code ?>"
    }
}
</script>

    <div class="work-bg">
        <div class="nav-fixed">
            <div class="container-fluid">
                <div class="nav-work row">
                    <ul class="menu-work">
                        <li>
                            <a class="selected" href="#w-slides"><?= Yii::t('board', 'Overview') ?></a>
                        </li>
                        <li>
                            <a href="#w-info"><?= Yii::t('board', 'Description') ?></a>
                        </li>
                        <?php if ($model->shipping_info) { ?>
                            <li>
                                <a href="#w-delivery"><?= Yii::t('board', 'Terms of delivery') ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="work-content row">
                <div class="work-side col-md-8 col-sm-8 col-xs-12">
                    <div id="w-slides" class="work-section">
                        <div class="work-header">
                            <h1 class="work-title text-left">
                                <?= Html::encode($model->translation->title) ?>
                            </h1>
                            <?= Breadcrumbs::widget([
                                'itemTemplate' => "{link}/", // template for all links
                                'activeItemTemplate' => "&nbsp;{link}",
                                'links' => $breadcrumbs,
                                'options' => ['class' => 'breadcrumbs hidden-xs'],
                                'tag' => 'div'
                            ]); ?>
                        </div>
                        <div class="gallery-container" id="zoom">
                            <div class="zoom-desktop">
                                <div class="container">
                                    <div class="big-image-container">
                                        <a class="lens-image" data-lens-image="<?= $model->getThumb() ?>">
                                            <img src="<?= $model->getThumb('catalog') ?>" class="big-image lazy-load"
                                                 data-src="<?= $model->getThumb() ?>" alt="<?= Html::encode($model->translation->title) ?>" title="<?= Html::encode($model->translation->title) ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-prod-mob">
                                <div class="container">
                                    <div class="big-image-container slider-bigz">
                                        <?php
                                        if ($model->attachments) {
                                            foreach ($model->attachments as $attachment) { ?>
                                                <div data-lens-image="<?= $attachment->getThumb() ?>"
                                                     data-big-image="<?= $attachment->getThumb() ?>">
                                                    <?= Html::img($attachment->getThumb('catalog'), [
                                                        'alt' => $model->translation->title,
                                                        'title' => $model->translation->title,
                                                    ]) ?>
                                                </div>
                                            <?php }
                                        } else { ?>
                                            <div data-lens-image="<?= $model->getThumb() ?>"
                                                 data-big-image="<?= $model->getThumb() ?>">
                                                <?= Html::img($model->getThumb('catalog'), [
                                                    'alt' => $model->translation->title,
                                                    'title' => $model->translation->title
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="thumbnails-container">
                                <?php
                                if ($model->attachments) {
                                    foreach ($model->attachments as $attachment) { ?>
                                        <a href="#" class="thumbnail-wrapper"
                                           data-lens-image="<?= $attachment->getThumb() ?>"
                                           data-big-image="<?= $attachment->getThumb() ?>">
                                            <?= Html::img($attachment->getThumb('catalog'), [
                                                'alt' => $model->translation->title,
                                                'title' => $model->translation->title
                                            ]) ?>
                                        </a>
                                    <?php }
                                } else { ?>
                                    <a href="#" class="thumbnail-wrapper" data-lens-image="<?= $model->getThumb() ?>"
                                       data-big-image="<?= $model->getThumb() ?>">
                                        <?= Html::img($model->getThumb('catalog'), [
                                            'alt' => $model->translation->title,
                                            'title' => $model->translation->title,
                                        ]) ?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $accessInfo,
                        'template' => 'mini_view_mobile',
                    ]) ?>

                    <div id="w-info" class="work-section">
                        <div class="head-work">
                            <?= Yii::t('board', 'About This Request') ?>
                        </div>
                        <div class="body-work mobile-description">
                            <?= preg_replace('/<a\s*.*?\s*href=\"(.*?)\".*?>(.*?)<\/a>/', "\\1", $model->translation->content) ?>
                            <div class="mobile-more-gradient">
                                <a class="mob-more-descr" href="#"><?= Yii::t('board', 'See more'); ?></a>
                            </div>
                        </div>
                        <div id="map"></div>
                    </div>
                    <?php if ($model->shipping_info) { ?>
                        <div id="w-delivery" class="work-section">
                            <div class="head-work">
                                <?= Yii::t('board', 'Terms of delivery') ?>
                            </div>
                            <div class="body-work">
                                <p><?= Html::encode($model->shipping_info) ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (!empty($model->tags)) { ?>
                        <div class="tags hidden-xs">
                            <?php foreach ($model->tags as $tag) {
                                echo Html::a('<h2>' . Html::encode($tag->value) . '</h2>', ['/board/category/tag', 'reserved_product_tag' => $tag->value_alias]);
                            } ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if (!hasAccess($model->user_id)) { ?>
                    <div class="add-to-cart-mob pos-bottom visible-xs">
                        <?php if ($model->status === Product::STATUS_ACTIVE) { ?>
                            <?= Html::a(Yii::t('board', 'To respond'), null, $respondParams) ?>
                        <?php } else { ?>
                            <?= Html::a(Yii::t('board', 'Unavailable'), '#', [
                                'class' => 'btn-big add-to-cart',
                                'disabled' => 'disabled'
                            ]) ?>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="author-side col-md-4 col-sm-4 col-xs-12 hidden-xs">
                    <div class="aside-price">
                        <div class="ap-head text-center">
                            <div class="ap-type">
                                <?= Html::encode($model->translation->title) ?>
                            </div>
                        </div>
                        <div class="ap-head text-center">
                            <div class="ap-type">
                                <?= Yii::t('app', 'Price') ?>:
                                <span class="ap-typeprice"><?= $price ?></span>
                            </div>
                        </div>
                        <div class="ap-body show">
                            <div class="ap-title">
                                <?= Yii::t('board', 'Required') ?>:
                                <span>
                                    <?= $model->amount . ' ' . ($model->measure ? $model->measure->translation->title : Yii::t('board', 'pc'))?>
                                </span>
                            </div>
                            <?php if (array_key_exists('shipping_type', $model->productAttributes)) { ?>
                                <div class="ap-info">
                                    <div class="ap-info-item">
                                        <?= Yii::t('board', 'Delivery type') ?>:
                                        <div class="delivery-type door-to-door">
                                            <?= $model->productAttributes['shipping_type']->attrValueDescription->title ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if (array_key_exists('condition', $model->productAttributes)) { ?>
                                <div class="ap-title">
                                    <?= Yii::t('board', 'Condition') ?>:
                                    <span><?= Html::encode($model->productAttributes['condition']->value) ?></span>
                                </div>
                            <?php } ?>
                            <?php if (array_key_exists('brand_a', $model->productAttributes)) { ?>
                                <div class="ap-title">
                                    <?= Yii::t('board', 'Brand') ?>:
                                    <span><?= Html::encode($model->productAttributes['brand_a']->value) ?></span>
                                </div>
                            <?php } ?>
                            <?php if (array_key_exists('material', $model->productAttributes)) { ?>
                                <div class="ap-title">
                                    <?= Yii::t('board', 'Material') ?>:
                                    <span><?= Html::encode($model->productAttributes['material']->value) ?></span>
                                </div>
                            <?php } ?>
                            <?php if (array_key_exists('color_a', $model->productAttributes)) { ?>
                                <div class="ap-title">
                                    <?= Yii::t('board', 'Color') ?>:
                                    <span><?= Html::encode($model->productAttributes['color_a']->value) ?></span>
                                </div>
                            <?php } ?>
                            <div class="ap-buttons text-center btn-prod">
                                <?php if (!hasAccess($model->user_id)) { ?>
                                    <?php if ($model->status === Product::STATUS_ACTIVE) { ?>
                                        <?= Html::a(Yii::t('board', 'To respond'), null, $respondParams) ?>
                                    <?php } else { ?>
                                        <?= Html::a(Yii::t('board', 'Unavailable'), '#', [
                                            'class' => 'btn-big add-to-cart',
                                            'disabled' => 'disabled'
                                        ]) ?>
                                    <?php } ?>
                                <?php } ?>

                                <?= Html::a(Yii::t('board', 'Show Customer`s Contacts'), null, [
                                    'class' => 'read-more',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#contacts'
                                ]); ?>
                            </div>
                        </div>
                    </div>

                    <?= MiniView::widget([
                        'user' => $model->user,
                        'accessInfo' => $accessInfo,
                    ]) ?>

                    <div class="work-share text-center">
                        <?= SocialShareWidget::widget([
                            'url' => Url::current(['invited_by' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->getCurrentId() : null], true),
                            'twitterMessage' => Yii::t('app', 'Check out this request on {site}:', ['site' => Yii::$app->name]),
                            'mailItem' => [
                                'label' => $model->getLabel(),
                                'thumb' => $model->getThumb('catalog'),
                                'name' => $model->getSellerName()
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ($accessInfo['allowed'] == false && isset($accessInfo['heading'])) { ?>
    <div class="modal fade" id="tariffModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="form-title"><?= $accessInfo['heading'] ?></div>
                </div>
                <div class="modal-body">
                    <p style="font-size: 18px; text-align: center; line-height: 28px; margin-top: 15px;">
                        <?= $accessInfo['body'] ?>
                    </p>
                    <?= $accessInfo['button'] ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <div class="modal fade contact-user-form" id="contacts" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header contact-header">
                    <button type="button" class="close-contact" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="contact-head-avatar">
                        <?= Html::a(
                            Html::img($model->user->getThumb()) . (!empty($model->user->activeTariff) ? Html::img($model->user->activeTariff->tariff->icon, ['class' => 'tariff-icon','alt' => Yii::t('app','Tariff'), 'title' => Yii::t('app','Tariff')]) : ''),
                            $model->getSellerUrl()
                        ) ?>
                    </div>
                    <?= Html::a(Html::encode($model->getSellerName()), $model->getSellerUrl(), ['class' => 'contact-head-name']) ?>
                    <div class="contact-header-date text-center"><?= Yii::t('board', 'Member from {date}', ['date' => Yii::$app->formatter->asDate($model->created_at)]) ?></div>
                    <div class="contact-header-rate text-center">
                        <?= $this->render('@frontend/widgets/views/rating', [
                            'rating' => $model->profile->rating / 2,
                            'size' => 14
                        ]) ?>
                    </div>
                </div>
                <div class="modal-body contact-body">
                    <?php if ($model->user->phone !== null) { ?>
                        <a href="tel:<?= $model->user->phone ?>" class="contact-phone">
                            <?= $model->user->phone ?>
                        </a>
                    <?php } ?>

                    <?php if ($model->user->email !== null) { ?>
                        <a class="contact-email" href="mailto:<?= $model->user->email ?>"><?= $model->user->email ?></a>
                    <?php } ?>

                    <p class="text-center">
                        <?= Yii::t('board', 'Say, that you found his announcement on {site}', ['site' => 'uJobs']) ?>
                    </p>
                    <p class="warning text-center">
                        <span><?= Yii::t('board', 'Warning!') ?></span>
                        <?= Yii::t('board', 'Do not agree on deposit, if you are not sure on seller`s reliability. When paying with cash, check product right on receiving. If you use «Secure transaction», then check product before completing order, or right on receiving on shipping.') ?>
                    </p>
                    <?= Html::a(Yii::t('board', 'Contact seller'), ['/account/inbox/start-conversation', 'user_id' => $model->user_id], ['class' => 'button green no-size']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="PriceModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-title"><?= Yii::t('app', 'Send response') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'tender-price-form',
                        'action' => ['/board/tender/respond'],
                        'options' => [
                            'class' => 'row',
                            'data-pjax' => 0,
                            'style' => 'margin: 0;'
                        ]
                    ]); ?>
                    <?php if ($tenderResponseForm->scenario == ProductTenderResponseForm::SCENARIO_CONTRACT_PRICE) { ?>
                        <?= $form->field($tenderResponseForm, 'price', [
                            'template' => "{input}\n{error}",
                            'options' => ['class' => 'form-group offer-price', 'enctype' => 'multipart/form-data']
                        ])->textInput([
                            'placeholder' => Yii::t('app', 'Offer your price on this request'),
                            'type' => 'number'
                        ]) ?>
                    <?php } ?>
                    <?= $form->field($tenderResponseForm, 'quantity', [
                        'template' => "{input}\n{error}",
                        'options' => ['class' => 'form-group offer-price', 'enctype' => 'multipart/form-data']
                    ])->textInput([
                        'placeholder' => Yii::t('app', 'Quantity of products you are willing to provide'),
                        'type' => 'number'
                    ]) ?>
                    <?= $form->field($tenderResponseForm, 'id')->hiddenInput(['value' => $model->id])->label(false) ?>
                    <?= $form->field($tenderResponseForm, 'maxQuantity')->hiddenInput()->label(false) ?>
                    <div class="col-md-12 white-mes-bg">
                        <?= $form->field($tenderResponseForm, 'content', ['template' => "{input}\n{error}"])->textarea([
                            'class' => 'readsym',
                            'placeholder' => Yii::t('app', 'Tell a little about yourself. Why choose you?')
                        ]) ?>
                        <div class="white-mes-bottom text-left row">
                            <div class="counttext">
                                <span class="ctspan">0</span> / 500
                            </div>
                            <div class="fileup">
                                <label class="file-upload">
                                    <span class="button text-center hint--top-right"
                                          data-hint="<?= Yii::t('account', 'Attach files. {mb}MB max.', ['mb' => 30]) ?>">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </span>
                                    <?= $form->field($tenderResponseForm, 'uploadedFiles[]')->fileInput([
                                        'id' => 'file-input',
                                        'multiple' => true
                                    ])->label(false) ?>
                                    <mark style="background-color: #fff"></mark>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-big width100']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="spinner-bg">
                <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            </div>
        </div>
    </div>

<?php if ($accessInfo['allowed'] === false && !isGuest()) { ?>
    <?= $this->render('@frontend/views/site/partial/response-tariff-modal', [
        'model' => $model,
        'accessInfo' => $accessInfo,
        'profile' => Yii::$app->user->identity->profile
    ]) ?>
<?php } ?>

<?php if ($contactAccessInfo['allowed'] == false) { ?>
    <?= $this->render('@frontend/views/site/partial/contact-tariff-modal') ?>
<?php } ?>

<?php $encodedName = Html::encode($model->getSellerName());
$escapedLabel = addslashes($model->getLabel());
$translations = json_encode([
    'see_less' => Yii::t('board', 'See less'),
    'see_more' => Yii::t('board', 'See more')
]);
$responseUrl = Url::to(['/board/tender/respond']);
$currentUrl = Url::current([], true);

$script = <<<JS
	const translations = $translations;
    if($(window).width()>992){
        $('#zoom .big-image').simpleLens({
            loading_image: '/images/new/loading.gif',
            //open_lens_event: 'click'
        });
         $('#zoom .thumbnails-container img').simpleGallery({
            loading_image: '/images/new/loading.gif',
            show_event: 'click'
        });
    } else {
        var slickDir = false;
        if($('body').hasClass('rtl')){
            slickDir = true;
        }
        $(".slider-bigz").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            rtl: slickDir,
            asNavFor: ".slider-navz"
        });
        $(".slider-navz").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: ".slider-bigz",
            dots: false,
            centerMode: false,
            arrows: false,
            rtl: slickDir,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },{
		        breakpoint: 600,
		        settings: {
		            slidesToShow: 4,
		            slidesToScroll: 1
		        }
		    }, {
		        breakpoint: 480,
		        settings: {
		            slidesToShow: 3,
		            slidesToScroll: 1
		        }
		    }, {
		        breakpoint: 360,
		        settings: {
		            slidesToShow: 2,
		            slidesToScroll: 1
		        }
		     }
        ]
    });
    }
    $(document).ready(function () {
        if($('body').hasClass('rtl')) {
            $('.slick-slider').parent().attr('dir', 'rtl');
        }
    });
	$(document).on('click', '.mobile-btn', function(){
        $(this).closest('.work-section').toggleClass('mobile-open');
    });
    $(document).on('click', '.mob-more-descr', function(e){
        e.preventDefault();
        $(this).closest('.mobile-description').css('max-height','99999px');
        $(this).closest('.mobile-more-gradient').hide();
    });
    $(document).on('click', '.work-section  .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.work-section').find('.mob-more-info').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { console.log(translations);
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
    $(document).on('click', '.author-intro .mob-show-info', function(e){
        e.preventDefault();
        $(this).closest('.author-intro').find('.short-descr-desktop').toggleClass('open');
        if($(this).data('expanded') === 0) {
            $(this).html(translations.see_less);
            $(this).data('expanded', 1);
        } else { 
            $(this).html(translations.see_more);
            $(this).data('expanded', 0);
        }
    });
    
    $(document).on("change", ".extra-trig", function() {
	    var type = $(this).data("type"),
	    	target = $(this).data("target");

        if(type === "checkbox") {
            var isChecked = $(this).is(":checked");

            $("#" + target).val(isChecked ? 1 : 0);
        } else if (type === "select") {
            var value = $(this).find("option:selected").val();
            $("#" + target).val(value);
        }
	});
    
    $(document).on('submit', '#tender-price-form', function(e){
        $('#PriceModal .spinner-bg').show();
        
        let formData = new FormData($(this)[0]),
            self = $(this);
        $.ajax({
            url: '$responseUrl',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
                if(response.success) {
                    $('#PriceModal').modal('hide');
                }
                alertCall('top', response.success ? 'success' : 'error', response.message);

                $('#PriceModal .spinner-bg').hide();
            },
        });
        return false;
    });
    $(document).on('change', '#file-input', function(){
        var names = $.map($(this).prop('files'), function(val) { return val.name; });
        $(this).parent().siblings('mark').html('(' + names.length + ') ' + names.join(', '));
    });
JS;

$this->registerJs($script);