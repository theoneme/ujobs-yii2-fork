<?php

use common\helpers\FileInputHelper;
use common\helpers\ImperaviHelper;
use common\models\Attachment;
use frontend\assets\CropperAsset;
use frontend\assets\SelectizeAsset;
use frontend\components\WizardSteps;
use frontend\models\PostJobForm;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostJobForm
 * @var $exampleModel common\models\Job
 * @var $action string
 * @var $currencies array
 * @var $measures array
 */

CropperAsset::register($this);
SelectizeAsset::register($this);

$this->title = Yii::t('board', 'Create Product: ') . $model->title;
?>
    <div class="modal fade in" id="note" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="mobile-note-title"></div>
                </div>
                <div class="modal-body">
                    <div class="mobile-note-text"></div>
                    <div class="mobile-note-btn text-center">
                        <a class="button big green" href="#"><?= Yii::t('app', 'Continue') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= WizardSteps::widget(['steps' => [
    1 => Yii::t('app', 'Overview'),
    2 => Yii::t('app', 'Description'),
    3 => Yii::t('app', 'Publish')
], 'step' => 1]); ?>
    <div class="profile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'class' => 'formpost',
                            'data-step' => 1,
                            'data-final-step' => 3
                        ],
                        'id' => 'job-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]); ?>
                    <?= Html::hiddenInput('id', $model->product->id, ['id' => 'job-id']) ?>
                    <div class="tab-content nostyle-tab-content">
                        <div class="settings-content step tab-pane active" id="step1" data-step="1">
                            <div class="formgig-block">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label class="control-label" for="postjobform-title">
                                                    <?= Yii::t('model', 'Title') ?>
                                                </label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <?= $form->field($model, 'title', [
                                            ])->textInput([
                                                'class' => 'readsym',
                                                'placeholder' => Yii::t('board', 'Name of product that you need. For example: Videocard'),
                                                'data-action' => 'category-suggest'
                                            ])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('board', 'Name of product') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Describe what do you want to buy') ?>
                                        </p>
                                    </div>
                                    <div class="example">
                                        <p>
                                            <span><?= Yii::t('board', 'For example') ?>: </span>
                                            <?= Yii::t('board', 'Blue shirt') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row set-item">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                                            <div class="set-title">
                                                <label class="control-label" for="postproductform-category_id"><?= Yii::t('board', 'Category') ?></label>
                                                <div class="mobile-question" data-toggle="modal"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                                        'prompt' => ' -- ' . Yii::t('board', 'Select An Upper Category'),
                                                        'class' => 'form-control'
                                                    ])->label(false); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                                    <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postrequestform-parent_category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('board', 'Select A Category'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helper'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('board', 'Select A Category'),
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ],
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-md-offset-6 col-xs-12 select-margin 3rd-level">
                                                    <?= $form->field($model, 'subcategory_id')->widget(DepDrop::class, [
                                                        'pluginOptions' => [
                                                            'initialize' => true,
                                                            'depends' => ['postrequestform-category_id'],
                                                            'placeholder' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                                            'url' => Url::to(['/ajax/subcat']),
                                                            'params' => ['depdrop-helperx'],
                                                            'skipDep' => true,
                                                            'emptyMsg' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                                        ],
                                                        'pluginEvents' => [
                                                            "depdrop:change" => "function(event, id, value, count) { 
                                                                let container = $(this).closest('.3rd-level');
                                                                if(count > 0) {
                                                                    container.removeClass('hidden');
                                                                } else {
                                                                    container.addClass('hidden');
                                                                }
                                                            }",
                                                        ],
                                                        'options' => [
                                                            'class' => 'form-control',
                                                        ]
                                                    ])->label(false); ?>
                                                    <?= Html::hiddenInput('depdrop-helperx', $model->subcategory_id, ['id' => 'depdrop-helperx']); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="suggestions-block" class="col-md-12">

                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title">
                                            <?= Yii::t('board', 'Where will your request be?') ?>
                                        </div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Please select a category and subcategory most relevant for your request.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formgig-block">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <?= Html::submitButton(Yii::t('board', 'Save & Continue'), ['class' => 'btn-mid next-step', 'data-orientation' => 'next']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane settings-content step form-gig file-input-step" id="step2" data-step="2">
                            <div class="gig-head row">
                                <div>
                                    <div class="profileh"><?= Yii::t('board', 'Post a Request') ?></div>
                                </div>
                            </div>
                            <div class="formpost-block">
                                <div class="form-group">
                                    <div class="form-box">
                                        <?= $form->field($model, "description")->widget(Widget::class, ArrayHelper::merge(ImperaviHelper::getDefaultConfig(), [
                                            'options' => [
                                                'placeholder' => Yii::t('board', 'Describe product that you need.')
                                            ],
                                            'settings' => [
                                                'buttonsHide' => [
                                                    'deleted',
                                                    'horizontalrule',
                                                    'link',
                                                ]
                                            ]
                                        ]))->label(false); ?>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title">
                                            <?= Yii::t('board', 'Description of product that you need') ?>
                                        </div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Describe in more detail what exactly you want to buy. Provide information for the sellers.') ?> </p>
                                    </div>
                                    <div class="example">
                                        <p>
                                            <span><?= Yii::t('board', 'For example') ?>
                                                : </span><?= Yii::t('board', 'Blue shirt with size M.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formpost-block">
                                <div class="form-group">
                                    <div class="form-box">
                                        <div class="cgp-upload-files">
                                            <?= FileInput::widget(
                                                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                    'id' => 'file-upload-input',
                                                    'options' => ['accept' => 'image/*', 'multiple' => true, 'class' => 'file-upload-input'],
                                                    'pluginOptions' => [
                                                        'overwriteInitial' => false,
                                                        'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                                            return $var->getThumb();
                                                        }, $model->attachments) : [],
                                                        'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                                                        'previewThumbTags' => [
                                                            '{actions}' => '{actions}',
                                                        ],
                                                    ]
                                                ])
                                            ) ?>
                                            <div class="images-container">
                                                <?php foreach ($model->attachments as $key => $image) {
                                                    /* @var $image Attachment */
                                                    echo $form->field($image, "[{$key}]content")->hiddenInput([
                                                        'value' => $image->content,
                                                        'data-key' => 'image_init_' . $image->id
                                                    ])->label(false);
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('board', 'Add Photo') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'You may add a photo to your request. This will make your request more attractive.') ?>
                                        </p>
                                    </div>
                                    <div class="example">
                                        <p>
                                            <span><?= Yii::t('board', 'Note:') ?></span><?= Yii::t('board', 'If you can not upload a photo, our system will install a general picture or leave your profile without photo.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formpost-block">
                                <div class="form-group">
                                    <label><?= Yii::t('app', 'Price for:') ?></label>
                                    <div class="form-box money">
                                        <?= $form->field($model, 'measure_id')->dropDownList(
                                            ArrayHelper::merge($measures[$model->category_id] ?? [], [null => Yii::t('board', 'item')]),
                                            ['class' => 'form-control measure-select']
                                        )->label(false); ?>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('app', 'Price type') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Specify what you will pay for. Whether it is product item or a certain unit of measure.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="formpost-block">
                                <div class="form-group">
                                    <label> <?= Yii::t('board', 'What is your budget for this request?') ?> </label>
                                    <div class="form-box money">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <?= $form->field($model, "currency_code")->dropDownList($currencies, [
                                                    'class' => 'form-control'
                                                ])->label(false); ?>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <?= $form->field($model, 'price')->textInput(['placeholder' => '9 999', 'id' => 'price-budget'])->label(false); ?>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <?= $form->field($model, 'contract_price', ['template' => '{input}{label}'])
                                                    ->checkbox(['id' => 'contract-price-checkbox'], false)
                                                    ->label(Yii::t('app', 'Contract price'))
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('board', 'Set Your Budget') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'How much are you ready to pay for this product?') ?>
                                        </p>
                                        <div class="example">
                                            <p>
                                                <span><?= Yii::t('board', 'Recommendation:') ?></span>
                                                <?= Yii::t('board', 'We recommend to set your price. Requests with contract price receive responses 7 times less than requests with exact price.') ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="formpost-block">
                                <div class="form-group">
                                    <label><?= Yii::t('board', 'Quantity') ?></label>
                                    <div class="form-box money">
                                        <?= $form->field($model, "amount")->textInput([
                                            'type' => 'number',
                                            'class' => ' form-control'
                                        ])->label(false) ?>
                                    </div>
                                </div>
                                <div class="notes col-md-6 col-md-offset-12">
                                    <div class="notes-head">
                                        <div class="lamp text-center"><i class="fa fa-lightbulb-o" aria-hidden="true"></i></div>
                                        <div class="notes-title"><?= Yii::t('board', 'Quantity') ?></div>
                                    </div>
                                    <div class="notes-descr">
                                        <p>
                                            <?= Yii::t('board', 'Set quantity if you want to buy more than one item.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="create-gig-block row">
                                <div class="col-md-12">
                                    <?= Html::button(Yii::t('board', 'Back'), ['class' => 'btn-mid-gray prev-step', 'data-orientation' => 'prev']) ?>
                                    <?= Html::submitButton(Yii::t('board', 'Save & Continue'), ['class' => 'btn-mid fright next-step file-input-step', 'data-orientation' => 'next']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="settings-content tab-pane" id="step3" data-step="3">
                            <div class="publish-title text-center">
                                <?= Yii::t('board', 'It\'s almost ready...') ?>
                            </div>
                            <div class="publish-text text-center">
                                <?= Yii::t('board', "Let`s publish your request and <br/> attract the attention of sellers.") ?>
                            </div>
                            <div class="create-gig-block row">
                                <div class="col-md-12">
                                    <?= Html::button(Yii::t('board', 'Back'), ['class' => 'btn-mid-gray prev-step', 'data-orientation' => 'prev']) ?>
                                    <?= Html::submitButton(Yii::t('board', 'Publish Request'), ['class' => 'btn-mid fright']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?= $this->render('@frontend/views/common/crop-bg.php')?>

<?php if (isset($action) && $action === 'create') { ?>
    <?php $this->registerJs('
        function makeDraft() {
            return true;
        }
'); ?>

<?php } else { ?>
    <?php $this->registerJs('
        function makeDraft() {
            isDraftProcessing = false;
            return false;
        }
'); ?>
<?php } ?>

<?php $uid = Yii::$app->user->identity->getId();
$categorySuggestRoute = Url::toRoute('/ajax/category-suggest');
$attachments = count($model->attachments);
$measures = json_encode($measures);

$script = <<<JS
    let uid = {$uid};
    let isDraftProcessing = false;
    let attachments = {$attachments};
    let	measures = $measures;

    function loadMeasures(categoryId) {
        $('.measure-select option:not([value=""])').remove();
        $.each(measures[categoryId], function(index, value) {
            $('.measure-select').append('<option value="' + index + '">' + value + '</option>');
        });
    }
    
    $(document).on("change", "#postrequestform-category_id, #postrequestform-subcategory_id", function(e) {
        loadMeasures($(this).val());
    });
    
    let suggestTimer = null;
    $(document).on("keyup", "[data-action=category-suggest]", function() {
        let self = $(this);
        clearTimeout(suggestTimer);
        suggestTimer = setTimeout(function() {
            let val = self.val();
            $.get('{$categorySuggestRoute}',
                {entity: 'product', request: val},
                function(data) {
                    if (data.success === true) {
                        $('#suggestions-block').html(data.html);
                    } else {
                        $('#suggestions-block').html('');
                    }
                },
                "json"
            );
        }, 700);
    });
    
    $(document).on('click', '[data-action=suggestion-select]', function() {
        let parentCategoryInput = $('#postrequestform-parent_category_id');
        let categoryInput = $('#postrequestform-category_id');
        let subcategoryInput = $('#postrequestform-subcategory_id');
        let pId = $(this).data('p-id');
        let cId = $(this).data('c-id');
        let sId = $(this).data('s-id');
        
        categoryInput.attr('data-c-id', cId);
        subcategoryInput.attr('data-s-id', sId);
        parentCategoryInput.val(pId).change();
        
        return false;
    });
    
    $('#postrequestform-category_id').on('depdrop:afterChange', function(event, id, value) { 
        let cId = $(this).attr('data-c-id');
        if(cId) {
            $('#postrequestform-category_id').val(cId).change();
        }
    });
    
    $('#postrequestform-subcategory_id').on('depdrop:afterChange', function(event, id, value) {
        let sId = $(this).attr('data-s-id');
        if(sId) {
            $('#postrequestform-subcategory_id').val(sId)
        }
    });
    
    $(document).on('click', '.mobile-question', function() {
        let ntitle = $(this).closest('.formgig-block').find('.notes-title').html(),
            ntext = $(this).closest('.formgig-block').find('.notes-descr').html(),
            note = $('#note');
        note.find('.mobile-note-title').html(ntitle);
        note.find('.mobile-note-text').html(ntext);
        note.modal('show');
    }).on('click', '.mobile-note-btn a', function(e) {
        e.preventDefault();
        $('#note').modal('hide');
    });

    $(document).on("change", "#contract-price-checkbox", function() {
        if ($(this).prop("checked")) {
            $("#price-budget").parent().removeClass("has-error");
            $("#price-budget").siblings(".help-block").html("");
            $("#price-budget").val("");
        } else {
            $("#price-budget").prop("disabled", false);
        }
    });

    $(document).on("keyup", "#price-budget", function() {
        let value = $(this).val();
        value = parseInt(value.replace(/\s/g, ""));
        value = value > 0 ? value : 0;
        if (value === 0) {
            $("#contract-price-checkbox").prop("checked", true);
        } else {
            $("#contract-price-checkbox").prop("checked", false);
        }
    });

    $("#job-form").on("beforeValidateAttribute", function(event, attribute, messages) {
        let current_step = $(this).data("step");
        let input_step = $(attribute.container).closest(".step").data("step");
        if (current_step !== input_step) {
            return false;
        }
    }).on("afterValidate", function(event, messages, errorAttributes) {
        if (errorAttributes.length) {
            let current_step = $(this).data("step");
            $.each(errorAttributes, function(k, v) {
                $(v.container).closest(".faqedit-block").addClass("feb-open");
            });
            $("html, body").animate({
                scrollTop: $(this).find("div[data-step=\'" + current_step + "\'] .has-error").first().offset().top - 180
            }, 1000);
        }
    }).on("beforeSubmit", function(event) {
        let current_step = $(this).data("step");
        let self = $(this);
        if ($(this).find("div[data-step=\'" + current_step + "\']").hasClass("file-input-step")) {
            let returnValue = true;
            $(".file-upload-input").each(function(index) {
                if ($(this).fileinput("getFilesCount") > 0) {
                    $(this).fileinput("upload");
                    returnValue = false;
                    return false;
                }
            });
            if (!returnValue) {
                return false;
            }
        }
        if (current_step !== $(this).data("final-step")) {
            if (!isDraftProcessing) {
                $.when(makeDraft()).done(function() {
                    self.find("div[data-step=\'" + current_step + "\']").removeClass("active");
                    $(".gig-steps li").removeClass("active");
                    current_step++;
                    self.data("step", current_step);
                    self.find("div[data-step=\'" + current_step + "\']").addClass("active");
                    $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                });
            }
            return false;
        } else {
            if(typeof Live !== 'undefined') {
                Live.postTender({
                    label: $("#postrequestform-title").val(),
                    price: $("#price-budget").val() + " р.",
                    categoryId: $("#postrequestform-category_id option:selected").val()
                });
            }
        }
    });

    $(".prev-step").on("click", function() {
        let current_step = $("#job-form").data("step");
        if (current_step > 1) {
            $("#job-form").find("div[data-step=\'" + current_step + "\']").removeClass("active");
            $(".gig-steps li").removeClass("active");
            current_step--;
            $("#job-form").data("step", current_step);
            $("#job-form").find("div[data-step=\'" + current_step + "\']").addClass("active");
            $(".gig-steps li:nth-child(" + current_step + ")").addClass("active");
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        }
        return false;
    });

    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
        attachments++;
    }).on("filedeleted", function(event, key) {
        $(".images-container").find("input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $("#job-form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
        alertCall('top', 'error', msg);
    });

    $(".link-blue").on("click", function() {
        let target = $(this).data("target");

        $("#" + target).remove();
    });
    $("#contract-price-checkbox").trigger("change");
JS;

$this->registerJs($script);

