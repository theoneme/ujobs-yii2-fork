<?php

use backend\assets\MyCropAsset;
use backend\models\PostJobForm;
use common\helpers\FileInputHelper;
use common\models\Attachment;
use common\models\Job;
use frontend\components\GmapsInputWidget;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $rootCategoriesList array
 * @var $model PostJobForm
 * @var $measures array
 */
MyCropAsset::register($this);
?>
<div class="page-form">
    <?php Pjax::begin(['id' => 'new_job', 'enablePushState' => !$isAjax]) ?>
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => false,
            'class' => ($isAjax ? 'ajax-submit' : ''),
            'data-pj-container' => 'grid-container'
        ],
        'id' => 'job-form',
    ]); ?>
    <div class="row">
        <?= Html::hiddenInput('id', $model->product->id, ['id' => 'product-id']) ?>
        <?= $form->field($model, 'title', [
            'template' => '
                            <div class="col-md-2 col-sm-3 col-xs-3 leftset">
                                <div class="set-title">
                                    {label}
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-3 col-xs-3 rightset">
                                {input}
                                {error}
                            </div>
                        ',
            'options' => ['tag' => false]
        ])->textInput([
            'class' => 'readsym form-control',
            'placeholder' => Yii::t('app', 'I will do something I am really good at')
        ]) ?>
        <?= $form->field($model, 'locale', [
            'template' => '
                            <div class="col-md-2 col-sm-3 col-xs-3 leftset">
                                <div class="set-title">
                                    {label}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3 rightset">
                                {input}
                                {error}
                            </div>
                        ',
            'options' => ['tag' => false]
        ])->dropDownList(Yii::$app->params['languages'])->label('Язык товара') ?>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label"
                           for="postproductform-category_id"><?= Yii::t('board', 'Category') ?></label>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                        <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                            'prompt' => ' -- ' . Yii::t('board', 'Select An Upper Category'),
                            'class' => 'noscript-sel form-control'
                        ])->label(false); ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                        <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                            'pluginOptions' => [
                                'initialize' => true,
                                'depends' => ['postproductform-parent_category_id'],
                                'placeholder' => ' -- ' . Yii::t('board', 'Select A Category'),
                                'url' => Url::to(['/category/subcat']),
                                'params' => ['depdrop-helper']
                            ],
                            'options' => [
                                'class' => 'form-control noscript-sel',
                            ]
                        ])->label(false); ?>
                        <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-md-offset-6 col-xs-12 select-margin hidden 3rd-level">
                        <?= $form->field($model, 'subcategory_id')->widget(DepDrop::class, [
                            'pluginOptions' => [
                                'initialize' => true,
                                'depends' => ['postproductform-category_id'],
                                'placeholder' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                'url' => Url::to(['/category/subcat']),
                                'params' => ['depdrop-helperx']
                            ],
                            'pluginEvents' => [
                                "depdrop.change" => "function(event, id, value, count) { 
                                    let container = $(this).closest('.3rd-level');
                                    if(count > 0) {
                                        container.removeClass('hidden');
                                    } else {
                                        container.addClass('hidden');
                                    }
                                }",
                                "depdrop:change" => "function(event, id, value, count) { 
                                    let container = $(this).closest('.3rd-level');
                                    if(count > 0) {
                                        container.removeClass('hidden');
                                    } else {
                                        container.addClass('hidden');
                                    }
                                }",
                            ],
                            'options' => [
                                'class' => 'form-control noscript-sel',
                            ]
                        ])->label(false); ?>
                        <?= Html::hiddenInput('depdrop-helperx', $model->subcategory_id, ['id' => 'depdrop-helperx']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="attributes-info"></div>
    <div class="row">
        <?= $form->field($model, "description", [
            'template' => '
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        {label}
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    {input}
                    {error}
                </div>
            '])->widget(Widget::class, [
            'settings' => [
                'lang' => 'ru',
                'minHeight' => 200,
                'plugins' => [
                    'fullscreen',
                    'fontsize',
                    'fontcolor'
                ]
            ]
        ]);
        ?>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label"
                           for="postproductform-uploaded_images"><?= Yii::t('board', 'Photos') ?></label>
                    <?= Yii::t('board', '(Here you can upload photos)') ?>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <div class="cgp-upload-files">
                    <?= FileInput::widget(
                        ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                            'id' => 'file-upload-input',
                            'name' => 'uploaded_images[]',
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'dropZoneTitle' => Yii::t('app', 'Drag & drop photos here &hellip;'),
                                'overwriteInitial' => false,
                                'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                    return $var->getThumb();
                                }, $model->attachments) : [],
                                'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                            ]
                        ])
                    ) ?>
                    <div class="images-container">
                        <?php foreach ($model->attachments as $key => $image) {
                            /* @var $image Attachment */
                            echo $form->field($image, "[{$key}]content")->hiddenInput([
                                'value' => $image->content,
                                'data-key' => 'image_init_' . $image->id
                            ])->label(false);
                        } ?>
                    </div>
                </div>
            </div>
            <div class="notes col-md-6 col-md-offset-12">
                <div class="notes-head">
                    <div class="lamp text-center">
                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                    </div>
                    <div class="notes-title"><?= Yii::t('board', 'Photos') ?></div>
                </div>
                <div class="notes-descr">
                    <p>
                        <?= Yii::t('board', 'It’s photo time! You can upload as many images as you want.') ?>
                    </p>
                    <ul>
                        <li><?= Yii::t('board', 'Allowed formats: JPEG, JPG and PNG') ?></li>
                        <li><?= Yii::t('board', 'Dimensions: more - better') ?></li>
                        <li><?= Yii::t('board', 'Max File size: {count} MB', ['count' => 5]) ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label">
                        Цена за:
                    </label>
                    <div class="mobile-question" data-toggle="modal"></div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <?= $form->field($model, 'measure_id')->dropDownList(
                    ArrayHelper::merge($measures[$model->category_id] ?? [], [null => Yii::t('board', 'item')]),
                    ['class' => 'form-control measure-select']
                )->label(false); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label" for="postproductform-currency_code"><?= Yii::t('board', 'Price and currency') ?></label>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <div class="row">
                    <div class="col-md-6">
                        <div class="cgb-head" style="padding: 0"><?= Yii::t('board', 'Currency for price') ?></div>
                        <?= $form->field($model, "currency_code")->dropDownList($currencies, [
                            'class' => 'noscript-sel form-control'
                        ])->label(false); ?>
                    </div>
                    <div class="col-md-6">
                        <div class="cgb-head" style="padding: 0"><?= Yii::t('board', 'Price') ?></div>
                        <?= $form->field($model, "price")->textInput([
                            'type' => 'number',
                            'class' => ' form-control',
                            'placeholder' => '9 999'
                        ])->label(false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?= $form->field($model, "amount", [
            'template' => '
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    {label}
                        <div class="mobile-question" data-toggle="modal"></div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                {input}
                {error}
            </div>
        '])->textInput([
            'type' => 'number',
            'class' => ' form-control'
        ])->label(Yii::t('board', 'Quantity')) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'shipping_type', [
            'template' => '
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        {label}
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    {input}
                    {error}
                </div>
            '])->radioList($model->getShippingTypes(), [
            'class' => 'text-left',
        ]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, "shipping_info", [
            'template' => '
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        {label}
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    {input}
                    {error}
                </div>
            '])->textarea([
            'placeholder' => Yii::t('board', 'Set how product can be sent and who pays for shipping')
        ]) ?>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label"
                           for="postproductform-length"><?= Yii::t('board', 'Product sizes') ?></label>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <div class="row">
                    <div class="col-md-4">
                        <div class="cgb-head"
                             style="padding: 0"><?= Yii::t('board', 'Length') ?></div>
                        <?= $form->field($model, "length")->textInput([
                            'placeholder' => '100 ' . Yii::t('board', 'cm')
                        ])->label(false); ?>
                    </div>
                    <div class="col-md-4">
                        <div class="cgb-head"
                             style="padding: 0"><?= Yii::t('board', 'Width') ?></div>
                        <?= $form->field($model, "width")->textInput([
                            'placeholder' => '50 ' . Yii::t('board', 'cm')
                        ])->label(false); ?>
                    </div>
                    <div class="col-md-4">
                        <div class="cgb-head"
                             style="padding: 0"><?= Yii::t('board', 'Height') ?></div>
                        <?= $form->field($model, "height")->textInput([
                            'placeholder' => '200 ' . Yii::t('board', 'cm')
                        ])->label(false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                <div class="set-title">
                    <label class="control-label"
                           for="postproductform-address"><?= Yii::t('board', 'Address') ?></label>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                <?= GmapsInputWidget::widget(['model' => $model, 'inputOptions' => [
                    'placeholder' => Yii::t('board', 'Moscow, Novy Arbat 12')], 'modal' => false
                ]); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <?= $form->field($model, 'status', [
            'template' => '
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        {label}
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    {input}
                    {error}
                </div>
            '
        ])->radioList(Job::getStatusLabels(), [
            'class' => 'text-left',
        ]) ?>
    </div>
    <div class="container box box-success" style="padding:5px;">
        <?= Html::submitButton(Yii::t('app', 'Save'), [
            'id' => 'submit-button',
            'class' => 'btn btn-success pull-right'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php $attachments = count($model->attachments);
    $renderAttributesRoute = Url::toRoute('/board/admin/render-attributes');
    $measures = json_encode($measures);

    $script = <<<JS
        let attachments = $attachments;
    	let measures = $measures;
		$(".closeeditt").on("click", function(e) {
		    e.preventDefault();
		    let target = $(this).data("target"),
		        faqBlock = $("#" + target),
		        key = $(this).data("key"),
		        isNew = $(this).data("new");
		    if (isNew === true) {
		        faqBlock.remove();
		    } else {
		        let faqEditBlock = faqBlock.find("#faq-edit-" + key);

		        faqEditBlock.find(".feb-head:visible").trigger("click");
		    }
		});
		
        function loadAttributes(categoryId, productId) {
            $.get('$renderAttributesRoute', {
                category_id: categoryId,
                entity: productId,
            }, function(data) {
                $("#attributes-info").html(data);
            }, "html");
        }
        
        function loadMeasures(categoryId) {
            $('.measure-select option:not([value=""])').remove();
            $.each(measures[categoryId], function(index, value) {
                $('.measure-select').append('<option value="' + index + '">' + value + '</option>');
            });
        }
        
        $("#postproductform-category_id, #postproductform-subcategory_id").on("change", function(e) {
            let categoryId = $(this).find('option:selected').val();
            let productId = $('#product-id').val();
            loadAttributes(categoryId, productId);
            loadMeasures(categoryId);
        });
        $('#postproductform-parent_category_id').on("change", function(e) {
            $('#attributes-info').html('');
        });
        if($('#depdrop-helperx').val()) {
            loadAttributes($('#depdrop-helperx').val(), $('#product-id').val());
        } else if($('#depdrop-helper').val()) {
            loadAttributes($('#depdrop-helper').val(), $('#product-id').val());
        }  

        const input = $("#file-upload-input");
        let hasFileUploadError = false;
		input.on("fileuploaded", function(event, data, previewId, index) {
		    let response = data.response;
		    $(".images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
		    attachments++;
		}).on("filedeleted", function(event, key) {
		    $(".images-container input[data-key=" + key + "]").remove();
		}).on("filebatchuploadcomplete", function() {
            if (hasFileUploadError === false) {
                $(this).closest("form").submit();
            } else {
                hasFileUploadError = false;
            }
        }).on("fileuploaderror", function(event, data, msg) {
            hasFileUploadError = true;
            $('#' + data.id).find('.kv-file-remove').click();
        });
		
		$("#submit-button").on("click", function(e) {
		    if (input.fileinput("getFilesCount") > 0) {
		       input.fileinput("upload");
		        return false;
		    } else {
		        return true;
		    }
		});

		$(".link-blue").on("click", function() {
		    var target = $(this).data("target");

		    $("#" + target).remove();
		});
JS;

    $this->registerJs($script); ?>
    <?php Pjax::end() ?>
</div>