<?php

use common\components\CurrencyHelper;
use common\models\Message;
use common\modules\board\models\Product;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\board\models\search\ProductSearchAdmin */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('board', 'Products');
$this->params['breadcrumbs'][] = $this->title;
$sup_id = Yii::$app->params['supportId'];
$sentMessages = Message::find()
    ->joinWith(['conversationMembers'])
    ->select(['COUNT(message.id)', 'conversation_member.user_id as uid'])
    ->where(['message.user_id' => $sup_id])
    ->andWhere('conversation_member.user_id != message.user_id')
    ->indexBy('uid')
    ->groupBy('uid')
    ->asArray()
    ->column();
$unreadMessages = Message::find()
    ->joinWith(['conversationMembers'])
    ->select(['COUNT(message.id)', 'message.user_id as uid'])
    ->where('message.created_at > conversation_member.viewed_at')
    ->andWhere('conversation_member.user_id != message.user_id')
    ->andWhere(['conversation_member.user_id' => $sup_id])
    ->indexBy('uid')
    ->groupBy('uid')
    ->asArray()
    ->column();
?>

<div class="product-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?php \yii\widgets\Pjax::begin(['id' => 'grid-container']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'header' => 'Продавец',
                        'attribute' => 'username',
                        'value' => function ($model) {
                            /* @var $model Product*/
                            return Html::a(Html::img($model->getSellerThumb(), ['style' => 'width: 50px']) . "&nbsp;" . $model->getSellerName(),
                                ['/user/admin/update-ajax', 'id' => $model->user_id],
                                [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]
                            );
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Product*/
                            return $model->getStatusLabel(true);
                        },
                        'filter' => Product::getStatusLabels()
                    ],
                    [
                        'header' => 'Дата создания',
                        'format' => 'date',
                        'value' => function ($model) {
                            /* @var $model Product*/
                            return $model->created_at;
                        },
                    ],
                    [
                        'attribute' => 'title',
                        'value' => function($value) {
                            return StringHelper::truncate($value->getLabel(), 45);
                        }
                    ],
                    [
                        'attribute' => 'locale',
                        'header' => 'Язык',
                        'filter' => Yii::$app->params['languages']
                    ],
                    [
                        'label' => Yii::t('model', 'Photo'),
                        'format' => 'image',
                        'value' => function ($model) {
                            /* @var $model Product*/
                            return $model->getThumb();
                        },
                        'contentOptions' => ['class' => 'grid-image-container', 'style' => 'max-width: 40px;']
                    ],
                    [
                        'label' => Yii::t('app', 'Price'),
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model Product*/
                            return $model->contract_price ? 'Договорная' : CurrencyHelper::convertAndFormat($model->currency_code, 'RUB', $model->price);
                        },
                    ],
                    [
                        'header' => 'Письма',
                        'value' => function ($model) use ($sentMessages, $unreadMessages) {
                            /* @var $model Product*/
                            return (isset($sentMessages[$model->user->id]) ? $sentMessages[$model->user->id] : 0) . ' отправлено<br>' .
                                Html::a((isset($unreadMessages[$model->user->id]) ? $unreadMessages[$model->user->id] : 0) . ' не прочитано',
                                    Yii::$app->urlManagerFrontEnd->createUrl(['/account/inbox/start-support-conversation', 'user_id' => $model->user_id]),
                                    ['target' => '_blank', 'data-pjax' => 0]
                                );
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 150px; white-space: normal; text-align: center;'],
                        'options' => ['style' => 'width: 150px']
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'template' => Yii::$app->user->identity->isAdmin() ? '{update} {delete}' : '{update}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Update {0}', (new ReflectionClass($model))->getShortName()),
                                    'data-pjax' => 0,
                                    'class' => 'modal-edit'
                                ]);
                            }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'update':
                                    $url = Url::toRoute(["/board/admin/{$action}-tender", 'id' => $model->id, 'user_id' => $model->user_id]);
                                    break;
                                default:
                                    $url = Url::toRoute(["/board/admin/{$action}", 'id' => $model->id]);
                                    break;
                            }
                            return $url;
                        }
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>