<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.08.2017
 * Time: 14:46
 */

use backend\assets\MyCropAsset;
use common\helpers\FileInputHelper;
use common\models\Attachment;
use common\models\Job;
use frontend\models\PostJobForm;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $rootCategoriesList array
 * @var $model PostJobForm
 * @var $exampleModel common\models\Job
 * @var $action string
 * @var $currencies array
 * @var $measures array
 */

MyCropAsset::register($this);

$this->title = Yii::t('board', 'Create Request: ') . $model->title;
?>
    <div class="page-form">
        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'class' => 'formpost' . ($isAjax ? ' ajax-submit' : ''),
            ],
            'id' => 'job-form',
        ]); ?>
        <?= Html::hiddenInput('id', $model->product->id, ['id' => 'job-id']) ?>
        <div class="formgig-block">
            <div class="row set-item">
                <?= $form->field($model, 'title', [
                    'template' => '
                        <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                            <div class="set-title">
                                {label}
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                            {input}
                            {error}
                            <div class="counttext"><span class="ctspan">0</span> / 80 ' . Yii::t('app', 'Max') . '</div>
                        </div>'
                ])->textarea([
                    'class' => 'readsym form-control',
                    'placeholder' => Yii::t('app', 'I will do something I am really good at')
                ]) ?>
            </div>
        </div>
        <div class="formgig-block">
            <div class="row set-item">
                <div class="form-group">
                    <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                        <div class="set-title">
                            <label class="control-label"
                                   for="postjobform-category_id"><?= Yii::t('board', 'Category') ?></label>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                <?= $form->field($model, 'parent_category_id')->dropDownList($rootCategoriesList, [
                                    'prompt' => ' -- ' . Yii::t('board', 'Select An Upper Category'),
                                    'class' => 'form-control'
                                ])->label(false); ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 select-margin">
                                <?= $form->field($model, 'category_id')->widget(DepDrop::class, [
                                    'pluginOptions' => [
                                        'initialize' => true,
                                        'depends' => ['postrequestform-parent_category_id'],
                                        'placeholder' => ' -- ' . Yii::t('board', 'Select A Category'),
                                        'url' => Url::to(['/category/subcat']),
                                        'params' => ['depdrop-helper']
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                    ]
                                ])->label(false); ?>
                                <?= Html::hiddenInput('depdrop-helper', $model->category_id, ['id' => 'depdrop-helper']); ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-md-offset-6 col-xs-12 select-margin hidden 3rd-level">
                                <?= $form->field($model, 'subcategory_id')->widget(DepDrop::class, [
                                    'pluginOptions' => [
                                        'initialize' => true,
                                        'depends' => ['postrequestform-category_id'],
                                        'placeholder' => ' -- ' . Yii::t('board', 'Select A Subcategory'),
                                        'url' => Url::to(['/category/subcat']),
                                        'params' => ['depdrop-helperx']
                                    ],
                                    'pluginEvents' => [
                                        "depdrop:change" => "function(event, id, value, count) { 
                                            let container = $(this).closest('.3rd-level');
                                            if(count > 0) {
                                                container.removeClass('hidden');
                                            } else {
                                                container.addClass('hidden');
                                            }
                                        }",
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                    ]
                                ])->label(false); ?>
                                <?= Html::hiddenInput('depdrop-helperx', $model->subcategory_id, ['id' => 'depdrop-helperx']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="formpost-block">
            <div class="form-group">
                <div class="form-box">
                    <?= $form->field($model, "description")->widget(Widget::class, [
                        'settings' => [
                            'lang' => 'ru',
                            'minHeight' => 200,
                            'plugins' => [
                                'fullscreen',
                                'fontsize',
                                'fontcolor'
                            ]
                        ],
                        'options' => [
                            'placeholder' => Yii::t('board', 'Describe product that you need.')
                        ]
                    ])->label(false); ?>
                </div>
            </div>
        </div>
        <div class="formpost-block">
            <div class="form-group">
                <div class="form-box">
                    <div class="cgp-upload-files">
                        <?= FileInput::widget(
                            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                'id' => 'file-upload-input',
                                'name' => 'uploaded_images[]',
                                'options' => ['multiple' => true],
                                'pluginOptions' => [
                                    'dropZoneTitle' => Yii::t('app', 'Drag & drop photos here &hellip;'),
                                    'overwriteInitial' => false,
                                    'initialPreview' => !empty($model->attachments) ? array_map(function ($var) {
                                        return $var->getThumb();
                                    }, $model->attachments) : [],
                                    'initialPreviewConfig' => !empty($model->attachments) ? Yii::$app->utility->makeImageConfigFromObjects($model->attachments) : [],
                                ]
                            ])
                        ) ?>
                        <div id="images-container">
                            <?php foreach ($model->attachments as $key => $image) {
                                /* @var $image Attachment */
                                echo $form->field($image, "[{$key}]content")->hiddenInput([
                                    'value' => $image->content,
                                    'data-key' => 'image_init_' . $image->id
                                ])->label(false);
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="formpost-block">
            <div class="form-group">
                <label> <?= Yii::t('board', 'Price for:') ?> </label>
                <div class="form-box money">
                    <?= $form->field($model, 'measure_id')->dropDownList(
                        ArrayHelper::merge($measures[$model->category_id] ?? [], [null => Yii::t('board', 'item')]),
                        ['class' => 'form-control measure-select']
                    )->label(false); ?>
                </div>
            </div>
        </div>
        <div class="formpost-block">
            <div class="form-group">
                <label> <?= Yii::t('board', 'What is your budget for this request?') ?> </label>
                <div class="form-box money">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?= $form->field($model, "currency_code")->dropDownList($currencies, [
                                'class' => 'form-control'
                            ])->label(false); ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?= $form->field($model, 'price')->textInput(['placeholder' => '9 999', 'id' => 'price-budget'])->label(false); ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?= $form->field($model, 'contract_price', ['template' => '{input}{label}'])
                                ->checkbox(['id' => 'contract-price-checkbox'], false)
                                ->label(Yii::t('app', 'Contract price'))
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="formpost-block">
            <div class="form-group">
                <label><?= Yii::t('board', 'Quantity') ?></label>
                <div class="form-box money">
                    <?= $form->field($model, "amount")->textInput([
                        'type' => 'number',
                        'class' => ' form-control'
                    ])->label(false) ?>
                </div>
            </div>
        </div>
        <div class="formpost-block row">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->radioList(Job::getStatusLabels()) ?>
            </div>
        </div>
        <div class="container box box-success" style="padding:5px;">
            <?= Html::submitButton(Yii::t('app', 'Save'), [
                'id' => 'submit-button',
                'class' => 'btn btn-success pull-right'
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php if (isset($action) && $action == 'create') { ?>
    <?php $this->registerJs('
        function makeDraft() {
            return true;
        }
'); ?>

<?php } else { ?>
    <?php $this->registerJs('
        function makeDraft() {
            isDraftProcessing = false;
            return false;
        }
'); ?>
<?php } ?>

<?php $uid = Yii::$app->user->identity->getId();
$attachments = count($model->attachments);
$measures = json_encode($measures);
$script = <<<JS
    let uid = {$uid};
    let isDraftProcessing = false;
    let attachments = {$attachments};
    let	measures = $measures;
    
    function loadMeasures(categoryId) {
        $('.measure-select option:not([value=""])').remove();
        $.each(measures[categoryId], function(index, value) {
            $('.measure-select').append('<option value="' + index + '">' + value + '</option>');
        });
    }
    $(document).on("change", "#postrequestform-category_id, #postrequestform-subcategory_id", function(e) {
        loadMeasures($(this).val());
    });
    
    $(document).on("change", "#contract-price-checkbox", function() {
        if ($(this).prop("checked")) {
            $("#price-budget").parent().removeClass("has-error");
            $("#price-budget").siblings(".help-block").html("");
            $("#price-budget").val("");
        } else {
            $("#price-budget").prop("disabled", false);
        }
    });

    $(document).on("keyup", "#price-budget", function() {
        let value = $(this).val();
        value = parseInt(value.replace(/\s/g, ""));
        value = value > 0 ? value : 0;
        if (value === 0) {
            $("#contract-price-checkbox").prop("checked", true);
        } else {
            $("#contract-price-checkbox").prop("checked", false);
        }
    });

    let hasFileUploadError = false;
    $("#file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        $("#images-container").append("<input name=\'Attachment[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
        attachments++;
    }).on("filedeleted", function(event, key) {
        $("#images-container input[data-key=\'" + key + "\']").remove();
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $(this).closest("form").submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });

    $("#submit-button").on("click", function(e) {
        if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
            $("#file-upload-input").fileinput("upload");
            return false;
        } else {
            return true;
        }
    });

    $(".link-blue").on("click", function() {
        let target = $(this).data("target");

        $("#" + target).remove();
    });
    $("#contract-price-checkbox").trigger("change");
JS;

$this->registerJs($script);

