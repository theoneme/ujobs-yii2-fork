<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\board\models\Product
 * @var $measures array
 */

$this->title = Yii::t('board', 'Update {modelClass}: ', [
    'modelClass' => 'Product',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('board', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('board', 'Update');
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'measures' => $measures
    ]) ?>

</div>
