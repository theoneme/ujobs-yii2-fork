<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.02.2017
 * Time: 17:00
 */

use common\models\DynamicForm;

/* @var array $attributes */
/* @var DynamicForm $model */

?>

<?php if (!empty($attributes)) { ?>
    <?php foreach ($attributes as $k => $attribute) { ?>
        <div class="row show-notes">
            <div class="form-group">
                <div class="col-md-3 col-sm-4 col-xs-4 leftset">
                    <div class="set-title">
                        <label class="control-label"><?= $attribute->translation->title ?></label>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-8 rightset">
                    <?= $this->render($attribute->type, [
                        'values' => $attribute->values,
                        'model' => $model,
                        'attribute' => $attribute
                    ]) ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>

