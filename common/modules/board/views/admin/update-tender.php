<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.08.2017
 * Time: 14:46
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\board\models\Product */

$this->title = Yii::t('board', 'Update {modelClass}: ', [
        'modelClass' => 'Request',
    ]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('board', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('board', 'Update');
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
