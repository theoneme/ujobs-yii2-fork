<?php

use common\models\Setting;
use yii\helpers\Html;
use common\modules\attribute\models\Attribute;

/* @var $this yii\web\View */
/* @var Attribute[] $searchableAttributes */

$this->title = Yii::t('filter', 'Filter settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-index">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="box-body">
            <?= Html::beginForm('', 'post') ?>
            <h4><?= Yii::t('filter', 'Filterable attributes') ?></h4>

            <div class="row">
                <?php foreach ($searchableAttributes as $attribute) {
                    $setting = Setting::find()->where(['key' => "filter_attribute_{$attribute->id}"])->one();
                    ?>
                    <div class="col-md-6">
                        <div class="list-group">
                            <div class="list-group-item">
                                <p class="bold text-info"><?= $attribute->name ?></p>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon3"><?= Yii::t('filter', 'Display in filter as') ?></span>
                                    <?= Html::dropDownList("attribute[{$attribute->id}]", $setting ? $setting->value : null /*$attribute->getFilterSetting("filter_attribute_{$attribute->id}")*/, [
                                        'hide' => Yii::t('filter', 'Do not display'),
                                        'dropdown' => Yii::t('filter', 'Dropdown'),
                                        'radio' => Yii::t('filter', 'Radio'),
                                        'checkboxlist' => Yii::t('filter', 'Checkbox list'),
                                        'checkbox' => Yii::t('filter', 'Checkbox'),
                                        'range' => Yii::t('filter', 'Range'),
                                        'links' => Yii::t('filter', 'Links'),
                                    ], [
                                        'class' => 'form-control'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?= Html::submitButton(Yii::t('filter', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>