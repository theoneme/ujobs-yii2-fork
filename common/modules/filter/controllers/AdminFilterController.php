<?php

namespace common\modules\filter\controllers;

use common\controllers\BackEndController;
use Yii;

use common\modules\attribute\models\Attribute;
use common\models\Setting;

/**
 * Default controller for the `filter` module
 */
class AdminFilterController extends BackEndController
{
    private $filterableAttributeTypes = [
        'list',
        'radio',
        'checkbox',
        'checkboxList',
        'radiolist'
    ];

    const filterSettingPrefix = 'filter_attribute_';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchableAttributes = Attribute::find()->where(['is_searchable' => 1])->all();

        if($input = Yii::$app->request->post()) {
            foreach($input['attribute'] as $key =>$value) {
                $settingKey = self::filterSettingPrefix . $key;

                $setting = Setting::find()->where(['key' => $settingKey])->one();

                if(!$setting) {
                    $setting = new Setting();
                }

                $setting->key = $settingKey;
                $setting->value = $value;
                $setting->is_visible = false;

                $setting->save();
            }
        }

        return $this->render('index', [
            'searchableAttributes' => $searchableAttributes,
            'filterableTypes' => $this->filterableAttributeTypes
        ]);
    }
}
