<?php

use common\modules\livechat\models\LiveClient;
use yii\helpers\Html;

/* @var string $client_id */
/* @var bool $show_form */
/* @var LiveClient $client */

?>

<?php if ($show_form) { ?>
    <div class="message our no-operator" id="no-operator-form" style="display: none;">
        <div class="avatar">
            <?= Html::img(['/images/new/moder.jpg']) ?>
        </div>
        <div class="message-block">
            <div class="message-body">
                <p><?= Yii::t('livechat', 'You may leave your contacts, so we will be able to contact you when we can.') ?></p>
                <?= Html::beginForm(['/livechat/ajax/update-member-info'], 'post', ['id' => 'live_contact_form']) ?>
                <?= Html::hiddenInput('id', $client_id) ?>
                <div class="input-group">
                    <?= Html::input('email', 'email', $client->email, ['placeholder' => 'Email', 'class' => 'form-control']) ?>
                </div>
                <div class="input-group">
                    <?= Html::input('text', 'name', $client->name, ['placeholder' => Yii::t('livechat', 'Name'), 'class' => 'form-control']) ?>
                </div>
                <div class="input-group">
                    <?= Html::a(Yii::t('livechat', 'Send'), '#', ['class' => 'live-send-button', 'id' => 'live_contact_form_submit']) ?>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
        <div class="message-props">
            <div class="left">
                <span><?= date('H:i', time()) ?></span>
            </div>
        </div>
    </div>
<?php } ?>

<div class="message our no-operator" id="no-operator" style="display: none;">
    <div class="avatar">
        <?= Html::img(['/images/new/moder.jpg']) ?>
    </div>
    <div class="message-block">
        <div class="message-body">
            <p><?= Yii::t('livechat', 'There are no operators ready to response now, but you can leave your question and we will help you during working time.') ?></p>
        </div>
        <div class="message-props">
            <div class="left">
                <span><?= date('H:i', time()) ?></span>
            </div>
        </div>
    </div>
</div>