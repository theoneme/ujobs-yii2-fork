<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.06.2017
 * Time: 14:44
 */

use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use yii\helpers\Html;

/* @var LiveDialogMessage $message */
/* @var LiveDialogMember $adminMember */

?>

	<div class="message our" data-timestamp="<?= $message->created_at?>">
		<div class="avatar">
			<?= Html::img(['/images/new/moder.jpg']) ?>
		</div>
		<div class="message-block">
			<div class="message-body">
				<p>
                    <?php if($message->message) { ?>
                        <?= $message->message ?>
                    <?php } else { ?>
                        <?= Yii::t('livechat', '{who} has offered services to you.', ['who' => $adminMember->liveClient->getName()]) ?></p> ?>
                    <?php } ?>
				<p><?= Html::a(Yii::t('livechat', 'View them'), '#', ['data-target' => $message->id, 'class' => 'live-offers-trigger']) ?></p>
			</div>
			<div class="message-props">
				<div class="left">
					<span><?= date('m.d.y H:i', $message->created_at) ?></span>
				</div>
			</div>
		</div>
	</div>
<?php if (count($message->customDataArray) > 0) { ?>
	<div class="live-offer-wrap" data-id="<?= $message->id ?>">
		<?php foreach ($message->customDataArray as $item) { ?>
			<div class="live-offer-item">
				<div class="live-offer-image">
					<?= Html::img($item['img']) ?>
				</div>
                <div class="live-offer-info">
                    <div class="live-offer-title">
                        <?= $item['label'] ?>
                    </div>
                    <div class="text-right">
                        <?= $item['price']?>&nbsp;&nbsp;<?= Html::a(Yii::t('livechat', 'View details'), $item['url'], ['target' => '_blank', 'class' => 'live-offer-order-button']) ?>
                    </div>
                </div>
			</div>
		<?php } ?>
	</div>
<?php } ?>