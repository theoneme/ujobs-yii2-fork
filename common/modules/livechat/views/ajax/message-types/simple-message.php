<?php

use yii\helpers\Html;

/* @var string $text */
/* @var integer $date */
/* @var string $side */
/* @var integer $timestamp */

?>
<div class="message <?= $side?>" data-timestamp="<?= $timestamp?>">
	<?php if ($side === 'our') {?>
		<div class="avatar">
			<?= Html::img(['/images/new/moder.jpg']) ?>
		</div>
	<?php } ?>
	<div class="message-block">
		<div class="message-body">
			<p><?= $side === 'our' ? Yii::$app->utility->stringToLink($text) : $text ?></p>
		</div>
		<div class="message-props">
			<div class="<?= $side === 'user' ? 'right' : 'left'?>">
				<span><?= date('m.d.y H:i', $date) ?></span>
			</div>
		</div>
	</div>
</div>