<?php

use common\modules\livechat\assets\LivechatAdminAsset;
use common\modules\livechat\assets\LivechatAsset;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\search\LiveDialogMemberSearch;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel LiveDialogMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $membersOnline array */
/* @var $operatorIds array */

$this->title = 'Messager';
LivechatAsset::register($this);
$assetBaseUrl = LivechatAdminAsset::register($this)->baseUrl;
$defaultColumnConfig = ['headerOptions' => ['class' => 'column-name'], 'filterInputOptions' => ['class' => false], 'format' => 'ntext'];
$columnsConfig = [
	[
		'label' => 'Имя',
		'format' => 'raw',
		'attribute' => 'name',
		'value' => function ($model) {
			/* @var $model LiveClient */
			return Html::a('
				<div class="avatar-author">' .
					Html::img($model->avatar) .
					'<div class="online-author ' . ($model->isOnline() ? "yes" : "no") . '"></div>
				</div>
				<div class="name-author">' .
					$model->getName() .
				'</div>',
				['/livechat/admin/view', 'id' => $model->id],
				[
					'title' => 'Переписка с пользователем "' . $model->getName() . '"',
					'data-pjax' => 0,
					'class' => 'modal-view author'
				]
			);
		},
		'contentOptions' => ['style' => 'min-width: 150px;'],
		'options' => ['style' => 'min-width: 150px']
	],
	[
		'label' => 'Телефон',
		'attribute' => 'phone',
	],
	[
		'label' => 'Email',
		'attribute' => 'email',
	],
	[
		'header' => 'Последняя активность',
		'value' => function ($model) {
			/* @var $model LiveClient */
			return Yii::$app->formatter->asDate($model->last_action_at);
		},
	],
	[
		'format' => 'datetime',
		'header' => 'Последний контакт',
		'value' => function ($model) {
			/* @var $model LiveClient */
			return $model->dialog->lastMessage->created_at ?? null;
		},
	],
	[
		'format' => 'datetime',
		'header' => 'Последний ответ',
		'value' => function ($model) use ($operatorIds) {
			/* @var $model LiveClient */
			if ($model->dialog !== null) {
                $lastAnswer = $model->dialog->getLastAnswer($operatorIds);
                if ($lastAnswer !== null) {
                    return $lastAnswer->created_at;
                }
            }
			return null;
		},
	],
//	[
//		'header' => 'Отписался от <br> емейлов'
//	],
    [
        'label' => 'Источник рекламы',
        'attribute' => 'advert_referrer'
    ],
	[
		'label' => 'Сессии',
		'attribute' => 'sessions_count'
	],
	[
		'label' => 'Страна',
		'attribute' => 'country'
	],
	[
		'label' => 'Регион',
		'attribute' => 'region'
	],
	[
		'label' => 'Город',
		'attribute' => 'city'
	],
];
$columnsConfig = array_map(function($var) use ($defaultColumnConfig){
	return array_merge($defaultColumnConfig, $var);
}, $columnsConfig);
?>
	<aside>
		<div class="aside-header">
			<div class="segment-text">Сегменты</div>
			<div class="count-dialogs">2</div>
		</div>
		<div class="filter-body">
            <?php Pjax::begin(['id' => 'filter-container', 'timeout' => 6000]);?>
                <div class="segments">
                    <?= Html::a('Все пользователи',
                        Url::to(['/livechat/admin/index', 'page' => Yii::$app->request->get('page')]),
                        ['class' => Url::current(['page' => null, '_pjax' => null]) === Url::to(['/livechat/admin/index']) ? 'active' : '']
                    )?>
                    <?= Html::a('Отправлен поп-ап',
                        Url::current([$searchModel->formName() => ['popup_sent' => $searchModel->popup_sent ? null : true]]),
                        ['class' => $searchModel->popup_sent ? 'active' : '']
                    )?>
                    <?= Html::a('Есть имя',
                        Url::current([$searchModel->formName() => ['has_name' => $searchModel->has_name ? null : true]]),
                        ['class' => $searchModel->has_name ? 'active' : '']
                    )?>
                    <?= Html::a('Есть телефон',
                        Url::current([$searchModel->formName() => ['has_phone' => $searchModel->has_phone ? null : true]]),
                        ['class' => $searchModel->has_phone ? 'active' : '']
                    )?>
                    <?= Html::a('Есть email',
                        Url::current([$searchModel->formName() => ['has_email' => $searchModel->has_email ? null : true]]),
                        ['class' => $searchModel->has_email ? 'active' : '']
                    )?>
                    <?= Html::a('Размещал услугу',
	                    Url::current([$searchModel->formName() => ['posted_job' => $searchModel->posted_job ? null : true]]),
	                    ['class' => $searchModel->posted_job ? 'active' : '']
                    )?>
                    <?= Html::a('Размещал заявку',
	                    Url::current([$searchModel->formName() => ['posted_tender' => $searchModel->posted_tender ? null : true]]),
	                    ['class' => $searchModel->posted_tender ? 'active' : '']
                    )?>
                    <?= Html::a('Добавлял в корзину',
	                    Url::current([$searchModel->formName() => ['added_to_cart' => $searchModel->added_to_cart ? null : true]]),
	                    ['class' => $searchModel->added_to_cart ? 'active' : '']
                    )?>
                    <?= Html::a('Удалял из корзины',
	                    Url::current([$searchModel->formName() => ['removed_from_cart' => $searchModel->removed_from_cart ? null : true]]),
	                    ['class' => $searchModel->removed_from_cart ? 'active' : '']
                    )?>
                    <?= Html::a('Были больше 1 раза',
                        Url::current([$searchModel->formName() => ['more_than_one_session' => $searchModel->more_than_one_session ? null : true]]),
                        ['class' => $searchModel->more_than_one_session ? 'active' : '']
                    )?>
                    <?= Html::a('Активные за 30 дней',
                        Url::current([$searchModel->formName() => ['active_last_days' => $searchModel->active_last_days === true ? null : true]]),
                        ['class' => $searchModel->active_last_days === true ? 'active' : '']
                    )?>
                    <?= Html::a('Неактивные больше 30 дней',
                        Url::current([$searchModel->formName() => ['active_last_days' => $searchModel->active_last_days === false ? null : false]]),
                        ['class' => $searchModel->active_last_days === false ? 'active' : '']
                    )?>
                    <?= Html::a('Переходил из Facebook',
                        Url::current([$searchModel->formName() => ['facebook_referrer' => $searchModel->facebook_referrer === true ? null : true]]),
                        ['class' => $searchModel->facebook_referrer === true ? 'active' : '']
                    )?>
                    <?= Html::a('Переходил из Google',
                        Url::current([$searchModel->formName() => ['google_referrer' => $searchModel->google_referrer === true ? null : true]]),
                        ['class' => $searchModel->google_referrer === true ? 'active' : '']
                    )?>
                    <?= Html::a('Переходил из Яндекса',
                        Url::current([$searchModel->formName() => ['yandex_referrer' => $searchModel->yandex_referrer === true ? null : true]]),
                        ['class' => $searchModel->yandex_referrer === true ? 'active' : '']
                    )?>
                    <?= Html::a('Переходил из Mailru',
                        Url::current([$searchModel->formName() => ['mailru_referrer' => $searchModel->mailru_referrer === true ? null : true]]),
                        ['class' => $searchModel->mailru_referrer === true ? 'active' : '']
                    )?>
                </div>
                <hr>
                <div class="filter-bottom">
                    <?= Html::a('Очистить фильтры', Url::to(['/livechat/admin/index']), ['class' => 'filter-btn'])?>
                </div>
            <?php Pjax::end(); ?>
		</div>
	</aside>
	<div class="content">
		<div class="content-top">
			<div class="top-buttons">
				<div class="buttons-arrow"></div>
				<a class="blue-btn disabled" href="#">
					<div class="icon i-email"></div>Написать email
				</a>
				<a class="blue-btn disabled" href="#">
					<div class="icon i-popup"></div>Показать pop-up
				</a>
				<a class="blue-btn disabled" href="#">
					<div class="icon i-chat"></div>Написать в чат
				</a>
				<a disabled="disabled" class="grey-dashed" href="#">Добавить тег</a>
				<a disabled="disabled" class="grey-dashed" href="#">Экспорт</a>
			</div>
			<div class="columns-data">
				<div class="columns-block">
					<div class="choose-columns">
						<div class="text-choose">Столбцы в таблице <i class="fa fa-caret-down"></i></div>
						<div class="list-columns">
							<?php foreach ($columnsConfig as $id => $column) {
								$label = $column['header'] ?? $searchModel->getAttributeLabel($column['attribute']);
								echo '
									<div class="column-check">
										<input id="col' . $id . '" type="checkbox" value="' . $label . '"/>
										<label for="col' . $id . '">' . $label . '</label>
									</div>
								';
							}?>
						</div>
					</div>
					<a class="refresh" href="#"></a>
				</div>
			</div>
		</div>
        <div class="list-messages">
            <?php Pjax::begin(['id' => 'grid-container', 'timeout' => 6000]);?>
                <?php
                array_unshift($columnsConfig, [
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '
                            <div class="user-check">
                                <input id="user' . $model->id . '" type="checkbox" value="user' . $model->id . '"/>
                                <label for="user' . $model->id . '"></label>
                            </div>
                        ';
                    },
                    'header' => '
                        <div class="user-check">
                            <input id="userall" type="checkbox" value="userall"/>
                            <label for="userall"></label>
                        </div>
                    ',
                ])
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'list-messages-table'],
                    'filterRowOptions' => ['class' => false],
                    'layout' => "{items}\n{pager}",
                    'columns' => $columnsConfig
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
	</div>
<?
$liveUrl = Yii::$app->params['liveUrl'];

$script = <<<JS
	socket = io.connect('$liveUrl');
	socket.emit('ifyouwannabeasmartandfuriousthengetyourcarandsmashthiswall');
	member_id = '';
    function sendMessage(message) {
        if (message.length > 0) {
            $('.chat-message').val('');
            socket.emit('sendMessage', message);
        }
    }
    function scrollBlockToBottom(block) {
	    if(block.length) {
	        var height = block[0].scrollHeight;
	        block.scrollTop(height);
	        
	        return true;
	    }
	    
	    return false
	}
 	function addZero(i) {
        return i < 10 ? "0" + i : i;
    }
	$(document).on('keypress', '.chat-message', function (e) {
        if (e.which == 13) {
            sendMessage($(this).val());
            return false;
        }
    });

    $(document).on('click', '.messager-send', function (e) {
        sendMessage($('.chat-message').val());
        return false;
    });
	$(document).on('click', '.refresh', function (e) {
 	   	$.pjax.reload({container: "#grid-container", timeout: 6000});
        return false;
    });
    //$(document).on('click', '.filter-body a', function (e) {
 	//   	$('#grid-container').one('pjax:success', function(event, data, status, xhr, options) {
 	//   	    $.pjax.reload({container: "#filter-container", url: $(this).attr('href')});
    //    });
 	//   	$.pjax.reload({container: "#grid-container", url: $(this).attr('href')});
    //    return false;
    //});

    $('#filter-container').on('pjax:success', function(event, data, status, xhr, options) {
        $.pjax.reload({container: "#grid-container", timeout: 6000});
    });
    $('#grid-container').on('pjax:success', function(event, data, status, xhr, options) {
        $('.column-check input:checkbox').trigger('change');
    });

    socket.emit('isOperatorOnline');
    socket.on('isOperatorOnline', function(result) {
 		console.log(result);
 	});
    
 	socket.on('update', function(message) {
 		console.log(message);
 	});
 	
 	socket.on('refreshGrid', function() {
 	   	$.pjax.reload({container: "#grid-container", timeout: 6000}); 
 	   	
 	   	var audio = new Audio('$assetBaseUrl/media/0477.mp3');
		audio.play();
 	});

    socket.on('message', function(response) {
        var dt = new Date();
        var whose = (response.user_id === member_id ? 'y' : '') + 'our-message';
        var image = '';
        if (!$('.message').last().children('.' + whose).length) {
            image = '<div class="message-from-image">' + $('.' + whose + ' .message-from-image').html() + '</div>';
        }
        $('.chat-bottom').append(
            '<div class="message">' +
                '<div class="' + whose + '">' +
                    image +
                    '<div class="message-time">' + addZero(dt.getHours()) + ':' + addZero(dt.getMinutes()) + '</div>' +
                    '<div class="message-text">' +
                        '<p>' + response.message + '</p>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
        scrollBlockToBottom($('#edit-modal'));
        $.pjax.reload({container: "#grid-container", timeout: 6000});
 	});
    
    $('#edit-modal').on('shown.bs.modal', function() {
        scrollBlockToBottom($('#edit-modal'));
    });
	
	$('#grid-container').on('pjax:error', function (event) {
		console.log('pjax fail');
		event.preventDefault();
	});
JS;

$this->registerJs($script);