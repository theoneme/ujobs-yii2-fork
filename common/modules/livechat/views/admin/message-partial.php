<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.06.2017
 * Time: 18:22
 */

use common\modules\livechat\assets\LivechatAdminAsset;
use common\modules\livechat\models\LiveClient;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

$assetBaseUrl = LivechatAdminAsset::register($this)->baseUrl;

/* @var LiveClient $model */
/* @var LiveClient $adminModel */

?>

    <div class="modal-header grey-header">
        Отправка сообщения
        <a class="send-grey" href="#" data-dismiss="modal" aria-hidden="true">
            &times;
        </a>
    </div>
    <div class="modal-body">
        <div class="to-user">
            <div>Кому:</div>
            <div class="to-name">
                <?= $model->getName() ?>
                <div class="count-msg">

                </div>
            </div>
        </div>
        <div class="send-message">
            <div class="subtitle">
                Тип сообщения:
            </div>
            <ul class="type-msg">
                <li class="active">
                    <a href="#chat" data-toggle="tab">
                        <?= Html::img($assetBaseUrl . '/images/popup-chat.png') ?>
                        <span>Чат</span>
                    </a>
                </li>
                <li>
                    <a href="#popup" data-toggle="tab">
                        <?= Html::img($assetBaseUrl . '/images/popup.png') ?>
                        <span>Поп-ап</span>
                    </a>
                </li>
                <li>
                    <a href="#email" data-toggle="tab">
                        <?= Html::img($assetBaseUrl . '/images/email.png') ?>
                        <span>Email</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content tab-type">
                <div class="tab-type-content tab-pane fade in active" id="chat">
                    <?= Html::beginForm('/', 'post', ['id' => 'text-chat-form']); ?>
                    <div class="send-cols">
                        <div class="left-column">
                            <div class="msg-option">
                                <?= Widget::widget([
                                    'selector' => '#text-chat',
                                    'name' => 'chat-message',
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 200,
                                        'plugins' => [
                                            'clips',
//											'fullscreen'
                                        ],
                                        'changeCallback' => new yii\web\JsExpression('function(e) {
										var msg = this.code.get();
        								$(\'#example-chat\').html(msg);
									}'),
                                    ]
                                ]);
                                ?>
                                <textarea id="text-chat" name="chat-message" class="form-msg"></textarea>
                            </div>
                            <div class="send-from">
                                <div class="subtitle">Отправить от имени:</div>
                                <div class="from-avatar">
                                    <div class="avatar">
                                        <?= Html::img($assetBaseUrl . '/images/message1.png') ?>
                                    </div>
                                    <span><?= $adminModel->getName() ?? null ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="right-column">
                            <div class="center-example">
                                <div class="site-chat">
                                    <div class="close" href=""><i class="fa fa-times-circle"></i></div>
                                    <div class="msg">
                                        <div id="example-chat">...</div>
                                    </div>
                                    <div class="chat-icon">
                                        <?= Html::img($assetBaseUrl . '/images/message1.png') ?>
                                        <div class="count">1</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="send-buttons clearfix">
                        <div class="right-side">
                            <a class="bigBlue-btn inverse modal-dismiss" href="#">Отмена</a>
                            <?= Html::submitInput(Yii::t('livechat', 'Submit'), ['class' => 'bigBlue-btn']) ?>
                        </div>
                    </div>
                    <?= Html::endForm(); ?>
                </div>
                <div class="tab-type-content tab-pane fade" id="popup">
                    <?= Html::beginForm('/', 'post', ['id' => 'popup-form']); ?>
                    <div class="send-cols">
                        <div class="left-column">
                            <div class="msg-option">
                                <div class="subtitle">Тип поп-апа</div>
                                <div class="popup-check">
                                    <input id="small-popup" type="radio" name="typePopup" checked value="small"/>
                                    <label for="small-popup">
                                        Маленький попап (350px)
                                    </label>
                                </div>
                                <div class="popup-check">
                                    <input id="big-popup" type="radio" name="typePopup" value="big"/>
                                    <label for="big-popup">
                                        Большой попап (580px)
                                    </label>
                                </div>
                            </div>
                            <div class="msg-option">
                                <?= Widget::widget([
                                    'selector' => '#text-popup',
                                    'name' => 'chat-message',
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 200,
                                        'plugins' => [
                                            'clips',
                                            'fullscreen'
                                        ],
                                        'changeCallback' => new yii\web\JsExpression('function(e) { 
											var msg = this.code.get();
											$(\'#popup-example\').html(msg);
										}'),
                                    ]
                                ]);
                                ?>
                                <textarea id="text-popup" name="popup-message" class="form-msg"></textarea>
                            </div>
                            <!--<div class="msg-option">
                                <div class="subtitle">Тип ответа</div>
                                <select>
                                    <option value="Без ответа">Без ответа</option>
                                    <option value="Текст">Текст</option>
                                    <option value="Оставьте Email">Оставьте Email</option>
                                    <option value="Оставьте телефон">Оставьте телефон</option>
                                </select>
                            </div>-->

                            <div class="send-from">
                                <div class="subtitle">Отправить от имени:</div>
                                <div class="from-avatar">
                                    <div class="avatar">
                                        <?= Html::img($assetBaseUrl . '/images/message1.png') ?>
                                    </div>
                                    <span><?= $adminModel->getName() ?? null ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="right-column">
                            <div class="pop-up example small">
                                <div class="popup-icon">
                                    <?= Html::img($assetBaseUrl . '/images/message1.png') ?>
                                </div>
                                <div class="popup-body">
                                    <div class="popup-text" id="popup-example">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="send-buttons clearfix">
                        <div class="right-side">
                            <a class="bigBlue-btn inverse modal-dismiss" href="#">Отмена</a>
                            <?= Html::submitInput(Yii::t('livechat', 'Submit'), ['class' => 'bigBlue-btn']) ?>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                </div>
                <div class="tab-type-content tab-pane fade" id="email">
                    <?= Html::beginForm('/', 'post', ['id' => 'email-form']); ?>
                    <div class="send-cols">
                        <div class="left-column">
                            <div class="msg-option">
                                <div class="subtitle">Тeма письма</div>
                                <?= Html::textInput('subject', '', ['id' => "email-subject-input"]) ?>
                                <?= Html::hiddenInput('clientId', $model->id) ?>
                                <?= Html::hiddenInput('operatorId', $adminModel->id) ?>
                            </div>
                            <div class="msg-option">
                                <?= Widget::widget([
                                    'selector' => '#text-email',
                                    'name' => 'chat-message',
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 200,
                                        'plugins' => [
                                            'clips',
                                            'fullscreen'
                                        ],
                                        'changeCallback' => new yii\web\JsExpression("function(e) {
											$('#email-text').html(this.code.get());
										}"),
                                    ]
                                ]);
                                ?>
                                <textarea id="text-email" name="message" class="form-msg"></textarea>
                            </div>
                            <div class="send-from">
                                <div class="subtitle">Отправить от имени:</div>
                                <div class="from-avatar">
                                    <div class="avatar">
                                        <?= Html::img($assetBaseUrl . '/images/message1.png') ?>
                                    </div>
                                    <span><?= $adminModel->getName() ?? null ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="right-column">
                            <?= $this->render('../email/preview/mail-template', [
                                'model' => $model,
                                'assetBaseUrl' => $assetBaseUrl
                            ]) ?>
                        </div>
                    </div>
                    <div class="send-buttons clearfix">
                        <div class="right-side">
                            <a class="bigBlue-btn inverse modal-dismiss" href="#">Отмена</a>
                            <?= Html::submitInput(Yii::t('livechat', 'Submit'), ['class' => 'bigBlue-btn']) ?>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>


<?php $liveUrl = Yii::$app->params['liveUrl'];
$emailUrl = Url::to(['/livechat/admin/send-mail-ajax']);
$script = <<<JS
	var member_id = '$memberId',
		client_id = '$clientId',
		dialog_id = '$dialogId',
		data = {
			'member_id': member_id,
			'dialog_id': dialog_id,
			'client_id': client_id,
			'access': 'patchworkwantstoplay'
		};
	socket.emit('joinServer', data);
   
	function sendMessage(message) {
        if (message.length > 0) {
            socket.emit('sendMessage', message);
            
            return true;
        }
        
        return false;
    }
    
    function sendPopup(message, type) {
	    if(message.length > 0 && type) {
	        socket.emit('sendPopup', message, type);
	        
            return true;
        }
        
        return false;
    }
	
	$('#text-chat-form').on('submit', function() {
		if(sendMessage($(this).find('#text-chat').val())) {
		    notyCall('bottom', 'success', 'Сообщение отправлено');
		} else {
		    notyCall('bottom', 'error', 'Произошла ошибка');
		}
		
		$("#text-chat").redactor('code.set', ''); 
		
		return false;
	});
	
	$('#popup-form').on('submit', function() { 
	    let message = $(this).find('#text-popup').val(),
			type = $(this).find('input[name=typePopup]:checked').val();
	    
		$("#text-popup").redactor('code.set', '');
		
		if(sendPopup(message, type)) { 
		    notyCall('bottom', 'success', 'Popup отправлен');
		} else {
		    notyCall('bottom', 'error', 'Произошла ошибка');
		}
		
		return false;
	});
	
	$('#email-form').on('submit', function() {
		let data = $(this).serialize();
		$.post('$emailUrl', data, function(e) {
		    if(e.success) {
		        notyCall('bottom', 'success', e.message);
		    } else {
		        notyCall('bottom', 'error', e.message);
		    }
		});
		
		return false;
	});
JS;
$this->registerJs($script); ?>