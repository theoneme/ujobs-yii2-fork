<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 19.06.2017
 * Time: 10:01
 */
use common\modules\livechat\assets\LivechatAdminAsset;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveClientHistory;
use common\modules\livechat\models\LiveDialog;
use common\modules\livechat\models\LiveDialogMessage;
use kartik\editable\Editable;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var LiveClient $model */
/* @var LiveClientHistory $history */
/* @var LiveDialog $dialog */
/* @var LiveClientHistory[] $openPageHistory */
/* @var LiveClientHistory[] $startSessionHistory */
/* @var LiveClientHistory[] $startDialogHistory */
/* @var LiveClientHistory[] $messageSentHistory */
/* @var LiveClientHistory[] $messageReceivedHistory */
/* @var LiveClientHistory[] $postJobHistory */
/* @var LiveClientHistory[] $postTenderHistory */
/* @var LiveClientHistory $cartHistory */
/* @var bool $communicated_today */

$assetBaseUrl = LivechatAdminAsset::register($this)->baseUrl;

?>

<?php Pjax::begin(['id' => 'client-view-container', 'timeout' => 6000, 'enablePushState' => false]); ?>
	<div class="modal-header user-info-form-head">
		<div class="left-side">
			<div class="main-info">
				<div class="user-avatar">
					<div class="avatar">
						<?= Html::img($model->avatar) ?>
					</div>
					<div class="status off">
						<?= $model->isOnline() ? Yii::t('livechat', 'Online') : Yii::t('livechat', 'Offline') ?>
					</div>
				</div>
				<div class="contact">
					<div class="contact-item">
						<i class="ico-user i-name"></i>
						<?php echo Editable::widget([
							'model' => $model,
							'attribute' => 'name',
							'value' => $model->getName(),
							'asPopover' => false,
							'format' => Editable::FORMAT_BUTTON,
							'formOptions' => [
								'action' => ['/livechat/admin/update-ajax', 'id' => $model->id],
							],
							'size' => 'md',
							'options' => ['class' => 'form-control', 'placeholder' => Yii::t('livechat', 'Set client name'), 'id' => 'client-name',]
						]) ?>
					</div>
					<div class="contact-item">
						<i class="ico-user i-email"></i>
						<?php echo Editable::widget([
							'model' => $model,
							'attribute' => 'email',
							'value' => $model->getEmail(),
							'asPopover' => false,
							'format' => Editable::FORMAT_BUTTON,
							'formOptions' => [
								'action' => ['/livechat/admin/update-ajax', 'id' => $model->id],
							],
							'size' => 'md',
							'options' => ['class' => 'form-control', 'placeholder' => Yii::t('livechat', 'Set client email'), 'id' => 'client-email',]
						]) ?>
					</div>
					<div class="contact-item">
						<i class="ico-user i-phone"></i>
						<?php echo Editable::widget([
							'model' => $model,
							'attribute' => 'phone',
							'value' => $model->getPhone(),
							'asPopover' => false,
							'format' => Editable::FORMAT_BUTTON,
							'formOptions' => [
								'action' => ['/livechat/admin/update-ajax', 'id' => $model->id],
							],
							'size' => 'md',
							'options' => ['class' => 'form-control', 'placeholder' => Yii::t('livechat', 'Set client phone'), 'id' => 'client-phone',]
						]) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="right-side">
			<div class="top-alias">
				<a id="client-view-refresh" class="blue-alias" href="#">
					<div class="icon refresh"></div>
					Обновить данные
				</a>
				<a class="blue-alias" href="<?= Url::to(['/livechat/admin/dialog', 'id' => $model->dialog->id]) ?>"
				   data-toggle="tooltip" data-placement="bottom"
				   title="Открыть диалог в отдельном окне(Ctrl+L)">
					<div class="icon alias"></div>
					Ссылка на диалог
				</a>
				<a class="blue-alias" href="#" data-dismiss="modal" aria-hidden="true">
					<div class="icon closeform"></div>
				</a>
			</div>
			<?= Html::a('<div class="icon i-email"></div>Написать email',
                ['/livechat/admin/message', 'id' => $model->id],
                ['class' => 'blue-btn modal-send modal-dismiss', 'data-tab' => '#email']
            )?>
			<?= Html::a('<div class="icon i-popup"></div>Показать pop-up',
                ['/livechat/admin/message', 'id' => $model->id],
                ['class' => 'blue-btn modal-send modal-dismiss', 'data-tab' => '#popup']
            )?>
			<?= Html::a('<div class="icon i-chat"></div>Написать в чат',
                ['/livechat/admin/message', 'id' => $model->id],
                ['class' => 'blue-btn modal-send modal-dismiss', 'data-tab' => '#chat']
            )?>
		</div>
	</div>
	<div class="modal-body clearfix">
		<div class="uForm-column info">
			<div class="title">Свойства</div>
			<div class="white-block">
				<div class="current-page"><i
							class="ico-user i-page"></i><?= $model->isOnline() ? Yii::t('livechat', 'Current page') : Yii::t('livechat', 'Last page') ?>
				</div>
				<?= isset($model->lastOpenPageHistory) ? Html::a($model->lastOpenPageHistory->url, $model->lastOpenPageHistory->url, ['target' => '_blank']) : null ?>
			</div>
			<div class="white-block">
				<div class="title">
					Сегменты (<span>4</span>)
				</div>
				<div class="segments">
					<span>
                        Отправлен поп-ап
                    </span>
					<span class="<?= $communicated_today ? 'active' : ''?>">
                        Общались в течении суток
                    </span>
					<span class="<?= $model->sessions_count > 1 ? 'active' : ''?>">
                        Были больше 1 раза
                    </span>
					<span class="<?= $model->last_action_at > (time() - 60 * 60 * 24 * 30) ? 'active' : ''?>">
                        Активные за 30 дней
                    </span>
				</div>
			</div>
			<div class="white-block">
				<div class="title">
					Теги (<span>0</span>)
				</div>
				<a class="plus-prop" href="#"><i class="fa fa-plus"></i> Добавить тег</a>
			</div>
			<div class="white-block">
				<div class="new-prop">
					<div class="title">
						Ваши свойства пользователя
					</div>
					<a class="plus-prop" href="#"><i class="fa fa-plus"></i> Добавить свойство</a>
				</div>
				<div class="title">
					Системные свойства
				</div>
				<div class="system-props">
					<div class="prop-item">
						<div class="prop-title">Сессии</div>
						<div class="prop-text"><?= $model->sessions_count ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">Локация</div>
						<div class="prop-text"><?= $model->getAddress() ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">Первый источник</div>
						<div class="prop-text"><?= $model->getAdvertReferrer() ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">Первый источник (домен)</div>
						<div class="prop-text"><?= $model->getAdvertSource() ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">Последняя активность</div>
						<div class="prop-text"><?= Yii::$app->formatter->asDatetime($model->last_action_at) ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">User ID</div>
						<div class="prop-text"><?= $model->getUserId() ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">Livechat ID</div>
						<div class="prop-text"><?= $model->id ?></div>
					</div>
					<div class="prop-item">
						<div class="prop-title">Почтовые рассылки</div>
						<select name="subscribe">
							<option value="Подписан">Подписан</option>
							<option value="Отписан">Отписан</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="uForm-column history">
			<div class="title">События</div>
			<ul class="history-switch">
				<li class="active">
					<a href="#chronology" data-toggle="tab"> Хронология событий </a>
				</li>
				<li>
					<a href="#group" data-toggle="tab"> Группировка событий </a>
				</li>
			</ul>
			<div class="tab-content tab-history">
				<div class="tab-history-content tab-pane fade in active" id="chronology">
					<div class="chronology">
						<?= ListView::widget([
							'dataProvider' => $historyDataProvider,
							'itemView' => 'chronology-partial-single',
							'emptyText' => "<div class=\"no-data\">Событий данного типа не было выполнено пользователем</div>",
							'itemOptions' => [
								'tag' => 'div',
								'class' => 'chronology-item'
							],
							'layout' => "{items}{pager}"
						]) ?>

					</div>
				</div>
				<div class="tab-history-content tab-pane fade" id="group">
					<div class="chronology">
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Переход по страницам</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $openPageHistory
								]); ?>
							</div>
						</div>
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Коммуникации: Отправлено сообщение</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $messageSentHistory
								]); ?>
							</div>
						</div>
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Коммуникации: Получено сообщение</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $messageReceivedHistory
								]); ?>
							</div>
						</div>
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Начал сессию</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $startSessionHistory
								]); ?>
							</div>
						</div>
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Корзина</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $cartHistory
								]); ?>
							</div>
						</div>
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Публикация работы</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $postJobHistory
								]); ?>
							</div>
						</div>
						<div class="chronology-group">
							<a class="chron-head" href="#">
								<div class="arrow-cell">
									<div class="icon-arrow"></div>
								</div>
								<div class="group-name">Публикация заявки</div>
								<div class="chevron">
									<i class="fa fa-chevron-right"></i>
								</div>
							</a>
							<div class="group-body">
								<?= $this->render('chronology-partial-multiple', [
									'items' => $postTenderHistory
								]); ?>
							</div>
						</div>
                        <div class="chronology-group">
                            <a class="chron-head" href="#">
                                <div class="arrow-cell">
                                    <div class="icon-arrow"></div>
                                </div>
                                <div class="group-name">Оплата</div>
                                <div class="chevron">
                                    <i class="fa fa-chevron-right"></i>
                                </div>
                            </a>
                            <div class="group-body">
                                <?= $this->render('chronology-partial-multiple', [
                                    'items' => $paymentHistory
                                ]); ?>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="uForm-column history">
			<div class="title">История диалогов</div>
			<div class="chats-history">
                <?php if ($dialog) { ?>
                    <div class="history-body">
                        <div class="history-opt">

                        </div>
                        <?php
                            foreach ($dialog->messages as $message) {
                                switch ($message->type) {
                                case LiveDialogMessage::TYPE_SIMPLE_MESSAGE:
                                    echo $this->render('message-types/simple-message', [
                                        'message' => $message,
                                        'adminMember' => $adminMember,
                                        'assetBaseUrl' => $assetBaseUrl
                                    ]);
                                    break;
                                case LiveDialogMessage::TYPE_SERVICE_OFFER:
                                    echo $this->render('message-types/offer-message', [
                                        'message' => $message,
                                        'assetBaseUrl' => $assetBaseUrl
                                    ]);
                                    break;
                                }
                            }
                        ?>
                    </div>
                    <div class="history-bottom">
                        <?= Html::a(Yii::t('livechat', 'Continue chat'), ['/livechat/admin/dialog', 'id' => $dialog->id], ['class' => 'continue']) ?>
                    </div>
                <?php } ?>
			</div>
		</div>
	</div>

<?
$url = Url::to(['/livechat/admin/view', 'id' => $model->id]);
$script = <<<JS
	$('#client-view-refresh').on('click', function() {
	    $.pjax.reload({container: "#client-view-container", url: '$url'});
	    return false;
	});
JS;
$this->registerJs($script);

Pjax::end(); ?>