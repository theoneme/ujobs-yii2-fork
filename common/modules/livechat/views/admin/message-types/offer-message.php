<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.06.2017
 * Time: 14:44
 */

use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use yii\helpers\Html;

/* @var LiveDialogMessage $message */
/* @var LiveDialogMember $adminMember */

?>

<div class="message-actions">
	<div class="table-actions">
		<div class="circles">
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
		</div>
		<div class="avatar">
			<?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
		</div>
		<div class="name"><?= $adminMember->liveClient->name ?></div>
		<div class="text"><?= Yii::t('livechat', 'Has offered services to user: ') ?>
			<?php if (count($message->customDataArray) > 0) { ?>
				<?php foreach ($message->customDataArray as $item) { ?>
					<div class="dialog-tag"><?= Html::a($item['label'] . "({$item['price']})", $item['url'], ['target' => '_blank']) ?></div>
				<?php } ?>
			<?php } ?>
		</div>
		<div class="circles">
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
			<i class="fa fa-circle"></i>
		</div>
	</div>
</div>
