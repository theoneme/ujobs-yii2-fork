<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 22.06.2017
 * Time: 14:44
 */

use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use yii\helpers\Html;

/* @var LiveDialogMessage $message */
/* @var LiveDialogMember $adminMember */

$side = $message->member->live_client_id === $adminMember->live_client_id ? 'user' : 'our';

?>

<div class="message <?= $side ?>">
	<div class="avatar">
		<?= Html::img($message->member->liveClient->avatar ?? $assetBaseUrl . '/images/default-v2.png') ?>
	</div>
	<div class="message-block">
		<div class="message-body">
			<p><?= $side === 'user' ? Yii::$app->utility->stringToLink($message->message) : $message->message ?></p>
		</div>
		<div class="message-props">
			<div class="left">
				<?= date('H:i:s', $message->created_at) ?>
				<span>
					<?= date('d.m.Y', $message->created_at) ?>
				</span>
			</div>
			<div class="right">
				<span>Через: </span> ручное сообщение
			</div>
		</div>
	</div>
</div>
