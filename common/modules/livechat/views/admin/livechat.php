<?php

use common\modules\livechat\assets\LivechatAdminAsset;
use common\modules\livechat\assets\SelectizeAsset;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use common\modules\livechat\models\search\LiveDialogMemberSearch;
use kartik\editable\Editable;
use kartik\editable\EditableAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $messages LiveDialogMessage[] */
/* @var $dialog_id string */
/* @var $member_id string */
/* @var $client_id string */
/* @var $userMember LiveDialogMember */
/* @var $adminMember LiveDialogMember */
/* @var $searchModel LiveDialogMemberSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $membersOnline array */
/* @var $client LiveClient */
/* @var $action string */

$liveUrl = Yii::$app->params['liveUrl'];
$adviceUrl = Url::to(['/livechat/ajax/send-advice']);
$assetBaseUrl = LivechatAdminAsset::register($this)->baseUrl;
SelectizeAsset::register($this);
EditableAsset::register($this);

$script = <<<JS
    $.pjax.defaults.timeout = 6000;
    socket = io.connect('$liveUrl');
	socket.emit('ifyouwannabeasmartandfuriousthengetyourcarandsmashthiswall');
	member_id = '';
	
    delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    function sendMessage(message) {
        if (message.length > 0) {
            $('.write-message').val('');
            socket.emit('sendMessage', message);
        }
    }
    function scrollBlockToBottom(block) {
	    if(block.length) {
	        var height = block[0].scrollHeight;
	        block.scrollTop(height);

	        return true;
	    }

	    return false
	}
 	function addZero(i) {
        return i < 10 ? "0" + i : i;
    }
	$(document).on('keypress', '.write-message', function (e) {
        if (e.which == 13) {
            sendMessage($(this).val());
            return false;
        }
    });

    $(document).on('click', '.rly-send-msg', function (e) {
        sendMessage($('.write-message').val());
        return false;
    });
	$(document).on('click', '.refresh', function (e) {
 	   	$.pjax.reload({container: "#pjax-container"});
        return false;
    });
    $(document).on('click', '.dialog-item', function (e) {
 	   	$.pjax.reload({container: "#big-pjax", url: $(this).attr('href')});
        return false;
    });
    $(document).on('click', '.advice', function (e) {
 	   	$(this).siblings('.dropdown-menu').slideToggle();
        return false;
    });
    $(document).on('click', function (e) {
        if (!$(e.target).hasClass('remove') && !$(e.target).hasClass('selectize-control') && !$(e.target).hasClass('advice') && !$(e.target).hasClass('dropdown-menu') && !$(e.target).parents('.dropdown-menu, .selectize-control').length) {
            $('.dropdown-menu').slideUp();
        }
    });
    $(document).on('submit', '.advice-form', function (e) {
        var ids = $('.input-container textarea.autocomplete').val();
        $.post('$adviceUrl', {ids: ids}, function(data) {
            if (data.success) {
                socket.emit('sendOffer', data.data);
                
                notyCall('bottom', 'success', 'Предложение отправлено');
                $.pjax.reload({container: "#big-pjax", url: $(this).attr('href')});
            }
        });
        return false;
    });

    socket.emit('isOperatorOnline');
    socket.on('isOperatorOnline', function(result) {
 		console.log(result);
 	});

 	socket.on('update', function(message) {
 		console.log(message);
 	});

 	socket.on('refreshGrid', function() {
        delay(function(){
 	   	    $.pjax.reload({container: "#pjax-container"});
            var audio = new Audio('$assetBaseUrl/media/0477.mp3');
            audio.play();
        }, 1000);
 	});

    socket.on('message', function(response) {
        var dt = new Date();
        var whose = (response.user_id === member_id ? 'user' : 'our');
        var image = '<img src="$assetBaseUrl/images/default-v2.png" />';
        if (response.user_id === member_id) {         
            response.message = response.message.replace(/(http(s)?:\/\/[\w#%?&\/.\-_=\[\]+]*\.(?:jpg|jpeg|png|gif))(?!(?:(?:[^'<>]*')|(?:[^"<>]*"))[^<>]*>)/is, "<a href='$1' target='_blank' rel='nofollow'><img src='$1'></a>");
            response.message = response.message.replace(/(http(s)?:\/\/[\w#%?&\/.\-_=\[\]+]*)(?!(?:(?:[^'<>]*')|(?:[^"<>]*"))[^<>]*>)/is, "<a href='$1' target='_blank' rel='nofollow'>$1</a>");
        }
        if ($('.message.' + whose).length) {
            image = $('.message.' + whose + ' .avatar').html();
        }
        $('.chats').append(
            '<div class="message ' + whose + '">' +
                '<div class="avatar">' + image + '</div>' + 
                '<div class="message-block">' +
                    '<div class="message-body">' +
                        '<p>' + response.message + '</p>' +
                    '</div>' +
                    '<div class="message-props">' +
                        '<div class="left">' +
                            addZero(dt.getHours()) + ':' + addZero(dt.getMinutes()) + ':' + addZero(dt.getSeconds()) + ' <span>' + addZero(dt.getDay())  + '.' + addZero(dt.getMonth()) + '.' + dt.getYear() + '</span>' +
                        '</div>' +
                        '<div class="right">' +
                            '<span>Через: </span> ручное сообщение' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
        scrollBlockToBottom($('.chats'));
 	});
    scrollBlockToBottom($('.chats'));
JS;

$this->registerJs($script); ?>

<?php Pjax::begin(['id' => 'big-pjax', 'timeout' => 6000, 'linkSelector' => false, 'formSelector' => false]); ?>
<div class="left-dialog">
    <?php Pjax::begin(['id' => 'pjax-container', 'timeout' => 6000, 'linkSelector' => false, 'formSelector' => false]); ?>
    <div class="filter-dialog">
        <div class="head-filter">
            <div class="filter-icon"><i class="fa fa-filter"></i></div>
            <div class="filters-left">
                <div class="filter-group menu-select">
                    <div class="filter-group-name">
                        <span class="selected-item"><?= $searchModel->getStatusLabel() ?></span>
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <div class="filter-group-box">
                        <?php foreach ($searchModel::getFilterLabels() as $key => $label) { ?>
                            <a class="filter-group-item"
                               href="<?= Url::current(['LiveDialogMemberSearch' => ['filter' => $key]], true) ?>"><span
                                        class="filter-prop"><?= $label ?></span></a>
                        <?php } ?>
                    </div>
                </div>
                |
                <div class="filter-group menu-select">
                    <div class="filter-group-name">
                        <span class="selected-item">Все операторы</span>
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <div class="filter-group-box big">
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar"></div>
                            <span class="filter-prop">Все операторы</span>
                        </a>
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar"></div>
                            <span class="filter-prop">Никому не назначенные</span>
                        </a>
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar">
                                <?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
                                <div class="online"></div>
                            </div>
                            <span class="filter-prop">Сергей</span></a>
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar">
                                <?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
                                <div class="online off"></div>
                            </div>
                            <span class="filter-prop">Алёна</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="open-filter">
                <i class="fa fa-bars"></i>
            </div>
            <div class="close-filter">
                <i class="fa fa-times"></i>
            </div>
        </div>
    </div>
    <div class="filter-message-body">
        <div class="filter-opened">
            <div class="filter-item">
                <div class="title"><?= $searchModel->getStatusLabel() ?></div>
                <div class="fake-select menu-select">
                    <div class="select-box selected-item">
                        Открытые диалоги
                    </div>
                    <div class="filter-group-box">
                        <?php foreach ($searchModel::getFilterLabels() as $key => $label) { ?>
                            <a class="filter-group-item"
                               href="<?= Url::current(['LiveDialogMemberSearch' => ['filter' => $key]], true) ?>"><span
                                        class="filter-prop"><?= $label ?></span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="filter-item">
                <div class="title">Назначенный оператор</div>
                <div class="fake-select menu-select">
                    <div class="select-box selected-item">
                        Все операторы
                    </div>
                    <div class="filter-group-box big">
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar"></div>
                            <span class="filter-prop">Все операторы</span>
                        </a>
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar"></div>
                            <span class="filter-prop">Никому не назначенные</span>
                        </a>
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar">
                                <?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
                                <div class="online"></div>
                            </div>
                            <span class="filter-prop">Сергей</span>
                        </a>
                        <a class="filter-group-item" href="#">
                            <div class="operator-avatar">
                                <?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
                                <div class="online off"></div>
                            </div>
                            <span class="filter-prop">Алёна</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="filter-item">
                <div class="title">Теги (<span id="tags">0</span>)</div>
                <form>
                    <div class="filter-tag">
                        <input type="search" placeholder="Найти тег по названию">
                    </div>
                </form>
                <div class="text-center">Тег с таким названием отстутствует</div>
            </div>
            <div class="filter-option">
                <div class="text-center"><a class="bigBlue-btn" href="#">Применить фильтр</a></div>
                <div class="text-center"><a class="bigBlue-btn inverse clear-filter" href="#">Очистить фильтр</a></div>
            </div>
        </div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'viewParams' => ['membersOnline' => $membersOnline, 'dialog_id' => $id, 'action' => $action],
            'itemView' => '_member',
            'options' => [
                'class' => 'list-dialog'
            ],
            'itemOptions' => [
                'tag' => false
            ],
            'layout' => "{items}{pager}"
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
<div class="dialog-right">
    <?php if ($id !== null) { ?>
        <div class="dialog-body">
            <div class="dialog-top">
                <div class="left-side">
                    <div class="text">
                        Диалог назначен
                    </div>
                    <div class="fake-select menu-select" data-toggle="tooltip" data-placement="bottom"
                         title="Диалог назначен, назначить на меня(Ctrl+M), снять назначение(Ctrl+B)">
                        <div class="select-box selected-item">
                            Все операторы
                        </div>
                        <div class="filter-group-box big">
                            <a class="filter-group-item" href="#">
                                <div class="operator-avatar"></div>
                                <span class="filter-prop">Все операторы</span>
                            </a>
                            <a class="filter-group-item" href="#">
                                <div class="operator-avatar"></div>
                                <span class="filter-prop">Никому не назначенные</span>
                            </a>
                            <a class="filter-group-item" href="#">
                                <div class="operator-avatar">
                                    <?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
                                    <div class="online"></div>
                                </div>
                                <span class="filter-prop">Сергей</span>
                            </a>
                            <a class="filter-group-item" href="#">
                                <div class="operator-avatar">
                                    <?= Html::img($assetBaseUrl . '/images/default-v2.png') ?>
                                    <div class="online off"></div>
                                </div>
                                <span class="filter-prop">Алёна</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <div class="top-alias">
                        <a class="blue-alias" href="#" data-toggle="tooltip" data-placement="bottom"
                           title="Открыть диалог в отдельном окне(Ctrl+L)">
                            <div class="icon alias"></div>
                            Ссылка на диалог
                        </a>
                        <a class="blue-alias" href="#" data-toggle="tooltip" data-placement="bottom"
                           title="История диалога">
                            <div class="icon history"></div>
                            История диалогов
                        </a>
                        <a class="blue-alias" href="#" data-toggle="tooltip" data-placement="bottom"
                           title="Закрыть диалог(Ctrl+O), закрыть и снять назначение (Ctrl+Q)">
                            <div class="icon xclose"></div>
                            Закрыть диалог
                        </a>
                    </div>
                </div>
            </div>
            <div class="chats">
                <?php
                $previous_member_id = null;
                foreach ($messages as $message) { ?>
                    <?php switch ($message->type) {
                        case LiveDialogMessage::TYPE_SIMPLE_MESSAGE:
                            echo $this->render('message-types/simple-message', [
                                'message' => $message,
                                'adminMember' => $adminMember,
                                'assetBaseUrl' => $assetBaseUrl
                            ]);
                            break;
                        case LiveDialogMessage::TYPE_SERVICE_OFFER:
                            echo $this->render('message-types/offer-message', [
                                'message' => $message,
                                'adminMember' => $adminMember,
                                'assetBaseUrl' => $assetBaseUrl
                            ]);
                            break;
                    } ?>
                <?php } ?>
            </div>
            <div class="write">
                <div class="bottom-tags">
                    <ul class="tags">
                        <li>Тег1 <a class="tag-delete" href="#">&times;</a></li>
                    </ul>
                    <input class="add-tag" type="text" placeholder="+ Добавить тег ...">
                </div>
                <textarea class="write-message" placeholder="Введите сообщение и нажмите Ctrl+Enter"></textarea>
                <div class="buttons-message">
                    <div class="buttons-prop">
                        <a class="msg-btn note" href="#" data-toggle="tooltip" data-placement="top"
                           title="Заметка(Ctrl+H)"></a>
                        <a class="msg-btn save" href="#" data-toggle="tooltip" data-placement="top"
                           title="Сохранённые ответы(Ctrl+S), вставить ответ под номером(Ctrl+1-9)"></a>
                        <a class="msg-btn file" href="#" data-toggle="tooltip" data-placement="top"
                           title="Отправить файл"></a>
                        <a class="msg-btn smile" href="#" data-toggle="tooltip" data-placement="top" title="Смайлы"></a>
                    </div>
                    <a class="send-msg rly-send-msg" href="#">
                        <div class="msg-btn send"></div>
                        Отправить</a>
                    <a class="send-msg advice" href="#">
                        <div class="msg-btn send"></div>
                        Предложить услугу</a>
                    <ul class="dropdown-menu">
                        <li>
                            <?= Html::beginForm(['/livechat/ajax/advice-search'], 'post', ['class' => 'form-inline advice-form']) ?>
                            <div class="form-group input-container">
                                <?= Html::textarea('request', null, ['placeholder' => 'Строка поиска', 'class' => 'autocomplete']) ?>
                            </div>
                            <div class="form-group button-container">
                                <?= Html::submitButton('Предложить', ['class' => 'btn btn-primary']); ?>
                            </div>
                            <?= Html::endForm() ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="user-info">
            <div class="main-info">
                <div class="user-avatar">
                    <div class="avatar">
                        <?= Html::img($userMember->getThumb()) ?>
                    </div>
                    <div class="status <?= in_array($userMember->id, $membersOnline) ? 'on' : 'off' ?>">
                        <?= in_array($userMember->id, $membersOnline) ? 'Онлайн' : 'Оффлайн' ?>
                    </div>
                </div>
                <div class="contact">
                    <div class="contact-item">
                        <i class="ico-user i-name"></i>
                        <?php echo Editable::widget([
                            'model' => $client,
                            'attribute' => 'name',
                            'value' => $client->getName(),
                            'asPopover' => false,
                            'formOptions' => [
                                'action' => ['/livechat/admin/update-ajax', 'id' => $client->id],
                            ],
                            'size' => 'md',
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('livechat', 'Set client name'), 'id' => 'client-name',]
                        ]) ?>
                    </div>
                    <div class="contact-item">
                        <i class="ico-user i-email"></i>
                        <?php echo Editable::widget([
                            'model' => $client,
                            'attribute' => 'email',
                            'value' => $client->getEmail(),
                            'asPopover' => false,
                            'formOptions' => [
                                'action' => ['/livechat/admin/update-ajax', 'id' => $client->id],
                            ],
                            'size' => 'md',
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('livechat', 'Set client email'), 'id' => 'client-email',]
                        ]) ?>
                    </div>
                    <div class="contact-item">
                        <i class="ico-user i-phone"></i>
                        <?php echo Editable::widget([
                            'model' => $client,
                            'attribute' => 'phone',
                            'value' => $client->getPhone(),
                            'asPopover' => false,
                            'formOptions' => [
                                'action' => ['/livechat/admin/update-ajax', 'id' => $client->id],
                            ],
                            'size' => 'md',
                            'options' => ['class' => 'form-control', 'placeholder' => Yii::t('livechat', 'Set client phone'), 'id' => 'client-phone',]
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="all-info">
                <div class="white-block">
                    <div class="current-page"><i
                                class="ico-user i-page"></i><?= $client->isOnline() ? Yii::t('livechat', 'Current page') : Yii::t('livechat', 'Last page') ?>
                    </div>
                    <?= Html::a($client->lastHistory->url, $client->lastHistory->url, ['target' => '_blank']) ?>
                </div>
                <div class="white-block">
                    <div class="title">
                        Сегменты (<span>4</span>)
                    </div>
                    <div class="segments">
                        <span>Отправлен поп-ап</span>
                        <span>Общались в течении суток</span>
                        <span>Были больше 1 раза</span>
                        <span>Активные за 30 дней</span>
                    </div>
                </div>
                <div class="white-block">
<!--                    <div class="new-prop">-->
<!--                        <div class="title">-->
<!--                            Ваши свойства пользователя-->
<!--                        </div>-->
<!--                        <a class="plus-prop" href="#"><i class="fa fa-plus"></i> Добавить свойство</a>-->
<!--                    </div>-->
                    <div class="title">
                        Системные свойства
                    </div>
                    <div class="system-props">
                        <div class="prop-item">
                            <div class="prop-title">Краткая информация</div>
                            <div class="prop-text"><?= nl2br($userMember->liveClient->getShortInfo()) ?><br>
                                <?= Html::a(Yii::t('livechat', 'View detailed information'), ['/livechat/admin/view', 'id' => $userMember->liveClient->id],
                                    [
                                        'title' => 'Информация о "' . $userMember->liveClient->getName() . '"',
                                        'data-pjax' => 0,
                                        'class' => 'modal-view'
                                    ]) ?>
                            </div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">Сессии</div>
                            <div class="prop-text"><?= $userMember->liveClient->sessions_count ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">Локация</div>
                            <div class="prop-text"><?= $userMember->liveClient->getAddress() ?? '(Не определен)' ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">Первый источник</div>
                            <div class="prop-text"><?= $userMember->liveClient->advert_referrer ?? '(Нет)' ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">Первый источник (домен)</div>
                            <div class="prop-text"><?= $userMember->liveClient->advert_source ?? '(Нет)' ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">Ключевое слово рекламы</div>
                            <div class="prop-text"><?= $userMember->liveClient->advert_keyword ?? '(Нет)' ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">Последняя активность</div>
                            <div class="prop-text"><?= Yii::$app->formatter->asDatetime($userMember->liveClient->last_action_at) ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">User ID</div>
                            <div class="prop-text"><?= $userMember->liveClient->user_id ?? '(не определен)' ?></div>
                        </div>
                        <div class="prop-item">
                            <div class="prop-title">uJobs crm ID</div>
                            <div class="prop-text"><?= $userMember->live_client_id ?></div>
                        </div>
<!--                        <div class="prop-item">-->
<!--                            <div class="prop-title">Почтовые рассылки</div>-->
<!--                            <select name="subscribe">-->
<!--                                <option value="Подписан">Подписан</option>-->
<!--                                <option value="Отписан">Отписан</option>-->
<!--                            </select>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <?php
        $name = $adminMember->liveClient->getName();
        $script = <<<JS
            member_id = '$member_id';
            var	client_id = '$client_id',
				dialog_id = '$dialog_id',
				name = '$name',
				data = {
					'member_id': member_id,
					'dialog_id': dialog_id,
					'client_id': client_id,
					'name': name,
					'access': 'patchworkwantstoplay'
				};
			socket.emit('joinServer', data);
			
			$(".advice-form .autocomplete").selectize({
				valueField: "id",
				labelField: "label",
				searchField: ["label"],
				plugins: ["remove_button"],
				dropdownDirection: 'up',
				persist: false,
				//create: true,
				maxOptions: 10,
				"load": function(query, callback) {
					if (!query.length) {
						return callback();
					}
					$.post($('.advice-form').attr('action'), {request: encodeURIComponent(query)}, function(data) {
						callback(data.data);
					}).fail(function() {
						callback();
					});
				},
				sortField: [[]],
			});
JS;
        $this->registerJs($script); ?>
    <?php } ?>
</div>
<?php Pjax::end(); ?>
