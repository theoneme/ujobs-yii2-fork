<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.06.2017
 * Time: 16:25
 */

use yii\widgets\ListView;

/**
 * @var $operatorIds array
 */

\yii\widgets\ActiveFormAsset::register($this);

$liveUrl = Yii::$app->params['liveUrl'];
$script = <<<JS
    socket = io.connect('$liveUrl');
	socket.emit('ifyouwannabeasmartandfuriousthengetyourcarandsmashthiswall');
JS;

$this->registerJs($script);
?>

<div class="live-head">
	<div class="left-side">
		<div class="live-title">
			Live
			<a class="icon-refresh" href="#">
				<i class="fa fa-refresh"></i>
			</a>
		</div>
	</div>
	<div class="right-side">
		На этой странице показаны активные посетители, находящиеся на сайте прямо сейчас
	</div>
</div>
<div class="live-content-over">
	<div class="live-content">
		<div class="list-online">
			<table class="list-online-table">
				<thead>
				<tr>
					<th>Имя посетителя</th>
					<th></th>
					<th>Зашел <br> на сайт</th>
					<th>Страница</th>
					<th>Страна</th>
					<th>Регион</th>
					<th>Город</th>
					<th>Источник</th>
					<th><div class="icon-msg1" title="Время с последнего сообщения"></div></th>
					<th><div class="icon-msg2" title="Время с последнего ответа посетителя"></div></th>
					<th><div class="icon-visit" title="Количество сессий посетителя"></div></th>
					<th></th>
				</tr>
				</thead>
				<?php echo ListView::widget([
					'dataProvider' => $dataProvider,
					'viewParams' => ['operatorIds' => $operatorIds],
					'itemView' => 'live-client-partial',
					'layout' => "{items}{pager}",
					'options' => [
						'tag' => 'tbody',
						'class' => ''
					],
					'emptyText' => '<tr><td colspan="11">Никого нет дома</td></tr>',
					'itemOptions' => [
						'tag' => 'tr',
						'data-toggle' => '#modal'
					],
				]);?>
			</table>
		</div>
	</div>
</div>
