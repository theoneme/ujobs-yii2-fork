<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.06.2017
 * Time: 17:55
 */
use common\modules\livechat\models\LiveClientHistory;
use yii\helpers\Html;

/* @var LiveClientHistory $model */

?>

<div class="left-line"></div>
<div class="circle-cell">
    <div class="circle-arrow"></div>
</div>
<div class="time-cell">
    <p><?= Yii::$app->formatter->asDate($model->created_at) ?></p>
    <p><?= Yii::$app->formatter->asTime($model->created_at) ?></p>
</div>
<div class="props-cell">
    <div class="props-title">
        <?= $model->getLabel() ?>
    </div>
    <div class="props-table">
        <?php if ($model->url) { ?>
            <div class="props-row">
                <div class="props-name">Страница</div>
                <div class="props-text"><?= Html::a($model->url, $model->url, ['target' => '_blank']) ?></div>
            </div>
        <?php } ?>
        <?php if (array_key_exists('message', $model->customDataArray)) { ?>
            <?php if (array_key_exists('message', $model->customDataArray['message'])) { ?>
                <div class="props-row">
                    <div class="props-name">Сообщение</div>
                    <div class="props-text"><?= $model->customDataArray['message']['message'] ?></div>
                </div>
            <?php } ?>
            <?php if (array_key_exists('dialogId', $model->customDataArray['message'])) { ?>
                <div class="props-row">
                    <div class="props-name">ID диалога</div>
                    <div class="props-text"><?= $model->customDataArray['message']['dialogId'] ?></div>
                </div>
            <?php } ?>
            <?php if (array_key_exists('messageId', $model->customDataArray['message'])) { ?>
                <div class="props-row">
                    <div class="props-name">ID сообщения</div>
                    <div class="props-text"><?= $model->customDataArray['message']['messageId'] ?></div>
                </div>
            <?php } ?>
        <?php } ?>
        <?php if (array_key_exists('offers', $model->customDataArray)) { ?>
            <div class="props-row">
                <div class="props-name">Товар</div>
                <div class="props-text"><?= implode(', ', array_map(function ($value) {
                        return Html::a($value['label'] . "({$value['price']})", $value['url'], ['target' => '_blank']);
                    }, $model->customDataArray['offers'])); ?></div>
            </div>
        <?php } ?>
        <?php if (array_key_exists('postJob', $model->customDataArray)) { ?>
            <div class="props-row">
                <div class="props-name">Услуга</div>
                <div class="props-text"><?= Html::a($model->customDataArray['postJob']['label'] . " ({$model->customDataArray['postJob']['price']})", $model->customDataArray['postJob']['url'], ['target' => '_blank']);
                    ?>
                </div>
            </div>
        <?php } ?>
        <?php if (array_key_exists('postTender', $model->customDataArray)) { ?>
            <div class="props-row">
                <div class="props-name">Заявка</div>
                <div class="props-text"><?= "{$model->customDataArray['postTender']['label']} ({$model->customDataArray['postTender']['price']})";
                    ?>
                </div>
            </div>
        <?php } ?>
        <?php if (array_key_exists('addPayment', $model->customDataArray)) { ?>
            <div class="props-row">
                <div class="props-name">Что оплатил</div>
                <div class="props-text"><?= $model->customDataArray['addPayment']['orders'] ?>
                </div>
            </div>
            <div class="props-row">
                <div class="props-name">Сумма</div>
                <div class="props-text"><?= "{$model->customDataArray['addPayment']['revenue']} {$model->customDataArray['addPayment']['currency']} "  ?>
                </div>
            </div>
        <?php } ?>
        <?php if (array_key_exists('cartAdd', $model->customDataArray)) { ?>
            <div class="props-row">
                <div class="props-name">Товар</div>
                <div class="props-text"><?= Html::a($model->customDataArray['cartAdd']['label'] . " ({$model->customDataArray['cartAdd']['price']})", $model->customDataArray['cartAdd']['url'], ['target' => '_blank']); ?></div>
            </div>
        <?php } ?>
        <?php if (array_key_exists('cartRemove', $model->customDataArray)) { ?>
            <div class="props-row">
                <div class="props-name">Товар</div>
                <div class="props-text"><?= Html::a($model->customDataArray['cartRemove']['label'] . " ({$model->customDataArray['cartRemove']['price']})", $model->customDataArray['cartRemove']['url'], ['target' => '_blank']); ?></div>
            </div>
        <?php } ?>
        <?php if ($model->ip !== null) { ?>
            <div class="props-row">
                <div class="props-name">IP адрес</div>
                <div class="props-text"><?= $model->ip ?></div>
            </div>
        <?php } ?>
        <?php if ($model->browser !== null) { ?>
            <div class="props-row">
                <div class="props-name">Браузер</div>
                <div class="props-text"><?= $model->browser ?></div>
            </div>
        <?php } ?>
        <?php if ($model->referrer !== null) { ?>
            <div class="props-row">
                <div class="props-name">Источник</div>
                <div class="props-text"><?= $model->referrer ?></div>
            </div>
        <?php } ?>
        <?php if ($model->advert_compaign !== null) { ?>
            <div class="props-row">
                <div class="props-name">UTM-компания</div>
                <div class="props-text"><?= $model->advert_compaign ?></div>
            </div>
        <?php } ?>
        <?php if ($model->advert_keyword !== null) { ?>
            <div class="props-row">
                <div class="props-name">UTM-ключевое слово</div>
                <div class="props-text"><?= $model->advert_keyword ?></div>
            </div>
        <?php } ?>
        <?php if ($model->advert_source !== null) { ?>
            <div class="props-row">
                <div class="props-name">UTM-источник</div>
                <div class="props-text"><?= $model->advert_source ?></div>
            </div>
        <?php } ?>
        <?php if ($model->os !== null) { ?>
            <div class="props-row">
                <div class="props-name">ОС</div>
                <div class="props-text"><?= $model->os ?></div>
            </div>
        <?php } ?>
        <?php if ($model->device !== null) { ?>
            <div class="props-row">
                <div class="props-name">Устройство</div>
                <div class="props-text"><?= $model->device ?></div>
            </div>
        <?php } ?>
    </div>
</div>