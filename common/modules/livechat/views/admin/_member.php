<?php
use common\modules\livechat\models\LiveDialogMember;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model LiveDialogMember */
/* @var $dialog_id integer */
/* @var $action string */

$url = Url::current(['id' => $model->live_dialog_id]);

$messageClass = null;
if($model->live_dialog_id == $dialog_id) {
    $messageClass = 'active';
} else if($model->liveDialog->lastMessage->member_id === $model->id) {
    $messageClass = 'unread';
}

?>
<?php if($model->liveClient !== null) { ?>
    <a href="<?= $url ?>" class="dialog-item <?= $messageClass ?>">
        <div class="dialog-head">
            <div class="user-name">
                <div class="name"><?= $model->liveClient->getName() ?></div>
                <div class="online <?= in_array($model->id, $membersOnline) ? '' : 'off' ?>"></div>
            </div>
            <div class="dialog-time">
                <?= date('H:i:s', $model->liveDialog->lastMessage->created_at) ?>
                <span>
                    <?= date('d.m.Y', $model->liveDialog->lastMessage->created_at) ?>
                </span>
            </div>
        </div>
        <div class="dialog-message">
            <div class="avatar">
                <?= Html::img($model->getThumb()) ?>
            </div>
            <div class="short-message">
                <p><?= strip_tags($model->liveDialog->lastMessage->message) ?></p>
            </div>
            <div class="right-col">
                <div class="count"><?= count($model->liveDialog->messages) ?></div>
            </div>
        </div>
    </a>
<?php } ?>


<!--Как это должно появится я пока хз-->

<!--<a href="#" class="dialog-item close-dialog">-->
<!--    <div class="dialog-head">-->
<!--        <div class="user-name">-->
<!--            <div class="name">Паша</div>-->
<!--            <div class="online"></div>-->
<!--        </div>-->
<!--        <div class="dialog-time">-->
<!--            14:39 <span>06.06.17</span>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="dialog-message">-->
<!--        <div class="avatar">-->
<!--            <img src="/images/default-v2.png" alt="avatar">-->
<!--        </div>-->
<!--        <div class="short-message">-->
<!--            <p>-->
<!--                --- вопрос диалога разрешен ----->
<!--            </p>-->
<!--        </div>-->
<!--        <div class="right-col">-->
<!--            <div class="count">-->
<!--                2-->
<!--            </div>-->
<!--            <div class="who-avatar">-->
<!--                <img src="../images/default-v2.png" alt="avatar">-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="tags">-->
<!--        <div class="dialog-tag">tag1</div>-->
<!--        <div class="dialog-tag">tag1</div>-->
<!--    </div>-->
<!--</a>-->