<?php

use common\modules\livechat\models\search\LiveContactEmailSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $searchModel LiveContactEmailSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Контактные емейлы';
?>
<div class="setting-index">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title"><?= Html::encode($this->title) ?></h3>
		</div>
		<div class="box-body">
			<?php Pjax::begin(); ?>
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [

					'name',
					'email',
					'phone',
					'created_at:datetime',
					[
						'attribute' => 'message',
						'format' => 'ntext',
						'contentOptions' => ['style' => 'white-space: normal;'],
					],
				],
			]); ?>
			<?php Pjax::end(); ?>
		</div>
	</div>
</div>