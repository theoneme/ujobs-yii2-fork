<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 20.06.2017
 * Time: 17:55
 */
use common\modules\livechat\models\LiveClientHistory;
use yii\helpers\Html;

/* @var LiveClientHistory[] $items */

?>

<?php if (count($items) > 0) { ?>
    <?php foreach ($items as $history) { ?>
        <div class="chronology-item">
            <div class="left-line"></div>
            <div class="circle-cell">
                <div class="circle-arrow"></div>
            </div>
            <div class="time-cell">
                <p><?= Yii::$app->formatter->asDate($history->created_at) ?></p>
                <p><?= Yii::$app->formatter->asTime($history->created_at) ?></p>
            </div>
            <div class="props-cell">
                <div class="props-title">
                    <?= $history->getLabel() ?>
                </div>
                <div class="props-table">
                    <?php if ($history->url) { ?>
                        <div class="props-row">
                            <div class="props-name">Страница</div>
                            <div class="props-text"><?= Html::a($history->url, $history->url, ['target' => '_blank']) ?></div>
                        </div>
                    <?php } ?>
                    <?php if (array_key_exists('message', $history->customDataArray)) { ?>
                        <?php if (array_key_exists('message', $history->customDataArray['message'])) { ?>
                            <div class="props-row">
                                <div class="props-name">Сообщение</div>
                                <div class="props-text"><?= $history->customDataArray['message']['message'] ?></div>
                            </div>
                        <?php } ?>
                        <?php if (array_key_exists('dialogId', $history->customDataArray['message'])) { ?>
                            <div class="props-row">
                                <div class="props-name">ID диалога</div>
                                <div class="props-text"><?= $history->customDataArray['message']['dialogId'] ?></div>
                            </div>
                        <?php } ?>
                        <?php if (array_key_exists('messageId', $history->customDataArray['message'])) { ?>
                            <div class="props-row">
                                <div class="props-name">ID сообщения</div>
                                <div class="props-text"><?= $history->customDataArray['message']['messageId'] ?></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if (array_key_exists('offers', $history->customDataArray)) { ?>
                        <div class="props-row">
                            <div class="props-name">Товар</div>
                            <div class="props-text"><?= implode(', ', array_map(function ($value) {
                                    return Html::a($value['label'] . "({$value['price']})", $value['url'], ['target' => '_blank']);
                                }, $history->customDataArray['offers'])); ?></div>
                        </div>
                    <?php } ?>
                    <?php if (array_key_exists('postJob', $history->customDataArray)) { ?>
                        <div class="props-row">
                            <div class="props-name">Услуга</div>
                            <div class="props-text"><?= Html::a($history->customDataArray['postJob']['label'] . " ({$history->customDataArray['postJob']['price']})", $history->customDataArray['postJob']['url'], ['target' => '_blank']);
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (array_key_exists('postTender', $history->customDataArray)) { ?>
                        <div class="props-row">
                            <div class="props-name">Заявка</div>
                            <div class="props-text"><?= "{$history->customDataArray['postTender']['label']} ({$history->customDataArray['postTender']['price']})";
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (array_key_exists('addPayment', $history->customDataArray)) { ?>
                        <div class="props-row">
                            <div class="props-name">Что оплатил</div>
                            <div class="props-text"><?= $history->customDataArray['addPayment']['orders'] ?>
                            </div>
                        </div>
                        <div class="props-row">
                            <div class="props-name">Сумма</div>
                            <div class="props-text"><?= "{$history->customDataArray['addPayment']['revenue']} {$history->customDataArray['addPayment']['currency']} "  ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (array_key_exists('cartAdd', $history->customDataArray)) { ?>
                        <div class="props-row">
                            <div class="props-name">Товар</div>
                            <div class="props-text"><?= Html::a($history->customDataArray['cartAdd']['label'] . " ({$history->customDataArray['cartAdd']['price']})", $history->customDataArray['cartAdd']['url'], ['target' => '_blank']); ?></div>
                        </div>
                    <?php } ?>
                    <?php if (array_key_exists('cartRemove', $history->customDataArray)) { ?>
                        <div class="props-row">
                            <div class="props-name">Товар</div>
                            <div class="props-text"><?= Html::a($history->customDataArray['cartRemove']['label'] . " ({$history->customDataArray['cartRemove']['price']})", $history->customDataArray['cartRemove']['url'], ['target' => '_blank']); ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->ip !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">IP адрес</div>
                            <div class="props-text"><?= $history->ip ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->browser !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">Браузер</div>
                            <div class="props-text"><?= $history->browser ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->referrer !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">Источник</div>
                            <div class="props-text"><?= $history->referrer ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->advert_compaign !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">UTM-компания</div>
                            <div class="props-text"><?= $history->advert_compaign ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->advert_keyword !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">UTM-ключевое слово</div>
                            <div class="props-text"><?= $history->advert_keyword ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->advert_source !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">UTM-источник</div>
                            <div class="props-text"><?= $history->advert_source ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->os !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">Источник (домен)</div>
                            <div class="props-text"><?= $history->os ?></div>
                        </div>
                    <?php } ?>
                    <?php if ($history->device !== null) { ?>
                        <div class="props-row">
                            <div class="props-name">Устройство</div>
                            <div class="props-text"><?= $history->device ?></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="no-data">Событий данного типа не было выполнено пользователем</div>
<?php } ?>
