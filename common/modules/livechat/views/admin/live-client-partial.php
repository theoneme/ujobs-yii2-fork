<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.06.2017
 * Time: 17:28
 */
use common\modules\livechat\models\LiveClient;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var LiveClient $model
 * @var $operatorIds array
 */

if ($model->dialog !== null) {
    $lastAnswer = $model->dialog->getLastAnswer($operatorIds);
}
?>

<td>
	<div class="author">
		<div class="avatar-author">
			<?= Html::img($model->avatar) ?>
			<div class="online-author"></div>
		</div>
		<div class="name-author"><?= Html::a($model->getName(),
                ['/livechat/admin/view', 'id' => $model->id],
                [
                    'title' => 'Переписка с пользователем "' . $model->getName() . '"',
                    'data-pjax' => 0,
                    'class' => 'modal-view'
                ]
            ); ?>
        </div>
	</div>
</td>
<td><div class="write-user modal-send" title="Написать посетителю" href="<?= Url::to(['/livechat/admin/message', 'id' => $model->id]) ?>"></div></td>
<td><?= $model->getActivityTime() ?></td>
<td><?= isset($model->lastOpenPageHistory) ? Html::a($model->lastOpenPageHistory->url, [$model->lastOpenPageHistory->url]) : null ?></td>
<td>
	<?= $model->country ?>
</td>
<td><?= $model->region ?></td>
<td><?= $model->city ?></td>
<td><?= $model->getAdvertReferrer() ?></td>
<td><?= Yii::$app->formatter->format(isset($model->dialog->lastMessage) ? $model->dialog->lastMessage->created_at : null, 'relativeTime') ?></td>
<td><?= Yii::$app->formatter->format(isset($lastAnswer) ? $lastAnswer->created_at : null, 'relativeTime') ?></td>
<td><?= $model->sessions_count ?></td>
<td><i class="fa fa-chevron-right"></i></td>
