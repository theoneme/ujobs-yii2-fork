<?php

use yii\helpers\Html;
use common\controllers\BackEndController;
use yii\widgets\Menu;

/* @var BackEndController $controller */
$controller = Yii::$app->controller;
?>
<nav class="left-menu">
    <div class="avatar">
        <a href="/">
            <?= Html::img($this->params['assetBaseUrl'] . '/images/default-v2.png', ['class' => 'img-circle']) ?>
        </a>
    </div>
    <?= Menu::widget(
        [
            'options' => ['class' => 'nav nav-tabs'],
            'items' => [
                [
                    'label' => 'Live',
                    'options' => ['class' => 'nav-live'],
                    'url' => ['/livechat/admin/live'],
                    'active' => $controller->id == 'admin' && $controller->action->id == 'live',
                    'template'=> '<a href="{url}"><i class="fa fa-laptop"></i><div class="nav-title">{label}</div></a>',
                ],
                [
                    'label' => 'Лиды',
                    'options' => ['class' => 'nav-leads'],
                    'url' => ['/livechat/admin/index'],
                    'active' => $controller->id == 'admin' && $controller->action->id == 'index',
                    'template'=> '<a href="{url}"><i class="fa fa-users"></i><div class="nav-title">{label}</div></a>',
                ],
                [
                    'label' => 'Реклама',
                    'options' => ['class' => 'nav-adverts'],
                    'url' => ['/livechat/admin/adverts'],
                    'active' => $controller->id == 'admin' && $controller->action->id == 'adverts',
                    'template'=> '<a href="{url}"><i class="fa fa-info"></i><div class="nav-title">{label}</div></a>',
                ],
                [
                    'label' => 'Диалоги',
                    'options' => ['class' => 'nav-dialogs'],
                    'url' => ['/livechat/admin/dialog'],
                    'active' => $controller->id == 'admin' && $controller->action->id == 'dialog',
                    'template'=> '<a href="{url}"><i class="fa fa-commenting-o"></i><div class="nav-title">{label}</div></a>',
                ],
                [
                    'label' => 'История',
                    'options' => ['class' => 'nav-dialogs'],
                    'url' => ['/livechat/admin/history', 'LiveDialogMemberSearch' => ['filter' => \common\modules\livechat\models\search\LiveDialogMemberSearch::FILTER_ALL]],
                    'active' => $controller->id == 'admin' && $controller->action->id == 'history',
                    'template'=> '<a href="{url}"><i class="fa fa-commenting-o"></i><div class="nav-title">{label}</div></a>',
                ]
            ],
        ]
    ) ?>
</nav>
