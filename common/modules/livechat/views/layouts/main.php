<?php
use common\modules\livechat\assets\LivechatAdminAsset;
use common\modules\livechat\assets\LivechatAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

if (Yii::$app->controller->action->id === 'login') {
	echo $this->render('main-login', ['content' => $content]);
}
else {
	LivechatAsset::register($this);
	$this->params['assetBaseUrl'] = LivechatAdminAsset::register($this)->baseUrl;
	$this->beginPage(); ?>
	<!DOCTYPE html>
	<html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
            <?php $this->head() ?>
        </head>

        <body class="skin-purple sidebar-mini">
            <?php $this->beginBody() ?>
            <div class="mainblock">
                <?= $this->render('left.php') ?>
                <div class="wrapper">
                    <?= $content ?>
                </div>
            </div>
            <div class="modal fade" id="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header header-col text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-body"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="userForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-userForm">
                    <div class="modal-content">

                    </div>
                </div>
            </div>
            <div class="modal fade" id="sendForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sendForm">
                    <div class="modal-content">

                    </div>
                </div>
            </div>
            <?php $script = <<<JS
            $(document).on('click', '.modal-dismiss', function () {
                var target = $(this).data('target');
                if (target) {
                    $('.modal:not(' + target + ')').modal('hide');
                }
                else {
                    $('.modal').modal('hide');
                }
            });
            $(document).on('click', '.modal-send', function () {
                var tab = $(this).data('tab');
                if (tab) {
                    $('#sendForm').one('shown.bs.modal', function() {
                        $('a[href="' + tab + '"]').tab('show');
                    });
                }
                $('#sendForm')
                    .find('.modal-content')
                    .load($(this).attr('href'))
                    .closest('#sendForm')
                    .modal('show')
                    .find('.modal-header')
                    .html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3>' + $(this).attr("title") + '</h3>');
                return false;
            });
JS;
            $this->registerJs($script); ?>

            <?php $this->endBody() ?>
        </body>
	</html>
	<?php $this->endPage() ?>
<?php } ?>
