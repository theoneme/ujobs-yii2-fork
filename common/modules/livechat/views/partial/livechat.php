<?php

use common\modules\livechat\assets\LivechatAsset;
use common\modules\livechat\models\LiveContactEmail;
use yii\helpers\Html;
use yii\helpers\Url;

$assetBaseUrl = LivechatAsset::register($this)->baseUrl;

$getMemberRoute = Url::to(['/livechat/ajax/get-member']);
$clientUrl = Url::to(['/livechat/ajax/init-client']);
$liveUrl = Yii::$app->params['liveUrl'];
$loadMoreUrl = Url::to(['/livechat/ajax/load-more']);
$moderImg = Html::img(['/images/new/moder.jpg']);
$imHereText = Yii::t('livechat', 'I\'m here if you need me');
$isGuest = $isGuest ? 1 : 0;


/* @var $contactForm LiveContactEmail */
/* @var boolean $autoShow */

?>
	<div class="modal-popup">

	</div>
	<div class="online-chat">
		<div class="chat-button open" href="#">
			<div class="site-chat">
				<div class="msg default">
					<div><?= $imHereText?></div>
				</div>
				<div class="chat-icon">
                    <div class="offline-icon">
                        <i class="fa fa-envelope"></i>
                    </div>

					<?= Html::img($assetBaseUrl . '/images/moder.jpg') ?>
					<div class="count" style="display: none">0</div>
				</div>
			</div>
		</div>
		<div class="chat-block">
			<div class="chat-head">
				<div class="operator">
					<div class="avatar">
                        <?= $moderImg ?>
					</div>
					<div class="name"><?= Yii::t('livechat', 'Victoria')?></div>
				</div>
				<a href="#" class="chat-close"></a>
			</div>
            <div class="chat-load-more">
                <?= Html::a(Yii::t('livechat', 'Show earlier dialogs'))?>
            </div>
			<div class="chat-body">

			</div>
			<div class="chat-bottom">
				<textarea class="chat-message" placeholder="<?= Yii::t('livechat', 'Type your message and press Enter')?>"></textarea>
				<a class="send-message rly-send-message" href="#"></a>
			</div>
		</div>
	</div>

<?
$script = <<<JS
	var socket = io.connect('$liveUrl'),
	    member_id = '',
	    dialog_id = '',
	    client_id = '',
	    connected = false;

	function scrollBlockToBottom(block) {
	    if (block !== undefined) {
	        let height = block.scrollHeight;
	        block.scrollTop(9000);

	        return true;
	    }

	    return false;
	}

	function renderMessage(whose, message) {
	    return Live.renderMessage(whose, message);
	}

	function renderPopup(type, message) {
	    return Live.renderPopup(type, message);
	}

	function renderOffers(data) {
	    return Live.renderDiscountOffers(data);
	}

	function initClient() {
	    Live.setAssetPath('$assetBaseUrl');
	    $.post('$getMemberRoute', {},
	        function(response) {
	            member_id = response.member_id;
	            dialog_id = response.dialog_id;
	            client_id = response.client_id;
	            let chatBody = $('.chat-body'),
	                noOperatorMessage = response.no_operator_message,
	                data = {
                        "member_id": member_id,
                        "dialog_id": dialog_id,
                        "client_id": client_id
                    },
	                submitted = false;
	            chatBody.append(response.history);
	            if (response.loadMore) {
	                $('.chat-load-more').addClass('open'); 
	            }
	            $('.chat-button').removeClass('open');

	            socket.emit('joinServer', data);
	            socket.once('joined', function() {
	                connected = true;
	            });

	            function sendMessage(message) {
	                if (connected === true) {
	                    if (message.length > 0) {
	                        $('.chat-message').val('');
	                        socket.emit('sendMessage', message);
	                    }
	                }
	            }

	            $(document).on('keypress', '.chat-message', function(e) {
	                if (e.which === 13) {
	                    sendMessage($(this).val());
	                    return false;
	                }
	            });

	            $(document).on('click', '.rly-send-message', function(e) {
	                sendMessage($('.chat-message').val());
	                return false;
	            });
	            
	            $(document).on('click', 'body', function(e) {
                    if (!$(e.target).parents('.chat-block').length && $('.chat-block').is(':visible')) {
                        Live.forceClose();
                    }
	            });

	            $(document).on('submit', '#live_contact_form', function() {
	                $.post($(this).attr('action'),
	                    $(this).serialize(),
	                    function(data) {
	                        if (data.success) {
	                            $('#live_contact_form').remove();
	                            submitted = true;
	                        }
	                    },
	                    "json"
	                );
	                
	                return false;
	            });
	            $(document).on('click', '.chat-button, .chat-block .chat-close', function() {
	                $('body').toggleClass('no-overflow');
	                $('.chat-button, .chat-block').toggleClass('open');
	                scrollBlockToBottom($('.chat-body'));
	                $('.site-chat .msg').addClass('default').children('div').html("$imHereText");
	                $('.site-chat .chat-icon .count').html('0').hide();
	                $('.pop-up.small').hide();

	                return false;
	            });
	            
	            $(document).on('click', '#live_contact_form_submit', function() {
	                $(this).closest('form').submit();
	                $('.message#no-operator').show();
	                
	                return false;
	            });

	            $(document).on('click', '.chat-close', function() {
	                Live.doNotShow();
	                return false;
	            });
                $(document).on('click', '.chat-load-more', function() {
                    var time = $('.chat-block .message[data-timestamp]').length ? $('.chat-block .message[data-timestamp]').first().data('timestamp') : null;
	                $.post('$loadMoreUrl', 
	                    {dialog_id: dialog_id, time: time},
	                    function(response) {
                            chatBody.prepend(response.history);
                            if (!response.loadMore) {
                                $('.chat-load-more').removeClass('open'); 
                            }
	                    },
	                    "json"
	                );

	                return false;
	            });

	            socket.on('offer', function(data) {
	                chatBody.append(renderOffers(data));
	            });

	            socket.on('message', function(response) {
	                var whose = response.user_id === member_id ? 'user' : 'our';
	                chatBody.append(renderMessage(whose, response.message));
	                if (whose !== 'user') {
	                    $('.site-chat .msg').removeClass('default').children('div').html(response.message);
	                    $('.site-chat .chat-icon .count').html(parseInt($('.site-chat .chat-icon .count').html()) + 1).show();
	                }

	                chatBody.append(noOperatorMessage);
	                socket.emit('isOperatorOnline');
	                
	                scrollBlockToBottom(chatBody);
	            });

	            socket.on('popup', function(result) {
	                if (result.type == 'big') {
	                    $('.modal-popup').html(renderPopup(result.type, result.message)).addClass('open');
	                } else {
	                    $('.pop-up.small, .chat-body .pop-up').remove();
	                    $('.online-chat').before(renderPopup(result.type, result.message));
	                    chatBody.prepend(renderPopup('', result.message));
	                    chatBody.scrollTop(0);
	                }
	            });

	            socket.on('isOperatorOnline', function(result) {
	                let formExists = $('#no-operator-form').length > 0;
	                if (!result) {
	                    if($isGuest === 1 && formExists && !submitted) {
	                        $('.message#no-operator-form').show();
                        } else {
	                        $('.message#no-operator').show();
	                    }         
	                } else {
	                    $('.chat-icon .offline-icon').hide();
	                    $('.chat-icon img').show();
	                }
	            });

	            Live.processScenario(response.scenario);
	        },
	        "json"
	    );
	}

	setTimeout(initClient, 1500);
JS;

$this->registerJs($script);