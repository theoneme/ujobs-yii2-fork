<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 28.06.2017
 * Time: 14:00
 */
use common\modules\livechat\models\LiveClient;
use yii\helpers\Html;

/* @var LiveClient $model */
/* @var string $assetBaseUrl */

?>

<table style="padding:30px 10px;background:#eee;width:100%" cellspacing="0" cellpadding="0">
	<tbody>
	<tr>
		<td>
			<table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
				<tbody>
				<tr>
					<td style="text-align:left;;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
						<a target="_blank" data-saferedirecturl=""
						   style="text-decoration: none;display: block;">
							<?= Html::img($assetBaseUrl . '/images/logo.png') ?>
						</a>
					</td>
				</tr>
				<tr>
					<td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
						<table align="center" style="width: 100%">
							<tbody>
							<tr>
								<td style="border-bottom:1px solid #dfdfd0;color:#666;text-align:center;padding-bottom:30px">
									<table style="margin:auto; width: 100%;" align="center">
										<tbody>
										<tr>
											<td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
												Привет, <?= $model->getName() ?>
											</td>
										</tr>
										<tr>
											<td style="height: 10px;"></td>
										</tr>
										</tbody>
									</table>
									<table style="margin:auto; width: 100%;" align="center">
										<tbody>
										<tr>
											<td style="height: 10px"></td>
										</tr>
										<tr>
											<td id="email-text" style="color:#666;font-size:18px;text-align:left">
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="color:#aaa;padding:15px;font-size:11px;line-height:15px;text-align:left">
									<div style="color:#aaa;font-family:arial">
										Также хорошая идея добавить support@ujobs.me в Вашу адресную книгу чтобы
										получать наши письма (без спама, обещаем!)
									</div>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="max-width:650px" align="center">
				<tbody>
				<tr>
					<td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px">
						© uJobs
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
