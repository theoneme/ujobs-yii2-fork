$(document).on('click', 'a.modal-view', function () {
    $('#userForm')
        .find('.modal-content')
        .load($(this).attr('href'))
        .closest('#userForm')
        .modal('show')
        .find('.modal-header')
        .html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3>' + $(this).attr("title") + '</h3>');

    return false;
});

$('.column-check input:checkbox').prop('checked', true);

$('body').on('change','.column-check input:checkbox', function(){
    var index = $(this).parent().index() + 2;
    if($(this).prop('checked')){
        $('.list-messages tr td:nth-child(' + index + '), .list-messages tr th:nth-child(' + index + ')').show();
    } else {
        $('.list-messages tr td:nth-child(' + index + '), .list-messages tr th:nth-child(' + index + ')').hide();
    }
});

function notyCall(layout, type, text) {
    noty({
        layout: layout,
        type: type,
        text: text,
        textAlign: "center",
        easing: "swing",
        animation: {
            open: {
                height: "toggle"
            },
            close: {
                height: "toggle"
            },
            easing: "swing",
            speed: 500
        },
        speed: 1500,
        timeout: 2000,
        closable: true,
        closeOnSelfClick: true
    });
}