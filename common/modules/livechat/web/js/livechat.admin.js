$(document).ready(function(){
    //$('.list-messages thead tr:first-child th').each(function () {
    //    var dataId = 'col'+$(this).attr('data-id');
    //    $('#'+dataId).prop('checked', true);
    //});

    //$('body').on('change','.column-check input:checkbox', function(){
    //    var dataId = $(this).attr('id').replace('col','');
    //    var value = $(this).val();
    //    var lastDataId = 0;
    //    if($(this).prop('checked')){
    //        $('.list-messages thead tr:first-child th').each(function () {
    //            if($(this).attr('data-id')>dataId){
    //                $('.list-messages thead tr:first-child th[data-id='+lastDataId+']').after('<th data-id='+dataId+'><a class="column-name" href="#">'+value+'</a></th>');
    //                $('.list-messages thead tr:last-child th[data-id='+lastDataId+']').after('<th data-id='+dataId+'><input type="text" name="name'+dataId+'"></th>');
    //                $('.list-messages tbody tr').each(function(){
    //                    $(this).find('td[data-id='+lastDataId+']').after('<td data-id='+dataId+'></td>');
    //                });
    //                return false;
    //            } else {
    //                lastDataId = $(this).attr('data-id');
    //            }
    //        })
    //    } else {
    //        $('.list-messages thead tr').each(function () {
    //            $(this).find('th[data-id='+dataId+']').remove();
    //        })
    //        $('.list-messages tbody tr').each(function () {
    //            $(this).find('td[data-id='+dataId+']').remove();
    //        })
    //    }
    //});

    //$('body').on('click','.column-name', function(){
    //    if(!$(this).hasClass('sorted')){
    //        $('.column-name i').remove();
    //        $('.column-name').removeClass('sorted');
    //        $(this).addClass('sorted').addClass('desc');
    //        $(this).append('<i class="fa fa-caret-down"></i>');
    //    } else {
    //        if($(this).hasClass('asc')){
    //            $(this).addClass('desc').removeClass('asc');
    //            $(this).find('i').remove();
    //            $(this).append('<i class="fa fa-caret-down"></i>');
    //        } else {
    //            $(this).addClass('asc').removeClass('desc');
    //            $(this).find('i').remove();
    //            $(this).append('<i class="fa fa-caret-up"></i>');
    //        }
    //    }
    //});

    $('body').on('change', '#userall', function() {
        if ($(this).prop('checked')) {
            $('tbody .user-check input[type="checkbox"]').prop("checked", true);
            $('.blue-btn.disabled').removeClass('disabled');
        } else {
            $('tbody .user-check input[type="checkbox"]').prop("checked", false);
            if($('tbody .user-check input:checkbox:checked').length==0){
                $('.blue-btn').addClass('disabled');
            }
        }
    });
    $('body').on('change', 'tbody .user-check input:checkbox', function() {
        if (!$(this).prop('checked')) {
            $('#userall').prop("checked", false);
            if($('tbody .user-check input:checkbox:checked').length==0){
                $('.blue-btn').addClass('disabled');
            }
        } else {
            $('.blue-btn.disabled').removeClass('disabled');
        }
    });
    $('body').on('click','.filter-group-name', function(){
        var menu = $(this).closest('.filter-group').find('.filter-group-box');
        if(menu.hasClass('opened')){
            menu.removeClass('opened');
        } else {
            menu.addClass('opened');
        }
    });

    $('body').on('click','.select-box', function(){
        var menu = $(this).closest('.fake-select').find('.filter-group-box');
        if(menu.hasClass('opened')){
            menu.removeClass('opened');
        } else {
            menu.addClass('opened');
        }
    });

    $('body').on('click', function(){
        var menu = $('.filter-group-box');
        if(menu.hasClass('opened')){
            menu.removeClass('opened');
        }
    });

    $('body').on('click','.filter-group', function(e) {
        e.stopPropagation();
    });

    $('body').on('click','.fake-select', function(e) {
        e.stopPropagation();
    });

    //$('body').on('click', 'a.dialog-item', function(e) {
    //    e.preventDefault();
    //    if(!$(this).hasClass('active')) {
    //        $('a.dialog-item').removeClass('active');
    //        $(this).addClass('active');
    //    }
    //});

    $('body').on('click', '.open-filter', function() {
        $(this).hide();
        $('.close-filter').show();
        $('.filter-opened').show();
    });

    $('body').on('click', '.close-filter', function() {
        $(this).hide();
        $('.open-filter').show();
        $('.filter-opened').hide();
    });

    $('body').on('click', '.filter-group-item', function() {
        $('.filter-group-box').removeClass('opened');
        var text =  $(this).find('.filter-prop').html();
        $(this).closest('.menu-select').find('.selected-item').html(text);
    });

    $('body').on('click', '.clear-filter', function() {
        $('.filter-tag input[type="search"]').val('');
        $('.fake-select').each(function () {
            var text =  $(this).find('.filter-prop:first-child').html();
            $(this).find('.selected-item').html(text);
        });
    });

    $('body').on('click', '.tag-delete', function() {
        $(this).closest('li').remove();
    });

    $('body').on('click', '.chron-head', function() {
        var group = $(this).closest('.chronology-group');
        if(group.hasClass('open')){
            group.removeClass('open');
            $(this).find('.chevron i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        } else {
            $('.chronology-group').removeClass('open');
            $('.chronology-group .chevron i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
            group.addClass('open');
            $(this).find('.chevron i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        }
    });

    $('body').on('click', '.page-link', function(e) {
        e.stopPropagation();
    });

    // $('body').on('keyup', '#text-chat', function() {
    //     var msg = $(this).val().replace(/<[^>]+>/g,'');
    //     $('#example-chat').html(msg);
    // });

    $('body').on('keyup', '#text-popup', function() {
        $('#popup-example').html($(this).val());
        if($('#popup-example').html().length != 0){
            $('.popup-text').css('min-height',0)
        } else {
            $('.popup-text').css('min-height',80)
        }
    });

    $('body').on('keyup', '#text-email', function() {
        $('#email-text').html($(this).val());
    });

    $('body').on('change', 'input[name="typePopup"]', function() {
        if($(this).val() == 'small'){
            $('.pop-up.example').removeClass('big').addClass('small');
        } else {
            $('.pop-up.example').removeClass('small').addClass('big');
        }
    });

    $('.blue-alias').tooltip();
    $('.msg-btn').tooltip();
    $('.fake-select').tooltip();
    $('.tooltip-bs').tooltip();
});