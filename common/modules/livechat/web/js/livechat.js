$('body').on('mouseenter', '.pop-up.small', function() {
    $('body').addClass('no-overflow');
    $('.chat-button').addClass('open');
    $('.chat-block').addClass('open');
    $(this).hide();
    return false;
});

$('body').on('click', '.pop-up.big .close-popup' ,function() {
    $(this).closest('.modal-popup').removeClass('open');
    return false;
});

$('body').on('click', '.live-offers-trigger', function() {
    let id = $(this).data('target');

    Live.toggleOffer(id);
});

function LiveHelper() {
    this.addZero = function(i) {
        return i < 10 ? "0" + i : i;
    };

    this.scrollBlockToBottom = function(block) {
        if(block !== undefined) {
            let height = block.scrollHeight;
            block.scrollTop(9000);

            return true;
        }

        return false;
    };

    this.expandOfferBlock = function(trigger) {
        trigger.click();
    };

    this.getCookie = function (name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };
}

function LiveScenario() {
    this.default = function(data) {
        let from = data.time,
            currentTime = parseInt(Date.now() / 1000),
            diff = from + data.showIn - currentTime;

        if(diff > 0) {
            if(diff < 15) {
                diff = 30;
            }

            setTimeout(function() {
                let $chatBody = $('.chat-body');
                Live.sendRequest('/livechat/ajax-scenario/default-greeting', {}, function(response) {
                    Live.forceOpen();
                    LiveHelp.scrollBlockToBottom($chatBody);
                });
            }, diff * 1000);
        }
    };

    this.scenarioMessage = function(data) {
        let from = data.time,
            currentTime = parseInt(Date.now() / 1000),
            diff = from + data.showIn - currentTime;

        if(diff > 0) {
            if(diff < 15) {
                diff = 30;
            }

            setTimeout(function() {
                Live.sendRequest('/livechat/ajax-scenario/' + data.method, {}, function(response) {
                    let html = Live.renderMessage('our', response.text),
                        $chatBody = $('.chat-body');

                    $chatBody.append(html);
                    Live.forceOpen();
                    LiveHelp.scrollBlockToBottom($chatBody);
                    LiveHelp.expandOfferBlock($('a[data-target=' + response.messageId + ']'))
                });
            }, diff * 1000);
        }
    };

    this.scenarioOffer = function(data) {
        let from = data.time,
            currentTime = parseInt(Date.now() / 1000),
            diff = from + data.showIn - currentTime;

        if(diff > 0) {
            if(diff < 15) {
                diff = 30;
            }

            setTimeout(function() {
                Live.sendRequest('/livechat/ajax-scenario/' + data.method, {}, function(response) {
                    let html = Live.renderOffers(response),
                        $chatBody = $('.chat-body');

                    $chatBody.append(html);
                    Live.forceOpen();
                    LiveHelp.scrollBlockToBottom($chatBody);
                    LiveHelp.expandOfferBlock($('a[data-target=' + response.messageId + ']'))
                });
            }, diff * 1000);
        }
    };

    this.recommendAdvertServices = function(data) {
        this.scenarioOffer(data);
    };

    this.recommendTenderServices = function(data) {
        this.scenarioOffer(data);
    };

    this.goToCart = function(data) {
        this.scenarioMessage(data);
    };

    this.goToCartWithLink = function(data) {
        this.scenarioMessage(data);
    };

    this.recommendTariffs = function(data) {
        this.scenarioMessage(data);
    };

    this.recommendRegistration = function(data) {
        this.scenarioMessage(data);
    };

    this.recommendTenderPost = function(data) {
        this.scenarioMessage(data);
    }
}

function LiveChat() {
    this.assetBaseUrl = null;
    this.operatorAvatar = null;

    this.setAssetPath = function(path) {
        this.assetBaseUrl = path;
        this.operatorAvatar = '<img src="' + this.assetBaseUrl + '/images/moder.jpg' + '" />';
    };

    this.forceOpen = function() {
        $('body').addClass('no-overflow');
        $('.chat-button').addClass('open');
        $('.chat-block').addClass('open');
    };

    this.forceClose = function() {
        $('body').removeClass('no-overflow');
        $('.chat-button').removeClass('open');
        $('.chat-block').removeClass('open');
    };

    this.renderPopup = function(type, message) {
        return '<div class="pop-up ' + type + '">' +
                    '<div class="popup-icon">' +
                        '<img src="' + this.assetBaseUrl + '/images/default-v2.png">' +
                    '</div>' +
                    (type === 'big' ? '<a href="#" class="close-popup"></a>' : '') +
                    '<div class="popup-body">' +
                        '<div class="popup-text">' +
                             message +
                        '</div>' +
                     '</div>' +
                '</div>'
    };

    this.renderMessage = function(whose, message) {
        let dt = new Date();
        if (whose === 'our') {
            message = message.replace(/(http(s)?:\/\/[\w#%?&\/.\-_=\[\]+]*\.(?:jpg|jpeg|png|gif))(?!(?:(?:[^'<>]*')|(?:[^"<>]*"))[^<>]*>)/i, "<a href='$1' target='_blank' rel='nofollow'><img src='$1'></a>");
            message = message.replace(/(http(s)?:\/\/[\w#%?&\/.\-_=\[\]+]*)(?!(?:(?:[^'<>]*')|(?:[^"<>]*"))[^<>]*>)/i, "<a href='$1' target='_blank' rel='nofollow'>$1</a>");
        }
        return '<div class="message ' + whose + '" data-timestamp="' + parseInt(dt.getTime() / 1000) + '">' +
                    (whose === 'our' ? '<div class="avatar">' + this.operatorAvatar + '</div>' : '') +
                        '<div class="message-block">' +
                            '<div class="message-body">' +
                                '<p>' + message + '</p>' +
                            '</div>' +
                        '<div class="message-props">' +
                            '<div class="' + (whose === 'user' ? 'right' : 'left') + '">' +
                                '<span>' + LiveHelp.addZero(dt.getHours()) + ':' + LiveHelp.addZero(dt.getMinutes()) + '</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
    };

    this.renderOffers = function(data) {
        let html = this.renderMessage('our', '<p>' + data.text + '</p><p><a href="#" class="live-offers-trigger" data-target="' + data.messageId +'">Просмотреть</a>') ;
        if(data.offers.length > 0) {
            html += '<div class="live-offer-wrap" data-id="' + data.messageId + '">';

            $.each(data.offers, function(index, value) {
                html += '<div class="live-offer-item">' +
                            '<div class="live-offer-image">' +
                                '<img src = "' + value.img + '" />' +
                            '</div>' +
                            '<div class="live-offer-info">' +
                                '<div class="live-offer-title">' +
                                    value.label +
                                '</div>' +
                                '<div class="live-offer-author">' +
                                     value.author +
                                '</div>' +
                                '<div class="text-right">' +
                                    '<a target="_blank" class="live-offer-order-button" href="' + value.url + '">' + value.text + '</a>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            });
            html += '</div>'
        }

        return html;
    };

    this.renderDiscountOffers = function(data) {
        let html = this.renderMessage('our', '<p>' + data.text + '</p><p><a href="#" class="live-offers-trigger" data-target="' + data.messageId +'">Просмотреть</a>') ;
        if(data.offers.length > 0) {
            html += '<div class="live-offer-wrap" data-id="' + data.messageId + '">';
            html += '<div class="live-offer-main-img">' +
                        '<img src="/images/new/discount.jpg"/>' +
                    '</div>';
            $.each(data.offers, function(index, value) {
                html += '<div class="live-offer-item">' +
                            '<div class="live-offer-image">' +
                                '<img src = "' + value.img + '" />' +
                            '</div>' +
                            '<div class="live-offer-info">' +
                                '<div class="live-offer-title">' +
                                    value.label +
                                '</div>' +
                                '<div class="live-offer-author">' +
                                     value.author +
                                '</div>' +
                                '<div class="live-offer-price clearfix">' +
                                    value.oldPrice +
                                '</div>' +
                                '<div class="text-right">' +
                                    '<a target="_blank" class="live-offer-order-button" href="' + value.url + '">' + value.text + '</a>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            });
            html += '</div>'
        }

        return html;
    };

    this.processScenario = function(data)
    {
        let parsedData = JSON.parse(data);
        if(parsedData.action !== undefined) {
            switch(parsedData.action) {
                case 'default':
                    LiveScenarioWorker.default(parsedData);
                    break;
                case 'recommendTenderServices':
                    LiveScenarioWorker.recommendTenderServices(parsedData);
                    break;
                case 'goToCart':
                    LiveScenarioWorker.goToCart(parsedData);
                    break;
                case 'recommendAdvertServices':
                    LiveScenarioWorker.recommendAdvertServices(parsedData);
                    break;
                case 'recommendTariffs':
                    LiveScenarioWorker.recommendTariffs(parsedData);
                    break;
                case 'goToCartWithLink':
                    LiveScenarioWorker.goToCartWithLink(parsedData);
                    break;
                case 'recommendRegistration':
                    LiveScenarioWorker.recommendRegistration(parsedData);
                    break;
                case 'recommendTenderPost':
                    LiveScenarioWorker.recommendTenderPost(parsedData);
                    break;
            }
        }
    };

    this.addResponse = function(data) {
        this.sendRequest("/livechat/ajax-event/add-response", data)
    };

    this.addConversationMessage = function(data) {
        this.sendRequest("/livechat/ajax-event/add-conversation-message", data)
    };

    this.addPayment = function(data) {
        this.sendRequest("/livechat/ajax-event/add-payment", data);
    };

    this.doNotShow = function() {
        this.sendRequest("/livechat/ajax/do-not-show", {});
    };

    this.toggleOffer = function(id) {
        let selector = '.live-offer-wrap[data-id=' + id + ']';
        $(selector).fadeToggle();
    };

    this.addToCart = function(data) {
        this.sendRequest("/livechat/ajax-event/add-to-cart", data);
    };

    this.removeFromCart = function(data) {
        this.sendRequest("/livechat/ajax-event/remove-from-cart", data);
    };

    this.postJob = function(data) {
        this.sendRequest("/livechat/ajax-event/post-job", data);
    };

    this.postTender = function(data) {
        this.sendRequest("/livechat/ajax-event/post-tender", data);
    };

    this.sendRequest = function(address, data, callback) {
        $.ajax({
            url: address,
            type: 'post',
            data: data,
            success: function(result) {
                if(callback) {
                    callback(result);
                } else {

                }
            }
        });
    };
}

const Live = new LiveChat();
const LiveHelp = new LiveHelper();
const LiveScenarioWorker = new LiveScenario();