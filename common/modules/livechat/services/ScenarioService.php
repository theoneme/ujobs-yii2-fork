<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.07.2017
 * Time: 16:41
 */

namespace common\modules\livechat\services;

use common\modules\livechat\models\LiveClient;
use yii\web\Cookie;
use yii\web\CookieCollection;

/**
 * Class ScenarioService
 * @package common\modules\livechat\services
 */
class ScenarioService
{
    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param $action
     * @param LiveClient $client
     * @return mixed
     */
    public static function getScenario(CookieCollection $responseCookies, CookieCollection $requestCookies, $action, $client)
    {
        return self::$action($responseCookies, $requestCookies, $client);
    }

    /**
     * @param array $array
     * @param LiveClient $client
     * @return null|string
     */
    public static function getAction($array, $client)
    {
        foreach ($array as $key => $item) {
            $result = $client->{$key}();
            if (array_key_exists($key, $item)) {
                if (\is_array($item[$result])) {
                    $res = self::getAction($item[$result], $client);
                    if (\is_string($res)) {
                        return $res;
                    }
                } else if (\is_string($item[$result])) {
                    return $item[$result];
                }
            }
        }

        return null;
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @return array|mixed
     */
    public static function defaultScenario(CookieCollection $responseCookies, CookieCollection $requestCookies)
    {
        $data = json_decode($requestCookies->getValue('live_client_action', '{}'), true);
        if (!$data || $data['time'] < (time() - (60 * 3))) {
            $data = [
                'action' => 'default',
                'time' => time(),
                'showIn' => 60 * 3,
                'method' => 'default-greeting',
            ];
            $responseCookies->add(new Cookie([
                'name' => 'live_client_action',
                'value' => json_encode($data),
            ]));
        }

        return $data;
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param LiveClient $client
     * @return array
     */
    public static function recommendAdvertServices(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        $data = json_decode($requestCookies->getValue('live_client_action', '{}'), true);
        if (!$data || $data['time'] < (time() - 60)) {
            $data = [
                'action' => 'recommendAdvertServices',
                'time' => time(),
                'showIn' => 60 * 3,
                'method' => 'recommend-advert-services'
            ];
            $responseCookies->add(new Cookie([
                'name' => 'live_client_action',
                'value' => json_encode($data),
            ]));
        }

        return $data;
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param LiveClient $client
     * @return array
     */
    public static function recommendTenderPost(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        return [
            'action' => 'recommendTenderPost',
            'time' => time(),
            'showIn' => 30,
            'method' => 'recommend-tender-post'
        ];
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param $client
     * @return array
     */
    public static function goToCartIn3m(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        $data = json_decode($requestCookies->getValue('live_client_action', '{}'), true);
        if (!$data || $data['time'] < (time() - 60)) {
            $data = [
                'action' => 'goToCart',
                'time' => time(),
                'showIn' => 60 * 3,
                'method' => 'go-to-cart'
            ];
            $responseCookies->add(new Cookie([
                'name' => 'live_client_action',
                'value' => json_encode($data),
            ]));
        }

        return $data;
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param $client
     * @return array
     */
    public static function goToCartIn1m(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        $data = json_decode($requestCookies->getValue('live_client_action', '{}'), true);
        if (!$data || $data['time'] < (time() - 60)) {
            $data = [
                'action' => 'goToCartWithLink',
                'time' => time(),
                'showIn' => 60,
                'method' => 'go-to-cart-with-link'
            ];
            $responseCookies->add(new Cookie([
                'name' => 'live_client_action',
                'value' => json_encode($data),
            ]));
        }

        return $data;
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param $client
     * @return array
     */
    public static function recommendTenderServices(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        return [
            'action' => 'recommendTenderServices',
            'time' => time(),
            'showIn' => 30,
            'method' => 'recommend-tender-services'
        ];
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param $client
     * @return array
     */
    public static function recommendTariffs(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        return [
            'action' => 'recommendTariffs',
            'time' => time(),
            'showIn' => 30,
            'method' => 'recommend-tariffs'
        ];
    }

    /**
     * @param CookieCollection $responseCookies
     * @param CookieCollection $requestCookies
     * @param $client
     * @return array
     */
    public static function recommendRegistration(CookieCollection $responseCookies, CookieCollection $requestCookies, $client)
    {
        return [
            'action' => 'recommendRegistration',
            'time' => time(),
            'showIn' => 60,
            'method' => 'recommend-registration'
        ];
    }
}
