<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 18.05.2017
 * Time: 12:14
 */

namespace common\modules\livechat\services;

use common\modules\livechat\models\LiveClient;
use yii\swiftmailer\Mailer;

/**
 * Class MailerService
 * @package common\modules\livechat\services
 */
class MailerService
{
	/**
	 * @param Mailer $mailer
	 * @param $from
	 * @param $subject
	 * @param $view
	 * @param array $params
	 * @param LiveClient $to
	 * @return bool
	 */
	public static function sendMail(Mailer $mailer, $from, $subject, $view, array $params, LiveClient $to)
	{
		if ($mailer !== null) {
			$mailer->viewPath = '@common/modules/livechat/views/email';
			return $mailer->compose([
				'html' => $view
			], $params)
				->setTo($to->email)
				->setFrom($from)
				->setSubject($subject)
				->send();
		}

		return false;
	}
}