<?php

use yii\db\Migration;

class m170615_120702_live_dialog_adj extends Migration
{
    public function up()
    {
    	$this->dropForeignKey('fk_live_dialog_member_user_id', 'live_dialog_member');

		$this->dropColumn('live_dialog_member', 'session_id');
		$this->dropColumn('live_dialog_member', 'user_id');
		$this->dropColumn('live_dialog_member', 'name');
	    $this->dropColumn('live_dialog_member', 'email');

	    $this->addColumn('live_dialog_member', 'live_client_id', $this->integer());
	    $this->addForeignKey('fk_live_dialog_member_live_client_id', 'live_dialog_member', 'live_client_id', 'live_client', 'id', 'CASCADE');
    }

    public function down()
    {
	    $this->addColumn('live_dialog_member', 'session_id', $this->char(40));
	    $this->addColumn('live_dialog_member', 'user_id', $this->integer());
	    $this->addColumn('live_dialog_member', 'name', $this->string(55)->null());
	    $this->addColumn('live_dialog_member', 'email', $this->string(55)->null());

	    $this->addForeignKey('fk_live_dialog_member_user_id', 'live_dialog_member', 'user_id', 'user', 'id', 'CASCADE');

	    $this->dropForeignKey('fk_live_dialog_member_live_client_id', 'live_dialog_member');
	    $this->dropColumn('live_dialog_member', 'live_client_id');
    }
}
