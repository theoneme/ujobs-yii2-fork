<?php

use yii\db\Migration;

class m170619_084708_live_adj extends Migration
{
    public function up()
    {
		$this->addColumn('live_client_history', 'referrer', $this->string(125)->null());
		$this->addColumn('live_client_history', 'ip', $this->string(15)->null());
    }

    public function down()
    {
        $this->dropColumn('live_client_history', 'referrer');
	    $this->dropColumn('live_client_history', 'ip');
    }
}
