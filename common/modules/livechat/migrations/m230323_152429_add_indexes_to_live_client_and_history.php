<?php

use yii\db\Migration;

/**
 * Class m230323_152429_add_indexes_to_live_client_history
 */
class m230323_152429_add_indexes_to_live_client_and_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('live_client_advert_referrer_index', 'live_client', 'advert_referrer');
        $this->createIndex('live_client_history_event_code_index', 'live_client_history', 'event_code');
        $this->createIndex('live_client_history_lcid_ec_index', 'live_client_history', ['live_client_id', 'event_code']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('live_client_advert_referrer_index', 'live_client');
        $this->dropIndex('live_client_history_event_code_index', 'live_client_history');
        $this->dropIndex('live_client_history_lcid_ec_index', 'live_client_history');
    }
}
