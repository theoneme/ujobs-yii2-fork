<?php

use yii\db\Migration;

class m170614_112735_livechat_members extends Migration
{
    public function up()
    {
	    $this->createTable('live_client', [
		    'id' => $this->primaryKey(),
		    'created_at' => $this->integer(),
		    'updated_at' => $this->integer(),
		    'last_action_at' => $this->integer(),
		    'email' => $this->string(55)->null(),
		    'name' => $this->string(55)->null(),
		    'ip' => $this->string(15)->null(),
		    'session_id' => $this->char(40)->null(),
		    'user_id' => $this->integer()->null(),
		    'country' => $this->string(35)->null(),
		    'region' => $this->string(35)->null(),
		    'city' => $this->string(35)->null(),
		    'sessions_count' => $this->integer(),
		    'advert_referrer' => $this->string(75)->null(),
		    'advert_keyword' => $this->string(75)->null(),
		    'advert_source' => $this->string(35)->null(),
		    'avatar' => $this->string(155)->null(),
	    ]);

	    $this->createIndex('session_id_index', 'live_client', 'session_id');
	    $this->createIndex('user_id_index', 'live_client', 'user_id');
	    $this->createIndex('ip_index', 'live_client', 'ip');

	    $this->createTable('live_client_history', [
		    'id' => $this->primaryKey(),
		    'created_at' => $this->integer(),
		    'updated_at' => $this->integer(),
		    'live_client_id' => $this->integer(),
		    'event_code' => $this->integer(),
		    'url' => $this->string(155)->null()
	    ]);

	    $this->addForeignKey('fk_live_client_id', 'live_client_history', 'live_client_id', 'live_client', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('live_client_history');
        $this->dropTable('live_client');
    }
}
