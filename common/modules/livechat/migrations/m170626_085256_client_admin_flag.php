<?php

use yii\db\Migration;

class m170626_085256_client_admin_flag extends Migration
{
    public function up()
    {
		$this->addColumn('live_client', 'is_operator', $this->boolean());
		$this->createIndex('live_client_is_operator_index', 'live_client', 'is_operator');
    }

    public function down()
    {
	    $this->dropColumn('live_client', 'is_operator');
    }
}
