<?php

use yii\db\Migration;

class m170523_074137_jivo extends Migration
{
    public function up()
    {
	    $this->createTable('live_dialog', [
		    'id' => $this->primaryKey(),
		    'created_at' => $this->integer(),
		    'updated_at' => $this->integer(),
	    ]);

	    $this->createTable('live_dialog_member', [
		    'id' => $this->primaryKey(),
		    'live_dialog_id' => $this->integer(),
		    'user_id' => $this->integer()->null(),
		    'session_id' => $this->char(40)->null(),
		    'name' => $this->string(55)->null(),
		    'email' => $this->string(55)->null()
	    ]);

	    $this->createTable('live_dialog_message', [
		    'id' => $this->primaryKey(),
		    'message' => $this->text(),
		    'created_at' => $this->integer(),
		    'updated_at' => $this->integer(),
		    'member_id' => $this->integer(),
		    'live_dialog_id' => $this->integer(),
		    'is_read' => $this->boolean()->null()
	    ]);

	    $this->addForeignKey('fk_live_dialog_member_dialog_id', 'live_dialog_member', 'live_dialog_id', 'live_dialog', 'id', 'CASCADE');
	    $this->addForeignKey('fk_live_dialog_member_user_id', 'live_dialog_member', 'user_id', 'user', 'id', 'CASCADE');
	    $this->addForeignKey('fk_live_dialog_message_member_id', 'live_dialog_message', 'member_id', 'live_dialog_member', 'id', 'CASCADE');
	    $this->addForeignKey('fk_live_dialog_message_dialog_id', 'live_dialog_message', 'live_dialog_id', 'live_dialog', 'id', 'CASCADE');
    }

    public function down()
    {
	    $this->dropTable('live_dialog_message');
	    $this->dropTable('live_dialog_member');
	    $this->dropTable('live_dialog');
    }
}
