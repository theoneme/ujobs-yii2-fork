<?php

use yii\db\Migration;

class m170615_105228_livechat_history_adj extends Migration
{
    public function up()
    {
		$this->addColumn('live_client_history', 'browser', $this->string(55)->null());
		$this->addColumn('live_client_history', 'os', $this->string(55)->null());
		$this->addColumn('live_client_history', 'device', $this->string(55)->null());
    }

    public function down()
    {
	    $this->dropColumn('live_client_history', 'browser');
	    $this->dropColumn('live_client_history', 'os');
	    $this->dropColumn('live_client_history', 'device');
    }
}
