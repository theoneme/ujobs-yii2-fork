<?php

use yii\db\Migration;

class m170713_080338_client_scenario_data extends Migration
{
    public function safeUp()
    {
        $this->addColumn('live_client', 'custom_data', $this->binary());
    }

    public function safeDown()
    {
        $this->dropColumn('live_client', 'custom_data');
    }
}
