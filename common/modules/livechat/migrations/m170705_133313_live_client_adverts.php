<?php

use yii\db\Migration;

class m170705_133313_live_client_adverts extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('live_client', 'advert_referrer', $this->string(255));
        $this->alterColumn('live_client_history', 'url', $this->string(455));
        $this->addColumn('live_client', 'advert_compaign', $this->string(125));

        $this->addColumn('live_client_history', 'advert_compaign', $this->string(125));
        $this->addColumn('live_client_history', 'advert_keyword', $this->string(75));
        $this->addColumn('live_client_history', 'advert_source', $this->string(35));
    }

    public function safeDown()
    {
        $this->alterColumn('live_client', 'advert_referrer', $this->string(75));
        $this->alterColumn('live_client', 'advert_referrer', $this->string(155));
        $this->dropColumn('live_client', 'advert_compaign');

        $this->dropColumn('live_client_history', 'advert_compaign');
        $this->dropColumn('live_client_history', 'advert_keyword');
        $this->dropColumn('live_client_history', 'advert_source');
    }
}
