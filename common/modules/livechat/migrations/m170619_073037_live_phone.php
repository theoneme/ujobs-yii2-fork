<?php

use yii\db\Migration;

class m170619_073037_live_phone extends Migration
{
    public function up()
    {
		$this->addColumn('live_client', 'phone', $this->string(20));
    }

    public function down()
    {
        $this->dropColumn('live_client', 'phone');
    }
}
