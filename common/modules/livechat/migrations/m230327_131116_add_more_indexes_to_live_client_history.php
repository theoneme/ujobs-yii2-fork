<?php

use yii\db\Migration;

/**
 * Class m230327_131116_add_more_indexes_to_live_client_history
 */
class m230327_131116_add_more_indexes_to_live_client_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('live_client_history_advert_compaign_index', 'live_client_history', 'advert_compaign');
        $this->createIndex('live_client_history_advert_source_index', 'live_client_history', 'advert_source');
        $this->createIndex('live_client_history_advert_keyword_index', 'live_client_history', 'advert_keyword');
        $this->createIndex('live_client_history_referrer_index', 'live_client_history', 'referrer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('live_client_history_advert_compaign_index', 'live_client_history');
        $this->dropIndex('live_client_history_advert_source_index', 'live_client_history');
        $this->dropIndex('live_client_history_advert_keyword_index', 'live_client_history');
        $this->dropIndex('live_client_history_referrer_index', 'live_client_history');
    }
}
