<?php

use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveDialog;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use yii\db\Migration;

class m171002_121619_dialog_status extends Migration
{
    public function safeUp()
    {
        return true;

        $this->addColumn('live_dialog','status', $this->integer()->defaultValue(LiveDialog::STATUS_OPEN));
        $this->createIndex('live_dialog_status_index', 'live_dialog', 'status');

        $operators = LiveClient::find()->select('id')->where(['is_operator' => true])->scalar();

        $emptyDialogs = LiveDialog::find()
            ->joinWith(['members'])
            ->groupBy('live_dialog.id')
            ->having('count(live_dialog_member.id) < 1')
            ->all();
        if($emptyDialogs !== null) {
            foreach($emptyDialogs as $dialog) {
                $dialog->delete();
            }
        }

        $dialogsWithoutAnswers = LiveDialog::find()->where(['not exists', LiveDialogMember::find()
            ->where('live_dialog.id = live_dialog_member.live_dialog_id')
            ->andWhere(['live_client_id' => $operators])
        ])->andWhere(['exists', LiveDialogMessage::find()
            ->where('live_dialog.id = live_dialog_message.live_dialog_id')
        ])->all();

        /* @var LiveDialog[] $dialogsWithoutAnswers */
        if($dialogsWithoutAnswers !== null) {
            foreach($dialogsWithoutAnswers as $dialog) {
                $dialog->updateAttributes(['status' => LiveDialog::STATUS_OPEN]);
            }
        }
    }

    public function safeDown()
    {
        return true;

        $this->dropColumn('live_dialog', 'status');
    }
}
