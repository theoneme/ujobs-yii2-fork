<?php

use yii\db\Migration;

class m170620_085404_livechat_history_custom_data extends Migration
{
    public function up()
    {
		$this->addColumn('live_client_history', 'custom_data', $this->binary());
    }

    public function down()
    {
        $this->dropColumn('live_client_history', 'custom_data');
    }
}
