<?php

use yii\db\Migration;

class m170607_141259_create_live_contact_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('live_contact_email', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'message' => $this->text(),
            'created_at' => $this->integer()
        ]);

        $this->createIndex('live_contact_email_email', 'live_contact_email', 'email');
        $this->createIndex('live_contact_email_created_at', 'live_contact_email', 'created_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('live_contact_email');
    }
}
