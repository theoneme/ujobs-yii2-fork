<?php

use yii\db\Migration;

class m170621_104536_live_dialog_message_adj extends Migration
{
    public function up()
    {
		$this->addColumn('live_dialog_message', 'type', $this->integer()->defaultValue(0));
		$this->addColumn('live_dialog_message', 'custom_data', $this->binary()->null());

	    $this->createIndex('live_dialog_message_type_index', 'live_dialog_message', 'type');
    }

    public function down()
    {
        $this->dropColumn('live_dialog_message', 'type');
        $this->dropColumn('live_dialog_message', 'custom_data');
    }
}
