<?php

namespace common\modules\livechat\assets;

use yii\web\AssetBundle;

/**
 * Class LivechatAsset
 * @package common\modules\livechat\assets
 */
class LivechatAdminAsset extends AssetBundle
{
	public $sourcePath = '@common/modules/livechat/web';
	public $css = [
		'css/font-awesome.min.css',
		'css/index.css',
		'css/custom.admin.css',
	];
	public $js = [
		'js/socket.io.js',
		'js/livechat.admin.js',
		'js/custom.admin.js',
		'js/jquery.noty.packaged.min.js'
	];
	public $depends = [
		'yii\web\JqueryAsset',
		'yii\bootstrap\BootstrapPluginAsset'
	];
}
