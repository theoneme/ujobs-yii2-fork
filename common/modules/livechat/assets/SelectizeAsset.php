<?php

namespace common\modules\livechat\assets;

use yii\web\AssetBundle;

/**
 * Class CropperAsset
 * @package frontend\assets
 */
class SelectizeAsset extends AssetBundle
{
    public $sourcePath = '@bower/selectize/dist';
    public $css = [
        'css/selectize.legacy.css',
    ];
    public $js = [
        'js/standalone/selectize.min.js'
    ];
    public $depends = [
        'common\modules\livechat\assets\LivechatAdminAsset',
        'yii\web\JqueryAsset',
    ];
}