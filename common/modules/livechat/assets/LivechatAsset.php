<?php

namespace common\modules\livechat\assets;

use yii\web\AssetBundle;

/**
 * Class LivechatAsset
 * @package common\modules\livechat\assets
 */
class LivechatAsset extends AssetBundle
{
	public $sourcePath = '@common/modules/livechat/web';
	public $css = [
		'css/siteChat.css'
	];
	public $js = [
		'js/livechat.js',
		'js/socket.io.js'
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}
