<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 23.05.2017
 * Time: 14:56
 */

namespace common\modules\livechat\components;

use common\modules\livechat\models\LiveContactEmail;
use yii\base\Widget;
use Yii;

/**
 * Class LivechatWidget
 * @package common\modules\livechat\components
 */
class LivechatWidget extends Widget
{
    /**
     * @var string
     */
	public $template = 'livechat';

    /**
     * @return string
     */
	public function run()
	{
        if(array_key_exists('enableNodeApplications', Yii::$app->params) && Yii::$app->params['enableNodeApplications'] === false) {
            return null;
        }

	    $autoShow = true;
        $cookies = Yii::$app->request->cookies;
	    $doNotShowCookie = $cookies->get('livechat_do_not_show');
	    $module = Yii::$app->controller->module->id;

	    if(($doNotShowCookie !== null && $doNotShowCookie->value > time() - 60 * 60 * 24) || !isGuest() || $module === 'board') {
	        $autoShow = false;
        }

		return $this->render('../../views/partial/' . $this->template, [
			'contactForm' => new LiveContactEmail(),
            'autoShow' => $autoShow,
            'isGuest' => isGuest()
		]);
	}
}