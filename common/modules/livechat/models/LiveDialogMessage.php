<?php

namespace common\modules\livechat\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "live_dialog_message".
 *
 * @property integer $id
 * @property string $message
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $member_id
 * @property integer $live_dialog_id
 * @property integer $is_read
 * @property integer $type
 * @property mixed $custom_data
 *
 * @property LiveDialog $liveDialog
 * @property LiveDialogMember $member
 */
class LiveDialogMessage extends ActiveRecord
{
	/**
	 * @var array
	 */
	public $customDataArray = [];

	const TYPE_SIMPLE_MESSAGE = 0;
	const TYPE_SERVICE_OFFER = 5;
	const TYPE_DISCOUNT_SERVICE_OFFER = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'live_dialog_message';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['created_at', 'updated_at', 'member_id', 'live_dialog_id', 'is_read', 'type'], 'integer'],
            [['live_dialog_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiveDialog::class, 'targetAttribute' => ['live_dialog_id' => 'id']],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiveDialogMember::class, 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('livechat', 'ID'),
            'message' => Yii::t('livechat', 'Message'),
            'created_at' => Yii::t('livechat', 'Created At'),
            'updated_at' => Yii::t('livechat', 'Updated At'),
            'member_id' => Yii::t('livechat', 'Member ID'),
            'live_dialog_id' => Yii::t('livechat', 'Live Dialog ID'),
            'is_read' => Yii::t('livechat', 'Is Read'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiveDialog()
    {
        return $this->hasOne(LiveDialog::class, ['id' => 'live_dialog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(LiveDialogMember::class, ['id' => 'member_id']);
    }

	/**
	 *
	 */
    public function afterFind()
    {
	    parent::afterFind();

	    if($this->custom_data !== null) {
			$this->customDataArray = json_decode($this->custom_data, true);
	    }
    }
}
