<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 26.06.2017
 * Time: 13:54
 */

namespace common\modules\livechat\models;

use yii\base\Model;
use Yii;

/**
 * Class InvoiceForm
 * @package frontend\models
 */
class MessageForm extends Model
{
	/**
	 * @var string
	 */
	public $message = '';

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['message'], 'string'],
			['message', 'required']
		];
	}

	/**
	 * @return bool
	 */
	public function save()
	{
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'message' => Yii::t('livechat', 'Message'),
		];
	}
}