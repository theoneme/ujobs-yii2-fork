<?php

namespace common\modules\livechat\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "live_dialog".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 *
 * @property LiveDialogMember[] $members
 * @property LiveDialogMessage[] $messages
 * @property LiveDialogMessage $firstMessage
 * @property LiveDialogMessage $lastMessage
 * @property LiveDialogMessage $lastAnswer
 */
class LiveDialog extends ActiveRecord
{
	private $_firstMessage = null;
	private $_lastMessage = null;
	private $_lastAnswer = null;

	const STATUS_OPEN = 0;
	const STATUS_ANSWERED = 10;
	const STATUS_CLOSED = -10;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'live_dialog';
	}

    /**
     * @return array
     */
	public function behaviors()
	{
		return [
			TimestampBehavior::class
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['created_at', 'updated_at'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('livechat', 'ID'),
			'created_at' => Yii::t('livechat', 'Created At'),
			'updated_at' => Yii::t('livechat', 'Updated At'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMembers()
	{
		return $this->hasMany(LiveDialogMember::class, ['live_dialog_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMessages()
	{
		return $this->hasMany(LiveDialogMessage::class, ['live_dialog_id' => 'id']);
	}

	/**
	 * @return LiveDialogMessage
	 */
	public function getFirstMessage()
	{
		if ($this->_firstMessage === null) {
			$messages = $this->messages;
			uasort($messages, function ($a, $b) {
				return $a->created_at - $b->created_at;
			});
			$this->_firstMessage = array_shift($messages);
		}
		return $this->_firstMessage;
	}

	/**
	 * @return LiveDialogMessage
	 */
	public function getLastMessage()
	{
		if ($this->_lastMessage === null) {
			$messages = $this->messages;
			uasort($messages, function ($a, $b) {
				return $a->created_at - $b->created_at;
			});
			$this->_lastMessage = array_pop($messages);
		}
		return $this->_lastMessage;
	}

    /**
     * @param $operatorIds array
     * @return LiveDialogMessage|null
     */
	public function getLastAnswer($operatorIds)
	{
		if ($this->_lastAnswer === null) {
			$messages = array_filter($this->messages, function($var) use ($operatorIds){
                /* @var $var LiveDialogMessage*/
                return in_array($var->member_id, $operatorIds);
            });
			uasort($messages, function ($a, $b) {
				return $a->created_at - $b->created_at;
			});
			$this->_lastAnswer = array_pop($messages);
		}
		return $this->_lastAnswer;
	}

	/**
	 * @param $dialog_id
	 */
	public function mergeToDialog($dialog_id)
	{
		foreach ($this->members as $member) {
			$otherMemberId = LiveDialogMember::find()->where(['live_dialog_id' => $dialog_id, 'live_client_id' => $member->live_client_id])->select('id')->scalar();
			if ($otherMemberId === false) {
				$otherMemberId = $member->id;
				$member->updateAttributes(['live_dialog_id' => $dialog_id]);
			}
			LiveDialogMessage::updateAll(['live_dialog_id' => $dialog_id, 'member_id' => $otherMemberId], [
				'member_id' => $member->id
			]);
		}
		$this->delete();
	}

	/**
	 * @param null $dialog_id
	 * @return LiveDialog|static
	 */
	public static function findOrCreate($dialog_id = null)
	{
		$dialog = LiveDialog::findOne($dialog_id);
		if ($dialog === null) {
			$dialog = new LiveDialog();
			$dialog->save();
		}
		return $dialog;
	}
}