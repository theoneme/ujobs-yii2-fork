<?php

namespace common\modules\livechat\models;

use common\behaviors\LinkableBehavior;
use common\models\user\Profile;
use common\modules\livechat\helpers\Utility;
use common\modules\store\models\Cart;
use MaxMind\Db\Reader;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UserEvent;

/**
 * This is the model class for table "live_client".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $last_action_at
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property string $ip
 * @property string $session_id
 * @property integer $user_id
 * @property string $country
 * @property string $region
 * @property string $city
 * @property integer $sessions_count
 * @property string $advert_referrer
 * @property string $advert_source
 * @property string $advert_keyword
 * @property string $avatar
 * @property boolean $is_operator
 * @property mixed $custom_data
 *
 * @property LiveDialog $dialog
 * @property LiveDialogMember $dialogMember
 * @property LiveClientHistory $lastHistory
 * @property LiveClientHistory[] $history
 */
class LiveClient extends ActiveRecord
{
    const RECOMMEND_SERVICES_BY_KEYWORD = 'recommendAdvertServices';
    const DEFAULT_GREETINGS = 'defaultScenario';
    const WHY_YOU_HAVE_NOT_BOUGHT_3_MIN = 'goToCartIn3m';
    const WHY_YOU_HAVE_NOT_BOUGHT_1_MIN = 'goToCartIn1m';
    const RECOMMEND_SERVICES_ON_TENDER_POST = 'recommendTenderServices';
    const RECOMMEND_TARIFFS_ON_JOB_POST = 'recommendTariffs';
    const RECOMMEND_REGISTRATION = 'recommendRegistration';
    const RECOMMEND_POST_TENDER = 'recommendTenderPost';

    public $customDataArray = [];

    public $eventTree = [
        'isNewClient' => [
            true => [
                'hasTender' => [
                    true => self::RECOMMEND_SERVICES_ON_TENDER_POST,
                ],
                'isFromAdvert' => [
                    true => [
                        'hasCart' => [
                            true => self::WHY_YOU_HAVE_NOT_BOUGHT_3_MIN,
                            false => [
                                'hasRecommendedAdvertServices' => [
                                    false => self::RECOMMEND_SERVICES_BY_KEYWORD,
                                ]
                            ]
                        ]
                    ],
                    false => [
                        'hasCart' => [
                            false => [
                                'hasDefaultGreeting' => [
                                    false => self::DEFAULT_GREETINGS
                                ]
                            ],
                            true => [
                                'hasPayment' => [
                                    false => self::WHY_YOU_HAVE_NOT_BOUGHT_3_MIN
                                ]
                            ]
                        ]
                    ]
                ],
                'hasJob' => [
                    true => self::RECOMMEND_TARIFFS_ON_JOB_POST
                ],
                'hasConversationMessagesToday' => [
                    true => self::RECOMMEND_POST_TENDER
                ]
            ],
            false => [
                'hasCart' => [
                    true => self::WHY_YOU_HAVE_NOT_BOUGHT_1_MIN
                ],
                'hasTender' => [
                    true => self::RECOMMEND_SERVICES_ON_TENDER_POST
                ],
                'hasJob' => [
                    true => self::RECOMMEND_TARIFFS_ON_JOB_POST
                ],
                'isRegistered' => [
                    false => [
                        'hasRegisterNotification' => [
                            false => self::RECOMMEND_REGISTRATION
                        ]
                    ]
                ],
                'hasConversationMessagesToday' => [
                    true => self::RECOMMEND_POST_TENDER
                ]
            ]
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'live_client';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['history'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'last_action_at', 'user_id', 'sessions_count', 'is_operator'], 'integer'],
            [['email', 'name'], 'string', 'max' => 55],
            [['ip'], 'string', 'max' => 15],
            [['phone'], 'string', 'max' => 20],
            [['session_id'], 'string', 'max' => 40],
            [['country', 'region', 'city', 'advert_source'], 'string', 'max' => 35],
            [['advert_referrer'], 'string', 'max' => 255],
            [['advert_keyword'], 'string', 'max' => 75],
            ['is_operator', 'default', 'value' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('livechat', 'ID'),
            'created_at' => Yii::t('livechat', 'Created At'),
            'updated_at' => Yii::t('livechat', 'Updated At'),
            'last_action_at' => Yii::t('livechat', 'Last Action At'),
            'email' => Yii::t('livechat', 'Email'),
            'name' => Yii::t('livechat', 'Name'),
            'ip' => Yii::t('livechat', 'Ip'),
            'session_id' => Yii::t('livechat', 'Session ID'),
            'user_id' => Yii::t('livechat', 'User ID'),
            'country' => Yii::t('livechat', 'Country'),
            'region' => Yii::t('livechat', 'Region'),
            'city' => Yii::t('livechat', 'City'),
            'sessions_count' => Yii::t('livechat', 'Sessions Count'),
            'advert_referrer' => Yii::t('livechat', 'Advert Referrer'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(LiveClientHistory::class, ['live_client_id' => 'id'])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastHistory()
    {
        return $this->hasOne(LiveClientHistory::class, ['live_client_id' => 'id'])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastOpenPageHistory()
    {
        return $this->hasOne(LiveClientHistory::class, ['live_client_id' => 'id'])->andWhere(['event_code' => LiveClientHistory::EVENT_OPEN_PAGE])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastStartSessionHistory()
    {
        return $this->hasOne(LiveClientHistory::class, ['live_client_id' => 'id'])->andWhere(['event_code' => LiveClientHistory::EVENT_START_SESSION])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastJobHistory()
    {
        return $this->hasOne(LiveClientHistory::class, ['live_client_id' => 'id'])->andWhere(['event_code' => LiveClientHistory::EVENT_POST_JOB])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastTenderHistory()
    {
        return $this->hasOne(LiveClientHistory::class, ['live_client_id' => 'id'])->andWhere(['event_code' => LiveClientHistory::EVENT_POST_TENDER])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialogMember()
    {
        return $this->hasOne(LiveDialogMember::class, ['live_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialog()
    {
        return $this->hasOne(LiveDialog::class, ['id' => 'live_dialog_id'])->via('dialogMember');
    }

    /**
     * @return null|\yii\db\ActiveQuery
     */
    public function getCart()
    {
        if ($this->user_id !== null) {
            return $this->hasOne(Cart::class, ['sessionId' => 'user_id']);
        } else if ($this->session_id !== null && $this->user_id === null) {
            return $this->hasOne(Cart::class, ['sessionId' => 'session_id']);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        $address = ($this->city ? $this->city . ', ' : '') . ($this->region ? $this->region . ', ' : '') . ($this->country ?? '');
        return $address ?? 'Не указан';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name ?? Yii::t('livechat', 'Client #{num}', ['num' => $this->id]);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email ?? Yii::t('livechat', 'Unknown {item}', ['item' => 'Email']);
    }

    /**
     * @return mixed|string
     */
    public function getPhone()
    {
        return $this->phone ?? Yii::t('livechat', 'Unknown {item}', ['item' => Yii::t('livechat', 'Phone')]);
    }

    /**
     * @return bool
     */
    public function isOnline()
    {
        return $this->last_action_at > time() - 60 * 15;
    }

    /**
     * @return bool
     */
    public function isNewClient()
    {
        //if (!array_key_exists('default_greeting', $this->customDataArray)) {
        return $this->sessions_count < 2;//$this->created_at > time() - 60 * 30;
        //}

        //return false;
    }

    /**
     * @return bool
     */
    public function hasDefaultGreeting()
    {
        return array_key_exists('default_greeting', $this->customDataArray);
    }

    /**
     * @return bool
     */
    public function isFromAdvert()
    {
        return $this->advert_source !== null && $this->advert_keyword !== null;
    }

    /**
     * @return bool
     */
    public function hasRecommendedAdvertServices()
    {
        return array_key_exists('recommend_advert_services', $this->customDataArray);
    }

    /**
     * @return bool
     */
    public function hasConversationMessagesToday()
    {
        $now = new \DateTime('now');
        $month = $now->format('m');
        $year = $now->format('Y');
        $day = $now->format('d');

        $messagesToday = LiveClientHistory::find()
            ->where(['between', 'created_at', strtotime("{$day}-{$month}-{$year} 00:00:00"), strtotime("{$day}-{$month}-{$year} 23:59:59")])
            ->andWhere(['event_code' => LiveClientHistory::EVENT_CONVERSATION_MESSAGE, 'live_client_id' => $this->id])
            ->select('id, custom_data, created_at')
            ->all();

        if (count($messagesToday) > 0) {
            $countTargets = count(
                array_unique(
                    array_map(function ($value) {
                        return $value->customDataArray['addConversationMessage']['toId'];
                    }, $messagesToday)
                )
            );

            if ($countTargets > 1 && $this->customDataArray['conversation_tender']['time'] < time() - 60 * 60 * 24 && $this->customDataArray['last_scenario'] < time() - 60 * 60 * 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasCart()
    {
        // TODO connection with ujobs
        /* @var Cart $cart */
        $cart = Cart::find()->where(['or', ['sessionId' => $this->session_id], ['sessionId' => $this->user_id]])->one();

        if ($cart !== null) {
            $cartItems = unserialize($cart->cartData);

            $dataString = implode('_', array_map(function ($value) {
                return $value['item']->id;
            }, $cartItems));

            if (is_array($this->customDataArray['cart']['items'])) {
                if (in_array($dataString, $this->customDataArray['cart']['items']) || $this->customDataArray['cart']['time'] > time() - 60 * 60 * 24) {
                    return false;
                }
            }

            if (!array_key_exists('last_scenario', $this->customDataArray) || $this->customDataArray['last_scenario'] < time() - 60 * 60 * 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isRegistered()
    {
        return $this->user_id !== null;
    }

    /**
     * @return bool
     */
    public function hasRegisterNotification()
    {
        return array_key_exists('recommend_registration', $this->customDataArray);
    }

    /**
     * @return bool
     */
    public function hasTender()
    {
        /* @var LiveClientHistory $history */
        $history = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_POST_TENDER])->andWhere(['>', 'created_at', 1511450267])->orderBy('id desc')->one();

        if ($history !== null) {
            if (is_array($this->customDataArray['recommend_tender_services']['items'])) {
                if (in_array($history->id, $this->customDataArray['recommend_tender_services']['items']) || $this->customDataArray['recommend_tender_services']['time'] > time() - 60 * 60 * 24 * 2) {
                    return false;
                }
            }

            if (!array_key_exists('last_scenario', $this->customDataArray) || $this->customDataArray['last_scenario'] < time() - 60 * 60 * 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasJob()
    {
        /* @var LiveClientHistory $history */
        $history = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_POST_JOB])->andWhere(['>', 'created_at', 1511450267])->orderBy('id desc')->one();

        if ($history !== null) {
            if (is_array($this->customDataArray['recommend_tariffs']['items'])) {
                if (in_array($history->id, $this->customDataArray['recommend_tariffs']['items']) || $this->customDataArray['recommend_tariffs']['time'] > time() - 60 * 60 * 24 * 2) {
                    return false;
                }
            }

            if (!array_key_exists('last_scenario', $this->customDataArray) || $this->customDataArray['last_scenario'] < time() - 60 * 60 * 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasPayment()
    {
        /* @var LiveClientHistory $history */
        $lastJobHistory = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_POST_JOB])->andWhere(['>', 'created_at', 1511450267])->orderBy('id desc')->one();
        $history = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_PAYMENT])->andWhere(['>', 'created_at', 1511450267])->andWhere(['>', 'created_at', $lastJobHistory->created_at])->orderBy('id desc')->one();

        if ($history !== null) {
            if (array_key_exists('payment', $this->customDataArray)) {
                if (in_array($history->id, $this->customDataArray['payment'])) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @return null|string
     */
    public function getActivityTime()
    {
        if ($this->isOnline()) {
            $lastStartSessionHistory = $this->lastStartSessionHistory;
            if ($lastStartSessionHistory !== null) {
                $activityTime = Yii::$app->formatter->format($lastStartSessionHistory->created_at, 'relativeTime');

                return $activityTime;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAdvertReferrer()
    {
        return $this->advert_referrer ?? Yii::t('livechat', 'Unknown {item}', ['item' => Yii::t('livechat', 'Advert referrer')]);
    }

    /**
     * @return string
     */
    public function getAdvertSource()
    {
        return $this->advert_source ?? Yii::t('livechat', 'Unknown {item}', ['item' => Yii::t('livechat', 'Advert source')]);
    }

    /**
     * @return int|string
     */
    public function getUserId()
    {
        return $this->user_id ?? Yii::t('livechat', 'Unknown {item}', ['item' => Yii::t('livechat', 'User ID')]);
    }

    /**
     * @param $eventCode
     * @return bool
     */
    public function hasHistory($eventCode)
    {
        return LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => $eventCode])->exists();
    }

    /**
     * @return string
     */
    public function getShortInfo()
    {
        $info = Yii::t('livechat', 'Registered on {date}', ['date' => Yii::$app->formatter->asDatetime($this->created_at)]) . PHP_EOL;

        $hasPostedJob = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_POST_JOB])->exists();
        $hasPostedTender = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_POST_TENDER])->exists();
        $hasAddedToCart = LiveClientHistory::find()->where(['live_client_id' => $this->id, 'event_code' => LiveClientHistory::EVENT_ADD_TO_CART])->exists();

        if ($hasPostedJob !== false) {
            $info .= Yii::t('livechat', 'Has posted job.') . PHP_EOL;
        }
        if ($hasPostedTender !== false) {
            $info .= Yii::t('livechat', 'Has posted request.') . PHP_EOL;
        }
        if ($hasAddedToCart !== false) {
            $info .= Yii::t('livechat', 'Has added something to cart.') . PHP_EOL;
        }

        return $info;
    }

    /**
     * @param null $siteIdentity
     * @param null $session
     * @param null $ip
     * @param null $referrer
     * @return array|LiveClient|null|ActiveRecord
     */
    public static function findOrCreate($siteIdentity = null, $session = null, $ip = null, $referrer = null)
    {
        $client = $advertKeyword = $referrerHost = $country = $region = $city = $name = $email = $avatar = null;

        if ($siteIdentity !== null) {
            $client = self::find()->where(['user_id' => $siteIdentity])->joinWith(['lastHistory'])->one();
        }
        if ($client === null && $session !== null) {
            $client = self::find()->where(['session_id' => $session, 'user_id' => null])->joinWith(['lastHistory'])->one();
        }
        if ($client === null && $ip !== null) {
            $client = self::find()->where(['live_client.ip' => $ip, 'user_id' => null])->andWhere(['>', 'live_client.created_at', time() - 3600 * 6])->joinWith(['lastHistory'])->one();
        }

        if ($client === null) {
            $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-City.mmdb');
            if ($ip != '127.0.0.1') {
                $data = $geoIpReader->get($ip);
                $country = $data['country']['names']['ru'] ?? $data['country']['names']['en'] ?? null;
                $region = $data['subdivisions'][0]['names']['ru'] ?? $data['subdivisions'][0]['names']['en'] ?? null;
                $city = $data['city']['names']['ru'] ?? $data['city']['names']['en'] ?? null;
            }

            if ($referrer !== null) {
                $referrerParts = parse_url($referrer);
                $referrerQueryParams = isset($referrerParts['query']) && is_array($referrerParts['query']) ? $referrerParts['query'] : [];
                $referrerHost = $referrerParts['host'] ?? null;
                if ($referrerHost !== Yii::$app->request->serverName) {
                    if (array_key_exists('text', $referrerQueryParams)) {
                        $advertKeyword = $referrerQueryParams['text'];
                    } else if (array_key_exists('q', $referrerQueryParams)) {
                        $advertKeyword = $referrerQueryParams['q'];
                    }
                } else {
                    $referrer = $referrerHost = null;
                }
            }

            if (!isGuest()) {
                $profile = Profile::findOne(Yii::$app->user->identity->getId());
                $name = $profile ? $profile->getSellerName() : null;
                $email = $profile ? $profile->user->email : null;
                $avatar = $profile ? $profile->getThumb('catalog') : null;
            }

            $client = new LiveClient([
                'session_id' => $session,
                'country' => $country,
                'region' => StringHelper::truncate($region, 30),
                'city' => $city,
                'user_id' => $siteIdentity,
                'ip' => $ip,
                'advert_referrer' => StringHelper::truncate($referrer, 150),
                'advert_keyword' => Utility::getUtmKeyword() ?? $advertKeyword,
                'advert_source' => StringHelper::truncate($referrerHost, 30),
                'sessions_count' => 1,
                'name' => $name,
                'email' => $email,
                'last_action_at' => time(),
                'avatar' => $avatar
            ]);

            $client->bind('history')->attributes = [
                'event_code' => LiveClientHistory::EVENT_START_SESSION,
                'url' => urldecode(Utility::getClientReferrer()),
                'browser' => Utility::getClientBrowser(),
                'os' => Utility::getClientOs(),
                'ip' => Utility::getClientIp(),
                'advert_compaign' => Utility::getUtmCompaign(),
                'advert_keyword' => Utility::getUtmKeyword(),
                'advert_source' => StringHelper::truncate(Utility::getUtmSource(), 30),
                'referrer' => StringHelper::truncate(Utility::getOriginClientReferrer(), 140)
            ];

            if (!$client->save()) {
                Yii::error('************ ERROR CLIENT');
                Yii::error(VarDumper::export($client));
            }
            $client->setIsNewRecord(true);
        } else {
            $client->bump();
        }

        return $client;
    }

    /**
     * @param $referrer
     * @return bool
     */
    public function bump($referrer = null)
    {
        $result = false;
        $history = null;

        if($this->lastHistory !== null) {
            if ($this->lastHistory->created_at < time() - 60 * 60 * 2) {
                $history = new LiveClientHistory([
                    'url' => urldecode(Utility::getClientReferrer()),
                    'live_client_id' => $this->id,
                    'browser' => Utility::getClientBrowser(),
                    'os' => Utility::getClientOs(),
                    'ip' => Utility::getClientIp(),
                    'referrer' => Utility::getOriginClientReferrer(),
                    'advert_compaign' => Utility::getUtmCompaign(),
                    'advert_keyword' => Utility::getUtmKeyword(),
                    'advert_source' => Utility::getUtmSource(),
                    'event_code' => LiveClientHistory::EVENT_START_SESSION
                ]);

                $this->updateCounters(['sessions_count' => 1]);
            } else if ($this->lastHistory->url !== urldecode(Utility::getClientReferrer())) {
                $history = new LiveClientHistory([
                    'event_code' => LiveClientHistory::EVENT_OPEN_PAGE,
                    'url' => urldecode(Utility::getClientReferrer()),
                    'live_client_id' => $this->id,
                    'browser' => Utility::getClientBrowser(),
                    'os' => Utility::getClientOs(),
                    'ip' => Utility::getClientIp(),
                    'advert_compaign' => Utility::getUtmCompaign(),
                    'advert_keyword' => Utility::getUtmKeyword(),
                    'advert_source' => Utility::getUtmSource(),
                    'referrer' => Utility::getClientReferrer(true)
                ]);
            }
        }

        if ($history instanceof LiveClientHistory) {
            $result = $history->save();
        }

        $this->updateAttributes(['last_action_at' => time()]);

        return $result;
    }

    /**
     * @param UserEvent $event
     */
    public static function beforeLogin($event)
    {
        if ($event->isValid) {
            $ip = Utility::getClientIp();

            /* @var $prevMember LiveClient */
            /* @var $member LiveClient */
            $member = LiveClient::find()->where(['session_id' => Yii::$app->session->getId(), 'live_client.ip' => $ip])->andWhere(['not', ['session_id' => null]])->one();
            $prevMember = LiveClient::find()->where(['user_id' => $event->identity->getId(), 'live_client.ip' => $ip])->andWhere(['not', ['user_id' => null]])->one();

            LiveDialogMember::reassign($prevMember->id ?? null, $member->id ?? null);
            LiveClientHistory::reassign($prevMember->id ?? null, $member->id ?? null);

            if ($member !== null) {
                if ($prevMember === null) {
                    $attributesToUpdate = [];
                    $profile = Profile::findOne(['user_id' => $event->identity->getId()]);
                    $name = $profile->getSellerName();
                    $email = $profile->user->email;
                    $avatar = $profile->getThumb('catalog');
                    $phone = $profile->user->phone;

                    if ($member->email === null) {
                        $attributesToUpdate['email'] = $email ?? null;
                    }

                    if ($member->name === null) {
                        $attributesToUpdate['name'] = $name ?? null;
                    }

                    if ($member->avatar === null) {
                        $attributesToUpdate['avatar'] = $avatar ?? null;
                    }

                    if ($member->phone === null) {
                        $attributesToUpdate['phone'] = $phone ?? null;
                    }

                    $attributesToUpdate['user_id'] = $event->identity->getId();

                    $member->updateAttributes($attributesToUpdate);
                } else {
                    $prevMember->bump();
                    $member->delete();
                }
            }
        }
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->custom_data !== null) {
            $this->customDataArray = json_decode($this->custom_data, true);
        }
    }
}
