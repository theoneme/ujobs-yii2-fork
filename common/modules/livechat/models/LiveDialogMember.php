<?php

namespace common\modules\livechat\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "live_dialog_member".
 *
 * @property integer $id
 * @property integer $live_dialog_id
 * @property integer $live_client_id
 *
 * @property LiveDialog $liveDialog
 * @property LiveClient $liveClient
 * @property LiveDialogMessage[] $messages
 */
class LiveDialogMember extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'live_dialog_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['live_dialog_id', 'live_client_id'], 'integer'],
            [['live_dialog_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiveDialog::class, 'targetAttribute' => ['live_dialog_id' => 'id']],
            [['live_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiveClient::class, 'targetAttribute' => ['live_client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('livechat', 'ID'),
            'live_dialog_id' => Yii::t('livechat', 'Live Dialog ID'),
            'live_client_id' => Yii::t('livechat', 'Client ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiveDialog()
    {
        return $this->hasOne(LiveDialog::class, ['id' => 'live_dialog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiveClient()
    {
        return $this->hasOne(LiveClient::class, ['id' => 'live_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(LiveDialogMessage::class, ['member_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getThumb()
    {
        return $this->liveClient->avatar;
    }

    /**
     * @return string
     */
    public function getName()
    {
        if ($this->liveClient !== null) {
            return $this->liveClient->name;
        }

        return 'Клиент #' . $this->id;
    }

    /**
     * @param $id
     * @return array|LiveDialogMember|null|ActiveRecord
     */
    public static function findOrCreate($id)
    {
        $member = LiveDialogMember::find()->where(['live_client_id' => $id])->andWhere(['not', ['live_client_id' => null]])->one();
        if ($member === null) {
            $dialog = LiveDialog::findOrCreate();
            $member = new LiveDialogMember(['live_client_id' => $id, 'live_dialog_id' => $dialog->id]);
            $member->save();
        }
        return $member;
    }

    /**
     * @param $dialog_id
     * @return array|LiveDialogMember|null|ActiveRecord
     */
    public static function findOrCreateAdmin($dialog_id)
    {
        $id = LiveClient::find()->select('id')->where(['user_id' => Yii::$app->user->identity->getId(), 'is_operator' => true])->scalar();
        $member = LiveDialogMember::find()->where(['live_client_id' => $id])->andFilterWhere(['live_dialog_id' => $dialog_id])->one();
        if ($member === null) {
            $dialog = LiveDialog::findOrCreate($dialog_id);
            $member = new LiveDialogMember(['live_dialog_id' => $dialog->id, 'live_client_id' => $id]);
            $member->save();
        }
        return $member;
    }

    /**
     * @param $dialog_id
     * @return array|LiveDialogMember|null|ActiveRecord
     */
    public static function findOrCreateKnownAdmin($dialog_id, $operator_id)
    {
        $member = LiveDialogMember::find()->where(['live_client_id' => $operator_id])->andFilterWhere(['live_dialog_id' => $dialog_id])->one();
        if ($member === null) {
            $dialog = LiveDialog::findOrCreate($dialog_id);
            $member = new LiveDialogMember(['live_dialog_id' => $dialog->id, 'live_client_id' => $operator_id]);
            $member->save();
        }
        return $member;
    }

    /**
     * @param $prevClientId
     * @param $clientId
     */
    public static function reassign($prevClientId, $clientId)
    {
        /* @var $sessionMember LiveDialogMember */
        /* @var $userMember LiveDialogMember */
        $sessionMember = LiveDialogMember::find()->where(['live_client_id' => $clientId])->andWhere(['not', ['live_client_id' => null]])->one();
        if ($sessionMember !== null && $prevClientId !== null) {
            $userMember = LiveDialogMember::find()->where(['live_client_id' => $prevClientId])->one();
            $sessionMember->updateAttributes(['live_client_id' => $prevClientId]);

            if ($userMember !== null) {
                $sessionMember->liveDialog->mergeToDialog($userMember->live_dialog_id);
            }
        }
    }
}