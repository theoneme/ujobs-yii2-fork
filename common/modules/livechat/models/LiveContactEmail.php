<?php

namespace common\modules\livechat\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "live_contact_email".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property integer $created_at
 */
class LiveContactEmail extends \yii\db\ActiveRecord
{
	public $important;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'live_contact_email';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['important', 'message'], 'required'],
            ['email', 'trapValidator'],
            [['name', 'email', 'important'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['message'], 'string', 'max' => 2000],
            [['email', 'important'], 'email'],
        ];
    }

	/**
	 * @param $attribute
	 * @param $param
	 */
	public function trapValidator($attribute, $param)
	{
		if (!empty($this->email)) {
			$this->addError($attribute, Yii::t('model', 'Something wrong with this email'));
		}
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('model', 'Name'),
            'email' => Yii::t('model', 'Email'),
            'message' => Yii::t('model', 'Message'),
            'phone' => Yii::t('model', 'Phone'),
            'created_at' => Yii::t('model', 'Created At'),
	        'important' => Yii::t('model', 'Email')
        ];
    }

	/**
	 * @param bool $insert
	 * @return bool
	 */
    public function beforeSave($insert)
    {
	    $this->email = $this->important;
	    $this->important = null;

	    return parent::beforeSave($insert);
    }

	/**
	 * @param bool $insert
	 * @param array $changedAttributes
	 */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $mail = Yii::$app->mailer->compose()
            ->setTo(['info@ujobs.me'])
            ->setFrom(['support@ujobs.me' => 'Команда uJobs'])
            ->setSubject('Сообщение с контактной формы чата')
            ->setTextBody("Имя пользователя: " . $this->name . "\nEmail: " . $this->email . "\nТелефон: " . $this->phone . "\nСообщение: " . $this->message);
        $mail->send();
    }
}
