<?php

namespace common\modules\livechat\models\search;

use common\modules\livechat\models\LiveClientHistory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\livechat\models\LiveClient;

/**
 * LiveClientSearch represents the model behind the search form about `common\modules\livechat\models\LiveClient`.
 */
class LiveClientSearch extends LiveClient
{
    public $online = false;
    public $popup_sent;
    public $has_name;
    public $has_phone;
    public $has_email;
    public $more_than_one_session;
    public $active_last_days;
    public $posted_job;
    public $posted_tender;
    public $added_to_cart;
    public $removed_from_cart;
    public $facebook_referrer;
    public $google_referrer;
    public $yandex_referrer;
    public $mailru_referrer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'last_action_at', 'user_id', 'sessions_count'], 'integer'],
            [['email', 'name', 'ip', 'session_id', 'country', 'region', 'city', 'advert_referrer', 'advert_keyword', 'advert_source', 'phone', 'phone'], 'safe'],
            [['popup_sent', 'has_name', 'has_email', 'has_phone', 'more_than_one_session', 'active_last_days', 'posted_job', 'posted_tender', 'added_to_cart', 'removed_from_cart', 'facebook_referrer', 'google_referrer', 'yandex_referrer', 'mailru_referrer'], 'boolean']
        ];
    }

    /**
     * @param $attributes
     */
    private function toBool($attributes)
    {
        foreach ($attributes as $attribute) {
            $this->{$attribute} = $this->{$attribute} === null ? null : (bool)$this->{$attribute};
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $this->toBool(['popup_sent', 'has_name', 'has_email', 'has_phone', 'more_than_one_session', 'active_last_days', 'posted_job', 'posted_tender', 'added_to_cart', 'removed_from_cart', 'yandex_referrer', 'google_referrer', 'facebook_referrer', 'mailru_referrer']);
        $query = LiveClient::find()
            ->with(['dialog.messages'])
            ->andWhere(['is_operator' => false]);

        if ($this->online === true) {
            $query->andWhere(['>', 'last_action_at', time() - 60 * 15]);
        }
        if ($this->popup_sent === true) {
            // TODO
        }
        if ($this->has_name === true) {
            $query->andWhere(['not', ['name' => null]]);
            $query->andWhere(['not', ['name' => '']]);
        }
        if ($this->posted_job === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_POST_JOB])]);
        }
        if ($this->posted_tender === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_POST_TENDER])]);
        }
        if ($this->added_to_cart === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_ADD_TO_CART])]);
        }
        if ($this->removed_from_cart === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_REMOVE_FROM_CART])]);
        }
        if ($this->facebook_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'facebook'])
            ]);
        }
        if ($this->google_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'google'])
            ]);
        }
        if ($this->yandex_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'yandex'])
            ]);
        }
        if ($this->mailru_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'mail'])
            ]);
        }
        if ($this->has_phone === true) {
            $query->andWhere(['not', ['phone' => null]]);
            $query->andWhere(['not', ['phone' => '']]);
        }
        if ($this->has_email === true) {
            $query->andWhere(['not', ['email' => null]]);
            $query->andWhere(['not', ['email' => '']]);
        }
        if ($this->more_than_one_session === true) {
            $query->andWhere(['>', 'sessions_count', 1]);
        }
        if ($this->active_last_days !== null) {
            $query->andWhere([$this->active_last_days === true ? '>' : '<', 'last_action_at', time() - 60 * 60 * 24 * 30]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'last_action_at' => $this->last_action_at,
            'user_id' => $this->user_id,
            'sessions_count' => $this->sessions_count,
        ]);
        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'session_id', $this->session_id])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'advert_referrer', $this->advert_referrer])
            ->andFilterWhere(['like', 'advert_keyword', $this->advert_keyword])
            ->andFilterWhere(['like', 'advert_source', $this->advert_source]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAdverts($params)
    {
        $this->load($params);
        $this->toBool(['popup_sent', 'has_name', 'has_email', 'has_phone', 'more_than_one_session', 'active_last_days', 'posted_job', 'posted_tender', 'added_to_cart', 'removed_from_cart', 'yandex_referrer', 'google_referrer', 'facebook_referrer', 'mailru_referrer']);
        $query = LiveClient::find()
            ->joinWith(['dialog.messages'])
            ->andWhere(['is_operator' => false])
            ->andWhere(['not', ['advert_referrer' => null]])
            ->andWhere(['not', ['advert_referrer' => '']])
            ->groupBy('live_client.id');

        if ($this->online === true) {
            $query->andWhere(['>', 'last_action_at', time() - 60 * 15]);
        }
        if ($this->popup_sent === true) {
            // TODO
        }
        if ($this->has_name === true) {
            $query->andWhere(['not', ['name' => null]]);
            $query->andWhere(['not', ['name' => '']]);
        }
        if ($this->posted_job === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_POST_JOB])]);
        }
        if ($this->posted_tender === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_POST_TENDER])]);
        }
        if ($this->added_to_cart === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_ADD_TO_CART])]);
        }
        if ($this->removed_from_cart === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_REMOVE_FROM_CART])]);
        }
        if ($this->facebook_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'facebook'])
            ]);
        }
        if ($this->google_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'google'])
            ]);
        }
        if ($this->yandex_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'yandex'])
            ]);
        }
        if ($this->mailru_referrer === true) {
            $query->andWhere([
                'exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['like', 'live_client.advert_referrer', 'mail'])
            ]);
        }
        if ($this->has_phone === true) {
            $query->andWhere(['not', ['phone' => null]]);
            $query->andWhere(['not', ['phone' => '']]);
        }
        if ($this->has_email === true) {
            $query->andWhere(['not', ['email' => null]]);
            $query->andWhere(['not', ['email' => '']]);
        }
        if ($this->more_than_one_session === true) {
            $query->andWhere(['>', 'sessions_count', 1]);
        }
        if ($this->active_last_days !== null) {
            $query->andWhere([$this->active_last_days === true ? '>' : '<', 'last_action_at', time() - 60 * 60 * 24 * 30]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'last_action_at' => $this->last_action_at,
            'user_id' => $this->user_id,
            'sessions_count' => $this->sessions_count,
        ]);
        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'session_id', $this->session_id])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'advert_referrer', $this->advert_referrer])
            ->andFilterWhere(['like', 'advert_keyword', $this->advert_keyword])
            ->andFilterWhere(['like', 'advert_source', $this->advert_source]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
