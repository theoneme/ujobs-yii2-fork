<?php

namespace common\modules\livechat\models\search;

use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\httpclient\Message;

/**
 * LiveDialogMemberSearch represents the model behind the search form about `common\models\LiveDialogMember`.
 */
class LiveDialogMemberSearch extends LiveDialogMember
{
    public $membersOnline = [];
    public $online;
    public $answer;
    public $message;
    public $name;
    public $last_active_only = false;
    public $require_answer = false;

    public $filter = 'last_active';

    const FILTER_LAST_ACTIVE = 'last_active';
    const FILTER_REQUIRE_ANSWER = 'require_answer';
    const FILTER_NOT_REQUIRE_ANSWER = 'not_require_answer';
    const FILTER_ALL = 'all';

    const YES = 10;
    const NO = 20;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'live_dialog_id', 'online'], 'integer'],
            ['message', 'string'],
            ['last_active_only', 'boolean'],
            [['session_id', 'name', 'email'], 'safe'],
            ['online', 'in', 'range' => [self::YES, self::NO]],
            ['answer', 'in', 'range' => [self::YES, self::NO]],
            ['filter', 'in', 'range' => [self::FILTER_LAST_ACTIVE, self::FILTER_REQUIRE_ANSWER, self::FILTER_NOT_REQUIRE_ANSWER]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $operators = LiveClient::find()->select('id')->where(['is_operator' => true])->scalar();
        $query = LiveDialogMember::find()
            ->joinWith(['liveDialog.messages', 'liveClient'])
            ->where(['or',
                ['not', ['live_dialog_member.live_client_id' => $operators]],
                ['live_dialog_member.live_client_id' => null]
            ])
            ->andWhere(['not', ['live_dialog_message.id' => null]])
            ->andWhere(['not', ['live_dialog_member.live_client_id' => null]])
            ->andFilterWhere(['like', 'live_client.name', $this->name])
            ->andFilterWhere(['like', 'live_dialog_message.message', $this->message])
            ->groupBy(['live_dialog_member.id']);
        if (count($this->membersOnline)) {
            $query->orderBy([new Expression('FIELD (live_dialog_member.id, ' . implode(',', $this->membersOnline) . ') DESC')]);
        }
        if (!empty($this->online)) {
            $condition = ['live_dialog_member.id' => $this->membersOnline];
            if ($this->online == self::NO) {
                $condition = ['not', $condition];
            }
            $query->andWhere($condition);
        }

        switch ($this->filter) {
            case self::FILTER_LAST_ACTIVE:
                $query->having(['>', 'max(live_dialog_message.created_at)', time() - 60 * 60 * 2]);
                break;

            case self::FILTER_REQUIRE_ANSWER:
                $condition = ['exists', LiveDialogMessage::find()
                    ->from('live_dialog_message mes2')
                    ->joinWith('member')
                    ->where('mes2.live_dialog_id = live_dialog_message.live_dialog_id')
                    ->andWhere(['live_dialog_member.live_client_id' => $operators])
                ];
                $condition = ['not', $condition];
                $query->andWhere($condition);
                break;

            case self::FILTER_NOT_REQUIRE_ANSWER:
                $condition = ['exists', LiveDialogMessage::find()
                    ->from('live_dialog_message mes2')
                    ->joinWith('member')
                    ->where('mes2.live_dialog_id = live_dialog_message.live_dialog_id')
                    ->andWhere(['live_dialog_member.live_client_id' => $operators])
                ];
                $query->andWhere($condition);
                break;
            case self::FILTER_ALL:
                break;
        }
        $query->addOrderBy(['MAX(live_dialog_message.created_at)' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getYesNoLabels()
    {
        return [
            self::YES => 'Да',
            self::NO => 'Не'
        ];
    }

    /**
     * @return array
     */
    public static function getFilterLabels()
    {
        return [
            self::FILTER_LAST_ACTIVE => Yii::t('livechat', 'Last active'),
            self::FILTER_NOT_REQUIRE_ANSWER => Yii::t('livechat', 'No requires answer'),
            self::FILTER_REQUIRE_ANSWER => Yii::t('livechat', 'Requires answer'),
            self::FILTER_ALL => Yii::t('livechat', 'All dialogs')
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatusLabel()
    {
        $labels = static::getFilterLabels();
        return isset($labels[$this->filter]) ? $labels[$this->filter] : Yii::t('model', 'Unknown filter');
    }
}
