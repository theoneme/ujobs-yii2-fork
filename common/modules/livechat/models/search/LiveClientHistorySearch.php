<?php

namespace common\modules\livechat\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\livechat\models\LiveClientHistory;

/**
 * LiveClientHistorySearch represents the model behind the search form about `common\modules\livechat\models\LiveClientHistory`.
 */
class LiveClientHistorySearch extends LiveClientHistory
{
    public $facebook;
    public $google;
    public $yandex;
    public $mailru;
    public $search_google;
    public $search_yandex;
    public $added_to_cart;
    public $posted_job;
    public $posted_tender;
    public $paid;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'live_client_id', 'event_code'], 'integer'],
            [['url', 'browser', 'os', 'device', 'referrer', 'ip', 'custom_data', 'advert_compaign', 'advert_keyword', 'advert_source'], 'safe'],
            [['facebook', 'google', 'yandex', 'mailru', 'added_to_cart', 'posted_job', 'posted_tender', 'search_google', 'search_yandex', 'paid'], 'boolean']
        ];
    }

    /**
     * @param $attributes
     */
    private function toBool($attributes)
    {
        foreach ($attributes as $attribute) {
            $this->{$attribute} = $this->{$attribute} === null ? null : (bool)$this->{$attribute};
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiveClientHistory::find()
            ->joinWith(['liveClient'])
            ->where(['or',
                ['and', ['not', ['live_client_history.advert_compaign' => null]], ['not', ['live_client_history.advert_compaign' => '']]],
                ['and', ['not', ['live_client_history.advert_source' => null]], ['not', ['live_client_history.advert_source' => '']]],
                ['and', ['not', ['live_client_history.advert_keyword' => null]], ['not', ['live_client_history.advert_keyword' => '']]],
                ['and', ['not', ['live_client_history.referrer' => null]], ['not', ['live_client_history.referrer' => '']]],
            ])
            ->groupBy('live_client_id');

        // add conditions that should always apply here

        $this->load($params);
        $this->toBool(['facebook', 'google', 'yandex', 'mailru', 'added_to_cart', 'posted_job', 'posted_tender', 'search_google', 'search_yandex', 'paid']);

        if ($this->posted_tender === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_POST_TENDER])]);
        }
        if ($this->posted_job === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_POST_JOB])]);
        }
        if ($this->added_to_cart === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_ADD_TO_CART])]);
        }
        if ($this->paid === true) {
            $query->andWhere(['exists', LiveClientHistory::find()->where('live_client_history.live_client_id = live_client.id')->andWhere(['event_code' => LiveClientHistory::EVENT_PAYMENT])]);
        }
        if ($this->facebook === true) {
            $query->andWhere(['or', ['like', 'live_client_history.advert_source', 'facebook'], ['like', 'live_client_history.referrer', 'facebook']]);
        }
        if ($this->google === true) {
            $query->andWhere(['like', 'live_client_history.advert_source', 'google']);
        }
        if ($this->yandex === true) {
            $query->andWhere(['like', 'live_client_history.advert_source', 'yandex']);
        }
        if ($this->mailru === true) {
            $query->andWhere(['like', 'live_client_history.advert_source', 'mail']);
        }
        if ($this->search_google === true) {
            $query->andWhere(['like', 'live_client_history.referrer', 'google'])->andWhere(['or', ['live_client_history.advert_compaign' => null], ['live_client_history.advert_compaign' => '']]);
        }
        if ($this->search_yandex === true) {
            $query->andWhere(['like', 'live_client_history.referrer', 'yandex'])->andWhere(['or', ['live_client_history.advert_compaign' => null], ['live_client_history.advert_compaign' => '']]);;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'live_client_id' => $this->live_client_id,
            'event_code' => $this->event_code,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'browser', $this->browser])
            ->andFilterWhere(['like', 'os', $this->os])
            ->andFilterWhere(['like', 'device', $this->device])
            ->andFilterWhere(['like', 'live_client_history.referrer', $this->referrer])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data])
            ->andFilterWhere(['like', 'live_client_history.advert_compaign', $this->advert_compaign])
            ->andFilterWhere(['like', 'live_client_history.advert_keyword', $this->advert_keyword])
            ->andFilterWhere(['like', 'live_client_history.advert_source', $this->advert_source]);

        return $dataProvider;
    }
}
