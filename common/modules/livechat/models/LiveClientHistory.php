<?php

namespace common\modules\livechat\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "live_client_history".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $live_client_id
 * @property integer $event_code
 * @property string $url
 * @property string $ip
 * @property string $referrer
 * @property string $advert_compaign
 * @property string $advert_keyword
 * @property string $advert_source
 * @property string $browser
 * @property string $os
 * @property string $device
 * @property string $custom_data
 *
 * @property LiveClient $liveClient
 */
class LiveClientHistory extends \yii\db\ActiveRecord
{
	const EVENT_OPEN_PAGE = 5;
	const EVENT_START_SESSION = 10;
	const EVENT_START_DIALOG = 15;
	const EVENT_MESSAGE_SENT = 20;
	const EVENT_MESSAGE_RECEIVED = 25;
	const EVENT_OFFER_SERVICE = 30;
	const EVENT_ADVERT_FROM = 35;
	const EVENT_ADD_TO_CART = 40;
	const EVENT_TRIGGERED_ADVERT = 45;
	const EVENT_REMOVE_FROM_CART = 50;
	const EVENT_RECEIVED_POPUP = 55;
	const EVENT_POST_JOB = 60;
	const EVENT_POST_TENDER = 65;
	const EVENT_WENT_TO_CHECKOUT = 70;
	const EVENT_SUCCESSFUL_PAYMENT = 75;
	const EVENT_PAYMENT = 80;
	const EVENT_TENDER_RESPONSE = 85;
	const EVENT_CONVERSATION_MESSAGE = 90;

	public $customDataArray = [];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'live_client_history';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::class,
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['created_at', 'updated_at', 'live_client_id', 'event_code'], 'integer'],
			[['referrer'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 455],
			[['ip'], 'string', 'max' => 15],
            [['advert_source'], 'string', 'max' => 35],
            [['advert_compaign'], 'string', 'max' => 125],
            [['advert_keyword'], 'string', 'max' => 75],
			[['live_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => LiveClient::class, 'targetAttribute' => ['live_client_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('livechat', 'ID'),
			'created_at' => Yii::t('livechat', 'Created At'),
			'updated_at' => Yii::t('livechat', 'Updated At'),
			'live_client_id' => Yii::t('livechat', 'Live Client ID'),
			'event_code' => Yii::t('livechat', 'Event Code'),
			'url' => Yii::t('livechat', 'Url'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLiveClient()
	{
		return $this->hasOne(LiveClient::class, ['id' => 'live_client_id']);
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		$label = '';

		switch ($this->event_code) {
			case self::EVENT_OPEN_PAGE:
				$label = Yii::t('livechat', 'Page open');
				break;
			case self::EVENT_START_SESSION:
				$label = Yii::t('livechat', 'Start session');
				break;
			case self::EVENT_MESSAGE_RECEIVED:
				$label = Yii::t('livechat', 'Message received');
				break;
			case self::EVENT_MESSAGE_SENT:
				$label = Yii::t('livechat', 'Message sent');
				break;
			case self::EVENT_ADVERT_FROM:
				$label = Yii::t('livechat', 'Advert from');
				break;
			case self::EVENT_OFFER_SERVICE:
				$label = Yii::t('livechat', 'Received service recommendations');
				break;
			case self::EVENT_ADD_TO_CART:
				$label = Yii::t('livechat', 'Client has added service to cart');
				break;
			case self::EVENT_REMOVE_FROM_CART:
				$label = Yii::t('livechat', 'Client has removed service from cart');
				break;
			case self::EVENT_POST_JOB:
				$label = Yii::t('livechat', 'Client has posted job');
				break;
			case self::EVENT_POST_TENDER:
				$label = Yii::t('livechat', 'Client has posted request');
				break;
            case self::EVENT_PAYMENT:
                $label = Yii::t('livechat', 'Client has paid');
                break;
            case self::EVENT_TENDER_RESPONSE:
                $label = Yii::t('livechat', 'Client has left response on request');
                break;
            case self::EVENT_CONVERSATION_MESSAGE:
                $label = Yii::t('livechat', 'Client has sent message to another user');
                break;
		}

		return $label;
	}

	/**
	 * @param $prevClientId
	 * @param $clientId
	 */
	public static function reassign($prevClientId, $clientId)
	{
	    if($prevClientId !== null) {
            LiveClientHistory::updateAll(['live_client_id' => $prevClientId], ['live_client_id' => $clientId]);
        }
	}

	/**
	 *
	 */
	public function afterFind()
	{
		parent::afterFind();

		if ($this->custom_data !== null) {
			$this->customDataArray = json_decode($this->custom_data, true);
		}
	}
}
