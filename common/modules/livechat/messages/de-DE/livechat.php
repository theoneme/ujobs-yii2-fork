<?php 
 return [
    'Advert Referrer' => 'Quelle',
    'Advert from' => 'Der Adware-Quelle',
    'Advert referrer' => 'Quelle',
    'Advert source' => 'Domain Quelle',
    'All dialogs' => 'Alle Dialoge',
    'City' => 'Die Stadt',
    'Client #{num}' => 'Kunde #{num}',
    'Client ID' => 'Client-ID',
    'Client has added service to cart' => 'Der Kunde fügte eine Dienstleistung in den Warenkorb',
    'Client has left response on request' => 'Der Kunde hat auf Antrag',
    'Client has paid' => 'Der Kunde bezahlt die Dienstleistung',
    'Client has posted job' => 'Der Kunde legte die Arbeit',
    'Client has posted request' => 'Der Kunde legte einen Antrag',
    'Client has removed service from cart' => 'Der Kunde entfernt den Dienst aus dem Papierkorb',
    'Client has sent message to another user' => 'Der Kunde schickte eine Nachricht an einen anderen Benutzer',
    'Continue chat' => 'Den Dialog fortsetzen',
    'Country' => 'Land',
    'Created At' => 'Behandlungsdatum',
    'Current page' => 'Aktuelle Seite',
    'Event Code' => 'Ereignis-id',
    'Go to cart' => 'Gehen Sie in den Papierkorb',
    'Greetings, how can i help you?' => 'Hallo, wie kann ich helfen?',
    'Has added something to cart.' => 'Hat etwas in den Korb',
    'Has offered services to user: ' => 'Bot user-Services: ',
    'Has posted job.' => 'USeesoft Arbeit.',
    'Has posted request.' => 'Gepostet Antrag.',
    'I\'m here if you need me' => 'Ich bin hier, wenn was ist',
    'Id: {id}, Worker: {worker}' => 'ID: {id}, Artist: {worker}',
    'Is Read' => 'Gelesen',
    'It seems you are looking for professional. We can recommend you to post a request, so thousands of professionals will see your request. {link}' => 'Es scheint, Sie suchen einen Spezialisten für die Ausführung des Auftrags. Wir empfehlen Ihnen, den Antrag, den Sie sehen, Hunderte von Profis. {link}',
    'Last Action At' => 'Die Letzte Aktion',
    'Last active' => 'Die letzten aktiven',
    'Last page' => 'Die zuletzt besuchte Seite',
    'Live Client ID' => 'Client-ID',
    'Live Dialog ID' => 'ID des dialogs',
    'Member ID' => 'ID des Teilnehmers',
    'Message' => 'Nachricht',
    'Message received' => 'Nachricht empfangen wird',
    'Message sent' => 'Nachricht gesendet',
    'Name' => 'Name',
    'No' => 'Nein',
    'No requires answer' => 'Die Antwort ist nicht erforderlich',
    'Offline' => 'Offline',
    'Online' => 'Online',
    'Order for {price}' => 'Bestellen Sie für {price}',
    'Page open' => 'Übergang auf die Seite',
    'Phone' => 'Telefon',
    'Post a Request' => 'Platzieren Sie den Antrag',
    'Received service recommendations' => 'Erhielt Empfehlungen zur Verfügung',
    'Region' => 'Region',
    'Registered on {date}' => 'Erschienen im System {date}',
    'Requires answer' => 'Erfordert eine Antwort',
    'Send' => 'Senden',
    'Session ID' => 'Session-ID',
    'Sessions Count' => 'Die Anzahl der Sitzungen',
    'Set client email' => 'Geben die E-Mail-Client',
    'Set client name' => 'Der name des Kunden anzugeben',
    'Set client phone' => 'Angeben Telefon des Kunden',
    'Show earlier dialogs' => 'Zeigen die frühen Dialoge',
    'Start session' => 'Beginn der Sitzung',
    'Submit' => 'Senden',
    'Thanks for posted announcement. If you wish to make your service more noticeable for customers, please see {link}' => 'Vielen Dank für die veröffentlichte Anzeige. Wenn Sie möchten machen es sichtbar für den Kunden, bieten Sie sehen {link}',
    'Thanks for posted request. While it is on moderation, we can offer services to you, which may be interesting for you.' => 'Vielen Dank für eine gehostete Anwendung. Während Sie auf Moderation, bieten wir durchsuchen die Angebote, die Sie vielleicht interessieren.',
    'There are no operators ready to response now, but you can leave your question and we will help you during working time.' => 'Jetzt Betreiber keine fertigen Antworten, aber Sie können Ihre Frage und wir helfen Ihnen während der Arbeitszeit.',
    'Unknown {item}' => '{item} nicht definiert ist',
    'Updated At' => 'Letzter Beitrag',
    'User ID' => 'Benutzer-ID',
    'View detailed information' => 'Weitere Informationen',
    'View details' => 'Lesen Sie weiter',
    'View them' => 'Durchsehen',
    'We have noticed that you have services in your cart, but you still have not bought them. Is there anything that i can help you with?' => 'Wir haben bemerkt, dass Sie ergänzten Dienstleistungen im Warenkorb, aber noch nichts gekauft. Kann ich Ihnen irgendwie helfen?',
    'We have noticed that you were looking for {keyword}, maybe these service are exactly what you were looking for?' => 'Wir haben bemerkt, dass Sie gesucht haben {keyword}, vielleicht ist diese Dienstleistung genau das, was du gesucht hast?',
    'We offer you to register on this site to get access for all functions of service. {link}' => 'Wir bieten Ihnen auf der Website registrieren, um Zugriff auf alle seine Funktionen. {link}',
    'Yes' => 'Ja',
    'You have already sent offline message recently. Please, try later.' => 'Sie haben bereits vor kurzem schickten die Adresse durch das offline-Formular. Bitte versuchen Sie es später.',
    'You may leave your contacts, so we will be able to contact you when we can.' => 'Sie setzen Ihre Kontakte, dann können wir mit Ihnen Kontakt aufnehmen so bald wie möglich.',
    'You still have services in your cart that are awaiting for payment. {link}' => 'Im Warenkorb blieben blieben Dienstleistungen, ausstehende Erledigung. {link}',
    'by {author}' => 'von {author}',
    'our tariffs' => 'unsere Tarife',
    '{who} has offered services to you.' => '{who} bot Ihnen die Dienstleistungen.',
    'Victoria' => 'Victoria',
    'Type your message and press Enter' => 'Geben Sie die Nachricht ein und drücken Sie die EINGABETASTE',
];