<?php 
 return [
    'Advert Referrer' => 'स्रोत',
    'Advert from' => 'विज्ञापन से स्रोत',
    'Advert referrer' => 'स्रोत',
    'Advert source' => 'डोमेन स्रोत',
    'All dialogs' => 'सभी संवादों',
    'City' => 'शहर',
    'Client #{num}' => 'ग्राहक #{num}',
    'Client ID' => 'ग्राहक आईडी',
    'Client has added service to cart' => 'ग्राहक जोड़ा सेवा करने के लिए गाड़ी',
    'Client has left response on request' => 'ग्राहक प्रतिक्रिया के लिए अनुरोध',
    'Client has paid' => 'ग्राहक सेवा के लिए भुगतान किया',
    'Client has posted job' => 'ग्राहक प्रकाशित किया गया था काम',
    'Client has posted request' => 'ग्राहक आदेश रखा',
    'Client has removed service from cart' => 'ग्राहक हटा दिया सेवा से टोकरी',
    'Client has sent message to another user' => 'ग्राहक एक संदेश भेजने के लिए किसी अन्य उपयोगकर्ता',
    'Continue chat' => 'बातचीत जारी रखने के लिए',
    'Country' => 'देश',
    'Created At' => 'उपयोग की तारीख',
    'Current page' => 'वर्तमान पृष्ठ',
    'Event Code' => 'इवेंट ID',
    'Go to cart' => 'जाने के लिए टोकरी',
    'Greetings, how can i help you?' => 'हैलो, मैं मदद कर सकता है आप?',
    'Has added something to cart.' => 'जोड़ा गया करने के लिए कुछ की टोकरी',
    'Has offered services to user: ' => 'सुझाव सेवाओं के उपयोगकर्ता के लिए: ',
    'Has posted job.' => 'प्रकाशित किया गया था काम करते हैं.',
    'Has posted request.' => 'रखा एक बोली है । ',
    'I\'m here if you need me' => 'मैं यहाँ हूँ अगर आप की जरूरत है मुझे',
    'Id: {id}, Worker: {worker}' => 'ID: {id}: {worker}',
    'Is Read' => 'पढ़ें',
    'It seems you are looking for professional. We can recommend you to post a request, so thousands of professionals will see your request. {link}' => 'ऐसा लगता है कि आप के लिए देख रहे हैं के लिए एक विशेषज्ञ का काम है । हम अनुशंसा करते हैं आप जगह बोली कि सैकड़ों देखेंगे विशेषज्ञों की है । {link}',
    'Last Action At' => 'पिछले कार्रवाई',
    'Last active' => 'पिछले सक्रिय',
    'Last page' => 'पिछले दौरा किया पृष्ठ',
    'Live Client ID' => 'ग्राहक आईडी',
    'Live Dialog ID' => '# Dialogue',
    'Member ID' => 'भागीदार आईडी',
    'Message' => 'संदेश',
    'Message received' => 'संदेश प्राप्त किया',
    'Message sent' => 'संदेश भेजा जाता है',
    'Name' => 'नाम',
    'No' => 'कोई',
    'No requires answer' => 'की आवश्यकता नहीं है जवाब',
    'Offline' => 'ऑफ़लाइन',
    'Online' => 'ऑनलाइन',
    'Order for {price}' => 'उपलब्ध के लिए {price}',
    'Page open' => 'पृष्ठ पर जाने के लिए',
    'Phone' => 'फोन',
    'Post a Request' => 'करने के लिए एक आदेश जगह',
    'Received service recommendations' => 'प्राप्त सिफारिशों सेवाओं के लिए',
    'Region' => 'क्षेत्र',
    'Registered on {date}' => 'में दिखाई दिया प्रणाली {date}',
    'Requires answer' => 'अपेक्षित प्रतिक्रिया',
    'Send' => 'भेजें',
    'Session ID' => 'सत्र आईडी',
    'Sessions Count' => 'सत्र की संख्या',
    'Set client email' => 'निर्दिष्ट ईमेल क्लाइंट',
    'Set client name' => 'निर्दिष्ट ग्राहक का नाम',
    'Set client phone' => 'निर्दिष्ट ग्राहक के फोन',
    'Show earlier dialogs' => 'शो के शुरू संवाद',
    'Start session' => 'सत्र की शुरुआत',
    'Submit' => 'भेजें',
    'Thanks for posted announcement. If you wish to make your service more noticeable for customers, please see {link}' => 'धन्यवाद के लिए प्रकाशित विज्ञापन. यदि आप चाहते हैं करने के लिए यह अधिक दिखाई बनाने के लिए ग्राहकों के लिए, हम आप की पेशकश को देखने के लिए {link}',
    'Thanks for posted request. While it is on moderation, we can offer services to you, which may be interesting for you.' => 'पोस्टिंग के लिए धन्यवाद आवेदन है । जबकि यह संचालित किया जाता है, हम आपको प्रोत्साहित करते हैं देखने के लिए प्रदान करता है कि हो सकता है ब्याज तुम.',
    'There are no operators ready to response now, but you can leave your question and we will help you during working time.' => 'अब आप की जरूरत नहीं है ऑपरेटरों जवाब देने के लिए तैयार है, लेकिन आप छोड़ सकते हैं अपने सवाल है और हम मदद करेंगे आप काम के घंटे के दौरान.',
    'Unknown {item}' => '{item} से परिभाषित नहीं है',
    'Updated At' => 'अंतिम पोस्ट',
    'User ID' => 'उपयोगकर्ता आईडी',
    'View detailed information' => 'विस्तृत जानकारी',
    'View details' => 'और अधिक पढ़ें',
    'View them' => 'ब्राउज़ करें',
    'We have noticed that you have services in your cart, but you still have not bought them. Is there anything that i can help you with?' => 'हमने देखा है कि आप जोड़ दिया है करने के लिए सेवाओं की गाड़ी है, लेकिन अभी तक कुछ भी नहीं खरीदा है । कर सकते हैं मैं आप मदद करने के लिए कुछ है?',
    'We have noticed that you were looking for {keyword}, maybe these service are exactly what you were looking for?' => 'हमने देखा है कि आप के लिए खोज {keyword}, शायद इन सेवाओं वास्तव में कर रहे हैं क्या आप के लिए देख रहे थे?',
    'We offer you to register on this site to get access for all functions of service. {link}' => 'हम आपको आमंत्रित करने के लिए वेबसाइट पर रजिस्टर करने के लिए पहुँच प्राप्त करने के लिए अपने सभी क्षमताओं. {link}',
    'Yes' => 'हाँ',
    'You have already sent offline message recently. Please, try later.' => 'आप हाल ही में भेजा एक अपील के माध्यम से ऑफलाइन फार्म. कृपया बाद में फिर से कोशिश.',
    'You may leave your contacts, so we will be able to contact you when we can.' => 'आप रख सकते हैं अपने संपर्कों, तो हम में सक्षम हो जाएगा संपर्क करने के लिए आप जल्द से जल्द अवसर पर.',
    'You still have services in your cart that are awaiting for payment. {link}' => 'आप टोकरी में थे थे, सेवाओं, लंबित पंजीकरण. {link}',
    'by {author}' => 'से {author}',
    'our tariffs' => 'हमारे दरों',
    '{who} has offered services to you.' => '{who} की पेशकश की है सेवाओं आप के लिए है । ',
    'Victoria' => 'विक्टोरिया',
    'Type your message and press Enter' => 'अपना संदेश दर्ज करें और प्रेस दर्ज करें',
];