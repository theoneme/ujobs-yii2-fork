<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.07.2017
 * Time: 14:48
 */

namespace common\modules\livechat\controllers;

use common\helpers\ElasticConditionsHelper;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\modules\livechat\helpers\Utility;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveClientHistory;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Yii;
use yii\filters\ContentNegotiator;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class AjaxScenarioController
 * @package common\modules\livechat\controllers
 */
class AjaxScenarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => [
                    'recommend-advert-services',
                    'go-to-cart',
                    'recommend-tender-services',
                    'default-greeting',
                    'recommend-tariffs',
                    'go-to-cart-with-link',
                    'recommend-registration',
                    'recommend-tender-post'
                ],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array|bool
     */
    public function actionRecommendAdvertServices()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);

        $client->customDataArray['recommend_advert_services'] = true;
        $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

        /* @var JobElastic[] $jobs */
        $jobs = JobElastic::find()
            ->where([
                'type' => Job::TYPE_JOB,
                'status' => Job::STATUS_ACTIVE
            ])->query([
                'bool' => [
                    'should' => ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $client->advert_keyword)
                ]
            ])
            ->addOrderBy('tariff.price desc')
            ->limit(3)
            ->all();

        if (!empty($jobs)) {
            $output = [];
            foreach ($jobs as $job) {
                $output[] = [
                    'img' => $job->getThumb(),
                    'label' => $job->getLabel(),
                    'url' => Url::to(['/job/view', 'alias' => $job->alias]),
                    'price' => $job->getPrice(),
                    'text' => Yii::t('livechat', 'Order for {price}', ['price' => $job->getPrice()]),
                    'author' => $job->getSellerName(),
                ];
            }

            $message = new LiveDialogMessage([
                'message' => Yii::t('livechat', "We have noticed that you were looking for {keyword}, maybe these service are exactly what you were looking for?", ['keyword' => $client->advert_keyword]),
                'custom_data' => json_encode($output),
                'live_dialog_id' => $clientMember->live_dialog_id,
                'member_id' => $operatorMember->id,
                'type' => LiveDialogMessage::TYPE_SERVICE_OFFER
            ]);

            if ($message->save()) {
                return [
                    'messageId' => $message->id,
                    'offers' => $output,
                    'text' => $message->message
                ];
            }
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function actionRecommendTenderServices()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);
        $lastTender = $client->lastTenderHistory;

        if (is_array($client->customDataArray['recommend_tender_services'])) {
            $client->customDataArray['recommend_tender_services']['items'][] = $lastTender->id;
        } else {
            $client->customDataArray['recommend_tender_services']['items'] = [$lastTender->id];
        }
        $client->customDataArray['last_scenario'] = time();
        $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

        $jobs = JobElastic::find()->where([
            'type' => Job::TYPE_JOB,
            'status' => Job::STATUS_ACTIVE,
            'category_id' => $lastTender->customDataArray['postTender']['categoryId']
        ])->addOrderBy('tariff.price desc')->limit(3)->all();

        if ($jobs) {
            $output = [];
            /* @var Job[] $jobs */
            foreach ($jobs as $job) {
                $output[] = [
                    'img' => $job->getThumb(),
                    'label' => $job->getLabel(),
                    'url' => Url::to(['/job/view', 'alias' => $job->alias]),
                    'price' => $job->getPrice(),
                    'text' => Yii::t('livechat', 'Order for {price}', ['price' => $job->getPrice()]),
                    'author' => $job->getSellerName(),
                ];
            }

            $message = new LiveDialogMessage([
                'message' => Yii::t('livechat', "Thanks for posted request. While it is on moderation, we can offer services to you, which may be interesting for you."),
                'custom_data' => json_encode($output),
                'live_dialog_id' => $clientMember->live_dialog_id,
                'member_id' => $operatorMember->id,
                'type' => LiveDialogMessage::TYPE_SERVICE_OFFER
            ]);
            if ($message->save()) {
                return [
                    'messageId' => $message->id,
                    'offers' => $output,
                    'text' => $message->message
                ];
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function actionDefaultGreeting()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $client->customDataArray['default_greeting'] = true;

        $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

        return true;
    }

    /**
     * @return array|bool
     */
    public function actionRecommendTenderPost()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);

        $message = new LiveDialogMessage([
            'message' => Yii::t('livechat', "It seems you are looking for professional. We can recommend you to post a request, so thousands of professionals will see your request. {link}", [
                'link' => Html::a(Yii::t('livechat', 'Post a Request'), Url::to(['/entity/global-create', 'type' => 'tender-service']), [
                    'target' => '_blank', 'style' => 'text-decoration:underline'
                ])
            ]),
            'live_dialog_id' => $clientMember->live_dialog_id,
            'member_id' => $operatorMember->id,
        ]);
        if ($message->save()) {
            $client->customDataArray['conversation_tender']['time'] = time();
            $client->customDataArray['last_scenario'] = time();
            $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

            return [
                'messageId' => $message->id,
                'text' => $message->message
            ];
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function actionRecommendTariffs()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);

        $message = new LiveDialogMessage([
            'message' => Yii::t('livechat', "Thanks for posted announcement. If you wish to make your service more noticeable for customers, please see {link}", ['link' => Html::a(Yii::t('livechat', 'our tariffs'), Url::to(['/page/static', 'alias' => 'tariffs']), ['target' => '_blank', 'style' => 'text-decoration:underline'])]),
            'live_dialog_id' => $clientMember->live_dialog_id,
            'member_id' => $operatorMember->id,
        ]);
        if ($message->save()) {
            $lastJob = $client->lastJobHistory;

            if (is_array($client->customDataArray['recommend_tariffs'])) {
                $client->customDataArray['recommend_tariffs']['items'][] = $lastJob->id;
            } else {
                $client->customDataArray['recommend_tariffs']['items'] = [$lastJob->id];
            }
            $client->customDataArray['recommend_tariffs']['time'] = time();
            $client->customDataArray['last_scenario'] = time();
            $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

            return [
                'messageId' => $message->id,
                'text' => $message->message
            ];
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function actionGoToCart()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);

        $message = new LiveDialogMessage([
            'message' => Yii::t('livechat', "We have noticed that you have services in your cart, but you still have not bought them. Is there anything that i can help you with?"),
            'live_dialog_id' => $clientMember->live_dialog_id,
            'member_id' => $operatorMember->id,
        ]);
        if ($message->save()) {
            $cart = $client->cart;
            $cartItems = unserialize($cart->cartData);

            $dataString = implode('_', array_map(function ($value) {
                return $value['item']->id;
            }, $cartItems));

            if (is_array($client->customDataArray['cart'])) {
                $client->customDataArray['cart']['items'][] = $dataString;
            } else {
                $client->customDataArray['cart']['items'] = [$dataString];
            }
            $client->customDataArray['cart']['time'] = time();
            $client->customDataArray['last_scenario'] = time();
            $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

            return [
                'messageId' => $message->id,
                'text' => $message->message
            ];
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function actionGoToCartWithLink()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);

        $message = new LiveDialogMessage([
            'message' => Yii::t('livechat', "You still have services in your cart that are awaiting for payment. {link}", ['link' => Html::a(Yii::t('livechat', 'Go to cart'), ['/store/cart/index'])]),
            'live_dialog_id' => $clientMember->live_dialog_id,
            'member_id' => $operatorMember->id,
        ]);
        if ($message->save()) {
            $cart = $client->cart;
            $cartItems = unserialize($cart->cartData);

            $dataString = implode('_', array_map(function ($value) {
                return $value['item']->id;
            }, $cartItems));

            if (is_array($client->customDataArray['cart'])) {
                $client->customDataArray['cart']['items'][] = $dataString;
            } else {
                $client->customDataArray['cart']['items'] = [$dataString];
            }
            $client->customDataArray['cart']['time'] = time();
            $client->customDataArray['last_scenario'] = time();
            $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

            return [
                'messageId' => $message->id,
                'text' => $message->message
            ];
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function actionRecommendRegistration()
    {
        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());
        $operator = LiveClient::findOne(['is_operator' => true]);

        $clientMember = LiveDialogMember::findOrCreate($client->id);
        $operatorMember = LiveDialogMember::findOrCreateKnownAdmin($clientMember->live_dialog_id, $operator->id);

        $message = new LiveDialogMessage([
            'message' => Yii::t('livechat', "We offer you to register on this site to get access for all functions of service. {link}", [
                'link' => Html::a(Yii::t('app', 'Sign Up'), null, [
                    'class' => 'nav-autor green-b modal-dismiss',
                    'data-toggle' => 'modal',
                    'data-target' => '#ModalSignup'
                ])
            ]), 'live_dialog_id' => $clientMember->live_dialog_id,
            'member_id' => $operatorMember->id,
        ]);
        if ($message->save()) {
            $client->customDataArray['recommend_registration'] = true;
            $client->updateAttributes(['custom_data' => json_encode($client->customDataArray)]);

            return [
                'messageId' => $message->id,
                'text' => $message->message
            ];
        }

        return false;
    }

    /**
     * @param $historyKey
     * @param $event
     * @return bool
     */
    private function createEvent($historyKey, $event)
    {
        $input = Yii::$app->request->post();

        if ($input !== null) {
            $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());

            $data = [
                $historyKey => $input
            ];

            $clientHistory = new LiveClientHistory([
                'live_client_id' => $client->id,
                'custom_data' => json_encode($data),
                'event_code' => $event,
            ]);

            return $clientHistory->save();
        }

        return false;
    }
}