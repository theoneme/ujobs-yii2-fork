<?php

namespace common\modules\livechat\controllers;

use common\components\CurrencyHelper;
use common\helpers\ElasticConditionsHelper;
use common\helpers\SecurityHelper;
use common\models\elastic\JobElastic;
use common\models\Job;
use common\modules\livechat\helpers\Utility;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveContactEmail;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use common\modules\livechat\services\ScenarioService;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class AjaxController
 * @package common\modules\livechat\controllers
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'update-member-info' => ['POST'],
                    'get-member' => ['POST'],
                    'contact' => ['POST'],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['update-member-info', 'get-member', 'contact', 'advice-search', 'send-advice', 'do-not-show', 'load-more'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionContact()
    {
        $model = new LiveContactEmail();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $session = Yii::$app->session;
            $data = $session->get('livechat_offline_form');
            if ($data === false || $data['date'] < time() - 60 * 15) {
                $session->set('livechat_offline_form', ['date' => time()]);
                $model->save();
                return ['type' => 'success', 'message' => Yii::t('app', 'Message sent')];
            } else {
                return ['type' => 'success', 'message' => Yii::t('livechat', 'You have already sent offline message recently. Please, try later.')];
            }
        }
        return ['type' => 'error', 'message' => Yii::t('app', 'Error occurred during sending message')];
    }

    /**
     * @return array
     */
    public function actionUpdateMemberInfo()
    {
        $member = LiveClient::findOne(SecurityHelper::decrypt(Yii::$app->request->post('id')));
        if ($member !== null) {
            $member->name = Yii::$app->request->post('name');
            $member->email = Yii::$app->request->post('email');
            $member->save();
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * @return array
     */
    public function actionGetMember()
    {
        $cookies = Yii::$app->request->cookies;
        $referrer = $cookies->getValue('referrer');

        $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp(), $referrer);
        $member = LiveDialogMember::findOrCreate($client->id);

        $action = ScenarioService::getAction($client->eventTree, $client);
        $scenario = [];
        if ($action !== null) {
            $scenario = ScenarioService::getScenario(Yii::$app->response->cookies, $cookies, $action, $client);
        }

        $history = '';
        $time = time();
        $time -= ($time % (60 * 60 * 24 * 2));
        $messages = array_filter($member->liveDialog->messages, function ($var) use ($time) {
            return $var->created_at > $time;
        });
        if (count($messages)) {
            foreach ($messages as $message) {
                switch ($message->type) {
                    case LiveDialogMessage::TYPE_SIMPLE_MESSAGE:
                        $history .= $this->renderPartial('message-types/simple-message', [
                            'side' => $message->member_id === $member->id ? 'user' : 'our',
                            'text' => $message->message,
                            'date' => $message->created_at,
                            'timestamp' => $message->created_at
                        ]);
                        break;
                    case LiveDialogMessage::TYPE_SERVICE_OFFER:
                        $history .= $this->renderPartial('message-types/offer-message', [
                            'message' => $message,
                            'adminMember' => $message->member
                        ]);
                }
            }
        } else {
            $history .= $this->renderPartial('message-types/simple-message', [
                'side' => 'our',
                'text' => Yii::t('livechat', 'Greetings, how can i help you?'),
                'date' => time(),
                'timestamp' => time()
            ]);
        }
        $member->id = SecurityHelper::encrypt($member->id);
        $client_id = SecurityHelper::encrypt($member->liveClient->id);
        return [
            'member_id' => $member->id,
            'dialog_id' => SecurityHelper::encrypt($member->live_dialog_id),
            'client_id' => SecurityHelper::encrypt($member->liveClient->id),
            'no_operator_message' => $this->renderPartial('message-types/no-operator-message', ['client_id' => $client_id, 'show_form' => isGuest() && empty($client->email) && empty($client->name), 'client' => $client]),
            'history' => $history,
            'scenario' => json_encode($scenario),
            'loadMore' => count($messages) < count($member->liveDialog->messages)
        ];
    }

    /**
     * @return array
     */
    public function actionLoadMore()
    {
        /**
         * @var $member LiveDialogMember
         * @var $messages LiveDialogMessage[]
         */
        $history = '';
        $dialogId = SecurityHelper::decrypt(Yii::$app->request->post('dialog_id'));
        $member = LiveDialogMember::find()
            ->joinWith(['liveClient'])
            ->where(['live_dialog_id' => $dialogId])
            ->andWhere(['or',
                ['live_client.user_id' => userId()],
                ['live_client.session_id' => Yii::$app->session->getId(), 'live_client.user_id' => null],
            ])
            ->one();
        $loadMore = false;
        if ($member !== null) {
            $time = Yii::$app->request->post('time');
            $messages = array_reverse(LiveDialogMessage::find()
                ->where(['live_dialog_id' => $dialogId])
                ->andFilterWhere(['<', 'live_dialog_message.created_at', $time])
                ->limit(10)
                ->orderBy('created_at desc')
                ->all());
            $loadMore = count($messages) < LiveDialogMessage::find()->where(['live_dialog_id' => $dialogId])->andFilterWhere(['<', 'live_dialog_message.created_at', $time])->count();

            foreach ($messages as $message) {
                switch ($message->type) {
                    case LiveDialogMessage::TYPE_SIMPLE_MESSAGE:
                        $history .= $this->renderPartial('message-types/simple-message', [
                            'side' => $message->member_id === $member->id ? 'user' : 'our',
                            'text' => $message->message,
                            'date' => $message->created_at,
                            'timestamp' => $message->created_at
                        ]);
                        break;
                    case LiveDialogMessage::TYPE_SERVICE_OFFER:
                        $history .= $this->renderPartial('message-types/offer-message', [
                            'message' => $message,
                            'adminMember' => $message->member
                        ]);
                }
            }
        }
        return [
            'history' => $history,
            'loadMore' => $loadMore
        ];
    }

    /**
     * @return array
     */
    public function actionAdviceSearch()
    {
        $output = ['success' => false];
        $request = urldecode(Yii::$app->request->post('request'));

        if ($request) {
            /* @var $jobs JobElastic[] */
            $jobs = JobElastic::find()
                ->where([
                    'type' => Job::TYPE_JOB,
                    'status' => Job::STATUS_ACTIVE
                ])
                ->query([
                    'bool' => [
                        'should' => ElasticConditionsHelper::getNestedSearchQuery('translation', ['translation.title'], $request)
                    ]
                ])
                ->limit(30)
                ->all();

            if (!empty($jobs)) {
                $output['success'] = true;
                foreach ($jobs as $job) {
                    $output['data'][] = [
                        'label' => $job->getLabel() . "(" . Yii::t('livechat', 'Id: {id}, Worker: {worker}', ['id' => $job->id, 'worker' => $job->getSellerName()]) . ")",
                        'id' => $job->id
                    ];
                }
            }
        }

        return $output;
    }

    /**
     * @return array
     */
    public function actionSendAdvice()
    {
        $output = ['success' => false];
        $ids = explode(',', Yii::$app->request->post('ids', []));

        if ($ids) {
            /* @var $jobs JobElastic[] */
            $jobs = JobElastic::findAll($ids);
            if ($jobs) {
                $output['success'] = true;
                foreach ($jobs as $job) {
                    $output['data'][] = [
                        'img' => $job->getThumb(),
                        'label' => $job->getLabel(),
                        'url' => Yii::$app->urlManagerFrontEnd->createUrl(['/job/view', 'alias' => $job->alias]),
                        'price' => $job->getPrice(),
                        'text' => Yii::t('livechat', 'Order for {price}', ['price' => $job->getPrice()]),
                        'author' => $job->getSellerName(),
                        'oldPrice' => CurrencyHelper::convertAndFormat($job->currency_code, Yii::$app->params['app_currency_code'], ($job->price * 1.1), true)
                    ];
                }
            }
        }

        return $output;
    }

    /**
     * @return array
     */
    public function actionDoNotShow()
    {
        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'livechat_do_not_show',
            'value' => time(),
        ]));

        return ['success' => true];
    }
}