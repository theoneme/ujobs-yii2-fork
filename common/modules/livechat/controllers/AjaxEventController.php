<?php

namespace common\modules\livechat\controllers;

use common\helpers\SecurityHelper;
use common\models\JobPackage;
use common\modules\livechat\helpers\Utility;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveClientHistory;
use common\modules\livechat\models\LiveContactEmail;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class AjaxEventController
 * @package common\modules\livechat\controllers
 */
class AjaxEventController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => ContentNegotiator::class,
				'only' => ['update-member-info'],
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
		];
	}

	/**
	 * @return bool
	 */
	public function actionAddToCart()
	{
        return $this->createEvent('cartAdd',LiveClientHistory::EVENT_ADD_TO_CART);
	}

	/**
	 * @return bool
	 */
	public function actionRemoveFromCart()
	{
        return $this->createEvent('cartRemove',LiveClientHistory::EVENT_REMOVE_FROM_CART);
	}

	/**
	 * @return bool
	 */
	public function actionPostJob()
	{
        return $this->createEvent('postJob',LiveClientHistory::EVENT_POST_JOB);
	}

	/**
	 * @return bool
	 */
	public function actionPostTender()
	{
	    return $this->createEvent('postTender',LiveClientHistory::EVENT_POST_TENDER);
	}

    /**
     * @return bool
     */
    public function actionAddPayment()
    {
        return $this->createEvent('addPayment',LiveClientHistory::EVENT_PAYMENT);
    }

    /**
     * @return bool
     */
    public function actionAddResponse()
    {
        return $this->createEvent('addResponse', LiveClientHistory::EVENT_TENDER_RESPONSE);
    }

    /**
     * @return bool
     */
    public function actionAddConversationMessage()
    {
        return $this->createEvent('addConversationMessage', LiveClientHistory::EVENT_CONVERSATION_MESSAGE);
    }

    /**
     * @param $historyKey
     * @param $event
     * @return bool
     */
	private function createEvent($historyKey, $event)
    {
        $input = Yii::$app->request->post();

        if($input !== null) {
            $client = LiveClient::findOrCreate(Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getId(), Yii::$app->session->getId(), Utility::getClientIp());

            $data = [
                $historyKey => $input
            ];

            $clientHistory = new LiveClientHistory([
                'live_client_id' => $client->id,
                'custom_data' => json_encode($data),
                'event_code' => $event,
            ]);

            return $clientHistory->save();
        }

        return false;
    }
}