<?php

namespace common\modules\livechat\controllers;

use common\controllers\BackEndController;
use common\helpers\SecurityHelper;
use common\modules\livechat\models\LiveClient;
use common\modules\livechat\models\LiveClientHistory;
use common\modules\livechat\models\LiveDialog;
use common\modules\livechat\models\LiveDialogMember;
use common\modules\livechat\models\LiveDialogMessage;
use common\modules\livechat\models\MessageForm;
use common\modules\livechat\models\search\LiveClientHistorySearch;
use common\modules\livechat\models\search\LiveClientSearch;
use common\modules\livechat\models\search\LiveContactEmailSearch;
use common\modules\livechat\models\search\LiveDialogMemberSearch;
use common\modules\livechat\services\MailerService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\Response;

/**
 * Class AdminController
 * @package common\modules\livechat\controllers
 */
class AdminController extends BackEndController
{
    public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
	        [
		        'class' => ContentNegotiator::class,
		        'only' => ['update-ajax', 'send-mail-ajax'],
		        'formats' => [
			        'application/json' => Response::FORMAT_JSON,
		        ],
	        ],
        ]);
    }

    /**
     * Lists all LiveDialogMember models.
     * @return mixed
     */
    public function actionIndex()
    {
        try {
            $client = new Client(['baseUrl' => Yii::$app->params['liveUrl'],'responseConfig' => ['format' => Client::FORMAT_JSON]]);
            $membersOnline = $client->get("online")->send()->getData();
        }
        catch (Exception $e) {
            $membersOnline = [];
        }
//        $searchModel = new LiveDialogMemberSearch(['membersOnline' => $membersOnline]);
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    $searchModel = new LiveClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $operatorIds = LiveDialogMember::find()
            ->select('live_dialog_member.id')
            ->joinWith(['liveClient'])
            ->where(['live_client.is_operator' => true])
            ->andWhere([
                'exists', LiveDialogMember::find()
                    ->from('live_dialog_member ldm2')
                    ->where('live_dialog_member.live_dialog_id = ldm2.live_dialog_id')
                    ->andWhere(['ldm2.live_client_id' => ArrayHelper::getColumn($dataProvider->models, 'id')])
            ])
            ->groupBy(['live_dialog_member.id'])
            ->column();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'membersOnline' => $membersOnline,
            'operatorIds' => $operatorIds,
        ]);
    }

    /**
     * Lists all LiveDialogMember models.
     * @return mixed
     */
    public function actionAdverts()
    {
//        $searchModel = new LiveClientSearch();
//        $dataProvider = $searchModel->searchAdverts(Yii::$app->request->queryParams);
//
//        return $this->render('adverts', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);

        $searchModel = new LiveClientHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('adverts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing LiveDialogMember model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDialog($id = null)
    {
        try {
            $client = new Client(['baseUrl' => Yii::$app->params['liveUrl'],'responseConfig' => ['format' => Client::FORMAT_JSON]]);
            $membersOnline = $client->get("online")->send()->getData();
        }
        catch (Exception $e) {
            $membersOnline = [];
        }

        $id = LiveDialog::findOne($id) !== null ? $id : null;
        $searchModel = new LiveDialogMemberSearch(['membersOnline' => $membersOnline, 'last_active_only' => true]);
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $params = [
            'id' => $id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'membersOnline' => $membersOnline,
            'action' => 'dialog'
        ];

        if ($id !== null) {
            $adminMember = LiveDialogMember::findOrCreateAdmin($id);
            $params['adminMember'] = $adminMember;
            $params['member_id'] = SecurityHelper::encrypt($adminMember->id);
            $params['dialog_id'] = SecurityHelper::encrypt($adminMember->live_dialog_id);
            $params['userMember'] = LiveDialogMember::find()->where(['live_dialog_id' => $id])->andWhere(['not', ['id' => $params['adminMember']->id]])->one();
	        $params['client_id'] = SecurityHelper::encrypt($params['userMember']->live_client_id);
            $params['messages'] = LiveDialogMessage::find()->joinWith(['member', 'liveDialog'])->where(['live_dialog_message.live_dialog_id' => $id])->all();
            $params['client'] = $params['userMember']->liveClient;
        }

        if(Yii::$app->request->isAjax) {
	        Yii::$app->assetManager->bundles = [
		        'yii\bootstrap\BootstrapPluginAsset' => false,
		        'yii\bootstrap\BootstrapAsset' => false,
		        'yii\web\JqueryAsset' => false,
		        'kartik\editable\EditableAsset' => false
	        ];
        }

        return $this->render('livechat', $params);
    }

    /**
     * Updates an existing LiveDialogMember model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionHistory($id = null)
    {
        $id = LiveDialog::findOne($id) !== null ? $id : null;
        $searchModel = new LiveDialogMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $params = [
            'id' => $id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'membersOnline' => [],
            'action' => 'history'
        ];

        if ($id !== null) {
            $adminMember = LiveDialogMember::findOrCreateAdmin($id);
            $params['adminMember'] = $adminMember;
            $params['member_id'] = SecurityHelper::encrypt($adminMember->id);
            $params['dialog_id'] = SecurityHelper::encrypt($adminMember->live_dialog_id);
            $params['userMember'] = LiveDialogMember::find()->where(['live_dialog_id' => $id])->andWhere(['not', ['id' => $params['adminMember']->id]])->one();
            $params['client_id'] = SecurityHelper::encrypt($params['userMember']->live_client_id);
            $params['messages'] = LiveDialogMessage::find()->joinWith(['member', 'liveDialog'])->where(['live_dialog_message.live_dialog_id' => $id])->all();
            $params['client'] = $params['userMember']->liveClient;
        }

        if(Yii::$app->request->isAjax) {
            Yii::$app->assetManager->bundles = [
                'yii\bootstrap\BootstrapPluginAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\web\JqueryAsset' => false,
                'kartik\editable\EditableAsset' => false
            ];
        }

        return $this->render('livechat', $params);
    }

	/**
	 * @param $id
	 * @return string|Response
	 */
    public function actionView($id)
    {
    	$client = LiveClient::find()->joinWith(['history'])->where(['live_client.id' => $id])->one();

    	if($client !== null) {
		    if (Yii::$app->request->isAjax) {
			    $dialog = $client->dialog;
			    $historyQuery = LiveClientHistory::find()->where(['live_client_id' => $client->id])->orderBy('id desc');
			    $historyDataProvider = new ActiveDataProvider([
				    'query' => $historyQuery,
			    ]);
			    $adminMember = LiveDialogMember::findOrCreateAdmin($id);
			    $openPageHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_OPEN_PAGE])->orderBy(['id' => SORT_DESC])->all();
			    $startSessionHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_START_SESSION])->orderBy(['id' => SORT_DESC])->all();
			    $startDialogHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_START_DIALOG])->orderBy(['id' => SORT_DESC])->all();
			    $messageSentHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_MESSAGE_SENT])->orderBy(['id' => SORT_DESC])->all();
			    $messageReceivedHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_MESSAGE_RECEIVED])->orderBy(['id' => SORT_DESC])->all();
				$cartHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => [LiveClientHistory::EVENT_REMOVE_FROM_CART, LiveClientHistory::EVENT_ADD_TO_CART]])->orderBy(['id' => SORT_DESC])->all();
				$foundAdvertSource = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_TRIGGERED_ADVERT])->orderBy(['id' => SORT_DESC])->all();
			    $postJobHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_POST_JOB])->orderBy(['id' => SORT_DESC])->all();
			    $postTenderHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_POST_TENDER])->orderBy(['id' => SORT_DESC])->all();
                $paymentHistory = LiveClientHistory::find()->where(['live_client_id' => $id, 'event_code' => LiveClientHistory::EVENT_PAYMENT])->orderBy(['id' => SORT_DESC])->all();

				$communicated_today = count(array_filter($messageSentHistory, function($v){
					return $v->created_at > (time() - 60 * 60 * 24);
				})) > 0;
			    return $this->renderAjax('view-partial', [
				    'model' => $client,
				    'adminMember' => $adminMember,
				    'openPageHistory' => $openPageHistory,
				    'startSessionHistory' => $startSessionHistory,
				    'startDialogHistory' => $startDialogHistory,
				    'messageSentHistory' => $messageSentHistory,
				    'messageReceivedHistory' => $messageReceivedHistory,
				    'foundAdvertSource' => $foundAdvertSource,
				    'postJobHistory' => $postJobHistory,
				    'postTenderHistory' => $postTenderHistory,
				    'paymentHistory' => $paymentHistory,
				    'cartHistory' => $cartHistory,
				    'dialog' => $dialog,
				    'historyDataProvider' => $historyDataProvider,
				    'isAjax' => true,
					'communicated_today' => $communicated_today
			    ]);
		    }
	    }

	    return $this->redirect(['/livechat/admin/index']);
    }

	/**
	 * @return string
	 */
    public function actionLive()
    {
	    $searchModel = new LiveClientSearch(['online' => true]);
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $operatorIds = LiveDialogMember::find()
            ->select('live_dialog_member.id')
            ->joinWith(['liveClient'])
            ->where(['live_client.is_operator' => true])
            ->andWhere([
                'exists', LiveDialogMember::find()
                    ->from('live_dialog_member ldm2')
                    ->where('live_dialog_member.live_dialog_id = ldm2.live_dialog_id')
                    ->andWhere(['ldm2.live_client_id' => ArrayHelper::getColumn($dataProvider->models, 'id')])
            ])
            ->groupBy(['live_dialog_member.id'])
            ->column();

    	return $this->render('live', [
    		'dataProvider' => $dataProvider,
            'operatorIds' => $operatorIds,
	    ]);
    }

	/**
	 * @param $id
	 * @return string|Response
	 */
    public function actionMessage($id)
    {
	    $client = LiveClient::find()->joinWith(['history'])->where(['live_client.id' => $id])->one();
	    $adminClient = LiveClient::findOne(['user_id' => Yii::$app->user->identity->getId(), 'is_operator' => true]);

	    if($client !== null && $adminClient !== null) {
	    	$dialog = $client->dialog;
		    $adminMember = LiveDialogMember::findOrCreateAdmin($dialog->id);
		    $memberId = SecurityHelper::encrypt($adminMember->id);
		    $dialogId = SecurityHelper::encrypt($adminMember->live_dialog_id);
		    $clientId = SecurityHelper::encrypt($adminClient->id);

		    if (Yii::$app->request->isAjax) {
			    Yii::$app->assetManager->bundles = [
				    'yii\bootstrap\BootstrapPluginAsset' => false,
				    'yii\bootstrap\BootstrapAsset' => false,
				    'yii\web\JqueryAsset' => false,
//				    'common\modules\livechat\assets\LivechatAdminAsset' => false,
				    'common\modules\livechat\assets\LivechatAsset' => false
			    ];
			    $this->layout = false;

			    $messageForm = new MessageForm();

			    return $this->renderAjax('message-partial', [
				    'model' => $client,
				    'adminModel' => $adminClient,
				    'messageForm' => $messageForm,
				    'memberId' => $memberId,
				    'dialogId' => $dialogId,
				    'clientId' => $clientId,

				    'isAjax' => true,
			    ]);
		    }
	    }

	    return $this->redirect(['/livechat/admin/index']);
    }

    /**
     * Lists all LiveContactEmail models.
     * @return mixed
     */
    public function actionEmails()
    {
        $searchModel = new LiveContactEmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('emails', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * @param null $id
	 * @return array
	 */
    public function actionUpdateAjax($id = null)
    {
	    $output = [
		    'success' => false
	    ];

	    $model = LiveClient::findOne($id);
	    $model->detachBehavior('linkable');
	    if ($input = Yii::$app->request->post()) {
	    	$model->load($input);
		    if ($model->save()) {
			    $output = [
				    'success' => true,
			    ];
		    }
	    }
	    return $output;
    }

	/**
	 * @return array
	 */
    public function actionSendMailAjax()
    {
	    $output = [
		    'success' => false
	    ];

	    if ($input = Yii::$app->request->post()) {
	    	$targetClient = LiveClient::findOne((int)$input['clientId']);
	    	$operatorClient = LiveClient::findOne((int)$input['operatorId']);

		    $subject = $input['subject'];
		    $message = $input['message'];

		    if($targetClient->email && $operatorClient->email) {
			    $output['success'] = MailerService::sendMail(Yii::$app->mailer,
                    Yii::$app->params['ujobsNoty'],
				    $subject,
				    'custom-message',
				    [
					    'content' => $message,
					    'name' => $targetClient->getName()
				    ],
				    $targetClient
			    );
			    $output['message'] = 'Письмо отправлено';
		    } else {
		    	$output['message'] = 'У получателя не указан Email';
		    }
	    }

	    return $output;
    }
}
