<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 14.06.2017
 * Time: 16:01
 */

namespace common\modules\livechat\helpers;

use Yii;

/**
 * Class Utility
 * @package common\modules\livechat\models
 */
class Utility
{
    /**
     * @return mixed
     */
    public static function getClientIp()
    {
        return $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['REMOTE_ADDR'] ?? null;
    }

    /**
     * @return mixed|string
     */
    public static function getClientOs()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $os = "Unknown OS Platform";
        $patterns = [
            '/windows nt 10/i' => 'Windows 10',
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        ];

        foreach ($patterns as $regex => $value) {
            if (preg_match($regex, $userAgent)) {
                $os = $value;
            }
        }

        return $os;
    }

    /**
     * @return mixed|string
     */
    public static function getClientBrowser()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $os = "Unknown Browser";
        $patterns = [
            '/msie/i' => 'Internet Explorer',
            '/firefox/i' => 'Firefox',
            '/safari/i' => 'Safari',
            '/chrome/i' => 'Chrome',
            '/edge/i' => 'Edge',
            '/opera/i' => 'Opera',
            '/netscape/i' => 'Netscape',
            '/maxthon/i' => 'Maxthon',
            '/konqueror/i' => 'Konqueror',
            '/mobile/i' => 'Handheld Browser'
        ];

        foreach ($patterns as $regex => $value) {
            if (preg_match($regex, $userAgent)) {
                $os = $value;
            }
        }

        return $os;
    }

    /**
     * @param bool $onlyIfDifferent
     * @return mixed|null
     */
    public static function getOriginClientReferrer($onlyIfDifferent = true)
    {
        $cookies = Yii::$app->request->cookies;
        $referrer = $cookies->getValue('referrer');

        if ($referrer !== null) {
            $referrerParts = parse_url($referrer);
            $referrerQueryParams = [];
            if (!empty($referrerParts['query'])) {
                parse_str($referrerParts['query'], $referrerQueryParams);
            }
            $referrerHost = $referrerParts['host'] ?? null;

            if ($onlyIfDifferent) {
                if (empty($referrerQueryParams) || $referrerHost === Yii::$app->request->serverName) {
                    $referrer = null;
                }
            }
        }

        return $referrer ?? null;
    }

    /**
     * @param bool $onlyIfDifferent
     * @return mixed|null|string
     */
    public static function getClientReferrer($onlyIfDifferent = false)
    {
        $referrer = Yii::$app->request->referrer ?? null;

        if ($referrer !== null) {
            $referrerParts = parse_url($referrer);
            $referrerQueryParams = [];
            if (!empty($referrerParts['query'])) {
                parse_str($referrerParts['query'], $referrerQueryParams);
            }
            $referrerHost = $referrerParts['host'] ?? null;

            if ($onlyIfDifferent) {
                if (empty($referrerQueryParams) || $referrerHost === Yii::$app->request->serverName) {
                    $referrer = null;
                }
            }
        }

        return $referrer;
    }

    /**
     * @return mixed|null
     */
    public static function getUtmCompaign()
    {
        $referrer = Yii::$app->request->referrer;
        $utmCompaign = null;

        if ($referrer !== null) {
            $referrerParts = parse_url($referrer);
            $referrerQueryParams = [];
            if (!empty($referrerParts['query'])) {
                parse_str($referrerParts['query'], $referrerQueryParams);
            }

            if (array_key_exists('utm_campaign', $referrerQueryParams)) {
                $utmCompaign = $referrerQueryParams['utm_campaign'];
            }
        }

        return $utmCompaign;
    }

    /**
     * @return mixed|null
     */
    public static function getUtmKeyword()
    {
        $referrer = Yii::$app->request->referrer;
        $utmKeyword = null;

        if ($referrer !== null) {
            $referrerParts = parse_url($referrer);
            $referrerQueryParams = [];
            if (!empty($referrerParts['query'])) {
                parse_str($referrerParts['query'], $referrerQueryParams);
            }
            if (array_key_exists('utm_term', $referrerQueryParams)) {
                $utmKeyword = $referrerQueryParams['utm_term'];
            }
        }

        return $utmKeyword;
    }

    /**
     * @return mixed|null
     */
    public static function getUtmSource()
    {
        $referrer = Yii::$app->request->referrer;
        $utmSource = null;

        if ($referrer !== null) {
            $referrerParts = parse_url($referrer);
            $referrerQueryParams = [];
            if (!empty($referrerParts['query'])) {
                parse_str($referrerParts['query'], $referrerQueryParams);
            }
            if (array_key_exists('utm_source', $referrerQueryParams)) {
                $utmSource = $referrerQueryParams['utm_source'];
            }
        }

        return $utmSource;
    }
}