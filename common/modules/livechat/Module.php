<?php

namespace common\modules\livechat;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Event;

/**
 * Class Module
 * @package common\modules\livechat
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\livechat\controllers';

	/**
	 * @param \yii\base\Application $app
	 */
	public function bootstrap($app)
	{
		\Yii::$app->i18n->translations['livechat*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@common/modules/livechat/messages',

			'fileMap' => [
				'livechat' => 'livechat.php',
			],
		];

		if(!Yii::$app->request->isAjax) {
			$cookies = Yii::$app->response->cookies;
			$cookies->add(new \yii\web\Cookie([
				'name' => 'referrer',
				'value' => Yii::$app->request->referrer,
			]));
		}

		if (!defined('YII_BACKEND')) {
            Event::on(\yii\web\User::class, \yii\web\User::EVENT_BEFORE_LOGIN, ['common\modules\livechat\models\LiveClient', 'beforeLogin']);
        }
	}
}
