<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class TranslationsMapper
 * @package common\mappers
 */
class TranslationsMapper implements DataMapperInterface
{
    public const MODE_FULL = 0;
    public const MODE_SHORT = 10;

    /**
     * @param $rawData
     * @param int $mode
     * @return mixed
     */
    public static function getMappedData($rawData, $mode = self::MODE_SHORT)
    {
        $translationsArray = $rawData;

        if ($mode === self::MODE_FULL) {
            return $translationsArray;
        }

        $languageTranslation = array_key_exists(Yii::$app->language, $translationsArray)
            ? $translationsArray[Yii::$app->language]
            : array_pop($translationsArray);

        $languageTranslation = array_reduce($translationsArray, function ($carry, $var) {
            return $carry + $var;
        }, $languageTranslation);

        return $languageTranslation ?? [];
    }
}