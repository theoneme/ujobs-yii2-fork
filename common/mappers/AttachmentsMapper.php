<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class AttachmentsMapper
 * @package common\mappers
 */
class AttachmentsMapper implements DataMapperInterface
{
    public const MODE_FULL = 0;
    public const MODE_THUMB = 10;

    /**
     * @param $rawData
     * @param int $mode
     * @return array
     */
    public static function getMappedData($rawData, $mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_FULL) {
            return array_map(function ($value) {
                return Yii::$app->mediaLayer->getThumb($value['content']);
            }, $rawData);
        }

        return array_map(function ($value) {
            return Yii::$app->mediaLayer->getThumb($value['content'], 'catalog');
        }, $rawData);
    }
}