<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.11.2017
 * Time: 13:58
 */

/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.10.2017
 * Time: 13:59
 */

namespace common\dto;

use common\models\UserAttribute;
use stdClass;

class UserSkillDataObject
{
    /**
     * @var
     */
    private $_models;

    /**
     * UserSkillDataObject constructor.
     * @param $models
     */
    public function __construct($models)
    {
        $this->_models = $models;
    }

    /**
     * @param bool $asArray
     * @return array|bool
     */
    public function format($asArray = true)
    {
        if ($this->_models === null) return false;

        $output = [];
        foreach ($this->_models as $model) {
            $output[] = $this->makeItem($model, $asArray);
        }

        return $output;
    }

    /**
     * @param $model
     * @param $asArray
     * @return mixed|Object
     */
    private function makeItem($model, $asArray)
    {
        $map = $this->map($model);
        $output = $asArray ? $this->makeArrayItem($map) : $this->makeObjectItem($map);


        return $output;
    }

    /**
     * @param $model
     * @return array|null
     */
    private function map($model)
    {
        $output = null;

        switch (true) {
            case $model instanceOf UserAttribute:
                $output = [
                    'title' => $model->attrValueDescription->title
                ];
                break;
        }

        return $output;
    }

    /**
     * @param $input
     * @return mixed
     */
    private function makeArrayItem($input)
    {
        return $input;
    }

    /**
     * @param $input
     * @return StdClass
     */
    private function makeObjectItem($input)
    {
        $object = new StdClass();
        $object->title = $input['title'];
        return $object;
    }
}