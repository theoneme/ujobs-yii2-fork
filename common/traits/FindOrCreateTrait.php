<?php
namespace common\traits;

use ReflectionClass;
use yii\db\ActiveRecord;

trait FindOrCreateTrait {
    /**
     * @param $condition
     * @param $save bool
     *
     * @return ActiveRecord
     */
    public static function findOrCreate($condition, $save = true)
    {
        $item = static::find()->where($condition)->one();
        if ($item === null) {
            $item = new static($condition);
            if ($save) {
                $item->save();
            }
        }

        return $item;
    }
}