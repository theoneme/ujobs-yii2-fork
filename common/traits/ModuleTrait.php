<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.05.2018
 * Time: 17:46
 */

namespace common\traits;

/**
 * Class ModuleTrait
 * @package common\traits
 */
trait ModuleTrait
{
    /**
     * @return Module
     */
    public function getModule()
    {
        return \Yii::$app->getModule('user');
    }

    /**
     * @return string
     */
    public static function getDb()
    {
        return \Yii::$app->db;
    }
}