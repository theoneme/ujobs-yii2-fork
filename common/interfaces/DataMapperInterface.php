<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:43
 */

namespace common\interfaces;

/**
 * Interface DataMapperInterface
 * @package common\interfaces
 */
interface DataMapperInterface
{
    public static function getMappedData($rawData);
}