<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.04.2017
 * Time: 14:58
 */

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\BaseStringHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * Class ImageBehavior
 * @property ActiveRecord $owner
 * @package common\behaviors
 */
class ImageBehavior extends Behavior
{
    /**
     * @var string
     */
    public $imageField = 'image';

    /**
     * @var string
     */
    public $folder = null;

    /**
     * @var string
     */
    public $labelField = null;

    /**
     * @var string
     */
    public $label = null;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @return void
     */
    public function beforeSave()
    {
        if ($this->folder === null) {
            $this->folder = Inflector::camel2id(StringHelper::basename($this->owner->className()));
        }

        $this->label = $this->labelField && !empty($this->owner->{$this->labelField})
            ? Yii::$app->utility->transliterate(BaseStringHelper::truncateWords(preg_replace('/\s+/', ' ', trim($this->owner->{$this->labelField})), 5, ''))
            : Inflector::camel2id(StringHelper::basename(($this->owner->className())));

        if (!empty($this->owner->{$this->imageField})) {
            $oldPath = $this->owner->getOldAttribute($this->imageField);
            $webRoot = Yii::getAlias('@frontend') . '/web';
            if (file_exists($webRoot . $this->owner->{$this->imageField}) && preg_match('/^\/uploads\/temp\/.*/', $this->owner->{$this->imageField})) {
                $year = date('Y');
                $month = date('m');
                $basePath = "/uploads/{$this->folder}/{$year}/{$month}/";
                $dir = $webRoot . $basePath;
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                    if (Yii::$app instanceof \yii\console\Application) {
                        chown($dir, 'www-data');
                        chgrp($dir, 'www-data');
                    }
                }
                $extension = pathinfo($this->owner->{$this->imageField})['extension'] ?? 'jpg';
                $newPath = $basePath . $this->label . "-" . uniqid() . "." . $extension;

                rename($webRoot . $this->owner->{$this->imageField}, $webRoot . $newPath);
                $mediaLayer = Yii::$app->get('mediaLayer');
                $mediaLayer->saveToAws($newPath);
                $this->owner->{$this->imageField} = $newPath;
                if ($oldPath && $newPath != $oldPath) {
                    $mediaLayer->removeMedia($oldPath);
                }
            } else {
                $this->owner->{$this->imageField} = $oldPath;
            }
        }
    }
}