<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.04.2017
 * Time: 14:58
 */

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\BaseStringHelper;
use yii\widgets\ActiveForm;

/**
 * Class AttachmentBehavior
 * @property ActiveRecord $owner
 * @package common\behaviors
 */
class AttachmentBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attachmentRelation = 'attachments';

    /**
     * @var array
     */
    private $changes = [];

    /**
     * @var string
     */
    public $folder = 'job';

    /**
     * @var array
     */
    private $relationsBuffer = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->attachmentRelation === null) {
            throw new InvalidConfigException('The "attachmentRelation" property must be set.');
        }
    }

    /**
     * @param null $id
     * @return ActiveRecord
     */
    public function bindAttachment($id = null)
    {
        if ($id !== null) {
            $this->changes[] = $id;
        }

        /* @var ActiveRecord $class */
        $class = $this->owner->getRelation($this->attachmentRelation)->modelClass;

        /* @var ActiveRecord[] $records */
        $records = $this->owner->{$this->attachmentRelation};
        foreach ($records as $record) {
            if ($record->getAttribute('content') === $id && $id !== null) {
                $this->relationsBuffer[] = $record;

                return $record;
            }
        }

        $record = new $class;
        $records[] = $record;

        $this->relationsBuffer = $records;
        $this->owner->populateRelation($this->attachmentRelation, $records);

        return $record;
    }

    /**
     * @return void
     */
    public function afterValidate()
    {
        if (!Model::validateMultiple($this->relationsBuffer)) {
            $this->owner->addError($this->attachmentRelation);
        }
    }

    /**
     * @return void
     */
    public function afterSave()
    {
        $ids = !empty($this->changes) ? $this->changes : [];

        /* @var ActiveRecord $relationClass */
        $relationClass = $this->owner->getRelation($this->attachmentRelation)->modelClass;
        $relationQuery = $this->owner->getRelation($this->attachmentRelation);
        $foreignKeyName = array_keys($relationQuery->link)[0];

        $whereCondition = $relationQuery->where === null ? [] : $relationQuery->where;
        $onCondition = $relationQuery->on === null ? [] : $relationQuery->on;

        /* @var ActiveQuery $queryCondition */
        $queryCondition = $relationClass::find()
            ->andWhere($whereCondition)
            ->andWhere([$foreignKeyName => $this->owner->getPrimaryKey()])
            ->andFilterWhere($onCondition);

        if (!empty($ids)) {
            $queryCondition->andWhere(['not', ['content' => $ids]]);
        }
        $items = $queryCondition->all();

        /* @var ActiveRecord $relationClass */
        if ($items !== null) {
            foreach ($items as $item) {
                $item->delete();
            }
        }

        /* @var ActiveRecord $record */
        $relations = array_filter($this->relationsBuffer, function($value) use ($ids) {
        	return in_array($value->content, $ids);
        });

        $this->relationsBuffer = $relations;
        $this->owner->populateRelation($this->attachmentRelation, $relations);

        foreach ($relations as $key => $record) {
            if (file_exists(Yii::getAlias('@frontend') . '/web' . $record->content) && preg_match('/^\/uploads\/temp\/.*/', $record->content)) {
                $year = date('Y');
                $month = date('m');
                $dir = Yii::getAlias('@frontend') . '/web' . "/uploads/{$this->folder}/{$year}/{$month}/";
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                    if (Yii::$app instanceof \yii\console\Application) {
                        chown($dir, 'www-data');
                        chgrp($dir, 'www-data');
                    }
                }
                $pathInfo = pathinfo($record->content);
                $newPath = "/uploads/{$this->folder}/{$year}/{$month}/" . Yii::$app->utility->transliterate(
                        BaseStringHelper::truncateWords(
                            preg_replace('/\s+/', ' ', trim($this->owner->getLabel()))
                            , 5, '')
                    ) . "-" . ($this->owner->getPrimaryKey()) . "-" . uniqid() . "." . ($pathInfo['extension'] ? $pathInfo['extension'] : 'jpg');

                try {
                    rename(Yii::getAlias("@frontend") . '/web' . $record->content, Yii::getAlias("@frontend") . '/web' . $newPath);
                    $record->content = $newPath;

                    $record->{$foreignKeyName} = $this->owner->getPrimaryKey();
                    $record->save();
                } catch (ErrorException $e) {

                }
            }
        }
    }
}