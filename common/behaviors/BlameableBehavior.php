<?php
namespace common\behaviors;

use Yii;
use \yii\behaviors\BlameableBehavior as BaseBlameableBehavior;

class BlameableBehavior extends BaseBlameableBehavior
{
    /**
     * @inheritdoc
     */
    protected function getValue($event)
    {

        if ($this->value === null && Yii::$app->has('user')) {
            return Yii::$app->user->identity->getCurrentId();
        }

        return parent::getValue($event);
    }
}