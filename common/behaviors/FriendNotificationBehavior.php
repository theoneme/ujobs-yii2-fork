<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.04.2018
 * Time: 16:52
 */


namespace common\behaviors;

use common\models\Notification;
use common\models\user\User;
use common\models\UserFriend;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\BaseStringHelper;
use yii\widgets\ActiveForm;

/**
 * Class FriendNotificationBehavior
 * @package common\behaviors
 */
class FriendNotificationBehavior extends Behavior
{
    /**
     * @var string
     */
    public $template = null;

    /**
     * @var string
     */
    public $routeSlugParamName = null;

    /**
     * @var string
     */
    public $routeSlugParam = null;

    /**
     * @var string
     */
    public $route = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->template === null) {
            throw new InvalidConfigException('The "template" property must be set.');
        }

        if ($this->route === null) {
            throw new InvalidConfigException('The "route" property must be set.');
        }

        if ($this->routeSlugParam === null) {
            throw new InvalidConfigException('The "routeSlugParam" property must be set.');
        }

        if ($this->routeSlugParamName === null) {
            throw new InvalidConfigException('The "routeSlugParamName" property must be set.');
        }
    }

    /**
     * @return void
     */
    public function afterSave()
    {
        /** @var User[] $friends */
        $friends = $this->owner->user->friends;

        if(!empty($friends)) {
            foreach($friends as $friend) {
                $notification = new Notification([
                    'to_id' => $friend->id,
                    'from_id' => $this->owner->user_id,
                    'thumb' => $this->owner->user->getThumb('catalog'),
                    'custom_data' => json_encode([
                        'user' => $this->owner->user->getSellerName(),
                    ]),
                    'template' => $this->template,
                    'linkRoute' => [$this->route, $this->routeSlugParamName => $this->routeSlugParam],
                    'withEmail' => false,
                    'is_visible' => true
                ]);

                $notification->save();
            }
        }
    }
}