<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:27
 */

namespace common\controllers;

use common\models\Job;
use common\models\Setting;
use common\models\user\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class BackEndController
 * @package common\controllers
 */
class BackEndController extends Controller
{
    /**
     * @var array
     */
    public $_settings = [];

    /*
     * @var array
     */
    private $allowedModeratorsControllers = [
        'admin' => [
            'update' => true,
            'index' => true,
            'update-ajax' => true,
            'render-public-skill' => true,
            'render-public-language' => true,
            'render-public-portfolio' => true,
            'render-public-specialty' => true,
            'emails' => true,
            'live' => true,
            'dialog' => true,
            'view' => true,
            'message' => true,
            'send-mail-ajax' => true,
            'adverts' => true,
            'history' => true,
            'index-tender' => true,
            'update-tender' => true,
            'render-attributes' => true,
            'item' => true,
            'option' => true,
            'detail-category' => true,
            'detail' => true,
            'extra' => true,
            'delete-item' => true,
            'flush-cache' => true,
        ],
        'image' => [
            'upload' => true,
            'upload-profile' => true,
            'upload-wide' => true,
            'delete' => true
        ],
        'job' => [
            'update' => true,
            'index' => [
                'status' => [
                    Job::STATUS_REQUIRES_MODERATION,
                    Job::STATUS_DRAFT,
                    Job::STATUS_REQUIRES_MODIFICATION
                ]
            ],
            'auto' => [
                'status' => [
                    Job::STATUS_REQUIRES_MODERATION,
                    Job::STATUS_DRAFT,
                    Job::STATUS_REQUIRES_MODIFICATION
                ]
            ],
            'drafts' => [
                'status' => [
                    Job::STATUS_REQUIRES_MODERATION,
                    Job::STATUS_DRAFT,
                    Job::STATUS_REQUIRES_MODIFICATION
                ]
            ],
            'render-faq' => true,
            'render-extra' => true,
            'render-additional' => true,
            'render-attributes' => true,
        ],
        'tender' => [
            'update' => true,
            'update-ajax' => true
        ],
        'ajax' => [
            'attribute-list' => true,
        ],
        'category' => [
            'subcat' => true,
        ],
        'site' => [
            'index' => true
        ],
        'order' => [
            'index' => true,
            'view' => true,
            'tender' => true
        ],
        'payment' => [
            'index' => true,
        ],
        'stat' => [
            'index' => true
        ],
        'video' => [
            'index' => true,
            'create' => true,
            'update' => true
        ],
        'seo-advanced' => [
            'index' => true,
            'create' => true,
            'update' => true
        ],
        'live-dialog-member' => [
            'index' => true,
            'dialog' => true
        ],
        'livechat' => [
            'index' => true,
            'emails' => true
        ],
        'banner' => [
            'index' => true,
            'create' => true,
            'update' => true
        ],
        'job-search' => [
            'index' => true,
            'tree' => true,
            'step' => true,
            'option' => true,
            'entity-list' => true,
            'job-list' => true,
            'delete-tree' => true,
            'delete-step' => true,
            'delete-option' => true,
        ],
        'user-portfolio' => [
            'index' => true,
            'update' => true,
            'render-extra' => true,
            'old' => true
        ],
        'page' => [
            'index' => true,
            'create' => true,
            'update' => true,
            'articles' => true,
            'file-upload' => true,
            'images-get' => true,
        ],
        'conversation' => [
            'index' => true,
            'orders' => true,
            'view' => true,
            'create' => true,
            'update' => true,
            'delete' => true,
        ],
        'message' => [
            'index' => true,
            'view' => true,
            'create' => true,
            'update' => true,
            'delete' => true,
        ],
        'withdrawal-type' => [
            'index' => true,
            'update' => true,
        ],
        'payment-method' => [
            'index' => true,
            'update' => true,
        ],
        'file' => [
            'upload' => true,
            'delete' => true
        ],
        'user-withdrawal' => [
            'index' => true,
            'update' => true,
            'view-history' => true
        ],
        'attribute' => [
            'requires-moderation' => true,
        ],
        'attribute-value' => [
            'index' => true,
            'update' => true,
            'delete' => true,
            'merge' => true,
        ]
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAllowed();
                        }
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],*/
        ];
    }

    public function isAllowed($controllerToCheck = null, $actionToCheck = null)
    {
        $isGranted = false;
        if (!Yii::$app->user->isGuest) {
            /* @var User $identity */
            $identity = Yii::$app->user->identity;

            switch ($identity->getAccess()) {
                case User::ROLE_ADMIN:
                    $isGranted = true;
                    break;
                case User::ROLE_MODERATOR:
                    $controller = $controllerToCheck ? $controllerToCheck : Yii::$app->controller->id;
                    $action = $actionToCheck ? $actionToCheck : Yii::$app->controller->action->id;
                    if (isset($this->allowedModeratorsControllers[$controller][$action])) {
                        $isGranted = true;
                    }
                    break;
                default:
                    break;
            }
        }
        return $isGranted;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $cache = Yii::$app->cache;

        $settings = $cache->get('site_settings');
        if (!$settings) {
            $settingsAR = Setting::find()->all();
            $settings = ArrayHelper::map($settingsAR, 'key', 'value');

            $cache->set('settings', $settings, 1000);
        }

        $this->_settings = $this->view->params['settings'] = $settings;

        if (Yii::$app->request->isAjax) {
            $this->disableAjaxAssets();
        }

        return parent::beforeAction($action);
    }

    /**
     * @return bool
     */
    protected function disableAjaxAssets()
    {
        $this->layout = false;

        Yii::$app->assetManager->bundles = [
            'yii\bootstrap\BootstrapPluginAsset' => false,
            'yii\bootstrap\BootstrapAsset' => false,
            'yii\web\JqueryAsset' => false,
        ];

        return true;
    }
}