<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:27
 */

namespace common\controllers;

use common\components\Utility;
use common\models\Category;
use common\models\Event;
use common\models\Payment;
use common\models\PropertyClient;
use common\models\user\User;
use common\modules\store\models\Order;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\View;

/**
 * Class FrontEndController
 * @package common\controllers
 */
class FrontEndController extends Controller
{
    /**
     * @var array
     */
    public $_settings = [];
    public $layout = "@frontend/views/layouts/main";

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@frontend/views/site/error.php'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if (!isGuest() && Yii::$app->user->identity->site_language !== Yii::$app->language) {
            User::updateAll(['site_language' => Yii::$app->language], ['id' => userId()]);
        }
        return parent::afterAction($action, $result);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!$this->checkBlocked()) return false;
        $this->checkManualRedirects();

        if (!$this->fixLanguage()) return false;
        if ($this->route !== 'site/index') {
            Yii::$app->session->remove('returnUrl2');
        }
        if (!in_array($this->route, ['page/outsource-mpp', 'store/cart/index', 'site/error'])) {
            Yii::$app->session->remove('returnUrl3');
        }
        Yii::$app->cacheLayer->prepareCache();

//        $this->_settings = $this->view->params['settings'] = Yii::$app->cache->get('site_settings');

        if (!Yii::$app->request->isAjax) {
            if (!$this->fixCaseSensitivity()) return false;
            if (!$this->fixExtraSpaceUrl()) return false;
            if (!$this->fixUrl()) return false;
            Yii::$app->opengraph->image = Url::to("/images/new/og_default.png", true);
            $this->view->registerLinkTag(['rel' => 'manifest', 'href' => '/manifest.json']);
        } else {
            $this->disableAjaxAssets();
        }

        $this->updateLastAction();
        $this->checkReferralParam();
        $this->checkEmailActionParam();
        $this->registerLivePayment();

        return parent::beforeAction($action);
    }

    /**
     * @return mixed
     */
    protected function updateLastAction()
    {
        if (!isGuest()) {
            /* @var $user User */
            $user = Yii::$app->user->identity;
            if ($user->company_user_id !== null && $user->companyUser !== null) {
                $user->companyUser->updateAttributes(['last_action_at' => time()]);
            } else {
                if (random_int(0, 10) > 5) {
                    $user->updateAttributes(['last_action_at' => time()]);
                }
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkEmailActionParam()
    {
        $emailAction = Yii::$app->request->get('email-action');
        if ($emailAction !== null && !isGuest()) {
            $eventExists = Event::find()->where(['code' => Event::EMAIL_CLICKED])->andWhere(['<', 'created_at', time() - 3600 * 24])->exists();
            if ($eventExists === true) {
                Event::addEvent(Event::EMAIL_CLICKED);

                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    protected function checkBlocked()
    {
        if (!isGuest()) {
            /* @var $user User*/
            $user = Yii::$app->user->identity;
            if ($user->getIsBlocked()) {
                Yii::$app->user->logout();
                Yii::$app->session->setFlash('error', Yii::t('user', 'Your account has been blocked'));

                $this->redirect(['/site/index']);
                return false;
            }
        }
        return true;
    }

    /**
     *
     */
    protected function checkReferralParam()
    {
        $invited_by = Yii::$app->request->get('invited_by');
        if ($invited_by !== null && User::find()->where(['id' => $invited_by])->exists()) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'invited_by',
                'value' => $invited_by,
                'expire' => time() + 3600 * 24 * 15
            ]));
        }
        $client_id = Yii::$app->request->get('client_id');
        if ($client_id !== null && PropertyClient::find()->where(['id' => $client_id])->exists()) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'client_id',
                'value' => $client_id,
                'expire' => time() + 3600 * 24 * 15
            ]));
        }
    }

    /**
     * @return bool
     */
    protected function checkManualRedirects()
    {
        $redirects = [
            '/rasprodazha-paket-iz-35-ti-kursov-ot-bm-biznes-molodost-589c315f15d15-job' => Url::to(['/job/view', 'alias' => 'kursy-biznes-molodost-rasprodazha-5912f787ba4d9']),
            'site/index' => Url::to(['/'])
        ];

        $currentUrl = Url::current();
        if (array_key_exists($currentUrl, $redirects)) {
            $this->redirect($redirects[$currentUrl], 301)->send();
        }

        $currentUrl = Yii::$app->request->pathInfo;
        if (array_key_exists($currentUrl, $redirects)) {
            $this->redirect($redirects[$currentUrl], 301)->send();
        }

        if (strpos($currentUrl, '@20') !== false) {
            $cleansedUrl = str_replace(['@20'], '', trim($currentUrl));
            $this->redirect($cleansedUrl, 301)->send();
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function fixLanguage()
    {
        $languageParam = Yii::$app->request->get('app_language');

        if (!Yii::$app->request->isAjax && !Yii::$app->request->isPost && in_array($languageParam, Yii::$app->params['supportedLocales'])) {
            $locale = array_search($languageParam, Yii::$app->params['supportedLocales'], true);
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'language',
                'value' => $locale,
                'expire' => time() + 60 * 60 * 24 * 30,
            ]));
            Yii::$app->language = $locale;

            $leftPart = explode('?', Yii::$app->request->getUrl())[0];
            $rightPart = explode('?', Url::current())[0];

            if ($leftPart !== $rightPart && Yii::$app->request->cookies['language'] !== null) {
                $this->redirect(Url::current());
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function fixCaseSensitivity()
    {
        /* @var Utility $utility */
        $utility = Yii::$app->utility;
        $params = Yii::$app->request->queryParams;
        $hasUppercase = $utility->hasUpperCase($params);

        if ($hasUppercase === true) {
            $this->redirect(array_merge(["/{$this->route}"], $utility->arrayToLower($params)), 301);
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function fixExtraSpaceUrl()
    {
        $currentUrl = urldecode(Url::current());

        $hasExtraSpaces = preg_match('/[-]{2,}/', $currentUrl);

        if ($hasExtraSpaces) {
            $currentUrl = preg_replace('/[-]{2,}/', '-', $currentUrl);
            $this->redirect($currentUrl);
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function fixUrl()
    {
        return true;
    }

    /**
     *
     */
    protected function registerEcommercePayment()
    {
        $paymentId = Yii::$app->session->remove('paymentId');
        if ($paymentId !== null) {
            $payment = Payment::findOne((int)$paymentId);
            if ($payment !== null) {
                $paymentData = json_encode(['id' => $payment->id, 'revenue' => $payment->total, 'currency' => $payment->currency_code]);
                Yii::$app->view->registerJs("
                    ga('require', 'ecommerce');
                    ga('ecommerce:addTransaction', " . $paymentData . ");
                ");
                foreach ($payment->orders as $order) {
                    $orderData = [
                        'id' => $payment->id,
                        'name' => $order->product->getLabel(),
                        'price' => $order->total,
                        'quantity' => $order->orderProduct->quantity,
                        'currency' => $payment->currency_code,
                    ];
                    if ($order->product_type == Order::TYPE_JOB_PACKAGE) {
                        $orderData['category'] = $order->product->job->category->translation->title;
                    }
                    if (in_array($order->product_type, [Order::TYPE_JOB, Order::TYPE_TENDER, Order::TYPE_SIMPLE_TENDER])) {
                        $orderData['category'] = $order->product->category->translation->title;
                    }
                    $orderData = json_encode($orderData);
                    Yii::$app->view->registerJs("
                        ga('ecommerce:addItem', " . $orderData . ");
                    ");
                }
                Yii::$app->view->registerJs("
					ga('ecommerce:send');
                ");
            }
        }
    }

    /**
     *
     */
    protected function registerLivePayment()
    {
        $session = Yii::$app->session;
        $paymentId = $session->get('livePaymentId');
        if ($paymentId !== null) {
            $payment = Payment::findOne((int)$paymentId);

            $session->remove('livePaymentId');
            if ($payment !== null) {
                $paymentData = ['id' => $payment->id, 'revenue' => $payment->total, 'currency' => $payment->currency_code, 'orders' => ''];
                foreach ($payment->orders as $order) {
                    $paymentData['orders'] .= "ID: {$order->id}, {$order->product->getLabel()} ({$order->total} {$payment->currency_code}) x{$order->orderProduct->quantity}" . PHP_EOL;
                }
                $orderData = json_encode($paymentData);
                Yii::$app->view->registerJs("
					Live.addPayment($orderData);
                ", View::POS_LOAD);
            }
        }
    }

    /**
     * @return bool
     */
    protected function disableAjaxAssets()
    {
        $this->layout = false;

        Yii::$app->assetManager->bundles = [
            'yii\bootstrap\BootstrapPluginAsset' => false,
            'yii\bootstrap\BootstrapAsset' => false,
            'yii\web\JqueryAsset' => false,
            'frontend\assets\CommonAsset' => false,
        ];

        return true;
    }

    /**
     * @param mixed $object
     * @param array $templates
     * @return bool
     */
    protected function registerMetaTags($object, $templates = [])
    {
        if (!empty($templates)) {
            $title = $templates['seo_title'];
            $description = $templates['seo_description'];
            $keywords = $templates['seo_keywords'];
        } else {
            $seo = $object->getDefaultSeo();

            $title = $seo['title'];
            $description = $seo['description'];
            $keywords = $seo['keywords'];
        }

        $page = Yii::$app->request->get('page');
        if ($page !== null) {
            $title = Yii::t('app', 'Page {number}', ['number' => $page]) . " - {$object->translation->title}. " . Yii::t('app', 'Professional`s service uJobs.me');
        }

        Yii::$app->opengraph->description = $description;
        $this->view->title = Yii::$app->opengraph->title = $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => $description], 'description');
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $keywords], 'keywords');
        if (!($object instanceof Category) && strpos($object->getThumb(), '/images/new/locale/' . Yii::$app->language . '/no-image.png') === false) {
            Yii::$app->opengraph->image = Url::to($object->getThumb(), true);
        }

        return true;
    }

    /**
     * @param mixed $object
     * @param array $templates
     * @return array
     */
    protected function buildSeoArray($object, $templates = [])
    {
        return [
            'h1' => !empty($templates['heading']) ? $templates['heading'] : $object->getLabel(),
            'subhead' => !empty($templates['subheading']) ? $templates['subheading'] : $object->translation->content_prev,
            'upperBlockHeading' => !empty($templates['custom_text_1']) ? $templates['custom_text_1'] : Yii::t('app', 'You can simply order {subject}', [
                'subject' => $object->translation->title
            ]),
            'upperBlockSubheading' => !empty($templates['custom_text_2']) ? $templates['custom_text_2'] : Yii::t('app', 'Our freelancers will help you do {subject}', [
                'subject' => $object->translation->title
            ]),
            'upperBlockColumn1' => !empty($templates['custom_text_3']) ? $templates['custom_text_3'] : Yii::t('app', 'On our site you can choose a talented specialist or order a service on {subject} cheap', [
                'subject' => $object->translation->title
            ]),
            'upperBlockColumn2' => !empty($templates['custom_text_4']) ? $templates['custom_text_4'] : Yii::t('app', 'Ask chosen freelancer to start your order. Pay the service. Tell required details for adorable completion of {subject}', [
                'subject' => $object->translation->title
            ]),
            'upperBlockColumn3' => !empty($templates['custom_text_5']) ? $templates['custom_text_5'] : Yii::t('app', 'When specialist completes {subject}, you need to accept the job and leave review. Specialist recieves money when you accept the job', [
                'subject' => $object->translation->title
            ]),
            'textHeading' => $templates['custom_text_6'] ?? '',
            'textSubheading' => $templates['custom_text_7'] ?? '',
            'text' => $templates['content'] ?? ''
        ];
    }
}